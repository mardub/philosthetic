1924 979

when the chord of ID major is reached at the words 
‘once more,’ this feeling of unusualness disappears, 
and everything subsequently sounds familiar again. 
The next section contains, at the words ‘serene in 
changeless prime,’ one of the most sonorous 
passages in choral music—a kind of analogy to 
the passage, He was bruised for our iniquities’ 
from ‘The Messiah.’ In the last portion the poet 
and the composer are, strictly speaking, at variance. 
The poet says, ‘If I am not to meet my friend after 
death, then may I sleep never to awake.’ In fact 
he suggests the hateful doctrine of Nirvana, in which 
the sleep of death is not the thrilling prelude to 
Life, but a state of eternal annihilation. But 
Parry’s faith refuses even to consider such a creed, 
and the words ‘eternal be the sleep’ arouse in him 
feelings of sublime happiness, because he believes 
the sleep of death to be a period of joyful expecta- 
tion. So too he fills the word ‘waken’ with all 
the wonder and beauty of dawn. And this simple 
faith, learnt among the unsophisticated country 
folk of Gloucestershire, shines clearly in his 
treatment of each of the ‘Songs of Farewell.’ It is 
the unaffected creed of a wise and healthy man. 
These Songs use the musical phraseology of To-day, 
but they speak to ‘all men independent of creed, 
time, or race. ‘They are born of English thoughts, 
they drew their beauty from our English streams 
and fields, but the thoughtful listener can hear in 
these large utterances the voice of Bach, of 
Beethoven, of Palestrina, and of all great ones who 
have pondered on the ways of God to man

THE ART OF ACCOMPANYING SONGS 
J. Foss

music is strewn with the wreckage of operas which 
failed owing to inadequate libretti. Even the art of

sea and their seal-skins instead of going back to the| Beethoven and Weber could not instil real life into

king’s palace to confront their stepmother. But let 
that pass. Legend No. 3 concerns a Water Kelpie

are some figures concerning the works performed last

season: Pianoforte quintets, seven; string quintets, 
two; pianoforte quartets, three; string quartets, 
twenty; oboe quartet, one; pianoforte trios, seven ; 
pianoforte and violin sonatas, six ; works for pianoforte 
and violoncello, four ; for pianoforte and oboe, one ; 
for two pianofortes, two ; pianoforte solos, ten ; violin 
solos, one; miscellaneous vocal and instrumental 
works, two. Mozart (eleven), Beethoven (ten), and 
Brahms (seven), headed the list. The concerts are 
at 6.30, and admission is free (but of course there 
is a-collection). The College is easy to get at, being 
one minute from Mornington Crescent Station, and 
within touch of tram and bus routes. We add that 
at the same place a series of lectures on ‘Music and 
its Makers’ is now being given by Mr. David 
Cooper (weekly until next June), on Fridays, at 8.30, 
at a fee which works out at a copper or two per 
lecture. The synopsis shows the lectures to be just 
what some of our ‘ musical appreciation’ inquirers need

1oo4

Other vocal records are of Joseph Hislop, singing | 
very expressively in a couple of pieces from ‘ Lucia | 
di Lammermoor,’ 12-in. d.-s. ; Clarence Whitehill in | 
‘Wahn! wahn!’ from ‘The Mastersingers,’ and| 
‘Der Augen Leutchtendes Paar,’ from the ‘ Valkyrie,’ | 
12-in, d.-s. (fine singing, far above the average in| 
manly dignity and expressiveness) ; John McCormack 
in Schertzinger’s ‘Love Song of Old Mexico’ and 
Jones’s ‘Indiana Moon,’ 12-in. d.-s. (terribly common- 
place music, sung with a sentimentality that really 
deserves the epithet ‘snivelling’; it is difficult to 
believe that the singer is the undoubted artist who 
has been acclaimed lately at Queen’s Hall. The 
H.M.\. bulletin says that ‘there is probably no 
other singer who can render these songs in the 
manner of John McCormack.’ It is a pity that any 
singer of his standing should render them at all ! 
Selma Kurz in Taubert’s ‘ Vogel im Walde’ with flute 
obbligato by John Amadio, and the familiar Bird Song 
from Handel’s ‘I] Pensieroso,’ with Saint-Saéns’s 
cadenza, 12-in. d.-s. (two typical pieces of coloratura 
singing of the rather obvious bird-cum-flute type) ; 
Elsie Suddaby in two Purcell pieces—‘ Hark the 
echoing air’ and ‘When I am laid in earth,’ 
1o-in. d.-s. (with much of the right brilliance in the 
first, but too little ease in the second); and George 
Baker in Elgar’s ‘ Pipes of Pan’ and Eric Coates’s 
‘Pepita,’ with orchestral accompaniment, 1o-in. d.-s. 
‘excellent, manly singing, some of the best baritone 
recording I have heard lately). The only serious 
fault with most of the above records is in the matter 
ofarticulation. The words come through in most 
tases only fitfully, and as I have said before in this 
column, I cannot be persuaded that there is any 
real excuse for the defect, seeing that so many 
humorous singers somehow contrive to let us hear 
every syllable

In the way of choral singing there is a 10-in. d.-s. of 
the de Reszke Singers in ‘Adoration,’ by Beethoven 
\an arrangement by H. Johnson of the well-known

Creation’s Hymn’), and ‘On Wings of Song’ (a 
transcription of Mendelssohn’s song, by van der 
Stucken). The music is not particularly interesting 
in this form, and I suggest that (at all events for the 
| English market) these accomplished singers should 
find better material

aside from high and noble sentiment. Is it the craze 
for examinations that has contributed to the present 
conditions

Plans are already laid for a great Beethoven festival in 
1927. Shall we rise to the occasion ?—Yours, Xc

10, Westwood Road, FRANZ SOMERS

ff boys in the choirs of Cathedrals; and this question he 
ymsidered not only in relation to the music, but with 
reference to the boys themselves. ‘.\ choir boy,’ he 
said—‘I mean now a boy called upon to assist at one, or 
more often two, services every day—has, in comparison | 
with other boys, his chances of receiving a good education 
seriously diminished. A large portion of his time, and | 
more of his energy, are consumed not merely in the all

but exclusive study of one difficult and engrossing art, 
in the exercise of that art as a profession.” To remed\ 
this evil he proposed the employment of women in place 
f boys; and urged as a strong reason for so doing that 
the second parts in the great choral works of Ifaydn, 
Mozart, Beethoven, Spohr, and even Mendelssohn, are 
uniformly written for the contralto, or lower female voice. 
lhe address was listened to with the greatest attention

Sbarps and flats

197, Elmhurst Mansions, Clapham, S.W. 4

Highgate Village Orchestra. Practices every 
evening at North Hill Hall (33, North Hill), Highgate, 
N.6. Vacancies for good strings, flute, oboe, bassoon, 
and _ horn. Programme includes Beethoven’s fifth 
Symphony and ‘ Prometheus’ Overture, ‘ Ballet Egyptien,’ 
“Reine de Saba,’ Butterworth’s ‘Shropshire Lad,’ and 
German’s ‘Merrie England.’—Applications to Mr. 
PereER FARQUHARSON, ‘ Heather Hill,’ Bloomfield 
Road, Highgate, N.6

Brotherhood Orchestra, North Hill Hall (33, North Hill), 
Highgate, N.6, has vacancies for new members, ladies 
and gentlemen. Tuesday evenings. —Conductor, Mr. 
PeTER FARQUHARSON, ‘ Heather Hill,’ Bloomfield 
Road, Highgate, N.6

Trebles, altos, tenors, and basses desirous of assisting at the 
fortnightly evening services at St. Paul’s Church, 
Aldgate, E., are asked to communicate with F.R.C.O., 
22, Shelley Avenue, Manor Park, Essex. Good music

Besides assisting the choral performances of the Croydon 
Philharmonic Society, the Croydon Symphony Orchestra 
has chosen the following works for its season’s programme : 
Borodin’s Symphony in Lb minor; Beethoven’s 
Symphony ; d’Erlanger’s Vianoforte Concerto (Pouishnoff) ; 
Holst’s ‘Perfect Fool’ Ballet; ‘Italian Serenade’ and 
*Willo’ the Wisp,’ by W. H. Reed; Borodin’s ‘ Prince Igor’ 
Overture and Dances; and ‘ I’heeton,’ by Saint-Saéns

Good

Albert Coates ; November 24, Wilhelm Furtwangler; 
December 8, Bruno Walter; January 12, Vladimir 
Shavitch ; February 9 and 16, Furtwiangler; March 9 
and 23, Felix Weingartner ; April 6, Georg Schneevoigt ; 
May II and 25 (the ‘Choral’ Symphony with the 
‘anharmonic Choir), Serge Kussewitzky

Ivimey, on December 11, February 19, and April 23. 
The programmes include Holbrooke’s ‘ The Birds of 
Rhiannon,’ Cowen’s ‘ Concertstiicke ’ for pianoforte and 
orchestra, (:oossens’s ‘ Symphonietta,’ and two Tone 
Poems by Frank Bridge. 
AMATEUR ORCHESTRA Of Lonvon.—Concerts_ at 
Kingsway Hall on January 19 and May 4, under 
Mr. Wynn kReeves. Symphonies, Concertos, Xc. ; 
Moussorgsky’s *‘ Une Nuit sur le Mont Chauve.’ 
BARCLAY’s BANK Musica Society (AMATEUR).— 
Concerts at Queen’s Hall on December 12 and April 9. 
The ‘ Pathetic’ Symphony, Coleridge-Taylor’s Ballade 
in A minor, Moussorgsky’s ‘Une Nuit sur le Mont 
Chauve,’ Xc.. under Mr. Herbert Kouse. Also choral 
music under Mr. H. W. Pierce. 
CIVIL SERVICE ORCHESTRA announces a concert at 
Queen’s Hall on November 27, under Mr. B. Patterson 
Parker. Haydn’s * London’ Symphony, Beethoven’s 
fourth Pianoforte Concerto (Miss Irene Scharrer), and 
Handel’s ‘ Water Music

TH

EALING CHORAL AND ORCHESTRAL Society (Mr. A. C. 
Praeger).—*‘Ave atque vale’ (Stanford); * Walpurgis 
Night’; “The Desert’; *‘ The Spectre’s Bride’; * Hero 
and Leander

1026 THE MUSICAL TIMES—NovemsBer 1 1924 
EALIn -HILHARMONI Society (Mr. E, a ea Cnorat Sociery (Mr. Vincent Thomas) 
Williams).—* Athalie’; *The Dream of Gerontius >| —‘Be not afraid,’ ‘I wrestle and pray,’ ‘ Blessing 
Carmen.’ glory, and wisdom,’ and ‘‘iod so loved the world: 
EvtHamM Cnorat Society (Mr. Ernest Leeds).—‘* The (Bach); “Cum sancto spiritu’ (Mozart) ; “The Princess’ 
Rebel Maid’ (Montague Phillips). | Stanford) 3 St. Cecilia’s Day and *Acis and Galatea’ 
FIN EY Musica Socrery (Mr. A. C. Cundell).—|.. (Handel) ; Liebeslieder Walzer Brahms), 
or ; | WILLESDEN GREEN AND CRICKLEWOOD CHORAL Sociry 
\ Tale of Old Japan | ; . : M . bd, 
. R (Mr. F. W. Belchamber).—* Merrie England’; ‘The 
a ROW AND GREENHI CHot Sx ETY (Mr. F. W. Redemnti ; ? 
elchamber).—' Stabat Mater’ (Rossini); * The | wyscank Cy, 
vw ddine cr). eee Mec! — soe “1, , te WIMBLEDON Cuorat Sociery (Mr. Kenneth A. Brown) 
=cic y rr st ciean ; i.e ochnes 4 ‘ ; a . 1 : » 
om vill rt 100 acvean es Cloches de —‘ Hiawatha’; * The Golden Legend. 
Corneville | WIMBLEDON PHILHARMONIC SociRTY (Mr. F. Wilment 
. P . } _ \ ae ‘ “ > on r - ‘ 
biGH A Ci SOCIETY Mr, F. Cunningham Woods). | Bates).—‘A Tale of Old Japan’; ‘The Wedding of 
John Gilpin’ (Cowen). Shon Maclean’; part-songs. 
L.N. E.R. Mustcat Society (Col. W. Johnson Galloway). | Woopsipe Cuorai Sociery (Mr. N. Appleton).—‘ The 
—*‘ Christmas Eve’ (Dr. Stanley Marchant). Bard of Avon’ (Gaul); ‘The Wedding of Shon 
L X CHorat Sociery (Mr. Arthur Fagge).—Mass Maclean’; * Il’haudrig Crohoovre.’ 
in D (Beethoven); ‘The Kingdo miscellaneous. | ROVINCIAL SOCIETIES 
; . . ) n ~ : Ir. BARNSLEY Str. CectLiaA SocrETy.—’ Acis and Galatea’: 
Ilenry Kiding).—* The Last Judgment The Kebel cenen: * The Mesto’: ‘The A la § 
Maid’ (M yntague | hilli »S gis titi * eS rg poe a 
, “5 en ee BATH CHORAL AND ORCHESTRAL Socigery (Mr. H. T, 
\ , < ' “ane) The . se . ’ ° 
Mint FItit M Al. Society (Mr. L. A, Cane rhe Sims).—* The Spectre’s Bride. 
Messiah. BELFAST PHILHARMONIC Society (Mr. E. Godfre 
) ‘ Sr. Mary Cray Cuora AND 3rown).—* Elijah’; * Toward th: nknown Region 
ORCHI Ss ' Mr. Sydney Smith).—* The |} (Vaughan Williams); ‘To Wonder’ (Norman Hay); 
Pied Piper of Hamelin’ (Walthew); ‘Christmas Day’ *The Dream of Gerontius’ (conducted by Sir Henry 
Holst ‘Fantasia on Christmas Carols’ (Vaughan Wood). 
Williams) ; Passion Music (Haydn). BIRMIN iM Pacn Sociery (Mr. J. Bernard Jackson),— 
AN I) r ¢ RA LN .) 1ES \! * Praise the Lord, all ve heathen.’ * If thou but sufferest 
Sor Mr. Allred B. Choat).—Three concerts at (;od to guide thee,’ ‘ King of Heaven. he Thou welcome’ 
the Crystal Palace. ‘.\ Princess of Kensington’; * Tom Bach): * King Arthur’ (Purcell); “Whatsoe’er ye do 
Jones’; * King Olaf.’ Buxtehude). 
| b’s PALACE CHORAL Si \wp OkcHESTRAL | BIRMINGHAM FesTIvAL Cuorat Society (Mr. Adrian C. 
_— ames” e - a ius? *Sea’ Sv =" 
Sociery (Mr. Frank Idle).—‘ Elijah,’ ‘Cavalleria a ; = Dream of Gerontiu Sea fur ae 
Rusticana’; ‘The Pied Piper of Hamelin Parry \ aughan ili ams) 5 Blake ( iraham Godfrey); 
saat? (Mastin): ©The Messiah ° Mass in B minor; * The Messiah. 
: “a : > oe ;o1.TON CHOR JNion (Mr. 7 as Booth and 
LHARMON Cuorr (Mr. C. Kennedy Scott) Boi HORAL UNIO {Mr ae : ae 
ed 9 : ~—aste Ml tales Mr. Hamilton Harty).—‘ The Rebel Maid’ (Montague 
Concerts at ‘Jueen’s Hall « November 13, the i nen .- 4 - %, 
: . i + , Phillips): ‘The Messiah’; ‘At the Fastern Gate 
B minor Mass: May 21, Henschel’s ‘ Requiem, (Maclean! ‘Kubla Khan’ Colerid Tavlor 
% 6 > ? sc > . %. BR | Mé é ; uDdla a age - ay . 
Brahms’s ‘ Alto Rhapsody,’ and ‘St. Patrick’s Breast- ‘Elijah.’ . ” a yio 
plate,’ by Arn ax; ance of > B : .. . . . 
plat by Arnold Bax ; performance of the : minor Bosrow CworaL Society (De. Gordon A. Sleter< 
Mass for children on November 29; Delius’s ‘ Mass of | : ; hah , ges e 
of : aos . : ys) The Messiah’; St. Matthew’ Ilassion. Monthly 
Life’ for the Royal Philharmonic Society on April 2 ; : - 
i. ~. ohiagr : ors chamber concerts and song recitals. 
the ‘Choral’ Symphony for the London Symphony | . . weer » 2 
Sutbenten én Vow on “| BRADFORD FEsTIVAL CnHoraAt Society (Dr. E, C 
cnes ay <>. nad rs 8 a ’ ‘ - 
; Bairstow).—* A Vision of Life’ (Parry); ‘The Pie 
-ntsenenellig ay 7 ae. mo » ORCHESTRA Piper of Hamelin’; ‘Elijah’; ‘Stabat Mater 
wy } \ . ; } : or the P . 4 eee 
evan © Sens W. ils rab Hiaw atha I , the | (Stanford) : Mystical Songs (Vaughan Williams). 
Fallen”; “Cavalleria Rusticana fom Jones’; * The} Braprorp O_p CHorat. Society (Mr. Wilfrid Knight 
Messiah. | —* Requiem’ (Verdi) ; "The Messiah’; * Samson an 
PURLEY CHORAL UNION (Mr. Harold Macpherson).— | Delilah.’ 
*Blest Pair of Sirens’; ‘The Golden Legend’;| Bruitiovse Cuorat Society (Mr. W. Singleton).— 
*Requiem’ (Brahms). * The Sun- Worshippers’ (Bantock) ; ‘ Ode to the North: 
Sr. JAMEs’s Cuorat Soctrry, EpMonron (Mr. J. W, East Wind ’ (Cliffe). 
Barran),.—*‘ The May (Jueen’; * Olivet to Calvary.’ BRIGHTON AND Hove Harmonic Society (Mr. Percy

S t LONDON PHILHARMONIC Society (Mr. William H. 
Kerridge). ‘Elijah’; ‘Stabat Mater’ (Dvorak); 
‘Hymn of Jesus’ (Holst): “Hymn to Dionysus’ 
(Holst); ‘The Pied Piper of Hamelin’ (Parry); 
part-songs and madrigals

LAUNCESTON CHORAL Society (Mr. C. Stanley Parsonson). 
—*The Messiah’; * Hiawatha,’ Parts I and 2

LeepDs CHORAL UNION (Dr. Henry Coward).—* The 
Flying Dutchman’; *‘ The Messiah’; * Faust’ (Berlioz) ; 
Mass in D (Beethoven); ‘ Blest Pair of Sirens

Leeps PHILHARMONIC Society (Dr. E. C. Bairstow).— 
“Sea ’ Symphony (Vaughan Williams) ; ‘ The Messiah’ ; 
* Requiem’ (Verdi

LEICESTER CHORAL UNION.—‘ The Kingdom’; ‘ Song of 
Destiny’ (Brahms); ‘Choral Fantasia ’ (Beethoven). 
LEICESTER Musicat Society (Mr. C. Hancock

Elijah’; ‘St. Matthew’ Passion

ScCUNTHORPE CHORAL AND ORCHESTRAL SOCIETY (Mr. 
H. C. Burgess). —‘ Hiawatha’; ‘ Elijah

SHEFI Musica Dr. Henry Coward),— Mass 
in I) (Beethoven); ‘Sleepers, wake’ (ach); ‘The 
lessiah’; ‘Samson and Delilah

SITTINGBOURNE AND Disrricr MustcaL Society (Mr

Bruckner’s music reflects the same trait as did his organ 
playing. His nine Symphonies are of inordinate length, 
some of them requiring an hour and a half for performance. 
He was a pioneer of the ‘colossal’ in Teutonic music. 
London heard the third Symphony for the first time in 
r8or, and the seventh in 1887, both under Richter. The 
works were not so enthusiastically received as in Germany 
and Austria, Vienna gave Bruckner an honorary degree of 
Doctor, the occasion being an imposing féte in his honour. | 
He died in the same city on October 11, 1806, leaving | 
instructions for his ‘Te Deum’ of 1844 to be used as the 
Finale of his ninth and last Symphony

Bruckner was a ponderous kind of worker, his output 
being small in number but bulky in form. Besides the 
Symphonies, there are a few Masses, a String ()uintet, and | 
some detached pieces. He was a man of extremely simple 
heart and outlook. In conversation he was said to be unable 
to sustain any subject but music. He never married, 
because, he said, his own childhood had been so wretched | 
that he could not accept the responsibility of founding a 
family until it was too late tor his consideration. A 
monument to his memory was erected in the Vienna 
Stadtpark in 1899. Bruckner Festivals were held at Linz, 
where he was organist to the Cathedral, in 1902 and 1904, 
The city voted a fund for annual performances of his works | 
for twenty years after 1904. A Festival at Munich in 1909 | 
consisted entirely of Beethoven and Bruckner Symphonies. 
Miniature scores of some of Bruckner’s works, also

ithe Reger), and we had

symphony pianoforte transcriptions, are obtainable, ang 
probably wil! receive the attention of those interested jn 
a composer whose music was once pitted against that of 
Brahms and which shared a whole Festival with Beethoven

London Concerts

Miss Jelly d’Aranyi touched noble heights of emotion it

ithe slow movement of the Beethoven Violin Concerto o

the Royal night. The last movement seems to me one of 
those in which Beethoven was not at his best; but many 
will doubtless differ about that

Miss Anna Hegner is another extremely capable violinist

ultimate victor

in Beethoven’s ‘ Coriolanus

and women getting leave of absence from business. This 
year Mendelssohn’s ‘ Ruy Blas’ Overture brought five bands, 
two approximating reasonably to the description ‘ fyjj 
orchestras,’ and those two able to give really good per. 
formances. The others, not so strong numerically, resorted to 
the device of ‘ cueing in,’ some instruments making shift for a 
Although here full sonority was 
lacking, and some themes fell strangely on the ear in theirnew 
colouring, yet there was abundant evidence of true musicianly

RDIFF.—The Council of University College of South 
Wales and Monmouthshire has decided that the chamber 
concerts given on Saturday evenings, which have hitherto 
been carried on with the voluntary help of members of the 
College staff, shall in future be regarded as a necessary part 
of the work of the musical department. The members of 
the instrumental trio also undertake concert work in towns 
and villages in the area served by the College. The concerts 
during the coming winter will number twenty-five

BIRMINGHAM.—The City of Birmingham Orchestra | 
opened its season with a Symphony Concert on October 38, 
Taking his place for the first time as permanent conductor, 
Mr. Adrian C. Boult drew from his players no more than a 
passable performance of Brahms’s Symphony in C minor, 
There was too little light and shade in the playing, and it 
lacked the imaginative qualities without which Brahms is | ; f 
ull and grey. Strauss’s ‘Don Juan’ was hardly eager CHACEWATER.—A musical society has been formed for 
enough, but in ‘The Flying Dutchman’ Overture the | general instruction in music, under the direction of Mr, D. 
Orchestra was heard to great advantage. The novelty of | Behenna, organist of St. Agnes’ Parish Church, 
the concert was Armstrong Gibbs’s * Vision of Night.’—— DUNFERMLINE.—The Choral Union has been resus- 
Aseries of nine Saturday afternoon children’s concerts has | cjtated, and Mr. A. M. Henderson, organist at Glasgow 
een arranged. At the first of these Mr. Boult explained | University, has been appointed conductor. 
each piece before it was played, and showed a real under- | ; ; 
standing of the child-mind. An audience of school-children| EXr1ER.—The season opened on September 29 with 
crowded the hall.——At the first popular Saturday night | two concerts, given by the London String Quartet, at one of 
concert the third * Leonore’ Overture of Beethoven and the} Messrs. Paish’s series

second ‘ Arlésienne’ Suite of Bizet were included in the | — os 
programme. Miss Winifred Browne, whose technique | HARROGATE.—Mr. Basil Cameron and the Municipal 
improves at every hearing, played Rimsky-Korsakov’s | Orchestra are making Harrogate a musical centre of some 
Pianoforte Concerto. ——At the Sunday concert at the | portance. The weekly Symphony Concerts maintain a 
Futurist Theatre the orchestra gave some really fine playing | high standard in their programmes of classical and modern 
in Beethoven’s second Symphony and the * Shropshire | Works. The following have been | recently 5 or 
Lad’ Rhapsody of Butterworth. Mr. Samuel Saul sang a | oe s sme ee Saar Mn aca Reso 9 
‘Fidelio’ Aria ¢ so )vorak songs, ——Th idland | “100s fOr pianorone and Orchestra (Mis ing ’ 
Musical Rectang ~ bee see br a of he ‘on Cowen’s ‘The Butterfly’s Ball,’ the " Jupiter’ Symphony, “a 
October 6. In order to offer seats at cheap prices the | Harp Concerto by Pierné (Miss Hilda Atkinson), Arensky’s 
rchestra was dispensed with, and Mr. G. D. Cunningham | V@Flations ona theme by Tchaikovsky, Delius’s On Hearing 
substituted on the organ. ——At one of the Mid-day concert the First ( uckoo es Spring and Summer Night, Mozart's 
series the Elizabeth Trio gave some charming madrigals. |‘ Ein kleine Nachtmusik,’ and Beethoven’s first and third

Symphonies. 
BOURNEMOUTH.—Sir Dan Godfrey opened the thirteenth | ~? P

season of symphony concerts at the Winter Gardens on | IpswicH.—The season of municipal music opened on 
lctober 9, with a programme typical of Bournemouth | October 8, when the Conservatoire Ensemble Class, con- 
music: Beethoven’s seventh Symphony, de Falla’s ‘ Three- |ducted by Mr. Sydney Robjohns, played movements from 
Cornered Hat’ Suite, Rachmaninov’s D minor Concerto} Purcell’s ‘Golden’ Sonata, Mr. Noel Ponsonby played 
played by Miss Maud Agnes Winter), and Chabrier’s| Bach organ music, and the Ipswich Male Choir sang 
‘Gwendoline’ Overture. A feature of the season is to be| Walford Davies's ‘Hymn before Action’ and Grieg’s 
aseries of appreciation lectures for children, given by Sir | ‘ Landerkennung.’ Mr. A. H.. Welburn gave shore 
van Godfrey. | explanations of the music

THE MUSICAL

also sung 
by Miss Greta Scott, played music 
Wagner in Rushworth Hall on October 15. In Crane 
Hall, on the same date, Mr. Frank Bernand (pianoforte), 
Mr. Louis Cohen (violin', and Miss Ethel 
(vocalist) gave a recital.——Visiting artists have included 
maninov, Moiseiwitsch, and Madame Galli-Curci

by Beethoven

Rac

which gave Manchester its first true impression of the range

of Miss Olga Haley’s art. I have frequently written of the 
interpretative power of the Catterall (Quartet in the latest 
Beethoven (Juartets. On October 15 the players were even 
more convincing in the C sharp minor. Four * Lancashire 
Sketches ’ by George Whittaker, of Rochdale, had a genuine

Lancashire melody as the basis of the opening number

a 
seventeen of the Moericke songs at a mid-day recital o 
October 17, Mr. R. J. Forbes being at the pianoforte, 
the opening Brand Lane concert, Rachmaninov played bjs 
third Concerto, and Sir Dan Godfrey made his (I think) fry 
appearance here. C.H

NEWCASTLE.—The Chamber Music Society’s hundrej 
and ninety-second concert, on October 17, was provided 
by the Léner Quartet, who played Dohnanyi’s Quartet jp 
D flat, Haydn’s F major, and Beethoven’s F major,—~ 
The local branch of the B.M.S. listened to a lecture op 
October 11, given by Mr. Hubert J. Foss, on the music 
Peter Warlock (Philip Heseltine). The membership of th 
branch is sixty-five

PLYMOUTH.—On September 27 Mr. David Parkes: 
Orpheus Society, consisting of three hundred voices, ap 
the Royal Marine Band, conducted by Mr. P. S, G 
©’Donnell, gave an operatic concert, with selections frop 
* Paradise and the Peri,’ ‘ A L.ife for the Czar,’ and ‘ Faust

Albert Sammons.——F our concerts are announce: by th 
Southport United Choir. 
SOUTHAMPTON.—At University College, on Octobe

15, Beethoven’s second Sonata for violin and pianoforte 
pianoforte music by MacDowell, and songs by Baumer 
Walthew, and Sinding were performed.——On October 1; 
Shirley Parish Choir sang sea chanties, including ‘Ri 
Grande’ and ‘Shenandoah.’ A Trio for clarinet, ’cello, an 
pianoforte, by Mr. A. IP. McDonell, was played, with th 
composer at the pianoforte.——A choir of three hundre 
senior children from the Central Schools, conducted by 
| Mr. F. Permain, sang in the Coliseum on October16. The 
items included ‘The lark’s awake,’ ‘ Lift thine eyes,’ an 
“The Viking Song

Mr. L. Webb as conductor. Hitherto Tetbury has hadn 
musical organization. 
| THAME.—The Choral Society, suspended during recer 
| seasons, has been revived, and *‘ Hiawatha’s Wedding-Feast 
is in rehearsal

The general aspect of musical affairs has considerably 
changed owing to the return of normal conditions. The 
number of orchestral concerts announced for the season is 
very large, but their quality will not satisfy those who are 
tired of always hearing the same works. Many foreign 
artists who had avoided Germany on account of the bad 
economic situation are again available, but they are not 
bringing anything new with them. Ossip Gabrilowitsch, 
remembered as a pianist who had been particularly in favour 
with the public, made his reappearance as conductor of the 
Berlin Philharmonic Orchestra, giving good interpretations 
of some classical and fairly modern works

The death of Busoni has struck deep. The public con- 
sidered him a great virtuoso, and musical circles acknowledged 
him as composer. His musical legacy is important. What 
will become of his ‘Faust,’ which he did not live to finish? 
Busoni was one of those problematic artists who, proceeding 
from one experiment to another, leave the great mass of 
music-lovers behind them. His mind was never at a stand- 
still; but it is the definite quality of stability which after 
all affords the public enjoyment. It is curious to see how 
this man, in his first period, wrote music that was striking 
in its freshness, and how at a certain moment his spontaneity 
became undermined by intellectual processes which never 
came to finality. At a Busoni commemoration held by one 
of his pupils, the pianist Edward Weiss, the Roth Quartet 
gave a very animated performance of the String Quartet, 
Op. 19, a work full of the vitality and gaiety of Busoni’s 
earlier manner. It was very well received. In the 
*Geharnischte’ Suite, a symphonic work of four movements, 
performed at the first Philharmonic concert of the 
season under Furtwangler, we see the process of Busoni’s 
creative mind much more developed. This Suite was 
written at Helsingfors, when the composer came under the 
influence of northern men and countries. Though martial 
rhythms are predominant, the local landscape is reflected in 
the music. Finnish and Russian composers had undoubtedly 
impressed their characteristics on Busoni. At this time 
colour became personal to him, and with it thoughtfulness 
without sentimentality. On the whole the Suite produced 
a rather unexpected effect upon the visitors at the 
Philharmonic concert. Like subscribers in general, they do 
not, however, want to be troubled by problematic works, 
and remain attached to their Beethoven and Brahms

On this occasion we heard three songs by Richard 
Strauss, thin in substance but very melodious. Their 
melody has nothing to do with the poetic words by

Bruckner and Schonberg were both prominent in the 
exposition assembled for the Festival, and displayed in | 
the Municipal Museum under the collective title, ‘From 
Bruckner to the Present Generation.’ One room was | 
completely occupied by rare and interesting manuscripts of | 
Bruckner, ranging from his school-books to the ounagpagen | 
of his great composition. Another room peacefully united 
Johannes Brahms and his great opposite, Hugo Wolf, and | 
another was dedicated to Gustav Mahler. Schonberg and | 
his pupiis—Alban Berg, Anton Webern, Paul A. Pisk, 
Hans Eisler, Egon Wellesz, and Rudolf Réti—occupied a | 
special room, and among noteworthy objects were a portrait | 
of Schinberg, by Oscar Komoschka, and one of Berg, | 
painted by Schonberg himself

TWO STRAUSS PREMIERES 
The Staatsoper’s share in the Music Festival consisted of | 
afew performances of Richard Strauss’s older works and of | 
operas by various Austrian composers. These productions | 
of frequently - heard works were designated ‘festival | 
performances,’ but they differed in nothing from the] 
ordinary répertoire interpretations. The same true of | 
what the Staatsoper ambitiously termed a ‘complete | 
Mozart cycle,’ in which a ‘ restudied’ * Magic Flute’ was no | 
better, scenically and musically, than the usual Staatsoper 
standard. There have been two novelties at the | 
Staatsoper so far, both staged for the Festival, and both, 
significantly enough, works of Richard Strauss. 
One of these was a re-shaping of ‘Le 
gentilhomme, the Prelude to ‘Ariadne auf 
Theatres would not produce a play-opera which called 
for a double cast. so Strauss has made it all opera, using 
and expanding the original incidental music of ‘ Le bourgeois 
gentilhomme.’ The music proved of very light weight, 
hampered in fact by the rather obsolete and poorly-adapted 
Moligre play. Some delicate instrumental colour and 
graceful rhythm proved, however, as is customary with 
Strauss, a redeeming feature ; but it must be said that by 
far the best passages were those which Strauss frankly 
borrowed from the music which Jean Baptiste Lully once 
wrote for Molitre’s comedy—e.g., the Minuet in G major 
which furnishes the Prelude for the second Act. Undeniably 
this Strauss premiere was a failure, which assumed such 
forms as to cause visible discomfiture of the composer, who 
presided at the conductor’s desk. 
The second Strauss novelty was a ballet entitled ‘ Die 
Ruinen von Athen,’ with music based on Beethoven’s 
work of that name, and on the same composer’s ‘Die 
Geschipfe des Prometheus.’ Hofmannsthal, Strauss’s 
librettist, had written a scenario designed to combine the 
Music of both ballets, and Strauss arranged the music— 
much on the lines of modern musical comedies hashed up 
from the immortals, The feature of the entire piece is the 
plendid opportunity it affords for the display of fine 
Eroupings and choregraphic art, and Heinrich Krdller, the 
hallet-master, made the best of his chances. Even better 
nthe same respect was Gluck’s ‘Don Juan’ ballet. This

is

in an opus number. You ask why, in that case, they are

grouped. The answer is merely that a composer may feel 
that wk is too small to be dignified by an opus 
number, so he makes up a set. Beethoven was given to 
gr g works in threes, ¢.g., his Op. I consists of three | 
Trios; his Op. 2, three Pianoforte Sonatas; Op. 9, three 
Trios ; Op. 12, three Sonatas for violin and pianoforte—and 
there ar samples in his list of works

AW B.—YVou sa live in the country, are fond of 
music, cannot practise much or take lessons, ‘ intend to

rnold)—very full on the biographical side; * Brahms,’ 
J. A. Fuller- Maitland (Methuen)—the most thorough ont 
critical side; * Brahms,’ by I. C. Colles, deals briefly w

all his chief works; ‘ Mozart,’ by Otto Jahn (Novello); ! 
Haydn, see ‘.\ Croatian Composer,’ W. II. Hadow; a 
Daniel Gregory Mason’s ‘ Beethoven and his Forerunner 
‘Handel,’ by Streatfeild; and ‘Georges Frideric Hande 
by Newman Flower—a recent book that contains a gre 
deal of fresh matter, illustrations, facsimiles, Xc. We 
not know the publishers of some of the above. The boot 
may be obtained, however, through Novello

Duet.—For effective and not difficult duet arrangemen 
of chamber works, try the Haydn Trios, in the lete 
Edition ; for a difficult modern chamber work, the Kas 
Quartet; for moderately difficult orchestral transcriptior 
Haydn’s and Mozart’s Symphonies; for difficult orchest 
works, Franck’s Symphony, Elgar’s ‘ Enigma’ Variation 
Elga:’s Symphonies Nos, 1 and 2, also the same compose 
Introduction and Allegro ; and Edward German’s * Thee 
and Six Diversions.’ All these are published by, or 4 
obtainable through, Novello. In next month’s 
7imes our contributor ‘ Feste’ will be discussing the pia? 
forte duet in general, and will no doubt mention oth 
duets likely to be useful to readers like yourseli who # 
anxious to explore this delightful form of musical activity

