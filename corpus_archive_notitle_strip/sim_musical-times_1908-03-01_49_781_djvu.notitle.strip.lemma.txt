their staff of highly - skille workman , their work- | , TUESDAY , at 7.45 . work by Sir C. Hubert H. Parry ( conduct 

 by the ymposer ) , " the loto Eaters , " * * Blest Pair of Sirens , 
 ¢ $ 2 z iz " es . be W ) E alsew - Symphony ( the Cambridge ) , song . 
 shop and appliance , be without equal elsewhere in | WEDNESDAY , at thro . " the kingdom " ( Elgar ) ; Symphony 
 urope . in G , Op . 88 ( Dvorak ) , & c. ; 
 E I WEDNESDAY , at 7.45 . * * Jephthah ( Carissimi , 1600 - 1674 ) ; 
 Elijah , " no . ro to 20 ; scene from * * Phacbus and Pan ' ( Bach ) 
 the most celebrated instrument , especially those in | « the Raven , " New ( Bertram Shapleigh ) . 
 . PFHURSDAY at 7.45 " the buried Song , New ( Krug 
 the possession of great player , have be frequently | Waldsce ) ; Pianoforte Concerto in b flat minor ( Tchaikovsky ) ; Violi 
 ( erto ( Beethoven ) ; sonata in G , Op . 78 , for Pianoforte and Violin 
 entrust to they for repair . ( Brahms ) Zin 

 PrincivaAL artist 
 Miss AGNES NICHOLLS , Madame KIRKBY LUNN , Mlle . 
 ROSA OLITZKA , Mr. JOHN COATES , Mr. WILLIAM GREE 

 CHERUBINI in ENGLAND . ' musical Memoirs , ' say : ' Cherubini , who 
 select and compose this opera , be a scholar of 
 Luigi Carlo Zenobi Salvatore Maria Cherubini | Sarti ; he be a young man of genius , and the 
 to give his full name — be bear in florence,|overture and the duet in the third Act , give 
 September 14 , 1760 , and die at Paris , March | promise of future greatness . ' the next event of 
 15 , 1842 . his industrious life of eighty - two year | cherubinic importance be thus announce in the 
 cover an interesting period in the history of | /d / ic Advertiser of Saturday , April 2 , 1785 . ' at 
 1usic lo quote Sir George Macfarren : ' his | the King ’s ' Theatre , Hay - market , will be perform 
 ingularly long career of activity comprise a great / an entirely new Comic Opera , in two \ct , 
 poch in musical history , during which the art entitule Za Finta Principessa , the music be 
 pass through some of its most important change . | the first essay of Sig . Cherubini in this country , 
 he be busy as a composer before Mozart was|The same journal criticise the ' essay ' in the 
 otherwise acknowledge than as a youthful prodigy , | follow term : 
 ind after Verdi have found his reputation . he 

 this Burletta , which we doubt not may be , as it be say it 
 witness the beginning and the end of Beethoven ’s 

 ine ; ' _ — be , the ' first essay of Cherubini in this department of his 
 ibour , and of those of Weber , Rossini , and | art , ' make a large addition to his professional fame . 
 Boieldieu . ' Macfarren go on to say : ' he be _ Most of the music be pretty ; much be eminently so — the 
 still more distinguished by the opinion of his | " sts and yet more , the second air of Franchi , be as fanciful 

 he in term of respect ; anc 

 Beethoven write to 

 the father of Mendelssohn take the boy to Paris ! judge by its repetition during the season , ' La 
 for Cherubini to decide whether his indication of | Finta Principessa , ' Cherubini ’s ninth opera , achieve 
 erit warrant his dedication to the study of } success . it be interesting to notice that Gluck ’s 
 music . in regard to Cherubini ’s creative career , ' Orpheus ' be also perform at this time ( do 
 Professor add : * he compose a Mass with|Cherubini conduct it ? ) , the advertisement 
 success before he be thirteen , and compose a announce it as ' the music by Sir Christopher 
 string quartet when he be seventy - seven , and even | gly < k , with the addition of the celebrated John 
 this be follow by some other though small } Christ . Bach . ' accord to Burney ' in the 
 piece " — a truly remarkable man . summer [ 1785 ] the whole opera machine come to 
 Cherubini have just enter upon the twenty - fifth piece , and all its spring , disorder by law suit , 
 vear of his life when he pay his first visit to ! warfare . and faction , be not collect and 
 England . how came he to receive an invitation | regulate till the next year . ' verily , verily , the 
 to become ' composer for the Italian Theatre ' in | sea of operatic enterprise be a very stormy one . who 
 london ? his english biographer , Mr. Edward | chall estimate its wreck ? . 7 
 Bellasis , * say that ' it be through the connection | after a visit to Paris , Cherubini return to 
 of Sarti ( Cherubini ’s master ) with England , where | England ( his second visit ) in October , 785 . for 
 his ( Sarti ’s ) opera at this period find acceptance , | the performance of Paisiello ’s ' 1 Marchese 
 ind thank also to the reputation already acquire , | Tulipano’—on January 21 , 1786 — he insert six 
 that Cherubini have receive an invitation to visit| air of his own composition . Parke say that 
 l.ondon professionally . ' but be it not probable | Signor Babbini , the celebrated tenor , and Signora 
 that Earl Cowper ( George Nassau Clavering,| Sestini both make their first appearance here in 
 the third Earl ) may have have some influence | this opera ' under the direction of Cherubini . ' he 
 n cause Cherubini to set foot on our shores?|add : Babbini possess a pleasing voice and 

 a general history of music . ' by Charles Burney . 1750 

 Cherubini for the composition of a symphony , an 
 overture , and a concerted vocal piece to italian 
 word . Cherubini , whose reputation at that time 
 be second only to that of Beethoven , accept 
 the commission and the invitation — which it may 
 be presume accompany the financial proposal 
 and leave Paris on February 25 , 1815 , for London . 
 accord to the diary he keep during this visit his 
 composition be as follow 

 February [ 1515 ] and 

 Lindley - - - dA. ror 
 = ' se al volto , ' Mrs. Dickons , Mesers 

 Braham and Naldi . mo - a 
 | Sinfonia - - - Beethoven 

 A 

 Braham , Magrath , and Lacy Dr. © ' , 
 Overture ( Ulysse et Circe ) - B. Romberg . 
 leader , MR . SUAGNOLETTI . Pianoforte , MR . CLEMEN 

 it will be observe that the Beethoven symphony 

 be not specify . the first time any one of 
 the ' immortal nine ' could be identify in the 
 philharmonic programme be on April 14 , 1817 , 
 in No . 6 , call ' Sinfonia Pastorale 

 Parliament consist of Mr. Balfour , Sir John Gorst , 
 Sir Henry Drummond Wolf and Lord Randolph 
 Churchillshe say 

 Sir John have a music - love soul , and many be 
 the occasion when he and I and Arthur Balfour go 
 off to the " Monday Pops " together , to listen to the 
 sweet strain of Joachim and Norman Neruda . my 
 fashionable and frivolous friend , spy the three of we 
 | walk together , would tease I about my " weird " 
 companion , one solemn with beard and eye - glas , the 
 lother esthetic with long hair and huge spat . 
 Mr. Balfour 's knowledge of music be remarkable , 
 | consider the little time he be able to devote to it , 
 |and he be no mean performer at the piano , read 
 | and play classical music . we often play together 
 | Beethoven and Schumann . but it be not without 
 | difficulty that he could get away from his parliamentary 
 | duty , which increase yearly , and often I be 
 | disappointed of his company , as show by the follow 

 letter : 
 | 1883 . House of Commons . 
 my DEAR LADY RANDOLPH 

 CuHoraAL : Jephthah ( Carzssimz ) ; . scene from Phoebus 
 and Pan ( Bach ) ; Baal scene and invocation of rain from 
 Elijah ( A / ende / ssohn ) ; bl pair of Sirens and the Lotus 
 Eaters ( Parry ): the Kingdom ( & /gar ) ; Friihling ’s Chor 
 Hugo Wolf ): the Raven ( Shap / ezgh ) ; and the bury 
 Song ( Avug- Waldsee 

 INSTRUMENTAI Prelude , Der Himmel Lacht ( Bach ) ; 
 Violin concerto ( Beethoven ) ; prelude to act 3 , Lohengrin 
 ( /Vagner ) ; sonata for pianoforte and violin in G , Op . 78 
 ( Brahms ) ; Symphony in G , Op . 88 ( Dzordés ) ; Pianoforte 
 concerto in b flat minor ( 7chatkovsky ) ; Symphony , the 
 Cambridge ( Parry ) ; symphonic poem , Aus Bohmen ’s Hain 
 und Flur ( Swefana ) ; and Musette and Ele¢gie . from King 
 Christian Suite ( S7be / ius 

 Sir Hubert Parry will conduct his own work , the 
 onerous duty of conductor - in - chief of the festival 
 be in the experienced hand of Mr. N. Kilburn 

 with Schubert ’s * unfinished * symphony in b minor 

 Herr Hugo Becker play with remarkable technical skill 
 Volkmann ’s violoncello concerto in a minor ( Op . 33 ) , a 
 work which be of little value as music ; and the concert 
 open with Beethoven ’s ' Egmont ' overture and close 
 a good 
 beginning and a pleasant ending to this eventful music 

 making 

 ena — ' TI belis ritone r solo 

 orcl Fran L.€On 
 S ony inor , no Beethoven . 
 i vi s Elgar . 
 S phonic poem rill Eulenspiegel _ R. Strau 

 again Mr. Wood secure splendid interpretation of the 
 orchestral number . his reading of the C minor Symphony 

 display in the first movement at least of this now classic 
 Concert the second movement be also beautifully 
 lay , but the third movement be scarcely so successful . 
 lhe fine performance of Strauss ’s ' Tod und Verklarung 

 at it have be our good fortune to hear be another 
 Mischa Elman also 
 play Beethoven ’s Romance in F in fine style , and a 
 zlorious performance of the same composer ’s Symphony in A 
 rought to a noble conclusion one of the most successful 

 t give by the London Symphony Orchestra 

 pathetically and with romantic effect . 
 Ji 

 rm part be play by Mr. A. Borsdorf with a 
 nt and finish that contribute in great measure to 
 ire give by the musicia work . the remainder 
 programme Beethoven ’s Quartet in 
 r ( Op . 59 , No ling ’s Pianoforte quintet in 
 Op . Fischer Sobell rendering 

 4 at the 

 THE MUSICAL TIMES.—M , rcu 1 , 1908 . 183 

 MUSIC in VIENNA . include , for the orchestra alone , Beethoven ’s ' Egmont ' 
 overture , the prelude to Wagner ’s ' Tristan and Isolde , ' 
 Debussy ’s * L’aprés - midi d’un faune ' and Sullivan ’s overture 

 from our special correspondent 

 from our own correspondent 

 play by Messrs. Montagu Nathan , Arthur Bolton and | enthusiastic audience . the choir — on this occasion 
 F. H. Sawyer ( organist)—a Trio of Beethoven ’s , and solo } augment to seventy voice — give , under Mr. William 
 for violin and violoncello . | Morris 's conductorship , a number of part - song with excellent 

 _ M. Eugene Ysaye , with Miss Phyllis Lett and Mr. Charlton | artistic result , especially in German 's ' peaceful night , ' 
 Keith , give , on February 4 , a concert which form one of | Emlyn Evans ’s ' Ye captive tribe ' ( * the Captivity ' ) and 
 the series of Phillips ’ Subscription Concerts . the playing | James ’s ' Land of my father . ' Miss Gwladys Koberts create 
 of the great violinist almost reach perfection , while | an extraordinary impression with her luscious voice and 
 Miss Lett ’s selection of song be admirable and worthily fascinating style . Miss May John , the possessor of a light 
 sing . | but pleasing soprano voice , Mr. David Ellis and Mr. David 

 nder the able direction of Mr. George Riseley the Society of 
 Instrumentalists , with the aid of local professional musician , 
 nterprete several composition : some well - know favourite , 
 ther be hear in the city for the first time . special interest 
 be feel in respect to Mezart ’s Violin concerto No . 7 , which 
 be introduce to the public a short time ago , Mr. Harold 

 Bernard play the solo instrument , the four cadenza 
 introduce having be write by he for the performance . 
 the other unfamiliar work present be the viennese 
 dance by Beethoven . of these , six be perform , 
 he remain five be reserve for the next concert . 
 ther work be Mendelssohn ’s ' calm Sea and prosperous 

 Hall on 

 on February 8 an immense audience assemble at 
 Coiston Hall for the third concert of the Bristol Choral Society . 
 the choir and band number upwards of 500 performer , 
 and Mr. George Riseley direct the performance of 
 two work which be receive with much enthusiasm — 
 Max Bruch ’s ' Lay of the Bell ' ( soloist , Madame Esta 
 d’Argo , Miss Frederica Richardson , Mr. Lloyd Chandos and 
 Mr. Harry Dearth ) and Parry ’s ' Pied Piper of Hamelin ' 
 ( soloist , Messrs. Lloyd Chandos and Harry Dearth ) . both 
 cantata be admirably perform , especially the second , 
 in which the choir be highly successful in its interpretation 
 of the picturesque production 

 the second concert of the Bristol Symphony Orchestra 
 be hold at the Victoria Rooms on February 12 . form 
 during the past twelve month , the Society include 
 professional player of Bristolt _ with some from Bath , the 
 on this 
 occasion Mr. A. H. Peppin , music - master at Clifton College , 
 be the conductor . there be a numerous attendance , and 
 considerable enthusiasm prevail in respect of some of the 
 composition perform . Beethoven ’s seventh Symphony , 
 Wagner ’s Prelude to ' Die Meistersinger ' and ' ride of the 
 valkyrie , ' and Tchaikovsky ’s Valse from the opera ' Eugen 
 Onegin " and ' Marche Slave ' ( Op . 31 ) form the chiet 
 feature of the programme . Miss Edith Evans , the vocalist 
 of the evening , effectively render Schubert ’s ' Die 
 Allmacht ' and ' Senta ’s ballad ' from the ' Flying 
 Dutchman 

 liberal patronage be accord to the concert in aid of 
 the Postmen ’s Benevolent Fund , hold at the Victoria Rooms 
 on February 14 . among those who take part be 
 Miss Nellie Ellis , Miss Ethel Hook , Mr. Frank Webster 
 and Mr. Dan Price ( vocali : s ' . and Miss Marjorie Evans 
 ( the child violinist ) , with Mr. W. = . Fowler ( accompanist ) . 
 the performance be of a gratify ch > racter , and several 
 extra have to be give through the persistent applause 

 MUSIC in DUBLIN . 
 from our own correspondent 

 at the Antient Concert Kooms , on January 23 , the Dublin 
 Philharmonic Society give its first concert under 
 the conductorship of Mr. Charles Marchant , organist of 
 St. Patrick ’s Cathedral . the work perform include 
 Coleridge - Taylor ’s ' Hiawatha , ' Mendelssohn ’s ' hear my 
 prayer ' ( soloist , Miss Emilie Martyn ) and Beethoven ’s 
 ' hallelujah ' chorus ( ' Engedi ' ) . in the absence of an 
 orchestra the accompaniment be admirably play by 
 Mr. Arthur Oulton . the choir , number upwards of 20 
 voice — form partly from the recently disband Orpheus 
 Choral Society — prove to be quite worthy of the 
 occasion , and excellent work will be expect of they . 
 solo be sing by Miss Emilie Martyn , Mr. Dan Jones 
 and Mr. Vine Sanderson 

 on February 5 the Dublin Glee Singers , our old 

 T North City Choral Society ( conductor , Mr. George 
 Harrison ) give a concert in the Rotunda Kooms on February 
 10 , at which Schubert ’s ' Song of Miriam ' ( soloist , Miss 
 Madeline Macken ) and Mendelssohn ’s 13th Psalm ( soloist , 
 Miss Daisy Love ) be the chief feature . Miss Marie 
 Dowse be the solo violinist , and Messrs. H. V. Love and 
 J. \. Love play the organ and pianoforte accompaniment 
 respectively 

 kecital have be give at the Royal Dublin Society by 
 Mr. A. H. Fricker , organ ( Bach ’s Toccata and Fugue in 
 c , & c. ) , the Brodsky Quartet ( Mendelssohn ’s string quartet 
 in E flat , op . 44 , no . 3 , Schumann ’s Quartet in a major , and , 
 with Mr. Clyde Twelvetrees , Schubert ’s String quintet , | 
 Op . 153 ) , Miss Annie Lord , pianoforte ( Bach ’s prelude 
 and fugue in C sharp , Brahms ’s ' Handel ' variation , 
 Beethoven ’s sonata in e flat , Op . 31 , no . 3 , & c. ) , and Mr. 
 Herbert Walton , organ ( Mozart ’s ' clock ' fantasia , Bach ’s 
 Passacaglia , & c 

 the Sunday Orchestral Concerts , under Dr. Esposito ’s 
 spirited conductorship , seem to be firmly establish in 
 popular favour . during the month Madame de Vere Sapio , 
 Mr. Joseph O'Mara and Miss Agnes Nicholls have give 
 their service as soloist , and attract very large audience 

 the Dublin Orchestral Society give the first concert of its 
 tenth season on February 13 . her Excellency the Countess 
 of Aberdeen with a large party be present . the band , 
 under Dr. Esposito , play Nicolai ’s ' merry Wives of 
 Windsor ' overture , Bach ’s Brandenburg Concerto No . 3 , 
 in G , for string , Beethoven ’s seventh Symphony , and 
 Tchaikovsky ’s ' Casse Noisette ' suite 

 M. Eugene Ysaye give a violin recital on February 3 , 
 assist by Mr. Charlton Keith , pianist , and Miss Bingham 

 Don Giovanni , ' Vulcan ’s song from Gounod ’s ' Philémon 
 et Baucis , ' Schumann ’s ' the two Grenadiers , and the 
 Serenade from Gounod ’s ' Faust 

 on January 28 the Edinburgh String ( ) uartet give fine 
 performance of work by Morart ( in e flat ) , Brahms ( in 
 b flat , op . 67 ) and Beethoven ( in G , op . 18 , no . 2 

 at the third Harrison Concert , on February 1 , Madame 
 Albani and Mr. John McCormack be the chief attraction . 
 the other member of the party be Miss Marie Stuart , 
 Mr. Dalton Baker , Miss Vera Margolies ( pianist ) and the 
 \lexandra Ladies ’ ( ) uartet . Mr. Victor Marmont accom- 
 panie 

 give on February 8 , Mr. Ernst Denhof have the assistance 
 of the Rosé String Quartet from Vienna , and , as vocalist 

 include 
 Beethoven 
 ( in D , op . 18 , no . 8) , and Brahms ’s Pianoforte quintet in 
 f minor . Mr. Elwes sing song by Brahms , Blow , Anthony 
 Young and Maude V. White . the accompanist be 
 Mr. Edgar Barratt 

 the second concert of the Edinburgh Amateur Orchestral 
 Society be give on February 10 , Mr. T. H. Collinson 
 the programme comprise the overture to 
 ' Prometheus ' ( Beethoven ) and ' Stradella ' ( Flotow ) , 
 Haydn ’s Symphony in e flat , ' spring - time , ' by Svem 
 bj6rnsson , Mozart ’s Concerto in e flat for two pianoforte 

 Mr. Gervase Elwes . the concerted number 

 programme ! — the piece choose be almost identical with 

 past plébiscite selection , viz . , Beethoven 's fifth Symphony 
 and the Leonora No . 3 overture , the Ballet Air in G fror 
 ' Rosamunde , ' Mendelssohn ’s music to ' a midsummer 
 night ’s dream ' and the overture to ' Tannhiuser 

 on February 6 the Lansdowne United Free Church 
 Musical Association , conduct by Mr. J. E. Hodgson , give 
 a highly successful rendering of Mendelssohn ’s ' Elijah . ' 
 work be do by the chorus and soloist , 
 Mr. Joseph Lycett singe the music of the Prophet with 

 at the 143rd concert of the Sociéta Armonica on January 

 Mr. R. Daeblitz , with Mr. C. C. 22 the principal orchestral piece be Bach ’s Overture in C , 
 supply the accompaniment Dvorak ’s ' Heldenlied ' and Tchaikovsky ’s ' Casse - Noisette 
 Mr. Mack Borthwick , a local baritone vocalist with | Suite . Beethoven ’s Pianoforte concerto in C minor be 
 h artist m , give bis third recital on February 10 , a well play by Miss Lilian Risque , and a young soprano , 
 mewhat unique programme include few than | Miss Edina Thraves , make a distinctly favourable impression 
 twenty - tw ng , 1 by british composer ; of these | by her singe 
 Granville Bantock ’s six ' Jester ' song and Dora Bright 's Mr. Albert . Workman , who be re - establish high - class 
 ‘ s of song from Kipling ’s ' Jungle Book ' be | subscription concert in Bootle , give a preliminary concert 
 t a first hearing in Glasgow another novelty on } on January 27 , at which Mrs. Henry J. Wood , Mr. Willy 
 rogr e be Parry 's ' Partita ' in d minor for violin | Lehmann ( violoncello ) and Mr. Arthur Cooke ( pianoforte ) 
 i I apitally playe y Miss Bessie Spence and | assist . 
 Mr. A. M. Henderson , the latter give also three move at the fourth Ladies ’ Concert of the Orchestral Society 
 n rom | ) ’ Alber forte suite in D minor . onjon February 15 , Sibelius ’s ' Varsang ' or ' Spring song ' 
 Februar e at m Choral Society , under Mr. Alfred | ( Op . 16 ) be perform , and Brahms ’s Violin concerto , sol 
 h uve a praiseworthy performance of Elgar 's ' the ! by Mr. Alfred Ross Mr. Granville Bantock conduct 
 Banner St. George , in add some carefully - choose | special interest centre in the performance of Tchaikovsky ’s 
 idriga : fourth Symphony , conduct by Mr. Landon Ronald . 
 i de Notre Dan Training College — an | the vocalist , Miss Grainger Kerr , sing with much acceptance 
 nbr n record xcellent choral | J. S. Bach ’s Cantata for contralto ' sound your knell , bl 
 , ree public concert on February 19 , | bour of parting , ' a quaint and characteristic Bach air wit ! 
 in additior some vocal solo and instrumental | } a suggestive bell obbligato . Miss Kerr also sing thre 
 c progr ded Wilfred Bendall ’s ' the | musicianly song , with orchestral accompaniment , by Mr. 
 Lady SI , n Holst ’s song from * the l’rincess,’| F. C. Nicholls , a local composer . 
 I i y Elgar and Miss Macirone , and Somervell ’s rhe fourth and final concert of an interesting series of 
 ' Wind ers , a which be charmingly | Chamber Concerts in the Birkenhead Town Hall be give 
 r t Llolst ’s song merit special word of praise | under Mr. Lawrence Atkinson ’s direction on February 18 , 
 wer ng wit uutifal blending and great delicacy | when Madame Sobrino sing and Mr. Joska Szigeti play 
 Max Bruch ’s Violin concerto in g minor . 
 MUSIC in LIVERPOOI ; : 
 MUSIC in LUTON . 
 ‘ t n 
 ( from a correspondent . ) 
 ar t ng « e ! n Feb > ; by the , . . , 
 ' , ' : < few town in the home county be more often ignore 
 l ry Symp ! which Llerr le : 14 . sas 
 M mee “ wae : .~ Os { in the musical world , yet , paradoxically , be well provide 
 SZKOWS } ( ducte ‘ 1 7 mino ‘ er , - : 
 ge ae ; rag " % with good musical society , than Luton . he borough tha 
 Op . 47 ) , e sui rrov reign part , originally write ' . } . ' 
 : ; _ .\ | provide straw hat for half the world possess an old 
 r pianofort et . and the Pianoforte concerto in E ( Op . 59 ) . . . : 
 . Rae + } th . establish Choral Society , an Orchestral Society , and a 
 w the s part be finely play by Miss Dora Bright . | ; ' ? . ¢ . 
 , , ; least two other musical society that annually perform 
 rt celient playing ol r cal Symphony Orchestra 1 : . 4 
 . ue : ' Dl Bioeth Ol » : _ , | work which be often beyond the capacity of the average 
 eserves commer s es .L1SS i ] e , the vocalist . as . nt : 
 p . choral society . special attention be merit , however , by 
 ‘ . — ' ' ithe concert give by the Luton Choral Society on 
 i uharmor nce ) . . . . , $ . 
 Tv - February 5 . invoke the aid of Mr. Henry J. Wood and 
 ir Gor VSky give a poetical reading ae . p ge } 
 | ae . ( = considerable portion of the ( Jueen ’s Hall Orchestra , th 
 1al I conce in J e 

 H bril la nd Society present to an audience of 1,c00 people the most 
 d : rilli jlaved second solo , ; 
 " er ip , e flat ( o - ) | remarkable programme of music that have ever be hear in 

 kill in his performance of 

 e flat , Bach ’s chromatic Fantasia and Fugue in G , and , 
 for an encore , Beethoven ’s Rondo ( Op . 129 

 Dr. Richter resume the conductorship at the concert of 
 February 13 , and secure a delightfully inspiriting rendering 
 the Beethoven Symphony in A. the anniversary of 
 W agner ’s death be commemorate by the performance of 
 s ' ] overture ; and with the singing by Mr. Plunket 
 Greene of the Hans Sachs air , * be duftet doch der Flieder . ' 
 Mr. Willy Hess play Joachim ’s Violin concerto in d minor 

 aust 

 Messrs. Battala ( pianoforte ) , Villers ( violin ) and Bazelaire 
 ( violoncello ) , the Andante from a Sonata for pianoforte and 
 violin , two short piece for violoncello , and a Nocturne and 
 a Valse caprice for pianoforte solo . all these work show 
 nervous warmth of feeling as well as musicianly skill ; but it be 
 Février seem specially to excel . 
 some of these , nine in number , that be sing by Madame 
 Mantelin ( soprano ) and Mr. Baehrens ( baritone ) be 
 strikingly effective from an emotional point of view 

 Mr. Egon Petri , proiessor at the Royal Manchester 
 College of Music , give on January 27 the first of four recital 
 of Beethoven ’s sonata . at the second , on February 10 , as 
 at the previous performance , the large Whitworth Hall 
 f the Victoria University be crowd . M. Petri ’s 
 magnificent technical display excite great interest . in 
 another direction Mr. Edward Isaacs set himself a not less 
 trying task in his pianoforte recital on January 29 , when , in 

 addition to three cleverly - write piece of his own 
 composition , a Staccato - Caprice , a Reverie , and a Scherzo 
 Fantaisie — he play Liszt ’s Sonata in b minor and 
 Rubinstein ’s theme and variation ( Op . 858 ) . 
 music in NEWCASTLE and DISTRICT . 
 ( from our own correspondent 

 be give by the ( ) ueen ’s Hall 
 Orchestra on February 4 , under the direction of Mr. Henry 
 unfortunately a condemnation of the 
 arrangement on the occasion of the granting of the Town 
 Hall license create a scare , which have an adverse efiect 
 upon the sale of ticket , and add another difficulty to the 
 many which seem to prevent the establishment of a series of 
 orchestral concert here . as the Town Hall can not be 
 guarantee for next season , and as there be no other available 
 building , many musical arrangement must be hold in 
 suspense . such a state of affair be lamentabl 

 on the follow evening the conclude concert of 
 Classical Concert Society be devote to a superb pianoforte 
 and violoncello recita ! by Miss Johanne Stockmarr and 
 Professor Hugo Becker , who play sonata by Beethoven 
 and Brahms 

 at the third Harrison Concert , on I 
 be Madame Albani , Miss Marie 
 Baker , and Mr. J. McCormack , Miss Vera Margolies 
 be the solo pianist . three effective and pleasing 
 ' meditation , ' compose by Mr. Alfred Wall for violin and 
 pianoforte , be perform by the composer and Herr 
 Oppenheim at the concert of the Newcastle Musical Society 
 on February 12 ; Brahms ’s Pianoforte quartet and a Haydn 
 String quartet be also perform , and Canon Hughes be 

 Bingham . Mr 

 closely indeed . a most preinising young singer be intro 
 |duce at this concert in Miss Blanche Tomlinson , who , 
 though her resource can hardly be fully develop , have a fine 
 | soprano voice , very even in quality , and she use it very 
 | artistically . at the next Municipal Concert , on February 15 , 
 | Beethoven ’s seventh Symphony be the centre piece of the 
 programme , one of Stanford ’s ' irish ' Rhapsodies , the 
 | ' Midsummer Night 's Dream ' and ' Meistersinger ' overture , 
 and Tchaikovsky 's ' Battle of Poltava , ' be its other more 
 | important feature . 
 | the Rasch Quartet give a concert on February 5 , when 
 | Beethoven 's splendid Pianoforte trio in b flat ( Op . 97 ) be 
 give , together with Brahnis ’s first String quartet and 
 movement from ( ) uartet by Tchaikovsky and Glazounow . 
 it be a matter for congratulation that Leeds can now boast of 
 such capable string quartet as this and the bohemian 
 ( uariet , which , on February 19 , give quartet by Mozart 
 ( k. no . 590 ) , and César Cui ( op . 68 ) , and repeat 
 the very interesting and beautiful quartet by Mr. Arthur 
 Grimshaw , a Leeds musician , which be produce at 
 these concert three year ago . another chamber c 
 of more than common interest be that give by a Leeds 
 pianist , Miss Eisele , on February 8 , when , with the 
 help of that fine artist , the violoncellist Mr. Hugo 
 Becker , she play sonata by Brahms and _ Kichard 
 Strauss , while Mr. Becker ’s exceptional power be show 
 in two movement of a Haydn sonata , the Ada of 
 Schumann ’s Violoncello concerto , and a display piece by 
 | Popper . on February 12 ti :> Leeds Choral Union gavea 
 | concert of unaccompanied chora : .-usic , include Bach 's fine 
 motet ' sing ye to the Lord ' and San.nel Wesley ’s massive 
 ' in exitu Israel . ' Dr. Coward and Mr. Farrer Briggs 
 | conduct , and Mrs. Henry J. Wood , accompany by her 
 | husband , be the solo vocalist 

 a Harrison Concert on January 27 , at which Madame 
 | Albani appear , and the new tenor , Mr. J. McCormack , 
 make his first appearance at Leeds , and one of Messrs. 
 Haddock ’s musical Evenings on February 18 , when Miss de 
 Benici be the pianist , sum up the Leeds concert for the 
 month . a series of performance of Su!livan ’s ' Iolanthe ' by 
 a local amateur society , under the direction of Mr. Percy 
 Richardson , which begin on February 18 , deserve , h ver , 
 to receive brief chronicle 

 Parsifal ' and extract from ' the Flying Dutchman ' and 
 ' Tristan ' be the most conspicuous . on January 24 
 Miss Ada Sharp and Miss E. A. Atkinson give a very 

 interesting recital of violin and pianoforte music , include 
 sonata by Beethoven ( Op . no . 3 ) and Brahms 
 op . 108 ) , and the whole of Schumann ’s ' Kreisleriana . ' 
 a similar concert be give by Mr. S. Midgley , with the 
 o - operation of Mr. Catterall , the young Manchester 
 violinist , on February 19 

 30 

 Mr. Julian Clifford ’s conductorship , the programme include 

 some of the most popular orchestral piece , of which 
 Beethoven ’s fifth Symphony be the chief 

 f the player be admirable . some slight roughness 

 HEIDELBERG 

 the Academical Vocal Society , which be now rehearse 
 Beethoven ’s ' Missa Solemnis , ' under the direction ot 
 Dr. Philipp Wolfrum , have decide to devote an evening 
 during the summer term to the composition of Max Bruch . 
 MEXICO 

 Ricardo Castro , director of the National Conservatory , 
 who recently die after a short illness , be bear at Durango 
 in 1866 . he study in Europe , where he formerly give 
 many concert . he return to Mexico , and be onl ) 
 appoint director in December , 1906 

 CEI in BFLAT 
 : PSCHAIKOWSKY 

 minor ) : - _ i 
 5 . coronation MARCH ISCHAIKOWSKY 2 
 6 . three minuets.—(sympnonige in C , g minor , and 
 E flat ) . an : : MOZARI 
 7 minuet.—{sonata in E Fiat ) . ( op . 31 , iii . ) 
 BEETHOVEN 1 : 
 8 PRELUDE.—(“‘Cotomsa " ) , A.C. MACKENZIE 1 : 
 g. FINALE ( ' ' o may we once again " ) } — " bl pair oF 
 S1RENs " Cc . H. H. PARRY 

 MENDELSSOHN 1 

