ITALIAN OPERA , DRURY LANE 

 M. Amprotse THomas Opera , ‘ Mignon , ” produced 
 establishment 4th ult . , pronounced 
 decided success , result composer reason 
 congratulate , considering incidents 
 libretto , concocted suit palate Parisian 
 public , tampered known 
 French cooks , MM . Michel Carré Jules Barbier , 
 familiar title “ Miznon , ” persons wouldimagine 
 Goethe “ Wilhelm Meister ” work selected 
 desecration . expressed opinion 
 subject , needless 
 present occasion record surprise 
 aspire rank literary men , 
 simple love story , suitable operatic purposes , 
 fastening immortal creations , 
 living , openly protested viola- 
 tion laws honour , works art 
 protected sacred property world . M. 
 Thomas hope , Goethe Shaks- 
 peare merely considered valuable aids 
 securing good opinion public music ; . 
 boldly grapples subject “ Mignon ’ ” — 
 Beethoven Schubert dared 
 lightly touch — perfectly justified , 
 point view , accepting applause operatic 
 audience legitimate reward . Dismissing ques- 
 tion , , looking composer share 
 work , bound record “ Mignon ” 
 M. Thomas best music . 
 means disposed , , rate best 
 highly ; addition grace elegance 
 rus work , variety 
 colouring , artistic treatment orchestra 
 observable , vocal parts comparatively 
 unimportant , attention kept alive , spite 
 conviction , musically speaking , little 
 permanent place world real art . 
 overture , encored , contains skilful instru- 
 mentation , Polacca movement pretty 
 secure applause lovers light music , especially 
 forms good contrast andante , horn 
 solo , precedes . act contains good 
 bustling music , gipsies , Jfignon 
 stolen , important . trio Fi / ina , 
 Guglielmo Laerte effective characteristic 
 situation . Mignon song , “ Non conosci il bel suol ” ( better 
 known French title , “ Connais - tu le pays , ” ) 
 far best solo Opera , tender grace 
 melody expressing sympathetically dreamy recol 

 a. ” ee e 

 second act , sang music| Allegro Vivace , MS . Concerto C ” ( excellently 
 perfect mastery extreme difficulties.| played pianoforte composer , Mr. Shakes- 
 Signor Foli excellent representative Nor-|peare ) , proved admirably training institu- 
 wegian Skipper , Daland , Signor Perotti ( lately | tion brought bear natural gifts 
 successful début Faust ) , created enthu- | young artist . “ Adagio " based winning 
 siasm Lik Hunter . Opera placed subject ; “ Allegro Vivace , ” somewhat 
 stage , Signor Arditi contributed | Weberish character , freely written instrumented 
 success watchful intelligent conducting . | knowledge orchestral effect . Mr. Shakes- 
 r | peare , retiring orchestra , warmly 

 jand deservedly applauded . com . 
 PHILHARMONIC SOCIETY . | mend Recit , Air Mr. Parry , 
 Tue eighth concert Society pre- | effectively sung composer , chorus Mr , 
 sent season given St. James Hall 4th ult . ,| Brion , - song Miss Crawford , showed 
 programe devoted exclusively composi- talent young composers , piece espe- 
 tions Beethoven . directors right cially graceful , written voices . 
 congratulate having vindicated pianoforte performance Mr. Shakespeare , 
 character England charge neglecting mentioned , Miss Linda Seates Mr. Kemp played 
 memory great composer , born hun-| Andante Rondo Mozart Concerto E flat , 
 dred years ago — Mr. Macfarren truly says tyo pianotortes , marvellous precision effect , Miss 
 “ Analytical Programme , ” “ shall | ‘ Townshend gave intelligent reading Hummel 
 vast changes inward constitution outward | Fantasia Indian air , Miss Westmarland performed 
 acceptance musical art wrought with- | Weber Concertstiick decision spirit scarcely 
 period , creations influence | expected young student , Miss Chan- 
 wonderiul genius ? ” task representing differ-| nel } displayed musical feeling facility execu- 
 ent styles uf Beethoven stages career tion Professor Bennett Caprice E. 
 , imagined , extremely difficult ; instrumental piece Beethoven Romance F , 
 far limits ordinary — violin , Mr. Morley sufficiently proved 
 extraordivary — evening concert permit , idea progressing theright road , Miss Ferrari received 
 fairly carried . Commencing Sym-|an ovation refined rendering Meyerbeer 
 phony , aud ending niuth , certainly the| « En vain j'espere , ” Miss Marion Severn , Miss Mauds- 
 extremes style ; considering , addi-|Jey , Miss Rebecca Jewell highly effective 
 tion works , Dervish Chorus |t¢he solos allotted . powers choir 
 “ Ruins Athens , ” Scena , “ Ah Perfido , ” “ Choral | successfully tested Linley Madrigal “ iiark , birds , ” 
 Fantasia , ” Terzetto , “ Tremate , ” Overture | Mendelssohn - song , “ Ruy Blas , ” 
 “ Leonora , ’ question attention of| « listen Carols ? ” brightness soprano 
 listevers severely taxed admit possi-| voices especially observable . ‘ prizes dis- 
 bility close attention music demands . | tributed Mrs. Gladstone , previously address 
 compositions executed . | read Professor Sterndale Bennett , Principal 
 desired better soprano Miss Arabella| Academy , highly satistactory artistic state 
 Smythe , « ‘ Tremate ” Choral Symphony ; | institution particularly dwelt ; 

 Miss Juiia Elton , Mr. Cummings , Mr. Santley 
 gave strength cast , especially sing- 
 ing Bass recitative , commences vocal 
 Choral Symphony . better remember 
 heard . great Scena , “ Ah Pertido ” taxed 
 Madile . Nils - powers utmost , impassioned | 
 portions thrown true dramatic 
 feeling wonder shortcomings in| 
 expressive parts forgotten burst 
 applause wich enthusiastically greeted 
 end . Madame Arabella Goddaid played piano- 
 forte Choral Fantasia utmost delicacy 
 refivement , vocalists sang decision 
 firmness . fair tuo critical ninth 
 Symphony . < perfect performance grand work 
 expected rehearsals ; espe- 
 cially high pitch , spite repeated failures 

 interesting little episode occurred 
 Annual Examination students atthe Royal 
 Academy Music , Saturday 16th wt . pro- 
 ceedings temporarily suspended , Mr. George Mac- 
 farren , body Professors 
 attached Institution , presented Professor Stern- 
 dale Bennett , Principal , address congratulation 
 honour recently conferred 
 University Oxford 

 Miss Erten JARMAN gave concert 
 Hanover Square Rooms , Thursday , 7th ult . T'he 
 bensficiare played standard works Beethoven Woelf , 
 showing pianist musician ability . 
 assisted Madlle . Delise , Madlle . Santos , 
 Miss Lyndhurst ; Messrs. Walter Reeves , C. J. Bixhenden , 
 W. C. Bell , Madame Rivoldi ( Scala\ , 
 voice great volume richness . Mr. Lansdowne 
 Cottell Mr. C. F. Weber conducted ; concert 
 highly successful 

 Mr. Laxspownr Corrett gave fourteenth 
 Concert season Wornum Concert 
 Hall , Store Street , Saturday , 16th ult . , 

 composition , Truhn glee , ‘ ‘ Chafers , ” substituted , 
 humorously rendered , second recall 
 demanded . Mr. H.S. Trego conducted ability , 
 congratulated success concert 

 Mr. ArExanpErR Coopsr Concert took place 
 Beethoven Rooms , Harley Street , Friday evening 
 24th June , large audience . programme 
 interesting , included Mozart Trio K , 
 Beethoven Duet Sonata F , Schumann “ Blu- 
 menstiick ” “ Nachtstiick . ” 
 efficiently renaered concert - giver , assisted 
 Messrs. H. W. Hill ana ‘ Walter Pettit , received 
 hearty applause . vocal portion programme 
 included Handel “ ‘ Cangio d ’ aspetto ” ( sung 
 Miss Marion Severn ) , songs Schumann ( given 
 taste expression Madame Talbot Cherer ) , 
 “ Tn sheltered vale ” ( sung Mr. Chaplin Henry , 
 encored ) , Alex . Cooper recent com- 
 positions — “ yonder wave dear ” ( Barcarole 
 tenor ) , ‘ “ Love Love ” ( canzonet , tenor ) , 
 “ Hope Tears ” ( contralto ) , new M.S. glees . 
 execution , exception ‘ ‘ Hope 
 Tears , ” somewhat betrayed effect insufficient re- 
 hearsal . Haydn Trio G brought concert 
 successful conclusion , beneficiare received 
 ovation 

 Miss Karnertne Poyntz gave annual 
 evening Concert Hanover Square Rooms , 
 7th ult . programme selected , Miss 
 Poyntz received warmest applause vocal solos , 
 sufficiently varied test powers 
 execution expression . vocalists assisted 
 concert - giver Madame Osborne Williams 
 Madlle . Drasdil , Messrs. W. H. Hillier , W. H. Cummings 
 Harley Vinning . instrumentalists Mr. 
 Richard Blagrove ( concertina ) , Mrs. Richard Blagrove 
 Mr. W. G. Cusins , pianoforte 

 object entertainment value 
 powers national instrument Wales , triple - stringed 
 harp , specimens unaltered ancient Welsh 
 compositions , vocal instrumental , crowning incident 
 night considered event history music — 
 viz . , performance concerto composed Handel 
 celebrated Welsh harper Powell , play George IL . 
 valuable manuscript recently discovered British 
 Museum Mr. Brinley Richards , executed 
 marvellous precision memory triple harp Wales , 
 Herr Sjiéden , eminent performer pedal harp , 
 accompaniments violins , viola , violoncello , double 
 bass . played original score Handel MS . , 
 consists movements , “ Allegro , ” abounds 
 passages difficult puzzle modern players harp ; 
 graceful melodious ‘ Larghetto ; ” “ Finale , ” 
 quaint charming specimen great composer 
 mirthful humour ; , unlike ‘ ‘ concertos , ” 
 “ scored ” stringed instruments . Herr Sjiden , 
 repeated , played entire work memory , , 
 remarkable , p!ayed Welsh triple - stringed harp , 
 instrument composed . accompaniments 
 carefully performed ( Mr. Weist Hill , violin ) 
 direction Mr. Brinley Richards , exertions known 
 labour love 

 Tue Concert Herr Lehmeyer , well- 
 known pianist , took place 18th ult . , Beet- 
 hoven Rooms . programme devoted wholly 
 works Beethoven Schubert . singing 
 Mesdlles . Doria appreciated 
 evening . quartet canon “ Fidelio ” 
 ladies , Mr. Stedman Herr Deck , especially 
 sung , received unanimous encore , Mr. Bene- 
 dict refined accompaniment contributing 
 success piece . Mr. Stedman gave “ Sei mir gegrust ” 
 ( Schubert ) , highly artistic manner , com- 
 poser “ ‘ Wanderer ” finely rendered Herr Deck . 
 Herr Lehmeyer assisted Sig . Scuderi ( violin ) , 
 Mons . Albert ( violoncello ) . artists Mdme . 
 Thaddeus Wells Miss C. James . Mr. Parker 
 Herr Ganz conducted 

 Tue Concert season S. Mark 
 Choral Association took place School Rooms , 
 Rawstorne - street , City - road , 29th June . 
 performances choir extremely successful , in- 
 cluding “ Sanctus ” Bartniansky , “ Pilgrims ” 
 ( Leslie ) , ‘ Sweet low ” ( Barnby ) , “ Ave Maria ” ( Henry 
 Smart ) , “ Dawn Day ” ( Reay ) , “ Hear Prayer ” 
 ( Mendelssohn ) , solo capitally sung 
 Miss Maria Langley . ‘ soft southern breeze , ” 
 Mr. Barnby new Cantata “ Rebekah , ” effectively 
 given Mr. Stedman ; Mr. Paget sang 
 bass solos . Liszt arrangement airs “ Rigoletto ” 
 played Miss Florence Martin , per- 
 formance Herr Jacoby violin highly appre- 
 ciated . Mr. James Robinson conducted usual , 
 Mr. J. Tunstall accompanist 

 list new works promised 
 forthcoming Birmingham Festival ( given June 
 number ) , add Choral Ode Dr. 
 Stewart , composed expressly occasion . 
 mention miscellaneous evening concerts , 
 Mendelssohn Concerto G minor played 
 Madame Arabella Goddard , “ Kreutzer ” Sonata 
 given Madame Goddard M. Sainton , 
 Oveitures “ Der Freischiitz , ” ‘ Zampa , ” “ Guil- 
 laume Tell , ” performed . announced 
 Wednesday evening , second 
 concert consist entirely selections works 
 Beethoven 

 Mr. Ettts Roserts gave Concert St. 
 George Hall , patronage Royal Highness 

 MENDELSSOHN Lieder ohne Worte , 
 8 Books complete .. coe 

 BEETHOVEN Thirty- Sonatas ... 5 0 
 PY Thirty - Miscellaneous 

 Pieces ... ee oa 

 Piano Solo 

 BEETHOVEN . Thirty - Sonatas 

 Pianoforte Pieces ( Rondos , & ec 

 Schwanengesang 
 - Celebrated 
 Songs aa ove 
 WEBER . Sonatas , complete 
 — Pianoforte Pieces tee 

 Piano Ducts . 
 BEETHOVEN . Symphonies arranged 

 Ulrich : — 
 Vol . — , 1—5 
 Vol . Il.—No . 6—9 
 — Septett 
 HAYDN . Celebrated Sy mphonies arranged 
 Ulrich : - — 
 Vol . I.—No . 1—6 ee 

 Songs 

 BEETHOVEN . Songs , complete 

 SCHUBERT . Album , containing — Se héne Mil- 
 lerin , complete ; Winterreise , complete ; 
 Schwanengesang , complete ; Twenty- 
 celebrated Songs , original Edition 

 Pianoforte Scores , 
 ( g — German , f — French , — Italian words 

 BEETHOVEN . Fidelio , G 

 Missa Solennis , 
 CHE RUBINI . Demophon , g g. J 
 — — Mass ‘ ie 
 — — Mass D minor 
 — — Massin . ; 
 — — Requiem male voices 
 GLUCK Orphée , GS . 
 — — Alceste , 0 ia 
 — — Paris Helena , 9 , f 
 — — Iphigenia Aulia , y 

 ee SOO ee 

 GLUCK Armide , g , f ae 40 
 — — Iphigenia Tents , gt aes 28 
 0|JOMELLI . Requiem ee eg ea ee 
 0|MOZART . Don Giovanni , gi ee 
 0| — — Figaro , g.i .. . aes sia , 2 G 
 0| — — Zaubertlite , g , aaa « eo 6 
 0| — — King Thamos , g Pee ae Se 
 0|ROSSINI . Il Barbiere , 9,7 ... oe ee 
 0|}SPOHR . Jessonda , g aa ne ict ee 
 0|WEBER . Freyschutz , g _ ... en ‘ cae 
 O|SCHUMANN . Faust , g ... wm 
 0 ad 
 Complete Operas arranged Piano 
 : Solos . 
 0 AUBER . Maurer und Schlosser 20 
 0 BEETHOVEN . Fidelio 2 0 
 0 BELLINI . Norma ‘ < 2 0 
 Yt ee Sonnambula ... 20 
 BOIELDIEU . Dame Blanche aa 2 0 
 0 DONIZETTI . Lucia di Lammermoor 2 0 
 0 Elisire d’amore ia rr Maes 
 9 | HEROLD . Zampa aes rif eee 8 
 MOZART . Don Juan ae iio te 
 — — Figaro = ies pee oe 2 0 
 Zauberflite ... abe xa 2 0 
 ROSSINI . Il Barbiere ae ig 2 0 
 _ | WEBER . Freyschiitz aaa 2 0 
 0 | — — Oberon oe 2 0 
 0 } _ _ — Euryanthe ... aa 2 0 
 0 | _ — Preciosa 2 0 
 BEETHOVEN . Egmont 2 0 
 BELLINI . Puritani ati ‘ , ( ee 
 0 | — — Montecchi e Capuletti ... asa « s 2 9 G 
 0|BOIELDIEU . Jean de Paris 2 0 
 CHERUBINI . W ee Deux Journées ) 2 0 
 GLUCK . Armide .. pe oa em ( ae 
 0|MEHUL . Joseph ... 2 0 
 0 } MOZART . Seraglio 2 0 
 ‘ Titus ae 2 0 
 0 } ROSSINI . Otello ... es 2 0 
 SPOHR . Jessonda ‘ 2 0 
 o| Complete Operas arvanged Piano- 
 forte Duets . 
 BEETHOVEN , Fidelio 3.0 
 BOIELDIEU . Dame Blanche $ @ 
 9 ) MOZART . Don Juan 4 0 
 0| _ _ _ Figaro “ , 4 0 
 O } _ _ _ Zaubertlite ... 3.0 
 > 0 ROSSINI . 1 Barbiere 30 
 rd WEBER . Freyschiitz 3 : @ 
 0| Overtures Pianoforte Solo 
 | MOZ ART . Overtures , complete . = 0 
 } BEETHOVEN . Overtures , complete 2 0 
 | } CHERUBINL . Overtures , ae 20 
 ) | WEBER . ‘ Overtures , complete ... aku : sane 
 SCHUBERT , SPOHR , LINDPAINTNER . , Cele- 
 | brated Overtures 2 0 
 | BOLELDIEU , HEROLD , AUBER , SPON TINL . 
 | Celebrated Overtures ; neat ee 
 | BELLLNI , ROSSINI . Celebrated Overtures 2 0 

 o| Overtures = Duets . 
 0| MOZART . Overtures , ¢ omplete ... 2 0 
 0 } BEETHOVEN , Overtures , compl 3 0 
 0 } CHERUBINI . Overtures , cope 3 0 
 0O| WEBER . Overtures , complete ... gow aaa 
 0 | SCHUBERT , SPOHR , LINDPAINTNER . Cele- 
 0 brated Overtures V0 
 0| BOLE LDLEU , HEROLD , AUBER , SPONTINI . 
 0 Celebra‘ed Overtures ji ico 
 0 } BELLINI , ROSSINI , Cele brated tures .. 2 

 MUSICAL TIMES.—Avetst 1 , 1870 

 Finally , Messrs. Novello , Ewer Co. haye announce 
 issue Handbooks Oratorios convenient style form , 
 resolved apply principle popular secular works , publishing 
 series Handbooks ofthe Operas , English words , printed size 
 shape Octavo Oratorios ; added editorship 
 undertaken Mr. G. A. Macfarren , translations Madame Mactarren , 
 seen Publishers desirous making series unique 
 accuracy trustworthiness 

 appears Messrs. Novello hardly necessary eall attention eftorts 
 times past improve business , render Catalogue 
 nearly complete possible ; but- perbaps pardoned alluding 
 improvements efiected years , purchase 
 business Messrs. Ewer Co. , bringing entire collection 
 Mendelssohu works hands ; acquisition extensive premises , 
 . 1 , Berners Street ; reduction price greater portion 
 publications ; purchase Sacred Works great French musician , 
 Gounod ( works , deep devotional feeling , producing 
 beneficial effect services larger churches ) ; acquisition 
 entire compositions . Mr. Goss , Dr. Wesley , Mr. Turle ; publication 
 cheap editions Beethoven Graad Mass D , Bach subline Passion ; , 
 lastly , great beneficial influence exerted classes 
 musicians — professional amateur — medium dZusice ! 
 Times 

 London : 1 , Berners Street ( W. ) , 35 , Poultry ( E.C .. 
 August Ist , 1870 

