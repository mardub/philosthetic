A. console way . public 
 look laugh . Remember Schu- 
 mann Berlioz — bad critics , eh 2 ? 
 Gounod . know writes ? 
 day saw Sullivan list con- 
 tributors periodical ; Stanford Parry 
 dip pens critical ink ; Corder actually 
 editor . Look , old fellow ; Philistines — beg 
 pardon — order , 
 ructions , saying . 
 | C. suggest composers 
 | criticism , man confine 
 works , surely ought know 
 anybody 

 A. case , course , matters run 
 |smoothly , edification . 
 | C. edification ? 
 rejoice somebody discover — age 
 “ finds ’’ — elaborate criticism Beethoven 
 Symphonies written Beethoven 

 A. special case ; Jones 
 Robinson , writing works , throw 
 lights unduly 

 suggestion , doubt , conductors 
 jof oratorio concerts receive 
 gravity . effect performances 
 “ Messiah ” present 
 | Hallelujah Chorus . ‘ usual , ” writes critic 
 |anent recent occasion , “ audience rose 
 |the Hallelujah Chorus , 
 taken voices audience 
 hard understand . havoc played 
 parts ; majority 
 fairly known . ” ‘ audacity proposal 
 takes away breath . author 
 Danton musical journalists 

 musical world interested late 
 succession * finds , ” regular frequent occur- 
 rence possibly excite distrust 
 certain orderof minds . latest discoveries 
 portraits Beethoven parents — 
 require fully authenticated — , Manchester , 
 copies early works Mozart , 
 fragments , said master handwriting , 
 youthful opera ‘ “ * Mithridates . ” copies 
 works easily accessible , MSS . 
 valuable , interesting found 
 England . supposed brought 
 Miss Harford , , early present 
 century , studied Florence 

 Tue London School Board assented 
 introduction pianofortes elementary schools . 
 probably cost unhappy ratepayers sum 
 amounting nearly thousand pounds experi- 
 ment , lead results ends seen . 
 contemporary asks : ‘ * anybody suppose 
 instruments provided school pianoforte 
 movement spread ? doubt , urged 
 members , ‘ music hath charms ’ soothe 
 schoolboy breast , ‘ improves natures ’ ; 
 improvement kind entirely 
 counteracted deterioration natures 
 parents boys owing increased 
 ‘ Education Precept 

 Tue intended musical festival Bonn , , 
 aid Beethoven House , occasion per- 
 fect propriety . Germany , claims 

 musical country par excellence , need foreign 
 | help question honouring great 
 composers . Anattempt raise money England 
 ifor Bonn house , understand , 
 successful . reason clear . English amateurs 
 ardent worshippers Beethoven , 
 countrymen persons 
 create local memorial genius 

 English eagerness speed parting 

 vocalists prefer superfluous lag- 
 ging stage decent dignified retirement 
 means begin fail ? Miss Clara 
 Louise Kellogg ‘ ‘ concertising ” United 
 States , papers saying voice 
 “ painful wreck . ” best years artist life 
 devoted erection worthy memorial 
 public esteem ; closing period spent 
 demolishing 

 Boston writer music holds 
 Beethoven ‘ “ Pastoral ” Symphony 
 “ somewhat threadbare hackneyed . ’ , 
 suppose , beauty rural landscape ; 
 murmur brook ; grandeur summer 
 storm , feelings toil - worn man 
 finds comfort bosom great Mother . 
 threadbare , hackneyed , save creation 
 , - morrow , thrown aside turn 

 shining light musical criticism tells 
 slow movement ‘ “ Pastoral ” js 
 ‘ ‘ soporific , ” ‘ “ nauseatingly 
 reiterated , ” storm “ ‘ tempest tea . 
 pot . ” Ifthe idea absurd , 
 suppose Wagner fanatics 
 minds befoul render loathsome 
 music save man 

 ( editor ) enthusiast freedom 
 press ? protests Growler man 
 silly , tries , benefiting 
 cause ? ask information 

 absurdity insisting repetition 
 piece obviously displayed 
 Miss Janotha , , Concert , encore 
 Beethoven “ Sonata Appassionata , ” played 
 composer Variations “ Rule , Britannia , ” 
 , ‘ * Moonlight Sonata ” substi- 
 tuted Chopin ‘ * Marche Funébre 

 authority British Medical Fournal , 
 enemies performers bagpipes amply 
 tevenged persecutors , told 
 friction mouthpiece pipe 
 teeth players — ‘ set edge , ” like 
 listeners — actually severely injured 

 Tue Overture Entr’actes Mr. Irving pre 

 sentation ‘ ‘ Master Ravenswood ” 
 Lyceum autumn completed Dr. 
 A. C. Mackenzie , , Scottish composer , 
 considered peculiarly qualified task . 
 | Mr. Barnsy took chair , 24th ult . , 
 jlast course Lectures Sunday School 
 |Singing , delivered Mr. W. 
 | McNaught members Sunday School 
 | Union . 
 Beethoven Place Chicago , 
 | Mozart Square New Orleans . careful search 
 | map London necessary find Beet- 
 | hoven Street Kilburn way 

 best portraits Madame Albani 
 seen appears American Musician 
 | March 2 g. isa length figure dress 
 | Desdemona 

 281 

 low baritone . audience appreciated quality 
 voice solos , ‘ “ Qui sdegno , ” ’ 
 J ] Flauto Magico , ” Schumann * I'wo Grenadiers . ” 
 orchestra , - careful direction Mr. 
 Manns , surpassed performance Over- 
 ture Weber “ Oberon , ” Pastoral Symphony 
 Beethoven —— representation Summer 

 Storm brought remarkable effectiveness — | 
 composer Overture Opera | 
 “ Leonora 

 errs right . pro- 
 gramme present Recital pieces 
 fewer fourteen composers , important 
 work Schumann rarely - heard Sonata F sharp 
 minor ( Op . 11 ) . Pieces Ratt , Zarzycki , Nicode , 
 Gernsheim , Dupont proved interesting , 
 special mention fanciful refined 
 Nocturne E flat , E. Cutler ( Op . 43 ) , English 
 composer far met success Germany 
 native country 

 hall fairly filled 21st 
 ult . , Mr. Lamond gave Recital . programme 
 contained fewer pieces , alarger proportion important 
 works . young Scottish pianist created excellent 
 impression Beethoven Sonata flat ( Op . 110 ) , 
 work entertain special affection ; 
 Brahms Variations theme Paganini ( Op . 35 ) ; 
 Schumann Etudes Symphoniques . ren- 
 dered praiseworthy breadth style good 
 technique , Mr. Lamond reading suggesting influence 
 Hans von Bilow . touch times 
 hard , instrument . 
 Smaller pieces Chopin , Liszt , Raff completed 
 scheme 

 Mr. John H. O. Dykes trust entirely 
 powers Recital , 23rd ult . , took 
 place Princes ’ Hall . aided Beethoven 
 “ Kreutzer ’’ Sonata admirable violinist , Mr. Willy 

 Hess , played solos , Mrs. Helen 

 POPULAR CONCERTS . | 
 little space required complete record 
 thirty - second season Concerts . | 
 Saturday performance , took place March 29 , } 
 Beethoven programme provided far regards 
 instrumental pieces . Quintet C ( Op . | 
 29 ) , Serenade Trio D ( Op . 8) , Violin Romance | 
 F ( Op . 50 ) , played Dr. Joachim , ‘ Moon- | 
 light ’ Sonata , Miss Janotha executant . | 
 usual Polish pianist took opening Adagio a| 
 singularly rapid pace , thinking depriving | 
 large extent impressiveness . vocalist , Mr. | 
 Norman Salmond , decidedly improved position 
 declamatory perfectly legitimate rendering “ O 
 ruddier cherry . ” ’ Healso introduced extremely | 
 charming somewhat sad little song , ‘ Good Night , ” | 
 Battison Haynes , composer caught 
 spirit German verses Betty Paoli . | 
 final Concert , following Monday , dis- | 
 missed equal brevity . Critical remarks | 
 certainly required concerning Mozart Quintet G| 
 minor , Rubinstein Sonata D , pianoforte violon- | 
 cello ( Op , 18 ) , Schumann Quintet E flat ( Op . 44 ) . 
 acknowledged masterpieces added Spohr | 
 Concerto B minor , violins ( Op . 88 ) , accom- 
 paniment played pianoforte Miss | 
 Agnes Zimmermann . work suffered con- | 
 sequence arrangement said , interest | 
 lies mainly solo parts , splendidly written | 
 violin , , course , rendered perfection 
 executants Madame Néruda Dr. Joachim . | 
 remains added Miss Fanny Davies took 

 Trust , sang songs composed Concert- 
 giver . Mr. Dykes playing , , note- 
 worthy energy refinement . best 
 Schumann trying Toccata C ( Op . 7 ) . Mendels- 
 sohn Prelude Fugue E minor pieces 
 Chopin Rubinstein included programme 

 MUSICAL TIMES.—May 1 , 18go 

 Pianoforte Quintet ( Op . 81 ) , given 
 time . characteristic work gifted 
 Bohemian enjoyed . Miss Florence Hemmings 
 gave neat rendering Marcello Violoncello 
 Sonata F , Mr. Kruse played passionate expres- 
 sion Ernst “ Elégie , ” ’ astonishing bravura , 
 Polonaise , Wieniawski . programme 
 included Beethoven String Quartet F ( Op , 18 , . 1 ) , 
 went splendid aplomb 

 Organ Recitals Town Hall given 
 Saturday afternoon opening ceremony 
 mentioned columns . 5th ult . Mr. 
 C. W. Perkins played , , week later , Mr. J. Kendrick 
 Pyne delighted large audience vivacious 
 animated style performance . roth Dr. A. L. 
 Peace organist , attracted lovers 
 instrument . appearance distinguished organ per- 
 formers parts country new feature 
 musical life , , , impracticable 
 old arrangement 

 divisional choral societies , formed October , 
 |have completed work present season , 
 | good results cases . prizes awarded 
 Mr. G. Riseley Mr. E. T. Morgan efficient 
 members Bristol East Musical Society dis- 
 tributed March 19 

 Redland Orchestral Society , work- 
 ing private years , initial public 
 essay March 27 , Overture Hermann , 
 smaller works Beethoven , Grieg , Sullivan , Moszkowski , 
 Prout , performed commendable skill , 
 direction Mr. E. Purcell Cockram . Songs 
 concerted vocal pieces contributed Miss Kate 
 Nicholls , Mrs. Gridley , Mr. S. Evans , Mr. Percy 
 Baldwin 

 Bristol Society Instrumentalists , largest body 
 amateur players kingdom , gave second 
 annual Concert , 17th ult . , Colston Hall . 
 chief works performed Beethoven Symphony 
 D ( . 2 ) , Weber “ Peter Schmoll ’ ? Overture , 
 Mendelssohn ‘ Son Stranger ’ Overture , 
 Mackenzie beautiful ‘ ‘ Benedictus , ” 
 played surprising skill . tone strings 
 remarkably pure , rich , , marking light 
 shade desired . , per- 
 formance like band professionals 
 amateurs . Mrs. Nixon Mr. A. Wetten 

 

 , consequently , opportunity standing 
 eagerly seized ; bare new wood temporary 
 platform , interval enable choir 
 vestry , presumably refreshments , speech 
 minister Church Scotland , mild j¢ 
 invited ‘ ‘ hearty vote thanks , ” piece 
 opera glasses , general conversation audience , 
 ‘ ‘ evening dress optional ” programmes . 
 great opportunity offered lost . perform- 
 ance , good 
 better . orchestra sadly deficient strings . 
 doubt great pleasure able hand substa 
 sum deserving charity behalf Cor acert 
 given , Mr. Kirkhope Committee 
 higher duty — Brahms art — 
 said violins thirty sopranos 
 tended fulfil duty . ar yea form 
 choir shown unaccompanied - Motet , 
 ‘ * * Agnus Dei , ” Richter , received beautiful 
 s;mpathetic rendering . Gounod ‘ * Ave verum ” 
 softly sung effect . Twosolos , * “ 
 hearts * * Angels bright fair , ” varied 
 programme , orchestra strong 
 . Requiem , dimmer , religious 
 light , organ speech , 
 far ideal programme 

 second Concert Edinburgh Quartet , 
 important number Quintet ( Op . 81 ) Dvorak 
 classed famous Hungarian composers 
 educators musical opinion local Mr. 
 Francis Gibson played exacting pianoforte 
 admirably , Scherzo brilliantly rendered . 
 Messrs. Daly , Dambmann , Laubach , Carl Hamilton 
 justice work , failed maintain 
 wonted style Beethoven Op . 18 , . 2 . Extracts 
 Quartets Spohr , Raff , Rubinstein completed 
 Mr. Stronach 

 

 Weber ‘ Jubilee ” ? Cantata , Choir St. Vincent 

 Street U.P. Church produced Gounod ‘ * Messe Solen- 
 nelle , ” 
 performance Bennett ‘ ‘ Queen . ” 
 favourite Cantata Mr. E. Branscombe 
 appearance Glasgow , accorded flattering 
 welcome . miscellaneous Concerts comprised 
 good performance Stainer ‘ ‘ Crucifixion , ’’ 
 Greenhead U.P. Church Choir , highly successful 
 appearances Bridgeton Choral Society Mac- 
 Cunn ‘ * Cameronian Dream ” Mendelssohn 
 ‘ ‘ Hear Prayer . ’ ” ’ Society , ably con- 
 ducted Mr. George Taggart , young years , 
 promising work , vitality 
 assured . life excellent organisation 
 vigour . ‘ * Glasgow Glee 
 Catch Club . ” small — designedly — point 
 numbers , voices , exaggeration , 
 superb quality , musical culture mem- 
 bers foremost order . amply shown 
 evening roth ult . , Club gave Smoking 
 Concert presence large exceedingly representa- 
 tive audience . ‘ palpable hit ’ Mr. Joseph 
 Bradley elegant altogether clever Serenade , ‘ ‘ Good 
 night , love . ’’ composed Society , sung 
 remarkable appreciation beauties , received 
 uncommon marks favour . Beethoven ‘ * Hymn 

 , 

 St. Cecilia Choral Society gained credit | 
 
 

 pianoforte , orchestra chorus . work 
 means Society ; choral writing 
 melodic graceful , justice 
 choir . occasion composer adaptation 
 orchestral second pianoforte . 
 played lady friend Society , real 
 pianoforte Mr. Philip Halstead 
 achieved brilliant success . Concert 
 season Hillhead Musical Association took place 
 2nd ult . , supported Dr. Joachim Miss 
 Fanny Davies . enterprise management , 
 | understood , entirely successful , days gone 
 | reflections freely apathy Glasgow 
 amateurs chamber music , charge 
 | maintained . work accomplished 
 | Association invaluable , occasion 
 | brief notice programme sterling worth forth- 
 |coming . included Schumann Fantasie violin ( Op , 
 | 31 ) , Brahms Sonata violin pianoforte , D 
 |minor , Beethoven G major Sonata 
 jinstruments . Dr. Joachim accomplished 
 | coadjutor best evening , 
 ' need , , said concerning interpretation 
 ' programme 

 19th ult . Mr. Fred . Niecks , accomplished 
 | author ‘ Life Chopin , ” gave Lecture “ 
 | Sharp , Flat , Natural , ’’ Glasgow 
 | Society Musicians . good attendance , 
 | interest felt Mr. Niecks admirable paper 

 past month amateur operatic 
 performances Rock Ferry , directed Mr. D. Dean ; 
 St. Helen , management Miss H. Swift 
 Runcorn Musical Society gave 
 Concert season miscellaneous programme 
 15th ult . , Mr. E. W. Humphreys 

 Chester , Parry ‘ ‘ St. Cecilia Day ” ? Beethoven 
 Choral Fantasia , Mr. Steudner Welsing piano- 
 forte , formed programme Concert given 21st 
 ult . Dr. J. C. Bridge Musical Society . Waterloo , 
 Bennett ‘ “ * Queen ” given , Mr. E. J. 
 Morrison , 15th ult . ; farther afield , Portmadoc , 
 date , Mr. J. Roberts conducted Mendelssohn 
 “ Elijah . ” Stowell Brown Guild , veteran , 
 Mr. J. Sanders , chorusmaster , gave Barnett ‘ ‘ Ancient 
 Mariner , ” 17th ult . 28th ult . Barnby 
 “ Rebekah ” ? announced Bootle , Mr. A. 
 E. Workman ; numerous 
 performances interest . notable innova- 
 tion past month employment 
 orchestra dissenting place worship — congrega- 
 tional church Burlington Street — sort 
 having taking place previously Liverpool , 
 case Birkenhead 

 permission Mr. D’Oyley Carte , Rock Ferry 
 Amateur Opera Society , 22nd ult . , invaded 
 eastern shores Mersey , gave remarkably good 
 performance ‘ ‘ Mikado ” St. George Hall , aid 
 deserving local charities . 
 opera excellently performed , principals chorus 
 alike competent , , fact , conventional amateur 
 element conspicuous absence . 
 accompaniments pianoforte 
 Mustel organ-—both played , way-—the per- 
 formance best light 
 opera taken place city 

 Nottingham Philharmonic Choir received | 
 invitation Master Balliol College , Oxford , | 
 Concert hall College | 
 * “ * Eights week 

 MUSIC LEIPZIG . | 
 ( CorRESPONDENT . ) 
 tenth examination Conservatoire | 
 took place March 25 . summing resuit | 
 Easter trials safe affirm Greater Britain | 
 foremost , branches | 
 competitive exhibition young musical talent . piano- | 
 forte players , Messrs. George Moon , Plymouth , | 
 Ernest Hutcheson , Melbourne ( Australia ) , head list . | 
 genuine child Conservatoire , | 
 teacher home Dr. Torrance , Dublin , , | 
 1856 , studied Moscheles 
 original staff . child young Hutcheson 
 played minor pieces Concerts private 
 annual Recitals . entered Easter , 1886 , 
 Zwintscher chief instructor . received 
 lessons Reinecke . execution Beethoven Fifth 
 Concerto eighth trial admirable youth 
 nineteen . touch clear , vigorous , free 
 mannerisms , conception accurate 
 coldness . best pianoforte - playing 
 heard year Conservatoire promises 
 future . Mr. Moon played D minor Mendelssohn 
 Concerto official trials , exhibited | 
 high degree technical skill , prominent subordinate 
 phrase slighted , slurred , marred ; 
 conception work showed thorough appreciation 
 beauties . ninth trial produced 
 compositions , ‘ ‘ Variations Pianoforte | 
 original Theme , ” - defined , graceful piece 
 work Mendelssohn style , effectively performed | 
 . violin player Miss Brammer , 
 Grimsby , , , ripest fruit season 
 growth . daughter late Edwin Brammer , 
 leading musician Grimsby , played repeatedly 
 Concerts given native town . 1882 
 received Leipzig Conservatoire , 
 years old , strength musical ability , 
 year played Mayseder Variations E 
 orchestra , winning great applause . ‘ technical 
 precision , good bowing , singing tone ” | 
 flatteringly noticed public prints . con- 
 siderable répertoire command , plays 
 great ease manner maturity conception . 
 criticism April 2 , 1889 , says : ‘ * star Concert 
 undoubtedly Miss Brammer , violinist great talent | 
 grandly developed technique . played Spohr 
 Eighth Concerto difficult ‘ Faust ’ phantasie 
 complete mastery technical detail absolute preci- | 
 sion difficult passages , . . . showing | 
 musicianly earnestness doubly remarkable youthful 
 performer . ” high praise , merited . | 
 Miss Brammer , far distant time , high rank | 
 young artists Old England 
 proud . trial Mr. Edward Levy , Man- 
 chester , conducted original Overture , 
 exhibited mean talent . close list , mention 
 organist , Mr. Edwin Clemence , Plymouth , 
 played Toccata Fugue C major , Bach . 
 swiftness ease manual pedal technique 
 rival pupils year ; taste 
 registration , practical theoretical , judgment 
 passed , pupils allowed draw stops 
 . Mr. Clemence great capacity , 
 doubtless develop ensuing course | 
 study Conservatoire 

 formers chorus 

 c. , chance appeasing appetite . 
 fact chronicled , exception evenings 
 incomparable Patti created unbounded enthu- 
 siasm audiences filled spacious house 
 overflowing , attendance public left 
 desired . Tamagno , great tenor , sang 
 times ( comparatively ) small audience . 
 greatest artistic treats Italian opera season 
 wonderful singing Madame Albani Desdemona inVerdi 
 “ Otello , ” parts . regretted 
 great effort arrange 
 performance Gounod * * Redemption ” city , 
 Albani , Lloyd , Ludwig solo parts , failed 
 reason previous engagements prevented 
 great artists meeting suitable time 

 instrumental performances usual 
 number , prominent Concert 
 Symphony Society . 
 Pianoforte Concertos — Beethoven E flat Liszt 
 E flat — played Hans von Biilow somewhat 
 dry colourless manner ; Orchestral Ballad , 
 “ Des Singers Fluch , ” pen artist 
 Pianoforte Recitals given Bilow 
 Broadway Theatre create sensa- 
 tion year Recitals , reserved 
 great Chopin player , Wladimir de Pachmann , 
 create sensation hour . 
 wonderful success , Chickering Hall crowded 
 suffocation Recitals , audience listening 
 rapt attention marvellous execution 
 wonderfully retined rendering Chopin works . 
 Recitals originally announced Pachmann 
 taken place , great success 
 additional Recitals arranged . - night grand 
 Orchestral Concert place , play 
 Chopin Concerto F minor , Madame de Pachmann 
 perform Liszt E flat Concerto , play 
 Saint - Saéns Scherzo pianofortes 

 BOSTON HANDEL HAYDN FESTIVAL . 
 3oston , April 14 

 annual Concert Violin Classes connected 
 Birkbeck Institute took place 18th ult . 
 selections played members classes directed 
 Mr. Gatehouse Mr. Thornton ample evidence 
 given careful training received hands 
 teachers . vocalists assisted Miss 
 Margaret Hoare , Miss Mary Doughty , Mr. Arthur Thomp . 
 son , Mr. R. E. Miles . Mr. Alfred Izard played Greig 
 Wedding March accompanied accustomed 
 ability , Mr. Gatehouse gave clever rendering 
 “ Le Tremolo , ” De Bériot , , encored , 
 substituted Popper ‘ Gavotte . ’ Special mention 
 Miss Edith Doughty , joined 
 teacher , Mr. Gatehouse , Symphonie Concertante 
 Dancla , played tone breadth style 
 exceptional young girl , intonation 
 remarkably pure . recitations given Mr. Charles 
 Fry , obviously suffering severe cold , 
 favourably received . attendance 
 means large desired 

 Tue arrangements North Staffordshire Musical 
 Festival fairly complete . locus quo 
 Stoke - - Trent , dates October 1 2 . 
 | morning Mozart “ ‘ Requiem ” ? ‘ Golden 
 | Legend ” given ; evening , new 
 | Cantata , written expressly Festival Dr. Heap ( 
 | libretto late D. L. Ryan ) , called ‘ * Fair Rosamond , ” 
 short miscellaneous Concert . second morning 
 devoted orchestral works , Beethoven 
 Symphonies given ; evening 
 second parts ‘ Creation , ’ Stanford ‘ * Revenge , ” 
 jand Parry ‘ “ Blest Pair Syrens ” ’ form pro- 
 gramme . artists engaged Madame Nordica , Miss 
 Macintyre , Miss Damian , Mr. Edward Lloyd , Mr. Iver 
 McKay , Mr. Foli , Mr. Watkin Mills . band 
 consist seventy players , selected Sir C. Hallé band 
 } Birmingham , Mr. Willy Hess leader 
 solo violin ; Mr. F. Mountford master 
 |chorus ( 280 voices ) , Dr. C. Swinnerton Heap 
 Conductor 

 17th ult . Concert Finsbury Choral 
 | Association season given Holloway Hall , 
 ; conductorship Mr. C. J. Dale . band 
 | chorus numbered 300 , Mr. Carrodus leader . 
 |The vocal soloists Miss Kate Norman , Miss Lizzie 

 acquainted time . 
 selected “ ‘ Knotting Song , ” ‘ ‘ Nymphs Shep- 
 herds , ” ‘ * Libertine . ” ” - known airs 
 “ J attempt Love sickness , ” ‘ fathom , ” 
 * * Come unto yellow sands ” ’ included set , 
 recitative air * ‘ Dido /Eneas , ” 
 harmony constructed ground 
 bass ; “ * sail dog - star , ” ’ ‘ ‘ tell yon 
 mighty powers , ” ‘ ‘ brow Richmond Hill , ” 
 « Fairest isle , ” ‘ “ ‘ King Arthur ’ ; ‘ * ‘ shall , ” 
 ‘ * * Dioclesian , ” ’ supplemental second verse ; 
 Cantata ‘ Don Quixote , ” entitled ‘ rosy 
 bow'rs , ’’ , copy * * Orpheus Britannicus ” 
 states , ‘ * song Mr. Purcell sett , 
 sickness . ” words songs Shakes- 
 peare , Sedley , Howard , Dryden , Nahum Tate , Shadwell 
 ( Poet Laureate ) , Tom D’Urfey — Mr. 
 Cummings gives apostrophe usually employed 
 owner . songs 
 transposed original keys fit modern 
 needs , accompaniments 
 constructed Purcell figured basses , 
 chords found 
 accounted figures . introductory 
 symphonies required written 
 conformity character song 
 attached . lovers old English music general , 
 Purcell particular , glad welcome 
 tothis ably arranged collection music 
 * * gone blessed place harmony 
 exceeded 

 Songs . Composed L. van Beethoven . 
 version Rev. Dr. Troutbeck . Vol . III . 
 [ Novello , Ewer & Co 

 English 

 collection Songs Beethoven , 
 completes series , lesser known 
 songs great master better known . 
 bring interest , admirers 
 3eethoven note special interest settings 
 poem , Goethe , * * Sehnsucht ” ’ ( * * Longing ’’ ) , 
 melodies “ die Geliebte , ” ’ different 
 agem . - known contralto aria , ‘ ‘ questa tomba 
 oscura , ’’ songs ‘ * Egmont , ” noble scena 
 soprano voice , ‘ * Ah , perfido ! ” arealso contained 
 series . original words German , Dr. Trout- 
 veck English version , offer double means acceptance 

 FOREIGN NOTES 

 BASINGSTOKE.—The second Concert season Basing- 
 stoke Choral Society given Drill Hall , Tuesday , 22nd 
 ult . , Haydn Oratorio Creation performed . chorus 
 specially augmented numbered seventy members . 
 accompaniments played professional string orchestra 
 , led Mr. Charles Griffiths , wind parts given ona 
 large harmonium Mr. Frank Idle , whilst Mr. H. Shepherd 
 accompanied recitatives pianoforte . solo vocalists 
 Miss Kate Norman , Mr. Reginald Groome , Mr. B.H. Grove , 
 Conductor Mr. H. E. Powell 

 Battey.—On Monday evening , 14th ult . , St. Thomas 
 Church , Organ Recital given Mr. W. Marsden , Organist 
 Church . Miss Lottie Atkinson Mr. Harry W. Kemp 
 gave vocal selections . Mr. Marsden selection ‘ ‘ Concertstiick , ” 
 W. Spark ; Allegretto ( B minor ) , Guilmant ; ( ) Romance ( violin ) , 
 Beethoven ; ( b ) Fugue G major , Krebs ; March Romaine , Gounod ; 
 Sonata ( . 5 ) , Mendelssohn ; ( ) Andante F , Morandi ; ( 6 ) Cap- 
 riccio , Lemaigre ; Allegretto Moderato , Gambini ; Grand March D 
 major , Boyton Smith 

 BELVEDERE , Kent.—On roth ult . Belvedere Abbey 
 Wood Choral Society brought successful winter season close , 
 witha grand Concert given Public Hall , Belvedere . 
 programme devoted Van Bree Cantata St. Cecilia 
 Day , second consisted miscellaneous selection . 
 performers engaged occasion Miss Edith Clay , Miss 
 Bertha Colnaghi , Mr. G. F. Nichols , Mr. Charles Bonham ; Mr. 
 Sidney Horton ( violin ) ; Miss Thomas ( accompanist ) . Mr. Arthur W. 
 Castell , able Organist Saints ’ , officiated Conductor 

 COMPOSED 

 L. VAN BEETHOVEN 

 English Version Rev. Dr 

