Norwood , § ¢ Times , Oct. 14:—“Mr . Coates beautiful voice finished 

 style great impression tenor music " ( Beethoven 
 ) E N Mass D ) . 
 Standard , Oct. 12:—‘Mr . John Coates sing solo excel- 
 * ently . " 
 Globe , Oct. 12:—“A fine interpretation secure 
 uth wark , Dr. Stanford hardly desire , Mr. Coates singing solo 
 specially admirable achievement . " 
 Star , Oct. 12 : — " Mr. Coates singing valuable 
 addition rank festival tenor . " 
 Yorkshire Post , Oct. 12 : — ' performance calculated 

 societ place work favourable light , particularly 

 MUSICAL TIMES.—Novemser 1 , 1go1 

 Festival — ninety - eighth Psalm ( - 
 chorus orchestra ) consider sufficient , 
 B ’ certain collegiate circle — 
 Bach , Beethoven , Brahms — repre- 
 sente composition . strenuous 
 performance Schumann D minor Symphony 
 contrast somewhat unfavourably 
 rendering Mr. Manns — early apostle 
 Schumann country — use set 
 Sydenham . Spohr Concertante b minor , 
 violin , come novelty . work 
 play England Sivori Sainton 
 Philharmonic concert July 5 , 1845 , Sainton 
 Henry Blagrove concert March 20 , 1854 , 
 ( Philharmonic ) recently 
 March 20 , 1872 , perform Herr 
 Bargheer Herr Joachim . present occa- 
 sion Herr Joachim violin , 
 gift pupil , Senor Arbos , prove - rate 
 second , orchestral accompaniment 
 faultlessly play . Finale Act I. Parsifal , 
 , like Palestrina , sing 
 concert - hall fashion , conclude programme . 
 soloist Messrs. John Coates , David 
 Bispham , Plunket Greene 

 outstanding feature Friday evening 
 music — , great success 
 Festival — Dr. Elgar ' variation 
 original theme . ' fresh hearing confirm 
 impression poetic beauty artistic 
 creation . charmingly play orchestra 
 composer baton , receive genuine 
 enthusiasm . quasi novelty — z.e . , performance 
 England — furnish Memorial Cantata 
 Glazounow , compose year 1899 celebrate 
 centenary great russian poet , Pushkin . 
 work nature 
 picce d’occasion , , scarcely represent 
 true self distinguished slavonic composer . 
 word - inform annotator 
 programme , music , diatonic style , 
 seek reflect measure purity 
 transparent simplicity Pushkin character . 
 follow music complete programme : Brahms 
 seldom heard ' Rinaldo ' cantata , tenor solo 
 ( Mr. John Coates ) male voice chorus ; 
 ' Africa ' caprice , pianoforte orchestra , 
 Saint - Saéns , solo portion brilliantly play 
 Mr. Leonard Borwick ; duet Gounod 
 ' Romeo Juliet ' ( Miss Agnes Nicholls 
 Mr. John Coates ) overture ' Les deux 
 Journées ' ( Cherubini ) ' Benvenuto Cellini ' 
 ( Berlioz ) . soloist Glazounow cantata 
 Miss Ada Crossley Mr. John Coates 

 morning Festival ( Saturday ) open 
 Bach noble cantata ' sleeper , wake ' ( Wachet 
 auf ) , include scheme recent 
 Gloucester Festival . performance 
 western city , canto fermo Otto Nicolai fine 
 tune open chorus hardly prominent 
 ; contingent boy ’ 
 voice produce proper balance . 
 chorus work splendidly ; verse 
 old tune , simple - harmony , come 
 thrilling effect . duet — especially — 
 afford painful study - - tuneness . 
 soloist Miss Agnes Nicholls Mr. John 
 Coates , Dr. Joachim play violin obbligato 
 duet 

 Beethoven stupendous unfathomable Mass 
 D set seal Festival . perform- 
 ance reach spring - tide high - water 
 mark excellence , admire . 
 praise rightful chorus 

 Beethoven exacting production . soloist 
 Madame Albani , Miss Marie Brema , Mr , John 
 Coates , Mr. Plunket Greene , delicious 
 violin obbligato ' Benedictus ' interpret 
 true artistic devotion Dr. Joachim 

 regard final concert ( Saturday evening ) 
 suffice programme , add 
 Mr. Alfred Benton , indefatigable chory . 
 master , conduct Pearsall Choral Ballad , 
 warmly receive member 
 choir audience 

 contact , secret life unwearie 
 energy patient continuance . 
 man Leeds great truth Labor ipse 
 voluptas 

 Beethoven Mass D — form bonne 
 bouche fourth morning recent Festival 
 — obtain public hearing England 
 year 1839 , 1845 Philharmonic 
 Society , state . 
 programme interesting event . 
 performance society style 
 Choral Harmonists , somewhat exclusive body 
 amateur practise music pure love 
 art . place London Tavern , 
 Aldersgate Street , April 1 , 1839 , 

 Bec 

 MUSICAL TIMES.—Novemser 1 , Igor 

 want horn second trumpet . 
 absence instrument 
 important symphony trumpet drum 
 introduce recitative Agnus Dei sort 
 mock - heroic effect trumpet solo . ' 
 habit protest , justly , 
 long programme , good people 
 1839 furnish gargantuan feast music . 
 Mass D follow Mozart motet , 
 ' Laudate Dominum , ' Mendelssohn 42nd Psalm ; 
 old John Benet madrigal , ' sing loud , ye 
 nymph , ' Weber ' ruler spirit , ' overture ; 
 lastly , Handel ' L’Allegro ' ! footnote 
 programme state : ' J. A. Novello Collection 
 word classical sacred music 
 waiter , price shilling . ' end i. 
 programme come following refreshing 
 information : ' expect Tea , Ladies 
 Gentlemen resume place occupy 
 performance . ' Beethoven 
 D tea 

 gentleman ' write descriptive ' 
 leading Leeds paper fine Festival fettle 
 week . specimen graphic grasp 
 subject unacceptable . | 
 come Mr. Coleridge - Taylor , hand - in- | 
 hand Madame Albani Mr. Andrew 
 Black . triple bow acknowledg- 
 ment , pass short interval 

 Mendelssohn . Stephen S. Stratton . 
 Musicians ’ Series . 
 [ J. M. Dent Co 

 preface , late monograph Mendels- 
 sohn , Mr. Stratton state attempt 
 exhibit Mendelssohn biographical 
 literature concise form , ' close adherence 
 fact , portrayal ideal . ' 
 think succeed ; page certainly bear testimony 
 industry painstaking zeal . 
 actually new matter present ' man 
 know , ' author furnish general reader 
 chatty , trifling exception , accurate 
 ' Life ' composer ' Elijah . ' appendix 
 especially useful — text ' Elijah ' 
 original form — reference information 
 crowd seventy - page end 
 book . Mr. Stratton . 103 , Great Portland Street 
 location Mendelssohn lodging 
 early visit London ; number house , 
 , add , rebuild Mendels- 
 sohn stay , 79 thoroughfare . 
 statement need correction future edition : 
 ( t ) Beethoven e flat Pianoforte Concerto play 
 time country Mendelssohn 
 June 24 , 1829 ( p. 57 ) , Charles Neate , Phil- 
 harmonic Concert 8 , 1820 ; ( 2 ) Bach vocal music 
 hear England lecture 
 Dr. Gauntlett London Institution , March , 1837 
 ( p. 106 , note ) , , point ( 
 MusicaL Times , October , 1896 , p. 654 ) , Samuel Wesley 
 1809 , - year early . illustration 
 book reproach 

 Studies Music . Authors , 
 ' Musician ' edit Robin Grey . 
 ( Simpkin , Marshall , Hamilton , Kent , Co. , Ltd 

 concert english music 22nd ult . refer | " ane Sch gang og es micas s Pipe 
 page 730 . | come usin flush manhood , brilliant 
 | technique , temperament intelligence . interpret 

 BRASS BANDS CRYSTAL PALACE . Beethoven Schumann asif feel music ; butas 

 oh , listen band . ' yes , listen band , | 294 naturally , period storm stress 
 750 strong , Sydenham glass house September 28 . | ov " virtuosity hold dangerous sway . time 
 occasion ' Grand brass band contest | 0ught - balance artist 
 festival , ' organise Mr. J. Henry Iles . proceeding | POW ' 8—@ oe ae snag Magog " Agus recital 
 day commence competition ' National | tok place Queen Hall 5th roth ult . 
 challenge trophy value thousand guinea . ' | 
 - covet honour prize , | 
 - seven band compete . 
 list enter fray 

 Turpin . test piece bear title ' gem 
 Sullivan Operas , . 3 . ' follow prize- 
 winner , order merit 

 1 . Lee Mount . | 6 . Lindley . large choral work present Verdi 
 2 . Irwell Bank . 7 . Rochdale Public , ' Manzoni Requiem . ' instrumental number worthy 
 4 rect day | 8 , pon Mis . mention record Brahms Beethoven 
 . s. | g. Luton Red Cross . 

 5 . Kettering Town 10 . Wingates temperance . second symphony , Schumann overture , Scherzo 

 society - commence practice 
 busy musical season anticipate 

 Choral Society , Mr. George Riseley , conductor , 
 Sullivan ' golden Legend , ' Beethoven ' Mount 
 Olives , ' Dvorak ' Stabat Mater , ' Wagner 
 selection , ' Messiah 

 Clifton Choral Society , Mr. F. W. Rootham , conductor , 
 hand Handel ' Acis Galatea ' 
 Mendelssohn music ' Midsummer Night Dream 

 Chamber Music Union issue prospectus 

 violin ) , Mr. Delany ( second violin ) , Mr. Griffith ( viola ) 
 Herr Bast ( violoncello ) . concert 
 place afternoon 14th inst . , Beethoven 
 b flat trio Schumann minor quartet 
 perform 

 MUSIC EAST ANGLIA . 
 ( correspondent 

 Norwich Choral Society , recently 
 form place defunct Gate House Choir , 
 Dr. Bates conductor , Norwich Orchestral 
 Union rehearsal Reitz Oboe Concerto , 
 Weber ' Preciosa ' music , lesser - know 
 Symphonies Mozart 

 Great Yarmouth Musical Society select 
 Sullivan ' Martyr Antioch ' concert , 
 Sullivan ' golden Legend ' second , 
 Orchestral Society probably include Beethoven 
 Eighth Symphony concert early new year 

 Ipswich Choral Society perform Coleridge- 
 Taylor ' Hiawatha ' Mendelssohn ' Elijah . ' 
 - work figure programme 
 North Walsham Musical Society , Mr. A. S. 
 Wilde request permanent 
 conductorship 

 monthly concert Palette Club resume 
 onthe 2nd ult . , band club , Dr. Hardy , 
 excellent performance Mendelssohn Scotch 
 Symphony , overture ' Der Freischiitz , ' 
 March ' Tannhauser . ' Messrs. Gibson , Dallas , 
 Whittet contribute vocal solo 

 Glasgow Amateur Orchestral Society , conduct 
 Mr. W. T. Hoeck , resume rehearsal , chief 
 work study Prelude Dr. Elgar 
 ' dream Gerontius , ' Beethoven C minor Symphony , 
 composer Violin Concerto 

 early summer , lover music 
 amply cater International Exhibition , , 
 way , close door gth inst . band , 
 military string , nationality perform 
 daily . choral performance place 
 week . organ recital daily , 
 oftener , ' visit ' organist 
 Messrs. E. H. Lemare , Alfred Hollins , E. H. Thorne , 
 Dr. A. L. Peace . solo instrumentalist 
 perform Miss Fanny Davies , 
 Messrs. Ysaye , Hambourg , Kosman , Halstead ; 
 vocalist Mesdames Melba , Brema , 
 Macintyre , Messrs. Andrew Black Ben Davies . 
 unique feature Exhibition 
 music engagement week 
 Scottish Orchestra , Dr. F. H. Cowen . , 
 believe , instance country high - class 
 orchestral performance Exhibition 
 London . Dr. Cowen programme 
 select great care discrimination , ' popular ' 
 ' classical ' equally represent 

 - season 8th ult . , Dr. F. H. Cowen ! soloist 

 place director . scholarly reading 
 Beethoven ' Eroica ' Symphony obtain , 
 overture ' La Princesse Jaune ' ( Saint - Saéns ) 
 place programme . Lady Hallé , 
 artist popular Liverpool , play Beethoven 
 Romance G Mendelssohn Violin Concerto , 
 specially successful work . Miss Macintyre 
 sing song Mozart Chaminade . chorus 
 render power characteristically good attack 
 Handel ' Zadok Priest ' Macfarren ' Orpheus 
 lute 

 Richter concert ( season 
 ) occur 15th ult . , 
 substitution evening instead afternoon concert 
 apparently responsible increase attendance . 
 need scarcely concert plane 
 excellence Dr. Richter introduce 
 Liverpool music lover . programme constitute 
 entirely work Wagner , particularly 
 comprehensive character 

 Miss Teggin clear voice greatly delight large 
 audience 

 17th ult . , Hallé subscription season 
 inaugurate varied selection orchestral work , 
 include Nicodé variation ( op . 27)—whereby 
 interest excite scarcely maintain 
 close — concert arrangement Wagner Vorspiel 
 Liebestod ' Tristan , ' Dvordk second slavonic 
 Rhapsody , immortal C minor Symphony 
 Beethoven . oft select Prelude season 
 delight — Weber ' Oberon ' Overture — ethereal tone 
 horn Herr Paersch , extreme polish 
 performance prove , long vacation , 
 Dr. Richter force way deteriorate . 
 second meeting ( Oct. 24 ) notice defer : 
 Lady Hallé greet utmost 
 enthusiasm affection 

 importance recently organise Association 
 Nonconformist Choirs district scarcely 
 overrate . 
 roth ult . , extremely successful . Mr. Granville 
 Humphreys command 700 thoroughly enthu- 
 siastic , apparently experienced singer , 
 balance , soprano decidedly brilliant 
 fresh voice . Dr. Henry Watson ' Psalm 

 second meeting , Free Trade Hall 

 programme include Brahms horn trio Schumann 
 Quartet a. Miss Cantelo , solo pianist , q 
 beautiful rendering Beethoven Sonata Pathétique 

 flying visit Madame Blanche Marchesi , 
 16th ult . , noticeable fact party . 
 clude Miss Hilda Gee , violinist , native 
 Nottingham proud , warm welcome 
 hearty appreciation effort extend 

 YORKSHIRE town 

 record Yorkshire centre scanty 
 . Leeds unnaturally require breathing time 
 spasmodic effort imply Festival , 
 pass visit , 1st ult . , Mr. de Pachmann , 
 , - - way , excellent form , play Mozart 
 Chopin charmingly , present 
 chronicle . Bradford 
 November look return activity 
 musical matter . town excellent 
 Permanent Orchestra , strengthen large guarantee 
 fund , resume operation 4th ult . , Beethoven 
 Second Symphony Wagner ' Huldigungsmarsch ' 
 play Dr. Cowen direction , Madame Ella 
 Russell vocalist 

 foreign NOTES 

 DrespEN.—A new choral society form 
 performance oratorio important choral 
 work , old modern . thealready numerous choir , 
 consist musically cultured amateur , 
 direction Herr Waldemar von Baussnern . new opera 
 bring , ere long , Royal 
 Theatre . description ' humorous heroic opera , ' 
 point unconventional treatment 
 libretto , pen Eberhard Kénig 

 EIseENACH.—The Beethoven festival , hold 5th 
 7th ult . , direction Capellmeister Stein- 
 bach , Meiningen orchestra choir 
 Musik - Verein , exceedingly attend , 
 voice general excellence per- 
 formance . Professor Halir , Berlin , play 
 Violin Concerto , Mr. Frederick Lamond soloist 
 E flat Concerto , receive marked 
 applause . Capellmeister Steinbach conduct 

 _ FRANKFORT - - Matn.—An excellent highly appre- 
 ciate performance place 6th ult . , Stadt 
 Theater , Weber comic opera , ' Die drei Pintos ' 
 complete , great ability taste ( exist 
 sketch composition master ) 
 Herr Gustav Mahler , revise libretto 
 pen composer grandson , Carl von Weber . 
 Capellmeister Pittrich conduct 

 Maprip.—A successful performance place 
 month , Buen Retiro Theatre new - act 
 opera , ' Marcia , ' Cleto Zavala , obtain 
 prize competition open time ago 
 native composer . libretto , Senor Gonzalo Canto , 
 deal dramatically effective manner episode 
 roman conquest peninsula , musical 
 setting , national element want , 
 great satisfaction - minded 
 spanish musical amateurs.——Under auspex 

 Philharmonic Society , seventeen string quartet 
 | Beethoven perform present month , 
 | time Spain 

 MiLan.—The new sacred choral work , ' Moses , ' 
 | Lorenzo Perosi , shortly produce , 
 |been designate composer ' vocal symphonic 
 ' poem , ' divide , prologue . 
 ' important lottily 
 |conceive work produce young maéstro . 
 | Don Perosi , , apparently unlimited number 
 lof artistic message convey , busily 
 engage new sacred work , entitle ' 
 | Apocalypse 

 Erratum.—The statement issue ( p. 670 ) , KIMBERLEY . — second concert musical 
 performance Bach cantata ' Wachet aut ' ) 4 < cociation hold Town Hall , September 3 , 
 english word place recent Gloucester | hen programme include Mendelssohn 13th psalm , 
 Festival , apgee incorrect . Messrs. Boosey ' Lord , long wilt thou forget ? ' Mackenzie 
 Co. write ' english edition , Mr. Paul cantata , ' bride , ' - song , ' lullaby life ' 
 England word , perform Leeds Philharmonic ( Leslie ) , ' softly fall shade evening ' ( Hatton ) , 
 Society November , 1898 . ' steal love ' ( W. Macfarren ) . Madame Morton 
 Neville sing solo Mendelssohn Psalm , 
 — — — — — — — — — ol - — | Miss Dallas Mr. W. Burnford soloist 
 Mackenzie cantata . Mr. Frank Proudman conduct 

 BRIEF SUMMARY COUNTRY PERTH ( WESTERN AUSTRALIA).—The Musical Associa- 
 COLONIAL NEWS tion Western Australia concert Mechanics ’ 
 : Institute , September 7 , programme include : 
 hold responsible opinion express | sonata C major ( op . 3 , . 2 ) , Beethoven ( Miss 
 summary , notice collate local | Stafford ) ; Mozart trio string , b flat ( Mr. 
 paper supply correspondent . F. L. Parsons , Miss Parsons , Miss W. E. Parsons ) ; 
 prelude c sharp minor , Rachmaninoff ( Mr. R. 
 BrRIGHTON.—The Patronal Dedication Festival at|d’Arcy - Irvine ) . Mr. W. G. Ackray vocalist . 
 St. Michael Church , hold September 29 | musical evening , September g , 
 6th ult . Beethoven Mass C Goss anthem | Mechanics ’ Institute , Miss Stafford pupil . 
 ' o praise Lord Heaven , ' impressively render , 
 direction Mr. Edwin Stephenson , organist 
 choirmaster . feature service church 
 singing choral eucharist high gallery 
 west end , fine organ , recently build 
 Messrs. Norman Beard , place 

 Mr. Harold Hewitt appoint conductor 
 Catford Choral Society , succession Mr. Alfred Furse , 
 excellent work Society past year 

 single man . thoroughly competent , steady , reliable 

 good address . apply , state age . salary require , photo 
 reference , J. Bentley , Beethoven House , Nantwich 

 ante , improver tuning Re- 
 pairing . apply , reference , J. D. , Novello & Co. , Ltd. , 
 1 , Berners Street , W 

 ENUINE OLD VIOLIN sale . Pamphilon,| RRELIABLE second - hand organ 
 2 164 . original scroll . offer ? write A. Tarming , Banks ’ Rk p gg ge sa pegi omg ses tat 
 series aa sy = 6 , Pratt Street , Camden Town , N.W. Organ builder late 
 ante , PAIR DRUMS ( Tympani ) , second- | Majesty Queen H.R.H. Prince Consort . establish 1750 . 
 hand . m ch d d order . Repl Geo 

 Wallace , 4 , Brougham ee HUGH SWANTON . 
 sale , cheap , BEETHOVEN complete 
 work 47 Vols . B.andH. Kritische Gesammtausgabe . installation operation 

 ate Particulars A. S. H. , 1 , Craven Mansion , St. Paul Cathedral 
 Norwich Cathedral 

 CraMER.—In C , . 43 ( F. Taylor Studies , book xxv 

 BEETHOVEN.—Sonata E , op . 14 , . 1 ( edit 7 A.Zimmer- 
 mann , no.9 

 Bacu.—Invention , " . 6 , E 
 J. Higgs ) 

 Crementi.—In , . 9 Gradus ( F. Taylor wemeaine 
 book xv 

 BEETHOVEN . — allegretto f minor , Sonata , op . 10 , 
 . 2 ( edit A. Zimmermann , . 6 6 ) 
 HanpDeEv.—Courante e , — _ Suite IV 

 album , . 4 ) 
 LOCAL CENTRE — senior . 
 Bacu.—Fugue F , . 11 book I. ( 8 prelude fugue , 
 edit W. T. Best ) 
 MoscHELEs.—In e flat minor , op . 70 , . 8 ( F. Taylor 
 Studies , book xxxvi . ) 
 BEETHOVEN.—Prestissimo cc minor ( finale ) , " ' Sonata , 
 op . 10 , . 1 ( edit a. Zimmermann , . 5 ) 
 RHEINBERGER.—Fugue g minor , op . 5 , . 3 ( Pianoforte 
 Albums , . 23 ) ae 
 — — — .—Gigue , Suite . ( Pianoforte Albums , 
 o. 4 

 bedite 

 Pianoforte 

 CZERNY . — , op . 740 , . 26 ( F. Taylor Studies , Book xv i. ) 
 Mozart.—Adagio e flat , Sonata c minor noe el 
 a. Zimmermann , . 14a ) . 
 MENDELSSOHN.—Lied ohne Worte , b flat n minor , + op . 30 , . 2 
 ( lie . 8) e 
 school examination — elementary . 
 Lemoine.—In C , . 1 2 , op . 37 ( F. Taylor Studies , 
 Book iii . ) 
 Czerny.—In C , op . 599 , . 18 ( F. Taylor Studies , book 1 . ) 
 SCHOOL examination — low division . 
 Czerny.—In D , op . 636 , . 11 ( F. Taylor Studies , book x. ) 
 BEETHOVEN.—Rondo ( Allegro ) G , Sonatina , op . 49 , 
 . 1 ( edit A. Zimmermann , . 19 . ) .. 
 Czerny.—In C , - 299 , . 8 ( F. Taylor Studies , book 
 xvii . ) 
 Mozart . — minuet ; 2 . Sonata | ne flat ( edit 
 A. Zimmermann , . 4 ) . 
 SCHOOL EX amination — HIGHER division . 
 Bacu.—Invention aes . 13 , minor ( edit 
 J. Higgs ) 
 Czerny.—In C , op . 299 , ' . 5 ' ( F. Taylor " Studies , Book iv . ) 
 Mozart.—Allegretto ( Finale ) , Sonata d duaiaie ted el 
 A. Zimmermann , . 19 

 ORGAN , 
 LOCAL CENTRE — junior . 
 Best.—Thirty Progressive Studies , Op . 33 , . 18 , 29 , 30 
 SmarT.—Twelve short easy piece , no.1 e flat .. 
 MENDELSSOHN.—Allegretto F , Sonata IV 

 VIOLIN — ( continue 

 LOCAL CENTRE — senior . 
 FLoriLLto.—36 Etuden , . 3 , 12 , 28 .. 
 BEETHOVEN.—Sonata major , op . 12 , no.2 

 SCHOOL EXAMINATIONS — ELEMENTARY . 
 WILHELMJ.—Modern Violin School . BookIa .. 
 WILHELMJ.—Modern Violin School . book 
 Folk Dances Denmark . . 4 . 7 ( Wilhelm 

