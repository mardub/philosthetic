waltz king exis native city Viennz 

 king ag eaay « ag _ — debenture , retain year ( 
 scene early late triumph . MIS | sell advantage ) , order find 
 defect remedie erection | scholarship — honour immortal John 
 monument Stadtpark austrian } Sebastian Bach , honour 
 capital , similar memorial | immortal Ludwig van Beethoven — study 
 Schubert Bruckner . distinguished | work composer ; similar purpose 
 : Guildhall School Music , £ 5,000 Cambrian Railway 
 Preference Stock , hold year , 

 sculptor Edmund Hellmer commission 

 inclusive , follow work mainly 
 constitute programme : 
 Cuorat.—Elijah ( Mendelssohn ) ; Apostles ( E / gar 

 Noble number , solo voice , chorus , orchestra , 
 compose expressly festival ( Walford Davies ) ; 
 selection Lazarus , unfinished oratorio , per- 
 formance England ( Schudert ) ; selection Parsifal 
 ( lVagner ) ; Mass D ( Beethoven ) ; afraid , motet 
 double choir ( ach ) ; Job ( Parry ) ; ' , song , ' 
 chorus voice , time performance ( Z / var ) ; 
 creation , I. ( Haydn ) ; Messiah ( //ande 

 INSTRUMENTAL.—Symphony _ flat ( & /gar ) ; 
 symphony e flat ( / ozar¢ ) ; Old english Suite , time 
 performance ( arrange Granville Bantock ) ; 
 dance rhapsody , time performance ( Frederick elius 

 e 

 patronal festival Brighton Parish Church 
 hold July 1 , Parish Church Festival Chorus 
 service praise . choir , consist 120 voice , 
 accompany orchestra , include member 
 Municipal Orchestra , number - player , 
 service begin orchestral prelude , ' Sursum 
 Corda , ' Elgar , follow Schubert ' Song 
 Miriam . ' composer unfinished symphony 
 play orchestra , service conclude 
 festival anthem ' o praise Lord , ' Dr. , 
 Herbert Brewer , orchestra chorus . 
 Symphony , conduct Mr. Joseph Sainton , 
 musical director Brighton Corporation , service 
 conduct Mr. Chastey Hector , organist church , 
 Mr. T. Saxby , assistant - organist , play organ 
 voluntary . choir , year , 
 perform Bach Christmas oratorio , Beethoven ' Mount 
 Olives , ' Cowen ' Song thanksgiving , ' work 

 choir ( augment ) Kensal Rise Wesleyan 
 Church special musical service Sunday afternoon , 
 July 4 , Spohr cantata ' God , thou art great , ' 
 sing accompaniment string , pianoforte 
 organ , King Hall effective arrangement accompani- 
 ment use player keyboard instrument , 
 Sullivan setting ' lead , kindly light ' ( unaccompanied 
 Mozart ' glorious thy ' sing . 
 orchestra , lead Mr. Tinniswood , play Haydn second 
 Symphony Mendelssohn ' war march priest , ' 
 Mr. Arthur W. Daley , organist church , 
 play Hollins ' spring song ' organ . Miss F , 
 Kemp preside pianoforte , Mr. Charles E. Ransom 
 conduct 

 scoring . ambitious striking english | symphonic poem ' Villon , ' , remember , 
 work symphonic poem ' Boadicea , ' Mr. Montague | originally perform carly season New Symphony 

 hil t ( ' " lai » ox idered | . . ; 
 Phillips . forward strong claim consider | Oychestra . feature special interest Mozart 
 solely programme music , little characterization | Bauern - Symphonie movement , humorous 
 n theme fi . vove . > al e 
 theme , ' form govern abstract con-| character excellently reveal orchestra 
 sideration . music strongly conceive , | yy , William Murdoch soloist Beethoven fifth 
 construct musicianly hand . noisy ] pianoforte concerto . concert conclude walt 
 orchestration final section unfortunate defect,| } . Johann Strauss , vivacious strain wer 
 isno mere cloak emptiness , contrary , | play special verve student , doubtless inspire 
 mar effect imposing Conclanee . gleeful anticipation come vacation . Miss Gladys 
 neat performance Wieniawski Violin concerto Honey , Miss Doris Simpson Miss Dilys Jones 
 Miss Mary Law , direction Sir Charles | vocalist , Sir Charles Stanford conduct . 
 ~ . , 7 
 aine number , ' scottish 

 Stanford . 
 picture , ' programme conduct | 
 com poser . ; : p ; TRINITY COLLEGE MUSIC . 
 recent meeting Committee , follow | . 
 grant : - pound study orchestral concert student ( ) ueen 

 junds Mr. Edward Mason concert choral| exceedingly wholesome wherewith satisfy 
 work british composer . Committee decide artistic appetite , nourish desire good 
 publish Quintett pianoforte | true music . student acquaint fast 
 modern , ultra - modern , music ; 
 desirable know great classic 
 profit — word , learn t 
 walk learn run , sufier 

 Dunhill | Handel , Mozart , Beethoven , Schubert , Mendelssohn 
 | Schumann represent . substantial fare 

 delray cost 
 ng , compose Mr. James Friskin , Royal College 

 tion Al Ira Palace Ch dde othe 
 portion Alexandra Palace Choir add 
 laurel win Mr. Allen Gill 

 Mr. Victor Benham play Beethoven Pianoforte 
 concerto e flat , conduct Mr. Allen Gill . 
 ' Dylan ' perform Mr. Beecham direction , 
 remainder performance conduct 
 Mr. Holbrooke . glad concert - giver 
 expectation increase resonance absence 
 audience disappoint 

 Signor Illica , compiler 

 WIESBADEN 

 organ performance Signor . Enrico 
 Bossi , Bologna , introduce interesting | 
 beautiful Concerto minor ( ( ) p. 100 ) organ , string 
 orchestra , horn , drum . work , excellently | 
 perform composer , achieve great success 
 season conclude excellent choral performance 
 — Beethoven ' Missa solemnis , ' Cacilienverein , 
 Bach ' Passion ' accord St. Matthew , 
 Bachverein . work cut 
 original arrangement orchestra ; , 
 section day | 
 o'clock , overtire performer | 
 listener 

 

 prologue August Strindberg , great swedish poet 

 follow Beethoven programme , consist 
 String quartet e flat ( Op . 74 ) , ' geistliche 
 Lieder , ' great trio b flat ( Op . 97 ) , String 
 quintet ( Op . 29 ) . programme second day 
 devote exclusively swedish composition , bring 
 forward Franz Berwald String quartet e flat 
 Pianoforte quintet , Ludwig Norman 
 Pidnoforte quartet Ballad ' Kong Hakes Déd , ' 
 baritone , male chorus pianoforte . day 
 programme exclusively work Brahms 
 Pianoforte quintet ( op . 34 ) , trio e flat ( op . 40 ) , 
 String sextet G ( op . 36 ) , 
 beautiful song . artist festival 
 Messrs. Wilhelm Stenhammar , Aulin ( uartet , 
 Professor Franz Neruda , brother Lady Hallé , 
 Salomon Smith 

 design educational line refine influence , 
 , far , enable good classical modern music 

 commendable consideration set forth circular 
 announce promenade concert 
 music - love city November 18 December 2 . 
 orchestra , consist - player , 
 complete consist instrumentalist try 
 ability experience . concert , 
 purpose profit , artistic 
 sense , management conductorship 
 Mr. J. A. Rodgers . circular sign Messrs. E. 
 Willoughby Firth , T. Walter Hall C. D. Leng . 
 price admission range half - - crown shilling , 
 order ' class opportunity 
 attend . ' experiment watch interest 

 Kimberley Musical Association concert 
 Town Hall , Kimberley , June 10 11 . 
 occasion Elgar ' Banner St. George ' perform , 
 chief feature programme Beethoven 
 * Prometheus ’ Overture Haydn symphony D ( . 4 ) . 
 second concert ' Hiawatha Wedding - feast ' 
 main attraction , programme include spin 
 chorus ' fly Dutchman , ' Faning - song 
 * Moonlight , ' Meyerbeer ' coronation March , ' Haydn 
 Symphony repeat request . solo vocalist 
 Miss Ada Forrest Mr. L. Mirwish . orchestra 
 lead Herr Rybinkar , Mr. A. H. Ashworth 
 conduct excellent performance chief work 

 publisher ' Arethusa , ' Mr. W. H. Ibbersor , 
 review issue ( p. 455 ) , Messrs. J. Wood , ot 
 Huddersfield ; ' Pastoral overture G , organ , 
 review page , Mr. William Faulkes 

 Training College , Dublin 

 symphony Beethoven , include ' Choral , ' 
 perform Munich August | 
 week September connection series concert | * 
 refer p. 472 July issue . 

 Herr Hugo Becker appoint professor 
 violoncello K6nigliche Ilochschule fiir Musik , 
 Berlin , succession late Professor Hausmann 

 te ved le tet 7 ~ 
 enswer correspondent . 
 ' England bass singer | .. eee 
 low c : Russia are| 7HAEZE extra supplement number 
 xt 1 ptt ri » capable ak ° , . , ro ? " 
 extraordinary depth capable 1 . Portrait Johann Strauss : Waltz King . 
 = | 2 . Anthem Harvest : * Gt wr , o ye Heavens ’ 
 IValter G. 

 JUERY.—(1 ) play grace note indicate } 
 -otta edition ( Beethoven Pianoforte sonata D , Op . 10 , | 
 N 

 Competition Festival Ree 

