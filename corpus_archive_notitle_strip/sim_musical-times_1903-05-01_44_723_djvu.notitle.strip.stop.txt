ConTENTS 

 ... ae . Schumann 
 Song ... Beethoven 
 Autumn Song isa ‘ aa pe Mendelssohn 
 Come , smiling Liberty ... aaa ae Pee fe Handel 
 Come , gladsome Spring ( celebrated Largo — Ombra 

 mai Fi — ‘ “ ‘ Xerxes ’’ ) ... ep : d xe Handel 
 Come , happy Spring ( Caro mio ben ) ... Giordani 
 Contentment os aa aa es Mozart 
 Creation Hymn ... . Beethoven 
 Crusaders .. Schubert 
 Evening Song Mendelssohn 
 Fairest Isle ... Purcell 
 Forget W. S. Bennett 
 Greeting ... J Mendelssohn 
 Hark ! hark ! thelark ... ha Oa aed Schubert 
 Hear thou weeping ( Laschia ch’io pianga ) ‘ Handel 
 Hey , Baloo ! ... ea sas ‘ si pee ... Schumann 
 - dew fe W. S. Bennett 
 Song ... Aa eee Mendelssohn 
 mother bids bind hair ue Haydn 
 O wings dove Mendelssohn 
 O sunny beam ieee saat . Schumann 
 Rose softly blooming _ _ ... aaa Spohr 
 ye borrow ( Voi che sapete ) ... ies Mozart 
 Slumber Song tier cae aoa ; Mendelssohn 
 Sun sleepless Mendelssohn 
 Cottage « . Schumann 
 violet Mendelssohn 
 Fisherman < 0 Schubert 
 Mermaid Song ... Haydn 
 wandering Miller .. Schubert 
 Chloe ( sickness ) ... aa W. S. Bennett 
 Verdant meadows ( Verdi Prati ) és Handel 
 Welcome Spring ae Mendelssohn 
 rie ‘ Schubert 
 Sylvia ? Schubert 

 Voice Parts Staff Tonic Sol - fa Notations , Pianoforte 
 Accompaniment , price Shilling Sixpence 

 thing eminently remarkable , 
 worth noting , , 
 time siege person , 
 hear , church receive 
 harm devilish cannon shot ; 
 verily believe constantly 
 thousand persons service 
 Sunday time siege 

 York Musical Festivals deserve notice 
 connection history Minster . 
 , held 1791 , lasted days , 
 sacred music , Handel , performed 
 Choir Minster . Ashley Matthew 
 Camidge , son organist , 
 conducted . stated 
 Festivals held annually 1803 , 
 statement borne facts , 
 far Minster concerned . great 
 Festival , held September , 1823 , resulted 
 publication John Crosse elaborate 
 ‘ Account ’ , issued 1825 . 
 occasion performers—180 instrumen- 
 talists 285 vocalists — occupied platform 
 specially erected central tower . 
 scheme consisted sacred concerts 
 Minster , secular concerts 
 balls given Assembly Rooms . 
 receipts amounted substantial sum 
 £ 16,174 16s . 8d . , profits ( £ 7,200 ) 
 divided hospitals York , Leeds , 
 Sheffield , Hull . incidents 
 great music - making notice . Madame 
 Catalani , prima donna meeting , 
 appropriated ‘ Comfort ye ’ ‘ Ev'ry 
 valley , ’ sang D ! ‘ practice 
 sufficiently reprobated , ’ said 
 Harmonicon , ‘ distinguished singer 
 perform Handel wrote , 
 person ought selected 
 purpose ; Mr. Vaughan present , 
 justice . ’ second 
 evening concert ( September 25 , 1823 ) , Beethoven 
 C Minor Symphony headed programme . 
 Owing non - arrival London 
 additional string parts proposed omit 
 Symphony proceed 
 number programme — ‘ Charley 
 darling . ’ Miss Travis began sing 
 Scotch ballad general murmur disapproval 
 manifested audience , , 
 according late John Ella ( member 
 band ) , ‘ stewards , grave- 
 looking , bald - headed gentleman sten- 
 torian voice , lustily exclaimed : ‘ Symphony . 
 darlings , hear day 
 Yorkshire ; insist Symphony 
 played . ” ’ Itwas performed , 
 players crowd desks 
 order read music . honour 

 XUM 

 man like Stradella . scarcely 
 artistic ancestor , clamoured 
 expression pipes Pan ; 
 come came 
 music resounding ears ; 
 wise learn 
 technique art fell victim 
 common assassin . Corelli , , musician 
 springtime West , peered futurity 
 assumed position 
 considered lifetime absolute 
 fulfilment ; beautiful work 
 proclaim completely assimilated 
 law teaching past ; 
 help think — 
 inexorable law — trifle pathetic find 
 knowledge , 
 study , genius , leaf 
 spring summer 
 aught said 

 distinguished harbinger modern times , 
 Christopher Gluck . Gluck , true , 
 reformer : emphatically understood 
 essence dramatic art far applied 
 music . study Greek drama 
 straitly shown large extent 
 opera written 
 possess beauty musical expression , 
 dramatic verisimilitude . 
 despite theory , truth remains Gluck 
 contrive evade melodic spirit 
 time ; magnificent musical inspiration 
 , confident perfectly true 
 dramatic theory , remains 
 absolute fact accomplishment , 
 work exquisitely beautiful , reality 
 contained far spirit futurity 
 expected man 
 musical dramatic attitude . Gluck 
 far 
 present reckoning goes spring musicians . 
 Mozart led way thesummer ; Beethoven 
 set trees calling note birds ; 
 Wagner drove wain fields 
 harvest , , think , - day 
 time fruitage ripe corn . 
 need resurrection bring days 
 musical spring composers 
 learnt necessity ( mind , , 
 time , stern necessity ) avoiding 
 unused simplicity 

 sifting important 
 musical art utterly 
 ephemeral , earth earthy , task 
 care 
 undertake ; matter interest 
 observe later times , reason 
 accumulations mere material , distinctly 
 lowered average earlier days 
 saw rise music springtime . 
 old days man dream perpetuating 
 musical air definite genuine 
 value ; means disposal . 
 present time , , man 
 conceive melody utmost 
 unimportance power burden 
 public , utter 
 disadvantage care musical 
 phrase musical thought . 
 enforcement parable . perfectly 
 convinced privileges entailed 
 knowing , appreciating , enjoying 
 works later masters musical art . 
 , impossible feel 
 certain lingering envy heard 
 music composed enthusiasts , 
 played gentle artist , 
 circulated 
 music sacred requiring sacred 
 treatment . wandered idea 
 musical spring ? think . early 
 inspirations surely daffodils music ; 
 came swallow dared , 
 took winds March beauty . 

 309 

 late Mr. Henry C. Lunn , Tue Musicar 
 Times March , 1875 . merits quotation : — 
 time evening orchestral rehearsals 
 Academy , directed Sterndale 
 Bennett . students trained contem- 
 plation good music ; young 
 conductor , recollect , allow pupil remain 
 ignorance composition performed . ‘ Listen , ’ 
 said , evening entrance ; ‘ 
 Symphony Beethoven ; try comprehend 
 symmetrical construction work , carry away 
 think 

 Broadwood generosity exhausted 
 Diisseldorf trip , October 
 year Bennett visited Germany , Leipzig 
 destination . Mendelssohn 
 received open arms , introduced 
 Schumann . Temperamentally 
 different , musicians — Mendelssohn 
 Schumann — fell love young 
 Englishman . references Bennett 
 letters Schumann charming 
 Mendelssohn letter quoted . 
 sister - - law , Schumann writes ( Leipzig , 
 November 15 , 1836):—‘There young 
 Englishman meet day , 
 William Bennett — thorough Englishman , 
 glorious artist , beautiful poetical 
 soul . letter refers 
 ‘ perfect angel composer . ’ : 
 ‘ Mind send Bennett letter . 
 Bennett rascal , write soul . 
 thought . ’ ‘ 
 delight ’ writes Schumann 
 correspondent , . 
 . Schumann publicly proclaimed 
 opinion belief Bennett composer 
 high rank articles wrote 
 Neue Zeitschrift fiiy Musik , journal 
 ( Schumann ) founded edited . 
 specimen — _ similar 
 ‘ appreciations 

 WILLIAM STERNDALE BENNETT 

 thought best 
 present readers beginning year 
 1837 , better struck idea 
 placing ( good wishes ) 
 delightful individuality . Beethovenian 
 , drawing years strife , Berlioz , 
 preaching revolution heroic voice , spreading 
 destruction fear , gentle , 
 quiet spirit , labours high , matter 
 storms gather , like observer 
 stars , following course remarking 
 nature peculiarities . found 
 ; fatherland Shakespeare , 
 Christian poet . arts 
 poesy tone foreign 
 famous land given Shakespeare 
 Byron bring forth great musician ? 
 old prejudice begins waver means 
 names Field , Onslow , Potter , Bishop 
 ; abolishing 
 artist , cradle watched 
 kind Providence 

 Schumann , referring friend 
 Musical Sketches ( Lake , Millstream , 
 Fountain ) , calls ‘ lovely 

 MUSICAL TIMES.—May 1 , 1903 . 31 

 Menuetto Beethoven Fourth Symphony 
 shows conductor - analyst best vein — 
 ‘ twould vain attempt imitate . ( 
 example consists opening bars 

 Lee Williams cantata ‘ Bethany ’ given 
 English Church Palm Sunday , 
 direction organist , Mr. B. Ramsey , 
 assistance Miss Muriel Foster Herr Bertheau 

 Mr. Bosville parting shot . 4 

 Beethoven usual ‘ form 

 , delightful romp 
 , inexorable hour nursery 
 bed - time arrived , principal subject 
 moment toned comparative calm 
 quavers . gently fascinating 
 subject ‘ Good - night ’ assembled 
 company . dittle pauses momentarily delay 
 ceremony ( represent reluctance 
 displayed kissing ‘ strange gentleman ’ 
 bearded aunt ) , good little subject dashes 
 upstairs nursery final burst juvenile 
 exuberance 

 PHILHARMONIC SOCIETY 

 concert given March 26 Queen Hall 
 lengthy notice . sure , included 
 performance England Herr Emil Sauer second 
 Pianoforte Concerto C minor , composer 
 keyboard , work great artistic importance . 
 principal theme possesses distinctive didactic 
 character , designed style treatment 
 ( known metamorphosis ) invented Liszt , 
 music marred apparent effort produce effects 
 frequently resulting exaggerations . concert 
 opened performance London Mr. 
 Arthur Hervey overture ‘ Youth , ’ conducted 
 composer . described columns 
 occasion original production Norwich 
 Festival autumn , unnecessary comment 
 work , added 
 - hearing confirmed impressions recorded 
 vivacious inspiriting music . choice Dvorak 
 Symphony ( . 4 ) G commended , 
 interpreted fascinating crispness finish 
 able direction Dr. Cowen . engagement 
 Mr. Gordon Tanner play solo Beethoven 
 Violin Concerto error judgment . artists 
 rank heard masterpiece 
 Philharmonic Society concerts . vocalist 
 Fraulein Rosa Olitzka , sang finely aria 
 ‘ Aus der Tiefe des Grames , ’ Dr. Max Bruch 
 ‘ Achilleus 

 pianoforte recital given March 25 Bechstein 
 Hall Mr. Frank Merrick , native Clifton 
 pupil Professor Leschetitzky , deserves special mention . 
 interpretations Beethoven Sonata flat 
 ( Op . 26 ) classics justify sanguine 

 expectations future , expression 
 opinion , intellectually executively , young 
 Mr. Merrick makings great artist . 
 future career watched unusual interest 

 opera spending weeks ‘ road , ’ 
 theatrical phrase ( dare ‘ 
 provinces ’ cities concerned Boston , Chicago , 
 Cincinnati , Pittsburgh ) , metropolitan folk 
 enjoying rest indulging reminiscences . 
 season lasted seventeen weeks , unfortunately 
 ended promises prospectus 
 unfulfilled unavoidable causes ; 
 failure deplorable . projected Mozart 
 cycle proud , artistic , 
 achievement ; frustrated illness Madame 
 Eames . spare readers Musica TiMEs 
 names operas given , - - 
 correct indication tastes New York public 
 append comparative list composers : Wagner , 
 operas , 27 performances ; Verdi , seven 
 operas , 21 ; Gounod , , 29 ; Meyerbeer , 
 , 8 ; Donizetti , , 7 ; Puccini , , 7 ; 
 Leoncavallo , , 6 ; Mozart , , 4 ; 
 Rossini , , 3 ; Bizet , , 3 ; Mancinelli , 
 , 2 ; Miss Smyth , , 2 ; Mascagni , 
 , 1 . - performances Italian , 
 - German , - French 

 metropolitan concert season 
 active interesting predecessors 
 years . fewer visitors abroad , 
 Herr Hugo Heermann , German violinist , 
 proved - class attractiveness . success 
 indubitable , , especially centres musical 
 culture New York Boston . best achievements 
 Beethoven Brahms concertos ; 
 introduced Richard Strauss concerto . New York 
 listened large number concerts worthy 
 note beginning November . 
 - symphony concerts highest class , 
 - concerts chamber music ( public , course ) , 
 sixteen choral concerts , - pianoforte recitals , 
 - song recitals , - orchestral concerts 
 popular character , half - - 
 entertainments difficult toclassify . figures 
 approximately correct , depending 
 record date - book tell 
 affairs escaped record . choral works , performances 
 ‘ St. Paul , ’ ‘ Messiah ’ ( Professor Prout edition ) , 
 _ * ‘ Gerontius , ’ Oratorio Society , 
 noted 

 Dr. Elgar work profound impression 
 novelty years , time 
 longer period Oratorio Society found reward 
 attendance appreciation bestowed new work . 
 performance took place March 26 
 direction Mr. Frank Damrosch , solo parts 
 hands Miss Ada Crossley , Ellison Van Hoose 
 David Bispham , chorus 350 voices small 
 choir Musical Art Society — body professional 
 singers . orchestra numbered 80 , per- 
 formance short brilliant respect . 
 public enthusiasm great performance 
 wide expectation Society 
 repeat work ; conservative counsels prevailed , 
 thought best rest fragrant laurels garnered 
 season revive work . appearances 
 deceptive Oratorio Society added attractive 
 work permanent repertory . Witha single exception 
 New York critics placed Dr. Elgar forefront 
 living composers 

 MUSIC BIRMINGHAM . 
 ( CORRESPONDENT 

 ninth Halford concert , March 24 , 
 important function , introduced 
 time provinces ‘ Heldenleben ’ Richard 
 Strauss , work long rehearsal , 
 admirably performed . interest excited 
 great , music unanimously accepted 
 true art . concert Mackenzie Overture 
 ‘ Cricket Hearth ’ produced success , 
 Dr. Brodsky gave fine rendering Bach Violin 
 Concerto Aminor . Atthe tenth concert , 7th ult . , 
 ‘ Heldenleben ’ repeated , audience 
 largest season . work great 
 impression . Elgar beautiful Variations origina ) 
 theme orchestra , Beethoven Concerto G 
 pianoforte orchestra , Mr. Leonard Borwick 
 soloist , features fine concert . 
 Halford Concerts Society lacks adequate support , : 
 intend persevering , able conductor 
 Mr. Halford success crown efforts 

 concert given members Midland 
 Institute Amateur Orchestral Society late 
 March deserves notice , inasmuch introduced 
 work composer hitherto unknown , fancy , 
 England . dramatic overture based 
 De Musset poem ‘ Rolla , ’ work considerable 
 merit , composer Mr. C. E. Pritchard , 
 born France , musically educated Paris 
 Conservatoire . performance conducted 
 composer , cordially received . 
 remaining pieces programme — Svendsen Second 
 Symphony Spohr Overture ‘ Jessonda ' — went 
 Mr. E. W. Priestley , acting Mr. Granville 
 Bantock , conducting performance Elgar 
 ‘ Gerontius ’ Wolverhampton evening 

 33 

 Midland Institute School Music second 
 annual Wind Instruments Concert given 
 18th ult . performers professors School , 
 concert object - lesson students . 
 concerted pieces Reinecke Trio minor 
 ( Op . 188 ) oboe , horn , pianoforte , Beethoven 
 Quintet E flat ( Op . 16 ) pianoforte wind 
 instruments 

 MUSIC BRISTOL DISTRICT . 
 ( CORRESPONDENT 

 MUSIC DUBLIN . 
 ( CORRESPONDENT 

 6th ult . Dublin Orchestral Society gave 
 concert . Grieg ‘ Peer Gynt ’ Suite . 1 , 
 Max Bruch Violin Concerto G minor ( soloist , Herr 
 Adolph Wilhelmj ) , Wagner ‘ Siegfried Idyll , ’ 
 Beethoven Fifth Symphony programme . 
 Society started seasons ago , , 
 owing lack funds , come end . effort 
 reconstitute , hoped 
 successful . admirable orchestra Signor 
 Esposito trained worthy support 
 local amateurs 

 Orpheus Choral Society , conductorship 
 Dr. Culwick , gave 3rd ult.a concert madrigals , 
 glees , - songs . Miss Edith Marks ( soprano ) , Mr. 
 Redfern ( flute ) , Miss Annie Lord ( pianoforte ) , 
 soloists . Miss Constance Greene , talented amateur , 
 played accompaniments 

 MUSIC GLASGOW . 
 ( CORRESPONDENT 

 fourth season Halstead- 
 Verbrugghen Chamber Concerts took place March 23 
 good audience subscribers general 
 public . programme included Beethoven String 
 Quartet ( Op . 59 , . 3 ) , César Franck Sonata 
 Pianoforte Violin , Sinding Quintet E minor . 
 sonata , Messrs. Halstead Verbrugghen 
 distinguished appearance , quartet ensemble 
 irreproachable , especially slow movement , 
 played charming delicacy 

 March 24 choir Claremont Church ( Mr. 
 Hutton Malcolm , organist choirmaster ) performed 
 Theodore Dubois carefully - written somewhat un- 
 inspiring oratorio , ‘ seven words Christ . ’ 
 work , set soprano , tenor , baritone 
 soloists chorus , produced Paris 1867 , 
 frequently performed France Lent , 
 heard country . solo 
 music forms great work 
 given Miss Macfarlane Messrs. Adams 
 Malcolm , members choir sang choruses 
 creditably . performance oratorio 
 preceded interesting organ recital included 
 Guilmant seventh Organ Sonata 

 Cheltenham Musical Festival Society gave 
 2nd ult . fine performance ‘ Messiah ’ 
 Winter Gardens . soloists Madame Siviter , 
 Miss Susanne Palmer , Miss Bertha Salter , Mr. Charles 
 Saunders , Mr. R. Radford , trumpet obbligato 
 solo ‘ trumpet shall sound ’ finely played 
 Mr. J. Solomon . band led Mr. E. G. 
 Woodward Mr. Lewis Hann , able 
 conductorship Mr. Matthews , rendering 
 oratorio gave great pleasure large repre- 
 sentative audience 

 years existence , Gloucester- 
 shire Orchestral Society remarkable progress , 
 numbers efficiency . Mr. A. Herbert 
 Brewer , valued assistance Mr. W. H. Reed 
 assistant conductor instructor , gathered 
 band amateurs man 
 proud direct . second concert 
 Society given 15th ult . Shire Hall , 
 Gloucester , large appreciative audience . 
 works performed Schubert ‘ Unfinished ’ 
 Symphony , Sérénade Strings ( Elégie Valse ) 
 Tschaikovsky , Ambroise Thomas ‘ Mignon ’ Gavotte , 
 Beethoven ‘ Egmont ’ Overture , Vorspiel ‘ Hansel 
 und Gretel ’ Humperdinck , Bavarian Dances 
 ( Op . 27 ) Elgar . Mr. W. H. Reed contributed 
 violin solos , Nocturne ( Chopin - Sarasate ) ‘ Canzonetta ’ 
 ( D'Ambrosio ) , solo vocalist occasion 
 Miss Alice Hollander , pleasant 
 impression artistic singing Mr. A. W. Vine , 
 organist Tewkesbury Abbey , proved careful 
 sympathetic accompanist 

 Miss Edith Lavington , promising soprano , gave 
 successful concert Cheltenham 16th ult . 
 Victoria Rooms . sang solos intelli- 
 gently acceptably , assisted Miss 
 Hilda Wilson , Mr.:H. Lane Wilson , Mr. Herbert 
 Grover ( vocalists ) ; Miss Isabel Hirschfeld ( pianoforte ) ; 
 Mr. J. E. R. Teague ( violoncello ) ; Mr. James 
 Capener efficient accompanist 

 Stapleford Choral Society rendered Mendelssohn 
 « St. Paul ’ March 31 . artists Miss Gertrude 
 Crisp , Miss Eunice Paulson , Mr. Killingley Mr. 
 Harry Reynolds . Mr. Wyatt presided organ 
 Mr. George Spence conducted 

 orchestral concert season Nottingham 
 given Mr. Arthur Richards 4th ult . , 
 assisted group local performers . 
 programme decidedly ambitious , containing 
 Schubert ‘ Unfinished ’ Symphony Beethoven 
 C minor Pianoforte Concerto , Miss Alice Hogg 
 soloist . highest credit Mr. Richards 
 careful artistic performance trying pro- 
 gramme , Madame Annie Norledge , vocalist , 
 rendered songs , orchestral accompaniment , 
 keen artistic feeling 

 weeks good musical work 
 Nottingham churches—‘St . Paul ’ 
 ( Mendelssohn ) Tabernacle , ‘ Hymn Praise ’ 
 ( Mendelssohn ) Shakespeare Street Free Church , 
 ‘ Crucifixion ’ ( Stainer ) St. John Church , 
 new ‘ Passion ’ ( Varley Roberts ) St. Thomas Church 

 Mendelssohn ‘ Christus ’ Stainer ‘ Crucifixion ’ 
 furnished scope Eastertide musical activities 
 St. John Church , Ranmoor , Mr. J. C. V. Stacey 
 number years cause 
 music pleasant suburb . performance 
 Handel ‘ Judas Maccabeeus ’ Wycliffe Congregational 
 Church 6th ult . , furnished instance 
 popularity oratorios inchurch . Ecclesfield 
 14th ult . , concert given Instrumental Society 
 Mr. Thomas Brameld proved earnest 
 intelligent musical effort valuable work rural 
 districts — Mozart ‘ Parisian ’ Symphony , Weber ‘ Der 
 Freischiitz ’ Overture , works similar calibre , 
 indicate musical progress Ecclesfield way 

 2oth ult . Sheffield Male Glee Madrigal 
 Society gave concert Montgomery Hall , 
 direction Mr. J. A. Rodgers . programme 
 included glees , madrigals , - songs Netherclift , 
 Carnall , Mackenzie , Schumann ( ‘ Battle Song ’ ) , 
 Percy Pitt ( ‘ Sunset ’ ) . Lane - Wilson song cycle 
 ‘ Flora Holiday ’ sung Miss Margaret Cooper , 
 Miss Ada Freeman , Messrs. W. Burrows A. Muscroft . 
 Miss Dorothy Peck ( pianoforte ) Mr. John Peck 
 { violin ) performed Beethoven ‘ Kreutzer ’ Sonata . 
 evening wind instrument Chamber Concert 
 given Assembly Rooms , Rotherham , 
 direction Mr. Duffel ’ , Beethoven Quintet 
 ( Op . 16 ) pianoforte wind instruments , Pauer 
 Quintet ( Op . 44 ) , Duncan Quintet ( Op . 38 ) , 
 Quintet Barthe ( flute , oboe , clarinet , horn , 
 bassoon ) performed 

 closing days month brought pressure 
 musical fixtures . ‘ Prout ’ programme , given 
 Albert Hall Brincliffe Musical Society 
 21st ult . , interesting . Professor 
 conducted long list compositions , chief 
 Symphony ( . 3 ) F major , Suite de 
 Ballet ( Op . 28 ) , Triumphal March ‘ Alfred . ’ 
 Pianoforte Quintet G major , Romance 
 F major pianoforte violin ( Professor Prout 
 Mr. J. H. Parkes ) included excellent 
 scheme . Miss Nellie Chisholm Mr. J. Lycett 
 vocalists 

 DEWSBURY 

 Mr. Fricker conducts Dewsbury Choral 
 Society , gave March 24 , direction , 
 remarkably fine performance ‘ Triumphlied ’ 
 Brahms , work taxes powers best 
 choruses . sung spirit jubilation 
 sustained power demanded music , 
 marked effect . Beethoven Second 
 Symphony Mr. Arthur Hervey bright ‘ Youth ’ 
 Overture included generally interesting 
 programme 

 Harrogate , March 27 , Choral Society gave 
 artistic performance Dvorak ‘ Stabat Mater , ’ 
 conducted Mr. C. L. Naylor , gave sympathetic 
 reading beautiful music 

 October 1 competition day Mendels- 
 sohn scholarships 1,500 marks : composi- 
 tion , executive artists . time 
 interest capital sum 30,000 marks,—the gift 
 Herr Ernst von Mendelssohn - Bartholdy , 
 bankers Robert Franz von Mendelssohn — certain 
 accumulations interest , divided . scholar- 
 ships interest grants bestowed 
 deserving pupils training institutions 
 Germany subsidized State , irrespective age , sex , 
 religion nationality 

 programme Ninth Symphony Concert 
 included works : Beethoven ‘ Pastoral ’ 
 Liszt ‘ Faust ’ Symphony , admirable performances 
 given direction Felix Weingartner . 
 Berlin favourable Liszt 
 composer , addition ‘ Faust ’ ‘ Dante ’ 
 Symphony recently given , B minor 
 Sonata ‘ Dante Fantasie - Sonate ’ 
 heard , times . 
 reaction Liszt favour . writer 

 Allgemeniene Musik Zeitung recalls composer 
 prophecy , ‘ time come . ’ 
 COLOGNE 

 MARSEILLES 

 Beethoven Symphonies given 
 able direction M. Viardot . soloists 
 . 9 Mlles . Charlotte Lormont Charlotte 
 Melno MM . Challet Dantu Chevillard 
 Concerts . Richard Strauss paid visit March 
 Berlin Orchestra , occasion pro- 
 gramme included works : ‘ Aus Italien ’ 
 ‘ Tod und Verklarung 

 PARIS 

 M. Gailhard , hearing M. Vincent d’Indy 
 ‘ L’Etranger ’ Brussels , decided work 
 opera house season . M. Ernst Reyer remained 
 Paris longer intended hopes hearing 
 Jean de Reszke ‘ Sigurd . ’ Fates proved 
 unpropitious , doubtful 
 favourable circumstancés eminent tenor 
 sufficiently recovered attack influenza 
 appear opera Easter , veteran composer 
 returned estate near Marseilles 

 Neue Zeitschrift fiir Musik anarticle thirtieth 
 anniversary foundation Colonne concerts 
 states 808 concerts given 
 time , 267 composers represented : 129 
 French , 138 foreign . following comparative list 
 performances interesting : — Berlioz , 448 ; Beethoven , 
 374 ; Wagner , 366 ; Saint - Saéns , 338 ; Mendelssohn , 169 ; 
 Massenet , 166 ; Schumann , 136 ; Mozart 108 

 Eduard Grieg , concert - touring , 
 conducted Colonne Concert Chatelet 
 19th ult . programme included Overture 
 ‘ Autumn , ’ Pianoforte Concerto ( M. Raoul 
 Pugno soloist ) , ‘ Peer Gynt ’ suite , ‘ Vor der 
 Klosterpforte ’ soli , female chorus , orchestra , 
 songs rendered Madame Gulbranson 

 seventh Feis Ceoil , Irish Musical Festival , 
 held Dublin 18th , 19th , 2oth , 21st , 22nd , 
 23rd month , Antient Concert Rooms , 
 , kind permission Senate , Royal 
 University . previous Festivals , 
 taken place Dublin Belfast . 
 wide assortment competitions 
 Commercial Choirs , ranks ‘ sleeping 
 partners shareholders eligible compete ’ ; 
 ‘ orchestra - 
 players ’ ; particularly interesting special 
 competitions National music , 
 playings small Irish harp , pipes , & c 

 Lower Rhine Music Festival held 
 year Aachen 31st inst . , rst 2nd 
 prox . , direction Professor Eberhard 
 Schwickerath Hofkapellmeister Felix Weingartner 
 programmes include Beethoven Mass D 
 Seventh Symphony ( day ) , Berlioz ‘ Symphonie 
 Fantastique ’ ‘ Faust ’ ( second day ) . day 
 Festival promised miscellaneous selection 
 e.g. , Bach Church Cantata ‘ O Light everlasting , ’ Liszt 
 ‘ Mazeppa ’ Symphony , Weingartner ‘ Das Gefilde der 
 Seligen , ’ & c 

 Mr. William Pountney , sang Birmingham 
 Musical Festival 1846 ( ‘ Elijah ’ produced ) 
 subsequent occasion ( save ) , 
 gained admittance chorus year music- 
 making . understand examiner tried Mr. 
 Pountney bass voice lower D upper F said 
 satisfactory . length service 
 nature record , Mr. Pountney heartily 
 congratulated conservation vocal powers 

 GRAHAMSTOWN , CAPE CoLony.—Mrs . W. Deane ( Miss 
 Grace Batchelder ) gave lecture ‘ Schumann 
 Pianoforte Works , ’ March 12 , members 
 Grahamstown Atheneum . Mrs. Deane played 
 illustrations ‘ Papillons ’ ( Op . 2 ) , ‘ Carnaval ’ ( Op . 9 ) , 
 ‘ Etudes Symphoniques ’ ( Op . 13 

 GRAVESEND.—On March 25 Orchestral Society 
 gave concert New Public Hall aid 
 Gravesend Hospital . Selections Haydn , Beethoven , 
 Schumann , & c. , given . Vocal solos artistically 
 rendered Miss Winifred Marwood , Mrs. Firth , 
 Mr. Montague Borwell . Mr. C. Burrows Moss accom- 
 panied , Mr. Howard Moss conducted 

 Grimssy.—An enjoyable musical service took place 
 parish church 5th ult . , Spohr ' ‘ 
 Judgment ’ sung augmented choir seventy 
 voices , accompanied orchestra . choruses 
 given precision , expression , spirit , 
 skilful conductorship Mr. J. Forbes Carter . Miss 
 S. E. Bennett Miss Wyld organ 
 pianoforte respectively 

 SouTHPORT.—The Southport Orchestral Society gave 
 subscription concert sixteenth season 
 3rd ult . crowded audience . orchestra 
 numbered seventy , following interesting pro- 
 gramme performed conductorship 
 Mr. R. H. Aldridge : — ‘ Danse Négre ’ ‘ African 
 Suite ’ ( Coleridge - Taylor ) , Overture ‘ Cockaigne ’ ( Elgar ) , 
 Dream Music ‘ Hansel und Gretel ’ ( Humperdinck ) , 
 ‘ Nutcracker Suite ’ ( Tschaikovsky ) , military 
 marches , ‘ Pomp Circumstance ’ ( Elgar ) . Miss 
 Grainger Kerr vocalist 

 STOURBRIDGE — eighty - fourth concert Stour- 
 bridge Concert Society took place Town Hall 
 6th ult . programme consisted Coleridge- 
 Taylor ‘ Death Minnehaha ’ Violin Concerto 
 Symphony C minor Beethoven . choruses 
 ‘ Minnehaha ’ rendered great feeling 
 expression , solos given Miss 
 Melley Mr. Sidney Stoddard . Miss Margaret 
 Holloway gave excellent performance Beethoven 
 Concerto , band rendered valuable aid 
 accompaniments , gave capital per- 
 formance Symphony . Mr. George Halford 
 conducted - known ability 

 TUNBRIDGE WELLS.—The Vocal Association , 
 thirty - annual oratcrio concert March 25 , gave , 
 Great Hall , good rendering Mendelssohn 
 ‘ St. Paul . ’ chorus sang precision great 
 expression ; orchestra , led Mr. W. A. Easton , 
 played delicacy refinement . soloists 
 Miss Maggie Purvis , Miss Margaret Thomas , Mr. Henry 
 Turnpenny , Mr. Daniel Price . Mr. W. W. Starmer 
 conducted 

 Tune . '—You join sight - singing class 
 mental effects tonic sol - fa syllables 
 inculcated . near town music 
 cultivated , ought experience difficulty 
 getting help suggest 

 Music TEACHER.—There certain vagueness 
 inquiry : ‘ photos , life - size—/. , 
 taken bust Beethoven ? ’ Photos 
 taken thieves , busts . 
 best 

 M. C. K.—The equipment good singer consists 
 following : voice , poetic feeling , good health , 
 simple living , brains . Appearances count some- 
 thing , tenor grow beard , 
 find worth dispense hirsute ornament . 
 C. A. G.—We think Scholarships 
 combining tuition maintenance Royal 
 Academy Music , Secretary Institution 
 particulars application . 
 J : R.—Bach Chorales played slowly , 
 course , pauses pause signs . bea 
 study monotony play ‘ Variations ’ 
 stops — i.e. , organ stops , pause stops . 
 M. D. — ' premier tenors — ? ’ 
 ask . dare , knew ; 
 , , said : Premier tenors premium . 
 Soro.—The English official standard pitch 
 War Office military bands . 
 stands A= — 451 ° 9 

 Book 34 

 Harvest Song ( B flat major ) BS John E. West 
 Tempo di Minuetto ( E flat major ) . ese Beethoven 
 Andante Semplice ( G major ) _ ... Roland Rogers 
 March ( C major ) Schumann 

 Andante con doleezza ( major ) . . Franco Leoni 
 ‘ * , awhile ’’ ( ‘ ‘ St. Matthew " ’ Passion ) ( C minor ) .. Bach 
 Dona nobis Pacem ( G major ) H. Elliot Button 

 s.d 

 BACH . — “ Passion ’’ ( St. Matthew ) . ks ad Pema et 
 BEETHOVEN . — “ Mount Olives ” ’ ... ( Paper boards , 1s . ) 0 6 
 ( Tonic Sol - fa ) o 6 

 BENNETT . — “ Queen ” ’ ( Paper boards , 1s . 6a . ) 1 o 
 ELGAR . — “ King Olaf ” ’ ... ( Tonic Sol - fa ) 1 6 
 — — ‘ Dream Gerontius " ’ ( Tonic Sol - fa ) 1 6 
 GLUCK . — ‘ Orpheus ” ’ ... pan od ‘ ( Tonic Sol - fa ) 1 o 
 GRAUN , C. H. — ‘ ‘ Passion ” ’ ( ‘ ‘ Der Tod Jesu ” ’ ) ... 
 HANDEL . — “ Messiah ” ’ ... ( Paper boards , 1s.2d . ) 0 8 
 ‘ * Solomon ” ’ ... ae pon ie eas mm 2 

 ° 
 NSS 

 MUSICAL TIMES.—May 1 , 1903 . 345 
 yy 7 
 ANTHEMS ASCENSIONTIDE . 
 * praise majesty Mendelssohn ni * Let celestial concerts unite . Handel 14d . 
 * Achieved glorious work .. Haydn td . | * Lift heads ‘ - Handel 14d . 
 * Achieved glorious work ‘ Chorus ) Haydn 14d . ) * Lift heads S. 5 . Coleridge- Taylor 3d . 
 * glory Lamb Spohr < * Lift heads iG Bi J. L. Hopkins 14d . 
 Awake glory ae M. Wise 3d . | O , ye people , clap hands H. Purcell 3d . 
 * Christ obedient unto death | oh F. Bridge 14d.| * O ‘ clap hands .. J.Stainer 6d . 
 Christ entered 7 Places . Eaton Faning 14d.| Oclap hands ‘ es T. T. Trimnell 3d . 
 Bi .. Oliver King 14d.| * O God , King Glory « . H.Smart 4d . 
 * God gone . Croft 4d . | * O God , Thou ea enguate Mozart 3d . 
 * God , King ... Bach 14d.| * O howamiable ... J. Barnby 3d . 
 Grant , beseech Thee . ; H. Lahee rad * O Lord Governour ... H. Gadsby 3d . 
 * Hallelujah unto God Almighty Son Beethoven 3d . O Lord Governour ... Marcello 14d . 
 * excellent Thy , O Lord . Handel 14d/ * O risen Lord ns J. Barnby rad . 
 ye risen : J. Naylor 3d . | * Open gates Ba . _ F.Adlam 4d . 
 ye risen ( Parts ) ; Myles B. Foster 3d . | * Rejoice Lord 2 -¥. Baptiste Calkin 3d . 
 Father house , ... ) » Maude Crament 3d . | earth Lord . . T. T. Trimnell 4d . 
 day “ s = George Elvey 8d . | * Lord exalted .. J. E. West 14d . 
 * shall come pass B. Tours 14d.| Lord King H. Gadsby 6d . 
 * King glorious ... J. Barnby 6d . Lord King .. H.J. King 4d . 
 * Leave , forsake J. Stainer 14d.| * Unfold , ye portals ... Ch . Gounod 6d . 
 Let heart troubled ( Double Chorus , unaccompanied ) | Thou reignest . Schubert 3d . 
 Myles B. Foster 3d . | weak helpless Rayner 2d . 
 * Ditto ( - arrangement , wath hap yrs Myles B. Foster 3d . 
 * Anthems marked ( * ) Tonic Sol - fa , 1d . 2d . . 
 y 
 ANTHEMS WHITSUNTIDE . 
 people saw ... .. J. Stainer . 6d . shall come pass G. Garrett 6d . 
 * suddenly came Hs x Henry J. Wood 3d . * shall come pass B. Tours 14d . 
 Pentecost . .. Charles W. Smith 3d . Let God arise Greene 6d . 
 * pants hart ... os Spohr 14d . Let Godarise _ .. T. T. Trimnell 4d . 
 * hart pants .. es " “ \enhianalan 14d . | * Let heart troubled — H.G. Trembath 3d . 
 Behold , send promise ... J. Varley Roberts 4d . | * O clap hands , . Stainer 6d . 
 * Come , Holy Ghost .. T. Attwood 14d . * Ogivethanks .... “ George Elvey 3d . 
 Come , Holy Ghost J. L. Hatton 4d . * O Holy Ghost , minds ... G. A. Macfarren 14d . 
 Come , Holy Ghost George Elvey 4d .   * Oh ! closer walk God Myles B. Foster 14d . 
 Come , Holy Ghost * C. Lee Williams 2d . * O taste . Goss 3d . 
 « Eye hath seen ( - setting ) Myles B. Foster 3d . O taste ‘ A. H. Mann 3d . 
 * Eye hath seen ( - — Myles B. Foster 3d . * Otasteandsee .. “ s Sullivan 14d . 
 thanks unto God ... ea ie Spohr 4d . O Thou , true Light Mendelssohn 2d . 
 * God came Teman .. . C. Steggall 4d . | O shall wisdom found .Boyce 6d . 
 * God Spirit W.S. Bennett 6d . * blest Redeemer ‘ Rev. E. V. Hall 3d . 
 * Great Lord .. W.Hayes 4d .   * Praised Lord daily .. J.B. Calkin 14d . 
 Grieve Holy Spirit J. Stainer 3d . Sing Lord . Smart ts . 
 Happy man E. Prout 8d .   * Spirit mercy , truth , love B. Luard Selby 14d . 
 dwelleth secret place .. ‘ Josiah Booth 4d . eyes wait Thee saa Gibbons 4d . 
 + Holy Spirit , come , O come ( Ad Spiritum Sanctum ) G.C. Martin 14d . * pe se God Israel T. Adams 3d . 
 Spirit Blow 6d . Lord descended _ ... Hayes 14d . 
 + leave comfortless . . “ Bruce Steane 2d . Lord Holy Temple J. Stainer 4d . 
 * pray Father : Rev. G. W. Torrance 14d . Lord isin Holy Temple ' E. H. Thorne 14d . 
 * 1 away ... Thomas Adams 14d . love God shed $ S. Reay 3d . 
 ye love .. C.S. Heap 14d . condemnation “ HLS . Irons 34d . 
 * ye love W.H. Monk 14d . * wilderness ae .. Goss 6d . 
 * ye love oe . Tallis 14d . wilderness ... = 9 Wesley 8d . 
 + ye love R. P. Stewart 14d . * God old came Heaven Rev. E. V. Hall 3d . 
 ye love Herbert W. Ware ing 3d . rejoice ia « « ~ C¥o sh . 
 ye love Bruce Steane 2d . | Whosoever drinketh j. T. Field 14d . 
 Father house ee Maude Crament 3d . 
 * nthems marked ( * ) Tonic Sol - fa , 1d . 2d . . 
 * Almighty everlasting God ... .. Gibbon a. * Jewry God known .. fen Cate Whitfeld 144 , 
 Ascribe unto Lord ... S. S. Wesley sweet consent . - Thorne 3d , 
 Behold , God great E. W. Naylor = | fear Lord . “ cae vaiey Roberts 34 . 
 Beloved , God loved .. .. J.Barnby 14d.| Let peace God ‘ . J.Stainer 4d , 
 Beloved , let love ... Gerard F.Cobb 14d.| Light world es ae E. Elgar 34 , 
 ye mind aes .. Arthur E. Godfrey = | * Lord ofall power és William Mason 14d . 
 * Blessed man ms John Goss Ditto ( men voices ) .. es J. Barnby 44 , 
 Blessing glory Boyce a. |*Lord , pray Thee | J. Varley Roberts 14d . 
 Blessing , glory Pe Bach 6d . | O Father blest .. J. Barnby 3d . 
 * God came Teman ... ey Ras : Steggall 4d . | O joyful Light B. Tours 4d . 
 * God loved world ... Matthew Kingston 14d.|*O Lord , trust ... King Hall sq . 
 Grant , O Lord ... ans Mozart “ | * O taste ree J.Goss 3d . 
 * Hail , gladdening Light ... ae T. Field * O taste » . A.Sullivan 14d . 
 Hail , gladdening Light . “ G : C. Martin | O taste .. A.H. Mann 3d . 
 tears soweth ( s.s . A. ) Key A. . F. Hiller 44 O shall wisdom found ? “ Boyce 6d . 
 * tears soweth ( s.s . A. ) ares flat ... F. Hiller 14d . ) Ponder words , O Lord ... Arnold D. Culle y 14d . 
 * Holy , holy , holy . ‘ Crotch 3d . | * Praise awful .. - Spohr 2d , 
 * goodly Thy tents . F. Ouseley 14d . } Rejoicein Lord ; G. C. Martin 6d . 
 * lovely Thy dw veameamh .Spohr 14d.| * love hath Father . Mendelssohn 4d . 
 Iam Alpha Omega ... - Ch ‘ Gounod 4d . | Sing Lord . ss ‘ oe Mendelssohn gq . 
 * Alpha Omega ... J. Stainer 14d . * Stand bless aa - J. Goss 4d . 
 Alpha Omega .. J. Vv Varley Roberts 3d . | Teach Thy way ” ) W. H. Gladstone 14d . 
 beheld , lo ! ... ... Blow 6d . | Lord hat d mindful . . S.S. Wesley 3d . 
 know Lord great . F. Ouseley ii . * Lord Shepherd G. A. Macfarren jid . 
 saw Lord ne J. Stainer |*The Lord Shepherd J. Shaw 3d . 
 magnify J. Shaw - | Lord comfort Zion | H. Hiles 6d . 
 sing Thy pow er ss Greene 4d . | Thou shalt shew ron life .. Alan Gray 14d . 
 * sing Thy power . A. Sullivan 14d . ) heard ears . .. H. Aldrich ys , 
 sing unto Lord .. H. Wareing 3d . | Whatsoever born God . H. Oakeley 3d . 
 “ humble faith G. Garrett 1d . | Whocan comprehend Thee Mozart 3d 

 Anthens marked ( * ) Tonic Sol - fa , 1d . 2d . 

