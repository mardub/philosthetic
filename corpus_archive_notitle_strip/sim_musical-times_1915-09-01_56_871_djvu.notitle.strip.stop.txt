BROADWOOD 
 PIANOS 

 BOSWORTH EDITION 
 GRADED BEETHOVEN 

 DAILY TELEGRAPH 

 Messrs. 
 boon 

 price moderate ( 5s . ) , 
 admirable picture Beethoven 

 CELEBRATED 

 OUTLOOK RUSSIA . 
 Mrs. Rosa NEWMARCH 

 Having recently returned stay 
 months Russia , confidently affirm 
 fr War visibly affected 
 established musical organizations . opera 
 gason schemes leading concert 
 societies carried winter 
 spring years , came away , 
 spite foreshadowing events 
 doubtless cast temporary gloom 
 country , plans autumn season 
 aready discussion . observed 
 aquiet determination things going usual , 
 music , matters , panic - stricken 
 policies apparent Petrograd Moscow . 
 vas , course , early definite information 
 repertory opera houses , 
 schemes Russian Musical Society 
 Russian Symphony Concerts , told 
 interesting . 
 Possibly classics Bach Beethoven 
 find place programmes , 
 believe Russian public tolerate 
 recent way German music . 
 shops public places , corridors 
 hotels , conspicuous printed notices 
 forbidding use German language , 
 speaking streets tramcars 
 vould run risk set 
 cowd . Russians brutal warfare , 
 wounded numerous 
 mevidence , serve perpetual 
 minders brothers sisters 
 tolerate enemy midst 
 msult suffered defence 
 tome country 

 striking profoundly touching 
 music heard Russia present 
 moment music soldiers . hardly 
 wet heard military band visit , 
 carcely hour went regiment 
 ‘ mpany passing window singing 
 ingely sad solemn songs , 
 redolent ecclesiastical tradition 
 fipected procession monks 
 . . parade warriors . Generally speaking 

 24 , 18 

 choice specimens : trumped Nietzsche declaration th Overbeck : 
 - , , , , , autumn 1888 : 
 ‘ comical : ‘ 
 Wagner persuade Germans ‘ psychologically - important passages old frienc 
 Theatre seriously . remain [ “ R. Wagner Bayreuth ” ] | 
 cold contented — waxes hot , _ ; scruple changes h 
 salvation depended . ’ ‘ place , “ Zarathustra , ” 
 special form Wagner vanity text gives word “ Wagner ” . ’ paroxysms 
 set comparison Schiller , ingenious method transmuting J bardlyhelj 
 Goethe , Beethoven , Luther , ’ & c. ‘ | oy ‘ retinting ’ awkward documents , rapturous bas supe 
 arouses suspicion praise [ tg welcomed apt pupils cm fj Uexpecte 
 blame [ , auto- muniqués given lie dite ship towar 
 criticism ] . lacks alike charm , grace , Admiralty French authorities . gle 
 pure beauty , reflex soul Nietzsche curious views regardg BF 44 0 

 perfect balance ; seeks discredit friendship , things . fs da 

 fared better , inasmuch operas Puccini , 
 Mascagni , Leoncavallo vogue 

 Teutonic music 
 TEUTONIC period performed 
 MUSIC country present 
 BOYCOTTED ? juncture ? question 
 recently debated hotly , 
 especially reference cosmopolitan 
 programmes announced performance Queen 
 Hall Promenade Concerts . Sir Arthur B. Markham , 
 M.P. , speaking House Commons , 
 strong attack inclusion German music 
 scheme , wrongly attributed supposed 
 influence Sir Edgar Speyer , 
 Queen Hall newly organized 
 orchestra . Mr. Hubert Bath , following 
 statement views printed page 495 
 issue , vehemently supported 
 attempt boycott . Sir Henry Wood 
 placidly produces programmes , , 
 exclude music living Teutonic composers , draw 
 freely fount classics , including Brahms 
 Wagner . think compromise perfectly 
 rational . music composers 
 mother milk , banish 
 memory , 
 mental equipment . Bach , Haydn , Mozart , Beethoven , 
 Mendelssohn 2oth century 
 mad German megalomania . England 
 Prussia angels Beethoven 
 time 

 Mile . Maria Cecilia Natalie Janotha , pianist , 
 suddenly deported country Sunday 

 538 MUSICAL TIMES.—SEPTEMBER 1 , I9QIS5 

 important exception seen realised | Mapleson pens ) composing operas — jo , 
 folio volumes Palestrina thirty - in| music — big works jj 
 number , Vittoria , Orlando di Lasso , when| pride Europe Russian ¢ 
 complete , . octavo issues no| French . days ambitious English wo ; 
 figures . doubtless — — — — | worse . Marshall - Hall 
 country direction ‘ corpus ’ | . . : wes ie 
 English music polyphonic period - Germany |mothing auinous . , atte industry ve 
 : , , ~:~ | phenomenal . tire y ; 
 able relation Roman , Flemish | range gifts . linguist ab 
 Spanish schools ? Putting aside publications | bd . diggs -s Bel dist @ DOIN orate 
 Musical Antiquarian Society " forties , | SE os social disturbances , 
 hardly form indicated , Purcell Society , | © VETY reformer tongue unloosed , Marshal . 
 belong later period music , | Hall , outdone , armed mm 
 ecclesiastical domain volume formidable wooden chair snatched bs 
 point . refer Ouseley folio edition works | kitchen , stalked Hyde Park , stood \ 
 Orlando Gibbons printed Boyce . , view | portable platform , charmed utterance de 
 exception , allowed position claimed | largest crowds day . thes 
 . published Novello & Co. forty| , — Art ! Abee 
 years ago , print , easily obtainable . " | period startled Sir George Grove propos 
 refer efforts ! series lectures music Royal Colle 
 provide purely performing editions choral purposes | doubt hinted , suggested s 
 ecclesiastical music period question . | } , 5 Shot asap ‘ 6 Sey 88 ° 
 . “ ee . ig ote - | Board Professors hear . 
 , ‘ Cathedral Series , ’ thee = ye 
 associated , appreciative reference Hallé got authorities appor 
 July number / usical Times . difierent | Marshall - Hall Ormonde Professor Muse 
 lines . point secular domain Melbourne University , salary £ 1,000 yw 
 enterprising publication definite plan Madrigalian rheir gain loss . work 
 music Rev. E. H. Fellowes . wonderfully fruitful — witness founding & 
 popular thing present time } Conservatorium Antipodean capital . | red 
 good enemies , , whilst continuing to| thrill pride Michael Balling lox 
 emancipate undue Teutonic influence ; ago raised , sudden reference ‘ splendé 
 cramped times past , keeping mind musician- Marshall - Hall , ’ labours ki 
 seamy , shown Dr. Terry article , let } shared time Australia . 
 learn German enterprise little Marshall - Hall music 9 & 
 organizing power , setting foot scheme Send te ean int Nt ? cote 
 ‘ corpus ’ old music , commencing — oie Life — _ — s — . . seen W ; sat 
 complete works Tye , Tallis , Whyte , Byrd , Gibbons | ‘ 50ng - cycle Life Love ’ ( Joseph Williams\s 
 ( supplement Ouseley effort ) , help redeem | 4 wonderful early effort . sounds fresh - days 
 national reproaches . Messrs. | quarter century ago . Symphony : 
 Novello & Co. initiate scheme , aid | E flat ( played Queen s Hall , August 20 , 1907)is2 
 experts known exist ? Breitkopf s Library edition . 
 _ — Marshall - Hall latest works Siry 
 G. W. L. MARSHALL - HALL , quartets Quartet horn strings , som 
 COMPOSER POET : APPRECIATION , | 2nd - act opera , ‘ Stella . ’ impos 
 EDMONDSTOUNE DUNCAN ‘ Romeo Juliet , ’ probably greats 
 gneiss qos ee _ | work . Shakespeare text . 
 tall , lanky man striking personality — big , | opera published Enoch compe 
 - poised head , high frontal , black hair , bushy set Melbourne — journey . 
 eyebrows , firm jaw , prominent nose , courtesy son , violinist , Huber 
 kindly sensitive eye world — | Marshall - Hall , able state ey 
 harden steel occasion , scent thing played Melbourne t 
 battle philistinism — Marshall - Hall | composer lifetime . ‘ Stella , ’ appears , gv@ 
 knew student days . twice great success Majesty Theates 
 drew extraordinary } 1912 . ‘ Romeo , ’ Balcony scene bee 
 magnetism , followed grumbling . divined played . ‘ Stella , ’ remembered , ran : 
 inevitably soul world loved| week Palladium year . 
 music art sake , wholly , consumedly | rest , occasional orchest 
 single - heartedly — man . Byrd , Purcell , Bach , | performances , little composer 
 Beethoven , Wagner , themes kindled | output reached English public . @ 
 wonderful fires poetry . Marshall - Hall|a man operas , * Londoner , born Edgwat 
 essentially poet , understood musicians } Road 1862 , grandson famous physicias & 
 poets times places men| . died July 19 , 1915 , aged 53 
 know figures , newspapers , cricket , motoring . Contrary widely circulated statement , & 
 friends constant companions . musician relation eminent K.C , , thoy 
 promise early days quickly ripened } chance circumstances associated famut 
 action . completing studies | generations ago . 
 Royal College Music , writing articles } King College gone Oxford , 2 
 half - - dozen morning , evening , weekly , monthly |later Switzerland profane country , winds 
 papers magazines . stunning things | Royal College . held sot 

 — enthusiasm enlightenment early appointments Newton Abbot Welling 

 BIRMINGHAM 

 outlook coming musical season certainly 
 encouraging months ago , 
 present moment forecast liable variations owing 
 depression great War . Chamber music , 
 glad record , represented series 
 concerts organized Birmingham Chamber Concerts 
 Society , executive Catterall String Quartet , 
 introduce interesting novelties addition 
 string quartets , quintets , & c. , Beethoven , Mozart , 
 Brahms , César Franck . Messrs. Dale , & Co. 
 secured M. Ysaye M. Pachmann violin 
 pianoforte recital Town Hall , likely 
 prove special attraction . Mr. Max Mossel 
 decided hold enjoyable Drawing - room Concerts , 
 dates fixed , expects 
 hear artists high rank hitherto 

 Choral music rest Birmingham Festival 
 Choral Society , , , issued scheme 
 . given understand probably 
 arrange concerts addition customary 
 Yuletide performance ‘ Messiah , ’ 
 depend war conditions 

 CAMBRIDGE 

 committee University Musical Society , 
 meeting held July , decided promulgate scheme 
 concerts coming academical year , 
 conditional success . 
 year nearly chamber concerts 
 abandoned , year 
 impossible arrange programme normal lines . 
 view splendid support given 
 Society past critical months , com- 
 pensate loss concerts , committee 
 ventured adopt scheme , details 
 follows : — Michaelmas Term , 1915 : ( 1 . ) Schubert Octet , 
 Beethoven Septet ; ( 2 . ) Bach ‘ Christmas Oratorio . ’ 
 Lent Term , 1916 : ( 3 . ) Chamber Concert , Philharmonic 
 Quartet ; ( 4 . ) Choral Orchestral Concert , Beethoven 
 eighth Symphony , Mozart G major Violin concerto , Bach 
 Chaconne ( Albert Sammons ) , arrangements Folk - songs 
 Vaughan Williams . Easter term , 1916 : ( 5 . ) Chamber 
 Concert , London Quartet ; ( 6 . ) Choral Orchestral 
 Concert , Ballet Scene Borodin ‘ Prince Igor , ’ 
 ‘ Appalachia ’ ( Delius ) , ‘ L’Arlésienne ’ Suite ( Bizet ) , 
 Violoncello concerto 

 DEVON CORNWALL . 
 DEVON 

 e 

 set Scotch songs arranged Beethoven ( Op . 108 ) , 
 interesting feature 

 accompanied trio . 
 performance fact Mr. Thorpe played 
 actual violoncello performance 
 songs . Mr. Thorpe played solo Popper 
 ‘ Polonaise de Concert , ’ Op . 14 , Mlle . Cosyn 
 Mr. Jobn Collett sang songs , accompanied Miss Downes 

