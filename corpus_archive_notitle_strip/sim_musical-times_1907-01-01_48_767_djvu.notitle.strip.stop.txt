LENT TERM begins Thursday , January 10 . Entr : - | SATURDAY . FEBRUARY 
 tion , Monday , January 7 , 2 . , . 4 
 ~ Introduction Act III 

 Prospectus , Entrance Forms , information | ot , 
 ‘ ws vance Apprentice 
 F. W. RENAUT , Secretary . Procession Masters | 
 ROYAL COLLEGE MUSIC , maphony Io . 9 , J Beethoven . 
 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. Ne 

 Incorporated Royal Charter , 1 | : ernst von Dohninyi . 
 lelegrams — “ * Initiative , London . Pelephone — * * 1160 , Weste 4 

 tw . notes end sections 
 ne fifth ninth bars . 1 , 
 pa ives 

 th regard figure violas 
 ac ) ompany principal subject opening , 
 anecdote told Ferdinand Hiller 
 au'hority . Liszt arranged Beethoven symphonies 
 pianoforte preface 
 arrangement maintains orchestral effect 
 Cc : produced opr pianoforte . 
 Mendelssohn read turned Mozart G minor 
 Symphony said : ‘ Let hear 
 bars , viola figure rendered sounds 
 band , believe . ’ 
 second movement , Avdante , 
 way worthy successor A//egro molto . 
 E flat major , 6 - 8 time , starts follows 

 interesting 

 rattling passage closes section ot 
 Allegro 

 working - /7a / e Mozart , 
 movement , brings science play : 
 , , unscientific hearer little 
 suspects modulation contrivance 
 animates deeply poetical passages form 
 rapid fabric wonderful section , 
 science kept clear art part- 
 section begins B flat major 
 goes keys D , G , C , F minor , E fiat , 
 C minor , F sharp , & c. , ends original 
 G minor resumption theme . 
 interesting variation ordinary practice 
 repeating second theme , major 
 minor key ; equally interesting change 
 concluding passages , 
 developed form coda — short , true , 
 sufficient Mozart knew necessity 
 portion , constructed magnificently 
 weeks ‘ Jupiter ’ Symphony , 
 left Beethoven 
 significance importance . 2 , dozen 
 years later 

 regret record death , Florence , December 6 , 
 ConTE GIOVANNI GiGLiuccl , elder son Countess 
 Gigliucci ( née Clara Novello ) , sympathy 
 felt English friends 

 
 Weinga 

 Herr V 
 fame , wh 
 recognized 
 Beethoven 
 musicians 
 ninth Syr 
 art - work 
 ustified . 
 absolut 
 post - symph 
 complemen 
 elegant ’ ; | 
 fundam 
 schedule 
 States , equa 

 Ber 
 French co : 
 ‘ 
 hand , Liszt 
 music . 
 powerful ‘ * * 
 later works 
 word music . 
 briefest ] 
 acquain 
 translation 

 W. Reeves . | escape notice . Andante based 

 Herr Weingartner conductor won - deserved | expressive theme . Sv / erza lively , bright /7va / e 
 fame , merits composer duly major key , preceded minor / evo , based 
 recognized . Ife comes critic post | principal subject opening movement , brings work 
 Beethoven symphonists ; tells — | effective close . 
 musicians agree — Wagner opinion 

 Th ym phony ( ler stn Beethoven . Felix 

 ninth Symphony ‘ direct transition ’ KOOKS RECEIVED 

 B 
 ( Barcelona : Tipografia ‘ L’Avene 

 complement * Beethoven ; Mendelssohn ‘ clever | 
 elegant ’ ? ; Schumann , trying classic , damaged 
 fundamental nature ; author runs 

 schedule symphony writers , , rightly | Story Organ Music . C. F. Abdy Williams 

 States , equalled , far surpassed , Beethoven . | Pp . xiv . 295 ; 35 . 6d . net . ( Walter Scott Publishing 
 Berlioz , , came new departure . } Co. , Ltd 

 French composer worked ‘ poetic basis ’ lines , | Zhe Story Organ . C. F. Abdy Williams 

 surpassed ec 

 best ol ur modern music . December 1 
 selection consisted Theme , Variations Polonaise 
 Tchaikovsky Suite . 3 G , Beethoven eighth 

 Svmphony , Mozart Clavier concerto E flat Cesar 

 Clifton Quintet held second concert 

 season December 6 , Victoria Rooms . 
 executants Mr. Ilerbert Parsons ( pianoforte ) , Messrs 
 Maurice Alexander Hubert Hunt ( violins ) , Ernes 
 Lane ( viola ) , Percy Lewis ( violoncello ) . wer 
 admirable performances Beethoven ‘ Ilarp ’ quartet 

 E flat ( Op . 74 ) ; Brahms Sonata D minor ( Op . 105 ) 
 pianoforte violin , Saint - Saéns Pianoforte trio 

 ncluded Schumann Overture , Scherzo Finale , 
 Liszt symphonic poem ‘ Hungaria . ’ , wit 
 Elgar ‘ Cockaigne ’ overture , finely played 

 Mr. Kreisler b Violin concerto Beethoven 

 super 

 distinction engaged . glad 
 concerts ing supp yrted 

 second University concerts ( December 12 ) 
 devoted t Beethoven Pianoforte tri , | layed | 
 Messrs. Denhof ( pianoforte ) , Ver ! ( violin ) 

 Messeas ( vio Ilo ) . concerts local artists 
 outstar interest vocal recitals 
 Mr. Alfred C. ing , excellent baritone , November 21 ; 
 Miss Jean Waterson , promising young soprano , 
 December ! violin recital Miss Copeland ( 
 excellent appearance ) , November 20 ; 
 pianoforte recital Miss Muriel Kerr Brown , showed 
 pronounced advance art , November 22 . 

 ures Choral Orchestral 

 concert November 27 performance 
 Liszt symphonic poem ‘ Hungaria , ” Herr Fritz 
 Kreisler dignified rendering ol Beethoven Violin 

 mcerto . December 4 , Mr. Henri Verbrugghen , 
 accomplished leader Scottish Orchestra , occupied 
 conductor desk conspicuous success . 

 \ Miss H R S 1 gelis . 
 Ir ‘ M concel Ss 
 : " : l ri ( ra e incidental music 
 r 2 5 nde , ’ given firs 
 , wer wy Misses Gl é harles Tree vocalis 
 W 1 Ger l Messrs. Al t 1 l 
 Fowler Bur . ‘ rte ] < 
 Ir | ‘ — 
 IUSIC MANCHESTER . 
 MUSIC GLOUCESTER DISTRICT . R k ; N1 
 ( N ) H Dr. Kicht 
 , 1 N Scand 
 ‘ r » \ pr s Tes 
 ’ ' ld hor progra 
 W rfor Dre Geror s ’ . 
 } . wil Dvor verture 
 ) ! r 2 Shire H t r l ’ stified | , : xs ang ‘ 
 Dr. C 1 lin concer ur uving solo 
 fT ! ver Ss ( sire ne . ‘ ‘ 
 la é W er 2 Berlioz ‘ Faus 
 | 
 reache “ ty - second pertor e ese concerts 
 > 7 - 
 . . soloists Miss H | » Mr. John Harrison , 
 L / T ver 5 r 7 
 , Mr. Charles Clark , Mr. wler Burto \ 
 r 5 r nse ¢ y/. : rea ‘ } 
 e ving concert ( Vecember ) e \ ariations 
 “ Ss r saster ’ " : 
 ; ° fu n merry , ( rg > mann , wer 
 r lw rar ce . “ ‘ RB 
 NM ; 
 Phi por 
 ara 
 | ver Mr. ¢ klw Miss 
 : Mr. W. Hi 
 rs ! rs f 
 t ral S rt work wi 
 r. | \ Lier ‘ 
 Mu “ r ~ araw le r 
 Mr. ’ y. Brewer 1 t 
 Cir ( ' f wh Mr. A. H 
 ! perforn 
 M Par ( D Wi ind 
 M Dor W 
 | 3 . & Mr. | E. Miles 
 Mt \\ Sartets ea ae ; ) 
 7 F = Ni lls displayed excellent taste ) rendering groups 
 ‘ . ngs German , French , English . Englis 
 ! aris ; , 
 } 7 . includ ree litt ywer - song vig s Mr. Ha 
 r r t } } - 
 Harty , admirably ympanied throughou e 
 Il ) Str r ag - . ‘ 
 Mr. Isidor Cohn played witt acc 1s 

 M = wi Se 1s ‘ * Novelletten , ’ Beethoven 
 ly ‘ erir 

 c ee xe Appassi , pieces . Mr. Brand Lane thir 
 ; uy “ y - age Aes “ mm . | subscrif r 24 , Dudley Buck eig 
 [ \ t W \ ( orgar f ae 

 5 lyrical vein taste feel amateur 

 r 4 , ' tu f Beethoven Society concert 

 D er II played Gounod ‘ Mirella ’ overture 

 rendering 

 Beethoven C minor Symphony , 

 /iva / e taken s VY ; f 
 + ; f+ = } ‘ salls ; ne * ‘ sit 

 JANUARY , 1907 

 professor f music , Sir Hubert Parry , gave 
 nteresting lecture ‘ function thematic material 
 musical organization ” appreciative audience , 
 illustrations rendered Miss W. Evans , Miss 
 Colton Mr. Bolton . ve Vice - chancellor present 
 lecture , took place Sheldonian Theatre 
 November 28 
 following afternoon Dr. Joachim Berlin 
 Quartet gave chamber concert Town Hall 
 nnection Musical Clul programme 
 consisted ret juartets — Mozart D minor 
 ( Kk . . 421 ) , Brahms B flat ( Op . 67 ) , Beethoven 

 n C minor ( Op . 1 } ) . course , nearly 
 y played , concert 
 mstrated quartets su 
 constitute strain audience 

 Dvorak 
 od tHlect . Al 
 performance , 
 Clouds ’ 
 Ww hic h deserves 

 quartets Ilaydn Beethoven ( Op . 135 
 - called ‘ Dumky ’ Pianoforte trio , ( 
 event common interest 
 students Leeds University , ‘ TI 

 Aristophanes 

 Decer 

 rin Memorial Hall befor large audience . 
 ' er n Mr. Robert Jones , gave exc t 
 render Ilear prayer ’ ( Mendelssohn ) ar 
 t St. George ’ ( Elgar ) Ihe soloists Miss 
 \nr ] Mr. Charles Keywood . pianofor 
 ! accompaniment ble hands 
 y Shepperd Mr. A. F. Varker respectively . 
 Musical Soci g concert 
 Nov ber 27 Corn Exchange . 
 progr m « luded Brahms ‘ Song destiny , ’ Beethoven 
 rtur Coriol 5 Bach Suite D flute 
 ' ra Pia rte iolin solos contributed 
 “ ccess Herr } s Herr Sebald 
 t ( ( vocalist ng Ma e Lhombino . 
 1 chorus , numbering 25 
 e ol elr ul 
 Hare r c tne rior 
 rhe concert given season 
 St. ( ilia Voca n took place Dece er 10 
 | Hall , wi Handel ‘ Israel Egypt ’ 
 ] } rus hroughout given , 
 ‘ rchestr lected sual Hallé Orchestra 
 r wi t } choir , numbered 250 performers . 
 principal vocalists Miss Maggie Jaques , Miss Parr , 
 Miss Gertrude Lonsdale , Mr. Herbert Parker , Mr. Bridg 
 Peters Mr. Webster Millar . Dr. E. C. Bairstow 

 BROMLEY ( KEN?T).—The Choral Society opened season 
 Drill Hall December 6 performance 
 Barnett ‘ Ancient mariner , rendered 
 choir orchestra direction Mr. F. Fertel , 
 solo parts sung Miss Nellie Dunford , Miss 
 Esther Franklin , Mr. F. H. Blamey Mr. Graham 
 Smart . miscellaneous second included Eaton 
 Faning - song ‘ Liberty 

 ae = y 
 ORTI » FANTASIA axnp FUGUE 1x C munor C. P. E. BACH PEC 
 Ar ¢ J xn FE . Wes ‘ si . 

 ranged 
 WITTEN . PRELUDI IL . ( ‘ Tue LES ” ) 
 aes COENEN EDWARD ELGAR 
 ‘ Arranged G. R. S : x 
 
 tr , FINAL : SYMPHONY N BEETHOVEN 
 N ( v. Limite Arranged A. B. P es 
 12 , ADORAMUS TI HUGH BLAII 
 t \rra yl ) 
 , anil . : INTERMEZZO ( “ Tue \ ANES ) PI . 
 SYMPHONY FE : li C. H. H. PARRY 
 > 14 . BRIDAL MARCH FINALE ( “ Tue Bret 
 , \ Es ) Cc . H. H. PARRY 
 ; r oO oe \ g W.G. 
 ORCHESTRA ANDANTE ( 1 s c , O 
 ' \ E , W | . BRAHMS 
 FREDERIC H. COWEN . DANTE ( P ; N ( O 
 \ | k. W . BRAHMS 
 SCOR ] IODERATO CANZONA ( 1 . 
 t N Vi . ) \ ] P ‘ 
 | URCI 
 P ( nued . ) 
 N C , 1 

 MUSICAL TIMES sue cenccnrectiannaratsoeatncsat 
 ; Sa aa = ADORAMUS 4 
 » 2 » ag Na , 

