ly have be surprised that Madlle . ‘ Tietjens be 

 overture to Leonora , perform with wonderful spirit 
 and precision by the orchestra , and Professor 
 Bennett ’s beautiful Pianoforte Concerto in F minor , 
 execute by Madame Arabella Goddard in her usual 
 finished manner , be the special feature in this 
 miscellaneous act ; the rest of the programme be 
 compose of well wear material , the redeem 
 point be Mr. Sims Reeves ’ interpretation of 
 Beethoven 's Adelaide , with Madame Arabella God- 
 dard ’s pianoforte accompaniment 

 the performance of the Messiah on the third 
 morning of the Festival , draw an enormous audience 
 to the Hall ; there be , in fact , not even stand 
 room in any part of the building . Mr. Sims Reeves 
 be in excellent voice , and sing ' ' comfort ye " and 
 " every valley " with his usual truthful expression 
 and feeling ; but in the trying song ' thou shalt 
 break they , " there be evident trace of an 
 indisposition which he could not shake off . the 
 other principal vocalist be Madame Lemmens- 
 Sherrington ( who sing all the soprano solo music in 
 the first and second part ) , Madlle . Tietjens ( who 
 replace she in the third part , and be encore in 
 " t know that my redeemer liveth " ) , Mesdames 
 Sainton - Dolby Patey - Whytock , and Mr. Santley . 
 the chorus be give to perfection ; and , although 
 we can not endorse Mr. Costa ’s opinion that the 
 opening of ' for unto we a child be bear " should be 
 sing piano , in order that a theatrical burst of sound 
 shall come in upon the word ' wonderful , " the 
 general effect of this , as well as the ' hallelujah , " 
 be such as we never hear produce save by the 
 Birmingham choir 

 76 

 to the congregation . the service be intone clearly and solemnly 
 by the Rev. Mr. Williams , Rector of Beaumaris . the Rev. J. 
 Price read the first , and Archdeacon Jones the second lesson . 
 Tallis ’s Preces and Responses be use throughout . the chant 
 be all anglican , the Psalms be take to a double and single 
 chant of Dr. Monk 's ; while the Magnificat and Nunc Dimi : tis be 
 sing to a double and single chant from Mercer 's ' * Church Psalter . " 
 the anthem be Richardson 's , ' ' o how amiable be thy dwelling , " 
 which be well render . the service conclude with a Congre- 
 gational Psalm Tune , to the word ' ' duw mawr y rhyfeddodau 
 maith . " the number of chorister amount to nearly 300 . the 
 ' whole of the musical portion of the service have be organize by 
 Mr , E. W. Thomas , the Organist of St. Ann 's , who conduct the 
 festival of the day in the most effective manner . the following 
 be the choir present on this , the first festival : — Llanllechid , 
 Llanfairfechan , Aber , Llandudno , Glan Ogwen , St. Ann 's , Lian- 
 degai , Gelli , and Penmachno . a Pranororte Recital of 
 Classical Music be give at the Penrhyn Hall , by Mr. A. Lan- 
 dergan , Organist of St. James ’ Church , Upper Bangor , on Tuesday 
 evening , the 17th ult . vocalist , Miss Edith Wynne . ' the pro- 
 gramme be select from the work of Beethoven , Mozart , Men- 
 delssohn , Hummel , Dussek , Clementi , Gounod , Benedict , c. 
 Mr. Landergan also contribute an air and variation on an 
 original theme for the Pianoforte . the hall be well fill by a 
 fashionable and attentive audience , who thoroughly enjoy the 
 entire programme . the local paper state that the Concert be 
 a great success . and speak in the high term of the vocal ability 
 of Miss Edith Wynne , and of Mr. Landergan ’s pianoforte play 

 Boston , U.S.—Mr . James Pearce , Mus . Bac . , 
 Oxon . , of Philadelphia , give in July and August a series of Organ 
 Concerts , in the Music Hall , to large audience than on any of his 
 previous visit . the programme consist of the work of Bach 
 and Mendelssohn , and transcription from Handel and Beethoven 

 CARMARTHEN . — the Choral Association of the 
 Archdeaconry of Carmarthen hold its Second Festival in St. David 's 
 Church , on Wednesday , the 28th August . thirty - three choir 
 be present , number upwards of 800 voice . there be two 
 full choral service on the occasion , that in the morning be in 
 Welsh , under the direction of Mr. Owen Davies , Welsh Choir- 
 master , and that in the evening , in English , entirely in the hand 
 of Mr. Scotson Clark , who first organize the choir of this Asso- 
 ciation , and of whose great ability and untiring energy in train 
 they we can not speak too highly . the prayer be intone 
 in Welsh by the Rev. Mr. Howell , of Lianedy , the first and 
 second lesson read respectively by the Rev. Mr. James , of 
 Abergwili , and the Rev. Mr. Evans , of Llandebie , and the sermon 
 preach by the Rev. John Griffith , of Cwmavon . the musical 
 part of the service be conduct throughout by Mr. Owen Davies , 
 and accompany on the organ , in a very creditable manner , by 
 Mr David Williams , organist of the church . at 3.15 the english 
 service commence . prayer be intone by the Rey . Mr. Phil- 
 lips , of Llwynmadoc ; lesson read by the Rev. R. Lewis , of 
 Lampeter Velfrey , and the Rev. Mr. Harrison , of Laugharne , and 
 the sermon preach by the Rev. Montague Erle Welby , M.A. , 
 incumbent of All Saints , Oystermouth . the choral service , as a 
 whole , go remarkably well . Mr. Scotson Clark , as on a former 
 occasion , entirely dispense with the service of a conductor , pre- 
 side himself at the organ with his well - know ability . we may 
 particularly mention the careful rendering of the versicle and 
 canticle , and the life and spirit which be throw into the 
 hymn ; also the distinctness of the word in chant the psalm , 
 though on this latter point there be still room for improvement 

 Miiller 
 Miiller 

 Beethoven 

 J. Atterbury 

 313 thank be to God Elijah 
 317 then shall your light - . ditto 
 why , my soul 42nd Psalm 
 ww hy , my soul ( last chorus ) ditto 
 ye nation , offer Lobgesang 

 BEETHOVEN ’s " ‘ ENCEDI . 
 ( mount of OLIVES 

 195 o praise he , all ye nation 3 

 196 hallelujah ... ese wo 2 

 BEETHOVEN ’s MASS INC . 
 190 Kyrie — when I call upon thee 4 
 Gloria — praise the Lord en 
 qui tollis — give ear to my 

 22 oo ee om I co 

 11 . six Pianoforte Pieces , by Wallace 

 12 . Beethoven ’s Sonatas . edit by Charles Halle . 
 ( no . 1 . ) contain Sonatas no . 1 and 2 of 
 op . 2 complete 

 13 . twelve popular duet for Soprano and Contralto 
 Voices 

 19 . favorite air from the Messiah , arrange for 
 the Pianoforte 

 20 . Beethoven ’s Sonatas . edit by Charles Halle . 
 ( no . 2 . ) contain Sonata No . 3 of op . 2 , 
 and Sonata Op . 7 , complete 

 21 . nine Pianoforte Pieces , by Ascher and Goria 

 no 

 28 . Beethoven 's Sonatas , edit by Charles Halle , 
 ( no . 3 . ) contain the Sonatas no . 1 and 
 2 of op . 10 

 29 . ten Contralto song , by Mrs. Arkwright , Hon , 
 Mrs. Norton , & c 

 80 . Beethoven ’s Sonatas . edit by Charles Halle , 
 ( no . 4 . ) contain the Sonata No . 3 of op , 
 10 , and the Sonata Pathetique 

 31 . Beethoven 's Sonatas . edit by Charles Halle , 
 ( no . 5 . ) contain Sonatas no . 1 and 2 
 of op . 14 

 32 . Beethoven ’s Sonatas . .edite by Charles Halle 

 no.6 . ) contain Sonata Op . 22 , and Sonata 
 p. 29 , with the celebrated Funeral March 

