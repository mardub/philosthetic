hurries” they are tolerable only because they

are not mutilated Beethoven or Wagner

Dr. Tootell defends the use of standard works as 
an accompaniment to the pictures, but agrees with 
us In objecting to their mutilation

to write . it does not follow that because

a musician is an authority upon Bach, Beethoven

or the modern Russian school, he is sfso facto

invariably lose, and the loss will be heavy when the 
‘Forest Murmurs’ and ‘Siegmund’s Love Song’ are 
used in the way Dr. Tootell rightly condemns

Dr. Tootell favours the use of standard works as 
cinema music on the ground that he and _ his 
colleagues are out to get the best that can be had, 
The point may be conceded so long as the per 
formance is good, and provided that such ‘cuts’ as 
are made are discreet. But when the Doctor goes 
on to support his claim by saying that ‘had Mozcart, 
Beethoven, and Wagner been living at the present 
day they would have been among the very first to 
realise the wonderful scope and possibilities offered 
by the cinema for the development and display of 
their genius’ he uses an argument that may be turned 
against him. The great ones of the past, if alive 
to-day, would no doubt write film music, but they 
would not produce for the purpose C minor and 
‘Unfinished’ Symphonies, ‘ Forest Murmurs,’ and the 
like. We would pay a good sum to be en hand with 
a notebook while Beethoven was making a few 
appropriate remarks on finding his C minor Symphony 
being used to help out the crudities of 7% 
Blood or The Yellow just ‘released,’ in five 
reels, featuring Luce Tosh

If cinema musical directors wish to draw on the 
standard repertory, let them use their spoils as 
overtures and interludes. In a general way they 
should keep their hands off when they want music for 
accompanying purposes. It is not a question of the 
axe or the scalpel; the point is that association with 
a film is apt to spoil the music for abstract use 
which after all, was the purpose the composer had in 
view

Mosart: Doze sono

Beethoven : First theme of slow movement of B fiat

Pianoforte Trio

has

The ‘Choral’ Symphony 
responsible for more good ‘copy 
Beethoven’s Symphonies lumped together. 
always something to be said about it, and whatever 
is said is sure of a host of understanding readers, 
because the work is so familiar—not through per- 
formance, for it is rarely heard in its entirety, but by 
means of pianoforte-duet arrangements and miniature

scores. Here is Prof. Donald Tovey, with an 
uncommonly good contribution—Acethoven’s Ninth 
Symphony an Essay in Musical Analysis

it we shall grasp the secret of this strange power. 
There is nothing better in the essay than 
discussion of this question of scale and proportion

a factor more difficult to grasp in music than in 
any other art because the element concerned (time- 
duration) is exposed slowly, whereas breadth, size, 
height, and so on, by which we estimate 
proportions in sculpture, architecture, and in pictorial 
art are seen more or less at a glance. 
us, too, how it is that this movement, though not the 
longest of Beethoven’s, is yet the greatest, chiefly 
because of the scale of tone employed, which, of

course, is less a mere question of f/ and Jf than of

If there are large numbers of contemporary 
‘overs who are in heated revolt

music- against 
the wsthetics of Beethoven’s music, that is a 
nervous condition that concerns nobody but

themselves. There will always be still larger 
numbers of music-lovers who have not yet heard 
anything like as much classical music as they 
wish to hear. It is just as well that they should 
realise that there is nothing more than an 
irritated condition of nerves behind the talk that 
now goes on as to the need of a revolt against 
Beethoven

If there be anything important enough to be| of the Finale

called a revolt, it is not against Beethoven but

There is

On such points as these the essay is

good deal of poor music that would have been 
shelved long since had it not borne Beethoven’s 
ln name on the title-page. But Beethoven is not the 
victim of the present generation’s refusal to 
Yet why talk of ‘ victims’ ? 
The art will gain much and lose nothing from a 
| frank realisation that the greatest of composers 
| wrote on many days when they might have been 
better employed, and in the long run the great dead 
will be admired none the less. Discrimination is 
two-edged, and is as likely to rescue a neglected 
masterpiece as it is to dethrone a false idol

After all, musicians are only now beginning to 
apply the critical test that has long since been used 
on the works of the greatest of poets, dramatists, 
and novelists. For example, Shakespeareans have 
never made any bones about expressing their opinion 
of his weak plays, and even of the weak spots in his 
masterpieces. ‘The musical public has so far been 
less critical chiefly because it has been largely depen- 
dent upon conductors and public performers, who— 
|to some extent inevitably—have a good many extra- 
musical factors to consider when making up their 
programmes

In regard to this work of Beethoven Prof. Tovey

appears to think that some of us question the Choral 
Finale just because it 7s choral, and he is at some 
| pains to justify Beethoven’s introduction of voices in 
|a symphony. But surely the complaint is not that 
Beethoven has used a chorus here, but that he has 
abused it. Nobody objects to his bringing in voices, 
but a growing number of us object to his first giving 
them ungrateful music to sing and then adding 
| insult to injury by drowning them half the time with 
| his orchestration. Prof. Tovey admits that Beethoven 
was not a good choral writer, but he sees in the 
I) major Mass an advance on the /ivale of the 
Symphony, and infers that had the composer lived a 
few years longer there would have been ‘a fourth 
period in Beethoven’s development, which should 
| have been distinguished by a body of choral work 
fully equal in power and perfection to the Symphonies 
land String Quartets.’ This hypothesis disregards 
the obvious fact that from first to last Beethoven 
showed little realisation of the beauty of the human 
| voice, especially in its choral aspect. It was his 
ill-luck to live at a period that, so far as choral 
|music was concerned, was perhaps the worst 
jin the history of music. In the whole of the 
works of his school we look almost in vain 
|for any sign that the composers realised the 
| beauty of unaccompanied singing. There is little 
| reason to suppose that a fourth-period Beethoven, 
| deaf, and hide-bound by forty years of thinking 
|almost exclusively in terms of the orchestra, would 
| have developed on the choral side. However, in 
this Ninth Finale, we are prepared to forgive the 
worst of its miscalculations for the sake of the main 
| theme and its exquisite treatment by the strings and 
| bassoon. Is this theme foreshadowed in the first 
movement? Sir Charles Stanford, in his recently 
published /n/er/udes, says : ‘The second subject of 
| the Symphony

if it is looked at closely, will disclose the Joy theme 
Prof. Tovey will not have this. He 
| dismisses the resemblance as ‘ superficial and entirely

Two pianoforte records well above the average 
are a Io-in. d.-s. of Irene Scharrer and a 12-in. of 
Rachmaninov. Miss Scharrer plays a couple of 
Chopin pieces—the E minor Waltz and the fine 
F sharp minor Prelude ; Rachmaninov is heard in 
his own transcription of Kreisler’s Licbeslied, a 
All the 
above records are issued by H.M.V

In the Daily News of May 20, ‘K.,’ 
gramophone records, says he has received inquiries 
from correspondents anxious to know ‘ when we shall 
have some of Beethoven’s Sonatas on the discs.’ It 
has long since struck me as odd that these works 
should have been so neglected by gramophone 
companies. The only records of the kind known to 
me are of the /’rves/o from the Sonata in F, Op. to

writing of

title, we/modrame lyrique

BEETHOVEN’S MASS IN ID

The final concert of the season (May 8) gave us

was sometimes unsatisfactory the blame must be laid

first on the composer and secondly on Mr. Coates. 
It is late in the day to complain that Beethoven gives

his singers too much work and too few chances

QUEEN’s HALL SYMPHONY CONCERTS

The last of the Queen’s Hall Symphony concerts 
ittracted alarge audience. The soloist was Lamond, 
who played Beethoven with his usual finish and 
authority. But the outstanding event of the afternoon 
was the exceedingly fine playing of the Rachmaninoy 
Symphony by the orchestra, under the guidance of 
Sir Henry Wood. Discipline and enthusiasm are 
expected as a matter of course from an organization 
trained by Sir Henry. But this particular performance 
showed also a perfection of detail and of balance that 
raised it above the average-—high as that is. Such 
playing as this compares very favourably with that of 
the best foreign orchestras. It is certain that neither 
Paris nor Rome can surpass such an achievement. 
A Bach Suite arranged by Sir Henry was also 
well given, but 77// u/enspiege/ at the close of the

programme found the players apparently a trifle 
tired. B.V

Its | 
outlook is evidently more modern than that of the| 
Léner, for, whereas the latter was content to put 
forward Dohnanyi as a representative of its country, 
this quartet made its first appearance in a work by 
Bart6k, of which it gave the first London performance. | 
This is his second String Quartet, Op. 17, a work] 
that appears to be influenced by the composer’s known 
fondness for the music of the Hungarian countryside. } 
The work is individual rather than national. It might 
even be said that in this composition Barték leaves 
the nationalism of earlier compositions behind | 
him. But if he has_ changed his_ outlook, | 
it does not necessarily imply that he has changed 
his affections, and the middle movement in 
particular reminds us that he loves the peasantry. 
The first and third sections are less easy to grasp at | 
a single hearing, partly because the recurrence of 
certain themes appears to be governed by formal 
considerations which do not become evident before 
the end of the movements. But this is healthy, rich | 
music, of neo-romantic tendency, by a composer | 
who, whatever else we may think of him, certainly 
knows what he is doing. There is concentrated 
purpose in it. It has even been compared with the

posthumous Beethoven Quartets But in spite of 
thts concentration it ts free from the deliberate 
ingenuity of so mut h contemporary music, It was 
beautifully played, every point being made to stand 
out with the requisite character. At the same 
concert M. Max Karolik sang a group of songs, 
and Mr. Leonard Borwick, after playing some} 
Chopin, joined the Quartet in Brahms’s, Op. 25 
E.E

Mr. Webster Millar, whose absence from Covent | 
Garden is much regretted, gave a recital on May 2 at 
Eolian Hall. He began with a group of English 
songs, and sang with much variety of expression

lt was clear that the bulk of those who filled 
Queen’s Hall at Rachmaninov’s first recital on 
May 6 came to see the composer of a certain

Prelude rather than to hear a pianist of unusual 
excellence. We felt that Mozart’s Sonata in 
Beethoven’s in E (Op, a Chopin group, and 
Mendelssohn’s Andante and Hondo Capricciose were

of

with the Society in December, 1920, was his last

performance of the part before setting out on the 
7m —— — jy | American tour from which he was never to return. 
“ wouls ondhoniy. + batect pass o—_ = akon The presentation alluded to above took place at 
echnical virtues of Mischa Elman’s violin playing, Northampton Town Hall on May 5, when amid 
though easier and more profitable for the initiated ieeamatnedl . eiiien ail al elias te 
to observe them at first hand for themselves ee te ee eee 
: " . —r~ - >| Mayor handed to Mr. King a cheque for £220 anda 
behind and directing them all, is that rare quality of | beautiful silver casket containing a scroll on which 
, lity ] . . a 5 é s as é was 
iniversality, eloquence—call it what one may . - 
— : . 7 {were inscribed the names of the four hundred and 
which casts a spell, instant and profound, even on Sian wntenesiinens 
. . | Ss S¢ ) Ss. 
the lay listener. When Elman plays a mastorprece | This Se a a oe ee ee a 
like the Beethoven C » as he did at O , app) ‘ é ut) 
a a on oe ti. fins an, We happen to know some 
= _ 36 eat . | Mr. RS é Ss. Se Ne e 2 , 
: yn May it is 2 ve Vv ) eee: : . : 
aan On May 57—-8 as © Me very a & — thing of the rest—of his work as organist, choir- 
were pronouncing © Cocres of ineHable Gnality. The master, recitalist, adjudicator, lecturer, all-round 
human medium goes altogether into solution. And|, “",~” < : = nage 2 Am 
nef z se : salle teacher, &c.—always up to his eyes in work, yet 
this is the more gratifying in that when he first came ever ready to find time for lending a hand in ‘all 
umong us, at the age of sixteen, to play, if we ’ pry ; Bc ie 
" te ht the nieienal G : sorts of local musical doings, from taking the chair 
a Se so anne Eeernne ak a meeting to joining with his viola in a chamber 
lozen years ago and more, he was regarded with the aie atu tn thm telter oe enamel os te 
be « ° d 
reserve that sad experience has taught the wary critic beginning of this article Mr Cates cand : When 
to maintain towards ‘ prodigies.’ They are prone to people say that England has no musical life I think 
wilt like the hot-house flower. Elman has thriven. | o¢ Mr. Toms.’ Exactly ‘ena shen ee of sive 
; : : mal Mr. Toms. exactly ; ¢ yh ’ 
The other Concertos of the evening—by Bach (in E) honour where honour is due, we shall think a 
. -4 > : 
and Tchaikovsky—were read, as it seemed, with the al , in ss ; f th 
emia 1 a : 7 great deal more than we do at present of the 
same omniscience and played with the same enthusiastic general practitioners of music—the 
an” g ae H " rr ng ~oorggteng p~ loms’s, the King’s, and their fellows scattered about 
ase 1e ueen Ss a rchestra rose Oo 1e . , . 
el Sa lo ler Sir H » Wood’ the provinces. Without them there would indeed be 
— a S| a oS ey lee ciel Bie though ‘stars’ and ‘ international 
lirection, so that artistic unity was never in peril

H. F

an excellent method of helping the hearers to an under 
standing of these fine pieces. The Preludes played were 
three on the Kyrie, /esu, meine Freude, Liebster Jesu, 
Erbarm’ dich mein, Aus der Tiefe, and Wir glauben all? 
an Einen Gott

In connection with the Portsmouth branch of the 
Hampshire Association of Organists, a Festival Service was 
sung by the united choirs of the branch, at Portsea Parish 
Church, on May 17. The singers numbered five hundred, 
and were very effective in a scheme that included Wesley’s 
Blessed be the God and Father, the Chorale Slecfers, wake, 
as harmonised by Bach in the cantata, O gladsome Light, 
Beethoven’s Hallelujah, and Smart’s Te Deum in F. 
The conductors were Mr. Hugh Burry and Mr. i. 
Turner, and Mr, L. A. Lickfold was at the organ

R

extending from CC to

Theme of Beethoven, Bach’s Prelude and Fugue in D, and 
John E. West’s Song of Triumph

riNGs

THE MUSICAL TIMES—June 1 1922

PIZZETTI AND BEETHOVEN

Sir,—As I was lately the author of an attack upon 
| Pizzetti as a composer, to which you have alluded in the 
| Musical Times, am 1 not the proper person to defend him 
against aspersions made on him by others? There is an 
implied reflection on him on the part of your reviewer, 
|‘ F. B.,’ in your last month’s issue. It amounts to nothing 
more than a little slip, perhaps, but nevertheless it may 
have given a wrong impression to any of your readers who 
are interested in the views of the very active group of

In discussing the report of the recent Congress of Italian 
musicians at Turin, your reviewer says

The discussion on the reform of musical colleges 
and of education threw a curious sidelight on the 
opinions of a composer already well-known here. 
Ildebrando Pizzetti. Signor Pizzetti was, and, perhaps, 
still occupies the post of, professor of composition at 
the Bologna Conservatoire. He apparently does not 
believe that there is anything to be learnt from 
Beethoven, for Signor Giacomo Orefice told the 
Congress that, as external examiner, he found that 
the score of the Zrofca was suggested a test 
for sight-reading. He, of course, pointed out that 
the test was futile, as the Zrosca must be known to 
all students of composition. He was assured, however, 
that Signor Pizzetti’s students knew neither the score

as

music 
(as

to be learned from Beethoven,’ for in the preface of his 
Vusicisti Contemporanet we have abundant record of his 
admiration for that composer. He describes (in terms

perhaps, warmer than an Englishman could bring himself 
to use of anyone or anything) his reverent feelings and those 
of d’Annunzio, as musician and poet went threugh some of 
Beethoven's works together

Incidentally, it incorrect to * Pizzetti 
perhaps, still occupies, the post of professor of composition 
at the Bologna Conservatoire’ (though from the puzzling 
wording of the report your reviewer was justified in his 
mistake). The incident occurred at the Royal Musical 
Institution of Florence, of which Pizzetti is director, and

GILLINGHAM.—The chief 
the String ‘Juartet, on May 4

Quintet (with Mr. William Petchey) and a Beethoven 
| String (Juartet

G.iascow.—A choral society has been formed by the

MAIDSTONE.—The twelfth annual concert of the 
Maidstone Orchestral Society took place on May 10, 
nder the direction of Mr. Frederick Cole. The 
programme included the Vex lVorld Symphony

Beethoven’s Prometheus Overture, Percy Grainger’s lock | 
V/orris, and the second Peer Gynt Suite. Mr. Norman | 
Williams sang, and Mr. Albert Fransella played a Suite 
for flute and orchestra by Godard

Ni ASTLE.—-The Glee and Madrigal Society, conducted 
by Mr. RK. W. Clark, on May 4 sang music relating to spring— 
by Spoltforth, Beale, and Muller—pieces from Mendelssohn's

GERMANY 
MAX REGER

This is a time of progress and development. While some 
of the younger composers are zealously busy to cultivate 
the field of music with more or less success, Max Reger has 
since his death become a pillar in the land. He is looked 
upon not as a revolutionist, but as a link in the line Bach, 
Beethoven, Brahms. Since Lindner published his life of 
Reger (Stuttgart, I. Engelhorn’s Nachk/.) the world knows 
how he adored and loved these giants, and that ‘an 
intense study of their compositions has made him what he 
is.” Powerful factors are at work to bring Reger home to 
the masses. At Leipsic a Reger Gesellschaft has been 
formed, with Richard Strauss at the head, for the purpose 
of organizing regular Reger Festivals and _ publishing 
scientific and artistic works to explain his art and manner. 
Realising a wish of the deceased, funds are to be collected 
to assist young composers in the publication of their works. 
| As the result of a most successful Reger Festival, lasting 
| three days, another Max Reger Gesellschaft has been formed 
at Vienna, with Reichwein, Schiitz, Kostersitz, Foll, and 
Dr. Viktor Junk as founders, and aims similar to that 
| of Leipsic

One does not expect to fill a hall with an audience 
listening to a Reger programme, and it was a daring 
undertaking on the part of Wilh. Jinkertz and Emanuel 
| Gatscher, of Berlin, to advertise a concert devoted exclusively 
to Reger compositions for two pianofortes—the Variations 
and Fugue, Op. 132A, on a theme by Mozart, Introduction, 
Passacaglia, and Fugue, Op. 96, and Variations and Fugue, 
| Op. 86, on a theme by Beethoven. With Reger’s thorough 
mastery of the organ style the character of the 
| pianoforte disappears to a certain degree, and the aural 
effect of the performance does not exceed that of two 
performers on one instrument

On the other hand, in the domain of organ music Reger 
| is the unparalleled master of our time. When the Cologne 
| Gesellschaft fiir neue Musik enlisted the services of 
Hans Bachem, one of the most qualified interpreters of 
Reger’s organ music, it was thought advantageous to repeat 
either at the same or at some future concert the more 
abstruse works. It is characteristic of the audience which 
in this respect begged at the last of the six concerts a 
repetition of the Fantasia and Fugue in D minor, Op. 135R, 
| instead of the monumental Fantasia and Fugue, Op. 46, 
which had opened the cycle of concerts. With such 
programmes it was possible to form a judgment of the style 
|of the earlier works, with their unbridled strength, in 
contrast to the clear ripeness of the later compositions. 
Between the great Fugues, Fantasias, and Passacaglias, 
Bachem interpolated miniature specimens of Reger’s art, 
such as pieces from Op. 59, which depict in an especial 
manner the deep religious trait of the composer’s life work. 
While they showed the organist’s taste in the choice of 
tone-colour, the monumental works proclaimed him a 
master of form and matter

aroused 
formance. 
material, and under its new conductor has regained some of 
its lost prestige, but ist work. still harder to aspire 
to rival the Toronto Choir

To celebrate the eightieth anniversary of the founding of 
the Philharmonic Society—April, 1842—two performances 
of Beethoven’s ninth Symphony were given by the combined 
forces of the Philharmonic and the Oratorio 
Society, M. Willem Mengelberg conducting. This 
glorious composition never fails to draw large audiences, 
who listen reverently, as it were, to the supreme masterpiece 
The Philharmonic Society has given 
immortal over twenty times under several 
different conductors, but on two occasions the choral Finale 
On both of these the Symphony had been 
prepared and announced, but the Ode “ Joy could not 
be sung two weeks after the assassination of President 
Lincoln (April, 1865), or four days after the death of Anton 
Seidl (April, 1898), who was at that time the conductor of 
the M. Mengelberg gave a sound reading of 
the score, though lacked the inspiration he bestows on 
more modern works

Although the four orchestras which during the season 
give over a hundred concerts at Carnegie Hall, and the 
twenty-three weeks of opera given at the Metropolitan 
Opera House are the musical attractions which draw the 
largest audiences at New York, and which undoubtedly 
make the biggest noise, yet the smaller organizations often 
perform music equally (or more) grateful to the ear of 
true music-lovers. One of these, known as * The Society 
of the Friends of Music,’ is under the direction of 
M. Artur Bodanzky. The function of this Society is 
to produce works that are not suited to the modern: full 
orchestra or to the big choir of an oratorio society. The 
orchestra numbers about fifty performers, and on the pro- 
grammes for the last season are found Symphonies by Haydn

On the following Sunday, at the same Concerts, were| sacred Trilogy Zhe J/assion of Christ at the Royal 
given excerpts from Louis Aubert’s delightful fairy opera | Philharmonic Society’s hall, under the direction of Alexander 
Forét Bleue—the production of which at one of the Paris! Bustini, the artistic director of the Roman Philharmonic. 
theatres all music-lovers agree in clamouring for, as they do| This poem, written when Perosi was only twenty-four, is 
for that of de Bréville’s Aros Iaingueur (mentioned in my | the first of the series which has rendered his name famous 
last letter). But the ways of State-aided theatres are | and foreshadows the remarkable melody and orchestration 
strange ; and while these fine works and many others are| which later were to raise such a furore in the Na/a/e, 
waiting, the pageant of still-born operas written to order and | Aesurrection, and Transfiguration. The eminent composer 
produced * because it is the law’ continues its wearisome | (who is, it appears, about to secede to the Waldese 
course. Protestants) resolutely refused to attend the representation

At the Concerts-Colonne Pierné’s So/o de Concer? for | to which he was opposed. 
bassoon and orchestra was performed for the first time. It Amongst other events at the Philharmonic this month 
is an extremely clever and tasteful piece of writing, and was | are to be noted a concert given by the Bolognese ()uartet, 
well played by M. Dhérin, The same day Enesco’s second | which was heard in Beethoven in B flat major and Glazounoy 
Symphony was performed and received with favour. A/| in A, and a seriesof Laud written for strings and pianoforte 
new Trio by Pierné was the main feature at the concert of by Renzo Bossi, teacher of composition in the Milan Conser 
the Socicté Nationale given the next clay. vatory, and son of the famous organist Marc Enrico

The Bacchanal from Roger Ducasse’s Or/Ace received its A notable concert has also been given in the great Hal! 
first performance at the Concerts-Colonne on the following of the Pontifical School of Music by Ulisse Matthey, first 
Sunday. It is replete with effects of extreme violence, most | organist of the Santa Casa of Loreto, with a programme 
ably devised and carried out ; but the substance is somewhat | that included a Sonata in B minor by Schiepatti and a 
thin, and the music at times failed to carry conviction. Symphonic Study by E. Bossi. LEONARD PEYTON

the Concerts- Lamoureux, consists of altogether workmanlike 
developments of a folk-song from the Pyrénées

The Orchestre de Paris gave a Araczilian Suite, pleasing | recently, which proved thoroughly enjoyable. 
and picturesque, by a Brazilian composer, M. Nepomuceno. | gramme was devoted to Tchaikovsky, the other contained 
Other interesting events were the concert given by the Beethoven’s Variations on See fhe conquering hero come 
‘Studio’ at the Hall of the Theosophic Society, at which | for violoncello and pianoforte, with Mr. Boris Hambourg as

Madame Olénine d’Alheim sang a number of songs by Hugo | ’cellist and Signor Guerrero at the pianoforte. 
Wolf, which she interprets admirably ; a programme by The Orpheus Society, conducted by Mr. Dalton Baker. 
Madame M. Meyer devoted to works by the French ‘ Six,’ | gave an impression of careful training at its recent annua! 
Satie, and Stravinsky—all quite entertaining if the} concert. Works chosen included Bach’s / wrestle ana 
listener happens to be in the right mood ; a Vincent d’Indy | fray, Elgar’s Death on the Hills, Bantock’s One with eye 
evening given at the Nouveaux-Concerts (the nomenclature | /Ae fazrest, English Madrigals and part-songs by West and 
alone of new concert halls and new choral societies is| Walmisley, and anthems from the Russian School by 
becoming more bewildering than ever); at the Socicte | Gretchaninov, Tchaikovsky, and Balakirev. 
Musicale Indépendante a good “Cello Sonata by Opol Madame Emma Calvé, Dame Clara Butt, Mr. Kennerley 
Ygouw and Schmitt’s J//rages for pianoforte ; and I must | Rumford, and Mr. Watkin Mills were heard in recital. 
end by mentioning that Madame Roger Miclos, at her} Young singers of to-day might well take note of the dignity 
recital at the Salle Pleyel, gave the first performance of | of style which still distinguishes Mr. Mills. The present 
and John Heath’s A Chz/d’s | age has yet a few things to learn from its predecessor. 
A. BoLp. The efforts of the Toronto Chamber Music Society have 
BENS osm certainly increased the desire for this type of music. A 
ROME | well-filled house greeted the return concert of the London 
At the Augusteo Mr. Albert Coates renewed his triumphs | String Quartet. Never before has this body played 
of last year in a concert which included Holst’s 7%e ?/anets, | with such rare taste and purity of style. The Beethoven 
the Wedding-March from Rimsky-Korsakovw’s Cog d@’Ovr, | Quartet in F minor (Op. 95) gave ample proof of the unique 
Ravel’s Ma Mere 7 Oye, and the Death and Transfiguration | talent the Londoners have of passing the melodies smoothly, 
of Strauss. A second concert given by Mr. Coates was| almost unconsciously, from one instrument to another. 
notable for the performance of a Suite for strings by Purcell | Especially was this noticeable in the beautiful second 
and Tchaikovsky’s fifth Symphony; and his visit closed | movement. The first performance of H. Waldo Warner’s

Harrison’s County of Worceste: 
4 light

