QUEE

A fitting ti 
in the Susse 
except perhs 
by this Orc 
movements 
‘Dante,’ we 
oat on a larg 
by the meet 
amours of 
definite idic 
Spanish. I 
been writte! 
nationality. 
and, at tim 
appeal that 
was a mu: 
to express 
second secti 
if not so ' 
Francesca, 
thing ever 
by Miss I 
developed 1 
we are una! 
that if it we 
still more w 
of a remar 
Beethoven’s 
orchestral 
pianoforte | 
the same | 
played by } 
vid Balfour 
Dance

At the ca 
the previou 
Tanna is or 
A mposers 1 
right to a

sessing ff that if it was worth while doing so well once, it would be | 7 
al, ut f still more worth while to repeat it soon. The other numbers BEECHAM OPERA SEASON. 
wr othe f of a remarkable but far too lengthy programme were ALDWYCH THEATRE

Beethoven’s fifth Symphony, some wonderfully effective ‘ 
F thos § orchestral arrangements by Sir Henry Wood of Bach’s Since our last record there have been repeat performances 
icator, § pianoforte pieces, Tchaikovsky’s third orchestral Suite, and of ‘Madame Butterfly,’ ‘Samson and Delilah, ‘Tales of 
art @ f the same composer’s Pianoforte Concerto, magnificently Hoffmann,’ ‘ Cavalleria,’ and its pretty regular companion, 
warde! | played by Miss Adela Verne. British music had a look-in | ‘Pagliacci.’ A revival of ‘II Seraglio, with Miss Mignon 
ensed | vid Balfour Gardiner’s ever-welcome ‘Shepherd Fennel’s Nevada as Costanza and Mr. D’Oisly as Belmonte, was very 
Dance.’ attractive. A performance of ‘ Tristan,’ given on November 9, 
respec. Atthe concert given on November 11, the novelty, as at | WS made notable because, owing to the sudden illness of 
the previous concert, was by a Spanish composer. Joaquin Miss Rosina Buckman, the part of Isolda was taken ata 
cm is one of the most distinguished of the band of Iberian | few hours’ notice by Miss Agnes Nicholls. No rehearsal 
\mposers that has, by force of ability, recently asserted its | ¥@5 possible, and so with a strange company our intrepid 
mor | Tight toa hearing. The piece produced on this occasion | #4 brilliant British soprano—what an asset she is to the 
(the first performance in England) is entitled ‘ La Procession | COUDtry !—faced the task unflinchingly, and achieved a 
, wit | 42 Rocio,’ and is in two movements, respectively called triumph. She had not previousiy sung the part for three 
a ‘Trina en fete’ and ‘La Procession.’ Debussy once | Y¢@!5- ‘Aida’ was produced with great success on 
rge ot characterised this glowing music as a ‘luminous fresco,’ and November 22, under Sir Thomas Beecham’s direction. 
the description is apt. It depicts a procession in honour of 
yf a § the Blessed Virgin, an event which once a year in June i : Ee 
takes place in Seville. Triana is a suburb of Seville,| _The London Trio—(Madame Amina Goodwin (pianoforte), 
jenti | Unlike Granados’s ‘Dante,’ Turina’s music has the colour} M-. Louis Pecskai (violin), and Mr. W. E. Whitehouse 
and glow that are generally associated with Spanish music. | (’cello)—announces a season of six concerts to be given in 
The abundant life, the contrasts of gaiety and gravity, of the AZolian Hall at 3 o’clock in the afternoon. Chamber-music 
, | Rusic create an extraordinarily vivid impression of the scene is the specialty of this association of artists. The programmes 
eat in the composer’s mind. There can be no doubt that he has | 4re always interesting. Further particulars can be obtained 
of the = imaginativeness and a fine sense of orchestral colour. | from Madame Goodwin, 63, Drayton Gardens, South

look forward to another performance. The programme | Kensington. ; 
i The London Sunday-School Choir will hold its Spring

Mr. de Lara’s concerts at Steinway Hall continue to| Concert in the Ulster Music Hall on November 9 and bai Cecilie and 
present much British music as well as that of our Allies. On | 4" overflowing house. The programme included Haytts I accisted by 
November 15, French music was to the fore. On this| (Quartet in D major, Op. 76, No. 5, Tchaikovsky's Quatté soprano), at 
occasion Lord Burnham was the interval orator. He stated | Op- 14, and * Variations sur un theme populaire russe’ by ta Temple C 
that 550 War Emergency Concerts had been given by | Russian composers. Solos by Miss Burnett and Miss Taylt } Biizabethan 
Mr. de Lara. and songs by Miss Florence Nixon—all admirable of tht ]) Grosvenor

A chamber concert given by Royal College students in kind—contributed to the interest of the occasion. Snes the other ir 
their own Hall, on November 16, brought forward much Gounod’s Mors et Viva’ (Death and Life) yr * Band 17th | 
excellent accomplishment. Miss Kathleen Cooper and performed on November 5 by a specially-selected orchesin virginals, a1 
Mr. F. Holding distinguished themselves in Beethoven’s

does this instrument played in this exquisite fashion induce 
a mood of content and delight

The Clifton Chamber Concerts are being resumed, th 
being the fifteenth season during which the promotes 
| have maintained a highly-educational phase of mux 
Mr. M. Alexander being no longer a_ member 
the Quintet, the first violin is the French-born playa 
Madame Marie Faulkner Adolphi, who has often be 
associated with Mr. Herbert Parsons in recitals in Londe 
and elsewhere, but has not appeared in Cli:ton belo 
Miss Hilda Barr joins the Quintet as second violin, t 
remaining players being as before—viz., Mr. Alfred Bs 
(viola), Mr. Perey Lewis (violoncello), and Mr. Herbert 
Parsons (pianoforte). One Russian programme will be give 
(February 19), this having been a popular feature in the ts 
previous seasons; and the other three will be genen 
programmes, but will not include works by living Germ 
composers, though modern French and Russian ms 
figures largely

The subscribers to the Clifton Chamber Concerts wer 
conspicuous supporters of the concert given at the Victos 
Rooms on November 14 by the London String Quartet 
The visit of Mr. Albert Sammons and his able confréres 8 
arranged by the Clifton Ladies’ Musical Club, and t 
gentlemen of the Bristol Musical Ciub abandoned the 
weekly meeting on account of the concert. The result 
that, with other patronage, every seat was booked, and th 
performers had a very appreciative audience. Mr. T. W 
Petre, who has been away on military duties for some tim 
was able to take his original place as second violi 
Mr. H. Waldo Warner (viola), and Mr. C. Warwick-Evas 
(cello), completing the quartet. Although the chosen wort 
had been heard before in the same hall, the interpretatit 
by these brilliant players made the repetition of the 
numbers most pleasurable. Ravel’s Quartet in F ws 
especially in favour, not only because of the natuare of U 
composition, but on account of the polished and poet 
interpretation. Debussy’s Quartet in G minor (Op. 10) 
also delightfully played. Between the two modern wore 
Beethoven’s Quartet in C minor was given

Mr. Philip Ashbrooke has retired from the Conceft 
Direction Michell-Ashbrooke, and has opened an agent

for the audier ce at this concert was large and appreciative. 
On November 10 Miss Adcla Verne gave a_picnoforte

recital in the Guildhall of works by Mendelssohn, Beethoven, 
Liszt, Chopin, and Alkan

The Musical Club continues its activities under difficulties, 
chiefly financial. Various economies have been effected, and 
these itis hoped will enable the members who remain to 
continue to enjoy the advantages of the Club

DUBLIN

The chamber music recitals at the Royal Dublin Society 
were to have commenced on November 6 with a pianoforte 
recital by Miss Irene Scharrer, but unfortunately Miss 
Scharrer was prevented at the last moment from fulfilling 
her engagement. On November 13 the Brodsky Quartet 
played Mozart, No 6, in C major; Novacek in C, Op 13; 
and Beethoven in F, Op. 135. On November 20 the 
Wessely Quartet played Glazounov, Op. 64, and with 
Dr. Esposito at the pianoforte, Brahms, Op. 26, in A major, 
and Schumann, Op. 44. in E flat, both the latter being old 
favourites which have not been heard at these concerts for

564 THE MUSICAL TIM

he gave a delightful selection from Bach, Mozart, Schubert, 
Sehumann, and Brahms’s shorter works. On the same date 
Mr. Alfred Hollins gave an interesting address on ‘ Musical 
study’ in connection with the distribution of certificates to 
Trinity College candidates. A recital of music for pianoforte 
and organ was given on October 28 in St. George’s U. F. 
Church by Mr. Hollins and Mr. J. Connell, the organist-elect 
of Johannesburg. Unprecedented crowds were unable to 
gain admittance. The programme consisted of varied 
numbers, including a transcription of Elgar’s ‘ Carillon’ for 
pianoforte, organ, and bells, with Dr Kelman as elocutionist. 
Through the courtesy of the military authorities Edinburgh is 
benefiting musically from the talent available among the 
troops stationed here. On November 12 a fine chamber 
concert was given by the 1rogth Reserve Battalion. The finale 
of Mendelssohn’s first String Quartet was given by Lance- 
Corporal Kosky and Privates Leach, Swann, and Hill. 
Lance-Corporal Kosky, in conjunction with Miss Ruby Dunn, 
played the first movement of Grieg’s C minor Sonata. 
Mention should also be made of Lance-Corporal Glover's 
vocal items, ‘ A lover in Damascus’ and Stanford’s ‘ Songs 
of the Sea,’ with male chorus. Lance-Corporal Chanter is 
also becoming known here as an organist and accompanist. 
This concert is merely selected as one of many at which our 
military friends are proving that although occupied in the 
duties of soldiering, they seize every opportunity of keeping 
in touch with their real life-work to the benefit of all 
concerned

The University Historical Concerts have been announced 
for the season. They are six in number, and Prof. Tovey 
has undertaken to devote the entire series to Beethoven’s 
pianoforte works, and act as interpreter himself. The 
programmes are not continuous ; each more or less a 
retracing of the same path, so that each concert will be 
an illustration of the transition from the earlier to the later 
Beethoven. The first of the series was given on November 15

1S

GLASGOW

Although this season’s Choral and Orchestral Ug, 
concerts lose considerable interest through the abseng , 
the Scottish Orchestra, the management is endeavogp 
| to atone for this by presenting programmes of great yar, 
| and attractiveness. At the opening concert, on November; 
|the London String Quartet (Messrs. Sammons, Reg 
| Warner, and Evans) gave a charming reading of ty, 
| strongly - contrasted numbers — Beethoven’s Quartet 
C minor (Op. 18, No. 4) and Debussy’s Quartet 
G minor (Op. 10). The Choral Union, conducted }, 
Mr. David Stephen, sang unaccompanied pieces by Web 
Cornelius, Elgar, and Mendelssohn with good effect, alt

for music of this class a much smaller choir of carefy 
selected voices is essential to bring the performances ¢ 
to the standard to which the competitive festival movemg 
has accustomed us

of which the title of ‘ Rhenish’ was dropped on this occast® 
But the omission need not have been made, for the must 
whatever its title, has passed into the an/e-be//um realm 
things accepted. The same distinction hardly applies" 
Rubinstein’s third Pianoforte Concerto, a pretentious

__ 
in which tk 
was clever! 
who has st 
was furthe! 
ianoforte 
‘Carillon’ 
The Ru: 
sastained L 
whose sing 
Oneigin, 2! 
in which 
interpretatt 
evidently 
lawn © 
interesting 
(first mov 
‘Mephisto 
The firs 
(formerly s 
3o, when t 
String Qua 
in A min 
(Beethoven 
John Bridg 
a player © 
blending | 
deathless c 
was also | 
Arensky’s 
which mal 
modern ch: 
of the Soc 
carried it t 
continues 
and Messr 
secretaries. 
The con 
drew an in 
was suppc 
Arthur Ca 
a young pi 
in C sha 
heights of 
Donizetti’s 
‘Three 1] 
Dr. A. I 
Mr. H. C 
the genera 
were respo 
In conn 
Dr. Mark’ 
on ‘Mode 
November 
composers 
five ‘amal 
Rimsky-K. 
real Schoo 
of Arensky 
noticeable 
or the sens 
first compo 
wand Medtr 
list. compi 
vocal and

anthems

evidently enjoyed, in singing the melodious chorus, ‘At

f careful dawn of day,’ from Cowen’s * Sleeping Beauty,’ and an 
mances YP interesting concert ended with Lalo s Norwegian Rhapsody 
moveme J (frst movement) and Liszt’s more or less thrilling 
. ‘Mephisto’ Waltz. ; 
Universin The first concert of the Rodewald Concert Society 
ian muse f (formerly styled the Rodewald Club) was given on October 
ch Mase’ f 30, when the programme, sustained by the Arthur Catterall 
Univers; String Quartet, included Quartet in E flat (Mozart), Quartet 
tions. (J ip A minor (Arensky), Quartet in F minor (Op. 95), 
OM poser, (Beethoven). Mr. Catterall has able associates in Messrs. 
hmaning John Bridge, F. S. Park, and J. C. Hock, the latter being 
S of ex, # 4 player of exceptional beauty of tone. _The finished and 
| genenly | blending playing of these excellent artists invested the 
iewed, deathless charm of the Mozart music with new delight, and 
Served: f was also effectively employed on the larger canvas of 
ers in kf Arensky’s Quartet (In memoriam Tchaikovsky) a work 
ff (viol, } which makes a strong emotional appeal and, typical of 
member’ f modern chamber music, is orchestral in scope. The affairs 
's bandit f of the Society remain in the excellent hands which have 
l Trot F carried it through a period of difficulty. Mr. Ernest Bryson 
tributed: § continues as chairman, with Dr. Pollitt as hon. treasurer 
ssessor? # and Messrs. H. Ernest Roberts and W. Rushworth as hon. 
lection secretaries. 
anist. | The concert given on November 14 by Madame Clara Butt 
e of ol: # drew an immense audience to the Philharmonic Hall. She 
was supported by Madame Agnes Nicholls, with Mr. 
Arthur Catterall as solo violinist, and Mr. Vivian Langrish, 
a young pianist who was agreeably heard in Chopin’s Scherzo 
in C sharp minor. The great contralto rose to fullest 
~ heights of vocal beauty and dramatic expression in 
an Donizetti's ‘O mio Fernando,’ and invested Hullah’s old 
E ". # ‘Three Fishers’ with renewed charm. A song by 
~ 3 Dr. A. H. Brewer, ‘ Moontime,’ also found favour. 
toca Mr. H. Craxton was a sympathetic accompanist, and for 
Fine’ the general arrangements Messrs. Rushworth & Dreaper 
ea were responsible. _ ; 
- fre In connection with the local section of the I.S.M., 
. typia Dr. Markham Lee gave a timely and interesting lecture 
Thou 4% *Modern Russian Music’ in the Royal Institution on 
this November 4. Beginning with Rubinstein and Tchaikovsky, 
“4 | composers of classical outlook, the ‘revolutionary band’ of 
cae TF five ‘amateurs,’ Balakirev, Cui, Borodin, Moussorgsky, and 
= Rimsky-Korsakov, came under notice as the founders of a 
divide real School of Russian Music, national in type. The works 
edi of Arensky, Glazounov, Scriabin, and Stravinsky were less 
nal noticeable on this basis, being chiefly influenced by the soul 
orang or the senses, while Rebikov, a musical anarchist, was the 
ee first composer to write in the whole-tone scale. Rachmaninov 
: deal sand Medtner (the Russian Brahms) came last in the wonderful 
meloh list compiled by Dr. Lee, in whose musical illustrations, 
ale vocal and instrumental, the lecturer had able help from 
+ hak Miss Barrett and Dr. A. W. Pollitt respectively. _ 
pare Pending the appointment of a permanent organist for the 
ies th Lady Chapel of the Cathedral, Dr. T. Reynolds has 
lodest been appointed organist and choirmaster-in-chief pro tem. 
andi Dr. Reynolds, who is organist at Christ Church, Claughton, 
rota |S formerly 2 Hereford Cathedral chorister under Dr. 
anil Langdon Colborne, and afterwards music-master at Denstone 
College (on the recommendation of Sir F. Gore Ouseley), and 
on later organist of Oswestry Parish Church. One of his 
on tt anthems, an effective setting of Walsham How’s hymn, 
Shoat, For all the Saints,’ was performed on festival scale in 
sasio® 1911 by the Liverpool Church Choir Association. 
-— & Dr. James Lyon’s new fairy opera, ‘ The Palace of Cards,’ 
inc St be produced at the Repertory Theatre on December 21, 
ies ti and will be played on afternoons during the Christmas season. 
wat Report already speaks highly of the clever and melodious

music, and of the libretto by Miss Henrietta Leslie

Miss Alice Jones, a young pianist and a pupil of Mr. Arthur 
de Greef, gave a pianoforte recital in the Rushworth Hall on 
November 16, when she displayed excellent qualities of 
technique and temperament in a diversified selection which 
included Bach’s Italian Concerto, Beethoven’s Sonata in 
F sharp, César Franck’s Prelude, Aria and Finale, Liszt’s 
‘Campanella,’ and a new Rhapsody (MS.) by C. A. 
St. G. Moore

Mr. Vasco Akeroyd conducted the Symphony Orchestra’s 
first concert of the new series in the Philharmonic Hall on 
Saturday afternoon, November 18, before a large audience. 
Commencing with Massenet’s dramatic Overture ‘ Le Cid,’ 
a steady and impressive performance was given of Schumann’s 
fourth Symphony, of which the slow movement and Scherzo 
were especially well played. Grieg’s Pianoforte Concerto 
gave young Solomon plenty of opportunity in the brilliant 
features of the solo part, and vivacity as well as somewhat 
undue velocity marked his playing of a Schubert Impromptu 
and Chopin Valse. The vocalist was Mr. Frederick Ranalow, 
who sang with great acceptance in Verdi’s ‘O tu Palermo’ 
and Vulcan’s song from Gounod’s ‘ Philemon et Baucis.’ 
Led by Mr. J. Nichols, the fine band included several lady 
players in the string department in place of members now 
serving in H.M. Forces

A singularly interesting feature of our musical growth is 
to be found in the increasing tendency of music to penetrate 
into the unaccustomed recesses of our daily life and work. 
Years ago it was only to be had after the day’s business was 
past ; then came Sunday music at various hours, and lunch 
or dinner time was enlivened (?) by the strains of café, 
restaurant, or hotel band. Recently the best cinema houses 
have gone one better. Now at Manchester we are in a fair 
way for establishing noon-tide chamber-concerts of forty to 
sixty minutes’ duration as an integral part of the city’s 
weekly life. No doubt the organ recitals during lunch- 
time at the Town Hall and Cathedral have contributed 
something, and also more recently the policy of the Music 
during War-time Committee has served to give added stimulus 
to what was felt by many to be a real need

To-day, on the busiest of Manchester Exchange days, 
Tuesdays and Fridays, when the normal business population 
is powerfully reinforced for a few hours by merchants from 
many adjacent towns, the spectacle may be witnessed of an 
audience of from 500 to 800 business men and women 
snatching forty-five minutes from their brief lunch-time to 
hear Arthur Catterall and R. J. Forbes play Sonatas by 
Beethoven, Brahms, Busoni, or the moderns ; a pianoforte 
or vocal recital; or, maybe, an a//a cappella programme of 
the highest attainment of modern choral art

The performers are not always of the ‘star’ order. It is 
a convincing demonstration of the drawing power of pure 
music, and all musical idealists, not alone at Manchester, 
must find their enthusiasms re-kindled, feeling that in 
war-time we are on the verge of possible developments in 
this direction fraught with much good to our communal 
art-life. In this way chamber music is coming into touch 
with entirely new audiences, and where this fertilising 
process will end nobody knows. The day is not far distant 
when chamber-music will become a social habit, and the 
‘Kreutzer’ or the César Franck Sonata a subject for 
discussion and speculation much as the latest news from the 
Somme or the operations of the Man-Power Board. As 
though to enforce these thoughts, a programme has just 
come into my hands of a musical week-end at Norbreck 
Hydro (outside Blackpool), where the Brodsky Quartet was 
engaged for the enjoyment of the guests. The Quartets 
played (in two cases solitary movements) were not at all 
austere, but calculated to stimulate a demand for more, and 
the experiment is to be repeated in February. War-time 
has undoubtedly driven us into more rational views as to the 
purpose of music in life

responsive as

his temperament to such rapidly-changing conditions as were 
occasioned by a scheme which embraced Beethoven's 
* Femont Del ussy’s ‘F tes’ and “ Images,’ Ravel's

Mother Goose,’ 
a Vieuxtemps

no

Leeds concerts may be said to have begun with 
Saturday Orchestral Concert on October 28. The original 
programme included Beethoven’s second Symphony, the 
*Meistersinger’ Overture, nd Rachmaninov's second } 
Pianoforte Concerto ; but the last of these had to be altered, 
as Sapellnikov was unwell, but Miss Myra Hess took his 
place, and gave one of the most sympathetic and charming 
interpretations of Schumann’s Concerto it is possible to} 
imagine ; it brought to the present writer recollections of | 
that refined artist, Alfred Jaell, who was so happy in this } 
most poetic work. The perf wmances under Mr. Fricker | 
were uniformly good, but the outstanding featur f the} 
concert was the fact that a programme of this kind attracted 
an enormous audience, which filled the Town Hall; i Ss 
said, indeed, that some hundre were turned away. The 
second concert of the season w mn November 18, when 
Brahms’s second Symphony was found to be quite compatible | 
with a popular pr wramme, and was evidently enjoyed. | 
Madame Kirkby Lunn was the vocalist, id sa the | 
*Fiancée du Timbalier’ of Saint-Saéns and two of | gar's | 
“Sea Pictures.’ An expressive Aria for Strings by Florent

I. Wachet

great classical composers, which is unquestionably a world inheritance of priceless valy, , 7 
\ considerable knowledge of the geni f these c sers can fortunately be obtains} PIS 
A considerable knowledge of the genius of these composers can fortunately be obtain} P° 
by skilfully directed class study of their songs, carefully chosen for the purpose, 4 we 
collective performance, and it is found that this poetical and beautiful music, besig| 45S" 
affording a general and almost indispensable experience in senior classes, makes ; I 
specially deep appeal to the taste of the most musically gifted pupils. Franc

SONGS, each containing between thirty and forty songs by Handel, Bach, Haydn, Mozar (fifty- 
Beethoven, Schubert, Schumann, Cornelius, Mendelssohn, Brahms, and others, arf the'sa 
published (with the accompaniments as written by the composers) at ONE SHILLING an . A. 
SIXPENCE per volume. These songs are also issued separately at prices ranging fro} ¥. VI 
Id. to 3d., and in cheap Books each containing five or six songs of the SAME Gray 
OF DIFFICULTY

Voice-Culture and Sight-Singing.—It is generally recognised that th 
care of Children’s Voices is one of the most important responsibilities of the Clas 
Singing teacher, and experience has proved that without making undue demands on th 
time available for Class Singing, skilfully directed practice can be made to secure Cex 7 
ENUNCIATION and Purity oF VowEL PRropucTION, and by this means bad speech} the se 
habits can be corrected. In this connection, Messrs. Novello are fortunate in being abk 
to place before teachers the methods of Mr. James Bates as described in VoicE-CuLtux J 
FOR CHILDREN, and of Miss Margaret Nicholls as described in the shorter course, Scxou whick 
Cuoirk TRAINING. Hygienic methods of breathing are fully dealt with by Dr. H. Hg = mnun 
liulbert (Lecturer to the L.C.C.) in the Primer BREATHING FOR VOICE-PRODUCTIO be me 
Dr. Hulbert’s teaching has had widespread influence in school circles. ar

In the provision of “teaching,” Salon or “ Romantic” pieces, the practice of 
which is so necessary to the development of the skill and taste of pianists, there are 
innumerable examples by well-known British and other composers, amongst whom may 
be mentioned A. C. Mackenzie, Elgar, Hubert Parry, Tschaikowsky, Coleridge-Taylor, 
Edward German, Cowen, Dvorak, Gounod, Halfdan Kjerulf, César Cui, O’Leary, 
Liadoff, Rubinstein, Purcell, Spindler, and Henselt. Besides editions in folio form, there 
are Firry-Fiv—E ALBuUMs, each containing about twelve pieces and published at ONE 
SHILLING

The Classical Composers are fully represented by Handel, Bach, Haydn, Mozart 
(Sonatas, etc.), Beethoven (complete Sonatas, etc.), Schubert, and almost complete 
collections of the pianoforte works of Schumann and Mendelssohn

Some recently-issued Country Dance Books include many Old English tunes from 
Playford’s celebrated ‘‘ Dancing Master,” edited by Mr. Cecil Sharp. The Times 
Educational Supplement, in a recent review of this collection, said: ‘* There seems to be 
no reason why these healthy and invigorating tunes should not find their way into every 
department of school instrumental music, and there is every reason why their spirit 
should be disseminated as widely as possible

a

362 He that hath a ple asant face - 7 
363 Keep time, keep time ... 1. 3d. 
364 Lo, the peaceful shades ; 14d. 
365 Not for me the lark is singing ,, 3d. 
366 Spring, the sweet Spring 0 3d. 
367 Take heart - eco o d 
368 The ss boat 14d. 
369 The lark .. 3d. 
370 The moon shone ‘calmly bright |. 3d 
371 The reproa 14d. 
372 The swir “~y om ae 3d. 
373 The wrecked hop e . ” 3d. 
374 Twilight ... . 14d 
375 Twilight now is round u 3d. 
376 What is got by sighing 3d. 
377 Where shall the lover rest om 14d. 
378 Night Gounod 3d. 
379 The dawn ‘of day S. Reay 4d. 
3380 The calm of the sea H. Hiles 4d. 
381 The wreck of the Hesperus - 6d. 
382 Uncertain light - Schumann 3d. 
383 Confidence. Double Chorus . 3d. 
384 The Dream - - 14d 
385 — Boat... 3d. 
33¢ ng’s approg ch. a. Seyi mour Egerton 3d. 
337 Wi ild rose @ 3d. 
388 In the woods = 3d 
389 The rose and the soul 2 14d. 
390 Adieu to the woods ae 3d. 
391 King Winter ee 3d. 
392 The Miller G. A. Macfarren 14d 
393 At first the mountain rill ” 19d. 
394 Allis still. 14d. 
5 Sleep the birdisin in its nest J. Barnby 3d. 
399 Hushed in death . Hiles 6d. 
397 Evening ‘lt is the hour) Hy. ‘Leslie _ 
33 Now the bright morning star __,, 3d. 
309 Boat S Hail tothe chief) _,, 14d. 
yoo The tr f Death C. Holland 3d. 
gor Now the t morning star Pierson 3d. 
402 The bright-hair —_ orn S. Reay 3d. 
go3 Re o’er the tores 3d. 
404 Sweet isthe bre 4 of early morn ,, 3d. 
} 405 Where wavelets rippled Ciro Pinsuti 6d. 
s06 We'll gaily sing and play - 6d. 
307 Gently falls the evening Marenzio 3d. 
408 Lilies white, crimson roses (5 v.) ,, 3d. 
409 The shepherd's pipes (5 v.) am 3d. 
410 Spring returns (5 Vv.) ... 3d. 
411 See where witht apid! sound (6v.)., 3d. 
412 Those dainty daffadillies (5 v. ) Morley 3d. 
413 Dainty, fine, sweet nymph ,, on 3d. 
414 Shoot, false love, I care not ,, . 3d. 
415 O say what nymph(6v.) Palestrina 3d. 
4160 Ye singers all H. Waelrent 3d. 
417 Now fie on love G. A. Macfarren 14d 
418 Winds of Autumn! Chas. Oberthir 2d. 
41g Soitly fall the shades . _ E. Silas 2d. 
20 Love me little, love me long L. Wilson 2d. 
2t Shall I tell you whom I love Wesley 3d. 
22 Itwasa lover andhislass J. Booth rad. 
23 Love'’squestion and reply J.B.Grant ad. 
24 Hence,loathed melancholy(5v.)Lahee 4d. 
425 Evening Song ... .. E.M. Hill 3d. 
426 Welcomed 1wn of summer’s day o gf. 
427 Charge of the Light Brigade Hecht 4d. 
428 There is beauty on the mountain Goss 14d 
429 O my sweet Mary (5 v.) » 4d. 
430 Lo! where therosy-bosom’dhours ,, 4d. 
431 Her eyes the glow-worm » 44. 
432 Bells of St. Michael's Stewart 4d. 
433 The Cruiskeen Lawn (5 v.) - 3d. 
434 The wine cup is circling a 3d. 
435 Ye mariners of England H. Pierson rd. 
436 The Vesper Hymn Beethoven 2d. 
437 What though sorrow Naumann 2d. 
435 The Swe allows Pohlentz 2d. 
439 Hope and Faith Weber 2d. 
440 Hi ark, hark, the Lark . Kicken 3d. 
441 A walk at dawn... Gade 3d. 
442 Winter days A. J]. Caldicott 4d. 
443 Homeward Henry Leslie 4d. 
444 To sea! the calm is o’er Marshall 4d. 
445 Rest hath come. 2d. 
440 Hymn to the Moon cJesiat ‘Booth 4d. 
447 The Broo . G. Reissiger 3d. 
448 The Secret 3d. 
449 Is it to odours s\ weet R. Miller 3d. 
450 On the water R. de Cuvry 3d. 
451 The Water-lily N. W. Gate 2d. 
452 There's one that llove I. Kiicken 3d. 
453 The trees are all budding ~ 3d. 
454 There sings a bird Franz Abt 2d 
455 O world! thou art so.. Hiller 4d. 
456 Winter Song “ H. Dorn 3d. 
457 The arrow an ithe song W. Hay 3d 
458 Kings and Queen "Ciro Pinsuti 3d 
459 Wou sid you ask my heart? - 3d. 
460 The Rhine Raft Song _ 3d 
461 The Silent Tide ... ~ 14d. 
462 The Apriltime ... oe ” 2d. 
463 The Song to Pan - 3d. 
464 Autumn Is come again F. Corder 3d

Na

