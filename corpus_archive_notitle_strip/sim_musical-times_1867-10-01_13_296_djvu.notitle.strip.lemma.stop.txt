ly surprised Madlle . ‘ Tietjens 

 overture Leonora , perform wonderful spirit 
 precision orchestra , Professor 
 Bennett beautiful Pianoforte Concerto F minor , 
 execute Madame Arabella Goddard usual 
 finished manner , special feature 
 miscellaneous act ; rest programme 
 compose wear material , redeem 
 point Mr. Sims Reeves ’ interpretation 
 Beethoven Adelaide , Madame Arabella God- 
 dard pianoforte accompaniment 

 performance Messiah 
 morning Festival , draw enormous audience 
 Hall ; , fact , stand 
 room building . Mr. Sims Reeves 
 excellent voice , sing ' ' comfort ye " 
 " valley " usual truthful expression 
 feeling ; trying song ' thou shalt 
 break , " evident trace 
 indisposition shake . 
 principal vocalist Madame Lemmens- 
 Sherrington ( sing soprano solo music 
 second ) , Madlle . Tietjens ( 
 replace , encore 
 " t know redeemer liveth " ) , Mesdames 
 Sainton - Dolby Patey - Whytock , Mr. Santley . 
 chorus perfection ; , 
 endorse Mr. Costa opinion 
 opening ' unto child bear " 
 sing piano , order theatrical burst sound 
 shall come word ' wonderful , " 
 general effect , ' hallelujah , " 
 hear produce save 
 Birmingham choir 

 76 

 congregation . service intone clearly solemnly 
 Rev. Mr. Williams , Rector Beaumaris . Rev. J. 
 Price read , Archdeacon Jones second lesson . 
 Tallis Preces Responses use . chant 
 anglican , Psalms double single 
 chant Dr. Monk ; Magnificat Nunc Dimi : tis 
 sing double single chant Mercer ' * Church Psalter . " 
 anthem Richardson , ' ' o amiable thy dwelling , " 
 render . service conclude Congre- 
 gational Psalm Tune , word ' ' duw mawr y rhyfeddodau 
 maith . " number chorister nearly 300 . 
 ' musical portion service organize 
 Mr , E. W. Thomas , Organist St. Ann , conduct 
 festival day effective manner . following 
 choir present , festival : — Llanllechid , 
 Llanfairfechan , Aber , Llandudno , Glan Ogwen , St. Ann , Lian- 
 degai , Gelli , Penmachno . Pranororte Recital 
 Classical Music Penrhyn Hall , Mr. A. Lan- 
 dergan , Organist St. James ’ Church , Upper Bangor , Tuesday 
 evening , 17th ult . vocalist , Miss Edith Wynne . ' pro- 
 gramme select work Beethoven , Mozart , Men- 
 delssohn , Hummel , Dussek , Clementi , Gounod , Benedict , c. 
 Mr. Landergan contribute air variation 
 original theme Pianoforte . hall fill 
 fashionable attentive audience , thoroughly enjoy 
 entire programme . local paper state Concert 
 great success . speak high term vocal ability 
 Miss Edith Wynne , Mr. Landergan pianoforte play 

 Boston , U.S.—Mr . James Pearce , Mus . Bac . , 
 Oxon . , Philadelphia , July August series Organ 
 Concerts , Music Hall , large audience 
 previous visit . programme consist work Bach 
 Mendelssohn , transcription Handel Beethoven 

 CARMARTHEN . — Choral Association 
 Archdeaconry Carmarthen hold Second Festival St. David 
 Church , Wednesday , 28th August . thirty - choir 
 present , number upwards 800 voice . 
 choral service occasion , morning 
 Welsh , direction Mr. Owen Davies , Welsh Choir- 
 master , evening , English , entirely hand 
 Mr. Scotson Clark , organize choir Asso- 
 ciation , great ability untiring energy train 
 speak highly . prayer intone 
 Welsh Rev. Mr. Howell , Lianedy , 
 second lesson read respectively Rev. Mr. James , 
 Abergwili , Rev. Mr. Evans , Llandebie , sermon 
 preach Rev. John Griffith , Cwmavon . musical 
 service conduct Mr. Owen Davies , 
 accompany organ , creditable manner , 
 Mr David Williams , organist church . 3.15 english 
 service commence . prayer intone Rey . Mr. Phil- 
 lips , Llwynmadoc ; lesson read Rev. R. Lewis , 
 Lampeter Velfrey , Rev. Mr. Harrison , Laugharne , 
 sermon preach Rev. Montague Erle Welby , M.A. , 
 incumbent Saints , Oystermouth . choral service , 
 , remarkably . Mr. Scotson Clark , 
 occasion , entirely dispense service conductor , pre- 
 organ - know ability . 
 particularly mention careful rendering versicle 
 canticle , life spirit throw 
 hymn ; distinctness word chant psalm , 
 point room improvement 

 Miiller 
 Miiller 

 Beethoven 

 J. Atterbury 

 313 thank God Elijah 
 317 shall light - . ditto 
 , soul 42nd Psalm 
 ww hy , soul ( chorus ) ditto 
 ye nation , offer Lobgesang 

 BEETHOVEN " ‘ ENCEDI . 
 ( mount OLIVES 

 195 o praise , ye nation 3 

 196 hallelujah ... ese wo 2 

 BEETHOVEN MASS INC . 
 190 Kyrie — thee 4 
 Gloria — praise Lord en 
 qui tollis — ear 

 22 oo ee om co 

 11 . Pianoforte Pieces , Wallace 

 12 . Beethoven Sonatas . edit Charles Halle . 
 ( . 1 . ) contain Sonatas . 1 2 
 op . 2 complete 

 13 . popular duet Soprano Contralto 
 Voices 

 19 . favorite air Messiah , arrange 
 Pianoforte 

 20 . Beethoven Sonatas . edit Charles Halle . 
 ( . 2 . ) contain Sonata . 3 op . 2 , 
 Sonata Op . 7 , complete 

 21 . Pianoforte Pieces , Ascher Goria 

 

 28 . Beethoven Sonatas , edit Charles Halle , 
 ( . 3 . ) contain Sonatas . 1 
 2 op . 10 

 29 . Contralto song , Mrs. Arkwright , Hon , 
 Mrs. Norton , & c 

 80 . Beethoven Sonatas . edit Charles Halle , 
 ( . 4 . ) contain Sonata . 3 op , 
 10 , Sonata Pathetique 

 31 . Beethoven Sonatas . edit Charles Halle , 
 ( . 5 . ) contain Sonatas . 1 2 
 op . 14 

 32 . Beethoven Sonatas . .edite Charles Halle 

 no.6 . ) contain Sonata Op . 22 , Sonata 
 p. 29 , celebrated Funeral March 

