1076 MUSICAL TIMES — DecemBer 1 1927 

 apparatus criticus merely dismiss piece possess marvellous passage . 
 String Fantasia volume ' evidently| time listen sweet 
 early copy . ' ] polyphony Byrd ; hard , rasp 
 original ? ' undoubtedly purcell’s| counterpoint Heinrich Kaminski Busoni . 
 handwriting . Purcell habit , far ] certain passage poetry Schumann , 
 aware , copy works.| rhythmical vigour Beethoven , 
 old master , Blow , kind do| suddenly find shower 
 . intricate scholastic firework , equal 

 reverse huge volume _ turn it| Bach ' Art fugue . ' surprise 
 upside , find Trio figure - bass , | rhythm cadence charming 
 ' plunge ye confine dispair God cry ' ; | continual ; difficult imagine 
 Gloria ; motet , " Jehovah quam multi sunt | transposition viol modern string 
 hostes mei ' ; - anthem , ' o Lord , | /#strument piece harm , 
 Governour ' : tric . ' sick life ' : al especially slightly diflerent compass 
 - anthem , ' hear , o Lord . ' | TRIUEREINS , CIORRINY " 
 appear Purcell use Abbey avoid altogether KramSpOSHION 60 & key 
 book work compose set | tone high . transposition good , 
 string piece . object Purcell intend |s general pitch Purcell day 
 Fantasias precisely know . good deal | high — authority 
 music write sheer pleasure | extent rusts . Messrs. Warlock 
 home performance friend . Mangeot 8 gift 
 Pepys Evelyn witness laudable bring treasure light , 
 custom . , troublous | @¥ailable chamber music party 

 speak mark expression ; term 
 intensity mark . , people think 
 requirement , matter concern 
 tone , obedience ' mark . ' 
 disastrous error ! look instrumental 
 music possess , test follow statement . 
 string music fall period respect 
 Period I. , Fantasies , & c. , Tudor 
 mark ! clearly , 
 g heretoobey . Period ii . , Corelli , Handel , & c 

 ' mark ' occasional forte Piano , 
 , , player leave find scheme 
 iii . , Haydn , Beethoven : 
 Beethoven , 
 
 mislead 

 intensity 

 /egato . music connected 
 style (= /egato ) , play 
 slur ' smooth turn ' change } 
 bow direction . ( eg . theme Haydn 

 Emperor ’ Variations Quartet . 77 . ) } 
 music separated style , , the| 
 bow stop order produce silence 
 note . , example , 
 note follow passage , 
 open Beethoven Quartet , Op . 18 , . 1 

 Ex . 3 , 
 allegro con brio 

 uniformity , player chord 
 break effect anticipation , completion . 
 |the primal need music clear 
 personal , forceful , tone case 
 melodic sound . break chord 
 sotto voce , climactic point 
 occur . pedalling good , 
 |be revise individual teacher student . 
 Tempo , crotchet = 112 

 . 32 . Beethoven . Bagatelle G _ minor , 
 Op . 119 , . 1.—the chronology Bagatelle 
 certainly place year later 
 . 29 , g.v . certainly material 
 mature individual great master . 
 spacious air music 
 Episode cross - rhythm Coda . 
 case . 29 , form examine 
 view interpretation . piquant 
 strain section , pass thoroughly 
 beethoven atmosphere second . note 
 contain middle phrase , playing 
 qualify affect . use 
 accented auxiliary sound section note- 
 worthy . distinctly , gently 

 accented . pace 
 crotchet = 152 

 . 36 . Arne . Andante Sonata D minor 

 intensely interesting find ou : native 
 Arne tendency future sonata form 
 Haydn , Mozart , Beethoven 
 develop time C. Ph . E. Bach . 
 Arne year senior , 
 - Haydn . evident 
 consideration growth sonata 
 Arne account . 
 sharply - define subject , equally 
 definite transition lead short ( , accord 
 age , slightly apologetic ) second subject 
 rhereupon follow interesting development . 
 recapitulation pursue plan subsequently adopt 
 master sonata . fact 
 interpretation largely rest . great care 
 phrase - mark . entry 
 theme bar 38 ] 46 emphasise , 
 stress favour , 
 real - entry . tempo easy 
 flow , , crotchet = 100 108 

 . 37 . Edgar L. Bainton . ' Rivulet . ’—The 
 passage - playing fairly difficult , need 
 utmost care fingering . juvenile amiss 
 respect . passage kind 
 play twice loose incorrect fingering , 
 wrong mental image fingering acquire , 
 possible condition original freedom 
 entirely lose . abundant opportunity 
 cantabile , , bar 3 , 4 , & c. , type 
 touch contrast . poetic motto 
 repeat aloud stage study , 
 prevent unconsciousness rippling 
 nature music . need negative 

 musical texture . tempo minim 76 . 
 . 50 . Mozart . theme 
 variation ' Minuet Mr. Duport 

 Mozart variation , reach 
 intellectual grip independent significance 
 , later , distinguish Beethoven . 
 little need theme , 

 probably owe delicate treatment | 
 composer , feature find response | 
 variation 

 The| op . 10 , 

 . permanent mischief result . 
 , prefer start Jiano work 
 gradually climactic point mark # - 
 rate , interesting 
 steam way . tempo 
 necessarily depend skill . 
 low crotchet 104 , high crotchet 132 . 
 . 54 . Rameau . ' Le Rappel des Oiseaux . ' 
 exquisite early 18th - century piece veritable 
 multum parvo . ( recall remark 
 . 23 reference atmosphere . ) 
 special sense remember instru- 
 ment Rameau write piece , 
 adaptability light effect evanescent 
 tone . , heaviness , , relatively speak , 
 fullness tone avoid . direction 
 non legato sum type key - treatment desire , 
 mind conventional 
 /egato sign add . ' pedal prohibit , 
 use supreme discretion , 
 obliterate delicacy figure . 
 tempo approximately crotchet 88 . 
 . Beethoven . minuet Trio , 
 3.—a difficult task ! section 
 unusual power canfadile , 

 

 . 
 inspiration - player magically | 
 subtle tone - poem . scarcely need say| 
 delicacy conception interpretation | 
 keystone performance . let ornament 
 play , swiftly necessary , 
 grace . spare | 
 original print . general tempo } 
 dot crotchet = 50 

 . 65 . Beethoven . movement op . 26 

 difficult little space adequate 
 help movement specially require | 
 help . number . theme : 
 cantabile style prevail slight insistence | 
 melody . forget left hand 

 hand touch condition arm - consciousness 

 variation 3 : syncopation mentally clear 
 ; end z / orzsando 
 indication . pedalling insight require . 
 easy thicken general texture produce 
 confusion thought diction . variation 4 : 
 great care need touch - mark . 
 strife detached hold sound 
 large charm . thwart 
 , particularly way improvident pedalling . 
 variation 5 : Beethoven characteristic com- 
 mentarie theme . _ gentle 
 judicious rubato interpretative need . coda : 
 aftermath , think treat 

 l 
 Couperin idea , the| esthetic power command player , 

 later month , Paul Tafanel Paris 
 Conservatoire , season wind style 
 Stanford conduct ' St. Matthew ' Passion 

 end interregnum , autumn 
 1896 spring 1899 Cowen sole 
 charge . period date introduction 
 annotate musical quotation programme ( 
 C. A. Barry E. F. Jacques , 
 later date Ernest Newman ) , raising 
 orchestral strength normal , 
 wagnerian ' special . ' 
 régime , time , 
 Beethoven Symphonies chronological order 
 course season , conduct 
 score . memory fault , Richter 
 perform feat — autumnal 
 tour province London orchestra . 
 Cowen bring time all- 
 Tchaikovsky programme , ' - time ' item 
 night ! , January , 1898 , begin 
 russian vogue regard 
 bane blessing . authoritative 
 playing Violin Concerto Dr. Brodsky 
 mean share initial Tchaikovsky specialisa- 
 tion . memorable feature time 
 concert performance Berlioz ' Trojans 
 Carthage ' ( believe theatrical presenta- 
 tion time Hallé guidance , lack 
 authentic confirmation ) introduction 
 Elgar Manchester notice véd ' King Olaf 

 spring summer 1899 
 power Hallé throne learn 
 Hans Richter , Vienna , , 
 phrase , somewhat ofermiide , welcome 
 onerous job free entanglement 
 petty jealousy inseparable opera Vienna . 
 Manchester conductorship 

 1089 

 man contribute 
 Manchester , musical sense , long 
 commercially — world - centre . Richter 
 advent orchestral music speedily acquire 
 monumental grandeur . ennoble music 
 touch — Bach , Beethoven , Brahms , Mozart , Berlioz , 
 Strauss , Elgar — contrive 
 liftto sublime height . witness mighty evocation 
 Nature opening bar ' Zarathustra , ' 
 ' praise holy ' section ' Gerontius , ' 
 closing moment ' Leonora . 3 . ' 
 modern french school little affinity , 
 Debussy orchestral 
 clumsiness ; Richter 77 exvce / sis 
 Mass D b minor Mass , work 
 reveal truly olympian scope genius . 
 untiring Lisztian , Liszt 
 enthusiasm exist - day 

 . espousal Elgar characteristic 

 Evans , Lillie Wormald 

 ship know - day , Wagner epoch- 
 marking pamphlet ' conduct ' issue 
 year providential meeting 
 Hans Richter ( 1869 1870 ) , 
 joint study merely Beethoven Symphonies , 
 Quartets , fortify Richter 
 profound unique technical knowledge 
 instrument . vital factor power 
 audience player alike complete 
 mastery principle orchestral dynamic . 
 true musical world 
 know climax comparable , 
 phase art receive fine recognition 
 perfect expression Herbert Sidebotham 
 Manchester Guardian , Wagner 
 evening 

 patet incessu deus — Olympian 
 majestic discipline melodic advance , big 
 fate , regular calm , relentless 
 minded march Napier british infantry 
 hill Albuera , fervid tumultuous 
 depth impotence emotion 

 L. SABANEEV 

 Leonid Leonidovich Sabaneev bear 1881 , 
 sign talent age year , 
 receive - class musical education 
 home , guidance Prof. 
 N. S. Zverev , Moscow Conservatoire ( 1888 ) , 
 N. Ladukhin ( 1889 ) , S. I. Taneev ( 1890 ) . 
 childish attempt composition ( music 
 ' ( Edipus Rex ' Funeral March memory 
 Beethoven , attract attention 
 Taneev , supervise musical training 
 ® late brother ( Professor 
 organ Moscow Conservatoire ) 
 death Zverev , L. Sabaneev complete 
 Conservatoire course Prof. N. Y. Schloezer 

 1559 - 90 

 Rondo final movement sonata . 
 construction Allegro , 
 open subject , form theme , repeat 
 refrain end section 

 p. 42 tell Schuppanzigh 
 man interpret Beethoven Quartets 

 like M. de Pourtalés , Bidou devote 
 deal consideration te 
 inspire rain - storm Majorca . Bidou 
 follow Wodzinski decide b minor 
 Liszt f sharp minor , Ganche d flat 

 check compulsory sight - reading , course ; m Verlag Englert & Schlosser 

 soon feature tactfully introduce | ' Beethoven : Critical Study . ' J. W.N. Sullivan 

 better school festival movement . pp . 256 . Jonathan Cape , 7s . 6d 

 Paxton , 2s . 6d 

 Dr. Mansfield difficult thing 
 . book concise clear , 
 book rudiment , like anthology , 
 leave differ 
 include , strike commonsense mean 
 sparing - comprehensive . liberal 
 use music - type illustration good feature . 
 pity long list Errata insert . 
 reader transfer correc 
 tion place use book . 
 way , glossary italian foreign term 
 preface note tell 
 use english french composer 
 tongue direction ( presumably Beethoven 
 Schumann use German ) apparently 
 ' reason national vanity . ' 
 use Italian 

 far bad fault — affectation 

 van der Straeten 

 new serie comment violoncello solo 
 deal composer far apart Raff 
 Casella , Beethoven Mr. van der Straeten 
 che little volume useful hint ; 
 Mr. van der Straeten slightly overdo 
 speak Raff Cavatina ' wonderful 
 melody , great inspiration famous 
 composer’—or Mr. Algernon Ashton 
 ' eminent chamber music composer ' ? imagine 

 little volume write eye 

 prefer L.M.S. rhythm Southern 

 Railway . L.M.S. play Beethoven . 
 great Western ' catamaran , catamaran , ' 
 essay Beethoven rhythm 

 know , ask , cat lap milk triplet 

 music include Bach Chorale , ' God liveth 
 , ' Psalm ' Quam dilecta ' ( chant Dr. Bairstow ) , 
 Magnificat Nunc Dimittis ( Stanford G ) , anthem 

 Lord ' ( Walmisley ) , Beethoven 
 | ' hallelujah Father . ' singing marke.| 
 unusually good feature attack ensemble diff -=it 
 obtain vast building , generally - ult 
 amply justify arduous work preparation . 
 sermon preach Rev. H. McGowan , 
 Southport . Mr. H. Goss - Custard orgen’st , 

 Mr. Branscombe conduct . immense 

 party organist friend advantage 
 special train charter Messrs. Henry Willis 
 October 22 , attend organ recital Liverpool 
 Cathedral Mr. Harry Goss - Custard . 
 programme include Bach Toccata Fugue D minor , 
 Franck second Choral , Finale Vierne 
 symphony , John E. West ' song triumph , ' 
 Wolstenholme fantasia FE 

 Southwark Cathedral , December 17 , 3 , 
 perform Schumann Advent Hymn , _ Brent - Smith 
 " hymn Nativity , ' Dale ' paling 
 star , ' Beethoven seventh Symphony . New Year 
 Eve carol service 3 

 organ Camberwell Green Congregational Church 
 rebuild enlarge Messrs. Rest , Cartwright . 
 opening ceremony place November 10 , 
 Mr. Allan Brown recital . < collection realise 
 nearly pound 

 sharp flat , use - bow instead 

 - bow . teach Beethoven Sonata 
 Chopin Ballad ? , teaching mean 
 ability teacher interpret composition 
 properly pupil — tinkle right 

 

 string quartet , thank 
 e Léner ( Juartet 
 port suggestion recital Duke Hall 
 Monday afternoon , November 14 . concert great 

 cess , naturally M. Léner colleague 
 ociferous welcome appear . programme 
 va model good taste — Beethoven Quartet e minor 

 Op . 50 , . 2 , follow Mozart D minor . 

 rheir outlook ; dispassionate | St. Martin - - - field . choir 

 d iew . Beethoven | alert body , musicianship , adaptability , 

 nent Vieuxtemps , Chopin Schumann — master | enthusiasm prove success large 

 able pianoforte capable . tackle a| pest item bring good singing — healthy 

 Beethoven Sonata interested , know sign ; bring encore — good 4 
 ma ' bar — player know , — | token . admirable sonority creditable { 
 interest purely technical . KF . B. freedom Cavendish madrigal , ' come , gentle swain ' ; : 
 9 couple - song Sibelius striking ; \ 
 able Whittaker ' Bobby Shaftoe ' capitally touch . { 
 Miss TRIESTINO QUARTET Choir equip , bless rarity f 

 vand rhe appearance english audience | - day , rich ripe second - bass contingent , ' 
 lriestino ( Quartet ( Zolian Hall , November 17 ) provide | good string - like bite tocc , Mr. Thomas Armstrong f 

 Mr. Collingwood ' Macbeth . ' .. . 
 happen Shakespeare , old problem 
 recur : music tragedy superb 
 twice lifetime find dramatic 

 composer demand . reveal . 
 . freshness Haydn music , win sympathy . pp rere wage ens . ee ~ ght : 
 ae Beethoven Harp Quartet reading Mage meet = o o ee swiftly — 
 ber . ¥ : action , speech ( alter zs action ) , ' 
 remarkable neatness , tone | " o * 97 ® 6tOD , © : - es ‘ ay 4 
 > , , : . drag . Mr. Collingwood bad 
 nore luscious . Schubert romantic vein suit 5 . 
 . abundant dash energy , operatic composer matter — , 7 
 > etter . = abd é aasn ¢ ene ; sre , fie ver , . 
 ; b ) 1 : . time character some- 
 bax admirably contrast , excellent quality tone > : 
 : 6 . " ¢ , | thing near impassioned speech wonder 
 nore outcome skilled bowing ' wobbly 
 f. " | relapse moment customary long - draw j 
 ugh } linger . F. B. . - ' ; 
 5 method interest shift instrumental 
 background . method passable — good — 
 pen text poetaster . treat Shakespeare 

 KING DAVID 

 ing perpetrate , especially evening concert , 
 hen expensive prime donne warble royalty ballad , 
 ere ' nay . ' recent Festival 
 ere good ' chestnut , ' rate 
 vere unexceptionable quality . remember 
 festival audience compose expert , 

 f regular concert - goer , include large proportion 
 f describe ' triennial musician , ' 
 thing Beethoven C minor Symphony , 
 Mozart ' Jupiter , ' Schumann Pianoforte Concerto 
 entirely unfamiliar , mention 
 remind supremely fine interpretation receive . 
 Miss Myra Hess long associate work , 
 ind occasion tune exactly right 
 mood , reading perfect sympathy 
 freedom , degenerate licence , 
 remember hear surpass . refer 
 instrumental virtuoso , 
 choice Suggia Haydn Violoncello Concerto D , 
 Gieseking Tchaikovsky Pianoforte Concerto B flat 
 minor , Thibaud Lalo ' Symphonie 
 happy , artist 
 task — add - 
 artist reading strike correct inspired 

 function festival foster art 
 composition , com- 
 mission encouraging , principle 
 right . work , 
 orchestral composition , choose . passing , 
 suggest orchestral novelty 
 chance hear ordinary orchestral 
 concert , festival afford occasion choral music 
 unique opportunity rehearsal , reason 
 surely preference . decision 
 Frank Bridge orchestral rhapsody , * ' enter , 
 Spring , ' sound , Mr. Bridge 
 com poser unite complete 
 musicianship originality thought . work 
 lay open objection easily 
 bring ' programme music , ' com 
 poser conception subject coincide 
 listener . affect value 
 ' enter , Spring ' composer depart 
 conventional representation season , 
 imagine blustering , riotous equinox . safe 
 judge work simply music , point 
 view command respect , warm feeling 
 come close acquaintance complexity 
 elaboration demand . leave exhausted 
 certain breathlessness involve constant de- 
 pendence fragmentary theme incessant change 
 Andante middle section 
 entirely dissipate . realise 
 furnish problem worth solve , 
 performance great familiarity acquire 
 ease delicacy , rough place 
 plain 

 

 ALNWICK . n Alnwick District Male - Voice Choir 
 form direction Mr. A. G. Y. Brown . 
 London String Quartet play local B.M.S. 
 centre October 20 — Beethoven , Op . 18 , . 2 , Borodin 
 D , Goossens sketch 

 BEXHILL . orchestral concert 
 Colonnade time week municipal orchestra , 
 Mr. William Penman . programme 
 " popular good , ' enjoy large 

 Arensky Pianoforte Quintet play 
 Kathleen Frise - Smith Philharmonic Quartet 
 Queen College , Miss Fanny Davies join 
 Catterall Quartet Brahms concert Society 

 Bach recital Queen 
 College Mr. Claude Biggs.——Visitors include 
 Suggia , Bauer , Gerhardt , Cortot.——The London 
 String Quartet visit Bournville work October 22 , 
 play Dohnanyi Beethoven . 
 BOLTON.—At 

 Amateur Orchestral Society movement 
 ' Eroica ' Symphony , Mr. Archie Camden 

 BRADFORD.—At evening concert Philharmonic 
 Orchestra , Mr. Keith Douglas , Mr. Harry Horner 
 sing Mystical Songs Vaughan Williams . 
 concert place , 
 guitar recital M. Segovia rouse great interest 

 BRIGHTON.—At ' guest night ' Sussex 
 Women Musicians ’ Club , string - play member 
 performance Beethoven Septet.——An 
 Alderman provide prize win 
 festival musical ' Brighton Yell 

 Bristo .. — * Flying Dutchman ’ perform 
 Sir Herbert Brewer open concert 
 Bristol Choral Society October 29,——Bristol Tudor 
 Glee Singers Sunday evening concert 
 Little Theatre Orchestra.——Pachmann 
 celebrity concert organize Messrs. Duck , Son & 
 | Pinker . ' cosi fan tutte ' ' Travelling Com- 
 | panion ' Victoria Rooms week 
 | begin December 5 

 MUSICAL TIMES — DEcEMBER | 1927 1127 

 BurtTon.—Sir Hamilton Harty Hallé Orchestra | 
 visit Burton October 25 play Beethoven fifth 
 Symphony ' Scheherazade ' Suite . Mr. Tudor Davies 
 vocalist . — — Mark Hambourg Pouishnov 

 en recent visitor 

 Municipal County Chamber Concerts 

 Beethoven C sharp minor Quartet , Op . 131 , Brahms 
 minor , Op . 51 , . 2 . Hallé Orchestra book 
 future concert series . recent 
 visitor include Backhaus Dame Clara Butt 

 DONCASTER.—Purcell * King Arthur ’ perform 
 opening concert Doncaster Musical Society 
 November 3 . voice choir 
 effective work Mr. H. A. Bennett . orchestra , 
 chiefly draw Leeds Symphony Orchestra , play 
 Mozart G minor Symphony 

 Cecilia Orchestral Society hold October 30 . 
 Mr. W. Osborne conduct popular selection 
 include ' Fingal Cave ' Overture . -Mr . Wilham 
 Primrose , Mr. Douglas Cameron , Mr. Harry Isaacs 

 play trio Mozart , Beethoven , Brahms 

 Chamber Music Club November 4 . — Public 
 library lecture Mr. L. R. McColvin 
 russian music , Mozart opera , César Franck 

 LANDFORD.—A concert organize Miss Olive Boult | 
 Landford District Choral Society 

 November 4 . Beethoven second Pianoforte Trio 
 play Mr. Mrs. F. Alcock Dr. Adrian | 
 Boult 

 Leeps.—Brahms Violin Concerto play 
 Mr. Arthur Catterall concert Leeds 
 Symphony Orchestra , Mr. Julius Harrison conduct 

 LeICESTER . — Dr. Malcolm Sargent conduct enjoy 
 able concert Leicester Symphony Orchestra , 
 shief work Borodin second Symphony ' La 

 Mozart Pianoforte Concerto 
 George Chavchavadze . — — 
 Montfort Orchestra play ( uilter 
 join Mr. Sydney Johnson 
 Allegro Guilmant 
 Symphony organ.——The Rowena Franklin String 
 ( Juartet play Dvorak , Vaughan Williams , Beethoven 
 Chamber Music Club October Gerhardt 
 Cortot recital October 31 

 Boutique Fantasque . ' 
 play Prince 
 October 30 de 
 * Children Ov 

 Choir Dr. Hopkin Evans November 12.——two 
 successive philharmonic concert provoke criticism 
 dullness programme . follow 

 Rachmaninov second Pianoforte Concerto , 
 play Solomon , ' Escales , ' Jacques Ibert , Rimsky 
 Korsakov ' Grand Pique Glazounov 
 * " Stenka Razine ' ; ( November 2 ) Glazounov ' Carnival ' 
 Overture , Beethoven eighth Symphony , Grieg second 
 ' Peer Gynt ' Suite , Ethel Smyth ' wrecker ' Overture , 
 song sing Madame Olczewska . conductor 
 respectively M. Rhené - Baton Sir Henry Wood 

 Mr. Vickers - hundredth concert occur October 22 . 
 ' celebrity ' 

 e — DEcEMBER 1927 

 Dowland ' come , sweet love , ' Elgar ' o wild , west 
 wind , ' - song Brahms , Parry , Stanford , 
 Walford Davies . — — ' Israel Egypt ' revive 
 Hallé concert , 1894 , 
 Miss Caroline Hatchard , Miss Margaret Balfour , 
 Mr. Arthur Jordan solo singer . choir sing 
 finely Sir Hamilton Harty , Manchester 
 lack curiosity matter.——The Municipal Choral 
 concert open ' Lohengrin , ' Sir Hamilton 
 larty . — — Manchester Barclay Bankers , emulate 
 London , develop flourish musical society , 
 choose ambitious programme Wagner , Brahms , 
 Elgar November 15 . purely orchestral Hallé 
 concert following work play : Wagner 
 selection , Miss Florence Austral die speech 
 Briinnhilde Isolda ( October 20 ) ; ' Heldenleben ' 
 ' Norfolk rhapsody ' Vaughan Williams 
 ( October 27 ) ; Backhaus Brahms B flat Concerto 
 ( November 3).——two concert Catterall ( Quartet 
 provide Beethoven D , op . 18 , . 3 , Tchaikovsky 
 D , op . 11 ( October 19 ) , Brahms programme 
 Quartet B flatand Pianoforte Quintet ( November 8 

 Mr. Catterall soloist 
 month , Gieseking , Dr. Brodsky ( Bach 
 \ minor Concerto orchestra College student 
 Mr. R. J. Forbes ) , M. Etcheverria ( 
 spanish song ) , Mr. Robert Parker , Messrs. Harold 
 Dawber Albert Hardie pianoforte ( Arnold 
 Bax ' Moy Mell ' Rachmaninov Suite ) , Chaliapin , 
 Pachmann connection Chronicl 
 Cinderella Club sixteen free concert child , 
 - minute , morning 
 afternoon November 7 , 8 , 9 , Lewis Concert - Hall . 
 — — opera week , direct Sir Thomas Beecham , 
 Manchester Beecham Operatic Chorus , 
 collect thousand pound medical charity 

 tion , write operatic class 

 chamber music class contrive second 
 day miniature Beethoven festival , test 
 seven class choose early work . < fair 
 general criticism ensemble - playing 
 condense : trio suave Beethoven 
 quartet quintet impetuous — overlook 
 ' nameless grace ' latent op . ix , \ reversal 

 ve bring , 

 true g er WOrKS 

 playing day small orchestra Crewe 
 Blackpool * ' Coriolan ' Overture , , 
 salient Beethoven , despite handicap | ng 
 ' cue ' ( ) bassoon jf 
 euphonium . Saturday choral singing § 
 Gibbons , Weelkes , Brahms , Elgar , Parry , Bantock 

 Xc . , long process revelation tl 

 Music Scotland 

 EDINBURGH , \t series Edinburgh 
 Reid Orchestra Symphony concert , Mr. Paul Wittgenstein , 
 pianist , physical disability , confine 
 use left arm , introduce new concerted 
 work pianoforte orchestra , ' Parergon ' Strauss 
 ' Sinfonia Domestica , ' write composer , set 
 Concertante Variations Franz Schmidt Scherzo 
 Beethoven F Violin Sonata 

 orchestra 

 distraction parliamentary by-| Sunday , devote programme ' heroic ' music 

 Beethoven , play Reid Orchestra.——At 
 fifth annual series lecture - concert school 
 child , joint wgi Edinburgh 
 Education Authority Messrs. Paterson , direct 

 concert recital 

 GLasGow.—The Glasgow Abstainers ’ Union series 
 subscription concert big step forward upward 
 introduce Glasgow public Chaliapin 
 Olezewska.——Under auspex British Music 
 Society ( Glasgow centre ) Mr. Cyril Scott , assist Miss 
 Amy Samuel singer , recital com 
 Tovey 

 position voice pianoforte , Prof. 
 lecture - recital Beethoven ' Diabelli ' Variations 

 pianoforte.——mr . Adolphe Borschke , announce 
 ' master pianist composer , ' pianoforte recital 
 fail live announcement , 

 Willian 

 Mr. Arthur Williams ( ' cell 
 de Beethoven 

 lect 

 season generally scarcely begin , 
 number recital minor chora ) concert beer 

 . promise , , extremely busy , 
 probably include number item unusual 
 interest . important event celebration 
 fortieth anniversary opening Concertgebouw 
 Amsterdam . occasion concert , 
 Mengelberg , Bruno Walter , Pierre Monteux , , 
 opera - oratorio , ' ( Edipus Rex , ' Igor Stravinsky , 
 conductor . concert begin April i2 
 dutch programme , date April 19 , 22 , 24 , 
 26 , Strauss , Mahler , 
 Beethoven ' Choral ' Symphony . 
 likelihood time Huddersfield 
 Choral society 
 Holland , 

 
 
 concert british musi 

 , book famous composer , 
 brilliant series article brim 
 humour trenchant criticism diverse 

 subject Beethoven barrel - organ 

 portrait 

 119 - 125 , CHARING Cross ROAD , Lonpon , W.C.2 . 4 

 BEETHOVEN 

 Alishers 

 piece 

 BEETHOVEN 

 HuGH BLAIR 

 content 

 Arrival visitor ... aii ... 1 . Hofmann BASIL HARWOOD . 
 Bright Buxom Lasses , ' * Martha " ... Flotow 
 Price | RPENCE , 
 Instrumental Band , , ..   Fercy EB . Fletcher 
 little March , pon ‘ - .. J. D. Davis | ' ssue response t numerous req " r short setting 
 original Op . 6 , 
 March ' ' Egmont " - una Beethoven 
 : , London : NoveLto anp Company , Limited 
 March ' ' St. Polycarp " ' ... F. A. G. Ouseley 
 March ... _ aie — sis . Th . Kullak 
 ry 1 
 March ' ' Tannhauser " ... = e nia Wagner | y \| ac 8 > : ( ' ¢ 
 sch rom " Tanai " Masoug Comus 
 march Bed ... _ Myles B. Foster | 
 March Priests , ' ' Magic Flute " Mozart JOHN MILTON . 
 Soldiers ’ chorus , ' ' ] Trovatore " oe .. Verdi original music 
 _ HENRY LAWES . 
 Toy Soldiers ’ March ... = wi ... Lschaikowsky 

 Incidental Music , dance , & c. , 

