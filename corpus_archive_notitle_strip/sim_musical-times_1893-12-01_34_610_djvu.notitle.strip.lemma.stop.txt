715 

 unquestionably nature susceptible sensuous 
 impression , capable voluptuous enjoy- 
 ment fair gracious art life , ensure 
 advantage musician . olian harp 
 fragrant breath summer air 
 strike sweet harmony . music 
 need awaken touch . nature , 
 , fall worship 
 creator artistic beauty past , 
 instinctively devotee prostrate . 
 saint martyr . Gounod , respect , 
 true law . reverence 
 depart genius extend imperfection . 
 talk Paris liberty 
 Beethoven Ninth Symphony Wagner , 
 french composer write : ' touch work 
 great master . dangerous example daring 
 irreverence . " : " far 
 leave great master imperfection , 
 , impose . " 
 know Gounod artistic constitution , 
 natural thing world 
 Mozart chief god idolatry — 
 composer find sweet 
 noble combination beautiful true . 
 influence admiration necessarily raise 
 voice vagary day : ' 
 infect craze extreme . run 
 strange , excessive , bewildering 
 ( vertigineux ) ; simplicity silliness ; 
 rule timidity , method fetter , al ! disci- 
 pline prison . " write year ago , 
 support theory artist pro- 
 gresse art , Gounod : " 
 condition art fulfil ' Don Giovanni ' ? 
 art ancient modern ? 
 find , exquisite 
 incomparable charm pure music , 
 complete expression human truth , , conse- 
 quently , psychological profundity 
 demand drama ? " Gounod belie 
 nature commit develop- 
 ment present day . far modern pro- 
 gress beautiful 
 charm , scarcely meaning 

 advantage constitution 
 frame sensuous delight thing 
 beauty , find attend con- 
 siderable drawback . case creative artist , 
 drawback , , limited scope . 
 natural law decree 
 gain susceptibility particular im- 
 pression shall lose breadth depth general 
 sympathy . mystic dreamer declare 
 french master , apostle 
 ideal , charge reveal music world 
 . significant 
 wish compose Mass memory Jeanne 
 d’Arc amid architectural splendour , religious 
 historic impressiveness Rheims Cathedral ! 
 look inspiration — motive 
 power set chord sensitive 
 high - strung nature vibration . Beethoven 
 find power depth spirit , 
 spring unceasingly , intermission , 
 like flow mountain stream dependent 
 chance weather . early year , Gounod 
 like man " possess " place con- 
 dition appeal power extreme 
 sensibility . interesting example 
 find Fanny Mendelssohn letter , write 
 Rome Gounod residence papal city . 
 describe young student personality 
 extraordinary expansiveness , find 
 adequate word wish tell 

 influence , happy 
 presence . : ' ' Gounod 
 passionate romantic excess . german 
 music effect bomb burst 
 house . imagine confusion ! " : 
 ' play Sonata C minor Beethoven . 
 Gounod mad enthusiasm , finish 
 cry ' Beethoven blackguard ( folisson ) . ' 
 , friend , judge time 
 bed , bear away . " word folisson , 
 course , use term endearment , 
 french mother boy " ' mon petit chou " 
 mean cabbage , darling 

 drawback possession 
 nature Gounod danger limited expression . 
 form sensuous beauty , exist 
 actual world realm fancy , 
 appeal susceptible nature , 
 creative artist susceptibility abnormally 
 developed stand imminent danger 
 work groove , 
 manner . example find 
 poet , , famous indictment " fleshly 
 school poetry , ' Mr. Robert Buchanan point 
 uniform result case man — 
 Dante Gabriel Rossetti — poet 
 painter . Mr. Buchanan : " distinctively 
 colorist , capability colour 
 speak , guess good , 
 quality poem 
 specially mark , isa great sensitiveness hue 
 tint convey poetic epithet . 
 hand , quality impress casual 
 spectator photograph picture 
 find abundantly verse . 
 thinness transparence design , 
 combination simple grotesque , 
 morbid deviation healthy form life , 
 sense weary , waste exquisite sensuality ; 
 virile , tender , completely 
 sane ; superfluity extreme sensibility , delight 
 affected form , hue , tint , deep - seated 
 indifference agitate force agency . " 
 description apply point 
 work Gounod , indicate phenomenon 
 French Master measure exemplify . 
 follow train thought suggest , 
 music generally unvarying 
 expression construction — 
 distinguish recognise 
 reference title - page . inspire sight 
 beauty , mental conception , Gounod art 
 strain quality . revel 
 luscious harmony , slow moving languorous , 
 deck voluptuous colouring 
 modern orchestra afford ; rich melody 
 sensuous charm — chaplet flower lay 
 foot Venus chaster Diana , 
 christian embodiment feminine purity . 
 music natural outcome nature , 
 Gounod case possible 
 effort constraint fatal success . some- 
 time effort , subject 
 constraint , result 
 happy . rie 

 fact , rumour , remark 

 tue leading feature Musicat TIMES 
 supplement know , 
 widely approve , strictly necessary 
 , regard supplement 
 appear early month , 
 mention master choose subject . 
 Beethoven Mozart turn , 
 Handel receive honour 
 medium convey . illustration 
 numerous great interest , literary 
 matter , edit partly write Mr. Joseph 
 Bennett , hardly fail commend 
 nation Handel place 
 hero , Mr. W.H.Cummings , Mr. A. J. Hipkins , Dr. 
 A. H. Mann , Sir Walter Parratt 
 contribute article special importance 

 tuis generally sober contemporary , 
 Musikalisches Wochenblatt , write apropos Eugen 
 d’Albert opera ' Der Rubin , " lately produce 

 Ir interesting know string quartet 
 Monday Popular Concerts play 
 instrument Antonius 
 Straduarius , Mr. Alfred Gibson having lately pur- 
 chase Messrs. Hart Son Straduarius viola 
 great beauty . instrument 
 possession late Charles Reade , novelist , " 
 purchase late Mr. George 
 Hart , private collection remain upwards 
 year . pass hand 
 eminent collector Paris , subsequently 
 repurchase Messrs. Hart Son . 
 instrument compose quartet 
 " grand period " Straduarius work , date 
 follow : Lady Hallé , 1709 ; Mr. Ries , 1710 ; 
 Mr. Gibson , 1728 ; Signor Piatti , 1720 

 Allgemeine Musik - Zeitung print hitherto 
 unpublished letter Franz Schubert , 
 short , characteristic modesty 
 conscientiousness , striking proof 
 admiration great , feel 
 justified translate . bear date : ' * 
 esteem Herr v. Bartel,—As 
 orchestra send forth world 
 calm conscience , existence 
 piece great master — e.g. , Beethoven : 
 overture ' Prometheus , ' ' Egmont , ' ' Coriolan , ' & c. , 
 & c. , & c. , sincerely beg pardon 
 service occasion . 
 forgive , , hasty thoughtless 
 promise.—yours obediently , Franz SCHUBERT 

 Tue day Liibeck General Anzeiger gravely 
 announce new opera , ' ' ' Der Bajazzo , ' 
 Signor I. Pagliacci , ' shortly perform 
 old Hansa town . credit discover 
 composer , Signor I. Pagliacci , belong , 
 remark , London musical contemporary ; 
 Liibeck scribe certainly " " — 
 " Der Bajazzo " merely german title 
 Leoncavallo famous opera . far distant 

 second day morning programme 
 " Hymn praise " Rossini ' ' Stabat Mater , " 
 need , save solo vocalist 
 Madame Albani , Miss Landi , Mr. Lloyd , Mr. 
 Santley . attendance small 
 . accordance rule Festival , 
 performance place evening 

 morning day bring varied selection , 
 comprise S. S. Wesley anthem ' Wilderness , " 
 Dvorak Symphony G , Schumann " Paradise 
 Peri . " Dvordk work insert late , 
 increase attraction programme public 
 look askance , think , , ' ' Wilderness " 
 hear church , , know 
 think ' Paradise Peri . " rate , 
 booking Concert poor , order 
 audience respectable point number com- 
 mittee oblige scatter ' ' paper ' liberal hand . 
 Wesley anthem ( 
 original organ accompaniment ) tamely , 
 Symphony , capitally play , stir public , 
 success occasion . fine characteristic 
 work ! let weakly believe Wagner 
 Symphony word Beethoven . 9 
 perpend remarkable composition . ' paradise 
 Peri " ? hear , fear , somewhat 
 languid interest , performance gener- 
 ally good soloist — Miss Henson , Miss Landi , 
 Mr. Ben Davies , Mr. Andrew Black — 
 excellent . truth tell , portion Cantata 
 " hang fire " terribly , usual — nay , need 
 , Schumann , great measure prepare 
 libretto , skilful 
 task . evening day devote Wagner 
 programme , act " ' flying 
 Dutchman " number - know piece , orchestral 
 vocal . - discuss music attract large 
 audience , appear highly appreciate excerpt 
 master early opera . singer Miss 
 Palliser , Miss Boucher , Mr. Ben Davies , Mr. Lloyd , Mr. 
 Santley , Mr. Worlock , Miss Marie Gane , local artist 
 good service occasion week 

 proceeding end , morning fourth 
 day , performance ' ' Messiah , " 
 principal soloist engage . append 
 figure attendance Festival : ' ' Samson , " 
 1,307 ; ' * Faust , " 1,421 ; ' ' hymn Praise , " & c. , 1,652 ; 
 ' " ' Paradise Peri , " 1,701 ( count ' ' deadhead " ' ) ; 
 Wagner selection , 1,762 ; ' ' Messiah " — , ' 
 Messiah " head lot . grand total , 7,843 , 
 advance 170 1890 

 second day morning programme 
 " Hymn Praise ' ? Rossini ' ' Stabat Mater , " 
 need , save solo vocalist 
 Madame Albani , Miss Landi , Mr. Lloyd , Mr. 
 Santley . attendance small 
 . accordance rule Festival , 
 performance place evening 

 morning day bring varied selection , 
 comprise S. S. Wesley anthem ' ' Wilderness , " 
 Dvorak Symphony G , Schumann ' Paradise 
 Peri . " Dvordk work insert late , 
 increase attraction programme public 
 look askance , think , , ' ' Wilderness " 
 hear church , , know 
 think " Paradise Peri . " rate , 
 booking Concert poor , order 
 audience respectable point number com- 
 mittee oblige scatter ' ' paper " ' liberal hand . 
 Wesley anthem ( 
 original organ accompaniment ) tamely , 
 Symphony , capitally play , stir public , 
 success occasion . fine characteristic 
 work ! let weakly believe Wagner 
 Symphony word Beethoven . 9 
 perpend remarkable composition . ' paradise 
 Peri ' ? hear , fear , somewhat 
 languid interest , performance _ gener- 
 ally good soloist — Miss Henson , Miss Landi , 
 Mr. Ben Davies , Mr. Andrew Black — 
 excellent . truth tell , portion Cantata 
 " hang fire " terribly , usual — nay , need 
 , Schumann , great measure prepare 
 libretto , skilful 
 task , evening day devote Wagner 
 programme , act ' ' flying 
 Dutchman " number - know piece , orchestral 
 vocal . - discuss music attract large 
 audience , appear highly appreciate excerpt 
 master early opera . singer Miss 
 Palliser , Miss Boucher , Mr. Ben Davies , Mr. Lloyd , Mr. 
 Santley , Mr. Worlock , Miss Marie Gane , local artist 
 good service occasion week 

 proceeding end , morning fourth 
 day , performance ' Messiah , " 
 principal soloist engage . append 
 figure attendance Festival : ' ' Samson , " 
 1,307 ; ' Faust , " 1,421 ; ' hymn Praise , " & c. , 1,652 ; 
 ' " ' Paradise Peri , ' 1,701 ( count ' ' deadhead " ' ) ; 
 Wagner selection , 1,762 ; ' ' Messiah ' — , ' 
 Messiah " head lot . grand total , 7,843 , 
 advance 170 1890 

 Schumann Pianoforte Quartet e flat ( Op . 47 ) . 
 Miss Jessie Hudleston display pleasant well- 
 train soprano voice advantage Goring Thomas 
 ' Summer night , " pretty song , " * voice , " 
 Mr. Hermann Klein . Mozart Quartet e flat ( . 4 ) 
 complete excellent programme 

 little concern Concert 
 Saturday , 4th ult . concerted work Men- 
 delssohn Quartet e flat ( Op . 12 ) Beethoven 
 Pianoforte Trio key ( Op . 70 , : 2 ) . Mr. 
 Borwick pianist , beautiful reading 
 Schumann little piece , ' ' Kinderscenen " ' ( Op . 15 ) . 
 Mr. Hugo Becker , violoncellist occasion , 
 Boccherini old - fashioned Sonata excellent style , 
 Miss Florence Hoskins satisfactory Mr. Santley 
 " ' Ave Maria " Schumann song " noble 

 follow Monday Lady Hallé 
 appearance season warmly greet 
 large audience . magnificent performance 
 Beethoven Quartet E minor ( Op . 59 , . 2 ) , 
 moravian violinist hear Adagio 
 Dvorak Concerto minor ( Op . 53 ) . Mr. Borwick 
 repeat intelligent performance Chopin Sonata 
 b flat minor , Funeral March , Miss Damian 
 vocalist . Concert end Beethoven 
 rarely hear sonata D , pianoforte violoncello 
 ( op . 102 , . 2 

 Concert Saturday , 11th ult . , dismiss 
 equal brevity . Beethoven Quartet F minor 
 ( Op . 95 ) Schubert Pianoforte Trio b flat ( op . 99 ) 
 concerted work . Lady Hallé play fine 
 manner Dr. A. C. Mackenzie piece violin , 
 include popular ' " ' Benedictus , " Mr. Schénberger , 
 pianist occasion , vigorous rendering 
 Schumann sonata G minor ( Op . 22 ) , artist 
 encore . Mr. William Nicholl contribute effect 
 Brahms Lieder song Grieg 
 " ' Reminiscences Mountain Fiord 

 considerable assemblage welcome Mr. Piatti 
 return Concert Monday , 13th ult . 
 notwithstanding advance age power great 
 violoncellist sign deterioration , playing 
 Schubert Quartet D minor , Schumann abused 
 popular Pianoforte Trio D minor ( Op . 63 ) , 
 movement refined elegant Sonata 
 violoncello pianoforte C ( Op . 28 ) 
 unapproachable . Miss Fanny Davies 
 thank selection Chopin Preludes , 
 hear Concerts , 
 excellent interpretation brief 
 difficult little piece . time recall , 
 wisely refuse encore . Miss Amy Sherwin render 
 effect Handel air ' ' Ombra mai fu " 
 Schubert Lieder 

 Saturday , r8th ult . , programme consist , 
 exception Haydn quartet b flat ( op . 64 , . 5 ) , 
 unfamiliar work , vocal instrumental . Miss 
 Fanny daviesrevive Handel ' ' Suite de Pieces ' d minor 
 ( book I. , . 3),.and play quaint music exceedingly 
 ; Mr. Piatti revive Valentini melodious Sonata 
 violoncello e ( op . 8) , render exquisite 
 expression ; Concert conclude Saint - Saéns 
 Pianoforte Quartet b flat ( Op . 41 ) , , like 
 piece - , hear year 
 Mr. Arthur Chappell performance . Miss Liza 
 Lehmann , cordially receive , sing Bishop 
 song ' * simplicity Venus dove ' charming 
 new lyric , ' ' Fleur aimée , " Francis Thomé , 
 pleasing style twice encore 

 Concert notice month place 
 follow Monday , Goldmark Pianoforte 
 Quintet b flat ( Op . 30 ) perform time 
 Concerts . ' elaborate apparently , 
 hearing , laboured work ; theme 
 melodious hear second time 
 dogmatic opinion pass merit . Lady 
 Hallé magnificent rendering Tartini sonata * Il 
 Trillo del Diavolo , " ' hear good 
 Schubert delightful quartet minor ( Op . 29 ) . Mr. 
 Schénberger fairly commendable Beethoven Sonata 

 ( Op . ror ) , Mr. David Bispham receive enthusi- 
 astic applause expressive interpretation 
 Loewe fine song ' ' Archibald Douglas , " accompanist , 
 Mr. Henry Bird , fairly share compliment bestow 
 excellent performance 

 adopt expression common use present 
 day , Mr. Paderewski ' ' beat record " Recital 
 October 31 , St. James Hall , demand place , 
 notwithstanding high charge admission , 
 absolutely unprecedented . musical public 
 mind polish artist infinitely superior 
 pianist time , thoughtful 
 musician scarcely endorse extravagant estimate , 
 fail acknowledge exceptional gift , 
 display Recital fully 
 previous occasion . true , rendering Beet- 
 hoven favourite sonata d minor ( op . 31 , . 2 ) 
 open question ground extreme feminine 
 delicacy , particularly middle movement ; 
 piece Chopin , especially etude minor 
 ( op . 25 , . 11 ) , Mr. Paderewski fairly surpass . 
 magnificent , sense , execution 
 Bach Chromatic Fantasia fugue D minor 
 Schumann sonata f sharp minor ( Op . 11 ) . 
 pen introduce set variation ' Home , 
 sweet home , " ' English suite " shortly 
 publish . trace selection pretty 
 compliment anglo - saxon admirer , variation 
 clever far musicianly Thalberg . 
 sickly air hackneyed endurance 

 curiosity feel earnest lover pianoforte 
 music appearance Miss Thérésa Gérardy , 
 21st ult . , St. James Hall , young lady 
 sister Master Jean Gérardy , extraordinarily gifted 
 young violoncellist regard 
 * prodigy " accomplished artist . Miss Gérardy 
 understand year senior , 
 judge high standpoint , playing , 
 Beethoven Sonata , commonly know " Moon- 
 light , " intelligent expressive 
 average adult performer . especially commend- 
 able Mozart Rondo minor Chopin Scherzo 
 b flat minor — mean test pianist ability . Mr. 
 Edward Arthur , vocalist agreeable light 
 tenor voice , contribute song 

 following afternoon platform St. James 
 Hall occupy Mr. Siloti , russian pianist 
 Moscow , appear London Princes ’ 
 Hall 24 , year . programme present 
 occasion peculiar , great 
 master usually associate pianoforte music 
 represent . Bach celebrated - pre- 
 lude fugue movement Handel 
 play unexceptionable taste . Chopin Sonata 
 B fiat minor , Funeral March , Mr. Siloticommenced 
 , , unexplained reason , March , 
 middle section movement , sur- 
 prisingly rapid pace , consequently 
 ineffective . noteworthy piece rest 
 programme historical series variation write 
 Liszt , Thalberg , Pixis , Herz , Chopin 
 popular melody Bellini ' Puritani . " 
 example term virtuoso music 
 play brilliancy , , excessive force . 
 Mr. Siloti second Recital , fix 
 27th ult . , speak month 

 PIANOFORTE Quartet student , Mr. H. Walford 
 davy , open Concerts new term . 
 evidently early work highly creditable 
 String Quartet play term , theme 

 interesting reminiscence frequent 
 — sure sign youth ! play com- 
 poser , Miss Jessie Grimson , Mr. Charles Jacoby , Miss 
 Alice Elieson . programme include Beethoven 
 Septet . Orchestral Concert , rst ult . , bring 
 quasi - novelty shape Mozart Clarinet Concerto 
 , melodious work short movement . 
 Mr. Charles Draper , play solo , 
 capable performer ; tone execution 
 excellent . Miss Marie Motto meritorious rendering 
 Dvorak long Violin Romance , Mr. Albert 
 Archdeacon sing air ' ' Don Giovanni , " 
 fine voice , , hear good . 
 Schumann D minor Symphony , ' " joy , " 
 play orchestra , especially splendid 
 string , utmost gusto . inspiriting 
 enthusiastic young people performance 
 imagine 

 Concert notice month place 
 9th ult . programme contain ' " Kreutzer " 
 Sonata , play capital style Mr. Charles Jacoby 
 Miss Annie Fry ; Rubinstein polonaise ' ' Le Bal " 
 ( Op . 14 ) , Mr. Francis Bohr promise 
 pianist ; Max Bruch everlasting romance , 
 tone considerable ' " , " little 
 " romance , " Mr. William Ackroyd ; Schumann 
 String quartet minor ( op . 41 , . 1 ) , carefully lead 
 Miss Lilian Wright , Miss Kathleen Thomas , Mr. 
 Ackroyd , Mr. Paul Ludwig associate . 
 singer Miss Evelina Benzabatti , attempt 
 florid song ' " ' thy mighty power , " curiously 
 old - fashioned double bass obbligato Vincent Novello ; 
 Mr. Norman Jones , , beautiful song Jensen , 
 prove tenor average excellence 

 MR . COENEN concert 

 admirable programme Chamber music 
 forward Mr. Coenen Concert Brighton 
 Pavilion 15th ult . include sonata 
 pianoforte violin , Grieg ( Op . 45 ) Beethoven 
 " Kreutzer , " Mr. Coenen associate 
 Mr. Johannes Wolff . , course , 
 artistically play , attempt 
 obtain repetition single move- 
 ment ; encore , , firmly wisely 
 refuse evening , result 
 Concert finish reasonable time 
 satiate . gentleman play solo _ 
 respective instrument skill _ taste 
 famous , charming artist , Miss Liza 
 Lehmann , contribute song evident delight 
 hearer 

 WIIRA 

 Italian Opera perform night ( 6th-11th 
 ult . ) , , exception opening performance , 
 Gluck ' ' Orpheus , " excellent business . 
 round Mascagni - Leoncavallo opera 
 , vary excellent presentation ' Faust , " 
 " Lohengrin , " ' ' Carmen , " ' ' ' Les Huguenots . " 
 sister Ravogli greatly distinguish 

 party young instrumentalist , bear device 
 Birmingham String Quartet , Concert 
 7th ult . , Masonic Hall . leader Mr. Henley , 
 skilful playing gain good 
 local reputation ; undoubtedly " rise " artist . 
 principal piece occasion Beethoven Quartet 
 F ( op . 18 , . 1 ) , B. Godard Quartet ( op . 136 ) , 
 suite pianoforte violin — set short , 
 interesting movement — parisian composer , 
 Bernard , Dr. Winn pianist , Miss Laura 
 Taylor , vocalist , agreeably vary entertainment 
 song Gounod Weber 

 Miss Marie Fromm , assist Mr. Schiever ( violin 

 QJsAhIAA 

 tone violin singularly yeoa , 
 spirited effort overpower 
 reason undue energy trombone desk . ample 
 provision rehearsal , inasmuch 
 new - comer work fortnight 
 advance public appearance . 
 facility tell agreeable tale Beethoven C minor 
 Symphony , Mendelssohn Violin Concerto ( 
 Miss Frida Scotta welcome début ) , 
 " Oberon " Overture . performance 
 familiar work perfect acquaintance 
 text , remain Mr. Henschel , 
 reception nature ovation , 
 firm control force 

 evening 4th ult . Popular Concert 
 series place presence large audience . 
 programme include Mendelssohn ' Scotch " 
 Symphony — prime favourite Glasgow folk — 
 stirring performance Wagner " Rienzi " Overture , 
 good account ' ' Euryanthe " Prelude 
 anybody desire . vocalist Madame 
 Emily Squire 

 gth ult . Company Chamber Concert 
 . poorly attend , , truth tell , 
 Mr. Sons coadjutor good 
 Schubert C major Quintet . Classical Concert , 
 13th ult . , memorable Mr. Paderewski 
 appearance Scotland orchestra . exposition 
 pianoforte Schumann minor Concerto 
 overflowing point marked excellence , 
 close work Mr. Henschel polish artist 
 win enthusiastic favour audience . primary 
 interest centre , , new " Fantaisie " specially 
 compose Norwich Festival . enormous 
 difficulty stimulating , great work , 
 deftly overcome band soloist , 
 outburst enthusiasm greet concern perform- 
 ance . second Popular Concert , 18th ult . , 
 Beethoven C minor Symphony hear , 
 programme include Weber Pianoforte Con- 
 certo ( Op . 32 ) Mendelssohn " Ruy Blas " Overture . 
 pianist evening Miss Pauline Hofmann . 
 27th ult . Mr. Sarasate announce appear 
 Lalo ' Symphonie Espagnole " Saint - Saéns 
 ' ' Rondo Capriccioso " violin orchestra . Sym- 
 phony Brahms . 2 , D 

 Glasgow Choral Union season proper commence 
 14th inst . , Mr. Manns renew acquaintance 
 Glasgow friend . choice series 
 programme prepare , Mr. Frederic Lamond 
 pianist . write , know 
 popular Sydenham chef bring North band 
 member hold foremost rank 
 profession 

 speak ; , , clearly 
 potently disturbing influence 

 owe Bristol Festival subscription Con- 
 cert commence somewhat later usual , 
 worth wait week reward 
 rendering Mendelssohn great ' ' Midsummer Night 
 Dream " Overture Beethoven Seventh Symphony 
 balance admirably associated 
 Sir Charles Ha!lé orchestra . evidence 
 rendering Stanford " irish " Sym- 
 phony second Concert , Sullivan ' ' Macbeth " 
 overture fourth , small work scatter 
 arrange programme , Cambridge pro- 
 fessor conduct work , admirably score , 
 replete colouring suggest title . 
 soloist month , Signor 
 Sarasate — Lalo ' Symphonie Espagnole " 
 interesting " Carmen " Fantaisie ; 
 hail - appearance Manchester pianist , 
 proud — Mr. Frederic Dawson , un- 
 surpassed executive skill . Lady Hallé advent 
 delay till fourth concert — long 
 try patience admirer — , music - lover . 
 welcome Miss Palliser , Mr. Iver McKay , 
 Mr. Andrew Black Recital " Flying Dutch- 
 man " 16th ult . , warmly applaud Miss Emma 
 Juch open Concert 2nd ult . , admire 
 finish method — force 
 fair power — purity voice ; listen 
 pleasure warbling Madame Amy Sherwin . 
 delight excellence band 
 evening , congratulate , 
 Wagner opera , Mr. Wilson care choir , 
 promise afford admirable choral perform- 
 ance winter 

 Saturday evening Concerts Mr. Barrett 
 Mr. Cross successful swing ; , 
 student , Organ Recitals Mr. Pyne , 
 Town Hall , extremely attractive . Philharmonic 
 Choir , Mr. Lane , ' ' Judas Macca- 
 bus ' , admirably guide baton Mr. Goossens , 
 " Rustic Chivalry " Mascagni , intent 
 Wednesday evening alluring cheerful 

 Nottingham choralist , express hope future 
 contest arrangement prove attractive 
 local choir , consequently stimulate progress choral 
 singing 

 Mr. Allen second Classical Concert , 22nd ult . , 
 poorly attend , notwithstanding fact . 
 programme include Brahms Clarinet Quintet , 
 Beethoven Septet , Haydn ' ' Emperor " Quartet , 
 play artist Messrs. Willy Hess , C. Rawdon 
 Briggs , Spielmann , Carl Fuchs , Hofmann , Egerton , 
 | Lalande , Paersch . true , great star , 
 lamentable artistic treat 
 poor appreciation town professedly musical 

 400 scholar sing appropriate 

 month exceptionally busy 
 music . unusually heavy list Concerts 
 kind supplement Opera Theatres 

 amidst counter - attraction gratifying 
 find Herr Ellenberger Concert , October 26 , 
 support . audience reward 
 excellent rendering Brahms C minor Quartet , 
 Beethoven Quartet F major , Miss Ellenberger 
 solo Schumann Pianoforte Toccata C major , 
 Herr Ellenberger Andante Joachim Violin 
 Concerto G minor . Herr Ellenberger deserve high 
 praise care taste devote selection 
 preparation programme 

 Philharmonic Choir commence season 
 October 31 miscellaneous Concert ; share 
 programme modest contribution 
 - song : Benedict ' ' hunting song , " Pinsuti ' ' 
 sea hath pearl , " Caldicott ' Little Jack Horner , " 
 Berger ' ' lovely night , " admirably sing , 
 direction Mr. F , Marshall - Ward 

 Bradford Subscription Concerts afford 
 excellent example Concert . 
 programme include Brahms Symphony 

 Mendelssohn incidental music ' Midsummer 
 Night Dream , ' ' ' Egmont " Overture . 
 Brahms Symphony hear 
 Concerts , unfortunate . 
 formance , Sir Charles Hallé band , somewhat 
 perfunctory . Mendelssohn , hand , 
 justice . Mr. Sarasate 
 violinist arid introduce Lalo ' ' Symphonie Espagnole . " 
 vocalist Miss Landi replace Madame Vasquez , pre . 
 vent illness appear . roth ult . 
 Bradford Festival Choral Society perform Beethoven ’ 
 Mass C Mendelssohn " Israel Egypt 
 come " " Walpurgis Night . " 
 performance achieve mediocrity . 

 band , consist local player , efficient ; 

 new - act music - drama , ' ' Erlést ’' ( release ) , 
 Franz Curti , produce time , great 
 success , Mannheim , 7th ult . libretto 
 music write influence popular 
 specimen " ' young Italy " verismo 

 Beethoven Concert Kiel , 4th ult . , 
 Herr C. Borchers play Bonn master 
 Concertos — viz . , , fourth , fifth . slight task 

 Smetana opera " ' sell bride " 
 Frankfort - - Maine Opera - house , Magde- 
 burg , place 

 new italian opera " Il Castello di Brivio , " 
 Antonio Fissone , " ! Trionfo d’amore , ’' Luigi 
 Minuto . recently produce Asti Alba 
 respectively , success 

 Beethoven ' " Fidelio " appeal taste ot 
 Spaniards ; , recent performance 
 marvellous work Madrid leave audience cold . 
 look plenty local colour opera 
 deal spanish subject , disappoint 

 Concert Copenhagen Philhar- 
 monic Society , October 28 , composition Edvard 
 Grieg perform , conductorship 
 popular norwegian composer . Concert open 
 movement new Orchestral Suite 
 music Bjérnsen ' ' Sigurd Jorsalfar . " programme 
 comprise " Bergliot , ’' declamation orchestral 
 accompaniment ( op . 42 ) , Pianoforte Concerto 
 minor , play Madame Teresa Carrefio - d’Albert , 
 melody string ( op . 34 ) , song , 
 sing swedish vocalist , Mrs , Gulbranson , 
 pianoforte accompaniment composer . immense 
 audience include member royal family 
 élite musical world Copenhagen 

 BaSINGSTOKE.—An operatic performance Drill 
 Hall , 16th ult . , - act operetta present 
 alarge audience . Amateur , Miss Blanche 
 Powell Mr. Arthur Barlow sing act efficiently . 
 follow bright effective operetta , Feminine Strategy , 
 write Catherine Adams , compose F. G. Hollis . 
 Miss Jessie Moore Mr. Arthur Barlow acquit 
 admirably . performance conclude Girton Girl 
 Milkmaid , write Catherine Adams , compose Alfred J. 
 Caldicott . attractive number , ' ' come , bonny 
 cow , " charming pastoral air , encore . Miss Blanche Powell 
 appear advantage Girton Girl , Miss Jessie Moore 
 vivacious Milkmaid 

 CuristcHURCH ( N.Z.)—Mr . Wallace series Chamber Concerts 
 prove artistic financial success . assistance 
 Miss Beath , Miss Rich , Mr. Loughnan , quartet 
 Brahms , Mozart , Beethoven . pianoforte solo local player , 
 violin solo leader , Schubert " Trout " Quintet , 
 song lead Christchurch singer , whet appetite 
 subscriber , hope season fresh 
 series similar Concerts . Naaman , Elijah , Messiah 
 year Musical Society Motet Society 

 DemerARA.—An Organ Recital St. George Cathedral 
 October 17 , patronage Excellency Governor 
 Lees , Mr. Nusiom , Organist choir Director . 
 organ solo comprise Mozart " Jupiter " Symphony , Beethoven 
 fh Funébre , Fantasia Tours , " Quis est homo " 
 Stabat Mater , March Organist . Mr. Hemery violon- 
 cello solo , " heart " Lieder Mendelssohn , 
 faultlessly play . Santley " Veni , Sanete Spiritus , " sing 
 jhe Cathedral choir ( Mr. Van Elden solo ) , trio , 
 " protect come night ' ( Curschmann ) , 
 tender , " ' o rest Lord " " land bring 
 h frog , " sing respectively Masters Charles Lewis George 
 0 , member choir 

 Harrow - - - H1Lt.—Mr . H. W. Whatmoor , Assistant Piano- 
 forte Teacher Harrow School local Examiner Royal 
 College Music , Pianoforte Recital Public Hall 
 October 24 . attractive programme , play , enjoy 
 byalarge audience . Mrs. Trust sing customary taste 

 dedicate ( permission ) right Rev. Lord Bishop Ripon . 
 EW SONG , Jean Cuartes , LORD 
 SHALL COMFORT ZION , chorus ad /ib . , suitable 
 Church Choirs . price 2 . net ; post- ' free , 2s . 1d . 
 London New York : N6éveELLo , Ewer Co 

 COMING MUSICIAN.—Wind Instrument 
 Solos — Beethoven Homes.—See December Musical Opinion 
 Music Trade Review , 200 col . musical literature Adver 

 London New York : Nove . Lto , Ewer Co 

 ALFRED MOFFAT 

 content : 
 1 . Gavotte oe pr ie oe + . Gluck , 
 2 . Preludio Giga ot ¥ a8 Corelli . 
 3 . song word - - .. Mendelssohn . 
 4 . prayer ve ad ee ae ms Py Beethoven . 
 5 . Gavotta se te ov + e Leclair . 
 6 . Corrente ae ss ae ma oe - » Corelli , 
 7 . Lento .. 7 ry ae oe os Handel , 
 8 . Arioso ve ab ae oe .. Siprutini , 
 g. Bourrée ap $ 3 oe oe . Handel . 
 10 , song oe ae ee o8 . . » Schubert . 
 11 . Lento . af os ae 4 = ve * Gluck , 
 12 . Menuetto £ 3 se ah sa eo = « » Haydn 

 price shilling 

