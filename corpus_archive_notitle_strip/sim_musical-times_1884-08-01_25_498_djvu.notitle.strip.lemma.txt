term — one on the Pianoforte and Singing , and one in Harmony and 

 thorough - bass . applicant must state what salary they would accept . 
 Henry Brotherton , Beethoven House , Bishop Auckland 

 young man with address , desire a situa 

 Tue honour pay to live composer , instance 
 of which be now record in almost every number 
 of our journal , afford convincing proof of the 
 grow appreciation of art and artist throughout 
 the civilised world . dut it would indeed be strange 
 if this desire to pay a tribute to the genius of the 
 representative creative musician who be still 
 amongst we be not to be extend to the memory 
 of those who be pass away . pilgrim to the 
 grave of decease composer have indeed be 
 surprise at find that many of those artist who 
 have raise music to its present high standard be 
 bury with only the common inscription to mark 

 the place , and we all know that Mozart ’s remain be 
 inter in a pauper ’s grave , the exact site of which 
 be forget when the monument by Hans Gasser 
 be erect to the composer ’s memory on the anni- 
 versary of his death in 1859 . by a telegram from 
 Vienna , publish in the Daily Telegraph , we be glad 
 tofind that the municipality of that city have at length 
 resolve to show its respect to two , at least , of the 
 great musical hero of Germany . the communi- 
 cation , which be date July 25 , say : ' the remain 
 of Beethoven and Schubert be to be transfer from 
 the burial - ground of Waehring , one of the suburb of 
 Vienna , to the Central Friedhof , a large new cemetery 
 just outside the town , where ground have be specially 
 reserve for the interment of great man . ' the old 
 graveyard of Waehring have be close for the last 
 seven year , and as I see it this morning it look 

 dirty and neglected beyond description . the tomb 
 of Beethoven be erect four year ago ; until then 
 | his remain have be leave where they be deposit 
 | after his death , a common stone slab alone indicate 
 | the spot where they lie . this slab have be replace 
 | by something well , and when the change be make 
 | his ashe be put into a metal coffin . the grave be 
 now surround by a lowiron railing , and at the foot 
 |}stand a stone slab , pyramid - shape , bear for 
 | all inscription the name of Beetheven in large gilt 
 jletter . it be , however , still of modest appearance , 
 and no stranger would think of look there for the 
 | burial - place of Ludwig Van Beethoven . the tomb 
 |of Schubert be more pretentious , but , if possible , less 
 | imposing , and altogether unworthy of the great genius 
 | whose remain lie beneath . a bronze bust , reproduce 
 | the feature of the immortal tone - poet , be place 
 at the upper extremity ; but though we know Schubert 
 be not of prepossessing appearance , yet the metallic 
 | work of art I see this morning lack that expression 
 |so admirably render in a portrait take from life , 
 | which hang , if I mistake not , in the reception hall of 

 the Vienna Conservatoire . on the stone pediment 
 behind the bust be the follow inscription , * musical 
 art have bury here a rich possession , but still bright 
 |hopes . ' surely the memory of Franz Schubert might 
 |have inspire a noble epitaph than that . when 
 | the transfer to the Central Friedhof shall have be 
 | effect , a monument will be provide for the tomb 
 of Beethoven by the Society of the Conservatoire , 
 { and another for that of Schubert by the Vienna 
 Maennergesangverein , or Men ’s Choral Union . " all 
 | art - lover must , we be certain , be deeply interested 
 in this news , and will heartily agree with the sug- 
 gestion of the writer of the telegram , that the cere- 
 mony which will accompany these proceeding should 
 include a great musical Festival 

 reaper of Tue Musrcat Times have frequent 
 opportunity of becoming acquaint with the prin- 
 cipal musical event occur even at the antipode , 
 and must be gratify to find what rapid stride the 
 art be make in our distant colony . of the progress 
 of sacred music , however , as show in the Cathedral 
 services in the capital of South Australia , New Zea- 
 land , Tasmania , New South Wales , and Victoria we 
 vere but imperfectly acquaint until peruse , in 
 our contemporary 7/:e Globe , an article from a visitor 
 to Australia , who evidently take the deep interest 
 in the subject . he inform we that in Adelaide the 
 Cathedral organ be an exceptionally fine one , and that 
 the organist and choirmaster , Mr. Boult , a talented 
 amateur , have during the last few year raise the 
 musical portion of the service to a state of great per- 
 fection , " fairly place it in the front rank with some 
 of our most noted Cathedrals at home . " there be of 
 course much difficulty in secure the service of 

 the valuable co - operation of Madame Albani be again 
 secure in the performance of ' ' Der Fliegende Hollander , " 
 on June 20 . the embodiment of Senta by the canadian 
 artist be too well know to need description , but it may 
 be say that her physical effort in the part would render 
 it unwise for she to repeat it at frequent interval . artistic 
 absorption in a rvéle be an excellent quality , but in the 
 case of Wagner 's heroine the strain on the voice and the 
 nervous system must necessarily be exhausting . Herr 
 Reichmann give a praiseworthy impersonation of the 
 Dutchman , his acting be impressive and dignified with- 
 out be too lugubrious . the other member of the cast 
 do fairly well , but the scenic arrangement be not of the 
 good 

 a large audience attend the only performance of 
 Fidelio , " on June 25 , prove that Beethoven ’s Opera be 
 a power with the musical public even without the attrac- 
 tion of a celebrated prima donna as Leonora . Frau Luger 
 give an earnest , conscientious , and highly intelligent 
 reading of the character , fully satisfy all reasonable re- 
 quirement , though she could not compare with Marianne 
 Brandt , the fine Leonora we have witness in London 
 of late year . Fraulein Kalmann , as Marzelline , and Herr 
 Schrédter as Facquino be efficient , and Herr Oberlinder 
 as Florestan be tolerable , but Herr Reichmann as Pizarro 
 disappoint expectation . contrary to usual custom , 
 only one overture be play — namely , the great of the 
 four , know as Leonora No . 3 

 after several indifferent performance the rendering of 
 " Tristan und Isolde , " on the 2nd ult . , come as a pleasant 
 surprise , and , on the whole , it may be pronounce superior 
 to that at Drury Lane two year ago . Fraulein Lilli 

 three year ago , at the National Exhibition hold in 
 Milan , it may be say that practically music be completely 
 neglect . ' here be , it be true , a poor collection of 
 instrument of our own make exhibit , but no thought 
 be give either to operatic or concert music , nor be there 
 any performance of new composition . this year at Turin , 
 not only be the splendid national exhibition a perfect 
 success in all industrial department , but to music have 
 be assign the post and importance to which it have a 
 right in such an assembly of all the force of the country . 
 a special committee be appoint which have attend to 
 the most important point of provide performance of 
 musical composition , for which the municipality of 
 Turin subscribe an important sum . at Milan there 
 be a gallery for the display of instrument but no hall 
 for performance , and the Orchestral Society of La Scala , 
 vhich give a few Concerts at its own risk , have shortly to 
 abandon they as the receipt do not cover the expense . 
 the Turin committee begin by build a circular hall , 
 which for elegance , architectural grace , richness of orna- 
 mentation and fitting , be one of the fine and be also one of 
 the most frequented part of the Exhibition . a commission 
 be then form to organise all musical entertainment , 
 the principal be the performance of opera at the Teatro 
 Regio , and orchestral Concerts in the hall of the Exhibi- 
 tion . the operatic season be divide into two period — 
 one for the opening and the other for the closing of the 
 Exhibition , the first last through the month of May 
 vith two opera of the old style — ' ' Favorita " and 
 " Puritani’’—by artist of high repute , an excellent 
 orchestra lead by Faccio , and a fine mise ex scéne . the 
 singer be mostly know in London — Gayarre , tenor ; 
 Pasqua and Repeto Trisolini , prime donne ; Battistini , 
 baritone , and Silvestri , bass . these distinguished vocalist 
 perform Donizetti 's and Bellini ’s music in a remarkable 
 manner , especially ' ' I Puritani , ’' in which I first hear 
 Madame Repeto , whose lovely , sympathetic voice , 
 unite to an excellent method of singing , render she 
 worthy of the name of " Diva , " so often adopt by 
 other of inferior merit . the second season , which will 
 take place during September and October , will be a more 
 important one than the first , as there will be ballet 
 besides the opera , and the great attraction of a new Opera 

 Tsaura di Provenza , " compose expressly by Luigi 
 Mancinelli , the celebrated leader of the orchestra and 
 director of the Liceo Musicale of Bologna , and one of the 
 most ardent apostle of Richard Wagner in Italy . for 
 this Opera the service of the most eminent singer 
 have be secure , amongst whom be the two 
 lead star — Tamagno and Pantaleoni . theatrical 
 music once perfectly organise at the Turin Exhibi- 
 tion , Concerts have to be think of , and their success 
 surpass all hope , splendid performance having be 
 already give by the orchestra of Turin , Milan , Naples , 
 and Bologna , to be follow by that of Rome . a noble 
 competition and exemplary zeal on the part of the different 
 director in form programme of serious music , really 
 classical and symphonic , meet with constant patronage 
 from a large number of people , who seem to understand 
 the beauty of music which have remain too long unknown 
 or neglected . and even calumniate in Italy as be too 
 obscure , difficult , and ineffably wearisome . the ' Turin 
 orchestra , perform every Thursday in the large hallof the 
 Exhibition , be the same that play at the theatre , and Faccio 
 have be engage expressly to lead it during the whole 
 time of the Exhibition , that be , from April to November . 
 the other celebrated italian orchestra go expressly to 
 Turin to prove their merit , and therefore present them- 
 self with choose and well study programme . the 
 first to appear be the orchestra of La Scala at Milan , lead 
 by Faccio . it give three splendid Concerts with immense 
 success . it number 130 player . the good be the 
 string , remarkable for their precision , and more yet for 
 a certain communicative fire which exercise on the 
 italian public an irresistible charm . this quality , how- 
 ever , interfere with the true style of interpretation , and 
 in the choice of music too great a share be allot to 
 effect . for this reason the orchestra of La Scala have 
 never yet perform an entire Symphony by Beethoven , 
 but only the most effective number ; for instance , the 
 Scherzo of the ninth , marvellously execute be it own 

 a truly serious , classical , elevated orchestra be that 
 bring from Naples by Giuseppe Martucci . the complete 
 and clamorous success it obtain be quite a revelation . 
 Martucci be an extraordinary artist , who possess to an 
 eminent degree the three quality of pianist , composer , 
 and leader , excel in all . he create the neapolitan 
 orchestra , and in a very few year bring it to a perfection 
 that place it on a rank with the good foreign one . in 
 Germany , where some of his composition have be 
 publish , Martucci be in high repute . when lead the 
 orchestra he be most happy in the interpretation of classical 
 author , but more particularly in the work of Mozart , 
 Beethoven , and of symphonic fragment by Wagner . at 
 Turin he excite the great admiration by his perfect 
 rendering of Mozart ’s Symphony in G minor , of that in C 
 minor by Beethoven , and create decide enthusiasm with 
 the marvellous " Wald weben , " in Wagner ’s " Siegfried . " 
 Martucci be oblige to repeat this last piece in each of 
 the three Concerts he give . I hear the ' ' Niebelungen " ' 
 at Bayreuth , and I can certify that Martucci in this piece 
 be in no way inferior to Hans Richter , who have the advan- 
 tage of direct it under the immediate inspiration of its 
 author . to give an idea of the serious way in which be 
 make out the programme of the neapolitan orchestra , 
 here be that of the last Concert give at Turin:—1 . Schu- 
 mann , overture ' ' Genoveva " ; 2 . Beethoven , pastoral 
 symphony ; 3 . Boccherini , ' " Siciliana " ' ; 4 . Scarlatti , 
 Allegro from the first Sonata ; 5 . Haydn , Minuetto ; 
 6 . Berlioz , ' * ' Danse des Sylphes " and Marche Hongroise ; 
 7 . Wagner , overture ' ' Tannhauser . " this programme 
 contain a fine variety of style , and Martucci find mean 
 to make italian music shine in it , without the help of dance 
 music or noisy overture . Scarlatti ’s Allegro be a true gem , 
 in its primitive instrumental simplicity , so quaintly con- 
 traste with Wagner ’s polyphony . in a second letter I 
 will treat of the Bologna Orchestra , and of the exhibition 
 of instrument , not a great credit to our national industry 

 F. Fivippi 

 471 

 tion on a Beethoven Theme , ’' compose by Saint - S 

 to distance all competitor in this branch of music 

 by Joseph 

 tue idea of include biography of eminent musical 
 composer amongst the * Primers ’ ? issue by Messrs , 
 Novello be an exceedingly happy one ; for , as it tell usin 
 the Prospectus of the Series , * * knowledge of what aman 
 | be , help the understanding of what he do . " it be also 
 good that the execution of this task have be entrust to 
 Mr. Joseph Bennett , not only because he be eminently 
 fit for it , both in a literary and artistic sense , but 
 | because he be not likely to allow the biography , even of 
 his favourite composer , to degenerate into mere speti- 
 ; men of rhapsodical hero - worship . perhaps no man have 
 | be more subject to this treatment than Frederic Chopin , 
 | his * Life ’' by Liszt be , as Mr. Bennett truly observe , 
 |an attempt to put the composer before we ' " as a psycho- 
 | logical phenomenon . " there be certainly very much in 
 the poetical and sensitive temperament of Chopin to 
 favour this idea ; but fact be stubborn thing . and unfor- 
 tunately many of Liszt ’s assertion in support of his theory 
 be directly contradict by Karasowski in his well - know 
 Biography of this artist , a book frequently quote from in 
 the work before we . no music ever more perfectly reflect 
 the individuality of its composer than that of Chopin , and 
 we can imagine that all real lover of his work will like 
 to become acquaint with his inner character . Mr. 
 | Bennett 's book effectually supply this want . it be cer- 
 tainly not a rhapsody ; but in proof that the author 
 deeply sympathise with his subject we may quote his 
 conclude word . ' ' Chopin , " ' he say , " be no Beethoven , 
 to scale the high height , and sound the deep depth 
 of music . he labour within a small field , but he show 
 what infinite loveliness and charm may be find in the 
 minute thing of art as well as of nature 

 Hector Berlioz be a name now familiar to we all , and yet 
 only a few year ago his composition be comparatively 
 unknown . we have all hear something of a mad com- 
 poser who write orchestral music which could not be 
 understand , and there be not many who care to inquire 
 whether this be really true ; but we have lately unearth 
 these treasure , and now begin to wonder how they could 
 have so long remain bury . asa real exponent of the 
 ' ' music of the future ' ? he should retain an exalted post 
 tion in the history of the art , despite the more modern 
 disciple of the school ; and in Mr. Bennett 's biography of 
 this remarkable , but eccentric , artist , reader will plainly see 
 a foreshadowing of that indomitable spirit which seem to 
 culminate in the person of the great musical reformer , 
 Wagner . unlike he , however , he do not live to see his 

 Musik Zeitung , of Berlin . 
 o 

 Walkiire , " all three , as will be see , novelty only at 
 the particular institution in question . on the other hand 
 the catholic , and to some extent commendable impartiality , 
 characteristic of the german Hof - Theater generally , have 
 be maintain at Berlin , in the production of a variety of 
 operatic work , irrespective of nationality , such as Bizet ’s 
 " Carmen " ( sixteen time ) , Gounod ’s ' Faust " and 
 " Romeo and Juliet , " Thomas 's ' Mignon , " Verdi ’s 
 " Aida " ; together with a multitude of other work of 
 various style and age which , however interesting and 
 instructive to the musical student , would seem to be alto- 
 gether beyond the reach of , say , a London impresario . 
 thus Wagner be represent by forty - six performance , 
 Mozart by thirty - one , Lortzing by twenty - nine , Bizet by 
 sixteen , Meyerbeer by fifteen , Weber by thirteen , Verdi 
 by twelve , Gounod and Auber by eleven , Beethoven ( with 
 his one opera ) by eight , Gluck by seven , and so on . 
 there be some complaint be make as regard the ad- 
 ministration of this particular state - subvention institu- 
 tion in Germany , but , at any rate , no fault can be find 
 with it in respect to the cosmopolitan character of its 
 repe rtoire 

 the first novelty to be produce during next season at 
 the Berlin Opera will be an operatic work entitle ' Hero , " 
 by Herr Ernst Frank , a composer who have already acquire 
 some reputation by his able completion of the score of 
 Hermann Goetz ’s posthumous Opera ' Francesca da 
 Rimini 

 the progrz 

 Beethoven ' 
 prelude 

 i | all of wh 

 J. F. BRIDGE , anp JAMES HIGGS 

 no.1 . Slow Movement ( Quintet ) . ie eee UAT . 
 Mus . Doc . , Oxon . Mus . Bac . , Oxon . | Minuet ( 12 Minuets diccs pa ... Beethoven . 
 no.2 , Andante ( Pianoforte Sonata , Op . " 7 a ... Schubert . 
 Largo ... = ss pe ... Handel . 
 book i — eight SHORT prelude and fugue . 2s . € d. as pant the hart ss ase ve eee : Spohr 
 — PR 3 , fugue y s. od , | no 3 + Agnus Dei he sek aset SROBERT , 
 pee : Ce innate fugue , and TRIO ... _ ... 3s . 0d . ° overture , " AcisandiGalatea " ...   <   s. Handel , 
 » IIL — fantasia , prelude , and fugue ... 3s . od . | Albumblatter , no.1 ... « . Schumann 

 London : NoveELio , Ewer and Co 

 2 . Clementi . Rondo in C 

 s » 3- Beethoven . romance 

 4 . Mozart . Kom lieber Mai 

 5 . Clementi . Allegro in G 

 6 . Beethoven . Rondino 

 7 . Callcott . forgive , bl shade 

 dulce domum . S.A.T.B. the Lord be mig aeiliieg ee S.A.T.B. G. A. " Osborne 2d 

 z. 
 2 . downamong the dead man . S.A.T.B. Te Deum in as Jackson 2d , 
 3 . the girl I ’ve leave behind I . ' s. A.T.B. Te DeuminF .. , ane sae ae vas és Nares 24 , 
 4 . British Grenadiers . .a . t.0 . « Charity ( La Carita ) , s.s.s .... an ove " Rossini 4d , 
 5 . long live E ngland ’s future ou len . s. A.T.B. Dr , Rimbault 2d . | § 2 . Cordelia , A.T.T.B. ne ae ais iS G. A. Osborne 4d , 
 6 . my task I be end ( Song an d Chorus ) . A.T.B.B. os . e Balfe 4d . | 53 . I know . S.A.T.B. i Walter Hay ad , 
 7 . thus spake one summer 's day . S.A.T.B. oe we ase Abt 2d . / 54 . Chorus of Handmaidens ( from " Fridolin " ) A. Randegger ad , 
 8 . ga — . te fom as = + . Gounod on 55 + mi 1e heey sid oe ee pe aco de — Rogers 4d . 
 i ene om " Faust " or ses es d. ! 56 . the Red - Cross Knight Jan die ] 
 a sages ey og S. rs ee : sve ... Brinley Richards 4d . | 7 . the Chough and Crow e a ) Sir H. R. — . 
 a1 . maiden , never go a - woo . " s.s.7.7.b. Sir G. A. Macfarren 2d.|/ 58 . the " Carnovale " oe Max wee Rossini 4 
 12 . Faggot- -binders ’ chorus see ase Gounod 4d./59 . softly fall the moonlight ove ove » .. Edmund Rogers 4d , 
 13 . Sylvan hour ( for six female voice ) Joseph Robinson éd . 60 . air by Himmel ... see pee wie Henry Leslie 2d . 
 14 . the Gipsy Chorus ... ove eve wes .Balfe 4d.| 61 , Offertory sentence ... ove " 6 E. Sauerbrey 4d . 
 15 . Ave Maria ee owa « gs « . Arcadelt 1d . | 62 . the Resurrection C. Villiers Stanford 6d , 
 16 . Hark ! the herald angel sing . S.A.T.Bs ... pg cmon xd . 63 . a dg Peal Patriotic Song 22 i. i ron & W.M. Lutz 4d . 
 17 . England yet ( Solo and Chorus ) . § .A.T.B. ir J. Benedict 2d . | 64 . e Menof Wales ... - Brinley Richards 2d . 
 18 , the Stghert 's Sabbath day . S.A.T.B. ose J. L. Hatton 2d.j}65 . Dame Durden ... ' és ee ea ea 2 1d , 
 19 . thought of childhood . S.A.T.B. wove Henry Smart 2d.| 65 , a little farm well till 9.7 . " Hook 1d . 
 20 . pte Fear a wu : ee a ave a ad . | - F rsa Nero a simple maiden ... ae Sir G. A. Macfarren ud , 
 2i. no urc ong . S.A.T.B. _ ove " 2d . | 638 . air Hebe ove tine 1d , 
 22 . Sabbath Bells . s.a . T.B. ores oe " 2d . | 69 . once I love a maiden fair * .. eve ‘ * 1d . 
 23 . _ — S.A. = 7 in ove ove " - | = Ea ted pes — see ae in * 
 24 . Co utumn wind . § $ .A.T.B. iss an d.| 71 . ak e Ash ... aka " cae 1d , 
 25 . Orpheus with his lute . s.s.s . ces « . Bennett Gilbert ry | 72 . Heartofoak ... soe sv 1d , 
 26 . lullaby . sa ... oT sy +1 1d . | 73 . come to the sunset tree re si ee W.A. Philpott 4d . 
 27 . this be my own , my native land . s. SirG.A.Macfarren 1d.|74 . May . S.A.T.B. .. W.F.B haale 2d . 
 28 . _ — — of = = of Harlech . S.A.T.B. ... Dr. Rimbault 2d . |75 . pure , lovely innocence ee re di Lahore " ) . chorus for female R 
 29 . God save the queen . S.A.T.B. ose eee P 1d . Voices a. " " « e Massenet 4d , 
 30 . rule , Britannia . s. A.T.B. ... ane ose - 1d . | 76 . A Love Idyl . s. re 7 . b. per ees we ee . R. Terry 2d . 
 31 . the retreat . 1.t 3 nen ‘ a we - L.de Rille 2d.}77 . hail to the wood . A ... bi cas .. J. Yarwood 2d . 
 32 . - ! morn be breakir 86:8 , mee Sian ! " — - | 78 , ca the nage of Taunton ... Dean T thomas J. oe - 
 33 . e be spirit . S.S.S. ir acilarren 4d./79 . ur merry oy at sea « J. Yarwood 2d , 
 34 . Market chorus ( " Mas aniello " ) , S.A.T.B. eee Auber 4d.| 80 . Christ be rise ( Easter anth em ) . S.A.T.B. * Berlioz 34 . 
 35- a As Masaniello " ) . S.A.T.B. ove ans seit - | 81 . when the sun set o’er the mountain ( ' ' Il Demonio " ' ) 4 
 36 . e Water Sprites . s. A.T.B. ose eee ove icken ad . | A. Rubinstein 3¢. 
 37 . Eve ’s glitter star . s. A.T.B. ame ee sini ' 2d . | 82 . Hymn of Nature os : Beethoven 3 
 33 . bat maar pn . S.A.T.B .. be ie Fe * | 83 . eg Day ( humorous part -Songs , ' no . 1 ) ' WM Aaynard - 
 5 ewdrop brig S.A.T.B. oo . ove ove ' 1d . | 84 . Sporting Notes , g 4d , 
 4 Sanctus , tei the " Messe Solennelle . " ' s.a.r.n . Rossini 4d . | 8 . desea : an sora | see e — Nova wit mn Hay ydn 4a . 
 41 . Nine Kyries , ancient and modern . ean Gill 2d . ; 86 . a May Carol . s.s.c . ... ' ae Joseph Robineot 4d . 
 42 . sun ofmy soul . S.A.T.B ... . ... Brit aley Richards 2d.| 87 . the bright - hair ’ d a ATT . Theodor L. Clemens 34 . 
 = Pp eoeny " oo vey s. A.T.B. G. A. Osborne 4 | os pe Rest ( bene ‘ ai ss cae bg ag ee - 
 5 ve 2d.| 89 . love reigneth over all . T.T.b . B. ... sas > . G. Elsasser 6d . 
 45 . OThou , W spore ! mg li haga " Mosé in Eg ' itto " ) Rossini 2d.| 90 . Joy Waltz . T.1T.B.B. ... 6d . 
 46 . the Guard on the Rhine . s.a . t.1 Sir G. A. Macfarren 1d.]o1 . the Star of Bethlehem ( Christmas Carol ) " Theodor bi Clemens ad . 
 47 . the german fatherland . satb . ei 1d.192 . Busy , curious , ee Fly , 1.a.t.b. - 3 G 

 LONDON : 
 CHAPPELL & CO . , 50 , NEW BOND STREET , W 

