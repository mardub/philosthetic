706 MUSICAL TIMES.—Decemper 1 , 1887 

 PROFESSIONAL NOTICES . MISS LILY MARSHALL - WARD ( Soprano ) , 
 MISS JESSIE MARSHALL - WARD ( Contralto ) , 
 MADAME ANDERSEN ( Soprano ) . 80 , Addison Street , Nottingham . 
 Concerts , Oratorios , Matinées , Soirées , & c. , 59 , Victoria Road , ° c Db 
 Tuebrook , near Liverpool . - MISS BERT HA BALL ( Contralto ) . 
 Oratorios , Concerts , & c. , address , 55 , Breakspear Road , 
 MISS MARIAN BATES ( Soprano ) . eS ... 
 Concerts , Oratorios , & c. , address , Mr. Brook Sampson , Mus . Bac . , MISS BOU RN E “ ( Contralto ) . : 
 Beethoven House , Northampton , “ Possesses rich powerful voice . ” — Vide Press . 
 MADAME CARRIE BLACKWELL ( Soprano ) { | — _ _ _ © 3 ee Oe 
 ( Pupil late Madame Sainton - Dolby ) . MISS CHADWICK , R.A.M. ( Contralto ) , 
 Orchestral , Oratorio , Ballad Concerts , & c. , 44 , Sloane Square , S. S : Ww . | 58 , Henshaw Street , Oldham , Lancashire . 
 Birdalist ests | ah ges 
 MISS FRASER BRUNNER ( Soprano ) . | MISS NELLIE CLOUDESLEY ( Contral ) . 
 Oratorios , Operatic Ballad Concerts , address , 40 , George Street Concerts , Oratorios , & c. 
 West , Spring Hill , Messrs. Rogers Priestley , Colmore Row , | Addre ss , Crayford , Atherton Road , Forest Gate . 
 Birmingham . 
 ee ee — | MISS MARY DAKIN ( Contralto Mezzo - Soprano ) , 
 MISS AMY M. CARTER ( Soprano ) . Ballads , Oratorios , & c. , address , 31 , Monmouth Road , Bayswater 
 Oratorios , Concerts , & c. , address , 7 , Billing Road , Northampton . W. ; Baxter Gate , Loughboro ’ . , 
 MISS EFFIE CLEMENTS ( Soprano ) . MISS DEWS ( Contralto ) , 
 Silver Bronze Medalist , Certificate Merit , Royal Academy | 63 , Waterloo Road N. , Wolverhampton . 
 Music , London ; | ~ Ty BMV rre uf 
 Years Tour Mr. Sims Reeves . | MISS MARY EDYTHS ( Contralto ) 
 Concerts , Oratorios , Homes , Banquets , & c. , Mitchell , Old itm ee 
 r nceerts , Oratorios , : es , bang C. , mitche Oratorios , Concerts , & c. , 2 , Argyll Street , W 

 Bond Street ; Mr. Alfred Moul , 26 , Old Bond Street , W. | — — : 
 MISS CONWAY ( Soprano ) . MADAME CLARE FOY ( Contralto ) , 
 38 , Spencer Place , Leeds 

 work selected “ Golden Legend , ” 
 public evinces signs tiring , notwithstanding 
 times heard past 
 months . wasa case love sight , time 
 served cement affection . shall resist 
 temptation word favour Sir Arthur | 
 Sullivan masterpiece , speak performance 
 , respects , uncommon merit . Mr. 
 Barnby choir distinguished usual , 
 rendering Prologue Evening Prayer 
 good examples choral singing . Madame Nordica | 
 embodiment Elsie remarkably artistic creation , 
 carefully considered extremely sympathetic ; 
 satisfactory exponent appeared . Mr 
 Lloyd , usual , perfect music Prince Heury , 
 unqualified praise end . Mrs. Belle Cole 
 successful Ursula , fine voice badly 
 produced , Mr. Henschel appear advantage 
 Lucifer 

 reason hard divine , ‘ Israel Egypt ’ ? 
 poems years Albert Hall 
 24th ult . , w revived presence 
 large audience , th 
 usually thronged . Exceptat Crystal Palace , 
 Handel colossal work rendered effectively | 
 South Kensington , present occasion | 
 received enthusiasm reason believe 
 permitted drop | 
 repertory . great credit Mr. Barnby 
 way choral numbers rendered . 
 machinelike accuracy , 
 spirit important choruses 
 attacked excited assemblage . 800 singers 
 obeyed word command like perfectly drilled 
 regiment , , Handel Festival , 
 heard Hailstone Chorus , “ spake word , ’ 
 “ Horse Rider ’ given electrical 
 effect . impossible , , justify violence 
 Handecl intentions assigning ‘ “ Lord isa 
 Man war ” tenors basses chorus . True 
 effect fine , principle involved compels 
 disagree Mr. Barnby course . - rate 
 orchestra doubtless interpret Beethoven Quartet 
 perfect finish , save egotistical conductor 
 venture outrage ? Unnecessary 
 tampering composer score deprecated , 
 establishes bad precedent opens door 
 sorts abuses . add Miss 
 Anna Williams , Madame Patey , Mr. Lloyd sang 
 respective solos way entirely justified liberal 
 applause audience . vaiue Dr. Stainer 

 assistance organ noticeable marked 
 degree . 
 NOVELLO ORATORIO CONCERTS 

 MUSICAL TIMES.—Decemper 1 , 1887 . 725 

 LONDON SYMPHONY CONCERTS . | Monday , October 31 , littlke Hofmann second 
 } final appearance , enormous 
 winter numerous afford regard | § gathering , | unable - obtain admission . 
 lightly success failure Mr. Henschel enterprise , | 1 ° ite possible , , heard gifted 
 commenced title season . fear | lad time occ : asion disappointed , 
 result series Concerts encourag- | = oe : rendering wal Beethoven * Sonata 
 ing , understood support accorded | aot aed Jeft ‘ pe gaia , ere 
 present season depend continuance ; .U{71€¢ cieeae eee ie ) patina nl 
 undertaking . advisable assume | si sll ed false Banger audience , how- 
 useful , somewhat ungrateful , réle candid friend , pare » betrayed outward sign o1 Geenntens , , sha “ 
 giving credit good points | AA de eon child- pianist ht great Paani Cong . — 
 London Symphony Concerts , attention | gee Spee oS. played > 1 Mos art , Sonata 
 matters improvement advisedly . oa ) s ue ~ 1h psec . _ set bce pire ae st 
 important respect Mr. Henschel grown wise pot ee " EL ven ae : S ete ie ‘ . ae < o , 
 experience . perceives public predilec- ag es yer eee oe eo ah ya ook ha sepa liieds 
 tion novelties , simply novelties , | ~ ’ _ bi rage inet ) “ _ nion , — oP : 
 programmes future planned | Gon Ganka ehiee . ane | s chases 2 % seein Seal 
 pular basis case year . fault what- tained Schumann Quartet minor ( Op . 41 , . 1 ) . 
 ts oagpetlg : : ° Z er Miss Lena Little vocalist , introducing 
 found selections natediace vteees es sian Beso ea 
 Concerts , notice month . tical little song # lraume , 
 occasion pee items Beethoven C minor 
 Symphony Overtures ‘ Euryanthe ” 
 “ Tannhauser . ” Mr. Henschel publish list 
 orchestra , reason ashamed 

 ORCHESTRAL performances Metropolis 

 things Wagner poe 
 founded theme * Tris tan und Isolde . 
 Mdlle . Janotha , absent seasons , 
 returned , Saturday , 5th ult . , re- 
 appearance , choosing Schumann popular work , 
 . . . . : “ Carnaval , ” solo . warmly received , 
 time , points rendering ay — h ; Pent seutiaal ial es abies - se “ s 
 co ¥ es oa ace ate al ner , al = re- 
 works named calculated provoke difference opinicn . aul lo - eiies wilieiesl iain F potahn act Mayas pcp m2 
 . : : . amble played confusedly . Later , , 
 Conductor authority slow tempo | ‘ , : ped th Biss seully pees oe se ; : - : 
 : fn ee GS recovered , later numbers 
 adopted movement Symphony , ie ar nec Bien d. ’ Thet “ s en whee ae aun 
 , : . ae a7 Aa ; . | charmins 1ere concerted work 
 rapid pace Pilgrims ’ Chorus Wagner ] 2 " 9 " ? aes : pg Cs ? piety 
 ATL Monette abate boKreds “ wene rarteent 4 | pianoforte programme , String Quartets 
 Overture taken , cases certainly seen tented . Mesias ae ee 
 “ ° mm , : : ag - stead . 1ese ere .Nenaelssonn s li Ss Ne y. 
 loss effect . Scandinavian composer , Grieg , } ° Xi 2 Roney f hich ‘ sid aaa Pet 
 : : : : . P » . e piquant vzo Ol whic ras encored , ! 
 written charming Melodies Haydn ’ ie “ pi mm ; ic Ee fe kj ‘ c ( 0 ny : N es 
 ae . . ; r S € ‘ ym 33 , « VO . 3 ) . 
 Strings ( Op . 34 ) . entitled ‘ Hjertesar ’ ( Wounded Sse p eee layed twa te “ C P. 33 ; od 
 9 70 . . s : yr ¢ Sy oso 
 Heart ) * * Varen ” ’ ( Spring ) , originally settings : } P ; : 
 f ty SoD Oe Winie . lncther present fommihee Siciliano , pen , played 
 ae ane pried minded . Miss Thudichum 
 : parr isseh pen ater : : “ | applauded tasteful rendering Sullivan ‘ Orpheus 
 racter , deserve heard . Mr. Santley intro- rit Cin tele . ” seal tan 68 iGaeull dadtek Gaede acee 
 ; atari « + 49 | lute , ” < wo srulf quaint Swedish songs . 
 duced scena Glinka opera “ Russlan Ludmila , ” | * ttnie tedtinastvace Sencha Malle . | se pena scien 
 7 aa . : n tne lohowing . nday . ile . Jan aag appeare 
 Hans Sachs menckgnes , rhe pianist ao oxaleemed Mendelssohn vitae 
 old - fashioned remarkably vigorous piece , Ge vices . ae wellas tabi . DAKE Seiniiainie Bininaie 
 Mpeamey sug : Qt tet E flat ( Op . 4 £ ty uth b told , lan 
 , : c : , uartet fla 47 1 e tru nay told , 
 programme second Concert , Wednesday er ses st Lal lt se » ish aaineiitililin : tant 
 afternoon , 23rd ult . , evidently designed view |SPS ° é rap ptabl . 
 pine : * 3 7 : . os tmore thoughtful audience . certain 
 t au Cail . 
 gener al public , reason ie lindlahanak te : wxtracamatedind oa style mone ape 
 $ s “ 4 ge x Vag S1esS “ 4 Ze 4 ro Ce 
 — — brief notice , item os Sioa ar ieaeaes dik ” sd qui yl es . ons ith 
 = virtuoso < artist , ar uite oO ) 1 V1 
 familiar connoisseurs . need describe M al tt mes ra ; yc - Fe — : aie 
 ’ yo ge . Alendeissonn s s ewha severe work . riner , 
 Madame Néruda plays Beethoven Violin Concerto , ae SAGAS . oe : abe 
 3 Sse Sh ates vuentiees Se . > } asked encore , Mdlle . Janotha responded 
 regard Schubert unfinished Symphony B bra ura piece scarcely suitable tor classical Concert . le 
 : . r : + » | bravura piece scarcely suitable classica ncert . 
 minor , Liszt picturesque Symphonic poem * Les Préludes , ” | “ " ‘ 1 é . nt ee nie ie , howe 2 cata t apparent d ; rio 
 S ” easa VeV iat appare eteriora- 
 Mendelssohn ’ s Overture “ Camacho Wedding , bes tempor O g ‘ 
 : ; rary share Quartet 1 
 Wagner delicate little sketch “ Traume ” verythin init tee hanteadk Gis tes ie ch pans 
 -}ever ing Z > T 1e Wav Oo SNasteness 
 sufficient received adequate “ aD ppt igs ne eg Sage ste ae ; oe 
 ’ anc \ styie . uaed tne rogramme vere 
 justice Mr. Henschei fine orchestra . Wagner Midin Wivile teecatitn atu San nag D fs , 
 ; wala . adame Néruda favourite Corelli Sonat 
 early Symphony C announced performed | 7 ) ° C ° ™ © Cian FE . aR ere ly 
 time England zoth ult . , late | O24 " S ‘ cuartet , generally known . 6 . iis 
 rs ingl 2gth ult . , late | " 4 ° . sai tee ngs : Spe ees 
 notice present number work noteworthy composed 
 . 4 s aoe s . - . + . wa 
 great master , prominence violoncello 
 , quartets written Frederick 
 MONDAY SATURDAY POPULAR CONCERTS . | William IL . , skilful performer instrument . 
 Mr. Herbert Thorndike commended vocal 
 selections , comprised charming little known s ye 
 called “ Dream ” Haydn , clever songs 
 Miss Maude V. White . 
 Unreserved commendation accorded Mdlle 

 ito reli 

 

 Tue work far Concerts mainly 
 aroutine character , remarks performances 
 merit brevity . order 
 good start Saturday series October 29 
 Beethoven programme provided , result , usual , eget ‘ ; 
 fill St. James Hall end end . works | Janotha rendering eethoven _ Sonata D 
 selected Quartet C minor ( Op . 18 , . 4 ) , | ( OP . 25 ) , generally known “ Pastorale , ” atthe anew 
 Trio E flat ( Op . 70 , . 2 ) , Violin Romance G | Saturday , 12th mn . pure example classical 
 ( Op . 40 ) , Sonata , Funeral March , flat pianoforte playing , straining alter amects 
 ( Op . 26 ) . idea showing high |¢€*aggeration style . audience evidently 
 appreciation , audience insisted encoring delighted , unreasonable exacting encore 
 items - named , Madame Néruda responding | alengthy work . brilliant performance erro 
 companion Romance F ( Op . 50 ) Mr. Hallé , | ! Beethoven early melodious Quintet ( Op . 4 ) B 
 interpreted Sonata best manner , | lat , leadership Herr Straus , gave asa 
 contented Schubert ‘ “ Momens | S0lo Romance , Max Bruch ( Op . 42 ) . Rubinstein 
 Musicales . ” Mr. Santley , vocalist | Sonata JD , piano violoncello ( Op . 15 ) , completed 
 afternoon , introduced songs composers , | list instrumental works , Mrs. Henschel dbo 
 sake variety . fine voice , | 4¢cepté vble songs Grieg ha Purcell 
 fuller justice Handel “ Del minacciar del vento ” ; ‘ * Nymphs Shepher rds , hurried final cadence 
 Gounod “ Le Nom de Marie . ” admirers , | isa mistake , character 
 , glad enlarge repertory music period 

 restricts limecif Concerts . little said concerning Concert 

 MUSICAL TIMES.—Decemper 1 , 1887 

 14th ult . , works presented familiar habitués 
 Concerts . little short impertinence 
 criticise works Beethoven Quartet E flat 
 ( Op . 74 ) , Mozart Duet G , violin viola , Haydn 
 Trio C. Mdlle . Janotha pianist , 
 solo Chopin Barcarole F sharp ( Op . 60 ) , 
 rendering generally commendable little 
 hard unsympathetic . encore usual 
 demanded , gave Polish composer Berceuse D 
 flat , exquisite delicacy finish . vocalist 
 Miss Marguerite Hall , partially successful 
 Schubert “ ‘ Die junge Nonne , ” deservedly won 
 favour audience songs Kjerulf Miss 
 Mary Carmichael , encored - named 
 composer “ * June Song 

 novelty Concerts , entirely new 
 London amateurs , Brahms Sonata , piano 
 violin ( Op . 100 ) , played Mr. Hallé Madame 
 Néruda Concert Saturday , roth ult . 
 work heard Mr. Hallé Concerts 
 summer , surprised delighted 
 flow bright genial melody , Brahms , 
 , mood . second Piano Violin 
 Sonata rank attractive 
 finest works , likely greater 
 favourite predecessor G. Mendelssohn ever- 
 welcome Quartet E fiat ( Op . 12 ) opened Concert , 
 piquant canzonetta , usual , encored , 
 Fibich Quartet E minor ( Op . 11 ) closed . 
 promise work musicians glad 
 acquaintance efforts Bohemian 
 composer . Mr. Hallé played Beethoven “ * Moonlight ” 
 Sonata , Mr. Thorndike repeated artistic rendering 
 Haydn beautiful song ‘ Dream 

 Concert following Monday 
 notice month , began Brahms second 
 elaborate Sextet G , strings ( Op . 36 ) , 
 received magnificent interpretation . rendering 
 Finale Madame Néruda associates , 

 English players Messrs. Gibson 
 Howell , near perfection possible . Madlle 

 Janotha injured effect Beethoven Sonata D 
 minor ( Op . 31 , . 2 ) taking fast . 
 Adagio Andante con moto Allegretto , Presto . 
 dreamy poetical slow movement especially suffered . 
 Songs Schumann , Brahms , Bizet charmingly 
 rendered Mrs. Henschel , Schubert favourite Trio 
 B flat ( Op . gg ) concluded performance 

 CRYSTAL PALACE 

 Concert performance Mozart ‘ Don Giovanni , ” 
 available mode homage 
 paid genius composer occasion 
 hundredth anniversary production opera , 
 rendering given October 29 , , 
 circumstances , highly creditable . taken 
 orchestra scarcely necessary speak . Overture 
 accompaniments admirably played , 
 case diminution power occa- 
 sionally desirable . vocalists , Miss Annie Marriott , 
 good voice threw greatest earnestness 
 singing , successful . Mdlle . Gambogi 
 fair Zerlina , great advantage 
 home Tuscan tongue , 
 soloists pronounced Stratford - atte Bowe fashion . 
 Mr. King , sound conscientious artist , sadly mis 
 placed Don Yuan , matters worse slow 
 speed took serenade . Mr. Brereton 
 energetic Leporello , Mr. Egbert Roberts occasionally 
 sang tune Commendatore . hand , 
 Mr. Probert sweet voice heard great advantage 
 ‘ Dalla sua pace ’ ? ‘ Il mio tesoro . ” Mr. Probert , 
 , needs greater animation delivery , , let 
 add , greater respect text great masters 

 atest work like Beethoven Violin Concerto , 
 opportunities display quality requisite 
 great player , think sign fine artist 
 sparing introduction adventitious ornament . 
 M. César Thomson , appearance Eng- 
 landat fifth Concert series , 5th ult . , evidently 

 thought , availed oppor 

 tunities introduce - nigh interminable cadences , 
 certainly served advertise remarkable technique , 
 inappropriate context irritating 
 reverent auditor . M. Thomson produces sweet 
 round tone , playing marked certain individ- 
 uality conception . rendering Concerto , 
 , wanting dignity . frequent 
 inexcusable use rubato rendered hard 
 orchestra co - operate , dirge - like speed 
 took Rondo robbed final movement 
 life brightness . M. Thomson feel 
 cadences , Beethoven . con- 
 tribution Paganini Fantasia ‘ “ ‘ Non pit . mesta , ” 
 display virtuosity highly appreciated 
 orchestra . testimony guarantees technical 
 dexterity , magic interprets emotion- 
 suggesting music little 

 interesting novelty heard time 
 shape Concert Overture , ‘ Land fountain 
 flood , ” Mr. Hamish MacCunn , recently 
 completed studies Royal College . work — 
 spirited bold conception brilliantly 
 scored — finely played enthusiastically received . 
 Brahms beginner , Mr. Manns takes 
 equal care rehearsal new work . Mr. MacCunn , 
 summoned platform , felt 
 happy performance work . 
 Italian Symphony constituted fréce de résistance , 
 splendidly played , Mr. Manns . 
 new - comer heard Mrs. Belle Cole — 
 fine contralto rich quality 
 considerable compass flexibility , set 
 drawbacks possessing school faulty taste 
 ornamentation . natural resources great 
 , judicious guidance , render 
 valuable addition ranks concert - singers . 
 Mrs. Cole deserves , particular , cautioned 
 temptation exhibit extensive range taking 
 high notes , occasion generally sharp . 
 Concert , preceded Dead March 
 “ Saul , ” memory Sir George Macfarren , 
 buried morning , concluded fine performance 
 Dvorak beautiful Scherzo Capriccioso 

 Unper title Association formed , 
 Signor Carlo Ducci Director , performance 
 success series 

 works wind instruments , 
 Concerts given Royal Academy Music season 
 purpose suggested present enter- 
 prise . series progress consists perform- 
 ances Continental Gallery , 157 , New Bond Street , 
 Friday evenings . members Union Mr. 
 J. Radcliff ( flute ) , Mr. H. G. Lebon ( oboe ) , Mr. Gomez 
 ( clarinet ) , Mr. T. E. Mann ( horn ) , Mr. W. B. Wotton 
 ( bassoon ) , Mr Ducci ( piano ) . popular 
 success undertaking achieve , musicians 
 fail regard satisfaction , calculated bring 
 light number fine works hitherto neglected . 
 example , Concert , irth ult . , heard 
 Onslow Quintet F ( Op . 81 ) , Beethoven E flat 

 Op . 16 ) , Weber Grand Duo Concertant E fiat 

 piano clarinet , compositions far good 
 consigned oblivion . Onslow work , purely 
 original , distinguished refined elegant 
 musicianship characterises French 
 musician efforts , Beethoven genial _ tuneful 
 Quintet popular favourite better 
 known . Onthe , performances highly satis- 
 factory , English members Union , , 
 excelling foreign companions . Mr. Gomez 
 extremely brilliant performer , tone inclined 
 harsh . impossible speak favourably Mrs. 
 Belle Cole rendering airs Handel Gluck . 
 voice perfect control , scales far 
 perfect 

 quintet wind instruments , piano accompaniment , 
 Mozart Quintet E flat . Movements Concertos 
 flute , Macfarren , clarinet , Spohr , included . 
 vocalist , Madame Cornélie Dalnoky , displayed fine 
 voice Weber “ Softly sighs ” Wagner “ Elizabeth 
 Prayer , ” appear recognise fact 
 singing ina small room large theatre 

 common forks use difficulty hearing 
 ‘ far fifth sixth upper partials . tuning 
 harmoniums purpose standard pitch , 
 ; tuned eighth , ninth , twelfth partials addition 
 usual fifths octaves , reason 
 doubt correctness rule calculating beats . 
 Mr. Heffernan paper , , evidently outcome 
 independent thought experiment 

 MASTER JOSEF HOFMANN RECITALS . 
 “ took leave , loath depart . ” 
 Prior line apply Polish child - pianist , 
 | performances formed musical sensation 
 | season 1887 - 8 . reception awaits 
 |other Atlantic — scarcely 
 | doubt excitement - loving cousins rise 
 | occasion — placed charge 
 sober - minded English public sufficiently 
 appreciate wonderful little boy shown 
 mysterious quality musical genius , 
 independent age , bodily physique , experi- 
 ence . country record ; 
 place young Hofmann visited 
 like apathy shown , aggre- 
 gate said drew larger numbers people 
 Recitals living pianist , Rubinstein 
 excepted . particularly concerned 
 present farewell Recitals given 
 St. James Hall 7th andrqth ult . occasion 
 scene outside inside building remark- 
 able . announced advertisement 
 tickets sold , hundreds people came 
 disappointed , final Recital began 
 assemble hours time commencement . 
 , legitimate interest developed perfect craze , 
 | possible spoil Hofmann effusive admira- 
 tion , good doomed . , , 
 anticipate evil . programme Recital con- 
 sisted entirely minor pieces , including trifles 
 pen , germ individuality 
 noted . display improvisation attempted , 
 theme given Sonata Dussek ; 
 result remarkable . Saint - Saéns clever 
 difficult Variations Theme Beethoven , Mozart 
 | Rondo minor , noteworthy per- 
 formances afternoon . reading 
 | highly suggestive original , extravagant . 
 | Onthewhole , , calling astonish- 
 ment admiration final Recital . fewer 
 pieces Chopin programme , 
 played exquisite charm touch , 
 insight composer spirit truly marvellous . 
 specially mention rendering Nocturnes F 
 sharp E , Waltz flat ( Op . 42 ) . repe- 
 tition performance ‘ Moonlight ’ Sonata showed 
 Hofmann gaining facility octave playing . 
 item atrocious dis - arrangement Weber “ Invita 
 tion 4 la Valse , ” pianos . audience 

 728 

 MUSIC GLASGOW WEST 
 SCOTLAND . 
 ( CORRESPONDENT 

 prospectus new season Choral Union 
 Concerts issued . , mind , satis- 
 factory piece compilatory work 
 programme committee , , , abuse 
 praise disinterested labours . Guided 
 degree economical considerations , gentlemen 
 chiefly confined selections works 
 heard previous seasons , Concerts , sure , 
 prove attractive account . 
 present account principal orchestral works 
 performed course series , premising 
 Subscription Concerts referred , arrange- 
 ments Popular Concerts announced . 
 following Symphonies : Beethoven , . 5 , 
 C minor . 7 , ; Symphonie Fantastique 
 Berlioz ; Haydn E flat ( . 10 Salomon Set ) ; 
 Mendelssohn major , “ Italian ’’ ; Mozart E flat ; 
 Spohr ‘ * Power Sound ” ? Symphony . follow- 
 ing Concertos performed , : Beethoven , 
 . 3 , C minor , pianoforte ; Dvorak , violin ( time ) ; 
 Mendelssohn E minor , violin ; D. Popper 
 E minor , violoncello ( time ) . Overtures 
 Beethoven ‘ * Leonore , ” . 3 , Sterndaie Bennett 
 ‘ * Paradise Peri , ’ Berlioz ‘ * * Roman Carnival , ” 
 Cherubini ‘ “ * Anacreon , ’ ” ? Mendelssohn * * Calm Sea 
 prosperous Voyage ” “ lovely Melusina , ” Schu- 
 mann ‘ * Genoveva , ’’ Weber * “ Der Freischiitz . ” 
 need detail miscellaneous selections , 
 mention merely excerpts Wagner , * * Dreams 

 YUM 

 MUSIC LIVERPOOL . 
 ( CORRESPONDENT 

 Mr. Charles Hallé series Orchestral 
 Concerts given Philharmonic Hall , Tuesday , 
 1st ult . , fashionable critical , somewhat 
 sparse , audience . excellent programme compiled , 
 performance melancholy duty 
 gone tribute memory late Sir 
 George Macfarren , orchestra playing Handel 
 Dead March “ Saul , ” audience standing 
 . impressiveness deep silence 
 followed soon pass memory 
 present occasion . piéce de résistance 
 D minor Symphony ( . 4 ) , Schumann , given 
 time Concerts . Madame Norman - Néruda , 
 contributing Wieniawski ‘ Polonaise Brillante ” 
 major , took orchestra performance 
 Beethoven famous Violin Concerto D ( Op . 61 ) . 
 Madame Nordica , vocalist evening , sang 
 air Mozart ‘ * Zauberflote , ” cavatina Pacini 
 ‘ ¢ Niobe , ” Délibes ’ bolero , entitled * * Les filles de Cadiz . ” 
 Great expectations formed thislady share pro- 
 gramme , expectations realised 
 fullest extent bare truth . items 
 performed orchestra Overtures ‘ ‘ Oberon ” 
 ‘ * “ Marco Spada , ” “ Tragique , ” Brahms , 
 time , Wagner “ Introduction 
 Isolde Liebestod , ” ‘ Tristan und Isolde . ” Mr. 
 Hallé , course , conducted , met warm wel- 
 come 

 Tuesday , Sth ult . , justly termed 
 red - letter day musical public Liverpool , , 
 Subscription Concert Philharmonic Society , 
 works produced time , 
 renowned English composers , conducted 
 magnificent work person . production Sullivan 
 ‘ * Golden Legend ” Stanford “ ‘ Revenge ” long 
 memorable present occasion . 
 vocal principals Mesdames Nordica 
 Winant , Messrs. Lloyd Watkin Mills . 
 Great praise “ practical members ” 
 singing choral portions work , especially 
 sublime evening hymn ‘ “ O gladsome lig ! 
 exquisite chorus “ O pure heart , ” 
 encored , - named having repeated . band 
 played picturesque 
 orchestration . Mr. Best presided organ . ‘ 
 Revenge ” conducted Mr. Hallé 

 little wonder , Josef Hofmann , gave Recitals 
 month , taking place Saturday after- 
 noon , 5th ult . , Concert - room St. George 
 Hall , usual place afternoon Classical Recitals , 
 crowded excess . booking quick 
 great , Messrs. Cramer Co. advertised second 
 following Saturday afternoon 
 spacious Philharmonic Hall , packed 
 kindly critical audience . day programme 
 embraced , duets boy played 
 father , Beethoven Sonata Pathétique , Nocturne , 
 Mazurka , Valse , Chopin ; Liszt arrange- 
 ment Chant Polonais . second Concert , 
 host minor acceptable items , Beeth- 
 oven ‘ Moonlight ’ Sonata Mendelssohn Rondo 
 Capriccioso . varied expressions ot 
 , brilliancy vivacity , 
 brought prominently . difficult ac- 
 complishment test improvising theme given 
 audicnce marvellous 
 feat Concert , themes given respectively 
 Schubert ‘ Wanderer ” Fantasia melody 
 Weber . case theme cleverly 
 enlarged 

 second Hallé Concert , held Philharmonic 
 Hall 15th ult . , programme included Beethoven 
 Pastoral Symphony , Légendes time , 
 Dvorak , Wagner Overture ‘ Die Meistersinger , ” 
 Weber popular “ L’Invitation la Valse , ” instru- 
 mented Berlioz . - welcome “ Pastoral , ” 
 beautiful effects , given old force 

 members friends 

 Mr. Edward Lloyd 

 enjoyment richness volume tone produced , 
 appreciation general excellence 
 vigour performance . left 
 desired , esteemed director occa- 
 sionally compel powerful force sway 
 delicacy finish attractive 
 conspicuous solo playing , 
 season exhibited Concerti , Nos . 1 2 , ot 
 Beethoven , clearly fugitive ctforts ot 
 Chopin . Mr. Hallé shows greater advantage 
 interpretation graceful little fancies , briet 
 poetic glimpses Polish pianist , peculiar 
 genius composer blemished attempts 
 larger constructive development quali- 
 fied . addition solo playing , Mr. Hallé 
 introduced Master Josef Hofmann , , second 

 Concert , Recital following week 

 fourth Philharmonic Subscription Concerts | 
 given Tuesday , 22nd ult . , novelty | 
 Liverpool contained programme Haydn 

 Letter T ” Symphony . ‘ work , redolent freshness 
 humour , marvellous devices counterpoint , 
 thoroughly appreciated audience , splendidly 
 played orchestra . Mr. Hallé solo pianist , 
 joining band rendering Beethoven 
 familiar Concerto ( . 1 , G minor ) , played usual 
 quiet masterly style soli — , Chopin 
 Ballade F Gavotte , Gluck Brahms 

 national characteristics appeal ; , 
 unquestionably , powerfully — little rnonotonously 

 , ladies rendering Schu 

 reathe ye steps ” 
 programme , chorus , Mozart ‘ Sweet 
 peace descending , ” second . Mr. Hallé con- 
 ducted , Mr. Straus handling ddéton Beethoven 
 Concerto . fifth Concert , 6th inst . , Madame 
 Albani engaged 

 Tae ¥ , 0 

 MUSIC NEWCASTLE - - TYNE . 
 ( CORRESPONDENT 

 Owina fact Royal Jubilee Exhibition 
 remained open end October , musical season 
 somewhat late commencing year . long 
 Exhibition continued useless attempt any- 
 thing way Concert - giving . 
 amateurs , , means neglected 
 executive Exhibition , music relied 
 chief attraction . Organ Recitals — 
 good , bad , indifferent — given twice , 
 frequently thrice , day intermission past 
 months ; bands parts country 
 “ discoursed sweet music ” hours day night ; 
 best local choirs orchestral societies — 
 notably , Dr. Rea Choir , Tynemouth Philharmonic 
 Society , Mr. Alderson Choir , Northumberland Ama- 
 teur Orchestral Society , Mr. Vincent Ladies ’ Orchestra , 
 — given Concerts frequent intervals . 
 important musical undertaking , , 
 connection Exhibition series Chamber 
 Music Concerts , promoted Messrs. A. Hirschmann 
 Co. Concerts , number 148 , 
 given day Exhibition open . , there- 
 fore , impossible list ( published 
 neat form private circulation ) works 
 performed ; idea importance 
 undertaking mention included works 
 Beethoven Grand Septet strings wind ; quintets 
 Prout ( Op . 3 ; Schumann ( Op . 44 ) ; piano quartets 
 Prout F ( Op . 18 ) , Reissiger E flat , Rhein- 
 berger E flat ; string quartets Beethoven 
 Mendelssohn , large number piano trios , duets 
 piano violin , & c. impossible - estimate 
 value work accomplished Messrs. Hirsch- 
 mann Co. , means 
 introducing musical amateurs large number impor- 
 tant works schools , afforded 
 opportunities local musicians bring 
 prominently public 

 important Concert season took place 
 Wednesday , 16th ult . , promoted 
 Chamber Music Society . artists Madame 
 Norman - Néruda , Mr. Franz Néruda , Mr. Charles 
 Hallé , Madame Sandon vocalist . programme 
 included Beethoven Pianoforte Trio B flat ( Op . 97 ) 
 Schumann Trio F ( Op . 80 ) , solos 
 artists . Needless performed 
 satisfactory artistic manner . Concerts 
 Chamber Music Society partake character 
 drawing - room - unions ordinary Concerts . 
 musical arrangements resemble nearly possible 
 ‘ * Monday Pops ” St. James Hall , price 
 admission necessarily high , audiences 
 usually termed “ select 

 musical season promise busy 
 recent years . Hallé Orchestral 
 Concerts discontinued , undertaking said 
 financial failure . Dr. Rea , , 
 forward attractive programme , including 
 ‘ “ * Lobgesang , ” ’ Orchestral Concerts Glasgow 
 Choral Union Orchestra , Mr. August Manns , 
 Crystal Palace , Berlioz “ Faust ” 
 performed , assistance Dr. Rea choir , Mr. 
 Prout new Cantata “ ‘ Red Cross Knight 

 ult . , followed night second 

 Mrs. Viner Pomeroy series Classical Chamber Concerts 
 season . 
 entertainment , anc d works presented 
 Rheinberger Quartet E flat , piano , violin , viola , | 
 violoncello ; Beethoven Trio C minor , strings ; | 
 G. A. Macfarren Quintet G minor 

 time , repeated desire 
 Concert 7th inst . 
 contra - basso , Larghetto Mozart , Handel air 
 “ O ruddier cherry , ” effectively 
 played Mr. John Reynolds . executants 
 Mr. Henry Holmes , violin ; Mr. Ellis Roberts , viola ; 
 Mr. J. Pomeroy , violoncello ; Mrs. Pomeroy , pianoforte 

 conquered hearts enthusiastic audience | 
 Hundreds turned 

 assembled sce hear . 
 doors disappointed , wonder present | 
 steadily increase 
 trying programme . follows : 
 pianos ) , Weber ; Sonata Pathétique , Beethoven ; Nocturne , 
 E major , Mazurka , C major , Valse , 
 Romance Valse , Josef Hofmann ; Chant Polonais , 
 Chopin - Liszt ; Polacca , pianos , Weber - Liszt . 
 Add juvenile pianist improvised con- 
 siderable time theme supplied Mr. Joseph Roeckel , 
 present , evident talents 
 varied remarkable . performed 
 greatest apparent ease tasks hard 

 matured musician , went even- | + 
 

 gramme , splendid manner 
 |in produces compositions , 

 grow weary . selections 
 composers , band gave Beethoven Seventh 
 Symphony series Subscription Concerts , 
 auspiciously entered Mr. Sykes , Halifax . 
 Drill Hall Halifax scarcely kind place 
 Mr , Hallé band , , , 
 hall town sufficiently large accommodate 
 audience assembled . added 
 occasion Bradford Concert Madame Nordica 
 vocalist , Halifax Miss Adelaide Mullen 
 Mr. H. Beaumont gave songs . Mr. Sykes 
 Concert , Dr. Stanford Heckmann Quartet 
 provide musical fare 

 matter extreme regret musicians 

 appearances Madame Adelina Patti 
 Metropolis worthy record , 
 far . Concert 
 | Albert Hall , 16th ult . , deserve notice 
 | place . irony fate compels 
 gifted vocalist age mainly associated 
 enterprises artistic value . 
 orchestra bestow element dignity occasion 
 mentioned , programme second- 
 | rate ballad Concert , redeeming feature 
 | admirable glee singing London Vocal Union , 
 Mr. R. de Lacy , absence Mr. Frederick Walker . 
 | Madame Patti superb voice , notwithstanding 
 | dense fog filled hall , perfect singing lent 

 charm hackneyed commonplace selections 
 | , usual , contented . 
 artists took Concert Madame Trebelli , 
 | Mr. Lloyd , Mr. Santley . 
 | Miss Marnirpe Wer , sister believe Miss 
 | Marie Wurm , obtained Mendelssohn Scholarship 
 Royal Academy Music , gave Pianoforte Recital 
 Princes ’ Hall , Tuesday evening , Ist ult . 
 young artist , appeared twice 
 Popular Concerts , , , pupil Madame Schumann , 
 gained manner distinguished 
 instructress . displayed excellent technical ability 
 Schumann pleasing little sketches ‘ “ ‘ Les Papillons , ” 
 Beethoven Thirty - Variations C minor , smaller 
 | pieces , defect certain hardness touch , 
 , , apparent larger room . 
 | Miss Wurm valuable co - operation Signor Piatti , 
 | , playing solos , joined Rubinstein Sonata 
 D , piano violoncello . Madame Sophie Lowe , 
 seldom heard public , sang airs Brahms 
 Massenet artistic style 
 favourite vocalist high class concerts 

 o 
 fo 

 influential representative Committee Musicians 
 kingdom formed order found 
 Scholarship Royal Academy Music memory 
 late Sir G. A. Macfarren , Principal 
 Institution 1875 death . scheme 
 particularly appropriate view affectionate 
 solicitous interest deceased musician took 
 oldest School Music , trust goodly 
 sum money forthcoming , 
 provided bare cost education . 
 end , desire testify regard memory 
 commanded universal esteem , intimate 
 intention subscribe . Hon . Treasurer 
 Mr. Alfred H. Littleton , Hon . Secretaries 
 Mr. Charles E. Stephens Mr. J. Percy Baker . Com- 
 munications addressed Mr. Baker , Willersley 
 House , Wellington Road , Old Charlton , S.E 

 Tue Wycliffe Chapel Choir Mr. George Merritt 
 Choral Society gave performance pieces 
 gained prize Musical Competitions held 
 People Palace , July , large enthusi- 
 astic audience Chapel , Philpot Street , E. , 
 3rd ult . , direction Conductor Choir- 
 Mr. George Merritt . instrumental solos 
 included violin performance Madame Dunbar 
 Perkins Wieniawski Légende , Cavatina 
 Raff , Saltarella Papini , re- 
 called . Miss Marian Bonallack presided pianoforte 
 gave Rondino Capriccio Sterndale Bennett 
 excellent style . Mr. F. A. Jewson , F.C.O. , played 
 organ accompaniments , gave solos 
 Beethoven Grand March ‘ * Ruins Athens ” 
 selection Mendelssohn ‘ ‘ Hymn Praise 

 d 

 Hutton sang ‘ * * O Lord , Thou hast searched , ” Mr. G. Aveusius Hoimes , Organist St. George , 
 “ Woman Samaria . ” second service , | Camberwell , gave Organ Recital church 
 Sunday , 2oth ult . , music consisted Mendelssohn | 13th ult . programme , consisting selections 

 Hear prayer , ” solo Miss Ambler ; Goss * O taste Wallis , Salomé , Sullivan , Guilmant , Holmes , Smart , 
 , ” choir ; “ “ O God mercy ” ( St. Paul ) , | ctiectively rendered . Vocal music given Miss 
 Mr. W. H- Brereton , , Miss Ambler , Miss | Marion Holmes , sang ‘ verdure clad ” 
 Jessie Pate , Mr. H. T. Gill , gave - ‘ tet “ God | * Sion ” ’ ( P. Rodney ) excellent taste . 17th ult . 
 Spirit . ” Organist Mr. H. rdic , Mr. | Mr. Holmes gave Organ Recital Mansion House 
 Arthur E. Brown Choirmaster . Chapel , Camberwell . Included programme 
 _ | * Colonial Indian March ” ( Holmes ) , Andante , 
 Tue Inaugural Concert St. Stephen Canonbury ) | varied ( Calkin ) , March theme Handel ( Guilmant ) , 
 Musical Literary Society held roth ult.in } Andante G ( Batiste ) , Cantiléne Grand Cheeur 
 Schools , River Street , N. solo artists Miss ( Saiomé ) , Festal March ( Smart ) . 
 3renda Sutherland , Misses Ashton , Mr. Ernest A. | ; ; 
 Tietkens . Herr Francesco Berger Herr jacoby con-| October 30 , following programme given 
 tributed solos pianoforte violin Saints ’ , Kensington Park , evening service , 
 respectively , Beethoven Duo Sonata series musical evenings people , 

 Op . 24 ) . Mr. Ernest Paxon gave recitations direction Mr. Ernest Lake , Organist Musical 
 evening . choir sang good effect , aes Director Saints ’ , rhe vocalists Miss Norah 
 conductorship Mr. O. E. F. Cobb , Gaul ’ + sayee Mr. Vincent ; Mr. Basil Althaus ( solo violin ) . 
 “ Singers ” Smart “ oor = thou glorious | : gan Solo , ‘ Hear prayer ’ * ( Mendelssohn ) ; Aria , ‘ ‘ Lord 
 sun , ” end . Mr. W. J. Collins , deputy God Abraham ” ( Mendelssohn ) ; Aria , “ shall feed 
 Organist , addition eon al solo , assisted | § flock ” ( Handel ) ; Solo Andante ( Violin Concerto ) , 
 accompanist . Mendelssohn ; Aria , ‘ * heart faithful , ” Bach ( violin 
 obbligato , Mr. Turner ) ; Aria , ‘ “ ‘ mountains ” ( Men- 
 delssohn ) ; Organ Solo , Voluntary C(Stanley ) . series 
 continued monthly 

 Leipzig Cassel recent pr M. Victor Wilder added “ Siegfried ” 
 Peter Cornelius ’ s charming comic opera ‘ number admirable versions books Wagner 
 von Bagdad , ” met highly favourable rece ] : /music - dramas French language . pianoforte 
 work making round German | ; score Tetralogy , M. Wilder trans- 
 ments . lation , published Brussels 

 new dramatic Symphony , entitlec > Mr. Alexander Thayer , Beethoven biographer , 
 Herr L. Heidingsfeld , exceedingly nits } celebrated seventieth birthday Trieste , , 
 recent perform : ince Berlin Cc recently , occupied post Consul - General 

 Herr Goldmark completed new Symphony , w United States Government . Mr. Thayer 
 performed pr ason Dresden , | engaged completion fourth ( _ final ) 
 direction Capellmeister Schuch . volume highly meritorious work great Bonn 

 tral Recitals given St. Nicholas ’ Church , Holton , aid 

 Building Fund . orchestra chorus assisted . Mr. G. E. Lyle , 
 Organist Sherborne Abbey , presided organ . Pieces 
 Mendelssohn , Rossini , Beethoven — ‘ Te Deum ” ( G. E. 
 Lyle ) , choir , orchestra , organ — Haydn , Callcott , Hewlett , 
 Scotson Clark performed 

 Irvine , N.B.—Mr . Hinchliffe , Organist Parish Church , gave 
 Organ Recital church , Friday , 4th ult . 
 assisted church choir , conducted Mr. Allen , Choirmaster . 
 selections excellent , organ choir ample 
 justice . programme works 
 Wély , Ebdon , Mozart , Handel , Herold . Weber , Sir John Gos 

 65 . Dame Durden .. wee d. 
 66 . little Farm w cll tillec Hl “ es ite - Hook 1d . 
 67 . simple maiden ... G.A. Macfarren d. 
 68 . Fair Hebe ae 1d . 
 6 g. loved maiden fair auf F 1d , 
 70 . jovial man Kent ats 43 1d , 
 7h . Ane oak ash ... xe ‘ 1d , 
 72 . Hearts oak ... ne Pa 1d , 
 73 . Come sunset tree ay W. A. Philpoti 4d . 
 76 . Love Idyl .   s.a . T.n . ve » E.R. Terry 20 . 
 7 g. merry boys sea oe . F. Yarwood 24 

 o. Christ risen ; Easter Anthem ) . s.a.r . B. Berlioz ; 
 82 . Hymn Nature Sr -- Beethoven 3d . 
 87 . bright - hair’d morn . s.avr.p . T.L. Clemens 2d . 
 ot . Star Bethlehem ( Christmas Carol ) Fe 
 g2 . Busy , curious , thirsty fly . 1.4.1.3 . 3d . ) 
 93 . Love wakesand weeps . a. T.5.3 . Felix W . Morle -y 2d 

 Bonp Street , 15 , Pouttry , E.C 

