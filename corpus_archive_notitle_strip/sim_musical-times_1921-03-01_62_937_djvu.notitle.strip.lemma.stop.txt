edition ae 

 15,000 BEETHOVEN SONATAS | 
 order firm printer wh 
 London use hav 

 - ton 

 correct minutely Edward Watson , - know WO 

 editor work issue Blind Institute . 
 claim Beethoven correct 

 produce edition world — easy read , easy 

 song like q 
 gossamer web float air , vigorous 
 robust human pathos utter cry 
 desperation . exaggeration ( js 
 case score * L’Ombra ' ) , 
 continually simplify prune 
 expressive significant voice remain , 
 simplification mean spring 
 " poem Tagore , ' voice follow 
 mellifluous emotional line ( mere recitative , 

 gesture , ' Chanson de Bilitis ' 
 ' Quartetto d’archi , ' hand , 
 , affinity old symphony 
 later work . essentially contrapuntal , 
 entirely different quartet 
 modern french school ( 
 consider impressionistic suite ) , 
 spring directly quartet Beethoven 
 Brahms , possess 
 flavour new , living harmony , 
 vigour action perfect 
 accord rest composer work , 
 composition enthral 
 sole virtue sound . wish 
 signify , describe ; 
 need sentimental plot comment . 
 music — pure music , essentially physical 
 content , able transport soul 
 listener world dream 
 wish , speak true language 
 indefinite unknown 

 Symphony ' Sacintala ' 
 work reveal element strong 
 individuality Franco Alfano . ' 
 creation formidable dialectician 
 thought demonstrate unite edifice 
 stern pleasing ; 
 unrestrainable lyricism , song bear 
 heart effort : passion burst 
 forth , life felt , 
 struggle ideal , reach transfiguration 
 labour love 

 

 far change development 
 humanity . ' word press 
 logical extreme provide apology 
 excess modernism . course 
 conviction conservative art 
 ' zesthetical principle ' zo¢ change , basis 
 true . difficulty decide 
 finality true zsthetic principle 
 , knotty point afford everlasting 
 quarrel conservative radical , classicist 
 modernist , romanticist naturalist , 
 classify disputant . 
 Prout , man broad common - sense , 
 unquestionable bias conservative 
 position , ready admit ' example 
 free - writing find work 
 Bach Beethoven , Haydn 
 mozart’—example smile 
 think Strauss Debussy d’Indy ; 
 wise concede practice 
 precede theory , assertion 
 justify extreme novelty . ' inspired 
 composer , continue , invent new 
 effect ; theorist follow modestly 
 rule fit practice 
 master . suggest 
 humble conception theorist position , 
 surely function discover 

 inalienable eternal principle beauty 
 ‘ form great master adhere . 
 | judge advance musical composition , 
 _ force conclude work theorist 

 Sorabji throw , | 
 hold view Holst suite meet 
 practically unanimous approval . fact , know 
 new work scope challenging 
 character having receive 

 describe ' Ta - ra - ra - boom - de - ay ' ' 
 wretched tune ' | concerned quality , 
 source . Mr. Herbert W. Horwill letter tell 
 ' classical music , ' theme set 
 variation Beethoven . know 
 Mr. Horwill mean accuse lése - majesté 
 dig Beethoven . , letter 
 typical blind worship classic 
 responsible unsatisfactory 
 musical life . , dig astray , 
 analogy song melody 
 theme use variation development . 
 song tune stand fall melodic quality . 
 theme development — — 
 worthless composer 
 hand . return point , high time 
 recognise admit frankly lot 
 classical music bad , large 
 quantity popular music composer 
 good . ' recent controversy popular music 
 disputant far ready judge 
 title - page 
 matter inside . jazz enthusiast lightly dismiss 
 classical music caviare , forget innumer- 
 able piece Handel , Bach , Beethoven , Wagner , 
 classical composer enjoy , _ 
 enjoy , continue enjoy 
 civilised world popularity 
 late Charing Cross Road ' winner ' . 
 kind music — good bad , 
 good fair 
 chance . natural - musician look 
 recognise composer good type , 
 lamentable fetish mere 
 , great . place 
 democratic day snobbishness 
 prefer programme bad music dead 
 composer good music live 

 
 m 
 ression 
 rk 
 e cleems jt 
 Mr 
 ll 
 ' ontributor 

 MUSICAL TiIMES — Marcu 1 1921 173 

 matter live dog a| 
 dead lion , dog plain unknown | 
 Bill Smith lion Beethoven 

 Mr. Algernon Ashton lately 
 length ' classic ' fetish lead musician . 
 write press ' appalled ' read 
 4 Beethoven concert report critic opine | 
 composer write good deal | 
 music ' Emperor ' Concerto . | 
 Mr , Ashton ' appalled ' know 
 think year . 
 , place . } 
 squash critic ' person , ' | 
 Mr. Ashton decide ' Emperor ' ' fine | 
 grand pianoforte concerto existence , ' | 
 , having settle matter far present 
 generation concern , look ahead settle 
 posterity . ' perfectly certain , ' , 
 ' sublimely beautiful music 
 write long world . ' ’ 
 , Beethoven 
 Scale Arpeggio Manual ev forme de Concerto 
 ~—and — fine 

 peace conscientious Defender 
 Faith . hardly safely deliver 
 proclamation 
 . Rosenthal , return english 
 concert - platform , play Chopin E minor Concerto . 
 alot people wonder , , add , 
 truly , Chopin long way good . 
 Mr. Ashton 

 str,—whatever come music critic ? 
 day severely 
 reprimand Manchester musical reporter speak 
 disparagingly Beethoven great ' Emperor ' 
 Concerto ( fine pianoforte concerto write ) , 
 nearly London critic run 
 Chopin superb delightful Pianoforte 
 Concerto e minor thick thin ! 
 infect atrocious modern 
 stulf falsely music ? look 
 uncommonly like . ALGERNON ASHTON 

 day severely reprimand ' 
 — amusingly pontifical ? 
 person bit awed . 
 , nearly , speak 
 work Chopin product mere 

 human like 

 pained query end Mr. ashton’s| 
 letter nearly touch spot . critic 
 musician generally ave infect some- 
 thing modern , modern music 
 atrocious . refusal open ear 
 shut eye bigwig send 
 ‘ em . golden age music 
 round corner agree 
 classic place programme divine 
 right , ability answer challenge 
 present . good work , composer | 
 good , message , | 
 great work ? , let hear , no| 
 matter old . fourth - rate , Hummel | 
 inside , Beethoven label , speak ? , | 
 friendly good - bye top| 
 shelf . plenty good stuff , old 
 new , wait vacant place programme . 
 classical composer bad enemy | 
 champion Ashton type . admirer 

 complicated unbusinesslike person 

 concert , January 24 , London 
 Symphony Orchestra , Mr. Albert Coates , per- 
 form Mozart ' requiem , ' Mesdames Stralia 
 Olga Haley , Messrs. John Coates Norman 
 Allin soloist , Philharmonic Choir . 
 frankly disappointing performance , singularly lack- 
 e warmth expression devotional atmosphere . 
 incredible Mr. Coates 
 people lay open charge undue 
 rigidity , 

 Mr. Coates conduct Philharmonic 
 concert January 27 . begin overture 
 * Barber Seville , ' irresistibly 
 cheerful , string achieve magnificent 
 feat virtuosity . new 
 item programme : Hamilton Harty Violin 
 Concerto , play Miss Murray Lambert , 
 Ottorino Respighi ' Fountains Rome . ' Respighi 
 modern Italian , modern 
 Malipiero Casella . great gift 
 picturesque description luminous orchestral 
 effect . Mr. Coates splendid , 
 especially section descriptive Fountain | 
 Triton . suspicion , base 
 previous hearing , composer wish 
 dainty splendid . inclined 
 criticise Mr. Coates ’ reading 777o 
 scherzo Beethoven seventh Symphony 
 way . sound like battle- 
 hymn simple song pilgrim . 
 fine point /7ma / e , rhythmical 
 swing exhilarating ; surely open 
 doubt play string 
 melody open drown 
 accompaniment . Hamilton Harty Concerto 
 work aim impress , 
 succeed characteristically 
 celtic alternation gaiety melancholy . 
 scoring heavy , touch tell , 

 
 solo grateful . sympathetically 
 play Miss Murray Lambert 

 series recital english Church Music , 
 eld new , St. Mary , Primrose Hill , 
 Saturday , March 12 , 3 p.m. programme 
 contain recent work Holst , Rutland Boughton , Harvey 
 Grace , Martin Shaw , specimen 
 Plainsong , Faux Bourdon Canticles Byrd . little 
 know ' reproach ' Bernabei sing . 
 Mr. Geoffrey Shaw , master music St. Mary , 
 glad hear tenor bass willing 
 help 

 musical service enterprising character place 
 Andover Parish Church January 26 , Wesley 
 ' wilderness , ' Franck ' 150th psalm ' sing . 
 Mrs. Fiander , Miss Violet Bannerman , Mr. Mrs. 
 Julian Farmer play Beethoven quartet F , Op . 18 , 
 . 1 . Mr. Farmer play organ solo 
 Brahms , Karg - Elert , Parry , - know hymn 
 congregation share 

 January 23 , Seven Kings Baptist Church , 
 Maunder cantata ' Bethlehem ' sing church 
 choir . soloist Miss Vera Edwards Miss 
 Bertha Forster , Messrs. J. Piper , R. W. Bentley , 
 A. Gull . Mr. Edward D. G. Mason organ , 
 Mr. E. Carlier Mason , organist choirmaster , conduct 

 SYSTEM MUSICAL notation 

 Str,—Dr . Tozer write ' suppose 
 system discoverable notation Bach 
 Beethoven . ' know , 
 care , know vast ' common - sense ' 
 composer far deep 
 ' system ' ' surface - logic . " . t undertake , 
 cheerfully confidently , defend example 
 Bach Beethoven attack score 
 notation Mr. Button Dr. Tozer ( , 
 matter , 

 wish controversy — 
 friendly discussion — personal tinge ; help 

 TA - RA - RA - BOOM - DE - AY 

 S1r,—In ' ad libitum ' note February issue , 
 ' Feste ' describe ' Ta - ra - ra - boom - de - ay ' ' wretched 
 tune ' feeble air 
 obsess helpless population . escape 
 notice classical music . turn 
 Beethoven Clarinet Trio find essential 
 air theme Allegretto con 
 vartaztoni . — Y , XC . , HERBERT W. HORWILL 

 159 , King Henry Road , N. w.3 

 important current ' movement ' 
 London music chamber music 
 suburb . occasionally suburb join 
 chamber concert 

 example | 
 town Kingston , happy possess local | 
 String Quartet - class capacity . Quartet , | 
 consist Miss Yvonne Mont - Clar , Mr. S. Oakley | 
 Parrott , Mr. Bernard Dudley , Mr. Francis Hill , | 
 concert January 24 member Kingston | 
 Congregational Church Guild . programme comprise 
 Beethoven ( Quartet C minor ( Op . 4 ) , César Franck | 
 Violin Sonata , movement composer | 
 Pianoforte Quintet , pianist Mr. B. J. Harrison . | 
 work interpret , audience | 
 help enjoy preliminary remark | 
 Mr. Hill composer music . _ 

 season 
 Exhibition Rooms January 22 , introduce Fauré 

 work choir orchestra Brahms ’ splendid 
 " song destiny , ' perform manner 
 scarcely surpass , intelligent 
 reading , fine phrasing , precision . conduct 
 Mr. E. Godfrey Brown , conduct ' 
 Barber Bagdad ' ( Cornelius ) , ' slavonic rhapsody ' 
 ( Dvorak ) , march * Tannhauser . ' Mr. John 
 Dunn ( violin ) play Max Bruch Concerto G minor 
 orchestra , solo Schubert Wieniawski . 
 Miss Helen Anderton sing invocation Verdi 
 " Il Ballo Maschera , ' song 

 Quinlan subscription concert February 5 , 
 conduct Mr. Albert Coates . Miss Jessie Munro 
 ( pianoforte ) Mr. Mostyn Thomas ( baritone ) 
 assist presentation fine programme 
 include Suite Strings ( Purcell ) , arrange 
 Albert Coates , Isolda ' Liebestod ' ( Wagner ) , Beethoven 
 Symphony . 5 , Liszt Concerto E flat ( pianoforte 

 BIRMINGHAM 

 1921 

 conception extraordinary merit 
 uncommon promise . Beethoven C minor Symphony 
 concert , performance display unfailing 
 discernment complete realisation emotional 
 possibility . 2 

 bristol 

 Tale Old Japan ' * Hiawatha Wedding - Feast ' 
 perform Looe Choral Society February 3 , 
 help good orchestra . Rev. E. A. 
 Saunders secure artistic result choir 
 orchestra , singing remarkably good 

 Count Goston de Merindol , talented pianist , recital 
 Launceston February 3 , begin C sharp minor 
 Sonata Beethoven , play music Schumann , 
 Schubert , Chopin , represent modern music 
 russian composer , include Study Mazurka 
 Scriabin 

 COVENTRY DISTRICT 

 Coventry Choral Society , lead Mr. John Potter , visit 
 Birmingham Town Hall January 22 , appear 
 concert organize Mr. Appleby Matthews , create 
 favourable impression 

 counter - attraction adversely affect attendance 
 Chamber Music Society concert season , 
 place St. Mary Hall January 25 . Mr. 
 T. Henry Smith ( violin ) , Mr. Percy Hall ( violoncello ) , 
 Mr. Arthur Woodall ( pianoforte ) provide trio programme 
 include Beethoven Trio C minor , Op . 1 , | 
 Tchaikovsky Trio ' memory great Artist . ' | 
 vocalist Mr. Herbert Simmonds , establish 
 favourite Coventry , selection include * 
 Jester song ' Granville Bantock . Mr. William Woodall 
 play number modern pianoforte composition 
 Cyril Scott John Ireland 

 aid Railway Benevolent Institution well- 
 attend concert place Baths Assembly Hall 
 January 26 . soloist Miss Lilian Stiles - Allen , 
 Miss Norah Scott , Mr. Frank Webster , Mr. Norman Allin , 
 Miss Winifred Small ( violin ) . local pianist , Miss 
 C. M. Gibbs , accompany success 

 DARLINGTON DISTRICT 

 chamber music exceptionally 
 season . concert Darlington Chamber 
 Music Society Polam Hall January 20 , 
 Beatrice Hewitt Pianoforte Trio play Brahms ’ 
 Op . 8 Goossens ’ ' impression holiday , ' Op . 7 . 
 final concert March , London String Quartet 
 supply programme . second chamber concert 
 Middlesbrough Musical Union bring Brodsky 
 ( Quartet time town . play Elgar 
 Op . 83 , Tchaikovsky Andante Cantabile , Mendelssohn 
 Canzonetta , Mozart e flat . Miss Helen Anderton 
 vocalist , Mr. Paul Kilburn accompany . 
 fourth Corbett concert Middlesbrough , February 9 , 
 Beecham Symphony Orchestra , conduct 
 . Mr. Albert Coates . programme comprise 
 conductor Purcell Suite , Beethoven filth Symphony ( 
 opening note Adagzo ) , 
 ' Tannhauser ' Overture , Good Friday music , 
 ' Walkurenritt . " . fine concert . _ operatic 
 concert Miss Rosina Buckman , Miss Edna Thornton , 
 Mr. Maurice D’Oisly , M. Jean Vallier , form 
 Powell concert , Hartlepool 
 February 4 . Miss Marie Hall violinist . 
 fourth concert , March , Royal Albert Hall Orchestra , 
 Mr. Landon Ronald , appear , district 
 supply instrumental music season , 
 Darlington Choral Society prepare Handel Passion 
 Music Good Friday performance St. Hilda 
 Church 

 DEVON 

 ' Mater ' concert February 6 , new work , 
 ' Slumberland ' ( overture fairy - ballet ) , _ irish 
 composer , Mr. Hubert Rooney , _ _ favourable 
 impression 

 Brodsky Quartet delight appreciative 
 audience theatre Royal Dublin Society 
 February 7 . /ersonne/ combination 
 change , Dr. Brodsky outstanding figure , 
 dominate colleague characteristic style . 
 particular interest Beethoven Haydn item . 
 way variety , goodly crowd patronise Gaiety 
 Theatre February 7 12 enjoy popular strain 
 ' Country Girl . ' , bright 
 catchy music serve divert people mind 
 present somewhat gruesome time 

 British Music Society branch Sydney , 
 N.S.W. inaugural concert December | 
 Verbrugghen String ( Quartet principal artist . 
 programme include Herbert Howells ’ Pianoforte 

 record orchestral music month 
 complete . Prof. Tovey , enthusiasm know 
 bound , transfer Reid Orchestral Concerts 
 City Hall , semes Saturday evening , 
 trust popular price establish fine taste 
 matter musical masse . programme 
 unusually interesting , open series 
 February 5 

 Beethoven celebration form Festival 
 quartet London String ( quartet 
 January 18 - 26 . criticism unique combination 
 isneede , gratifying state concert 
 excellently subscribe - attend 

 Fe!.ruary 10 Miss Jean Waterston vocal recital . 
 unquestionably artist exceptional interpretative 
 ability , submit extraordinary programme draw 
 Schubert , Brahms , Wolf . british section 
 include number Delius , Stanford , Ireland , Boughton , 
 Vaughan - Williams , Gerrard Williams , Davidson , 
 Balfour Gardiner . breadth outlook catholicity 
 taste , Miss Waterston possibly great art - singer 
 Scotland produce 

 Weir Choir , conduct Mr. William Nisbet , 
 good concert January 27 programme 
 carefully select - song . choir 
 considerable number form connection large 
 industrial concern , recent year come 
 result Competitive Festival movement 
 Glasgow . evening , auspex 
 Glasgow Corporation , Scottish Orchestra play 
 popular programme large audience Springlurn 
 Public Hall , annual concert Athenzeum School 
 Music place January 27 , student 
 hear programme solu vocal instrumental 
 music , chamber composition . February 
 Choral Union Scottish Orchestra join force 
 rendering Dvorak ' Spectre kride . ' 
 year work hear , advance 
 choral technique sound somewhat obsolete . 
 Mr. Warren Clemens conduct , good 
 choir . Scottish Orchestra exhibit 
 occasional roughness interpretation instrumental 
 . Miss Stiles - Allen sing soprano air , 
 tenor Mr. John Booth overweighte . 
 Mr. Herbert Brown ( short notice ) baritone 
 acceptably . Glasgow Select Choir 
 rare public appearance February 5 , 
 Mr. Percy Gordon hold good place 
 local choral body . Choral 
 Orchestral Union classical concert place 
 February 8 . excellent programme , include 

 ' Leonora ' Overture . 3 , Schubert Symphony 
 C , Brahms ’ Violin Concerto D , play 
 Scottish Orchestra Mr. Landon Ronald . 
 M. Bronislaw Huberman solo violinist . pro- 
 gramme annual plébiscite concert February 12 , 
 terminate season , include ' Leonora ' . 3 
 ' mastersinger ' overture , Beethoven C minor 
 Symphony , Tchaikovsky ' nutcracker ' Suite , Délibes ’ 
 ' Sylvia ' ballet music , Scriabin ' poem ecstasy . ' 
 concert visit performer 
 Beecham Symphony Orchestra , Mr. Albert 
 Coates , Heifetz , vocal star Miss Myra 
 Hess Mossel series 

 HASTINGS 

 Miss Tessie Thomas Elgar Violin Concerto 
 come stranger Hastings January 22 . violinist 
 rank high contemporary pay 
 attention absolutely true intonation , 
 indicate capacity explore hide mystery 
 work baffle experienced player . 
 technique terror ; unfaltering 
 sense rhythm ; emotional temperament 
 perfectly compatible complex example 
 composer genius 

 Mr. Julian Clifford beneficiary crowded 
 concert February 5 , recipient 
 numerous present public compliment . M. Siloti , 
 come add glamour occasion , captivating 
 Tchaikovsky Concerto seven recall . 
 reading work certainly remove hackneyed 
 groove , differ , 
 fascinating . beautiful 
 distinguished playing afford indulge 
 wrong note start ; Andantino 
 delight , authority master 
 Finale . < violin solo conductor gifted son 
 attraction , dramatic singing 
 Miss Ethel Fenton , Mr. Ceredig Walters ’ song . 
 baritone sing man — 
 effort , vibrato , affectation . expectant 
 audience thoroughly enjoy Julian Clifford - write 
 ' ode New Year , ' perform Hastings 
 Madrigal Society Orchestra . organ harp 
 ( Miss Hilda Atkinson ) pleasantly combine Pierné 
 Harp Concerto Mr. Allan Biggs ’ recital 
 Christ Church , programme include / legretto 
 Beethoven seventh symphony 

 competition arrange Colonel Somerville , 
 Commandant Royal Military School Music , 
 Kneller Hall , original military band music , bring 
 - work , play 
 furthcoming concert season Kneller Hall . 
 judge , Captain Williams ( Grenadier Guards ) 
 Dr. R. Vaughan Williams , disappoint 
 general level music 

 old composer slow movement find modem 
 counterpart deep noble feeling -/dagio 
 Holbrooke Pianoforte Quintet . quartet - suite 
 * British Song Dance , ' work great sustained 
 harmonic invention , effective music , 
 string quartet . Holbrooke touch genius mor 
 appreciable work strong individuality 
 pianoforte piece ' Barrage ' immensely - cleve ; 
 Variations ' Auld Lang Syne , ' brilliantly play 
 com poser 

 follow evening , February 5 , concert 
 Welsh Choral Union , MHolbrooke Overture 
 * Bronwen ’ receive performance , conduct 
 Mr. Hopkin Evans . ' Bronwen ' title drama 
 form trilogy * Children 
 Don . ' commence irish air , ' Savourneen 
 Dheelish , ' overture frankly powerfully descriptive 
 depict ' Sea Music ' ( realistic storm ) , 
 ' irish welsh War Cries , ' Bronwen theme , 
 know welsh ' bottle tune ' work 
 Finale . altogether clever inspiriting work 
 decidedly favourable impression . Miss 
 Tessie Thomas ’ performance Elgar Violin Concerte , 
 true spirit beauty music generally reflect 
 | playing , note - perfect . splendid choir 
 singe orchestra 
 Borodin ' Prince Igor ' dance , ' hail , bright abode . ' 
 occasion lady chiefly score , 
 especially unfamiliar Borodin music . soloist 
 Miss Ruth Vincent , sing Mozart * dove sono ' 
 acceptably , orchestral evening complete 
 Beethoven seventh symphony , choice Mr. 
 Hopkin Evans commend . 
 Welsh Choral Union possess watchful , 
 helpful , masterful conductor 

 conduct Mr. F. H. Crossley , Warrington 
 Musical Society choir hear January 20 
 German ' o peaceful night , ' Grainger * Londonderry Air , ' 
 Gardiner ' cargo . ' vocalist Miss Dorothy 
 Greene , pianist Mr. Edward Isaacs play 
 Beethoven G major Concerto orchestra . Mr. 
 Crossley * ' Valse Lente ' appreciated item 

 West Kirby Hoylake subscription concert 
 January 26 , Dr. W. B. Brierley conduct Parry * Lady 
 Radnor ' Suite ; ' prelude ' Norman 
 O'Neill , base Barrie ' Mary Rose , ' orchestra 
 hidden chorus soprano voice , effectively hear . 
 solo pianist Mr. Frank Merrick , Miss Elsie 
 Thurston Mr. Ernest Williams vocal soloist 

 Quinlan concert January 29 serve indicate 
 M. Rosenthal hardly player pre - war 
 day , remember constant repetition 
 item tour exactly 
 exhilaration . Madame Suggia work bear evidence 
 zest , Miss Licette sing , 
 voice reveal beauty occasion . 
 Mr. Peter Dawson scarcely convincing , save 
 matter diction 

 Co - operative Wholesale Society concert , 
 February 2 , clash Beethoven Sonata recital 
 University Vocal Society performance 
 Bach Magnificat . Mr. Coates sing beautiful song , 
 Hallé concert week early . 
 way audience gain _ respectable 
 acquaintance modern song . choir main 
 contribution series Elgar Greek Anthology 
 - song Coleridge - Taylor ' Great God Pan 

 Bowdon Chamber Concert Society , situate 
 Manchester principal residential suburb , sole 
 survive evening chamber music body , contrive 
 marked success combine social musical quality . 
 January 22 Miss Charlotte Elwell Miss Lucy Pierce 
 play Lekeu Sonata G ( repeat cay later 
 Tuesday mid - day series ) . violin - p!aying 
 confident style Miss Elwell Bach performance 
 Hallé concert day early ; appear 
 work performer . 
 lack orchestral concerto present 
 sonata , impression heighten 
 repetition Tuesday mid - day concert . 

 NEWCASTLE - - TYNE 

 Catterall String Quartet visit city January 5 , 
 thoughtful performance Beethoven . 15 
 minor , Op . 132 . programme complete 
 Haydn . 75 , G , Holbrooke impression , 
 " Belgium , Russia , 1915 

 January 8 Bach Choir 4 , 5 , 6 
 ' Christmas Oratorio , ' , previous per- 
 formance half work , brilliance 
 choral singing feature . beautiful work ought 
 rescue neglect 
 long lie . prelude oratorio orchestra 
 play Suite . 3 , D. Mr. W. G. Whittaker 
 conduct 

 NOTTINGHAM DISTRICT 

 chamber music order evening fourth 
 People concert January 17 , programme 
 provide London String Quartet . Schubert : 
 posthumous Quartet D minor , Dvorak * Nigger ' Quartet , 
 H. Waldo Warner ' folk - song phantasy , ' wer 
 admirably interpret . enjoyable sketch Sj 
 Arthur Sullivan life work January 25 
 Mr. William Woolley , musical illustration 
 furnish lecturer choir , hear ip 
 * O Gladsome light , ' ' o love Lord , ' 
 example , present good balance tone . 
 evening Catterall String Quartet , conjunction 
 Miss Cantelo , draw large appreciative audience 
 University College . Mr. Arthur Catterall associate 
 Miss Cantelo Paderewski sonata minor 
 violin pianoforte , work distinguish extreme 
 emotionalism . item Beethoven Quartet 
 C major ( Op . 59 , . 3 ) string , Fauré Quartet 
 C ( Op . 15 ) pianoforte , violin , viola , violoncello , 
 mention Miss Cantelo fine performance 
 exacting pianoforte Fauré Quartet , 
 delightful brilliant work . Messrs. Wilson 
 Peck concert January 26 artist Miss Carrie 
 Tubb , Miss Margaret Cooper , Signor Giorgio Carrado , 
 Mr. Arnold Trowell . fifth People concert , 
 February 5 , form recital Miss 
 Florence Mellors Mr. William Murdoch . Miss 
 Mellors ’ song range Mozart Charpentier ' Louise ' 
 delicacy Roger Quilter , Mr. Murdoch 
 choice solo display equal catholicity . 
 interesting lecture - recital February 3 
 Mr. Charles Tree , Castlegate Hall . lecturer 
 valuable hint vocal tone , correct breathing , 
 enunciation , & c. , illustrate remark song draw 
 Morart , Korbay , Cyril Scott , 
 source . lecture ' music life ' deliver 
 Mr. J. S. Scott February 5 — auspex 
 Nottingham District Educational Study Society — 
 lecturer endeavour clear british nation 
 imputation unmusical ; prove English 
 reality singable language . demonstration 
 choir Holy Trinity Infants ’ School 
 Sneinton Boulevard Girls ’ School . 
 aid St. John Clergyhouse , Mansfield , pianoforte 
 recitai January 19 Rev. William 
 Lees , Mus . Doc . contralto solo contribute 
 Miss Lucy Bingham . 
 Melton Mowbray Choral Society annual concert 
 hold January 20 , Coleridge - Taylor ' Tale 
 Old Japan ' perform choir 
 orchestra , direction Dr. Malcolm Sargent . 
 principal Miss Elsie Suddaby , Miss Chrystabel 
 Snowden , Mr. Frank Webster , Mr. George Pochin . 
 February 10 Sacred Harmonic Society 
 fine performance Elgar ' Spirit England , ' follow 
 Coleridge - Taylor ' Hiawatha Wedding - Feast ' 
 ' Death Minnehaha . ' Mr. Allen Gill direction 
 choir orchestra acquit 
 distinction , solo effectively sing 
 Miss Lucy Goodwin , Mr. Frank Webster , Mr. Joseph 

 University November , 1920 , direction | Farrington . Society Elgar ' apostle ' 
 Mr. E. L. Bainton , fine performance shortly , Mr. Allen Gill deliver explanatory 

 Borodin Symphony . 2 , b minor . 
 January 27 Mr. Julian Clifford recital , play 
 Beethoven c sharp minor Sonata somewhat affected 

 lecture work February 14 , benefit 
 choir subscriber 

 second annual concert Cardiff Lolian Choral 
 Society Cory Hall January 26 . varied 
 entertaining programme great 
 success . proceed devote Lord Mayor 
 Fund unemployed 

 University Colleges display great musical 
 activity . Thursday evening concert Aberystwyth , 
 arrange School Music , ' continue attract large 
 audience , ' ' tend grow appreciation 
 chamber music student . ' Cardiff chamber 
 music popular . programme February 12 
 consist Brahms ’ Trio c major , Beethoven Trio 
 b flat , Trio Saint - Saéns . annual concert 
 College Choral Society , hold March 

 good music 

 Bradford Subscription Concert February 4 
 outstanding importance appearance 
 M. Busoni , play solo Mozart fine e flat 
 Concerto ( K. 482 ) masterly style , wonderful 
 sensitiveness power . soloist 
 ' indian Fastasy , ' base folk - tune american 
 Indians , work , clever , fail impress , 
 lack spontaneity charm . Tchaikovsky 
 * Romeo Juliet ' Richard Strauss ’ ' Tod und 
 Verklarung ' finely play Hallé Orchestra 
 Mr. Hamilton Harty direction , work 
 particularly strong impression revival . 
 Bradford Permanent Orchestra Concert 
 January 29 , Tcherepnin Pianoforte Concerto chief 
 feature programme . new West 
 Riding , sufficiently unfamiliar especial interest , 
 certainly repay close acquaintance , originality 

 conception variety mood remove 
 ordinary . brilliant solo , remarkable 
 cadenza , artistically play Miss Margaret Collins , 
 Mr. Julian Clifford , conduct , introduce tone . 
 poem , ' light , ' impressive evidently sincere 
 tribute memory Ernest Farrar , promise 4 
 composer cut short war . eleventh 
 series Free Chamber Concerts organize Mr , § , 
 Midgley place . occasion , 
 time war , work german 
 composer , excuse return 
 artistic broad - mindedness find 
 fiftieth anniversary birth Beethoven , 
 * Kreutzer ’ Sonata play concert 
 great B flat Trio(Op . 97 ) atthesecond . Grieg Debussy , 
 native composer Parry Ireland , furnish 
 instrumental piece . February 9 Mr. Edgar 
 Drake String Quartet chamber concert , 
 Mr. Edgar Knight sympathetic colleague , 
 Dohnanyi delightful Pianoforte Quintet C minor 
 , H. Waldo Warner ' Phantasy ' string quartet , 
 long - choose series songs-—admirably sing 
 Mr. Harry Horner—-being programme 

 SHEFFIELD 

 Quinlan concert February 4 provide disappoint- 
 ment absence Mr. Peter Dawson , , 
 audience inform , sail morning 
 concert India , compensation Madame Suggia , 
 Miss Miriam Licette , M. Moritz Rosenthal prove 
 capital form , furnish programme sufficient 
 variety interest concert success 
 artistically . Chopin B minor Pianoforte Sonata , 
 Boéllmann ' variation Symphoniques , ' operatic aria 
 charmingly sing Miss Licette , important 
 number 

 Sheffield Quartet appearance 
 University chamber concert , player acquit 
 Beethoven Op . 74 , Brahms 
 minor , ' novellette ' Frank Bridge . 
 ( quartet comprise Mr. John Collins , Mr. John Bingham , 
 Mr. Allan Smith , Mr. Collin Smith . - 
 fine violoncello recital January 20 , 
 assistance Miss Ivy Smith ( pianoforte ) 
 Miss Edith Groat ( vocalist 

 Eva Rich Tuesday concert , - o'clock 
 concert organize Misses Foxon , 
 continue , great value musical 
 education Sheffield . Mrs. T. P. Lockwood , 
 - o’clock event February 9 , ' talk ' english 
 song , introduce illustration series song 4 
 dozen composer range Dowland Frank Bridge , 
 capitally sing Miss Margaret Currie , Mr. Harold 
 Woodhead , Mr. Ernest Platts , accompany 
 excellent fashion Miss Ethel Cook , play 
 virginal music Byrd Farnaby 

 town 

 Halifax Chamber Concert , January 28 , consist 
 music viola ( Mrs. Rawdon Briggs ) pianoforte 
 ( Miss lucy Pierce ) , programme include Bantock 
 ' Colleen ' Sonata , Brahms ’ Clarinet Sonata F minor , 
 Romance Ernest Walker . Mrs. Briggs artistic 
 violist , fine tone ; playing , 
 colleague , high order . January 19 , Hull 
 Vocal Society , Dr. Coward , dramatic reading 
 Elgar ' King Olaf , ' Miss Agnes Nicholls , Mr. 
 Webster Millar , Mr. Charles Knowles principal . 
 Dr. Ethe ! Smyth conduct - song ' hey Nonny ' 
 thesameconcert , Hull Philharmonic Society concert , 
 January 27 , introduce orchestral sketch entitle 
 * Exhilaration ' ' D’Artagnan , ' Mr. Walter Porter , 
 conduct , Miss Guendolen Roe play 
 solo Grieg Pianoforte Concerto brilliant style . 
 Mr. J. W. Hudson conductor . Mr. Janssen 
 Hull Subscription Concert , February 4 , daughter , 
 Miss Ina Janssen , promising accomplished 
 contralto , successful appearance long 
 varied series song . Modern Trio hear 
 trio Beethoven , Brahms , Ireland 

 Mr. Hallas , Huddersfield tenor exceptional 
 artistic power , interesting recital town , 
 , January 24 , introduce admirably 
 choose effectively group song , include 
 Holst ' hymn Rig - Veda . ' ? able 
 colleague pianoforte Mr. James Stott . York 
 Musical Society chamber concert February 2 
 sustain Catterall Quartet , programme consist 
 String Quartets Beethoven ( op . 18 , . 5 ) 
 Debussy , Ernest Walker charming ' Phantasy ' 
 Quartet . Collingham Chamber Concerts continue 
 source light leading small village ; 
 January 15 present Miss Elsie Suddaby vocalist , Miss 
 Kathleen Moorhouse violoncellist , Mr. Herbert 
 Johnson pianist . , February 12 , César 
 Franck Pianoforte Cuintet Leeds 
 Bohemian ( Quartet , Miss Kathleen   Frise - Smith 
 pianist 

 musical wote abroad 

 assqrte concert 

 Paris concert end , asa rule 
 meet favour , Mr , Fernand Pollain , example , 
 recently violoncello recital admirably 
 support appreciate - contrast pro- 
 gramme . occasion Beethoven * sept variations 
 sur un theme de Mozart ' ( ' Magic flute ' provide 
 theme ) , Hillemacher ' Aria dans le style Ancien ' 
 ' Gavotte Tendre ' — engaging thing — exploit , 
 Jaubert ' Lament , ' Kach ' Musette , ' Schumann 
 ' Chant du Soir , ' Max Bruch ' Danses Suédoises , ' 
 Fauré * Elégie ' ' Sicilienne 

 M. Brailowsky , russian Chopin player , 
 Le Courrier Musical claim fine 
 Chopin interpretant - day , artist 
 Paris indebted . recital display 
 technique little short perfection , 
 understand composer . 
 particularly noticeable reading ' Etudes , ' 
 demi - teintes ( tone strong appeal 
 french ) enthusiastically acclaim discern 
 public . similar success await M , Jacques Thibaud , 
 , musical Parisian declare , remain long 
 America . virtuoso doubt lead 
 french violinist , astonishing execution ally 
 beauty steadiness tone seldom hear , 
 M. Thibaud recital exception , , 
 inclusion Vivaldi Concerto programme , 
 critic furnish list 
 composition play m place 

 ROME 
 AUGUSTEUM 

 January 2 visit Rome 
 violinist need introduction english reader — 
 Joseph Szigeti , famous young Hungarian , actual 
 successor Marteau Heerman Geufer- 
 Conservatorium . programme concert , direct 
 Signor Bernardino Molinari , comprise   Vitali 
 Chaconne , violin , string organ , arrange 
 O. Respighi ; Orefice ' Laudi Francescane , ' orchestra ; 
 Beethoven Violin Concerto D. ' Laudi , ' 
 new work professor composition Milan 
 Conservatory , prove dismal failure . programme 
 explain inspire famous Canticle 
 St. Francis Assisi , ' renounce appear- 
 ance instrumental mean order sustain 
 simplicity expression austere sentimentality . ' 
 matter fact , prove void idea , found 
 badly - treat imitative harmony , wearisome 
 monotonous persistent hammering inconclusive 
 unoriginal motive . regret 
 work present concert way 
 huge success . Szigeti play mastery 
 vigour captivate entrance great audience 

 _ — _ 

 January 9 programme direct Signor 
 Victor de Sabata , young Triestino 
 - know composer , barely thirty year 
 old . opera , ' Il Macigno , " successful issue 
 Scala 1916 , , , 
 great impression appearance 
 roman public . gesture lean exaggeration , 
 foseur , , interpretation 
 Franck Strauss reveal potentiality . 
 programme comprise ' magic flute ' Overture , Sibelius ’ 
 symphonic poem , ' swan Quonela , ' De Sabata 
 symphonic poem , ' Inventus , ' movement Franck 
 * Psyché , ' Pick - Mangiagalli ' Voci ed ombre del Vespero , ' 
 Strauss ’ ' Don Juan 

 immense success gain Ernest Wendel 
 second visit Augusteum . Sunday concert 
 draw record house , turn away , 
 week - day concert , - attend , receive 
 equal enthusiasm . , Wendel 
 conduct easily forget experience , particularly 
 interpretation Beethoven . critic hesitate 
 assert reading ' Pastoral Symphony ' 
 equal Augusteum . Wendel 
 queer little way enjoy music direct — 
 listen orchestra , distribute approving 
 nod smile satisfaction , time ardent 
 personality engage radiate music . 
 programme include ' Egmont ' Overture , Reger 
 variation Fugue string theme Mozart , 
 ' Pastoral Symphony , ' ' Oberon ' Overture 

 concert season Rome enjoy great prosperity , 
 hall usually fill notwithstanding 
 prevalent high price . Scala important series 
 concert include Burmeister , Casella , 
 Casimiri . visit Richard Burmeister Rome 
 awaken great interest , owe fact 
 Liszt pupil . wield wonderful technique , 
 interpretation lack warmth . far 
 good programme Liszt playing 

 concert Casimiri entirely dedicate 
 polyphonic music Palestrina school , 
 splendid success — programme 
 repeat day later . contain following 
 work Palestrina : ' Laudate Dominum ' ( voice ) , 
 ' o quantus luctus ' ( voice ) , * vox dilecti mei ' ( 
 voice ) , ' introduxit rex ' ( voice ) , ' Nigra sum ' ( 
 voice ) , * bonum est confiteri ' ( voice ) , , 
 addition item , ' estote fortes . ' ( voice ) , 
 Marenzio , Holy Week Responses 
 Ingegneri , * velum templi scissum est ' ( voice 

 hall M. Alfred Casella act pianist 
 concert young violoncellist , Signor Livio Boni , 
 present Bach Sonata D Beethoven Sonata 
 

 Philharmonic Society Rome inaugurate season 
 grand concert honour Sgambati , 
 Society hall , memory ( die 
 1914 ) prize Rome . pianist Signor 
 Giuseppe Cristiani , professor Accademia di 
 Sta . Cecilia , foremost Sgambati 
 pupil . Madame Mendicini Pasetti vocalist , 
 programme , exclusively devote Sgambati , 
 follow 

 5 Pianoforte.—Concerto , Op . 15 ( accompaniment 
 second pianoforte 

 concert Trio 
 Academy , represent Signori Cristiani ( pianoforte ) , 
 Luccarini , Rosati , respectively violin violoncello 
 Augusteum orchestra . programme 
 devote Beethoven . second programme include , 
 Beethoven Trio , Strauss ’ Sonata , Op . 18 , 

 4 . vocal 

 concert month Philharmonic 
 devote modern italian music , good 
 success , programme follow : 
 sketch orchestra pianoforte : ( 1 . ) " Death 
 chamber ' ; ( 2 . ) ' Children Garden ' 
 Storti ( Warsaw , 1873 ) . 
 sonata instrument . / ° . di Donate ( Rome , 1877 ) . 
 * Vita d'Infanzia , ' suite instrument 
 G. C. Paribeni ( Rome , 1881 ) . 
 + se \ orchestra Reggio Calabria , 1875 . 
 " Ave Maris Stella . " latin hymn choir , solo , 
 orchestra wae Licinio Refice ( Rome , 1885 ) . 
 mention excellent concert 
 auspex Amici della Musica Society . 
 , Signor Mario Corti , - know violinist , 
 hear Ildebrando Pizzetti Sonata ; group illustrative 
 18th century chamber music Porpora , Ferrari , Veracini , 
 Pugnani , Chiabrano ; * Kreutzer ’ Sonata . 
 second concert Quartet 
 Society , splendid success , 
 notable quartet concert Rome year . 
 work play Beethoven Op . 18 , . 5 , 
 V. di Donato Sonata , Op . 4 ( violoncello pianoforte ) , 
 Schubert Trio , Op . 100 ( pianoforte , violin , 

 violoncello ) . LEONARD PEYTON 

