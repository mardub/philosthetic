Rose Maiden , ” “ Bride Dunkerron , ” & c. ; ‘ “ ‘ Stabat Mater ” MADAME JULIA HUTCHINGS . 
 ( Rossini ) , ‘ Imperial Mass ” ( Haydn ) , ‘ Twelfth Mass ” ( Mozart 

 Requiem ” ( Mozart ) ; Haydn , Hummel , Mozart , Beethoven 
 sda Totets , Cherubini Mass C , “ ‘ O fons amoris , ” “ Alma MR . A. BOWYER BRISTOL 

 Ave Maria , ” & c. 
 Te ampicte vépertoire , Press opinions , photo , & c. , address , 239 , MR . EDMUND EDWARDS . 
 Upper Parliament Street , Liverpool ; , Agent , N. Vert , Esq . , 
 6 , Cork 

 Professor E. H. TURPIN , Mus . D. 
 Director STUDIES : 
 Professor BRADBURY TURNER , Mus . B. 
 DrirECTOR EXAMINATIONS : 
 Professor JAS . HIGGS , Mus . B 

 ORCHESTRAL CONCERT place Princes ’ 
 Hall , Piccadilly , SATURDAY , July 15 , 8 o’clock . Conductor , Mr. 
 F. CORDER . programme include : Suite Orchestra , 
 “ Italian ” ( Raff ) ; Concertstiick Pianoforte Orchestra ( Schu- 
 mann ) ; Concerto Violin Orchestra , . 6 ( De Beriot ) ; Con- 
 certos Pianoforte Orchestra F minor , . 4 ( Sterndale 
 Bennett ) , E flat , . 5 ( Beethoven ) ; Saltarello Orchestra 
 ( Gounod ) ; Rhapsodie Pianoforte ( Liszt ) ; Song , ‘ ‘ Reaper 
 Flowers ’ ( Cowen ) ; MS . Song ( Ketelbey ) ; Aria , “ Softly 
 sighs ” ( “ Der Freischiitz ” ) ( Weber 

 Tickets ( price 5s . 2s . 6d . ) undersigned . 
 Past Students Diplomés College granted usual 
 privileges tickets 

 audience Teutons ; title - réle 
 filled — word , way , having significance 
 connection — Mr. Ramon Blanchart . 
 doubted , , performance 
 suffered materially change . attitude 
 public cool Verdi masterwork , 
 musicians present nigh unanimous 
 expressing warmest admiration work 

 Stupvents Darwin need reminded 
 fact evolution , attended 
 curious phenomenon “ reversion ” original 
 parent type . instance , half - - century 
 arrived practically unanimous recogni- 
 tion genius Richard Wagner . result 
 achieved , startling observe , 
 recent cases , cropping old 
 Adam — old spirit ignorant prejudice 
 intolerance . useful little treatise ‘ “ Form 
 Design Music , ” intended use 
 amateurs , author , Mr. Heathcote Statham , 
 occasion attempt definition melody . 
 fails conspicuously blame ; 
 succeeded . Unluckily tries 
 examples good bad melody , matter 
 personal taste apt warp judgment . 
 quotations Bach , Mozart , Beethoven , 
 says : “ instructive contrast reader 
 cacophonous string notes given Ex . 9 , 
 forth melody , analogy 
 structure quoted 
 mere fact consists succession notes ; 
 , , common law rhythm 
 tonal relation , definite form balance 
 ; passage , speak , 
 beginning , middle , ending organic sense , 
 reason wriggle 
 fashion indefinitely : formless 
 thing . ” italics ; phrase alluded 
 , think , cor Anglais solo 
 ‘ ‘ Tristan ” violin passage prefaces 
 scene “ Siegfried , ” pathetic “ Herze- 
 leide ’ ? motive “ Parsifal 

 Sess SS S25 
 cement 

 RICHTER CONCERTS 

 TuaT success admirable Concerts shows 
 sign waning plain aspect St. James 
 Hall evening 5th ult . , , , 
 greeting Dr. Richter appearance ¢alled forth . 
 glad , precious qualities 
 Richter conducting precisely conspicuous 
 orchestral conductors generally , qualities 
 , interests interpreters listeners alike , 
 placed evidence . refer 
 particularly perfect ease transitions 
 effected degree extremes 
 force ; wonderful sense freedom produced 
 slight imperceptible changes speed , 
 prominence characteristic rhythm 
 accentuation musical phrase , , 
 occasion , bar ; finally admirable way 
 members orchestra trained 
 respond immediately suggestion chief . 
 find reason satisfaction somewhat limited 
 vépertoire programmes Concerts 
 selected , especially responsibility rests 
 public Concert - givers . 
 occasion question C minor Symphony 
 Beethoven , Wagner “ Meistersinger ’’ Overture ‘ ‘ Good 
 Friday ” music ‘ ‘ Parsifal , ” ’ Liszt Hungarian 
 Rhapsody . Smetana pretty picturesque Symphonic 
 Poem “ Vitava , ” heard England Crystal 
 Palace , March 5 , 1881 , place scheme . 
 welcome , account com 

 imposing , , 3,000 ” men 

 PIANOFORTE RECITALS 

 Tue Recital Madame Berthe Marx , St. James 
 Hall , 27 , pass brief record . Beethoven s 
 Sonata F minor-(Op . 57 ) , ‘ “ Appassionata , ” 

 principal feature programme ; delicacy 

 purity style characterise French artist 
 displayed greater advantage clavecin pieces 
 Couperin , Daquin , Rameau , Mendelssohn 
 “ Lieder ohne Worte , ” ’ Bach Preludes Fugues 
 C minor D major , book ‘ Das 
 Wohltemperirte Clavier , ’ minor pieces Schubert , 
 Schumann , Chopin , Weber , composers 

 crowd Recital - givers , M. Diémer 
 overlooked month , won slight favour 
 London amateurs omission 
 consequence . performance , 17 , 
 offended small audience ridiculous transcription 
 Mozart Overture “ ‘ Die Zauberfléte , ” manner 
 rendering clavecin pieces Couperin , Daquin , 
 Rameau modern satisfaction 
 study antiquarian music keyboard . 
 hand , excellent technique displayed Beethoven 
 Variations C minor Chopin Fantasia F 
 minor ( Op . 49 ) . second Recital , 29 , M. 
 Diémer gave French performance Schumann 
 Etudes Symphoniques , interpretation pieces 
 Chopin lacked distinction poetic feeling . regards 
 technique , , playing Recitals well- 
 nigh faultless , fact goes far account great 
 esteem held Paris teacher 

 31 Miss Margarethe Eussert gave second 
 Recital Princes ’ Hall , heard advantage 
 minor pieces Scarlatti D’Albert , 
 unsatisfactory Chopin selections , nervous 
 Beethoven Variations C minor Bach Chromatic 
 Fantasia Fugue , heard 
 frequently season 

 Friday , 2nd ult . , little Raoul Koczalski 
 appearance St. James Hall , playing 
 confirmed striking impression , 
 extent second Recital Princes ’ Hall . 
 gave highly finished rendering Beethoven early Sonata 
 C minor ( Op . 10 , . 1 ) , playing slow movement 
 marked extraordinary poetic feeling . Equally , 
 impressive interpretation Chopin Nocturne 
 D flat , Marche Funébre , 
 taken unusually slow pace . pieces Mendels- 
 sohn , Schubert , Tschaikowsky , Moszkowski , Paderewski , 
 played consummate excellence , 
 leaving account tender age executant 

 days later , 6th ult . , plat- 
 form St. James Hall occupied infant 
 prodigy , time little girl named Frida Simonson , 
 concerning puff pre- 
 liminary . Having amused audience curious 
 little bow , repeated frequent intervals , 
 seated instrument went 
 number selections , adapted 
 present physical means ; especially harpsichord 
 pieces Bach , Handel , Scarlatti , composers 
 displayed remarkably pure singing touch wonderful 
 neatness execution . Obviously , , 
 grasped spirit modern music , 
 promptly withdrawn public life unquestion- 
 able talents permitted develop natural healthy 
 manner 

 performance legitimate character given 
 Miss Fanny Davies following afternoon . 
 treat hear young English pianist Schu- 
 mann music , exception , , Mr. 
 Leonard Borwick , thoroughly enters 
 spirit German master executant 
 public . interpretation beautiful 
 “ Kreisleriana ’’ occasion remarkable 
 intellectual strength poetic feeling . Chopin 
 Preludes , including numbers frequently heard , 
 rendered equal finish expression . 
 Mendelssohn Prelude F ugue B minor Op . 35 
 smaller pieces composers included 
 programme , charming little 
 sketch called “ La Gondola , ” L. Heritte Viardot 

 iss Marguerite Hall contributed songs Beethoven , 
 Max Bruch , Goring Thomas , Brahms 
 artistic effect 

 Miss Emma Barnett annual Recital took place 

 following afternoon , Princes ’ Hall . rendering 

 Beethoven Sonata ( Op . 101 ) Schumann 
 “ Papillons ” ( Op . 2 ) remarkable ; special 
 mention new series ‘ * Characteristic 
 Studies , ” appropriate titles , brother , Mr. J. F. 
 Barnett . agreeable little sketches , useful 
 practice , piquant melodious . Madame Clara 
 Samuell happy choice English songs 
 execution 

 gth ult . Princes ’ Hall occupied Mr. 
 Edgar Hulland , sound conscientious , specially 
 brilliant executant . displayed excellent technique 
 Beethoven difficult Variations Fugue E flat , 
 Rubinstein pretentious satisfactory 
 Sonata violin pianoforte B minor ( Op . 98 ) , 
 associated Mr. Emile Sauret . Smaller 
 pieces composers included 
 programme 

 Mr. Buonamici , - known Florentine pianist , 
 heard previous occasions , gave 
 Recital Princes ’ Hall 13th ult . Mr. Buonamici , 
 remembered , pupil Liszt bears 
 impress master remarkably delicate touch . 
 exhibited Beethoven “ ‘ Waldstein ” Sonata , 
 Rondo G Op . 51 , Minuets ; 
 shown greater advantage Chopin 
 Studies pieces Raff 

 Mr. Stojowski , gave Recital 16th ult . , 
 appeared years ago , attract 
 attention . interim , , 
 compositions won favour account 
 unusually large audience gathered St. James Hall . 
 said Mr. Stojowski playing showed 
 individuality style expression . touch light 
 delicate execution neat 
 refined , manner performance somewhat frigid 
 impassive . programme peculiarly con- 
 stituted , prominence given Slavonic composers 
 Paderewski , Tschaikowsky , Zelenski , Moszkowski ; 
 jit certainly interesting account . 
 piece length , , Sonata G 
 pianoforte violin , Mr. Stojowski pen , 
 assisted compatriot , Mr. Gorski . 
 bright animated work movements , 
 , theme variations , cleverest , Slavonic 
 character perceptible phraseology . 
 smaller pieces , including numbers set 
 Danses Humoresques ( Op . 12 ) , pleased greatly 

 respect public appreciation , Recital ot 
 Mr. Paderewski , took place 2oth ult . , ought 
 command special attention ; 
 programme lengthy criticism applicable . 
 enormous audience filled St. James Hall 
 end end drawn simply magic 
 Polish artist , fair add 
 fully justified confidence musical amateurs 
 repose present occasion . extra- 
 vagance marred efforts appeared 
 London longer perceptible , fault 
 playing present occasion , accentuated 
 delicacy manipulation Beethoven Sonata E 
 flat ( Op . 31 , . 3 ) numbers 
 masculine vigour desirable . Schumann “ Papillons ” 
 delivered incomparable piquancy touch 
 general execution , charming sense 
 individuality rendering small Chopin pieces , 
 including Study F ( Op . 25 ) Waltz flat 
 ( Op . 34 ) , warmly encored . close ot 
 Recital ’ prolonged demonstration , 
 resulted seven recalls encores . Mr. 
 Paderewski certainly popular pianists 
 present public 

 words approval given Miss Verne , 
 known Miss Mathilde Wurm , gave Recital 
 St. James Hall 23rd ult . played sound 
 conscientious manner , insufficient warmth ot 
 expression , Schumann Fantasia C , minor 
 pieces Chopin , Saint - Saéns , Liszt , Weber , L. Heritte 
 Viardot , Bach ‘ Italian ’ ? Concerto , 
 best - named work 

 410 MUSICAL TIMES.—Juty 1 , 1893 

 musician best Mozart second 
 Fantasia C minor , played delicacy 
 finish music demands . Beethoven early 
 Mozart - like Sonata E flat ( Op . 7 ) played 
 needful refinement , programme included 
 Schumann Fantasia C ( Op . 17 ) works Chopin , 
 Weber , Liszt . Variety afforded 
 musicianly pleasing new songs vocal duet 
 pen Mr. Bonawitz , received justice 
 Miss Sylvia Wardell Mr. Arthur Oswald 

 CHAPEL ROYAL CONCERT 

 ROYAL COLLEGE MUSIC 

 chronicle Students ’ Concerts 
 month , heard performances excep . 
 tional interest . Chamber Concert , 8th ult . , 
 opened Beethoven ‘ ‘ Storm ” ’ Quintet strings ( Op . 
 29 ) , artistically played Misses Jessie Grimson , Lilian 
 Wright , Messrs. Leonard Fowles , William Ackroyd , 
 Thomas Hill . Miss Marie Motto distinguished 
 greatly rendering , alternately spirited expressive , 
 second book Max Bruch charming Swedish 
 Dances violin , Miss Blanche Wyatt sang 
 Schubert song ‘ “ ‘ Der Hirt auf dem Felsen ” tunefully 
 fluently , somewhat coldly . chief interest 
 Concert , , lay appearance ot 
 Mr. Albert Archdeacon , young singer highest 
 promise . speak advisedly 
 heard remarkable performance 
 Concert singing Wolfram ‘ “ Address 
 Evening Star , ” ‘ “ Tannhauser . ” voice 
 beautiful quality , intonation absolutely perfect , 
 sings ease finish masterful 
 expression astonishing student . Mr. 
 Archdeacon brilliant future 

 16th ult . Orchestral Concert given St. 
 James Hall , programme included Schumann 
 ‘ ‘ Manfred ’ Overture ( striking proof 
 nearly author inspired mood approached 
 Beethoven ) , Bach Clavier Concerto E , neatly played 
 Miss Katharine M. Ramsay , \Bruch Violin Con- 
 certo ( Op . 58 ) , Berlioz exquisitely orchestrated song 
 ‘ ‘ Spectre Rose , ” ‘ ‘ Nuits d’été , ” 
 Beethoven Romantic Symphony ( . 7 ) . 
 equal best heard College , 
 performances orchestra , especially 
 strings , distinguished precision attack , 
 careful phrasing , admirable spirit . second 
 violins , numerically stronger firsts , 
 inferior tone , fact 
 noticeable imitative passages Finale 
 Symphony . remedied future occa- 
 sions . Miss Grimson performance Bruch 
 Concerto spoken occasion ; 
 respect deserving high commendation ; 
 technique especially excellent . Miss Clara Butt 
 singing Berlioz lovely song worthy 
 rapidly - grown - deserved reputation . 
 hardly improved . Professor Holmes con- 
 ducted 

 ROYAL ACADEMY MUSIC 

 usual College Concerts , 

 interest programmes great . 
 day miscellaneous programmes ought past ; far 
 raising interest audience concerned 
 past . better provided University , 
 Brasenose , Exeter , Worcester , Pembroke , Magdalen 
 Concerts , - named College certain unity 
 maintained giving madrigalian character 
 performance . noticed Handel 
 “ Acis Galatea ’ ? given Keble College 
 Concert . Merton ( 25 ) Cowen ‘ ‘ St. John Eve ” 
 received fairly satisfactory rendering . 
 ae responsible arrangements 
 realised skeleton band 
 content recent years mistake , great improve- 
 ment observed present occasion . 
 plenty room amendment direction . 
 Queen College Concert ( 26 ) band 
 chorus excellent , capital performance 
 Markull “ ‘ Roland Horn ” given . chief event 
 Concert , chief musical event 
 term , production new Symphony F major 
 Josef Ludwig . proved work admirable 
 respect . beginning end instinct 
 vigorous power , rising unfrequently height 
 Beethovenish peculiar force direct- 
 ness . Ata hearing slow movement , 
 magnificent climax , Scherzo best move- 
 ments ; effect movement damaged 
 unlucky slip wind , judgment require 
 revision . Considering circumstances , rendering 
 good , composer , conducted , 
 enthusiastically applauded close . 
 Symphony deserves probably secure wide accept- 
 ance 

 MUSIC CHICAGO . 
 ( CORRESPONDENT 

 Tue Finsbury Choral Association congratulated 
 establishment new Metropolitan College 
 Music Holloway . Association time 
 enterprising kind , 
 labours culminated formation Institu- 
 tion calculated genuine advantage art . 
 increase number students years 
 160 considerably 500 rendered necessary larget 
 premises originally occupied teaching 
 branch Association , accordingly , soon 
 possible , removal effected satisfactory 
 quarters . Mr. G. W. Dale Principal new 
 College , Lord Mayor declared open 
 23rd ult . course preliminary proceedings 
 Dr. E. H. Turpin spoke immense strides 
 music late years result tuition great schools , 
 Sir Joseph Barnby stated Institution 
 City presides 3,50 
 students — astounding fact , considering Guild . 
 hall School situated midst busy hauntsof 
 men 

 interesting Orchestral Concert St 
 James Hall 30 , giver performance 
 Mr. Schénberger , sound forcible performance df 
 Beethoven Pianoforte Concerto G ( . 4 ) Mt 
 Saint - Saéns favourite work G minor ( . 2 ) showed 
 young executant making steady progres 
 principal feature interest , , st 
 performance Symphony E minor , Mr. Eman 
 Moor , young musician came notice 
 years ago asa pianist somewhat sensational type . 
 Symphony noteworthy pleasing characttt 
 themes excellence workmanship , con 
 struction movements t 

 unsat 
 consi : 
 able r 
 ability 
 music 
 rende ! 
 progr 

 D. Wetton . vocal quartet consisted Masters W. 
 Gough A. Lee , Mr. Probert Mr. Blackney , , 
 solo , duet , quartet , gained golden opinions 
 attentive critical audience . united forces 
 conducted Mr. Thomas Adams , accomplished 
 Organist Church indefatigable trainer 
 b»ys — rough diamonds picked regions Leather 
 Lane Baldwin Gardens 

 South Hampstead Orchestra held eighth annual 
 Concert 6th ult . , Hampstead Conservatoire , 
 able conductorship Mrs. Julian Marshall , 
 complete control forces manifested 
 Brahms Symphony D ( . 2 ) , Beethoven ‘ ‘ Leonora ” ’ 
 Overture , portion Schubert ‘ “ ‘ Rosamunde ” ’ ballet 
 music , Mr. Hamish MacCunn Overture ‘ ‘ Land 
 Mountain Flood . ” performance 
 marked precision conscientious regard 

 _ minute shades expression . leader 
 orchestra Madame Charlotte Wilkes ( Mrs. Stanley ) . 
 B : ethoven Romance F violin rendered 
 neatness judgment Miss Susan Lushington , 
 Miss Evangeline Florence varied instrumental pieces 
 refined singing Weber Romance ‘ “ Und ob die 
 Wolke , ” Handel “ Lusinghe pit care , ” Henschel 
 charming ‘ Spring Song , ” specially 
 complimented large audience 

 Master expressed appreciation work 
 steadfast faith German “ Geist , ” gave 
 sttength completion gigantic task 

 set . paper contains beautiful 
 Prologue Festival held Beethoven Haus 

 n ( Festival , way , resulted net 
 Profit 20,000 marks ) . author Ernst von Wilden 

 Signor Barbini , Conductor Russian Italian 
 Opera Panaieff Theatre , St. Petersburg , com- 
 pleted opera , ‘ ‘ Falcoda rupe , ’’ shortly 
 heard said Theatre 

 Beethoven colossal ‘ ‘ Missa Solemnis ’’ lately 
 performed Bayreuth , conductorship Herr 
 Julius Kniese . choruses sung local 
 Singverein , military bands supplied orchestra . 
 remembered Bayreuth large village 
 20,000 inhabitants , : achievement 
 considered remarkab'e 

 great Italian dramatic soprano , Signora Bellincioni , 
 created utmost enthusiasm con- 
 tinental towns , recently Berlin , 
 wonderful impersonations Santuzza ‘ Cavalleria , ” 
 Christina “ Mala Vita , ” & c. , taken daring step 
 playing Figaro Rossini ‘ ‘ Barbiere , ” ’ , strange 
 relate , assumption typical baritone 
 sides acknowledged enjoyable 
 performances conceivable 

 celebrate tercentenary death Orlando di 
 Lasso , took place 1594 , decided 
 arrange days ’ international musical Festival 
 Tournament year Mons , birthplace great 
 Flemish musician . Invitations partake Tournament 
 sent choral societies Belgium 
 abroad , hoped great number 
 accept 

 2nd 3rd inst . Hesse Palatinate 
 Musical Festival place Worms . 
 works performed ‘ “ ‘ Messiah , ” ‘ “ Choral ” 
 Symphony , “ Song Praise ’ ( Friedr . Gernsheim ) , 
 Beethoven Violin Concerto , selections Wagner 
 ‘ ¢ Walkire , ” ’ & c 

 Wagner “ * Walkire ’’ continues draw crowded houses 
 Paris Grand Opéra ; fact , receipts 
 largest known , 23,000 francs 
 taken evening . prevent possibility 
 postponement necessary 
 artists falling ill , understudies beea pro- 
 vided . intense interest taken work 
 shown enormous number articles ujon 
 appeared , appearing , French 
 press . appreciative , 
 reverse , supply amusing 
 reading 

 Dal Verme Theatre , Milan , new - act opera , 
 ‘ ‘ Spartaco , ” Pietro Piantania , director local 
 Conservatoire , produc.d time 13 
 success 

 Beethoven ‘ ‘ Eroica ” ” Symphony recently played 
 Venice time public ! Fortunate Venetians , 
 experience wonderful sensation listening 
 ‘ ‘ novelty 

 Berlin Philharmonic Choir , enter- 
 prising Institutions found , , usual , 
 perform number new compositions 
 winter season . important novelties 
 Rubinstein sacred opera ‘ ‘ Moses , ” cantatas , 
 ‘ ‘ Feuerreiter ” ’ “ Elfengesang , ” Hugo Wolf , 
 “ Heldenkiage , ” soli , chorus orchestra , Wilhelm 
 Berger . Herr Siegfried Ochs Conductor 
 Society 

