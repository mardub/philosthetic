BAND CHORUS 300 

 PROGRAMME . 
 WEDNESDAY EVENING , Sept. 17 . — ‘ ‘ Elijah ’’ ( Mendelssohn ) . 
 THURSDAY AFTERNOON . — Miscellaneous , Stanford ‘ Re- 
 venge , Tschaikowsky ‘ ‘ Symphonie Pathétique . ’’ Songs , Miss 
 Ada Crossley . 
 THURSDAY EVENING . — “ Faust ” ’ ( Berlioz ) . 
 FRIDAY AFTERNOON.—Miscellaneous Selection , 
 Beethoven . 7 Symphony . 
 FRIDAY EVENING . — ‘ Messiah " ’ ( Handel ) . 
 communications addressed , Hon . Secretaries , Musical Festival , 
 Scarborough 

 BRISTOL TENTH MUSICAL FESTIVAL 

 THURSDAY , Oct. 9.—1 o'clock , ‘ ‘ Coronation Ode " ( Elgar ) , 
 ‘ St. Christopher ’’ ( Parker ) ; 8 p.m. , ‘ ‘ Hiawatha " ’ ( Coleridge- 
 Taylor 

 FRIDAY , Oct. 10.—1 o'clock , ‘ ‘ Trauer - Marsch , ’’ ‘ ‘ Gotterdam- 
 merung " ’ ( Wagner ) , ‘ ‘ Requiem ” ’ ( Berlioz ) , ‘ ‘ Wotan Abschied " ’ 
 ( Wagner ) , ‘ ‘ Bergliot ' ’ ( Grieg ) , ‘ ‘ Overture , 1812 ' ’ ( Tschaikowsky ) , 
 & c. ; 8 p.m. , ‘ ‘ Emperor Concerto ” ( Beethoven ) , Polish Fantasia 
 ( Paderewski ) , Overtures ‘ ‘ autumn ’ ” ’ ( Grieg ) , ‘ ‘ Tannhauser ” ’ 
 ( Wagner ) , Songs , & c 

 SATURDAY , Oct. 11.—2.30 p.m. , ‘ ‘ Messiah 

 August great holiday month year , 
 musicians forsaking scene 
 daily round common task order obtain 
 change rest , possible 
 waiters change hotel - keepers 
 rest . facilities cheapness 
 present - day travel compared times , 
 experience eventful holiday 
 enjoyed Sir George Smart - quarters - - a- 
 century ago . turn Morning Post 
 October 8 , 1825 , shall discover following 
 information 

 Sir George Smart , making tour Germany , 
 visited Weber , Baths Ems 
 recovery health . Munich dined 
 veteran Winter natal day , completed 
 rrst year . Baden , near Vienna , kindly 
 entertained Beethoven , country house ; 
 gratified learn highly respectable [ ! 
 gentleman received profes- 
 sional brethren Continent respect 
 kind attention justly merits 

 Smart conducted ( pianoforte , 
 baton ) performance ‘ Choral 
 Symphony ’ England , given Philharmonic 
 Society March 21 , 1825 . experience 
 difficulties attending interpretation 
 existing conditions ‘ conducting ’ induced 
 Vienna order obtain right ¢empos 
 Beethoven . September 6 , Smart 
 dined Beethoven , repast 
 particularly asked host play 
 recitative passages connect movements 
 predecessors . Smart received 
 lesson great composer , Canon , 
 autograph dated ‘ September 16 , 
 1825 , Baden , near Vienna . ’ addition Weber 
 Beethoven , Sir George Smart called 
 Mendelssohns , Berlin , Spohr , Cassel , 
 recreative peregrinations — said . truly 
 eventful holiday 

 Sir Smart . ’ Beethoven called , 
 English visitor author ‘ Choral 
 Symphony ’ received Baden year 1825 . 
 Harmonicon contains little known 
 exceedingly interesting letter written English 
 lady Vienna October year . 
 writer given , 
 reason doubt genuineness . reply 
 note introduction sent , wrote 

 Avec le plus grand plaisir . je recevrai une fille de 
 BEETHOVEN 

 rest sufficient let fortunate 
 young lady speak 

 arrived , Beethoven returned home 
 shower rain , changing coat . 
 began alarmed -- heard 
 brusquerie — lest receive cordially : 
 came forth sanctum , hurried step 
 apparently nervous ; addressed 
 gentle , courteous , sweet manner , 
 truth sweetness , know Mr. 

 compared ; resembles , 
 features , person , address , opinions 

 Beethoven short , extremely thin , suffi- 
 ciently attentive personal appearance . observed , 
 — fond Handel ; 
 /oved ; proceeded time eulogizing 
 great composer . conversed writing , 
 found impossible render audible ; 
 clumsy mode communicating , 
 signify , talked freely 
 willingly , wait questions , 
 expect long replies . ventured express admiration 
 compositions , , , praised 
 Adelaide , terms means strong sense 
 beauties . mcdestly remarked , 
 poetry beautiful 

 Beethoven speaks good French — comparison 
 Germans — conversed little 
 — — — Latin . told sfoken 
 English , deafness prevented acquir- 
 ing language power reading 
 said preferred English French writers , 
 ‘ ils sont plus vrai . ’ Thomson favourite 
 author , admiration Shakspeare great 
 

 retire , desired stop 
 went 
 table adjoining room , wrote lines 
 music — little fugue pianoforte — presented 
 amiable manner . 
 desired spell , 
 inscribe impromptu correctly . took 
 arm led room written , 
 apartment , 
 author , perfectly clean ; 
 indicating like superfluity wealth , shew 
 want useful furniture neatness 
 arrangement . recollected , , 
 country residence , Viennese 
 costly particular domestic details 
 English . Jed gently room 
 , placed grand pianoforte , 
 presented Messrs Broadwood : looked , 
 thought , melancholy sight , said 
 order , country tuner 
 exceedingly bad . struck notes convince 
 placed desk page MS . music 
 given , played 
 simply ; prefaced chords — 
 handfuls notes ! gone 
 Mr. heart . stopped ; 
 account ask , found played 
 satisfaction 

 Je veux vous donner un souvenir de moi 

 Unfortunately , Beethoven set foot 
 British soil . welcome 
 

 death Mr. Thomas P. Chappell — 
 called forth expressions regret 
 personal worth influence referred 
 issue-—has drawn attention 
 introduction Gounod ‘ Faust ’ England . 
 believe correct stating 
 years ago Mr. Thomas Chappell , head 
 - known Bond Street firm , submitted 
 French publishers work Berlioz 

 Elijah ’ ( Mendelssohn ) , ‘ Revenge ’ ( Stanford ) , ‘ Sym 

 phonie Pathétique ’ ( Tschaikowsky ) , ‘ Faust ’ ( Berlioz ) , 
 Miscellaneous Selection ( Wagner ) , Symphony . 7 
 ( Beethoven ) , ‘ Messiah ’ ( Handel ) . 
 Dr. Thomas Ely Mr. Patman undertaken 
 duties chorus master , band 
 chorus , consisting 300 performers , 
 experienced direction Dr. Frederic Cowen 

 Coronation honours included conferring 
 Baronetcy Sir Charles Hubert Hastings 
 Parry , Knighthood Professor Charles 
 Villiers Stanford 

 PIANOFORTE MUSIC . 
 Novello School Pianoforte Music . 
 Klindworth . Nos . 1—14 . 
 ( Novello Company , Limited 

 Professor Karl Klindworth household 
 word , England , Continent 
 America , careful artistic editor pianoforte 
 music . edition Chopin known asa 
 painstaking piece work , thoroughness 
 seeking truth paramount . 
 features characterise series pieces — fourteen 
 number — form basis Novello School 
 Pianoforte Music . foundation good , 
 necessary mention names Beethoven , 
 Bach , Handel , Haydn , Mozart , Hummel , Mendelssohn , 
 Schubert , Weber . best known gems o ! 
 creative output masters provided 
 Professor Klindworth minuteness phrasing 
 indications — important desideratum — careful 
 fingering . , special attention given 
 accuracy text . passing judgment commenda- 
 tion addition practical pianoforte music , 
 better conclude quotation 
 Preface series : ‘ fundamental principles 
 good editing combined important , 
 neglected essential artistic presentment 
 music eye . Special attention accordingly 
 paid clearness , especially direction o ! 
 ample spacing notes , legibility , , far 
 possible , providing convenient turning - places — 
 important matters materially assist player 
 study pieces contained series . 
 annotations Editor , use greatly 
 aid students proper interpretation works , 
 speak 

 Edited Karl 

 Dondon Concerts 

 concert exceptional interest given Mdlle . 
 Marie de la Rouviére , 8th ult . , Bechstein Hall , 
 lady soprano vocal quartet La Scola 
 Cantorum de Paris , institution chiefly devoted 
 practice performance music past centuries , 
 members quartet Mdme . J. de la 
 Mare , M. Jean David , M. Gébelin , inter- 
 pretations given ‘ Dialogus la Pascua , ’ fine 
 example Schiitz seventeenth - century church music , 
 Beethoven noble ‘ Chant Elégiaque , ’ testified 
 painstaking intelligent rehearsal . Mdlle . Rouviére 
 powerful soprano voice excellent style . 
 gave dramatic rendering Gluck ‘ Songe 
 d'Iphigénie , ’ introduced attractive songs 
 collective title ‘ Le Berger Fidéle , ’ J. Rameau 

 Miss Alice Hollander , possessor genuine con- 
 tralto voice , gifted manifest artistic 
 sensibility , successful début country 
 concert 2nd ult . St. James Hall , 
 induced second place 
 8th ult . , sympathetic singing confirmed 
 good opinions previously expressed 

 Iscu_t.—A monument - erected Brahms , 
 garden villa belonging Johann 
 Strauss , master frequent visitor 

 Leipzic.—The - known Academical Choral Society , 
 present conductorship Herr Heinrich 
 Zoellner , celebrated eightieth anniversary 
 foundation , 13th ult . , concert sacred 
 music.——Dr . Carl Reinecke , celebrated 
 seventy - eighth birthday , retired rst ult . 
 post director studies Conservatorium . 
 veteran composer teacher succeeded 
 institution Herr Arthur Nikisch _ — — Max Klinger 
 discussed Beethoven statue secured , 
 aid influential committee , city . — -—The 
 death occurred recently Carl Piutti , 
 years highly - esteemed professor Conservatorium 
 successor Rust organist . St. Thomas 
 Church . wrote works instrument 

 MiLan.—Signor Sonzogno , - known impresario 
 music publisher , , 1890 , offered prize 
 - act opera awarded Mascagni ‘ Cavalleria 
 Rusticana , ’ opened international competition 
 - act lyrical drama , jury 
 international 

 hold responsible opinions expressed 
 sun ry , notices collated local papers 
 furnished correspondents 

 Croypon.—An interesting programme performed 
 students Croydon Conservatoire Music 
 orchestral concert , 11th ult . , able 
 direction Mr. S. Coleridge - Taylor . included 
 Hofmann Serenade ( Op . 72 ) ; Pieces : Allemande , 
 Sarabande , Cebell , Purcell ; Serenade ( Op . 20 ) , 
 Elgar ; Petite Suite pianoforte strings , Ole Olsen 
 ( pianoforte played Miss Jessie 
 Fennings Miss Van der Heyde ) , Gade Novelletten 
 ( Op . 53 ) . Miss Marjorie Ferguson , solo 
 pianist , played Beethoven Rondo Capriccio G major 

 Exeter.—On 17th ult . Service Praise held 
 Cathedral , occasion annual Festival 
 Exeter Diocesan Choral Society . fewer 
 - choirs , numbering 760 singers , took , 
 direction Dr. D. J. Wood Mr. T. 
 Roylands Smith . anthem Stainer ‘ Let 
 soul subject unto higher powers’—the congregation 
 joining hymn ‘ O King Kings ’ forms 
 climax anthem-——and Hopkins Te Deum B flat 

 Meyerbeer 

 ie Beethoven 
 . F. H. Cowen 
 Eaton Faning 

 Rossini 
 Handel 
 . Mendelssohn 

 MUSICAL TIMES.—Avcust 1 , 1902 . 9 

 eae ee : _ : : 
 Ihe Associated Board Examinations , 1902 
 4 9 eel 
 NOVELLO EDITIONS 
 PIANOFORTE . ee VIOLIN—(continued ) . 
 LOCAL CENTRE — JUNIORS . s. ¢. LOCAL CENTRE — SENIORS s. d 
 BacH.—Fugue E minor . - . 10 Book I. ( 48 Preludes FLoRILLO.—36 Etuden , Nos . 3 , 12 , 28 ate aaa int ) Oe 
 1 Fugues , Edited W. T. Best ) , sal ae .. 6 oO BEETHOVEN.—Sonata major , Op . 12 , . er Se 
 relay InC , . 43 ( F. Taylor s Studies , ‘ Book XXV . ) ax 8) O SCHOOL EXAMINATIONS — ELE -ME NTARY . 
 BEETHOVEN.—Sonata E , Op . 14 , . 1 ( Edited A. Zimmer- WILHELMJ.—Modern Violin School . Book Ia ... sini 
 mann , No.g ) ... . 2 0 WILHELMJ.—Modern Violin School . Book 2 
 Bacu.—Invention Din ts , Neck ri E \E dited Folk Dances Denmark . Nos . 4 3 7 ( WwW ilhe Imj 
 Higgs ) ce : ia 7 - sy ee School ) ... “ pee aaa 2 
 MenpELSSOHN.—Lied chne Worte , flat , Op . 38 , " . 6 SCHOOL EXAMINATIONS- — LE OWE R DIV ISION . 
 ( Lied . 18 ) ... Fe ee .. 0 9| Wu HELMJ.—Moder rn Violin School . BookIa ... 2 
 " ED ScHUBERT.—No 4 . C shi arp minor o * * Moments Musicals , ” Parry , C. H. H.—Twelve Short Pieces . Second Set ... = 
 Op . 94 ( Pianoforte Albums , . 49 ) 0 SCHOOL EXAMINATIONS — HIGHER DIV ISION . 
 CramerR.—In D minor , . 24 ( F. Taylor Studies , Book V II . ) ) 1 o Witnetmj.—Modern Violin School . Book IIIb ... es 
 CLremeNTI.—In , . 9 Gradus ( F. Taylor Studies , Warner , H. Watpo.—Elegie ... “ 1 aa 
 Book XV . ) = os ; dupe ew 
 BEETHOVEN.—Allegretto F minor , Sonata , “ Op . 10 , V IOLONCE L Lo . 
 N 2 ( Edited A. Zimmermann , . 6 ) 20 LOCAL CENTRE — SENIORS 
 Hanpe_.—Courante E minor , Suite IV . ( Pi Nnotanie MENDELSSOHN.—Last Movement , Sonata B flat ere owe 3 C 
 Albums , . 4 ) ren vee y ahi ae 2 - 0 waa — 
 gr ee SINGING . 
 LOCAL CENTRE- -SE NIORS . LOCAL CENTRE — SOPRANO 
 eee : ss ae . AL 22 — Ss RANO . : 
 Bac bd , om Ww 7 - 11 Book I. head Preludes Fugues , ( O worse death | 12 Soprano Songs , 
 seaiagenile ) ( 2 ) CLR ) MR + ute nee nee OO ) HanpeL.—-~- Angels bright- Handel Oratorios 2 
 MoscHELES.—In E orgie Op . 70 , . 8 ( F. Taylor ( fair . ( | ( Edited A. Randegger ) 
 Studies , Book VI . ) : ; mee + » O PuRCcELL.—Nymphs » pherds ( Edited W. H. Cum- 
 BeeTHOVEN.—Prestissimo C minor ( F inale ) , “ Sonata , mings ) . Novello Purcell Album a4 rere Ba 
 Op . , . 1 ( Edited A. Zimmermann , . 5 ) ... .. 2 0 Mozart.—Voi che sapete . Novello edition Songs 
 RHEINBERGE R.—Fugue G minor , Op . 5 , . 3 ( Pianoforte Le Nozze di Figaro ( Edited A. Randegger ) oa ‘ ok ane 
 Albums , . 23 ) Bs ine eke isu aie sao O MEZZO - SOPRANO . 
 HanpeL.—Gigue , Suite . 1 ( Pianoforte Albums , BorpoGNi.—24 Vocal Exercises ( Edited A. Randegger ) ... 1 | 
 . 4 ) Bes ae avs saa wa 2 - 0 Heaven Almighty ( 12 Soprano 
 Czerxy.—In , Op . 740 , . 26 ( F. Tz iylor Studies , Book XVI . ) 1 © H{anpeL. King ... _ « + ] Songs , Handel | , 
 Mozart.—Adagio E flat , Sonata C minor ( Edited | ? Liberty , thou choicest qo ( Edited dt 
 A. Zimmermann , . 144 ) ... cme tw Ss hehe ae ss aoe ( bedd . Randegger ) ... 
 MenpDELSSOHN.—Lied ohne Worte , ' B flat minor , " OP . 3 30 , ) , . : 2 cecilia ae pec sieation : 
 ( Lied . 8) .. LO . RAL : : 
 eae ° 9 ° PaNsERON.- 42 Exercises Contralto , I. ( Edited A. 
 SCHOOL EXAMINATIONS — ELEMENTARY . Randegger ) = Peer 
 LemoineE.—In C , Nos . 1 2 , Op . 37 ( F. Taylor Studies , ( shall eyes ) “ 12 Contralto Songs ) 
 Book III . ) xi ns ne .. t o Hanpet . - shall feed Hz — s Oratorios 2 
 Czerxy.—In C , Op . 599 , . 18 ( F. Taylor Studies , Book I. ) ... 1 0 . — - — m ( E — ; Pg og 
 SCHOOL EXAMINATIONS — LOWER DIVISION . Vena sesame gina ? son igs “ eh 3 0 
 Czerxy.—In D , Op . 636 , . 11 ( F. Taylor Studies , Book X. ) 1 0 TE . , 
 BEETHOVEN.—Rondo ( Allegro ) G , Sonatina , - 49 , ( O God , ) 12 Tenor Songs } 
 . 1 ( Edited A. Zimmermann , . 19 . ) ... 1 6 HANpDEL.—- Sing Songs ’ - Handel Oratorios ( Edited 2 
 Czerxy.—In C , 299 , . 8 ( F. Taylor Studies : Book { Praise ) A. Randegger ) tee ) 
 XVII . ) ... Io BARITONE . 
 Mozart . le ae 2 Seu Sonata Ge flat om dited PANSERON.—42 Exercises Baritone Bass , I. ( Edited 
 A. Zimmermann , . ape 1 6 A. Randegger ) oe sen = m 
 ( 12 Bass Songs ) 
 SCHOOL E XAMIN ATIONS — HIG R ‘ DIVE ISION . ( Behold , tell | Handel Ora- r 
 Bac aay seme parts . 13 , minor ( Edited mNDEL | Fhe trumpet shall sound torios ( Edited A. | = 
 iggs ) ... ee ess acs ee \ Randegger ) 
 Czerny.—In C , Op . 299 , . 5 ( F. Taylor Studies , Book IV . ) 1 0 BASS . 
 Mozart.—Allegretto ( Finale ) , Sonata D ( Edited ConconeE.—4o Lessons Bass ( Edited A. Randegger ) ee ie 
 A. Zimmermann , . 19 ) ... oe ee fe . ( 12 Bass Songs ) 
 m Wisse ! comforted _ Handel Ora- . 
 ORG : . \ Lord worketh wonders | torios ( Edited ? 
 . : ’ A. Randegger ) 
 Best.—Thirty Pr ae e “ JU age ScHUPERT.—The Wanderer , Schubert Songs , Vol . II . proms ot 
 fe ee ee ee ee ee SCHOOL EXAMINATIONS — ELEMENTARY . 
 Smart.—Twelve short easy pieces , . 1 E flat ... oo 2 6 SOPRANO . 
 MENDELSSOHN.—Allegretto F , Sonata IV . se ~2 6 } Concone.—-50 Lessons ( Edited A. Randegger ) ... ... 0 ... 1 6 
 LOCAL CENTRES — SENIORS . MEZZO - SOPRANO . 
 Best.—Studies Pedal , Nos . 89 , 96 , 102 . ( Art Organ- PANSERON.—40 Exercises , I. ( Edited A. Randegger ) 2 
 Playing , II . ) . .. 7 6 Braums.—Wiegenlied , E flat ( Lull aby Goodnight ) , Book 
 LeMMENS . — Allegretto ‘ B flat ( Pieces Organ , III . , Low Voice : “ ai os > 
 NGiu , ee ee ne . ae “ CONTRALTO . _ 
 Bacu.—Fugue mnie ( Bridge Higgs ! Edition , PANSERON.—42 Exercises , I. ( Edited A. Randegger ) ... 2 
 Book III . ) ss ae oe SG SCHOOL EXAMINATIONS — SOPRANO . 
 wars er moe ConconeE.—5o Lessons ( Edited A. Randegger ) .. exe 6 
 : PR SCHOOL Prine AMINATIONS -L -OWER DIN ISION . Satnt - S s.—Thou , O Lord , Helper ( 19th Psalm ) ... wa FS 
 ve STAINER.—Organ Primer , Nos . 35 , 56 , ste loi ee llo Haypn.—My mother bids bind hair aaa ee aantl 
 Primers , . 3 ) . > 250 MEZZO - SOPRANO . 
 m MENDELSSOHN.—Adagio flat , Son : ata waa os .. 2 6 ConconeE.—5o Lessons ( Edited A. Randegger ) ... vee ae 
 3ACH . ~Acht kleine Praludien und Fugen , . 1 C weit BENNETT , W. STERNDALE . — Dawn , gentle flower os ow FC 
 ( Bridge Higgs ’ Edition , Book I. ) ... 7 2.6 CONTRALTO . 
 SCHOOL EXAMINATIONS — HIGHER DIVISION . Paneteot-4a Rewsines Se Comisete , Poet 1 . ( aati hy 
 RCHER._—The oe S. 22 Ee Randegger ) . ian oa oun tee ass 2 
 = dacs : : Phe oe aes 237 ) 238 a0 . 2 © | HanpeL.—Return , O God Hosts . | 12 Contralto Songs 
 a. eet . te ! ste ae te te ” « + 2 6 Handel Oratorios ( Edited A. Randegger ) ... om ie 
 AC m ort > oue % y ; ridcee : 
 flere BARITONE BASS . 
 . Tt ee sat ae : aa oil Conconr.—4o Lessons Bass Baritone ( Ed . A. Randegger ) 1 6 
 ok V IOLIN .. MENDELSSouN.—For mountains shall depart . 
 LOCAL CENTRE — JUN Bass Solo Music Elijah ( Edited A. Randegger ) ... 2 
 . ' JUNIORS . MENDELSSOHN.—Consume ( St. Paul ) — ... aes Gade 
 EtGcar , E.—Chanson de Nuit ... e2 « + » 6 > Mozart.—Se vuol ballare . Novello edition Songs 
 HaNpbeEL.- Sonata , . 5 , Sec ‘ ond Movements ( Piano Mozart Le Nozze di Figaro ( Edited A. Randegger ) ... 2 0 
 Violin Albums , . 17 ) ae ‘ fixe eet . » 3 6 ScHUMANN.—Row gently ( Edited Macfarren , . 26 ) ... 0 9 
 eta : NOVELLO COMPANY , Limitep 

 XUM 

