AUBER ’S FRA DIAVOLO 

 BEETHOVEN 's FIDELIO 

 London : Novello , Ewer and Co 

 the last subscription concert 
 ON FRIDAY , MAY 65 

 BEETHOVEN ’S MASS in D 
 CHORAL SYMPHONY . 
 MR . BARNBY 

 door open at 7 , commence at 8 o’clock 

 Crampton . price 3d . post - free from Pitman ’s , 20 , Paternoster - row 

 bas DU PRINTEMPS , by Wituram Was- 
 seRzUG . a Saloon Waltz for the Pianoforte , price 4 . ; post- 
 free , 25 stamp . Augener , & c. , Beethoven House , Newgate- " street , 
 or of the Composer , 14 , Maryland - road , St. peter’s - park , W. Mr. 
 Wasserzug . pupil of the Leipzig Conservatoire , continue to give 
 instruction in Pianoforte and harmony 

 IRANA ’s NEW part song for 5.4.7.3 

 CATHEDRAL GEMS , by LOUIS DUPUIS . 
 NANTES.—Fantasia , on subject from Mozart ’s 12th Mass 
 WURMS.—Fantasia , on subject from Weber ’s Mass in G 
 ROTTERDAM . cannery on subject from Haydn ’s 3rd 

 imperial ) Mass 
 MALAGA.—Fantasia , on ' subject from Beethoven ’ 's Mass 

 nC 
 ROUEN . — Fantasia , on " subject from Gounod ’s " Messe 
 Solennelle « 
 CAEN.—Fantasia , on subject from Rossini ’s Stabat Mater 
 MAYENCE.—Fantasia , on ne from Haydn 's 1st 
 Mass , b flat 
 COLOGNE . — Fantasia , ' on subject . ' from Mozart ’s 1st 
 Mass , in C oe 
 9 . PARIS . — Fantasia , on subject from Mendelssohn ’ 3 ; hymn 
 of Praise 
 10 . LONDON . — Fantasia , on ' subject from Handel ’s Messiah 
 11 . NORWICH.—Fantasia , on subject from Haydn ’s creation 
 12 . CANTERBURY . ( have savin on saison from Spohr ’s as 
 pants the Hart .. oo cco oe 
 beautifully illustrate in Covure , with view of celebrate cathedral . 
 " these piece , like those by Felix Gantier , review some time 
 ago in the Musical Times , be especially intend for young player , 
 and will , we think , be find equally acceptable to teacher and 
 pupil . they be write in the form of little Fantasias , each com- 
 mence with a short introduction . the illustration , print in 
 colour , be exceedingly beautiful . ”—Musical Times , Feb. , 1870 

 PHILHARMONIC SOCIETY 

 the fiftv - ninth season of this Society commence at St , 
 James ’s Hall on the 8th ult . , special interest be give 
 to the concert by the presence of M. Gounod , who have 
 accept the invitation to conduct four of his own piece , 
 and by the exhibition of a fine bust of Beethoven , which 
 have be gracefully present to the Society by a lady of 
 Pesth , in acknowledgment of the intimate connection 
 between the great master and the Institution , not only 
 during his artistic career , but in the day of his last 
 illness , when help be spontaneously offer with a ten- 
 derness and delicacy which sink deeply into the heart of 
 the die composer , and reflect enduring honour upon 
 the donor 

 the reception award to M. Gounod must have con- 
 vince he of the high estimation in which his talent be 
 hold in this country .. the applause bestow upon his 
 symphony in D , which open the concert , must not , 
 however , be accept as a mere complimentary welcome . 
 although an early work , it be full of beauty , and be 
 write throughout with a clear and defined purpose 

 that any other baritone vocalist who attempt it will be 
 place at a serious disadvantage . the Scena and Cava 

 tina , " far great in his lowly state , " from " Irene , " 
 be the only other work by M. Gounod include in the 
 programme , and this be so well give by Miss Edith 
 Wynne as to elicit the warm applause . 
 request of the lady who present the bust of Beethoven , 
 the composer ’s symphony in C minor be perform — 
 perhaps hardly so well as we have hear it on former 

 by express 

 the MUSICAL TIMES.—Aprin 1 , 1871 . 45 

 occasion — and Herr Joachim play Mendelssohn ’s 
 Violin Concerto with that perfection of tone , executive 
 power , and intellectual appreciation of the author ’s mean- 
 ing which place he at the head of all live violinist . 
 Mr. G. A. Macfarren ’s highly dramatic Scena , " my child 
 be flee , " from his Opera " Robin Hood , " be sing with 
 good effect by Mr. Santley ; and the concert conciude 
 with Weber ’s Overture , " ' the ruler of the spirit . " Mr. 
 W .. G. Cusins , the conductor of the Society , be warmly 
 welcome on his entrance into the orchestra . ' the per- 
 formance be attend by the Princess of Wales , the 
 Princess Christian , and Prince Arthur . at the second 
 concert , which take place on the 22nd ult . , the performance 
 commence with Mendelssohn ’s " Reformation Sym- 
 phony , " every movement of which be receive with 
 marked applause , the melodious " Allegro vivace " be , 
 as usual , encore . the other orchestral piece be 
 Sir W. Sterndale Bennett ’s highly imaginative and 
 masterly overture " the Wood - Nymphs , ’' Mozart ’s 
 " Jupiter " symphony ( as it seems now universally call ) . 
 and Weber 's Overture to " Oberon . " Madame Schu- 
 mann ’s rendering of Beethoven 's Pianoforte Concerto in C 
 minor , be one of the most interesting feature of the 
 evening . Madame Parepa - Rosa , who be to have ap- 
 peare for the first time since her return from America , 
 be unfortunately too ill to fulfi : her engagement , and her 
 place be supply by Madame Lemmens - Sherrington , 
 who give " deh vieni non tardar " ( ' Le Nozze di Figaro " ) , 
 and " ah quelle nuit " ( ' Le Domino Noir " ) with 
 excellent effect . the other vocalist be M. Jules Lefort 

 MR . HENRY LESLIE ’S concert 

 ADELAIDE.—The Christmas performance of Handel ’s 
 Messiah , by the Philharmonic Society , be a decided success . the 
 orchestra consist of about 29 instrumentalist and 100 vocalist . 
 the principal singer — Mrs. G. T. Harris , Miss Effield , Miss Nimmo , 
 Miss Vaughan , Mrs. Walkley , Messrs. C. Lyons , E. H. Hallack , L. 
 Grayson , G. C. Smith , C. C. Gooden , J. Brooks and W. S. Dyer — 
 give much effect to the solo , and the chorus be render with 
 considerable power and precision . Mr. E. Spiller act as con- 
 ductor , Mr. R. B. White es leader , and Mr. James Shakespeare 
 preside at the harmonium 

 AYLEsBuURY.—The member of the Vale of Aylesbury 
 Sacred Harmonic Society give their first concert for the present 
 year on the 2lst February , in the Corn Exchange . the work 
 select for performance be Beethoven 's Mount of Olives 
 ( Engedi ) and Sullivan 's prodigal Son . in the first name com- 
 position the principal part be excellently sing by Miss Clare , 
 Mrs. Taylor , Messrs. Wootton , Ingram , Gibbons and Taylor ; and in 
 Beethoven 's Oratorio , the tenor music be most efficiently give by 
 Mr. Ingram , Mrs. Parslow , in the trying soprano solo , be also 
 entitle to the high commendation . the success of the concert be 
 — aid by the energetic and intelligent conducting of Mr. 
 orfe 

 BrruincoamM.—'The Postmen ’s Annual Concert be 
 give in the Town Hall , Birmingham , on the 2nd ult . Professor 
 Bennett 's May Queen form the first part of the programme , 
 the solo music be well interpret by Madlle . Liebhart , Mdlle . 
 Drasdil , Mr. Vernon Rigby , and Mr. Orlando Christian . the band 
 and chorus of two hundred voice be under the direction of Mr. C. 
 Heap . several ballad be introduce in the second part , and a 
 violin solo by Herr Pettersson . the member of the Philharmonic 
 ae give their service on this occasion , and the Hall be well 
 e 

 Rose of Castile , with spirit ; and Mr. Atkinson display great 
 vigour in * * let I like a soldier fall , " which be re - demand . 
 the duet , ' * love and war , " by the last - name gentleman and Mr. 
 Cook , be also well sing . the band under the talented conductor , 
 Mr. J. Sidney Jones , add very greatly tothe merit of the concert ; the 
 overture to Rossini ’s Stabat Mater , and a selection from Don Giovanni , 
 be play . Jullien ’s ' royal irish quadrille , " and the Night- 
 ingale Valse , with Corporal McCain ’s excellent piccolo solo , be also 
 much applaud . Mr. Tom Dodds play the pianoforte accompani- 
 ment with his usual ability . during the evening the gratifying 
 announcement be make by Mr. Bellchambers , the hon . treasurer 
 tothe Association , that there would be a surplus of at least £ 50 to 
 be hand over to the Infirmary.——Onge of the large concert 
 audience yet see in the Leeds Town Hallassembled on Saturday , 
 the 18th ult . , when the Madrigal and Motett Society occupy the 
 orehestra . the first part of the concert comprise sacred piece , 
 and include Handel 's duet , ' ' o lovely reace , " well sing by Miss 
 Blakeley and Miss Anyon . Dr. Spark ’s Easter Anthem be ad- 
 nirably perform , Miss Newell singe the solo with much taste 
 and effect . the wedding music , give in honour of the royal 
 marriage , include Benedict 's ' ' Wedding Chorus " from St. Cecilia 
 asongcompose by Mrs. Dodds , and effectively give by Miss Hiles ; 
 Mendelssohn 's Wedding March , spiritedly play by Dr. Spark , and 
 greatly applaud ; a new festal part - song , by the Doctor , ' * merrily 
 the bell ring out , " and several other piece more or less appropriate 
 tothe occasion . the concert conclude with ' ' the Marseillaise , " 
 " the Rhine Watch , " and " ' rule Britannia " , the singing of which 
 produce great excitement 

 Quartet for string , Mendelssohn in e flat , no.3 . ( Scherzo , encore ) ; 
 song ( Handel ) * * let I wander , not unseen " ; Sonata , in a major , 
 ( Herr Joachim ) for violin with pianoforte accompaniment , Handel , 
 ( Allegro , encore ) ; Aria ( Mozart ) ' * voi che sapete , " ( encore ) ; 
 Novelette ( Schumann ) in f major , op . 21 , for pianoforte alone ; 
 impromptu ' ' zur guitarre " ( encore ) and replace by Mendelssohn 's 
 so - call * * Bee ’s weading ; " sonata , in F , op . 24 for pianoforte and 
 violin ( Beethoven ) , Madame Schumannand Herr Joachim , ( Scherzo , 
 encore ) ; song ( Benedict ) ' ' I ’m alone " ; Quartet , in D major , op . 
 64 , no , 11 , for strings(Haydn ) . the concert be much appreciate 
 by a crowded audience.—the fifth Subscription Concert of the Phil- 
 harmonic Society take place on the 2lst ult . , and be mainly 
 devote to Sacred Music . the solo artist be Miss Edith Wynne , 
 Madame Patey , Mr. Vernon Rigby , and Mr. Santley . the most 
 interesting feature of the evening be the performance of M. 
 Gounod 's " ' Cecilian Mass , " conduct by the composer , whose 
 reception be enthusiastic . the Mass go well , with the exception 
 of some uncertainty in the vocal intonation — perhaps in consequence 
 of the difference of pitch , which seem to be slightly lower , to 
 suit that of tne organ . ( it would be well if this be the habitual 
 pitch . ) the orchestral work be Weber 's ' jubilee ' Overture ; 
 Spohr ’s little know but beautiful * * Concert Overture , " Op . 126 ; 
 sinfonia to Joseph , C , E. Horsley , " and Mendelssohn 's ' * Wed- 
 ding March , " in compliment , of course , to the Princess Louise . 
 two miscellaneous chorus in the second part ' ' when his loud 
 voice Jephtha , and the hallelujah in the Mount of Olives 
 ( Beethoven ) go steadily . the solo be well receive , especially 
 Mr. Santley ’s fire singing of Pergolesi ’s ' ' Sanctum et terribile " and 
 Madame Patey ’s delicate rendering of the ' ' evening prayer " 
 from £ li , this lady be , besides , encore in Gounod ’s new sacred 
 song , ' * ' there be a green hill far away 

 Lonertpee.—A concert be give in connection with 
 the Longridge Working Men 's Club on Saturday evening , the 11th 
 ult . , in the Boys ’ School . the vocalist be Mr. J. Edelston ’s 
 choir of tonic sol - faist . Mr. T. Woolman ably preside at the 
 pianoforte . the programme consist of glee , part song , & c. , 
 intersperse with occasional pianoforte solo , song , and other 
 piece . Master P. Edelston ( only eight year of age ) play a 
 fantasia so well as to call forth special admiration , and Mr. T. 
 Woolman perform a Rondo of Weber 's with much skill . dare the 
 interval between the part , Mr. Edelston give an explanation of 
 the principle of tonic sol - fa notation , and before conclude he 
 succeed in get the whole company to sing under his direction 
 ashort piece . there be a large attendance , the gathering be 
 preside over by Mr. Owtram , President of the Club . after the 
 concert the Rev. W. C. Bache , vicar , propose a vote of thank to 
 Mr. Edelston and his associate , and also speak in eulogistic term 
 of the manner in which Mr. Edelston have explain the principle 
 of the system he practise . the vote having be duly accord 
 and acknowledge , the proceeding terminate 

 Saissury.—A performance of Handel ’s Judas Maccabey 
 be give on the 2lst ult . by the Sarum Choral Society . the 
 soloist be Miss Lanham , Messrs. Cummings and Brandon , all of 
 whom acquit themselves in a thoroughly satisfactory manner , 
 the band be , on the whole , efficient , and the entire performance , 
 conduct by Mr. Read , be , much appreciate by a numeroyg 
 audience 

 SuEerriretD.—A Pianoforte Recital be give by Miss 
 Clara Linley , on the 8th ult . , in the Music Hall . the programme 
 comprise selection from the important work of Thal 
 Beethoven , Mendelssohn ' and Clementi , with Wehli ’s ' Lurline * 
 Rhenish Song ( variation , ky ) , and March from Tanhauser ( Liszt ) , 
 the audience much applaud the ' ' Rhenish Song , " and an encoie 
 be demand , Miss Linley accede by give Chevalier de 
 Kontski ’s duet from Faust . the performance be thoroughly 
 successful 

 Sr . Hetens.—In acknowledgment of the gratuitous 
 service render by Mrs. Cole , as organist of the Parish Church , 
 bracelet be present to she after the first part of a concert on the 
 20th February , accompany by a most complimentary speech from 
 one of the churchwarden , Dr. Chevalier . the gift be manufag 
 ture by Messrs. Watherston and Sons , ( Pall Mall ) , and cost £ 4 
 it have a raised oval centre of pale blue enamel , in which be se 
 opal and diamond ; on either side of the centre ornament ar 
 small grouping of a similar character , form a chaste and 
 beautiful specimen of high - class workmanship 

