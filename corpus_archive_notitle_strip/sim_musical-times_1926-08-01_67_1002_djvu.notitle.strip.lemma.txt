wit teen 8 : 8 | tec hnical and interpretative mastery so complete 

 Beethoven say : 4 
 it jeal j ' | as to impress even such"musician as Liszt an 

 I , too , playec the organ a great deal in my yout my | von Biilow " 7 have no IGsat ' write the latter 

 instrument first among virtuoso . |playe it . and _ probably ft icouldn’t be , for 

 the organ in Beethoven ’s day be still a clumsy | Best ’s only rival at that banc . 7 to — — 
 aflair , and he must have have vivid recollection of | Schneider , of Dresden ; and the ' ho | or pee 

 his early struggle with the example at the| Statham , a capital judge and one who hear Best 

 in a conversation , towards the end of his life 

 deserve some such handsome tribute as that pay | 
 by Beethoven . even to - day , when the organ be 
 both rich in resource and far easy to handle , a 
 first - rate recitalist probably use more mental and 
 physica ! energy than any other performer , with the 
 additional strain of have to adapt himself to a 
 fresh set of condition ( tonal scheme , touch , 
 acoustic , stop - position , & c. ) with every instrument 
 he play on . yet it be a notorious fact that , 
 however fine his performance , he meet with far } . . . merely the dry fact as to his various appoint- 
 less recognition than the pianist or violinist of | ments , without the slight attempt to give any real 
 ual , or even inferior , quality . * estimate of his extraordinary and quite unequalled 
 " ; wre : power as an executant and as a master of organ effect . 
 for these reason , the centenary of the birth of 
 one of the great organist of any age or on the other hand , Thalberg—'a mere piano 
 country call for special notice , if only by way of | yirtuoso’—was accord a far long and full 
 protest against the lack of appreciation of the ) article : 
 — = concert - player . , . whereas Best be not only fully equal to Thalberg 
 he biographical fact concern Best be so } 1 his own instrument , but be also a much well 
 easily accessible that there be no need to go over all - round musician , with much full knowledge of 
 them in this article . it will be of more use and| _ the art . 
 interest to glance at his achievement and 

 tibia in consider Best ’s attainment as a player , 
 Personamny . " we may pass over , as take for grant , his keyboard 
 technique , and concentrate rather on the interpre- 
 | tative side , especially registration — a department of 
 | organ playing in which , both as performer and 
 | transcriber , he have exercise so much influence . 
 concern his manual technique , it should , 
 | however , be point out that he be a first - rate 
 : pianist . Mr. Statham relate that he once hear 
 the proverb , a man may teach himself and | } ; fas Oe ree " ge 
 nim play the pianoforte for a whole evening , and 

 inevitably his skill in registration affect his 
 playing of Bach in a direction that be now not 
 generally approve . his own edition of Bach be 
 over - register , and the result be apt to be 
 unsatisfactory , even when such _ treatment be 
 comfortably manageable . Statham say that he 
 be at his good in play Bach , and give instance 
 of particularly striking performance . put aside 
 the question of registration , as a matter on which 
 widely different opinion be allowable , there can be 
 no doubt that his playing of Bach gain enormously 
 from a characteristic that seem to have impress 
 all who see and hear he play — that be , an almost 
 uncanny self - possession and nervous control . this , 
 at the back of his marvellous technique , must 
 have make his playing of a long work of Bacha 
 memorable experience . as to accuracy , Statham 
 say that , although he hear he play many 
 hundred of time , he remember hear he 
 make only one mistake 

 he be play Beethoven ’s Funeral March from 
 the A flat Sonata from memory , and in the first bar of 
 the 7rio he play the /vemolando through only a 
 quarter of the bar instead of the half - bar . I see he 
 shake his head , and it be set right in the next bar 

 his remarkable control and steadiness in play 
 long and difficult work have be ascribe to a 
 peculiar source . Best have an extraordinarily slow 
 pulse — so slow that I understand it be make the 
 subject of an article in the Zawcet by the late 
 Dr. Charles Hayward , who attend Best medically . 
 Dr. Hayward give the rate to Mr. Levien , who 
 have mislay the note . Mr. Levien tell we that 
 Dr. Stanley Mellville ( President of the Royal 
 Society of Medicine ) , who know Best ’s playing , 
 and who be himself a pianist ( having , by the way , 
 once accompany Santley ) , suggest that the very 
 slow pulse have a great deal to do with Best ’s 
 steadiness and absence of flurry or ' nerve ' 
 throughout long and exacting performance 

 a sense of motion in music be attain by a 
 skilful use of vary harmony . easily assimilate 
 harmony , judiciously mix with more compli- 
 cat chord , give a sense of motion by reason of 
 their clearly mark difference . we know exacily 
 how fast the music be go because we can 
 distinguish each separate harmony . but when 
 the harmony be ' all my own invention , ' to borrow 
 a phrase from that half - witted philosopher , the 
 White Knight , the musical language become as 
 heavy and _ stilted as the most ponderous 
 Johnsonese , and no lengthy burst of speed be 
 possible . although perhaps the ' palpitate 
 whirl joy - molecule ' do give a feeling of 
 animation , they no more suggest speed than do 
 the whirl palpitate gnat above the noisome 
 water of a stagnant pond . those frequent shake 
 ( puissant , radieux ) .and ejaculatory snippet of 
 sound , so beloved of Scriabin , be but the verti- 
 cally rise spark which belch forth from the 
 chimney of a traction - engine , prove that the 
 engine be certainly go , but at such a slow rate 
 and upon such cumbersome wheel that progress 
 be scarcely perceptible 

 and yet to many people Scriabin be not merely a 
 composer , he be " 4e composer . his pianoforte 
 composition be speak of as ' a distinct advance 
 upon any of his predecessor . ' ' think of that ! 
 not merely be his pianoforte piece write from 
 a diflerent point of view , with a different indi- 
 viduality , but they be a distinct advance . think 
 of Bach ’s Preludes : of the tragic nobility of that 
 in B flat minor ; of the gaiety of that in C sharp 
 major . but Scriabin ’s work be a distinct advance 
 upon they . think of Beethoven ’s sonata : of the * 
 sonata in A flat , Op . 26 ; of the ' appassionata , ' 
 Op . 57 : of the ' hammerclavier , ' Op . 106 ; and yet 
 Scriabin ’s work be a distinct advance upon they . 
 we be also tell that in Scriabin ’s tenth Sonata we 
 have a contribution to instrumental music of the 
 great importance perhaps since Beethoven . 
 ponder over that ! Schumann ’s Carnaval , Etudes 
 Symphoniques ; Chopin ’s Ballades , Etudes , Polo- 
 naises ; Brahms ’s variation and rhapsody — all 
 these work , great as they be , dwindle into 
 insignificance beside Scriabin ’s tenth Sonata . and 
 yet again we be tell that his orchestration eclipse 
 that of all contemporary ; that Schénberg , 
 Stravinsky , and Strauss — master , as all the world 
 acknowledge — never handle the orchestra with 

 alth 
 the : 
 sub 
 of 
 wou 
 the 
 find 
 Poe 
 peri 
 four 
 thir 
 A 
 wha 
 grea 
 con : 
 thos 
 of 1 

 and he proceed to say that Mace and the 
 Abbé speak the truth , ' for the same music 
 produce the same result to - day when it be hear 
 under proper condition . ' much well reasoning 
 than this will be need to convince I that our 
 art reach its culminating point nearly three 

 hundred year ago , long before Bach and Beethoven 

 true , Mr. Dolmetsch bring forward 
 at the 1925 

 summit of pure music’—the String Quartets of 

 Mozart and Beethoven be place a good way 
 down the slope , presumably . advocacy of this 
 unbalanced sort do no good whatever , least of 
 all to the early english composer whose music we 
 be all begin to appreciate — with reservation . 
 in certain form of music perfection be 
 undoubtedly achieve century ago ( a statement 
 which do not imply that perfection have not since 
 be reach again ) . for instance , many perfect 
 example of melody be produce in the palmy 

 day of plainsong , and another peak be reach 

 September.—The understanding of music come 
 with time . forty year ago K. A. Islavin play 
 Chopin as something perfectly new , and it seem 
 obscure 

 6 September.—In art the chief thing be to say some- 
 thing new — something of your own . all the great 
 artist can be distinguish by that . take Mendelssohn 
 — his music be beautiful , but still it be like everybody ’s . 
 but it be necessary to say something that be one ’s own . 
 Beethoven — I do not like he , but he possess that 
 quality . you never know in advance what he be go 
 to say 

 29 July , 1909.—do you remember what Schopenhauer 
 say about music ? when you listen to music it be as 
 if something remain unsaid , there be something 
 unsatisfying . that be perfectly true 

 keep a detailed Diary . 
 it relate to music at Yasnaya Polyana 

 2 ? December , 1907.—this morning Sibor ( violinist ) , 
 Bukinik ( violoncellist ) , and Goldenweizer play . in 
 the evening we have a concert . they play Mozart , 
 Haydn , Beethoven , Arensky . Tolstoy like Mozart , 
 and still more Haydn . he say : ' I particularly love 
 Haydn . he have modesty — that artistic self - denial 
 when an author do not show himself . ' he be 
 specially pleased with the Aiva / e , in which hungarian 
 melody be hear . ' I think Haydn be of croatian 

 origin , live among the Croatians , and make use 
 of their folk - tune . ' Goldenweizer confirm this . 
 after listen to Beethoven , Tolstoy remark 

 I remain tranquil and admire the artist ’ per- 
 formance . ' when those present begin to be enthusiastic 
 about Beethoven , he say : ' I think that you be 
 pretend . Mozart do not do it , but Beethoven 
 begin to combine the inner content with external 
 effect , and among his follower only external effect 
 be leave 

 Arensky ’s Trio also please he . on Sibor ’s 

 one 

 have admirable thing and have poor 
 you know that Beethoven also have poor thing . 
 but the public cry up everything that Beethoven 
 write 

 in the evening Tolstoy say : 
 old as I be , I do not know how to define 

 view of Tolstoy . ,—Ep1Tor 

 the Bradford Festival of Chamber Music ( October 5 
 and 6 ) hold out a very attractive scheme . 
 concert each day , at 11.15 to 3 ( with an hour ’s interval ) 
 and 7.30 to 9.30 . the work announce include String 
 sextet by Brahms and Schonberg ( ' Verklarte Nacht ' ) ; 
 Pianoforte ( quintet by Elgar , Fauré , and Franck ; Bax ’s 
 Quintets for oboe and string , and harp and string ; 
 Ravel ’s Introduction and Allegro for harp , string quartet , 
 flute , and clarinet ; String ( quartet by Beethoven , 
 Franck , Dvorak , Goossens , Debussy , & c. the player be 
 the Virtuoso String ( Quartet , William Murdoch , Gwendolen 
 Mason , Leon Goossens , Haydn Draper , Robert Murchie , 
 James Lockyer , and Ambrose Gauntlett . information as 
 to ticket , & c. ( early application be necessary ) , from the 
 hon . secretary , ( Jueen ’s Hall , Bradford 

 there will be 

 there be one or two composer of whom it 
 may be say that their position in the musical world 
 have suffer from their popularity . this be so in the 
 case of Mendelssohn , the extreme popularity of 
 the ' song without word , ' and ' Elijah , ' for instance , 
 having blind the eye of the musical world to the 
 beauty of the ' hebride ' Overture , the chamber 
 music , and other of his good work . particularly be 
 this so in the case of Grieg . Grieg be generally 
 regard only as the composer of the ' Peer Gynt ' 
 Suite and a few negligible trifle for pianoforte which 
 may be hear any evening in a thousand suburban 
 drawing room , his more important work , in which 
 there be often a much great amount of real beauty , 
 be in very considerable danger of total neglect 

 but not only have Grieg suffer from the popularity 
 of much of his music ; he have also suffer from 
 foolish remark make about he in various so- 
 call critical work , these remark generally 
 take the form of blame he for not be other 
 than he be , thus , while it be know that 
 Grieg be more successful in small form than 
 in large , yet his short piece be criticise for 
 not be sonata and symphony . this be as 
 sensible a procedure as criticise Beethoven ’s 
 Symphonies because they be not short lyric piece . 
 Parry rather loftily remark that ' the intellectual 
 process of concentrate development be not in his 
 [ Grieg ’s ] line . ' even if this statement be entirely 
 true ( and to those with a knowledge of the ' Holberg ' 
 Suite , ' in autumn , ' the first movement of the Sonata 
 Op . 45 , and other work , it be easy to doubt it ) , it be 
 not fair to make it a reproach to the composer 
 because his sympathy be largely with short and ' 
 more lyric formsthan the symphony . there be many 
 other desirable quality in musical composition 
 besides ' the intellectual process of concentrated 
 development , ' and some of these quality , such as a 
 gift for melody , harmonic originality , and rhythmic 
 interest be present in a great degree in Grieg ’s music 

 far , why should it be assume that a big 
 canvas be the only one on which great work of art 
 can be form , and why should it be assume that 
 nobility and grandeur of utterance be the essential 
 quality of great music ? let we have nobility by all 
 mean , but it be not an indispensable quality to good 
 music . Scarlatti never attempt nobility , and we 
 may search Couperin and Rameau for it and not 
 easily find it . yet these man take a very honourable 
 place among composer . and , turn to Bach ’s 
 french Suites , be not these as beautiful as anything 
 he ever write ? there be surely in music room for the 
 miniature as well as forthe symphony , and to blame a 
 composer of miniature for not be a composer of 
 symphony be clearly a cardinal error 

 the Spirit of Music : how to find it and how to|to playing and reproduction , be beyond all praise . 
 share it . by Edward Dickinson . pp . 218.| if anything could make a Schubert convert , it would 
 Charles Scribner . surely be this lovely work . it be worth a stack of 

 euvres inédites de Beethoven . ' publish with an| the over - rate song with their yearning and tear 
 introduction by Georges de Saint - Foix . Publica- | ( l1751 - 4 ) . 
 tion of the Sociétié Francaise de Musicologie.| the Salisbury Singers be unequal in the two 
 Vol . 2 , 30s . London : Harold Reeves item record on no . 3975 . ' their phrasing be very 

 Vom Volkslied bis zur Atonalmusik . ' by Franz ] short in an over - harmonize version by Ivimey of 
 Landé . pp . 69 . Leipsic : Verlag von Carl|/‘Drink to I only . ' the ending be too draw 

 contrapuntal setting of some word by an anonymous 
 writer . both be edit by Noel Ponsonby . 
 include in the same publisher ’ series of unison 

 Holst ’s short Festival Te Deum , ' Peace , ' from 
 Parry ’s ' War and Peace , ' Ethel Smyth ’s ' a Canticle 
 of Spring , ' Charles Wood ’s ' a dirge for two 
 veteran , ' Beethoven 's mass in D , ' Elijah , ' and 
 ' the Messiah , ' the London Symphony Orchestra 
 have be engage . Sir Herbert Brewer and 
 Dr. Percy Hull will share the organist ’s duty . the 

 follow be the singer : soprano — Beatrice 
 Beaufort , Agnes Nicholls , Dorothy Silk , and Elsie 
 |Suddaby ; contralto — Muriel Brunskill , Astra 

 Desmond , Olga Haley , and Millicent Russell ; 
 /tenor — John Coates , Edward Roberts , and Steuart 
 | Wilson ; baritone and bass — Norman Allin , 
 | Howard Fry , Herbert Heyner , Robert Radford , and 
 | Horace Stevens 

 regularly , every midsummer since the war , lie 
 rumour have tell we that there be to be no more 
 |*prom , ' and just as regularly August have find 
 this peculiarly London institution confound the 
 | prophet . the present season open on August 14 , and 
 | the concert will run on pretty much the usual line , 
 | with the evening allot as they have be for some 
 ‘ year past . the programme book arrive too near 
 |our press day for detailed discussion , but two point 
 catch our eye . first , eight of the Beethoven 
 | symphony be to be play with the chronological 
 order upside down , begin with no . 8 and end 
 } with no.1 . the ninth be omit — an odd decision , 
 surely . it be so frequently do without the choral 
 | Finale that no exception would be take to such a 
 |truncate method . no . 1 might well have be 
 |omitte , for after all its interest be little more than 
 |historic . the pleasant slow movement be all that an 
 | average audience want to hear to - day , whereas the 
 | ninth be too rarely perform . 
 | the other point note be that the organ solo be 
 |droppe . we presume that this be due to economic 
 reason , for there be no question as to the popularity 
 whatever the cause , the 
 |matter be one for regret . many of the general 
 | musical public welcome the prospect of the organ 
 | take a recognise place as a concert solo instrument 

 and part - song , edit by Martin Akerman , be E. T.| ( as it do in America and in France ) , if only for the 
 Chapman ’s setting of ' let all the world in every | reason that an opportunity would be give of hear 
 corner sing , ' and C , S. Lang ’s carol , ' Tres magi de | the great ( and eventually , perhaps , some of the less 
 gentibus , ' word translate from the latin text , 15th | familiar ) work of Bach , play on their proper 
 century . these be apparently write for school| instrument . at present the non - churchgoing musician 
 use . the former be a simple , dignified setting for ) rarely hear they save in a transcribe form , which 
 treble and man ’s voice , mainly in unison or two - part | inevitably rob they of certain quality and effect 
 harmony . for the carol , the composer have write a that only the organ can supply . meanwhile , the 
 stately tune for unison voice , with a brief refrain at | renovate instrument at Queen ’s Hall become again 
 the close of each verse for eight - part chorus . both | jittle more than a mere tap for the supply of re / fgioso 
 these effectively - write work be provide with an background to largo , solemn Melodies , and Bach- 
 excellent organ part . G. G. | Gounod concoction 

 require a totally different type of hall , audience , 
 and atmosphere ; he think , too , that the ordinary 
 ' Prom ' habitué would be drive away . we do not 
 think there be much in these objection . after all , at 
 the ' Proms , ' and also at the Symphony Concerts , 
 violin and pianoforte solo ( sometimes as intimate in 
 character as chamber music ) be frequently hear , 
 without suffer appreciably from the large hall . 
 as our contributor ' Feste ' point out some year ago 
 when advocate the introduction of an occasional 
 chamber work in orchestral concert , such music 
 provide far well relief than a vocal operatic aria 
 in which the full orchestra play a prominent part , 
 besides be as a rule superior in quality . the 
 fact be , in a long symphony concert the ear have a 
 surfeit of the orchestra , and there be real need for a few 
 short item in which the band have no part . . we believe 
 the success of the English Singers at last season ’s 
 ' Proms ' be due not only to their delightful singing , 
 but also to the refreshing contrast they provide . 
 and the orchestra itself gain from this relief , of 
 course . ( by the way , the English Singers also do 
 not appear in this year ’s programme 

 as to Mr. Midgley ’s fear that Promenaders 
 would be drive away by chamber music , everything 
 depend on the choice of work . nobody would 
 suggest , for example , one of the later Quartets of 
 Beethoven ; nor would it be advisable to play com- 
 plete work , unless they be short . now that detach 
 movement of symphony be play , there ought to 
 be little objection to the use of separate movement 
 from quartet . the Léner and other player have long 
 since be in the habit of make gramophone record 
 of detach movement . there be a further point to 
 be consider . the average orchestral audience 
 know little of chamber music ; here be an opportunity 
 of make convert , and eventually increase the 
 public for this high of all type of music 

 in the matter of the 7.30 to 8 organ recital , we 
 part company with Mr. Lorenz , especially as he 
 suggest that many organist would gladly do the 
 work for a merely nominal fee . a recital give 
 while the crowd be assemble be a slight on both 
 instrument and player . if organ solo be to 
 be hear in the concert - room , they should be 
 accord a recognise place in the programme , and 
 the performer should be pay an adequate fee 

 subtlety , variety , and skill it can be beat hollow | 
 by countless passage from such composer as Brahms 

 and Beethoven ( especially in their chamber music 

 go back to the choice of illustration : Sir 
 Landon no doubt please a good many of his 
 listener by play the ' blue Danube ' Waltz . but 
 there be plenty of Ronaldites who find the old 
 Waltz decidedly worn . the good that could be say 
 for it be that it be excellent to dance to . 
 however , the Waltz by Irving Berlin with which 
 Mr. Hylton hit back be far bad , because ( 1 ) it 
 be poor for dancing purpose ; ( 2 ) sickeningly 
 monotonous and unwholesome in style ; and ( 3 ) some 
 of it be so pianissimo and attenuated that a couple 
 or two waltz would have drown it . as for the 
 fox - trot which Mr. Hylton say would make it 
 impossible for we to keep our toe still , it be , so far 
 as at least one circle of listener be concern , a 
 dead failure . nary a toe stir — except a couple 
 that move doorwards after the fox - trot have 
 hiccup its way for a minute or two 

 ROYAL ACADEMY of MUSIC 

 the summer term , the third and last of the academic 
 year , be always a busy one , for in addition to the customary 
 youtine of work and concert , there be the add 
 excitement of examination and the opera week . the 
 student ’ chamber concert , on June 24 , be notable for 
 some excellent string - quartet play in the first movement 
 of Brahms ’s quartet in A minor , Op . 51 , No . 2 . there be 
 no doubt that the general standard of chamber - music 
 performance at the Academy be at the moment very high , 
 and in this particular instance not only be the ensemble 
 sedulously maintain , but the tone and conception of 
 the performance be exceedingly well sustain from 
 beginning to end . very good , too , be Beethoven 's Quintet 
 in E flat , for pianoforte and wind instrument , in which the 
 pianoforte be exceptionally well play . a word of praise 
 be due to the horn player for his clever management of this 
 always somewhat unruly instrument . an ex - student , Miss 
 Dorothy Howell , be represent by her Phantasy for 
 violin and pianoforte . it be pleasantly play , but be too 
 long . young composer , and for that matter those of more 
 mature year , should ever keep before they the idea that 
 ' brevity be the soul of wit.2 two MS . song by a 
 student , Mr. Norman McLeod Steel , a victim of the war in 
 the loss of his sight , be most pleasing , if only for the fact 
 that they both betray imagination — an unusual asset with 
 song - writer . compact and to the point be four tiny 
 song with string quartet accompaniment , by Miss Olive 
 Pull . they be intelligently sing by Miss Barbara 
 Pett - Iraser 

 the student of the dramatic class give two perform- 
 ance on June 25 and 26 , ( good acting be show in the 
 quaintly - name playlet , ' six who pass while the lentil 
 boil , ' especially outstanding be the Boy . the young 
 lady who play this part have a distinct fair for the stage , 
 her diction be good , and she know what to do when she 
 have nothing to say . ' the wonder hat ' be also well 
 act , and the interest sustain throughout , but the less 
 say of ' the queen of Cornwall ' the well . young 
 amateur , indeed very few amateur , be fit to play 
 tragedy , and generally succeed only in make themselves 
 and the play ridiculous 

 739 

 Mr. Solomon have reduce pianoforte - playing to a certain 
 pellucid perfection in which there be no note of passion . at 
 his recital at Wigmore Hall he choose a more or less stereo- 
 type programme , begin with Busoni ’s arrangement 
 of Bach ’s Chorale , * ' Wachet auf , ' and a spirited and 
 sparkling d major Sonata by Haydn . then come 
 Beethoven ’s Sonata , Op . 31 , no . 3 , and Schumann ’s 
 * carnival , ' which have character and brilliance if it lack 
 warmth and abandon 

 Miss Ania Dorfmann , though brilliant , by the use of 
 exaggerated rubato destroy the flow movement of the 
 Brahms - Handel Variations and Schumann ’s ' Carnival ' ; 
 but a Scarlatti Sonata and a Gavotte by Arne be 
 admirably rhythmical and full of charm 

 Miss Jessie Hall have a decided personality , and a keen 
 sense of rhythm and tone - colour which enable she to give 
 spirit and character to whatever she play . her programme 
 at -Eolian Hall be no light one . begin with Bach ’s 
 french Suite in E , she go on to Schumann ’s Etudes 
 Symphoniques , the * Waldstein ’ Sonata , Prelude , Fugue , 
 and Variation by Franck - Bauer , and end with Albeniz 

 Josef Hofmann be a great master of his craft , possess 
 that rare power of fill his audience with new life and 
 vigour . _ he have appear twice recently — the first time 
 in a violin and pianoforte recital with Madame Lea 
 Luboschutz , also a fine artist , in which they play three 
 Sonatas — Grieg in F , César Franck in a , and the ' Kreutzer ' 
 of Beethoven — all old friend whom one be delighted to 
 hear again . the fine style and perfect ensemble be 
 memorable . at his own recital he make a great opening 
 with the rarely - hear Schumann Sonata in F minor 

 perhaps the most notable feature of Miss Youra Guller ’s 
 recital at " Eolian Hall be her exceptionally pure , delicate 

 Music in the province 

 BIRMINGHAM,—The complete set of programme for the 
 eight symphony concert to be give by the City Orchestra 
 next season be to hand . the day for the concert be to be 
 alter from Tuesday to Thursday , which will be a well 
 arrangement for many people in the outside town where 
 Thursday be the early - closing day . the first concert 
 of the series will be hold at Central Hall , but the other 
 seven be to take place as usual in the Town Hall , which be 
 at present undergo reconstruction . the general scheme 
 for the series be fairly enterprising , yet do not include any 
 work of the ultra - modern school likely to frighten off the 
 more timid concert - goer . the principal item in the first 
 programme be Beethoven ’s Pianoforte Concerto No . 5 , in 
 e flat ( the ' emperor ' ) , with Mr. Harold Bauer as soloist , 
 Schubert ’s ' Fierrabras ' Overture , and the ' Enigma ' 
 variation . four movement from Holst ’s ' the planet ' 
 be promise on November 11 , and Mr. Albert Sammons 
 will play Elgar ’s Violin Concerto in b minor . at 
 the third concert Bruno Walter will be the conductor 
 instead of Mr. Boult , who , however , will conduct all the 
 other programme . the great c major Symphony of 
 Schubert should have a fine performance with Herr Walter 

 give on the same evening . at the December concert , 
 Mr. Harold Samuel will play Brahms ’s rarely - hear 
 Pianoforte Concerto No . 2 , in b flat , and will also take part 
 in the Triple Concerto in A minor , for pianoforté , violin , and 
 flute . Mozart ’s Symphony No . 38 , in D ( the ' Prague ' ) , 
 figure in the same programme . the next concert include 
 a novelty in the shape of Mahler ’s Symphony No . 4 , in G , 
 very little music by this composer have be hear at 
 Birmingham , and the event will be await with special 
 interest . the work require a soprano soloist in the last 
 movement , and Miss Dorothy Silk have be call upon to 
 fill the réle . Dukas ’s ' L’Apprenti Sorcier ' be include in 
 this programme . Mr. Johan Hock , the well - know 
 ’ cellist in the Catterall Quartet , be to appear at the sixth 
 concert , and will play a Cello Concerto of Schumann , and 
 he will , presumably , also be the Don Quixote in a 
 performance of Strauss ’s symphonic poem of that name , 
 an almost unknown overture by Weber , ' ruler of the 
 spirit , ' will be of special interest , and with Debussy ’s 
 * L’Aprés - midi d’un Faune ' complete a programme which 
 should suit all musical taste . composer of very varying 
 style will be represent in the programme of the next 
 concert — Brahms , Mozart , Bantock , Vaughan Williams , 
 and Delius all appear in turn . Delius ’s symphonic poem , 
 ' Paris , ' be something of a novelty , and will be hear with 
 interest by those who love the music of this composer , 
 Miss Jelly d’Aranyi be the soloist on this occasion , and will 
 play Mozart ’s Violin Concerto No . 4 , in D , and a piece 
 by Vaughan Williams , ' the lark ascend . ' Bantock ’s 
 ' Dante and Beatrice ' and Brahms ’s ' tragic ' Overture will 
 also be hear . the chorus of the Festival Choral Society 
 be to help in the final concert of the season , which will be a 
 celebration of the Beethoven centenary . the * Egmont ’ 
 Overture , song from ' Egmont , ' and the big Choral 
 Symphony will be give . the Sunday concert be to 
 be hold this season in the West - End Cinema , where there be 
 more seating accommodation . the Children ’s concert 
 will be give as usual , and a series of lunch - hour concert 
 be be arrange 

 BRADFORD.—The programme of the come Festival of 
 chamber music ( October 5 and 6 ) have be announce , the 
 four programme include Brahms ’s Sextet and Clarinet 
 ( Quintet , Schénberg ’s ' Verklarte Nacht . ' Bax ’s Oboe and 
 Harp ( uintet , Elgar ’s Pianoforte ( uintet , Debussy ’s 
 ( Quartet , the second Violin Sonata of Delius , the Phantasy 
 Quartet of Goossens , Ravel ’s Introduction and Allegro for 
 string and wind , and work of Mozart , Beethoven , Franck , 
 Dvorak , and Fauré , the principal artist will be the 
 Virtuoso Quartet 

 BRIGHTON.—The Charlier Quartet from Liége play at 
 the Pavilion on July 2 and 3 , the programme include a 
 ' selection in Old style ' from the work of Grétry , a 
 Quartet in C sharp minor by Rogister , and two serenade 
 by Joseph Jongen 

 HOLLAND 

 Utrecht have a Municipal Orchestra that maintain its 
 standard by refuse to lay down its instrument in the 
 summer . in the third week in June , under their regular 
 conductor , Evert Cornelis , these player give an excellent 
 Summer Music Festival , the programme of which 
 combine in happy measure the classical and the modern . 
 Handel 's ' water , Music , ' Beethoven ’s * Ninth , ' Mozart ’s 
 * Haffner ' Serenade , Mahler ’s * Lied von der Erde , ' as well 
 as short work by these same composer , be follow 
 quite appropriately by Debussy ’s three Nocturnes , 
 Chausson ’s ' Chant Funébre ' ( orchestrate by d’Indy ) , 
 and Honegger ’s ' Cantique de Paques , ' besides song 
 by Rimsky - Korsakov and Moussorgsky , sing by 

 Vera Janacopulos . in spite of her excellent singing , this 
 artist be rather wear out the welcome of some of these 
 number . she and Lotte Leonard be the visit 
 vocalist . it be pleasing to record that the dutch singer — 
 Suze Luger , Louis van Tulder , and Thom Denijs — be in 
 every respect their worthy associate , and that the orchestra 
 and the local branch of Toonkunst play and sing well . 
 the maintenance of these provincial orchestra be a difficult 
 problem here as elsewhere , and unless something 
 unforeseen happen it appear likely that the orchestra at 
 Groningen , an outpost that need all the musical help 
 that can be give to it , will not resume its activity next 
 season . the summer season at Scheveningen have start 
 with bad weather and few visitor , but with programme 
 that be thoroughly good and , if not exciting , at least 
 contain a number of item one seldom hear in England . 
 the one sensation have be a two day ’ visit of Paul 
 Whiteman and his band , and although critic and public 
 alike have be quick to see that many of his ' new 
 method ' and ' new idea ' be simply old one make 
 prominent by advertisement or by actual manner of 
 performance , the brilliant instrumentation of his repertory , 
 the remarkable virtuosity of his player , and the novelty of 
 some of his tone - colour have win favourable comment . 
 publicly and privately , however , much comment have be 
 make on the lack of newness and piquancy in his rhythm . 
 of actual novelty there have as yet be two , a ' jiidische 
 Trilogie ' by Asger Hamerik , the danish - american 
 composer , and a group of three song with orchestral 
 accompaniment by Leo Ruygrok , the second ' cellist and , 
 in the winter , second conductor of the Residentie Orchestra . 
 Hamerik ’s work be a pleasant , richly score miniature 
 symphony , reminiscent in its thematic material of various 
 19th - century leader and in its orchestration chiefly of 
 Wagner . a long solo for bassoon lightly accompany 
 form the bulk of the middle movement , ' lament , ' and 
 prove not only unusual but highly effective . the song 
 of Ruygrok , sing with pure tone and good expression by 
 Madame Stotijn - Molenaar , be evidently exercise in 
 writing for voice and orchestra by a composer who be a 
 master of the latter but scarcely yet master of the art of 
 combine the two . among other item of outstanding 
 interest already perform have be a Concertino for oboe 
 and orchestra of which the one really striking section be the 
 middle * ' romance , ' beautifully play by J. H. Stotijn ; 
 Strauss ’s ' also sprach Zarathustra , ' play in the same 
 programme as Wagner 's ' Faust ' Overture and Beethoven ’s 
 C minor Symphony ; two performance of Strauss ’s 
 * Don Juan ' and of Tchaikovsky ’s ' Romeo and ' Juliet ' 
 overture ; and small item by Alex Voormolen , Joh . 
 Wagenaar , Lekeu , & c. under Georg Schnéevoigt and 
 his capable assistant , Ignaz Neumark , the Residentie 
 Orchestra be play well than ever . other music be 
 represent chiefly by organ recital , and military band 
 concert in the open air , the former be on a high 
 standard of programme selection though not of performance , 
 than the latter . mention must be make of the incidental 
 music to outdoor play , write by local composer , a good 
 example be that by Emile Enthoven for the Utrecht 
 University Lustrum Feast play ' Ichnaton , ' in which the 
 spirit of the piece be reflect without any arbitrary attempt 
 at reproduce the egyptian instrument or their effect . 
 this be perform by the Municipal Orchestra , under 
 Evert Cornelis . HERBERT ANTCLIFFE 

 VIENNA 
 PHE CRITICAL STATE of the OPERA 

