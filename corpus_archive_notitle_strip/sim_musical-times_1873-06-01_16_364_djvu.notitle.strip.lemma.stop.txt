concert , Mendelssohn Overture " Ruy Blas " 

 Rossini " Semiramide " ' orchestral pi 
 Dr. von Biilow performance Beethoven P 
 forte Concerto G attractive feature 
 tion . - wear vocal nrusic opera 
 tribute Madlle . Titiens , Madame ' Trebelli - Bet 
 Signor Mongini , Signor Agnesi , elicit 
 usual enthusiastic applause 
 adhere opinion classical work 

 warm weather . Mr. Manns , usual , conduct 

 PHILHARMONIC SOCIETY 

 tue principal feature concert , 28th 
 April , performance Beethoven Concertoin E flat , 
 Dr. Hans von Biilow , artist come witha 
 german reputation raise public expectation 
 high pitch excitement . rendering 
 great work fully realise ideal 
 Gould conscientiously ; evince 
 possession true artistic feeling consummate power 
 execution , especially Adagio Rondo , un- 
 questionable . reception enthusiastic ; , 
 performance Bach ' chromatic Fantasia 
 Fugue , " second concert , applause 
 prolonged , return platform play 
 ' Passepied , " composer ' " Suites 
 Anglaises . " orchestral piece Haydn charm- 
 ingly melodious Symphony G ( Oxford ) , Sir Sterndale 
 Bennett Overture , ' ' Naiades , " Mendelssohn " Re- 
 formation Symphony , " Wagner overture , ' ' Der Flie- 
 gende Hollander , " , steady conductor- 
 ship Mr. W. G. Cusins , finely play . Madlle . Gelmina 
 Valdi , fine contralto voice , somewhat overtax 
 power select trying Scena , " ' O Prétres de 
 Baal " ( ' Le Prophéte ’' ) , applause , , 
 prove opinion share ! 
 vocalist Madame Alvsleben , 
 necessary fully sustain reputation . 
 concert 12th ult . , programme include 
 new Violin Concerto , Mr. G. A. Macfarren , play 
 Herr Straus , Andante Rondo Molique 
 Concerto flute , perform Mr. Oliif Svendsen , 
 artist fairly win reputation country , 
 wonder miss season 
 accustomed place orchestra Majesty 
 Opera . Mr. Macfarren , certain , thank 
 record conviction excessive 
 merit new Concerto , work demand 
 time thought composer dismiss 
 hasty word , praise censure , 
 listener . impression consummate 
 knowledge capability instrument , 
 score masterly hand . move- 
 ment elaborated , prefer ' ' Larghetto , " 
 exquisitely melodious theme , accompani- 
 ment delicately write materially enhance 
 effect , final Rondo — vivacious movement , 
 brilliant passage appearto grow spontaneously , 
 instead , case , patch 
 display . Herr Straus prove thoroughly master 
 work , lead composer con- 
 clusion performance , applause en- 
 thusiastic . Mr. Svendsen rendering movement 
 Molique Concerto faultless , executive 
 power expression . tone pure , 
 play sympathetic command earnest attention 
 hearer , fact amply demonstrate genuine 
 expression approval greet 
 retire platform ; , general regret 
 appear entire work . 
 orchestral piece Mozart " grand " Symphony 
 C major , Beethoven C minor , overture 
 " Anacreon " ( Cherubini ) , " Le Nozze di Figaro " 
 ( Mozart ) . Madlle . Justine Macvitz , Madlle . Alwina 
 Valeria vocalist , music choose 
 particularly interesting 

 108 

 WAGNER SOCIETY 

 Tue concert present series 
 St. James Hall , 9th ult . , success de- 
 cisive set rest doubt fate com- 
 poser music country . overture " Der Flie- 
 gende Hollander " receive perfect enthusiasm , 
 extract " Tannhiuser " " Lohengrin , " 
 perform concert , 
 applaud vigour astonish 
 believe generation 
 Wagner work accept . Mr. Hidward Dannreuther , 
 conduct composition , resign bdton 
 Dr. Hans von Biilow , direct introduction , 
 finale Act , ' ' Tristan und Isolde , " 
 entirely memory , vigour pro- 
 duce inagical effect band . Dr. Biilow per- 
 form tifteen variation fugue theme 
 Finale Beethoven ' ' Eroica " Symphony , power 
 executive skill hold audience spell - bound ; 
 endorse burst ap- 
 plause greet , question 
 , disagree eccen- 
 tricitie playing , mastery key - board , 
 command gradation tone , stamp 
 artist high rank . vocalist 
 concert Madame Otto - Alvsleben , sing Hlsa 
 song ' " ' Lohengrin " Elizabeth Prayer 
 " Tannhauser ' " ' earn truthful expres- 
 sion . performance terminate stirring 
 " Huldigungsmarsch , " ' finely play . 
 glad find Society intend series 
 concert , commence November , 
 work Richard Wagner perform , 
 great classical master , Sebastian Bach 
 present time 

 Miss AcNEs ZIMMERMANN assemble highly - appreciative 
 audience Hanover Square Rooms evening 
 29th April , programme , usual , appeal 
 cultivated musical taste . praiseworthy 
 modesty , Miss Zimmermann introduce piece 
 composition , " ' Suite ' " ' pianoforte , violin , 
 violoncello , speak favourable term 
 concert year . characteristic chain little 
 gem beautifully play composer , Madame 
 Norman - Neruda Herr Daubert , warmly applaud , 
 especially clever " Canon la 7itme " final 
 " Gigue . " effective pianoforte arrangement , Miss 
 Zimmermann , Handel overture ' ' Ariodante , " " Men- 
 delssohn variation pianoforte violoncello , 
 Brahms Quartett G minor , pianoforte , Violin , viola , 
 violoncello , piece concert- 
 giver display exceptional power pianist , 
 uee scarcely composition 
 equally successful . vocalist Miss Abbie Whinery 
 aud Madame Anna Regan - Schimon , - lady 
 kindly supply place Mr. Santley , whowas absent 
 indisposition . glad find Miss Zimmermann 
 announce programme door 
 concert - room close performance 
 piece , firmness act 
 notice refuse egress person , 
 usual , remain composition commence , 
 desirous treat voluntary play 
 room . reader know anxious 
 institute reform , warmly con- 
 gratulate Miss Zimmermann courage 
 carry 

 Tue Southwark Choral Society excellent concer , 
 Emerson Street School - room , Tuesday , 
 ult . chief feature evening small 
 tion - know Handel oratorio , " ' Joseph , " 
 areading , Mr : W. F. Taunton . solo 
 Master Porter , Miss Dear , Miss L. Giblett , Miss Jenki 
 Mrs. Underwood , Mr. A. Bunker ; Mr. W. H 
 harmonium , Mr. Ralph Horner piano , 
 valuable aid , solo accompaniment . M , 
 J. Courtnay conduct usual ability 

 excellent concert St. James 
 10th ult . Mr. J. G. Calicott , artist nam 
 long connect Mr. Henry Leslie concer 
 ensure large audience , cordial w 
 operation eminent vocalist instr 
 mentalist . admirable programme provide , 
 principal singer Miss HKdith Wynne , Miss Hi 
 D’Alton , Madile . Justine Macvitz , Mr. Edward Lloyd , Mf 
 Maybrick , Signor Borella , - music suy 
 perfection Mr. Henry Leslie choir . selectiog 
 include melodious - write - song tht 
 concert - giver — ' ' love wake weep , " " ' Hark , 
 pretty lark’’—(the time ) , 
 favourite choral composition Sullivan , Benedict 
 Henry Leslie . ' ' Andante " ' Allegro " 
 Beethoven Quintett e flat finely render Mess 
 Lazarus , C. Harper , Hutchins , Horton , J. G. Calleoti 
 applaud ; feature programme Wil 
 violin performance Mr. Henry Holmes . ' accom 
 panist Sir Julius Benedict , Mr. John C. Ward , Signa 
 Pinsuti , , Mr. J. G. Callcott 

 band real negro vocalist appear m 
 tropolis past month , concert , whid 
 highly successful . troop consist { og 
 male seven female , emanci 
 slave . singing enlist sympathy audi 
 extreme purity ; simple eloquence maki 
 somewhat uncouth sacred verse , thel 
 music ally , acceptable hearer , othé 
 circumstance , pronounce word profane . theg 
 " Jubilee Singers , " term , — 
 Fisk University , object visit count 
 raise fund build " Jubilee Hall " 1n conneetidl 
 University . little doubt 
 succeed laudable undertaking , concer 
 fully attend , popularity 
 creasing 

 MUSICAL TIMES.—Jvnz 1 , 1873 . 109 

 music Beethoven othercomposer : | transcription , severely tax power pianist . 
 rendering composition Bach , Chopin , Liszt | conventional word commendation 
 vad = ee , aaegie . » Parone apart — oe eg < 0 . = — mr paw ay 
 y ; concert als agrove , hand appeal po 
 cages 5 ame interpretation " — — - | — hardly — Sa Te pas pli 
 e inuetto , " marvellous display | membere instrument ev 
 manual dexterity , combine true poetical feeling . | Molique write , concert work cast 
 arrival England | truly classical form Mr. Silas — Quintett iano- 
 note v0 nerva ~ — forte , — Ripe . — — erage ir hg yt 
 desire place record wonderful exhi- | compose concertina con- 
 bition memory , believe | viction quality deserve ex- 
 matter intelligent listener | tensive recognition . expressive power 
 concern . reception recital | instrument successfully reveal MS . " Duo 
 icing qoced cae Ges io te Einglacd om setiense ah| Gounedy * Mkieciic ® tp Oohasy este ieureae alana 
 appeal hearer exceptional eloquence | concert - giver ) , warmly de- 
 h ppeal h h th ptional el y g 
 Hans von Biilow . | servedly applaud . Messrs. Dando , J. Zerbini , 
 Tue fifteenth annual concert Great Northern | Daubert lend valuable assistance instrumental 
 Musical Society place 2nd ult . large Meet- | department ; vocalist Miss Matilda Scott 

 ing ’ | Miss Abbie Whinery , Miss Atkins , Miss Arabella West- 
 Rosy ag . ecbiagige te ane palbordpe ae cgewre EP peri brook ; Herr Oberthtir contribute solo harp , 

 Miss Jeannie Brown encore Benedict ' rock me| Tux 219th Festival Sons Clergy hold 
 sleep , " similar compliment award for| St. Paul Cathedral 14th ult . , large congre- 
 Gounod " Serenade , " Miss Kate Worth , only| gation . choir consist 250 voice , 
 recently hear metropolitan circle . Mr. W. steady conductorship Dr. Stainer , Mr. George Cooper 
 Bowyer contribute De Beriot Seventh Air Varied , with| preside organ . service render 
 good tone style . impressively , " Magnificat " ' ' Nunc 

 Mrs. Joun Macrarren evening concert | ) Dimittis , " composition Dr. Stainer , especially pro- 
 Hanover Square Rooms 9th ult . , large | ducing marked effect listener , result attribu- 
 audience . performance Beethoven ' moonlight " | table alike truly devotional masterly setting 
 Sonata , Rondo Dussek , Mozart Sonatain piano- | word admirable manner sing . 
 forte violin ( Mr. Carrodus ) , Chopin Polonaise , | complete orchestra ( lead Mr. Weist Hill ) play 
 pianoforte violoncello ( Herr Daubert ) , exhibit | movement Mendelssohn " ' hymn praise , " 
 power pianist high order effect , | commencement service ; choral por- 
 warmly applaud . vocalist | tion work anthem , tenor 
 Madlle . Nita Gaetano , Miss Banks , Mr. Vernon Rigby , Signor | solo sing Mr. Kerr Gedge . Psalms 
 Caravoglia , Mr. Santley , sing , | sing Sir John Goss chantin E. sermon 
 decide success , " hunting song , " compose | " hallelujah " chorus choir , 
 Signor Piatti . Mr. Walter Macfarren efficient | conclude voluntary , finely play Mr. George Cooper , 
 accompanist . include beautiful ' ' Andante , " Mr. Henry Smart 

 Mr. Mrs. Ricuarp BLaGRove annual morning concert } Mr. E. H. Tuorne evening concert place 
 Beethoven Rooms 21st ult . , draw large | 30th April , Hanover Square Rooms , large 
 fashionable audience . principal feature pro-| audience . programme excellently select , 
 gramme pianoforte performance Mrs. Blagrove| display concert - giver power 
 concertina playing Mr. Blagrove , , ! pianist high class , exhibition 
 needless , high order merit . mrs.| claim composer . trio g major , 
 Blagrove ( earn reputation public as| pianoforte , violin , violoncello ( ably 
 Miss Freeth ) firm touch , facile execution , com- | assist Herr Louis Ries Mr. Howell ) deservedly 
 mand gradation tone , ensure | receive warm mark favour , vocal 
 sympathy hearer ; composition | piece — ' " Lake Waterfall " ( - song 
 select occasion quality display to| choir ) , Psalm , lady ’ voice , song , " 
 utmost advantage . especially | faded Violet " ( charmingly sing Miss Enriquez en- 
 rendering Stephen Heller arrangement " laj|cored)—are work melodious refined character 
 Truite , " little piece , , course mere|as justify anticipate successful career 

 110 

 MUSICAL TIMES.—Junz 1 , 1873 

 writer lately appeal London 
 public . Trio remarkably write 
 instrument , movement ( precede short 
 Adagio ) vivacity opening phrase , 
 contain charmingly melodious second subject . 
 Adagio , follow , base theme speedily 
 win way sympathy audience ; 
 " Finale la Polonaise , " brisk animated , passage 
 pianoforte especially , extremely brilliant 
 effective . - song compose thorough 
 appreciation verse , ' 
 Year round . " melody appropriately simple , 
 voice write care . point 
 tell admirably performance , unison 
 passage tonic minor , boldly sing choir , 
 afford excellent contrast subdued por- 
 tion composition . doubt , 
 * * Lake Waterfall , " favourite Choral 
 Societies . Mr. Thorne performance Beethoven ' Sonata 
 Appassionata , " pianoforte Bach Sonata 
 major ( Herr Ries join violin ) prove 
 alike thorough mastery executive difficulty , 
 true feeling classical music . choir 
 voice sing choral music , effect . Mr. 
 Thorne second evening concert announce 6th 
 instant , perform new trio C minor ( 
 Messrs. Henry Holmes Paque ) , pianoforte 
 piece composition 

 Mr , Riwiey PRENTICE evening concert , 14th ult . , 
 Hanover Square Rooms , attend , 
 respect thoroughly successful . concert - giver 

 erformance Beethoven " Sonata Appassionata " 

 ighly appreciate , applause elicit 
 genuine deserved . programme 
 include " Allegro Assai " Schubert , 
 violin , viola , violoncello , Mendelssohn variation 
 d major , pianoforte violoncello ( Mr. 
 Prentice ably second Mr. Walter Pettit ) , Weber 
 * Duo Concertante , " e flat , pianoforte clarionet 
 ( Messrs. Prentice Lazarus ) , Quintett 
 G minor , Mr. Prout , pianoforte , violin , viola , 
 violoncello , excellently play Messrs. Prentice , 
 Holmes , Folkes , Barnett , Reed . Miss Katharine Poyntz , 
 Madame Patey , Mr. Cummings vocalist , 
 highly effective ; sacred song , Mr. 
 Prentice , " ' hear prayer ' " ' ( exquisitely sing Madame 
 Patey ) , receive enthusiastic encore . Mr. Minson ac- 
 companie ability 

 concise practical explanation rule simply 
 Harmony Thorough Bass , chapter time , K 
 interval , general elementary musical knowledge . 
 Thomas Smith 

 TxoseE constant reader cur review col 
 time convince amateur pianist 
 study theory — music , itis 
 want sufficient supply book subjec , 
 spite fact , , , th 
 " organise choir - master Church Music Society fy 
 Archdeaconry Sudbury , " great fault whichiy 
 , attempt teach , teach 
 thoroughly . instance , ' scale , " author , 
 " compose thirteen semitone , comprise @ 
 octave ; " " ( major ) scale compose precisely thy 
 manner — i.e. , semitone anj 
 fourth , seventh eighth note scale , th 
 r tone . " constructiq 
 aminor scale ; , speak key , » 
 tell " major key relative minor ; " 
 , minor scale , pupil lefty 
 discover itis form . false notiong 
 believe composition major key iti 
 prove minor enforce word ; 
 " find piece major minor 
 look line music , fifth 
 key repeatedly sharpen position accort 
 e signature , major key represent y 
 signature , relative minor , affected note bei 
 seventh minor scale . " , teach thy 
 student thing cautione 
 . Mr. Smith turn , example , th 
 opening Beethoven " Sonata pathétique , " whe 
 ther believe pupil , average intellett , 
 ' ' look line music , " ant 
 imagine key - note c ; , , isit 
 utterly absurd talk " fifth key ' 
 ' ' repeatedly sharpen ? " whilst young player 
 allow grow instruct 
 right believe , expect limited 
 musical knowledge rise generation . 
 chapter " Time , " explanation di 
 ference duple triple rhythm ; " 
 time " " common time " 
 information compound time gather 
 following observation : " simple time thos 
 principal accent bar , " 
 infer compound time : w 
 mean loss understand . ' chapter om 
 interval contain complete table ar 
 chromatic , diatonic interval leave find 
 student , guide " com 
 pose note incidental signature key . " 
 speak inversion common chord , 0 
 author figure 6 indicate harmony 
 play , " common chord sixth n 
 , " previously explain chord 
 form bass instead fu » 
 damental note . chord diminished seventh , bh 
 , " form raise bass dominant seveutl 
 semitone ; note incident chord remain 
 unaltered ; " mention fact thal 
 root key completely change process 
 question ' little knowledge " kind 
 " dangerous thing " young pupil . agree 
 Mr. Smith " popular idea use Time 
 music erroneous , " , truly - y 
 " figure denote time commencement & 
 piece way indicate speed 
 play . " observation usual method 
 teaching weight ; , ) , 
 matter contain book want careful cob 

 sideration author bestow ; wef 

 Church Modes , " publish Novello , Ewer Co 

 J. STEwARtT.—Thompson Collection Scottish Airs edit arrange 
 Haydn Beethoven ( publish volume ) , Napier 
 Collection , , Violin , voice . Bass , edit Haydn 
 print , occasionally meet ssecond - hand 
 music shop 

 AMATEUR.—The Organ use performance Oratorio , 
 case composer intend . organ 
 play occasion allude write composer 
 . loudness organ explain position 
 * Amateur " occupy hail 

 scarcely add Mr. Best fully maintain high 
 reputation gain , amply realise sanguine expecta- 
 tion form , display power varied 
 capability organ good advantage . evening pro- 
 gramme — Andante , H. Smart ; Allegretto , A. Guilmant ; March 
 ( vox humana ) , W. T. Best ; variation hymn , " o sanctis- 
 sima , " F , Lux ; Andante , Handel ; Marche funebre , et chant seraphique , 
 A. Guilmant . occasion piece intersperse 
 verse hymn thechoir . atthe conclusion collection 
 = behalf organ fund , total realise 
 48 

 LINCOLN.—The - opening Wesleyan Chapel organ , 
 extensive addition improvement Mr. T. H. Nicholson , organ 
 builder , Lincoln , place 23rd ult . , recital 
 Mr. W. T. Best , celebrated organist Liverpool . 
 instrument good , good , county Lin- 
 coln . p plete clavier , separate pedal organ . 
 thirtv - stop ( thirty - sound stop ) 
 contain 1,635 pipe . wind supply large horizontal 
 bellow , place basement remote organ , 
 blow hydraulic engine . recital place 
 afternoon , company number 700 . piece se- 
 lecte thoroughly test tne instrument , Mr. Nichol- 
 son fully deserve high reputation attain organ 
 builder . Mr. Best manipulation wonderful , rich 
 musical treat provide city year . 
 follow programme afternoon recital : overture 
 oratorio , Samson ( Handel ) ; Andante variation ( J. Lem- 
 mens ) ; prelude Fugue ( Bach ) ; allegretto , Symphony 
 hymn Praise ( Mendelssohn ) ; march , Orchestral Suite 
 ( F. Lachner ) ; air variation ( W. T. Best ) ; chorus ' hallelujah , " 
 ( Mount Olives ) , Beethoven . second recital place 
 7 p.m. gallery chapel crowd excess , 
 low fairly fill , company number 1,700 . 
 programme follow : Organ Concerto ( Handel ) ; variation 
 hymn , " O Sanctissima ’' ( F. Lux ) ; Organ Sonata ( Mendelssohn ) ; 
 chorus , choir , " Hallelujah " ( Messiah ) , Handel ; Toccata 
 Fugue ( Bach ) ; air variation ( Haydn ) ; air , ' layeth 
 beam chamber water " ( Handel ) ; chorus , choir , 
 creation ' heaven tell " ( Haydn ) 
 chapel choir assist member Cathedral choir 
 city , chorus render style rarely hear 
 Lincoln . " hallelujah " wonderfully effective , de- 
 servedly applaud enthusiastically . Mr. C. Roberts conduct 
 usual ability 

 LiveRPooL.—Miss Lillian Barrett annual concert 
 Ist ult . , assist Mrs. Skeaf , Mrs. G. Keef , Mr. Geo . Barton , 
 Mr. G. Hardie , Mr. W. Jarrett Roberts . Mr. Skeaf preside 
 piano . numerous audience , concert 
 satisfaction 

 ScarBorouGH.—On Tuesday evening 29th April , invitation 
 concert member Amateur Vocal Instru- 
 mental Society , tue Prince Wales Hotel . ' Society 
 establish year ago , office conductor 
 undertake Dr. Sloman labour love , wish pro- 
 mote taste good music inhabitant Scarborough . 
 entire work , - song , publicly 
 perform member Society . concert 29th 
 April consist Mozart 12th Mass , foilowe miscellaneous selec- 
 tion consist - song , vocal solo , instrumental trio . 
 choir instrumentalist performance 
 Mass , especially difficult fugue ' Cum Sancto Spiritu " ( 
 frequently omit amateur society ) , nad 
 carefully train 

 SeLkirk.—The Choral Union , form November , 
 meet weekly practice winter , 
 conductorship Mr. F , K. Stroh , organist Episcopal Church , 
 Selkirk , concert Volunteer Hall evening 
 Friday , 25th April . 70 member Society 
 platform , service Miss H. C. Lindley Mr. G. M. 
 Dayidson , Edinburgh , secure soprano tenor 
 solo , Mr. C. Guild ( Dalkeith ) , act accompanist . concert 
 consist , sacred secular . , chorus 
 anthem work Handel , Haydn , Beethoven , 
 Mozart , & c. ; second comprise glee part- 
 song . able conductor , member Union render 
 piece precision expression evidence skilful 
 training , careful practice theirs ; sacred 
 solos artd song Miss Lindley Mr. Davidson contribute 
 success entertainment 

 Sovutuport.—On Tuesday 29th April Southport Choral 
 Union second concert Tewn Hall , band 
 chorus upwards 100 performer . band lead Mr. 
 Lawson , anu Mr. H. Hudson preside harmonium . prin- 
 cipal engage Miss Clelland , Mr. Edmondson Mr. A. Wroe . 
 programme iste d Messe Solennelle , 
 selection Mendelssohn constitute second . man- 
 ner band chorus acquit evidence 
 appreciative study careful training , reflect utmost credit 
 conductor Mr. Dobson , Society 
 steady progress 

 ATURE LULLABY . arrange voice 

 solo . subject Beethoven . S8vo . , 6d 

 ELCOME NEW DAY . arrange 

 professor MUSIC , reside large 

 provincial town , close sea , require partner . 
 gentleman wish join excelient Pianist , sound 
 musician , competent teach singing , harmony , composition . 
 practice year ’ standing , increase year 
 year . address ' ' Beethoven , " care Messrs. Metzler Co. , 37 , Great 
 Marlborough - street , London , W 

 LADY , good vocalist musician , having 
 time disengage day , wish engage 

 126 

 edit 
 ATALIA MACFARREN BERTHOLD TOURS agrly 
 . petore , 13d . 
 curren 
 AUBER FRA DIAVOLO . J 
 1 . comrade , fillyourglasse . . . ie DONIZETTI LUCREZIA BORGIA . 
 en bons militaires . : , : , 49 ] Sh . Blot 8 ped . , ri . ' 
 2 . hail , festal morning . . 
 Crest avant file , 4 ‘ : ; ; 2d.| 33 . window . 
 - bs panes éla finestra . 
 Perens : 34 . know away : 
 ; ' AU BER S MASANIELLO . II segreto esser felice . instal " " Mig Cani 
 3 . hail bright auspicious day . ' é : , ; ad . BS Che 
 . hae — objet de notre amour . MOZART DON GIOVANNI . 17 alittle 
 . ail bright auspicious day : enjoy whi invi 
 Du Prince objet = notre amour ad . 35 tet " Giovtn ante ip - yiocttn ' ¢ * Sele 
 Pt " tt z ' tovinette , che fate ’ amore . 9 mid 
 + f ta ey igh MOZART LE NOZZ jeu , 
 E DI FIGARO . 101 adieu , 
 6 . ee ( Fishermen chorus ) ' ' ; 2d . | 36 . — - flower . . # 3 aa 
 + . Bebe Bens iovani liete . ai 
 : , 7 ic oie : ; ' : ' . , sant nl — ate aninent . 7 ' ' ' ' 78 Awake 
 8 . age - wish buy . ( Market chorus ) ' ' 3d . | 38 PE ssn os « rong 382 Awake 
 u marché qui vient d ( j ; edt : Pa , Vibe o > 
 9 . come , av bes vuamal 3d Amant , costes . eae 
 Courons & la vengeance . : ; . ; ; ; v1 ’ R SRE { 
 o poenreash la ve¥e ROSSINI IL BARBIERE . Sate 
 Saint bien heureux . 39 . Sir , humbly thank yourhonour . . . + + 6 Britons 
 60 . hail , noble victor . ( Marchand chorus ) . j ' 6d oh , 
 honneur ! honneur et gloire . ; VERDI IL TROVATORE 160 Cel 
 : ; p 2 ae 130 calm : 
 BEETHOVEN FIDELIO . ot ete oe hoy 
 10 . oh whatdelight . ( prisoners’choru ) . . . « . . 3d.|4 % dice invite leisure ! " ede beat 
 o welche Lust . co’dadi ase : + NS Chloe 
 11 . farewell , thou warm sunny beam . 1 , | 42 + miserere pay — 2 Chloe s 
 Leb ’ wohl , du warmes Sonnenlicht . j , , ; ; : } : ; 
 VERDI RIGOLETTO 164 C 
 77 ? t c y . ° . : om 
 BELLINI PURITANI . 43 . hush , silence fulfil errand 4 33 come , 
 12 . yonder bugle callsus . : ; va zitti , zitti , moviamo vendetta come , f 
 quando la tromba — ; " | 44 vanes lonely abode direoted . 1 g # 0 . come 1 
 13 . rejoice ! ! ; ; ; ; - | Scorrendo uniti remota 
 esta . . ; ; 
 14 . Noble Arthur , welcome , . ° r F . d WAGNER LOHENGRIN , * cote 
 ‘ aid Avtereianors . * + + TG . ) 45 . hath summon usbetime . . « + - come | 
 15 . Isought thee . ; , . . : Friih’n versammelt uns der Ru , : Ful 
 ; ; od f 1 fa 
 te , o cara . ° * | 46 . follow lead ! . > ' _ ' chor 
 16.fatalday . . . . 1 1 ee ve zum streite séumet nicht ! * " p24 come u 
 pice pe _ % | 47 . joy attend thee det ama Gently 
 Gesegnet soll sie schreiten ( a.7.1 
 48 . faithful true lead ye forth 3 . 
 , BELLINI NORMA . Treulich gefiihrt ziehet dain 3 * 
 17 . Hasten , ye Druids , height cend ' ' f ; " mvondy 
 te sui colle , O diuidt " sascend . . . « sd . WAGNER TANNHAUSER . Eventic 
 zhi Naoaiaicometh f 3 : : d 49 . hail , bright abode ( March ) . ' P - . ' Fair Fi 
 Norma viene . . . . nae Frew lig begriissen . fair 
 19 . ? , yettheylinger . . ra . | 3 ° j joy . ( Pilgrim chorus ) 
 ee non parti ? finoraé al campo . . . . Begliickt darf nun dich ed Ki 
 . engeance , vengeance . . . e . sy 
 Guerra , guerra ! 1d . WEBER OBERON . tir 
 51 . wie fairy foot fall . . ; ; ' e oem 8 
 B Nr ~ ieve il pié cola volgiam 8 
 ae ELLINI LA SONNAMBULA . 52 . honour eater ee ie Cee ye ose 
 é Vit cy , " Amina ! " ! : , : : , : + 53 . gl neg Caliph Gente 
 va ! viv ; 3 . glory Calip " ' ' . é ' . = pe 
 = Fare Gower ol memouniis © + + L wrnogoul ee es ai 
 vezia : 54 . stay coral cave 4 gi ia — orlou 
 23 . bist — ieee ak aed Oe . : ee ~ potria fra l'onde restar God sar 
 24 . moment shelter rest ' od ora : oS noma ' CREO ET j ing hrr 
 Qui la selva é pitt folta ed ombrosa . > 6 . . . ( mixed voice ) good m 
 2 Py ! ee good ni 
 DONIZETTY LA FIGLIA . WEBER DER FREYSCHUETZ . good ni 
 25 . pleasure , gladness " piece , se 4 eee wags 
 cantiamo , cantiamo . , : , : % Victoria , hey tay 
 niu taoas tha aaa fe la 58 . Bridal w reath thee bind hail ! 
 ‘ sprona il tamburo ¢ oes lll : : : : 1d . Wir winden dir den Jungfernkranz Hi ¥ 
 , 27 , Rataplan , Rataplan 59 . joy Hunter . ( Huntsman chorus ) é f ‘ ail , bl 
 Retaplan , Fetaplan . rune 6 . F . ; ; rd . gleicht wohl auf Erden , 
 
 ROSSINI GUILLAUME TELL . happy | 
 DONIZETTI LUCIA . . ee rosymom , . . areal Hark ! t 
 28 . let roam th yest yy enoheg 
 peared oe omer equate ° f 1d . | 62 . come , flower crown bower . . . . 
 29 . hail , happy bridal day Hyménée , ta journée Hark ! 
 wh te d’immenso giubilo . ' 5 : ; cach hea mK : nol raphy bonocd _ 
 30 . login weddemine vet uelle sauvage harmonie 
 Chi rafirena cng . ah lea ee . SE | 64 . hail mighty Pdler V4.4 BIN oatgh 
 31 . warlike minstrelsy : | Gloire au pouvoir supréme Hear , hi 
 D'immenso ginbslo . ; , : . : F 1d . | 65 . Swift bird summer sky ( Tyrolean ) ; ee 
 | Toi que Voiseau ne suivrait pas ge 
 ( continue . ) sle 
 LON 

 LONDON : NOVELLO , EWER & CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C. ) 
 NEW YORK : J. L. PETERS , 599 , BROADWAY 

 sleep , soft evening breeze 
 Sir H. R. Bishop 

 278 snow - flake - Arthur Cottam 
 16 soldier , brave — , 
 ( 8.S.A.T7.B. ) Gastoldi 
 341 Songtospring - Francesco Berger 
 3 Soonaslcarelessstrayed - esta 
 45 spring delight ( s.4.1.B. ) Miiller 
 145 spring delight ( T.T.B.B. ) Miiller 
 216 spring - time ( t.1.B.B. ) - Beethoven 
 353 summer winter - B. Tours 
 95 summer coming ( 4 treble 

 7 sweet enslaver ( Round , 3 voice ) 
 J. Atterbury 

