3 

 4 ; ; 
 Sonata . 5 , F major ( op . 24 ) x ; 
 Sonata . 6 , major ( op . 30 , . 1 ) .. j Beethoven 

 . 2 ) 
 

 PUGNO 

 Beethoven 

 s. 6d . , 7s . 6d . , 5s . ( reserve ) . 2s . 6d . 1s . ( u nreserve ) . 
 ROB ER1 * NEWMAN , 320 , Regent Street , W. , 
 manager Queen Hatt Orcnestra ( Ltd. ) . 
 ; QUEEN HALL 

 teach art criticising , 
 remain find way . deem 
 doubtful criticism , altogether 
 matter keen personal discernment , 
 teach . musical criticism 
 concerned , case difficult 
 perplexing , matter appertain 
 wsthetic music infancy . 
 extent afore - mention lesson , 
 lesson found similar plan , useful ; 
 reliable way teach criticism 

 criticism set fundamental disadvantage 
 soon confront modern work . whilst 
 grant , Mr. Newman , ' critic 
 right symphony - day 
 Beethoven symphony , ' ignore 
 fact difficult 
 reader amow right . Mr. 
 Herbert Antcliffe ( J / ustcal Times , March , 1911 ) 
 judiciously question possibility 
 knowledge contemporary work ; 
 add mean easy discard 
 prejudice time , view new 
 work clearly dispassionately 

 reason suggest distinc- 
 tion opinion statement mere 
 fact , distinction strive found 
 course tuition allude . ' 
 thousand esthetic judgment 
 agree , ' Mr. Newman , refer 
 work past . ' right opinion 

 expense Congress certain 
 large , guarantee fund form . 
 reach £ 10,000 . outside mor 

 impossible , substitute " national " foreign spelling , | 
 monstrosity " Biiro " " Bureau . " art | 
 music feel influence , Richard 
 Wagner carry principle commend- | 
 able thoroughness , sternly banish " welsch " | 
 word score . | 
 countryman , , direction , } 
 Beethoven propose substitute " Klangstiick " | 
 " sonata " ; Robert Schumann rightly 
 reject ridiculous " Bardiet " 
 " symphony , " allow " Werk 1 " supplant 
 " Opus 1 

 committee having decide incur indebtedness 
 percentage total fund . special 
 privilege far offer guarantor , 
 list meeting , concert , ETC 

 little Franz Liszt foresee , 
 pianoforte performance term ' Recitals , ' 
 important innovation bring world . 
 alive - day , gasp 
 astonishment number recital 
 London week — nay , day — 
 musical season ? broad - minded genius , 
 hate staleness convention , 
 note pianoforte recital - day 
 rut ; programme pianoforte recital 
 contain number piece 
 ad nauseam , year year , 
 time exist quantity splendid pianoforte music 
 unperforme . surely opinion | 
 matter undecided term 

 know exactly expect 
 typical modern pianoforte recital . Beethoven | 
 Sonata ; Chopin selection ; large | 
 Schumann work ; Brahms Intermezzi , 
 maybe Handel Paganini Variations ; 
 Liszt arrangement Bach organ fugue ; 
 certainly , sive gua non , Liszt hungarian 
 Rhapsody , blatant doxology . course , 
 work ( save Liszt Rhapsody ) | 
 cream pianoforte music ; surely work 
 . , example , Liszt 
 generally represent hungarian Rhapsodies , 
 write piece infinitely 
 superior artistic value 

 think mass pianoforte music bestow | 
 ungrateful world Anton Rubinstein : | 
 Caprices , valuable artistic ; Preludes | 
 Fugues , Op . 53 ; ' Kamenoi - Ostrow , ' gallery | 
 delightful portrait ; sonata , number | 
 nocturne , Impromptus , melody ( yes ; | 
 melody Rubinstein evergreen F ) ; | 
 cabinet piece . Rubinstein | 
 occur recital programme , generally | 
 connection ' Valse - Caprice , ' youthful | 
 freak disown composer mature year . 
 surely Fate irony gifted , genial , | 
 earnest composer know 
 people couple small inferior piece , 
 , rightly , think 

 MUSICAL TIMES.—May 1 , 1911 . 309 

 play pianoforte . use Sonatas ? hear — out- 
 . unaccustomed estimate — dozen . thirty- 
 hand badly . 1 level two| . Beethoven , 
 day bitter scorn sarcasm . i|composer . good world , 
 address talk ape . accuse lay | miss reasonable opportunity , 
 expression coarse slab . nearly ex-| grave having explore 

 lead house . gradually begin | quarter heritage . silent score 
 fnd term . learn | — thousand ! — speak . iam 
 nature , feel way , speak , its| shutout nolonger . visible end 
 close confidence , | find great delight | great wealth new experience . ordinary 
 capable fine gradation , plastic , | lifetime supply run short . 
 sensitive , responsive high degree . at| long lay futile siege strong - room 
 come home find ally | treasure lie , listen keyhole , peer 
 wide splendid capability . |in window , baffle blank 

 let understand , strict | steadfast wall . hold key . 
 sense , musical person . mean } . great 
 perform , shade success , } music remote sense . 
 instrument ; able recognize | hear symphony concerto 
 ' diminish seventh ' meet ; | concert , pass . casual acquaint- 
 grave doubt meaning | ance complex score little study 
 phrase ' return dominant , ' | | great picture lightning flash . bold 
 count laboriously | enormous majority audience 
 trace note bar space . | country — Promenades — 
 time keen , | able gather assimilate small 
 hope , intelligent listener . 1 mightily home| music hear . . 
 Beethoven Symphonies , think ! beauty come home , 
 hard catch Wagner | roll far head ; hear 
 Leit - motif . rebel | symphony year , year — 
 sheer inaccessibility great mass music | hear time week grasp 
 master . year steady consistent | meaning . , instance , amateur 
 concert - , know reach furth |ha special study hope Liszt 
 mere fringe subject . , a| Pianoforte concerto hearing , 
 moment , change . hope hear second time 

 strong desire defend , acclaim , ) | think — experience — 
 glorify piano - player . know case | find great performance 
 . candid friend | arid desert , travel , catch 
 listen , blankly refuse ' | thing beauty 
 music machinery . ' thing of| revelation power sweep 
 beauty , tell , machinery — | grasp . theme stand , remain 
 aeroplane , silken fabric , chronometers.| , end message come 
 music ? grant ' attach- | vague , confused , obscure . surely great 
 ment ' machine , pianoforte aj art - work tell ? 
 machine , highly complex machine , create } surely worth , possible , 
 note strike ? step far | approach near , wring , clear coherent 
 strike ? set thief catch thief , | tale , wealth beauty , let speak lucid 
 set machine play ? ! intimate voice 

 contend result mechanical soulless , ! ' interpretation piano - player 
 music . let mé admit } faulty . contend 

 superiority nimble human finger , guide | compare grand complex 
 fine human intelligence , form contri-| voice orchestra . 
 vance . let admit music , | score , dwell time , 
 music , player render | force render secret . , 
 care hear . remain | look deep heart , 
 . great pianist , let | greet concert - room friend 
 point , play hand , player |a tale tell . believe , orchestra w ill discourse 
 . great pianist furthermore | sublimely . soon indebted 
 dere : pianist . want ! player countless golden hour . 
 dress - clothe sit draught when- 
 listen music . want remain home 
 leisure . finally nd Oraan Music 
 great fact répertoire . unplayable | Church § 
 exception , music reach . _ | — 
 , mi ‘ plore s rg joy . 2 
 arvel « - g. : — - 
 come examine world } _ article oe ot ae bg ory 
 music | ‘ emote , lock } Ze 7zmes Apml 15 , reaa 
 et gee sar sting , penile floes scans ein nal 4 ] organ répertoire appeal 
 generality mankind . assiduous | organ rey ire appeal . 
 pursuit . 1 attend literally | writer deserve thank w holly 
 concert . arm with|admirable manner present 
 miniature score , richly annotate red | subject , organist proud 
 ink . relentlessly track great | instrument receive . eo ee 
 orchestral work , till hear nearly | point , gradua 
 . extent | attainment permanent form organ 
 conquest ? familiar | possibility composition — ' 
 Beethoven Symphonies . | generally musician large . influence 

 310 

 ' CHORAL SYMPHONY ' CAMBRIDGE 

 Dr. Mann symphony concert , whic ! 
 place Guildhall , Cambridge , March 16 , 
 programme Beethoven Choral Symphony , 
 exponent ( Jueen Hall Orchestra choir 
 200 voice , direction Sir Henry Wood . 
 merit performance rouse audience point 
 enthusiasm rarely feel , rarely express , jn 
 Cambridge . soloist Finale Miss Esta d ’ Argo , 
 Miss Alice Lakin , Mr. Joseph Cheetham Mr. Thorpe 
 Bates 

 view performance ( serial ) lecture 
 work day previously Archwological 
 Museum , Dr. E. W. Naylor . description , 
 base independent research , 
 interest , abundant proof lecturer 
 close attachment Symphony , industry , 
 analysis movement closely work , 
 attractive view offer psychology , 
 musical content , Finale 

 leave - concert conduct 
 Dr. Richter quality british 
 Musicians ’ Pension Fund Concert Queen Hall 
 March 30 , farewell concert 
 conductor April 10 , hall . case 
 instrumental body London Symphony 
 Orchestra , preserve close connection 
 Dr. Richter formation . programme 
 concert consist ' fly Dutchman ' 
 Overture ; Elgar ' Enigma ' Variations ; Brahms second 
 Symphony ; Miss Muriel Foster ( Mrs. Goetz ) 
 vocalist , Mozart ' non pit di fiori ' ' La clemenza di 
 Tito , " ' Angel farewell ' ' dream 
 Gerontius . ' programme second consist 
 ' Meistersinger ' Overture , Bach ' Brandenburg ' Concerto 

 G , Brahms Variations theme Haydn , Elgar 
 ' Cockaigne ' Overture , Beethoven seventh Symphony . 
 need hardly performance 
 magnificent . remain regretfully add 
 audience inadequate justice occasion 

 morning April 10 , rehearsal enliven 
 interesting ceremony London 
 Symphony Orchestra pay tribute regard 
 Dr. Richter . Mr. E. F. James , behalf 
 Orchestra , present massive silver love - cup 
 scription : ' present Dr. Hans Richter 
 member London Symphony Orchestra , April 10 , 
 iqii , remembrance artistic association . ' 
 illuminated address present 

 Bach * St. Matthew ’ Passion adopt asa concert- 
 work province , London 
 familiar performance work condition 

 orchestral class Guildhall School Music 
 concert direction Mr. Landon Ronald 
 March 31 , soon benefit teaching 
 Beethoven eighth Symphony Weber ' Oberon ' . 
 ture play admirable precision , unity Spirit , 
 soloist Miss Audrey Richardson ( violinist ) 
 Mr. William Cooper ( vocalist 

 performance form church service . 

 NEW SYMPHONY ORCHESTRA 

 Mr. Landon Ronald conductor stamp 
 personality undertake . interpretation 
 work Beethoven include programme 
 New Symphony Orchestra concert 
 March 29 exception . reading 
 tally preconceived idea , strong 
 claim admiration . work choose ' Egmont ' 
 ' Leonore ' . 3 overture , seventh Symphony , 
 perform nervous energy spirit , | 
 precision rhythmic life . Misses 

 rT ' | 
 sing Purcell * Ode 
 Miss 

 ( ueen Hall 

 Miss Gwynne Kimpton - devise series ' Concerts 
 | young people ' come end March 31 . Mis 
 | Gilford speak form , programme include 
 movement Beethoven Symphony , Beethoven 
 c minor pianoforte concerto , Mr. Herbert Fryer : 
 | soloist , Wagner ' Siegfried Idyll . ' report 
 | previous concert wrongly place heading 
 | * amateur orchestra 

 AMATEUR ORCHESTRAS 

 MUSICAL TIMES.—May f , rgrt . 323 

 interpret Beethoven Sonata , Op . 111 , César Franck 
 ' Prélude , Chorale et Fugue , ' hall March 31 , 
 high promise . Mr. Thomas Fielden , young 
 janist promise attainment , recital 
 Bechstein Hall April 5 , introduce , begin- 
 ning programme , rhapsody Dohnanyi , 
 Op . 11 . play Debussy , Chopin , Moaart , 
 finish Beethoven ' Appassionata Sonata . ' 
 reading spirited , expressive interesting 
 Madame Rose Koenig Wagner transcription 
 recital speciality , Leighton 
 House April 6 

 recital Bechstein Hall Mr. Frederic 
 Lamond ( March 25 ) , Madame Frickenhaus ( March 29 ) , 
 Miss Ella Spravka ( March 30 ) , M. Moiseiwitsch ( April 8) ; 
 Zolian Hall Miss Ellinor Lloyd ( March 27 ) , M. 
 Marcian Thalberg ( March 28 April 4 ) , Mr. Leonard 
 Borwick , pre - eminent english pianist 
 ( March 29 April 5 ) , Miss Kathleen Chabot ( March 30 ) , 
 Mr. Wesley Weyran ( March 31 ) , M. Alfred Cortot 
 ( March 31 ) ; Queen Hall , M. Pachmann ( April 8 

 Bach ' Chaconne ' play feminine 
 expression precision mastery , Herr 
 Bronislaw Hubermann Queen Hall , March 28 

 Beethoven sextet e flat string horn , 
 Op . 818 , revive Miss Helen Sealy Bechstein Hall 
 March 28 . horn player Mr. A. Borsdorf 
 Mr. T. R. Busby 

 Mr. Fritz Hirt , swiss violinist , recital 
 Bechstein Hall April 3 . performance Bach 
 minor Sonata sufficient stamp artist 
 high rank 

 Brahms Sonata e flat , Op . 120 , clarinet 
 pianoforte , work combination , 
 play Mr. Charles Draper Steinway Hall April 8 . 
 Walenn Quartet concert Zolian Hall 
 April 10 , play Mr. Frank Bridge Phantasy Quartets 
 Dvorak ( Op . 106 ) Dittersdorf ( e flat 

 ' Concerto ' pianoforte , violin string quartet 
 play AZolian Hall March 28 , M. Alfred Cortot 
 M. Jacques Thibaud soloist . work 
 great interest , apart unusual combination , 
 repay frequent performance . artist mention 
 hear Violin pianoforte sonata 
 Fauré Franck . second concert April 8 , 
 Schubert Duo , Op . 162 , Schumann Sonata , Op . 105 , 
 Beethoven Sonata , Op . 30 , . 2 , programme , 
 carry admirably 

 symphonic tone - picture Mr. Morton Stephenson 
 base creation , overture , ' hill , ' 
 Mr. Percy Bowie , produce orchestral concert 
 Royal Academy Music , Queen Hall , 
 April 7 

 March 28 , Bath Choral Orchestral Society 
 excellent performance ' dream Gerontius , ' 
 Assembly Rooms . performance regard 
 vith special gratification choir , rehearse 
 work carefully ; seventy - number , 
 Mr. H. T. Sims ( conductor ) , Bristol precede 
 week hear Bristol Choral Society interpretation . 
 soloist Miss Palgrave - Turner , Mr. Alfred Heather , 
 Mr. David Hughes , equal demand 
 principal vocalist c ym poser . 
 BELFAST 

 detachment Hallé Orchestra , Dr. Richter 
 conductor , visit Belfast March 17 ( auspex 
 Mr. H. B. Phillips ) , expect , attract 
 large audience . programme admirable , 
 Beethoven Symphony . 7 kernel . clever 
 young violinist , Anton Maskoff . far , play 
 Saint - Saens Concerto . 3 manner disarm 
 criticism 

 concert Philharmonic season , March 24 , 
 occupy entirely Berlioz ' faust , ' soloist 
 Miss Mabel Manson Messrs. James Hay Robert 
 Burnett . performance creditable . 
 consider difficulty work , choir 
 orchestra 

 save concert large Lecture Theatre Midland 
 g 

 Institute April 1 . excellent body singer , 
 fine male - voice choir hear Birmingham , 
 conduct Mr. G. H. Woodhall . selection 
 comprise Elgar ' feast , watch , ' Schumann * 
 night march , ' Beethoven ' creation hymn , ' Elgar ' ’ 
 oh ! bea wild wind ' ' reveille , ' Bantock ' 
 lost leader , ' Coleridge - Taylor , ' o mariner , sun- 
 light , ' Grieg * Landerkennung . ' violin solo 
 Mr. Arthur Hytch , accompanist Mr. 
 Clarence Raybould 

 Gounod sacred trilogy ' redemption ' 
 choose Midland Musical Society annual Good 
 Friday evening concert Town Hall Mr. 
 > ' Cotton conductorship . production 
 Birmingham Triennial Festival 1882 , favourite 
 oratorio exception figure principal 
 attraction Good Friday . work 
 familiar choir orchestra need 
 offer fresh comment . solo assign 
 Madame Laura Taylor , Miss Olive Pank , Miss Elsie Palmer , 
 Mr. Ernest Ludlow , Mr. Ernest Quinton , Mr. Henry 
 Bannister . Mr. C. W. Perkins organist . Messrs. 
 Dale & , enter arena local 
 impresarii , secure service M. Vladimir de Pachmann 
 pianoforte recital Town Hall March 27 , prior 
 extended tour United States 

 Birmingham Festival Choral Society choose 
 close concert present series Bach b minor Mass , 
 Town Hall , April 6 , conductorship 
 Dr. Sinclair , compliment magni- 
 ficent performance . certainly history 
 local premier choral Society , great 
 accomplish , recall delight magni- 
 ficent rendering Beethoven Mass D. choir 
 completely carry honour evening , 
 touching exposition ' Crucifixus ' 
 spirited animated performance ' Sanctus , ' 
 stand marvellous achievement 
 singer . solo entrust Madame Emily 
 Squire , Miss Phyllis Lett , Mr. Alfred Heather 
 Mr. Robert Radford , singing 
 marked impression . Mr. C. W. Perkins 
 valuable help organ 

 BOURNEMOUTH 

 performance new composition some- 
 prevalent late Winter Gardens concert . 
 programme - sixth Classical Concert 
 current series , April 3 , head Coronation 
 March Mr. Mauritz Speelman ; music tuneful 
 direct , great originality . Mr. Dan Godfrey 
 energy employ authoritative work 
 Beethoven C minorSymphony , Stanford * ' Irish rhapsody ' 
 . 1 , Sinigaglia overture ' Le Baruffe Chiozzotte . " 
 concert conclude effective performance Messrs 

 King - Hall , Robinson , Alberts , Zeelander portion 

 member choir Parish ( St. Andrew ) Church , 
 conduct Mr. B. Crocker assist small band . 
 * Saviour man ' Embankment Road 
 United Methodist Free Church April 9 , Mr. W. H. 
 Martin conduct Mr. C. Jane lead small band . 
 simultaneously , impressive rendering Mutley 
 Wesleyan Church ' crucifixion , ' Mr. J. W. 
 Wibberley conductor , Mr. David Parkes organist , 
 Messrs. Foster Sydney Smith soloist . 
 TORQUAY 

 fifteenth concert Haydn String Quartet 
 March 23 occasion highly enjoyable interpre- 
 tation Mozart work D minor , Grieg g minor 
 ( Op . 27 ) . Mr. J. P. Curran vocalist . Musical 
 Association thirty - seventh concert April 6 , 
 Mr. J. H. Webb conduct . choir excellent 
 account Mendelssohn * Walpurgis 
 night , ' refinement tone phrasing sing 
 madrigal . orchestra play overture ' Leonora ' 
 . 3 ( Beethoven ) ' Jubel ' ( Weber ) . combined 
 force , choral orchestral , number 130 

 EDINBURGH 

 April 3 , Miss Nora Thomson String Quartet ( include 
 Miss Madeleine Moore , Mons . Grisard , Mr. Clive twelve- 
 tree ) recital Aberdeen Hall . programme 
 consist Tchaikovsky Op . 11 ( Herr A. Gebler ) 
 Mozart Clarinet quintet . Miss Madeleine Macken sing 
 3rahms song viola obbligato ( Mons . Grisard 

 April 5 , Dublin Orchestral Society Shake- 
 speare concert Gaiety Theatre . programme 
 consist Schumann ' Julius Cesar ' Overture ; Liszt 
 symphonic - poem ' Hamlet ' ; Beethoven ' Coriolanus ' Over- 
 ture ; Berlioz ' love scene ' ' Romeo Juliet ' ; 
 Dr. Esposito ' Othello ' Overture ; Mendelssohn 
 * Midsummer night Dream ' incidental music . Liszt , 
 Berlioz , Schumann piece time 
 , interesting item ' Othello ' 
 Overture conductor Society . powerful 
 dramatic musical impression play , 
 beautifuly score orchestra 

 April 6 , Philharmonic Society performance 
 ' * Messiah . ' small efficient band play | 
 accompaniment , lack organ instru- | 
 ment fill figure - bass feel . 
 soloist Madame Borel , Miss Eileen Stephens , Mr. Albert | 
 Maltby ( Chester ) , Mr. Percy Whitehead . | 
 especially acquit distinction . | 
 Mr. Charles Marchant conduct , ' God save 
 King , ' band choir sing con- | 
 ductor entry Excellency Lord } 
 Lieutenant Ireland , conductor appear 

 health useful art 
 intend , time good health . 
 year live year happiness , 
 thank undisturbed sympathy enjoy . 
 request ? support successor 
 sympathy undisturbedly enjoy 
 year , continue patronise excellent 
 Hallé Orchestra . happy remembrance 
 — hope entirely . intention 
 come , long , year conduct 
 Pension Fund Concert , * * auf wiedersehen . " ' 
 season end witness Dr. Richter 

 Berlin . Halié Pension Fund concert play , 
 Dr. Richter , César Franck Symphonic Variations , 
 Liszt grisly ' Tétentanz . ' ethereally pzanzssimo 
 tone hear Free Trade Hall Petri 
 secure variation , play consummate 
 mastery insight reveal work fresh 
 light . conclude Brodsky Quartet concert , Mr. Petri 
 join Dr. Brodsky Bach F minor Sonata . 
 player unusually fine interpreter Bach , approach 
 opposite point view , Petri — like revere | 
 exemplar , Busoni — fix design work line | 
 perfect beauty detail , whilst Brodsky colour 
 great emotional intensity warmth . — probably recital 
 stand future year illuminative 
 past season Ancoats , Petri 
 set Busoni asa composer , use , con- 
 siderable extent , man work , viz . , Bach , Beethoven , 
 Brahms , Liszt , Meyerbeer , . Ancoats 
 recital folk keen , hall 
 inconveniently situate . player audience alike dispense 
 * evening dress ° formality , sort 
 atmosphere big competitive musical festival , 
 democracy close grip high 
 good music . Mr. Petri playfully allude 
 ' slum - pianist , " year come memory | 
 probably dwell aspect work | 
 great pride 

 interesting recent chamber concert 
 Musical Society , Mr. Julius Harrison 
 come , help College Music 
 opportunity gauge ability D minor Quartet | 
 ( stillin MS . , product year ) , prelude 
 double fugue pianoforte , sonnet violon- 
 cello , song choral work female voice . 
 string writing uncommonly lay 
 instrument , Violoncello sonnet rare intimate 
 beauty 

 amateur orchestral society Bolton , Blackbur , 
 Oldham , Rochdale , Withington ( Manchester ) , con- 
 clude strenuous year work , case useful 
 encouraging result attain . annual 
 operatic production student Mr. Albert Cross 
 Manchester School Music , ' Esmeralda ' choose , 
 performer scarcely find interest 
 ' La Bohéme ' month ago ; case ' Esmeralda ' 
 little appeal present - day music - lover , 
 performer audience 

 Lady Hallée death link Manchester 
 musical past . Dr. Richter lead 
 platform play , orchestra rise 
 foot spontaneously . appearance 
 December 9 , 1909 , play Mendelssohn 
 Concerto ( , Manchester mind , associate 
 Sarasate , Beethoven Joachim , 
 Tchaikovsky Bach Brodsky ) vitality 
 style , rhythmical grace , certain classic purity 
 conception memory performance 

 sharply 

 Suite , ' evidently greatly enjoy . Mr. J. A. Rodgers 
 conduct , Mr. J. W. Phillips organist , soloist 

 Miss Maude Phillips , Miss Agnes Griffith , Mr. John 
 Booth , Mr. Wilfrid Douthitt . 
 | interesting orchestral concert 
 | April . Mr. Frederick Dawson , - know pianist , 
 | direct Amateur Instrumental Society Beethoven 
 |Symphony . 2 , Liszt ' Les préludes , ' Wagner 
 | * Huldigungsmarsch , ' work . tuition 
 | Society advance technique ensemble . 
 | Bach ' Brandenburg ' concerto . 5 admirably 
 | play . soloist Miss Alice Walker ( pianoforte ) , 
 | Mr. A. B. Cawood ( violin ) , Mr. G. A. Brooke ( flute ) . 
 Sheffield Philharmonic Orchestra body amateur 
 | section . senior branch concert 
 |the Albert Hall April 7 , introduce Sheffield 
 | Godard ' Gothique ' symphony . work , 
 | play , uninteresting , hardly worthy revival . 
 | member play prelude ' Tristan , ' Elgar 
 | ' South ' overture , work . follow 
 | evening junior section - prepare concert . 
 keen young musician play brightly Haydn 
 ' Farewell ' Symphony German ' Welsh ' rhapsody , 
 work somewhat power . 
 Mr. J. H. Parkes direct concert 

 Heeley Wesley Choral Society evidence 
 revival enthusiasm merit performance Bridge 
 * Flag England , ' Mr. M. J. Shipman ; 

 TOWNS 

 Middlesbrough , April 5 , interesting revival 
 place Bach ' St. John ' Passion , 
 hear Yorkshire — save necessarily incomplete church 
 performance — year . Middlesbrough 
 Musical Union , Society , Mr. Kilburn 
 direction , distinguish artistic enter- 
 prise , responsible production , worthy 
 work . Mr. Frank Mullings declaim narrator 
 recitative unfailing taste , Mr. Herbert Parker sing 
 reverent expression Saviour word , Miss 
 Gladys Honey , Miss Edith Clegg , Mr. Herbert Brown 
 sing rest difficult ungrateful solo music 
 artistically . March 28 , Dr. Richter 
 appearance Yorkshire Huddersfield Subscription 
 Concert , conduct Hallé Orchestra 
 Tchaikovsky ' Pathetic Symphony ' familiar 
 overture , Mr. Johan Rasch soloist Max 
 Bruch G minor Violin concerto . York Symphony 
 Orchestra , March 20 , concert distin- 
 guishe appearance Miss Fanny Davies , 
 play Mendelssohn G minor Concerto charming 
 sympathy breadth style . Mr. T. Tertius Noble 
 conduct . April 4 , York Musical Society , 
 Mr. Noble , Dvordk ' Stabat Mater , ' 
 noteworthy feature generally adequate performance 
 fact sing english version , Mr. 
 Crowe adaptation text employ . prin- 
 cipal Miss Bywater , Miss Coppin , Mr. Heather , 
 Mr. Campbell McInnes . Mr. Noble ' solemn Prelude ' 
 ' unfinished ' Symphony include 
 programme . work day , 
 Sterndale Bennett ' woman Samaria , ' 
 Hull Vocal Society , Dr. G. H. Smith , 
 Miss Gladys Honey , Miss Mandeville , Messrs. 
 Brearley Herbert Brown soloist . 
 choose special service Ripon Cathedral April 12 , 
 , Mr. Moody direction , receive 
 sympathetic interpretation . Keighley Musical Union , 
 Mr. R. Moore , , March 21 , Beethoven 
 * Mount Olives ' Mendelssohn ' hymn praise , ' 
 Miss Newport , Miss Day , Mr. Brearley , Mr. W. 
 Moore principal . March 24 , Harrogate Choral 
 Society , Mr. C. L. Naylor , Mr. Hubert Bath 
 * Wedding Shon Maclean , ' evening 
 popular cantata choose performance Ilkley 
 Vocal Society , Elgar ' King Olaf , ' Mr. 
 Akeroyd conductorship . occasion Madame 
 Poole , Mr. Thorogood , Mr. O’Connor prin- 
 cipal : Ilkley , solo Miss Rich , 
 Mr. Mullings , Mr. Hoyle 

 Country Colonial Hews . 
 BRIEFLY SUMMARIZED 

 t , beseech thee ( cx 
 ‘ lujah unto God Almighty Son 
 xcellent . thy Na 

 Beethoven 3d . 
 Handel 
 Ivor Atkins 

 ume , o Lord 

 Giazounow.—Prclude fugue d minor ( op . 62 ) 
 Braums.—Fugue variation theme Handel ( op . 24 

 BeETHOVEN.—Sonatas , op . 2 , 26 , 31 , 57 , 78 , 109 ( complete 
 sonata ( edit Agnes Zimmermann 

 variation ( op . 35 

