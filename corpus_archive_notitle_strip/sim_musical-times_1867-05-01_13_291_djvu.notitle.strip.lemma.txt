Pianoforte Solo , 
 Miss ZIMMERMANN . 
 part - song , . " o hush thee my babie " A. 8 . Sullivan . 
 part - song , « Fairy Song " A. Zimmermann . 
 recit . and air , ' " * deep and deep still " ' Handel . 
 . Mr. Stus REEVEs . 
 Anthem ( eight part ) , ' judge I , o God " Mendelssohn . 
 part ii 

 Motett ( double choir ) , " I wrestle and pray " e J. 8 . Bach 
 song , ° ° Adelaida ° Beethoven . 
 Mr. Srus Reeves 

 part - song ° * awake , awake " Henry Leslie . 
 Scena , e " casta diva " - Bellini . 
 Madame ViILpa 

 my heart be indite .. . 
 let thy hand be strengthen 
 the way of ZION do mourn 

 ALEXANDER 'S FEAST eve sas oo 2 
 ACIS and GALATEA ‘ a oo | 
 ODE on ST . CECILIA ’s DAY ace oe FE 
 the last three , bind in one vol . , whole scarlet cloth , 6s . 6d . 
 L'ALLEGRO , IL ales sieeamaaai ED IL MODE- 
 RATO 3 
 MENDELSSOHN . 
 psalm , complete in one volume 
 or singly , thus : — 
 AS the HART PANTS ( 42nd Psalm ) .. ooo J 
 COME , let we sing ( 95th psalm ) 1 
 when ISR : KE l out of EGYPT . come 
 ( 114th psalm ) . eight voice . 1 
 NOT UNTO US ( lléth psalm ) ae ove 8 
 ST . PAUL 3 ; ns ie aus aa S 
 HYMN of PRAISE 2 
 the hymn of praise and * as the Hart Pants , " bind in one 
 volume , whole scarlet cloth , 5 . ' 
 MOZART , HAYDN . and BEETHOVEN . 
 the Three favorite Masses , with the latin word , and an english 
 adaptation by R. G. Loraryg , Esq 

 ANNO AQNoH 

 o 

 MOZART 's TWELFTH MASS 20 3 6 
 HAYDN ’s THIRD , or imperial mass zo 78 - 3 
 BEETHOVEN 's MASS in c ... oe 2 0 3 6 § 
 the above Three Masses . one volume , whole scarlet cloth , 7 . t 
 MOZART ’s GRAND REQUIEM MASS ws ge 
 MOZART 's FIRST MASS ove we ( 2 om 
 HAYDN ’ 's FIRST MASS « - 2 o 3 6 i 
 to these Masses be add Mr. E. Hoses ’ Critical Essays , 
 extract from the Musieal Times . ' 
 MOZART 's LITANIA DI VENERABILE t 
 ALTARIS ( in e flat ) ove as 2 6 3 0 
 MOZART 'S LITANIA DI VENERABILE 
 SACRAMENTUM ( in b flat ) oss aw a6 3 0 
 CHERUBINI . 
 REQUIEM MASS ( in be saline with Latin and 
 english word eo aa 20 3 6 
 SPOHR . 
 CALVARY ... ae see 2 6 40 
 the FALL of BABYLON . 3 © g 0 4 
 LAST JUDGMENT ... 2 0 4 0 
 the above with the original word , by Professor TAYLor . 
 the CHRISTIAN ' ’s PRAYER os 8 © 3 0 
 GOD , thou art great ... 10 
 Cc . M. VON WEBER . 
 MASS ( in G ) , with latin and english word .. 1 0 2 6 
 MASS in e flat ditto a ma = 30 
 BEETHOVEN . | 
 ENGEDI ; or , David in the w aaa ‘ ent of 
 Olives ) re pe ue 2 6 30 yf 
 RO MBERG . x 
 the LAY of the BELL .... « a * 6 30 § 
 ROSSINI . | 
 STABAT MATER , with the Latin word , and an t 
 english adaptation by W. Batu ° 6 3 0 

 specimen page and list of the work in Novello ’ $ Octave Series may be 

 Matpstone.—A public performance be give on 
 Thursday , March 28th , in the Wesleyan School - room , by the 
 Maidstone Choral Society . the programme consist of portion 
 of Handel 's Messiah , aud Mozart 's 7wel / ih Mass. the solo be 
 sustain by lady and gentleman ( amateur ) connect with the 
 Society . Mr. George Tolhurst conduct 

 MontTreAL , CaNADA.—The fifth of a series of 
 Grand Instrumental and Vocal Concerts be give at Nordheimer ’s 
 Hall on the 25th March . the programme be carefully select . 
 the chief feature of the performance be the Andante and Finale 
 from Beethoven 's First Symphony ; a violin solo , * * Souvenir de 
 Bellini , " with orchestral accompaniment , ( Artot ) , well play by 

 Capt . Stephens , Rifle Brigade ; and the Andante and Finale from 
 Mendelssohn 's Pianoforte Concerto in G minor . the latter ( perform 
 by Mr. Benson ) be perhaps the well execute piece of the concert . 
 the second part of the performance consist of sacred music , 
 embrace selection from the Mass in G , by Weber ; alto solo , 
 ' but the Lord be mindfal , " from St. Paul , * Inflammatus " ( Stabat 
 Mater ) , and * Hallelujah Chorus " ( Mount of Olives ) , the whole be 
 render in a very creditable manner . Mr. Torrington be the 
 conductor 

 the Opera Porus , Handel , and Allegretto ( Seventh Symphony 

 Beethoven . 
 OOPER , GEORGE . — Organ arrangement . 
 no . 20 . price 2s . contain Chorus , ' ' he rebuke the Red 
 Sea " ( Israel in Egypt ) , Handel ; Chorus , * tu es sacerdos " ( Dixit 
 Dominus ) , A. Romberg ; and Andante ( Quintett Op . 18 ) , Mendel- 
 ssohn 

 MART , HENRY.—Original composition for the 

 Organ . no . 3 contain Grand Solemn March . 2s . 6d 

 TEPHENS , CHARLES FE . fantasia for the Or- 
 \ gan , on jRalph Courteville ’s Chorale , St. James 's ; dedicate 
 to Edward J. Hopkins . 2s , 
 \ ESTBROOK , W. J. — voluntary for the 
 j Organ . no.16 . price 1s . 6d . Contains:—Introduction and 
 fague from the Eleventh Trio , Dr. Boyce ; chorus , * by slow 
 degree the wrath of God " ( Belshazzur ) , Handel ; chorus and Recit . , 
 Bia Mater " ( Stabat Mater ) , G. Rossini . 
 — — no . 17 . price 1 . 6d . Contains:—March , W. J. Westbrook ; 
 Adagio , 8 . Sechter ; Andante ( Sonata Op . 14 ) , Beethoven ; fugue , 
 T , Adams 

 IELD , HAMILTON . — " : my sword ! my sword 

 16 air . Spohr 

 17 slow movement from Sonata , 
 Op . 2 . ( Pedal Obb . ) Beethoven 

 1 Andante from a Quartett 

 88 Adagio Movement . Rinck 49 Allegro Movement . Rinck 
 89 Aria from P.F. Sonata . Mozart | 50 slow — ement 

 40 Arietta . Haydn J. W. C. 0 . Sauerbrey 
 | 41 Slow movement . Beethoven 51 Slow movement . W. Volckmar 

 42 Slow movement . Beethoven 52 Slow movement . Haydn 
 43 fugue . J. £ . Ebderlin 53 Ave Verum . Mozart 
 | 44 Agnus Dei from the 4th mass | 54 full Voluntary . ( Pedal Obb , ) 
 | ( Pedal Obb . ) Mozart A. Hesse 
 content of second hanes in six bcok , 
 book vii . oe Boox X. ye 
 | 75 oluntary . Anton Andre 
 ad a — 2 m op . 9 , | 76 Dona nobis pacem . Mozart 
 f ve Marig ubini 77 prelude . C. G. Hépner 
 56 Ave Maria . Cherubint | rai Attn Dusk [ Mozart 

 is " s. Robinsor 
 4 pc ttocsmnge 2 , no . 6 . | 79 Andante from Sonata , op . 69 , 
 59 Hallelujah , amen , from Judas ) 80 rg but the Lord be mind . 
 Maccabeus [ dussex| ul . " Mendelssohn 
 60 Adagio , L'Invocation Sonata.| 81 Chorale " sleeper , awake 

 Boox xi 

 84 Aria , La Serenade . Beethoven 
 | 85 Andante , from Sonata . Mozart 
 | 86 full voluntary . a Hesse 

 7 Andante , from first Concerto 

 131 Andante , Sonata , op . 10 

 101 Aria , Sonata , op.30 . Beethoven be a Bodenschat 
 | 102 Pastoral Air . Geminiani 197 Aria op . 107 . Hummel 
 | 103 Andante , Septuor , op . 74 128 Aria , Methfessel 

 ot Aria , Batam = 7 " |129 Aria . Haydn 

 Boox xiv 

 107 Aria . A. Hesse 
 | 108 Aria , from op . 18 . Beethoven 
 0 1 Steibe 
 ( 1 slow Mare Stbat | 04 Avia Quartet , op . 7 . Ha 
 | 111 " fix in his everlasting | 333 a oe wee 
 seat . " Handel | 
 | 312 solemn March . Dr. Boyce 
 113 impromptu , op . 142 . ( Pedal 
 Obb . ) FF . Schubert 
 _ Boox xviii . 
 Boox XY . 89 Aria , Cantabile . 2eethoven 
 \ 140 Adagio from the Lobgesang . 
 414 Aria , Quartett , op.77 . Hayda 

 Men 
 | 115 " thou shalt bring they in . " | 141 Aria . Kalkbrenner 
 | Israel in E gypt .. Handel 

 certo . ( Pedal Obb ) 
 Morar 

 117 Larghetto , symphony in D. | 143 Andante . Haydn 
 | Beethoven | 144 prelude . J. E. Eberlin 
 | 118 prelude . A. Hesse 145 full Voluntary . A. Hesse 
 | 119 Adagio movement . Rinck 146 Ave Maria . Arcadelt 
 120 Aria Varied . ( Pedal Obb . ) | 147 Chorale . MM . G. Fischer 
 A. Hesse | 148 Hallelujah , Saul . ( Pedal 
 | 121 soft Voluntary . A. Hesse Obb 

 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDON 

