psalm life . new work , compose expressly 
 Festival , Mr. Davip Jenxins , Mus . Bac . conduct 
 composer 

 principal work Mendelssohn " St. Paul , " 
 Verdi " Requiem , " Berlioz ' ' Faust , " " Beethoven Ninth Symphony , 
 Mozart Symphony G minor , ' Messiah 

 Principa Artists : Madame Albani , Miss Ella Russell , Miss Clara 
 Butt , Miss Florence Oliver , Mr. Ben Davies , Mr. Whitney Mockridge , 
 Mr. Watkin Mills , Mr. Plunket Greene , Mr. Douglas Powell , 
 Mr , Ffrangcon - Davies . leader Orchestra , Mr. Alfred Burnett 

 direct , march with|man hour like ? , 
 regular step regiment parade . that|him time , ought know , 
 easy comfortable process . | protean , aspect dispose 
 rest , strive man hand,|to thing puzzle 
 observe f ’ / ’ , regard , |their lack necessity excess 
 , vague conceptions| gratuitous daring . mood 
 exist , generally follow letter | achievement , man equip 
 composition reasonable exactness . |minute knowledge work hand , 
 method favour music| hasstudie composer lively perceptive . 
 play — music , rule , broad | ness , bound sober judgment 
 obvious significance , clear andjand spirit reverence ; 
 simple construction guilty } reason base , hisown 
 mean thing appear | fancy , text ; 
 . ' reading , ' present | acquaint mean 
 sense word , primitive conductor | work end seek , know inti- 
 dream . text , with|mately strength weakness 
 composer indication rendering ; | instrument orchestra , entirely 
 travel , attempt fill/ observation , experience , 
 , record ? conductor finally , master domain , rule 
 question ; idea simply absolutely , force , cheerfully 
 occur . correct truly acknowledge right aristo . man , 
 primitive instinct love bring / height depth , breadth length 
 melody , hear , haply | special knowledge skill , 
 follow , understand . , | completeness power , entirely product 
 Wagner point , naiveté his|of late time , circumstance 
 method lead successful result , | . ask chef 

 Habeneck ( " medium 
 abstract esthetical inspiration " 
 ' ' devoid genius ’' ) , rehearse Beethoven 
 Ninth Symphony till man orchestra 
 know feel melody come 
 instrument , obtain performance beauty 
 Wagner attempt describe . 
 old conductor , tradition 
 time - apparatus , pass away . serve 
 day generation like early edition 
 ' ' Encyclopedia Brittanica " ' ( 
 disappear ) room new man 
 interpreter new measure 

 reference complicated 
 structure modern music , 

 Sunday ( 2nd ult . ) , day 
 Lower Rhenish Festival , ask 
 ' * Humperdinck arrive ? " , how- 
 , ; , far , 

 intimate term composer , 
 enquiry . , , 
 dark . suggest 
 Dr. Wette , husband Humperdinck 
 talented sister , authoress libretto 
 ' Hansel und Gretel , ' 
 glean information . Dr. Wette second 
 daughter receive , inform Hum- 
 perdinck wife arrive , 
 ask . somewhat bold 
 break family gathering , 
 meet cordial reception , 
 ask sit join 
 family coffee - party ; Frau Wette , 
 surround child . | Humpet- 
 dinck remember having 
 ; , fact , meet Beethoven 
 Festival , Bonn , previous year , 
 minute . fortunate 

 rom os Bw oo oe 

 effect . success opera England 
 appear afford composer great satis- 
 faction . meet approval 
 premature report appear 
 paper concern music 
 furnish ' ' Schneewittchen . " " answer 
 question finish Sym- 
 phony long time 
 portfolio , reply , moment , 
 occupy song chorus 
 " ' Die Kénigskinder , " ' play write 
 sister 

 meeting Humperdinck 
 evening Concert . cantata 
 perform brilliant success ; 
 close , , summon 
 platform , receive enthusiastic ovation . 
 composer spoil success ; 
 accept necessary evil accom- 
 panye greatness . concert 
 simple genial . 
 arrange , approach- 
 e , worthy Herr Degen , Amtsgerichtsrath 
 Bonn , come , vasty voice , 
 : " Beethoven , 
 Wiillner Prophet 

 close sentence short 
 speech M. Vincent d’Indy , french 
 composer , gathering honour Dr. 
 Wiillner Beethoven Festival Bonn 
 year . Herr Degen farewell 
 word leave 
 quit Bonn . remind 
 pleasant day past , turn 
 Humperdinck , moment life 
 composer appear danger . 
 worthy Councillor Degen falstaffian 
 proportion , overjoyed meet 
 old friend clasp tightly 
 arm ; Humperdinck , 
 man big stature , kill 
 kindness enthusiasm greeting . 
 emerge embrace unharmed , 
 able congratulate 
 success work . venture remark , 
 , fault — , 
 - brief . twinkle eye , 
 assure think fault , 
 advantage 

 morning , 
 slip appointment 
 meeting ; business necessitate , 
 find consult time table , 
 early departure Cologne 
 anticipate , composer 
 " ' Hansel und Gretel 

 444 MUSICAL TIMES.—Juty 1 , 1895 

 Mr. Sims Reeves late Mr. Weiss consider rival Beethoven , 
 colleague . like , , | start advantage Mozart teach . 
 ' easy , graceful style her|ing , Hummel certainly brilliant career , 
 brilliant singing , spite perceptible | pretty mind 
 defect english pronunciation ; she| Mr. Dannreuther : " endow 
 learn | curiously little inventive power , rarely 
 libretto , oblige withdraw . " in}warm , incapable humour 
 1849 accept engagement Royal passion , fully equip virtue 
 Italian Opera , play parisian|that acquire steady plod , 
 réles , fail , , create enthusiasm.| appear expressly cut hero 

 feel , Mdlle . Dorus — 
 Madame Dorus Gras , 

 apple , orange , bill play , ginger beer 

 Mr. Punch soon pass rhymed criticism 
 criticism prose , lay violent hand 
 Mercadante ' ' Elena Uberti . " extract 
 passage , somewhat anticipate 
 " new critic ’' 1895 : ' learn 
 work good operatic writer , ' grand 
 tragic opera ' Mercadante , Donizetti , 
 Pacini , suggest idea symphony 
 Beethoven play german flute , 
 Weber overture arrange - handed 
 accordion . " " silly extravagance , Punch 
 pretend seriousness hard fact , 
 advantage 
 criticism anticipate . Madame 
 Vestris produce , 1842 , ' ' Marriage 
 Figaro " ' Mozart leave , Mr. Punch 
 prose critic fittingly appreciative . 
 long , state , opera 
 " altogether solemnise english 
 stage " form " hash dish 
 concocter introduce ballad native 
 composer , duet Paesiello , chorus 
 Rossini , compliment 
 pay Mozart taste british 
 audience , add , , , , 
 great master piece . " supersede 
 awful mess genuine article , Madame 
 Vestris properly praise , , 
 performance critic tell truth , doubt , 
 save trouble comprehensive 
 highly professional utterance : ' 
 performer , hero , Leffler , 
 drunken gardener , Horncastle , appear 
 possess , , appreciation 
 author , enjoy , sing , 
 music . case , 
 invidious select remark . " 
 course , critic , lay 
 pen , glass 

 enthusiastic audience assemble hear 
 Persiani , meet communication 
 Wardrop , M.D. ' Madame Persiani , " 
 Wardrop , " continue suffer severely 
 effect sea sickness , accompany 

 musical progress , pupil Adam ( father 

 composer Adolphe Adam ) later 
 Beethoven friend , violinist Hullmandel . 
 introduce Francois Louis Gounod , 
 father , distinguished painter , marry Mdlle . 
 Lemachois 1806 , - 
 - seven . Gounod picture father 
 man exceptional integrity culture , 
 lacking power application . death , 1823 , 
 leave Madame Gounod son — Louis - Urbain , 
 aged , Charles , future composer 
 " Faust , " aged , mean 
 support hope 
 provide talent . actually continue 
 drawing class late husband , 
 success grow large , 
 course - commence music lesson . 
 point Gounod begin describe 
 early characteristic . fact set forth 

 substantially describe 
 felicitously Mdlle . de Bovet life composer , 
 ut acquire new interest reason delicate 
 humour literary grace impart Gounod 
 manner relate . penchant music 
 80 strong lead occasionally neglect 

 mutual friend , unfortunate conduct 
 Handel rival , Bononcini . 
 late period busybody MS . composition 
 Greene Handel , forthwith hang 
 window , remark want " air’~g 
 preposterous story , striking 
 feature Greene music overflow melo . 
 diousness . easily verify examine 
 anthem publish Novello ; , 
 example , " acquaint thyself God , " " thou , 0 
 God , art praise Zion , " ' ' thou visitest earth , " 
 easily discover Greene , 
 , write noble majestic music ; 
 anthem ' ' God hope strength , " " 
 sing thy power , " " o clap hand , " 
 ' * o thank " cite proof . 
 fashion - - year ago decry 
 Greene music , honest examination 
 print anthem convince honest 
 musician composer 
 justly proud ; hope bi- 
 centenary birth induce lover church 
 music endeavour rescue unmerited neglect 
 treasure genius bequeath 

 Mr. F. CorpEr recently conclude lecture 
 student Royal Academy Music , musical 
 composition , notice appear 
 column , contain truth scarcely 
 deeply impress young musician , 
 old * knowledge power " art 
 true music . know 
 , realise 
 successfully express , acquaint 
 work contemporary writer — mean 
 technical knowledge turn 
 satisfactory account . endeavour , , 
 dispel absurd , fear , widely hold 
 view concern ' inspiration , ' Mr. Corder 
 somewhat far . perfectly true good 
 work art produce thoroughly 
 train musician series carefully calculate 
 effort employment - know method . 
 thousand work . crowd 
 shelf publisher — remain . 
 paradoxical , thing easy 
 accomplished writer express vacuity 
 mind glowing language . composer 
 constantly prove fact . , , 
 deft workman , certain charm 
 practically , 
 Act Parliament prevent . 
 ! bear calculation 
 differ widely brain produce 
 unconscious effort . mental assimilation con- 
 sequent development , probably , 
 result working certain definite process ; 
 inasmuch ignorance , con- 
 venient result " inspiration . " 
 scarcely justifiable Beethoven 
 " inspire " ? spend year pet- 
 fecte theme . step perfection 
 probably true inspiration 
 conception subject . 
 song equal beauty form , harmony , work- 
 manship , possess ' 
 speak force live thing . 
 analyse " life . " appeal 
 reason deny existence life art . 
 difference . isthere . feed , nourish 
 , treasure , o , young composer , strive fit 
 expression , 
 , cry birth . , statement 
 melodic phrase character 

 MUSICAL TIMES.—Juty 1 , 1895 . 451 

 melody 

 know Beethoven Sketch Books , 
 mutilate form , scatter 
 world . leave lose , 
 tear satisfy longing ardent 
 worshipper master possess autograph , 
 book actually cut 
 commercial purpose . scat- 
 tere leave gather 
 doubtful . , , fresh discovery 
 happily . recently refer 
 sheet acquire British Museum ; 
 , Dr. Otto Lessmann paper , Allgemeine 
 Musik - Zeitung , 14th ult . , Dr. Wilhelm Kienzl 
 interesting account Beethoven 
 sketch discover , beginning year , 
 Herr Guido Peters . look 
 abundle old music , death relative , 
 come possession . cover read , 
 great surprise joy , word , ' ' Manuscript 
 Beethoven . " inside page undoubted 
 autograph sketch , belong year 1809 . 
 contain sketch second 
 movement E flat Pianoforte Concerto . 
 fourth page present detailed sketch 
 introduction Choral Fantasia ( Op . 80 ) ; 
 note write pencil , ink . 
 Beethoven play pianoforte work 
 produce Vienna , December 22 , 1808 , 
 introduction , print , 
 compose . Carl Czerny relate Fantasia 
 complete Jate , scarcely rehearse . 
 head sketch mention 
 stand word : " Morgens fertigen , Nachmittags 
 probiren , nachsinnen , griibeln , " , 
 doubt , actual paper Beet- 
 hoven work free " fantasia " Concert . 
 Dr. Kienzl highly interesting 
 sketch . far , sheet contain sketch 
 " Kennst du das Land , " hitherto know , 
 sketch patriotic song , " ' wenn es nur 
 , " probably sing Redouten Saal , Vienna , 
 exciting day March , 1809 . com- 
 position hitherto unknown ; Dr. Kienzl 
 fac - simile . curious note resem- 
 blance opening song 
 ae du das Land . " place 
 

 ibe 24 serre tce TE 

 Tue printer boy good business scotsman 
 office afew day ago . journal London musical 
 critic wire : * * Harold absolve oath 
 Stigand , " boy , " Harold 
 absolve bath St. Igand . " hear 
 poor critic smile 

 ANnoTHER London correspondent unhappy . 
 Lanchester Courier devote par . Nikisch 
 /ncert , begin : " work 
 Dvorak perform England musical 
 reputation Hungary establish . Herr 
 Nikisch , Buda - Pesth opera , prove 
 country capable produce great con- 
 ductor . programme Queen Hall 
 ambitious scope , comprise C minor Symphony 
 Beethoven , Overture ' Tannhauser , ' 
 ' Peer Gynt ' Suite . " bad enemy 
 acritic , boy ambitious sub - editor addict 
 air knowledge Dvordk establish 
 musical reputation Hungary ? 
 Manchester paper , 

 JoserH BENNETT 

 PHILHARMONIC SOCIETY 

 great master uniform 
 method composition . Schubert dash thought 
 seldom trouble correction . 
 Beethoven numberless sketch seriously 
 plan ascore , Mendelssohn touch - touch 
 work absolutely liking . Dr. 
 Hubert Parry resemble - 
 composer , symphony F ( . 3 ) , 
 produce Cambridge University Musical Society 
 June 12 , 1883 , twice subject revision , 
 present late form penultimate Phil- 
 harmonic Concert season , 30 . 
 movement slightly alter , entirely new 
 Finale supply . section bluff , hearty english 
 style Dr. Parry excel , worthily crown 
 work . Symphony finely interpret 
 composer direction . feature Concert 
 Beethoven Pianoforte Concerto G ( . 4 ) Dvordk 
 piquant Violin Concerto minor ( Op . 53 ) , 
 beautifully play , Mr. Leonard Borwick 
 Mr. Franz Ondricek . Mrs. Henschel 
 sing Handel air " Alessandro , " ' Lusinghe pit 
 care , " usual purity style 

 piece programme 
 Concert , 13th ult . , novelty . 
 overture entitle ' ' Melpomene , " Mr. 
 G. W. Chadwick , american musician , bear 
 Lowell 1854 . study Germany time 
 evidently know Wagner . Mr. Chadwick 
 write department art , 
 present overture earn , conscientious 
 musician , command tragic expression , 
 climax powerful . Miss Chaminade Concert 

 stiick C sharp d flat pianoforte orchestra 

 , hand , bright lively work , open 
 distinct resemblance Wagner Overture ' ' 
 Flying Dutchman , " remind 
 Liszt Grieg , Miss Chaminade original 
 certainly eclectic , write music 
 pleasant listen . play solo 
 " Concertstiick ’' delightful touch fluency . 
 ormance absolutely unsurpassable respect 
 iven Beethoven Violin Concerto , Miss Camilla 
 Landi air , ' " ' o ma lyre , " Gounod ' ' Sapho , " 
 dramatic feeling voice power ; 
 splendid performance Mozart - ' Jupiter " 
 symphony , Sir Alexander Mackenzie direction , 
 bring successful season triumphant conclusion 

 RICHTER CONCERTS 

 NIKISCH concert 

 present craze new conductor , presume , 
 responsible series Concerts inaugurate 
 Queen Hall , 15th ult . , enterprise Mr. 
 Daniel Meyer . early express final opinion 
 merit Herr Nikisch interpreter work 
 music - lover familiar , 
 reading differ numberless way 
 accustom , difference , charm ot 
 novelty notwithstanding , come invariably 
 ashock . Herr Nikisch great reputation abroad having 
 long ago matter common knowledge 
 interest thing , audience 
 naturally come prepared find quality 
 distinguish great leader man , 
 disappoint . bdton employ 
 indicate effect accent , phrasing , expression 
 beat ' time ' ordinary acceptation term , 
 way complicated highly significant 
 suggestion carry orchestra speak 
 volume . responsiveness band — , 
 way , consist chiefly english player — , 
 way , remarkable . work play 
 orchestra Beethoven C minor Symphony , 
 Wagner " * Tannhauser " Dvorak ' Carnaval " over- 
 ture , Grieg ' ' Peer Gynt ' Suite ( . 1 ) . 
 orchestral detail stand 
 instance perfect clearness , gradation 
 speed power indication mind sense 
 form strongly develop act case 

 456 

 recital 

 early Pianoforte Recitals record 
 month Madame Augarde , 
 St. James Hall , Monday , 27 . Bach " italian " 
 Concerto , Beethoven sonata ( op . ror ) , piece 
 Mendelssohn Chopin , Madame Augarde ample 
 evidence earnest musician ; sheis 
 brilliant executant present , 
 execution somewhat imperfect 

 afternoon , St. James Hall , Mr. 
 Willy Burmester Violin Recital present , 
 programme calculated astonish 
 hearer power virtuoso secure theit 
 homage gift artist . play . 
 ment Concerto minor Raff , Wieniawski 
 ingeniously write Fantasia theme Gounod 
 " Faust , ’' Saint - Saéns brilliant Rondo Capriccioso — 
 favourite Mr. Sarasate — Brahms Hun- 
 garian Dances , display executive facility time 
 simply marvellous . alive remember 
 Paganini prime , suggest 
 cloak great italian fiddler fall 
 shoulder Mr. Burmester 

 Miss Chaminade , fourth annual Concert 
 St. James Hall , 7th ult . , form programme 
 mainly entirely composition . Mrs. 
 Helen Trust , example , sing Arne song " Phyllis , " 
 arrange Miss Carmichael , Mr. Ben Davies 
 favourite air F. Clay , " sing thee song Araby . " 
 rest scheme , , consist vocal 
 instrumental piece Concert - giver , Mr. 
 Johannes Wolff Mr. Mariotti assist _ instru- 
 mentalist , Miss Esther Palliser , Miss Camilla Landi , 
 Mr. Plancon vocalist . piece 
 movement Pianoforte Trio ( . 2 ) , song 
 previously hear , new lyric , 
 " sur la Plage , " ' ' Le Noél des oiseaux , " " Viatique , 
 worthy Miss Chaminade reputation 

 Miss Pauline St. Angelo , clever young pianist , 
 second recital St. James Hall , 1th 
 ult . , impressive performance Beethovens 
 Sonata C sharp minor — commonly term ' ' Moor- 
 light " Sonata — selection Chopin , Schubert , 
 Schumann , Rubinstein , Liszt , Henselt , Moszkowski . 
 Miss St. Angelo hear pleasure 
 receive attention busy period 
 year 

 4th ult . Madame Mathis 
 Pianof ‘ rte Recital Queen ( Small ) Hall , 
 date happen coincide death Weber 

 pleasure ; Herren Birrenkoven R. Kaufmann — 
 know London audience — , 
 rate , matter voice , far successful . 
 orchestral accompaniment , , play 
 utmost refinement 

 Monday evening Concert commence 
 Bach cantata ' ' Wir danken Dir , Gott , ’' soli , chorus , 
 orchestra , beautiful soprano aria , 
 ' * Gedenk ' uns mit Deiner Liebe " sing 
 Fiaulein Johanna Nathan . performance , generally , 
 good , exception high trumpet note , 
 pleasant listen . come Mozart Symphony 
 E flat , triumph band conductor . wasa 
 beautifully pure , unaffected reading . 
 good , dangerous select movement special 
 mention ; award palm 
 Andante . Schumann " Faust " prove 
 victory choir . principal solo 
 vocalist particularly Fraulein C. Huhn . 
 conclude scene ' ' Parsifal " follow . Herr Perron 
 intelligent rendering Amfortas , 
 voice Herr Birrenkoven ( Parsifal ) . 
 sufficiently good order enavle appreciate 
 greatness music . welcome half- 
 hour pause , ' * Eroica ’' complete 
 programme . magnificent performance ! 
 new work , 
 praise conductor sing 
 column . Dr. Willner render justice 
 Symphonies Beethoven , conception 
 * * Eroica " ' C minor fine 

 day Festival 
 plenty good thing , special attraction . 
 performance Heine ' ' Pilgerfahrt nach Kevlaar ' " 
 Humperdinck . short work , write tenor 
 contralto soli , chorus , orchestra , delightfully fresh . 
 music influence Schumann 
 Wagner . instrumental writing clever 
 orchestral colouring admirable ; fluttering flag , 
 effect miraculous cure lame 
 sick folk , peaceful sleep sick boy — 
 detail poem depict realistic , 
 means extravagant , fashion . soloist Fraulein 
 Huhn Herr W. Birrenkoven . composer , 
 present , platform close 
 performance . second special attraction pianist , 
 Eugen d’Albert , play Weber ' Concertstiick " ’ 
 Liszt Concerto e flat . sympathetic touch , 
 perfect master keyboard , play feeling 
 masked intelligence . Germany justly regard 
 great live pianist . receive 
 tremendous enthusiasm ; audience try hard 
 encore , D’Albert positively decline gratify 
 wish . encore nuisance soon disappear 
 artist hold high position act similar 
 manner 

 " CHRISTUS " symphony GHENT . 
 ( SPECIAL CORRESPONDENT 

 SYMPHONIE Mystique , entitle ' ' Christus , " Mons . 
 Adolphe Samuel , eminent director Royal Con- 
 servatoire Ghent , perform Casino 
 city , Sunday afternoon , gth ult . present 
 issue find account Rubinstein " Christus . " 
 idea impersonate Saviour stage offend 
 — , , mention Mons . Samuel 
 repugnant — advantage art 
 strong direct appeal emotion 
 illustrate intensify story Christ suffering 
 tragic death certainly legitimate . subject 
 frequently treat oratorio form : Handel , Bach , 
 Graun , Beethoven , Gounod , Kiel , Liszt 
 present memory ; Mendelssohn , 
 , commence " Christus , ’' live com- 
 plete . Mons . Samuel , like Beethoven ninth 
 Symphony , summon voice aid ; 
 employ division work . 
 ' " ' Christus " ' open movement bear super- 
 scription ' ' Nazareth , " contain characteristic 
 theme clever development . annunciation form 
 subject - matter section ; second , 
 Shepherds Magi occupy attention . 
 movement attractive ; melody theme 
 charm , enhance masterly 
 orchestration , second movement , " Au désert de Juda : 
 Le mont de la tentation , " impressive . contain 
 theme special moment , refer 
 Christ ; stand symbol authority , 
 , suffering . movement , , open 
 , powerfully proclaim brass 
 instrument ; , readily understand , 
 employ effect Passion . 
 ' * authority " motive , stern , menacing character , 
 despite modification undergo 
 course work , easy recognition . 
 movement , ' ' Scénes de l’Apostolat , " open 
 lively theme descriptive common folk crowd 

 esus hear blessed word fall lip 

 ony music grammar language . 
 harmony regard student dry subject , 
 mastery intimate acquaintance 
 _ power write correctly gain . 
 usic enjoy , comprehension 

 acquire great cumbrous pain . 
 harmony probably arise accidental jangling 
 intend sing unison , 
 anart definite duration sound 
 arrive . early write music harmony 
 term accidental , cause progression 
 independent . diatonic , 
 subsequently chromatic . Monteverde term 
 founder homophonic style — i.e. , 
 merely act accompaniment melody . Abbé 
 Vogler pioneer harmonic colouring . Spohr help 
 forward frequent use diminished seventh , 
 Chopin constant change harmony , Wagner 
 masterly employment dissonance . 
 early harmonic resource use pedal note , 
 exemplify bagpipe . repetition certain 
 series note , know ground bass , 
 time favourite device , use Purcell . 
 - know lament Dido " Dido AEneas " 
 build ground bass extend bar . 
 effect harmony expression ot 
 melody , lecturer harmonise National Anthem 
 minor mode play example Grieg . con- 
 clusion , Mr. Corder express hope remark 
 listener attribute failure 
 musical study want special gift refuge ot 
 idle ; great composer succeed 
 produce masterpiece study sus- 
 taine effort — instance , case Beethoven , 
 extend long period ; attribute 
 success inspiration insult . 
 hard work capability bring ' maturity , 
 great tell . theugly 
 little orchid root , exercise patience 
 nourishment , bring forth flower wonder 
 world 

 ROYAL COLLEGE MUSIC 

 Leipzic.—Professor Reinecke , month enter 
 seventy - second year , , understand , shortly 
 retire conductorship famous Gewandhaus 
 Concerts , hold thirty - year , 
 predecessor office having Julius Rietz , 
 succeed Mendelssohn 

 Lispon.—There — cosa rarissima little 
 capital — series Concerts chamber - music 
 lately Salon San Carlos Theatre excellent 
 native artist , Senhores Rey Colacga , V. Hussla , Selfredo 
 Gazul , Cunha Silva . programme include 
 number Beethoven , Mozart , Brahms , Mendelssohn , 
 Grieg , Saint - Saéns . probability ot 
 Concerts permanently established.—Augusto 
 Machado , successful composer opera , ' ' Lauriana , " ' 
 write ' ' hymno - Marcha " forthcoming 
 Festival St. Antonio 

 Maprip.—A new zarzuela , " ' El Senor Baron , ’' word 
 Frederico Jaques , music Zabala , bring 
 recently marked success Eslava Theatre . 
 libretto musical setting characterise 
 charming 

 important office capacity Conductor Principal 
 Conservatorium Rotterdam.——The fiftieth birth- 
 day composer , Thomas Koschat , celebrate 
 month Klagenfurt , number choral 
 society , Vienna Maennergesang - Verein 
 head , assemble musical ovation intent.——The 
 director Philharmonic Society , recent 
 periodical meeting , decide celebrate , series 
 musical performance large scale , birth 
 centenary Franz Schubert , occur 1897.—the 
 late Franz von Suppé leave nearly complete score 
 operetta finish competent hand , 
 mount Director Janner open 
 performance new Carl - Theater.——Carl Mayerhofer , 
 famous basso Imperial Opera , retire 
 private life , - year ’ connection 
 establishment . splendid voice rare dramatic 
 power remember London opera - goer 

 WEIMaAR.—The announcement 
 appointment Herr Bernhard Stavenhagen Conductor 
 Court Theatre , conjunction Herr Eugen 
 d’Albert . celebrated pianist début new 
 position month ably conduct performance 
 ' * Tannhauser . ’”——-A _ musical Festival hold 
 September 29 October 1 , work Bach , 
 Beethoven , Brahms form principal feature 

 ZurRicH.—The famous old Tonhalle , familiar periodical 
 visitor town , close door , 
 solemn performance Handel ' ' Messiah . " 
 le roi est mort , vive le roi ! — new Tonhalle , erect 
 Alpine Quay , inaugurate October , 
 musical festivity extend day 

 concert early , medizval , modern music 
 respectively Queen ( Small ) Hall 
 Madame Marie Mély , Miss Adelina de Lara , Mr 

 Sydney Brooks . programme ( 30 ) date 
 range 1250 1740 , include finish 
 performance Miss de Lara , pianist , couple 
 Scarlatti Sonatas Bach Fantasia C minor . Mr , 
 Brooks play considerable skill violoncello Sonatas 
 Marcello Padre Martini . song 
 Mdlle . Mély draw store Purcell , Handel , 
 Arne , . second Concert ( 5th ult . ) 
 scheme extend 1740 1825 , Gluck , Haydn , 
 Mozart , Beethoven , Weber , Schubert repre . 
 sente . Miss Isabel Hirschfeld pianist , 
 prove thoroughly efficient . 13th ult . list 
 composer virtually bring time , 
 Miss de Lara resume place pianoforte solo 
 piece , neat rendering Study Chopin , 
 ' * Waldesrauschen " Liszt , contrast sketch 
 Paderewski Stojowski , afford utmost satis- 
 faction 

 300th anniversary death St. Philip Neri , 
 founder Oratorians , celebrate 
 Brompton Oratory 26 . programme 
 day select admirably execute 
 choir . Beethoven Mass C accom- 
 paniment band , offertory Schulthes 
 " Amavit " sing , Master Hogan 
 solo . afternoon Mozart Vesper service per- 
 form band , Master Turnbull Sinclair singe 
 solo ' ' Laudate Dominum " Magnificat . 
 Austin " ' Tantum Ergo " boy effectively 
 hear . great feature evening service 
 Te Deum compose late Mr. Thomas Wingham , 
 sing trumpet accompaniment , render- 
 ing chorus fine specimen spirited singing . 
 service Mr. Arthur Barclay Jones conduct 
 Mr. D’Evry preside organ 

 FRENCH music form material Concerts 
 Salle Erard ( 11th ult . ) Princes ’ Hall ( 12th ult . ) 
 respectively , appear MM . Alfred 
 Jules Cottin , vocalist mandolinist Paris . 
 branch exhibit considerable taste refine- 
 ment , second occasion deservedly com- 
 mend - balance rendering duet , 
 accompaniment guitar , ' ' Il est Minuit , " 
 Mario Costa ' ' Sérénade Andalouse . ’' artist 
 Parisian Concerts - Colonne , 
 M. Mariotti , violoncellist , excellent tone 
 execution advantageously manifest ' " Medita- 
 tion ' Massenet ' ' Thais . " ? Mdlle . Louise Douste de 
 Fortis solo pianist . programme comprise 
 striking specimen grace delicacy Jonciéres , 
 Augusta Holmés , Délibes , Guy d’Hardelot , Bemberg , 
 Godard , modern composer 

 MUSICAL TIMES.—Juty 1 , 1895 . 479 

 Tue South Hampstead Orchestra , ably conduct 
 Mrs. Julian Marshall , offer ambitious programme 
 tenth annual Concert , hold Hampstead Con- 
 servatoire , 29 . work undertake 
 Beethoven ' " Eroica ' " ' Symphony , Bizet 
 « LArlesienne " ' Suite , Brahms " variation theme 
 Haydn , " Dr. Hubert Parry Overture ' ' frog " 
 Aristophanes , overture ' ' Euryanthe . " 
 rendering Symphony characterise 
 exceptional intelligence conscientiousness . Mr. David 
 Bispham sing , accustomed - control energy , 
 Lysiart   scena ' ' Euryanthe ’'   Stanford 
 " Cavalier song , ' support 
 male choir 

 ar Military Concert Queen Hall , rath 
 ult . aid Royal Cambridge Asylum Soldiers ’ 
 Widows , need large attendance . 
 band Grenadiers , Coldstreams , Scots Guards , Royal 
 Engineers , Royal Artillery play , respective 
 conductor , selection embrace movement 
 symphony suite , overture marche . 
 Guards ’ band combination justice Massenet 
 " § cénes Pittoresques , " ' special success occa- 
 sion gain string band Royal Artillery 
 ( conduct Cavaliere Zavertal ) portion 
 Schubert ' ' unfinished " ? symphony majestic 
 " memoriam " overture Sullivan — superb 
 performance 

 Queen Hall , roth ult . , Miss Carlotta Elliot 
 delightful Concert , aid Miss Marie 
 Wurm Herr von Dulong . Miss Elliot sing 
 perfect taste appropriateness style selection 
 vocal piece range Scarlatti Blumenthal , 
 include example art Schubert , Schumann , 
 Brahms , Massenet , Beethoven , . Miss Wurm , 
 play accompaniment vocal piece , 
 contribute solo , figure scheme 
 composer charming duet soprano 
 tenor , repeat . 
 previously hear public . Herr von Dulong solo 
 - _ Mozart " Un aura amorosa " Grieg ' m 

 ne 

 connection Croydon Conservatoire Music 
 Violin Recital Small Public Hall , 
 Croydon , evening 4th ult . , Mr. Frederik 
 Frederiksen , professor violin Conservatoire . 
 programme include sonata D minor ( Gade ) , 
 Mr. Frederiksen join Miss Grace Henshaw ; 
 movement Mendelssohn Violin Concerto , 
 introduction Rondo ( Saint - Saéns ) , Elegie 
 Rondo , Op . 48 ( Sauret ) . Miss Waite contribute song 
 Chaminade Sullivan charming compo- 
 sition Mr. Harvey Lohr , accompany 
 composer 

 Mr. Frank Howerave , Princes ’ Hall , 28 , 
 far manifest ability pianist . successfully 
 join Mendelssohn quartet b minor ( op . 3 ) , 
 associate Mr. Louis H. Hillier ( violin ) 
 meritorious performance Beethoven sonata G 
 ( Op . 30 , . 3 ) . Mr. Howgrave solo essay include 
 Paderewski nocturne b flat Caprice g , 
 play correct feeling . Miss Martyn - Hart 
 deservedly applaud rendering Godard " angel 
 guard thee , " valuable service Madame 
 Elise Inverni Mr. Sydney Brooks ( violoncello ) . 
 aoe Henry R. A. Ropinson Concert Black- 
 ag Rink Concert Hall , 29 . artist 

 iss Margaret Hoare , Miss Helena Dalton , Meister 

 Miss BeatricE HALLET interesting historical 
 lecture - recital Somerville Club , 4th ult . , 
 illustrate development musical form Suite 
 Partita Sonata modern programme 
 music . example choose Suite , . 5 , G major 
 ( Bach ) , Sonata , Eis ( Haydn ) , Sonata Pathétique ( Beet- 
 hoven ) , Kinderscenen ( Schumann ) , Hungarian Dance 
 Volkslied ( Brahms ) , small piece Chopin , 
 Schubert , Mendelssohn . admirably 
 play Miss Hallet 

 MasTER REDGRAVE Cripps Pianoforte Recital 
 Steinway Hall , 11th ult . young gentleman , 
 pupil Mr. Carnell , indication high 
 endowment . programme include selection 
 Suite Bach , Beethoven sonata D minor ( Op . 31 ) , 
 selection Mendelssohn , group piece 
 Chopin . Master Cripps technical 
 acquirement far advance year , undeniable 
 sign intelligence feeling . Mr. Arthur Oswald 
 assist 

 MDLLE . DE Lipo Concert , Steinway Hall , 
 13th ult . singing Goring Thomas " ' Le Baiser , " 
 scena " est doux , " ' ' Hérodiade , " number 
 song duet , effective , 
 evidently suffer hoarseness . Madame de Swiat- 
 lovsky , Mr. Hirwen Jones , Mr. Herbert Thorndike 
 sing , violin pianoforte solo play 
 Messrs. Johannes Wolff Wilhelm Ganz 

 W. MacponaLp SMITH 

 " CON SORDINO " BEETHOVEN . 
 editor " ' MUSICAL TIMES , " 
 Str,—I read interesting article fac 

 compare quotation print copy fail » 
 manner bar Trigg 
 Funeral March differ regard employman 
 pedal generally print . Senza sordiy 
 mean , course , ' damper " — i.c . , lou 
 pedal — con sordino , reverse . printe 
 edition . 
 error article ? — faithfully , 
 FRED . G. Suny , 
 [ accept correspondent statement 
 regard meaning senza sordino con sordin , 
 marked difference anto . 
 graph edition circulation , include 
 critical publish Messrs. Breitkopf Harte , 
 Beethoven write senza con " sordini , " Mr. Shinn 
 right contention . article ney 
 month shall discuss length special meaning 
 word sordino use Beethoven 
 Sonatas ; error editor eye 
 learn doctor fall distinguish 
 word sordino sordini . term 
 merely singular plural form word , 
 appear think.—ep . , M.T 

 canticle church . 
 EDITOR ' MUSICAL TIMES 

 agree " Enquirer " 

 simile Beethoven sonata flat ( Op . 26 ) , 

 great pity lose effective setting Cantate 

 content 

 BEETHOVEN . — March ; MENDELSSOHN.—War March 

 Egmont . " | MENDELSSOHN.—Funeral March 
 BEETHOVEN.—Funeral March . | ( op . 103 ) . 
 Cuopin.—Funeral March ( op . 35 ) . | MEYERBEER.—March " Le 
 Hanpet . — dead March | Prophéte 

 Samson . " | Mozart . — March ' " TIdo- 
 HanDEL . — dead March meneo 

