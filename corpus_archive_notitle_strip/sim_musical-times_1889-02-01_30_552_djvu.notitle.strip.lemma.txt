C RYSTAL PALACE.—OTTO HEGNER will play 

 J Concerto for Pianoforte and Orchestra , no . 1 , in C ( Beethoven 

 and various favourite Solos Mage his répertoire , at the SATURDAY 
 CONCERT , FEBRUARY vocalist : Miss Emily Spada . Con- 
 ductor : Mr. ’ August Sean Numbered Seats , § s. ; serial ticket 

 for Concerts and Oratorios , address , Smethwick , Birmingham . 
 8o , Addison Street , Nottingham . reference kindly permi tte to A. J. C : ildicott , Esq . , Atherstone 
 ms . " 4 7 . rreont . P ce , G 
 MADAME CLARA WEST ( soprano ) , cin or ig 
 MISS LOTTIE WEST ( contralto ) , MR . C. E ML YN JONE S ( tenor ) _ 

 Beethoven Villa , King Edward Road , Hackney . ( winner of Tenor Solo , National Eisteddfod of Wales , 1888 

 nae as : 3 Saree : for Oratorios , Ballad Concerts , & c. , address , 10 , Prince ’s Square , 
 MASTER R. TRELEAVEN HOAR ( Solo soprano ) . Kennington , London , S.F. 
 lor Oratorios , Concerts , Banquets , & c. term and vacant date on grand performance of " ExtjaAn . ’' " — we have no hesitation in 
 application to Mr. Charles Radburn , Elmers End , Beckenham , S.E. | say we be delight with the rendering of the air ' if with all 
 _ codldtrt : : your heart , " by Mr. C. Emlyn Jones . his rich tenor tone be 
 MISS BERTHA BALL , R.A.M. , Medalist ( contralto ) . | excellently suited to the air , and his earnestness to the subject . " — South 

 a writer in the Atlantic Monthly refer those who be 
 | curious about the influence of Wagner ’s music on the voice 
 jto Heinrich Vogl , ' ' who have the advantage of a perfect 
 | method , add to the gift of an organ exceptionally strong , 
 | yet the tired sound do not leave his voice for week ( afzer 
 |a course of the ' Nibelungen ' ) , and there be no doubt that 
 | his power will fail prematurely in consequence of the tre- 
 | mendous strain so frequently apply . " " . we may add that 
 | Mr. E. Lloyd , warn by experience , refuse any more to 
 | sing the Sword Song of Siegfried 

 here be another addition to the universal apocrypha : 
 isatlantic paper declare that an english gentleman , 
 bear the very english name of Kunwald , " be the fortu- 
 |nate , possessor of the violin on which Beethoven ofte : 
 play . " ' for the removal of any doubt , we be tell : " I 
 bear on its back a roughly cut initial b , apparently exe- 
 cut with a chisel . ' this would be convincing be it 
 not a fact that the famous Bill Stumps have many relative 
 about 

 t 

 GENERAL Harrison , President - elect of the United 
 States , be credit with ability to recognise a tune some- 
 time , although he have ' ' a bad ear for music . " " he hear 
 a march during the civil war and identify it year after in 
 an opera - house . it be the Soldiers ’ March and Chorus in 
 * Faust . " " . the General be rather inclined to boast of his 
 perceptiveness . 
 | we be tell on the high authority that an ingredient 
 in ' pure religion and undefiled ' be the charity which 
 minister comfort to ' the fatherless and widow in their 
 affliction . " for the noble of reason , therefore , let our 
 reader assist Mrs. Desmond Ryan and her orphan boy . 
 | subscription may be send to Mr. W. Wiener , 21 , Sunder- 
 jland Terrace , Westbourne Square 

 _ — - 
 | ' the Philharmonic Society ’s opening Concert , on March 
 |14 , will have a good programme , include Beethoven 's 
 | Fourth Symphony , Sterndale Bennett 's " Parisina " ' ? Over- 
 ture , Grieg ’s Suite ( Op . 46 ) and Pianoforte Concerto in 
 A minor . by way of something to clear the hall , the 
 director have choose Saint - Saéns ’s ' * Danse Macabre ' — 
 a very effectual piece for some of we 

 _ 

 in the present instance it show th : 
 cultivate taste give 312 vote to the |to never ha 

 opp wtunitie of 
 -Tannhauser * overture and only 225 to Beethoven 's such be fame 

 Pastoral * * Symphony 

 86 the MUSICAL TIMES.—Fesruary 1 , 1889 

 a part as Mephistopheles , but he may claim the credit due |and Mr. Willy Hess , the leader of Sir Charles Hallé ’s 
 to conscientious effort . the chorus be so beautifully | orchestra , make a very favourable impression in Spohr 's 
 render that the loss of some of the more delicate orches- | work , his playing be noteworthy for good intonation 
 tral effect be less felt than it otherwise would have be . | and grace of style . 
 for the first time the work be give without any encore } at the follow Concert the audience be much large 
 and , as a consequence , a large proportion of the audience | than usual , and a glance at the programme reveal the 
 retain their seat to the end . |cause . there be no doubt whatever that a large propor- 
 : | tion of the english public be attract by choral ! than by 
 oon ~ i spp tabeiahy te lorchestral music , and for the first time Mr. Henschel 
 NOVELLO ’S ORATORIO CONCERTS . bring forward a choral piece — namely , Mendelssohn 's 
 the first performance of ' Elijah " ? at these Concerts , on | " hear my prayer , " Mrs. Henschel be announce to 
 the 23rd ult . , be of course an event of some interest , and this take the soprano solo . she sing it with singular purity ot 
 be increase by the accidental circumstance that Mendels- | style , and with perfect expressiveness , untainted by affecta- 
 sohn ’s most popular work have not be hear previously in | tn . phe chorus be supply by Mr. McNaught ’s Bow 
 London this season . it be scarcely too much to say that , |@Nd Bromley Institute Choir , and right well do the 
 on the whole , no fine rendering have ever be hear in | # mateur from the far east acquit themselves . it be to be 
 St. James 's Hall . the hypercritical might have discover | hope that on some future occasion they will appear again 
 a flaw or two occasionally , but they be as spot | in St. James 's Hall to take part in some work of great 
 in the sun . of Mr. Henschel ’s interpretation of the | ! Mportance . animate , perhaps , by the encouraging 
 principal réle it be not necessary to say very much , jattendance , the orchestra ’ play remarkably well 
 as it be become nearly as familiar as that of Mr. | during the evening , very few flaw be noticeable 
 Santley . comparison between the two would be bad | ! " the rendering ot Mozart 's * Jupiter ' Symphony , 
 than useless ; an artist have aright to form his own judgment | Schumann 's ' Genoveva " Overture , and a selection 
 as to the way in which a part of such importance should be | from the third act of * Die Meistersinger . a some- 
 render , and to act upon it so long as he do not mis- | what puzzling item be an enbract from Weber 's 
 represent the composer’sintentions . Mr. Henschel impart unfinished CODMESODERA . Phe three Pintos . Benedict in 
 quite asardonic spirit into his taunt of the Baal worshipper , | his life of Weber make no mention of this piece , but he 
 and he sing with the utmost expression the air in the | vas thoroughly familiar with the work so far as it go , 
 second part , " it be enough " and " for the mountain and avow that he could have complete it from memory 
 shall depart . ' Madame Nordica be hear to great immediately after the composer 's death . the evtr'acte be 
 advantage in the principal soprano music . she use iM two section , a short slow introduction in D , very 
 her beautiful voice with much tact and _ skill , and |elicately score , lead into a bright movement in EF 
 wisely avoid the air and grace of the operatic | flat , rather suggestive of a polacca . it be not fully write 
 stage . of Madame Patey and Mr. Lloyd it only need be | 0ut and score by Weber , but the idea be his . it would 
 say that they be , as usual , matchless in their respective | have be an advantage to the hearer to have state on 
 duty . among the subordinate vocalist , first mention be |the programme the amount of work towards complete 
 due to Miss Lizzie Neal , who display a very fine con- | the score which be do by Herr Mahler at the request oi 
 tralto voice and a forcible style in the air * * woe unto they . " | the descendant of the composer . 
 she have something to learn in the art of manage the en eeaee 
 voice , but as she be still a student in the Royal Academy of | , OE Ne ieee SP , 5 ee eee 
 music this be merely a question of time . the efficient | MONDAY and SATURDAY popular concert 
 service render by Miss Emily Squire . Mr. Maldwyn ) oy the 7th ult . these performance be resume in a 
 Humphreys , Mr. Lucas Williams , and Mr. Hughes | quiet and unostentatious manner . there be only two 
 should not pass without acknowledgment . it would be | concerted work in the programme — namely , Beethoven ’ . 
 impossible to overpraise the effort of the choir . these | quartet in E flat ( Op . 74 ) , sometimes know as the * Har : 
 enthusiastic amateur , who be equal to master the | Quartet , " and Rubinstein ’s sonata in D , for pianoforte an ‘ ! 
 most difficult work in three or four week , seem | violoncello ( Op . 18 ) . the latter be one of the most pleasing 
 determined to show that they do not despise such | example of the russian composer ’s chamber music , thank 
 familiar strain as those of * * Elijah . " hey sing through- | as much to its symmetrical form as to the beauty and 
 out the performance with an amount of vigour that be freshness of its theme . Mdlle . Janotha be the pianist 
 simply astonishing , and , to name but one example , the | of the evening , and the audience be not satisfy with he : 
 rendering of * * thank be to God * have surely never be | rendering of Chopin ’s Barcarolle in F sharp ( Op . 60)—that 
 surpass . Dr. A. C. Mackenzie must have be proud of | js to say , they ask for more . Mdlle . Janotha comply by 
 his force ; they could not have render he well service . | give the same composer 's Berceuse in d flat with charm 
 e delicacy . Madame Néruda play two of her favourite 
 : I seatee pay eae ss solo by Spohr and Leclair , and Mr. Santley be hear at 
 LSNDON SIMEON Y SOMES TS . his good s two'of Brahms ’s Lieder and Gounod ’s * le 
 in the present issue of the MusicaL Times it be only | Nom de Marie . " 
 possible to notice two of these performance — namely , those | on the follow Saturday a highly attractive programme 
 which take place on the 15th and 22nd ult . the pro- | be provide , and despite the inclement weather there be 
 gramme on both occasion be admirably select and |a very large attendance . it would be superfluous to say 
 arrange , though the novelty introduce be not in any | anything concern such masterpiece as Mozart ’s quintet 
 sense important . at the first of the two Concerts an Over- | inG minor and Beethoven 's * * Kreutzer " Sonata , both ot which 
 ture by Tschaikowsky , entitle ' 1812 , be bring forward . | be splendidly render , the latter by Madame Néruda 
 since the introduction of his fine and original Pianoforte | and Sir Charles Hallé . the pianist must be thank for 
 concerto in b flat minor at the Crystal Palace nearly a | revive Schubert 's so - call Fantasia Sonata in G ( Op . 75 ) 
 dozen year ago , the work of the russian composer , hear | after seven year of neglect . as the term Fantasia be prove 
 from time to time , have prove disappointing . it be to be | to be without authority , it should now be remove from a 
 hope that the new symphony in E minor , which he | work which be as clear in outline and as correct as to form 
 be to conduct at the Philharmonic Concerts next } as any Sonata ever pen . of the manifold beauty of 
 season , will make amend , for ' 1812 ' be a sorry piece | the work there be no occasion to speak to those who know 
 of programme music . Tschaikowsky employ an old|it , and those who do not should make its acquaintance 
 russian hymn - tune , at least two national air , and aj without delay . Mrs. Henschel be hear in Purcell ’s now 
 fragment of the Marseillaise . these theme be work up | familiar ' * Nvmphs and Shepherds " and two french song 
 into anoisy and rhapsodical piece , the exact import of which | by Mr. Goring Thomas and M. Massenet respectively . 
 can not be determine , as the programme do not vouchsafe | the former of these , ' * Midi au village , " be a very charming 
 any explanation . the rest of the programme be far more | little effusion . 
 acceptable . it include Mendelssohn 's Overture ' Fingal ’s | there be only a small audience at the Concert of the 14th : 
 Cave , " Beethoven 's Symphony in D ( no . 2 ) , Wagner 's | ult . , and the cause must be attribute to the unimportance 
 ' * Siegfried Idyll , " and Spohr ’s Violin Concerto in D minor | of the pianoforte solo , as the rest of the programme be 
 ( no . g ) . the playing of the orchestra be very creditable , | attractive . but many attend these Concerts for educational 

 XUM 

 87 

 purpose , and they be not likely to be content with | 
 Chopin ’s Impromptu in F sharp ( Op . 36 ) , excellently as it | 
 be interpret by Madame Haas . indeed , an encore be 
 so warmly insist upon that it have to be grant 
 at last . the concerted work be Beethoven 's 
 Quartet in F ( Op . 59 , no . 1 ) and Brahms ’s Pianoforte 
 Quartet in A ( Op . 26 ) , by far the fine of his Pianoforte 
 Quartets . Miss Florence Hoskins , the vocalist of the 
 evening , make a fairly favourable impression ; but as she be 
 still a student at the Royal College of Music it would be 
 unfair to subject she to severe criticism 

 as a matter of course there be a great crowd on the 
 rgth ult . , for Beethoven ’s Septet be in the programme . 
 whether the public will ever tire of this favourable example 
 of the Bonn composer ’s early manner can not be say , but 
 there be no symptom as yet of any such change of feeling . 
 the performance be of noteworthy excellence , as may be 
 suppose with such artist as Madame Néruda and Messrs. 
 Hollander , Lazarus , Paersch , Wotton , Reynolds , and Piatti . 
 another popular work , give by desire , be Haydn 's 
 Quartet in C ( Op . 76 , no . 3 ) , contain the variation on 
 " God preserve the emperor . ' " " Madame Haas give a 
 charmingly finished and expressive rendering of Beethoven 's 
 Sonata in A flat ( Op . rro ) , and Mr. Santley render 
 Schubert ’s ' " ' Erl - King " ' and two other equally familiar 
 song in his good manner 

 the same executant , allow for the addition of Mr. 
 L. Ries and the replacement of Mr. Hollander by Herr 
 Straus , give a magnificent performance of Schubert 's 
 Ottet on the follow Monday . hitherto it have be the 
 custom to divide this work , allow an interval after the 
 third of the six movement ; but on the present occasion 
 it be play without break , and , moreover , be place at 
 the end of the programme . this may have seem 
 rash , but it be justify by the result , for very 
 few person stir before the end . a less satisfactory piece 
 be Liszt ’s transcription of Bach ’s Organ Prelude and 
 Fugue in a minor , which for some inexplicable reason 
 Madame Haas select in preference to more legitimate 
 pianoforte music . Mendelssohn ’s sonata in b flat , for 
 pianoforte and violoncello ( Op . 45 ) , complete the instru- 
 mental programme , and there be only one song , Miss 
 Maude White 's ' * come to I in my dream , " which be 
 fuirly well render by Miss Helen d’Alton 

 se 

 MR . DE MANBY SERGISON ’s WINTER concert . 
 Mr. W. pe Mansy Serartson , the Organist of St. 
 | Peter 's , Eaton Square , give the first of a series of ten winter 
 | Concerts , at 62 , Warwick Square , on the 17th ult . , a select 
 | audience be present . the programme of these Concerts , 
 | which , by the way , commence at the unusual hour of half- 
 | past four in the afternoon , consist exclusively of chamber 
 | music , with vocal item intersperse . that urder notice 
 |comprise Beethoven ’s Quartet in F ( Op . 18 , no . 1 ) and 
 | Haydn 's Quartet in D ( op . 12 , no . 63 ) , play by the 
 ‘ talented Hann fam'ly — viz . , Messrs. Lewis Hann ( first 

 88 the MUSICAL TIMES.—FEeEprvary 1 , 1889 

 perform several solo on the pianoforte 

 an Allegro from Beethoven 's 
 which be give with great spirit and 

 the MUSICAL TIMES.—Fepruary 1 , 1889 

 the Concert announce by Mrs. Hutchinson and Messrs. 
 Plunkett Greene and Drummond Hamilton attract a 
 large and fashionable assembly to the Antient Concert 
 Rooms , on the 17th ult . in addition to the above - mention 
 vocalist , Mrs. Scott - Ffennell , Signor Papini ( violinist ) , and 
 Mr. W. H. Collisson , Mus . B. ( pianist ) , contribute to the 
 success of a very enjoyable performance of well select 
 music . the singing by Mrs. Hutchinson of Gounod ’s * Ave 
 | Maria , ' by Mr. Plunkett Greene of * * Nazareth , " ' by Mrs. 
 Scott - Ffennell of * * La Cieca ’' ( Gounod ) , and by Mr. Hamil- 
 ton of Grieg ’s * * Ragnhild ' ? be much admire . some 
 quartet be also charmingly give by this talented party , 
 and Signor Papini and Mr. Collisson each contribute a solo 
 on his instrument . the pianoforte accompaniment be 
 play by the last - name gentleman 

 the Royal Dublin Society ’s Chamber Music Recitals 
 be resume after the Christmas holiday , on Monday , the 
 14th ult . at this and at the follow recital ( 21st ult . ) 
 the programme consist of Beethoven ’s Quartet in G 
 ajor , for string ( Op . 18 , no . 2 ) , admirably play by 
 or Papini , Mr. Newman , Herr Lauer , and Mr. 
 dersdorff ; Brahms ’s Sonata in A major , for pianoforte 
 and violin ( Op . 100 ) , by Messrs. Esposito and Papini ; and 
 Mendelssohn 's Trio in C minor , for pianoforte , violin , and 
 violoncello , of which last - name work the performance by 
 Messrs. Esposito , Papini , and Rudersdortt seem to lack 
 no quality of excellence in tone , technique , or expression 

 the first appearance of Madame Nordica at the Saturday 
 Popular Concerts be promise for the 26th ult . , in com- 
 pany with Madame Joyce Maas and the Dublin Quartet 
 Union , but too late for notice in the present letter 

 on New Year ’s Day the member of the Choral Union 
 give a very successful performance of * * the Messiah , " the 
 Music Hall be fill to overflowing . the principal 
 vocalist be Miss Resch Pettersen ( soprano)—her first 
 appearance here — Madame Annie Grey ( contralto ) , Mr. 
 Gledhill ( tenor ) , who , sit among the audience , be un 
 expectedly call upon to replace Mr. Newbury ; and Mr. 
 Horsfall ( bass ) . the accompaniment be play by 
 Mr. Charles Bradley ( organ ) and Mr. Scott Jupp ( piano 
 forte ) . Miss Resch Pettersen make a very favourable im- 
 pression , her rendering of * * 1 know that my Redeemer 
 liveth " be especially good ; Madame Annie Grey dis- 
 tinguishe herself notably in ' he be despise . ' the 
 chorus throughout go very steadily , and do credit to 
 Mr. Collinson , the conductor 

 the third Orchestral Concert take place on the 7th ult . , 
 the soloist be Madame Belle Cole and Mons . A. Gillet , 
 leader of the violoncellist . 
 |Grieg ’s Overture " Autumn , " Raft ’s Concerto for violon- 
 cello and orchestra , in D ( op . 193 ) , symphonic Prelude to 
 Byron ’s * Manfred , " by F. Praeger ; Beethoven ’s Sym- 
 | phony ( no . 2 ) , an Intermezzo for string ' * Loin du Bal , " 
 by A. Gillet , and Liszt 's hungarian Rhapsody ( no . 1 ) . 
 | Madame Belle Cole sing a Recitative and Aria ' * awake , 
 Saturnia , ' from Handel 's ' ' Semele , " ' and * * knowest thou 
 the land , " from Ambroise Thomas ’s * * Mignon . ’' the good 
 effort of the orchestra be undoubtedly the hungarian 
 rhapsody , whilst with regard to the soloist , Madame Belle 
 | Cole and Mons . Gillet be recall , the former choose 

 the programme consist of 

 to sing , 2 as encore , the song * well " bide a wee , " which | 
 after a superb rendering of her ' * Mignon " song be , to 
 sav the least , in somewhat questionable taste 

 on the Monday follow , the 14th ult . , we have the 
 fourth Concert of the series , with Mdile . Elvira Gambogi 
 a vocalist and Madame Helen Hopekirk as solo pianist . 
 the latter , be fresh from her renew study in Vienna 
 under Leschetitzky , be an especial attraction . her 
 rendering of Beethoven 's Concerto ( no . 5 , e flat ) certainly 
 reveal the fact that she have improve immensely , parti- 
 cularly in breadth of style and repose . her technique be 
 clear and brilliant . it be to be regret that the piano- 
 forte use on the occasion be out of tune with the 
 orchestra . this naturally mar the ensemble playing . 
 Madame Hopekirk ’s solo be Menuetto Capriccioso in E 
 ( Leschetitzky ) , * Traumerei ’’ ( Schumann ) , and * the 
 Erl - King " ( Schubert - Liszt ) . she be twice recall . the 
 vocal piece be Gounod ’s * jewel Song , " Schumann ’s 

 Lotos flower , " and G. J. Bennett 's Serenade , to all of 
 which Mdile . Gambogi do full justice . the principal 
 orchestral item be Dr. Villiers Stanford 's * Irish Sym- 
 phony ( first time in Scotland ) and Dr. A Mackenzie 's 
 ' Benedictus " ' for violin and orchestra . each be receive 
 vith great applause . Mr. Manns conduct with his usual 
 evergy and ability 

 Madame Helen Hopekirk accept an engagement 

 oifered she by the Directors of the Literary Institute , and 
 give in their hall a Pianoforte Recital later in the week , 
 the 17th ult . , in which she even more fully confirm the 
 bove express opinion . Beethoven 's ' * appassionata , " 
 three piece of Chopin , a set of Variations by Nawratil ( a 
 master with whom she have be study composition ) , a 
 Caprice of Leschetitzky , and Liszt 's twelfth hungarian 
 rhapsody form her programme 

 flat 

 5th and r2th 

 ult . , the following principal selection wv plave : — Intro- 
 duction to ' * Colomba " ™ ( Mackenzie ) , always much admired ; 
 the ' " * Voices of the Forest * ( Wagner ) , Concerto for clarinet 
 and orchestra , no . 1 ( Weber ) , Mr. J. Clinton , soloist ; the 
 no . 1 Symphony in C ( Beethoven ) , the * * Symphony 
 ( Mozart ) , Concerto for violin and o in A minor 
 ( Vieuxtemps ) , Mr. Maurice Sons ( leading violin of the 
 band ) in the solo part . the programme of the Subscrip- 
 tion Concerts on the a of the sth and 15th ult . 
 comprise chiefly Raff 's Concerto for violoncello and 
 orchestra . in D ( Op . 93 ) . Mr. E. " Gill et take th : solo part 
 with very marked approval ; Praeger ’s S c Prelude to 

 Sympho 

 Byron 's * * Manfred , ' and the no . 2 y ot Beethoven , 
 together with selection from the ' * Rosamunde ™ music of 
 Schubert , Concerto for pianoforte and orchestra ( no . 5 ) , in 
 e flat , with Madame Hopekirk at the soloinstrument ; and the 
 ' Trish Symphony of Villiers Stanford , hear here for the 
 first time and play with great ability and decide approval . 
 Mdlle . E. Cansbog ! be the vocalist at the Concerts of the 
 12th and r5th ult 

 at the Popular Concert of the 19th ult . H. MacCunn ’s 
 Ballad for orchestra * * the Dowie Dens o ° Yarrow , " be 
 present for the second time here , other princip : il item in 
 the programme be the Ove ' Leonore ( no . 2 ) , of 
 Beethoven , and Mendelssohn 's * * Italian ’' Symphony . Miss 
 Agnes Larkcom contribute some song whic favour 

 ablv receive 

 Charles Hallé be again to the fore in the production of 

 Berlioz ’s Symphonie Fantastique and Wagner 's * Ride of 
 the walkyrie . " " . these , which have often be hear in 
 the same hall , receive full justice on this occasion . the 
 work of the band also include the overture : 
 Schubert , the ' Melusina , ’' by Mendelssohn , and Beethoven 's 
 " gratulation ’' minuet , as well as two piquant little com- 
 position by Grieg . Madame Nordica be the only solo 
 vocalist , and make a brilliant success in her singing of 
 selection from Mozcart ’s * Seraglio " ' and from ' * Mignon 

 at the Concert of the 22nd inst . , the chief feature be 
 the interpretation of Brahms ’s * Gipsy Songs , " by Miss 
 jena Little , Miss Fillunger , Mr. Shakspeare , and Mr. 
 Thorndike , and the violin playing of Mr. Willy Hess 

 3irkenhead ’s Subscription Concerts be always classical , 
 enjoyable , and now — although it be not always the case 
 — be highly appreciate . the entertainment of the gth 
 ult . be quite up to the average , the artist include Sir 
 Charles Hallé and his accomplished wife . Foremost in the 
 programme , both in order and importance , be Dvorak ’ 
 Quintet ( Op . 84 ) , a work which be at the same time charm- 
 e , scholarly , and likely to become popular at Chamber 
 Concerts . of the four movement , the second , with the sig- 
 nificant music allot to the pianoforte , be certainly the most 
 impressive , be perfect in proportion , beautifully balance , 
 and work out to an effective conclusion . the Finale be 
 full of busy earnestness and brilliant scoring , rich in inven- 
 tion and detail , and serve as an admirable contrast to the 
 more delicate workmanship of the early movement . the 
 performance of this work be beyond comment in its 
 masterly efficiency and intelligent reading . another well 
 execute number be Mendelssohn ’s Quartet No . 2 in 
 e flat , and in addition to these Sir Charles and Lady 
 Hallé join in a Schubert Rondo , and separately play 
 Beethoven 's ' Moonlight ' ? Sonata and   Wieniawski ’s 
 " [ égende " in G minor respectively 

 if " * the Messiah " ' be peculiarly appropriate at Christmas 
 time , " Elijah " ? be welcome all the year round , and the 
 performance on the 16th ult . , by the Xaverian Choral 
 Society , show that the work have be well study and 
 rehearse . Mr. John Ross be an able conductor and a dis- 
 criminate musician , and it be pleasing to find that he 
 have so well inculcate in his force the feeling of earnest- 
 ness and sympathy which be essential to a true reading of 
 " Elijah . " the chorus be bright and accurate , and the 
 band efficient . the principal singer be of local reputa- 
 tion , include Madame Laura Haworth , Miss Hallwood , 
 Mr. Bradley , and Mr. Edward Grime . 
 of the réle of the Prophet , Mr. Grime have make another 
 substantial step forward in his art 

 in his assumption 

 Mackenzie ’s Oratorio " * the Roseof Sharon . ' the perform . 
 ance , on the roth ult . , be in every respect very successful , 
 and it be important that we should notice how completely 
 groundless have prove the fear which be say to deter our 
 | large Concert - giver from venture upon choral novelty , 
 the fact be that there be no place in the kingdom have the 
 resource which this city enjoy , and surround , as 
 | Manchester be , with a teem host of lover of choral 
 ; music , in which so little opportunity be afford of 
 | becoming acquaint with work thus peculiarly meet our 
 english taste . the excitement create by the announce- 
 iment of Dr. Mackenzie ’s Oratorio be prove by the fact 
 that every seat in the Free Trade Hall be occupy , and 
 by the close attention pay to a really admirable rendering 
 } of a work so original in design and execution , and there 
 |fore so little likely to be at once adequately appreciate 
 jor to produce on a first hearing its full effect . in 
 spite of a difficulty in enter into the inner meaning 
 of the libretto , and the consequent strain of mind in 
 endeavour to penetrate the allegory veil by word 
 evidently use in a mystic sense , the continuity of idea 
 pervade the whole of the music , the wealth of melody 
 enrich its fertile how , and the complete mastery evince 
 over all the mean of expression , prove the composer ’ 
 possession of power sufficient for the production of a yet 
 sreater work , when supply with a subject afford oppor- 
 tunitie for more varied treatment . Madame Nordica 
 and Mr. Watkin Mills appear hardly so thoroughly con- 
 versant with the music , or so completely in accordance with 
 its spirit , as might have be desirable ; but Mr. E. Lloyd 
 be admirable , and Miss Hope Glenn decidedly advance 
 herreputation . the choir appear to great advantage ; and , 
 at the organ , Mr. Fogg manage with good judgment to 
 give , without obtrusiveness , the requisite weight and support . 
 at Sir Charles Hallé ’s other Concerts during the month 
 | there have be capital rendering of Brahms ’s Fourth and 
 lof Mozart ’s * Jupiter ’ ? symphony , of Schumann ’s melo 
 dious * * Genoveva " Overture , and of Bizet ’s Suite * * Roma , " 
 in which the Vivace and Vivacissimo prove as ex 
 citing as before , although the more ambitious movement 
 perhaps scarcely maintain their effect . the band have 
 also be as skilfully , but less worthily , occupy with the 
 ' Héroide Elégiaque * and the * * Mazeppa " poem of Liszt . 
 | Signor Piatti give we , on the 17th ult . , his Concertino in A 
 |minor ; and Lady Hallé , on the 3rd ult . , play Beethoven 's 
 | Violin Concerto in a style to be long remember . Miss 
 Macintyre and Miss Liza Lehmann ( so opposite in tempera- 
 ; ment ) have be the vocalist , in addition to Madame 
 | Hess , a mezzo - soprano of considerable power and purity ot 
 | tone , and likely to be very useful . 
 Mr. de Jong also have discover the charm of Dr. Mac 
 kenzie ’s orchestration , andon the 19th follow his previous 
 | selection by the ' * Rhapsodie Ecossaise , ’' a work of great 
 | vitality . in play the ' Concertstiick " ' of the danish 
 | musician , Joachim Andersen , he have enlarge the repertory 
 | of our flute lover , and he be to be thank for afford we 
 a second opportunity of listen to the pianoforte playing of 
 Mr. Bartle , of Southport . as usual , Mr. de Jong ’s supply 
 of vocalist have be liberal — in quantity , at any rate 

 Sir Charles Hallé ’s afternoon Recitals at the Concert 
 Hall become increasingly attractive , and offer evident sugges- 
 tion as to the future of that institution ; and at the Town 
 Hall Mr. Pyne ’s Organ Recitals continue popular . Otto 
 | Hegner also have be here again , excite great enthusiasm . 
 | Dr. Watson deserve praise for his boldness in present 

 Philharmonic Society , and great improvement have be make in th 
 Church service in the town and neighbourhood , which be in no smgj 
 measure due to Mr. Strickland ’s successful labour . 
 ST . 
 Concert 
 jarnett 

 LEONARDS - ON - SEA.—Two concert be give at the Roy ) 
 Hall , on Friday , the rth ult . by Mr. J. Maug ’ 
 who have be recently appoint Organist of St. 
 Magdalen Church , the performer be Mons . Tivadar Naché ; 
 Miss Edith Thairlwall , and Mr. Barnett . Mons . Tivadar Naché ; 
 violin solo , which be much applaud , include Mendelssohn 's g 
 minor Concerto , several of his own composition , and the Pra 
 theme and variation , by Paganini . Miss Edith Thairlwall ’s sin 
 be tasteful and effective , and her song be well and suitably choose 
 Mr. J. Maughan Barnett display ability as a solo pianist in Schubert ’ 
 Fantasia ( Op . 15 ) , " the wanderer , " Beethoven 's * moonlight ’ 
 sonata , c sh arp minor , two compo sition of his own , and pleces } Y 
 Chopin , Jensen , Fumz agalli , and Grieg 

 Es 

 hand ) . by Th . Herbert .. AY 2,2 ne ss ne fas new sacred work 
 ASCHER , J.—Fanfare Militaire . op . 40 . for Pianoforte ( six as Aull 

 hand ) . by Th . Herbert .. be ne be ' ee ni 5 oo for the season of 
 BEETHOVEN , — overture : ' ' Egmont . " for two Pianofortes LENT and GOOD FRIDAY 

 eight hand ) , by Th . Herbert é os e se oe " ir ee " 
 BOIELDIEU.—Overture : " Calif of Bagdad . " for Pianoforte by 

 8 . Soldiers ’ chorus . 1.7.B.B. .. : Gounod 44 . Bo . Christ be rise ( e aster : anthem ) . S.A.7.B. Berlioz 4 

 g. the Kermesse ( scene from ' " F wnat a 6d . 81 . when the sun set o’er the mountain ( * ii 
 to . up , quit thy bower . s.a.t.p . 9 Brinley Richards 4d . Demonio ’’ ) we ee a. Rubinstein 34 , 
 14 . the Gipsy Chorus .. a f .. Balfe 4d . 82 . hymn of nature .. : + » Beethoven 3 , 
 21 . an old Church song . s.a.7.b. i ? ury Smart 2d . 83 . Michaelmas Day ( humorous part - song , no . 1 

 22 . Sabbath Bells . s.a.7.3 . ae ir a an . VV falter Maynard 46 , 
 23 . Serenade . SATB . os 2d . 84 . SportingNotes(Humorous part - song , No.2 ) , , 44 , 
 28 . Marchofthe Men of Harlech . S.A.T.B. 3 . Dr. Rimbault 2d . gt . the Star of Bethlehem ( Christmas Carol ) 
 29 . God save the queen . S.a.7.B. ais s 1d . T. L. Clemens 24 , 
 30 . Rule , Britannia ! s.a.t.p . .. Ss f id. 92 . busy , curious , thirsty Fly . 7t.a.7.b. ci 3d . 
 34- Market Chorus ( ' Masaniello " ) . s.a.t.p . Auber 4d . 93 . love wake and weep . a.7.B.B. FelixW. Morley 24 , 
 35 . the prayer ( ' ' Masaniello " ' ) . s.a.1.b , ' 3 i d. g4 . brother , thou art go before we Arthur Sullivan 64 , 
 51 . Charity ( La Carita ) . s.s.s ... Rossini 4d . 95 . comeaway with willing foot .. ss 64 , 
 57 . the Chough and Crow ae Sir H. R. Bishop 3d . 96 . Madrigal ( from ' * Mikado " ' ) .. " 6d , 
 58 . the " Carnovale " .. be Rossini 2d . 97 . three little maid ( ditto ) .. " 6d , 
 65 . Dame Durden . : oe sit td . 98 . climb over rocky mountain ( from 
 60 . a little Farm well till ia ' Hook 1d . ' the Pirates of Penzance " ' ) " 6d , 
 67 . there be a simple maiden . , é . ' A. Macfarre n 1d . too . hunting chorus ... oe Alfred Cellier 24 , 
 69 . once I love a maiden fair .. be 1d . ror . song from Orval ... 23 Isidore de Lara 64 , 
 71 . the oak and the ash .. oe Bs i td . 102 . Madrigal ( from " ' Ruddigore " ) Arthur Sullivan 64 

