C. B. Tirbutt , Esq . , Bromsgrove . ~ ¥ _ 

 J. Vezey , Esq . , Brandram - road , Lee . part 1 . voluntary ; subject by Heller , Mendelssohn , Schu- 
 Samuel Waldron , Esq . , St. Leonard 's , Bilston . mann , Beethoven . _ A 
 D , Wilson , Esq . , St. Marncck ’s , Kilmarnock . » 2 voluntary ; subject by Handel , Beethoven , Rossini , 
 W. J. Winbolt , Esq . , St. Paul ’s , Islington . Mozart , & c. re 

 for first and second list see Musical Times for November 0 % ofsratic melody , by ' Mocart , Donivettl , Auher 

 opening of the ROYAL WESTMINSTER 
 AQUARIUM 

 Aw Aquarium without fish can scarcely be say to offer 
 a legitimate attraction to the public , and it be not surprising , 
 therefore , that on Saturday , the 22nd ult . , when the 
 * * Royal Aquarium , Summer and Winter Garden " open 
 its door for the first time , the programme of entertainment 
 provide for the visitor should include such item as 
 * * Arrival of Lord Mayor and Lady Mayoress , " " Arrival 
 of H.R.H. the Duke of Edinburgh and Suite , " and that 
 military band should relieve the monotony of stare at 
 the gorgeously fit up royal box , which be , however , 
 place in such a position that the occupant of the reserved 
 seat in the gallery could only guess what be go on 
 during the reading of the address and the reply thereto . 
 but our business be only with the music ; and here it must 
 be say that , whatever deficiency may have be observe 
 in other department , no fault could be find with the 
 band which Mr. Arthur Sullivan have form , nor with the 
 programme exclusively select from the work of english 
 composer , which have be draw up for the opening 
 concert . Macfarren ’s ' ' Festival Overture , " to which the 
 composer have write additional part for a military band 
 expressly for the occasion , be finely play , the effect 
 indeed be so materially heighten by the extra instru- 
 ment as to cause we to hope that it may often be hear in 
 this form ; the Minuet and Trio from Sir Sterndale 
 Bennett ’s Symphony in G minor be also a feature in the 
 instrumental portion of the programme , and Sullivan ’s 
 brilliant ' ' procession March , " perform by the orchestra 
 and , military band , form a fitting finale . the vocal 
 music be well choose for a popular audience ; Madame 
 Edith Wynne give Bishop ’s ' ' bid I discourse , " and 
 Clay ’s ' ' she wander down the mountain side ; ’' Madame 
 Patey , Arne ’s ' ' gentle youth , " and Wallace ’s ' sweet 
 and low ; ' ? and Mr. Sims Reeves , Balfe ’s ' " ' when other 
 lip , " and Hatton ’s ' ' good bye , sweetheart . " to say 
 that all these piece be receive coldly would not be to 
 record the whole truth ; for at the conclusion of the instru- 
 mental composition positively not even a recognition of 
 their having be perform be hear in any part of 
 the building ; even the song be acknowledge by that 
 faint murmur of satisfaction which seem more to 
 express a sense of relief at their termination , than of plea- 
 sure at their performance ; and the singer , who have so 
 long be accustom to hear the ringing applause of the 
 audience in acknowledgment of their effort , leave the 
 orchestra as if glad to escape from the chilly atmosphere 
 with which they be surround . we do not attempt to 
 account for this , but merely state the fact ; and heartily 
 congratulate Mr. Sullivan , who have evidently his heart in 
 the cause , on the firm and energetic manner in which he 
 conduct his band under such adverse circumstance . 
 in the evening a concert be give , under the direction of 
 Mr. George Mount , when the power of the Aquarium 
 band be successfully test in a well - select programme , 
 include Mendelssohn ’s Overture to ' ' Ruy Blas " and the 
 slow movement from Beethoven ’s Symphony in C minor 

 Tue authority of the Alexandra Palace offer two prize 
 of £ 20 and £ 5 respectively , together with a certificate , 
 for the good two Orchestral Symphonies to be write 
 by british composer , the judge be Professor G. A. 
 Macfarren and Herr Joachim . the work which gain the 
 first prize be to be perform at one of the Saturday 
 concert , and the second , if of sufficient merit , will also be 
 present to the public . manuscript must be send into Mr. 
 H. Weist Hill , Alexandra Palace , on or before March 13 

 on Thursday , the 30th December , the member of the 
 Evening Service Choir of St. Paul ’s show their appre- 
 ciation of the service of Dr. Stainer ( organist of the 
 Cathedral ) by present he with a handsome bdton of 
 ivory and gold , accompany by an illuminate address on 
 vellum , congratulate he upon his recovery from the 
 serious accident which have for a short period incapacitate 
 he from the performance of his professional duty . the 
 presentation be make at a soirée give to the gentleman 
 of the choir at the Chapter House of the Cathedral , under 
 the presidency of the Very Rev. the Dean ( Dr. Church ) , 
 Canons Gregory and Lightfoot be present on the 
 occasion 

 a coop word be due to Mr. Frederick Penna for his 
 thoughtful lecture on the character of the Prophet Elijah , 
 as musically depict by Mendelssohn , which be give 
 for the second time on the 30th December , at the Beethoven 
 Rooms , Harley - street . Mr. Henry Lesingham , who de- 
 livere the lecture , be much applaud ; and Mr. F. 
 Penna ’s musical illustration be thoroughly appreciate . 
 in the double capacity of analyser and vocalist , Mr. Penna , 
 at the conclusion of the lecture , be call for and warmly 
 greet . in the concert which follow , Miss Catherine 
 Penna display a carefully cultivate soprano voice , and 
 in many of her song a dramatic power which will doubt- 
 less ripen by experience 

 an Evening Concert for a charitable purpose take place 
 at the Drill Room , Well - walk , Hampstead , on the 17th 
 ult . , when the follow vocalist give their valuable 
 assistance : — Madame Antoinette Sterling , Miss Emily 
 Spiller , Miss Gill , Madame Lia Rohan , Mr. E. Lloyd , Mr. 
 Thurley Beale , and Signor Caravoglia , all of whom be 
 highly successful , Madame Sterling receive an unanimous 
 encore for her expressive rendering of Barnby ’s song , 
 " when the tide come in . " Miss Amy Turner Burnett , 
 of the Royal Academy of Music , fully support the repu- 
 tation of the institution in which she be still a student by 
 her facile and brilliant rendering of Arthur O’Leary ’s 
 pianoforte piece , ' ' there ’ nae luck about the house , " and 
 Herr Otto Booth be much applaud in his violin solo . 
 the conductor be Mr. James Shaw , who also play a 
 solo on the pianoforte 

 Tue Arion Musical Society give its first concert at the 
 Burdett - road School , E. , onthe 30th Dec. the first part 
 of the programme consist of Haydn ’s Mass No . 1 , with 
 Cherubini ’s " ' Ave Maria " as an offertory ; the second 
 part , of Schumann ’s ' gipsy Life , " and a miscellaneous 
 selection . the solo be render by Miss Lizzie 
 Emblem , Mrs. Hickie , Miss McGaw , and Messrs. Nolan 
 and Donnelly . Mr. J. B. Hulbert conduct 

 for the Edinburgh Orchestral Festival , on Feb. rr , 12 
 and 14 , in connection with the Reid Concert , Professor 
 Oakeley have engage Mr. Hallé ’s orchestra . the work 
 to be perform are:—symphonie , Beethoven no . 7 ; 
 Gade , no . 4 ; Raff , no . 5 ; overture , ' ' Don Juan ; " 
 " Egmont ; " ' " Fire - a - bras ’' ( Schubert ) ; ' Euryanthe : " 
 " ' Hebrides ; " ' ' ' Ferdinand Cortez " ( Spontini ) ; ' " ' carnival " 
 ( Berlioz ) ; ' Handel adventure " ? ( Reinecke ) . Pianoforte 
 concerto , Beethoven , no . 1 ; Sterndale Bennett , no . — ; 
 and Grieg in a minor . also Lachner ’s Orchestral Suite , 
 no . 6 ; Brahms ’s variation on a theme of Haydn ; Rhein- 
 berger ’s Scherzo , & c. , ' ' Wallenstein ’s Camp ; " solo by 
 Mr. Hallé from the work of Bach , Schubert , Chopin , and 
 Schumann . the vocalist be Madame Antoinette Sterling 
 and Mr. E. Lloyd 

 at the recent examination hold by the Academical 
 Board of Trinity College , London , the following candidate 
 pass : — Senior Choral Fellows : F. G. Cole , St. Mary ’s , 
 Staines ; H. W. Little , New College , Oxford.—Choral 
 fellow : George F. Smith , Royal Academy of Music ; 
 Walter Stokes , Birmingham.—Choral associate : F. H. 
 Dale , Trinity College , Manchester branch ; J. Hawley , 
 Trinity College , london.—examiner , Edward Dearle , 
 Mus . D. , Cantab . , S.C.F. ; Roland Rogers , Mus . D. 
 Oxon . ; William J. Jennings , B.A. , Cantab . , S.C.F. ; and 
 J. Gordon Saunders , Mus . B. , Oxon . , S.C.F.—There be 
 ten candidate in all 

 B , D , F , AD . Dr. Stainer call this the first inversion of 
 the chord of the minor ninth on the dominant of the key 
 of C minor , with the root omit . Richter call it the 
 chord of the seventh on the seventh degree of the scale 
 of the same key . or take again the ' imperfect common 
 chord " B , D , F , in the key of C major . Dr. Stainer call 
 this the first inversion of the dominant seventh with the 
 root omit . Richter call it the ( imperfect ) common 
 chord on the seventh degree of the scale . it appear to 
 we that it would be a rash thing to say positively that one 
 of these view be right , and the other be wrong . it be 
 rather the old story of the gold and silver shield — the 
 author look at the matter from different point of view . 
 the practical question be which will be find easy for 
 teaching purpose ; and on this , never having use Dr. 
 Stainer ’s book , we be not able to offer a reliable opinion 

 our reader will pardon we for dwell in some detail 
 on these matter , because it seem to we that here be the 
 very kernel of Dr. Stainer ’s theory . perhaps there be no 
 chord which have be more a bone of contention to theo- 
 rist than the so - call ' ' add sixth . ' " " we quote Dr. 
 Stainer ’s own example — the first chord of Beethoven ’s 
 sonata in e flat , op . 31 , no . 3 

 this he call ' " ' the third inversion of the chord of the 
 eleventh of BD . " we must confess that we consider 
 Richter ’s explanation simple . he would define it as 
 ' ' the first inversion of the chord of the secondary seventh 
 on the second degree of the scale . " we be incline . to 
 think that on Dr. Stainer ’s theory some of the high 
 inversion of the chord of the eleventh and thirteenth 
 would be find decidedly difficult to analyze by average 
 pupil . on the other hand , it be only fair to say that 
 this system simplify , by do away with altogether , the 
 subject of suspension , retardation , add discord , 
 fundamental discord , and primary and secondary 
 chord . we be dispose , on the whole , to think that 
 the advantage of this system outweigh its inconvenience ; 
 but on this matter it be impossible , without practically 
 test it , to pronounce a decide opinion 

 in this example we enter the chord mark * from C 
 minor , as the ' ' second inversion of the minor ninth of G " ' 
 ( to define it on Dr. Stainer ’s system ) ; but we give it the 
 resolution , not of that chord but of the first inversion of the 
 minor ninth of BD , the dominant of ED minor , the by 
 be change to cp . we should rather define an enhar- 
 monic modulation as one in which the modulation itself be 
 actually alter by the change of notation , either write 
 down or imply . here the resolution of the bz would be 
 upwards ; it be only by mentally change it to cd that we 
 can bring it down to bp in the follow chord . we be 
 aware that the distinction we have draw here be over- 
 look , or ignore , by many theorist ; but we can not help 
 think that it exist 

 we must not dwell on the subject of ' consecutive 
 fifth " and ' ' false relation , " both of which be admirably 
 treat by Dr. Stainer ; we will only remark that many 
 reader will probably be surprise at the number of 
 consecutive fifth that the author have hunt up in the 
 standard work of such composer as Bach , Handel , 
 Haydn , Mozart , Beethoven and Mendelssohn . and this 
 lead to the observation that throughout the whole book 
 the example be most excellent , always judiciously choose 
 and to the point , and show very extensive reading , and 
 a large acquaintance with the masterpiece of various 
 school , such as might be expect from a musician of Dr. 
 Stainer ’s acquirement . occasionally , through a printer ’s 
 oversight , the * mark the special chord under notice have 
 be omit ; we point out three such , which we have 
 observe , that they may be correct in the next edition . 
 they be to be find in ex . 8 , p. 25 , ex . 32 , p. 34 , and Ex . 
 11 , p.108 . in all case , a teacher would have no difficulty 
 in supply they for his pupil , but for the sake of those 
 who be study alone , it be well that they should be 
 insert . we will also suggest to Dr. Stainer an import- 
 ant example which have perhaps escape his notice . inthe 
 treatment of the " tonic minor thirteenth " ( pp . 67 and gg ) , 
 he mention the ' " ' very rare " occurrence of the complete 
 form of this chord , but give no example of it . he will 
 find one ( in the first inversion , but in a complete shape ) , in 
 the finale of Beethoven ’s Choral Symphony , just before the 
 entry of the baritone solo ( p. 195 of Breitkopf and Hartel ’s 
 score , in their complete edition of Beethoven ) . it be this 
 chord which have be much discuss by musician , but 
 which , on Dr. Stainer ’s system , be capable of clear and 
 satisfactory explanation . it be as well also here to call 
 attention to a slip of the pen on p. 74 , in which ex . 110 be 
 quote as from Tannhduser ; it be really from Lohengrin , 
 and will be find on p. 44 of Novello ’s octavo edition 

 at the end of the book be give a series of question 
 for examination , and a set of sixty exercise . the former 
 be all that could be wish ; but we think that the latter 
 advance too rapidly in difficulty . our own experience in 
 teach harmony be that for the first stage of the pupil ’s 
 progress the exercise can hardly be too simple . the 
 mistake , to say nothing of mere awkwardness , in the first 
 attempt of beginner be such as would hardly be credit 
 by those who have not teach an elementary class . 
 matter that appear perfectly simple to the teacher be al- 
 most hopeless stumbling - block at first ; and we can not help 
 fancy that such an exercise as no . 4 , contain several 
 modulation ( simple one , it be true ) , should have be 
 precede by a large number of easy one . on this point , 
 however , we again speak with diffidence , not having use 
 the book for teaching purpose . Dr. Stainer ’s experience 
 with pupil may be more favourable than our own 

 and without necessarily pledge ourselves to acceptance 
 of all Dr. Stainer ’s view , we can at least honestly thank 
 he for an interesting and valuable addition to our theo- 
 retical work 

 the Ruins of Athens . a dramatic masque . compose 
 by Louis Van Beethoven . the english adaptation by W. 
 Bartholomew 

 by the publication of this work in the octavo form 
 there may be some hope of its manifold beauty becoming 
 extensively know . the Duet , ' ' faultless , yet hate , " 
 the March and Chorus , ' ' twine ye the garland , " and the 
 highly dramatic Chorus of Dervishes , be really the only 
 piece familiar even to the majority of amateur vocalist ; 
 but those who possess themselves of the whole of the 
 music will find many other portion equally deserving of 
 attention , amongst which we may mention the bass solo , 
 * * Deign , great Apollo , " and the open Chorus , " ' daughter 
 of high - throne Jove . " " the performance of this compo- 
 sition , in its entirety , be one of the feature of the secular 
 concert at the Worcester Festival of 1872 , the Dervish 
 Chorus elicit the usual amount of enthusiasm , and 
 be , of course , redemande . Choral Societies would do 
 well to include the work in their programme for the 
 come season , for the music be sufficiently interesting 
 without the recitation of the word , which , by the way , 
 be excellently adapt by Mr. Bartholomew . it be 
 almost needless to say that this popular edition be clearly 
 print and have be carefully revise 

 EETHOVEN ’s SONATAS . edit and finger 

 by AGNES ZIMMERMANN . Paper Covers , 5 . ; Cloth , gilt , 7 . 6d . 
 " of all the small edition yet issue we must certainly give the 
 palm to that before we , which be a very careful reprint of the full - size 
 publication edit by Miss Agnes Zimmermann , of which we speak in 
 the high term when it first appear a year ortwosince . this 
 volume be not only the well engrave , print , and bind that we have 
 see — it be far and away the most carefully edit ; so that , look where 
 we will , and with whatsoever keenness we may , it be imposible to 
 discover a mistake . the accomplished editress — one of the most 
 distinguished of the later pupil of the Royal Academy of Music — have 
 evince sound judgment in her treatment of questionable matter , and 
 have earn the gratitude of future art - student by the patient care she 
 have bestow upon the fingering of this mass of music — no light task 
 when its extent and ( in some instance ) complicated character be 
 borne in mind ; and , by her interpolate mark of phrasing , she have 
 do much towards render the least intelligible of Beethoven ’s 
 intricacy clear and appreciable . all honour , then , to Miss Zimmer- 
 mann , who have accomplish her difficult and responsible duty to 
 Beethoven ’s memory , and to the public in a manner above all praise , 
 and have present to the world one of the great product of the 
 human mind with a completeness and thoroughness worthy its 
 immortal author . "—The Queen , Oct. 15 . 
 London : Novello , Ewer and Co. , 1 , Berners - street , W 

 ARCH , by OsstanFeti . full Score , 15 . net . 
 2 violin , viola , cello , basso , 2 piccolo , 2 oboe , 2 clarinet , 
 2 bassoon , 2 horn , 3 trombone , 2 ophicleide , tympani , cymbal , 
 2 trumpet , big drum . 7o pp . 491 bar . 
 PIANOFORTE ARRANGEMENT , 38 . 6d . net . 
 London : Novello , Ewer and Co. , 1 , Berners - street ( W. ) , and 35 , 
 Poultry ( E.C. ) New York : J. L. Peters , 843 , Broadway 

 Melody , Romance , and Rondo site Renaud de Vilbac . 
 La Recréation ( duet ) 2 Renaud de Vilbac . 
 Sonatina in c ‘ 2 ne ree be Steibelt 

 Sonatina in G Beethoven . 
 two short waltz ( duet ) " Moscheles . 
 Sonatina ons we p , Clementi . 
 Sonatina in f o pl s > isle Beethoven . 
 Sonatina in G Clementi . 
 two short waltz ( duet ) , Op . 33 

 Nos . 3 and4 .. Moscheles . 
 Fleurs de Mai oe ee se ee F. Spindler . 
 Valse ( Originale ) .. oe Se Weber . 
 Canzonetta ( Rondo in G ): - Dussek . 
 * " La Matinée " ( Rondo in D ) ne 8 Dussek 

 just publish . 
 a new EDITION ( OCTAVO ) of 

 BEETHOVEN ’s 
 ruin of athen 

 the English Adaptation by W. BarTHOLOMEW . 
 paper cover , 2 . ; cloth gilt , 3 . 6d . 
 London : Novello , Ewer and Co. , 1 , Berners - street , W 

 E. ASPA . | the Passion ( DER Top Jesu ) - - - 20 
 ENDYMION - - - - ' . f o | 
 BACH . | J. O. GRIMM 

 Tue Passion ( S. MaTTHEW ) - - ar eon oe Tue SouL ’s aspiration - - - a £ 950 
 the Passion ( S. JOHN ) - - - Saeed 
 CHRISTMAS ORATORIO - - - Bw Sy | 
 Gop ’s time be the good - - - - ¥ © | HANDEL . 
 my spirit be in heaviness - - a ee 
 o light everlasting - - ' I , ee , Tue MESSIAH , edit by V. NOVELLO - 2 o 
 magnificat a - - - - - to | the MESSIAH , DITTO , POCKET EDITION - I 4 
 BIDE with US - - - « - ae ee the MESSIAH , edit by W. T. BEsT - 2 © 
 A STRONGHOLD SURE - - - " 2 50 ; « | ISRAEL in EGYPT - - - : at ee 
 I WRESTLE and PRAY - - S - 04 | IsRAEL in EGYPT , POCKET EDITION - - £ 6 
 be not afraid - - - - - = o.16 oy Jupas Maccaspz#us- ) - - - - = 256 
 | Jupas MAcCAB&US , POCKET EDITION - - I 0 
 J- BARNBY . SAMSON - - - - - . - 2:0 
 REBEKAH - - - - - - Se a SoLomon - - - - - - - 2 0 
 JEPHTHA - - - - - . - 20 
 BEETHOVEN . | Josuua : e a ' . ? * 
 Ruins oF ATHENS ) - - 212 i DEBORAH - = = : ‘ - 20 
 ENGEDI , or DavID in the witperness # 0469 SAUL - - - - - - - - 20 
 ( Mount of Olives . ) BELSHAZZAR a . = * Fs - 30 
 Mass in C ( LATIN and english bxicwsay - 2 0 THEODORA - - - - - - 2 3 © 
 Mass in D - — 2 se - - 2 0 SUSANNA - — - so tity - - = 350 
 ESTHER - - - - - - - 30 
 , SIR JULIUS BENEDICT . Tue CHanpos Te Deum - - - - I 6 
 St. PETER- - - - - + +40 DeETTINGEN TE DEuM - " Pee ee 
 UTRECHT JUBILATE - - - ie 
 J. BRAHMS . the coronation and funeral anthem 
 A Sone oF DESTINY - - " . - 10 ( CLotH ) - x e : - 5 oo 
 or , singly — 
 CHERUBINI . the king shall rejoice - - - o 8 
 ReEQuIEM Mass 1n C minor ( LATIN and ZADOCK the PRIEST - - - = @ 2 
 EnGLIsH Worps ) - - - - - 2 0 my heart be indite - - - o 8 
 let thy hand be strengthen - 0o 6 
 HENRY FARMER . the way oF ZION - - - - I 6 
 mass in B Fiat ( LATIN and ENGLISH ALEXANDER ’s feast - - - - - 20 
 Worps ) - - - - - - 20 Acis and GALATEA - - - - - I 6 
 OvE on St. CEcILIA ’s DAY - - og 
 NIELS W. GADE . L ’ ALLEGRO , IL PENSIEROSO ED IL MODERATO 3 © o 
 Tue Ert - Kine ’s daughter - - = sa6 
 SprinG ’s MESSAGE - - - . - o 8 
 HAYDN . 
 CH . GOUNOD . 
 De ProFunpDIS ( PSALM CXXx . ) - - - 4 0 the CREATION - - - - = -"2 © 
 GALLIA . = - - . = - ' 2 the CREATION , POCKET EDITION - - Io 
 Messe SOLENNELLE - - - - - £ 6 Ture season - 2 3 ‘ - - 3 0 
 Ditto , CoMMUNION SERVICE - - Ae 6 SprinG , SUMMER , AUTUMN , 
 SeconD MESSE des ORPHEONISTES - - 20 and WINTER - . : a -each I oo 
 Ditto , CoMMUNION SERVICE - - 2 0 first mass 1x b erat ( latin and english ) 2 0 
 the SEVEN Worps oF OuR SAVIOUR on THIRD Mags ( IMPERML ) DITTO - 2 0 
 the Cross ( FIL1Z JERUSALEM ) - - I O Tue Passiy ( the Hees Wornps oF our 
 DAUGHTERS of JERUSALEM ( anthem for Saviour on the ROSS ) - - - 20 
 ee ee ee ee ee te te Deum { enctisn amp Latin ) - - I 0 

 fe ae ae ey , ee ee ee ee ee , ee ee ee ae 

