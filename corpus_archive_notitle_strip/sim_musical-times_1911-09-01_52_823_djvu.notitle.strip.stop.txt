RATIS . — “ Diagrammatic : View Principal 
 J Har Rises Falls Movement Sonata 
 y Form . Explanatory Text , Musical Illustration , Refer 

 Beethoven Pianoforte Sonatas Free music students 
 A. C. , St. Cecilia House , Balfour Ri ad , Wimblex s. W 

 

 instrumental soloists M. Ysaye , play 

 Flgar concerto , Herr Moriz Rosenthal , Lady 
 Speyer . chief orchestral work Beethoven 

 vocalists Madame Agnes 
 Nicholls , Madame Lillian Blauvelt , Miss Ada Forrest , 
 Madame Kirkty Lunn , Madame Ada Crossley , Miss 
 Phyllis Lett , Miss Ellen Beck , Mr. Gervase Elwes , 
 Mr. Herbert Hegner , Mr. Joseph Reed , Mr. Thorpe 
 Bates Mr. Wilfrid Douthitt . Mr. Haydon Hare 
 chorus - master , organist conductor con- 
 concert . Sir Henry J. Wood 
 conductor - - chief . thirtieth Norfolk 
 Norwich Festival , erroneously printed 
 advertisement p. 497 August issue , 
 thirteenth 

 conclusions allowed stand , examina- 
 tion wi ill disclose _ passages 
 mezzo - legato retero , called 
 examples wi ll suffice : semi iqui avers marked 

 i. beginnin g V7vactssin ate 
 Beethoven Sonata E flat , Op . 81.A , 
 played messo-/egato ; octaves marked ‘ f / f 
 - bar Prestissimo 

 Rondo ‘ Waldstein ’ Sonata undoubted 

 examples /eggiero playin 

 pointed direction 
 ‘ non legato , occurs Adagio 
 movement Beethoven Sonata 
 C minor , Op . 111 , best interpreted , hot 
 /ortamento indication , merely cancelling 
 slurs accompany pair semiquavers 
 , tempo given ( A//egro con 
 brio ed appassionata ) switt /egato playing 

 kind physically possible . 
 conclusion mentioned s 

 ind @ @ o 

 matter indifference , ’ co iredl 
 jwith ‘ Bemerkungen zu L. v. Beethoven 
 Klaviersonaten ’ Carl Krebs 

 Sonata 

 edition Beethoven 
 Breitkopt & Hartel 
 Original Texts ) classischer Musikwerke . 
 original manuscripts edition wa 

 Beethoven dashes ( ' ) , 

 collection 

 ' ! ! 
 ee 

 violin , changed Beethoven 
 

 Coa 

 original MS . minor Quartet , | 
 mentioned , sfaccafo Porn non b indicated 
 dashes portamento ( spiccato , violin phri iseology 

 dots . pi assage letter claimed | 
 refer dots curved line,—and | 
 Beethoven supposed chosen t 

 ewan representation sharper blow 

 suppose conditions Cathedral 
 Festivals , , ideal performance 
 unaccompanied motets Bach , like ‘ Sing ye Lord vs 
 ‘ Jesu , priceless treasure ’ ; ‘ Spirit helpeth ’ ; ‘ Come , 
 Jesu , come ’ ; noble ‘ Festival Commemoration 
 Sentences ’ Brahms , Fest- und Gedenk - spriiche ( Op . 109 , 
 Nos . , 2 , 3 ) ; motets Cornelius ‘ Liebe ’ 
 cycle ( ‘ surrender soul ’ Worcester 
 1905 ) , exception , works await 
 performance Festivals - Choirs ; 
 true César Franck ‘ Psalm 150 ’ ; Max 
 Reger ‘ Psalm 100 ’ ‘ Palm Sunday morning ’ ; 
 Schubert ‘ Mass E flat , ’ Graun ‘ Der Tod Jesu 

 sixteen Festivals 1895 - 1910 Sir C. Hubert H. 
 Parry represented chorally fourteen times 
 works : ‘ Job , ’ ‘ love casteth fear , ’ 
 ‘ voices peace , ’ having 
 repeated 1907 - 1910 . past 
 years following sung : Bach B minor 
 Mass ; Berlioz ‘ Te Deum ’ ; Walford Davies ‘ Everyman ’ ; 
 César Franck ‘ Beatitudes ’ ; Verdi ‘ Stabat Mater ’ 
 ‘ Te Deum ’ ; Dvordk ‘ Te Deum ’ ; Brahms ‘ Song 
 Destiny ’ ; Mendelssohn ‘ St. Paul ’ ; Schiitz ‘ Lamentatio ’ ; 
 Leonardo Leo ‘ Dixit Dominus ’ ; Beethoven ‘ Mass C. ’ 
 Spohr ‘ Judgment ’ performances 
 years ( 1897 , 1899 , 1901 ) ; Goetz innocuous setting o 
 Psalm 137 sung 1896 1910 . 
 Havergal Brian setting ‘ Babylon wave ’ psalm 
 ( performed Musical League Liverpool , September , 
 1909 ) considered 

 Verdi ‘ Requiem ’ sung 1896 , 1900 , 1901 , 
 1907 , 1910 ( occasions Gloucester ) ; 
 minds startling feature 
 review period . Beethoven ‘ Choral ’ Symphony 
 performed 1900 , given 
 Hereford ( year Worcester scheme ) , Brewer 
 ‘ Emmaus , ’ produced 1901 Gloucester , repeated 
 1907 , Gloucester waiting hear ‘ . 9 

 Considering comparative restriction imposed 
 conductors choice music , necessity 
 performances cathedrals , 

 Gustav Merkel 

 Theme Beethoven . 
 Violin Concerto . 4 ) p 

 te 61 . 
 Arranged Organ 

 Monday , August 14 , usual Wagner night spectacle 
 presented . Sir Henry Wood selection 
 excerpts characterized , , brightness , 
 cheering influence pervaded concert . programme 
 August 15 contained * ‘ Leonora ’ overture . 3 , Elgar 
 ‘ Wand Youth ’ suite , Arensky Pianoforte concerto , 
 Op . 2 ( Mr. Edward Goll ) , Dvorak ‘ Slavische ’ Rhapsody 
 . 1 , excerpt Delibes ‘ Coppélia ’ Ballet music ; 
 complain lacked interest 

 novelty season given August 16 , 
 shape * Pavane pour une infante défunte , ’ Maurice 
 Ravel . arch - modernist France wrote work 
 singular restraint . little quaint 
 pleasing - - - way — ordinary 
 harmonies disturb conservative ear . piece 
 based afew simple charming ideas , moves 
 sedate sweetly mournful grace old - world mien 
 admirably befit subject . concert provided 
 performances Debussy ‘ Danse sacrée et danse profane , ’ 
 Mr. Alfred Kastner harpist , Strauss ‘ Don Juan , ’ 
 Tchaikovsky fifth Symphony — interesting 
 evening . quality interest maintained 
 following night , Orchestra 
 performance Svendsen picturesque legend * Zorahayda , ’ 
 
 classical programme , August 18 , Beethoven 
 Symphony occupied usual position 

 August 19 , second ‘ popular ’ 
 provided - hearing l’romenade successes 
 Scheinpflug ‘ Overture comedy Shakespeare , ’ Jan 
 Blockx ‘ Flemish dances , ’ Dr. Walford Davies 
 fine ‘ Festal overture 

 direction Mr. Julian Clifford 

 602 MUSICAL TIMES.—Sepremper 1 , rort . 
 Music Provinces . | C ARMARTHEN . — counties Carmarthen , Cardigan 
 jand Pembroke held great united Psalmody Festival th : 
 ( CORRESPONDENT . ) | Eisteddfod Pavilion Carmarthen August 16 . 
 choir 10,000 voices , conducted Mr. Harry Evang , 
 MANCHESTER DISTRICT sang , addition hymns , Handel ‘ Worthy 
 : weil aenetllinie eaialeall tue ill Ui wei tee Deal Lamb * ‘ Hallelujah ° Chorus , Mr. Emlyn Evans 
 vertises m local Press fact thet ! Eisteddai Teithiwr Blin . ” orchestra eighty assisted , 
 . s Schiller - Anstalt sale , MELBOURNE.—The University Conservatorium gave 
 : uttem pt resuscitate club , | mid - winter students ’ concert audience 
 d | ] t 1 2 der footing Altogether filled Town Hall , July 11 . excellent orchestra 
 improbable Manchester permanently deprived accompanied numerous concerted items , 
 ) sel n ! Established | fruits policy encouraging study wind 
 years , appear outlived utility a| instruments offering bursaries seen thirtee ; 
 ] pure ] nd ¥ t r osmopolitan Manchester | student players flute , oboe , clarinet , bassoon , horn 
 life need - day body | trumpetand trombone . programme included movement 
 ussociation serve medium interpret ition | Pianoforte concertos Beethoven , Rubinstein , Grieg , 
 German thoucht rt Britishers . rior coming | Tchaikovsky , Violin concertos De Beriot 
 Mid ] HH theatre midst , German | Vieuxtemps , Flute concerto Mozart , ‘ Kreutzer 
 dr ti ies 1 Schiller winter ; | Grieg Sonata pianoforte violin , solos 

 1886 ! sic t ] highest grade bee organ , viola , Xc . 
 . rd wit ts W .- _ " eg chamber NEW BRIGHTON , concert given Tower , 
 acne ; " « vel thong ? Fy é psy “ July 2 , novel experience audiences district , 
 1 101 e Visit otrauss paid Manchester , | choir orchestra 125 conducted lads 
 al quartet trio established repute enjoyed Madame de Boutllers . choir , consisting ” 
 hospitality . Mr. Car music irector , Liverpool Ladies ’ Choir Liverpool Vocal Union , 
 cont attentions Continental artists , ] heard number operatic selections , _ 
 frequently deviating | usual run chamber | « } fallelujah ’ chorus , - songs . Madame de 
 concerts , wit ! nsider justification . instance , | Boufilers sang soprano ‘ Inflammatus , 
 vas concerts termed ‘ newer 

 celebration - fifth anniversary opening 

 lling , : ; ; - 
 Ncipals f Municipal Theatre , festival performance Beethoven choir ( conductor , Herr Philipp Wolfrum ) . 
 ; Rena ‘ Fidelio given great success 

 ae JENA . 
 SCCOI e 
 5€COI EISENACH . | recently founded local branch International 
 Ny th : “ oe : > 
 lly ’ sal Society ofess Ste -hmz iining 
 , ' Neue Bachgesellschaft announces autumn Musical Society ( Professors ‘ rv Lehmann ag oe nings 
 — Bach aap festival Eisenach place | forming committee ) decided revive - — — 
 inded . " . sic , , — - . . . , 
 1 th September 23 24 . intended chamber- | ™ Usicum cultivation ancient chamber er 

 , IQIl 

 Messrs. Si Saens Julien Tiersot , performances , 
 vy Messrs. G. Dor nd Gabriel Grovlez , 
 Mil ( rbonnel ( tl Paris Opéra - Comique ) 
 title - par f ghest merit . scheme 
 vith complete s 2 
 . TOS 0 
 5 ! nd conductor , M. Sergius 
 . witzsky , f endowed new permanent 
 S y Orchestra seventy performers . association , 
 vhicl ly t ecom mportant factor Moscow 
 e , W mel ! ts activities coming 
 M ! W r Festival commenced 
 | 1 Prinzrege theater fine performances 
 2 G ni und Isolde , ’ 
 rshij Messrs ind Otto Lohse . -A 
 f Symphony conce arranged 
 lirectior f Herr Ferdinand Léwe , Tonhalle . 
 programmes includ Beethoven 
 S\ honies , works ydn , Mozart , Schubert , Liszt , 
 Ber Brahms , Tcl wsky , Bruckner , Mendelssohn , 
 \ r , S unn , Wagner , Richard Strauss . 
 t s Herr Max Reinhardt ( Sumurun fame ) 
 ta performances given enormous 
 Kiinstlerthe works included 
 scheme | r er yach * Die schéne Helene ’ 
 wenty - f times t l houses ) , new 
 ta * J : 
 ' : e- o 
 St f Bavar s il 
 sculptor B w placed temporarily 
 | 1 ult tely included national 
 Karl Ble latest work , * Ein 
 Hat Klang r alt é ir orchestra , 
 rod siderable ss Akademischer 
 ‘ ing 
 \PLES 
 \ ¥ oO era iciro , ’ composed 
 Onoft \lt tl Alberto Colantuani , 
 prod y att Teatro Mercadente . 
 PAI . 
 n Prix Rome young composers 
 uwarded M. Paul Paray . thirty voters 
 tl t r ttee , twe nty favour . 
 Hew rnin 1886 , 1 pupil Messrs. Charles 
 d Paul \ t Conservatoire 
 RAGUE . 
 T : t Deutsches Landestheater 
 ted festival works selected 
 Wagner ‘ Isolde , ’ ‘ Die 
 M r er , Mozart . , Verdi ‘ Ballo 
 3 r * Rigoletto , ’ 1d Ambroise Thomas 
 H ter r operas Italiar 
 gag Messrs. van Rooy 
 
 YRM 
 \ | Br Reger festival place July 20 
 2 ‘ rable s favourable reception 
 accorded Herr Max Reger , conducted Bliithner 
 Orchestr ] e work 
 r r 
 Att Theats Art fragments Lully opera 
 t l r t ‘ Psyche ’ proved great 
 ir r 
 { } 
 hare Strauss ‘ Elektra ’ 
 ] n lately included 
 ‘ r e ( rt Oper interesting feature 
 f Lortzing 1 comic operas ( Spielopern ) , 
 /ar 1Z ermann , * Der Wildschiitz , ’ ‘ Der 
 \ c ‘ Die beiden Schiitzen ’ 

 
 ‘ r theatre e ) special 
 r fl wig T } ile opera ‘ Lobetanz , ’ 
 . der direction Dr. Hess . 
 T t } t é ssful 

 English 

 published , trace influence opera 
 Beethoven Goethe 

 Mr. Edward Collier 8 , Hornsey Rise , London , N. , | 
 draws attention scheme graduated claviers . 

 Jubilate Deo , E. a.t.1.B. ( . 95 , Novello 

 Services , Anthems , Xc . , Men Voices . ) 3d . 
 \ ERKEL , GUSTAV.—Variations theme 
 4 Beethoven . Op . 45 . Edited Jounn E. West 

 . 22 , Original Compositions STAY 

 7 MINUET 

 Symrnonies 1x C , G Minor , 
 MOZART 
 iii . ) 
 BEETHOVEN 1 
 MACKENZIE 1 
 “ Brest Pair 
 - ‘ C. H. H. PARRY 1 
 “ MipsumMer Nicut Dream ” 
 MENDELSSOHN 1 

 Sonata E erat ) . ( Op . 31 , 
 & PRELUDE . — ( “ Cotompa ” ) A.C. 
 9 FINALE 

 Op . 48 

 3 Books ‘ oe @acl 
 LAZARUS , G.—Album 32 Plancfirte places ee os oe 
 MALIPIERO , G. F.—Three Pianoforte pieces . 1 . Gavotta , 1s . 
 net ; 2 . Mir uetto , TS . net ; 3 . Giga . - . 
 le Jeunesse Valselente . Pianoforte 
 Polka italienne . Pianoforte Solo , rs . 
 Pianoforte Duet ee ee oe os ee 
 RU R INSTE , Romance . Op . 44 . Transcribed 
 Op . 9 . ‘ Pianoforte pieces . 1 . Burletta , 
 Bohmisch 
 Op . 16 . Piéces antiques . Pix forte Solo 
 WINDING , A. Cwenty - Preludes . Pianoforte 
 Solo . New Edition A. F. Wouters . 
 ZILCHER , P.—Ornamental Technic . 100 examples fron 
 Beethoven , Chopin , Liszt , & c 

 CHAMBER MUSIC . 
 LE BORNE , F.—Pensée Nuptiale . Violin Pianoforte 
 NE ~ IA , F. P. Op 1 g. 1 . Intermezzo ; 2 . Capriccio - Walzer 

