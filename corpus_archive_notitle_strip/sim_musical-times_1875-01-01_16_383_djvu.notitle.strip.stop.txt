SECTION V. — “ difficult . ” 
 s. d.| 

 o . Bach , J. S.—Fantasia Chromatica D minor 
 4 © } 10 . Henselt , AA — Romanza Study F sharp , Op . 2 ...... 
 : D -M 11 . Chopin , F.—Scherzo B flat minor , Op . 31 ... - .. --+++ 
 “ Hell min 6 Press . 
 + Beth , St. — 4 © |12 . Schumann , R.—Romanza , D Minor , Op . 32 
 oven , 13 . Mendelssohn , F.—Capriccio , F sharp minor , Op . 5 “ si 
 & Men ie ) 6 © } 14 . Beethoven , L. van.—Grand Sonata , E major , Op . 109 

 13 . Schumann , R.—Two Caprices C E , Op . 5 
 x ay FE iP 4 16 . Bach , S.—Prelude Fugue , minor 
 eber , C. M. 17 . Chopin , F.—Impromptu G flat , Op . 51 
 tab = 4 18 , Liszt.—Three Hungarian Airs ............ 0iesseessseessees eee akon 
 title « Difficult ” meant convey idea Section provide pieces extreme difficulty suited 
 q na ’ cases ( scope “ School ” ) ; taxing high degree general Student intellectual 
 Fee oa mechanica ! powers works included found “ difficult ” play 

 C. JEFFERYS , 67 , BERNERS ST . 
 PUBLISHED 

 NEW PIECES , sy LOUIS DUPUIS . 
 GEMS SACRED ART : — 
 . 1 . HENRY VII . CHAPEL ... aa Sue 
 Beethoven “ Hallelujah , ” Mount qtcge 
 2 . ST . GEORGE — — EL 

 Jackson Te Deum 

 hael ; veriest dolt feel 

 soul force purity beauty . 
 missing link god - like 
 nobleness Apollo Belvidere , inimitable 
 tenderness sorrow Milton Lycidas . 
 cases types , mind directly 
 mm rapport creator , 
 come affect natural working 
 . music , poor music ! 
 Beethoven rose eagle wings genius 
 height Ninth Symphony , 
 immediate result ? mass paper covered 
 ink - stains ; differing way appearance 
 mass paper , containing 
 abortive oratorio Jones , gone butter- 
 man , Nay , chances Jones work , 
 neatly written handsomely bound joyful anti- 
 cipation British Museum honours , touches 
 butterman heart , finds place book- 
 shelf , “ fill , ” Ninth Symphony MS . 
 distributed customers . rub 

 hoven majestic art- 
 tteation , remains speechless unlovely — 
 achaos blots lines — save 
 hold key meaning know way 
 tum lock . come intermediaries 

 spite attractions announced concerts 
 evenings devoted higher class music , 
 doubt termed “ People 
 nights ” drawn far largest numbers . 
 “ Irish , ” ‘ ‘ Welsh , ” ‘ ‘ English ” Evenings 
 powerful appeal sympathies 
 love national music sake , names 
 Madame Lemmens - Sherrington , new soprano , Madlle . 
 Levier , Miss Edith Wynne , Madame Patey , Miss Sterling , 
 Mr. Sims Reeves , Mr. Cummings , Mr. Whitney , 
 artists established reputation , offered sufficient 
 guarantee vocal music ample justice 
 . ‘ Israel Egypt ” Bach St. Matthew ‘ “ Passion 
 Music ” ’ ( Mdlle . Levier , sang soprano 
 , confirmed success ) shown 
 powers choir advantage . Mention 
 performance Dr. Hans von Bilow , 
 Mr. C. Hallé , Miss Agnes Zimmermann , Miss , Miss 
 Emma Barnett ( played brother clever Concerto ) , 
 Mr. Walter Bache Mr. J. F. Barnett , 
 thoroughly supported reputation exponents 
 highest style classical music . ‘ ‘ English ” 
 nights Sir Sterndale Bennett Overture ‘ ‘ Paradise 
 Peri , ’’ Mr. G. A. Macfarren “ ‘ Festival Overture ” Mr. 
 Prout Organ Concerto ( Dr. Stainer exponent 
 Organ ) conspicuous features 
 programme , elicited warmest applause . 
 impossible half compositions 
 performed month ; 
 scheme announced conscientiously 
 carried . Mr. Barnby talented conductors 
 occasionally replace , fully deserving 
 highest praise energy good cause 

 concert given Royal Academy Music 
 roth ult . , Hanover Square Rooms , attracted 
 large number , patrons friends 
 Institution , general public ; , apart 
 fact excellent orchestra , choir , solo vocalists , 
 instrumentalists appearing occasion , circum- 
 stance performance given 
 Rooms , highest degree interesting . 
 able direction Mr. Walter Macfarren , excellent 
 selection performed , 
 execution pieces , audience forgot 
 fact concert students . pianists — 
 Miss Alice Curtis , Miss Conolly , Miss Katie Steel , Miss 
 Bucknall Mr. Walter Fitton — especially distinguished 
 ; Mdlle . Gabrielle Vaillant Beethoven 
 Romance F , Violin , enthusiastically 
 received . Mr. G. A. Macfarren Cantata ‘ Christmas , ” 
 rendered care precision , 
 principal parts admirably given Miss Jessie Jones 
 Miss Barkley . composer loudly called 
 conclusion Cantata , bowed Royal 
 box . Praise given vocalists — 
 Misses Marie Duval , Reimar , Bolingbroke Nessie 
 Goode , Messrs. Henry Guy Ap Herbert — Miss Jessie 
 Jones creating marked effect “ Hear ye , Israel , ” 
 “ Elijah . ” concert concluded National Anthem . 
 Sir Sterndale Bennett , Principal Academy , 
 professors students Institution 
 present 

 Royat AcapEmy Music.—The examination 
 Westmorland Scholarship Potter Exhibition took place 

 Monday 2rst ult . , examiners Pring ; 
 ( Sir Sterndale Bennett ) , Mr. F. R. Cox , Signor M. , Gari 
 Mr. H.C. Lunn , Mr. G. A. Macfarren . 
 follows:—Westmorland Scholarship , 
 Charlotte Agnes Larkcom , elected ; Potter Exhibition , 
 Miss Alice Mary Curtis , elected 

 Miss Grace Linpo gave highly successful concert 
 Beethoven Rooms , Harley Street , rsth ult 
 programme carefully drawn , entire 
 performance gave greatest satisfaction . Miss Lindo 
 sang recitative aria , ‘ “ ‘ Non piu di fiori ” ( 
 unrivalled clarinet obbligato Mr. Lazarus ) , Adolphe 
 Adam “ Cantique de Noél , ’ Dr. C. G. Verrinder new 
 ballad , ‘ ‘ tale told , ” Lachner ' [ ig 
 “ ‘ Waldvoglein ” ( violoncello obbligato , Herr Schuberth ) , 
 Miss Julia Sydney , Madame Elwood Andrea , Messrs. Noble , 
 Belmont , Le Messurier , Dexter , Trelawney Cobham 
 rendered valuable service vocal selections , Miss 
 Josephine Lawrence , Mr. Pearce , Herr Schuberth , Mr. 
 Lazarus , Herr Oberthir contributed instrumental 
 pieces . vocal music accompanied Dr , 
 Verrinder 

 musicaL performance given pupils 
 London Society Teaching Blind Read 4th 
 ult . , Institution , Upper Avenue Road , Regent Park , 
 included selection ‘ Messiah , ” 
 second miscellaneous . able 
 direction Mr. Edwin Barnes , Professor music 
 Society School , choral pieces 
 excellently rendered ; solos , vocal 
 instrumental , worthy praise . Chair 
 occupied F. Peterson Ward , Esq 

 Gallery German Composers . Prof. Carl Jager . 
 Biographical Critical Notices Edward F. 
 Rimbault , LL.D 

 Tuts volume Portraits conscientiously recom- 
 mended valuable gift - book , having 
 acquaintance great German composers 
 works , anxious look features 
 expression men bequeathed inestimable 
 legacies world . Bach , Handel , Gluck , Haydn , 
 Mozart , Beethoven , Schubert , Weber , Mendelssohn , Schu- 
 mann , Meyerbeer Wagner artists chosen 
 illustration , portraits having carefully executed 
 oil paintings Professor Jiger , photographed 
 style art . biographical critical 
 notices , supplied Dr. Rimbault , 
 short , afford necessary information , advantage 
 , unlike biographies come 
 , facts relied . small , 
 designed wood - cut forms head - piece notice ; 
 gorgeous binding volume — apart 
 richness contents — attractive book 
 drawing - room table . work 
 warmly welcomed art lovers country , 

 

 fashionable , performance gave greatest satisfaction 

 Currron.—On Saturday , roth ult . , Mr. Charles Hallé 
 Madame Norman - Neruda gave pianoforte violin Recital 
 Victoria Rooms , forming ofthe series Clifton Winter Entertain- 
 ments organised Mr. James C. Daniel . programme included 
 selections Beethoven , Rust , Heller , Ernst , Chopin , Liszt , 
 Vieuxtemps , Brahms , Joachim , Schumann . Notwithstanding 
 inclemency weather , large audience . 
 artists played usually excellent manner , Madame 
 Neruda , regret hear , suffering severe indisposition , 
 obliged sit portion performance 

 ee 

 Croypon.—The annual concerts given Whitgift School - boys 
 took place Wednesday Friday evenings , 16th 18th ult , 
 Mr. Cummings Fairy Ring performed , selection 
 - songs , & c. band efficient , played diff . 
 cult accompaniments Cantata excellent style . concl . 
 sion concert , R. A. Heath , Esq . , governors 
 admirable institution , effective speech advan 
 giving boys knowledge music , highly complimented Mr , 
 Griffiths state efficiency brought 
 boys . magnificent testimonial ( gift Mr. Heath ) 
 presented Mr. Griffiths , consisting silver - gilt inkstand , mam . 
 factured Messrs. Elkington , suitable inscription ona 
 scroll , supported figures boys holding trumpets , valued 
 - guineas . Mr. Griffiths affected 
 unexpected honour , returned thanks suitable speech , Mr 
 Hullah present concert Friday 

 Devizes.--The Amateur Choral Society gave performance 
 Christmas Carols miscellaneous selection Monday , 14th 
 ult . , direction Mr. J. T. Abraham . admired 
 pieces Baumer - song , “ chimes Oberwesel , ” 
 bells obbligato , Pinsuti ‘ ‘ shall , ” Roeckel “ Sweet Lisette ” 
 “ Wave , ” duet ( Guglielmo ) , “ Scenes brightest , ” 
 Maritana , chorus Flotow Marta . great attraction 
 evening pianoforte playing Mr. Bambridge , 
 organist Marlborough College . performance finale 
 Beethoven “ Waldstein ” Sonata , “ Norwegian Melodies ” ( W.§ 
 Bambridge ) created perfect furore large audience 
 filling Town Hall . Mr. Sly Mr. W. Price accompanied 

 Devonport.—A concert given Mechanics ’ Institute 
 Wednesday evening , gth ult . , Mr. W. H. Hannaford , organist 
 St. John . orchestra composed band chors 
 numbering seventy performers . pieces form . 
 ing principal features programme , Birch pastoral 
 Operetta , Robin Hood , Mendelssohn Motett , ‘ Hear prayer . ’ 
 Mr. W. H. Hannaford conducted ; Mr. W. W. Brown presided 
 pianoforte Mr. C. Clemens harmonium . Miss Triggs 
 ( soprano ) sustained Maid Marian . Mr. Donovan , 
 Mr. Boolds , Mr. Rendle vocalists . Mendelssohn 
 “ Hear prayer , ” Miss Triggs chorus , admirably ret 
 dered . local favourite , Miss Snell , sang “ O bid faithful Arie 
 fly ” effect enlist hearty encore . duo pian 
 forte harmonium , Mr. Master Hannaford , encored , 
 close second performance Master Hannaford 
 ceived ovations house . solo violin , Mr 
 Pardew , chorus , “ Let hills resound , ” brought 
 evening entertainment close 

 DurHaM.—On 22nd ult . Mr. J. C. Whitehead , late organist 
 St. Cuthbert Church , assistant organist Cathedral , 
 appointed organist Bury Parish Church , , 
 choir St. Cuthbert , entertained dinner 
 vicar , churchwardens , congregation . dinner Mr. 
 head presented valuable testimonial , consisting 
 watch chain , gold signet ring , inkstand 

 EastTBourNE.—On Monday , 21st inst . , Mr. J. H. Deane , 
 organist Trinity Church , gave series weekly 
 concerts classical music . assisted Mr. J. Taylor 
 ( organist St .. Saviour , Eastbourne ) , brother , Mr. Edward 
 Deane , Crystal Palace Philharmonic Orchestras , Miss 
 Roper , Miss Headland , Herr Cramer , Herr Siebenheller , Messrs. 
 Cooper , & c. , instrumentalists ; Miss F. Douglas , Misses 
 Mr. C. Roper , vocalists . works , Haydn 
 Symphony , portion Beethoven Pastoral Symphony , Septett , ft , 
 performed . songs included Gounod “ Ave 
 Maria , ” Handel “ Let wander unseen , ” “ Revenge ! Ti 
 cries , ” “ Honour arms , ” & c 

 EpInBurGH.—Sir Julius Benedict St. Peter performed 
 30th November , Edinburgh Music Hall , Choral bas 
 vocalists Mdlle . Enequist , Miss Marion Severn , 3 % 
 Bentham , Signor Agnesi . tone chorus g¢ 
 good , parts balanced , points Spree 
 caught precision vigour , , taking , 4 
 singing credit Society , Mr. Adam Hami — 
 conductor . Mr. Carrodus - disciplined followers 
 thoroughly efficient , accompaniments incidental sy@- 
 phonies splendidly played . Miss Severn produced 2 | pa 
 impression ‘ O , thou afflicted , ” sung Lee = 
 expression . Mdlle . Enequist gave “ mourn dove 
 quisite tenderness . Mr. Bentham sang tenor solos , feel 
 Wadmore , took short notice , sang great 
 “ O head waters.——PRoFESSOR OAKELEY gave af ‘ 
 performance roth ult . , University Music Classroom 

 generally effective , especially string portion , advan Second ] 
 n ective , e . - F 

 tage assistance Beethoven Quintette Club . Mr. F. M2@LLoY , Pe L.—The Shipwright . Song . Words 

 H. Torrington conducted . concert given written F. E. Weatherley . 2s . NG 
 conte imped bg ag ee Mrs. ge F ned — — Rose - Marie . Song . Words written F. E. Weatherley , » D 
 vocalist , applauded songs . e Epwin 
 Beethoven Quintette Club , consisting Messrs. C. N. Allen J. C. ACIRONE , C. A.—Lullaby . Song . C E , “ op 
 Mullaby ( violins ) , H. Heind’l W. Rietzel ( violas ) , Wulf Fries Words written Sir Walter Scott . 2s . “ Penite , 
 ( violoncello ) , performed Beethoven Theme Variations , Op . 104 , TKINSON , FRED . C.—When World ‘ - 

 Mendelssohn Scherzando Adagio , Op . 87 , Piano Concerto , ss 
 Op . 25 ( conjunction Miss L. Crowle ) , overtures Young . Song . Words Prof. Charles Kingsley . 2s . suitable 

 WoopxuouseE.—On 8th ult . miscellaneous concert sacred BRADFORD , JACOB . asp Riper RB . Nune 
 secular music given Mechanics ’ Institute , Institution dimittis ( Chant Service ) . Set 3rd 8th Gregorian 
 Street , members choir St. Mark Church , assisted | Tones , Endings . 8vo . , 3d . } io 
 - known singers . object entertainment , ROSLAND JONATHAN.—Evening Service sdditiona 
 stated vicar ( Rev. J. S. Abbott ) , laudable ® Bb pee NG hy Magnificat Nunc dimittis , accom dicite , ” a1 
 establishing fund providing music use church . | 2h : ent ge Pinucheate 8vo . , 8d ‘ pc 
 programme consisted selections | P 8 : z baa 

 Oratorios Handel , Haydn , Beethoven , Mozart AREBROTHER , BERNARD.—Te Deum lauds . bishop 

 ae Mass ; hm agg oo bg apa glees , pisi - cvees mus , . 8vo . , 6d . EN 
 iss L. A. Buckingham , ‘ ‘ Thou didst leave , ” acquitted her- 7 

