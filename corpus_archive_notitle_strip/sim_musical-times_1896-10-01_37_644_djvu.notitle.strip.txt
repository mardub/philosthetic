Melody is the most important branch of 
all musical art, and the one about which 
theorists tell us singularly little. Few com- 
posers build up melody deliberately and 
with a clear consciousness of the operation. 
There is a widespread, but quite erroneous 
notion that the process is automatic, and can 
only be properly accomplished under emotional 
stress. Yet it is just those two composers (Beet- 
hoven and Wagner) who built up their melody 
with the most deliberation and patient labour 
(could any emotional stress last so long?) who 
have risen to the greatest artistic heights

In Wagner’s later works the most intensely 
passionate and expressive melodies will be found, 
upon close inspection, to be consciously built 
up out of the representative phrases which form 
his plastic material for the time being. So 
clearly is this the case that the student is 
inclined to think it a very easy and mathematical 
way of composing music ; but wait till he tries! 
It was easy for Purcell to write Dido’s lament 
on a ground bass, and easy for Bach to write 
the most spontaneous sounding chorus as a 
four-part canon on a choral; but no one else 
could do these things, simply for lack of that 
power which enables the possessor to rapidly 
survey thousands of possible alternatives instead 
of two or three, and to choose the best out of 
all these. For the elements which go to make 
a composer are, firstly, memory, without 
which nothing can be done; before we can 
employ material we must be able to bear 
in mind what has already been done and 
what we have got to go upon. Secondly, 
intellect, which varies of course enormously 
with different individuals. The composer who 
reasons out his procedure and deduces his 
proper course from experience is not necessarily 
a great composer, but his work is sure to have 
value. The third element is the esthetic 
faculty or feeling for a particular form of 
beauty, and this sense is seldom found in 
company with the intellectual faculty. They 
are the masculine and feminine elements of art, 
and when we do find them combined we havea 
very great artist indeed. Thus Cherubini had 
intellect, Chopin had feeling; Beethoven 
and Wagner were among the few who had 
both in a very high degree

It has always seemed to me that Wagner’s 
intellect developed early and his esthetic feeling 
ripened slowly; his early musical productions 
follow existing models with wonderful clever- 
ness, but are deficient in feeling and charm— 
indicating that his power of choice in the 
selection of alternatives was slight at first. In 
none of his works until ‘‘ Lohengrin” do we 
find much natural ease in the melodic phrases. 
The interesting thing about the first four operas 
is to see how the composer rises to the occasion 
in the important places and how he deliberately 
makes padding in the unimportant ones, thus 
showing that he had mastered the secret of 
dramatic effect from the beginning. His melody 
was, of course, founded on that of his immediate 
predecessors and contemporaries, Beethoven, 
Spohr, Weber, &c. Very curious it is to think 
that the composer of “Tristan’’ could ever 
have penned that Beethovenish aria in ‘ The 
Fairies” which begins

Larghetto. 
cantabile, : vom 
aes SSS 
a =. ze eg 
_——— ~ 
p

Eas aol

and confess that the art of thematic develop- 
ment did not die with Beethoven. I need not 
multiply examples, but look at the infinite 
resource shown in handling the Siegfried 
theme

a

ry ad

Sir George Grove has well observed that the 
principal feature of Beethoven’s melody is the 
conjunct flow. Wagner’s special feature, on 
the contrary, is the bold skip, a feature which 
can never fail to arrest attention; but if used 
frequently in so similar a manner as the above 
quotations show, it is apt to convey to the 
superficial listener the idea that the composer 
is deficient in inventive resource. Although I

have expressed the opinion that he was so 
originally, one has only to read through the

Ir is to be sincerely hoped that the multiplicity 
of orchestral concerts in London this autumn will 
not withdraw, to any serious extent, the support of 
music-lovers from the long-established and magnifi- 
cent series of Saturday Afternoon Concerts at the 
Crystal Palace, which enter on their forty-first 
season on the 3rd inst. Foreign conductors may 
come and give us readings new and strange, but it 
should not be forgotten that Mr. Manns has toiled 
long and with rare perseverance and singleness of 
artistic purpose, that every music-lover is deeply 
indebted to him for acquaintance with masterpieces 
that for years were virtually unknown in this country, 
and that his orchestra is the nearest approach we

possess to the ideal permanent body of instru- 
mentalists. Moreover, it is only at such serial 
Concerts that the less important works of the great 
masters are likely to get a hearing, for in a short 
series of performances it is necessary to place on the 
programmes the best known symphonies. It is 
obvious that this winter the Crystal Palace Concerts 
will have to compete with far greater rivalry in the 
metropolis than has existed before, and hence the 
generous support of the Sydenham neighbourhood 
would be peculiarly opportune. To attend these 
Concerts regularly is to receive a musical education 
which will widen appreciation of the art and increase 
the capacity for its enjoyment. Great care and 
thought have manifestly been devoted to the pro- 
grammes, which present an excellent selection of 
music *“‘new and old.” Amongst the novelties are 
noticeable a Concerto for violoncello and orchestra, 
by Klughardt; an Overture, entitled ‘ Otello,” by 
Walter Macfarren; a Suite of characteristic Dances 
from the opera of “ Mlada,” by the Russian com- 
poser, Rimski-Korsakoff; a Symphonic Prelude, by 
W. Wallace; the Vorspiel to E. d’Albert’s opera 
‘““Ghismonda’”’?; Miss Chaminade’s ‘ Callirhoé”’ 
Suite, and a Concertstiick for pianoforte and 
orchestra by the same gifted lady; a Légende 
Symphonique, by Vincent d’Indy; the new Concerto 
for violin and orchestra, composed by Frederick 
Cliffe for the forthcoming Norwich Festival; the 
first performance in England of Richard Strauss’s 
Symphony in F minor (Op. 12) ; F. H. Cowen’s “‘ Four 
Old English Dances”; Barclay Jones’s Symphony in 
C minor, and Dvorak’s Concerto for violoncello and 
orchestra. Beethoven’s first, fourth, and fifth 
Symphonies, with a large number of favourite works, 
are also promised before December 19, when the 
first half of the series will end. No less compre- 
hensive is the list of principal artists, which include 
many who are justly considered to be the finest 
exponents of the particular branch of art that they 
have specially cultivated. The choral works selected 
are Sullivan’s ‘* Golden Legend” and Berlioz’s 
“Faust,” which are severally announced for the 
31st inst. and November 21. No pains, indeed, 
would seem to have been spared to make the 
Concerts as attractive as possible to all who derive 
pleasure from music, and in these days such a scheme 
should not appeal in vain

Tue humble consciousness of our inability to 
adequately render the following ‘* programme,” 
contained in a German paper, of Herr Richard 
Strauss’s mysteriously named symphonic poem “ Thus 
spake Zarathustra,” should be no valid excuse for 
withholding it from our readers. It runs thus: 
‘First movement. Sunrise. Man (the ‘human man,’ 
as Mr. Chadband would have expressed it) feels the 
power of the Godhead.—Andante religioso.—But his 
ardent longing remains unsatisfied. He now plunges 
into a whirl of extravagant passion (second movement) 
without finding satisfaction therein. Science next 
attracts him, engaging all his faculties, and in a 
fugue (third movement) he endeavours to evolve the 
problem of existence. Lost, however, in the maze of 
conjecture, he finally despairs of finding a solution. 
Thereupon, an inner voice calls upon him to rejoice 
in the very fact of his being, dance tunes (fourth 
movement) supervene, in the rhythm of which all 
preceding motivi are recapitulated, ascending higher, 
and higher still, in unheard-of contrapuntal combina- 
tions, until the soaring soul of the earthly pilgrim 
disappears in the rosy dawn of morn (B major), while 
the world (C major) sinks down into the profundity 
of the basses, and there remains.” Thus, then, we

THE fine organ in Bangor Cathedral, built by 
Messrs. W. Hill and Sons twenty-five years ago, is 
in so bad a state of repair that it has been found 
imperatively necessary to thoroughly overhaul it, 
and a Bangor Cathedral Organ Restoration Fund has 
been established for the purpose. It is proposed to 
supply new mechanism to the manuals, pedals, and 
draw-stops; to remove the keyboards from their 
present position to one that will better enable the 
organist to hear the effect of his instrument and the | 
singing of the choir; to supply hydraulic blowing | 
apparatus; to add sixteen new “ stops,” one of these | 
being a 32-ft. pedal diapason; and to effect various 
other modifications suggested by the great advance 
made of late in the art of organ-building. These 
improvements and the repairs (which are so sadly 
needed that the instrument, at present, scarcely bears 
the strain of its periodical tuning) will cost at least 
£2,000, of which only about half has yet been 
subscribed, and of this £250, given by an anonymous 
donor, is promised conditionally on the scheme 
being carried out in its entirety at once. The list of 
subscribers is headed by Lord Penrhyn, and includes 
the Duchess of Wellington, the Duke of Westminster, 
Lord Harlech, Lord Boston, and the Bishop and 
Dean of Bangor. Subscriptions should be sent to 
the hon. secretaries (the Rev. R. S. Edwards and 
Mr. T. Westlake-Morgan, organist of the Cathedral), 
Bangor Cathedral Restoration Fund, Bangor

THE issue of a second edition of Sir George Grove’s 
delightful book, ‘Beethoven and his Nine Sym- 
phonies,” has enabled the accomplished author to 
effect several improvements. First and foremost, the 
work has now an Index, the want of which was, in 
the earlier edition, much felt—how much, may be 
gathered from the fact that this absolutely indis- 
pensable addition to the volume occupies quite six 
pages and contains over 200 entries, many of them 
with quite a number of references. ‘“ Mendelssohn,” 
for instance, and ‘*Schumann” have nineteen each, 
‘Schubert’ has ten, “ Berlioz” fifteen, and

of the Worcester Musical Festival

Bonaparte” twelve. Secondly, the useful list of the 
i 5 with their dates, &c., at the beginning | 
of the book now contains also the opus numbers of | 
the ‘‘immortal nine,” and, thirdly, several slight | 
errors and omissions have been rectified in the | 
metronome marks, quotations from books, &c., and | 
musical examples. The number of the latter con- | 
tained in the book is more than four hundred, so that | 
altogether ‘‘ Beethoven and his Nine Symphonies” is | 
by far the most complete and satisfying work of the | 
kind extant in any language. That such a monument | 
to the greatest of German composers should have | 
been erected in “unmusical” England and by the 
patience and enthusiasm of an Englishman will, we 
trust, be allowed to weigh against the many artistic 
sins with which we are so often charged

Mr. F. G. Epwarps, in the first instalment of his 
interesting article ‘‘ Bach’s Music in England,” in 
our last number, refers correctly to the earliest 
edition of the ‘* Well-tempered Clavier,” by Nageli of

this season with sixty excellent players, Mr. Newman was 
so encouraged by the support he was receiving that, on 
the 12th ult., the orchestra was increased to ninety, and 
on and after that night performances have been given that 
might challenge comparison with those at Philharmonic, 
Richter, Mottl, or other metropolitan Concerts where the

best tastes are consulted. To go seriatim through every 
programme would be impossible, but it is only fair to draw 
attention to a few of the salient features of these admirable 
Concerts. Mr. Newman does not believe in playing down 
to so-called “popular” taste. Whatever might have 
been the case in a former generation, there is abundant 
evidence to prove that the best music has now become 
the most popular. Mondays are being devoted to 
+ Wagner and Liszt, Wednesdays to various classical 
masters, and Fridays to Beethoven. Even on the miscel- 
laneous nights the programmes have been strongly leavened 
with selections to which any musician might listen with 
pleasure. Many works new to London, chiefly by modern 
composers, have been introduced, and some of them may 
be mentioned. We have heard a bright and piquant 
‘‘Joyeuse Marche” by E. Chabrier, and a ‘ Slavonic 
Dance” from the same French composer’s opera ‘“ Le Roi 
malgré lui’; three numbers from Moszkowski’s ballet 
‘“‘ Laurin,” the second of which, a stately Sarabande, might 
almost have been written by Handel ; a Fest March by Cyril 
Kistler, whose operas, penned in a Wagnerian vein, have 
yet to be heard in this country; and J. L. Nicodé’s 
Symphonic Variations in C minor, Slavonic in character as 
becomes a Polish composer. The vocal and instrumental 
soloists have, generally speaking, been well chosen, though 
of course many of the best artists are not available at this 
period of the year

Special record is required of the Concert on the 23rd ult., 
in commemoration of Her Majesty the Queen’s ‘‘ Record 
Reign.” Mr. Newman was fortunate in,being manager of 
Promenade Concerts on such an occasion, and he proved 
himself equal to it. So did the public, for they came in 
such swarms that the Queen’s Hall was crowded as it has 
never been crowded before. The first part of the pro- 
gramme could not have been more happily chosen. The 
National Anthem was given according to Costa’s arrange- 
ment, and the audience were invited to join in the 
third verse. This they did with a will, and the cheers 
and applause that followed were so prolonged that the 
verse had to be repeated. Then came Sir Alexander 
Mackenzie’s ‘‘ Britannia” Overture, with its many happy 
patriotic and nautical touches. The third item was a new 
‘‘ Coronation’? March, specially composed for the occasion 
by Mr. Percy Pitt. This is a well wrought piece, built on 
the lines of Wagner’s “ Huldigungs” and ‘“ Kaiser” 
Marches—that is to say, slightly solemn and religious in 
character and yet festive. The composer was called to the 
platform, and the symmetrical and well-scored piece should 
be heard again. A very fine performance of ‘‘ The Hymn 
of Praise’? brought the first part to a conclusion. Mr. 
Wood had his forces under perfect control, the choruses 
being sung with surprising spirit and unfailing accuracy ; 
while the solo parts were perfectly safe in the hands of 
Madame Fanny Moody, Miss Isabel MacDougall, and Mr. 
Ben Davies. The Concert wasa triumph in every respect

Novello’s Music Primers and Educational Series. Edited 
by Sir John Stainer and Dr. C. Hubert H. Parry. Sonata 
Form. By W.H. Hadow. [Novello, Ewer and Co

TuIs is one of the more recently issued volumes of 
Messrs. Novello’s Music Primers and Educational Series, 
the importance of which it would be difficult to over- 
estimate. The present number of the series is from the 
pen of a musician who has given for many years his special 
attention to this particular subject. The importance to the 
musical student of the subject of ‘‘ form,” even in its broad 
acceptation, will be at once admitted, for where there is no 
form of any kind, art is not. A thorough knowledge of 
Harmony, even the mastery of Counterpoint with all its 
purifying influence, will be in a great degree neutralised if 
the student has neglected the study of ‘‘ form ’’—the mode 
of building up his structure in an orderly and intelligent 
manner. The book before us opens with a capital chapter 
on the Elements of form, next its history and development 
are treated of, a lucid explanation is given of the circum- 
stances which caused the lines of sacred and secular music 
early to diverge—how ecclesiastical music was bent upon 
maintaining the ancien régime; how, on the other hand, 
secular music, unfettered by the restrictions imposed by the 
Church, and turning off into fields and tracts unknown, 
was destined to become a brilliant guiding-star, the harbinger 
of a complete revolution. It would be pleasant to follow 
Mr. Hadow farther along the historical line did space 
permit us todo so. The next chapters treat respectively 
of Binary and Ternary forms. Here it may be said that in 
regard to the actual evolution of the Sonata there will 
probably be some who disagree with Mr. Hadow; he is, 
however, a man of such experience that whatever he says 
should be listened to with respect, nor would it be safe 
lightly to contradict him. The various blocks of work, 
‘* Exposition,” ‘“‘ Free Fantasia,” and ‘' Recapitulation ” are 
excellently treated, a large number of the examples being 
taken from Beethoven’s Sonatas, which is a great advantage, 
as students have them ready at hand for reference. Even 
his account of the ‘‘ Introduction” to a First Movement— 
which in many books is little more than touched upon—has 
here received very careful consideration. After pointing 
out its obvious utility, that of preparing the -mind for the 
key of the First Movement proper, he says: ‘‘ Another higher 
and far more important use of this Preface, as it were, is 
to foreshadow in miniature the more salient points of key- 
system embodied in the First Movement.” A happy 
instance is given from Beethoven’s Seventh Symphony, 
where it is apparent that the keys of A, C natural, and F 
natural are those centres in which important material is to 
be subsequently planted; and turning to the First Move- 
ment itself (which is in the key of A) its development, or 
Free Fantasia portion is found to be working largely in the 
fields of C natural and F natural, the same centres being 
chosen also for important work in the Finale. In the 
explanations of the First and Second Subjects and their 
possible appendages, we do not quite agree with the 
classification of our author. Mr. Hadow would include 
under the term ‘‘ Subject” the pieces of Codetta or 
connecting work which, for obvious reasons, are frequently 
found tacked on to the Theme after it has run its own 
course; but it is a known fact that when we arrive at 
the Recapitulatory section, these appendages are generally 
thrown off, because at this point the idea of concentration 
is so much in the ascendancy, that short, fragmentary 
work becomes absolutely de tvop. We would therefore 
prefer the retention of such terms as ‘‘ Codetta,”’ and would 
draw a line of demarcation for the use of students at the 
actual end of the subjects proper. Coming to the Episodes, 
which are explained in the most thorough and exhaustive

manner, we can only say that the student will here find all 
the information he may require, everything being most 
clearly mapped out, lucidly explained, and supported at 
each stage by well-selected illustrations. Farther on a 
chapter is devoted to the discussion of those important 
“ extraneous influences” which, in no slight degree, have 
| affected the Sonata in its growth upwards. The ancient 
| and modern forms of the Rondo are fully explained in 
| Chapter ix., and the book ends with the treatment of the 
“Concerto.” Musicians may possibly have formed their 
| Own opinions on the subject of the historic growth of the 
| Sonata; be that as it may, they will find in this book a 
|mine of useful matter, the result of long and careful 
|research. There is a slip on page 59, where the paragraph 
| number should obviously be § 65 and not § 64

Mr. Kirkhope’s Choir is to give “Samson” at its first 
Concert, and Mr. Millar Craig’s Choir is preparing 
Cherubini’s Requiem Mass

The programme of Messrs. Paterson and Sons’ tenth 
annual series of Orchestral Concerts shows an immense 
improvement on last year’s scheme. The artists already 
announced include Mesdames Henschel and Saddler Fogg, 
Miss Ada Crossley, and Messrs. Holmes, Hedmondt, and 
Andrew Black; Mdlle. Soldat and Messrs. Willy Hess 
and Willem Kes; Fraulein Ilona Eibenschiitz and Messrs. 
Sapellnikoff and D’Albert. The programmes have avoided 
the sins of omission and commission of last year, and 
Brahms, Schubert, Schumann, Haydn, and Dvorak are well 
represented by symphonies, while important novelties are 
Tschaikowsky’s ‘Romeo and Juliet,” German’s new Suite, 
Richard Strauss’s ‘' Till Eulenspiegel,” &c. In one pro- 
gramme stand Spohr’s Nonet, Schubert’s Octet, and 
Beethoven’s Septet, and in another the first Act from the 
“ Walkie

The great difficulties which the very inadequate accom- 
modation of our Music Hall have always placed in the way 
of cheap concerts is at last to be removed. The whole city 
has been stirred to its depths in its efforts to suggest the 
best site for the new Music Hall, the munificent gift of Mr. 
Andrew Usher. This generous donor has placed £100,000 
for the purpose in the hands of the Lord Provost and Town 
Council, and the work will be begun as soon as a suitable 
site is chosen

MUSIC IN GLASGOW. 
(FROM OUR OWN CORRESPONDENT

THE supplementary prospectus of the Choral and Orches- 
tral Union shows that the coming season will extend over 
a period of sixteen weeks, from November 2 till Saturday, 
February 20, 1897. The scheme provides for fifteen 
Classical Concerts (twelve orchestral and three choral) 
and twelve popular Orchestral Concerts in St. Andrew’s 
Hall. Mr. Willem Kes returns, of course, as conductor of 
the Orchestral Concerts, and Mr. Joseph Bradley will, as 
heretofore, direct the Choral Concerts. The list of works 
includes Bach’s ‘St. Matthew” Passion Music, Beet- 
hoven’s “ Ruins of Athens” and Choral Symphony, as also 
Mendelssohn’s ‘“ Elijah.” Beethoven figures largely in

the prospectus; he will, indeed, be represented by six 
Symphonies, three Pianoforte Concertos, the ‘ Fidelio” 
(No. 4) and the ‘‘Coriolan”’ Overtures, and a selection 
from the Septet. Dvorak’s E minor Symphony (from ‘“ The

The Trojansat Carthage’ is thesecond of the two operas 
by Berlioz, which, together, have the comprehensive title of 
“Les Troyens,” the first being entitled ‘The siege of 
Troy.’ It was first produced in Paris in 1863, at the 
Théatre Lyrique, and had a run of thirty nights; and 
although the opera was a comparative failure with the 
general public at the time, its great merits were acknow- 
ledged by all the musicians and musical élite of Paris. It 
was revived at the Opéra Comique in 1892. The only 
other performance I know of was at Carlsruhe about 
three years ago, under the direction of Mottl. The plot 
deals principally with the love of Dido and A2neas, and 
the death of the former. The work is thoroughly charac- 
teristic of Berlioz’s most matured style, it being, indeed, 
almost the last important thing he composed, and contains 
many numbers of great beauty and interest. ‘The per- 
formance in concert form by the Liverpool Philharmonic 
Society in March next will be the first complete one in this 
country, although short excerpts from the opera have at 
rare intervals been heard in the concert-room

The Symphonies to be performed during the season are 
as follows: Beethoven’s No. 7, in A; Haydn’s in G, 
Letter V; Tschaikowsky’s No. 6, in E minor; Liszt’s 
Poem, ‘ Tasso’; Mozart’s “ Haffner” Serenade; 
Brahms’s in C minor; and Raff’s ‘“‘ Lenore ”’—a truly 
comprehensive catalogue

680

the carrying on of the Thursday evening Subscription Con- 
certs with even more than their former efficiency. The 
artists secured are more numerous than heretofore and ot 
the highest available talent. Only one nameis missing for 
which we earnestly looked—namely, that of the distinguished 
lady who, for so many years, was closely and affectionately 
associated with some of our chief musical delights. We 
hope that Lady Hallé may soon feel strong enough to meet, 
without overpowering pain, the audience of old friends and 
admirers which would only too gladly and sympathetically 
again listen to her matchless playing

Messrs. Forsyth have cared also for the physical 
comfort of their subscribers, and are happy in being able to 
announce that ‘the old uncomfortable benches have been 
done away with, and that luxuriously upholstered chairs 
have been substituted.” It was high time for such a 
change in the seating of our chief concert hall. The season 
will open on the 2gth inst., when Senor Sarasate will play 
the Concerto in C minor of M. Saint-Saéns; and, during 
the winter, Gounod’s ‘‘ Redemption,’’ Cowen’s ‘“ Ruth,” 
and Beethoven’s Ninth Symphony will diversify the choral 
undertakings. Mr. R. H. Wilson, who rendered such 
important service last year, has remodelled the choir, 
weeding out the voices which had lost their freshness 
however reliable in attack they may have become

Another most hopeful fact is that, after many years of 
hesitation and of fluctuation, the Saturday Evening 
Concerts will be placed upon a firm and reliable footing. 
Messrs. Forsyth avow their responsibility for a set of eight 
performances, with the full orchestra to which we are 
accustomed, under the guidance of its chief, Mr. Cowen. 
The difference between the Thursday and the Saturday 
evening programmes will lie chiefly in the greater liveliness 
of the latter; but with a resolute avoidance of all triviality 
and of the devotion of so capable a band to anything 
worthy of its power. No one who has ever noticed the 
immense crowds thronging Peter Street and Oxford Road 
on Saturday evenings could doubt the eagerness of the 
response which will be made to the invitation to be given 
to all lovers of music on November 7 and on many subse- 
quent Saturdays

681

mornings and evenings of the 13th and 14th inst. Mr. 
August Manns will conduct, and the orchestra will consist 
of forty-four members of the Crystal Palace band, supple- 
mented by nineteen local instrumentalists. The works to 
be performed are ‘‘ Elijah ’’ (Mendelssohn), “‘ The Golden 
Legend” (Sullivan), ‘‘ Job” (Parry), and “ Faust” (Berlioz). 
The second and third Concerts will be completed with 
miscellaneous selections, among them being Beethoven’s 
‘“* Pastoral’? Symphony and Dvordk’s ‘‘ America’? Sym- 
phony. Dr. Parry will conduct the performance of ‘‘ Job” 
and Dr. Coward will direct his unaccompanied double 
chorus ‘‘ The Word of the Lord giveth life.” Mr. J. W. 
Phillips will play Handel’s Organ Concerto (No. 2). The 
principal vocalists are Mesdames Ella Russell, Medora 
Henson, Ada Crossley, B. Burrell; Messrs. Ben Davies, 
Herbert Grover, Santley, Plunket Greene, David Bispham, 
Arthur Barlow, and Master Sterndale Bennett

At the Winter Concert of the Sheffield Amateur Musical 
Society Gounod’s ‘‘ Redemption ” will be performed

Tue Middlesbrough Musical Union will give as usual 
three Subscription Concerts. On December 17, a Jubilee 
performance of “ Elijah,” with Mr. Santley ir his famous 
interpretation of the part of the Prophet ; the other soloists 
engaged being Miss Maggie Davies, Miss Hannah Jones, 
and Mr. Edward Branscombe. On February 24 the 
Committee will make what is considered a new departure, 
and in place of the usual oratorio or cantata give 
Schumann’s beautiful music to Byron’s ‘‘ Manfred.” The 
work contains a considerable amount of recitation with 
music and for this purpose the services of Mr. Charles Fry, 
who has made a special reputation in this branch of 
elocution, have been retained. For the third Concert, on 
March 31, Dr. Joachim, Miss Maggie Davies, and Signor 
Piatti have been engaged, and the choir of the Society 
will perform a selection of glees and part-songs. Mr. 
Kilburn will, as usual, be the conductor

THE most important of the various provincial Festivals 
to come is that of Norwich (details of which have already 
appeared in our columns), which begins on the 6th inst, and 
ends on the oth inst. The North Staffordshire Festival will 
be held at Hanley on the 29th and 30th inst., and for which 
are in rehearsal a new work by Mr. Edward Elgar, entitled 
** King Olaf,’ Mendelssohn’s ‘‘ Hymn of Praise,” Barnby’s 
“The Lord is King,’”’ Beethoven’s ‘‘ Mount of Olives”’ and 
Choral Symphony, and Dvordk’s ‘‘ Spectre’s Bride.’? The 
principal artists engaged include Madame Ella Russell, 
Madame Medora Henson, and Messrs. Lloyd, Ben Davies, 
Andrew Black, and Ffrangcon-Davies. Dr. Swinnerton 
Heap will conduct. The selection for the Cheltenham

Festival, to be held from November 3 to 5, comprises 
“The Golden Legend,” “Elijah,” and a new cantata, 
“Morning,” by Dr. F. Iliffe

HAMBURG.—The performances at the Stadt-Theater, 
under Herr Pollini’s zealous management, have now been 
actively resumed. As the first novelty of the season a 
new dramatic opera (whatever may be implied by the term), 
by Herr W. Freudenberg, entitled “ Johannis-Nacht,” was 
announced for performance on the 2oth ult. Herr Ignaz 
Brill’s new opera ‘‘ Gloria” is also to be produced here 
for the first time on any stage

Lerpzic.—Mr. Henry Field, a gifted young pianist, 
pupil of Professor Krause, gave a Recital here on the 3rd 
ult., before a specially invited audience, and created a 
highly favourable impression in pieces by Beethoven, 
Weber, Chopin, and Liszt. Mr. Field is a Canadian by 
birth.——The performance by the Riedelsche Gesangverein 
of Handel’s oratorio ‘‘ Deborah,” in Dr. Chrysander’s 
version—postponed last spring on account of the illness of 
Dr. Kretzschmar, the conductor—is to take place on the 
18th inst., and will be followed, probably in November, by 
that of “Hercules.” These performances are looked 
forward to with much interest here as marking a new 
departure in the treatment of Handelian scores from a 
critically retrospective point of view

LieGnitz.— Musik-Director Bilse, the once famous 
orchestral conductor, has just celebrated his eightieth 
birthday in this town, where he has lived in retirement for 
some years past. He was amongst the first to institute 
classical concerts at popular prices both in Berlin and in 
periodical tours throughout Germany.——Herr Ludwig 
Heidingsfeld, well known as the composer of several 
symphonic poems, for the last twelve years conductor of 
the Liegnitz Sing-Akademie, has accepted the post of 
director of the Philharmonic Society in Dantzig

President: Sir ARTHUR SULLIVAN. 
Conductor: Dr. CHuRCHILL SIBLEY

Arrangements are now being made for the Oratorio Season of 
1896-97, and performances will take place on Sunday Evenings at 
Queen’s Hall, Langham Place, W., Holborn, Battersea, Bermondsey, 
Shoreditch, and Stratford Town Halls, &e. he works to be per- 
formed will include : ‘Golden Legend,” “Light of the World,” “ The 
Prodigal Son” (Sullivan); ‘“ Redemption” ,(Gounod) ; “ Messiah,” 
“Samson,” “Judas Maccabeus, " “Tephtha” (Handel): _ Creation,” 
“ The Seasons ” (Haydn); “ Dream of Jubal” (Mackenzie); ‘“ Elijah,” 
“Ss, Pasi,” “Lobgesang,’ ’ “Athalie,” “Lauda Sion’ (Mendels- 
sohn); ‘ Mount of Olives” (Beethoven); ““Stabat Mater” (Rossini) ; 
“Rebekah ” (Barnby); “ Jerusalem” (Turpin) ; “ Light of Life,” Elgar

All music required is provided. No Subscription payable, 
P oe every week. Choral, Tuesdays; Orchestral, Wednes- 
ays, 8 p.m 
Ladies and Gentlemen with knowledge of music are invited to join 
the Society

Tuespay Morninc.—‘ ELIJAH ” (Mendelssohn

Tuespay EveENING. —“THE GOLDEN LEGEND” (Sullivan); 
Overture, “In Autumn” (Grieg); Duet from “Der Fliegende 
Hollander " (Wagner); Double Chorus, ‘‘The word of the Lord 
giveth life,” from the ‘“ King's Error” (Coward); Symphony, No. 6, 
in F (Pastorale) (Beethoven

WEDNESDAY MorninG.—Overture, “‘ Paradise and the Peri” (Stern- 
dale Bennett); JOB” (C. H. H. Parry), conducted by the Composer ; 
Symphonic Poem, ‘Le Rouet d’Omphale” (Saint-Saéns); “ Eliza- 
beth’s Prayer” (‘‘ Tannhauser”) (Wagner); Organ Concerto No. 2, 
B flat (Handel); Symphony (‘‘ From the New World”’) Dvorak

NORTH STAFFORDSHIRE MUSICAL 
FESTIVAL

President: THE Duke oF SUTHERLAND. 
October 29 and 30, 1896. 
Tuurspay Eventnc.—‘Hymn of Praise,’’ Mendelssohn; ‘ The 
Lord is King,” Barnby; “ Mount of Olives,’ Beethoven. 
Fripay Morninc.—" King Olaf,” Edward Elgar (specially written), 
and Miscellaneous. 
Fripay Eveninc.—‘ The Spectre’s Bride,’ Dvorak; ‘ Choral 
Symphony,” Beethoven. 
Miss Ella Russell, Miss Medora Henson, Miss Marie Hooton. 
Mr. Edward Lloyd, Mr. Ben Davies, Mr. Andrew Black, 
Mr. Ffrangcon-Davies. 
Conductor: Dr. Swinnerton Heap. Chorus-master: Mr. Fred. 
Mountford. Solo Violin: Mr. Willy Hess. 
Programmes may be had from the Hon. Gen. Secretary, Mr. H. J. 
Wildin, Stoke-on-Trent

CONTENTS

