wit teen 8 : 8 | tec hnical interpretative mastery complete 

 Beethoven said : 4 
 jeal j ' | impress such"musicians Liszt 

 , , playec organ great deal yout | von Biilow “ 7 IGsat ’ wrote 

 instrument virtuosi . |played . _ probably ft icouldn’t , 

 organ Beethoven day clumsy | Best rival banc . 7 — — 
 aflair , vivid recollections | Schneider , Dresden ; ‘ ho | pee 

 early struggles example the| Statham , capital judge heard Best 

 conversation , end life 

 deserves handsome tribute paid | 
 Beethoven . - day , organ 
 richer resource far easier handle , 
 - rate recitalist probably uses mental 
 physica ! energy performer , 
 additional strain having adapt 
 fresh set conditions ( tonal scheme , touch , 
 acoustics , stop - positions , & c. ) instrument 
 plays . notorious fact , 
 fine performance , meets far } . . . merely dry facts appoint- 
 recognition pianist violinist | ments , slightest attempt real 
 ual , inferior , quality . * estimate extraordinary unequalled 
 “ ; wre : powers executant master organ effect . 
 reasons , centenary birth 
 greatest organists age hand , Thalberg—'a mere piano 
 country calls special notice , way | yirtuoso’—was accorded far longer fuller 
 protest lack appreciation ) article : 
 — = concert - player . , . Best fully equal Thalberg 
 biographical facts concerning Best } 1 instrument , better 
 easily accessible need - round musician , fuller knowledge 
 article . use and| _ art . 
 interest glance achievements 

 tibia considering Best attainments player , 
 Personamny . " pass , taken granted , keyboard 
 technique , concentrate interpre- 
 | tative , especially registration — department 
 | organ playing , performer 
 | transcriber , exercised influence . 
 Concerning manual technique , , 
 | , pointed - rate 
 : pianist . Mr. Statham relates heard 
 proverb , man teach | } ; fas Oe ree “ ge 
 nim play pianoforte evening , 

 Inevitably skill registration affected 
 playing Bach direction 
 generally approved . edition Bach 
 - registered , result apt 
 unsatisfactory , _ treatment 
 comfortably manageable . Statham says 
 best playing Bach , gives instances 
 particularly striking performances . Putting aside 
 question registration , matter 
 widely different opinions allowable , 
 doubt playing Bach gained enormously 
 characteristic impressed 
 saw heard play — , 
 uncanny self - possession nervous control . , 
 marvellous technique , 
 playing long work Bacha 
 memorable experience . accuracy , Statham 
 says , heard play 
 hundreds times , remembered hearing 
 mistake 

 playing Beethoven Funeral March 
 flat Sonata memory , bar 
 7rio played /vemolando 
 quarter bar instead half - bar . saw 
 shake head , set right bar 

 remarkable control steadiness playing 
 long difficult works ascribed 
 peculiar source . Best extraordinarily slow 
 pulse — slow understand 
 subject article Zawcet late 
 Dr. Charles Hayward , attended Best medically . 
 Dr. Hayward gave rate Mr. Levien , 
 mislaid note . Mr. Levien tells 
 Dr. Stanley Mellville ( President Royal 
 Society Medicine ) , knew Best playing , 
 pianist ( having , way , 
 accompanied Santley ) , suggests 
 slow pulse great deal Best 
 steadiness absence flurry ‘ nerves ’ 
 long exacting performances 

 sense motion music attained 
 skilful use varying harmonies . Easily assimilated 
 harmonies , judiciously mixed compli- 
 cated chords , sense motion reason 
 clearly marked differences . know exacily 
 fast music going 
 distinguish separate harmony . 
 harmony ‘ invention , ’ borrow 
 phrase half - witted philosopher , 
 White Knight , musical language 
 heavy _ stilted ponderous 
 Johnsonese , lengthy burst speed 
 possible . ‘ palpitating 
 whirling joy - molecules ’ feeling 
 animation , suggest speed 
 whirling palpitating gnats noisome 
 water stagnant pond . frequent shakes 
 ( puissant , radieux ) .and ejaculatory snippets 
 sound , beloved Scriabin , verti- 
 cally rising sparks belch forth 
 chimney traction - engine , proving 
 engine certainly going , slow rate 
 cumbersome wheels progress 
 scarcely perceptible 

 people Scriabin merely 
 composer , “ 4e composer . pianoforte 
 compositions spoken ‘ distinct advance 
 predecessors . ’ ‘ Think ! 
 merely pianoforte pieces written 
 diflerent point view , different indi- 
 viduality , distinct advance . Think 
 Bach Preludes : tragic nobility 
 B flat minor ; gaiety C sharp 
 major . Scriabin work distinct advance 
 . Think Beethoven Sonatas : * 
 Sonata flat , Op . 26 ; ‘ Appassionata , ’ 
 Op . 57 : ‘ Hammerclavier , ’ Op . 106 ; 
 Scriabin work distinct advance . 
 told Scriabin tenth Sonata 
 contribution instrumental music 
 greatest importance Beethoven . 
 Ponder ! Schumann Carnaval , Etudes 
 Symphoniques ; Chopin Ballades , Etudes , Polo- 
 naises ; Brahms Variations Rhapsodies — 
 works , great , dwindle 
 insignificance Scriabin tenth Sonata . 
 told orchestration eclipses 
 contemporaries ; Schénberg , 
 Stravinsky , Strauss — masters , world 
 acknowledges — handled orchestra 

 alth 
 : 
 sub 
 
 wou 
 
 find 
 Poe 
 peri 
 
 thir 
 
 wha 
 grea 
 con : 
 thos 
 1 

 proceeds Mace 
 Abbé spoke truth , ‘ music 
 produces results - day heard 
 proper conditions . ’ better reasoning 
 needed convince 
 art reached culminating point nearly 

 years ago , long Bach Beethoven 

 True , Mr. Dolmetsch brings forward 
 1925 

 summit pure music’—the String Quartets 

 Mozart Beethoven placed good way 
 slope , presumably . Advocacy 
 unbalanced sort good , 
 early English composers music 
 beginning appreciate — reservations . 
 certain forms music perfection 
 undoubtedly achieved centuries ago ( statement 
 imply perfection 
 reached ) . instance , perfect 
 examples melody produced palmy 

 days plainsong , peak reached 

 September.—The understanding music comes 
 time . years ago K. A. Islavin played 
 Chopin perfectly new , 
 obscure 

 6 September.—In art chief thing some- 
 thing new — . great 
 artists distinguished . Mendelssohn 
 — music beautiful , like everybody . 
 necessary . 
 Beethoven — like , possesses 
 quality . know advance going 
 

 29 July , 1909.—Do remember Schopenhauer 
 said music ? listen music 
 remained unsaid , 
 unsatisfying . perfectly true 

 kept detailed Diary . 
 relate music Yasnaya Polyana 

 2 ? December , 1907.—This morning Sibor ( violinist ) , 
 Bukinik ( violoncellist ) , Goldenweizer played . 
 evening concert . played Mozart , 
 Haydn , Beethoven , Arensky . Tolstoy liked Mozart , 
 Haydn . said : ‘ particularly love 
 Haydn . modesty — artistic self - denial 
 author . ’ 
 specially pleased Aiva / e , Hungarian 
 melodies heard . ‘ think Haydn Croatian 

 origin , lived Croatians , use 
 folk - tunes . ’ Goldenweizer confirmed . 
 listening Beethoven , Tolstoy remarked 

 remained tranquil admired artists ’ per- 
 formance . ’ present began enthusiastic 
 Beethoven , said : ‘ think 
 pretending . Mozart , Beethoven 
 began combine inner content external 
 effects , followers external effects 
 left 

 Arensky Trio pleased . Sibor 

 ones 

 admirable things poor 
 know Beethoven poor things . 
 public cries Beethoven 
 wrote 

 evening Tolstoy said : 
 Old , know define 

 Views Tolstoy . ,—Ep1Tor 

 Bradford Festival Chamber Music ( October 5 
 6 ) holds attractive scheme . 
 concerts day , 11.15 3 ( hour interval ) 
 7.30 9.30 . works announced include String 
 Sextets Brahms Schonberg ( ‘ Verklarte Nacht ’ ) ; 
 Pianoforte ( Quintets Elgar , Fauré , Franck ; Bax 
 Quintets oboe strings , harp strings ; 
 Ravel Introduction Allegro harp , string quartet , 
 flute , clarinet ; String ( Quartets Beethoven , 
 Franck , Dvorak , Goossens , Debussy , & c. players 
 Virtuoso String ( Quartet , William Murdoch , Gwendolen 
 Mason , Leon Goossens , Haydn Draper , Robert Murchie , 
 James Lockyer , Ambrose Gauntlett . Information 
 tickets , & c. ( early application necessary ) , 
 hon . secretary , ( Jueen Hall , Bradford 

 

 composers 
 said position musical world 
 suffered popularity . 
 case Mendelssohn , extreme popularity 
 ‘ Songs Words , ’ ‘ Elijah , ’ instance , 
 having blinded eyes musical world 
 beauty ‘ Hebrides ’ Overture , chamber 
 music , best works . Particularly 
 case Grieg . Grieg generally 
 regarded composer ‘ Peer Gynt ’ 
 Suite negligible trifles pianoforte 
 heard evening thousand suburban 
 drawing rooms , important works , 
 greater real beauty , 
 considerable danger total neglect 

 Grieg suffered popularity 
 music ; suffered 
 foolish remarks so- 
 called critical works , remarks generally 
 taking form blaming 
 , , known 
 Grieg successful small forms 
 large , short pieces criticised 
 sonatas symphonies . 
 sensible procedure criticising Beethoven 
 Symphonies short lyric pieces . 
 Parry loftily remarks ‘ intellectual 
 processes concentrated development 
 [ Grieg ] line . ’ statement entirely 
 true ( knowledge ‘ Holberg ’ 
 Suite , ‘ Autumn , ’ movement Sonata 
 Op . 45 , works , easy doubt ) , 
 fair reproach composer 
 sympathies largely shorter ’ 
 lyric formsthan symphony . 
 desirable qualities musical composition 
 ‘ intellectual processes concentrated 
 development , ’ qualities , 
 gift melody , harmonic originality , rhythmic 
 interest present great degree Grieg music 

 , assumed big 
 canvas great works art 
 formed , assumed 
 nobility grandeur utterance essential 
 qualities great music ? Let nobility 
 means , indispensable quality good 
 music . Scarlatti attempted nobility , 
 search Couperin Rameau 
 easily find . men honourable 
 place composers . , turning Bach 
 French Suites , beautiful 
 wrote ? surely music room 
 miniature forthe symphony , blame 
 composer miniatures composer 
 symphonies clearly cardinal error 

 Spirit Music : find to|to playing reproduction , praise . 
 share . Edward Dickinson . Pp . 218.| Schubert convert , 
 Charles Scribner . surely lovely work . worth stack 

 Euvres inédites de Beethoven . ’ Published an| - rated songs yearnings tears 
 Introduction Georges de Saint - Foix . Publica- | ( L1751 - 4 ) . 
 tions Sociétié Francaise de Musicologie.| Salisbury Singers unequal 
 Vol . 2 , 30s . London : Harold Reeves items recorded . 3975 . ‘ phrasing 

 Vom Volkslied bis zur Atonalmusik . ’ Franz ] short - harmonized version Ivimey 
 Landé . Pp . 69 . Leipsic : Verlag von Carl|/‘Drink . ’ ending drawn 

 contrapuntal setting words anonymous 
 writer . edited Noel Ponsonby . 
 Included publishers ’ series unison 

 Holst short Festival Te Deum , ‘ Peace , ’ 
 Parry ‘ War Peace , ’ Ethel Smyth ‘ Canticle 
 Spring , ’ Charles Wood ‘ Dirge 
 Veterans , ’ Beethoven Mass D , ‘ Elijah , ’ 
 ‘ Messiah , ’ London Symphony Orchestra 
 engaged . Sir Herbert Brewer 
 Dr. Percy Hull share organist duties . 

 following singers : sopranos — Beatrice 
 Beaufort , Agnes Nicholls , Dorothy Silk , Elsie 
 |Suddaby ; contraltos — Muriel Brunskill , Astra 

 Desmond , Olga Haley , Millicent Russell ; 
 /tenors — John Coates , Edward Roberts , Steuart 
 | Wilson ; baritones basses — Norman Allin , 
 | Howard Fry , Herbert Heyner , Robert Radford , 
 | Horace Stevens 

 Regularly , midsummer war , lying 
 rumour told 
 |*Proms , ’ regularly August found 
 peculiarly London institution confounding 
 | prophets . present season opens August 14 , 
 | concerts run pretty usual lines , 
 | evenings allotted 
 ‘ years past . programme book arrives near 
 |our press day detailed discussion , points 
 catch eye . , Beethoven 
 | Symphonies played chronological 
 order upside , beginning . 8 ending 
 } No.1 . Ninth omitted — odd decision , 
 surely . frequently choral 
 | Finale exception taken 
 |truncated method . . 1 
 |omitted , interest little 
 |historic . pleasant slow movement 
 | average audience wants hear - day , 
 | Ninth rarely performed . 
 | point noted organ solos 
 |dropped . presume economic 
 reasons , question popularity 
 cause , 
 |matter regret . general 
 | musical public welcomed prospect organ 
 | taking recognised place concert solo instrument 

 - songs , edited Martin Akerman , E. T.| ( America France ) , 
 Chapman setting ‘ Let world | reason opportunity given hearing 
 corner sing , ’ C , S. Lang carol , ‘ Tres Magi de | greater ( eventually , , 
 gentibus , ’ words translated Latin text , 15th | familiar ) works Bach , played proper 
 century . apparently written school| instrument . present non - churchgoing musician 
 use . simple , dignified setting ) rarely hears save transcribed form , 
 treble men voices , mainly unison - | inevitably robs certain qualities effects 
 harmony . carol , composer written organ supply . , 
 stately tune unison voices , brief refrain | renovated instrument Queen Hall 
 close verse - chorus . | Jittle mere tap supply / fgioso 
 effectively - written works provided backgrounds Largos , Solemn Melodies , Bach- 
 excellent organ . G. G. | Gounod concoctions 

 requires totally different type hall , audience , 
 atmosphere ; thought , , ordinary 
 ‘ Prom ’ habitué driven away . 
 think objections . , 
 ‘ Proms , ’ Symphony Concerts , 
 violin pianoforte solos ( intimate 
 character chamber music ) frequently heard , 
 suffering appreciably large hall . 
 contributor ‘ Feste ’ pointed years ago 
 advocating introduction occasional 
 chamber work orchestral concerts , music 
 provides far better relief vocal operatic aria 
 orchestra plays prominent , 
 rule superior quality . 
 fact , long symphony concert ear 
 surfeit orchestra , real need 
 short items band . . believe 
 success English Singers season 
 ‘ Proms ’ delightful singing , 
 refreshing contrast provided . 
 orchestra gains relief , 
 course . ( way , English Singers 
 appear year programmes 

 Mr. Midgley fear Promenaders 
 driven away chamber music , 
 depends choice work . 
 suggest , example , later Quartets 
 Beethoven ; advisable play com- 
 plete works , short . detached 
 movements symphonies played , ought 
 little objection use separate movements 
 quartets . Léner players long 
 habit making gramophone records 
 detached movements . point 
 considered . average orchestral audience 
 knows little chamber music ; opportunity 
 making converts , eventually increasing 
 public highest types music 

 matter 7.30 8 organ recitals , 
 company Mr. Lorenz , especially 
 suggests organists gladly 
 work merely nominal fee . recital given 
 crowd assembling slight 
 instrument player . organ solos 
 heard concert - room , 
 accorded recognised place programme , 
 performer paid adequate fee 

 subtlety , variety , skill beaten hollow | 
 countless passages composers Brahms 

 Beethoven ( especially chamber music 

 Going choice illustrations : Sir 
 Landon doubt pleased good 
 listeners playing ‘ Blue Danube ’ Waltz . 
 plenty Ronaldites found old 
 Waltz decidedly worn . best said 
 excellent dance . 
 , Waltz Irving Berlin 
 Mr. Hylton hit far worse , ( 1 ) 
 poor dancing purposes ; ( 2 ) sickeningly 
 monotonous unwholesome style ; ( 3 ) 
 Pianissimo attenuated couple 
 waltzing drowned . 
 fox - trot Mr. Hylton said 
 impossible toes , , far 
 circle listeners concerned , 
 dead failure . Nary toe stirred — couple 
 moved doorwards fox - trot 
 hiccupped way minute 

 ROYAL ACADEMY MUSIC 

 summer term , academic 
 year , busy , addition customary 
 youtine work concerts , added 
 excitements examinations opera week . 
 students ’ chamber concert , June 24 , notable 
 excellent string - quartet playing movement 
 Brahms Quartet minor , Op . 51 , . 2 . 
 doubt general standard chamber - music 
 performance Academy moment high , 
 particular instance ensemble 
 sedulously maintained , tone conception 
 performance exceedingly sustained 
 beginning end . good , , Beethoven Quintet 
 E flat , pianoforte wind instruments , 
 pianoforte exceptionally played . word praise 
 horn player clever management 
 somewhat unruly instrument . ex - student , Miss 
 Dorothy Howell , represented Phantasy 
 violin pianoforte . pleasantly played , 
 long . Young composers , matter 
 mature years , idea 
 ‘ Brevity soul wit.2 MS . songs 
 student , Mr. Norman McLeod Steel , victim war 
 loss sight , pleasing , fact 
 betrayed imagination — unusual asset 
 song - writers . Compact point tiny 
 songs string quartet accompaniment , Miss Olive 
 Pull . intelligently sung Miss Barbara 
 Pett - Iraser 

 students dramatic class gave perform- 
 ances June 25 26 , ( Good acting shown 
 quaintly - named playlet , ‘ pass lentils 
 boil , ’ especially outstanding Boy . young 
 lady played distinct fair stage , 
 diction good , knows 
 . ‘ Wonder Hat ’ 
 acted , interest sustained , 
 said ‘ Queen Cornwall ’ better . Young 
 amateurs , amateurs , fitted play 
 tragedy , generally succeed making 
 play ridiculous 

 739 

 Mr. Solomon reduced pianoforte - playing certain 
 pellucid perfection note passion . 
 recital Wigmore Hall chose stereo- 
 typed programme , beginning Busoni arrangement 
 Bach Chorale , * ‘ Wachet auf , ’ spirited 
 sparkling D major Sonata Haydn . came 
 Beethoven Sonata , Op . 31 , . 3 , Schumann 
 * Carnival , ’ character brilliance lacked 
 warmth abandon 

 Miss Ania Dorfmann , brilliant , use 
 exaggerated rubato destroyed flowing movement 
 Brahms - Handel Variations Schumann ‘ Carnival ’ ; 
 Scarlatti Sonata Gavotte Arne 
 admirably rhythmical charm 

 Miss Jessie Hall decided personality , keen 
 sense rhythm tone - colour enable 
 spirit character plays . programme 
 -Eolian Hall light . Beginning Bach 
 French Suite E , went Schumann Etudes 
 Symphoniques , * Waldstein ’ Sonata , Prelude , Fugue , 
 Variation Franck - Bauer , ended Albeniz 

 Josef Hofmann great master craft , possessing 
 rare power filling audience new life 
 vigour . _ appeared twice recently — time 
 violin pianoforte recital Madame Lea 
 Luboschutz , fine artist , played 
 Sonatas — Grieg F , César Franck , ‘ Kreutzer ’ 
 Beethoven — old friends delighted 
 hear . fine style perfect ensemble 
 memorable . recital great opening 
 rarely - heard Schumann Sonata F minor 

 notable feature Miss Youra Guller 
 recital “ Eolian Hall exceptionally pure , delicate 

 Music Provinces 

 BIRMINGHAM,—The complete set programmes 
 symphony concerts given City Orchestra 
 season hand . day concerts 
 altered Tuesday Thursday , better 
 arrangement people outside towns 
 Thursday early - closing day . concert 
 series held Central Hall , 
 seven place usual Town Hall , 
 present undergoing reconstruction . general scheme 
 series fairly enterprising , include 
 work ultra - modern school likely frighten 
 timid concert - goer . principal items 
 programme Beethoven Pianoforte Concerto . 5 , 
 E flat ( ‘ Emperor ’ ) , Mr. Harold Bauer soloist , 
 Schubert ‘ Fierrabras ’ Overture , ‘ Enigma ’ 
 Variations . movements Holst ‘ Planets ’ 
 promised November 11 , Mr. Albert Sammons 
 play Elgar Violin Concerto B minor . 
 concert Bruno Walter conductor 
 instead Mr. Boult , , , conduct 
 programmes . great C major Symphony 
 Schubert fine performance Herr Walter 

 given evening . December concert , 
 Mr. Harold Samuel play Brahms rarely - heard 
 Pianoforte Concerto . 2 , B flat , 
 Triple Concerto minor , pianoforté , violin , 
 flute . Mozart Symphony . 38 , D ( ‘ Prague ’ ) , 
 figures programme . concert includes 
 novelty shape Mahler Symphony . 4 , G , 
 little music composer heard 
 Birmingham , event awaited special 
 interest . work requires soprano soloist 
 movement , Miss Dorothy Silk called 
 fill réle . Dukas ‘ L’Apprenti Sorcier ’ included 
 programme . Mr. Johan Hock , - known 
 ’ cellist Catterall Quartet , appear sixth 
 concert , play Cello Concerto Schumann , 
 , presumably , Don Quixote 
 performance Strauss symphonic poem , 
 unknown Overture Weber , ‘ Ruler 
 Spirits , ’ special interest , Debussy 
 * L’Aprés - midi d’un Faune ’ completes programme 
 suit musical tastes . Composers varying 
 styles represented programme 
 concert — Brahms , Mozart , Bantock , Vaughan Williams , 
 Delius appear turn . Delius symphonic poem , 
 ‘ Paris , ’ novelty , heard 
 interest love music composer , 
 Miss Jelly d’Aranyi soloist occasion , 
 play Mozart Violin Concerto . 4 , D , piece 
 Vaughan Williams , ‘ Lark Ascending . ’ Bantock 
 ‘ Dante Beatrice ’ Brahms ‘ Tragic ’ Overture 
 heard . chorus Festival Choral Society 
 help final concert season , 
 celebration Beethoven centenary . * Egmont ’ 
 Overture , songs ‘ Egmont , ’ big Choral 
 Symphony given . Sunday concerts 
 held season West - End Cinema , 
 seating accommodation . Children concerts 
 given usual , series Lunch - hour concerts 
 arranged 

 BRADFORD.—The programme coming Festival 
 chamber music ( October 5 6 ) announced , 
 programmes include Brahms Sextet Clarinet 
 ( Quintet , Schénberg ‘ Verklarte Nacht . ’ Bax Oboe 
 Harp ( uintets , Elgar Pianoforte ( uintet , Debussy 
 ( Quartet , second Violin Sonata Delius , Phantasy 
 Quartet Goossens , Ravel Introduction Allegro 
 strings wind , works Mozart , Beethoven , Franck , 
 Dvorak , Fauré , principal artists 
 Virtuoso Quartet 

 BRIGHTON.—The Charlier Quartet Liége played 
 Pavilion July 2 3 , programme including 
 ‘ Selection Old Style ’ works Grétry , 
 Quartet C sharp minor Rogister , Serenades 
 Joseph Jongen 

 HOLLAND 

 Utrecht Municipal Orchestra maintains 
 standards refusing lay instruments 
 summer . week June , regular 
 conductor , Evert Cornelis , players gave excellent 
 Summer Music Festival , programmes 
 combined happy measure classical modern . 
 Handel ‘ Water , Music , ’ Beethoven * Ninth , ’ Mozart 
 * Haffner ’ Serenade , Mahler * Lied von der Erde , ’ 
 shorter works composers , followed 
 appropriately Debussy Nocturnes , 
 Chausson ‘ Chant Funébre ’ ( orchestrated d’Indy ) , 
 Honegger ‘ Cantique de Paques , ’ songs 
 Rimsky - Korsakov Moussorgsky , sung 

 Vera Janacopulos . spite excellent singing , 
 artist wearing welcome 
 numbers . Lotte Leonard visiting 
 vocalists . pleasing record Dutch singers — 
 Suze Luger , Louis van Tulder , Thom Denijs — 
 respect worthy associates , orchestra 
 local branch Toonkunst played sang . 
 maintenance provincial orchestras difficult 
 problem , 
 unforeseen happens appears likely orchestra 
 Groningen , outpost needs musical help 
 given , resume activities 
 season . summer season Scheveningen started 
 bad weather visitors , programmes 
 thoroughly good , exciting , 
 contain number items seldom hears England . 
 sensation days ’ visit Paul 
 Whiteman band , critics public 
 alike quick ‘ new 
 methods ’ ‘ new ideas ’ simply old ones 
 prominent advertisement actual manner 
 performance , brilliant instrumentation repertory , 
 remarkable virtuosity players , novelty 
 tone - colour won favourable comment . 
 Publicly privately , , comment 
 lack newness piquancy rhythms . 
 actual novelties , ‘ Jiidische 
 Trilogie ’ Asger Hamerik , Danish - American 
 composer , group songs orchestral 
 accompaniment Leo Ruygrok , second ‘ cellist , 
 winter , second conductor Residentie Orchestra . 
 Hamerik work pleasant , richly scored miniature 
 Symphony , reminiscent thematic material 
 19th - century leaders orchestration chiefly 
 Wagner . long solo bassoon lightly accompanied 
 forms bulk middle movement , ‘ Lament , ’ 
 proved unusual highly effective . songs 
 Ruygrok , sung pure tone good expression 
 Madame Stotijn - Molenaar , evidently exercises 
 writing voice orchestra composer 
 master scarcely master art 
 combining . items outstanding 
 interest performed Concertino oboe 
 orchestra striking section 
 middle * ‘ Romance , ’ beautifully played J. H. Stotijn ; 
 Strauss ‘ sprach Zarathustra , ’ played 
 programme Wagner ‘ Faust ’ Overture Beethoven 
 C minor Symphony ; performances Strauss 
 * Don Juan ’ Tchaikovsky ‘ Romeo ’ Juliet ’ 
 Overture ; smaller items Alex Voormolen , Joh . 
 Wagenaar , Lekeu , & c. Georg Schnéevoigt 
 capable assistant , Ignaz Neumark , Residentie 
 Orchestra playing better . music 
 represented chiefly organ recitals , military band 
 concerts open air , higher 
 standard programme selection performance , 
 . Mention incidental 
 music outdoor plays , written local composers , good 
 example Emile Enthoven Utrecht 
 University Lustrum Feast play ‘ Ichnaton , ’ 
 spirit piece reflected arbitrary attempts 
 reproducing Egyptian instruments effects . 
 performed Municipal Orchestra , 
 Evert Cornelis . HERBERT ANTCLIFFE 

 VIENNA 
 PHE CRITICAL STATE OPERA 

