perform great success Madlle . Sophie Heilbron 

 LOUIS DUPUIS , 
 cathedral gem : 
 1 . Nantes.—Fantasia , subject Mozart 12th Mass 
 2 . bmg subject Weber Mass G 
 8 . RorrERDAM . — Fantasia , subject Haydn 8rd 
 ( imperial ) Mass ... < o 
 4 , Mataca.—Fantasia , subject ' Beethoven Mass 

 C 
 5 Roven . — Fantasia , subject Gounod Messe 
 Solennelle 
 6 . Cazn.—F antasia , subject Rossini ’ Stabat Mater 
 7 . Mayvence.—Fantasia , subject Haydn 1st Mass , 
 bflat .. aed wi ea os ese 
 8 . CoLoane.—Fantasia , subject Mozart 1st Mags , 
 C ase ose ese eco eve 
 9 . Panis.—Fantasia , ange Mendelssohn aacarne 
 Praise 
 10 . Lonpon.—Fantasia , " subject Handel ’ 8 Messiah ... 
 11 . Norwicn.—Fantasia , subject Haydn creation 
 12 , CanrerBuRY.—Fantasia , Spohr pant Hart ... 
 18 . Yorx.—Fantasia , Handel Judas Maccabeus " 
 14 . SaLispury.—Fantasia , Mendelssohn St. Paul " 
 15 . Weiis.—Fantasia , Haydn Mass b flat ( . 16 

 Burnett , Pezze , Ridley Prentice , Minson ; Miss Purdy . Quartett 

 b flat , Haydn ; Moonlight Sonata , Beethoven ; Sonata P.F. Violin 

 W. H. Holmes ; P.F. Quartet , Schumann . ticket , Guinea , 5 . , 
 2s . 6d . , 1 . , Mr. Ridley Prentice , 304 , Wimpole - street , W 

 éil mar " ( soprano solo Madlle . Tietjens ) 
 welcome item selection . piece follow 
 " Dalla sua pace , " Mr. Vernon Rigby achieve 
 merit success careful earnest singing , arid 
 " deh vieni " Madame Lemmens - Sherrington , composi- 
 tion admirably suited voice style . Mozart 
 feast appropriately terminate symphony G 
 minor , excessive beauty produce 
 effect hear conversation 
 Choir Festivals reserve 
 instrumental movement . second , include 
 overture " Masaniello , " finely perform 
 receive enthusiastic encore , contain showy song 
 , vocalist , addition 
 mention , Miss Alice Fairman , Mr. E. Lloyd , 
 Mr. Lewis Thomas , unanimous encore reward Mr. 
 Lloyd refined rendering Sullivan ' . " 
 second evening concert commence selection 
 Handel " L’Allegro " ' Il Pensieroso , " precede 
 overture " Alexander Feast . " allude 
 unsatisfactory effect perform portion work , 
 affording faint impression composer meaning ; 
 objection scarcely applicable Handel setting 
 text , supply Charles Jennens , 
 Milton word , piece arranged 
 work effect ofa " selection " " composition " 
 hear composer write . 
 occasion Robert Franz additional instrumentation 
 use , certainly brightness performance , 
 render work unusually interesting . piece 
 produce effect " sweet bird " ( Mr. 
 Radcliffe excellent flute obdigato ) , render purity 
 vocalisation Madame Lemmens - Sherrington ; ' hide 
 day garish eye , " Madame Sherrington , 
 consequence indisposition Madlle . Tietjens , 
 apology ; " mirth admit , " sing 
 Mr. Vernon Rigby ; " ' populous city , " effectively inter- 
 prete Mr. Santley , " haste thee , nymph , " 
 Mr. Lewis Thomas highly successful , arouse some- 
 apathetic audience infectious laugh . Haydn 
 " Surprise " Symphony perform beginning 
 second , solo principal vocalist 
 , Madame Lemmens - Sherrington ably supply 
 place Madlle . Tietjens . Mendelssohn noble March , 
 write honour painter Cornelius , " honour " 
 play people carriage 

 concert Thursday evening highly satisfactory , 
 Beethoven fine music " ruin Athens " 
 entirety . thoroughly dramatic composi- 
 tion like allow languish 
 obscurity year extraordinary 
 experience teach country audience 
 attract novelty thanworth . true 
 solid work art universally admit , glittering jewel 
 acknowledge worth wear fashionable society ; 
 produce priceless gem 
 lie unnoticed uncared 
 ? honour , , Worcester bring Beethoven 
 glorious music Festival audience , scarcely 
 believe consign comparative neglect . 
 composer having originally intend music 
 accompany representation Kotzebue play , obviously 
 impossible fairly judge true effect ; dramatic 
 power piece manifest , abstract 
 work genius , fairly rank Beethoven 
 composition , apart connection stage sur- 
 rounding , save music necessarily create 
 mind auditor . duet " faultless , hate , " 
 sing Madame Lemmens - Sherrington Mr. 
 Lewis Thomas , resolve develop numerous 
 beauty utmost power . air " Deign , 
 great receive excellent rendering Mr. 
 Lewis Thomas , somewhat trying interval 
 justness intonation high degree commend- 
 able . chorus , " daughter high throne Jove , " 
 carefully sing , wonderful chorus Dervishes ( 
 - demand ) energy , whirl 
 triplet accompaniment excellently play 

 626 

 MUSICAL TIMES.—Oorozer 1 , 1872 

 orchestra , melodious March Chorus , ' ' twine ye 
 garland , " Chorus ( bass Recitative ) , " susceptible 
 heart , " thoroughly satisfactory , elicit 
 warm applause . overture turkish March 
 band absolute perfection , point instrumenta- 
 tion bring utmost clearness . work 
 listen interest , frequent mark 
 approbation prove Beethoven 
 way , mixed audience inartistic 
 musical entertainment , " ' miscellaneous concert 

 rest vocal piece contain pro 

 iri 

 Offenbach Auber , certain cleverness 
 treatment composer study 
 effect . Mr. Cowen conduct overture , rea- 
 son complain reception . concert , open 
 Beethoven overture " Fidelio " contain 
 miscellaneous vocal piece calculate mixed audi- 
 ence . Madlle . Albani create genuine enthusiasm 
 singe , especially " ' Ardon gl‘incensi , " " Lucia , " 
 Madlle . Tietjens " softly sigh " usual success , 
 vocalist Madame Trebelli - Bettini , Messrs. 
 Lloyd , W. H. Cummings Santley , gentleman 
 delight connoisseur rendering excellent 
 oe Hatton , " Anthea " " voice Weste 

 Wind . " wale 

 MUSICAL TIMES.—OUcroser 1 , 1872 

 touch , excellent tone , phrase intelligence 
 true artist . reception enthusiastic , 
 right congratulate success 
 legitimately earn . welcome item 
 gramme melodious Andante Clarionet , 
 orchestral accompaniment , composition Mrs. Meadows 
 White ( Miss Alice Mary Smith ) , play 
 perfection Mr. Lazarus , receive favour 
 composer executant , well- 
 select vocal piece include concert , prin- 
 po ae singer Madame Florence Lancia ( place 
 lle . Tietjens , indisposed ) , Madlle . Albani , 
 Madame Patey , Madame Trebelli - Bettini ; Messrs. Santley , 
 E. Lloyd ( highly effective Beethoven ' ' Ade- 
 laida " ) , W. H. Cummings . overture ' Guil- 
 laume Tell " " Les Diamants de la Couronne " 
 excellently play orchestra , applaud 

 principal portion Thursday morning devote 
 performance Sir Julius Benedict Oratorio , " St. 
 Peter , " time Norwich , 
 excite interest , account popularity 
 work obtain , high ’ estimation 
 composer hold city . ex- 
 pectation form Oratorio 
 stranger merit , result presentation 
 St. Andrew Hall evidently satisfactory , 
 small applause , despite sacred- 
 ness work , permit performance 
 oratorio Festival , grow 
 strength salient point " St. 
 Peter " greet . Madlle . Tietjens sufficiently re- 
 cover voice sing soprano musie , includ- 
 e beautiful air , " mourn dove , " 
 render purity style tenderness 
 expression - demand , quartett , 
 " QO , come , let sing , " Madlle . Tietjens 
 . Madame Lancia come rescue , 
 sing trying solo , " gird thy loin , " 
 energy astonish know short time 
 totally unacquainted music . 
 encore follow graceful recognition in- 
 valuable service , Madame Patey repeat 
 air , " o , thou afflict , " good style , 
 fine quality voice tell usual 
 effect , suffer cold . 
 tenor song , ' " ' Lord pitiful , " « O 
 House Jacob , " receive utmost justice , 
 Mr. E. Lloyd second Mr , W. H. Cummings ; 
 Mr. Kerr Gedge mention having 
 render important service recitative . Mr. 
 Santley sing finely oratorio , especially 
 expressive solo , ' ' o head water , " 
 throw pathos feel listener . 
 high power choir appear re- 
 serve work , chorus 
 solidity , decision attack , careful balance tone 
 highly gratifying composer . 
 applause audience , , 
 degree repress , Sir Julius Benedict receive 
 perfect ovation , entrance , retirement , 
 orchestra . superfluons , opinion , 
 interfere effect ' St. Peter , " perform- 
 ance commence Handel " Occasional Overture , " 
 Madille . Albani sing ' angel bright fair " 
 scarcely religious feel warrant encore 
 effort reward 

 avid Sassoon , respect thoroughly worthy 
 high reputation eminent firm 
 peaboome 

 Tue Saturday Afternoon Concerts Crystal Palace , 
 commence 5th inst . , promise ww 
 usuallyinteresting . symphony Beethoven 
 perform chronological order , Sympho- 
 ny Haydn , Mozart , Schubert , Mendelssohn , Schuman , 
 Spohr ; Serenade D Brahms , & c. , & c. amo 
 work produce time concert 
 Symphonies e flat , Mozart ( 1773 ) , b flat 
 Schubert ( MS . ) , new MS , work orchestra ° 
 pen Sir W. Sterndale Bennett , compose ee 
 Crystal Palace . choral work broug ¢ forward 

 2 
 se 

 _ 

 include " St. Paul ’’ ( Mendelssohn ) , " Paradise 
 Peri " ( Schumann ) , " queen " ( Sir W.S. Bennett ) , Te 
 " ( Sullivan ) , & c. solo instrumental work 
 orme time concert Concerto 
 B flat Mozart ( ) ; Rondo B flat ( Beethoven ) 
 ( posthumous ) , Concerto D minor ( Rubinstein ) . 
 rmance , usual , able conductorship 

 Herr Manns 

 taste " ( Goss ) , carefully sing ; hymn 
 sermon 360 , sermon 325 , recessional hymn " Nunc 
 dimittis " ( Russell ) . service intone 
 Vicar , follow anthem Rev. J. Mason . 
 lesson read Rev. G. Langley , second Rev. G. 
 Packer . thesermon preach Rev. Canon Yard , rector 
 Ashwell , Ecclesiastes xi . 6 , ' morning sow thy seed , " & c , , 
 preacher enforce perseverance charity , especially 
 connection education . offertory nearly £ 14 , 
 devote St. John Sunday Schools . conclude 
 voluntary , finely play organist , Mr. E. J. Crow , Mus . Bac . , 
 Mozart ' ' splendente te deus 

 PENzANcE.—On Wednesday , 18th ult . , Matinée Musicale 
 ay St. John Hall Mr. Mrs. J. H. Nunn , excel- 
 ent selection music creditably perform 
 pupil large fashionable audience . instrumental 
 programme include Beethoven Serenade Trio Strings , 
 pianoforte Trio Haydn , Quintett Reissiger , Men- 
 delssohn variation Violoncello Piano ; 
 vocal piece song Smart , Hatton , Benedict , modern 
 composer . execution piece high order , 
 speak concert - giver ’ power training juvenile 
 advanced student 

 Port EvizapetH , CaPE Goop Horr.—On 12th July Mr. 
 Fred . Griffiths concert Town Hall benefit 
 Captain le Gallais , vessel wreck short time ago 
 coast . notwithstanding continuous downpour rain 
 occasion , , honour Port Elizabeth , 
 good attendance , receipt plainly prove . programme 
 entirely new audience , great feature intro- 
 duction english concertina , high degree 
 successful . effective piece programme 
 song " Clochette , " ' ' surprise , " ' Rock’d cradle 
 deep , " duet " o may’st thou dream ? " Mr. Griffiths 
 left - hand solo pianoforte admire deservedly 
 encore . - song male voice elicit warm 
 applause . mention , conclusion , 
 piece - demand 

 content volume 

 Beethoven 
 Andante supplichevole ... Himmel 
 Benedictus . 1st Mass , op . 77 Hummel : 
 BOOK X. 
 God high . chorale ... Mendelssohn 
 Kyrie eleison . Mass no.8 Mozart 
 andante grazioso e con moto . sonata 

 Op . 87 .. Fr . Lauska 
 pious orgy . Judas Maccabaus Handel 
 March . occasional Oratorio - . Handel 
 andante con moto . sonata Op.12 , Beethoven 
 poco adagio . 5th quartett eee Ha : 
 Organ piece — Larghetto ove Max Keller 
 Agnus Dei . Mass no.1 Mozart 
 Larghetto con espressione . 3rd Concerto 

 Dussek 

 jante ( 

 BOOK iv , BOOK vii . 
 Agnus Dei . mass . « .. G. A. Naumann | adagio cantabile . trio , op . 1 , no.1 
 Siciliano con espressione ooo - . long 
 Minuet . Saul ove seo Handel | Organ Piece — Andante ... Z- H. Knecht | 
 Largo . sonata ... = Haydn | Organ Piece — Un poco Adagio J. H. Kno 
 Larghetto . sonata .. eco Mozart | poco adagio affettuoso . sonata , op . 8 ] 
 Organ piece — Larghetto ... Max Keller G. F. Pin 
 Organ Piece — Andante ... « . Max Keller | Andante con moto . Divertimento , op . 88 
 Largo . sonata ... Defesch Weber 
 Serenade . op . 44 . « + »   Mendelssohy 
 Andante con espressione . Sonata Clement | 
 BOOK V. 
 Wiegenlied ... ose eee Spohr 
 Andantino eo + . Spohr BOOK viii . 
 ancient melody ( a. d. 1659 ) | Lawes 
 Air — Andante con espressione assai Rousseau | swedish melody ... 
 Andante , " enchantress " ... Weber | Andante . trio , op . 40 
 Allegretto , chorus . Les Deux Journées Sonntagslied = os " ea 
 Cherubini | Andantino grazioso . trio , op . 77 
 volkslie sie Weber | Schiénes Madchen . Jessonda 
 wir nahte mir der Schlummer . " Der Andantino , Sonata , op.45 .. " ite 
 Freischiitz Weber | romance ... humnd 
 Andante con moto . fantasia " Mendelssohn Organ Piece — Cantabile un poco adagio 
 ancient hymn , " Alla Trinita Beata " J. H. 
 Laudi Spirituali , a.p . 1545 spanish romance . op.8 Mendelssohg 
 BOOK vi . 
 BOOK ix . 
 romance . Euryanthe ove ee . Weber 
 Andante . trio , op . 22 es | Hummel | Andante . op . 12 oon 
 Organ piece — Andantino .. J.H. Knecht | lie ohne worte — Andante . op . 88 
 Organ piece — Vivace ... J. H. Knecht 
 Organ Piece — Andantino J. H. Knecht | Siciliano . quartett , op . 188 
 andante cantabile . trio , op.97 Beethoven | Andantino ove 
 ah ! fast joy ese Mendelssohn | Adagio . quartett , op . 58 
 lie ohne worte — Moderato . op . 19 Andante ... 7 oe 
 Mendelssohn | Volkslied . op . 47 ° oo 
 original Air . op.1 eee ee Weber | Aria — Andantino .. ave 
 content volume Ttoo . 
 BOOK xiii . BOOK xvi . 
 Larghetto 1 ) organ piece — Adagio . op . ali 
 Sostenuto con espressione . " Quartett Weat tho ' Pier Rhy ' _ ua 
 Op . 50 + . f , Kuhlau ! Andante . quartett , op . 18 Mendelssohn 
 Andante . sonata Op . 37 . D. Steibelt | Kyrie eleison . mass no.17   w. Mozart 
 Andante . symphony no.8 ... Mozart | March . overture Ptolemy Pe Eat 
 = ig * Berger . trio , op 68 pace Credo . 1st Mass 
 rgan Piece — Poco adagio ove ré | andante cantabil 
 ae omar David ° + » Handel — op . 8 — Jupiter Symphony Ma 
 enedictus . mass . 17 eco Mozart aheede uni " 
 dona nobis pacem . mass no.14 ... Mozart Op . ayes 3 egret . quartet , 
 ymn ove + » Donizetti | Organ Piece — Adagio ed - — -Biak 
 BOOK xiv . book xvii 
 Andante . 5th quartet , Op . 9 Mayseder |Atia — Siciliano .. = . 
 adagio maestoso . sonata Op . 20 need Largo . concerto , Op . 15 ae 
 Sanctus . Mass no.8 ae w. Mozart | Canzonet ... gee — 
 Hosanna , Mass no.8 ... ‘ e Mozart | Benedictus . requiem auaeinee 
 quoniam tu solus . mass no.12 ... Mozart | Hosanna excelsis . requiem 
 Lord , man ? semele . Handel | Pastorale 
 fac ut portem . Stabat Mater ... Rossini | 1 know Redeemer liveth 
 Larghetto . concerto , Op . 61 Beethoven pa sont — — Op . 4 " ‘ wendea 
 Benedictus . mass . 12 ooo Mozart Organ piece — Adagio . Julius André 
 BOOK xv . book xviii . 
 piece — Larghetto ee Max Keller | Quitollis . mass no.12 / ... » Mozart 
 et vitam venturi . mass no.8 « Mozart | etincarnatus . mass no.14 .. Moxart 
 Adagio . nonetto ove Spohr | Andante — Organ Fantasia ... ... Freyet 
 March , Palestine . Dr. Crotch | Organ Piece — Larghetto .- . Max Keller 
 Andante — _ cantabile ed espressivo Farewell . op . 50 ... Mendelssohn 
 sonata , op . 109 « . Beethoven ! departure . op.59 Mendelssohn 
 thou didst leave . Messiah Handel quoniam tu solus . mass no.8   .. Mozart 
 Kyrie eleison . mass 1 se Mozart | Andante . Quartett , Op . 4 ae Spohr , 
 Sanctus . mass . + . Mozart | achieve glorious work ... Hayda 
 Adagio . ean 1 Mendelssohn | organ Piece — Adagio ... din Bink 

 eethone 
 , 
 Ho 
 F , pin 
 p. 88 
 Weber 
 \delasohy 
 Clementi 

 edit , correct accord Original Scores , 
 translate English , Natatia Macraraen . 
 price 2 . 6d . ; handsomelv bind scarlet cloth , gilt 
 edge , 4s 

 ready . 
 BEETHOVEN FIDELIO 

 german english word . ) 
 great overture usually perform ; 
 Pianoforte Score publish agree original 
 score note sign phrasing 

