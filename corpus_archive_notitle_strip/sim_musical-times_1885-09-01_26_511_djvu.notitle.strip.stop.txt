Symphony — Dvorak D 

 Rhapsodie Hongroise F ( Lis ‘ t ) . CONCERT AGENTS , 
 Introduction 3rd Act “ Lohengri d agner ) . " ERP 
 Pageant March Chorus , “ Reine de Saba ” ( Gounod ) , & c. | — _ _ _ _ eee 
 THURSDAY MORNING.—FAUST ( Berl lioz ) . Vio YENIE 
 THURSDAY EVENING , Symphony , ninor ( Beethoven ) . | 2 0 ROUGH HACKNEY CHORAL 
 Overtures — ‘ Tannhiuser , ” “ ‘ Jube 7 T 
 Andante Finale Conce < B minor ( Handel ) . | ASSOCIA ION . 
 Finale “ Loreley ” ( Menc oe 1 ) , & e. | SEASON 1885 - 86 
 FRIDAY MORNING.—MESSIA | pee j 
 Vocalists : Madame ALBANI , Mit 3s , WILLIAMS , Mesdames | CONCERTS wi ! given SHOREDITCH TOWN 

 Tan ZY TREBELLI , Mr. E. LLOYD , Mr. J. Mz AAS , Mr. W.H. | HALL , M ane EVENINGS , 8 o'clock . 
 RCY , Mr. R. HIL TON , Mr. W ORLOCK , Mr. SANTLEY saductar Mr ERENE 
 Mr. Charles Hallé Band ‘ 99 Performers . Goaiater . ih SRR ESE PRUs , BP 

 Hon . Sec . , Colston Hall , Bristol . Fourth Concert , April 12 , 1886 , Handel DETTINGEN TE DEUM 

 Beethoven CHORAL SYMPHONY 

 IUFNELL PARK CHORAL SOCIETY 

 

 - known composer studied means 
 letters musical scores . note . 
 worthy items named Beethoven - called 
 , letter March 18 , 1827 ( 
 |days death ) , thanking Philharmonic 
 Society gift £ 100 ; 
 autograph scores Handel Oratorios Buc } king . 
 |}ham Palace . relics lace ruffle worn 
 Handel , mask Beethoven taken years 
 death , silver lock 
 hair 

 said pictures prints relat- 
 ing music imposing point numbers , 
 hang walls ithe vast majority 
 come sources . Taking Great 
 Britain , enormous quantity 

 MUSICAL TIMES.—Septemper 1 , 1885 

 character words . words ‘ ‘ Mortuos cum stare 
 jubes , ” fugue started basses - marked sub- 
 ject , ample development ( noticeable point 
 presentation subject augmentation bass ) 
 suddenly arrested supertonic chromatic harmony , 
 leaves ear suspense long pause . 
 original baritone solo recurs minor , 
 figure accompaniment ; key 
 changing tonic major , solo , united 
 chorus , continues conclusion , appropriately 
 calm suggestive hopeful resignation . Mr. F. King 
 sang solo Hymn true artistic feeling , 
 choir respect thoroughly efficient through- 
 , Composer called forward 
 deservedly applauded end performance . 
 Orchestral Selection ‘ Tristan und Isolde ’ ? 
 Beethoven * * Leonora ’’ Overture purely instru- 
 mental pieces ( magnifi 

 cently rendered ) ; Madame Albani , solo 

 prominent Society — Philharmonic Choral — 
 shaken trammels financial 
 embarrassment past season involved , 
 Committee firm decision place Society 
 safe unequivocal basis announcing 
 programme . public 
 naturally look tor support , marked re- 
 flection musical taste city Society 
 , past performances , earned claim 
 choral societies North England 
 allowed collapse simply want sufficient 
 monetary assistance enable pay way . Sucha 
 collapse , , scarcely contemplated , hoped 
 desired Guarantee Fund , & c. , soon ac- 
 quired . case , Society start Rehearsals 

 7th inst . , commencing “ Elijah ” thei 
 probable performance — high Society 
 having originally achieved rendering 
 Mendelssohn greatest oratorio . pretty certain 
 Philharmonic Choral Society assist Herr 
 Richter performance Beethoven Choral Symphony , 
 given Philharmonic Hall month 
 November 

 scheme Mr. Hallé series Orchestral Concerts 
 announced , Concerts doubt- 
 maintain high standard efficiency 
 marked past seasons 

 James 

 prospectus Borough Hackney Choral 
 Association , season 1885 - 6 , announces 
 Concerts given ; , pe 2 , 
 Schubert Mass F Prout “ Alfred ” per- 
 formed ; second , December 21 , Siekansis ’ s 
 ‘ * Rose Sharon ’’ ; , February 22 , 1886 , 
 Mendelssohn “ Elijah ” ; fourth , April 12 
 Handel Dettingen Te Deum Beethoven Choral 
 Symphony . orchestra , soloists , choir 
 usual scale completeness efficiency ; Mr 
 Ebenezer Prout retain post Conductor , office 
 filled years credit 
 benefit Society 

 Tue rg8th monthly Concert St. George Glee 
 Union took place 7th ult . , Pin lico Rooms , 
 Warwick Street , S.W. programme included Part- 
 Songs Glees Sullivan , Horsley , Michael Watson , 
 Martin , H. Smart , Purcell , extremely 
 rendered choir , directi Mr. 
 Joseph Monday . ‘ T’other day sat ” ( Sir John Goss ) 
 sung double quartet . Songs given 
 Miss Susetta Fenn , Miss Kate Flinn , Mr. H. G. Ryall , 
 Mr. Walter Joy , Mr. Charles Copland Mr. Theodore 
 Distin . Mr. F.R. Kinkee accompanist 
 contributed pianoforte solo 

 nae | 
 numbers publication , 

 original pieces editor . best 
 . 2 , asmoothly written melodious Romance | 
 D , . ro , ‘ * Schlummerlied . ” ’ impossible 
 favour arrangements , 
 regards selection pieces manner 
 task Drape carried . Tor 
 example , . 3 , March “ Fidelio , ” player 
 directed use organ , including 16 - feet trombone , 
 . 8 , Bridal March ‘ * Lohengrin , ’’ 
 symphony return principal theme | 
 marked organ ! LEven worse caricature 
 Minuet Mozart E flat Symphony ( . 11 ) . . 4 , 
 Largo Beethoven Sonata D , Op . 2 ; . 6 , 
 ‘ Evening Star ” song Tannhauser 
 better . curious want judgment 
 stop directions , numerous typographical errors 
 evidence editorial carelessness 

 Songs Corn Field . Cantata female voices , 
 accompa wniment pianoforte ( harmonium 
 harp ad libituin ) . poetry written Christina Rossetti . 
 music composed G. A. Macfarren 

 ublic , probably little changed 
 days Mozart wrote ‘ ‘ Cosi fan tutte . ” 
 Mozart humour 
 fancies frivolous libretto , time 
 elevate subject undying strains heaven- 
 inspired music 

 committee formed Vienna purpose 
 founding termed ‘ ‘ Beethoven Museum ” ( some- 
 analogous “ Mozarteum ” Salzburg ) 
 capital , Numerous offers Beethoviana 

 posscssors promoters 

 learn Italian papers Verdi recently 
 visited Arrigo Boito , found Maéstro busily 
 engaged new opera ‘ “ Jago , ” ’ , thought , 
 brought year , Milan 

 Mr. Walter Damrosch , New York German Opera , 
 recently visited Germany , completed engage- 
 ments principal singers institution 
 coming season , named 
 Mesdames Marianne Brandt , Lilli Lehmann , Herren 
 Fischer ( Dresden ) , Robinson , Kaufmann . 
 projected performances include ‘ Rienzi , ” ‘ ‘ Tann- 
 haiuser , ” ‘ * Lohengrin , ” ‘ * Die Meistersinger , ’ ‘ * Walkiire ” 
 “ Gotterdimmerung ’ ” ( Wagner ) ; ‘ Carmen ” | Bizet ) , 
 “ Aida ” ( Verdi ) , “ Fidelio ” ( Beethoven ) , ‘ Le Prophite , ” 
 ‘ * * Les Huguenots , ’ ‘ L ’ Africaine ” ( Meyerbeer ) , ‘ * Oberon ” 
 ( Weber ) , prominent operatic works . ambi- 
 tious programme , sure ! Herren Walter Damrosch 
 Seidl orchestral Conductors 

 fourteenth Festival German Choral Societies 
 successfully held second week July , 
 Brooklyn | U.S 

 WWwWWHR 

 No.1 . Slow Movement ( Quintet ) . ove eee e Schumann . 
 Minuet ( 12 Minuets ) .. di « . Beethoven 

 2 . Andante ( Pianoforte Sonata , Op . 147 ) Schubert . 
 Largo ... Handel . 
 aa hart ma “ eve Spohr 

 3 . Agnus Dei Schubert 
 Overture , “ Acis Galatea ” Handel . 
 Albumblatter , No.1 .. Schumann 

 4 . Adagio ( Pianoforte Sonata , Op . “ . 1 ) Beethoven 
 Cat F ugue ss , Scarlatti . 
 Albumblatter , " . 5 ( Op . 99 ) . Schumann 

 5 . Romanze Scherzo ( Fourth Sympho ny ) Schumann 
 Air icivertare | Suite ] D ) Bach 

