AND

BEETHOVEN’S 
NINTH (CHORAL) SYMPHONY

Artists

Lent Half-Term begins Monday, February 18. Entrance, Wednes- 
day, February 13, at 2

Lectures by Walter Macfarren, Esq., F.R.A.M., on “ Beethoven’s 
Thirty-two Solo Sonatas,” every Wednesday until end of March

Organ Recital, Monday, February 11, at 3

SIXTH CONCERT. 
SATURDAY, FEBRUARY 4g, ArT 3

Huldigungsmarsch re re Wagner. 
Symphony, No. 5, in E minor .. Tschaikowsky. 
Violin Concerto in D ; aa aca Brahms. 
Overture—' The Taming of the Shrew” . Percy Pitt. 
Voca.ist: Miss FLORENCE SCHMIDT. 
Soto Viotin: Lady HALLE. 
SEVENTH CONCERT. 
SATURDAY, MARCH 2, aT 3. 
New Overture—“ The Butterfly's Ball” F. H. Cowen. 
(First performance.) 
Symphony in C (The “ Jupiter”) = Mozart. 
Pianoforte Concerto in B flat minor .. Tschaikowsky. 
Verwandlungs Musik (“ Parsifal”’) Wagner. 
Voca.ist: Madame AMY SHERWIN. 
Soto P1anoForTE: Signor BUSONI. 
EIGHTH CONCERT. 
SATURDAY, MARCH 16, art 3. 
Overture—"' Iphigenia in Aulis ” xs es Gluck. 
Symphony, No. 9, in D minor (The “ Choral ”) .. Beethoven. 
Violin Concerto in G minor(No.1) .. “6 P .. Max Bruch. 
Serenade in E flat (Op. 11) for Wind Instruments .. Richard Strauss. 
Ride of the Valkyries os “a ee Pe Wagner. 
Madame LILLIAN BLAUVELT. 
Madame KIRKBY LUNN. 
VocatistTs

Mr. LLOYD CHANDOS. 
Mr. DANIEL PRICE. 
Soto Viotin: Madame VON STOSCH

d is iended 
span ‘Wiapenr, tthe genes empentall We-telr 4a | regarded as a sister, and who warmly befriende

him. As director of the Cherubini Society, 
which this lady had founded for the propagation 
of German classical music (Beethoven, &c.), he 
did his best to make Florence a musical place 
from his German point of view, by instituting 
performances of German classical works, both 
chamber and orchestral, and with a decided 
amount of success

Though he regarded Florence as a temporary

music, of course, he could not appreciate, and 
| inall directions did his best to Germanise it by

bringing forward German masterpieces, especi- 
ally of Beethoven, which had not been heard 
there before. To a great extent his efforts 
were crowned with remarkable success. So 
frequently was he on the move, that at this 
time he habitually signed his letters to Madame 
Laussot with ‘ Quicksilver,’ or one of its many 
varied equivalents

During his wanderings in Italy he was 
assiduous in studying the language of his 
adopted country, and this he did with such 
success that he not only learnt to speak it 
fluently, but also to correspond in it, as his 
many letters to his old friend and pupil, 
Giuseppi Buonamici (all written in Italian), 
sufficiently testify. Great were his linguistic 
acquirements. During his visits to England he 
not only learnt our language, even to the use 
of ‘slang’ terms, so as to be able to speak it 
fluently and correspond in it, but also studied 
Spanish. With French he had been familiar 
from his earliest youth

The brothers Stephen and George Job Elvey were 
natives of Canterbury and choristers in the Cathedral ; 
and a wayfarer passing along Best Lane early in 
January, 1760, might have learned from the gossips that 
Shrubsole, the blacksmith, had received an addition 
to his family—a son, who has since become widely 
known as William Shrubsole, the composer of the 
hymn tune ‘Miles’s Lane.’ But one’s pleasant 
retrospect of these bygone times is broken by strains 
of music which prelude the special service on this 
first New Year’s Day of the new century

The municipal and military authorities of Canter- 
bury co-operated with the Cathedral dignitaries in 
making the morning service (at 11.30 a.m.) worthy 
of the city and the occasion. The Mayor and 
Corporation attended in state and there were con- 
tingents from cavalry and infantry regiments. The 
local clergy mustered in strong force, chief among 
them being the venerable Primate (who vigorously 
preached an extempore sermon of thirty minutes’ 
duration) and Dean Farrar. As it was a Precum day, 
the prayers were intoned by a canon—Canon Mason— 
instead of by a minor canon. The specially aug- 
mented choir consisted of about 140 singers, of whom 
forty were ladies. The band of the Royal Engineers 
(Chatham division) furnished the players on brass 
instruments—the corniters and sackbutters—which, 
in combination with the organ, produced an excellent 
accompaniments throughout the 
service. The Te Deum and Benedictus, a typical 
festal setting and quite modern in character, was 
from the pen of Mr. H. C. Perrin, organist of 
the cathedral, while the anthem—a setting of the 
words ‘O Lord, Thy word endureth for ever in 
heaven ’—had been specially composed for the 
occasion by Sir Frederick Bridge, who conducted it 
in person. After the Archbishop’s sermon, a hymn, 
written for the occasion by Dean Farrar, was sung to 
a pleasing tune—also new—by Mr. Perrin, and the 
voluntaries, played by ‘ brass and organ,’ were selected 
from Beethoven and Gounod. With the exception of

the anthem, Mr. Perrin conducted throughout, and

97

hort duration, its introduction into operas and plays saved 
Hit from oblivion. To give a few instances—Beethoven in 
Fidelio, Weber in Der Freischiitz and Preciosa, Marschner 
hin Der Vampyr and Hans Heiling, Mendelssohn in A 
j Midsummer Night's Dream,and Schumannin Manfred. But 
' shout the middle of the nineteenth century composers began 
F again to cultivate the melodramatic form independently. 
It will suffice to mention here Schumann, Liszt, Grieg, 
‘Mackenzie, and Richard Strauss. Mackenzie cannot be 
‘named without mentioning his felicitous utilisation of the 
melodramatic form in his cantata, The Dream of Fubal

The aim of the melodramatic treatment is to emphasise 
what the words express, and to supply what they only 
imply or hint at; music being superior to speech in 
emotional expression, and also able to add descriptive 
‘touches. As a rule, the most important part of melo- 
dramatic music is heard in the pauses of the recitation, 
during the speaking the music is either silent or subordinate, 
consisting oftenest only of a few sustained harmonies. 
The procedures of composers differ, however, in this 
respect very considerably. To show how much they 
sometimes differ, Massenet’s opera Manon may be instanced, 
where the continuous music accompanying the purely 
conversational spoken dialogue is neutral with regard to 
expression, and serves only to bridge over the musical 
silences that otherwise would arise between the lyrical and 
dramatic portions of the work. This, however, is melo- 
dramatic treatment only in form, not in spirit

setting of the 1ooth Psalm. Eighteen years later (i 
1878) he adapted a portion of this Exercise to forp 
an independent eight-part motet, which was py}. 
lished by Messrs. Novello. 
his degree of Doctor of Music, the examiners on thy 
occasion being Sir F. A. Gore Ouseley, Dr. Corfe. 
and Dr. Stainer—the last-named then a young ma 
of twenty-seven. When, in 1876, the University of 
London decided to confer Degrees in Music, Dr, Pole, 
at the request of the authorities, drew up the Regul. 
tions and became one of the first examiners.—(Vii, 
THE Musica Times, August, 1886, p. 461

It is now time to turn to the literary side of Dr, 
As far back as 1855 he wrote 
analytical programmes for the concerts of the New 
Philharmonic Society, among the works analyse 
being the last seven symphonies of Beethoven and, 
new symphony by Gounod, then a ‘ new man’ in this 
In 1857 he wrote, for the Crystal Palace 
Company, an analysis of ‘ Judas Maccabzus’ and in 
1879 he published his ‘The Philosophy of Music’ 
He wrote eleven articles for Sir George Grove’ 
‘Dictionary of Music and Musicians.’ 
to the article entitled ‘Musical Degrees in the 
University of London’ (already referred to), Dr. Pole 
contributed to THE MusicaL TIMES a paper on 
‘Professional Musicians and Musical Amateurs’ 
He wrote a very valuable and 
interesting monograph on Mozart’s ‘ Requiem,’ which 
originally appeared in the columns of this journal

In 1857 he proceeded t

THE story of the life of Wagner is in many ways a 
remarkable one. His uncle Adolf was a man of great 
culture, and one who, like his famous nephew, ‘ ever 
battled valiantly against the vulgar, bad, and superficial.’ 
His father loved poetry and the drama, and, occasionally, 
took part himself in private theatricals, while Geyer, the 
father-in-law, was both painter and actor. So that from his 
earliest days the boy lived under conditions which seemed 
most favourable for the development of his latent genius. 
Wagner was not quite five years old when his sister Rosalie 
made her début on the stage, and she too, by the career 
which she had embraced, and by her sisterly affection, 
helped in the same direction. Young Richard went to 
school at Dresden and was accounted good iz literis; we 
find him also especially attracted by Greek mythology and 
history ; it was there, too, that he heard ‘ Der Freischittz,’ 
and there that he saw the composer conducting his opera, 
and from that time forth music exerted powerful influence 
over hims

Then he studied Shakespeare, made acquaintance with 
the glorious music of Beethoven, wrote an overture which 
was performed under Dorn at the Leipzig Court Theatre, 
other overtures which were produced at the Gewandhaus, 
Leipzig ; and, after some further attempts at instrumental 
music, we find him at work—and before he was out of his 
teens—on a first opera, ‘Die Hochzeit.’ Sister Rosalie

however, disapproved of the poem, and it was destroyed

Ludwig van Beethovens Leben. Von Alexander Wheelock 
Thayer. Zweite Auflage. Neu bearbeitet und erganzt von 
Hermann Deiters. Erster Band

Berlin: W. Weber

Ir is now thirty-five years since the first volume appeared 
of Thayer’s ‘ Life of Beethoven’; after six years followed 
the second, and then after seven years the third, Carrying 
the biography only down to the year 1816. The author 
lived until 1897, but business matters—he was United 
States Consul at Trieste—the difficulties and delays in 
collecting material, and, finally, the infirmity of age pre. 
vented Thayer from completing the opus magnum of his 
own life. Dr. Deiters, fortunately, undertook to write the 
fourth and last volume, making use, naturally, of all the 
documents, papers, and notes left by Thayer. He also 
undertook to revise the three volumes already issued, and the 
first fruits of his labour of love are now before us. Thayer 
himself had perceived the necessity of such revision, and 
had already set to work on the first volume; besides 
corrections and additions, he had actually re-written the 
first chapter. It is always a task more or less difficult for 
one man to complete what another has begun, but Dr. 
Deiters enjoys special advantages. He was not only the 
translator into German of the original English manuscript, 
but from the beginning, in 1866, he was in constant inter. 
course with Thayer. How much the latter valued his 
counsel and trusted him may be seen from Thayer’s letter 
addressed to him in the first edition of the first volume, 
And we may add that before his death the author expressed 
the wish that his work should be revised and completed by 
Dr. Deiters. In the preface to the volume under notice the 
latter fully explains the kind of changes which have been 
made; readers, however, will find most of the important 
ones in signed foot-notes

The original text has been materially altered in one 
respect. Theodor Fischer and Beethoven’s father were

Then came ‘ Die Feen’—never performed during Wagner's | comrades in their youth, and, in after years, when both 
lifetime, with the exception of the overture, which was | were married, the Beethoven family lived in Fischer’s house

played at the Gewandhaus—and ‘ Das Liebesverbot,’ only | after the birth of the composer. Caecilia (1762-1845) and 
once performed, and miserably, at Magdeburg. In May, | 
1836, the composer is at Berlin, ‘ without the smallest | 
certain prospect.’ He was expecting to be deputy-conductor

But he was not ; 
appointed, and in despair he went as capellmeister to | 
Konigsberg, and next to Riga ina similar capacity. Itwas | Appendix to his first volume, evidently thought them a | 
in the latter town that he wrote down the text of ‘ Rienzi.’ |! mixture of reality and romance

Gottfried (1780-1845) —son and daughter of Theodor 
Fischer—remembered much, talked much, and wrote much 
about that family, when the Beethoven and the Fischer 
children used to be constantly together. Before they died 
the two Fischers wrote down their reminiscences of 
those olden days. Thayer, who published them in an

Dr. Deiters, however, 
having come to the conclusion that the writers were 
thoroughly honest, and that although a certain allowance 
must be made for the tricks time plays with the most honest 
memory, their story was in the main true ; he has, in fact

whole Fischer text in an Appendix, as in the first edition

though marked with initials ‘‘H. D.” in brackets after the 
heading, he has incorporated portions of it into the text. 
There is, for instance, the charming description of the home 
festivities on the anniversary of the birthday of Beethoven’s 
mother. And then, again, how interesting is the simple

statement of Gottfried, that ‘ in the house Rheinstrasse, 934, | 
Mozart was often the subject of conversation.’ ‘The

The Recital Series of Transcriptions for the Organ

Eleven numbers.) By Edwin H. Lemare. 
[Novello and Company, Limited.] 
OverTuREs figure largely in this the latest instalment ot 
these popular organ arrangements. The ‘Coriolan’ and 
‘Prometheus’ of Beethoven, the ‘Don Giovanni’ of 
Mozart, the ‘ Stradella’ of Flotow, and last, but not least, 
the ‘In Memoriam’ of Sullivan are all transcribed with

an ex 
pleasi: 
mainté 
theme 
panied 
tranqu 
name | 
excelle 
sO mas

By oUR YORKSHIRE CORRESPONDENT

THE appointment of a successor to Sir Arthur Sullivan 
in the conductorship of the Leeds Musical Festival has for 
some time past occupied more minds than those of the 
committee. As everyone is by this time aware, their 
choice has fallen upon Dr. Stanford. It was an unsolicited 
honour, and we believe we are right in saying that it was 
conferred without any of the contention which too often 
accompanies these matters. Dr. Stanford is, however, no 
stranger to Leeds or its festivals. According to Messrs. 
Spark and Bennett’s ‘History of the Leeds Musical Fes- 
tivals,’ he was first represented in the programme in 1883, 
when the beautiful song, ‘ There’s a bower of roses,’ was 
sing by Miss Anna Williams. This was but a prelude 
to more important things. In 1886 ‘The Revenge’ was 
produced at Leeds, in 1889 ‘The Voyage of Maeldune,’ 
and in 1898 a Te Deum; in each case for the first time. 
In 1897 he formed a closer connection with the West 
Riding town, being invited to take up the conductorship 
of the Leeds Philharmonic Society, the premier choral 
society in the town. Under him the Society has given, in 
addition to the usual stock oratorios, performances of 
Brahms’s ‘German Requiem,’ Bach’s ‘ Wachet auf,’ 
Beethoven’s great Mass in D and Choral Symphony, 
the ‘Childhood of Christ,’ of Berlioz, the Prologue to 
Boito’s ‘ Mefistofele,’ and Dr. Stanford’s own Requiem 
and Elegiac Ode. During these three years and more he 
has thoroughly gained the confidence of his chorus, and, 
with the help of an excellent choirmaster in Mr. Fricker, 
the Town Hall organist, he has materially advanced the 
tliciency of the Society, in addition to so influencing the 
programmes as to make them by far the most interesting 
of their kind in the West Riding. It is to be hoped he will 
be allowed to exercise a similarly good influence on the 
festival programme; but the ways of festival committees 
ae peculiar, especially when the independence of the 
Yorkshire character has to be taken into account

So far only three works have been chosen: Handel’s 
‘Messiah,’ Beethoven’s Mass in D, and Bach’s cantata 
‘Wachet auf’ (‘ Sleepers, wake’). No new departure is 
involved in any of these, for at Leeds the custom seems to 
te established of alternating ‘The Messiah’ with ‘ Elijah’ 
and Beethoven’s Mass in D with Bach’s in B minor

As regards works specially commissioned for the Festival, 
the committee has been singularly unfortunate. Composi- 
tions were invited from Dvorak, Sir A. C. Mackenzie, Mr. 
Edward German, and Mr. Coleridge-Taylor. Dvorak, 
perhaps recollecting his experience of. Leeds in 1886, did 
not even reply to the request, but the other three consented. 
Some time ago Sir Alexander Mackenzie found himself 
unable to complete the cantata he had begun, on the subject 
of ‘Balder, the Sun-God,’ and now Mr. Edward German, 
who had begun work on a violin concerto, asks to be 
excused in order that he may undertake the more pressing 
task of finishing Sir Arthur Sullivan’s last Savoy operetta. 
So that only one of the quartet remains faithful, Mr. 
Coleridge-Taylor, who is busily engaged upon his cantata, 
based on Longfellow’s poem, ‘The Blind Girl of Castel 
Cuillé.’ No doubt the committee will endeavour to supply 
the deficiency, and we imagine there are plenty of young 
composers, with unheard masterpieces in their portfolios, 
quite ready to step into the breach

SAINT-SAENS’S QUARTET IN E MINOR

Special interest was imparted to the second appearance 
of the party by the first production of a Quartet for strings 
in E minor (Op. 112) by M. Saint-Saéns. This is dedicated 
to M. Ysaye, and, it may be added, has manifestly been 
written largely with regard to his exceptional abilities. 
The third and fourth movements belong to the class 
commonly known as ‘ solo quartets,’ but it need scarcely 
be said that this does not diminish their attractiveness to 
an audience when such an artist as M. Ysaye is leader. 
The first violin is also favoured in the opening number, 
but not excessively, and, although somewhat loose in 
design, this portion holds the attention, chiefly by reason 
of its poetically suggestive character. The second move- 
ment is remarkably clever, and, be it added, interesting. 
It is in Scherzo form, and includes in the trio portion an 
effective fughetta. M. Gabriel Fauré’s refined Quartet in 
G minor (Op. 45) was also performed on this occasion, and 
at the concert on the rgth ult. the ensemble works chosen 
were Beethoven’s Quartet in G (Op. 18, No. 2) and Brahms’s 
Pianoforte Trio in C minor (Op. ror). The solo pianist on 
the two last-named occasions was Herr Schénberger, and 
the vocalists have been Miss Louise Dale, Madame Lillian 
Blauvelt, and Mr. Meux

LONDON AND SUBURBAN CONCERTS, &c

Amoncst the musical events of the past few weeks which 
have attracted general attention, the foremost place must 
be accorded to the performance of Herr Gustav Mahler’s 
First Symphony, by the Philharmonic Orchestra, of which 
he is the conductor. Already more than a twelvemonth has 
elapsed since the Viennese public were made acquainted, 
by the same excellent body of instrumentalists, aided bya 
mixed choir and solo vocalists (for the score requires all 
these), with Mahler’s Second Symphony—a work charac- 
terised by somewhat unbridled fancy, a highly developed 
sense of the poetic and picturesque, an amazing skill and 
raffinement in the orchestration, but somewhat deficient 
withal in original musical invention. In his earlier work, 
recently introduced tous, the composer has contented himself 
with the employment of more modest means. Dispensing 
altogether with the choral element, the symphony exhibits, 
in its purely orchestral movements, a deep pathos, 
combined with great brilliancy of colour, while, on the 
other hand, its positive musical contents are even more 
problematical than in the case of the later work

Another noteworthy event was the performance—for the 
first time in the city in which the master wrote them |— 
of the complete cycle of Beethoven’s Symphonies, in 
consecutive order, by the very active new Concert-Verein. 
These performances, which are not yet completed, are 
under the conductorship of Herr Ferdinand Loewe, who 
is proving himself eminently worthy of so interesting and 
significant an occasion. The anniversary of Beethoven's 
death was signalised in a special manner by the Gesell- 
schaft der Musikfreunde, who on that occasion announced 
the result of a competition promoted by that Institution; 
the prize of 2,000 crowns being awarded, for a Symphony 
in E major, to a hitherto quite unknown composer, Hert 
Franz Schmidt, a member of the Opera orchestra. The 
new work, the merits of which had been specially com- 
mended by the jury, will doubtless ere long be produced in

ublic. 
, A number of more or less interesting choral pieces, some 
with pianoforte accompaniment and others unaccompanied, 
were produced at a concert of the Singakademie. Amongst 
those of more remote origin, two old French ‘ Brunettes, 
the composers of which are unknown, attracted the most 
favourable attention; while amongst the modern ones, 
the palm was given toa chorus for female voices, entitled 
‘ Windzauber,’ by Heinrich Rietsch, the newly appointed 
professor of musical science at Prague University, and toa

Amongst the virtuosos who have recently favoured us 
with their visits, the greatest popular success has been 
achieved by Jan Kubelik, whose recitals have been invariably 
crowded by enthusiastic audierices. It is true that, from 
a purely musical point of view, the young artist does not 
reveal to us much that is new; but genuine virtuosity, 
such as his, will always exercise its great powers of 
attraction. Eugen Gura, the Munich ‘ Meistersanger,’ 
gave a recital, in which he interpreted, in his superb 
manner, a number of Loewe’s Balladen, as well as songs 
by Hugo Wolf

Some special interest attached to the visit paid us by 
the young Belgian pianist, Emile Bosquet. As the winner 
of the Rubinstein prize, awarded in Vienna last summer, 
the young artist had been invited to appear before a 
larger public here, in a chamber concert, and although, 
in his interpretation of Beethoven’s Concerto in E flat 
major and participation in Schumann’s Quintet, he cannot 
be said to have exhibited exceptional merit, he neverthe- 
less proved himself an artist of much culture and refinement, 
whose technique is irreproachable. Another interesting and 
sympathetic acquaintance we made in the person of a young 
composer and pianist, Mr. Donald Tovey, of London, who, 
in concerts given by Fraulein Fillunger, was the unassum- 
ing but truly excellent accompanist. His introduction in 
leading musical circles here has gained for him the reputa- 
tion of a musician of more than ordinary attainments as 
one who is gifted and already considerably advanced asa 
composer and as an expert pianist. A characteristic and 
very interesting pianoforte quartet, by another young and 
promising composer, of Czech nationality—Victor Novak 
—attracted the attention of connoisseurs at its performance 
by the Tonkiinstler-Verein, an association which renders 
good service in encouraging young and, as yet, little known 
composers

Something of a sensation was produced by the début 
here of a girl violinist, the twelve year old Steffi Geyer, of 
Budapest. The picture of robust health and strong 
physical development, her appearance is the very opposite 
of that associated with the youthful prodigy. Simple and 
perfectly natural, too, is her playing, which is distinguished 
by marvellous purity of tone, singularly mature powers of 
expression, and an unerring and, as it were, matter-of-fact 
precision in surmounting the greatest technical difficulties. 
Ararely and magnificently gifted child, truly

The Brodsky Quartet, from Manchester (Messrs. 
Brodsky, Briggs, Speelman, and Fuchs), performed at each 
concert a number of the finest pieces of chamber music, 
and their fine tone and perfect ensemble playing was 
worthy of their great reputation. A few songs by Miss 
McKisack gave variety to the programmes

At the first concert the works were :—Beethoven, String 
Quartet in C (Op. 59, No. 3); Bach, ‘Ciaconna’ (Mr. 
Brodsky); Schumann, Quintet (Op. 44), Dr. Walker 
(pianoforte). At the second concert:— Mozart, String 
Quartet in C; Marcello, Sonata in F (Mr. Fuchs) ; Schubert, 
Variations from String Quartet in D minor (‘Der Tod 
und das Madchen’); Dvorak, Quintet (Op. 81

MUSIC IN BIRMINGHAM. 
(FROM OUR OWN CORRESPONDENT

FROM OUR OWN CORRESPONDENT

the concert concluded with what must be considered 
|as the best performance of Beethoven’s Septet given for 
many years in Dublin. The performers, in addition to 
those already named, were Monsieur Grisard (viola), Mr, 
Conroy (clarinet), Mr. Taylor (bassoon), Mr. Morrissy (horn), 
and Mr. May (bass). A very large and appreciative 
audience attended and the success of the first meeting of 
the Union augurs well for its future

At the second concert, on the 18th ult., Brahms’s beay. 
| tiful Pianoforte Quartet was played by Messrs. Wilhelm, 
Grisard, Bast, and Esposito. It was avery fine performance, 
Beethoven’s Sonata for pianoforte and violin and Haydn's 
String Quartet in G minor were also played

The syllabus of the Feis Ceoil includes some new features 
of interest: a prize for the best performance of any com. 
bination of wind instruments, two competitions for trios 
(pianoforte and strings), a viola competition, and a special 
prize for the best singing of an Irish character song— 
additions to the usual competitions. The tests for school 
choirs have been made simpler and the sight tests in the 
vocal trios and quartets have beeneliminated. The adjudi. 
cators are as follows :—Professor Ebenezer Prout, Mr, 
Ivor Atkins, Mr. Denis O’Sullivan, Mr. Carl Fuchs, Mr, 
Oscar Beringer, Mr. J. Ord Hume, Mr. Brendan Rogers, 
Mr. R. Young, and Mr. P. J. M‘Call. 
| The prize for the best work for soli, chorus, and orchestra 
| Offered by the Feis Ceoil Association has just been won by 
| Carl Gilbert Hardebech, of Belfast, who has frequently 
| won smaller prizes at former Festivals. The work is very 
| fully scored, with solos for mezzo-soprano, tenor, and bass, 
The title is ‘The Red Hand of Ulster,’ and treats of a 
favourite Irish legend. The work is to be performed at the 
Festival next May

_—_——_——_

On the 
performance 
chief solo’ 
evidenced | 
ain admis 
in the hall. 
representin 
of Music, 
17th ult. 
(No. 1, Op 
Sharpe c¢ 
Beethoven 
audience tc 
Miss Hele 
acted as de

Dr. Ric 
enlightene 
works as 1 
with select 
hitherto w 
note must 
Symphony 
a decid 
previous mr 
of Bach fo 
parts. It 
listening t 
Fuchs, altl 
works for 
Lalo, has 
incoherent 
at the Hal 
Lillian Ble

12!I

On the 16th ult. the Choral Union gave a ‘popular’ 
performance of ‘ The Messiah,’ with Mr. Andrew Black as 
chief soloist. The drawing power of the oratorio was 
evidenced by the fact that the number of people unable to 
gain admission was almost as great as that accommodated 
inthe hall. Messrs. Cole, Melville, Joachim, and Augless, 
representing the string quartet of the Atheneum School 
of Music, gave an enjoyable Chamber concert on the 
7th ult. The programme included Haydn’s Quartet 
(No. 1, Op. 74) and Beethoven’s Trio in E flat. Mr. J. W. 
Sharpe contributed vocal solos. A performance of 
Beethoven’s Pastoral Symphony attracted an enormous 
audience to the Popular Orchestral concert on the rgth ult. 
Miss Helen Jaxon was vocalist, and Mr. Maurice Sons 
acted as deputy-conductor

MUSIC IN MANCHESTER. 
(FROM OUR OWN CORRESPONDENT

Dr. RICHTER has recently presented us with yet more 
enlightened and finished interpretations of such classical 
works aS we may never too fully understand, alternately 
with selections from both older and more modern schools 
hitherto withheld from us. Among the former especial 
note must be made of the rendering of Beethoven’s Eighth 
Symphony, on the 17th ult., so admirably contrasted with 
the decidedly noisy Sinding Symphony in D minor of the 
previous meeting and with the staid, sober orchestral suite 
of Bach for strings, oboes, drums, and with three trumpet 
parts. It is always gratifying to have an opportunity of 
listening to the charming violoncello playing of Mr. Carl 
Fuchs, although one ever regrets the dearth of really great 
works for his instrument. The Concerto in D minor, of 
Lalo, has many interesting passages ; although somewhat 
incoherent and unsatisfactory as a whole. The vocalists 
at the Hallé concerts during January have been Madame 
Lillian Blauvelt, Miss Brema, and Mr. Santley

As usual, Mr. Lane's patrons flocked to his excellent and

SONDERSHAUSEN.—At a concert given by the Conserva- 
torium, under the direction of Professor Schroeder, a new 
composition for chorus and orchestra, ‘ The Lady of Castle 
Windeck’ (founded upon Chamisso’s ballad), by Rudolph 
Werner, was received with much favour. The same 
concert included the performance of a ‘ Hymn to Life,’ for 
chorus and orchestra, by the late Friedrich Nietzsche, a 
somewhat ambitious, but distinctly amateurish production, 
scarcely calculated to place the vaunted musicianship of 
the famous poet-philosopher in a very favourable light

TEPLITz.—Commemorative tablets have been affixed to 
the houses at Schénau, known by the signs of ‘ Zur Harfe’ 
and ‘Zur Eiche,’ in which Beethoven resided during the 
summer months of 1811 and 1812

MISCELLANEOUS

the Society, gave a most interesting analysis of musical 
forms, which he illustrated at the pianoforte by playing

excerpts from the works of Handel, Bach, Beethoven, 
and Mendelssohn. The vocal illustrations were given in a 
refined manner by Miss Beatrice May and a quartet of 
male voices—Messrs. Oakeley, Turner, Bennetts, and 
Stanley Smith. Before the performances Mr. Gilbert gave, 
in concise and lucid language, a description of each class of 
composition, which ranged from canon and fugue to the 
sonata and song without words, and which included the 
madrigal, the part-song, the glee, the early Italian song, 
the ballad, and the modern song

An exceedingly pleasant evening was spent on the 
17th ult., at the residence of Dr. W. G. McNaught, when 
a private representation was given of an original operetta, 
in two acts, entitled ‘ Lucette,’ composed by Mr. William 
McNaught. The libretto, by Mr. W. G. Rothery, was 
smartly written and very much up-to-date, and the music 
revealed in the youthful composer (who, we understand, 
is quite untrained in composition) a considerable gift of 
melody and distinct capacity in writing which should 
surely induce him to further cultivate these gifts. It 
would be invidious to specially mention any of the 
performers; sufficeth it to say that all worked together 
with earnestness and spirit and that the result was highly 
satisfactory

M. JuLes Barsigr, the distinguished opera librettist, 
died on the 16th ult., at the age of seventy-eight. ‘Faust’ 
alone will carry his name down to posterity, but he also wrote 
the libretti of ‘ Roméo et Juliette,’ ‘ Noces de Jeannette,’ 
‘ Paul et Virginie,’ and many other opera books

The death occurred, on December 26, at St. Louis 
(U.S.), of Professor AuGust WatLpaueEr, founder and, 
for many years, director of the Beethoven Conservatory in 
that city, aged seventy-five

The death of Dr. W. Pote is referred to on another page

WINCANTON.—The Christmastide cantata, by Dr, Pearce, 
was performed in the Parish Church, on the 15th ult., 
when Mr. Chuter (organist of Sherborne Abbey) accom- 
panied, and Mr, E. Harold Melling, organist of the church, 
conducted

ANSWERS TO CORRESPONDENTS. 
G. L.—(1) The passage in Beethoven's Rondo for piano- 
forte in C, Op. 51, No. 1, bar 24, groups 3 and 4, may

be played :— 
- Z _-@- -@ jako, » ee a 
-2P-ejpe0 tit tee

ORGAN. 
THE VILLAGE ORGANIST.—Boox 18

1, Funeral March (Sonata, Op.26) .. ae oe -. Beethoven

2. Ditto (Sonata, Op. 35).. we ee “« Chopin. 
3. Dead March (“‘ Saul’’) sis aes oa a Handel. 
4. Funeral March (‘‘Story of Sayid”’) wa . A.C. Mackenzie. 
5. Ditto (‘‘Liederohne Worte,’’No.27)  .. oe Mendelssohn

ORGAN MUSIC

THE VILLAGE ORGANIST.”—Book 18, containing Funeral 
Marches by Handel, Beethoven, Chopin, Mendelssohn, and 
Mackenzie. 1s

BEETHOVEN.—Funeral March on the death of a hero (Op. 26), 
arranged by W. T. Best (Best, 83), together with Schubert's 
March in B minor. 2s

Funeral March (Op. 26), arranged by W. J. Westbrook

SCHUBERT. —Grand Funeral March, arranged by E. Silas. ts

March in B minor (Op. 27, No. 1), arranged by W. T. Best 
(Best, 83), together with Beethoven's Funeral March. 2s

Marche Solenne'le (Op. 40, No. 5), arranged by W. T. Best. 2s

ConTENTS.—Book II. (Just PuBiisHED

Birthday March .. Schumann, 
British Boys’ March Richards, 
Dessauer March.. ne ve ve Ae 
Gavotte .. oe ee ore a re «- Elvey. 
Gavotte se ae ce os ee Handel. 
Gipsy March. ‘is a us +» Weber. 
Hohenfriedberger “March ee ae 
March from ‘Judas Maccabeus” .. Handel. 
March from “ Le Nozze di “ane ne a Mozart. 
ae arg al ais ‘ ea Schubert. 
arseillaise, La mae - ae ‘ - Rouget de Lisle, 
Merry Peasant, The .. ae At oe pe nny 
Turkish March . Beethoven. 
Ye Mariners of England Pierson

Wedding March Mendelssohn

