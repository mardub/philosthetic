Royal College Organists 

 candidate fellowship request note 
 stall , , 6d . ; Arena , 7s . 6d . ; Balcony ( reserved ) , 5 . ; | copy current regulation , requirement 
 unreserved , 3 . 6d . ; Gallery - Promenade ( 1,000 seat ) , 2s . question 3 ( c. ) morning paper leave blank ; Septet 
 | & flat , Op . 20 , Beethoven , Miniature Score . ( Goodwin & Tabb 

 solo - play test F.R.C.O. EXAM 

 middle plain 

 reason impel inspiration 
 child - like sincerity Wesley instinct 
 Beethoven great moment . modern 
 music impress cleverness 
 texture , leave disappointed 
 lack wonderful flash inspiration 
 find work great man — moment 
 , simple technical material , 
 suddenly lift heaven , 
 hold breath . ' quote well- 
 know instance : Choral death 
 Christ ' St. Matthew ' Passion , bar 
 end Scherzo c minor 
 symphony Beethoven , horn passage 
 slow movement Schubert Symphony C , 
 entry chorus brass 
 word ' Lord word endureth ' 
 Brahms * requiem 

 mind way Wesley 
 transcend contemporary 
 follow . bar 
 choral recitation fuga ! chorus , 

 ' Frste 

 article Dat / y Telegraph February 27 , 
 Robert Lorenz , express good 
 feel regard certain classic . article 
 entitle " fault ? ' question 
 writer discovery 
 idol begin wear badly . 
 hear Schumann * Manfred ' Overture , 
 awestruck complete 
 insignificance . ' Schumann , song piano- 
 forte music enjoy , ' appear 
 Overture sink low rung 
 Kapellmeistership . ' Mr. Lorenz dodge item 
 follow order think thing , ' 
 preparation ordeal — Beethoven 
 B flat Pianoforte Concerto . ' hear 
 work long virtually new 

 remain oblivion [ 

 think thing , eventually 
 arrive conclusion , 
 need concern . decide great 
 classical symphonist deficient subtlety , 
 experience modern composer 
 Elgar , Franck , Wagner , Debussy , 
 ' impatient brusque , straightforward method ' 
 old writer . incline -except 
 Bach , music find ' Shakes- 
 pearean range subtlety far 
 absent 

 Mr. Lorenz dozen 
 friend phase , 
 man mid - thirty think 
 complaint 
 age . presumably idea old 
 immune ultra - modernism , 
 young die - hard classicist . let assure 
 age - limit matter . 
 know plenty musician ( amateur like Mr. 
 Lorenz friend , professional ) like 
 past mid - thirty , feel 
 pretty regard large pro- 
 portion classical music . ' fault ? ' ask 
 Mr. Lorenz . ' answer failure 
 respond Beethoven work necessarily 
 fault hearer ; merely 
 misfortune . case certain work 
 undoubtedly Beethoven fault , great 
 composer let completely 
 bad work . 
 fault , simply inevitable result passage 
 year . fill volume harmony , 
 rhythm , modulation , & c. , thrilling 
 use , threadbare . 
 work use 
 depend success , 
 appeal , element 
 surprise , freshness , vital . Beethoven 
 suffer badly way . isamusing remember , 
 example , tonic seventh opening 
 symphony cause like 
 commotion . ' audacious , ' Grove , ' 
 amply sufficient justify unfavourable recep- 
 tion meet establish critic 
 day . ' ' audacious ! ' raise eye- 
 brow — , 
 Symphony shelf . think , , 
 discord beginning Pres¢o ninth 
 Symphony . Grove ' horrible clamour , ' 
 accustomed violent dis- 
 sonance _ shatter ( /orttssimo 
 scarcely notice . occasion 
 actually miss , probably 
 recover slumber induce 
 slow movement 

 — slow movement — departt- 
 ment music classical composer 
 fail hold . leisurely discourse , 
 repeat figuration galore , doubt mean 
 contemporary . - day 
 find lack emotional significance , 
 devoid musical interest . 

 1926 313 

 matter manner fade . day 
 come round — history music 
 roundabout fashion — 
 ashamed slow move- 
 ment Beethoven boring 
 thing music . ( hasten add 
 String Quartets ; 
 exception dare comment 

 unfavourably Beethoven 

 Mr. Lorenz right track 
 absence subtlety classical 
 work cause lack interest 
 feel ? doubt . like , 
 try account diminished appeal 
 certain composer , especially Beethoven 
 Mozart . ( murder ’ ! 
 Mozart devotee begin boil ! ) 
 twofold kind poverty — triteness melody 
 uninteresting harmony ? aware 
 great reputation composer tune writer , 
 inspired melody 
 half - - dozen mere platitudinous 
 string note . sick . 
 constant half - close close , 
 long stretch harmony entirely tonic 
 dominant , occasional diminished seventh . 
 ( case Beethoven , Mozart chamber 
 music . ) Mr. Lorenz hit 
 reason lack appeal , 

 subtlety rarely absent Bach , add , 
 " Bach belong early era music , 
 polyphony discard favour | 
 homophony trouble begin . ' true ; 
 ' trouble ' loss subtlety , think , 
 sudden reduction harmonic interest . 
 good contrapuntal texture , base 
 tonic dominant harmony , set sort 
 harmonic reaction — tiny , total 
 efiect produce constant variety interest . Jn 
 fact , hardly progression , type 
 ' modern , ' produce 
 momentarily adventitious way . Bach 
 work anticipation , touch 
 passing . polyphony fashion , 
 composer begin concern 
 entirely melody , form , orchestration , 
 lot lose , believe Mr. Lorenz 
 flee Beethoven B flat Concerto ( 
 present , flee ) drive 
 work lack subtlety 
 poor thematic harmonic material . similarly , 
 find Bach subtle Beethoven , 
 probably impression Bach great 
 continuity movement , obvious cast 
 melody , , , harmonic interest 
 result largely polyphonic process — 
 course homophonic 
 moment Bach rarely enterprising 
 harmony 

 write far come 

 thing occur century 
 certain mineral stratum , day 
 exhaust , consequently abandon , 
 later - exploit new method 
 available . doubt Bach stratum 
 capable nourish musical art long 
 century , - day renew technique 
 allow draw immense legacy rich 
 romanticist suspect 

 attitude peculiar young composer . 
 general musical public 
 hint revolt Casella 
 ' uniquely egotistical art ' century , 
 ' personal expression composer 
 precedence law musical con- 
 struction . ' Beethoven like 
 lead direction work , 
 tend suite fugue 
 form , personal 
 expression . quote end Casella article 

 young composer copy 

 pronounced dent ought 
 bump reverence , merely stir 
 nose weak spot 

 course weak spot easily find 
 composer , Bach downwards , 
 think prove true facile 
 composer plentiful 
 Bach science usually 
 descend certain level . 
 inspiration flag invariably 
 nter attach consummate skill . 
 beat air padding consist ( 
 composer later 
 generation ) mere tiresome repetition tonic 
 dominant chord . arrive end 
 movement , content tie strand 
 counterpoint knot tonic chord , 
 . leave Beethoven madden 

 fulsome beslavering genius 

 frantic banging tonic chord 
 end work . ( C minor Symphony , 
 example , end - bar ! ) 
 tell repercussion express 
 soul - state composer , soul - state 
 lead noisy futility matter . 
 concern composer 
 , ( like preacher story ) 
 know 

 young friend 
 encouraged think thing . 
 course accuse presumption . 
 dare anybody find Beethoven tedious ? 
 answer generation 
 relation classic , ascertain 
 relation exercise critical faculty . 
 obvious , come think 
 art 
 blind thing trust , swallow 
 classic , music . life long 
 | enjoy beautiful thing 
 | write ( large proportion 
 ‘ composer rank ) , shall 
 |continue defraud concert 
 | programme , teaching list , examination 
 syllabus purge work write 
 | bigwig - day 

 Mr. Lorenz _ 

 , , connection 
 _ wane appeal certain classic lack 
 | discrimination choice music 

 concert , teaching , & c. example ( 
 | dozen ) , mention pianoforte 
 recitalist recently play Beethoven Fantasia , 
 Op . 77 — rambling work begin g minor 
 end b major , scarce half - dozen 
 consecutively interesting bar . 
 doubt single member audience 
 raise hand favour , know 
 receive silent derision . 
 Fantasia sign numerous 
 second- - rater live Beethoven 
 time leave richly deserve 
 ‘ obscurity . similarly , know feeble little 
 |pianoforte piece Mozart Beethoven 
 |include competition festival syllabus , 
 /playe time 
 audience suppose develop 
 taste mean festival . wonder 
 decide music represent 
 great classical composer 
 classical music miss possible 

 combination wireless _ psychical 
 research communication 
 | Elysian Fields , message reach 
 round robin group 
 classical composer . , " save 
 - enthusiastic friend — die - hard , last- 
 ditch , - hogger hero - worshipper ! write 
 ' share " dud , " mortal 
 | reverence way 

 Rhythm Song . ' C. F. Abdy Williams . 
 [ Methuen , 7s . 6d 

 rhythm doubt great 
 elemental force music art , 
 , Mrs. Meynell insist , life 
 organization universe . like 
 big thing , matter heart 
 head , feeling 
 thinking , discussion apt vague 
 loose . word easily confuse 
 term ' accent ' ' time , ' , , 
 author notice p. 30 , somewhat change 
 meaning . Beethoven speak ' Ritmo 
 di tre battute , ' speak waltz- jazz- 
 rhythm , apply word short rhythmical 
 figure , sense word large 
 meaning . ' rhythm good ' mean 
 vividness feeling short 
 rhythmical figure , growth consistent 
 plan handling figure mass , 
 ' rhythm ' work 

 prevalence loose thinking rhythm 
 prompt Mr. Abdy Williams . write 
 book . treatment subject noticeably 
 different Mr. Plunket Greene , con- 
 centrate general , broad aspect , 
 performer standpoint . Mr. Williams wish 
 place rhythm harmony 
 counterpoint branch science com- 
 position ; manifestly difficult , bring 
 task musicianshipand literary scholarship 

 gas - lamp , Musical 
 Union , half century Yorkshire 
 choral history different , 
 certainly glorious 

 programme Choirs Festival 
 Worcester ( September ) settle . contain 
 judicious blend old new . 
 choral work , addition customary ' Messiah ' 
 ' Elijah ' Beethoven ' Missa Solemnis , ' ' 
 Kingdom , ' ' fall , ' Berlioz ' Te Deum , ' 
 Charles Wood ' dirge veteran , ' Bach 
 Cantata , short piece Weelkes , Holst , 
 Ethel Smyth , & c. orchestral work include 
 Brahms C minor Symphony ' Enigma ' Varia- 
 tion . new work Bainton , Brent - Smith , 
 Walford Davies , W. H. Reed . Sir Ivor Atkins 
 point difficulty justice modern 
 composer , experimental nature 
 work , music 
 exacting type present 
 expense rest programme 

 classic place popular 
 pastime hour , thing 
 neatly completely extract 
 recent programme Hallé Orchestra 

 music , good surprised 
 little . chief need new 
 drastically revise edition ' English Hymnal , ' 
 base experience use 
 . result near ideal 
 reasonably hope . ' song praise ' 
 near approach ideal , 
 adopt ' E. H. ' feature 
 generally admit weak 

 school worship ' compile use 
 school , week - day Sunday , Young People 
 Fellowships , Guilds , & c. Mr. Thalben Ball 
 excellent lot tune , good 
 new pen . 
 regret inclusion weak folk - tune 
 poor arrangement french melody 
 ' English Hymnal . ' , , adaptation 
 type ought extinct , ¢,g . , poor 
 tune Beethoven romance G , couple 
 Mendelssohn ' song word , ' & c. 
 bad case ( _ per- 
 petrate - day folk ought know ) 
 maltreatment fine Rouen melody change 
 777.777 775.775 metre . ( . 69 . 
 adapter hide identity , modesty shame 
 initial ' R.E. ' ) apropos adaptation , 
 Boyd ' pentecost ' ( ' fight good fight ' ) state 
 ' harmonize H. Walford Davies , ' 
 true , truth , Sir Walford 
 completely change rhythm — bett ’ r , . 
 melody rewrite , good 
 tune result . book , 
 end , nearly page ' order 
 worship occasion , prayer , litany , 
 responsive devotion . ' notation 
 use , Sol - fa place conjunction staff 
 way user book 
 little trouble dual notationist . capital 
 feature clear spacing music ; 
 addition Tonic Sol - fa lead congested 
 ugly page . ' school worship ' strike 
 notable advance hymnal design 
 use young 

 ' golden bell ' ' favourite tune ' pull hard 
 reform poor chance . _ 
 Committee preface read 

 PHILHARMONIC : PAUL KLENAU concert 

 Philharmonic concert February 25 
 conduct Paul Klenau , friend 
 performance Delius ' Mass life ' 
 year . symphony 
 concert , Miss Erica Morini play Beethoven 
 Violin Concerto , programme include 
 new newish work 

 . minute 
 promise performance ( revised publish 
 form ) Dr. R. Vaughan Williams Violin Rhapsody , 
 * Lark ascend , ' cancel . appear 
 popular young violinist busy 
 world gather harvest 
 nimble skill . omit , day 
 , pick work month ago 
 allot . doubt 
 occupy polish playing dreary 
 Concertos Goldmark , Glazounov , Tchaikovsky , 
 — youthful ignorance insignificance — 
 prepare inflict . fiddle 
 virtuosa , victorious hemisphere , 
 naturally think composition supremely fine 
 music . , feel sorry 
 nonchalantly drop Vaughan Williams piece . 
 healthy , bloom young girl , extra- 
 ordinarily good violinist . ephemeral attribute ! 
 good fiddler deteriorate . hand stiffen , 
 tuning wobble time . , like man , 
 descend grave . 
 remember forget 
 myriad depart buxom maiden good 
 fiddler think playing 
 play ? — carelessly 
 let chance slip — link 
 creation music . honour 
 invitation play new , newly 
 - form , work Vaughan Williams . 
 blind luck , privilege . 
 child mistake diamond marble , 
 throw , thought , river . 
 pity young person unusual aptitude 
 fiddling stupid 

 WEINGARTNER 

 Weingartner programme contain Schumann 
 Symphony Beethoven seventh , Brahms 
 variation Theme Haydn , Holst 
 ' Mercury , ' ' Venus , ' ' Jupiter . ' Holst 
 ; note play , 
 understanding mood . 
 sici / iano close fugue 
 exalted moment Brahms — spacious 
 reserved . Schumann symphony prattle delight- 
 fully , fresh , spring - time mood , 
 Weingartner B flat , way 
 good . write pianistically 
 orchestra penalty write orchestrally 
 pianoforte . Schumann successful long work 
 combine . 
 hear symphony having 
 orchestrally sensitive . music , 
 medium , appeal 
 wayward sentence unencumbered logic . 
 waywardness Beethoven , art 
 set sentence relation context , 
 forwards backwards , 
 understand Weingartner . M. F 

 ORCHESTRAL concert 
 Sunday orchestral music , lately low ebb , 
 suddenly rise tide . Sir Landon Ronald 
 series concert Royal . Albert Hall 

 London Chamber Orchestra concert 
 excitement satisfaction AZolian Hall 
 March 5 . Bach second ' Brandenburg ' Concerto 
 ( Mr. Herbert Barr garrulous high 
 trumpet ) beginning , Mozart G minor 
 Symphony end . Mr. Frederick Thurston 
 play Debussy Clarinet Rhapsody , 
 probably unknown Debussy ninety - concert- 
 goer ; Stravinsky ' Pulcinella ' 
 suite follow Delius Violoncello 
 Concerto , cause strange 
 beautiful noise . Miss Beatrice Harrison 
 violoncellist . admirable programme ad- 
 mirably play , Mr. Anthony Bernard 
 credit interesting recent 
 concert 

 aniateur orchestra pace . 
 Strolling Players conduct Mr. Joseph 
 Ivimey February 18 Beethoven eighth 
 Symphony , tax capacity delicate 
 playing . chief work play Royal 
 Amateur Orchestra , Mr. Arthur Payne , 

 February 26 , ' Meistersinger ' 
 ' Di Ballo ' Overtures . solo player 
 concert organist — fact note 

 chief work hear Contemporary Music 
 Centre March 2 Pianoforte Trio Rebecca 
 Clarke , Duo violin violoncello Kodaly , 
 Sonata violoncello Hindemith . 
 player Mr. Arthur Alexander , Mr. Zoltan 
 Szekely , Mr. Paul Hermann 

 Casals Quartet , lead M. Enric Casals , come 
 Spain Music Society ( exchange 
 Music Society Quartet , recently tour Spain ) 
 March 9 . player straightforward , 
 acceptable performance Beethoven ( Op . 95 ) 
 light caprice Conrado del Campo 

 HUNGARIAN QUARTET 

 Hungarian Quartet ( 
 Budapest Quartet , Quartet Budapest lead 
 Mr. Jenner ) recital season 
 Wigmore Hall February 22 , begin 
 music countryman , Béla Bartédk , 
 proceed serene mood 
 classic . year ago late Arthur Johnstone 
 define Barték gifted madman . Quartet 
 ( . 2 , op . 17 ) , play Messrs. Emerie Waldbauer , 
 Jack Kessler , Jean de Temesvary , Eugéne de 
 Kerpely , fully bear description . lest 
 misapprehension , let add 
 intention cast doubt sanity 
 aman . artist question , 
 artist undoubtedly suffer appear 
 hallucination . define curious 
 belief phrase need repeat countless 
 time interesting significant ? 
 explain rude strength exhibit , 
 cunning trick ? let admit , 
 , genius ally madness , maybe 
 symptom decay reality sign 
 new life . midst death life 
 surely midst life death . 
 hungarian Quartet play Bartdk work great 
 care zest , Beethoven Quartet 
 equal care distinctly zest . B.V 

 BLOCH PIANOFORTE QUINTET 
 idea utilize equal temperament quarter- 
 tone occur Busoni , theorize 
 practice . M. Bloch believe 
 ( 1 ) string agree 

 Arne , great neatness finish , 

 admirably clear rhythmic performance Bach 
 Toccata C minor follow . unequal 
 Beethoven Sonata , op . 31 , . 2 , fully 
 reveal beauty slow movement , 
 blurred effect a//egretto . 
 fast . light , cleverly - write suite Hugo 
 Anson provide novelty programme , 
 play 

 Mr. Henry ’ Bronkhurst , recital 
 Wigmore Hall , plunge Bach straight 
 romanticism picturesque — , Bach 
 romantic flavour — , piece 
 Fantasia ? introduce Danza Festiva 
 Medtner , modern composer unashamed use 

 play , power , 
 esthetic development , enable 
 achieve . Brahms G minor rhapsody 
 requisite energy , blur 
 definite crisp progression , 
 desire ' push ' miss ' approach ' 
 interest variety work 

 Miss Isabel Gray Saturday afternoon 
 recital Wigmore Hall , entirely 
 Beethoven programme . Miss Gray conviction 
 able express uncertainty , 
 interpretation understanding . 
 playing somewhat academic , rise nobly 
 great Sonata , Op . 110 , hold 
 life experience , lead serene con 

 templation wide horizon . principal 
 work second programme Schumann 

 phrasing . 
 M. Alexandre Borovsky 

 recital ASolian Hall March 16 . M. Borovsky 
 great power breadth expression , 
 playing Beethoven sonata , Op . 111 , 
 remarkable — boldly conceive subtly express . 
 etude Scriabin follow — 
 late abstruse Scriabin , early work 
 — playing comprehensive character , 
 graceful , fiery , romantic . player 
 worth hear 

 principal group Mr. Leslie England 
 recital Wigmore Hall Beethoven , Chopin , 
 Debussy . sonata , op . 31 , . 2 , 
 Adagio wander uneven _ lengthy 
 manner . apart Mr. England reading 
 interesting . general impression , 
 hear Chopin Debussy , 
 musical content plane 
 technical efficiency . D’A 

 singer month 

 think abnormal , unnatural , suffer 
 willingly pretentious emptiness 

 Beethoven . — /John H. Culley 

 musical world comiag correspondent , 
 obscure amateur , 
 outrageous effrontery speak * pretentious emptiness 
 Beethoven ' ! — 4 / gernon Ashton 

 Miss B — — sing feelingly 
 horse . ’—New Zealand Paper 

 ' Elijah ' like human appendix ? 
 omnipresent , use anybody ; 
 cut ' painful process , prefer 
 leave is.—Zric Blom 

 detest Bach ; Beethoven leave utterly cold ; 
 abominate * Messiah ' ; Mozart Verdi 
 furiously angry , loathe Weber Schubert . 
 hand , Vaughan Williams extent 
 feel bear long ; Holst excite 
 stand shout joy ; Elgar 
 time verge tear ; Ireland 
 find lovable . , ? — /ohn read 

 ROYAL ACADEMY MUSIC 

 seven concert recital 
 month , devote chamber music 
 successful varied . Brahms 
 Pianoforte Quartet major Ernest Walker 
 Fantasia string quartet noteworthy feature 
 instrumental ensemble work , elizabethan 
 madrigal Ballets , vocal quartet Ernest Walker , 
 reflect great credit vocal ensemble class . 
 performance song college composer 
 , include Elizabeth Maconchy 
 Eric Freeman . concert second 
 orchestra usual opportunity college 
 conductor , dozen charge 
 performance symphony , overture , concerto , 
 new Scena voice Frederic Westcott 
 ( student 

 student recital Miss Edna Stanton , 
 return South Africa complete study 
 College , prove , somewhat unusual design , 
 highly successful . programme entirely 
 Fantasias ( Bach , Mozart , Beethoven , Schumann , 
 Chopin ) , find variety interest . 
 Schumann Fantasia , instance , 
 easily sound dull , play vitality , 
 sensitiveness , feeling colour associate 
 mature experienced artist 

 Patrons ’ Fund rehearsal term bring 
 forward new work british composer , viz . , Percy 
 Turnbull , Harold Rhodes , Edric Cundell , Allan Sly 

 1926 

 signor Zino Francescatti possessor 
 brilliant violin technique , ability play 
 passionate feeling occasion . Miss Silk sing Bach 
 song usual impeccable style.——it feel 
 music - lover Madame Gerhardt mistake 
 singe item ' celebrity ' concert 
 March 22 , set scottish song arrange Beethoven . 
 music dull , lack direct simplicity 
 scottish folk - song . group Schubert Brahms 
 song magnificently . concert 
 Léner Quartet play Mozart B flat Quartet , 
 Andante cantabile Tchaikovsky Op . 11 , 
 Schubert ' death Maiden ' Quartet . — — 
 February 25 , City Birmingham Choir 
 Wolverhampton Musical Society join force 
 performance ' hymn praise . ' ' 5i.t. ' 
 orchestra , conduct Mr. Joseph Lewis , 
 uncomfortable moment evening , 
 choir swing effort little . 
 Miss Carrie Tubb Mr. Walter Hyde soloist . 
 — -—The singing Mr. Charles Hedges Evangelist 
 music outstanding feature performance 
 Bach ' St. Matthew ' Passion Midland Musical 
 Society February 21 . voice right lightness 
 ease , diction equal tonal beauty 
 Mr. Hedges vocal equipment complete . 
 Madame Parks Darby , Miss Elsie Napier , Mr. William 
 Bennet , Mr. Leslie Bennett soloist . 
 Dr. Darby conducted.——Mr . William Murdoch 
 soloist Symphony concert City Orchestra 
 March 2 . movement Beethoven Pianoforte 
 Concerto C minor playing border 
 commonplace , work proceed , 
 final movement brilliant performance . 
 dance Suite Béla Bart6ék novelty 
 evening , play orchestra . 
 amazingly intricate score daring harmonic scheme 
 arouse discussion . Mr. Adrian C. Boult con- 
 ducted.——The classical bohemian composer 
 represent Sunday concert February 22 , 
 Smetana ' Vitava ' Dvorak fourth Symphony figure 
 programme . occasion soloist 
 Mr. Andrew Clayton . possessor fine 
 tenor voice , singing real artistic insight . — — 
 Mr. Joseph Lewis , deputy - conductor City 
 Orchestra , charge performance Schubert 
 * Tragic ' Symphony subsequent Sunday concert . 
 handle score capably , draw player 
 sound inspired reading work . 
 remainder programme , conduct Mr. Boult , 
 include Beethoven Scherzo Ravel * ' Mother Goose ' 
 Suite . — — Sunday concert March 14 offer 
 somewhat miscellaneous programme . Miss Joan Willis 
 play Tchaikovsky variation Rococo Theme 
 ‘ cello orchestra . good artist , Miss Willis 
 playing improve technically artistically 
 hear city . péanissimo note 
 special beauty , reveal capacity finely 
 emotional playing . Ravel introduction Allegro harp , 
 string quartet , flute , clarinet , Scherzo C minor 
 Frederick Morrison , . Mr. Sydney Lewis sing 
 song Korbay Handel aria extremely . 
 — — mid - day concert March 2 , Miss Beatrice 
 Hewitt Mr. Johan Hock Beethoven Sonata 
 composer sonata C ' cello piano- 
 forte . concert series Philharmonic 
 String ( Quartet play Schubert ' death maiden ' 
 ( quartet . Miss Kathleen Frise - Smith mid - day 
 pianoforte recital occasion , playing 
 Busoni arrangement Bach chromatic Fantasia 
 thing remember masterly style . Organ 
 prelude fugue D equally good . 
 attempt play Chopin find 
 fault general musicianship . interpre- 
 tation justice polish composer 
 Polonaise flat.——The concert Catterall 
 series March 10 . programme consist 
 Beethoven work — quartet G , Op . 18 , 
 C , Op . 59 , big quartet B flat , Op . 130 

 BoGNor.—Parry ' Pied Piper Hamelin ' 
 Stanford ' revenge ' Philharmonic 
 Society Theatre Royal recently , direction 
 Mr. Norman Demuth , contribute new 
 * Aubade ' programme . Mr. Albert Sammons 
 play Mendelssohn Violin Concerto 

 Macfarren * Day ' perform choir ef 
 | Derby Road Baptist Church March 11 , 
 | direction Mr. C. E. Blyton Dobson 

 Oxrorp.—The Rosé Quartet play Town Hall 
 |on February 25 , programme include Haydn ( Quartet 
 B flat ( Op . 76 ) , Beethoven ' Rasoumovsky , ' 
 Schumann 

 PENRYN.—The newly - form Choral Society come 
 recently performance ' Hiawatha Wedding- 
 | Feast ' credit conductor ( Rev. C , Daly 
 | Atkinson ) , singer , player . 
 | PLymMourH.—With help Royal Marines String 

 READING.—A successful concert 
 February 20 University College Orchestra Choral 
 Society , Mr. W. Probert - Jones . ' Jesu , priceless 
 treasure ' Vaughan Williams ' Old King Cole ' Ballet 
 Suite principal feature programme 

 RICHMOND ( YORKS).—The programme 
 February 16 Richmondshire Choral Society , 
 enterprising body direct Mr. Arthur Fountain , deserve 
 quote ( chamber work play 
 Yorkshire String Quartet ): Choral Hymns * ' Rig 
 Veda , ' Holst ; quartet D , op . 18 , . 3 , Beethoven ; 
 * Liebeslieder Waltzer , ' Brahms ; quartet F , Op . 96 , 
 Dvorak ; ' spirit England , ' Elgar ; ' Jerusalem , ' 
 Parry 

 SEAFORD.—Purcell ' Dido A‘neas ' 
 opera March 3 Seaford Musical Training Society , 
 conduct Miss Strudwick . work Miss Dorothy 
 Greenhill stage manager , Miss Mabel Marks dance 
 mistress , principal , chorus , orchestra 
 contribute successful performance reward 
 | singular local enterprise 

 music Wales 

 ABERYSTWYTH.—On February 18 , 
 ninety - fifth concert , hold University Hall , programme 
 contain Beethoven Trios — b minor , op . 9 , . 3 , 
 movement b flat , Brahms 
 Pianoforte Quartet g minor.——On March 4 , Dr. 
 Vaughan Thomas lecture student ' Schubert 
 Song - Writer . ' evening concert , University 
 Hall , College Quartet hear Schubert String 
 Quartet C.——On March 11 , College Choral 
 Society sing movement Brahms * Requiem , ' 
 College Orchestra play movement 
 Beethoven Symphony . 2 . Miss Sybil Eaton 
 appreciate performance Beethoven Violin 
 Concerto . Dr. David de Lloyd conduct 

 BANGOR.—College concert programme recently 
 include song Violin Sonata Madame Marthe 
 Servine , North Wales resident ; Pianoforte Quartet 
 Herbert Howells ; chamber music , include 
 Boughton ' Celtic Prelude , ' audience child . 
 Musical Club sixth concert Miss Isolde 
 Menges Mr. Howard - Jones play Elgar Violin 
 Sonata.——On March 6 Mrs. Gough Orchestra , conduct 
 Ceridwen Lloyd , play Haydn ' Oxford ' Symphony 
 Corelli Concerto solo violin , solo ’ cello , 
 string orchestra , St. Mary College 

 Phaudrig Crohoore ’ _ selection Haydn 
 * Seasons . ' 
 Dusiin.—The R.D.S. chamber music recital 

 member ’ Hall February 15 ( Catterall 
 Quartet ) , February 22 ( Esposito , Bridge , 
 twelvetree ) , March 1 ( Sir Hamilton Harty 
 Chamber Music Orchestra — brilliant performance , 
 include Esposito * Neapolitan ' Suite ) , attend 4,536 
 person , record hall.——Dame Melba farewell 
 concert Theatre Royal , February 20 , 
 huge dubliner invariably old friend 
 good send - , Melba ' farewell ' 
 exception . Mr. Lionel Tertis , Mr. Hackett - Granville , 
 Mr. Harold Craxton lend valuable aid 
 diversified programme.——dr . W. H. Gater preside 
 meeting Leinster Society Organists 
 Choirmasters , February 22 , Dr. H. G. 
 Smith read paper * German School Organ 
 Music . — — musical comedy , * Riquetto ' _ 
 performance Dublin week February 22 - 27 , 
 prior production London , 
 receive . — — O’Mara Opera Company occupy 
 board Gaiety Theatre short season commence 
 March | , open * Tannhauser ' close 
 week perennial * Bohemian Girl , ' 
 baton Messrs. Walter Meyrowitz R. Gasnier . 
 second week ' Lohengrin ' ( Mr. O’Mara farewell 
 appearance opera ) , * Rigoletto , ' * Maritana , ' ' Faust , ' 
 * Carmen , ' ' jewel Madonna , ' received 
 adequate treatment.——Dublin University Choral Society , 
 Dr. Hewson , _ fine performance 
 Beethoven Mass C , March 5.——-The Army School 
 Music . | Band sacred concert Theatre 
 Royal , March 14 , conductorship Col . Fritz 
 Brase , Mr. P. Duffy vocalist.——much interest 
 centre visit Cambridge University Singers , 
 Dr. A. H. Mann , interesting concert 
 Examination Hall Trinity College , March 16 , 
 include programme item specially compose 
 visit Dr. Charles Wood , work John 
 Dowland , Dublin , great irish Elizabethan , 
 draw . addition , instrumental selection , 
 include Brahms string ( Quartet C minor . man 
 seventy - , Dr. Mann display wonderful vigour ; 
 fiftieth year director choir 
 King College , Cambridge , old friend 
 Ireland 

 success 

 snow stili , fact spring 
 whisper bring home crowding 
 concert pinnacle season . Mischa 
 Elman draw customary thousand , , _ usual , 
 look impose tone 
 technique . Sophie Braslau , contralto 
 soul mind musician , 
 faultless programme , Edward Johnson , 
 Canada great tenor , appear young New 
 England soprano , Joan Ruth ( Metropolitan 
 Opera ) , costume excerpt opera Puccini , Verdi , 
 Gounod 

 news concern effort artist , 
 item — brilliant organ recital Edwin H. 
 Lemare . concert come New 
 Symphony Orchestra , Luigi von Kunits , talented 
 pianist — Evlyn Howard - Jones , 
 débit Beethoven ' Emperor ' Concerto , 
 young Pole , Mieczyslaw Munz , adequately express 
 music find Liszt Concerto 
 . 2 . orchestra enjoy variety experience 
 Berlioz ' Carnaval Romain , ' ' L’Aprés - midi d’un Faun , ' 
 ' Egmont ' Overture , beautiful , sensitive romance , 
 ' Temple Goddess , ' F. H. de Massi- 
 Hardman . recital , Mr. Howard - Jones dignified 
 musicianship find expression work Bach , Beethoven , 
 Brahms , Chopin , Debussy , Delius , Ireland 

 chamber music rely solely 
 Hart House ( Quartet , concert afford 
 feast modern work interesting fascinating . 
 Goossens Fantasy Macmillan Quartets 
 familiar , Ernest Bloch ' paysage ' novelty 
 like hear — distinctiveness 
 

 recital 

 Franz Steiner , viennese Lieder singer , introduce 
 ' novelty ' Franz Schubert — song entitle * Lied der 
 Abwesenheit , ' recently unearth 
 publish connection unknown 
 Schubert piece pianoforte solo . * Minuet ' 
 Schubert youth ; * Landler ’ time origin 
 unknown ; Waltz write 1826 , 
 appear collection print Vienna 1825 , 
 contain Waltz Beethoven . 
 Schubert work hitherto unpublished , manuscript 
 possession Schubert survive 
 grand - niece 

 return Jascha Heifetz , widely famed russian 
 violinist , long absence , prove admit 
 disappointment : truly phenomenal technical equipment 
 atone complete absence 
 feeling , Germans ' style , ' classic 
 composition . Heifetz reverse Eugen d ’ Albert , 
 pianistic technique negligible 
 quantity recent year . D’Albert gigantic conception 
 great classic unsurpassed grandeur , 
 fail dexterity come point 
 mar interpretation . notable pianist Alexander 
 Tcherepnin , young Russian serve interpreter 
 brilliant grateful composition . 
 composer , Tcherepnin ponder little problem , 
 allow imagination stream forth unaffected 
 intellectualism . native russian temperament afford 
 satisfying synthesis elegance style fluency 
 diction acquire France , country adoption . 
 young , eminently gifted , Erhard Kranz , 
 young musician covet 
 laurel organist . modern orchestra 
 advanced device colouring shading , offer new 
 scope organist modern idea , 
 artist use . PAUL BECHERT 

 College Music . day entry , April 26 

 String Orchestra London Academy Music 
 concert Hampstead Conservatoire February 18 , 
 play work Bach , Mozart , Beethoven , Mendelssohn , 
 Grieg , West , conduct Mr. Carl Weber . Miss 
 Madeleine Wermecke , pupil Mr. Charles Fry , recite 
 admirably Shelley ' Ode West Wind ' ; Mr. 
 Cecil Turner , pupil Mr. Weber , great promise 
 Grieg Pianoforte Concerto 

 Annnal Dinner musician ’ Benevolent Fund 
 place April 15 7 7.30 New Princes 
 Galleries , Piccadilly . Lord Darling chair , 
 speaker Mr. J. R. Clynes 
 Mr. Albert Coates . ticket ( 15 , 6d . ) 
 secretary , Mr. Frank Thistleton , 5 , John Street , 
 Bedford Row , W.C 

