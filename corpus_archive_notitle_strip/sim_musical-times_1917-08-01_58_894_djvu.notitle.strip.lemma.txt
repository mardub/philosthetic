academic mind be , however , of very varied type . 
 as in politic , there be conservative and pro 
 gressive ; there be group ; finally , there be one- 
 man academy . their common denominator 1 
 reliance upon a system which admit of be 
 embody in a code . every country have its 
 prominent conservative academic , but perhaps the 
 great proficiency in this generation have bee 
 attain in Russia as a sequence to the reaction 
 against the imperfect technique of the nationalist 
 pioneer , which be find to be an impediment to 
 free expression . it be significant that the composer 
 who , in his youth , be look upon as the hope of 
 the Nationalist and nickname ' the little Glinka , ' be 
 now the strong representative of academic music 
 in Russia . the circumstance that the most practical 
 guide to the writing of orthodox traditional polyphony 
 be a russian book be corroborative evidence , for the 
 demand create the supply 

 progressive academism seem for the present ( 0 
 find the french soil the most congenial in Europe , 
 largely in consequence of César Franck having make 
 his home in Paris . his position in musical history 's 
 a singular one . when Beethoven die his compatriot 
 be singularly obtuse in read his mus ! 
 testament , which may be describe as the problem of 
 unity in the cyclic form solve by mean of the 
 Beethoven variation . the german symphonist seem 

 to have evade the point altogether . it be leave to 

 Liszt - Wagner , whom we may treat zsthetically as one 
 composer , to resuscitate it in another field , which be 
 enough to throw german criticism off the scent 

 Rand lead to the Beethoven variation and the so 

 call metamorphosis of theme be regard as 
 antagonistic when they be identical . the attempt 
 to set up Wagner in opposition to the Beethoven 
 tradition , and represent the latter by Brahms , be one 
 of the humorous episode of i9gth - century music . 
 in the symphonic field it be César Franck 
 who first build securely upon the foundation 
 project by Beethoven , and this lead in France 
 to a form of academicism which , whilst liberal 
 and progressive in every other respect , be sternly 
 uncompromising upon this question of formal 
 construction . the distinguished head of the Schola 
 Cantorum accept innovation which fill the orthodox 
 mind with horror , but would turn away with an equal 
 disgust from an orthodox musical texture if it fail to 
 fulfil certain formal condition . in short , the motto of 
 the franckist tradition in France be loyalty to principle 
 combine with the utmost liberality in detail — in other 
 word , progressive academicism . but the same motto 
 with slightly less exclusive principle would apply to a 
 considerable proportion of modern french music out- 
 side of the Franckist tradition , for the working of the 
 french mind be nothing if not passionately logical . 
 even when it give the impression of jump at a 
 conclusion , the truth be often that the intermediary 
 process of thought have be omit as be 
 equally obvious to the listener and therefore not worth 
 state . a great deal of the unexpected in french 
 harmony arise in this manner , which do not modify 
 its fundamentally academic character 

 atypical instance of a one - man academy be Scriabin , 
 whose ingenious development of the use of chromatic 
 alteration lead to the creation of a personal code or system 
 regard which he become more uncompromising 
 than the most dogmatic of conservative . a capricious 
 theorist might even set up the hypothesis that he be 
 compel to standardise the harmonic stage he 
 have arrive at because further extension of the same 
 process would have bring he back to his point 
 of departure 

 whilst wish to give full credit to the effort that be 
 be make by english composer to develop on national 
 line and create an English School , which effort there be 
 much hope will be crown later with success ( in the same 
 way as the Russians have find themselves , musically 
 speak : the outcome of sixty year ’ earnest and intense 
 strive ) , whilst desirous of be both reasonable and 
 seasonable , it be useless to deny that the time have not 
 yet arrive when we can make good this important claim . 
 we have many charming salon piece , but there be no 
 special work of any special composer of which it can be 
 say , ' this be epoch - making — this be a masterpiece of 
 world - value that our own and future generation will study 
 with enthusiasm and teacher be thankful to teach 

 even assume that some such transcendent and planetary 
 work do actually exist on a parity with the classic ( which 
 Mr. Legge prefer to dub ' foreign ' ) , would it be possible 
 for the student to develop musically on one nation ’s output ? 
 z the present generation to be debar from the privilege 
 in mustcal existence that former generation have profit by , 
 include Mr. Legge himself ? would he have it that the 
 grandchild of whom he speak should be deny 
 the study of a Bach Fugue or a Beethoven Sonata , to 
 have withhold from they the joy of hear a Mozart opera , 
 or the mature happiness of play a Chopin Ballade ? 
 ( I take it that the latter composer be now taboo , see that 
 his birthplace have become ' enemyised 

 assume that we be at war with Spain or Holland , 
 should we deny our art student the benefit of inspiration 
 that should ensue from the study of Vandyck , Hobbema , or 
 Rembrandt ? should we say ' young man , and likewise 
 young woman , you will , in visit exhibition and gallery , 
 be expect to dodge enemy taint canvas , avoid a 
 direct gaze at any picture save those paint by native 
 artist . Cooper , Tadema , Stone , Lavery , Orpen , and the 
 like only be suitable for your vision at the present time , 
 when it be seemly that your artistic development should be 
 base purely on patriotic and local line 

 direction ) , with Sonatas by Locke , and some in the 
 handwriting of Purcell ( £ 29 ) , and the same composer ’s 
 manuscript of music for the Yorkshire Feast ( £ 32 

 organ book , date 1448 , by Adam Ileborgh , of Kendal 
 ( 421 , Mr. Tregaskis ) ; Mendelssohn ’s autograph copy of 
 ' a Midsummer Night 's Dream ' ( Scherzo , Notturno , and 
 Wedding March ) , ( 471 ) ; Mozart , autograph copy of two 
 Cadenzas for his Pianoforte Concertos ( £ 36 ) ; Johann 
 Sebastian Bach , autograph score of ' Allein Gott in der 
 hoh ' sei Ehr ' ) 498 ) ; Schubert ’s ' Salve Regina , ' autograph 
 score , sign 1819 ( £ 52 ) ; Beethoven ’s Grand Trio in 
 e flat ( violin , viola , and ' cello ) , Op . 3 , autograph ( £ 98 ) ; 
 William Byrd , ' psalme , sonet , and song of sadness , 
 make into musicke of five part , ' date 1588 ( £ 34 ) ; 
 Thos . Ravenscroft , ' deuteromelia , ' date 1609 ( 416 ) ; 
 Coperario ( Giov . ) and Thom . Nupo , fancy , in five part , 
 twenty - four by G. Coperario ( otherwise John Cooper ) and 
 twenty by Thomas Nupo , manuscript , 228 page 
 ( £ 40 , Quaritch ) ; Dr. Wilson , music to song in ' the 
 tempest ' ( manuscript copy by Playford ) , ( 458 , Maggs ) ; 
 1 cambric hand - rufile ( 44 - in . by 10 - in ) , the only fragment 
 uf Handel ’s clothing extant ( £ 10 10 

 the whole - TONE SCALE 

 the GUILDHALL SCHOOL of MUSIC 

 a concert give at ( Queen ’s Hall on July 3 afford 
 incontestable evidence of the value of the instruction afiorde 
 by this establishment . the programme be a long one , 
 and if we elect to mention the excellent performance of Miss 
 Dorothy Davies and Miss Kathleen B. McQuitty of Saint- 
 Saéns ’s variation on a theme by Beethoven , and that of 

 on July 14 the first performance in England be give , 
 a string quartet in a minor compose by Signor Scontrip 
 the principal of the Florence Conservatoire of Music , 
 turn out to be an important contribution to chamber mug , 
 about which we hope to have something to say after a secon 
 hearing , which we trust will be afford during the autum 
 season 

 remain to be deal with , however , a few concert that take | ' Elijah ' on March 23 

 lace prior to the instrumentalist ’ temporary disbandment . 
 he chief interest of the sixth summer Symphony Concert 
 be centre in the first Bournemouth performance of 
 Beethoven ’s early Pianoforte Concerto in D , a posthumous 
 work that be write by the composer when he be but 
 fourteen year old . detailed criticism of such an immature 
 composition would serve no useful purpose , but one can 
 faithfully avow that its dulcet and easily comprehend 
 strain , reminiscent though they be of Beethoven ’s early 
 exemplar , Haydn , be by no mean lack in pleasurable 
 quality . this appreciation , again , be very much enhance 
 by the exceedingly polished and musicianly reading of the 
 solo part by Miss Edith Ashby , the highly efficient Winter 
 Gardens accompanist . the programme also include Arthur 
 Somervell ’s ' Thalassa ' Symphony ; the Overture , ' L’Epreuvg 
 Villegoise , ' by Grétry ; Sibelius ’s well - wear Valse Triste ; 
 and the Goring Thomas aria , ' my heart be weary ' — this 
 charming number be sing in very good style by 
 Miss Nellie Walker . a decidedly attractive programme , 
 which include Haydn ’s Symphony No . 2 , in D , 
 Ewna ’s ' Hans Andersen ' Overture , Svendsen ’s ' norwegian 
 Carnival , ' Prowinsky ’s ' a passing serenade ' ( orchestrate 
 by Sir Henry Wood ) , and the ballet music from 
 Borodin ’s ' Prince Igor , ' be arrange for the follow 
 week . two soloist also appear — a violinist , Madame 
 van den Eeden , who play Beethoven ’s romance in F 
 tastefully but with a somewhat small tone ; and Miss 
 Winifred Williamson , who score a success with Ambroise 
 Thomas ’s aria , ' connais tu le pays . ' her interpretation , 
 however , would have be still more impressive if the 
 thythm have be make more elastic , and if she have adhere 
 a little more closely to tradition . on July 4 , Mr. F. 
 King - Hall , leader of the Orchestra , conduct in the absence 

 of Mr. Godfrey , the programme be as follow : overture , 
 ' Cosi fan tutte ' ( Mozart ) ; Symphony in D _ minor 
 ( Schumann ) ; suite no . 1 , ' Peer Gynt ' ( Grieg 

 the Bristol Musical Club , notwithstanding the absence of 
 several member on service , have have a very successful 
 season musically . many excellent programme have be 
 forthcoming , and that which bring the season to a close 
 on July 17 include a Pianoforte Quintet in D by Fauré , 
 Paderewski ’s Sonata in A minor , for pianoforte and violin , 
 and violin solo by Tchaikovsky and Kreisler . the 
 meeting be to be resume on September 25 

 the Ladies ’ Musical Club , which be found in 1906 by 
 the Misses Fyffe , and hold its meeting at the Royal West 
 of England Academy , have have a very satisfactory season , the 
 membership having reach its high level ( 140 ) , and the 
 programme of chamber and other music having prove most 
 enjoyable . the outstanding event be the engagement of 
 the London String Quartet , the concert result in a small 
 balance in hand . there have also be lecture by Miss 
 Denneke on Beethoven and Schumann , with musical illustra- 
 tion . the season close on June 29 , and when the 
 meeting be resume in the autumn it will probably be 
 decide to invite the London String Quartet to give a second 
 concert 

 DEVON and CORNWALL . 
 DEVON . 
 during June the Torquay Municipal Orchestra be on 

 and a madrigal class , Plymouth be destitute of choral societig 
 for public purpose 

 the Band of H.M. Grenadier Guards , under Capt . 4 
 Williams , have tour Devon , play ( as already note ) g 
 Exeter , at Torquay on July 6 , Plymouth on July 7 and§ 
 and Exmouth on July 9 . immense crowd flock to th 
 Pier Pavilion at Plymouth for the four concert . the band 
 which have very recently return from a four month ’ sojoun 
 in France , play with its accustomed finish and perfectiq 
 of ensemble and tone effect . the principal item we , 
 one of Grieg ’s Lyric Suites , Op . 54 , Beethoven ’s ' Leonon 
 Overture , No . 3 , the ' Finlandia ' tone - poem ( Sibeliny , 
 Schubert ’s ' unfinished ' Symphony , and a suit 
 * Esquisses Caucasiennes , ' by Ippolitov - Ivanov . at Devon 
 port , on July 5 , Gloucester Street Ladies ’ Choir give , 
 concert for church fund conduct by Miss Davey at th 
 organ ; and on board H.M.S. ' Indus , ' on July 10 , the bo . 
 artificer give a concert . a group of fifty child perform 
 the operetta ' Fays and Elves , ' patriotic song and tableau , 
 and characteristic song and dance , at Peverell ( Plymouth ) , 
 on July 11 

 CORNWALL 

