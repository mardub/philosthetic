

 notice Hereford Musical Festival ( p. 688 | 
 October issue ) need amplify far | 
 regard special opening service cathedral | 
 Sunday afternoon , September 9 . impressive | 
 service begin Sir Alexander Mackenzie 
 ' song thanksgiving , ' orchestral Suite 
 ' London day day ' ( Op . 64 ) . lento espressivo 
 movement — admirably suited th 1 € | 
 solemnity cathedral — date ' June 1 , i902 . ' on| 
 afternoon day news reach England 
 peace proclaim South Africa 
 terrible war country . ere nightfall | 
 glad day composer conceive plan 
 ' Song thanksgiving ' begin work : 
 piece genuine product feeling 
 moment , time , adequate 
 expression . order complete record , 
 add Handel Overture ' Samson ' 
 Beethoven ' hallelujah ' chorus perform 
 special opening service Hereford Cathedral 

 occasional Wotes 

 hear bell Christmas Day , 
 old , familiar carol play , 
 wild sweet 
 word repeat 
 peace earth , good - man ! 
 think , day come , 
 belfry Christendom 
 roll 
 unbroken song 
 peace earth , good - man ! 
 till , ring , singe iis way , 
 world revolve night day , 
 voice , chime , 
 chant sublime 
 peace earth , good - man ! 
 long fellow 

 typically Hans von Biilowic anecdote 
 Mr. John Francis Barnett relate book ' musical 
 reminiscences impressions ' ( Hodder & Stoughton 
 review p. 827 . Biilow pianoforte 
 recital lady pass close , way 
 seat , finish Introduction 
 Beethoven Sonata Pathétique . annoy 
 redoubtable Hans purposely adapt 
 speed 4//egro pace fair footstep . 
 lady realize joke 
 perpetrate expense , hurry 
 place quickly short running , 
 Biilow opportunity accelerate 
 ais pace , result , Mr. Barnett 

 Adagio 

 R. Woodward 

 December 23 , 1806 , Beethoven Violin concerto 
 publicly perform . interesting event 
 |took place Vienna concert Franz 
 | Clement , - know virtuoso principal violin 
 Theatre der Wien , , strangely 
 , record exist . Sir George Grove 
 : ' evidence , 
 assume Beethoven habit postpone 
 bespeak work , write 
 hurry Clement play rehearsal 
 sight . ' autograph score preserve 
 Imperial Library , Vienna , entitle 

 Concerto par Clemenza pour Clement , primo Violino e 
 Direttore al Teatro Vienna 
 dal L. v. Bthvn , 1806 

 Saturday Tschaikowsky ' 1812 ' overture 

 perform 
 hus forecast London daily newspaper . are| 
 look - similar announcement 
 Monday Beethoven Pastoral Symphony 

 desire earnest 

 doubtful organize opposition 
 - know story . event triumph 
 poor Cornelius . ' artist , 
 enthusiasm . Liszt act 
 altogether incomparable manner ... . 
 Grand Duke receive yesterday , extremel 
 gracious , encourage , prophesy fame 
 course interview Liszt remark : " 
 Royal Highness , Cornelius noble fellow " ( e 
 nobler Mensch ) . Grand Duke shake hand 
 parting friend 

 scandalous treatment friend work 
 Liszt wash hand thing theatrical Weimar . 
 public appearance place December 17 
 day ' Barber ' Aremitre connec 
 tion festival concert honour Beethoven ’ 
 birthday . Cornelius write fine prologue 
 occasion , Liszt conduct Beethoven * Weihe 
 des Hauses , ' major Symphony , ' 
 demoniac force scarcely dare look .. 
 performance 
 hear 

 Liszt retirement public life Weimar , 
 ratson d'étre Cornelius presence 
 virtually disappear . leave place 
 immediately , short stay native town , 
 Mainz , arrive April 12 , 1859 , Vienna , 
 hope living teacher , find time 
 inspiration composition second 
 opera , ' Cid . ' Vienna hold year 
 great event day 
 acquaintance Wagner , induce exchange 
 austrian capital Munich , Cornelius 
 teacher Conservatorium , salary 
 fix salary — 1,000 gulden . 
 expect , letter deal 
 Wagner party , include young King 
 Ludwig , interesting 
 volume , exigency space forbid 
 quote 

 title - page , ' useful performer , instrumental 

 Methuen & Co. ] |as vocal . ' Signor Giannandrea Mazzucato , Grove 
 interesting inform volume | ' Dictionary Music Musicians ' ( ses voce tosi ) : 
 issue week gifted author to/| ' practical treatise singing , aged 
 ‘ bey summon great reaper Death . | teacher embody experience 
 book invest pathetic interest , contemporary , time art probably 
 t need adjunct strengthen importance thoroughly teach . 
 valuable book bell countrv . | remark highly useful . 
 fourteenth year Rev. Dr. Raven devote music - morrow , study . Lawrence 
 study bell , result life - long observation , | Gilman . pp . 144 ; 45 . 6d . net . ( John Lane . ) 
 wit rea g ar personal inve igation mad ; mph ny writer ethoven . Felix 
 deat t seventy - ti e , ¢ test authority « ? . . , Tee 
 . N , f seventy - , grea thority | Weingartner . German Arthur Bles . 
 t sul oo t 1e Ss ov que . js penne es 
 . — ae great request a| portrait . pp . 163 . ( William Reeves . ) 
 lecturer bell bell - ringing — - . ' : . 
 } — . theory mustc jor student teacher Dr. J. 
 country enthusiasm know bound — instance , . . " $ 
 ale Tennis m Decline ol Muti Hendail tn stan ecet tien Lightfoot . pp . 263 ; 2s . net . ( Ralph Holland & Co. ) 
 ver rtv vear labour hou St. Martin Street , chronicle th 
 rhe preset lume , write readable style , | 2¢7vey fam Constance Hill , illustration | 
 hat fail rove attractive are| Ellen G. Hill , ea reproduction portrait , & c. pp . 
 interested campanology . early reference , 366 ; 21s . net . ( John Lane . ) 
 book , Burney ' Elistory John Mason Neale , D.D. , Memoir . Eleanor 
 f Music . ' later mt e Fire - Bell Consorte,’| A. Towle . pp . xiv . 338 : Ios . 6a . net . ( Longmans , 
 mpose old John Jenkins , , ilso | Green & Co. ) 
 ' Tennyson ' chime Sir John Stainer compose Vozart . Beethoven . Friedrich Kerst . translate 
 f + } = f - . } le * ol , " . " pe . i. " . 
 bell Freshwater Church Isle Wight . | jnto English , additional note , H. E. Krehbiel . 
 " | : _ _ ae } . : Fone SBE , . ' : . / ' > 
 rious gin Cambridge chime , familiar to| pp . 143 110 . book 4s . 6d . net . ( Gay & Bird . ) 
 londoner | tone Big Ben , detail . 
 rich varied information page — | } rns 
 word ' carillon ' find Dr. Johnson | 
 dictionary ; tl e€ pew system early Stuart | SACKBUT 

 period ; ut require - man ring bell 
 r ~ ' c ° . ° 
 present Canterburv Cathedral Prior Conrad the| _ . meeting Musical Association hold 
 r2tl soraner ¢ j n — fact . hardl , | November 20 , Rev. F. W. Galpin read paper 
 p cen ry ind tact , nere 31s hardly page | pte . ° ° . 
 fail learn interest , | ' 2 ! sackbut , evolution history ; illustrate 
 humour Oo absent Dr. Raven excellent | imstrument 16th century , specimen . ' 
 volume : specimen , an| lecturer begin suggestion 
 hustration . © ' Occasional Notes ' ( p. 811 ) | | origin t instrument , 
 = " < ow ? 7 " t ee | ) rer > ace > » & 
 conclusion , heartily commend book reader ; | ™ osts nerally accept present time spanish 
 sul matter ie , copious _ illustratior .| Sacar del buche — ' exhaust chest’—alluding 
 ; ' Bells Englan | effort require blow sackbut slide - trombone , 
 present nvone : rest instrument pr : actically identical . 
 shlw ts puerile . word Sacabucke Spain use 
 — form pump , apparently early application 
 o » uctin Felix Weingartner , translate | word . sugge sted whilst 
 Ernest Newma Breitkopf & Hartel word derive spanish sacar , draw — 
 WW Berl . : identical latin dzxrss , pipe , originally 
 rand r wt quently c¢ ‘ : ° , : . " » * 
 oe ,   . , W omens . " " © | boxwood , classical time apply pipe 
 u ( iter igner Dis ! la , . . . ~ 
 " esas . material . derivation support portuguese 
 np — s bear sam tl s nder notice . f 1 sane ' d : ' 
 : ; rs | orm Sacabuxo , : word simply mean ' draw - pipe , 
 ; ; cot allude pump - pis ston movement 
 pres¢ ne cons ntiy nis remark ire " " 3 . . 
 : : ' weit : , ff slide . great confusion cause 
 r resul ( instructiv yract 1 , . " 9 ° . ° . 
 og 4 ee mistaken identity Samdéu 
 instar ' nat mposer * n think } h 
 : } tirhe . . . small asiatic lyre Latin 
 . core und , * * : 5 Coeef f 
 . . Sambuca \ word , savz+ucus , Latin 
 wor W composer wante la | : 
 elider - tree , pithy wood pipe 
 : . nstructe , Saméuca wes consider medizval 
 « $ page ar wever , devote weak ; iam : J ' 
 ) ' — — ' writer wind instrument . - illustration 
 « gift nductor , Hans v Biilow , tendency 1 , . } > } 
 . sackbut trombone Boulogne goth century 
 ex erat Germar m } ippeare 1896 . : : " ae . 
 ] ; , P pr ae . Biil ; Psalter merely imaginary 
 ar author find ' little Bilows , s . . . 2 2 eos 
 te ge * s com bh sese ' , Sambute , old french form Saméuke , asiatic lyre . 
 ( d , lack genius famous Hans , wer . , 4:3 
 : : reason present serve slide 
 rely imitate eccentricity present day , . 
 , ; , , | principle trombone know 
 wher ere gifted conductor , Weingartner | 5 , k d } , . Jat P 
 , é ‘ , Romans : specimen find Pompet 
 remark ra f - date , , frank , . . 4 : 
 : : : George III . forthcoming , 
 ( yncerning Biilow , spitefu ne 
 , probably spec imen large Auccina , calle 
 en Italy Zremba grande , Trombone , 
 oo receivi specimen exist National Museum Naples . 
 WV . Reminiscen wd Impression John suppose quotation Apuleius ( A.D. 160 ) , 
 Jarnet Illustrated . pp . xvi 341 ; 10 . 6a . net . | Grove Dictionary ( s.v . Trombone ) , simply 17th century 
 er & Stoughton . ) review p. $ 27 . gloss Fortunatus Sacchi imagine Apuleius 
 - — , ; mean : original , speak form 
 na vwusic . anthology . edit Duncan : o- th 4 . 8 ee tery 
 t ma ll pp ' - " 62 ent reed pipe . latin phrase Tuba ductilis — apply 
 ADA cu lou 1 g p. xxx. 493 2 . vl . el . ° ° 
 HW ) 3 e trombone medizval writer late date — 
 " insor » ¢ , 
 instance imply trumpet draw ¢he hand , 
 ton teachin , > lhe ] ac . oe , - ! 
 Lh _ ge , Wilhelm Carrie | trumpet metal draw ¢he hammer 
 Kylau . pp . 155 . ( R. Voigtlander Verlag Leipzig . ) distinct trumpet cast metal , horn , wood . 
 observation mit florid ng r , nliment thé sackbut appear 14th century , 
 : t tern singer . Pier Francesco Tosi , | derive folding tube attachment 
 translate English Mr. Galliard . pp . xix . 184 ; | slide long straight trumpet Busine Boccine 

 LONDON SYMPHONY ORCHESTRA 

 series concert , conduct Dr. Hans Richter , 
 inaugurate ( ueen Hall November 5 . 
 second November 19 , Sheffield Choir 
 co - operate Orchestra , Beethoven ' Choral ' 
 symphon occasion memorable , Queen 
 Duke Connaught present , audience 
 remarkably representative . Orchestra play 
 superbly ; honour evening fully 
 share Sheffield Choir , , singe 
 magnificent volume tone precision Symphony , 
 rendering ( Dr. Henry Coward direction ) 
 Bach mighty motet , ' sing ye Lord , ' 
 — remarkable performance rouse audience 
 enthusiastic evidence appreciation . vocal quartet 
 consist Miss Perceval Allen , Miss Alice Lakin , 
 Mr. John Harrison Mr. Ffrangcon - Davies 

 t hese 

 P. A. Heise , danish 
 composer , die 1879 

 r tot ° : = e ; 
 834 MUSICAL TIMES.—DEcEMBER 1 , 1906 . 
 PIANOFORTE recital . Sophocles , Beethoven ' Eroica , ' tragic Overture 
 . : C minor Ignaz Briill , celebrate sixtieth 
 ve 5 M. Godowsky play notable little | ; . othe . . - . 
 1n . " r5m.g oe ky play fi — eaaenat birthday . real pleasure occasion able 
 rie y Scriabine , efiective arrangement ae ; ? : 2 
 gg ge geug : BoP ge " 5 | , long period , admire Briill 
 hnimselil ol sara ande , hig audon , p\ Tenuet anc yianist b - > t } Ov " n ’ e fl Concerto 
 Tambourin Rameau.—Another fine player Sige EO ea creeeangy phen -a ow = 
 » Herr Buhlic . play .Kolian Hall November 12 Opera passing novelty , ' polish 
 oy % h ee tl : ' eae 7 @ fee Jew , ' Erlanger . subject fresh , 
 2 programme distinguish hi ab : \ 
 nos ie t ol yee ey s oft tino dente year ago opera Weiss , 
 " ¢ tion old modern composer , reading | ¢ : ; : - 7 
 - ina lar -y compose : < fer sexi able czechish composer . music present 
 characterize insight earnestness . — notable advance 
 ee > cesta french composer new , failure 
 Miss Vera Margolies Hall . ; ' : " er 
 n Mr. H , : , arr Pear new , pleasing refined ballet , ' Marionettentreue , ' 
 oward mes 1 e congratulate . ° ¢ 
 sae : hi lay | gifted , unfortunately blind , composer , 
 t signil > vivacity oo dhe | oo RP < eae . 7 : rg 
 nc vow . B , ; hi il : c er Oe hee 7 Rudolf Braun , pleased spite weak action , 
 wove t 4 secnsteir li ; , oO ctobe 20 , ° ¢ ° p 
 : em rs LP og : ' iad Seetteed : amnall tl accept repertoire 
 tein irgo urthne ! O . > 
 nla 3 ; , S Sexi f vi Court Opera . Braun win good reputation 
 terpretalive gilts Varied s Stion 1eces . 7 . © : . " 
 — s > ee ee oe " excellent chamber - music . Gustav Mahler 
 distinguish conductor , performance 
 , > m > 9 ore tt } > « . > - > ? t } > 
 pianoforte viola recital Mr. York Bowen Hermann Goetz opera ' Taming Shre 
 Mr. Lionel Tertis October 30 " Zolian Hall , | beautiful , sincere music work , wonderfully 
 play time second movement poetical staging , careful study 
 s suite al instrument , compose | vocal , instrumental , especially dramatic portion 
 ; , . Oe - - _ nies " = « 4 - enti Tetenl 
 Mr. Benjamin ] . Dale , prove musical essence produce unity rare elevating kind . 
 create desire hear remain number | true zeal , Jubilee Theatre cultivate opera , 
 work . programme contain Mr. York bowen’s| nd performance work Mozart 

 clever Sonata . 1 , viola pianoforte 

 Bruckner lifeti work receive 
 - sided , noisy , ostentatious applause , merit 
 recognize quiet spirit , true 
 stand time | } h 

 luctor Philharmonic concert appear 
 turr Felix Mott ] conduct Schumann c major 
 symphony [ ans Ptitzner incidental music Kleist 
 drama , ' Katchen von Heilbronn , ' create 
 deep impr n. hand , Schalk lucky 
 idea Beethoven great fugue b flat ( Op . 133 

 wchestra , proceeding justify 

 Concert Society hear Handel Concerto grosso 
 E minor , verture Max Schillings — 
 modern kin phonic Prologue — ' ( dipus rex 

 Beethoven 

 direction eminent conductor , Alexander von Zemlinsky , 
 meet marked success . rendering ' 
 Marriage Figaro ' masterly ; ' magic flute ' 
 * Don Juan , ' soon follow , good 

 Examination Hall College November 8 

 Miss Madge Murphy violinist , Mr. Herbert Walenn 
 violoncellist , Dr. Laurence Walker pianist . 
 programme comprise Sonata pianoforte 
 violoncello Saint - Saéns ( op . 32 ) , Corelli ' La folia , ' 
 violin , piece Massenet Godard violoncello 
 Beethoven Pianoforte trio ( op . 11 ) . Miss A. C. Kemp 
 sing song Marcello , Ponchielli , Stanford ' 
 miniature ' ( Op . 77 ) . Dr. Walker contribute solo 
 Brahms variation theme Schumann ( Op . 9 ) . 
 work perform _ thoroughly artistic 
 manner , concert excellent opening 
 promise successful series 

 MUSIC BIRMINGHAM 

 Dublin Orchestral Society , conductor Dr. Esposito , 
 fourth concert year presence large 
 audience November 14 . occasion deserve special 
 notice , Beetloven ' Eroica ' symphony , rarely hear 
 Dublin , perform Society time . 
 programme include Humperdi>:k Vorspiel 
 * Hansel und Gretel , ' Saint - Siens Prelude ' Le Deluge , ' 
 ballet - music second act Verdi ' Aida , ' 
 impressive performance Wagner ' Tannhauser 
 overture . excellency Lord Lieutenant 
 Countess , Aberdeen honour society 
 presence occasion 

 October 25 Royal Irish Academy Music 
 concert chamber music Antient Concert Rooms . 
 member chamber music class 
 Gade Pianoforte trio F , Beethoven String quartet 
 C minor ( Op . 18 , . 4 ) , Rheinberger Pianoforte 
 quartet e flat . young people 
 reflect credit , u 
 professor , Herr Bast 

 Miss Nora Thomson violin recital Antient 
 Concert Rooms November 19 , play Jensen 

 Suite Moderne , ' Max Bruch ' swedish Dances , ' Spohr 

 Gesangsscene , ' Beethoven romance F , 
 | Brahms hungarian dance . Miss Sophie Allen 
 accompanist , Mr. Denis O’Sullivan sing song 
 |in usual attractive manner 

 ks 

 musical treat high order . November 19 
 | Amateur Orchestral Society provide friend 
 | capital feast music , guidance Mr. T. H. 
 |Collinson , rendering maintain good 

 tradition Society . Beethoven fourth Symphony , th « 
 ' Midsummer Nigt Dream " music , overture 
 * Anacreon ' ' Merry wife Windsor , ' 
 chief feature enjoyable programme , song w 
 agreeably contribute Mr. William Howorth 

 Classical Concerts long honourably associate 

 Mr. James Pattinson , creditable appearance 

 October 30 programme include Beethoven 
 ' Egmont ' overture , Haydn ' clock ' symphony , 
 composition Cowen , Gounod , . 
 concert Mr. Cullen Choir , new organization 
 line Glasgow Select Choir , place ot 
 October 30 , respect successful . 

 choir consist sixteen - train vocalist , voice 
 blend excellently tone fine quality , 
 conductor skill experience choi 
 | capable artistic work 

 occasion time Scotland . feature | Hubert Parry express desire ' love casteth 
 concert fine singing Miss Grainger Kerr . the| fear ' repeat 
 chamber concert organize Pollok- | compose new work occasion 

 shields Philharmonic Society November 5 
 12 respectively , programme sustain 
 Verbrugghen Quartet , delightful rendering 
 quartet Mozart Tchaikovsky , Beethoven 
 Serenade Trio D string . Miss Ailie Cullen , 
 act accompanist , play acceptance pianoforte 
 Schumann ( Quintet ( Op . 44 ) 
 opening concert Choral Orchestral Union 
 n November 13 , Dr. Cowen 
 Scottish Orchestra welcome great heartiness 
 large audience . personnel band remain 
 unchanged , Mr. Henri Verbrugghen occupy 
 post violin . addition - know number 
 ' Midsummer Night Dream ' 
 Tchaikovsky fifth Symphony , programme include 
 Coleridge - Taylor symphonic variation 
 african air , Sibelius tone - poem ' Finlandia , ' 
 receive sympathetic interpretation hand Dr. 
 Cowen . Madame Kirkby Lunn contribute song , 
 ind highly dramatic rendering Saint - Saéns 
 * La Fiancée du Timbalier . ' 
 event worthy notice pianoforte 
 recital Messrs. Matt . Lowson Alfred Graham 

 overture 

 s2 

 include Beethoven ' Choral ' symphony , Miss Perceval 
 Allen , Miss Edna Thornton , Mr. Webster Millar Mr. 
 Fowler Burton soloist , Sir Hubert Parry ' Pied 
 Piper Hamelin . " characteristic music 
 work eflectively interpret Dr. Cowen direction 

 revival city Richter concert , 
 interval year , quicken interest public 
 unusual extent , Dr. Richter place 
 Philharmonic Hall November 13 
 exceedingly warm greeting . programme include 
 Tchaikovsky ' Francesca da Rimini ' symphonic poem , 
 Beethoven C minor Symphony 

 Symphony Orchestra second concert , 
 November 12 , Tchaikovsky ' Romeo Juliet ' overture 
 perform , Mr. Albert Garcia singer 
 evening . Misses McCullagh 
 successful concert November 19 , 
 assist Miss Lillie Wormald . Welsh Choral Union , 
 conductor Mr. Harry Evans , splendid rendering 
 Coleridge - Taylor ' Hiawatha ' November 17 , Miss Louise 
 Dale , Mr. John Harrison Mr. Charles Tree 
 soloist 

 ful flute playing Mr. V. L. Needham 
 col ues eighth number suite — | 
 desicnate ' Badinerie’—achieved movement | 
 rare ncession encore . Mr. John Coates sing | 
 ' Elizabethan Pastorales ' compose } 
 Dr. A. Herbert Brewer 

 Se‘ior Sarasate centre interest afternoon 
 reci Gentlemen Concerts November 7 . 
 Seficr Sobrino play capably pianoforte solo , | 
 join Sefior Sarasate performance 
 * kr ver ’ sonata . Friken Theodora Salicath , vocalist , 
 song native Scandinavian . 
 Brodsky Quartet — Dr. Brodsky Messrs. Briggs , 
 Spee!man Fuchs — commence series chamber 
 concert October 24 . programme contain 
 yy Ottokar Novdéek , e flat ( op . 10 ) , Schubert 
 posthumous quartet G , Beethoven Trio ( op . 1 , 
 . 1 ) . Mr. Egon Petri join Dr. Brodsky Mr. Carl 
 Fuchs performance Trio . Novacek , die 
 year ago age thirty - , viola player 

 Dr. Brodsky Quartet Union Leipzig 

 Lady Halle second concert } 
 Quartet , November 21 , receive warm | 
 Manchester welcome . Dr. Brodsky play 

 Spohr duet violin D ( Op . 67 ) . quartet , | 
 play , Schumann F ( op . 41 , . 2 ) , | 
 Beethoven b flat ( Op . 130 ) . Mr. Max | 
 Mayer chamber concert November 19 , | 
 colleague Madame Marie Soldat ( violin ) 
 Percy ( violoncello ) performance Grieg 
 pianoforte violoncello minor ( Op . 36 ) , 
 ss Sonata pianoforte violin G ( Op . 78 ) , 
 ven Pianoforte trio D ( Op . 70 , . 1 ) . 
 specially play . Mrs. Max Mayer 
 calist , song — manuscript — 
 sition concert giver . 
 umson " perform Mr. Brand Lane second 
 iption concert November 10 . choir sing 
 excellently . principal Madame Conly , 
 Miss Alice Lamb , Mr. Herbert Grover , Mr. Robert 
 Radford . service Miss Meta Diestel ( vocalist ) , Mr. 
 Leonard Borwick ( pianoforte ) , Mr. Theodor Spiering 
 ( vic Mr. Carl Fuchs ( violoncello ) , available 
 opening concert Schiller - Anstalt October 27 . 
 Mr. Spiering capable executant professionally engage 
 Chicago . artist remarkably fine 
 rendering Brahms Pianoforte trio C minor 

 second Promenade Smoking Concerts , 
 Mr. S. Speelman conductorship , place November 3 . 
 programme occupy example 
 light french school , second devote 
 Wagner . Madame Effie Thomas Mr. Webster Millar 
 vocalist . Edward German ' Gipsy Suite ' 
 place programme follow concert , November 
 17 , Tarantella movement rapturously encore . 
 brilliant execution Mr. Josef Greene — play 
 movement Rubinstein Pianoforte concerto D minor 
 Liszt Polonaise E — produce great impression . 
 Mr. Fowler Burton vocalist 

 MUSIC NEWCASTLE DISTRICT . 
 ( correspondent 

 public orchestral concert season 
 hold October 26 , programme include 
 Beethoven seventh Symphony Brahms variation 
 theme Haydn , nobly perform Halle 
 Orchestra Dr. Richter . item Weber 
 * Euryanthe ' Berlioz ' Carnaval Romain ' overture , 
 Liszt ' Les Préludes . ' Miss Evangeline Florence 

 vocalist 

 Mr. Wall associate perform Stanford Piano- 
 forte quintet D Newcastle Musical Society 
 concert October 30 . Classical Concert Society 
 meeting November 12 programme devote 
 Beethoven Schumann . Pianoforte trio play 
 Madame Marie Fromm , Miss Elsa Wagner , Mr. Willy 
 Lehmann ; striking feature evening 
 impassioned artistic singing Fraulein Meta Diestel , 
 contribute Beethoven Gellert song ( Op . 48 ) 
 Schumann song - cycle ' Frauen Liebe und Leben 

 November 7 rebuild Victoria Hall Sunderland 

 November 6 , Mr. Rohan Clency violin recital ; 
 November 7 , Herr Kreisler recital over- 
 flowing enthusiastic degree ; November 14 
 Mr. Maynard Grover pianoforte recital , 
 assist Mr. Robert Radford , receive 
 hearty welcome native city 

 Leicester Symphony Orchestra concert 
 October 26 , programme include Beethoven 
 ' Fidelio ' overture , Mozart G minor Symphony , Jensen 
 Suite , German ' Nell Gwyn ' dance . vocalist 
 Miss Justina Keightley Mr. George Pochin ; violin 
 solo contribute Mr. J. Colin Muston , leader 
 Orchestra , bassoon solo Mr. J. W. Bird . 
 Orchestra , seventy performer , ably direct 

 Mr. J. A. Adcock 

 epic peasant , reveller , ‘ r , fairy , demon , xc . , 
 rea fidelity h aying orche 
 w le successful , save March Ballet music , 
 admirably render . soloist Madame 
 ( , Mr. Henry Brearley , Mr. Charles Tree Mr. 
 Kk . Charleswor 
 MUSIC YORKSHIRE . 
 RRI NDENT . ) 
 LI 

 music Yorkshire long time | 
 way summer r ss , till near end | 
 f October occur deserving record . | 
 October 24 e Bohemian Chamber Concerts begin 
 eightl eason , programme _ includin quartet | 
 Beethoven ( Op . 59 , . 2 ) , Grieg Haydn , | 
 enjoyable performance Messrs. Elliott , | 
 Wrig Moxon Bolton . striking proof success | 
 unconventional ' smoking concert ' afford | 
 fact series similar line organize | 
 ' Rasch ' Quartet ( Messrs. Rasch , Drake , Haigh | 
 Giessing ) , open season November 7 
 prospect success . season propose | 
 Beethoven - ' posthumous * quartet , 
 ( Op . 127 ) chief feature concert . on| 
 October 27 Municipal Concerts , found 

 visit 

 October 20 Permanent Orchestra resume 
 proceeding concert programme 
 consist english french music , composer 

 represent include Elgar , Cowen , German , Bizet 
 Guilmant , interest centre chiefly symphonic 
 ballad Mr. Ernest Blake , entitle ' far madding 
 crowd , ' couple Dorset ballad tune 
 serve material use exceptional skill , 
 construction attractive work artistic 
 interest . creditably play Mr. 
 Blake direction , Mr. Allen Gill conduct rest 
 concert . subscription concert November 2 
 orchestral music . fine performance Schumann 
 D minor Symphony , Dr. Richter direction , 
 Hallé Orchestra , Mr. Backhaus brilliant 
 artistic reading Beethoven fourth Pianoforte concerto . 
 Miss Esta d’Argo vocalist . November 20 Mr 

 Ilerbert Johnson , young gifted pianist , recital 1 

 MUSICAL TIMES.—DeEcEmner 1 , 1906 

 ief con d chorus - singing splendid colour / programme devote exclusively Mozart 
 , th . force . subscription ccncert appearance | Beethoven . connection unveiling 
 * » Lady Hallé Mr. Leonard Borwick , Mr. Plunket | monument Albert Lortzing , Theater des Westens , 
 Mr Gree vocalist , November 20 , event | October 27 , revive long - lose fairy opera , ' Die drei 
 1 Halifax Orchestral Society , Mr. Van } Rolandsknappen . ' work originally produce 
 Dvk , concert November 15 , programme | Leipzig 1849 , remain forgotten till 
 a0ps : whic nclude Mozart Symphony Max Bruch’s| bring hearing Bremen early year.——The new 
 G minor Violin concerto ( Mr. Johan Rasch ) , } Lortzing monument Tiergarten unveil 
 nly verture , ' Amicitia , ' write Mr. Van Dyk especially | October 28 ceremony usual occasion . 
 _ Society . Wakefield chamber concert November 8 | artist , musical society , deputation 
 shed distinguish performance Brahms F minor | present , General - Intendant von Hiilsen , represent 
 ebay Pianoforte quintet Saunders string quartet party , | Kaiser , place wreath'on monument . 
 wen Elsie Hall pianist . Mr. Quinlan vocalist . 
 ave Keighley Musical Union , enthusiastic local ARLSRUHE . 
 * r , Mr. J. B. Summerscales , " cond actor , F. Smetana rarely - hear grand opera , ' Dalibor , ' 
 ; fresh genial ' Seasons ' November 20 , ! ot art . TI H csggentty os Digg : oa oan 
 - prit l Miss Teresa Blamy , Mr. Alfred Heather | p oe ce ee ee es ee ee 
 = Mr. Joseph Lycett . — ieee 
 ge , Hull boast orchestra , number local player _ 
 . vi band form Hull Symphony Max Reger new orchestral Serenade ( Op . 95 ) 
 acing Orchestra , Mr. Wallerstein appoint | produce Generalmusikdirektor Fritz Steinbach 
 ay cond r. member engage local Giirzenich concert October 23 , meet great 
 theatre afternoon concert possible , | success . composer , present , declare 
 = place _ creditable | performance absolutely perfect . Madame Kirkby - Lunn , 
 19 , performance interesting work , include | London , sing air Mozcart ' Titus , ' contribute 
 y Haydn ' La Chasse ' symphony , ' unfinished ' | song Gustav Wolf Richard Strauss.——At 
 Schubert , Mendelssohn ' Scotch . ' band | second Giirzenich concert November 6 new symphony 
 improve co - operation , Mendelssohn work | hungarian composer Emanuel Moor produce , 
 T ) play , chief drawback excellence | Signor Sg uml uti melodious impressive ' Requiem 
 _ performance prominent player } perform time Germany . 
 = refined tone style . , | composer present receive warm congratulation 
 _ ver , interesting venture promise | audience , thank Herr Fritz Steinbach 
 vt . utistic sense . November 16 Hull Harmonic Society | excellent performance . 
 noes good - round performance ' Elijah , ' 
 ity Miss Ethel Wood , Miss Edna Thornton , Mr. John Bardsley - REFELD . 
 ind Mr. Frederic Austin . chorus efficient October 27 28 Konzertgesellschaft hold 
 Mr. Walter Porter conduct ability . Jn Memoriam Schaumann festival . programme 
 average interest , include rarely 
 — hear work fst version D minor Symphony 
 wae ( edit Franz Wiillner ) , beautiful ' Nachtlied ' 

 ate , foreign wrote . chorus orchestra , Festival overture , — 

 JOSt PH BARNBY — Original Hymn Tunes cloth 3 € z : 
 STERNDALE BENNETT — Songs cloth 2 € : si | 
 BERLIOZ \ Treatise n Modern Instrumentation 

 roan 6 o 
 , — ditto . edit Kari ks 
 BEETHOVEN — Sonatas \. ZIMMERMANN . micheal Royal gto , paper , 5 . ; roan 10 6 
 folio , Cloth , 21 . , ovo , th , 7 . 6d . 5 svo , Kutla roa 

 FREDERICK CHOPIN — man musician . : 257 
 EDI K Nieckxs . vol . 25 

 life t Cy 4 
 Orchestral s application Com ER , Elmham , Derehan 
 Tonic Sol - fa Editi half « | f mark * 
 le n : Novi ) ( , Li d 

 Ty 
 ORGAN 
 vy ™ " = ! v , rtrys 
 ARRANGEMENTS 
 4 + sa th 
 ed v 
 overture ( " m ) SCHUMANN 
 arrang y ] nE. Wt ' os & @ 
 INTERMEZZO ( ' ‘ 1 Rost SHARON " ) 
 . \. C. MACKENZIE 
 arrange Jounn E. ' « < 2 
 whim ( " grilie ? f FANTASIE CKE ) 
 SCHUMANN 
 arrange Joun E. Wet . o « 8 
 4 . ANDANTE ( \ n cop ) MENDELSSOHN 
 arrange W. A. C. ¢ . INK ss 2 
 SYMPHONY B MINOR ( Tue * unfinishe , 
 movemt ) SCHUBERT 
 arrange W. A. C. € ANK ' 2 8 
 BERCEUSE CANZONETTA ( op . 20 , . 8 9 ) 
 CESAR CUI 
 arrange Pr E , FLeTcui oo 8 
 7 , SCHERZO RUSTIQUE ( op . 20 , . 12 ) CESAR CUI 
 Arrang¢ y pt vy E , FLETCHER se 8 
 ‘ NACHTSTUCK ( 0 ; . 4 ) SCHUMANN ) 
 arrange A. B. P | 
 8 .. MOMENT MUSICAI F minor ( op . 94 , . 3 ) 
 S¢ HUBERT | 
 \ Arrange y A. B. , --J 
 FANTASIA anp FUGUI C Cc . P. E. BACH 
 arrang « y Joun E. Wet . _ o 8 
 10 PRELUDI IL . ( " Tue Es " ) 
 EDWARD ELGAR 
 arrange y G. R. Sp 2 
 11 . FINALI SYMPHONY N BEETHOVEN 
 arrange \. BL : 
 12 ADORAMUS TI HUGH BLAIR 
 \rrange y h 
 . INTERMEZZO \ ) 
 Cc . H. H. PARRY 
 arrange y W. ¢ \ 
 14 . BRIDAL MARCH FINALE ( " Tue 1 
 \ rOPHANES ) C. H. H. PARRY 
 arrange W. G. 
 ANDANTI l ES < < 
 \ ] Kk . W iMs 
 ANDANT ( O 
 t y ] k. Wi BRAHMS 
 ( ) 
 London : Nove . Company , Limited . 
 SHORT account organ 
 ( illustrate 

 
 ( W. Pa X 

