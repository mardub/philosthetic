THURSDAY MORNING, at 11.30

Tickets, including admission and rail from London Bridge, Ludgate PSALM CXXXVII. For Soli, Chorusand Orchestra 
Hill, Victoria, I igton, and intermediate stations, 1s, 6d. | sy MPH : gana t "ER KING 
M: 1y be obt of Messrs. Novello, Ewer and Co., Queen Street, | SYMPHOS CANTATA J beng ine: KING. 
E.C.; Tonic Sol-fa Agency, Warwick Lane: Mr. W. H. Bonner, Compose d expressly for the Festival. 
go, Ont sborne Road, Forest Gate; Mr. G. M. Williar ns, 102, Antill Road, SYMPHONY i IN C MINOR (Beethoven). 
mes ~ ss — es REQUIEM (Verdi). 
SOUTH LONDON MUSICAL CLUB | paimay Monxixo, at 11.20. 
(Fou inn S053) SYMPHONY IN B MINOR (Schubert). 
PRIZE GLEE COMPETITION. | ENGEDI (Beethoven). 
he Committee offer £10 ros. for the best setting for a Choir of LOBGESANG (Mendelssohn). 
Men's Voices (T.T.B.B.), with Pianoforte Accompaniment, of verses toa ity _— 
specially written for the purpose. | FripAy Eventnc.—THE REDEMPTION (Gounod). 
Th ard will be made by a select Committee of the "| ub, inclu ding

sical Director, guided by the advice of Dr. J. F. Bridge, of , oT >) IRIC BT

MISS FUSSELLE arena 
(Pupil of Madame Sainton-Dolby, formerly her Assistant Professor; 
Licentiate (Artist) of the Royal Academy of Music.) 
For Concerts, Oratorios, &c., a idress, 37, Harrington Square, N.W. 
MISS ELLIOT RICHARDS (Soprano). 
For Oratorios, Concerts, &c., address ee Oakley Street, Nortt 
Cc to

MADAME CLARA we ST (Soprano), 
MISS LOTTIE WEST (Contralto), 
Beethoven Villa, King Edward Road, Hackney

MRS. LINDLEY W i! rE (Soprano

AS MANAGER or TUNER (Out-door). Sade | ase SALE. — Broapwoop DRAWING-ROOM

standing all branches of Trade thoroughly. 17 years’ experience. GRAND (Class 134). Superior snake. Cost 200 Guineas in 
Of good address, tempera e,and good testimonials. Good salary and | 1579. Partict lars, Wilmot, Beethoven Hot e, St. James’s R i. Craaue. 
ermanency indispensable. Apply, B. A., Messrs. Novello, Ewer a pe 
Co.,1, Berners Street, W. +OUR DOUBLE-BASSES, belonging to the late 
zs Willia ell. T id Inst nts by

GOOD TUNER and REPAIRER secks a) ypakers, anc cau 
Country Tuner's post. Was brought up at Broadwood’s, and | eveni ng by y app oir atmer it. R

audience, whose best wishes for his success and safe return | 
The chief features in the pre

could not well be mistaken. 
gramme were Beethoven’s C minor Symphony, and a new 
orchestral work, entitled ‘* Three Mythological Pieces,” 
from the pen of Mr. Silas. The Pieces are respectively 
named after Venus, Vulcan, and Pan, and purport to be a 
musical expression of the ideas generally associated with 
those classical personages. We hear Vulcan at his forge, 
for example, making the arms of heroes and diverting his 
mind by thoughts of the goddess with whom he spent an 
indifferent matrimonial time. Without being specially 
remarkable, Mr. Silas’s music is very pleasing and well

written. The themes are genuine melodies, and their treat- 
ment is that of a composer never lacking in skill and 
resource. Madame Sophie Menter made her rentrée at

this Concert and played Liszt’s Pianoforte Concerto in A. 
She desired, we believe, to open with Beethoven’s in E flat, 
but, as that had been appropriated by Mr. Hollins for the 
sixth Concert, Madame Menter substituted Liszt’s work, 
not greatly to the satisfaction, perhaps, of many among 
the audience. Her performance exhibited all the features 
with which amateurs are familiar, and was a remarkable 
display of executive power. Madame Fursch-Madi supplied 
the vocal music and the Concert ended with the Overture to 
“¢ Oberon

THE BACH CHOIR

Ir will be remembered that the Society bearing this 
name came into being twelve years ago for the production 
for the first time in E ‘neland of J. S. Bach’s great Mass in 
B minor, a work which for length, elaboration, and diffi- 
culty is only paralleled by Beethoven’s Mass in 1D; 
Though, of course, the event created the highest amount 
of interest in strictly musical circles, the Mass proved as 
caviare to the general public. Still, the Bach Choir has 
not allowed it to drop, and it would seem that the time is 
coming when it will be more widely appreciated, for the 
audience at the ninth performance on the r2th ult. at St. 
James’s Hall was much larger than on any former occasion. 
This was the first rendering under the bdton of Dr. Villiers 
Stanford, and as it was known that the constitution of the 
choir underwent a great change when Mr. Otto Goldschmidt 
resigned the conductorship, apprehensions might not 
unre easonably have been entertained that the new-comers 
would fail at once to gain the necessary familiarity with 
the very arduous choruses. As a matter of fact, some 
falling off in vigour and unanimity of attack was notice- 
able, but, on the whole, the performance left little to 
desire. Miss Anna Williams and Miss Damian rendered

and the performance—a triumph of delicacy and | 
y

MR. CHARLES HALLE’S CONCERTS

ANOTHER series of these admirable entertainments, aptly 
designated the ‘* Pops” of the summer season, commenced 
on Friday afternoon, the rith ult As in former years 
Mr. Hallé is associated with three members of the Monday 
Popular Concerts’ string quartet—-namely, Madame 
Norman-Néruda, Herr L. Ries, and Herr Straus, Herr 
Franz Neruda retaining his placeas the violoncellist. Special 
interest was given to the opening programme by the pro- 
duction of a new Quintet in A, for pianoforte and strings, 
by Dvorak (Op. 81). Whether this is really a recent com- 
position or another of those early efforts penned by the 
composer in his days of poverty and neglect, we have no 
authority for saying, nor is there any internal evidence 
either one way or another. ‘The work is full of Dvorak’s most 
strongly marked characteristics, and though somewhat 
unequal is worthy of him in every respect. The least 
satisfactory movement, at any rate on a first hearing, is the 
** Dumka” or elegy, in which the ideas are treated at incon- 
siderate length. On the other hand, the Scherzo or 
“Furiant”’ is a capital example of genuine Bohemian 
music, and the first and last movements are very spirited 
and effective. ‘The rest of the programme need not be 
referred to at length. It included Beethoven’s Sonata in F 
sharp (Op. 78), Bach’s Violin Sonata in E, and Brahms’s 
Quintet in F minor, for pianoforte and strings (Op. 34

Another important work of the Slavonic school was 
brought forward at the second Concert on the 18th ult. 
This was Tschaikowsky’s Trio in A minor (Op. 50). A full 
description of this work was published in the Musical 
Review in 1883. It is dedicated ‘a la memoire d’un 
grand artiste,” and the first movement is marked Pezzo 
elegiano. This determines its character, which is sad and 
regretful, with a strong infusion of national wildness and 
ruggedness of character. This movement occupies eighteen 
minutes in performance, and the next, a theme with varia- 
tions, is almost as long. But so cleverly has the Russian 
composer treated his material that scarcely any sense of 
weariness is felt. The Finale is remarkably spirited and 
energetic, though scarcely cheerful, and the return of the 
principal subject of the first movement just before the close 
confirms the elegiac character of the work. We regard 
this Trio as one of the most effective produced for a long 
time, and concert-givers have been very unwise to neglect 
it. The impression it produced on the audience was unmis- 
takable, the applause being more than usually enthusiastic. 
Continuing his task of playing Beethoven’s last eight 
sonatas during the present series of Concerts, Mr. Hallé 
interpreted the one in E flat, Op. 81, ‘* Les Adieux,” &c., 
on this occasion. The remaining works in the programme 
were Brahms’s Sonata in G (Op. 78) and Schumann’s 
‘‘ Phantasiestiicke ’ (Op. 88), for pianoforte, violin, and 
violoncello

The third Concert, on the 25th ult., only needs brief 
reference, as the programme did not contain any novelties. 
The Beethoven Sonata was the one in E minor (Op. go), 
in the last movement of which Mr. Hallé is always at his 
best. Dvorak’s Trio in F minor (Op. 65) is one of his 
ripest works, and needs close acquaintance in order to 
appreciate its manifold beauties. Brahms’s Trio in C 
minor (Op. 101) is, on the other hand, singularly terse and 
clear in outline, and can be followed with ease. Schubert’s 
Fantasia in C (Op. 159), for pianoforte and violin, and a 
fine Sonata in D minor, for violoncello, by Marcello, com- 
pleted the selection

XUM

VIIM

354 THE MUSICAL TIMES.—June 1, 1888. 
NOVELLO, EWER AND CO.’S NOVELLO, EWER AND CO.’S 
a, ' rn a 
YOFORTE VOCAL ALBUM 
PIANOFORTE CAL 3UMS. 
ALB U MS — _ 
BEETHOVEN . el 
- > 1 PER TOUR aed aXe wide wee 
EDITED BY BERTHOLD TOURS. TWENTY-SIX SONGS (Vol. 1). nage he 
ia Sisk Paper Cloth KAREL BEN NDL. 
" B ACH. tr | age | GIPSY SONGS. Seriesrand2 ... .each 260 = — 
° s d,s. d.J 
1. TWENTY COMPOSITIONS oe _ wae ES on E RND. AL E BE N N ET > 
2. TWENTY COMPOSITIONS hi <y see — | SWELVE SONGS. (English and German) ... Io 2 6 
3. TWENTY COMPOSITIONS es as eee NP «| | we 
In One Volume = ove ss = eo 4-6 GEO. is BENNETT. 
. : | TEN SONGS (Robert Burns) ‘ 2 6 _— 
HANDEL. | EWELVE SONGS (Shelley and Rossetti) 2 6 = 
4. TWENTY-FOUR COMPOSITIONS ... ... 1 0 =H BERL 10Z. 
5. TWENTY-FOUR COMPOSITIONS aoe sie. ae. — SUMMER NIGHTS... ss 26 = 
6. TWENTY-FOUR COMPOSITIONS .... ... 1 0 
In One Volume _ 4 a plead) eae iB RAHMS. 
| TWENTY-ONE SONGS... 1 6

VARIOUS COMPOSERS. | E. DANN RE U Ty IE Ro 
7, FIFTEEN MARCHES aoe Io | SIX SONGS (D. G. Rossetti) 2 6 = 
8 FIFTEEN MARCHES a ine eg a EO — | FIVE SONGS (W. Morris). e 2 6 = 
9. ype or cg eee oe ” cao : ; % A. DV oR AK. 
co, SIXTEEN GAVOTTES, &. .. .. .. 10 | SIXTEEN SONGS (Op. 2, 5,17, and 31) ax so 
uz. SIXTEEN GAVOTTES, &c. ee ae J. W. LIOTT. 
12, SIXTEEN GAVOTTES, &e. Reese) hes oO NATIONAL NURSERY RHYMES. 65 I!ustrations 7 6 
In One Volume - 4 @ R. P R ANZ. 
‘: NR 
le Geese WOLL ENH: \UPT. THIRTY SONGS i a 
s3. TEN COMPOSITIONS a és Io | FOURTEEN SONGS (Robert B : ee 2 6 _ 
oo ee HERMANN GOETZ, 
tS: , ee ete EIGHTEEN SONGS (Op. 4,12,19) — ... a 
stated — EDVARD GRIEG 
4 P| I. 
O. SCHWEIZER. TWENTY-FOUR SONGS... a 
6. EIGHT SCOTTISH AIRS (Duets) ro 
' < LADY AR THUR HILL. 
FRITZ SPINDLER. | HOLIDAY SONGS ree 
rz, NINE COMPOSITIONS .... Rane a LISZT. 
18. NINE COMPOSITIONS aes ae wae ws. & O TWENTY SONGS . 6 aes 
19. TEN COMPOSITIONS eae wie ey) ak SO : 
In One Volume eee ee - = 4 of A. C. M. AC KE NZIE. 
|] EIGHTEEN SONGS. Three Books » each 2 6 _ 
HERM: NN GOETZ. | EIGHTEEN SONGS, Ore Vol. 7 6 
20. LOSE BLATTER (Op. 7), 1-5 arenes ETO MARI: ANI. 
21 ppd ea ry Oi) 69 eee OO - | TWENTY-TWO SONGS. (Italian) is 26 = 
22. GENREB YER (Op 13) ... one a ; f0 is 
spent ee MENDELSSOHN. 
SONGS. (With Portrait) . Folio 2I 0 
fe RHE INBERGE ae SONGS, (German and English) ... 4 0 6 0 
#3. SEVEN COMPOSITIONS ..._ ... Rem oa SONGS (Deep Voice). (Ditto) Po 8 0 
24. ELEVEN COMPOSITIONS ey CS ets eae 2 MOORE. 
25. SEVEN COMPOSITIONS ... Soe see mm 2 2 — IRISH MELODIES. ie . 2-9 ae oa 40 
InOne Volume. - 4 ©! IRISH MELODIES . : .. Folio — 21 0 
BERTHOLD TOURS. RANDEGGER. 
26. A JUVENILE ALBUM (Duets) .. a _ | SACRED SONGS FOR LITTLE SINGERS. Illus. 2.6 5 0 
RUBINSTEIN 
an ans 
J. MOSCHELES. TWENTY-FIVE SONGS 1 6 — 
27. DOMESTIC LIFE (12 CharacteristicDuets) Bk. I. 2 0 - 
28. Ditto ditto ne Il. 20 - SCHUB E RT. 
TORE Volume 4 0 | TWENTY SONGS (Mezzo-soprano) t 6 - 
YTY SONGS (Contralto) i 1 6 - 
H ree 2 D AN KJE RU Lk. =NTY SONGS (Soprano or Tene) .. 1 6 _— 
MPOSITIO?} i i . _ | SCHWANENGESANG (Swan Songs) . 1 6 - 
a von Pressetiiconeirncai peer: DIE SCHONE MULLERIN(The Fair Maidofthe Mill) 1 6 9 — 
30. oy es = 
3t. TWENTY-THREE COMPOSITIONS . anon anieeiaiaen _— 
In One Volume wo 4 0}/SONGS «» Folio - 10 6 
MYRTHEN (26 Songs) ave I 6 - 
EDVARD GRIEG. LIEDERKREIS (12 Songs) ... io = 
32. VIER ALBUMBLATTER (Op. 28), &. 9... 1 0  — | VOCAL ALBUM _ .. z 10; 1440 
33. POETISCHE TONBILDER (Op. 3), and Nor- WOMAN’S LOVE AND LIFE (3 $ Songs) 1 o _ 
wegian Dances and Songs (Op. 17), &c.. r oO _ y > 
34. LYRISCHE STUCKCHEN (Op. 12), and AUS VARIOUS COMPOSE RS. 
DEM VOLKSLEBEN sa 19)... Io — | OLD IRELAND (Irish Melodies) .. 26 _ 
In One Volume ao + o| THE SUNLIGHT OF SONG, 46 Illustrations = 5 0 
es ra 1; VOLKSLIEDER ALBUM (Forty | English 
List of Contents may be ha we ralis. and German... ss ee en eG ae 
LONDON & NEW YORK: NOVELLO, EWER AND CO, LONDON & NEW YORK: NOVELLO, EWER, AND CO

c he play 
2 display 
irasate has 
evinced by

performance of the best works written for his instrument. 
This season he made his rentrée in Beethoven’s Concerto, 
by common consent tl test of a violinist’s 
capacity. In Raft’s etfec ‘as unexceptionable

Finale exhibiting the 
with which he 
can regard 
audiences 
‘s, they are 
means acquire 
Aa gy On 
rendered Men 
and the Overture to

The crowd at the third Concert, on the 26th ult., was 
greater than ever, probably from the fact that Mendels 
sohn’s Violin Concerto was in the programme. This work 
is a special cheval de bataitlle of the Spanish violinist. The 
sweetness and tenderness he infuses into the Andant 
could not be surpassed, and his brilliant execution in the 
Finale is very exciting, though Mendelssohn never intended 
the movement to be taken at such a furious pace. A Con- 
certo in B minor, No. 3 by M.S tlso created a 
great effect. It is an i mere work, the

middle movement, a Ba ircarole, being extremely nes we 
At the close of this a passage, written for the | highest har 
monics, enabled the player to. exhibi y of a 
wonderful order. His extraordinary on Airs 
from “Carmen” completed the list of solos. A 
somewhat tame and perfunct rendering of Beethoven's 
C minor Symphony commenced the Concert, and an

Bernard, closed 
defer an opinion 
position in the

Princes’ Hall, on the 3rd _ ult., however, it was seen that 
although she is not of mature years, she is no longer a

gramme ynsisting ot 
pianoforte n peculiar, bicaan of 
Sonatas “a Beethoven and Chopin being mingled with 
trifles by Godard, Lack, and Mdl Folville_ herself. 
ly there was no real test of artistic excellence in 
‘lange, and as a matter of fact the young pianist 
only proved that she has well-trained Her com- 
positions are feeble student’s efforts demanding no serious 
criticism. In the second part Miss Folville appeared as a 
violinist, and materially advanced herself in the estimation 
of the audience. Her rendering of Mendelssohn’s Con- 
certo, with an excellent pianoforte accompaniment vd Mr. 
Ganz, was not only neat and exact, but noteworthy for 
good tone and style. On the whole, the young ero 
justified her appearance, though she has yet a good deal to 
learn. 
The annual Recital given by Mr. Oscar Beringer is 
always an interesting event, and that of the 13th ult. at 
James’s Hall was so in a special sense to musicians. 
In the supplementary volume of Beethoven’s works recently 
published by Messrs. Breitkopf and Hartel, is a Trio in G, 
for pianoforte, flute, and bassoon, supposed have been 
written in 1786, when the composer was sixteen years old. 
As every note penned by Beethoven should be performed, 
at any rate, experimentally, Mr. Beringer must be thanked 
for bringing forward this juvenile effort of the greatest of 
all composers. Unfortunately we must couple commenda- 
tion with condemnation, for without any assigned reason 
he allotted the wind instrument parts to violin and violon

fin gers

CHAMBE R ‘CONCE RTS

the tinal movement, an air with variations. If the work 
was worth doing at all it was worth doing as Beethoven

left it. The loss of effect was considerable and produced Mr. Ernest Kiver’s annual eects at the Princes

amply justified the high position she has attair ned

among contemporary pianists of her sex. Her principal] ‘Tuts gifted child has lately been exhibiting his marvel- 
effort was a rendering of Beethoven’s Sonatas in E lous talent to country audiences, and has only appeared

minor (Op. go} and in A major (Op. 101), =e et twice in London since our last notice. The first of these 
together in the midst of a long and diversified selection of occasions, however, at St. James’s Hall, on April 30, 
short works by dead and living masters. Miss Kleeberg}|had special interest, as he had the co-operation of an 
played the Sonatas with the classical purity of style and | orchestra for the performance of a Concerto. ‘The work 
dignity of feeling which she has accustomed us to expect,}| chosen was Beethoven’s (No. 1, in C), with which Josef 
eliciting therein the demonstrative approval of her auditors. | Hofmann astonished a Philharmonic audience last summer. 
Her crisp, elastic touch and irreproachable technique were ; Comparisons were therefore instituted between the 
advantageously displayed in Handel’s Chaconne and Varia- | “readings” of the two precocious children, and we do 
tions and Mendelssohn’s Caprice in A minor ; while the same | not think Hegner suffered by them. While his mastery of 
qualities, allied with a deeper touch of sentiment, gave | the executive difficulties of the work was as remarkable as 
delightful effect to Schubert’s Moment Musical in A flat, |that of Hofmann, his playing had more dash and aplomb. 
and Schumann’s Romance in F sharp. We have not]| He attacked every passage as if he knew that his fingers 
space to detail the remaining items of the programme, but | were certain to do what was required of them, and his 
may mention as among the less familiar a Ménuet by | confidence was justifiable, for not once, so far as the ear 
Paderewski, a ‘* Sérénade d’Arlequin” by Francis Thomé, | could detect, did he miss a note. Specially noteworthy 
a Valse by Tschaikowsky, and last, but not least. a grace- | was his brilliant rendering of Beethoven’s own cadenzas

ful morgeau by Sir George Macfarren, entitled “Welcome.” | and also the expression and feeling he threw into the

are Rossini’s lioz’s * Faust

Beethoven's

ASSOCIATION

MUSIC IN BIRMINGHAM. 
(FROM OUR OWN CORRESPONDENT

Most of the blanks in the Festival scheme have been 
filled up in the course of the past month, and though one 
more disappointment has yet to be recorded, in the with- 
drawal of Mr. Goring Thomas’s promised work, owing to 
the composer's inability to complete it in time, the pro- 
gramme as a whole has been considerably strengthened, 
alike as regards works and executants. In addition to 
the three Symphonies previously announced—Mozart’s 
** Jupiter,” Beethoven’s C minor, and Haydn’s No. 6 of 
the Salomon set—we are to have a Pianoforte Concerto of 
Schumann, for which the services of Miss Fanny Davies 
have been enlisted, Brahms’s ‘* Academische”’ Overture, 
written in 1880, Weber’s Hymn “In seiner Ordnung,” a 
Suite and Concert Overture (“In Autumn”’) by Grieg, a 
Psalm by Robert Franz, Liszt's third Rhapsodie, and the 
‘‘Meistersinger”’ Vorspiel of Wagner. The choral 
rehearsals of Dr. Bridge’s ‘ Callirhoé,’? as well as of 
Handel’s * Saul”? and Dvorak’s ‘ Stabat Mater” are now

Question and Answer

completed, and the choir is engaged upon Berlioz’s 
** Messe des Morts.”” Herr Richter has paid several visits 
to Birmingham and is well pleased with the progress made

One of the events of the musical season, which occurred 
too late for notice last month, was the Pianoforte Recital of 
Miss Fanny Davies, which attracted a large and enthusi- 
astic audience to the Masonic Hall on April 27. Since her | 
previous appearance in her native town, Miss Davies had 
been reaping fresh laurels and extending her experience on 
the Continent and elsewhere, and her playing e: 
an ease as well as finish and maturity of style some- 
times wanting in her earlier displays. Her selection 
comprised, among other noteworthy items, Beethoven’s 
Sonata in D major (Op. 10, No. 3), Mendelssohn’s 
Prelude and Fugue in E minor (Op. 35, No. 1), Schumann’s 
Carnaval,a Romance in E flat minor of Madame Schumann, 
Brahms’s Rhapsodie in G minor (Op. 79, No. 2), and 
Chopin’s Valse in A flat (Op. 34, No. 1

The Edgbaston Amateur Musical Union wound up their 
season with an Orchestral Concert at the Edgbaston 
Assembly Rooms on the 14th ult., when the principal items 
of the programme were Gade’s Third Symphony in A 
minor, Mozart’s Pianoforte Concerto in C minor, Onslow’s 
Overture to ‘* Le Colporteur,’’ Reinecke’s Entr’acte from 
‘* King Manfred,’ and Mendelssohn’s *“‘ Cornelius’? March. 
The band, under Mr. F. W. Cooke’s direction, shows 
steady progress, though it is scarcely strong enough yet to | 
do justice to a symphony or concerto without professional 
assistance. It was heard to most advantage in the Onslow 
Overture and the Mendelssohn March. Mrs. Richardson 
gave an admirable rendering of the pianoforte part in 
Mozart’s Concerto, and Miss Florence Donaldson, a young 
local violinist, fairly roused the enthusiasm of the audience 
by the grace and finish of her performance in Vieuxtemps’s 
** Réverie ” and De Bériot’s “ Scéne de ballet.” Mr. Percy 
Taunton, the solo vocalist of the evening, was most suc- 
cessful in Tito Mattei’s ‘‘ La Patria

Sefior Sarasate was accompanied by Herr Schénberger, 
| excepting the two last-named, which were accompanied by 
| Herr Goldschmidt

Herr Schénberger’s solos were an 
Etude of his own, Chopin’s Valse in A flat (Op. 42), 
Godowski’s ‘* Moto perpetuo,” and, as an encore, two 
numbers of a set of German Dances (unpublished, the 
MS. in the possession of Professor Seiss, of Frankfort). 
These dances were written by Beethoven for a small 
orchestra, and Herr Schénberger has arranged them for 
pianoforte. Some amusing guesses were made by the 
audience as to the name of the composer of the German 
dances

A large and representative company of the professional 
and amateur musical talent of Edinburgh gathered, under 
the auspices of the Edinburgh Society of Musicians, in the 
Waterloo Hotel, on the evening of the gth ult., in honour 
of Dr. A. C. Mackenzie, who, by invitation of this Society, 
paid a visit to his native city. Over a friendly supper he 
received the warmest congratulations on his appointment to 
the distinguished post of Principal of the Royal Academy 
of Music and the honour paid him by the city of Glasgow 
in connection with its Exhibition. The Society had, at the 
same time, the pleasure of entertaining Mr. Barrett, of 
London. His kindly, warm-hearted speeches, and those of 
Dr. Mackenzie for the further success of this Society 
will not easily be forgotten. Dr. Pryde, of this city, 
was also a guest of the Society on tl occasion. 
Apologies were read for the absence of Mr. Selig- 
mann, President of the Glasgow Society of Musicians, 
Sir George Grove, and Mr. Alfred Littleton. Mr. George 
Lichtenstein occupied the chair and discharged his duties 
in a most genial and able manner

The fifth annual Concert was given in the Masonic Hall, 
on the evening of the 18th ult., by the pupils of Herr Otto 
Schweizer, assisted by a ladies’ choir under the direction 
of Mr. Arthur Edmunds. The more important numbers of 
the programme were Beethoven’s Sonata (I* major, Op. ro, 
No. 2), Mendelssohn’s Variations Sérieuses, Chopin’s 
Fantaisie in F major (Op. 49), ‘ Erl-King”’ (Schubert- 
Liszt), and ‘“ Barcarole” (Schweizer, Op. 22). These 
two last-named were played by Miss Charlotte Gibson, 
and Chopin’s ‘ Polonaise” (A flat, Op. 53) by Mr. 
Theodor Hoeck. These two pupils also played, as a 
pianoforte duet, Biilow’s arrangement ofthe “ Tannhauser

360

is both novel and effective. It is said that the composer 
intends scoring it for the ordinary orchestra, so that it may 
be available for other occasions than that for which it was 
originally written

The other choral selections given on the day were 
“* Hail, bright abode,” from ‘‘ Tannhauser,”’ ‘* Twine ye the 
garlands’’ from Beethoven’s ‘‘ Ruins of Athens,” and the 
Hallelujah Chorus from “‘ The Messiah

The musical arrangements in connection with the Ex- 
hibition are of the usual character, military band music 
prevailing. Daily organ recitals and fortnightly choral 
concerts are part of the musical recreation provided. The 
Glasgow Choral Union give two concerts, at one of which 
Dr. Mackenzie’s Ode, ‘* The New Covenant,” will be 
repeated. The Entertainments Committee, indeed, have 
here missed their opportunity of bringing together the best 
choral talent in the country to add to the attractions of the 
Exhibition, and, following a somewhat parsimonious policy, 
have accepted the services of choirs and societies reputable 
enough in their own position, but not at all equal to the 
needs of the undertaking

people

little hand scarcely capable of 
grapples with passages that are really difficult for ex- 
perienced pianists, but that he plays a Beethoven Sonata 
(like the Op. 22 in B flat) with ics entering into 
its varied meaning, and a Bach Fugue with clear perception 
of the true devotion of its contrapuntal intricacies to a 
high artistic purpose. Messrs. K yth, undeterred by 
the loss attending their first attempt, announce the 
re-appearance here of young Hegner early in this month. 
On Thursday, the 7th ult

two young pianists, Miss 
Fanny Davies and Miss Mathilde Wurm, and a still 
younger violinist, Miss Geraldine Morgan, invited music 
loving folk to assemble at the Concert Hail, so suitable for 
recitals of classical works such as they delight in. Of the 
talent of the young trio I need not write. Miss Fanny 
Davies is already well known in London and in many 
other places as a young English player of hig! 
promise and of very high present attainment. Her 
ability was variously and triumphantly di i 
Brahms’s C minor Trio (Op. 101) and 
solos, as well as when modestly subordi 
support of Miss Liza Lehmann in her 
dering of songs by Schubert, Schumann, 
Miss Wurm exhibited a remarkable li c 
ness of touch in Mendelssohn’s Variations in D, for 
violoncello and pianoforte, and in Rubinstein’s ‘ Staccato 
Etude” (No. 2 of Op. 23) a truly extraordinary 
elasticity of wrist. Miss Geraldine Morgan’s violin playing 
justifies the highest hopes for her future. She produces a 
resonant, full, rich tone; her bowing is graceful and ex- 
ceedingly bold and free; and her intonation unerring, 
except, perhaps, in such octave passages as require a very 
extended grasp. The three concert-givers are admirably 
associated, and with the co- operation of Signor

composed for the festival, but there was no novelty in the

Sacer aacay) The Macnificat and Nunc dimittis wer 
present service 1e Magnificat and Nunc dimittis were 
by Mr. Eaton Faning, the Anthem ‘ Lord, thou art God, 
by Dr. Stainer, and at the close Beethoven’s Hallelujah 
Chorus from “ The Mount of Olives ” In the 
there was occasional unsteadiness in s} 
careful conducting, due no doubt to the impossibility of 
obtaining general rehearsals. The sermon was preached 
by the Bishop of Marlborough

Tue Hampstead Choral Society gave a Concert at the | 
Vestry Hall, Hampstead, on April 30, when Mendelssohn’s

Subscription Concert in the Chur

een prepared, containing many items of interest, and 
| great credit is due for the manner in which they were 
rendered. Mendelssohn’s “ Presto,” from the G minor 
Concerto, was performed by Miss Thurston, accompanied 
on a second pianoforte by Miss Scott, and Mr. Warner 
on the organ. Beethoven’s Sonata in I (Op. to) was 
given with much spirit by Miss Gu yon. Miss Wild gave a 
refined rendering of Schubert's “ Impromptu” in ‘A flat 
(Op. go). Miss Cullum, Mrs. Walter Tudor, and Miss 
Wise were responsible for the vocal selections, while the

by Mr. Louis bbins) gave a 
al pieces of a light and pleasing

Tue Stock Exchange Amateur Orchestral Society gave 
its third and last Concert for the present season at St. 
James’s Hallon the gth ult. Exceedingly creditable perform- 
ances were given of Mendelssohn's ‘* Scotch”? Symphony, 
the Ballet music from ‘* La Gioconda,” and the Overtures 
‘** Coriolan”’ and ‘* Mireille.” The Conductor, Mr. George 
Kitchen, deserves much credit for the present highly 
efficient condition to which he has brought his amateur 
instrumentalists

On Wednesday, the gth ult., at the Princes’ Hall, 
Madame de Llana gave a Pianoforte Recital. This talented 
pianist was assisted by Mr. Ben Davies and Mr. Bernhardt, 
violinist. The programme was made up of selections from 
Beethoven, Mendelssohn, Grieg, Dvorak. Chopin, and 
Liszt. Greig’s interesting Sonata in C minor, for piano- 
forte and violin (Op. 45), was rendered by Madame de 
Llana and Mr. Bernhardt. Mr. Ben Davies’s songs were 
given in excellent style

toso,”’ from

Mr. Epwin Barnes gave two Organ Recitals, interspersed 
with vocal music, on the large and improved organ at the 
School for the Blind, Swiss Cottage, on the evenings of the 
12th and roth ult

An Organ Recital was given at Holy Trinity Parish 
Church, Rotherhithe, on Whit Sunday, by Mr. Ernest R. 
Foster. <A selection from the works of Handel, Beethoven, 
Bennett, Bach, and the player formed the programme

Mr. NorMAN CuMMINGS, son of the well-known tenor, 
teacher, and conductor, Mr. W. H. Cummings, has made 
his début at Leipzig as a pianist with marked success

Herr Julius Stockhausen, the eminent German baritone 
and unrivalled interpreter of his country’s most refined 
lieder, commemorated, on the 26th ult., the fortieth anni- 
versary of his first public appearance in the part of Elijah, 
at a performance of Mendelssohn’s Oratorio, at Basle. In 
the same month, twenty-five years ago, Stockhausen 
undertook, for the first time, the rendering, at a Concert 
given in Vienna, of the entire series of songs known as 
‘Die schéne Millerin” and “ Die Winterreise,” by 
Schubert, with which his name has since become asso- 
ciated, both in Germany and elsewhere. Numerous tokens 
of regard and admiration were forwarded from all parts of 
the Fatherland to this veritable ‘ meistersinger,’’ on the 
interesting anniversary in question

A new “infant prodigy” has been discovered at Vienna 
in the person of Leopold Spielmann, aged five, who plays 
Mozart’s and Beethoven’s Sonatas from memory, and with 
a degree of expression, we are told, truly surprising in one 
so young

Weber’s “ Die drei Pintos,” as completed by Herr Mahler, 
has now been most successfully brought out at the 
Dresden Hof-Theater, the first and third acts more especially 
eliciting enthusiastic plaudits, while several numbers had 
to be repeated

Cav atina for soprano solo and a mixed choir, intended to | 
celebrate the silver wedding of his daughter. They are | 
said to be written in Spohr’s best manner, and well adapted | 
for performance by small choral societies

A monument is to be raised to Beethoven at Philadelphia, | 
in the shape of a statue of the composer, to be erected in| 
Fairmont Park. Subscriptions have been opened for the 
purpose for some time past, and there is every probability 
of a speedy realisation of the scheme

The town of Chicago is contributing to the cost of the | 
Mozart monument in Vienna. ‘The musicians of the Lake 
City have given two Concerts for this purpose, the amount 
obtained being 3,000 dollars

371

LonponpEerry.—An Organ Recital was given in the ven on 
Monday, the r4th ult., by Mr. D. C. Jones, Mus. Bac F.C.O. His 
programme was selected from the works of Bach, Batiste, Guilmant, 
Rheinberger, Spohr, and Beethoven. Mr. Hemingway, of the C athe- 
dral, was the vocalist

Neatu.—Mendelssohn’s Elijah was performed by the Harmonic 
Society on the 3rd ult. in St. David’s Church. The choru 
numbered some hundred voices, well balanced and well trained, and 
they did their work in a most satisfactory manner. The Society has 
been in existence some fifteen years and has given a long list of 
oratorio and choral works. The present effort was probably the most 
exacting and most successful. The choir was supported by a small 
orchestra and the organ of the church. The solos were sung by 
Miss Jenny Eddison, Miss H. M. Jones, Mr. Hopkin Morgan, and Mr. 
B. H. Grove. The Conductor was Mr. j.L. Matthews, and Mrs. R. P. 
Morgan presided at the organ

a Ee ee Ss

BACH, J. S.—Bleib bei uns.” Cantata. With additional 
Accompaniments by R. Franz. Orchestral parts... eve 15 
BEETHOVEN.—Works. Série 25, containing 46 works, now 
published for the _ time complete... ase wos mee-28 
May a had singly : 
No. 1. Cantata on the Death of the E mye ts eph II: 
Full score . ‘ ‘ ‘je net $ 
Parts (vocal and orchestral) ... : 
» 2. Cantata on the Coronation of ‘the Emperor 
Leopold II.:— 
Full score... ane ies oe §6net «6 
Parts (vocal and orchestral = 
— Small Pieces. For Pianoforte. (Hitherto un 
— Songs. With Pianoforte. (Hitherto unknow 
CUI, C.—Six Miniatures. For Pianoforte. Op. 39 van om

Deux Bluettes. For Pianoforte. Op, 2 3 
DANCLA, CH.—Trois Trios faciles. For Pianofor te, Violin, 
and Violoncello Op. 117 isi 
No. 1 a <n ‘ia wan vam wan am wa Z 
2 ‘aa 1 1 ase ea sn @ 6 
ee ee ~ ose as 6 
DVORAK, A. Third Sy mphon ny in F. Op. 76: 
Full score. ° eee eee ove net 40 
Orchestral ‘part fe ae ZA 
Arranged for pi forte ‘duet. se eee 2 
— Symphonic Variath ions on an “Orig tinal Theme. For 
Orchestra. 2 78: 
Full score. see eee eee oe ae we et 36 
Orchestral p parts... aia ian as ooo 48 
Arranged for pian oforte duet. vas ses eo 36 
— The 149th Psalm. For mixed “Chorus and Orchestra. 
Op. 79:— 
Full score ... see eee eee aa nee eo. net 16 
Vocal score Pee ea nas in as ae oe. a 
— Quintet. Op. 81. For Pianoforte, two Violins, Viola, and

Prayer from ‘ Mosé in

1. Dulce Domum.  S.A.T.B. 
2. Down among the Dead Men.  s.a.T. i 1d. Egitto”) . Rossini 2d. 
4. British Grenadiers.  S.A.1.B. = ee 2d. 46. TheGuardon the Rhine. S.A.1T.B. G.A.Macfarren 1d. 
6. Mytask is ended(Song& Chorus). a.t.p.p. Balfe 4d. 47. The German Fatherland. s.a.1.1 1d. 
7. Thus spake one Summer’s Day. $.A.1.B. Abt 2d. 51. Charity (La Carita). .s.s.... a "Rossini 4d. 
8. Soldiers’ Chorus. 1.1.3.3. ... : Gounod 4d. 52. Cordelia. A.T.T.B. . AM G.A. Osborne 4d. 
g. The Kermesse (Scene from ‘Faust’’) ... ,, 6d. 54. Chorus of Handmaidens (from ‘ Fridolin’’) 
10. Up, quit thy bower. s.a.t.B. 9 Brinley Richards 4d. A. Randegger 4d. 
11. Maidens, never go a-wooing. § $.S.1T.T.B. 56. The Red Cross Knight . Dr. Callcott 2d. 
G. A. Macfarren 2d. 57. The Chough and Crow Sir rH, R. Bishop 3d. 
14. The Gipsy Chorus ; - Balfe 4d. 58. The “ Carnovale” : Rossini 2d. 
17. England Yet (Solo& Chorus) .S.AT.B. $. Benedict 2d. 63. Our Boys. New Pz itriotic Song 
18. The Shepherd's Sabbath Day. s.a.t.p. $. Hatton 2d. H. F. Byron and W.M. Lutz 4d. 
1g. Thoughts of Childhood. s.a.1.B.... Henry Smart 2d. 64. The Men of Wales Brinley Richards 2d. 
21. Anold Church Song. s.a.1T.B. ... - 2d. 65. Dame Durden.. ose 1d. 
22. Sabbath Bells. s.a.71.b. - 2d. 66. A little Farm well ti illed ges - Hook 1d. 
23. Serenade. S.A.T.B. «. nee ae ae 2d. 67. There was a simple maiden... G.A.Macfarren 1d. 
25. Orpheus with his lute. s.s.s. Bennett Gilbert 2d. 68. Fair Hebe oe ove " 1d. 
26. Lullaby. s.A.A. er 1d. 69. Once I loved a maiden fair ... Pa 1d. 
28. Marchofthe Me n of Harlech : . Dr.Rimbault 2d. 70. The jovial man of Kent 1d, 
#9. God save the Queen. S.A.T.B. * 1d. 71. The oak and the ash... ” 1d. 
30. Rule, Britannia! s.a.1.B. ... rs Id. 72. Hearts of oak ... 1d. 
31. The Retreat. T.T.B.B. L.de Rille 2d. 73. Come to the sunset tree W. ¢ Philpott 4d 
32. Lo! Morn is breaking. s.s.s. ... Cherubini 2d.\ 76. A Love Idyl.  s.a.T.B. ove ay i 'R. Terry 2d. 
33. Weare Spirits.  s.s.s. G. A. Macfarren 4d. 79. Our merry boys at sea . ¥. Yarwood 2d. 
34. Market Chorus | " Masanielio ). s.At.b. Auber gd. 8o. Christ is risen(Easter Anthem). s.a.t.B. Berlioz 3d. 
35. The Prayer (‘ hecantello’ ), S.A.T.B. © 1d. $2. Hymn of Nature eae Beethoven 3d. 
39. O dewdrop bright. s.a.1.p. Kiicken 1d. 87. The bright-hair’d morn. s.a.t.B. T.L. Clemens 2d. 
43. “Iwas fancy, and the ocean’s spray. $.A.T.B. gt. The Star of Bethlehem (ChristmasCarol) __,, 2d. 
G.A. Osborne 2d. 92. Busy, curious, thirsty fly. T.A.7T.B. a 3d. 
44. A Prayer for those at Sea. s.a.T.p. G.A. Osborne 2d. 93. Love wakesand weeps. A.1T.b.B. Felix W. Morley 2d

Hymn of the Fisherman’s Ch ildren

