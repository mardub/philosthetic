BARITONE 

 RECITAL , BECHSTEIN HALL , | GRAND OPERA , COVENT GARDEN 
 | 
 PRINCIPAL festival 
 ; | N Book ! » R SEA 
 TIMES . 
 14 , 1 ° | 
 Mr. Robin Overleigh , vocal recital yesterday F ING ENGAGEMENTS tnciuvt 
 Bechstein Hall , baritone voice charming quality , | QUEEN HALL , LONDON . 
 evidently train . . . . old | ee . 
 french song phrase deliver distinct BRISTOL | MUSICAL festival . 
 musical ability ; Caccini ' ' Amarilli " good example ' King Olaf " ( Elgar ) . 
 sustained singing , Carissimi ' ' Vittoria " Choral Symphony ( Beethoven ) . 
 brilliantly sing . . . . Walford Davies * ' ae nighte " BRISTOL CHORAL SOCIETY . 
 duly impressive , , ' ' love jocund Reauiem " ( Brahms ) 
 dance " skilfully sing repeat . " * Fiery Cross " ( Max Bruch ) 
 couple song A. H. Brewer , Blow " ' Self - banish , " Poe lee : ais 4 ; 
 - know song Bennett , Arthur Somervell , LEEDS CHORAL UNION 
 Parry , singer distinct " * Golden Legend " ( Sullivan ) . 
 success . | new work . ' ' homage E. A. Pos H ke ) . 
 7 BURNLEY CHORAL SOCIETY . 
 STANDARD . _ — — — — 
 14 , 1908 . NEWPORT CHORAL SOCIETY . 
 praise Mr. Robin Overleigh present | " Stabat Mater " ( Rossini 

 unconventional programme recital Bechstein 
 Hall yesterday afternoon , ability sustain 

 Westminster pay tribute respect 
 statesman faithfully serve country . 
 funeral service , April 27 , 
 ex - Premier , Sir Henry Campbell - Bannerman 

 note music come Edward 
 Confessor Chapel , great altar , 
 quartet trombonist ( direction 
 Dr. Borland ) play Funeral Music compose 
 Henry Purcell ee Queen Mary II . 
 1694 . Dr. W. G. Alcock , assistant - organist 
 Abbey — w hose able direction service 
 music admirably render — play Rachmaninoft 
 Prelude C sharp minor . succeed , 
 strong unwelcome contrast , Beethoven 
 equale trombone , music dignified 
 tender expression . ' organ solo follow : 
 Dr. Basil Harwood ' Requiem Zternam ' 
 Tchaikovsky Funeral March . , 
 russian portion preludial selection serve 
 accentuate solemn simplicity _ perfect 
 appropriateness funeral music , vocal 
 instrumental , compose countryman 

 change year , form 
 goodly portion rich heritage 
 proud boast solemn duty cherish preserve 
 dignity purity 

 thought , ' announce intention apply 
 orchestral writing want 
 practice . proof success Schumann 
 point confidence Symphony b flat . 
 write happy period life , 
 long - continue obstacle marriage Clara 
 Wieck overcome . marriage place , 
 September 12 , 1840 , church Schonfeld , 
 near Leipzig , find 
 high recognise position composer 
 authority music . aletter Carl Kossmaly , 
 date 9 , 1841 , write week 
 performance Symphony , : ' 
 household , circumstance 
 different . time 
 hear pass happiness 
 work . wish hear Symphony . 
 happy | performance 

 Symphony Beethoven 

 state thing music reflect 
 characteristically . mind great 
 energy composition entire work — 
 course scoring — 
 day . 
 bestow Beethoven patience 
 consideration . know entitle 
 originally ' Spring Symphony ' ; , 
 mention possess 

 Fancy ! Symphony , _ Spring 
 Symphony , . ' } connection bursting 
 seasun spring evidently original idea , 
 hold end , leave record , 
 inscription portrait , inspire 
 poem Adolf Béttger’s.§ quote 

 

 bar , , ' beginning Symphony , 
 | suggest poem Adolf Béttger . Poet 
 |in remembrance , Robert Schumann . Leipzig , 
 | October , 1842 . ' orchestral work 
 free cloud melancholy 
 disturb obscure later work . 
 tunefulness fluency , happy expression , 
 novelty material , close 
 masterly manner treat , 
 unbroken continuity burst emotion 
 succeed , truly remarkable , 
 wonder receive . think , witha 
 charming vaiveté , excite sympathy 
 ' Symphony Beethoven . 
 wrong suppose rest oar 
 feat . month , 
 |‘The Symphony lie , eye 
 |are fix fresh goals’—a good characteristic 
 instance theimpulsive progress sodistinguishe 
 . ' goal ' infer 
 fact end year 
 |compose symphony — 
 publish . 4 , d minor , overture , 
 | Scherzo , Finale , movement , like 
 - know Mozart . 
 movement Pianoforte concerto minor 
 belong eventful year . Symphony b flat 
 begin career , perform early 
 year [ 1842 ] Hamburg ; shortly 
 Russia . performance England 
 Philharmonic Society June 5 , 1854 ; Overture , 
 Scherzo , Finale perform 
 Society April 4 previous year ( conduct 
 | Costa ) , 

 movement brilliant thought 
 | happy stroke harmony , thing 
 music elevate effect climax near 
 end , new inspiriting phrase 
 simple harmony string 
 | orchestra , air hymn thanks- 
 giving happiness . ( . 6 . ) 
 | movement distinct obvious 
 | point novelty interest . 
 |name — , passage finally close 
 | Scherzo , perfectly original 
 |also charming effect ; secondly , trombone 
 | passage second portion /ia / e , , 
 |though inspire movement 
 Schubert great symphony C — hear Schumann 
 | time Leipzig month 
 composition work — treat 
 way , produce solemn religious effect 
 easily forget . * 77io Scherzo arean 
 innovation established form , possibly suggest 
 Beethoven repetition 7770 fourth 
 seventh Symphonies , actually find , 
 writer knowledge , Symphony 
 Schumann . , work 
 earnestness individuality 
 Schumann marked characteristic . 
 word stand orchestra old 
 Gewandhaus - Hall Leipzig—- ' res severa est verum 
 gaudium , ' form unfit motto life . 
 , Mendelssohn , ' great joy ' 
 ' thing . ' precious gift gaiety 
 light - heartedness enviable , 
 Mendelssohn possess large measure , 
 Schumann probably know , 
 . recompense Nature 
 thing equally precious . endow 
 ambition , force , passion , imagination , tenderness , love 
 beauty — great purity nobility 

 Schubert Symphony C bring Schumann MS . fror 
 Vienna , play time Leipzig October 29 42 , 
 direction Mendelssohn 

 Sets i. II . edit Alberto Randegger . 
 [ Novello Co. , Ltd 

 originate publication book 
 credit happy thought ; 
 find excellent ¥ ocal material suitable organ recital 
 
 old composer represent selection Bach , 
 Handel , Haydn , Beethoven Schubert , modern 
 sample beautiful composition — ' heart , 
 inflame burn ' ( /”//ammatus ) , Dvorak ' Stabat 
 Mater , ' ' thou , o Lord , art Protector , ' charming 

 soprano air Dr. Saint - Saéns , sing witha 

 

 orchestra . 
 th favourable light . 
 1 novelty , novelty far 
 new , Carissimi oratorio ' Jephthah , ' revival 
 arouse interest pertain 
 ar mere historical curiosity . combine _ certain 
 " e intelligible naiveté expression , music reveal 
 striking feeling declamation poignant expression , 
 } : efiect Filia ( daughter ) song , ' lament , 
 ye valley , " cadence section 
 prolong , strong appeal imagination . 
 Carissimi orchestral accompaniment imprac- 
 ticable , Mr. Kilburn , conductor , discreetly supply 
 necessary direction . Miss Agnes Nicholls , 
 Miss Phyllis Lett , Mr. John Coates Mr. Herbert Brown 
 c principal . rest lengthy programme 
 include orchestral prelude Bach cantata ' Der 
 Himmel lacht , ' satirically - humorous scene 
 composer ' Phoebus Pan , ' couple song , 
 selection-—actually selection ! — ' Elijah . ' 
 final concert series largely instrumental , 
 embrace admirable performance Tchaikovsky 
 Pianoforte concerto b flat minor Miss Fanny Davies 
 Beethoven Violin concerto Miss Vivien Chartres 

 

 Der Freischiitz ’ overture Haydn Symphony B flat 
 Salomon set , programme , 
 conclude Tchaikovsky E minor Symphony . 
 Mr. Nikisch conductor , need add 
 stirring performance work 

 concert series , 9 , principal 
 feature Brahms Symphony , interpretation 
 Mr. Nikisch good , 
 individuality matter /emf7 movement 
 detract general efiect brilliance vigour . 
 distinguished conductor Berlioz ' Carnaval 
 Romain ' overture extraordinary power 
 work sustain climax . Beethoven G major 
 Pianoforte concerto play Miss Irene Scharrer , 
 young pianist way . 
 programme , include novelty , end Wagner 
 * Flying Dutchman ’ overture 

 LONDON CHORAL SOCIETY 

 ie Miss Hall interpretative versatility . the| song good countryman produce 

 programme include Beethoven coriolan overture and| finish singing remarkable 

 Granville Bantock aria ' christ Wilderness , ' | versatility , Mr. Nikisch pianoforte 

 sing Mrs. Henry J. Wood . day Mr. Mrs. Ienschel recital 
 series equal artistic interest 

 pleasant evening spend ( ueen Hall Mr. Robin Overleigh evidence excellent baritone 
 12 Amateur Orchestra conduct ] voice song - recital Bechstein Hall 13 , 
 Mr. Wilhelm Sachse . good account } interpret interesting selection English , 
 ' Die Meistersinger ' overture Mendelssohn ' Scotch ' | french , german song . Mr. York Bowen play 
 Symphony , sympathetic support render ] pianoforte composition , Miss Marguerite 
 Mr. Godowsky Beethoven pianoforte concerto G. | Swale accompany 

 vocalist Miss Susan Strong . splendid recital german lieder place 
 ; Bechstein Hall respectively 1 8 , vocalis 
 CHAMBER CONCERTS . | Mr. Ludwig Wiillner ; fine bari 

 Tay 5 : ' interesting character . contain 
 achieve Mr. Godowsky Bechstein 5 usual interesting characte c 

 il LT — ae ran . ho yi 
 Hall April 25 2 , Mr. Mark Hambourg Bach ' Brandenburg ' Concerto , Beethoven Symphony n 

 . 8 > Che > cue ' Igar ' Caractacus , 
 Albert Hall 9 . Che artist F ( . 8) , Choral Epilogue Elgar * car oni 
 | | } : ilungarian dance Brahms , Dr. Charles 
 play finely London . reading remarkable . 4 ae ? } 
 superb techniat et foe intellectual nana Ilarriss choric idyll ' Pan . " presence composer 
 } oo ) technique , l ’ h ctu acume r . . . 
 1 artistic -- \ , | - work England enable Society 
 artistic restraint Mr. Hambourg play - : - & f 
 sieve , + } 7 invite assistance , cantata advantage 0 
 gence exaggeration han previously , - , . 
 roportionately impressive result conduct satisfactory result . 
 conclusion , composer performer accord 
 se ovation . soloist Miss Oswyn Jones , Mr. 
 RECITALS , Frederick Norcup Mr. Stewart Gardner 
 Miss Kathleen l’arlow successful reappearance | " mainder programme admirably carry ou 

 William Maxwell Mr. Harry Dearth . orchestra , 
 compose chiefly professional player , lead Mr. George 
 Wilby , contribute small degree success TONBRIDGE DIsTRICT . 
 performance . April 28 29 . 
 number entry standard 
 Staines Choral Society second concert | performance festival great headway 
 season Town Hall April 30 , Mozcart inauguration seven year ago . senior competition , 
 ' Twelfth Mass ' Mendelssohn ' Hymn Praise ' , West Malling successful male - voice class 
 form programme . choir excellent work , class singe church music , West Malling Church 
 especially ' Hymn Praise . ' solo vocalist Choir winner anthem - singing competition 
 Miss Kate Cherry , Miss Annie McBride , Mr. Henry F. | sight - reading contest . Chevening Choral Society 
 Coote Mr. George Stubbs . performance | award challenge shield chief choral class , 
 conduct Mr. James Brown . Wateringbury Choir secure Lady Mary Lygon bowl 
 sight - reading . Dr. Henry Coward adjudicator . 
 course conclude concert , Tonbridge 
 Kensington Park Choral Society , School , prize distribute Countess Stanhope 

 conductorship Mr. H. Scott - Baker , concert WESTMORLAND ( KENDAL ) . 
 St. Mark Hall , Blenheim Crescent , 12 . April 29 30 , 1 2 . 
 programme include march chorus ' Tannhauser , ' festival year performance 
 Elgar ' challenge Thor , ' Grieg ' Landerkennung’| combine resource district lead feature , 
 Mozart Concerto pianoforte orchestra , } event rank importance 
 solo play Mr. Scott - Baker . solo| festival ordinary non - competitive type . Mr. Henry 
 vocalist Miss Dora Morris Mr. Hubert Baker,|J. Wood bring Queen Hall Orchestra , 
 introduce song Mr. Scott - Baker . | programme concert include important work 
 | instrumental choral . 
 | chief work perform occasion Brahms 
 Se Besintens Chased Gost — tte th | ' german ' requiem . resource available 
 ee Hall M page " C concert | adequate demand great composition , 
 Public Hall 5 , Mendelssohn ' hymn | impressive interpretation . choral work 
 praise ' chief feature programme , | perform ' revenge ' ( Stanford ) , ' god Nature ' 
 include Edward German - song ' o peaceful night ' | ( Schubert ) , ' recognition land ' ( Grieg ) , ' evening 
 ' o lovely . ' Miss Florence Holderness , Miss| scene ' ' spanish serenade ' ( Elgar ) , 
 Edith Nutter Mr. Henry Beaumont solo ’ madrigal - song . instrumental programme 
 vocalist , Mr. George J. Hall conduct . include Mendelssohn Violin concerto Handel 
 |a major Sonata violin pianoforte , 
 Lady Speyer violinist , ' Peer Gynt ' . 1 
 performance Parry ' Pied Piper ' ( Grieg ) , | ' hungarian ' rhapsody F ( Liszt ) , 
 St. James ’ Parish Room , Blackheath , 12 , | * " Beethoven major Symphony Tannhauser 
 conductorship Mr. W. E. Wearden , oe solo singer Miss Marie Brema , 
 congratulate spirited rendering | Miss. yy hite , Miss bees Wilson , Frederic 
 cantata , choir orchestra ( lead Mr. Edward | " ' SU0 Mr. Ben Davies . Master Toni Maarkoft ne 
 O’Brien ) exhibit precision attack good tone . play violin solo . child concert chie 
 Pied Piper render Mr. L. E. Dennis . cantata luck Edenhall ’ ( Sydney H. 
 singing Miss E. Croft special feature Nicholson ) , conduct Mrs. T. A. Argles . 
 concert . competition close festival 
 - | standard create exertion 
 local conductor . view scattered population 
 East Finchley Muswell Hill Musical Society | difficulty locomotion lake district 

 excellent performance Coleridge - Taylor ' Hiawatha | especially winter month , striking testimony 
 departure ' Mendelssohn ' Walpurgis night’|to stimulating educative effect competitive 
 concert New Lecture Hall , East Finchley , | principle excellent result attain 
 April 30 , able conductorship Mr. George R. | local unit addition magnificent consumma- 
 ceiley . choral singing specially noticeable | tion combine performance . 
 intelligence expression , receive satisfactory child day 500 juvenile compete , 
 support capable orchestra , lead Mr. E. Meier . | combine perform . competition reveal fair 
 solo vocalist Miss Edith Evans , Madame Kate | standard execution skill sight - singing . 
 Bauer , Mr. Gwilym Richards Mr. Montague Borwell , | morning devote adult competition . 
 successfully sustain respective . chief result follow : choral societies—1st , Carnforth 
 ( Mr. Rathbone ) , entry ; Village choral society — Ist , 
 Yelland ( Mr. Percy Smale ) , seven entry . Kendal Parish 
 Church ( Mr. Granger ) following competition 

 distribute prize . Dr. McNaught adjudicate . 
 Leith Hitt , DorkInGc ( 13).—there 
 school junior class scheme . 
 tribute - entry section . Coldharbour 
 ( Miss Vaughan Williams ) win cup offer female- 
 voice choir , Shere ( Mr. Whittington ) , banner 
 madrigal singing , Capel ( Mr. Gore ) , banner male- 
 voice choir , Shalford ( Miss F. B. Clarke ) , banner 
 chorus singing . importance combine 

 performance . occasion Bach cantata ' Sleepers , 
 wake ' _ Beethoven C minor Symphony | 
 perform . Dr. Vaughan Williams conduct . Dr. Allen 

 Mr. Henry R. Bird adjudicate 

 compose , enthusiastically 
 receive . Mr. Charles Collier hear advantage 
 brilliantly execute harp solo . Mr. Arthur Cooke 
 ably fill dual role solo pianist conductor 

 Birmingham Handsworth Orchestral Society 
 Midland Musical Society combine force 
 interesting popular concert Town Hall April 16 . 
 Mr. Johan C. Hock conduct orchestral item 
 secure praiseworthy reading Beethoven second 
 Symphony , Tchaikovsky ' Elégie ' ( string ) , Sibelius 
 symphonic poem ' En saga , ' Introduction Act iii . 
 f ' Lohengrin . ' choral number assign Midland 
 Musical Society restrict unaccompanied part- 
 song : Cooke ' strike lyre , ' Elgar ' love dwell 
 northern land , ' Sullivan ' O gladsome light , ' 
 splendid ensemble purity tone 
 Mr. A. J. Cotton direction . Miss Grace Ivell , solo 
 vocalist , sing , Mr. Arthur Hitch , solo 
 violinist , play Max Bruch Violin concerto G minor 

 MUSIC BRISTOL DISTRICT . 
 ( correspondent 

 challenge Thor , ' Pinsuti ' kiss ' Webbe 
 ' wind breathe soft . ' singing separate 
 choir , , arouse chief interest competitor 
 audience , speak zeal display 
 occasion inducement offer 
 prize baton . competition appear 
 keen account . choir choose separate test- 
 piece , Mr. Harry Evans , adjudicator , place 
 3urnley Clarion ( conductor , Mr. F. Brunton ) 
 singing Pinsuti ' hour softened 
 splendour . ' Manchester Clarion ( conductor , Mr. T. 
 Corlett ) , place second , sing Walmisley ' Music 
 powerful 

 concert April 29 Leopold Orchestra , 
 small combination lady gentleman 
 pleasure study cultivation high - class orchestral 
 music . programme include Mendelssohn ' Melusine ' 
 overture , Beethoven Symphony , Délibes Ballet 
 Suite ' Kassya . ' Mr. Leopold conduct , 
 proceeding vary soprano solo contribute 
 Miss Amy Taggart 

 concert wane season creditable 

 month bring veteran i’rench 
 composer , Dr. Camille Saint - Saéns , present 
 fourth ' rench concert 12 , programme 
 constitute entirely composition . Dr. Saint 
 Saéns play pianoforte accompaniment , 
 co - operate Madame Chailley- Richez 
 performance piece pianoforte 
 * Réverie du Soir ’ Scherzo — later 
 interesting Pianoforte trio e minor . String quartet 
 e minor , play Messrs. Chailley , Gravrand , 
 Jurgensen Schidenhelm , open concert . Madame 
 Georges Marty sing series song ; M. Marcel Chailley , 
 able executant , appear previous concert , 
 play Concertstiick ( Op . 20 ) dedicate Sarasate ; 
 M. Schidenhelm contribute violoncello solo , 
 * Le Cygne ' Allegro appassionato 

 concert Beethoven Society 
 April 28 , honorary conductorship 
 Mr. E. Gordon Cockrell . programme include 
 * Hebrides , ' ' magic flute ' ' Rienzi ' overture , 
 movement ' Pastoral ' symphony , Widor rarely 
 hear ballet suite ' La Korrigane . ' Mr. A. Barnes , local 
 amateur , play Mozart Violin concerto , 
 prelude romance suite Reiss , Miss 
 Gertrude Grant contribute song 

 Brodsky Quartet Concerts 
 7 . programme comprise Boccherini String 
 quartet . 2 e minor , Tchaikovsky Pianoforte trio 
 minor ( op . 50 ) , Beethoven Quartet b flat 
 op . 130 ) . Mr. Percy Grainger pianoforte help 
 secure specially fine interpretation trio . Edith 
 Robinson Quartet , consist Miss Edith Robinson , Miss 
 Isabel McCullagh , Miss Edith Craven Miss Mary 
 McCullagh , play surprisingly concert 
 4 , Mozart ( Quartet C ( Op . 76 ) , Dvorak 
 ( Quartet e flat ( Op . 51 ) , Beethoven ( ) uartet e flat 
 ( op . 74 ) perform 

 quartet combination 

 district disadvantage topographically 
 culture concerted music choral instrumental 
 south - west corner England . distance 
 metropolis , land’s - end position , peninsular formation 
 rural character condition 
 cultivation music town country 
 difficult expensive . particularly gratifying 
 note increase number choral society , chiefly 
 | small country place member 
 case rough road long distance travel weekly 
 rehearsal , difficulty secure 
 scratch band involve hard work expense . 
 case conductor man high aim enthusiasm 
 spare effort pocket desire attain 
 high standard , personal 
 | inconvenience 

 
 | Plymouth , Devonport Stonehouse suffer 
 | disadvantage rural district , 
 matter choral music advance quantity . 
 | fact , old society cease exist , 
 | ground leave open survive risk 
 | competition want support . exist Plymouth 
 |only . Devonport Stonehouse participate effort 
 | premier ( size ) town , exercise 
 |any enterprise account , 
 | Devonport suggestion 
 season form local choral society , apparently 
 idea fall . old - establish choral 
 | orchestral society Dr. Mr. Walter Weekes 
 | progress , having past season 
 | adopt subscription scheme . second 
 concert , February 26 , Beethoven Symphony C minor 
 movement Raff C play 

 Misses Smith deserve commendation thank 
 presevering effort cause chamber music , 
 | effort Plymouth dependent 
 |this particular entirely visit virtuoso . 
 | January 2 , Brahms Pianoforte quintet ( Op . 34 ) 
 successfully play Misses Florence Lily Smith , 
 Messrs. A. E. Serle H. R. Ball , Mrs. H. R. 
 Freeman . February 19 Sonata Veracini 
 pianoforte violin introduce , Sonata pianoforte 
 violoncello César Franck play , 
 F major Pianoforte trio Schumann , performer 
 trio sister , Misses Florence Lily Smith 
 Mrs. Freeman 

 Thomas Church , unveil Sunday , 17 . 
 MADRID 

 concert Berlin Philharmonic Orchestra , 
 |at Paris , Dr. Richard Strauss come city , 
 Beethoven - Wagner - Strauss concert Royal 
 Theatre immense success 

 St 

 Miss Ethel Dyer sing . unaccompanied singing 

 choir excellent , playing orchestra ( led| LeoMINSTER.—The Chorai Society concert 
 Mr. W. A. Easton ) Mendelssohn ' Cornelius ' march , | 13 , ' Hiawatha Wedding - feast ' Stanford 
 Minuet Finale Beethoven Symphony , and| ' revenge ' satisfactorily perform , 
 dance ' Nell Gwyn ' Edward German | conductorship Mr. Herbert Crimp , credit 

 greatly appreciate . Mr. G. A. Stanton conduct , 
 Miss M. C. Williams accompanist 

 glory God nature ic reation ’ 

 4 
 Beethoven 

 5 . fac ut portem ( ' ' Stabat Mater " ' ) ... . »   G. Rossini 

 o come , let worship ( ' * Psalm xcv . " ) 
 F. Mendelssohn - Bartholdy 

 twilight gently fall ( Ave Maria ) J. Raff 
 song Penitence ( Busslied ) Beethoven 
 BASS . 
 . mighty Lord king glorious 
 ( * * Christmas Oratorio ’' ) . J. S. Bach 
 . roll foam billow ( ' * creation " ) ... J. Haydn 

 F. Schubert 
 glory God nature ( Creation Hymn ) 
 Beethoven 
 . Paui 7 
 . Mendelssohn- Bartholdy 
 Ch . Gounod 

 consume 

 20 . BaTTENBERG , H.R.H. Princess Henry . hear , Holy 660 . Morty , F.W. O thou Altar ’ s live coal 

 Father es - - ei ie ee ad . » d.| 514 . thank Gop ( Nun danket ) . om 
 BatTTenseRG , H.R.H. Princess Henry . LordofGrace .. 1d.| 538 . Otprootn , ( A'l people ) . ( 7 wo versi ns ) 
 BeetHoven . Vesper hymn . ( version ) oe - . d./624 . Paxratt , W. God Glory , King nation 

 jot . Bennett , G.J. O Perfect Love ( wedding ) .. tad . | 529 . tParratt , W. Lord Hosts , hast enduedu 

