Vocalist, Madame Valleria. V ioloncello, Herr Julius Klengel. The | WEDNESDAY, THURSDAY, FRIDAY, and SATURDAY, 
full Crystal Palace Orchestra. Conductor, Mr. August Manns. Stal! October 22, 23, 24, 25, 1890. 
tickets (transferable) for the twenty Concerts, Two Guineas each

pei He 7 Wepnespay Morninc—REDEMPTION . Gounod. 
CHELTENHAM MUSICAL FESTIVAL, | Wepsespay Evexisc—GRAND MISCE LLANEOUS CONCERT. 
1890 Tuurspay Morninc—ELIJAH ae Mendelssohn. 
2 | Fripay Morninc—JUDITH C. Hubert H. Parry. 
President: Sir Hersert Oaketey, Mus. Doc | Zespay Evewno— Tle sy DEN LEGEND. +» Sullivan. 
Musical Director: Mr. J. A. MATTHEWS. | SaturDay Morninc—MESSIAH ase +2 Handel. 
. owes - . Principal PERFORMERS. 
TUESDAY EVENING, OcTOBER 28. | 
| Mac 3 
THE CREATION (PART 1), HAYDN. | se naeicetes. 
CONCERTO in D (For Viottn and OrcuEstRA), BEETHOVEN, Miss HILDA WILSON. Madame HOPE-GLENN. 
STABAT MATER, DVORAK. | Mr. EDWARD LLOYD. 
- | Mr. IVER —, Mr. ANDREW BLACK

VeEpDNESDAY EVENING, OCTOBER 29. . WATKIN MILLS 
THE RE PENTANCE OF NINEVEH, Professor J. F. BRIDGE Mr. B. PIERPOINT: Mr. MONTAGUE WORLOCK. 
(Conducted by the Composer

exists of noting it, must eventually become utterly

lost. We need not go so far as the time of old 
Egypt to illustrate this vanishment. How little do 
we now know of the effect produced by the instru- 
mental music used (say) in the Elizabethan Masques; 
or how can we judge what “The Messiah” 
sounded like with the multitude of oboes and 
bassoons Handel employed, as compared with the 
meagre provision of strings; or to come to later 
times, Ries and Czerny describe Beethoven’s extem- 
poraneous music as finer and more wonderful than 
anything he committed to paper—alas! it is lost 
for ever. What remembrance have we of Handel’s, 
and of Mendelssohn’s marvellous improvisations ? 
Unless music is noted—pricked, as our forefathers 
termed it—it soon becomes forgotten and lost

586 THE MUSICAL TIMES.—Ocroser 1, 18go

ES.—OcroBer 1, 18go. 591

THE BEETHOVEN-HOUSE SOCIETY IN 
BONN

Apart from the interest attaching to it as the 
birthplace of Beethoven, Bonn is memorable for the 
three Beethoven Festivals which have been held 
there in honour of the great master. The first, con- 
ducted by Liszt and Spohr, took place in 1845, on 
the occasion of the unveiling of the monument 
erected there to Beethoven’s memory. ‘The second, 
conducted by Ferdinand von Hiller, tollowed in 1871, 
in commemoration of the centenary of Beethoven's 
birth. The third, the primary object of which was 
to augment the fund which had been raised for the 
purchase of the house in which Beethoven was born, 
and for the establishment of a Beethoven Museum, 
was held in May last. Of these three Festivals the 
first two were orchestral and choral; the last was 
devoted exclusively to the master’s chamber music, 
both instrumental and vocal. Of the first two it is 
unnecessary at this date to speak in detail, except to 
point out that by a strange chance a retrospective

account of that of 1845 appeared in THE MusIcat

With so well devised and representative a pro

gramme, and with such an array of executive talent 
for its presentation, the success of the Festival, 
from an artistic point of view, was assured. It is 
satisfactory to add that financially it was equally 
successful, the sum of 16,000 marks (f{800) having 
been netted for the fund. Unique in its scope, as 
illustrating by no means the least important side of 
3eethoven’s artistic achievements, and as one which 
seldom figures on Festival occasions, it must have 
been a cause of unusual satisfaction to many in 
Germany, where Beethoven’s chamber music is less 
generally familiar than, thanks to Mr. S. Arthur 
Chappell’s ‘ Popular” Concerts, it is in England. 
From these preliminary remarks we may now turn 
to the main subject of this notice—the Beethoven- 
House Society, or to give it its German appellation, 
the “ Verein Beethoven-Haus.” It was not till the

news of Beethoven’s death, which took place at 
Vienna on March 26, 1827, reached Bonn that the

worthy burghers of that city fully realised the loss 
|they had sustained, and the honour which accrued 
|to their city as the birthplace of so great a master. 
| For the first time they now felt thoroughly proud of 
[their former townsman, who, as a young man of 
| twenty-two, and the descendant of an obscure and im- 
| poverished family, had some five-and-thirty years 
| before migrated to Vienna, and had since made himself 
| famous throughout the musical world as the greatest 
jliving composer. Foreseeing the lustre which the 
| memory of so great aman would shed upon their city, 
| they soon began to investigate his antecedents and

search the records for the house in which he was 
|born. As in the days of ancient Greece, where seven 
| cities contested the honour of being the birthplace of 
|Homer, so now the inhabitants of four separate 
| streets in Bonn, in which at one time or another the 
| Beethoven family had resided, asserted their claim 
|to Beethoven having been born in their midst. For

a long time a house in the Rheingasse, in which his 
| mother died, was believed to have been his birthplace, 
|andatablet to this effect was affixedtoit. Subsequently 
a claim was put in in favour of a house in the Bonn- 
| gasse, which the Beethoven family had previously in- 
|habited. In 1838, or thereabout, the question was 
| fiercely debated as to which of these two houses was 
|the master’s veritable birthplace. At length Mr. 
A. W. Thayer, Beethoven’s enthusiastic and most 
|indefatigable biographer, came to the rescue, and 
lafter diligently searching the municipal and eccle

siastical records, conclusively proved in favour of the 
| house in the Bonngasse. A tablet recording the fact 
| was accordingly affixed to it on the occasion of the 
| Centenary Festival of 1571

It was not, however, until the spring of 1859 that, 
| under the honorary presidency of Dr. Joseph Joachim, 
a society was established in Bonn for the purchase of 
|the house, and for its preservation, as a lasting

memorial of the great master who first saw the light 
init. During the Festival of May last, when an ex- 
{tensive loan collection of Beethoven relics was 
brought together from all parts, it was thrown open 
to view, but only to a limited number of visitors at 
ithe same time. Since that date immense progress 
| has been made in the work of restoring it, as far as 
| possible, to the exact condition it bore at the period 
| of Beethoven's childhood. 
The progress thus made is said to be chiefly due to 
|the laudable exertions and enthusiasm of Herr 
| Wilhelm Kuppe, a musician long resident in Bonn, 
| who, by the energy and perseverance which he has 
| displayed in the exercise of his art and profession, 
|has acquired a competence, which enables him to 
| devote the greater part of his time to superintending 
ithe work of restoration now in progress, and has 
admitted of his undertaking the honorary curator- 
| ship of the house and its contents. 
| <A recent visit to it, or rather to that portion of it 
| which the Beethoven family occupied, and which was 
opened to the public in August last, was one of 
extreme interest. It might, perhaps, best be described 
as a double house, or as a tolerably commodious 
| house, facing the street, with a smaller one back to 
back with it in the rear and looking into a small 
garden. At the time of the Beethoven family’s 
residence there, the front ground floor and the first 
floor were occupied by one Clasen, a dealer in lace, 
and the owner of the house. Salomon, famous for 
his connection with Haydn, whose Symphonies he 
was the first to bring to England, with his family, in- 
habited the second floor. The Beethoven family 
dwelt in the smaller tenement at the back

The four small rooms which it contains are fully 
large enough to hold the small collection of Beethoven 
relics which the Society has as yet acquired, and

92 THE MUSICAL TIMES.—Ocvoser 1, 1&go

which at present can only be regarded as forming

the nucleus of the larger collection which, as 
opportunities arise and as funds permit, it hopes to 
acquire, and for the accommodation of which the 
larger rooms in the front part of the house 
will amply serve. But as Beethoven relics are 
both costly and difficult to meet with, a long 
time must probably elapse before the promoters 
of the scheme can hope to see themselves in 
the possession of so complete a Beethoven Museum 
as they desire. The collection, though at present a 
small one, contains much that, in the way of 
original manuscripts, busts, portraits, &c., cannot 
fail to be of interest to the student of Beethoven; 
and much that is calculated to rouse the visitor’s 
sympathy and commiseration for the mean and 
sordid surroundings of the great composer's early 
youth. Many will surely shed tears on beholding the 
wretched garret—a small lean-to attic in the roof—in 
which Beethoven is said tohave been born. Scarcely 
less affecting is the sight of the ear-trumpets, manu- 
factured by Maelzel in 1812-14, which on account of 
his deafness Beethoven used up to the time of his 
death. They are four in number, and of various size 
and shape. What adds to the horror, at least of one 
of them, is the metal band with which it is furnished, 
for the purpose of attaching it to his head while 
playing the pianoforte or conducting. As a special 
point of interest allusion may be made to the grand 
pianoforte specially manufactured for the master by 
Graf of Vienna. Its peculiarity consists in the fact that 
almost throughout it has four strings to each note. 
In addition to the ordinary so- called loud and soft 
pedals, it has a pianissimo, or echo, pedal, acting as a | 
damper upon the strings, and is said to have been for- 
merly furnished with resonators, but which were long 
agoremoved. Noless interesting isthe quartet of string 
instruments upon which the ce lebrated Schuppanzigh 
quartet party were in the habit of playing. It con- 
sists of a violin by Nicolaus Amati, 1690; a vioiin by 
Jos. Guarnerius, fil. Andreas, Cremona, 1718; a viola 
by Vicenzo Ruger detto il Per, Cremona, 1690; and a 
violoncello by Andreas Guarnerius, Cremona, 1675, 
together with four bows, &c. ‘The two violins bear 
Beethoven’s seal on the back just below the neck; 
and a big B has been scratched on the back, presum- 
ably with the point of a pen-knife. It would be easy 
to extend the list of curiosities, but we think we have

said enough to show that the house and its contents

pronounce its opinions. 
no other profession so full of impostors, charlatans

This is not quite correct, at 
At the Richter Concerts 
Beethoven is often graciously admitted to the com

and dilettante, and no other class of charlatans that | panionship of Wagner

edifying to the ungodly of Yeovil! jarranged, and that the singer now takes a large 
—— | proportion of the property

Le Ménestrel announces the immediate issue by | 
Breitkopf and Hartel of two unpublished works by | An example of respect for a composer's inten- 
Beethoven—an arrangement for pianoforte only of | tions :—At Vienna, lately, ‘* La Juive” was performed 
the Pianoforte Concerto in E flat, and, in full score, | with the serenade, all the duets, and several other 
the first movement of a Pianoforte Concerto in D,/numbers omitted, while the ballet was repre- 
presumably that which Beethoven is known to have | sented by a pas de deux danced to music not by 
begun subsequent to the completion of the! Halévy! 
“Emperor

A BoLp journalist of Buenos Ayres, writing of an 
Wuen Scharwenka and “a party of gentlemen” | opera company there, declared the feminine artists 
entered the Concert-room at Brighton Beach, “ inci- | t9 be an assemblage of Junos and Venuses, but that, 
dentally making considerable noise,” Anton Seidl, | unhappily, the Lucrezias were rarer than the Mes- 
the conductor, looked annoyed, but, recognising the | salinas. It is to be hoped that the writer “ cleared 
pianist, forthwith played his arrangement of Chopin’s | out * with speed when he saw the werds in print. 
“Funeral March!’? An American contemporary | 
7 Ic 6“ 2gAEe incide ” 
ail a aeiiialals M. Maxime Lecomte has drawn up a bill for lay- 
ae . |ing a tax of ten francs upon each organ, pianoforte, 
Horrors upon horrors’ head accumulate! Acertain} 44 harmonium. He claims that revenue should be 
man named Kuhmeyer, of Presburg, has invented a | obtained, as far as possible, from articles of luxury. 
machine played like a pianoforte, and producing | 4 cynical commentator adds “and especially troim 
sounds from six violins, two violas, and two violon- | instruments of torture.” i 
cellos, concealed in the body of the instrument. This | 
— ORTH TE: SECON BY SE RTE 6g ce ci) xs has given the nucleus of a valu- 
— able museum to Dieppe, in the shape of rich Louis 
Ir is difficult to mistake the “true inwardness ” of Quinze furniture, jewellery, paisngt, é. large library, 
the following advertisement, which recently appeared | 2 Collection of autographs, the MS. of ‘the Soldiers 
ae 2 : : y apr Chorus in “ Faust,” and a MS. march by Mozart. 
in France: ‘“*A grand opera in manuscript, un- Th Pre Rite gad i ie pe ? 
published and complete, with full orchestration, the COMICON 5 VEER BE £4,000. 
work of a composer recently dead, is for sale secretly. 
Strictly confidential. Address. A. M., Poste Restante, 
Marseilles.” Here is a chance

598 THE MUSICAL TIMES.—Ocroser 1, 1890

torious execution of Beethoven’s ‘“ Ah, pertido!”’ and Mr. 
Edward Elgar for the personal introduction of a romantic 
Concert-Overture, entitled ‘ Froissart.” Mr. Elgar, 
formerly of Worcester, is now, we believe, a resident in 
London, where, it may be hoped, and, given opportunity, 
even expected that he will make his mark. The Overture 
is of course chivalric in style, and, perhaps, more commend- 
able for what it tries to say than for the manner of its 
expression. There is upon it, what surprises no one—the 
mark of youth and inexperience; but it shows that, with 
further thought and study, Mr. Elgar will do good work. 
He must acquire greater coherence of ideas, and concise- 
ness of utterance—those inevitable signs of a master, only 
to be attained by extended and arduous effort. For such 
effort, no doubt, Mr. Elgar may be trusted, ‘‘ Froissart”’ 
was much applauded—the Prophet had honour even in his 
own country. In the second part of the programme were 
the Introduction to the third act of “* Lohengrin” ; Grieg’s 
Suite ‘“ Peer Gynt,’ and a new eight-part chorus, ‘lo 
morning,” composed by Mr. C. Harford Lloyd. This is a 
very interesting and effective example of pure vocal writing, 
in a style which combines to some extent the science of the 
ancient madrigal with the grace and charm of the modern 
part-song. Mr. Lloyd's counterpoint is facile, each 
“voice” moving freely even amid the difficulties of eight 
parts, while the general etfect of the music is in the highest 
degree pleasing. Pending the composer’s next important 
production, amateurs are glad to have such an example of his 
taste and skill as ‘To morning.” The piece was capitally 
sung by the Leeds people

Thursday morning brought the novelty of the 
Festival—Professor Bridge’s Oratorio ‘‘ The Repentance 
of Nineveh ’’—to hear which came many musicians from 
stress of work or from enjoyment ofrest. The curiosity was 
natural. The composer had done nothing on the same 
scale before, but had accomplished enough to excite 
an expectation of success. How far that expectation was 
met is a question to which different people might give 
answers not exactly the same, but there can be no doubt 
that ‘“* Nineveh ” has advanced its composer’s position by

Of large standard compositions the programme 
Mozart’s * Requiem

upon it. 
contains the ‘*Golden Legend,” 
and two parts of the “ Creation.” These, with the new 
Cantata, occupy the two mornings. ‘The evening Concerts 
are both of a miscellaneous character, having among their 
more important features Mendelssohn’s Violin Concerto, 
Beethoven’s Symphony in A, Parry’s “blest Pair of 
Sirens,” Stanford’s “ Revenge,’ Mendelssohn's ‘ Judge 
me, O God,” and Sullivan’s Overture, *\ [n Memoriam.” 
(hese are certainly good enough for any Festival. 
Mesdames Nordica, Damian, and Macintyre: 
Lloyd, Mills, and Foli are engaged as solcists; there will

The

Tue local musical season bids fair to be one of singular 
activity. It looks, indeed, as if all previous records are to 
be broken, and, as is too often the case, Concert-givers will 
be found in serious collision with each other in the matter 
of dates. For example, Sarasate, Sir Charles Hallé’s 
orchestra (two performances), and the Carl Rosa Opera 
Company appeal in one and the same week—just a large 
enough order all at once for most Glasgow amateurs

A forecast of the Choral and Orchestral Concert scheme 
was given in last month’s Musica. Times, and to the list 
of choral works Verdi’s ‘‘ Requiem” has now to be added. 
It will be given the same evening as Dr. Hubert Parry’s 
‘Ode to St. Cecilia’s Day.”” The orchestral programmes, 
carefully drawn up under the guidance of Mr. August 
Manns, will be found to include Dvordk’s new Symphony, 
No. 4, in G; Spohr’s Violin Concerto, No.g; Mr. Fred. 
Cliffe’s Tone-Picture, ‘“‘ Clouds and Sunshine’”’; Mr. Staven- 
hagen’s new scena for soprano and orchestra, ‘“ Suleika”’ ; 
Henselt’s first Pianoforte Concerto; selections from Raff’s 
‘“Ttalian’’ Suite, and from Saint-Saéns’s Ballet music 
‘¢ Ascanio,” and Beethoven’s Symphonies, Nos. 7 and 8. 
The list of solo violinists has been further strengthened by 
the engagement of Mr. Emil Sauret and Herr Hans Wessely. 
The services of Mr. Santley have also been secured, and it 
may be of interest to note that the great English baritone, 
who has not been heard at these Concerts for many years, 
will make his first appearance on returning from his 
Australian tour. Up to date the Guarantee Fund amounts 
to £3,335, almost £1,000 more than at this time last year

The thirty-seventh season of the City Hall Saturday 
Evening Concerts commenced on the 2oth ult., when an 
excellent company, which included Mr. Ffrangcon Davies, 
the new baritone, appeared. Mr. Airlie has booked, we 
understand, several important engagements for these 
popular and well-conducted Concerts, and in quite 
another line of business the smaller choral societies 
have also been showing renewed enterprise. Thus 
Mr. A. R. Gaul, whose good fortunes are so well merited, 
is again to the fore with one of his Cantatas, no fewer 
than four church choirs having taken up his latest sacred 
work, ‘*The Ten Virgins.” These are St. George’s- 
in-the-Fields, Shamrock Street, John Street, and Kelvin- 
grove Street United Presbyterian Church Choirs. Mr. 
Gaul’s perennial Cantata, “The Holy City,” has also 
found favour with the Strathbungo Parish Church Choir ; 
Weber’s Mass in G will be rehearsed by the Pollock 
Street U.P. Church Musical Association; Mr. Louis N. 
Parker’s * Silvia” has been fixed upon by the Eglinton 
Street Congregational Church Choir, as also by the Cath- 
cart Musical Association; and the Bridgeton Choral 
Union will produce during the season Handel’s ‘‘ Joshua

But our thoughts are now beginning to turn toward 
the winter Concert rooms, and to be occupied with 
anticipation of keener musical delights. Rumours of what 
may be expected during the colder months are beginning to 
{ly about, and hopes are excited that some novelties may 
reach our longing ears, as well as excellent feasting upon 
well known and loved classical dainties. At the large St. 
James’s Hall, which has been elaborately decorated, Mr. 
T. A. Barrett has resumed the entertainments which 
last season proved so attractive, announcing a great 
number of engagements which ought to satisfy his friends 
and to ensure no diminution of the vigour which hitherto 
has characterised his undertaking. The Carl Rosa 
Company is occupying the Prince’s Theatre, and will, 
probably, ere it leaves us, depart a little more boldly from 
the old list of familiar works. Especially would we 
welcome a revival of Goring Thomas’s ‘ Nadeshda,”’ 
which, after the encomiums bestowed upon it, has so 
strangely been withheld from us of late

Sir Charles Hallé announces that, as usual, on the last 
Thursday in this month he will open his season, when he 
will, no doubt, meet the congratulations of his friends upon 
his return from triumphs in Australia and immediately 
after will conduct what we all hope will be a successful 
Festival at Bristol. That the twenty Concerts will be 
enriched by many orchestral novelties and exhibit the 
splendid orchestra in undiminished force and finish of 
performance no one doubts; and the promise of Dr. Parry’s 
“ Judith’? and Brahms’s ‘“‘ Requiem” after Christmas is 
most welcome ; while the revival here of Beethoven’s great 
Mass in D must be taken as evidence that the able Choir 
Director, Mr. R. H. Wilson, is grasping the reins firmly 
and hopefully. At the first Choral Concert, November 6, 
“Judas” will be given; to be followed, of course, by 
“Elijah,” and the customary two performances of ‘ The 
Messiah” on December 18 and 19

To open his twentieth campaign Mr. de Jong has 
secured the services of Mr. George Grossmith, who always 
crowds the Free Trade Hall to its utmost capacity ; and 
during the winter we are to have five Orchestral and five 
Ballad Concerts, for one of which the new precocity, Max 
Hambourg, is engaged

Two weeks after your own Worcester Festival, the 
thirty-third annual Musical Festival of Worcester, 
Massachusetts, will take place, under the Conductorship of 
Mr. Carl Zerrahn. The choral works to be performed are 
‘Israel in Egypt” (Selections), ‘‘ Elijah,” ‘* The Golden 
Legend,” “ The Erl-King’s Daughter,’ J. C. D. Parker's 
“*1\¢demption Hymn,” and choruses from “ Lohengrin’ 
and ** Tannhauser.”’ The principal instrumental composi

tions selected are Schumann’s Symphony in E flat (No. 3), 
| Beethoven’s Seventh Symphony, a Suite for string 
orchestra, by Victor Herbert, the Assistant Conductor of 
the Festival; Rubinstein’s ‘‘ Bal Costumé,”’ and ‘An 
Island Fantasy,” by J. K. Paine

__ In our city the dead season has not come to an end yet. 
| With the exception of a few comic operas at some of the 
smaller theatres, the music lover who was obliged to spend 
the dog daysin hot New York had nothing but the Strauss 
| Concerts at the immense Madison Square Garden Hall to 
| appease his musical appetite with. These Strauss Concerts 
| are not an artistic success. The band is too small for this 
| large hall, and does not play well together. They perform 
| nothing satisfactorily but Strauss waltzes, but even these 
we have heard given more effectively at the Thomas 
Concerts at Lenox Lyceum last season. ‘The Strauss 
Concerts are soon to end, and in their place the popular 
Conductor, Herr Anton Seidl, is to commence, on the 
2oth inst., a series of Orchestral Concerts, with a band 
numbering 100 performers. Besides these “‘ Seidl ’’ Concerts 
| we shall soon have the regular Sunday night ‘“‘ Thomas” 
Concerts at Lenox Lyceum, while Mr. Franko promises a 
lseries of Orchestral Concerts of lighter character. 
| Between these and the Concerts given annually by the 
| Boston Symphony Orchestra and our own Philharmonic 
and Symphony Societies, our amateurs will soon have 
plenty of opportunity to revel in the realms of their 
favourite art

The New York Music Hall, the laying of whose founda- 
tion stone we reported in our last letter, is to be ready for 
occupancy by January 1, 1891. It is a very handsome 
building in the most fashionable part of our city, and holds 
within its walls three separate halls to accommodate 
respectively 3,000, 1,200, and 500 persons, each Hall having 
| separate lobbies, ticket offices, and entrances. It will be 
| the home of our Oratorio, Symphony, and Philharmonic 
| Societies

Tue Hornsey Rise Amateur Orchestral Society (Con- 
|ductor, Mr. Arthur Moody) proposes to include in the 
| works to be performed during the season, which com- 
| mences on the rst inst., the following works: Symphonies, 
|Haydn’s No. 2, in D; Beethoven’s No. 1, in C; 
Haydn’s ‘“ Military,” Haydn’s ‘* The Farewell’ and ‘‘ Toy 
| Symphony,” &c.; Overtures, ‘ William Tell,” ‘* Masa- 
| niello,”’ “* Poet and Peasant,’ and ‘*Semiramide,” &c. ; 
lselections from ‘ Nautical Songs,”  ‘ Carmen

Bohemian Girl,” “ Pinafore,’ &c.; ‘* The War March ot

and ending on March 21. The Monday Concerts will 
commence at eight instead of half-past eight, and the 
Saturday Concerts will begin at three

THE fifth series of Messrs. Hann’s Chamber Concerts 
will be given at the Brixton Hall, Brixton, on the 28th 
inst., and November 18 and December 9. The following 
works, among others, will be performed: Quartets by 
Schubert, Mozart, Haydn, Mendelssohn; together with the 
Trio in B flat, Beethoven (Op. 97); the Quintet (pianoforte 
and strings), Dvorak, &c. Some vocal selections will be 
given at each Concert

S1cNnor LaGo announces the opening of his opera season 
at Covent Garden on the 18th inst. The operas likely 
to be produced are Ponchielli’s ‘‘Gioconda,” Wagner's 
“‘Tannhauser,” and Verdi’s ‘ Othello,” with, probably, 
Gluck’s ‘‘ Orfeo.””, Among the artists engaged are Madame 
Albani, Miss Damian, Miss Fanny Moody; Messrs. 
Manners, Galassi, Giannini, and Maurel

Some few years ago the Prussian Government purchased | 
fom Herr Paul de Witt, of Leipzig, a most interesting | 
collection of antique musical instruments, which has been | 
the delight ever since of connoisseurs visiting the Berlin | 
Museum. In the comparatively short time which has | 
elapsed, the indefatigable collector has succeeded in bring- 
ing together a second and almost equally valuable collection 
which, in the exercise of a most praiseworthy public spirit, | 
has just been likewise acquired by the authorities of the | 
museum in question

A highly interesting sale of autographs by eminent musi- 
cians is announced by the firm of Liepmannssohn, of 
Berlin, to take place on the 13thinst. It includes important | 
numbers by Beethoven, Berlioz, Cherubini, Chopin, Men- | 
delssohn, Meyerbeer, Schumann, Verdi, Wagner, C. M. von

Weber, and many others

The Recent Discovery of Egyptian Flutes and thei

The Great Composers—Wagner ee ee ee ae £ 
| Advance, Chicago! .. = ee e ee as ee wa 59 
The Beethoven-House Society in Bonn 591 
| Occasional Notes .. aa aa oe oe ae es 502 
| Facts, Rumours, and Rem: " S £G2 
Worcester Musical Festival ae sof 
Coming Festivals da ‘ea -. 599 
Report of the Associated Boa die of the Royal and Royal 
College for Local i: nations in Music me at +. 599 
| Dr. Mackenzie's Music to“ Ravenswood” .. oa re ant 
| Globe Theatre ax x“ 601 
| The Tonic Sol-fa Jubilee in 1 601

strument in the world, and was | 
constructed by Messrs. Hill and Son, of London, the builders of the

18, and December 9 (Tuesday), 1890, at 8 p.m. The following works

will be performed: Quartets, Schubert, Mend: ‘Issohn, Mozart, Haydn; 
Trio in B flat, Beethoven (Op. 97); Quintet (piz inoforte and st rings), 
A. Dvorak, &c

M USIC SCHOOL.—CHURCH OF ENGLAND

ANALYSIS OF FORM 
AS DISPLAYED IN

BEETHOVEN’S

THIRTY-TWO PIANOFORTE

VINCENT

Studies and Pieces required for the above Examinations :— 
STUDIES. 
BACH.—Invention in two parts, No. 9, in F minor 
CRAMER.—In A, No. 13 ; a 
CHOPIN.—Prelude in F ‘sharp, Op. 28, No. 13 a 
CLEMENTI —In B, Gradus ad Parnassum, No. 28 
CZERNY.- In F, Op. 299, No. 4 
BERTINI. ine, Op. 29, No. 2 
CR. \ME R.—In C minor, No. 4. 
MOSCHELES,.—In E, Op. 70, No. me 
PIE CES. 
BEETHOVEN.—Rondo in C, Op. 51, No. 1 sis oe “308 
MENDELSSOHN,—Lied ohne W orte, in B minor os oa. a 
PARADIES.—Sonata in F, No. 5 - xe his 
BACH.—Pre 7 and F ugue in x minor ‘ : 
BEETHOVEN.- Allegro from Sonata in FE flat, Op. 31 
SCHUMANN.—Novellette in E, Op. 21, No. 7

BEETHOVEN.—Quant e piu bello, with variations 
MOZART

Allegro maestoso from Sonata in A minor .. 
SCHUBERT.—Impromptu in B flat, Op. 142, No. 3 
EXAMINATIONS OF THE NATIONAL SOCIETY 
OF PROFESSIONAL MUSICIANS :— 
SCARLATTI.—Harpsichord Lesson in D, No. 16.. ‘ 3 
Harpsichord Lesson in E minor, No. 18 2 
Harpsichord Lesson in D, No. 21. ee “« 2 
Dr. C.— ty Vivace .. aie 4 4 
Etude Melodieuse 4 
"INCE NT, G. F.—Con Energia ae as ne : 
Staccato e legato.. Ss ‘ ey iar

NEW FOREIGN PUBLICATIONS.| THE

BECKER, ALBERT.—Auf Kaiser Friederich’s Tod ; Funeral 
March. For Orchestra and Chorus. Op. 60 :— 
Full Score ae vn ne ‘is a a a5-O 
BEETHOVEN, L. V AN — Overture from the Ballet ‘ The 
Creatures of Prometheus.” Op. 43. Arranged for two 
Pianofortes (eight hands) .. ed uP “a a are 
DANCLA, C.—Three Short Pieces. For Violin and Pianoforte

Op.177. No.1. L'Eglantine; No.2. La Violette ; No. 3

