x jante , beginning January , 
 organist choirmaster parish West- 
 mill . surpliced choir . present organist hold appointment 
 Music Master Buntingford Grammar School . good 
 field choral class private tuition neighbourhood . 
 apply Rev. J. A. Ewing , Westmill Rectory , Buntingford 

 CHUBERT SOCIETY , Beethoven Rooms 

 27 , Harley - street , W.—President : SIR JULIUS BENEDICT 

 Crystal Palace distinguish 
 attention pay work english composer . 
 appropriate concert 
 present season open overture late 
 Sterndale Bennett , select ' " 
 Merry Wives Windsor , " 

 hear Sydenham . Overture inclined 
 consider Bennett good work , pleasing idea , 
 mark finish detail 
 characteristic composer . novelty 
 concert — solid - write , 
 heavy , Concerto Hans von Bronsart , 
 admirably play Mr. Fritz Hartvigson ; , 
 selection suite french composer Ernest 
 Guiraud , assuredly solid heavy , 
 contrary light piquant , great 
 depth invention . afternoon superb 
 performance Beethoven Symphony . 
 vocalist concert Madame Signor 
 Campobello 

 second concert include Haydn delightful ' Mili- 
 tary " ? Symphony , Weber Concertino clarionet , 
 fine tone , remarkable execution , excellent 
 style Mr. Clinton ( clarionet Company 
 band ) hear great advantage . special feature 
 afternoon , , large selection 
 work Wagner , include overture ' ' Rienzi , " 
 Prelude ' " Elsa Dream " ' ' Lohengrin , " 
 Funeral March ' " Gétterdammerung , " March 
 compose Centennial Festival Philadelphia . 
 number pre- 
 viously hear concert - room ; need 
 dwell . Funeral March , follow 
 death Siegfried , truly wonderful instrumentation , 
 splendidly Mr. Manns direction , 
 create profound impression , apart stage . 
 Centennial March , hand , alike unworthy 
 occasion write repu- 
 tation . composer . vocalist concert 
 Miss Sophie Lowe Mr. W. Shakespeare 

 fourth concert , Crystal Palace Choir appear 
 time season ; , far judge 
 single hearing , marked improve- 
 ment performance . chief feature con- 
 cert Gade charming Cantata ' Erl - King 
 daughter , " work , way , recom- 
 mend attention Choral Societies , 
 beautiful music , long ( occupy 
 hour ) difficult . solo 
 sing Madame Lemmens - Sherrington , Miss Boling- 
 broke , Mr. Maybrick . Adagio , string , 
 unpublished early symphony Haydn , 
 novelty special interest . movement 
 appendix Pohl " Life Haydn , " 
 decidedly old - fashioned style , worth revive , 
 merely historical point view , 
 sake . second novelty ' ' Marche Héroique , " 
 Camille Saint - Saens , brilliant composition , 
 workmanship interesting idea . 
 overture " Fidelio , " " Mendelssohn ' ' Reformation " 
 Symphony , song complete afternoon 
 programme 

 fifth concert , place 
 press ( 2oth ult . ) , chief work announce 
 Raff ' Lenore ' ? Symphony , Schubert Overture 
 ' * Alfonso Estrella , " Sullivan ' ' Ouvertura di Ballo , " 
 Beethoven Violin Concerto , play M. 
 Wieniawski 

 CARL ROSA OPERA COMPANY 

 person actively engage musical tuition agree 
 little know , cultivated 
 amateur , Mozart Sonatas . true iti 
 popular character stand 
 public estimation , find 
 embed heap showy piece form content 
 ' ' Canterbury " ' fashionable drawing - room ; 
 fact select represent Mozart 
 genius class composition prevent 
 search deeply wealth Miss 
 Zimmermann bring attractive volume 
 . , purity melody 
 masterly construction , study touch , phrasing , 
 cultivation shade expression , 
 work remain unrivalled , Sonatas 
 rarely play contain beauty surprise 
 hear time . editress , like 
 true artist , approach task reverence ; 
 Preface , , reason alter 
 insert offend rigidly 
 demand text Mozart . little 
 way , , need small apology , slur 
 ( add lengthen ) accurately 
 define , , " phrasing musical sense 
 different passage : " doubtless feel 
 valuable guide study master , 
 help professor , supply 
 explanation pupil case 
 clearly paper . change notation 
 portion Trio Sonata 11 perfectly 
 justifiable ; everybody sustain melody - 
 passage , write 
 play . vexed question appoggiatura 
 acciaccatura settle draw line 
 occur , allow 
 receive value emphasise note , retain 
 case original notation . as_it 
 ; , , importance preserve 

 old appoggiatura matter eye ear , 
 especially know Mozart adhere 
 positive system , proof instance 
 . celebrate Sonata minor , 8 , 
 open subject , commencement bar 2 , 
 appoggiatura g¥ , repetition , bar ro , 
 note appear ordinary quaver link 
 follow . experienced musician like Miss 
 Zimmermann , , carefully separate appoggiatura 
 acciaccatura , doubt subject set 
 rest , notation little consequence 
 player . glad find edition care 
 ensure correct intelligent phrasing 
 attention minute mark punctuation , 
 plain direction preface manner pro- 
 duce require accent touch . Mozart 
 time , , know leave 
 taste performer , confirmation 
 cite bar mark " piacere , " ' movement 
 sonata C minor , 14 , , 
 pause , authority late enthusiastic ex- 
 ponent Mozart music , custom ( 
 presume imitate method initiate composer 
 ) introduce short ornamental passage , 
 find indication notation . proof 
 necessity select - class executant edit 
 work importance , attention 
 fingering , need scarcely find 
 invaluable amateur . great change place 
 system fingering lately ; Miss Zimmermann 
 , , conservative adhere old 
 method passage freely execute 
 accurately phrase new . conclusion , 
 music clearly print , 
 date composition sonata , 
 ascertain , state . purity text 
 certify reference good english foreign 
 edition ; respect volume worthy 
 companion recently issue firm 
 editorship , contain 
 Sonatas Beethoven 

 Musical Myths fact . Carl Engel . vol . 2 

 MUSICAL TIMES.—Novemser 1 , 1876 . 671 

 hoven Sonata ( Op . 12 ) violin pianoforte , perform 
 Mr. Peck Mr. J. W. Phillips . overture — * La Dame 
 Blanche " ( Boieldieu ) ' ' Le Pré aux Clercs " ( Herold)—were effec- 
 tively play orchestra . Evening Concert , 
 management , Cutlers ’ Hall . Miss Barton 
 vocalist . Mr. Whitehead contribute : violoncello solo , Mr. 
 Phillips join Mr. Peck Beethoven Sonatas . 
 good glee - singing given.——On Monday , 16th ult . , Miss 
 Clara M. Linley Concert Albert Hall . pianoforte 
 playing Miss Linley , doubtless , chief item interest , 
 important piece Sterndale Bennett ' ' Maid Orleans " 
 Sonata , Liszt ' " ' Rhapsodie Hongroise , " 
 encore . Mr. John Peck ( violin ) render valuable assistance , 
 especially duet Miss Linley , ' Guillaume Tell , " 
 De Beriot Osborne , finale redemande , 
 Mr. J. Wainwright highly successful performance 
 english concertina . Madame Thaddeus Wells Mr. Laxton 
 vocalist . band 15th regiment , Mr. Murdoh , 
 play selection effect 

 STAFFORD.—Mr . E. W. Taylor , organist choirmaster St. 
 Thomas Church , successful Concert Shire Hall , 
 Monday evening , 2nd ult . , following eminent artist 
 engage : Madame Lemmens - Sherrington , Miss Jessie Jones , 
 Madame Patey , Mr. Hollins Mr , Patey , vocalist , Herr 
 Theodor Frantzen , solo pianist . excellent programme pro- 
 vide , render satisfactory manner 

 SouTHAMPTON.—Mr . J. Ridgway Pianoforte Recitals 
 past month Hartley Hall , play selection 
 work Bach , Beethoven , Mozart , Mendelssohn , Schu- 
 mann , Dussek , Chopin , Liszt , Sterndale Bennett , & c. vocalist 
 Miss Amy Aylward Miss Dones , Miss Ridgway 
 act accompanist 

 SouTHEND.—A concert Public Hall , oth ult . , 
 Miss Goodman , assist Miss Annie Butterworth , Mr. Stedman , 
 Mr. Thurley Beale , Mr. Osborne Williams . Miss Goodman 
 singing " O mio Fernando " shew possess capa- 
 bilitie , unite excellent voice , , study , enable 
 good position . Miss Annie Butterworth artistic 
 rendering Smart " Lady Lea . " Mr. Stedman Mr. 
 Thurley Beale sing accustomed success , Mr. Osborne 
 Williams play pianoforte solo satisfaction 
 numerous audience 

 WALLINGFoRD.—The organ lately erect St. Leonard Church 
 successfully open 26th Sept. , day Harvest 
 Festival observe . new instrument , build Messrs. 
 Ginns Brothers , Merton , Surrey , pronounce highly 
 satisfactory competent judge , quality 
 effectively display Dr. Sloman Mr. A. Eyre , R.A.M. , 
 preside respectively morning evening service 

 WeysripGe.—The Harvest Festival Service place Thursday 
 evening , 28th Sept. , St. James Church , beautifully 
 decorate corn , flower , fern fruit , & c. evensong com- 
 mence o'clock , following music perform : 
 processional Hymn , " plough field ; " Ely Confession 
 Tallis Responses , proper Psalms chant Sir G. Elvey , Magni- 
 ficat Nunc dimittis Wesley F ; anthem , " o thank " 
 ( Sir J. Goss ) ; hymn sermon , " come , ye thankful people ; " 
 sermon , ' ' sower forth sow ; " recessional , ' ' hark ! 
 hark soul ! ' music sing surpliced choir , num- 
 bere 40 voice , assist 30 lady . Rev. W. Money 
 intone service , Rector ( Rev. E. Rose ) preach 
 eloquent sermon . service Mr. Brooke ( organist 
 choirmaster ) play selection work Handel , Beethoven , 
 Bach . congregation number 1,100 , offertory , 
 sufferer Bulgaria , £ 50 

 Wok1nGHaM.—The Harvest Thanksgiving hold S. Paul 
 Church Sept. 28th . choral service evening choir 
 S. Paul assist Saints ’ . anthem 
 " Lord hath great thing , " H. Smart . church , 
 usual , decorate lady parish 

