second subscription concert , 
 
 WEDNESDAY evening , MARCH 11 , 1868 . 
 programme include 
 GOUNOD " MESSE SOLENNELLE , " 
 new Morert , " King Glorious , " J. Barysy , 
 MENDELSSOHN 114 m psalm , 
 " Israel Egypt come 

 BEETHOVEN PIANOFORTE CONCERTO 

 E FLAT , & e 

 protest , vehemently , 
 custom appropriate anthem 
 selection large work de- 
 sign purpose 

 bad , come 
 knowledge , movement instrumental 
 composition , distorted 
 render vocalization possible , misfit 
 text desecrate unholy 
 alliance . overture Arne , quartet 
 Haydn , trio Beethoven , fact 
 , injustice music , profanation 
 word , distraction hearer thought 
 place worship theatre concert - room , 
 sacrilege temple 

 deprecate , association piece 
 Masses , composition latin word , 
 english text purport sentiment different 
 original . ' case 
 literal translation close imitation Latin 
 sense ; lesson catholicity 
 tenet common Roman 
 Fnglish Church , 
 illuminate master - thought conceive 
 service , aggrandize 
 explain enforce point unity . 
 quote clerical impression 
 powerful annotation text , embody 
 setting Credo Beethoven Mass C ; 
 auditor sensitive quote , 
 deep significance great commentary 
 Christian belief ; , 
 degree , help Beethovén music 
 comprehension text , 
 language employ enunciation . 
 , , note twist 
 round chapter David Psalms Jeremiah 
 lamentation syllable sustain 
 , sound sense 
 antagonism , truth expression annihi- 
 late explosion , abuse art 
 tendency evil proper ap- 
 plication good . thing , fraud 
 musician audience , 
 practise movement Mozart Masses 
 work kind ; piece 
 despoiled expression , chief 
 element merit , work 
 repertory cathedral 

 objectionable — , ah ! objection- 
 able degree — usurpation 
 Church , music design use , 
 appropriation piece Oratorios purpose 
 Anthems . , place , effect 
 piece materially injure omission 
 write orchestra , 
 integral write voice , 
 misrepresent arrangement 
 organ . , seriously injure 
 enormous disparity number 
 singer Church Choir number 
 accustomed hear Oratorio performance , 
 performance small scale 
 bring public ; disparity pro- 
 voke comparison hear 
 hear , character music change ac 

 permanently exemplify valuable 

 linteresting collection refer , instructive 
 jalso , inquire musician , trace change 
 lin style passage writing exhibit . early 
 luse formal sequence run ; grand , sustained , 
 church - like sublimity Sebastian Bach , change 
 inventor , second son Emmanuel , light graceful 
 melodic style mark transition school 
 harpsichord music , : s formality excessive use 
 lof arpeggio , expansion symmetry form 
 develop Haydn Clementi , ennoble rich 
 romantic imagination Mozart , culminate 
 symphonic grandeur Sonatas Beethoven — 
 different stage progress precede 
 great development pianoforte music , fully 
 illustrate - collection choose 
 ispecimen composer school Italy , 
 England , France , Germany , early example 
 | charming piece antique quaintness " 
 | Carman Whistle , " " ' lhe King Hunting Jigg , " 
 jour elizabethan composer , Dr. Byrd Dr. Bull ; 
 late consist italian german speci- 
 men close century . admirable 
 piece son Bach , 
 fantasia fugue John Ernst Bach worthy 
 great Sebastian . Emmanuel Bach ( second son 
 great contrapuntist ) , , 
 commence period transition old formal 

 modern pianoforte music , receive illustration 

 branch musician art recently 
 largely develop England organ playing . 
 mere musical respect — beauty , grandeur , 
 variety tone , instrument advance 
 , scarcely sustain , excellence 
 reach great german builder century a- 
 half , hear organ Silber- 
 mann , instance Catholic Church 
 Dresden , scarcely hesitate admit supremacy 
 combine brightness liquid sweetness , 
 human quality tone soft stop . ' hese 
 high merit scarcely parallel 
 » instrument excellent live french artist , M. 
 Cavaillé - Col , good english builder . 
 superiority organ Germany likewise 
 add early application important ac- 
 cessorie — pedals,—which cen- 
 turie hold indispensable 
 clavier finger performer . construe- 
 tion german pedal , hinge heel 
 performer , admit facility rapidity execution 
 confer advantage hand,—while 
 clumsy imitation adopt later french organ 
 builder — obsolete , know french 
 pedal , short project lever hinge 

 music elevation special application 
 distinguish Bach organ playing characterise 
 work instrument . ' doubtless , , 
 Handel chiefly view public 
 light style , strong constrast solemnity 
 oratorio , organ performance usually 
 associate , commencement 
 present century organ playing England begin 
 assume character long distinguish 
 Germany . late Samuel Wesley Thomas 
 Adams , flourish early century , 
 early pioneer school ; 
 great artist time intersperse admi- 
 rable improvisation episode light ad 
 captandum style english organist pre- 
 vious century . extemporaneous performance 
 artist uncommon thing hear 
 admirable movement alla Capella style , fugue 
 treat masterly clearness skill , alternate , 
 trilling secular prettyness 
 association . late Samuel Wesley belong 
 merit having extensively know 
 - prelude fugue Bach ( Das Wohl- 
 temperirte Clavier ) publication performance . 
 , , later knowledge great 
 organ work Bach special use pedal in- 
 dependently hand , obtain ; 
 early artist introduce important feature 
 good school organ playing present Dr. 
 Samuel Sebastian Wesley , skilful performance 
 grand organ work Bach , pedal 
 write line independent clavier 
 hand — 
 admirable improvisation instrument , 
 great impetus organ playing England ; impetus 
 largely help produce skilful per- 
 numerous mention . 
 , Mr. Best , organist St. George Hall , 
 Liverpool , long eminent power a_per- 
 command finger - board pedal alike 
 unbounded . ' gentlemen time past 
 contribute valuable addition thelibrary organ 
 music series arrangement , late number 
 refer . excellent transcrip- 
 tion draw partly sacred partly 
 secular source ; include orchestral piece , 
 Mr. Best contrive 
 original effect skilful carefully indicate combi- 
 nation change stop in- 
 strument . piece arrange , organ music 
 , line , pedal line obbiigato , 
 independent hand . mean 
 true effect organ playing , comprehensive 
 adaptation score , obtain . sacred piece 
 contain number refer magnificent 
 chorus Bach church cantata , Mendelssohn 
 overture Athalie , motett , ' ' hear prayer ; " 
 secular piece Handel overture Porus , 
 Allegretto Beethoven seventh Symphony ; por- 
 tion Mozart Divertimento wind instrument ; 
 Gavotte Rondo Bach , Spohr overture 

 point performer toot — allow 
 occasional holding note , scarcely admit pas- 
 sage playing . pedal adopt 
 country , supersede german pedal 
 half century . little remark- 
 able Handel , cotemporary Bach , belong 
 period locality , grand 
 style organ playing reach high development , 
 protest submit inferiority 
 english organ , size compass , especially 
 absence pedal , use 

 lodge 989 1612 , evening 

 15th ult . , new Orange Hall , York - street . 
 evening appropriate song chorus 
 sing Miss Murphy , Miss Mahon , Brothers 
 Craig Dyas ; proceeding conclude 
 National Anthem.——Tuer Second Series Monthly 
 Popular Concerts inaugurate Ancient Concert 
 Room 13th ult . , fair auspex . Mr. 
 Charles Hallé Pianoforte Recital afternoon 
 listen crowded audience utmost atten- 
 tion ; piece — especially Beethoven ' Pastoral 
 Sonata , " eighth book Mendelssohn « Lieder 
 ohne Worte " — appear thoroughly appreciate . 
 afternoon concert , Beethoven string Quartett 
 F ( Op . 18 ) , excellently play messr , R. M. Levey , 
 Liddell . Halton , Elsner ; Mr. Charles Hallé 
 pianoforte performance attractive feature 

 concert . Mr , Arthur Barraclough 
 appearance vocalist occasion ; and.in Gounod’s| 
 song , " valley , " display good baritone voice , 
 considerable compass , legitimate style vocalisation | 
 augur future . join | 
 Miss Minnie Hodges Mozart duet « Crudel perche , " | 
 utmost success . credit Messrs. | 
 Elsner Gunn spirit en- | 
 tere speculation object the| 
 elevation musical taste Dublin ; cordially 
 wish success deserve 

 Institution Blind , Nottingham , Mr. F. M. Ward , 
 teacher music Institution , preside piano . 
 hall nearly fill highly respectable 
 audience , evidently thoroughly appreciate 
 talent display performer . ' pro- 
 gramme excellent , consist selection 
 favourite glees , song , & e. " death Nelson , " 
 Mr. W. Lock , receive - merit enthusiastic 
 | encore . Miss Armitage , lady possess excellent 
 jtreble voice , especially happy rendering 
 " Beautiful , " Miss Jeffries applaud 
 " sing old song ’ Messrs. Gibsun ( bass ) 
 Lock ( tenor ) sing duet " o Albion ! " remark- 
 ably good effect . glee strengthen Messrs , 
 | Bennett Shepherd , Miss Cope . understand 
 company pay visit G : imsby 

 mendatory sentence conclusion orchestra , } summer , intend devote proceed 
 numerous efficient — Jarghetto |of performance benefit old schoolfellow , 
 Beethoven ' Second Symphony D , ' " ' introduce | Mr. J. Radman , town , educate 
 miscellaneous selection morning , / institution 

 piquant , somewhat trite , overture Masaniello | Hampron.—The Choral Society Con- 
 Zampa evening entertainment , highly pleas- | enc tb 

 Supsury.—A Concert , member 
 Sudbury Amateur Musical Society , 
 Town Hall , evening 2nd ult . 
 exception , programme compose entirely vocal 
 piece , , considering wholly 
 sing amateur , creditably perform . 
 choir contain excellent female voice ; . 
 weak tenor . instrumental piece wag 
 Mendelssohn " Andante Rondo Capriccioso , " 
 execute pianoforte Mr. Orlando Steed , 
 officiate conductor ; society 
 mainly indebted state vocal proficiency exhi- 
 bite occasion . Mrs , Lishman preside 
 pianoforte 

 Torney , CamBripGEsnire.—Mr . Thacker 
 Concert.—The weather , unpropitious , 
 little effect annual entertainment , 
 look forward large interest , 
 receive patronage year . 
 merly . programme highly interesting ; andthe 
 professional talent engage , Mr. Sturge 
 appear , satisfy Mr. Thacker numerous 
 patron . Miss Clara Wight principal soprano , 
 sing delicacy taste evening , 
 receive encore . Mr. Otto Booth play 
 violin solo great brilliancy , encore 
 celebrate " Carnival Venice . " join Mr , 
 Thacker Beethoven Sonata , know " Kreutzer , " 
 violin piano , play great taste 
 precision . piece sing 
 member Choral Soviety . Mr. J. Baker , 
 Whittlesey , assist Mr. Thacker pianoforte , , 
 daughter , play Overture ' " Bohemian Girl " 
 effectively 

 Wiean.—The Messiah Wigan 
 Choral Union , 27th December , Public Hall , 
 instrumentalist select Mr. Charles . 
 Hallé band , conductor M. Gustave Steig- 
 meier , professional vocalist engage Mis 
 Hall , Miss Marie Gondi , Mr. W. H. Cummings , Mr. 
 Orlando Chrisiian . Miss Hall " rejoice greatly , " 
 excellent effect ; Miss. Gondi sing music 
 allot pleasing style . Mr. Cummings ’ 
 singing admire ; Mr. Christian , po 
 sesse fine voice , thoroughly efficient bass 
 . chorus generally sing 
 member Union 

