TecunicaL Stupres , ’ MAcFARREN ‘ CHILD ' Fir : 1 Mesic 
 Lessons , ’ ‘ Seppinc Tiny Tunes Metuop , ’ Etc . 
 PuBLIsHED . 
 . 95.—-NOVELLO MUSIC PRIMERS 

 BEETHOVEN 
 PIANOFORTE SONATAS 

 HINTS RENDERING , FORM , Er 

 reason fostering growth smaller 
 practicable forms . need discuss 
 songs music keyed instruments ; 
 safe case . need waste time 
 talking string music great German 
 composers ; secure generations 
 tract lesser - known 
 concerted chamber music British origin , 
 ignorance likely baulk native 
 developments . beginning 
 realise folly past efforts develop 
 great native choral art works 
 Bach Handel Elizabethan 
 madrigalists Purcell , shall shortly 
 aware English string music 17th 

 idiomatic , suggestive development 
 modern British composers   master- 
 works Bach , Beethoven , Brahms 

 vocal concerted instrumental 
 music Elizabethans belongs , course , 
 category chamber music . Madrigals 
 ‘ apt voyces vials , ’ string Fantazias 
 Byrd practically Madrigals Words . 
 fact string quartets play 
 pieces seldom ineflective 
 ( performance sweetness 
 subtlety unlike realm 
 chamber music ) , 
 difficult ; ridiculously simple 
 degree technique needed play notes , 

 space illustrate Rolland 
 grasp general character motive ; 
 firmly treads 

 peculiar domain musical novelist — artist 
 psychology . infallibly , instance , 
 superficial writer Christopher ‘ 
 infant prodigy , young Mozart Schubert 
 redivivus ! Rolland knew better . boy 
 exploited , , wastrel 
 ff father . nonsense 
 youthful works genius . “ moment 
 illusion , wrote , saw 
 written worthless . ’ , surely , 
 sounder psychology . Christopher mind , _ like 
 Beethoven , order ripens slowly 

 composer work 
 glow conscious power : arrogant , intolerant , 
 staking sheer intuition , scornful construc- 
 tion intellect 

 - day , music fills 
 wider place general culture , English 
 novelist realise fascination 
 study musical character , wide range 
 human life covered 
 terms ‘ musical background ’ ‘ musical 
 atmosphere . ’ models outside 
 ‘ Jean - Christophe , ’ failures pre- 
 decessors instruct . particular , 
 moral aspect music needs warytreatment . ‘ 
 incredible things reader asked believe 
 superficial writers — eflects song , 
 instance ! patent attempts cover 
 weak psychology , pretexts sermonising 
 sentimentality ! ‘ best seller ’ dozen 
 years ago ( song ID flat ) remains 
 sufficiently terrible example 

 Iangerous , , historical variety , kind 
 exemplified Marion Crawford ‘ Stradella . ’ 
 Faint conventional psychology , work 
 deft construction apt musical detail . 
 books hero tends disappear 
 setting ; Stradella life , told tradition , 
 thrills interest clever p . 
 weaver , altogether apart music . « 
 imagine Beethoven smothered tri,'al 
 historical detail , things matter 
 mind art 

 profitable field musical novel 
 surely obvious moder mu ical 
 society , broadly understood . extreme 
 inconsequent lightness , writer wit aod 
 knowledge wonders 
 theme E. F. Benson * Queen Lucia . ’ 
 course bring music definitely 
 atmosphere story ; mightily 
 entertain pricking bubbles pseudo - musical 

 Chorale Preludes restrained 

 emotion , frankly lighthearted — 
 skittish . Beethoven 
 “ unbuttoned ’ Bach organ 
 Lutheran Cathechism 

 works . 
 

 Courrier Musical ( June ) Vuillemin protests | 
 ° . : f ° ° | 
 adjunction dancing music 
 masters written purpose 

 Rach , Becthoven , Wagner failed foresee ’ 
 Duncanism . dance 
 music Suite D , Funeral 
 March * Eroica , ’ Isolde Death , 
 connection music — case 
 odious — truly expresses ( point 
 doubts ) Bach faith , Beethoven pathos , 
 Wagner lyricism — case indecently exhibits , 
 glare stage , composers genius 
 elected entrust voice music unassisted 

 COLLABORATOR LISZT 

 notice previous concert , 
 writer expressed hope 

 year shall hear Beethoven 

 Wagner , Mendelssohn , Schonberg , Bartok 

 LITTLE KNOWN WRITER MUSIC 

 issue La Critica Musicale contains 
 picturesque effusions mysticism music , 
 Beethoven , Wagner , Debussy , Giuseppe 
 Vannicola ( 1877 - 1915 ) , violinist writer , 
 capacity Balestrien model 

 appalling ‘ Beethoven 

 BACH KNOWN 

 fond Wagner work . let 
 sort passionate fluid found music . 
 came day friend took Bayreuth , 
 devilishly bored ? Valkyries ’ 
 battle cries right bit , hours 
 straight send mad . _ shall 
 remember scandal caused , 
 nerves edge , struck match find way 
 theatre 

 prefer decidedly Italian music ; school- 
 masterly German . Beethoven 
 ‘ professor ’ aspect makes 
 squirm . , comes little tune 
 Couperin Grétry , matter old 
 French music 

 eta ta3 

 Si1r,—As regular interested reader excellent 
 journal , permitted remarks 
 reference ‘ Feste ’ article July issue vexed 
 question ‘ greatest composer 

 careful survey subject appear 
 Beethoven occupies peculiar unique position 
 art . music personality 2 
 | universal appeal , emotionally intellectually 

 nme meets wo 8 oO Ce 

 1921 

 tothe heart mind humanity 
 composer . ground music- 
 lovers professional musicians acclaim 
 Beethoven greatest composers , June 
 number Za Revue Musicale M. André Suares appears 
 support contention . curious note ( far 
 | aware ) Beethoven composer 
 linked greatest names sister arts 
 poetry painting ( Homer , Dante , Shakespeare , Michael 
 Angelo , Xc 

 conclusion , beg leave repeat quotation 
 Saint - Saens regard matter : ‘ Et parceque 
 Beethoven chanté la fraternité universelle , parceque 
 son ame au lieu d’étre seulement l’Ame allemande est 
 ame humaine , il reste le plus grand , le seul vraiment 

 grand . ’ 
 music - lovers , , favourites . — 
 , Xc . , K. HEATLEY 

 5 

 ROYAL ACADEMY MUSIC 
 students ’ orchestral concert given ( ) Jueen Hall 
 Tuesday afternoon , June 21 . interesting 
 items movement Glazounov Pianoforte | 
 Concerto F minor ( Mr. Reginald Paul ) , Sir Alexander | 
 Mackenzie Pibroch violin ( Miss Gladys Chester ) , | 
 Witch Song Parry ‘ Saul , ’ sung Miss Jenny | 
 Roberts , Vaughan Williams ’ Overture ‘ Wasps , ’ | 
 orchestra gave excellent interpretation . } 
 concert concluded admirable rendering /2xa / e | 
 Mendelssohn ‘ Loreley ’ ( solo , Miss E. Mellor ) , | 
 choir Mr. Beauchamp baton gave evidence | 
 excellent training . 
 chamber concert given Duke Hall } 
 Wednesday , July 6 , included compositions | 
 students , works considerable | 
 promise . student works ranged songs by| 
 Kathleen Palmer , Russell Chester , Dorothy Hogben , | 
 Pianoforte Sonatas Alan Bush Desirée MacEwan , | 
 included interesting suggestive musical 
 accompaniment poem Gurnett , ‘ Echoes tide . ’ 
 music composed Inie Bell , gave 
 excellent recitation poem , accompaniments 
 played Miss Sadie Clayton . programme 
 included movements Pianoforte Quartet 
 Chausson , Franck Sonata violin pianoforte , 
 Variations Beethoven Sonata ( Op . 109 ) , played 
 Miss Joan Lloyd , songs Stanford admirably 
 sung Mr. Howard Fry . somewhat long programme 
 concluded arrangement Bach Toccata F 
 pianofortes , Vivian Langrish , played 
 Miss Kathleen Wood Miss K. Ranee Corlett . 
 annual prize distribution took place Queen Hall 
 Friday afternoon , July 22 

 ROYAL COLLEGE MUSIC 

 extent resembled summing judge 

 jury ; , , leave hearer 
 little doubtful real attitude speaker 
 ‘ developments ’ question . Ouseley 
 Macfarren mentioned compassion ; 
 unlucky dictum ‘ making 
 variations original bass harmony zezer 
 altered ’ — , unwise advice 
 student apt imagine 
 begin Beethoven left ; advice , funnily , 
 inconsistent Ouseley previous suggestion ‘ 
 melody bass ’ ! _ extraordinary 
 statement Ouseley ( , 

 symphonies Haydn Mozart , earlier ones 

 Beethoven , contain movements cast mould — z.e . , 
 modern binary form ) escaped censure . reference 
 Macfarren , Dr. Buck understood 
 denied employment mediant triad music 
 termed ancient strict diatonic style . 
 statement found ? ‘ 

 Lectures Harmony ’ ( p. 46 ) says , ‘ major key , 
 common chord mediant scarcely employed 
 | involving juxtaposition extreme notes 

 tritone produces harsh effect ; , 
 chord found ancient music , good 
 taste scarcely accept concord . ’ 
 means endorsing views eminent men , 
 surely admit good service 
 furthering claims music universities 

 , course , true critics , 
 past , condemned advance music . Nowadays 
 afraid condemn . matter ? 
 cares know Beethoven sonatas Mr. X. 
 thinks worth playing ? critics , Lord Beaconsfield 
 said , men failed . 
 necessarily follow new thing past 
 condemned , new thing present 
 good . Dr. Buck pleaded , pleaded eloquently , 
 sympathy ; lack virtue 
 Surely artists long novelty 
 art , provided good . impossible forget 
 reform thing , revolution . 
 connection , Dr. Buck drew striking distinction 
 rules laws . said rule deduced 
 general practice , exceptions ; 
 law wasimmutable . ( ‘ principle ’ 
 better term , laws — events human laws — 
 liable change . ) example , consecutive fifths 
 contrary rule 

 recollect Dr. Buck mentioned example 
 musical law , principle ; venture 
 principle musical composition founded 
 common chord . law observed 
 composer Hucbald Brahms . Dr. Buck 
 allusion ‘ - tone scale , ’ said good 
 deal ‘ harmonic chord , ’ surely far better 
 described harmonic series ; chord 
 sense musician employs term , 
 second partial - cent . 
 loudness primary . Dr. Buck mentioned organ 
 tested , , commencing lowest 
 G pedals , possible sustain 
 harmonic series thirty - second ; sounds 
 heard individually , simply combined 
 huge G ; chord F sharp sounded 
 satisfactorily played combination , course 
 impossible express opinion instrument 
 heard . said 
 objection introduction sounds 
 composition , provided sounds question 
 inaudible . famous ‘ false entry ’ second horn 
 ‘ Heroic ’ Symphony , banged pianoforte 
 sounds horrible . played Sir Henry Wood plays , 
 delicious , past time notice . 
 great objection 20th century compositions 
 , sounds indicated notes ave audible 

 discussion followed Dr. Buck address , 
 Dr. Rootham expressed opinion 
 eve great changes , expect wonderful 
 artistic revelation probable consequence . 
 passed age score heard 
 prophecies sanguine fulfilment . 
 eve great changes , - day 
 nations stand edge precipice ; 
 probable great cays creative art . 
 evidence look pictures 
 disgrace walls Burlington House season . 
 thing Bolshevism art , 
 religion politics 

 memory misleads , Dr. Buck concluded 
 address advocating performance Beethoven 
 symphony /er recte et retro — involving , course , couple 
 orchestras . meant movement 
 treated fashion , symphony — 
 case orchestras meeting , suppose , 
 near end slow movement — 
 quiteclear . clear , suggesting , 
 quietly poking fun tendency good 
 deal - - date orchestral music 

 end meeting Mr. W. L. Luttman proposed 
 hearty vote thanks president having initiated , 
 carried action , idea calling conference 
 members Union Oxford . said 
 enjoyable time , thanks president 
 kindness authorities New College , 
 earnestly hoped Union able hold 
 meetings future . proposition received 
 great applause heartily agreed present . 
 president reply said glad 
 conference proved successful , hoped 
 forerunner interesting 
 gatherings 

 MUSICAL TIMES — AvcusT 1 192 ! 387 

 organ admirable . tenor , calls BERLIN 
 singer possessed ordinary musicianship , Germany totters brink bankruptcy , 
 doubt better performer found | politicians cast help succour 
 M. Louis van Tulder . work , | Bolshevism threatens Eastern frontiers , art 
 given times succession , roused greatest | music flourishing yore . True conservatoriums 
 interest country , pains bestowed | music lacking funds . salaries professors 
 production rate vain . _ | wholly inadequate , furthur rise fees sure 
 June 15 Prof. Georg Schnéevoigt resumed | create dismay poorer students . question 
 activities conductor Hague Orchestra | issue artistic ethical character , 
 Scheveningen Kurhaus , summer season . | State wel ! municipalities 
 According time - honoured custom concert | close attention preservation great 
 opened National Anthem , ‘ Wilhelmus van | musical institutions . 
 Nassouwen , ’ programme proper consisting ] [ t foist public operas like Egon 
 ‘ Meistersinger ’ Prelude , Sibelius ’ ‘ Elegia , ’ Liszt Préludes , | Wellesz ‘ Princess Girnara , ’ recently produced Frankfort , 
 Tchaikovsky ‘ Romeo Juliet , ’ Weber ‘ Invitation | composer purposely renounced melody 
 4 la Valse ’ Weingartner orchestration . Schnéevoigt |thematic treatment ( z7de story fox 
 met hearty reception audience , | grapes ) , result - called harmony consists 
 achieved big , comparatively easily attained | series grunting , scraping , squeaking noises . Luckily 
 success . subsequent concerts | press public declined tiresome unmusical 
 pleasure renewing acquaintance old favourite | work , showing spirit classics 
 times members Berlin Philharmonic leading overwhelming majority . 
 Orchestra regular guests Scheveningen Kurhaus , | estranged Mendelssohn Schumann 
 , splendid leader , Anton Witek , | symphonies . grateful Otto Klemperer , 
 finished playing Brahms ’ Violin Concerto revealed | Cologne , playing Schumann - blooded * Rhenish ’ 
 fine musician old . June 29 | Symphony , heard 1897 . 
 startled sensation , inasmuch youthful Klemperer gave Bruckner C minor Symphony . 
 conductor , enjoys Polly Fistulari , | possesses composer romanticism , lacks 
 appeared head orchestra . musical fighting strength . 3 , Scherzo 
 prodigies , believe , guard | weighty impetuosity . earlier Bruckner , 
 conducting prodigy , sincerely hoped | characteristic language , composer 
 class going institution , | gather strength . symphonies Max 
 function orchestral director claims | Strub played Brahms ’ Violin Concerto , gave 
 place marked personality , endowed ripe strongly heroic reading . time German 
 musicianship vast experience . | Tonkiinstlerfeste , new symphonies chamber 
 reason regard mere boy acting such| music . fusion Philharmonic Society 
 capacity curiosity . theatre orchestra old Nuremberg , town Hans Sachs 
 appearance M. Gerard Hekking concerts | Meistersingers , saw time Allgemeine 
 gave audience highly appreciated occasion listening | Deutsche Musikverein walls , walls 
 foremost violoncello players . July ©   everystone art - work preaches adherence ‘ Meister , ’ 
 Prof. Schnéevoigt , having gratified listeners ) step perceives - guarded 
 splendid performance Rimsky - Korsakov enticing | generally unspoiled expression past - knit 
 symphonic poem , ‘ Sadko , ’ handed baton ! society art life welded unity . 
 remainder evening American conductor-| Musically , Nuremberg compete great 
 composer , Mr. Samuel Gardner , interpreted | industrial cities Western Germany . Nuremberger , 
 Symphonic Poem , proving | historical centre beautiful town 
 versatile conductor able , littletoo built large industrial suburbs , knows work . 
 academic , composer . time writing climax | reason concerts A.D.M. 
 concerts reached altogether praise-| amidst proper surroundings . intended , 
 worthy performance Choral Symphony July 8 . | place , men profession , differ 
 Prof. Schnéevoigt able raise | musical festivals inasmuch 
 chorus , consisting mixed assembly singers , | expression eminent artistic work , meant 
 artistic level orchestra , performance | cultivate musical life spirit Liszt , 
 successful , solo parts sustained | /.¢. , sense gradual development . opposition 
 Mesdames Di Moorlag Dresden - Dhont Messrs. | maintains Society sufficiently carry 
 L. van Tulder Karel Butter van Hulst . July 2 intention founder . true listeners 
 announcement concert , programme | Jed new lands , radicalsamong composers 
 devoted exclusively works Bach , especially | stand aside , comparatively unknown men 
 - ordered selection master Church cantatas , chance . chief work festival new opera 
 called usto Great Church Nairden . M.Schoonderbeck , | Max Wolff , Frankfort o / M. , mysterious title , 
 lately recovered illness , spared painsto | ‘ Frau Berthe Vespergang . ’ 
 present large congregation real treat . remarkable prominent composers 
 soloists occasion Mesdames Anna Stronck- | Germany cultivating sonata form , widening , 
 Kappel Maria Philippi Messrs. Jos . Holthaus | filling new thought . Hans Pfitzner Sonata 
 Thom Denys , orchestral accompaniment furnished | violin pianoforte ( Peters edition ) , recently played 
 Amsterdam Orchestra . Special mention | Cologne Therese Sarata Alfred Kiirmann , 
 M. Speets ’ fine trumpet playing . romantic work great beauty , activity innate 
 M. Orazio Daniele , Italian tenor ( agent force . observe clearly strange duality Pfitzner 
 claims successor Caruso ! ) gave recitals art : open portals melodic gift , inviting 
 Concertgebouw . voice execution , | listener enter , Gothic fretwork working - 
 , gave evidence rival his| sections . final movement breath 
 famous colleague . scored means| spring misunderstood . _ different 
 ordinary success , credit if| world thought Paul Biittner leads Sonata 
 agent pitched puff preliminary high aj C minor , violin pianoforte , 
 note . W. HARMANS . | powerful movement , Beethovenian breadth 
 — — — — — — — — — — — — — ~ = =| beauty melody Adagio , joyous final 
 Miss Katherine Ruth Heyman lectured * Rhythm ’ | OvEMEM . | Egon Kornauth , Sonatas oe 
 American Women Club , Park Lane , July | ! | > — gnmeer aes ae Soe rs 
 th ’ — + * + -*“| Francis Aranyi , recognise perfection art . 
 Miss Heyman studied nature rhythm | works technically immense difficulty . violin 
 searchingly , lecture learned enlightening . | , written highest position , demands 

 588 MUSICAL TIMES — Avcusr 1 1921 

 cut ; concierges workers , | es 
 devote Sunday afternoon operatic relaxation , resent | ROME 
 mystification . Set - thinking , consider | rHE FLOWERS CRITICISM 

 management withholding money title ‘ feast Rhythm , ’ recent number 
 command . | 7emfo following paragraph . notice comes 
 concert - opera season , , like Opéra , is/| Naples : . 
 assisted State subsidy , entirely devoted opera . ‘ young creature played violin yesterday 
 important orchestral works performed , hall Ilusi personification rhythm , 
 festivals place . Recently , example , manifestation grace animated music . 
 Beethoven Wagner programmes , varied ! plays express youth , blossoming 
 examples Boieldieu , tinkling , Bellini - like strains } jnnocence , eyes closed , borne away 
 curious fascination French , Paris and| river , makes think nymph ‘ sung 
 en province . Oddly , Meyerbeer ‘ Marche des| Virgil , knew came Syracuse . 
 Flambeaux , ’ trivial meaningless thing , popular bore soul sounds 
 concert - opera patrons . given recent ¥ schylus ’ tragedy , eyes marvel great 
 Sunday concert intense satisfaction audience } crowd sea . saw , said 
 numbered thousand persons . immediately , ‘ ‘ Arethusa . ” years 
 seliom Paris displays extradrdinary lack of| old , unknowingly lives musical life 
 judgment . Happily , frequent Tuileries world , know brings message 
 Gardens applaud good . Mendelssohn’s| yast harmony , voice 
 ‘ Midsummer Night Dream ’ music , example , always| mysterious . plays eyes closed , listening 
 received , recent Georges Hiie festival voice tide bears away , confused 
 composer ‘ Les Pantins ’ Overture symphonic poem , waves , transformed wave . 
 “ Les Emotions , ’ vigorously applauded . the| transformed rhythm . 
 appreciated programmes partly new , pure apparition grace ; recalls 
 devoted ‘ chansons que tout le monde connait , ’ quote | memory sister stolen away 

 announcement . ‘ Credo du Paysan , ’ ‘ L’Angelus| whilst gathered asphodel flowers , 
 de la Mer , ’ ‘ Le Vin de Marsula , ’ popular ditties| flees complains , whilst companion 
 wildly acclaimed . caresses check amber hand . called 

 North Wales . combined rehearsal | 
 possible , baton Prof. Walford Davies | - Songs ) os - 4d . 
 singing ‘ St. Paul ’ showed little weakness . tone | OULDERY , CLAUDIUS H. — * Valse nena 
 pleasant , definite expression | C ( ‘ Suite G ) . Pianoforte Solo . 2s . 
 choruses . morning afternoon concerts ~RIMP , HERBERT E. — ‘ ‘ Wilderness Way . ” 
 congregational tunes sung choir , part- Songs Childhood . 3s . 
 songs given separate choirs . _ professional ~LLIS , MRS . EDWARD.—‘‘The Bells . ” Vesper 
 orchestra took mectings . 7 Heme d. 7 , ‘ 
 Norwich.—Rossini ‘ Stabat Mater ’ recently per-| 7—~ pRMAN EDWARD . — “ ‘ Orpheus lute . ” 
 formed Agricultural Hall Handel Society J Arranged Voices Composer . ( . 
 Mr. Ernest Harcourt . choral singing vigour , | 1304 , Novello - song Book . ) od 

 expressiveness , sureness associated " 
 s . n Nunc 
 work Society . July 8 choir took | ~ IBBONS , ORI ANDO — Magnificat _ 
 music pageant , ‘ Alceste , ’ held grounds Dimittis . Set Gregorian Tones verses 
 Palace pag ; , 8 | faux - bourdon . Edited Roy_e Snore . ( . 1036 , 
 Novello Parish Choir Book . ) 3d . 
 CONTENTS . Page . | (; IBBONS , ORLANDO , axnp JOHN HOLMES . — 
 Early English Chamber Music . Rutland Boughton ... ons : OPT J Magnificat Nunc Dimittis . Set Gregorian 
 ‘ Jean - Christophe ’ Musical Novel . W. Wright | Tones verses faux - bourdon . Edited Roy.ie 
 Roberts « ss » 538 } SHORE . ( . 1038 , Novello Parish Choir Book . ) 3d . 
 Dramatic Works Vincent Indy . * L'Etranger ' ( contd ) . { 3 : oe ) 3 
 M.-D. Calvocoressi . vee se ees ane $ 42 | * OSS , J.—‘‘I sing unto Lord . Anthem 
 Organ “ 1 = Bach . Harvey Grace ( concluded ) : .. |X _ y Festival General use . ‘ * Glory 
 iditum y CSte ... eee eee eee ese oO } ” 7 Th » y TT . - _ 
 New Light Early Tudor Composers XIX . Richard Davy Lord . ( . 942 , Zhe Musical Times . ) wa . 
 W. H. Grattan Flood — - Oe OYLAND , C. E.—Communion Service moda 
 Manuscript Libretto * Faust . ’ Camille Saint- Saéns 553 style Unison Singing Price , 6d . : People 
 Music Foreign Press . M.-D.Calvocoressi ... ... 558| & & Style . Fo , P 
 English Folk - Songs Dances . Hubert Fitchew   ... + + 560 } , 3d . 
 London Concerts . Alfred Kalisch . _ - » 566 | OWE Cc EGERTON . — Beethoven Pianoforte 
 Patron Fund . aie ae ws 967 | s 7 gong . . " 
 Russian Ballet ( ith portrait ) . ‘ 6 ; ve 568 | Sonatas . Hints Rendering , Form , Xc . ; 
 Co - operative Opera : New heat riment . oe , . 568 } Appendices Definition Sonata , Music Forms , 
 om aaah - - ee ee = | Ornaments , & c. ( . 95 , Novello Music Primers . ) 5s . 
 Renoir Portrait W agner . 570 N ATTA , ARTHUR L.—Four Hymn _ Tunes 
 Gramophone Notes . ‘ Discus < = ae & 3 ? . 
 Chamber Music Amateurs ‘ : woe 578 | ( n ee ) . 
 Church Organ Music ... - se ho JYATTERSON , ANNIE W.—‘‘A Lay Spring . 
 Royal College Organists ’ Pass List | . ... | . - 572 ] Song . . 6d . 
 Church Music Free Seats . - aii - Sei non . . . 
 Organs Organists St. Olave , Tooley Street , Southwark | RESTON , THOMAS . — Short Picces . 
 Andrew Freeman ( illustration ) - 873 ) Pianoforte Solo . 2s . 
 New Organ Public Hall , Blackburn ~ < « “ ec ” . . , 
 Letters Editor om ; ; ” § 5>| — — “ ‘ Serenade . ” Song . French Words . 2s . 
 Royal Aandems Music ... , cae * Che ” ¥ > eS 
 Royal College Music sas | — — “ * Chanda . ” Eastern Love Song . . 6d . 
 Sharps Flats ... ~ ss se ae 581 | ( * CHOOL MUSIC REVIEW ( . 350 ) , contains 
 ixty Years Ago eco ove . 581 “ Wigwam 
 Hand Development Performer ... cae | ioe fog Son .~ gg 3 igw ~s 
 Glastonbury Festival .. ae om wn nm & - O- mose seg 
 Union Graduates Music ’ Arthur T. Froggatt | . 582 | Invitation . ” Unison Song arranged R. T. WHITE . 2d 

 Celtic Congress Isle Man . Rosaleen Graves ... § 83 ey SONGS , Published forms . 4 . Voice 

 PP SY OYEY Ds 

 . 5 . 
 de Noces . ee * - Stuart Archer 
 estival Prelude « “ Ein ’ feste Burg ' wa " W. Faulkes 
 Legend .. oe - Harvey Grace 
 Allegretto Pastorale | . os ee oe . M. Higgs 
 Benediction “ pad ee Alfred Hollins 
 Sursum Corda .. ee oe ohn N. foe 
 Alla Marcia ... ohn N. Ireland 
 Adagio Cantabile ° win H. Lemare 
 Fanfare .. . os os os ee f. Lemmens 
 Intermezzo os os oe se es -- B. Luard - Selby 
 Easter Morn ... es ohn E. W 
 Finale B flat .. se ee W. Wolstenholme 
 . 6 . 
 Nocturne Thomas F. Dunhill 
 Postludium .. William — 
 Andante Tranquillo . . H. M 
 Springtime ee ee os Alfred Hae 
 Madriga , , ae . Edwin H. Lemare 
 Triumphal March J. Lemmens 
 Allegro B flat F. Mendelssohn- Bartholdy 
 Choral Prelude “ Rockingham " C. Hubert H. Parry 
 Preludium Pastorale ° ee ‘ J. Stainer 
 omance F minor ee ee oe ° Tschaikowsky 
 Romance flat H. Sandiford Turner 
 Festal Commemoration John E. West 
 . 7 ( F ‘ uneral Music ) . | 
 Funeral March ( Pia macterte Sonata , Op . 26 ) Beethoven 
 Blest mourn ( Requiem ) - - rs ahms 
 Funeral March es Sonata , vo 35 ) o Chopin 
 Funeral March .. William aulkes 
 Funeral March ( “ Saul ’ Dia Handel 
 Handel 

 know Redeemer liveth ( “ Messiah ) 
 Funeral March ( “ ‘ Lieder ohne Worte " ) ° 
 O rest Lord ( “ Elijah 

 T'schaikowsky 
 diford Turner 
 John E. West 

 Beethoven 
 Brahms 

 Chopin 
 liam Faulkes 
 Handel 
 Handel 
 Mendelssohn 
 Mendelssohn 
 Schubert 
 schaikowsky 
 schaikowsky 
 ohn E. West 

