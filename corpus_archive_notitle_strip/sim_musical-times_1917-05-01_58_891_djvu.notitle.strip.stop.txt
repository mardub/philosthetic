Classics word correct refined Witnes 
 ideal v 

 ENGLISH reproduction Beethoven Sonatas , |   disapp 
 Mozart Sonatas , Chopin Works , etc . , etc . urged t 

 Manch 

 YLIM 

 contention concert ‘ 
 artistic event ’ [ ! ] ‘ 
 item news , good journalism requires 
 shall dealt accordingly . ’ reply 
 cases ‘ news ’ 
 slightest interest , worth 
 paper print . , imagine , 
 average reader Dai / y Monitor 
 interested announcement Miss Jones 
 played Beethoven Sonata little hall 
 London . country Miss Joneses , 
 able play Beethoven Sonata , 
 feel stirring pulse news 
 vast brood amusing 
 fashion scores hundreds miles away . | 
 dream reading 
 paragraph nature London paper ; glance 
 dealt 
 ordinary way ordinary occurrence , 
 concern 
 adjacent reports inquest chimney - sweep 
 Bermondsey company meeting Holborn . 
 Mr. Scott reply read paragraphs 
 sort shall know going . 
 ! want know going , 
 goings - insignificance . 
 urged merely ordinarily good 
 performer performance worth writing 
 reading . work stands 
 level ordinarily capable actor 
 elocutionist photographer , skilled 
 reproducer . paper dream reporting 
 doings ‘ hese people day day ; life 
 report doings 
 sorts musicians merely average 
 practitioners line . concert 
 undoubtedly ‘ news , ’ Mr. Scott says ; think 
 find , inquiry , species news 
 average reader takes little interest . 
 surprised , imagine , discover 
 people read news kind daily press . 
 case little stronger ‘ news ’ 
 performances bigger people , | 
 doubt people Aberdeen , , 
 read right paragraph London daily 
 told unlistening world programme played 
 Paderewski yesterday recital . 
 recital , interesting 
 catalogue public buildings Paderewski 
 passed way Trafalgar Square 
 Westminster Abbey 

 thing , surely , 
 interesting treatment critic . 
 fresh works 
 Paderewski played Paderewski playing 
 paragraph worth reading , 
 . critic able supply 
 readable matter zs critic , reporter 
 supplying ‘ news . Mr. Scott pours scorn idea 
 critic central figure article . 
 reply , article 
 worth reading . mean , course , 
 critic set bigger person 
 Paderewski Chopin . mean 
 justification writing familiar 
 writes artistic value 
 . convey Chopin 
 great composer Paderewski great pianist , 
 save trouble . knew 
 long ago , need reminding ina 
 merely platitudinous way . critic 
 readable , justify writing 
 performance , quicken 
 nerve reader appreciation — , 
 cases , depreciation — work 

 melodies ( ‘ L’Enfant Prodigue , ’ 
 ‘ Mandoline , ’ ‘ Romance , ’ ‘ Le temps laissé son manteau , ” 
 ‘ Noél des enfants qui n’ont plus de maison ’ ) , 
 named obtained biggest success . encored 
 twice , good reason . Christmastide . Homeless 
 little children , enemy having sacked occupied 
 house , sing misery pathetically : ‘ Nous n’avons 
 plus de maison , les ennemis ont tout pris , tout pris , tout 
 pris ! ’ actually simply Debussy paints 
 homeless children trials . marvellous power 
 adaptation ages circumstances . 
 home Pagan world expression 
 medieval , especially , modern . Prelude 
 ‘ l'aprés - midi d’un faune , ’ ‘ Martyre de St. Sebastien , ’ 
 ‘ Pelléas , ’ aspects manifold 
 personality . hand , mastery classic 
 art prevents falling extravagance , 
 ultra - modern Russian confréres 

 string Quartet , sealed Palais de Glace 
 Festival , monumental work deserving comparison 
 Beethoven Quartets conception , severity style , 
 construction . Debussy written work 
 claim leading exponents 
 contemporary French music , title deserves 

 grounds . Pérro J. PE&TRIDIS 

 petition . 
 STUDENTS ’ CONCERT 

 Students ’ Orchestral Concert , given Queen Hall 
 March 30 , found agreeable large audience 
 assembled . Miss Catherine E. O’Brien displayed excellent 
 capacity Rimsky - Korsakov Pianoforte Concerto 
 C sharp minor , lad Joseph Coleman played Paganini 
 Violin Concerto D astonishing brilliance , Miss 
 Irene E. Francis sang Elgar beautiful song ‘ Poet 
 Life ’ charm sympathy . items 
 programme Beethoven seventh Symphony , Suite 
 Flute Godard , Op . 116 , played Miss Lilian B. 
 Cook , ‘ Memoriam ’ ‘ Jubel ’ Overtures . 
 Mr. Sachse conducted 

 218 MUSICAL TIMES.—May 1 , 1917 

 EOLIAN HALL 

 Mr. Mark Hambourg gave Beethoven recital March 
 jl . played Sonata , Op . 111 , joined Messrs. 
 aaw ( violin ) Doehaerd ( ‘ cello ) Op . 97 Trio . 
 Oriana Madrigal Society excellent form 

 YLINM 

 ancients represented . Leefiler choral ode , ‘ 
 fell battle , ’ American tribute , written , 
 believe , soon Civil War . fairly impressive . 
 ‘ Elegiac ’ Trio , flute , violin , harp , Arnold 
 Bax , feature . Mr. Kennedy Scott conducted 

 large audience attracted April 14 , hear Mr. 
 Albert Sammons Mr. William Murdoch perform 
 Sonatas violin pianoforte . charming 
 Beethoven Sonata F , Op . 34 . second item 

 uasi - novelty . MS . Sonata D minor late 

 Miss Olga Haley , highly favourable appearances 
 earlier season , gave vocal recital March 30 . 
 gifts voice , shows considerable culture taste , 
 vocalises adroitly . Probably able later 
 sing intense arresting expressiveness . 
 programme occasion mainly foreign . Mrs. Haley 
 accompanied fair sympathy , Mr. Sydney Brooks 
 played ’ cello solos 

 concert thirty - season ( 738th concert ) 
 South Place Sunday Popular concerts given 
 March 25 . Beethoven Septet , Op . 20 , Schubrrt 
 Octet , Op . 166 , instrumental items , Miss 
 Helen Henschel sang British French songs 

 South Place String Orchestra gave - second 
 orchestral concert Sunday , April 1 . Mr _ Richard 
 Walthew conducted . Parry ‘ Lady Radnor Suite , ’ 
 Elgar Serenade , Op . 20 , welcome items . 
 ‘ Symphonie Spirituelle , ’ Op . 38 , Asger Hamerik , 
 selection . Mr. John Saunders played solo 
 Bach Concerto E major violin strings 

 excellent beat orchestra gave , addition § . * 
 

 accompaniments Concerto Fan 
 Beethoven Symphony . 5 , C minor , Wagned 
 * Meistersinger ’ Overture . M. de Greef appeared 
 conductor , directing success effective t¢ 
 scriptions Flemish folk - songs 

 Birmingham Choral Orchestral Associatiog 
 concert season given Town 
 March 24 , miscellaneous character . 
 Joseph H. Adams , conductor , provided - gener 
 programme , account orchestral items } 
 omitted owing lateness hour . Rossj 
 Overture , ‘ William Tell , ’ Wagner Prelude ‘ Tristan 
 Isolda , ’ conductor Miniature Suite 
 orchestra , * ‘ Swiss Scenes , ’ constituted principal orchest 
 contributions . dainty lyrical Suite received bg 
 performance . choir gave artistic interpretation 
 Elgar welcome - song , ‘ Weary wind Wes 
 exhilarating exposition Percy E   Fletche 
 polyphonic choral rhapsody Welsh airs . pleas 
 interlude Mr. Arthur Cooke remarkably brill 
 performance Mendelssolin G minor Piancforte Con 
 played captivating ‘ Staccato Caprice . 
 solo vocalists Miss Mary Whitfield Mr. Arthy 
 Jordan , artists excellent form 

 SHEFFIELD DISTRICT 

 closing concerts Misses Foxon Winter series 
 oThursday o’Clock brought forward interesting 
 music . Miss Agnes Griffith sang Hurlstone Miniature 
 Ballads penetrating insight , suitably dramatic 
 Purcell ‘ Mad Bess ’ ; Miss Helen Guest gave strong 
 - proportioned interpretation Beethoven 
 ‘ Appassionata ’ Sonata ; Miss Minnie Wilson Mr. 
 Allan Smith played movement Lekeu 
 Sonata G , pianoforte violin ; Miss 
 Lenore S. Carter showed versatility songs 
 Korting , Lie , Aulin , Mozart , MacDowell 

 Spring Concert Sheffield Amateur 
 Musical Society , Mr. J. A. Rodgers conducted perform- 
 ances Coleridge - Taylor ‘ Tale Old Japan , ’ 
 Overture ‘ Tannhiiuser , ’ Act 
 work . choir Society maintains good balance 
 parts , - supported ( especially men singers — 
 working daily severe pressure ) . occasion 
 choral - singing equalled , cantata surpassed , 
 hitherto best effurts Society . concert 
 shorn public attractiveness failure 
 Mr. Frank Mullings appear . Apologies 
 unexplained absence , omitting parts 
 ‘ Tannhiuser , ’ requisitioning chorus tenor , 
 improvised devices , concert successfully 
 aaried . Miss Eva Rich sang brilliantly 
 Elizabeth Venus ; Miss Daisy Evans , talented young 
 contralto , won golden opinions ; Mr. George Parker 

 Symphony . Miss Lucy Pierce gave artistic ; — _ — - 
 refined reading solo Schumann Pianoforte editorship Music Trades Review , RAS 
 : contemplates starting trade journal title 

 panne Music Trade . 
 BIDEFORD.—The choir Parish Church gave Ilffe Messrs. Enoch & Sons removed aia publishing al 
 ‘ Crucis ’ April 30 . Mr. Henry Hackett , organist | Office 58 , Great Marlborough Street , W.- card , 
 choirmaster , played organ . conductor . — _ — ee ART 
 CaLcutTra.—We received Mr. H D. Statham Lu 
 programmes symphony concerts given Einswers Correspondents , ITT , 
 auspices Calcutta School Music December , 1916 anc 
 January , February , March , 1917 . fu'l orchestra E. I. C. B.—Your definitions * music ’ hold ARG 
 - players orchestra | water . ‘ sound mudulated Tri 
 School augmented members Excellency the| ear , ’ ‘ svunds accordance harmony . ’ Trios , © 
 Governor Band . character concerts be|ear ? accordance idea harmony ? hs 
 seen following synopsis progrimmes : | dues ‘ modulated ’ mean connection ? yt 
 ea raggge ‘ Scotch ° ( Mendelssohn ) , ° Military , ASPIRANT.—Stainer ‘ Harmony ’ ‘ Composition , ’ + Mae 
 ( Haydn ) , . 39 , E flat ( Mozart ) , ‘ Eroica " | ad Bridge ‘ Counterpoint , ’ Primers Novello Series | gow ? ” 
 Overtures : * Don Juan ’ ‘ Hills ’ ( H. D. | wil greatly help 
 Statham ) . Pianoforte Concertos : Beethoven C major ; & y os Hi , Balfo 
 Schumann , Grieg ; B - ethoven Violin Concerto — CHO 

 Romance , Op . 50 , Elgar ‘ Canto Popolare . ’ Pul 
 soloists Lady Woodroffe ( pianoforte ) , Mrs CONTENTS . Tonic S 

 MUSICAL TIMES.—May 1 , 1917 . 235 

 praise majesty Mendelssohn 1$d.| Letnotyour ms wet te al unac . ) M.B. Foster 3d . ) 
 Achieved glorious work .. . Haydn 1d . | * Let ( Four- -part arrangement , organ ) Myles B. Foster 3d . 
 tachieved glorious work - — C horus ) Haydn 14d . | * Let celestial concerts unite .. andel oy 
 ‘ glory Lamb Spohr 14d . | * Lift heads . Handel J. L. Hopkins , 1d , 
 Awake , glory M. Wise 3d . | * Lift heads S. Coleridge- -Taylor 3d . 
 “ Christ obedient unto de ath J. F. Bridge 14d Lift heads . Turner 2d . 
 Christ entered Holy Places Eaton Faning 14d . | * Look , ye saints .. Myles B. Foster 3d . 
 Come , ye children oe oe os Henry John King 3d . O ye people , clap hands H. Purcell 3d . 
 Oliver King 14d . | * O clap hands : . Stainer 6d . 
 God gone * Croft , 9 4a 5 W. B. Gilbert 2d . O clap hands T. T. Trimnell 3d . 
 ‘ God , King ... Bach 1d . | * O God , King Glory H , Smart 4d . 
 Grant , beseech T hee . H. Lahee 14d . | * O God , Thou aan Mozart 3d . 
 Grant , beseech Thee ( Collect ) A. R. Gaul 3d . | * O amiable J. Barnby 3d . 
 ‘ Hallelujah unto God Almighty Son .. Beethoven 3d . | * O Lord Governour Gadsby 3d . 
 ‘ excellent Thy , O Lord Handel 14d.| O Lord Governour Marcello 1d . 
 ‘ fye risen Christ .. Ivor Atkins 4d . | * O risen Lord os J. Barnby 19d . 
 Ifye risen * F. Osmond Carr J. Naylor , ea . 3d . | * Open oan F , Adlam 4d . 
 Ifye risen ( parts ) .. les B. Foster 3d . * Rejoice Lord > Calkin 14d . 
 Father house H. Elliot Button J. M : wdeC rament , ea . 3d . | * Sing unto God - Bevan 3d . 
 Isthatday . ; ze Elvey 4d * thousand times thousand en - E , Vi ine Hall 3d . 
 day ( Open ye e g ates ) Maker 3d . earth Lord .. oe os ve T. T. Trimnell 4d . 
 ‘ Itshall come pass ‘ - ‘ S. Tours 14d . | * Lord exalted . ee - John E , West 14d . 
 [ leave comfortless . W. Byrd 3d . Lord King H. Gadsby , 6d . ; H. J. King 4d . 
 “ King - glorious .. J. Barnby 6d . Thou art priest ‘ S. Wesley 3d . 
 King - glorious ( C “ horus ; arr . “ 1 voices ) J. Barnby 4d . * Unfold , ye portals $ Ch . Gounod 3d . 
 “ lave , forsake J. Stainer 14d . | * Thou reignest Schubert 3d . 
 lt heart Eaton Faning G. Gardner , 3d . weak helpless : Rayner 2d . 
 ANTHEMS WH ITSUNTIDE . 
 people saw . Stainer 6d . Father house - . J. Maude Crament 3d . 
 “ suddenly came men j. Wood 3d . shall come pass G. Garrett 6d . 
 day Pentecost C. W. Smith 3d .   * shall come pass B. Tours 14d . 
 ‘ ds pants hart .. ae Spohr 14d Let God arise : “ , - Greene 6d . 
 tds hart pants .. Mendelssohn 14d Let God arise - os T. T. Trimnell 4d . 
 Bebold , send promise - J. Varley Roberts 4d . | * Let : ut troubled . H. G. Trembath 14d . 
 Come , Holy Ghost ; Attwood 14d Look , Holy Dove .. B. Selby 3d . 
 Come , Holy Ghost iy Elvey J. L. Hatton , 4d . | * O clap hands sk J. Stainer 6d . 
 Come , Holy Ghost . C. Lee Williams Palestrina , 2d * O thanks ~~ G. Elvey 3d . 
 Come , Thou Holy Spirit ° J. F. Barnett 3a . | * O Holy Ghost , minds .. G. A. Macfarren 14d . 
 Donot fill heaven earth . - Hugh Blair 3d * Oh ! closer walk God ‘ .. Myles B. Foster rgd . 
 ‘ Bye hath seen ( - setting ) . Myles B. Foster 3d . O taste os : * Goss ; ; A. H. Mann , 3d . 
 ‘ Bye hath seen ( Four- _ setting ) Myles B. Foster 3d . | * Otasteandsee . si ‘ Sullivan 14d . 
 Fear thou ° ‘ Josiah Booth 14d . ' O Thou , true Light Mendelssohn ad . 
 thanks unto God ; pohr 4d . O shall wisdom found Boyce 6d . 
 Glorious powerful God Orlando cithons 3d . | * blest Redeemer E. V. Hall 3d . 
 “ God came Teman C. Steggall 4d .   * Praised Lord daily J. B. Calkin 1$d , 
 4edisa Spirit . W.S. Bennett 14d Sing Lord .. Smart ts . 
 Great Lord . W. Hayes 4d . * Spirit mercy , truth , love . B. Luard - Selby 7 ” 
 Grieve Holy Spirit J. Stainer 3d . * Spirit mercy , truth , love H. A. Chambers 14d . 
 Hail ! breath life ; . Thomas Adams _ 1}d eyes wait Thee Gibbons 4d . 
 Happy man . ae E. Prout 8d .   * Glory God Israel T. Adams 3d . 
 dwelleth secret place os Josiah Booth 4d . * Lord came Sinai John E. West 3d . 
 tHoly Spirit , come , O come ar , Ad ae Sanctum ) G. C. Martin 14d Lord descended Hayes 14d . 
 Iwas spirit . Blow 6d , Lord Holy Te mple J. Stainer 4d . 
 ‘ Iwill magnify Thee : J. H. Parry 3d . Lord Holy Temple E. H. Thorne 14d . 
 ‘ Iwill leave comfortless Bruce Steane 2d . love God shed abroad S. Reay 1 
 ‘ lwill pray Father G. W. Torrance 14d condemnation H.S. Irons 3d . 
 IfI away oe - “ Thomas Adams 14d Spirit God ° Arthur W. Marchant 3d . 
 Iflgonot away .. os “ “ A. J. ¢ Caldicott 3d . | * wilderness . . John Goss , ad . ; * S.S. Wesley 6d . 
 Ifye love S. Heap 14d . * God old came Heaven .. E. V. Hall 3d . 
 Ifye love W. H. “ Monk , Tallis , R. P. art , 14d rejoice . os Croft 4d . 
 tIfye love Bruce Steane 2d . Day Pentecost A. Kempton 3d . 
 Kye love Herbert W. Wareing ‘ W. . Westbrook , 3d . Whosoever drinketh J. T. Field 14d . 
 ANTHEMS TRINITYTIDE . 
 ‘ Almighty everlasting God . ° Gibbons 14d . | * Jewry God known .. . J. Clarke - Whitfeld 14d . 
 Almighty God , hast promised H. Elliot Button 1d . sweet consent E. H. Thorne 3d . 
 * Angel Spirits , blessed eg sky ad . fear Lord J. Varley Roberts 3d . 
 Ascribe unto Lord S.S. Wesley 4d . Let peace God J. Stainer 4d . 
 " Behold , God great - E , Ww . Naylor 4d . | * Let Thy merciful ears A. R. Gaul 14d . 
 Beloved , God loved J. Barnby 14d . | * Light world - ‘ E. Elgar 3d . 
 Beloved , let love . Gerard F. Cobb 14d . | Lord power .. ee - E. T. Chipp 3d . 
 ye mind .. Arthur E. Godfrey 3d . | * Lord power . William Mason 14d . 
 “ Blessed man ° John Goss 4d . Lord power ( men ' s voices ) . J. Barnby ed . 
 Blessing glory Boyce 14d . | * Lord , pray Thee ‘   ' Varley Roberts 14d . 
 ‘ Blessing , glory .. Bach 6d . O Father blest én J. Barnby 3d . 
 Come , ye children Josiah Booth 3d . O God , hast prepared A. A. Gaul ed . 
 came Teman C. Steggall 4d . O joyful Light . Tours 4d . 
 ‘ God loved world .. ’ Matthew Kin gston 14d . | * O Lord , trust . King Hall 14d . 
 Grant , O Lord Mozart 14d O taste .*J. Goss A. H. Mann , 3d . 
 Grant , Lord . oe es es - . H. Elliot Button d. | * O taste : A. Sullivan 14d . 
 " Hail , gis addening L ight nee J. T. Field , 2d . ; * G. C. Martin 4d . Oo wher e shall wisdom found ? ? y 6d . 
 ‘ Holy , holy , holy .. + : ae Crotch 3d . Ponder words , O Lord Arnold D. Calley 14d . 
 Holy , Lord God Almighty T. Bateson 4d . | * Praise awful .. Spohr ed . 
 “ goodly Thy tents F. Ouseley 14d Rejoice Lord os G. C. Martin 6d . 
 ow lovely Thy dwellings Spohr 14d . | * love hath Father Mendelssohn 14d . 
 * Hymn Trinity Tchaikovsky 14d Sing Lord Mendelssohn 8d . 
 lam Alpha Omega .. . ee Ch . Gounod 3d . * Stand bless - Goss 4d . 
 “ lam Alpha Omega .. se ‘ J. Stainer 14d Teach Thy way “ W. HL Gladstone , aid . ; Frank L. Moir 3d . 
 lam Alpha Omega ‘ se 2 Varley Roberts 3d . | * Lord hath mindful Ss . S. Wesley 3d . 
 beheld , lo ! ae ee es slow 6d . * Lord Shepherd G. A. Macfarren 14d . 
 know ord great “ e ine om F. Ouseley 14d . | * Lord Shepherd J. Shaw 3d . 
 Sawthe Lord . os Cuthbert Harris , 3d . ; * J. Stainer 6d . | Lord comfort Zion H. Hiles 6d . 
 magnify ma oe ia aa J. Shaw 3d . Thou art worthy , O Lord F. E. Gladstone 3d . 
 sing Thy power . Greene 4d . | Thou shalt shew path life Alan Gray 14d . 
 * sing Thy power . A. Sullivan 14d . | humbly beseech Thee H. Elliot Button 1d . 
 J sing unto Led H. Wareing 3d . | Whatsoever born God H. Oakeley 3d . 
 Ta humble faith ‘ G. Garrett 14d . ' comprehend Thee Mozart 3d 

 236 MUSICAL TIMES.—May 1 , 1917 

