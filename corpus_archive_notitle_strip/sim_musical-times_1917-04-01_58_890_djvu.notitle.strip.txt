It is an interesting little fact—interesting specially 
to musicians—that Prince Lvov, one of the leaders of 
the great Russian Revolution, should belong to the 
same family as the Lvov who composed the Russian 
National Anthem, the glorification of Tsardom. The 
Anthem has not, at the time of writing, been officially 
deposed ; but we gather from the reports which 
appear in this country that its place has been taken 
by revolutionary songs—some specifically Russian, 
some international. It will no doubt be necessary to 
create a new song for the new nation, and it will be an 
enviable task for those who are charged with it. As 
a rule, National Anthems made to order are not 
successful: such things should be a _ spontaneous 
growth. A few months ago the German government 
offered a prize for the words and music of a national 
hymn for the ‘ Independent State’ of Poland, and we 
have not yet heard with what result ; but unofficial 
musical Germany was sceptical about it

The war has not interfered with 
the indefatigable industry of 
German antiquaries. Theodor von 
Frimmel, the well-known Beethoven 
scholar, has discovered a hitherto unknown com- 
position of Beethoven. It was known that Beethoven

A BEETHOVEN 
FIND

had collaborated in Stark’s ‘ Pianoforte School’ which

On March 13 Miss Thelma Bentwich, whose skill as a 
violoncellist gives her high rank, and Miss Myra Hess 
co-operated to give a recital. Brahms’s Sonata in F, 
Op. 99, was a notable item

It was gratifying to note the good attendance at Miss 
Gwynne Kimpton’s London Amateur Orchestral War 
Concert on March 14. No doubt one of the great attrac. 
tions was Mr. Albert Sammons, who played Beethoven's 
Concerto. Miss Ilma Elliot was an acceptable singer

The Royal College of Music students’ concerts have 
brought forward attractive chamber music programmes. On 
February 22 the Quartet for pianoforte and strings in G minor 
(Op. 25) by Brahms was a well-played item. Miss Maud 
Gold, a young violinist, played with much distinction 
movements from Bach’s Partita in B minor. On March 8a 
String Quartet in A minor by Mr. Stanley Wilson showed 
that the young composer could move freely on classical lines. 
A Phantasy Trio by Mr. John Ireland was also performed. 
On March 15 an excellent performance of Faure’s Sonata 
in A for pianoforte and violin was given by Miss Kathleen 
Long and Miss Nancy Phillips

The South Place Sunday Popular Concerts are faithful to 
the British composer. On February 25, Quartets by 
H. Waldo Warner, Joseph Speaight, Joseph Holbrooke, 
and James Friskin (C minor, Op. 1), were played by a party 
led by Mr. John Saunders. Miss Janet Dunlop Smith sang, 
and Mr. Richard Walthew played a Beethoven Pianoforte 
Sonata. On March 11 a String Quartet in E flat, by 
Mr. Walthew, was given its first public performance. The 
Brahms Quintet in G minor, Op. 34, was another item

The students of the Royal Academy of Music gave an 
attractive concert in the Academy Hall on March 9

The soloists were Mr. William Murdoch, the Australian

ianist, Miss Mina Harpur, an admirable local violinist, and 
Mr. Harold Morrow, a local singer. Mr. Murdoch’s selection 
was from Chopin, Debussy, and Liszt, Miss Harpur played 
the Adagio and Finale from Max Bruch’s G minor Concerto, 
accompanied by the Orchestra, and the orchestral pieces 
included Beethoven’s C minor Symphony, Borodin’s Overture 
to ‘ Prince Igor,’ Overture in A minor, ‘John of Gaunt,’ by 
Dr. Wadeley, organist of Carlisle Cathedral,—who con- 
ducted his own very interesting composition,—and Edward

German’s incidental music to ‘ Nell Gwyn

The Midland Musical Society gave at the Town Hall, on 
February 16, a concert recital of Gounod’s opera ‘ Faust,’ 
under the direction of Mr. J. A. Cotton, who had under his 
beat a well-equipped orchestra and the excellent choir of 
this Society, and he was further supported by Mr. C. W. 
Perkins, organist, and the following principals: Miss Lilian 
Dillingham, of the Queen’s Hall concerts, Madame Malvena 
Edwards, Mr. Walter Ottey, Mr. Arthur Cranmer, and 
Mr. Alfred Askey

The Birmingham Symphony Orchestra, conducted by 
Mr. Julian Clifford, gave a concert at the Town Hall on 
February 24, for which the Society had secured Mr. Albert 
Sammons, who appeared in khaki and who gave a fine 
performance of Beethoven’s Violin Concerto. He also gave 
a brilliant interpretation of a Caprice by Paganini, and a 
Prelude by the same composer. The outstanding feature of 
the orchestral contributions was a spirited performance of 
Sibelius’s ‘ Finlandia’ and the ‘ Prince Igor’ Suite of dances 
by Borodin. Quite charming was the Bolzoni Minuetto for 
strings

Owing to the counter-attraction at the Prince of Wales 
Theatre the chamber concert at the Rooms of the Royal 
Society of Artists, on February 28, suffered in point of 
attendance. Once more the Catterall Quartet supplied the 
programme, which opened with a novelty, a Quartet in G 
major by the Russian, Gretchaninov, a charming and 
attractive work which one was glad to hear, especially as it 
was faultlessly performed. Another work new to Heniahem 
was a Fantasie for String Quartet by Dr. Ernest Walker, 
a charming composition full of graceful fancies. The concert

THE MUSICAL TIMES.—Aprit

1 Strings, igh splendid Symphony must have convinced all the serious 
artet in gf musicians present—if conviction were needed—that Scriabin’s 
death robbed the world of a very great composer. Capital 
last concer} performances have taken place, too, of Elgar's ‘Cockaigne’ 
n March 19 f Overture and Debussy’s lovely Prelude, ‘ L’aprés-midi d’un 
ormance qf Faune.’ Beethoven’s fourth Symphony was a very welcome 
n,” based qf feature of the concert on March 8, and at this concert also 
ructure, ig} was brought to a first hearing at Bournemouth the Rhapsody 
» were ‘Pan,’ by Cyril B. Rootham, a pleasing work, though 
taking cop.f unequal. — ; 
Miss Mare§ Respecting the soloists at these concerts, we must first 
e, and Mr single out Mr. Frederick Dawson, our indebtedness to him 
rvice. An} being very great for the opportunity he has again given us of 
vur Cooke's} hearing the wonderful Pianoforte Concerto of Delius— 
Pianofort | undoubtedly, in our opinion, the finest concerto written for the 
> orchestm, | pianoforte since the Grieg. In spite of a badly-tuned 
ival Chori} instrument, Mr. Dawson’s illuminating performance was a 
rmances off splendid example of emotional playing and transcendent 
t even tha | technique. Other artists have been Miss Marjory Dorning, 
enry Wood a Bournemouth violinist of rare refinement and charm, who 
knowl produced for the first time in England the G minor Concerto 
yur to th} of d’Ambrosio, an extremely good composition. The 
grandiose } interpretation was equal to Miss Dorning’s best, which is to 
cured thay | say that it was first-rate. On March 15, Bach’s noble Violin 
ter Millar, | Concerto in E was the medium for some expressive playing 
artists are} by Miss Constance Izard, a very tasteful performer whose 
rior to the | temperamental qualities, however, are in advance of her 
Post’ was} technical attainments. 
of the late} |The writer’s attendance at the ‘ Monday Special’ Concerts 
being impossible, no reference to the playing of the attractive 
Bantock’s | programmes can be made ; but there have been various other 
nposer, to f events which call for brief mention. Foremost among these 
tice, as in | was Mr. Godfrey’s annual concert, which was highly successful. 
descri Sir Henry Wood conducted several numbers in his own inimit- 
technique, able manner, Mr. Mark Hambourg played the Tchaikovsky 
al creation | Concerto in B flat minor, and Mr. Frank Mullings was the 
t is almost } vocalist. An orchestral concert with which Madame Kirkby 
wealth of | Lunn was associated was another attraction. [ler contri- 
will arise } butions, though beautiful from the purely vocal standpoint, 
took place F were it must be confessed rather prosaic, and much more 
could have been made of Mendelssohn’s ‘O rest in the 
Lord.’ A return visit by M. Strockov, the Russian violinist, 
assisted this time by the Municipal Orchestra, was a 
pleasurable event; and a Chopin recital by M. Vladimir 
de Pachmann on March 8 proved delightful, the famous 
pianist by his beautiful playing almost recalling the days 
t is often | when he was at his zenith. Mr. Frederick Dawson, in 
the other | addition to his appearance at a Symphony Concert, gave a 
ustifiable, } very interesting recital, but for a second time laboured under 
lary. To} the weighty disadvantage of having to perform on an instru- 
Symphony ment that was badly out of tune. On March 14 Miss Adela 
aled with | Verne and M. Vallier gave a pianoforte and vocal recital, 
d say, an} both the performers receiving a warm welcome. Miss 
must be | Verne’s playing must indeed have been a revelation to those 
was writ | hitherto unacquainted with this great artist. Our final entry 
) over the | for this month is the visit paid by Mr. Plunket Greene on 
composet St. Patrick’s Day, when this admirable singer appeared in 
g requisi- | onjunction with the Orchestra. In the interpretation of 
forte and — song it is doubtful whether he has an equal. 
‘Dream- 
ol brooke, 
ined that BRISTOL. 
ered that The two concerts given by the Choral Society this season 
e on the have been splendidly supported, a fact that seems to indicate 
ave since | that the oratorios of Handel and Mendelssohn are still a 
it to itto | daw. There was not a vacant seat at the Colston 
pressure } Hall on March 17, when ‘For the Fallen’ and ‘Elijah’ 
a special | received interpretations of the high standard expected of 
e concett | Bristol’s premier Choral Society. Elgar’s requiem was of 
slebrated | course new to the singers, but most impressive was the effect. 
, but the | The choir and orchestra numbered five hundred performers, 
orsakov's | Miss Agnes Nicholls was the soloist, and Mr. George Riseley 
5 place | directed the performance. In ‘ Elijah’ the principal soloists 
y skilfal | were Miss Agnes Nicholls, Miss Margaret Balfour, Mr. 
Concerto | Joseph Reed, and Mr. Herbert Brown. As Mr. Riseley is 
never satisfied with the choir even in familiar works, unless 
ver, was} it has been thoroughly rehearsed, a grand presentation of 
ink that sach numbers as ‘ Thanks be to God’ was forthcoming, and 
criabin's} as this preceded the interval it was followed by a great 
yed, this} outburst of applause, this and the close of the oratorio being

I, 1917. 179

Plymouth Guildhall was varied on March 3 by a 
programme in which the string band of the Royal Garrison 
Artillery (Mr. R. G. Evans, conductor) collaborated with 
the borough organist in a Postlude by Smart, Tchaikovsky’s 
*Marche Slav,’ and the ‘Solemn Melody’ of Walford 
Davies. The band played asa novelty a Suite ‘ From the 
country-side ’ by Eric Coates, which proved picturesque and 
rhythmic music with attractive melodies

Chamber music at Torquay Pavilion, on February 20, 
consisted of Beethoven’s String (Juartet, Mozart’s Pianoforte 
Quintet in G minor (played by Mrs. Lennox Clayton, Miss 
Jessie Bowater, Mr. Lennox Clayton, and Miss Ethel Pettit, 
with Mr. Edgar Heapat the pianoforte), and Miss Ethel Pettit 
and Mr. Heap gave a beautiful performance of Brahms’s 
Sonata for Cello and Pianoforte, No. 1. On February 24, 
Mr. Philip Cathie(violin)and Miss Dorothy Dawson-Campbell 
(pianoforte) played music by Bach, Arensky, Paganini, 
Sarasate, Liszt, Chopin, Rachmaninov, and Scriabin, and 
with the orchestra the pianist played MacDowell’s Concerto 
in D minor, No. 2. Mr. Lennox Clayton conducted, and 
Miss Winifred Fisher sang. On March 9, the Reserve 
Battalion of the London Regiment R.F. gave two concerts, 
severally in orchestral and military combination, the most 
important item being Tchaikovsky’s ‘ 1812’ Overture played 
by the military band. In the Museum Hall at Torquay, on 
March 3, a patriotic Empire Cantata written and composed 
by Mr. E. P. Bovey, was interpreted by sixty youthful 
performers under the composer’s direction. At Waldon 
House, Torquay, members of Torquay and District 
Organists’ Association listened to a paper read by Mr. W. L. 
Twinning on ‘ Organ Accompaniment

At the tiny village of Wrafton, near Barnstaple, on 
February 19, a miscellaneous concert was given by Miss 
Pauline Hook, the Misses Alford and Hunt, Messrs. 
S. Harper and Minchington (vocalists), Miss Steadman and

Mr. Joseph Holbrooke’s fourth programme at the Crane 
Hall, on March 5, was typically modern, and provided 
the uninitiated with one or two nuts to crack in the 
elusive harmonic scheme of Ravel’s Quartet in F, in 
Mr. Holbrooke’s Pianoforte Toccata, an unbeautiful 
tour de force, and Mr. Eugéne Goossens’s clever and 
imaginative little pieces ‘ By the Tarn’ and ‘ Jack o’ Lantern.’ 
Miss Gladys Moger sang artistically in songs by Purcell and 
Hook, as well as in Mr. Holbrooke’s ‘ Killary,’ a characteristic 
example not exactly compelling in its vocal charm. On this 
occasion, in the string quartet, Mr. John Saunders was 
associated with Mr. C. Woodhouse, Mr. E. M. La Prade, 
and Mr. Felix Salmond

For the fourth and final chamber concert of the Rodewald 
Concert Society on March 12, a selection of somewhat 
violent contrast was offered in Ravel's Quartet in F, 
Dittersdorf’s Quartet in E flat, and Beethoven’s Quartet 
in G, Op. 18, No. 2, which by its calm and formal beauty 
helped to restore the equanimity of those whom Ravel had 
puzzled. These of course did not include the ultra-moderns 
of the audience, who had reason to be delighted with the 
performance of the complex music given by Mr. Catterall and 
his associates, Mr. John Bridge, Mr. F. S. Park, and 
Mr. J. S. Hoek, who are players of infinite resource

Conducted by Mr. John Tobin, Elgar's latest choral 
works ‘ For the Fallen’ and ‘To Women’ were sung by the 
Crosby, Waterloo, and Blundellsands Choral Union with 
Miss Ella Rees as soprano soloist, in the Waterloo Town 
Hall on March 14

artistic cellist, Mr. Maurice Taylor, played, with Miss Ethel 
Cook, the D minor Suite of Saint-Saéns for pianoforte and 
’cello, and was also heard in Bach’s Suite No. 3 for ’cello 
alone. The finished violin-playing of Miss Zoe Addy, the 
thoughtful interpretation by Miss Ethel Cook of two of 
Brabms’s Intermezzi, and the tasteful singing by Miss Ena 
Roberts of some modern art-songs, are to be recorded

A recital of music for two pianofortes was given at the 
Montgomery Hall by Miss Margaret Welby and Miss 
Marguerite Pogson. The programme included ‘ Silhouettes,’ 
Op. 23 (Arensky), Variations on a theme of Beethoven, 
Op. 35 (Saint-Saéns), ‘Petite Suite’ (Debussy), and 
‘Variations on an Irish Air,’ Op. 17 (Norman O'Neill). 
The net impression of the recital was favourable to the 
combination, in hands capable of securing contrast and 
variety of treatment. The programme, so excellently 
diversified, was much enjoyed. Miss Eva Rich and Mr. E. 
Platts sang

The Hallé Orchestra, with Sir. Thomas Beecham in 
command, played at the last Subscription Concert. Mr. 
William Murdoch was the soloist in a masterly performance 
of Beethoven’s ‘ Emperor’ Concerto

YORKSHIRE

The Huddersfield Choral Society, on March 2, gave 
Handel’s * Samson,’ which suited well the forceful splendour 
of its remarkable choir. Miss Esta d’Argo, Miss Dorothy 
Webster, Mr. Alfred Heather, and Mr. Herbert Brown were 
a most capable quartet of principals, and Dr. Coward con

the Huddersfield Glee and Madrigal Society, which under y 
artistic conductor in Mr. C. H. Moody and an enterprisj 
president in Mr. Charles Sykes, has made a great advance q 
late, gave a very enjoyable concert of vocal music whig 
ranged from Orlando di Lasso to Debussy, and include 
an adaptation by the ingenious Sir Frederick Bridge of th 
* Battle Prayer’ in ‘ King Henry V.’ to music by Shakespeare; 
contemporary, Dering. Madame Kirkby Lunn’s dramat; 
singing enlivened the programme, and a young local violinig 
Mr. J. E. Crowther, showed that he has already achievg 
considerable technical ability. At Halifax, on March 8, My 
Fricker conducted a concert by the Choral Society which endg 
its ninety-ninth season, and included Debussy’s ‘ Blessed 
Damozel,’ Verdi’s ‘ Stabat Mater,’ and Mozart’s * Splendent 
Te, Deus,’ while Mr. Herbert Johnson repeated his Leeds 
success as soloist in Franck’s Symphonic Variations. It wasa 
altogether delightful programme, and the performancs 
sustained throughout a high standard of efficiency. Mig 
Olive Sturgess and Miss Hilda Mitchell were the principd 
vocalists. On March 2, Mr. Catterall’s quartet party 
appeared at the Halifax Chamber Concert, and played 
Quartets by Beethoven (in G, Op. 18), Brahms (in A minor, 
Op. 51), and Taneiev (in A minor, Op. 11) in brillian 
style. Messrs. J. S. Bridge, F. S. Park, and J. C. Hoe 
were Mr. Catterall’s colleagues. On February 24 Mis 
Ella M. Bradford gave a chamber concert at Harrogate, and 
joined Mr. Rawdon Briggs in Violin Sonatas by Beethoven 
(in C minor, Op. 30) and Brahms (in A). Miss Dorothy 
Milnes and Miss Ethel Milnes contributed songs to the 
programme. On March 9 the Ilkley Vocal Society, with 
Mr. Akeroyd as conductor, and Mr. Percy Richardson at the 
pianoforte, gave a pleasing miscellaneous programme, 
including Brahms’s ‘ Song of Destiny,’ and Stanford’s * Songs 
of the Fleet.’ Mr. John Dunn was a brilliant solo violinist, 
and Miss Elsie Suddaby’s singing was a pleasing feature of the 
concert

CRAVEN ARMS.—The District Choral Society’s sixteenth 
concert on February 13, presented the concert-edition d 
‘A Princess of Kensington’ (Edward German). The Rev, 
W. M. D. La Touche conducted. The proceeds were far 
V.A.D. funds

Great Ayron (YorRKs).—Mr. F. 
March 6, lectured on ‘ French composers.’ 
vocal and instrumental illustrations

HANLEY.—The North Staffordshire Symphony Orchestra 
brought forward an excellent programme on March 1. It 
included Beethoven’s No. 5 Pianoforte Concerto, the solo in 
which was played by Miss Lucy Pierce, and his seventh 
Symphony. Mr. John Cope conducted

Rivers Arundel, on 
There were

Requiem was given a rich and sympathetic performance

ee 
sh under y 
enterprisi 
advance 
lusic whid 
id include 
idge of th 
akespeare' 
’s dramatic 
al violinig, 
ly achieve 
arch 8, Mr 
yhich ended 
's * Blessed 
Splendent 
his Leed 
- It wasa 
rformance 
ney. Mis 
€ principal 
tet party 
nd_ played 
n A minor, 
in. brilliant 
|. C. Hock 
y 24 Mis 
ogate, and 
Beethoven 
s Dorothy 
zs to the 
ciety, with 
dson at the 
rogramme, 
d’s * Songs 
O violinist, 
ture of the

s sixteenth 
-edition of

Miscellaneous

There has been trouble at Helensburgh (Scotland). The 
town rejoices in a small but excellent concert hall in which 
chamber subscription concerts of a high class have been 
given for twenty-nine years. Panels in the hall have borne 
the names of Palestrina, Gounod, Purcell, Beethoven, 
Mendelssohn, and Wagner. Ina fit of patriotic indignation 
with these abominable ‘ foreigners’ the Town Council, who 
are the guardians of the edifice, recently had a// these names 
erased and the badges of six Highland regiments substituted. 
What next? Will the representatives of the town inhibit 
the performance of music by the above-mentioned composers 
and others of a like kidney

At the recent conferring of degrees in Dublin University, 
Mr. John F. Larchet, Mus. Bac., nondum graduatus in 
artibus, obtained the Mus. Doc. Dr. Larchet is Musical 
Director of the Jesuit Church of St. Francis Xavier, Dublin, 
and conductor of the Abbey Theatre orchestra. By a special 
grace at a meeting of the Senate, on March 17 (Feast of 
St. Patrick), the honorary degree of Mus. Doc. was granted 
by the Board of Dublin University to Rev. Edmund H. 
Fellowes, Mus. Bac., in reccgnition of his work as editor of 
‘The English Madrigal School,’ of which thirteen volumes 
have been published

THE MUSICAL TIMES.—Aprit 1, 1917. 187

rd.) 
ANTH EMS FOR ASCENSIONTIDE. 
*Above all praise and all majesty ° Mendelssohn 14d. | Letnotyour heart be troubled(DoubleChorusunac.) M.B. Foster 3d. 
Achieved is the glorious work .. Haydn 1d. | *Let not (Four-part arrangement, with organ) Myles B. Foster 3d. 
YORK. *Achieved is the glorious work _ Chorus) «. Haydn 14d. | *Let their celestial concerts all unite .. Handel 14d. 
*All glory to the Lamb . Spohr 14d. | *Lift up your heads “ Handel and J. L. Hopkins, each 14d. 
am.” A Awake up, my glory _ oe she M. Wise 3d. | *Lift up your heads - . Ss. ree Taylor 3d. 
 Oret Y Christ became obedient unto death . es J. F. Bridge 1$d.| Lift up your heads - os - oe Turner 2d, 
rehestra, Christ is not entered into the Holy Places .. Eaton Faning 14d. | *Look, yesaints . he oo“ = we. +S Foster 3d. 
Come, ye children én .. Henry John King 3d. oO - ye people, cle ap your ‘hands os - H. Purcell 3d. 
For it became Him ee os es es Oliver King 1}d. | *O clap your hands os os J. Stainer 6d, 
seth under. Godisgoneup .. sa an *Croft, 4d.; W. B. Gilbert ad. O clap your hands an on ch T. T. Trimnell 3d. 
x Solo and § *God, my King - os am Bach = *O God, the King of Glory “ = ” H. Smart 4d. 
Grant, we beseech Thee ee ee H. Lahee 14d. | *O God, when Thou aroun os oe es Mozart 3d. 
Grant, we beseech Thee (Cc collect) ahs ‘on A. R. Gaul 3d. *O howamiable . és wk ad . Barnby 3d. 
ch i *Hallelujah unto God's Almighty Son .. - Beethoven 3d. | *O Lord our Governour .. - os < . Gadsby 3d. 
cn 1s keep. *How excellent Thy Name, O Lord .. . Handel 14d.| O Lord our Governour .. - : — Marcello 1$d. 
| (6d.). *If ye then be risen with Christ .. Ivor Atkins 4d. | *O risen Lord mY “s i ‘a on J. Barnby 14d. 
If ye then be risen *F. Osmond Carr and J. Naylor, ea. 3d. *Open tome the gates... oe - oe F. Adlam 4d. 
——$—$—$——. If ye then be risen (Two pe arts) . Myles B. Foster 3d. | *Rejoice in the Lord ee ee oe os J. B. Calkin 14d. 
3 In My Father's house H. Elliot Button and J. MaudeC rament,ea. 3d. | *Sing unto God <. ve F. Bevan 3d. 
‘PORARY Inthatday .. oe . George Elvey 4c. | *Ten thousand times ten thousand ee os E. Vine Hall 3d. 
; mie In that day (Open ye ‘the g: ates) - ws F. C. Maker 3d. The earth is the Lord's .. a a iiss T. T. Trimnell 4d. 
f testimonials *It shall come to pass ee se B. Tours 14d. | *The Lord is exalted se es - os John E. West 14d. 
| will not leave you comfortless _ ee - W. Byrd 3d. The Lord is King ja .. H. Gadsby, 6d.; H. J. King 4d. 
———— § ‘King all-glorious .. on oo. J. Barnby 6d. Thou art a priest for ever aes i eg S. Wesley 3d. 
-L.R.AM King all-glorious (with C “horus arr. for four voices) J. Barnby 4d. | *Unfold, ye portals os an se on Ch. Gounod 3d. 
wrasse  *Leave us not, neither forsake us ‘J. Stainer 14d. | *W here Thou reignest oe _ Schubert 3d. 
~ La Let not your heart .. Eaton F aning and G. Gardner, each 3d. Who is this so weak and hel pless . - Rayner ad. 
i ANTHEMS FOR W HITSUNTIDE. 
St. Peter's, And all the people saw .. - . Stainer In My Father's house .. .. J. Maude Crament 3d. 
er at £12 pe *And suddenly there came . oe oe Hear) Wood ot It shall come to pass ae es oe oe G. Garrett 6d. 
. 5 And when the day of Pentecost — os os C. W. Smith 3d.  *It shal! come to pass os se oe os B. Tours 14d

As pants the hart .. . oe os os Spohr 14d. Let God arise we - os oe oe Greene 6d

