CHARLES V. STANFORD N H 
 " irish RHAPSODY LINCOLN FESTIVAL 

 EDWARD ELGAR H 
 VEDNESDAY , June 5 p.m. 
 " introduction ALLEGRO " SrrINGs ORCHESTRAL concert CORN EXCHANGE : Works 
 y Sir C. H. H. Parry Dr. F. H. Cowen ( conduct 
 FREDERIC H. COWEN U Composers ) ; Beethoven symph ( . 8) F , " Siegfried 
 : ; 9 Rheinfahrt , " * * Kaisermarsch , L’Arlésienne " Suite , & c. 
 " 6 ¢ > > 2 br , sr : 
 VERTURI BUTTERFLY BALI THURSDAY . Junt 
 N ORATORIO service C \THEDRAL . 
 CHARLES HARRISS 15 p.m. : Dvorak Te Deum ; Parry * Voces Clamantium " ( 
 | ducte Composer ) ; Strauss " ' Tod und Verklarung 
 Cuoric Ipyi * f PAN . Brahms " * requiem . 
 7 - 15 p.t Schubert ' * Unfinis S p Handel Israc 
 Mavame ALBANI Mite . PAULINE DONALDA hens soloist 
 1 > oct ) s : Miss Agnes Nicholls . Mr. Ss . 
 Mr. FFRANGCON - DAVIES v asssi rp 5 : acta Mr. 
 Miss Ada Crossley . Mr 

 LONDON SYMPHONY ORCHESTRA Principal - . 
 Org : t Mr. H. L. Balfour Dr. W. G. Alcock . 
 . + . . . ew conductor Dr. G. J. Bennett . 
 chorus voice : , , t 
 pr yspectus , ft information , ticket e obtain 
 se nc ini Festival Office , 347 , High Street , Linc 

 MOZART 2 o 

 7 . MINUET.—{Sonara E Fiat ) . ( op . 31 , iii . ) 
 BEETHOVEN 1 o 
 8 PRELUDE.—(‘‘CoLtomsa " ) C. MACKENZIE 1 6 
 9 . FINALE ( " o sing " ) } — " ' bl pair 
 siren " : .. C.H. H. PARRY 1 

 NiGut ' dream 
 MENDELSSOHN 1 6 

 MES.—JUNE 1 , 1906 

 November 28 , 180g — furnish connect link 
 composer Sfontini ( 1774 - 1851 ) 
 great Napoleon . letter 
 composer great man , refer 
 Napoleon having deign signify satisfaction 
 Spontini service honourable token good 
 . communication mention 
 gift prize Napoleon found , 
 year new opera 
 great success period : 
 instance award Spontini . 
 Napoleon warm interest production 
 ' Fernand Cortez ' — performance 
 idea 
 influence public opinion favour plan 
 spanish war , progress . 
 letter Spohr , page music 
 score Spontini opera ' Agnes von Hohenstaufen , ' 
 date ' July 7 , 1837 . ' letter ' far 
 include fugue / aximilian Stadler ( 1748- 
 1833 ) , friend Haydn , Mozart , Beethoven ; 
 song compose singer Joseph Staudig/ 
 ( 1807 - 61 ) , create thetitle - rélein Mendelssohn | 
 * elijah letter pen /udius 
 Stockhausen ( bear 1826 ) , eminent singer 
 teacher vocal art ; large number letter | 
 write Aichard Strauss ( bear 1864 ) , addition 
 sketch ' Feuersnot ' ( Kunrad monologue ) , | 
 endorse ' } 
 collection . Sohani Strauss ( 1825 - 1899 ) , | 
 Waltz King composer ' Blue Danube , ' 
 represent sketch operetta 
 * Der Zigeunerbaron 

 present 

 _ 

 address Director Court Opera in| 
 city . young composer , 
 - , ask honour play 
 pianoforte King Wiirtemberg , 
 concert auspex 
 Theatre management . : ' 
 play pianoforte sonata 
 Beethoven , work great 
 master , conduct symphony 
 composition , overture 
 opera . self - conceit , 
 happy gain favour Majesty 
 applause connoisseur place . ' 
 Joseph Weigl ( 1766 - 1846 ) , contemporary 
 Haydn , Mozart , Beethoven , know 
 giant music , find place Mr. Speyer 
 collection portrait , air ' Pria 
 che | ' impegno ' ( opera ' Amor Marino ' ) , 
 write , Beethoven rescue oblivion 

 theme /ina / e 
 Clarinet trio , Op 

 Sinfonia e flat , , Mozart 

 Scena , Miss Do.ny , ' ah ! perfido ' . Beethoven 

 Concerto , Pianoforte , M. MENDELSSOHN BARTHOLDY 

 _ _ 

 ent performance Beethoven Choral 
 Symphony Philharmonic Society , 
 stupendous work expressly compose , 
 serve recall previous rendering ' . 9 ' 
 sa auspex . complete list 
 yerfor ces , - - date , tabulate form 

 t ( 
 M 21 , 1825 Argyll Rooms . | SirGeorge Smart 

 ' Command concert , ' Queen Victoria 
 Prince Consort present — Scherzo 
 Choral portion perform , conduct 
 Spohr . performance 1837 Moscheles 
 write organ additional accompaniment 
 Choral section 

 Sir Charles Stanford , April issue 
 Monthly Journal International Musical Society , 
 attention point interpretation 
 Beethoven Choral Symphony . refer 
 absurd break - neck speed conductor 
 adopt 7770 section Scherzo , 
 Beethoven intention entirely set naught . 
 matter , , thresh Sir George 
 Grove , wonted enthusiasm , meeting 
 Musical Association February 12 , 1895 . 
 occasion Sir George error state 
 frs¢ edition work contain metronomic 
 direction 6 , 116 apply Preso question , 
 , matter fact , edition — copy 
 — contain metronomizing 
 later indication .-- 116 stamp 
 plate , duly print . 
 unfortunate doubtless accidental — wilful — 
 omission s / em body note 
 subsequent octavo edition cause motor - car 
 speed , speak 

 

 doubt minim 
 semibreve sign , Beethoven dictate letter 
 write Schindler Ignatz Moscheles expressly 
 letter , dictate Beethoven 
 death - bed day draw 
 breath , sign , possession 
 Mr. Felix Moscheles , kindly allow 
 view ' Occasional Note . ' 
 letter translate form 

 

 earn , heartfelt thank sympathy 
 assistance render . | com- 
 pelle ' , sum 
 1000 florin , reduce painful 
 necessity oblige borrow money , 
 far involved . regard 
 concert , Philharmonic Society 
 determine benefit , let 
 beg abandon generous 
 project , deduct gross receipt 
 concert , 1000 florin present 
 advance . Society kindly allow 
 surplus , undertake prove deep 
 gratitude , write new 
 symphony , sketch lie 
 desk , new overture , 
 Society wish . Heaven 
 soon restore health , | prove 
 noble - hearted English highly | appre- 
 ciate sympathy sad fate . shall 
 forget noble conduct , hope soon 
 send special letter thank Sir Smart 
 Herr Stumpff . farewell , sentiment 
 true friendship , remain , great 
 esteem , ~ 
 ' friend 

 LUDWIG VAN BEETHOVEN . 
 ' p.5.—my hearty greeting wife . 
 thank Philharmonic Society 
 new friend Herr Rau . pray 
 Philharmonic Society symphony mark 
 metronome tempo ; | enclose 

 un povo 

 maestoso 838 e 
 Molto vivace 16 - ge 
 Prest 116 | 
 Adagio molto e cantabile - 60= @ 
 Andante moderato 63- @ 
 Finale presto 96 - 5 . 
 Allegro , ma non troppi 88 = @ 
 Allegro assai 80 - 2 
 Alla marcia 84= @. 
 Andante maestoso 72 - 
 Adagio divoto 60 - 2 
 Allegro energico 84 - 0 . 
 Allegro , ma non tanto 120 - 5 
 Prestissimo 32 - 5 
 Maestoso 60 

 reference communication ' 
 splendid generosity Philharmonic Society ' 
 gift 4100 Society send 
 Moscheles great composer illness . 
 special general meeting purpose 
 unanimously resolve : ' sum 
 pound send , hand 
 Mr. Moscheles , confidential friend 
 Beethoven , apply comfort nece 
 sitie illness 

 388 musical ti 

 Sir Charles Stanford point new | 
 important . know wonderful tune 
 /7na / e symphony violon- | 
 cello double - bass , charm 
 repetition ( bar 25 ) viola violoncello 
 unison upper octave , captivating 
 counter - subject bassoon . accord tothe 
 score second bassoon silent , Beethoven 
 indicate abbreviation sign autograph 
 preserve Royal Library , Berlin ) 
 play octave double - bass , 
 fill wide gap 
 violoncello . Gevaert writer Instru- 
 mentation draw attention peculiar scoring 
 Beethoven , leave double - bass 
 octave covering , error need 
 correction text - book author 
 work dark regard feature great 
 masterpiece 

 Herr Otto Fiebach Kénigsberg bold 
 believe possible return 
 licentious extravagance modern music 
 style strict regard rule 
 form , harmony counterpoint shall ally 
 ' plastic ' melody ' modern ' harmony mean 
 expression generally . hope inaugurate 
 ' musical renaissance . ' 
 good composer believe act , 
 musical renaissance new idea ; prove 
 faith idea Herr Fiebach write 
 style Madrigalhymnus ' Die neun Musen ' ( 
 muse ) , produce 3 con- 
 siderable success . outcome 
 experiment , doubt 
 desire end present - day licentious- 
 ness extravagance express 
 frequently german musical press . 
 return sanity , order simplicity possible ? 
 fain hope impossible , musical | 
 history successful precedent 
 course . wild musical extrava- 
 gance commit eye ( ear ) 
 cause turn away disgust , 
 turn wheel progress , 
 greatly desire hold , shall 
 content confess helpless 
 face fact , hear pioneer ' progress ' 
 exclaim Galileo : ' e pur si muove ' ( 

 October month accomplish 

 surprising feat train new choir 350 voice till al 
 letter perfect ' apostle , , ' dream 
 Gerontius , ' Brahms ' german Requiem , ' Bach ' Actus 
 Tragicus ' choral portion Beethoven ninth 
 Symphony . \ supplementary choir 1,000 child 

 e public school train sing Benoit cantata 
 little people acquit 

 festival concert number , contain 
 choral work evening Tuesday , Wednesday , 
 Friday Saturday , afternoon 
 Thursday Saturday , scheme 
 orchestral piece solo principal singer . 
 chief popular point view Madame 
 Gadski , sing hackneyed air Weber ' Ocean , 
 thou mighty monster , ' letter rondo ' Don 
 Giovanni , ' wagnerian battle - horse final 
 scene ' Gétterdimmerung ' ' Tristan und Isolde ' 
 entrance air Elizabeth ' Tannhauser . " 
 english singer solo oratorio — 
 John Coates D. Ffrangcon - Davies . tastefulness 
 intelligence singing help materially 
 disclose heart Sir Edward music . Madame Homer , 
 Metropolitan Opera Company New York , 
 engage sing contralto oratorio , 
 health shatter awful experience t 
 San Francisco catastrophe , burden fall Miss 
 Janet Spencer , New York , acquit rare 
 efficiency work . 1 

 soprano oratorio 
 ninth symphony Mrs. Rider- Kelsey , young 
 singer fine voice equally fine artistic instinct 
 open wide field usefulness . remain 
 solo singer Charles W. Clark Herbert Wither 
 spoor orchestral list Beethoven 
 overture ' Leonore ' . 3 _ ninth Symphony 
 Schumann Symphony B flat ; Elgar ' Sout ! 
 ' introduction allegro ' ; Liszt ' Les Preludes ' ; 
 Loettler ' Mort de Tintagiles ' ; Strauss Serenade wind 
 instrument ; Dvorak * Husitzka ' overture ; Bach 
 Brandenburg Concerto grosso ; festival prologue entitle 
 ' Pax Triumphans , choir school child 
 introduce thrilling caus firmus shape 
 german chorale ' Nun danket alle Gott ' ; Tchaikovsky 
 ' pathetic ' symphony ; love scene Richard Strauss 
 * Feuersnot ' ; prelude ' Die Meistersinger 

 impossible rate value Mr. Van der Stucken 
 achievement connection festival high . 
 train choir , festival orchestra 

 H. E. KRenBII 

 Malta Musical Union concert Lamplo 
 Hall , Floriana , 17 , programme inclu 
 Barnby ' Rebekah , ' Beethoven fourth Symphony 
 ( srieg ' Peer Gynt " suite . Society active 

 season , having concert November , include 

 MUSICAL T 

 402 IMES.—JvuneE 1 , 1906 . 
 performance ' DREAM Ol SCHUMANN MEMORIAL FESTIVA 
 GERONTIUS ' FRANCE . \T BONN . 
 ( special correspondent . ) ( special correspondent . ) 
 Paris , 25 . : 
 afternoon performance Elgar jonn , 24 , 
 " Dr um Gerontius " France . event place ii year Robert Schumann die , — _ 
 nder th — s Société des Grandes Auditions | year bodily mental suffering , Endenich , ne nn . 
 m oe France , whic ! : La ( Gretiulhe | story illness premature death sad , 
 ellent . nte > . cale performance | pleasant dwell . fest hold 
 x splendid Salle Palais du Trocadéro . — sing | Beethoven - Halle , 22 - 24 , te remem . 
 » french translation M. d’Offoél , title | brance master fine work ; admiration 
 ) Gerontius , ' interpretation work | composer overshadow time rn 
 detail . orchestra chorus — | man 

 Yoncerts Lamoureux — 

 function music secondary place . address 
 deliver Dr. Joseph Joachim grave 
 friend Robert Clara Schumann , great violinist 
 speak Schumann nobility character 

 modesty notable instance . : 
 Schumann Clara occasion visit 
 Hanover , hope performance musi 
 | pleasure . play quartet master , 
 naturally select thing 
 favourite work , Beethoven ( ) uartet F minor . 
 , having play , place desk 
 Schumann noble quartet , 
 hand true - hearted manner , characteristic 
 | beautiful expression wonderfully soft eye , 
 | ' , hear 

 programme concert contain y 
 | work . Symphony E flat , 
 |}the direction Dr. Joachim , , concert 
 | London notwithstanding , conduct marked vigour 
 close enthusiastically applaud . hall 
 | fill ; seat concert fact sell 
 | day commencement festival . second 
 programme devote ' Scenes 
 Goethe Faust . " * Gretchen music sing 
 Fraiilein Anna Cappel . soloist 
 | deserve special mention , viz . , Dr. Felix von Kraus , 
 delivery Mephistopheles music dramatic 
 withal cynical . orchestra Philharmonic 
 | Berlin , excellent choir Bonn Gesangverein . 
 second day programme open 
 exhilarate Symphony B flat , spirited perform- 
 } ance Prof. Griiters , municipal music 
 | director city Bonn . hear delicate 
 | * Mignon ’ Requiem music , unequal Neujahrslied 
 soli , chorus orchestra ; choir 
 hear advantage . Herr von Dohnanyi achieve great 
 legitimate success playing Schumann noble 
 | Pianoforte concerto . able horn - payer aris 
 | play Concertstiick horn orchestra , 
 |a work — ' Romanze ' section — 
 | composer inspiration low ebb . 
 | player , account difficulty soli , deserve 
 mention : MM . J. Penable , E. Vuillermoz , J. Capdevielle 
 A. Delgrange . grand overture , ' Genoveva ' 
 ' Manfred , ' perform , Dr. Joachim 
 | Prof. Griiters 

 elementary school violin class . school class combine 

 t erform cantata ' Vogelweid ' ( Kathbone ) , ( 2 3 . ) 
 Lady Mary Forbes - Trefusis . Stourport ( Mr. G. Jackson ) festival second year ; Miss Christian Egerton 
 tu ccessful choir class large choral society , | chief promoter . school choir enter , 
 Croome ( Lady Dorothy Coventry ) Powyke| - entry adult choral class , 
 ( Mr. G. Street Chignell ) tie banner village | village orchestral class . Dr. Somervell adjudica 
 choir Dr. Percy Buck ute , conduct a| evening concert second day , Mendel 
 | performance Spohr ' God , thou art great ' | sohn second Symphony perform , direction 
 mbine choir orchestra . choir , under| Mr. Ivimey , Miss Egerton conduct good 
 Lady Mary Forbes - Trefusis , sing Filson young’s| performance Mendelssohn 42nd psalm . 
 n harmony harmony . " soloist 
 Sefior Alvar Miss Anne Malvern , Miss Amy Newton WENSLEYDALE Tout MENT LEYBURN , \ 
 Kev . A. L. G. Griffiths , Mr. G. Street Chignell ( 2 3 . ) 
 wa | rt ist . -m ™ . : ; . 
 ' tournament music ' institute year 
 \ ago late Hon . A. Lucien Orde - Powlett . 
 , exercise considerable influence district 
 ( Apr 1 2 . ) scope . lure day 112 entry 
 ul necessitate t devotion | vocal instrumental class . child day 
 thr t work . instrumental performance | prove interesting . culminat perform- 
 ! w strong al nce Rathbone cantata * Orpheus ’ con ed 
 present oc ion | choir direction Mr. T. Tertius N , 
 day stringed instrument . Mr. Alfred | adjudicator competition . senior 
 g n vv t ( string quartet party play | second day bring small adult choir 
 rs nd movement Schubert ( juartet in| quartet party . class enumerate 
 \ mi ( Mr. Horwood party | schedule , ( violin violoncello ) 
 iccessful . entry Beethoven ] entry . proceeding conclude concert 
 Pianoforte trio ( Op . 1 ) , party ably lead Miss } ' revenge ' ( Stanford ) perform 
 Willoughby come . advanced choir 

 
 success . 
 Elwes , 
 Lady W 
 came , al 
 entries 1 
 choir se 
 suc ! 
 Dr. Mc } 
 great int 
 sing @ S 
 Dukes , 
 beautifu 
 Miss Lu 

 imes s 23 

 programme Schumann ' Manfred ' overture , 
 Elgar ' introduction Allegro string , ' 
 Beethoven - welcome C minor symphony 

 distinction mark concert 17 
 appearance Bradford Festival Choral Society , 

 distinguish , fully uphold splendid 
 tradition Yorkshire chorus - singing , Bach Motet 
 double chorus ' sing ye Lord , ' choral 
 portion Beethoven ninth Symphony , close 
 concert nearly hour — long 
 evening music open Hermann Goetz ' 
 frithling ' overture — music neglect ! — 
 include Beethoven Pianoforte concerto G , 
 solo beautifully play Mr. Richard Buhlig . 
 Dr. Frederic Cowen conduct concert 
 usual ( Jueen Hall . information concern 
 previous performance Philharmonic Society 
 Beethoven Choral symphony find page 387 

 410 

 ee 
 LONDON SYMPHONY ORCHESTRA . 
 concert 5 potentiality Tcha kovsky 
 ' pathetic ' symphony overpoweringly manifest ' 

 neurotic forcefulness hand — literally 
 hand , conduct baton — c /Mposer 
 fellow - countryman , M. Wassili Safonoff . forcefyl 
 russian musician successful secure true 
 fairy - like daintiness Mendelssohn ' Midsummer Night 
 dream ' overture , fail sufficiently poetise Beethoven 
 eighth Symphony , substantiate claim 
 rank masterful orchestral conductor , 
 19 Dr. Frederic Cowen occupy conductor 
 | desk , secure excellent rendering good 
 symphony ( ' Scandinavian ' ) , play pianoforte 
 ( substitute harmonica ) Adagio Rondo 
 Mozart harmonica , flute , oboe , clarinet , viola 
 violoncello . Schumann ' Genoveva ' overture , Tchaikovsky 
 Suite G ( . 3 ) , german dance C ( string ) 
 Schubert complete interesting enjoyable . 
 noon music . concert Queen Hall 

 pleasant recent music - making 

 Queen Hall April 25 12 , notwith- 
 stand fact large building unsuited 
 ideal interpretation chamber music . 
 occasion programme consist Brahms B minor 
 quintet ( Op . 115 ) clarinet ( superbly play Prof. 
 Richard Miihlfeld , Brahms write ) 
 string ; Mendelssohn rarely hear Octet ( string ) ; 
 delightful Octet serenade e flat Mozart 
 ( Kochel . 375 ) , pair wind instrument — oboe , 
 clarinet , horn bassoon — charmingly play 
 Messrs. W. M. Malsch , E. Davies , Richard Miihlfeld , 
 M. Gomez , A. Borsdorf , H. Vandermeerschen , E. F. James 
 Wilfred James , fascinating little gem 
 conduct Dr. Joachim evident enjoyment 

 chamber - music classic furnish pleasure 
 audience second concert : Beethoven Quintet 
 E flat ( Op . 16 ) pianoforte wind instrument ; 
 Brahms Quintet B flat ( Op . 18 ) string ; 
 Schubert Octet string wind . player 
 stringed instrument 
 concert Dr. Joachim , Prof. Carl Halir , 
 Maurice Sons Mr. Thomas F. Morris ( violin ) ; 
 Mr. Alfred Gibson Mr. Alfred Hobday ( viola ) ; 
 Prof. Robert Hausmann Mr. Percy ( violon- 
 cello ): Mr. C. Winterbottom ( double - bass , Schubert 
 octet ) Mr. Donald Francis Tovey ( pianoforte ) . 
 regret record Prof. Emanuel Wirth , viola player 
 Joachim ( ) uartet , return Berlin 
 eye trouble ; , heretofore , Mr. Alfred Gibson 
 prove worthy substitute 

 DR . GRIEG LONDON 

 QUEEN HALL ORCHESTRA 

 chief feature Symphony Concert 
 |(Queen Hall Orchestra 5 performance 
 |the symphonic poem ' Don Quixote , ' conduct 
 | composer Dr. Richard Strauss . nature work 
 ji musician rejoice 
 grieve good seat , 
 influence music undoubted , opinion 
 | divide concern artistic value Dr. Strauss 
 orchestral work ' Don Quixote ' particular . 
 |rendere bear witness assiduous rehearsal , praise 
 instrumentalist , manifest zeal try 
 meet composer requirement . remainder 
 concert consist overture ' Tannhiauser , ' 
 Venusberg music , Beethoven eighth Symphony 
 excerpt Berlioz ' Faust , ' selection conduct 
 | Mr. Henry J. Wood 

 programme concert 10 
 exception devote entirely Wagner , exception 
 Schumann Pianoforte concerto , solo 
 entrust Mr. Harold Bauer , remarkably 
 virile significant reading romantic music 
 sympathetically support orchestra 
 Mr. Henry J. Wood direction 

 Coriolan 

 ver training 
 » verture 
 Beethoven 

 Hoeck 

 s deputy 
 ] \ nd 
 S | band 
 res 

 dy Mr. Walter 
 strated , piece 
 script com- 
 pa vent modern tendency deve nt 
 s \t concert Royal Artillery strit nd 
 inder Mr. Rk . G. Evans , February 15 , Miss Hel Sealy 
 highly artistic rendering Max Bruch G 
 Violin concerto , band play Beethoven ' eg nt ' 
 verture Mr. Frank Winterboitom 

 perfectly 

 concert season Gloucester Choral 
 Society usual arrange president , Mr. Joseph 
 Bennett , place Shire Hall 22 . 
 large number artist service include 
 Madame Fanny Moody , Miss Fanny Davies , Miss Perceval 
 Allen , Miss Marie Motto ( violin ) , Mr. Charles Manners , 
 Mr. J. Bardsley , Miss Gertrude Ess ( violoncello ) 
 Mr. Patrick Munro ( reciter 

 music MANCHESTER . 
 ( correspondent . ) 
 Gentlemen Concerts frequently play 
 open voluntary season , ' seldom closing 
 . season ; secure 
 subscriber rare pleasure welcome Dr. Joachim , 
 hear famous Quartet , final concert 
 postpone March 19 April 25 . greeting 
 accord great violinist warmly enthusiastic . 
 programme consist Mozart ( Quartet G 
 ( Kéchel 465 ) ; Schubert quartet D minor , . 6 
 ( ' death maiden ' theme ) ; Beethoven 

 Quartet e flat major ( Op . 74 ) . playing 
 Dr. Joachim colleague — Profs . Halir , Wirth , 
 Hausmann — distinguish remarkably sym 

 , condition user , transfer 
 municipality . form valuable addition 
 Henry Watson Music Library possession 
 corporation 

 Beethoven Society , amateur orchestra 
 eighty performer , conduct Mr. E. Gordon 
 Cockrell , second subscriber ’ Concerts 
 April 23 , bring eighteenth season successful 
 close capital performance Haydn Symphony 
 D ( . 2 Salomon set ) . novelty 
 programme movement , designate Nocturne , 
 Sibelius suite , ' King Christian II . " Miss Amy Dobson , 
 promising pupil Miss Olga Neruda Royal 
 Manchester College Music , play Saint - Saéns Piano- 
 forte concerto . 2 , G minor . Miss Jessie Young , 
 vocalist evening , student College . 
 season Brodsky ( ) uartet 

 concert result contribution £ 133 
 | College Students ’ Sustentation Fund , widow 

 arrange , include ' Pied Piper Hamelin 

 Parry ) , ' Landerkennung ' ( Grieg ) , Beethoven C minor 
 Symphony , ' fly , singe bird ' ( Elgar ) . masterly 

 setting ' Pied Piper Hamelin ' receive vigorous 
 intelligent interpretation orchestra chorus 
 inspiring conductorship c mposer 
 Sir Hubert Parry , soli admirably sing 

 programme eighty - Lower Rhenish Musical 
 Festival June 3 - 5 consist Bach B minor Mass 
 und orchestral Suite C ( day ) ; Schumann ' Manfred ' 
 verture , Brahms Violin concerto ( M. Marteau ) 
 Hlarzreise ’ rhapsody contralto solo ( Frl . Philippi ) 
 horus man voice , Liszt 13th Psalm ' Faust ' 
 Berlioz overture ' Benvenuto 
 * Leonore ' overture ( . 3 ) e flat 
 Katharine Goodson ) , Wagner 
 meistersinger ' Vorspiel , song tenor solo 
 iccompaniment , ' Traumnachi ' ' Sturm- 
 r chorus orchestra Weingartner ( day 

 Beethoven 

 wrchestral 

 Felix Weingartner bid farewell Berlin 
 Royal Orc hestra tenth Sy mphony concert 
 7 . programme include work french 

 yser , doubt compliment nation 
 receive ent y connection 
 Paris Beethoven - Berlioz festival 

 composition 
 ' L’aprés - midi d’un Faune ' Bruneau 

 Debussy 

 La belle au bois dormant . ' follow 
 Schubert ' unfinished ' symphony , 
 gre : Beethoven work , 1 Weingartner 

 mself great th occasion 
 solutely surpass , viz . , * Eroica , ' concert 
 indescribable enthusiasm . 
 idience vociferously 

 concert 

 7 
 Beethoven Hall . programme include new Symphony 

 db 4 4 
 C minor , Serenade D orchestra , song 

 BONN 

 Prof. Joseph Joachim honorary citizen 
 town , help yearly 
 Beethoven festival great success attend 
 recent year . artist worthy honour 
 find veteran master 
 intimately connect spread 
 Beethoven cult 

 BREMEN 

 LINZ 

 year ago town council little city 
 beautiful blue Danube , suggestion Herr August 
 Gollerich , vote sum money interest 
 - year devote biannual Anton 
 Bruckner Festival Concert popular price . Bruckner 
 organist Linz Cathedral 1855 1867 , 
 having bear , September 4 , 1824 , 
 Ausfelden , Archduchy Upper Austria , 
 Linz capital . , consequently , method 
 Linz town council madness : laudable desire 
 honour memory artist , suffer cruel 
 neglect lifetime , 
 appreciated Austria Germany , speak 
 great symphonist Beethoven . fifth 
 concert April 7 Linz Musikverein , 
 programme consist seventh Symphony , Te Deum 

 setting , - chorus trombone , 

 PARIS 

 great event musical season 
 Beethoven - Berlioz festival conduct Herr Felix Wein- 
 gartner . concert Chatelet Theatre 
 Grand Opéra . 3rd , 5th , 6th , 7th , sth 
 Choral Symphonies , Choral fantasia , G major 
 Pianoforte Violin concerto , ' Egmont , ' ' Coriolan ' 
 ' Leonore ' overture represent Beethoven ; 
 Fantastic Symphony , ' Carnaval Romain ' 
 ' Benvenuto Cellini ' overture ' Faust ' serve 
 illustrate genius Berlioz good . Lamoureux 
 orchestra , thesuperb chorus Amsterdam Oratorium 
 Vereeniging achieve wonder , especially excite 
 public Press enthusiastic praise . Weingartner 
 hero week receive tremendous ovation 
 concert ( Choral Symphony ) great festival , 
 end ' dans une gloire triomphale . ' 
 new opera recently produce mention 
 * Le Clown , ' M. Isaac de Camondo , wealthy amateur , 
 bear cost ' special ' performance 
 * Le roi aveugle ' 

 Nouveau Théatre April 205 

 AMBLESIDE.—The Ambleside Westmorland Competition 
 Choir annual concert Assembly Rooms 
 1 , Brahms rhapsody alto solo ( Miss Barnett ) , 
 male - voice choir orchestra , Cliffe ' ode 
 North - east wind ' perform . choir , especially 
 Cliffe work , exceptionally good ably assist 
 orchestra . Mr. Rauling conduct 

 BEDFORD.—The Musical Society excellent per- 
 formance Dr. Cowen ' John Gilpin ' ' Fairyland ' 
 orchestral suite , conductorship composer , 
 15 . item programme 
 ( conduct Dr. H. A. Harding ) Mozart ' Zauber- 
 fléte ' overture , Bach air C string ( suite 
 D major ) movement Beethoven fourth 
 symphony Luigini ' egyptian ballet . ' Miss Frederica 
 Richardson vocalist 

 BoGNoR.—The Musical Society perform Parry ' Pied 
 Piper Hamelin ' April 25 success , solo 
 undertake Mess Clifford Hunnybun 
 W. Coleman , vocalist Miss Edith Evans 
 Mr. Mark Gould . programme include 
 Stanford ' Corydon , arise * Elgar ' love dwell 
 northern land , ' unaccompanied choir ; 
 orchestra hear Smetana overture ' bartered 
 bride ' Auber overture ' Zanetta . ' Mr. F. J. W. 
 Crowe conduct 

 opera : Elegie serenade string , 2s . 6d . Potpourt 
 ' Casse Noisette ' suite , 4s . Walzer ' Eugene Onegin , ' 4s . 
 Walzer ' Dornroschen , ' 2s . 6d . Potpourri ' Pique 

 Dame , ' 4 . arrangement ' l’athetic 
 symphony pianoforte violin . ( 2 ) trace 
 Beethoven Mozart waltz arrange 
 instrument 

 O. M.—The remain follow musician 
 - interréd : Bach , Haydn , Beethoven , Weber , Schubert 
 Dragonetti ; Englishmen , Dr. Greene , John Davy 
 ( churchyard , St. Martin - - - Fields ) Sir 
 W. G. Cusins . com / f / ete list apply 
 Mr. Algernon Ashton : doubtless furnis ! 
 musician 
 bury sea — word , sleep sleep 

 

 SOLI 
 N 
 ARCADELT , J. — ' * ' Ave Maria ' 
 BARTLETT , H. N. — ' * Tue CHIEFTAIN . " 
 short cantata 2 
 * * mile Edinboro ’ 
 town ; fe 

 BEETHOVEN - SPICKER . — * hymn night " o 

 BREWER , J. H. — ' ' break , break , break ~ oo 
 99 * * night thousand eye " 0 

 SOEBEN ERSCHIEN 

 BEETHOVEN 
 NEUN SYMPHONIEN 
 GROVE 

 BEARBEITUN 

 Se ae aoe . 18 movement , string 
 ( ) li ) e n ( y l | s | | quartet ( Mozart ) . arrange Violin i1 
 ( position ) Pianoforte ( tl 
 t 2nd Violin ad /#4 . ) . . § to8 ae nw , qe 
 v lol n ml sic 19 . SONATA F MAJOR ( KOCHEL , . 376 ) n 
 ( Mozart ) . Violin ( position 

 Pianoforte .. 
 ALF red mof fat . . RONDO G MAJOR ( BEETHOVEN 

 Violin ( position ) Pianoforte 

