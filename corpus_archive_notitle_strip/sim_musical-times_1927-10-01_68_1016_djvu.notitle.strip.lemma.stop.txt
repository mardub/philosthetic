orchestral music evoke damaging 
 comparison regular orchestral 
 conductor 

 critic forget , , eminent 
 conductor limitation . 
 example , hand strong 
 suit russian music , hear performance 
 Beethoven Mozart far good 
 Franck Delius direct Dr. Percy 
 Hull Hereford . easy point 

 - know critic recently ) 
 b 

 unnecessary . consider 

 standing experience debate 
 Beethoven mark c € trio 
 Scherzo ninth symphony , 
 view accept extreme 

 caution , downright scepticism . more- 
 ' spirit ' apparent 

 point concern use gramophone 
 word . possible standardised 

 performance , advantage 
 regard immediate repetition entire 
 work certain salient passage . surface 
 noise , lapse purity tone 
 exact reproduction tone - colour , 
 disability . curious finding . , 
 set experiment , victim ask 
 express , mean figure , proportion 
 enjoyment derive rhythm , Melody , 
 | design , Harmony , tone - colour . 
 /composer include Handel , Bach , Beethoven , 
 Wagner , Liszt , Brahms , Debussy . 
 reader guess score heavily 
 ' ' Tone - Colour ' ? course : hear 
 reply , crowd , ' Wagner ! ' wrong : try ! 
 ' Liszt ? ' wrong : shot ! 
 * Debussy . " : poll head Handel ! 
 Wagner score heavily rhythm ( 347 ) , Melody 
 |(674 —— year ago ' write 
 chune ' ! ) , design ( 295 ) , Harmony ( 400 ) . 
 Tone - Colour manage mere 217 . 
 clearly gramophone , marvellous , 
 | 800d think reproduction 
 instrumental timbre . prove 
 lfact ' Tone - colour figure 
 |comparatively low . ( course train 
 | musician good defect mentally , 
 subject experiment probably 
 able 

 Handel total Tone - colour ( 235 ) raise 
 interesting point . piece 
 represent " 
 | Messiah . " ( , way , expose 
 |weakness experiment . basis 
 |comparison selection 
 18th - century oratorio series extract 
 |from Wagner ? ) expect 
 | Handel receive large figure 
 | tone - colour ( possibly ) harmony . 
 low compare composer 

 table worth quote 

 
 | com poser Rhythm Melody Design . harmony . hn 
 | Handel 119 144 137 65 235 
 | Bach 4460 730 727 352 179 
 | Haydn 211 217 ist 61 73 
 Mozart 135 203 153 5u 76 
 | Beethoven 492 522 338 332 1go 
 | Schumann 827 839 227 350 74 
 | Chopin 382 406 220 234 114 
 | Mendelssohn 134 202 78 116 40 
 Wagner 347 674 205 400 217 
 | Liszt 133 173 50 157 q2 
 | Brahms 212 192 124 140 37 
 | MacDowell 135 162 34 152 34 
 Debussy O4 100 22 12 2 
 \‘it % improbable yocal tone - colour 

 attract instrumental tone - colour , ' 
 book , attempt explain curious fact 
 Handel reach high total column 
 look find small . 
 probable : certain — event 
 far sophisticated hearer concern . 
 respond vocal music start — 
 partly , course , word 
 association . instrumental tone - colour rarely 

 audience consist group 
 size - , job note 
 describe change attitude 
 music repetition . work 
 grade : ' severely classical ; popular 
 classical ; easy popular classical ; popular . ' 
 division fast slow 

 difficulty classify music way 
 fact * severely classical ' 
 work consist ( fast ) movement 
 * unfinished , ' Fugue Beethoven 
 C major String Quartet ; ( slow ) Mozart 
 Andante Wagner ' Traiume . ' 
 ' severe , ' Fugue attrac- 
 tion energy , pace , great rhythmic vitality . 
 fact , practically piece mention 

 category classical interchangeable . 
 come section head 
 * popular ' safe fox - trot 
 waltz . face , clear 
 hastily decide comparative 
 attractiveness ' severely ' classical , 
 _ ' ' easy ' popular classical . 
 severe ' way merely ° ' 
 hearing , * easy ' good 
 

 place conception tone . overwhelming 

 fact ear hear thing , hear thing 
 | , . sound crop 
 | - day unknown year ago 
 | ( produce machinery , x&c . ) ! sound 
 | induce new reflex affect attitude 
 music . tone elastic 
 tensile . conception tone similar 
 prevail Middle Ages . 
 ' reality ' tone Middle Ages know : 
 dream - conception . 
 great similarity Middle Ages 
 - day lie fact music Middle Ages 
 bass , music young 
 ' advanced ' composer long 
 bass . attract early 
 music , music , purely 
 ' linear ' lacking tonal definition , 
 certainly characterise positive conception tone 
 long achieve . change 
 | sense hearing originate , paradoxically , 
 Beethoven deafness . ' dream tone ' late 
 Quartets Sonatas Ninth Symphony 
 influence Wagner Beethoven effort 
 new architecture . Beethoven grope 
 | attempt create , basis new tone - concep- 
 | tion , architecture unite definition form 
 | boundless scope imagination 
 | Schénberg impulse write great chamber 
 | work . Beethoven longing great compass 
 } instrument likewise symptomatic . ( intro 

 duction new instrument , decay clavichord 

 organ , complete ' atonalisation ' ear . ) 
 | Beethoven conception tone , end , 
 purely ideal ; account fact 

 later work 
 ' come 

 MUSICAL ‘ TIMES — Octroser 1 1927 

 896 
 Gramopbone Wotes Tchaikovsky D major Quartet . playing 
 safe ee ata recording alike hardly better ( 9203 ) . 
 ' Discus pianoforte record Evlyn Howard- 
 — Jones Brahms Capriccio B minor 
 COLUMBIA un - scottish Ecossaise Beethoven : crisp 
 month output contain - rate | playing , Brahms model 

 orchestral record . ' Venusberg Music , 
 play Philharmonic Orchestra , conduct 
 Bruno Walter , leave desire 
 matter vividness , , fact , good 
 recent recording feat ( l1982 - 83 

 Voluntary ' ( arrange Sir Henry Wood ) 
 Walford Davies ' solemn Melody . ' 
 organ , play Harold Dawber , use great 
 effect . Alexander Harris trumpet soloist 
 ' Voluntary , ' the’cello solo ' Solemn melody ' 
 play Clyde Twelvetrees . 
 Hallé , conduct Sir Hamilton Harty . 
 constituent — orchestra , soloist , organ — 
 blend , balance , reproduce unusual 
 success . organ , way , sound 
 like organ ; ( Purcell piece especially ) 
 result glorious welter sound ( l1986 

 orchestral work bear transference 
 military band , ' Fidelio ' Overture suffer 
 . playing La Garde Républicaine 
 Band course expect medium , 
 want Beethoven ( 9208 

 cinema organ record average merit 
 Quentin Maclean play , fine instru- 
 ment Shepherd Bush Pavilion , series 
 extract ' Classica , ' arrange Montague 
 Ewing . title flattering 
 material , range Toreador Song 
 Handel Largo . Mr. Maclean playing first- 
 rate ( 9225 

 old pupil home abroad ; musician 
 general ; desire recognise valu- 
 able public service . warmly commend Fund 
 reader ’ notice . hon , secretary 
 treasurer Mr. H. Dover , 14 , Hinderwell Street , 
 Hull 

 Sunday Evening Concert Society , 
 eighth year , announce series chamber concert 
 great interest . October 2 performer 
 Kutcher Quartet , Harriet Cohen , John Goss 
 ( Franck Quintet , String Quartets Beethoven 
 Dvorak ) ; gth , Pianoforte Violin 
 sonata Mozart Brahms , violin 
 pianoforte solo Bach Brahms , fare , 
 player William Primrose Soloman . 
 Isobel Maclaren sing song Brahms . 
 Wood Smith Quartet play quartet Mozart , 
 16th ; sextet Brahms Schénberg 
 . specimen programme serve 
 whet appetite chamber music enthusiast . 
 concert place Working Men 
 College , Crowndale Road , N.W. programme 
 particular hon . secretary , 
 Miss Mabel Fletcher , 26 , Uplands Road , n.8 

 correspondence column lately | 
 need regulation use initial | 
 reader 

 charming town Salzburg , partly | 48@!n action play great effect 

 way austrian Tyrol , especially since| 8 p.m. day 
 wish hear much- | Cathedral hear Mass D. choir 
 advertise musical Festival , | 8004 : soloist , tenor 
 August . able hear incline portamento , bass swamp 
 ' Everyman ' ; Beethoven Mass D , ' Fidelio ' ; | occasionally orchestra — 
 Mozart ' Don Juan ' ; ' Midsummer night’s| Beethoven fault . fact , Joseph Messner , 
 dream , ' Mendelssohn music , | conductor , hardly realise acoustic lofty 
 orchestral concert . programme | spacious Cathedral . Vienna Philharmonic 
 include Haydn - know Symphony | Orchestra obviously good , playing 
 D , Beethoven fifth Symphony , work | 2 t sound , owe erratic /empi 
 Mozart — March D , Serenade , D , a| conductor . Gloria fast move- 
 work hear -before , Concerto | ™ ent fast echo place ; 
 solo instrument orchestra . Austria | ‘ result muddle . hand , 
 evidently musical good . Vienna | slow movement drag . 
 contribute Philharmonic Orchestra Mass | " ¢ver hear Kyrie slowly . rhythm 
 orchestral ’ concert , Opera | WS lose time , owe undue length- 
 orchestra opera . choir Mass , | " ing note . illustration 

 solo.singer , come Vienna 

 , Hans Moser , 
 overdone . com- 
 pletely misunderstand wonderful line 
 Shakespeare , rate , 
 wake sleep end scene , 
 act 4 , try recall dream . Herr Moser 
 comic ; surely touch 
 wistful tragedy amazing , halting word 
 disillusioned village clown deliver , 
 fairy vanish , leave stage ! 
 Lysander , Demetrius , Helena , Hermia , 
 audience laugh great deal : , , 
 badly - act . fact , trouble 
 watch clever production Reinhardt , 
 german word , surely Shakespeare . 
 ' Midsummer Night Dream ' 
 know ? music 
 Shakespeare verse ? spirit poetry 
 play largely 
 . brilliant production , mot Shakespeare . 
 Mendelssohn music , , sound pleasant 
 

 Sunday orchestral concert 
 Mozarteum , 11 a.m. Franz Schalk conduct 
 programme orchestral work , 
 - know — Haydn Symphony D Beethoven 
 fifth . middle work , Mozart , describe 
 Konzertantes Quartet ( K. v. Anh . 9 ) , solo 
 instrument orchestra . work 
 hautboy , clarinet , bassoon , horn soloist play 
 magnificently . hardly know admire 
 performer genius composer . 
 player deal passage 
 utmost Virtuosity , apparently ease ; 
 virtuosity effective , 
 musical . learn 
 capacity instrument Mozart 
 time . Schalk conduct Haydn Symphony per- 
 functorily , clear beat : 
 cause smudge playing , especially 
 beginning end phrase — unneces- 
 sary fine orchestra . Beethoven 
 Symphony , , come , conductor 
 wake ; , think , 
 passage needlessly underline , pace 
 Scherzo slow , performance 
 thoughtful impressive 

 musical event Festival 
 Mozart ' Don Juan , ' Stadttheater , 
 good production , accompany orchestra 
 Schalk . performer stage 
 unequal . outstanding figure Leporello 
 ( R. Mayr ) , Zerlina ( Adele Kern ) , Don Juan ( Hans 
 Duhan ) . Adela Kern good ' Don Juan ' 
 ' Fidelio . ' charming voice , 
 vivacious actress . Richard Mayr 
 versatility equally home effective 
 different Rocco Leporello . Hans 
 Duhan large voice , possess 
 fine , tall , athletic figure , look act 
 . rest principal disappoint . 
 Elvira Anna elaborate coloratura 
 sound difficult laboured . Claire Born 
 Maria Nemeth voice heavy 
 cumbrous Mozart opera . Alfred Piccaver 
 ( clearly favourite audience ) 
 big , inexpressive voice , acting Ottavio 
 wooden . true Ottavio 
 cut fine figure opera ; , , 
 greatly impress actor 
 singer ' Fidelio . ' chorus sing act 

 arrangement new term include number 
 student ’ concert lecture . begin week 
 inaugural address Rev. W. J. Foxell , chaplain 
 College , subject ' music beauty , ' 
 end choir chamber music concert Grotrian 
 Hall orchestral concert Queen Hall . large 
 number distribution diploma certificate 
 local centre gazette , , College 
 represent Dr. J. C. Bridge , chairman Board , 
 Dr. E. F. Horner , director examination , 
 secretary , Mr. C. N. H. Rodwell 

 CHORAL SOCIETY PROG 
 list 
 LONDON 
 RoyAL CHoraL Society ( Mr. H , L. Balfour visit 
 conductors).—October 22 , ' Elijah ' ; November 19 , 
 Brahms ' Requiem ' ' hymn Jesus ' ; 
 December 17 , Carols ; January 7 , ' Messiah ' ; 
 February 4 , ' Hiawatha ' ; March 3 , ' Dream 
 Gerontius ' ; April 6 , ' Messiah ' ; April 28 , 
 Beethoven Mass D. 
 PHILHARMONIC CHorR ( Mr. C. Kennedy Scott ) . 
 December 14 , ' Antiphon ' ( Vaughan Williams ) ; 
 ' Psalmus Hungaricus ' ( Kodaly ) ; ' Sleepless dream ' 
 ' hey , nonnyno ' ( Smyth ) ; ' requiem ' ( Brahms ) ; 
 16 , ' mass life ' ( Delius 

 RAMMES 

 PROVINCIAL 

 ABERDEEN CHORAL ORCHESTRAL sociery ( Mr. 
 Willan Swainson).—Beethoven mass D } ' sing 
 ye Lord 

 BatH CHORAL ORCHESTRAL socigery ( Mr. H. T. 
 Sims ) . — ' Tale Old Japan ' ; ' Elijah ' ; madrigal 

 Frome CHORAL Sociery ( Mr. A. M. Porter ) . — ' 
 Messiah 

 GLASGOW CHORAL ORCHESTRAL UNION ( Mr. 
 Wilfred Senior).—Beethoven Mass D ; ' 
 Messiah ' ; ' Dream Gerontius 

 Grays ( Essex ) DisTricr choral MUSICAL 
 Society ( Mr. W. H. Fraser).—‘St . Paul ' ; ' 

 GvuILpFORD CHORAL Society ( Mr. 
 ' Everyman ' ( Walford Davies 

 HaALirFAxX CuHoRAL Society ( Dr. A. C. Tysoe ) . — 
 Beethoven Mass C ; Bach Magnificat ; ' 
 Messiah ' ; ' Semele 

 HASTINGS MADRIGAL Society ( Mr. Reginald E. Groves ) . — 
 * Hiawatha departure ' ; ' Elijah ' ; ' Messiah 

 LIVERPOOL PHILHARMONIC Society ( Sir Henry Wood ) . 
 — ' Sea drift ' ( Delius ) ; ' thing man ' 
 ( Vycpalek ) ( performance England 

 LiverPooL WeLtsH CHORAL UNION ( Dr. Hopkin Evans ) . 
 — ' Blessed Damozel ' ( Debussy ) ; ' Dream 
 Gerontius ' ; ' Messiah ' ; Beethoven Mass D 

 LONDONDERRY PHILHARMONIC Society ( Mr. Henry 
 Franklin ) . — ' Hiawatha Wedding - Feast ' ; Holst 
 * Psalm 148 

 QUEEN HALL PROMENADE concert 

 Beethoven concert season , 
 August 19 , Symphony . 7 . Mr. Leslie England 
 play E flat Pianoforte Concerto spirit , 
 accuracy , sense measure . Mr. Steuart Wilson 
 sing song - cycle , ' absent sweetheart ' ( new 
 translation ) , thoughtful expression . 
 Miss Lilian Stiles - Allen sing Clarchen song 
 ' Egmont . ' incidental lyric 
 satisfactory concert song , Miss Stiles - Allen 
 sheer quality tone good soprano 

 Arnold Bax Symphonic Variations ( August 20 ) 
 exceptionai fare Saturday night audience , rich 
 somewhat confusing . composer 
 deny expression fancy come 
 inventive head . Variations charming 
 excursion , beauty lead . 
 grant ungrateful feel 
 inclined complain lack general direction 
 work , time cloyed heavy pro- 
 fuseness . Miss Harriet Cohen play exacting piano- 
 forte solo , think , newly increase power 
 grip 

 Elgar flat Symphony distinguish programme 
 September 8 . performance good clear- 
 cut way . miss wayward feeling , unexpected 
 happy thought , poetry peculiar 
 composer conducting music . new Queen 
 Hall orchestral Fantasy , ' song Gael , ' 
 Mr. B. Walton O’Donnell , conduct . heavy 
 unfeeling , businesslike hand grasp number 
 old irish tune Cockney nosegay 

 Beethoven concert September 9 , Mr. Maurice 
 Cole play c minor Pianoforte Concerto neatly . Mr. 
 Arthur Fear , promising young bass , sing ' hear , ye 
 wind wave . ' good thing singer 
 learn dispense sheet scrap paper 
 platform . Miss Dorothy Helmrich 
 good level good . 
 ' creation hymn ' hear different 
 voice 

 set orchestral variation intermezzo , Scherzo , 
 finale conduct composer , Mr. Victor 
 Hely - Hutchinson , September 10 . theme 
 rustic flavour appreciate day 
 R.C.M. composition attractive 
 commonplace . languish time . moment 
 hushed intensity prolong emptiness . 
 regard proof real talent , genuine 
 musical impulse , delicacy mind . Dobnanyi 
 familiar popular ' nursery song ' variation 
 play night , Mr. Harold Williams 
 sing 

 proportion british work scheme 
 concert , , ridiculously small — Elgar ( ' 
 Kingdom , ' ' Enigma ' variation , String introduction 
 Allegro ) , Bryson ( second Symphony ) , Delius ( ' Paris ' ) , 
 Harrison ( ' prelude music ' ) . conductor 
 formidable difficulty draft programme find 
 small work , , minute ’ duration , 
 lighten steady succession large - scale item . 
 work draw continental source past season , 
 Sir Hamilton Harty successful 
 man solve problem . young britisher 
 deserve severe critical trouncing 
 serve thing endure 
 season . british score quality 
 reject merely reading , 
 playing . seriously argue 
 school find thing ' worth 

 standard ' classical ' symphony 
 interesting find Harty include Sibelius . 2 . 
 Brahms Strauss , , Berlioz , provide Harty 
 main enthusiasm , represent , 
 congratulate ' Heldenleben ' ' Don Quixote , ' 
 season . year choral course 
 monumental Masses match fullowe 
 work : ' Israel Egypt ' ; ' Romeo Juliet ' ( Berlioz ) ; 
 * Fidelio ' ; ' Kingdom ' ; Alto Rhapsody Brahms 
 Choral Symphony Beethoven ; ' Meister- 
 singer ' Finale season wind - ( March 15 ) . 
 chief instrumental soloist engage Backhaus , Cortot , 
 Godowsky , Pouishnov , Catterall , d’Aranyi , Sammons , 
 Suggia , Cassado 

 unusually choral season outline 
 programme Hallé series augment 
 numerous concert Mr. Brand Lane conductorship , 
 culminate complete performance ' Hiawatha ' 
 trilogy March . zxgi , Manchester 
 welcome visit orchestra — Berlin Philharmonic , 
 Furtwingler , London Symphony , 
 Beecham , specially select Waltz Orchestra 
 Johann Strauss . public usually find way 
 Hallé Band week week use chance , 
 capital opportunity test relative 
 orchestral value sustain opinion 
 Manchester player ' measure ' 
 famous orchestra . occasion past Hallé 
 habitué inconspicuous Brand Lane 
 audience , Strauss come . 
 Brand Lane prospectus promise ' star ' vary 
 magnitude , reputedly international fame . 
 ’ Appen th ’ lancashire tenor , Tommy Burke , sing 
 yed ! ( pardon lapse native doric 

 Bach popular England , Mischa 
 Elman play variation second Sonata 
 good effect obtain big popular success . 
 item Mendelssohn Concerto Lalo Symphonie 
 Espagnole 

 failure season music 
 compose arrange Arthur Honegger film 
 ' Napoleon . ' contain good idea , 
 fault average ' fit ' music , depend 
 borrowing Honegger previous music 
 ' Marseillaise . ' autumn : season Amsterdam 
 begin Sunday , September i1 , popular concert 
 conduct Cornelis Dopper , programme 
 include Beethoven fifth Symphony , Elgar ' Cockaigne ' 
 Overture , ' Adagio con Variazioni ' dutch 
 composer Theo van der Bijl 

 HERBERT ANTCLIFFE 

 scrutinise hearer seven year ’ perennial repetition , 
 gladden box - office man Festival 
 Society . compare , muted sarcasm , 
 enormous mean place disposal Reinhardt 
 , modest pain expend 
 production ' Don Juan ' ' Marriage 
 Figaro . " singer ( Richard Mayr , Hans Duhan , 
 Alfred Piccaver , Adele Kern , Claire Born , Maria 
 Németh ) , conductor ( Franz Schalk 
 Robert Heger , respectively ) , Vienna Opera , 
 imply high musical standard performance . 
 scenery ! shabby , old fashioned costume ! 
 stage management , absence ! festival 
 performance ? 

 Vienna Staatsoper , Salzburg 
 Festival spirit , achieve , conclusively 
 production ' Fidelio . " achievement 
 difficult speak succumb ' pathetic 
 fallacy . " sum , superb performance 
 Vienna Opera decade , 
 great Beethoven step - child opera 
 privilege receive . marvellous orchestra 
 evoke , ' Leonore ' overture , ovation seldom 
 witness , audience rise man 
 shout thank ; scenic setting — Clemens Holzmeister 
 — catch spirit scene innermost 
 subtlety project grip audience ; 
 stage management ( work Lothar Wallerstein ) 
 reflect virtually rhythmic dynamic shading 
 score create perfection ensemble 
 writer witness , divest 
 ominous Prisoners ’ Scene Miannergesangverein 
 atmosphere inseparable ; cast 
 singer , hardly exception , perfect 
 musical , histrionic , emotional , 
 brave pitfall speak dialogue , 
 usually tempt pathos utter passiveness 
 . Leonore , Lotte Lehmann truly find 
 dramatic soul ; touching 
 noble simplicity flawless command 
 intricate vocal difficulty . notwithstanding 
 masterly portrayal , notwithstanding Alfred Jerger sinister 
 Pizarro , great individual triumph evening 
 Richard Mayr role Rocco , aged 
 jail - keeper . great , mature artist wonderful 
 tone pose . touching warmth voice , 
 word reflect pure , clarified 
 humanity ; big heart , great mind , strong , 
 compelling human force . singer evening 
 verily surpass , work enhance scenic 
 setting unrivalled beauty , lighting grouping 
 unfathomed suggestiveness , musical guidance 
 Franz Schalk , great Beethoven conductor . 
 memorable evening , perfection suffice lend 
 lustre rest Festival 

 time year Festival programme 
 seek include choregraphic art radius 
 action . achieve compromise , dance 
 programme Festival comprise , 
 wish , modern ballet de Falla 
 * - Cornered Hat ' Barték ' wonderful Mandarine . ' 
 modest dance evening suffice provide saltatory 
 portion Festival , noteworthy 
 presence Harald Kreutzberg . young dancer , 
 actually merit sobriquet bestow 
 ' German Nijinsky , ' surely dancer Europe , 
 outside Diaghilev troupe , divest choregraphic 
 art odious air dilettantism ambitious 
 semi - professional recently surround . slender 
 young man — light air — infallible technique , 
 subtle grace season welcome virility , 
 power facial expression stamp 
 great mimic actor . * music - dance ' 
 series strong mimic impersonation — E. Th . , 
 Hoffmann Edgar Allan Poe set dance . c major 
 scale couple tantalizing , monotonous ticking 
 merciless metronome , stage , accompany 
 dance — haunt , slur step scene ; 
 polytonal , invisible gramophone , 
 virtuoso dancing , hand , electrifying 

 reply post 

 C. E. M.—We know work deal specially 
 keyboard writing ; book composition ( 
 harmony ) usually contain section treat . surely 
 subject study page 
 great composer ’ pianoforte music . begin , 
 , slow movement Beethoven sonata G 
 ( Op . 14 , . 2 ) ; write simple tune line , 
 add variation lay plan movement . 
 Beethoven model advanced pianoforte writing , 
 Sonatas contain simple movement 
 base study . write song - accompani . 
 ment , study collection Irish Folk - Songs Stanford 
 Charles Wood ( Boosey ) , devise accompaniment 
 similar line folk - tune . 
 perfect example simplicity effectiveness , 
 Stanford accompaniment ' National Song Book ' 
 ( Boosey ) . Prof. Kitson recently publish ' Elementary 
 harmony ' ( Oxford University Press ) contain chapter 
 accompaniment writing write piano- 
 forte variation 

 H. W. J.—(1 . ) certainly spare - time pianoforte - tuning 
 repairing ' degrade music - teacher , ' 
 teacher fill day lesson , ability 
 good job tuning , ? 
 ask practice general . think , 
 teacher ear skill fine tuner ; 
 , regard job infra dig . 
 ( 2 . ) like straight talk ! 
 - , aspire organist teacher . 
 : ' diploma ; believe . ' 
 letter badly - write , ungrammatical , 
 spelling shaky . know 
 music , heap learn way , 
 humble learn , chance 
 success professional musician slight 

 little March , . J. D. Davis 

 March ' * ' Egmont " Beethoven 

 March ' " ' St. Polycarp " ' ... F. A. G. Ouseley 

