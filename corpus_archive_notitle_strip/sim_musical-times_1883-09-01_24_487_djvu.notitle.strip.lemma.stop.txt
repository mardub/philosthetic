THURSDAY evening . — Macfarren " LADY 
 LAKE " miscellaneous SELECTION 

 FRIDAY MORNING.—Beethoven ' MOUNT OLIVES , " 
 Gounod * MESSE SOLENNELLE , " Hummel ' ' ALMA VIRGO , " 
 Mozart " ' jupiter " SYMPHONY 

 FRIDAY EVENING.—Mackenzie " JASON " MISCEL- 
 laneous selection , include Mendelssohn VIOLIN CON- 
 CERTO E MINOR ( Mr. CARKODUS 

 WEDNESDAY MORNING.—Mendelssohn " ELIJAH 

 WEDNESDAY EVENING.—"“GRAY elegy , " Cantata 
 ( write Festival ) , Alfred Cellier ; Beethoven SYM 
 phony D ( . 2 ) , & c. ; eo 

 THURSDAY MORNING.—Ratfi Oratorio " W ORLD 
 END " ( performance England ) ; selection 
 Works Handel . ; seed 

 FRIDAY evening . — " * crusader , " Niels Gade ; 
 Overture , " " GENOVEVA , " Schumann ; March Chorus 
 " TANNHAUSER , " & c 

 SATURDAY MORNING.—GRAND MASS D , Beethoven 

 HYMN PRAISE , " Mendelssohn . 
 serial ticket , limited number ... dem Sones £ 5 5 . eacl 
 Seats — single ticket ( reserve ) , morning , LEIS 
 " " " Evening , 158 . " 
 second Seats — single ticket ( reserve ) , morning , 10s.6d . , , 
 Evening , 7s . 6d 

 SS ADA SOU TH , R. A. M. ( mezzo - soprano 

 Tor Oratorios , Operatic , 
 7 Brondesb r| 
 MISS LILY MARSH ALL- WARD ( ( soprano ) . + 
 MI ISS JESSIE M ARSHALL- WARD ( contralto ) . 
 ka arc dicey Crescent , S. Kensington ; 80 , Addison St. , Notting 2 
 MADAME CLARA ST ( soprano ) . 
 MISS LOTTIE WEST ( contralto ) . 
 : Beethoven Villa , King Edward Road , Hackney , London . 
 MISS AGNES MARY EVERIST ( Contra!to ) . 
 Operas , Concerts , Oratorios , & c. , address , 59 , Camden Square , N.W. 
 Keppel Co , , 221 , Reg rent Street , W 

 9 

 Hymn Praise . " 
 " Walpurgis Night 

 Taydn " * Imperial Mass. " 
 Gounod " St. Cecile . ' Beethoven " ' Mass € 
 Mozart " ' Twelfth Mass. " 6 Acis Galatea 

 Rossini   Stabat Mater . Mozart " Requiem . ' 
 " Judas Maccabzus . " " Dettingen Te Deum . " 
 " Messiah . " ' Israel Egy pt 

 MUSICAL TIMES.—Septemper 1 , 1883 

 visibly " form " modern notation ; 
 finally influence habit contem- 
 plate musical form present 
 certain period development art . 
 cause Mr. Gurney notice . Wagner 
 allude high value melodic idea 
 spring " harmonious relation , " Mr. Gurney 
 reply : " notan uncommon notion harmony 
 rate fundamental , sole 
 originate source impressive musical effect 
 age ; misconception far disastrous 
 result old idea phantom harmony 
 bass essential ingredient melody 
 Helmholtz coup de grice . ' Mr. Gurney 
 far , quote Wagner 
 follow passage , characteristic great 
 reformer , thinker , music dramatist , Herr 
 Wagner , German , chauvinist , musician 
 period . : " italian opera melody re- 
 maine satisfied harmonic basis 
 astounding poverty , exist 
 accompaniment . " astounding ? 
 special virtue merit italian melody consist 
 independence support . italian com- 
 poser period Wagner refer afford 
 treat orchestra ' " guitar ' ; person 
 endow power expression voice 
 fine quality venture sing unaccompanied . 
 italian opera melody ' ' impressive " 
 " expressive . " Bellini air , example , mia man 
 al fin tu sei , " exquisite melody — 
 intensely dramatic expression scene 
 occur . doubtful Wagner , 
 theoretically conceive 
 intend , musically accomplish 
 comparable melody , consider purely 
 dramatic point view . splendour orchestration 
 improve . grandly represent dramatic 
 element melody , means concentrated 
 expression , antithesis critic 
 intend signify talk ' * logical develop- 
 ment . " ' rational thought , " Wagner , " exist 
 reason consequence , find 
 hold " symphony Beethoven . 
 music . canon 
 logic , simply orderly arrangement . 
 Wagner , marvellously clear exposition 
 idea , letter M. Villot , lay 
 law form music melody , utter 
 scientific zsthetical common - place ; 
 proceed explain ingenious fertile con- 
 ception infinite melody , dependent 
 notion melody " harmonious relation . " 
 fact , conception mix 
 " change expression " know impart 
 melody ' ' harmonic turn , " appear 
 direct outcome mere technical device 
 day . harmonic effect shape 
 evanescent . unite fragment melody 
 shining ripple roll 
 billow — strength intrinsic 
 musical power motion . infinite melody , combine 
 large outline — necessarily know form — 
 ultimate completion wagnerian 
 theory . fortunate idea 
 Wagner orchestra illus- 
 trate action drama , motive 
 action . attempt purely 
 melodic symbolical device . melody 
 sense harmony articulate speech 
 sound ; limit definiteness music 
 impassable . overstrain deliver our- 
 self bind hand foot upholder 
 " impressive music . " orchestra opera 
 , Voltaire je spectacle tragedy 

 share interest . infancy early 
 youth opera , Quinault report 
 place tragedy apprenticeship music . 
 result eliminate complication 
 action drama , motive 
 action , adapt piece musical setting . M. 
 Gounod lately " difficult 
 people listen piece music 
 portrait character . " afraid ; 
 ' " ' character " people seek painting , 
 definable . - quote dictum 
 adoration music address Deity 
 mistress immaterial , usually attri- 
 bute Cusin ; 
 word long think , 
 Chabanon , Gluckist , precursor 
 time Wagnerism . need depend 
 intense penetrative criticism Wagner , 
 theory musical illustration , 
 dramatic element incidental quality , 
 beginning end music . 
 advantage certain darwinian hypothesis , 
 inconvenience Mr. Gurney himselfseem con- 
 scious , toshow — hypothesis like 
 truth , practically acknowledge phase 
 common heredity — origin 
 music ' ' expressive , " deeply dramatic . 
 , elementary technicality music , 
 mode , distinctly ' ' expressive , " ' diiferen- 
 tiation precise definition subjective mood ; 
 sense 
 employ ancient . early musical 
 effort artistic merit , apart organise 
 cry exclamation , nation 
 expressive particular subjective mood 
 beginning dramatic . mediaeval music ally 
 solemnity impose church ritual , 
 word hymn " Stabat " 
 Requiem , spirit origin dramatic . 
 modern symphony , pure representative 
 independent music , , hand 
 glorify , dramatic tendency ; 
 weak effort direction , 
 , similar pretension . culmination 
 tendency theatre . ' discipline 
 requirement stage , musical point 
 view , induce condensation concentration 
 thought expression . stage intolerant 
 technical amplification recasting 
 reminiscence . demand stroke shall 
 tell , cost inventive power . 
 regard demand sacrifice 
 ideal , vague musical impres- 
 sion . new mental trajectory , 
 level ordinary surrounding , widen 
 lower vision composer . objective 
 clearness scene , action situation , stir 
 mind new direction . ultimately react sub- 
 jectively , provoke unity , intensity , glow 
 characterise originaliiy ; complex 
 quality , result certain mental focus vivid- 
 ness apparent balfian strain 
 elaborated exordium asymphony . musician 
 rule acknowledge essentiality genius write 
 stage . dabble symphony 
 chamber music , hope clench extend 
 fame write opera . 
 attack middle - life success . history , 
 , record instance success 
 circumstance ; generally 
 musically dramatic genius specific , 
 born possessor , stage 
 early naturally . curious sign time , 
 composer evince desire adapt cantata 
 — sacred cantata — tothestage . attraction 

 great composer . 
 JosepH BENNETT . 
 . xiii — CHERUBINI ( continue page 437 

 CHERUBINI visit austrian capital , 
 unattended success , scarcely 
 equal expectation . upset 
 campaign Austerlitz . time 
 fabric society disorganised , , 
 doubt , failure 
 commercial enterprise , accord Pic- 
 chianti , master associate Steibelt . 
 lure temptation draw artist 
 proper path bog manner 
 unpleasant place , composer start 
 music - printing establishment . course 
 know manage amid prevail 
 confusion , collapse soon end 
 anxiety . Cherubini share debt 
 heavy , meet manfully 
 time fight battle life 
 crippled resource . amid disagreeable 
 Vienna , Cherubini consolation , 
 " Faniska " receive play 
 - night . Allgemeine Musthzcitung 
 Leipzig , curious thing 
 Beethoven period , notice work 
 course , remark 

 music respect worthy great 
 master write ; exception certain 
 passage reproach 
 artificial ; profound , warmth 
 power , thoroughly characteristic . rich 
 harmony — — truly 
 striking strongly dramatic . buta person 
 hear time understand seize 
 thoroughly 

 reason point Cherubini 
 remain Vienna produce second 
 opera engage write . autobio- 
 graphical sketch read : " opera 
 ' Faniska , ' perform time 
 February 26 , 1806 , start Vienna March 9 , 
 arrive Paris April 1 , absence 
 month day . " right add 
 " Faniska " work write Vienna 
 master . catalogue enter , 
 date 1805 : ' March wind instrument , 
 compose Vienna Baron von Braun private 
 band . sonata barrel organ belong 
 Baron Baum 

 great story tell Cherubini 
 relation Beethoven , Haydn , 
 illustrious musician Vienna 
 " city master . " shall concern 
 . large proportion mere 
 fancy , game sift chaff 
 wheat worth candle 

 Cherubini receive Paris open 
 arm . especially professor pupil 
 Conservatoire greet confer great 
 lustre institution help found . 
 organise musical entertainment 
 impromptu sort , whereat perform sundry selec- 
 tion work . propos , M. Pougin 
 place direct opposition Fétis 
 point form interest 

 Restoration Cantatas ; , 
 accompaniment , military féte July 20 ; 
 — voice , chorus 
 accompaniment — perform King 
 August 29 . important thing 
 work bear date . 
 1814 master compose string quartet 
 ( e flat ) refined classic beauty , 
 need foil , find author warlike 
 effusion 

 Government Louis XVIII . slow 
 mark Cherubini worth official honour , 
 deny unforgiving Napoleon . 
 December , 1514 , master gazette Chevalier 
 Legion Honour , month 
 succeed M. Martini Superintendent King 
 musical establishment . , thing 
 run smoothly new régimc . 
 Government play havoc Conservatoire ; 
 dismiss founder director , Saretti , 
 circumstance positive contumely , dis- 
 organise establishment Cherubini find 
 . cir- 
 cumstance invitation come England , 
 accept . London 
 Philharmonic Society flush youth- 
 ful vigour . found Cramer , Clementi , 
 Neate , fellow , year , 
 naturally desire achieve 
 sensational nature . invitation Cherubini , 
 receive compliment 
 accept Beethoven , Spohr , Weber , Mendelssohn , 
 Wagner , Gounod . master case 
 offer compose work , conduct 
 . piece form overture 
 symphony ; Cherubini write vocal 
 composition , , selection 
 repertory , duly perform . accord 
 diary , start London February 25 , 
 1815 , return Paris June 8 . 
 english capital , experience , 
 form subject chapter 

 continue 

 regard composition result Eisteddfod 
 far satisfactory . prize offer 
 good cantata , anthem , - song , psalm - tune , chant , & c. , 
 adjudicator case withhold reward alto- 
 gether , temper bestowal 
 good deal qualification . seventeen cantata send 
 £ 50 prize , good write Englishmen , 
 find worthy . general result 
 department creative music education 
 spread sufficiently good result . 
 working waiting welsh 
 people distinguish composer 
 Eisteddfod platform . know 
 action Cardiff adjudicator , 
 base decision high standard usually 
 rule meeting . question answer 
 composition good welsh 
 composition , absolute claim work 
 art 

 come orchestral performance , 
 place evening 7th 8th , let 
 direct great zeal intelligence Mr. E. 
 H. Turpin , act Eisteddfod organist 
 week . admirable work Con- 
 cert - know performer Herr Pollitzer 
 head , programme include , 
 thing like sort , Beethoven C minor Symphony 

 500 

 evening sitting , preside Mr. S. Pope , Q.C. , 
 devote performance " Judas Maccabaus . " 
 prove hardly satisfactory , solo 
 sing Miss Marriott , Miss Roberts , Mr. Dyfed Lewis , 
 Mr. Lucas Williams . accompaniment play 
 pianoforte harmonium course ineffective , 
 choir , having rehearse , sing 
 obvious disadvantage . trust 
 occasion manager able obtain 
 orchestra , arrange sort rehearsal 
 case large important work . Festival 
 useful interesting allow degenerate 
 finally disappear 

 Tue Duchess Connaught Princess Christian 
 Saturday afternoon , 11th ult . , 
 Concert Ascot aid fund comple- 
 tion St. Anne Church , Bagshot , erection 
 mainly attributable exertion Duke 
 Connaught parishioner village . 
 entertainment place ball - room 
 ground Grand Stand . Signor Tosti conduct . 
 programme consist selection Beethoven , 
 Offenbach , & c. ( strange combination composer ) , 
 amateur musician Lord William Comp- 
 ton , Mrs. Ronalds , Mr. Charles Wade , Miss Damian , 
 Miss Shinner Mr. Parratt ( St. George Chapel ) , 
 render assistance . course concert 
 Princess Christian , receive enthusiastic greeting , 
 Mr. Parratt play pianoforte selection Chopin 
 Saint - Sains . Duchess Connaught sing * * 
 dream , " ' Bowen , chansonet Alary , dis- 
 play sweet musical voice , greatly 
 applaud 

 Tur work perform Tufnell Park Choral 
 Society forthcoming season wi!l select 
 following list : ' ' End World " ( Raff ) , 
 " Lord King " ( J. Barnby ) , ' St. Mary Magdalen " 
 ( Dr. Stainer ) , ' Jason " ( A. C. Mackenzie ) , ' St. Peter " 
 ( Sir J. Benedict ) , ' * Bride Dunkerron " ( H. Smart ) , 
 " Queen " ( Sir Sterndale Bennett ) , " Acis 
 Galatea " ( Handel ) . rehearsal commence 
 gth prox . , Concert place Dec. 18 . 
 Mr. W. Henry Thomas continue conductor , 
 open season 2nd prox . annual 
 Concert , postpone 

 502 MUSICAL TIMES.—Septemper 1 , 1883 

 tue detailed programme Leeds Triennial Musical 
 Festival , commence October 10 , pub- 
 lishe . opening performance Mendelssohn 
 ' * Elijah , " principal vocalist Madame Valleria , 
 Miss Annie Marriott , Madame Patey , Miss Damian , 
 Mr. Maas , Mr. Santley . evening Concert , 
 Mr. Alfred Cellier Cantata ' ' Gray elegy , " write 
 Festival , perform conductor- 
 ship composer , solo singer Misses Anna 
 Williams Hilda Wilson ; Messrs. Lloyd king . 
 second miscellaneous , include Beet- 
 hoven Symphony D , . 2 , vocalist 
 Madame Valleria Mr. Lloyd . performance 
 England Joachim Raff Symphony - Oratorio ' * 
 Worlds End , judgment , New World , " 
 place second morning , Miss Damian Mr. 
 Santley singe principal ; second com- 
 prise selection work Handel . 
 evening , Mr. Joseph Barnby setting 97th Psalm , 
 ' " " Lord King " ( write festival , con- 
 ducte composer ) Bach Sacred Cantata 
 " Thou Guide Israel ’' ( time country ) , 
 willbe ; Mozart motett ' glory , Praise , honor , " 
 Rossini ' * Stabat Mater " include 
 Concert . Sir G. A. Macfarren Oratorio ' * King David , " 
 compose Festival , occupy morning , 
 Madame Valleria , Madame Patey , Mr. Lloyd , Mr. 
 Santley solo vocalist ; evening 
 Gade * ' crusader , " Miss Hilda Wilson , Mr. Lloyd , 
 Mr. Blower principal singer , miscel- 
 laneous form programme . Beethoven 
 great Mass D , solo Miss Anna Williams , 
 Madame Patey , Mr. Lloyd , Mr. Santley ; Men- 
 delssohn ' ' hymn praise " 
 morning ; evening extra Mis- 
 cellaneous Concert , consist selection 
 Festival work . choir number 313 , band 
 112 ; leader , Mr. Carrodus . Sir Arthur Sullivan 
 conductor , Dr. Spark Mr. W. Parratt 
 organist . choirmaster , Mr. J. Broughton ; accompanist , 
 Mr. A. Broughton 

 Tue Edinburgh Choral Union , conjunction 
 Glasgow Choral Union , series Choral 
 Orchestral Concerts , conductorship Mr. 
 August Manns , month December 
 January . arrangement fully 
 complete , note work 
 Haydn " season , " Miss Mary 
 Davies Messrs. Maas Bridson ; MHandel 
 " ode St. Cecilia , " A. C. Mackenzie ' Jason , " 
 Mrs. Hutchinson Messrs. Edward Lloyd 
 Santley . Orchestra strengthen 
 M. Buziau leadership . Choral 
 Union begin new life season . 
 existence - year , 
 good raise tone choral singing 
 improve taste orchestral high class music . 
 seventeen year Conductor Mr. 
 Adam Hamilton , influence training 
 past success . succeed Mr. Thomas 
 H. Collinson , Mus . Bac . , Oxon . , organist St. Mary 
 Cathedral . accompanist season Mr. 
 Charles Bradley , Organist St. George Parish Church 

 Natal Mercury July 21 correspondent , speak- 
 ing Durban Philharmonic Society , : ' * 
 lot good thing Society hand . 
 week Concert follow- 
 e - song — Macfarren ' hunting song , ' Berger 
 ' lovely night , ' Gipsy Chorus Weber ' Pre- 
 ciosa , ' Macirone ' Sir Knight , ' Rossini ' Carnovale ' ; 
 orchestra overture Bishop 
 ' Guy Mannering ' Handel ' Samson , ' 
 piece , vocal instrumental 
 solo . Concert week later 
 Beethoven ' ruin Athens ' Van Bree ' St. Cecilia 
 Day . ' practise . 
 week member selection ' Saul , ' 
 ' Elijah , ' ' Judas , ' possibly ' Messiah , ' 
 fifth big Concert 1883 series . truly Mr. 
 McColl wonder music Durban 

 annual meeting Watford School Music , 
 distribution certificate successful student , 
 place Saturday afternoon , 4th ult . , large 
 Hall Public Library — Sir George Grove , Director 
 Royal College Music , chair . behalf Miss 
 Alice Brooks , hon . secretary , Report progress 
 school read Mr. Brooks . an- 
 nounce scholarship present 
 Institution , pianoforte - playing head master , 
 Mr. Henry Baumer , singing Mr. Visetti . 
 scholarship award 
 Examiner , Mr. John Francis Barnett , Miss K.S. Burns , 
 second Miss C. S. Grindley , Examiner 
 Mr. W. H. Cummings . exhibition , 
 guinea guinea , offer 
 Council compete student limited 
 mean possess musical talent , money spend 
 education School . Council , 
 recommendation Examiner , Dr. C. H. H. Parry , 
 award pianoforte - playing Miss Mary B. 
 Grindley Miss Bluett , singing Miss 
 Raynes Mr. A. Spicer , adjudge equal . 
 case exhibition equally divide . 
 examiner School , Dr. Parry , report 
 appear decide advancement 
 department standard year , especially 
 commend singe Miss Alice Wilson Mr. 
 Healey ; Miss Rebecca Longland , Miss Kate Longland , 
 Mr. Spicer entitle favourable mention , 
 Sir George Grove , elogquent speech , 
 praise connect School , particularly 
 Miss Alice Brooks , hon . secretary , unwearied 
 exertion cause . speech 
 Rev. N. Price , Dr. Brett , Hon . R. Capell , Mr. 
 Baumer . certificate present student 
 Hon . Miss Grosvenor 

 long fill Mr. Proudman 

 concert Mr. Sanders Beethoven 
 Rooms Thursday evening , July 26 . vocalist 
 Miss Clara Dowle , Miss Jessie Royd , Miss Eleanor Crux , 
 Mr. Thurley Beale , Mr. Holmes ; solo pianoforte , 
 Miss Edith Collins ; solo violin , Miss Charlotte Wilkes ; 
 solo trumpet , Mr. T. Harper ; violoncello , Mr. Edmund 
 Woolhouse . singing Miss Clara Dowle 
 violoncello - playing Mr. Woolhouse special 
 feature Concert 

 tue prospectus Monday Popular Concerts 
 announce - sixth season series twenty- 
 evening Concerts , commence November 5 , 1883 , 
 extend April 7 , 1884 . Saturday Popular 
 Concerts consist performance , 
 afternoon , extend November io , 13853 , 
 April 5 , 1884 . artist include Madame Norman- 
 Néruda , Herr Joachim , M. Pachmann Signor Piatti , 
 engagement pende 

 Schott Fréres 

 goodly volume nearly 400 page M. Grégoir 
 Grétry Otto Jahn achieve 
 Mozart , Mr. Thayer Beethoven . 
 monument industrious research , cover ground 
 leave stone unturned . gather pre- 
 face author compile work competition 
 prize offer Académie Royale de Belgique , 

 504 

 excellent Recital , highly appreciate . selection vocal 
 music contribute Miss Shaw , Mrs. Clarke , Mr. C. W. Smith , 
 Mr. Emsley.——The organ Holy Trinity Church having 
 renovate Messrs. Wordsworth Maskell , Leeds , in- 
 augurate 2nd ult . Mr. J. H. Rooks , , - select 
 programme , display quality instrument utmost 
 advantage . ordinary choir church reinforce 
 number friend , chorus effectively render 
 direction Mr. Popplewell ; choirmaster , Mr. B. N. 
 Parkinson , contribute tenor solo 

 Bristo_.—Beethoven ' Mass C perform St. Mary 
 Roman Catholic Church Sunday , rgth ult . , time 
 Roman Catholic Church Bristol , orchestra . 
 soloist Misses L. Benham , Flemming , Jackson , Messrs. 
 James Williams , George Williams , Partridge . leader 
 band Mr. Jacobs , conductor Mr. Richelieu Jones , 
 choirmaster , Mr. Augustus Simmons , preside organ . 
 performance highly appreciate crowded congregation , 
 reflect credit concern , evening service orchestra 
 assist , item render Sir M. Costa 
 " date Sonitum , " Emmerig Magnificat , Tantum ergo ( Lutz C 
 minor ) , & c. , conclude voluntary band organ 
 play Sir M. Costa March Naaman 

 Carpirr.—The large Taff Vale Railway engine - shed , scene 
 recent eisteddfoddic meeting , use 11th 
 13th ult . musical purpose , cencert aid fund 
 provide telephonic communication new Infirmary 
 building residence medical staff . large 
 attendance occasion , fame band 2nd Battalion 
 Royal Highlanders , programme principally devolve , 
 having doubtless contribute result . sum £ 170 
 require complete project work . Concert clear 
 expense band , & c. , second night 
 desire obtain , gentleman personally offer 
 contribute small balance necessary sum 

 Mr. Byrom Dewhurst . choir " ( compose 100 voice 

 
 = e 
 b excellent rendering chorus . fant Class } » o soul " ; Toy b yy 
 sitet train , extremely effective . Cantata 2 , " th 
 si follow selection Messiah : " worthy } brus 
 cted Lamb , " * nation " ( exceec y sing Mr. J. D. | 1aps biz 
 nost Smit h , Mancheste T ) , nd " hallelujah " C A. Wood | Sy contain son 
 ay 4 leader orchestra , Mr. Meal organ . | guet tenor bass , 
 rn great praise exertion Mr. Allen , | 1 quintet " R tejoice th 
 ii Conductor , Magnificat Nunc di : 
 ' | ye ompose occasion Mr. 
 ry Heiensnurcu . — new organ construct d | Ions Iral . Service cont 
 ein Church SS , Michael Angels inaugurate Satur : par > render c 4 ie 
 eveni : g 18th ult . % Organ recital Dr. Spark , ners " pr : rite = service paro bial ch 
 nee : t lilt Mr. August — Foviten , ihe speciii- | aith 1¢ dimittis successfu 
 pr » musical s tructural , Mr. C. W. Met ven , Greenock . : Pda E. Perry , McDonale d , 
 pre nme pone select dis f ne instrument McCall , facie ye > thoroughly eficient , 
 good ivant : 2ne Choir lar augment , Sullivan * On- chorus effectively render througho Dr. Gladstone 
 ward , Christian Soldie 3 " Dr. Spark Authem " Israel " present conc 1 ! work . Mr. lons preside organ 
 stra exceliently render . ndplaye tl n swith great taste anda 
 ta hi DDERSFIELD.—The Hillhouse Musical Festival , aid wa conclude rth € singing festival hymn commence : 
 1 C lerstieid Anfirmary , place July 27 , nu ous | festal day , sanctitie ! " ' music compose Dr. Armes , 
 gan py large orchestra erect field lend c occa- = 1 — annual Musical Flower § ervice 
 sion , efficient band lead Rey , J. Thomas , M.A , , | ¥ hold Elsy . oye portage eat Wednesday , t Patil 
 Conductor Mr. John North . programme s ult . , new Canta pedal s ¢ oe : nity eatin 
 ind include rhe gs chorus Messiai , wh baie dan d George t pa Mi ss Beh ahe sol 
 ad Nently 1 mention le hymn , compose | Miss Shs yr om . TR os Rnaigeto ef 
 en Mr , Richard . ' Mellor , expressively sing highly appre- | Thompson ( Cathec S. Seatree , J. Robinson , 
 e ciate chorus number s Mr. G. Dobbs conduct , Mr. j. MM . 
 ws eiage F preside organ , Mr. A.B. Thomsonat oforte . 
 { STPIERP n 9 » 
 ie Pm » TPIERPOIN ~An open - air Conc cert choir Wareis aware s : . thir 1 Orga cn Reciia ! 
 ’ » inner quadrangle , Tuesday , july Ethie amie oe Shurch . pro- 
 vas ta choose Rombe rg Lay meio sede " 7 whe 1 , , B h , Ha -d aig ® 
 red Sell , ° chorus solo render highly | 8ramme > comprise work y eS pere eae ces Haetes 
 preciate y large audience present . Mr. J. H. Bebbington sing delssohn , & c. : 
 ng Masier creditably . second miscellaneous . pes sg Am.—At opening Fine Art Industrial Exhibition 
 Mr. E. C. Allen , Choirmas ter , conduct . rst ult . , evening concert Mr. G. R. Platt 
 ae te ienes 
 “ nt ILFracomrr.—A series Concerts Oxford Choir © — — _ > fon ots ha wen 8 thes — 
 ng Hall week end 25th ult . artist Miss tal Quartet . — t cane : 15th ae owe Apollo Choi 
 0 , carne , teen er hee olin aa Hr jalig | wast monthly Concore : Tit Baw Colle , Tavers , 
 ‘ ibe Keaig : ee — dirbali ody voit | Greaves cond lucting , accompaniment play b 
 » piano . programme popular character , | 4H. Greaves 
 e audie : ice , large , appreciative . 3 : 
 nt 2 SaLtsurn.—Dr . ark , Organist Leeds Town Hall , 
 KILMARNOCK.—What term { " ' Burns Musical Festival " | present series Organ Recitals North Yorkshire . 
 Fer rrth ult . , immense open - air gathering | frst place Wednesday , sth ult . , n Parish Church , ' 
 ty asse { hear musical setting good programme incluce aC onzerto Handel , Fugue Bach , 
 - iyeic .. chorister belong chiefly church choir toy important interesting wate si delssohn , Beethoven 

 510 MUSICAL TIMES.—Seprempser 1 , 1883 

 Guilmant , Rossini , Batiste , performer , 
 highly appreciate audience . evening Dr. Spark 
 preside organ Festival Choirs , consist 200 
 voice , piece sing effect 

 ScARBOROUGH.—At Spa Theatre , 6th ult . , Mr. Owen 
 Williams , Organist St. Martin , Pianoforte Kecital aid 
 Royal Northern Sea - Bathing Infirmary , attend . 
 programme include Beethoven sonata , prelude 
 Fugue Bach , selection Chopin , trio Gade violin , 
 violoncello , pianoforte , Mr. Williams ably assist 
 Misses Alderson - Smith . performance concert- 
 giver elicit warm - deserve applause , solo 
 violoncello Miss M. Alderson - Smith feature 
 Concert 

 tuincton , N.Z.—The Orchestral Society Con- 
 cert July , programme draw exclusively classical 
 source present large attentive audience , 
 include Governor New Zealand . Concert 
 Society , Symphony entirety central feature , 
 play occasion Mozart Fiupiter , , 
 spite slip , admirably , splendid 
 final movement unflagging spirit precision . 
 overture Cherubini Lodoiska Weber Oberon ; 
 Schubert ballet music Rosamunde selection Beet- 
 hoven ruin Athens . movement 
 - composer " Septuor ’' smoothly play member 
 Society ; song Beethoven , Mozart , Kiicken 
 , orchestra number thirty- performer , 
 direction Mr. Robert Parker 

 Wuitsy . — Whitby Choral Society Concert 
 Saloon , Thursday , 2nd ult . large brilliant 
 attendance . Cowen Cantata 7he Rose Maiden admirably sing , 
 principal Madame Tomsett , Mr. Moxon , Mr. R. Grice . 
 Miss Trust harpist , Mr. N. Kilburn , Mus . Bac . , Cantab . , 
 able accompanist . Saloon Band good service . Mr. 
 Henry Hallgate , Society Conductor , musical management 
 Concert .. —-a Festival Choirs hold St. Michael 
 Church 21st ult . , large congregation assemble . special 
 psalm sing chant Elvey Jones , andthe Magnificat 
 Nune dimittis Lloyd . Anthem ' 
 dwell sky , " Dr. Walmisley . hymn sing 
 sermon , offertory . 
 musical portion service render taste 
 judgment , reflect great possible credit 
 active . prayer intone 
 Rev. S. Flood Jones , Precentor Westminster Abbey , 
 sermon preach Rev. Canon Wright , Vicar Doncaster . 
 organ accompaniment play Mr. H. R. Bird , Organist 
 ke nsington Parish Church . Mr. Carr choirmaster , Mr. 
 Henry Hallgate 

 Organ . op . 34 ... aa oae 
 LASSEN , e.—ballet music " Ub ber Sieabew : trt p 
 p73 
 orchestral score ... jas ret 
 orchestral ... ' aes 
 Pianosolo ... sas 
 LEBEAU , L. A.—Three sin rs Viola | Pianolorte . 
 op . 26 : — 
 . i. ee oes wee ° 
 . IL . axe sin aie 
 . iii 

 L LEON ARD , H. — Cadenza Beethoven ! s V ‘ oli concer 

 Op . 530 . 
 Quartet ae ee ove oe 
 Piano ... esp si ‘ se kab 
 LISZT , F.—Années de Pélerinage . aide Piano . 
 Series . no.1 Angelus ... ove e é ove 

