CANTATAS and ORATORIOS 

 Bach , the passion of our Lord 
 Magnificat ‘ il 
 Cantata 50 , ' ' now shall the grace " 
 Cantata 53 , " * strike , thou hour " 
 Cantata 50 , " ' Ich will den kreuzstab 
 gerne tragen " 
 Cantata 2i!1 , * * be silent all " 
 ( Coffee Cantata ) 
 Beethoven , Choral Symphony 9 
 Haydn , the season endorsement 
 mass in b ? : etek veeiitithoes 4 

 Mendelssohn - Bartholdy , Elijah pande naka asp taint = tae 
 Mozart , the Impresario olga taradoremplapcoaalicnis 

 ne achievement 

 Beethoven , string ( Quartet , F , op . 18 

 Op . 59 

 the MUSICAL TIMES — Octroser 1 1925 

 _ 
 wtto Mr. Evans , who be constitutionally incapable 
 { quote m= accurately . never do I describe 
 vavinsky as a chicken with its throat cut . it 
 g to Mr. Evans and the other Stravinsky 
 putic that | apply a simile resemble that . 
 yj » have be discuss , in 1921 , the then 
 sition of Stravinsky . again and again I have 
 iggise his genius in the great work and 
 .. ette the depth to which he have fall in his 
 ker one ; again and again I have urge the 
 bavinsky claque to recognise , as the only way of 
 se their damage reputation , that Stravinsky 
 » j his off moment even as Bach and Wagner 
 i Beethoven and Mozart have . they still went 
 congratulate themselves on the superiority of 
 iit forward - look mind long after every one 
 » ) be not a blind partisan have recognise that , 
 othe time be at any rate , Stravinsky be 
 ite below his own good . by their refusal to 
 eeognise plain fact they finally write themselves 
 smoletely out of the regard of plain people who 
 nte to get at the truth , the whole truth , and 

 wthing but the truth with regard to Stravinsky . 
 itherefore , in an article in the Suaday Times of 
 , describe these partisan critic as 

 the MUSICAL TIMES — OctToser 1 1925 

 HOLST ’s choral symphony impression that Holst have simply | ' and rightly 
 regard the singer as a group of wind instrument 
 | and have assign to they the job that they alone 
 the title of Holst ’s new work " at once rouse | can tackle — v.e . , the delivery of the text jp a 
 interest . a composer do not use the word ' First’| musical idiom most suitable to it and they . in 
 thus on a title - page without an eye to the future.| theory , of course , this be what all choral writer 
 every orchestral composer of importance soon } set out to do , but in actual practice the purel ; 
 or later write a symphony ; it be natural that } musical interest and the convention of choral . 
 Holst , with his strong choral leaning , should open | writing be too strong for they . 
 his innings as a symphonist with a choral example . the work be in the usual four movement 
 it might reasonably be say that the - first ’ has| | the first be usher in by a slow Prelude 
 another and wide significance . for , when one } | correspond to the Adagio of many classica 
 go into the matter , how many real choral | |symphonie . the word be from various ne 
 symphony be there ? Beethoven ’s be an| of Keats , and an excellent ' book ' they make , be 
 orchestral symphony with a choral section tack | well - contrast , singable , understandable , and very 
 on — to its detriment , some critic hold . and the lapt for musical setting . not often do fine 
 few example subsequently produce under the | poetry so amply meet the composer 's require 
 title might as fairly be call choral work with an | ment . ' the selection be wide and ingenious , and 
 unusually important part for orchestra . in other ] its detail be here set forth for the benefit of 
 word , the composer have write the choral ! reader who wish to hunt up the passage in their 
 portion as if for an oratorio or cantata . but| Keats . the prelude , call ' Invocation to Pan , 
 surely a genuine choral symphony would treat the| draw on ' endymion ' for the first and last stanza 
 vocal force as a component part of the ensemble . | of the chorus in which the shepherd of Latmos 
 there would be little room for the customary - fat ' | invoke Pan . the first movement proper be a 
 for the choir in the shape of long section in}‘song and bacchanal , ' and have for text some 
 which a fragment of the text be discuss — perhaps | extract from the ' roundelay ' that occur later 
 worried ' would be a well word — by all the parts}in the poem . the slow movement take t ! 
 over and over again . florid writing be also more| ' ode on a grecian Umm ' intact . ( what magica 
 or less rule out , because the utmost a group of and familiar line meet one here ! ~ hear melodie 
 singer can manage in this way can be done|are sweet , but those unheard be sweet there 
 immeasurably well by their colleague in the| fore , ye soft pipe , play on ... beauty be truth , 
 orchestra . and , just as no orchestral work would | truth beauty , — that be all ye know on earth — 
 contain a long section for string , wood - wind , or } the 7 : » use most of the poem call 
 brass alone , so in a true choral symphony alengthy | ' fancy ' ( ' ever let the fancy roam ' ) , and the 
 1 cappel / a section would be out of pli ice . these | well- " dill n ' Folly ’s Song ( ' when rye re 
 remark be prompt by the way in which this | be a - play , Huzza for Folly - O ' ' ) , one of t 
 Holst work live up to its title in regard to|short piece head ' Extracts at an Opera 
 structure , distribution of interest , and , not least , | for the Fiva / e the composer take the first stanza 
 the treatment of the voice . * | of the ' spirit Song ' ( ' spirit here that reignest 
 here it might be well to dispose of a point that | the first four line of the " hymn to Apolle 
 will at once suggest itself to reader in connection | ( God of the golden bow ' ) , and the main part oi ! 
 with the choral part . how be the text aflected?|the ' ode to Apollo . ' it be worth note that 
 will not the treatment of the choir as a mere ] he omit the last two line of the follow verse 
 member of the orchestral family tend to reduce 
 the importance of the word , and so depreciate 
 the very fons e¢ origo of the whole composition ? 
 che answer be easy this Symphony of Holst ’s while the trumpet sound afar : 
 give the text the good of chance . ' there be very but , what create the most intense surprise , 
 little repetition of word , and none of the treat- his soul look out through renovated eye , 
 ment of the all - too - familiar type that be hit off 
 in the " Bill , give I _ that spike ' parody . |—a dreadful lapse on Keats ’s part that create ' 
 moreover , in a very large proportion of the work | most intense surprise until we remember shat bis 
 all the voice be singe the same word , and in| mighty subject also nod at time . ( yet there 
 note of the same value . when to these aid to| be nothing wrong with ' intense ' or ' surprise ' 4 
 clarity be add Holst ’s customary and scrupulous | poetical diction . p erhaps their combination have 
 regard for verbal accentuation , it will be see that| } become cheapen in conversation , like ' awfully , 
 if the text be not both audible and significant , it| and other over - work epithet . ) 
 will not be the composer 's fault . nor be this effect of | a detailed analysis of the Sy mphony would be 
 verbal naturalness achieve at the cost of the music . | useless without a prohibitive amount of illustration 
 although the vocal writing be mainly diatonic , } moreover , some of the fine passage do not lend 
 often slight in texture and simple to the eye , it be of | themselves to quotation . it must suffice to give & 
 great rhythmic variety and interest . one have an } brief description of each movement , with a few 
 extract of the kind that suffer least by remov ® 
 | from their context 

 by Harvey GRACE 

 omposs ' @ bis own . no one privileged to hear anton| : 
 one who use only the natural volume of his 

 may © Mf Avdinstein , or Busoni , can have forget the p " 
 ce " Mi straordinary variety of sound these artist of | ® ° & S : the sound be less vibrant and lack 
 to pro ’   Xalus obtain from their instrument . listen to | tensity . if one give way to a sort of impulse 
 [ 0 , | — — — — | which some call temperament , as above mention , 
 te lively BM icine ai ot mart : Beethoven , Schubert . — — — — Saint Saéns . | then he be ' hammer upon’—not play — the 

 pianoforte 

 1925 899 

 gellectual purpose of widen their musical 
 iowiedge , most people will be draw there by 
 i , emotiona ’ interest , centre round Wolf 's per- 
 goality , with a reflect sympathy for the performer 
 sho share their enthusiasm for the composer . 
 they will therefore come with a definite feeling in 
 ommon , which feeling will be define still more 
 shen it find its most complete expression in the 
 susic . it be likely , moreover , that such an audience 
 ‘ ill quickly become aware of a common spirit 
 smongst its member , and thus the enthusiasm of 
 sery listener will be intensify by the conscious- 
 ness that itis share — and all this may happen before 
 he concert have begin . if this be true of a Wolf 
 ecital , much more will it apply to music by a 
 willy establish composer like Beethoven . 
 [ select Beethoven because until recently he be 
 svariably choose to represent music by writer 
 , non - musical subject , and be therefore very 
 sommonly assume to be ¢4e composer among 

 mposer a composer of limited vision will 

 jaw an audience of like nature ; but a really 

 eat composer will bring together a body of 
 people 
 fele , enrich by wide difference of opinion 
 » music and life in general . far , a Beethoven 
 udience will surely feel more enthusiasm from the 
 nsciousness that so many other music - lover , 
 ast and present , could share their enjoyment . 
 nally , we may note that a performer will work 

 an enthusiasm for a particular composer most 
 shen he make a habit of play that composer . 
 when he give a one - man recital only on occasion 

 give . or again , we be in an anti 

 beethoven period and , feel we miss so much , | 
 who | 
 in the| 
 in such instance the common interest be 

 go to see if Kussewitzky , the conductor 

 next we may take the average programme , in 
 Fach there be enough of one or more composer to 
 constitute some common interest in a potential 
 wdience , but a certain variety be aim at in order 
 catch more people . the mere step from one 
 ‘ omposer to two may often be a wide step in the 
 direction of variety . Bach - Liszt , the subject of a 
 recital ( one of six ) give by Moiseiwitsch some 

 year ago , be an instance . those who like Liszt 
 must surely find Bach unromantic , and those who 
 like Bach must hate Liszt , except in so far as Bach 
 anticipate the modern extravagance exploit by 
 Liszt . similarly , Mr. Samuel ’s recital of x. and 
 british composer , x. be a classic ’ and there- 
 fore generallya german composer . the opposition 
 of the british and german character be pretty 
 strong . on the other hand , it be quite possible to 
 present the same general feeling through the mouth 
 of different composer : for example , a recital of 
 elizabethan music , or of Zzeder , or of the german 
 classical school ( Bach - Beethoven - Brahms , a_pro- 
 gramme give by Miss Hess and other ) . such 
 | programme will organize some common mode of 
 feeling in the audience they attract , though less 
 definitely than the one - man programme , and there 
 | will be less reciprocal influence between individual 
 |member . they be , however , a practical com- 
 promise , for it may take some time to work up an 
 audience for a one - composer recital 

 unite by a solid block of common 

 in 

 as regard reduction , we 
 sliminate such as _ consist reduce an 
 orchestral work or theatrical score for the 
 pianoforte ; for whereas by this process esthetic 
 may theoretically , it acquire practical 
 advantage which it be unnecessary to state . we 
 should regard it out place to give , in a 
 large concert - hall , transcription of Beethoven ’s 
 Symphonies for four hand , see that they be 
 éS maisons , as a master 
 once say in the day of the renaissance . 
 Liszt also remark , these transcription play the 
 part of engraving that reproduce picture . to 
 continue our comparison , let we suppose that 
 reduction of 
 ment , enrich with a few ' wood ' effect 

 lose 

 Stravinsky ’s new Pianoforte Sonata 

 the first movement seem early Beethoven , the 
 second a Bach Partita , both slightly ( as the housewife 
 say ) ' on the turn , ' but not yet sour 

 it would be hard to find a well description of the 
 curiously perverse quality that mark so muc 
 music . it claim to be ' advanced 

 when he be a choir - boy , have find that ' their 
 music have stand the test of constant usage far well 
 than that of any other composer 

 I take it that there be few guide who be more 
 affectionately trust for their sanity of judgment , 
 | broad - mindedness , and knowledge , than Mr. Harvey 
 Grace ; and so ! have no doubt that this book ( reprint 
 from paper in the Musical Times ) will be keep 
 who read these line . ' those 
 who know the music will enjoy the exposition of its 
 quality , which do not lack the touch of enthusiasm 
 that bespeak the real music - lover keen on help 
 anyone who be interested , to taste the joy for 
 himself ; those who do not know Rheinberger have 
 here matter that , with the plentiful illustration , will 
 show they how to get the good out of the Sonatas , and 
 can not fail to stimulate many to search for beauty 
 in other department of music . here be something 
 of the gusto that make Grove ’s book on the 
 Beethoven Symphonies so enjoyable , apart even from 
 what it teach 

 Mr. Grace comment on Rheinberger ’s excellence 
 as a writer of variation . why be there so few good 
 modern set for the organ , of some scope ? if we 
 more with the brahmsian concentration and 
 breadth of a set by Harford Lloyd ( Augener ) , that I 
 remember strike I as unusually good a few year 
 ago , we should be happy . there be no form in which 
 player and audience be more interested , when the 
 variation really have meat in they . Rheinberger , 
 both in his fugue and in his variation ( especially in 
 his grind - bass treatment ) , seem to I to have more 
 than a tincture of those brahmsian quality , and of 
 the big - chested freedom of stride of Bach . some of 
 the meditative slow movement , and certain of the 
 short piece , be as good as anything ever write in 
 that kind for the organ 

 12 the MUSICAL TIMES — Octoser 1 1925 

 out of all the mass of interesting stuff at his disposal . 
 he have consult score galore , first edition , 
 ontemporary print , manuscript , & c. he begin 
 by discuss the instrument of the 17th century , 
 ind carry his study of method from the early| 
 effort of the 16th century , through the four- part | 
 string band of Purcell - Scarlatti , the Bach - Handel | 
 orchestra , the transition period of Gluck , the era of | 
 Haydn - Mozart , Beethoven - Schubert - Weber - Rossini , | 
 Meyerbeer - Berlioz - Mendelssohn -Glinka , Wagner , | 
 Krahms - Tchaikovsky , to that of Strauss - Debussy- | 
 Elgar , with a break halfway for a chapter concern 
 the instrument of the 19th century . there be an| 
 abundance of illustration , pictorial and music - type . | 
 the style be always interesting ( though not always | 
 areful : e.g. , there be a few split infinitive and | 
 some slip in spelling of proper name . particularly | 
 yood be the page devote to Berlioz , and very much | 
 to the point concern some contemporary composer 
 s the remark of Spohr concern Berlioz , quote by | 
 Mr. Carse : ' I have a special hatred of this etern : ul 
 speculate upon extraordinary instrumental effect . ' | 
 Louis should have live to - day , when so many } 
 score contain an intolerable amount of extraordinary 
 nstrumental effect to a mere ha’p’orth of musical 
 nvention . Mr. Carse ’s history well deserve to 
 vecome the standard authority on this perennially 
 nteresting subject H.G 

 T 

 mention in this list neither imply nor preclude 

 review in a future t sue . | 
 Beethoven . ' by Joseph de 
 Paris : Libraire Félix Alcan 

 quatuors de 
 pp . 403 

 year ; 
 sorry ! my mistake . 
 part . I read the statement 
 can not now trace the source ) , 
 my simple , trustful way 

 Che pick of the Columbia basket this month be 
 surely the reproduction of the Léner player in 
 Beethoven 's E flat Quartet , usually know as ' the 
 Harp ' ( four 12 - in . Che whole touch high - water 
 mark , both in playing and recording , and one need 
 especially good be the first movement 
 and the /’resfo , which come on the first and third 
 record of the set . the brilliance and precision of 
 the latter be remarkable 

 perhaps | be not a sufficiently keen Berliozian to 
 appreciate fully the ' Roman Carnival . ' I have never 
 get over my feeling that the opening section be dull , 
 and that ( as usual with this composer ) we do not get 
 an approach to his good until he his go full steam 
 ahead . hence my pleasure in the 12 - in . record of the 
 Hallé Orchestra ’s performance be almost confine to 
 the second side . I have an impression , too , that the 
 quiet side be less well record 

 VOCALION 

 here , as in the other parcel , we find a big classical 
 work — Beethoven ’s Pianoforte Concerto No . 4 , in G , 
 play by York Bowen and the Aolian Orchestra , 
 with Mr. Stanley Chapple conducting ( four 12 - in . ) . 
 this be unusually successful in regard to the piano- 
 forte tone . indeed , I can not recall so long a work 
 } in which the level be so high . Mr. York Bowen ’s 
 | fluent , happy style be well suited here . the orchestral 
 | part be at time too much in the background , but on 
 | the whole the balance be good . thisis one of the very 
 | good concerto record . only in the slow movement 
 do I feel some lack of significance — a not uncommon 
 experience in gramophone performance , and one for 
 |which the mechanical side be probably to blame . 
 | a capital fiddle record be the to - in . of Albert 
 | Sammons in Samuel Gardner 's ' from the Cane- 
 brake , ' and Dvorak ’s slavonic Dance No . 1 , arrange 
 by Kreisler . the Gardner piece be rather small beer , 
 but the playing of both be first - rate , and the repro- 
 duction all that can be desire 

 to many the chief vocal record will be that of 
 Luella Paikin in Benedict ’s ' La Capinera ' and 
 Bishop ’s ' Lo ! here the gentle lark ' ( or , as the singer 
 seem to prefer it , ' gentell ' ) . the fi very 

 1925 

 Debussy Quartet , the Beethoven E flat , the 
 Schénberg Sextet , & c. , at a subscription rate which 
 work out at five shilling a record — about 50 per cent . 
 less than the usual price for a 12 - in . of the same 
 standard . ' the late issue be the Beethoven Quartet 
 in F , Op . 59 , no . 1 . the player be again the 
 Spencer Dyke Quartet . I can not say that this set 
 of record strike I as be quite up to the level of 
 the good of the Society 's production so far . there 
 be more than a hint of doubtful intonation here and 
 there , and the quiet passage suffer from a pronounced 
 scratch that I be sure must evoke some pungency 
 from Mr. Compton Mackenzie . but there be much 
 that be really first - rate — the freedom and nuance of 
 the first movement and the delicacy of the 4//egre / to , 
 for example . reader who have not yet hear of 
 the Society , or , having hear , be yet tremble on 
 the brink , should write to the Secretary ( 58 , Frith 
 Street , W.1 ) for particular . now be a good 
 moment , for the Society ’s year start at Michaelmas , 
 and there be a scheme whereby new member may 
 make their membership retrospective , and so obtain 
 the privilege of the past year so long as the supply 
 of record hold out . | have not space to give the 
 detail . | add that the issue of the record of the 
 Brahms Sextet and the Mozart Oboe Quartet be 
 the Society deserve well of gramo 
 phonist ' especially such as be chamber music 
 enthusiast ) not only for undertake the recording 
 of work that might be too risky for the regular 
 company , but in the long run by encourage — one 
 might almost say goad — those company into 
 give more and more consideration to the claim of 

 imminent 

 i 
 after a bout of modernism . Mlle . Darré play j 
 with a delicate elegance that be admirably suitable 

 these three roll be all ' Duo - Art , ' Haydn ang 
 Brahms provide the straight - cut roll . the forme , 
 give we the first movement of his sonata in D , op . > 
 ( a.c. t24626 , a.s. 93470 ) , a jolly little piece 
 depend very largely upon manner of performanc 
 for its effect . play inattentively it be dull , but wit , 
 a proper rhythmic accent and observance of dynam 
 contrast it not only provide good practice , but js ap 
 excellent example of early sonata form . the Brahms 
 intermezzo , op . 116 , no . 5 , in E ( A.C , t2g549 ) 
 have a beauty which be the more precious for be 
 not too obvious . it demand and repavy carefyj 
 study , with a fairly negligent eye upon the accent 
 perforation . Beethoven ’s sonata , op . to , no , 2 , jp 

 f ( a.s. 93465/6 ) , I comment upon in the August 
 | issue . 
 | the ' animatic ' roll include a specially 
 distinguished performance of Debussy ’s ' reflets dans 
 | eau , ' by Walter Gieseking . I fancy that we have 
 | be privileged to hear he but twice in this country , 
 |when his playing be universally praise , its most 
 |remarkable feature be , I think , his wonderful 
 | command of the soft tone - colour . in this roll , of 
 | course , we be leave to emulate these to the good of ou 
 | } ability , but all the beauty of his Zemfo and nuane 
 be represent , supply a powerful stimulus to the 
 |imagination . I must add also that special attention 
 |must be give to pedalling . much of the effect of 
 |this music depend upon the subtle blending of 
 chord - colour with the aid of the sustain peda ' , 
 and if the automatic sustain device be not use — 
 |and this be notoriously unsatisfactory — I recommend 
 | my reader to observe the pedal perforation ver 
 | carefully , and to control the lever accordingly 

 the Daniel Jazz , ' for tenor , string quartet , trumpet , 
 percussion , and pianoforte Louis Gruenberg 

 though a Pole , and bear in Ukraine , Szymanowski | 
 at first favour german rather than slavonic 
 influence . he be then incline to a turgid , over- 
 crowded mode of writing in recent year he } 
 appear to have be strive to emerge from the fog , | 
 and in this new Quartet he show a great advance 
 towards clarity . it be a good Quartet , and should be 
 hear in London . Malipiero ’s super - cycle ( each song 
 be as long as any conventional scena ) be an important | 
 work , full of significance , demand a genuine 
 interpretative artist to sing it . the performance , by | 
 Spinella Agostini , with Alfredo Casella at the piano- 
 forte , be a fine one . the poem be from different 
 source and of varied style , each be impart 
 with a declamatory vocal line support by an| 
 amplified commentary from the pianoforte , which | 
 merge into interlude , so that the cycle is| 
 perform without a break . the sonority of six 
 trumpet be so captivating that Carl Ruggles deserve 
 double blame for not employ this feature to 
 well advantage than in his ' angel . ' it last 
 less than three minute and seem too long . from 
 Bach , to whom he have recently pay the tribute of a 
 concerto , Stravinsky appear to be work his way 
 through early Beethoven , for there be a good deal of 
 both in his Pianoforte Sonata , of which he give a good 
 performance , though suffer from a sore finger . 
 there be not much to carry conviction in the slow 
 movement of either the Concerto or the Sonata , but 
 the /7na / e of the latter hasan impressive vigour . the 
 open section be almost incredibly beethovenish . 
 fancy Mr. Steuart Wilson singe ' the Daniel Jazz ' ! 
 if one read Vachel Lindsay ’s poem he will realise 
 the miracle . and he sing it not merely well , but 
 with such conviction that Gruenberg want he to 
 sing it wherever it may be perform in Europe . 
 as the work ' catch on , ' this may involve his do 
 the ' grand tour . ' the setting , with occasional 
 spoken passage , be curiously compound . when 
 the composer remember , it be a Negro Spiritual , a 
 sort of sublimate ' when Joshua fit the battle of 
 Jericho , ' with a jazz accompaniment for small 
 orchestra . when he forget it be ' penny plain , 
 tuppence colour . ' but when we come to ' Daniel ’s 
 tender sweetheart ' we find to our amazement that 
 she be know in another incarnation as Sieglinde , 
 or maybe Eva . in short , it be untidy and even 

 of 

 though Handel and Mendelssohn monopolise 
 nearly the whole of two programme , Continent,| 
 composer be in a minority , if a strong one , fo ; 
 Bach , who have be somewhat neglect » 
 Gloucester , be represent by the unfamiliz , 
 cantata , ' give the hungry man thy bread , which 
 in view of its very picturesque and finely developed 
 open chorus , have unusual interest , and Brahms } y 
 the ' St. Antoni ' Variations . ' Tod und Ve . 
 klirung ' of Richard Strauss have long be favourej 
 by the Three Choirs , no doubt because it be one of 
 his few work which be suit to Cathedral per . 
 formance . it be give at Worcester in 1902 , 190 : 
 and 1923 ; at Gloucester in 1910 ; and if it seem 
 less sensational than in the early day , it have gain 
 something in intimate charm . Sibelius be to have 
 bring over a new Symphony , but it be not finish 
 in time , so we have to be content with ' Finlandia ' 
 which sound well in the Cathedral , and the popular 
 ' Valse Triste ' in the Shire Hall . that Verdi ' 
 ' requiem ' be favour by Gloucester be show by 
 the fact that this be its sixth performance sinc 
 its first appearance at a Gloucester Festival in 
 1901 , since when it have be absent from the 
 programme only once — in 1904 . it certainly have 
 an exceedingly fine performance on all hand , and 
 the quartet of soloists—-mis Dorothy Silk , Mis 
 Astra Desmond , Mr. John Coates , and Mr. Rober 
 Radford — be an admirable one 

 Mr. Norman Allin sing Hans Sachs ’s monologue , 
 * Wahn , Wahn , ' and Miss Dorothy Silk introduce a 
 Mozart aria , at Wednesday ’s concert , so two great 
 name be not entirely neglect . the great 
 absentee be Beethoven , whose ' Choral ' Symphony 
 might well have be include in this the centenary 
 year of its introduction to this country 

 in addition to the principal already mention 
 there appear Miss Agnes Nicholls , Miss Else 
 Suddaby , and Miss Flora Woodman as soprano , 
 Miss Margaret Balfour among the contralto , 
 Mr. John Booth and Mr. Gwynne Davies amonj 
 the tenor , Mr. Herbert Heyner among the bass . 
 special mention should be make of Mr. Keith 
 Falkner , a young baritone who essay the exacting 
 part of Job , and , in spite of his youthful personality 
 and voice , sing it with remarkable intelligence , ane 
 of Mr. Horace Stevens , who in one day appear in 
 the very different part of Elijah and Jesus ( ' the 
 Apostles ' ) , and achieve an unqualified success 
 which place he in the front rank of oratono 
 singer . the vividness of his impersonation 0 
 Elijah can never have be exceed . the London 
 Symphony Orchestra be engage , and Sir Ivor 
 Atkins and Dr. Percy Hull share the duty at the 
 organ , the pianoforte , and the celesta . finally , more 
 than a conventional word of acknowledgment be due 
 to Dr. Brewer , who , beyond his more obvious task as 
 conductor , have do so much to arrange the 
 programme , organize the performance , and manta 
 local enthusiasm at a high pitch . a new secretar ) 
 be in office in the person of Mr. A. A. G. Jones . 
 but with the hearty co - operation of his predecessor 
 Mr. P. Barrett Cooke , there be no hitch in the 
 machinery of the Festival 

 quaintly figure Ilarvey Grace ’s Postlude be a dart from 
 a seldom - ride blue , close with a problem climax chord 
 into the Infinite . — Guernsey Newspaper 

 dance to the Minuet of Beethoven ’s first symphony 

 would have split the seat of every pair of breech in the 
 | ballroom . — Compton Mackenzie 

 the more modern composer be constantly re- 
 pay the debt which they owe to their predecessor 
 at first by secure full appreciation for they after- 
 wards 

 thus , while the composition of great master such as Bach 
 and Beethoven , represent as they do culminate point 
 olution of the art , have eclipse the effort of 

 in the ev 

 val be now affiliate , and several new festival | : page : + i 
 | performance of Bach ’s ' St. Matthew ' Passion 

 Beethoven , Delius , & \c.——aa series of five chamber concert 
 be announce by the Catterall ( Quartet . first performance 
 at Birmingham will be give of Brahms ’s Clarinet ( ) uartet , 
 Op . 115 , Alfred Wall ’s ' Idyll , ' Casella ’s Concerto ( ) uartet , 
 and Dvorak ’s Quartet in G major . Beethoven , Bax , and 
 Tchaikovsky be among the name that figure in the pro- 
 grammes.——lIt be understand that the ' Mid - day ' concert , 
 under the directorship of Mr. Johan Hock , will be confine 
 entirely to instrumental music . a number of programme 
 will be give by the Philharmonic ( uartet , and there will 
 be concert by the Beatrice Hewitt Trio , by Mr. William 
 Primrose , and by Miss Lucy Pierce and Mr. Charles Kelly 
 in duet for two pianofortes.——The principal artist in the 
 ' international celebrity ' series be John McCormack , Heifetz , 
 Frieda Hempel , the Léner ( Juartet , Pachmann , and Elena 
 Gerhardt.——The name of Elizabeth Schumann , Cortdt , 
 Casals , Dorothy Silk , Maurice Ravel , and Arthur Rubenstein 
 appear among other in the programme for the forthcoming 
 series of Max Mossel concerts.——The Festival Choral 
 Society have four concert in its subscription series this 
 season . Vaughan Williams ’s ' Sea ' Symphony , Parry ’s 
 ' Job , ' and a Mozart ' Mass ' figure in the scheme . — — 
 Purcell ’s ' Dido and .1ineas ' be to be give by the City of 
 Birmingham Choir . the Midland Musical Society will be 
 hear in a performance of Dvorak ’s ' Stabat Mater ' and 
 Holst ’s ' hymn of Jesus , ' under the directorship of the new 

 conductor , Dr. Darby . an outstanding feature of the 

 Music in 

 oo ' HAM and DistRict.—In addition to a series 
 ~ ' wenty - five Sunday concert , eight symphony concert 
 a be give this season by the City of Birmingham 
 eerie one of these programme will be con- 
 " » by Vaclav Talich , of Prague ; in another , Holst 
 hon on his * Fugal Overture ' and the Scher 20 
 8 * rom his Choral Symphony . symphony by 
 Jc " , Beethoven , Dvorak , Vaughan Williams , and 
 " tock will be give , as well as Concertos by Bach 

 individual and unrivalled way ) , which tax the resource of 
 Usher Hall , and send every one away inspired and happy . — — 
 the prospectus of the thirty - sixth season of the Paterson 
 Orchestral Concerts at Edinburgh , for which the scottish 
 Orchestra have again be engage , be on similar line to the 
 Glasgow scheme set out below , but find rocm for some 
 individual difference , the most interesting of which be , 
 perhaps , the inclusion in its survey of one of our young 
 british conductor , Dr. Malcolm Sargent . the promoter , 
 Messrs , Paterson , Sons   Co. , one of the oldest - establish 
 music house in the country , point out in a foreword 
 that they have carry on these concert at their own 
 financial risk for the last thirty - five year , and they 
 appeal for increase support , as since the war it have be 
 possible to run the season only at a loss . — — the 

 Balling be 
 bring to Edinburgh and Glasgow by Denhof to conduct 
 in Scotland 

 the first performance of ' the Ring ' give 
 he also conduct a notable Beethoven Festival at 
 Edinburgh , which extend over a week . 
 GLascow.—A fourteen - week ’ season of the scottish | 
 ’ echestra be cover by the prospectus of the Glasgow | 
 horal and Orchestral Union , just issue . the con- 
 juctor engage be Abendroth , Weingartner , Mlynarski , 
 Talich , and , for the choral concert , Wilfrid Senior , a 
 feature of special interest be the large number of unfamiliar | 
 wk include in the orchestral programme . the choral | 
 work announce ar Bantock ’s ' Omar Khayyam ' 
 part 1 ) , Brahms ’s ' Requiem , ' Debussy ’s ~*~ Blessed 
 Damorel , ' the Coronation S from * Boris , ' and ( con- | 
 ducte by Weingartner ) the ninth Symphony . néwcomer | 
 among the solotst be Mile . Irene Dobieska , the polish | 
 violinist , and Mijn . Lidus Van Giltay , the dutch violinist . | 
 the many admirer of Mr. Philip Halstead , the veteran 

 Glasgow pianist , will welcome his re - appearance as a soloist | 
 ift too long an absence from the scottish Orchestra | 
 concert che programme of the Glasgow Bach Society | 
 conductor , Mr. J. Michael Diack and Mr. F. H. Bisset ) | 
 include two chamber concert by the Society ’s chamber } 
 orchestra , part of the * Christmas ' Oratorio , the Motet | 
 ' Jesu , meine Freude , ' Psalm 109 and Psalm 121 ( two 
 p he s Grawn from the lesser - know Church Cantatas of | 
 Bach by Ivor Atkins and J. Michael Diack ) , and _ the 
 St. Matthew ’ Passion . all the Church work 
 ill be sing in Glasgow Cathedral . — — the   pro 

 production of 

 aa , 
 on this occasion as he show himself to be in Brahms ’s 
 second Symphony . nor be Dr. Muck , with his stern 
 and rigid method , _ as fine a conductor for Mozart ’s 
 * Don Juan ' or for his G minor Symphony as he prove 
 to be for Beethoven ’s ' Eroica . ? Muck ’s sharp profile 
 which have age very much in recent year , look more 
 remarkably like Wagner ’s now than ever before , the 
 concert direct by Schalk — the Vienna Philharmonic 
 Orchestra be the instrument for all three conductor — 
 bring forth excellent performance of Schubert ’s ' up . 
 finish ' and Bruckner ’s seventh Symphony . the cycle of 
 concert be fill out with a number of ' chamber concert ' 
 which , aside from those give by the Rosé ( uartet and by 
 the Wood - wind Society of the Vienna Opera ( in conjunc . 
 tion with Rudolf Serkin , the pianist ) , resolve into a series 
 of pleasing if unassuming song recital by Madame Ivogun 
 ( in conjuction with her husband , the tenor Karl Erb ) , 
 Madame Lotte Schone , Soprano of the Vienna Opera , 
 Richard Mayr , and Josef Schwarz 

 Mozart music — compile by Einar Nilson into a balle 
 entitle ' the Green Flute ' — furnish the vehicle also 
 for the first appearance anywhere of a new ' International 
 Pantomime Company , ' launch under the name of Max 
 Reinhardt , but in fact stage - direct by Ernst Matray , a 
 well - know dancer . much have be anticipate from this 
 venture towards the modernisation of pantomime as 4 
 specie ; whatever have be attempt in this field by man 
 like Stravinsky ( in ' Petrouchka ' ) and Béla Bartok ( in ' the 
 Wooden Prince ' ) have remain more or less entangled in 
 the fetter of choregraphic display . those who have 
 expect more from the ' International ' Company be 
 bound to be disappoint , for it give little more than mere 
 dancing — and rather antiquated and uninteresting dancing — 
 in conjunction with a rather naive stage management . there 
 be not even an attempt to make pantomime a medium , as 
 it be , whereby to ' hold up the mirror to a con 
 temporary life ' in a satirically witty , even grotesque ! 
 comical manner ( which would seem to be its mission 
 true that neither Mozart ’s music , nor that of Muflat — 
 compile into an insignificant marionette play — give wide 
 scope for such aim . but the fault lie with those who 
 make such an unwise choice 

 ancient 

 interesting to your congregation ; they also help in a 
 right performance . however , if you want more , consult 
 the historical edition of Hymns A. & M. , or J. T. 
 Lightwood ’s ' Hymns and their Story ' ( Epworth Press ) . 
 ( 2 . ) Tliffe ’s ' Analysis of Bach ’s 48 Preludes and fugue ' 
 Harding ’s ' Analysis of Beethoven ’s sonata ' ; and 
 * Beethoven ’s Pianoforte Sonatas : hint on their rendering , ' 
 by C. Egerton Lowe , all publish by Novello 

 INQUIRER.—(I. ) the right of public performance be 
 reserve in order to prevent performance from copy 
 purchase or hire elsewhere than from the publisher . you 
 will have no difficulty in obtain permission in the case of 
 the work you mention , provide you obtain the copy from 
 the publisher , ( 2 . ) the examiner be not bind by the 
 rule of any book on counterpoint , and will accept all good 
 musicianly working . Prout ’s work on the subject be not , 
 we think , up to the standard of the rest of his series of 
 text - book , and we prefer the two other you mention . you 
 should also see Kitson ’s ' Applied Strict Counterpoint ' and 
 ' Counterpoint ' ( Clarendon Press 

 een . n atior f scale t 
 anthem . \withi the great composer . a scric of pen 
 pict « } t n the for f Inter 

 he \ ds by t tic ee v i reat tone | 
 BAXTER . | by Gerald Crmberland . Crown vo , 
 ose for the 27lst FESTIVAL OI BEETHOVEN ’S nine symphony fully describe 
 on of the clergy 1 ana d \ ser ter 2 

 BY 

 ines , \ ( ) \ \ | \ \ erroneous or insufficient 
 a » ia hbhnnen eh he . 
 ynatas ( it be know that the } de ing j 
 . am fing 9 destine 
 asascmemtienedineiinmeiientie ' a 4 4 ' \ modern ring destine 

 to ensure to a great degree 
 the order of composi the musical accent , 
 in relation to the usual with copious explanatory note 
 n of Beethoven ’s the complete indication of 
 modern pedalling (: 
 edit , RE - finger , frequency wit 
 change _ the 
 and correct nowadays 
 . , , indication / 
 emati in x ( he . 
 practicabk 
 these indication 
 he price of eacl ! substitute t i 
 net r to y ) ~ » ry a ' Py ( 
 - het ( postage a ALFRED CASELLA tion 

 paper covers—()uart the length 

 Instrumental Band , the ercy E. Fletcher 

 March from ' * ' Egmont " Beethoven 

 EI » and Company , i 
 . i ~ . r x = 
 March from ' ' St. Polycarp " F. A. G. Ouseley S L | | | 
 — _ 

