WepneEspAy Morninc—‘ELIJAH " ( Mendelssohn ) , Madame 
 Albani , Master Frank Gardner , Miss Hilda Wilson , Fraulein Witting , 
 Messrs. Edward Lloyd , Braxton Smith , and Andrew Black 

 WEDNESDAY Eventnc — overture , ' ' Leonora , " no . 3 ( Beethoven ) ; 
 " Blest pair of Sirens , " Ode by Milton ( Dr. C. Hubert H. Parry ) ; 
 Scena , ' Ocean , thou mighty monster " — ' Oberon " ( Weber ) , Fraulein 
 Malten ; Grand Symphony , " no . 2 , in d major " ( Brahms ) ; Aria from 
 " Armida " ' ( Gluck ) , Fraulein Malten ; overture , ' Othello " ( MS . ) , 
 ( Walter Macfarren ) ; overture , ' ' Carnival " ( Dvorak ) ; selection from 
 " Gotterdammerung ' — Funeral Marchand Closing Scene ( Wagner ) — 
 Briinnhilde , Fraulein Malten ; rhapsodie in F minor and major ( Liszt ) ; 
 Choral Ballad , ' Sir Patric Spens " ( Pearsall ) ; overture , ' ' Tann- 
 hauser ’' ( Wagner ) . vocal solo by Mr. Braxton Smith , & c 

 THurspAy Morninc — overture , ' ' in memoriam " ( Sullivan ) ; 
 " REQUIEM MASS " ( Gounod)—Posthumous work , first perform- 
 ance in England — Madame Albani , Fraulein Witting , Messrs. Lloyd 
 and Black ; ' ' hear my prayer " ( Mendelssohn ) , Madame Albani ; 
 symphony inC minor , no . 5 ( Beethoven ) ; oratorio , ' ' creation , " 
 part i. and ii . ( Haydn)—Madame Albani , Messrs. Lloyd and Black 

 Tuurspay Eveninc — overture , ' Manfred " ( Schumann ) ; new 
 Cantata , ' hymn before Sunrise , ' Poem by Coleridge ( P. Napier 
 Miles)—Soloist , Mr. D. Bispham ; song , ' Lorelei ' ( Liszt)—Fraulein 
 Malten ; orchestral suite in d minor ( e. German ) ; Schmiedelieder , 
 from " Siegfried " ( Wagner)—Siegfried , Mr. Ben Davies ; Mime , Mr. 
 Braxton Smith ; Vorspiel and Liebestod , ' Tristan and Isolde " 
 ( Wagner)—Isolde , Fraulein Malten ; overture , " ' Land of the Mountain 
 and the Flood ’' ( MacCunn ) ; selection from ' ' Die Walkiire , " Act 3 , 
 include the Walkirenritt , Duet , the Feuerzauber , and Wotan ’s 
 Abschied ( Wagner)—Briinnhilde , Fraulein Malten ; Wotan , Mr. D. 
 Bispham ; rhapsodie in D minor ( Liszt ) ; overture , " Oberon " 
 ( Weber 

 NORTH STAFFORDSHIRE MUSICAL 
 FESTIVAL 

 President : the Duke oF SUTHERLAND . 
 October 29 and 30 , 1896 . 
 TuurspAy Eventnc . — hymn of praise , ' Mendelssohn ; " the 
 Lord be King , " Barnby ; ' " mount of Olives , " Beethoven . 
 Fripay Morninc . — " King Olaf , " Edward Elgar ( specially write ) 
 and miscellaneous . 
 Fripay Eveninc . — the spectre ’s bride , " Dvorak ; ' choral 
 symphony , " Beethoven . 
 Miss Ella Russell , Miss Medora Henson , Miss Marie Hooton . 
 Mr. Edward Lloyd , Mr. Ben Davies , Mr. Andrew Black , 
 Mr. Ffrangcon - Davies . 
 conductor : Dr. Swinnerton Heap . chorus - master : 
 Mountford . solo Violin : Mr. Willy Hess . 
 programme may be have from the Hon . Gen. Secretary , Mr. H. J. 
 Wildin , Stoke - on - Trent 

 Mr. Fred 

 BACH . — " Passion " ( St. Matthew ) .. ai me ee 

 BEETHOVEN . — " Mount of Olives " .. ( paper board , 1 

 Tonic Sol - fa ) 
 BENNETT . — " the May Queen " .. ( paper board , rs . 6d . ) 
 ELGAR . — King Olaf ” ’ .. ne ss ( Tonic Sol - fa ) 
 GLUCK . — " Orpheus " .. i ae ee ( Tonic Sol - fa ) 
 GRAUN , C. H. — ' the Passion " ( ' Der Tod Jesu " ) .. as 

 BACH ’S music in ENGLAND 

 Joun SepastiAN Bacu die in the year 1750 . 
 very few of his composition , and those be 
 amongst the lesser known , be print during 
 his lifetime , and few still in the fifty year 
 immediately follow his death . it be not 
 until the nineteenth century have fully dawn 
 that the transcendent genius of this great 
 musician become manifest , and even then in a 
 very limited degree . english musician , steep 
 in handelian and other tradition , regard the 
 music of the great Cantor with a distrust bear 
 of prejudice against anything new . in later 
 year Beethoven and Wagner furnish instruc- 
 tive parallel . it be ever thus ; and in the 
 case of Bach it need all the red - hot enthu- 
 siasm of such disciple as Felix Mendelssohn , 
 in Germany , and Samuel Wesley , in England , to 
 kindle the flame of Bach devotion which now 
 burn in the breast of every true musician 

 the question might naturally be ask : 
 " what do J. C. Bach , the ' english Bach , ' do 
 for the propagation of his father ’s music in 
 England ? " the answer , so far as we know , 
 be " nothing . " John Christian Bach , the 
 eleventh son of Sebastian Bach , live in 
 London from 1759 ( or 1762 ) till his death in 
 1782 . he be a well - know figure in musical 
 and fashionable circle , and enjoy all the 
 prestige attendant upon his appointment of 
 ' « * music - master to the Queen . " in conjunction 
 with C. F. Abel , the eminent viol - da - gamba 
 player , he give Subscription Concerts at the 
 Hanover Square Rooms , which be thus 
 " open " by they on February 1,1775 . here 
 and elsewhere he have splendid opportunity of 
 bring forward his father ’s chamber music , 
 if he have be so disposed . Samuel Wesley 
 record that J. C. Bach call his father " the 
 old wig . ' but whatever filial affection and 
 respect he feel towards his illustrious parent , 
 J. C. Bach fail to show it in any public 
 performance of his father ’s music . the artistic 
 creed of the english Bach may be summarise 
 in his own word , say in relation to his 
 brother , C. P. Emanuel Bach : " my brother 
 live in order to compose ; and I compose 
 in order to live . " the fact be that J. C. 
 Bach be only a Bach in name . in his life , 
 his art , and in his religious belief he cast 
 off all the good tradition of his distinguished 
 kinsman . he live in great style in London 
 and keep his carriage ; but he die in poverty 
 and in debt , one of his creditor be his own 

 beyond the actual effect of the performance to 
 judge by 

 enlarge upon this thesis he remind we 
 that Wagner — whose love for Mozart ’s instru- 
 mental music have only be excite when , on 
 conduct it himself , he come to recognise 
 the beauty of its cantilene — in his youth have 
 attend a performance of the Ninth Symphony 
 at the Leipzig Gewandhaus , when , though he 
 have copy out the score for himself and have 
 make a pianoforte arrangement of it , he be 
 so much astonished at its utterly confused and 
 bewilder effect that he lose courage , and 
 for a time give up the study of Beethoven . in 
 1839 he receive a good lesson in Paris , where 
 he hear the orchestra of the Conservatoire 
 rehearse the enigmatical Ninth Symphony . 
 ' ' the scale , " Wagner write , " fall from my 
 I come to understand the value of 

 rs 

 with wrathfull tempest arm all his Hoast ; : t 
 when ( angry ) stretch his strong , sinewy arm , correct execution , and the secret of a good 

 with bend back he throw down thundry storm ; performance . the orchestra have learn to 
 the harmonious sigh of his heart - turn Sheep look for Beethoven ’s melody in every bar — that ' 
 oe 2 g pa - prseciagohaey iio melody which the worthy Leipzig musician . 
 ile mud - ey avlercy Steale rom his han ; ' ° a 
 the sulph’ry Plagues prepare for sinfull Man . have fail to discover ; and the orchestra sate 

 that melody . this be the secret 

 591 

 it be Habeneck , who , though not a con- 
 ductor of special genius , but one whom all 
 obey , solve the difficulty by perseverance in 
 rehearsing , until Beethoven ’s new melo — i.e. , 
 melody in all its aspect — be fully com- 
 prehend and correctly render by every 
 member of the orchestra 

 2 . thus the proper tempo be fix by the 
 character of the melody 

 3 . the occasional modification of tempo 
 which Wagner recommend he , doubtless , 
 learn from Weber , who write : ' time 
 should never be a tyrannical mill - work ; it be 
 in music what the pulse be in the human body . 
 there be no slow movement in which there 
 may not occur the necessity of an ' accelerando , ' 
 in order to avoid a sensation of dragging ; 
 there be no ' presto ' in which passage may 
 not be find require a ' ritardando , ' in order 
 to escape a sense of hurry 

 Herr Weingartner fully accord with all that 
 Wagner have put forth in regard to the proper 
 treatment of classical work , even to express 
 his surprise , as other have do before he , 
 that any conductor should dare to bring for- 
 ward the Ninth Symphony , except in accord- 
 ance with the propose emendation in the 
 instrumentation , which Wagner , as the result 
 of his Bayreuth performance in 1872 , set forth 
 in an article , ' Zum Vortrag der Neunten 
 Symphonie Beethovens , " which in the follow 
 year he contribute to the Musikalisches 
 Wochenblatt , and which have since be incor- 
 porate in Vol . ix . of his ' collected Writings 

 Wagner hold the post of conductor in Dresden 
 from 1842 to 1849 , when the Revolution of that 
 year drive he into exile , from which he do 
 not return till 1860 . this enforce absence 
 from Germany , and the fact that he never 

 Herr Weingartner do not confine himself 
 to speak of the eccentricity of conductor , 
 but have a good deal to say about the unpleasant- 
 ness which the post of opera - conductor in 
 Germany generally involve . nor be he silent 
 as to the trick which , Eulenspiegel - like , he 
 have himself play off upon the public and 
 critic . he amusingly relate that some ten 
 year ago he and his friend Alfred Reisenauer 
 have agree to play Liszt ’s Symphonic Poem 
 " Die Ideale " and his ' ' Faust " Symphony , 
 arrange for two pianoforte , at a Concert of 
 the Cassel Wagner Society . the committee 
 beg that they would not introduce two 
 composition of the " horrible " Liszt , as this 
 would drive the public away . they stick to 
 their resolution , play both work , but 
 announce " Die Ideale " as a ' " Fantasia for 
 two pianoforte , by Franz Schubert ( after the 
 ' unfinished ' Symphony , the master ’s last 
 work ) , " which naturally do not exist . it 
 be most warmly receive by the public , and 
 be generally speak of by the press as a 
 " pearl of schubertian melody . " only one of 
 the critic discover in it ' an element which 
 be strange to and far - fetched for the lyrical 
 Schubert . the " Faust " Symphony , on the 
 other hand , be roundly abuse 

 while on tour in 1883 he play at Diissel- 
 dorf , as no . 5 on the programme , Beethoven ’s 
 Sonata , Op . 10 g. by an accident copy of the 
 programme of a previous Concert which he have 
 give in another place be distribute among 
 the audience . except as to no . 5 , which figure 
 in this as ' ' Pianoforte piece by Weingartner , " 
 the two be identical . to his astonishment 
 the next morning he read in a Disseldorf 
 paper : " Herr Weingartner , of Vienna , scorn 
 precaution , and for his début here play some 
 new pianoforte piece of his own , which certainly 
 serve to display his virtuosity , but beyond 
 this be of no account . one may be a good 
 executant , but at the same time an indifferent 
 composer . ' the poor man , who do not know 
 Beethoven ’s Sonata , have evidently get hold of 
 the wrong programme , and be thus lead to speak 
 of Beethoven as an indifferent composer 

 the above brief and very incomplete sketch 
 of the content and character of Herr Wein- 
 gartner ’s little book will suffice to show the 
 kind of book it be . have he abstain from so 
 cruelly make Bilow a scapegoat for many of 
 his remark — Biilow , who so often during his 
 lifetime be justly speak of as an incom- 
 parable artist — and have he heap less abuse 

 the series of Concerts give by the Queen ’s Hall 
 Choral Society , under the direction of Mr. Robert 
 Newman , will consist of ten Concerts , commence on 
 November 5 and extend to May 27 . the following 
 work will be perform : the ' Creation " ( Haydn ) , 
 ' * Samson and Delilah " ( Saint - Saéns ) , ' " ' the Messiah " 
 ( Handel ) , ' ' Elijah ' ? ( Mendelssohn ) , " golden Legend " ' 
 ( Sullivan ) , ' ' Stabat Mater " ( Rossini ) , ' ' hymn of Praise " 
 ( Mendelssohn ) , ' Faust ’' ( Berlioz ) , ' * Redemption " 
 ( Gounod ) , and " St. Paul " ( Mendelssohn ) . Mr. Randegger 
 will conduct , as usual 

 the National Sunday League Musical Society will 
 perform the following work : Sullivan 's ' golden Legend , " 
 ' * Prodigal Son , " and ' " Light of the World " ; Gounod ’s 
 ' ' Redemption " ; Mackenzie ’s " ' Dream of Jubal " ; Handel ’s 
 " Messiah , " ' Samson , " ' Judas Maccabeus , " and 
 ' ' Jephtha " ' ; Mendelssohn ’s ' Elijah , " ' St. Paul , " 
 ' ' Athalie , " " Lobgesang , " and ' ' Lauda Sion " ; Haydn ’s 
 " creation " ' and ' " ' the season ’' ; Beethoven ’s ' ' Mount 
 of Olives " ; Rossini ’s ' Stabat Mater " ; Barnby ’s 
 ' " ' Rebekah , " & c. the performance will be give every 
 Sunday evening from October to May , under the direction 
 of Dr. Churchill Sibley 

 the Richter Concerts ( under the direction of Mr. N. 
 Vert ) will be give on Monday evening , October 109 , 26 , 
 and November 2 

 La Scala be redolent as a sepulchre of faded garland , 
 withered rose ; only the golden chain of art continue . 
 unbroken , its link forge by live genius from one 
 generation to another 

 music at the funeral of SIR JOHN MILLAIS . 
 music form an important feature at the funeral of Sir 
 John Millais , at St. Paul ’s Cathedral , on the 2oth ult . 
 the Cathedral clergy and choir meet the body at the West 
 door , and precede it towards its temporary resting - place 
 under the Dome , singe the open sentence to Croft ’s 
 music . then follow Psalm xc . , sing to the Rev. F. A. J. 
 Hervey ’s chant in F minor . the reading of the lesson by 
 Canon Newbolt be follow by Brahms ’s anthem ' ' behold , 
 all flesh be as the grass . " at the graveside the sentence 
 be sing to the music by Croft and Purcell , follow by 
 Dr. C. V. Stanford ’s anthem " I hear a voice from 
 Heaven " and the hymn " the saint of God , " ' to the tune 
 by Sir John Stainer . the ' * dead march " in " Saul " be 
 then play , but so slowly as to entirely lose its charac- 
 teristic as a march . Beethoven ’s ' equale " ' be play 
 by a quartet of trombone , as at Lord Leighton ’s funeral , 
 before the procession arrive at the church . it be a pity 
 that the solemnity of such occasion should be»marre by 
 the gesticulation of a conductor as the body be bear 
 from the west door . surely such a choir as that of St. 
 Paul ’s Cathedral could sing in time without such aid 

 AUGUSTUS HARRIS MEMORIAL FUND 

 SONDERSHAUSEN.-—A new opera , entitle ' ' Riscatto , " by 
 Herr Otto Goetze , be bring out at the Stadt - Theater 
 last month with considerable success 

 StutTrGart.—The fifth General Festival of german 
 male choral society , hold here on the ist to the 4th ult . , 
 bring together some 15,000 singer from all part of 
 Germany and Austria - Hungary , who testify , severally 
 and conjointly , to the excellent training accord to these 
 characteristic teutonic institution . two principal per- 
 formance take place , on August 2 and 3 respectively , the 
 former open with Beethoven ’s hymn " Die Himmel 
 riuhmen " and conclude with Mendelssohn ’s Festgesang 
 ' ' An die Kistler , ' the latter include , as one of the 
 specially appreciated number , Bruckner ’s ' ' Germanen- 
 zug . " the King and Queen of Wurtemburg be present 
 on both occasion , the town be , of course , en féte during 
 the whole time 

 the Hacue.—M. Stortenbecker , Court pianist , have 
 receive a decoration from the Queen Regent , in acknow- 
 ledgment of his service render as teacher to the youthful 
 Queen Wilhelmina , who have complete her musical study . 
 — — M. Richard Hol be report convalescent , but it be 
 doubtful whether he will be able to resume his professional 
 duty . the veteran artist have just complete his seventy- 
 first year 

 Dr. Hans RicuTeER , have address the follow letter 
 to the Editor of the Times : — ' ' Sir,—Both in the column 
 of the Times and in those of other english paper the 
 grow influence of Richard Wagner 's son , Herr Siegfried 
 Wagner , in the management of the festival performance 
 at Bayreuth , have be animadvert upon in a tone which 
 be very severe for the promising young man , and unjust 
 towards the able and conscientious manager of the Fest- 
 spiele . nevertheless , I should not have think it necessary 
 to join in the discussion , confident as I be that time wiil 
 fully justify the high opinion Frau Wagner entertain cf 
 her son ’s ability , have not my name be introduce in a 
 manner that almost make it look as if there be at least 
 a latent opposition between the lead factor of Bayreuth 
 and myself in respect to Herr Siegfried Wagner ’s participa- 
 tion in the artistic work . I beg permission to declare 
 publicly , through the medium of your esteemed journal , that 
 this be not the case . I be present at the rehearsal lead by 
 Herr Siegfried Wagner , and if his performance have be 
 in any measure unsatisfactory I would not have fail , as 
 one of the old friend of the family , to express my most 
 decide opinion against his be intrust with such a 
 heavy responsibility . I have hear Herr Siegfried Wagner 
 conduct , and I have see he at work as stage - manager . 
 in my humble opinion he be a competent , and even a 
 remarkable leader , and he be a stage - manager of great 
 promise 

 the special performance at Munich of opera by 
 Wagner and Beethoven commence on the 8th ult . , at the 
 Royal Theatre , and have attract numerous visitor to 
 the bavarian capital . the follow be the order of the 
 remain representation during the present month — viz . , 
 ' " ' Tannhauser , " on the 3rd , 17th , and 2gth ; ' " ' Lohengrin , " 
 on the 5th , rgth , and 26th ; ' ' Tristan und Isolde , " on the 

 610 

 will be perform : ' " hymn of praise , " Mendelssohn ; | sazione . 
 " the Lord be king , " Barnby ; " Mount of Olives , " Beet- 
 hoven ; ' King Olaf , ' Edward Elgar ( specially write 

 the Spectre ’s Bride , " Dvorak ; ' ' Choral Symphony , " 
 Beethoven . the artist engage be Miss Ella Russell , | 
 Miss Medora Henson , Miss Marie Hooton , Mr. Edward | 
 Lloyd , Mr. Ben Davies , Mr. Andrew Black , and Mr. | 
 Ffrangcon - Davies . Mr. Willy Hess will be solo violinist , | 
 and Dr. Swinnerton Heap will be the conductor 

 Mr. G. F. GEAussEnrT have withdraw from the position 
 of Principal at the Hampstead Conservatoire , and that | 
 institution will in future be direct by an Executive Council | 
 of prominent musician . the member of the first Council | 
 be Mr. Frederic H. Cowen , Mr. Joseph Bennett , Mr. | 
 George Henschel , Mr. Julian Marshall , Professor Ebenezer | 
 Prout , Mr. G. F. Geaussent , and Mr. Arthur Blackwood | 
 ( ex - officio ) . the new Council have make the follow | 
 addition to the staff : Herr Wilhelmj ( violin ) , Signor | 
 Franco Novara ( singing ) , and Miss Cowen ( elocution 

 296 . hymn to nature 7 a - .   L.Streabbog 3d . 
 297 . Dickory , dickory , dock " - - . Herbert W. Schartau 3d . 
 298 . whither away ? + e ee oe c , Villiers Stanford 8d 

 299 . summer .. ee ee ee ee Hamilton Clarke 4d . 
 300 . tothe wood .. on ée a re bs 4d . 
 301 . noble be thy life a « e ee ee Beethoven 3d . 
 302 . sothe world goesround .. oe « > Marie Wurm 2d . 
 303 . softly the moonlight .. f , Iliffe 3d . 
 304 . you steal my love ( arrange by F. Maxson ) W. Macfarren 2d . 
 305 . moonlight ee " e ue ea Hamilton Clarke 4d . 
 306 . the snow ee ee oe oe ee .. E.Elgar 6d . 
 307 . fly , singe bird ee e oe ee + s - 6d . 
 308 . to - day and to - morrow oo " , ae Hamilton Clarke 4d 

 to be continue 

 CHIGWELL.—Speech - day at the Grammar School include 
 a Concert , at which a good selection of part - song , & c. , 
 be well give by the school choir , under the direction of 
 Mr. Henry Riding , who also write incidental music for the 
 greek play perform on the same day 

 Dursan , Natat.—The Musical Association hold its first 
 musical Festival on July 15 , 17 , and 18 , on which date 
 the following work be perform : Macfarren ’s ' ' May 
 Day , " Mendelssohn ’s 42nd Psalm , Haydn ’s ' creation , " 
 German ’s " Richard III . " overture , part of no . 1 sym- 
 phony ( Beethoven ) , Sullivan ’s graceful dance from 
 " Henry VIII . , " and a Suite , " scene of childhood , " 
 write especially for the Festival by Mr. C. Hoby . this 
 work be so well receive that it be repeat at a later 
 Concert . the soloist engage be Miss Scriviner , Miss 
 Florence Stoward , Mr. G. C. Macfarlane ( tenor ) , Mr. F. 
 Woods ( bass ) , Signor Rotondo ( violoncello ) , Mr. Roger 
 Aschem ( pianoforte ) , and Mr. R. H. Macdonald ( Borough 
 organist ) , conductor . the orchestra and chorus number 
 about 200 . altogether the Festival be a great success 

 HanpsworTH.—-At the Patronal Festival hold at St. 
 James ’s Church , on July 25 , C. Lee Williams ’s dedication 
 cantata be sing as the anthem by the combine choir of 
 St. James ’s and St. Michael ’s . the bass solo be take 
 by Mr. Challens , of St. James ’s choir . Mr. W. Terence 
 Jenkins , organist of St. Michael ’s , accompany on the 
 organ , and Mr. Richard Richards , organist of St. James ’s , 
 conduct . the canticle be sing to tour in F 

 Organist of St. Paul ’s Cathedral 

 no . s. d. 
 181 . Melodia " a a ne -- William Creser 1 0 aa 
 182 . Andante Fughetta : William Creser 1 0 Stow Movement ome | _ & 
 183 . postlude se ee eesti ww em Cotlew FG Minuet ( twelve menuet for Orchestra ) G.C. Martin 1 
 384 . Minuet . .- Hamilton Clarke 1 0 | se 
 185 . old Easter Melody ( 0 Filii et Filiz ) with variation : om | Andante ( Pianoforte Sonata , Op . 147 ) 
 ohn est r 6 } . 
 186 , Wedding March ’ .. William Creser 1 6| 2 Lar Sot G.C. Martin 1 
 ge go Handel 
 187 . six piece ( nos . 1 , 2 ) Alfred Redhead 1 6 } as panta the hart ( " Calvar " ) Spohr 
 188 . six piece ( nos . 3 , 4 ) Alfred Redhead 1 0 } p y p 
 189 . six piece ( nos . 5 , 6 ) Alfred Redhead 1 6 | ( Agnus Dei(MassinG ) .. | Schubert ) : 
 190 , Antiphon and Interlude E. Silas x 6 } > Pitter Acis and Galatea ) Handel+ .. G.C. Martin 1 
 191 . Savoyard Chant Herbert W. Wareing 1 6 Albumblatter ( no.1 , op.99 ) Schumann ) 
 192 . three intermezzo _ .. ; . AlanGray 1 o Adagio ( sonata , op.2 , no . Pe Scaviatt } , 
 193 . solemn Processional March 6 . J. B. Meacham 1 o| 4.3 ; the Cat ’s Fugue .. - . _ Scarlatti+ .. G.C. Martin 1 
 194 . third Concert Fantasia Ke Otto Dienel 1 6 Albumblatter ( no . 5,op.99 ) Schumann } 
 195 . Allegretto Cantabile . . < a oe ae Otto Dienel x 0 } Romanze and Scherzo ( Fourth sym- 
 196 . Andante with variation ee ee Otto Dienel 2 0 | 5 . | phony ) .. .. Schumann G.C. Martin 1 
 = aa may yong ae = < s xe ce a 5 6 air ( Overture ( or Suite ) i inD ) Bach ) 
 . Offertoire in b flat .. re - ward Cutler 1 0 
 199 . ' — ve ee my om i 0 ' Aingre Coenete , on , 6 , — ) G. C. Marti 
 200 . postlude a se .. E. Duncan 1 o . -\. Martin fr 
 2o1 . a and Prigre .. " < py eey ne s ¢ | Menuetto ( Pisnelerte Sonesta , op . 122 ) 
 202 . Fantaisie Pastoral . re oe + ert Renaud 1 o | " " yor ' 
 203 . Prelude and fugue .. oe oe ee ee E. Silas 1 6 } 7 tame _ — — ; ' teat -- G.C. Martin 1 
 204 . Réverie and Intermezzo .. ee lB. Luard Selby 1 0 } y 
 Romance ( Op . 174 , no . 1 ) | 8 . Minuet and Allegro i Handel .. B. Luard Selby 1 
 5 | loscnen ( op . 174 , no . 2 ) } + jas ¢ 9 . male March ( " * aes fy red ee G.C. Martin 1 
 § aspiration ( op . 174 , no . 3 ) ' | Z C. Mackenzie } * sdbia 
 206 . \ contemplation ( op . 174 , no . 4 ) } .. J. Rheinberger 1 6 | 10 , Chaconne in F major Purcell * W. Marchant 1 
 Agitato ( op . 174 , no . 5 ) . | 11 , Adagio in b minor oe Mozart A.W. Marchant 1 
 207 . as . Rheinberger 1 6 
 { improvisation eo 174 ) + " 6 ) ) J 6 | 12 , Adagio ( Sextet , op . 81 ) .. Beethoven A.B. Plant 1 
 208 . 1 Duet ( op . 7 vg ly a aa } + » J. Rheinberger 1 6 | 13 . in Elysium ( ' ' Orphée " ) .. Gluck E. J. Bellerby 1 
 Ricercare ( op . 174 Ne , 9 ) | 14 . Judex ( " mors et vita " ) .. Gounod .. John E. West 1 
 ay : { Evening Rest ( Op . 174 , no . 10 ) } + » J. Rheinberger 1 6 ) 15 Grand March ( introduction , Act iii . , ) W.c 
 Melodia Ostinata ( op . 174 , no . 11 ) } hei | ( " Lohengrin " ) .. ea Wagner ) * 5 
 21 ° | finale ( Op . 174 , no . 12 ) j J. ahanige 3 | 16 . Bridal Chorus ( " Lohengrin " ) Wagner W. Creser 1 
 { prelude ( Grand Suite , Op . 341 ) Tami | pa : 
 211 . 1 Allemande ( Ditto ) } « » Hamilton Clarke r 6 a 17 . Concerto Grosso ( no . x. ) Corelli A. B. Plant 
 { elegy ( Ditto ) ' | 18 , Passecaille .. P .. Couperin James Shaw 
 aes { Gavetee ( Ditzo ) | " a8 " s + » Hamilton Clarke 1 = of chaos ( " the Crea- } ; Wm . H. Stocks z 
 213 . { nade aaa aa - » Hamilton Clarke 1 al Haydn j * iis 
 inuet ( Ditto 
 214 . introduction and Fughetta ( Grand ae tan 347 ) Clark | 20 . ees Gat Movement , > eee } . A. B. Plant 2 
 amilton Clarke 1 6 
 215 . Capriccio ' .Silas 1 6 ) 2 ! . — . con moto © ( Uniinished , sym- ) W. Cruickshank 2 
 216 , Lullaby ( op . 348 , no.1 ) .. ..   « . Hamilton Clarke 1 0 | phony ) pale 
 217 . Bridal March ( op . 348 , no.2 ) .. . Hamilton Clarke 1 6/| 22 , gee en Movement in A flat ( 3rd = el W. Cruickshank 1 
 218 , Pastoral Fantasia ( op . 348 , no . 3 ) . Hamilton Clarke 1 0 phony ) .. - Schumann 
 219 . Allegretto Grazioso .. + e oe oe John E. West 1 6 ) 23 . solemn March ( " ' the Black Knight ’' ) ) BE Riess 
 220 . melody oe ee oe + e oe .- King Hall 1 o E. Elgar } * * a 
 221 . ) onsen oe ee eo ea ee ae en I : | 24 . ' eo con moto an " AB Plane 
 222 . Canzone ois ee ee eo oe ee ing Hall 1 chubert ) * * a 
 223 . intermezzo .. ne " C. Charlton Palmer 1 0 } " — to the " Ode on St. Cecilia ’s ) 
 224 . Grand March ( Op . 158 ) " ie 3 — — . Clarke 1 6 ! aa { ° S Handel } Chas . Macpherson 1 
 225 . Andante con moto ( Op . 97 , no . 1 ) ee ‘s - Bossi 1 0 
 226 . aspiration ( op . 97 , no.2 ) .. + » « M.E. Bossi 1 o 5 , | Earahet ( symphony as 3b tat ) } A. W. Marchant 1 
 227 . Grand Cheeur ( op . 97 , no . 3 ) i -.M.E. Bossi 1 6 ww 3 sch 
 228 . four Sonatinas , no . 1 , in d minor oe .. A.B. Plant 1 6| 27 . { hi ene he d ee ) A. W. Marchant 1 
 229 . four Sonatinas , no . 2 , in G major .. A.B. Plant 1 6 ee — — 
 230 . four Sonatinas , no . 3 , in A minor .. A.B. Plant 1 6/28 , { Adagio Sostenuto cae , op . 76,1 4 Ww . Marchant 1 
 231 . four Sonatinas , no.4,inc major .. 5 A.B. Plant 1 6 | | no . x ) Haydn j ' 
 232 . second Réverie te de * ' . Luard Selby 1 0 introduction and Pew ! in e flat ) 
 233 . third postlude ae ee ee - . » B.Luard Selby 1 0 4 Hesse } A. W. Marchant 1 
 234 . Fantasia in D minor - . AlanGray 1 6| 30 . Andante in C minor ( Quartet ) Spohr A.W. Marchant 1 
 235 . Preludium and Fuga in A ( no . 2 , from six prelude and 31 . Andante in A ( Quartet ind ) Mozart A.W. Marchant 1 
 Fugues ) ae . A. Gore Ouseley 1 . 0 f es cae Aibrechtab a W. March . 
 236 . triumphal March ( op . 26 ) ee es Dudley Buck 1 ‘ 6| 3 % Minuet ( fi pe i peer " sea . W. Marchant 1 
 237 . Rondo Caprice ( o ee ee ee Dudley Buck 1 o " inuet ( First Violoncello Suite ) Bac 
 i : at eon rn _ ' Dudley Buck 1 o a ose in E minor Veracinif 4 : W. Marchant 1 
 239 . Allegro Maestoso , ist Movement of Sonata i " = — 34 . Adagio in D Mozart A. W. Marchant 1 
 ohn E. West 1 6/35 , La Carita .. Rossini A. W. March 
 240 . Andante Religioso , 2nd Movement of Sonata abel j ~ lina a a major Pena it Peean aes ; 
 ohn E. West 1 i ? : ; 
 241 . Allegro Pomposo , 3rd Movement of Sonata ind minor 37 : Ls iy in D minor Purcell .. W. Alcock 1 
 ohn E. , West 1 6 finale from Symphonie Pathétique 
 242 . aeetes in . * ee ee ee we oe az ; i r 0| 3 % { ai Tschaikowsky | Chas . Macpherson 2 
 243 . romance in Pe .. H.S.Irons 1 o f 1 March in c ae 
 244 . elegy on theme by Henry Purcell ... E. Silas 1 0| 39 _ i acne a eckabert } -- E. Silas 1 
 zig Andante , Largheto and Allegro J. Valey Robert 1 21 , { Ais loud to Theemyheatt prof " ) John D.Codner 1 
 247 . six fugue on the name " Bach , " * ' BookI. .. Schumann 2 0 - si 
 248 . six fugue on the name " Bach , " book ii . .. Schumann 2 o Andante con moto ( symphony in B fat ) W. Cruickshank 1 
 41 . Schub * 
 249 . cue ig pa > Z + . « » Sir George so ' 1 6 first M oh ' a ubert J 
 250 . six fugue ( Nos , 1- 3 ) oe ee oe ee andel 1 6 irst movement ( " hymn of praise " ) ) : 
 251 . six fugue ( Nos . 4 - 6 ) oe ee " mate cae Handel 1 6|4 * { Mendelssohn } Cruickshank 2 

 LONDON & NEW YORK : NOVELLO , EWER and CO 

 NOVELLO ’S SHILLING 
 CANTATAS and ORATORIOS 

 THOMAS ADAMS . 
 * the holy child . 
 THOMAS ANDERTON . 
 the NORMAN BARON . 
 * the wreck of the HESPERUS . 
 E. ASPA . 
 the GIPSIES . 
 ASTORGA . 
 STABAT MATER . 
 BACH . 
 GOD so love the world . 
 GOD GOETH up with shouting . 
 * GOD ’s time be the good . 
 my spirit be in heaviness . 
 O light everlasting . 
 BIDE with we . 
 a pl doh SURE . 
 MAGNIFICA 
 THOU EUIDE of ISRAEL . 
 ESU , priceless treasure . 
 ESUS , now will we praise THEE . 
 HEN will GOD recall my spirit 
 J. BARNBY 
 * REBEKAH . 
 BEETHOVEN . 
 * the CHORAL FANTASIA . 
 * the CHORAL SYMPHONY ( the VocaL 
 PORTION ) . 
 ENGEDI . 
 * mount of olive , 
 ruin of athen . 
 KAREL BENDL . 
 WATER SPRITE ’S revenge ( FEMALE 
 voice ) . 
 G. J. BENNETT . 
 EASTER HYMN ( on the Morn oF EASTER 
 day 

 SIR W. STERNDALE BENNETT . 
 EXHIBITION ODE , 1862 . 
 G. R. BETJEMANN . 
 the song of the WESTERN MEN . 
 HUGH BLAIR . 
 HARVEST - TIDE . 
 J. BRAHMS . 
 a song of DESTINY . 
 J. F. BRIDGE . 
 * ROCK of AGES . 
 the INCHCAPE ROCK . 
 * the LORD ’s prayer . 
 CARISSIMI . 
 TEPHTHAH . 
 SIR M. COSTA . 
 the dream . 
 F. H. COWEN . 
 a song of thanksgiving . 
 H. W. DAVIES . 
 HERVE RIEL , 
 ROSALIND F. ELLICOTT . 
 ELYSIUM . 
 ROBERT FRANZ . 
 PRAISE YE the LORD ( 117th PsALy 

