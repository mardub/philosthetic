WEDNESDAY morning , 11.30 , Concerto ( Lloyd ) , New 
 Work ( Hubert Parry ) , ' ' requiem " ' ( Brahms 

 THURSDAY morning , 11.30 , ' ' Apostles ' ( Elgar ) , 
 symphony F ( Beethoven 

 THURSDAY evening , 8 , ' ' Holy Innocents " ' 
 Brewer ) , ' ' hymn praise 

 FRIDAY evening.—overture , ' ' Euryanthe ’' ( Weber ) ; New 
 Cantata , ' ' Ballad Dundee ’' ( Charles Wood ) ; Violin Concerto 
 ( Stanford ) ; Songs Sea ( Stanford ) ; overture , ' ' lustspiel " ' 
 ( Smetana 

 SATURDAY morning . — 
 D ( Beethoven 

 SATURDAY evening . — ' ' golden Legend " ( Sullivan ) ; 
 sixth Chandos Anthem ( Handel 

 symphony b flat ( Beethoven ) ; mass 

 serial ticket , admit Concerts ... ee eee 
 serial ticket , admit seven Concerts ( exclude 

 FRIDAY evening.—overture , 
 Cantata , ' ' ballad Dundee ' 
 ( Stanford ) ; 
 ( Smetana 

 SATURDAY morning . 
 D ( Beethoven 

 SATURDAY evening . — " 
 sixth Chandos Anthem ( Handel 

 new 
 Violin Concerto 
 overture , ' ' lustspiel 

 symphony b flat ( Beethoven ) ; Mass 

 golden legend " ( Sullivan 

 response commandment select 
 service composer Myles B. Foster , 
 Dr. Garrett , Ch . Gounod , Dr. E. J. Hopkins , Dr. C. H. Lloyd , 
 Merbecke , H. Smart , John E. West , S. S. Wesley , 

 Sir John Stainer sevenfold Amen include , Vesper 
 hymn Beethoven , Sullivan , ; conclude 
 Vestry Prayers S. S. Wesley Rev. Canon Hervey 

 preface 

 ap 

 504 MUSICAL TIMES.—Avucust 1 , 1904 . 
 ' unbuttoned ' letter write Beethoven } ' turn Beethoven Wagner find 

 Sir John Falstaff , ' nickname jocose composer 
 friend Ignaz Schuppanzigh , 

 celebrate violinist . letter 
 good specimen irreproachable caligraphy 
 german character , 

 decipher enthusiastic Beethoven scholar , 
 Mr. J. S. Shedlock , kindly 
 follow translation article 

 good Falstaff 

 rest affectionately 

 Beethoven . 
 Artaria Co 

 Sir John Falstaff , Messrs 

 Thayer tell Beethoven singular 
 kind friendship Schuppanzigh . useful 
 friendship 
 develop great mutual liking , 
 actual affection . good - looking man , 
 Schuppanzigh , year , 
 stout , Falstaff joke Beethoven , 
 write Graf Brunswick word : 
 ' Schuppanzigh married . ' wife 
 fat . family ! ! ' ' great 
 joker , ' Grove Beethoven , find 
 opportunity practise ' rough drollery 

 corpulent friend , Schuppanzigh . 
 blank page Beethoven Pianoforte Sonata 

 1 ) ( Op . 28 ) , composer scrawl following 
 asinine composition 

 latt master represent original text 
 ' Tristan , ' pen neat hand 

 strong contrast erratic = 
 undecipherable ' pot - hook hanger ' 
 Beethoven . London letter Wagner , lend 
 Lady cusin address Mr. G. F. 
 Anderson , Treasurer Philharmonic 
 Society , special interest . year 1855 , 
 memorable season Wagner conduct 

 concert Philharmonic Society . 
 communication ( translate 

 F. G. E. 
 ( continue 

 follow reply — genuineness 
 fully authenticate — answer question 
 examination paper set examiner repute : — 
 Marenzio german lady introduce note 
 music . live 13th century a.d. 
 burn martyr Rouen . 
 Palestrina son stock - broke 
 Beethoven 

 r brother 

 guite sure person live ! needless 
 come near receive sound thrashing 
 fool people little game 

 meeting hungarian violinist 
 Eduard Reményi amusing detail . 
 characterize strange 
 clever charlatan — , - - way , german- 
 hungarian Jew , Hoffmann — story 
 use play bar theme 
 slow movement Beethoven Kreutzer Sonata , 
 ‘ hungarian style 

 famous incident Joachim 
 draw young composer 
 close , life - long friend brave , unfaltering 
 champion destine , 
 correct Herr Kalbeck important par- 
 ticular . Sonata Violin pianoforte 
 C minor ( op . 30 , . 2 ) Brahms trans- 
 pose pianoforte half - - tone high , 
 memory ! Reményi , vo¢ Kreutzer 
 Sonata , Mr. Hadow beautiful monograph 
 ( ' study Modern Music , ' Second Series ) state . 
 happen , suppose happen , 
 painstaking author unable , 
 year event , verify detail — Celle , 
 Hanover , Géttingen ; Joachim 
 present . great violinist meet Johannes 
 Hanover , ideal friendship commence 
 rich result art , 
 fraught happiness friend . life 
 ' Altenburg , ' Liszt , Princess 
 Sayn - Wittgenstein house Weimar , describe 
 considerable length , order contrast hero 
 unsophisticated nature simple mode existence 
 Piano King exotic life ' manage ' 
 ' bon ange adorable , ' aforesaid russian 
 princess , Herr Kalbeck allow prejudice bias 
 amusing extent . doubt 
 young Brahms display remarkable 
 strength character decide escape 
 magic charm Liszt wonderful personality — 
 charm succumb readily 
 completely rest mankind . find 
 impossible join chorus adulation 
 visitor Altenburg chant daily 
 hourly great glory Liszt composer , 
 flee . life , , enthusiastic 
 admirer Liszt pianist . remark 
 occasion Klaus Groth , low german poet : 
 ' play piano ( 
 pianist mark ) , 
 finger hand . ' : ' 
 hear Liszt simply speak piano playing ; 
 come , long come 
 . play 
 unique , incomparable , inimitable 

 preliminary particular Festivals 
 come hand — Gloucester Cardiff . 
 programme Gloucester meeting — hold 
 September 4 g — include following 
 novelty , set forth order performance : 
 Magnificat Nunc dimittis ( Ivor A. Atkins ) ; ' 
 song Zion ' ( John E. West ) ; Festival Hymn 
 ( C. Lee Williams ) ; ' thing availeth , 
 short oratorio ( C. Hubert H. Parry ) ; ' Holy 
 innocent , ' oratorio ( A. Herbert Brewer ) . 
 familiar work comprise ' Messiah ' 
 selection ( Handel ) ; ' Elijah ' ' Hymn 
 Praise ' ( Mendelssohn ) ; Te Deum ( Stanford ) ; ' 
 time - spirit ' ? ( Granville Bantock ): Organ Concerto 
 ( C. H. Lloyd ) ; German Requiem ( Brahms ): ' 
 Apostles ' ( Elgar ) , & c. Mr. A. Herbert Brewer , 
 organist Gloucester Cathedral , occupy 
 usual place conductor time - honour Festival 
 Choirs 

 hundredth anniversary birth George 
 Sand duly celebrate Paris month . 
 friendship Liszt Chopin 
 connect music , 
 write subject art . appreciation 
 Chopin , somewhat extravagant , lack 
 interest . : ' genius deep 
 imbue feeling . 
 instrument speak language infinite . . . 
 individuality - exquisite Bach , 
 powerful Beethoven , dramatic 
 Weber . . . Mozart superior 
 , addition calmness health 
 bring — z.c . , life fulness 

 fourth Cardiff Triennial Festival fix 
 September 21 , 22 , 23 24 . novelty 
 present : welsh rhapsody 
 Orchestra ( German ) ; * Eve ' ( Massenet ) , 
 time England ; ' East , ' tone - poem 
 orchestra ( Hervey ) ; ' John Gilpin , choral ballad 
 ( Cowen ) ; ' victory St. Garmon ' ( Harry 
 Evans ) . addition forego scheme 
 embrace performance ' Elijah ' ' hymn 
 Praise , ' Midsummer Night Dream ' music , 
 ' Samson Delilah , ' dream Gerontius , 
 ' Faust ' ( Schumann ) , ' Desert ' ( Félicien David ) , 
 Requiem ( Verdi ) , Wagner selection , & c. Dr. Cowen 
 discharge duty conductor . 
 forecast Leeds Meeting — hold October 
 5 8—-in June issue , complete trio 
 Musical Festivals shortly hold , doubtless 
 usual success , England 

 Schubert occupy low place 
 Vienna Society , unknown short 
 lifetime , die early age thirty - , 
 leave 600 + song , Masses , dozen 
 Operas , symphony , mass Sonatas , 
 Quartets , miscellaneous music 
 uncountable . child poor 
 viennese schoolmaster . marry ; 
 poor money 
 dinner ; friend level ; 
 hold post , settled income , 
 scanty , month ; 
 dissipate man , fondly attach 
 belonging , idol friend ; 
 rarely travel , reading scanty ; fact 
 heaven - bear genius , 
 nature art , 
 hour minute life 
 directly devote music . person short 
 stout , ordinary look } ; poorly clothe , 
 probably little manner , certainly 
 modest retiring disposition possible 

 bear 1797 , die 1828 , year 
 Beethoven . life pass 
 city Beethoven , time great 
 composer produce year year 
 remarkable work . , 
 live Beethoven shadow , compose 
 immense mass music enumerate , 
 great beauty absolute individuality 
 originality , gradually recognise 
 world hardly inferior great 
 contemporary , different . 
 Beethoven woman 
 man , comparison happy . 
 slightly ! acquainted ; Beethoven 
 acquaintance Schubert music 
 death - bed , sure eye genius 
 experience recognise transcendent quality , 
 express admiration 

 colossal Symphony C form climax 
 Schubert achievement department 
 orchestral music , work per- 
 mitte ; compose commence 
 March , 1828 , die follow November . 
 culminate work Schubert life . 
 peculiar , unearthly tone 
 wild , mystical , tender melancholy mark 
 symphonic movement b minor ( . 8) 
 entr’actes ' Rosamunde , ' large 

 work ' 10 ° , chain 
 evidence collect writer notice publish 
 Atheneum November 1gth , 1881 , disprove , 
 assume ' Gastein Symphony , ' write place 
 summer 1825 , ' 9 , ' existence . 
 discovery Beethoven youthful cantata 
 need hope case 

 publish Messrs. Breitkopf Haertel 
 splendid edition volume , edit Dr. Mandyczewski , 
 Vienna , scholarlike accurate style , 
 index list exacting investigator require . 
 considerable number song print time , 
 great interest 

 Ganz wie ein Fiaker ' ; exactly like cabman — Lachner 
 Mr. C. A. Barry 

 Spaun testimony fact Schubert having loudly 
 regret Beethoven death Beethoven 

 silastic effort cause music sea 

 iii . Scherzo Trio — allegro vivace 

 Scherzo lose primitive meaning 
 ' joke , ' independent movement , 
 equal importance , 
 large dimension Beethoven _ 
 Eroica . 7 , . 9 . content 
 quotation , attention 
 subsidiary passage , reason 

 appear . follow idea 
 opening : — 
 . 16 . 
 string 

 iv . Finale — Allegro vivace 

 /7za / e form astonishing climax 
 precede . thing strike 
 hearing ; _ , wonderful impetuosity 
 resistless force — difficult understand 
 man volcanic piece music 
 inside , burst ; secondly , 
 marked character rhythm . 
 movement , Beethoven , 
 rhythm evident irresistible . open 
 bar , sonorous clang , nobility 

 time come new feature — ' second 
 subject ' movement , key G , precede 
 marked note horn , consist 
 bar minim succeed bar 
 crotchet 

 cre 

 strong relationship theme /7a / e 
 Beethoven Choral Symphony 

 shortly , end 
 half /7v«/e , beautiful passage 
 bass gradually 
 octave , force sound diminish 
 time , orchestra /// simple 
 quartet string . half moveinent 
 close modulation e flat , clarinet , 
 tone shiver , begin 
 ' working - ' second subject ( . 24 ) , 
 cast recall distinctly passage 
 allude Ninth Symphony Beethoven . 
 trombone use entirely 
 original manner , Beethoven leave 
 instance . passage follow 
 novelty trombone player 
 1828 

 . 25 . = 
 Tenor Trombone . — — = > Alto Trombone . — 

 ee 
 3ass Trombone 

 fact , Beethoven drum , Schubert 
 Symphony trombone . 
 use effect , deepen 
 shadow , bring spot bright colour 
 , double choral music ; 
 release subordinate position , 
 given;them independent , new 
 office great family orchestra 

 return /7vale . development 
 second subject time , soon 
 minim begin force way , violin 
 triplet follow , movement start , 
 rest moment till 
 rush final catastrophe . near close 
 tremendous significance minim — 
 /z , fz , f= , fz=—appear ; manner 
 return Unison C widely intervene 
 note wander , repeat dreadful 
 stroke , like blow direful instrument 
 destruction , truly extraordinary . Coda begin 
 long crescendo commence ppp contain 
 progressive section bar , 
 increase uniformly force , AAA , AP , 7 , 72/ , 
 final climax crash reach 

 way , movement ( 7ema con variasion ? ) 
 ingenuity invention ally feeling 

 appearance spontaneously express . young 
 composer view , occasionally ,   orches- 
 tration , effect set Thames 
 fire , nearly ' come , ' use jargon 
 orchestral specialist . work receive degree 
 enthusiasm suggest birth veritable master- 
 piece . let hope hero ' overwhelming 
 reception ' live level head . 
 young composer achieve second success smooth 
 distinctly musical performance solo 
 Beethoven G major Pianoforte Concerto . Maria Yelland 
 sing ' Inflammatus ' Dvorak ' Stabat Mater . ' 
 powerful voice good quality , energetic , 
 assertive , way ejaculate word 
 wonder understand meaning . 
 difficult ' Chorus low Maidens ' Act II . ' Parsifal ' 
 brightly lightly sing Choral Class , 
 complex pattern Wagner vocal polyphony stand 
 clearly 

 orchestra usual standard 
 Royal College Music . apparent vast deal 
 ' spirit , ' degree roughness , especially 
 string , greatly interfere enjoyment 
 Concerto Schumann C major Symphony ( . 2 ) , 
 close evening music . draw 
 attention cacophonous ' tuning ' 
 player indulge movement work ? 
 irritating habit veritable nuisance , 
 work , pause divide 
 movement , hear snatch work ( 
 programme ! ) play zw7 / h , travesty individual 
 player orchestra . time come utter 
 protest proceeding inartistic unworthy 
 great Music School 

 shortly début country Master Vecsey 
 come rumour achievement 
 marvellous prodigy Master Florizel von Reuter , young 
 gentleman wonderful 
 violinist little boy , composer 
 conductor boot , age ! wonder 
 large audience , include ( ) ueen 
 Alexandra , gather Queen Hall June 29 
 expectancy unmixed curiosity 
 appearance England prodigy violinist - composer- 
 conductor 

 performance Beethoven ' Fidelio ' 
 overture Dr. Cowen direction Master Reuter 
 appearance , find slight - build boy 
 fair complexion flaxen curly hair , great contrast 
 dark sturdy Master Vecsey conceive . 
 begin play Vieuxtemps Violin Concerto F : 
 soon manifest tone , 
 conscious effort juvenile rival . 
 technical facility astounding , 
 prominently manifest keen sense rhythm 
 accentuation . expressive passage render 
 great refinement , rapid s / accato note run 
 execute fascinating crispness clearness 
 attest extraordinarily developed nervous energy 
 young 

 sensational moment afternoon come 
 lad baton conduct Symphony 
 F sharp minor , sharply tap desk 
 huge orchestra matured musician attention , 
 sight strange partake 
 incongruous ripple subdued merriment 
 hall . scarcely amusing 
 note punctilious care boy lead 
 entrance instrument energetic 
 gesture approach climax . scene 
 topsy - turveydom ! | Symphony chiefly remarkable 
 thematic material , principal subject 
 melodious significant , symphonic character . 
 need scarcely add performance 
 boyish creation elicit enthusiastic applause , 
 Master Reuter sul : sequently hear Max Bruch 
 ' scottish Fantasia ' extra piece demand 

 DucciANa.—-Mr . Arthur G. Hill ' Organ Cases 
 Organs Middle Ages Renaissance ' ig ge 
 drawing following english organ case : vol . 
 Westminster Abbey ( organ specially erect " 
 Coronation William M : uy 1677 ) ; Gloucester 
 Cathedral ; King Coliege , Cambridge ; Exeter Cathedral . 
 vol . ii.—Stanford Church , near Rugby ; Tewkesbury 
 Abbey ; Church St. Catherine Cree , Leadenhall 
 Street , London ; Eton College Chapel ( organ ) ; 
 Finedon Church , Northamptonshire 

 THELMA.—The following serve ' c / asstcad 
 lively duet pianoforte , vexy hard play ' : 
 Beethoven — sonata D ( op . 6 ) marche ( op . 45 ) ; 
 Mozart — sonata D , b flat , c ; Hummel — sonata 
 e flat flat , nocturne F ; Schubert — marche , 
 rondo , Variations , Divertissements Hongroise , 
 Polonaises ; Schumann — Bilder aus Osten Ballscenen ; 
 Weber — piece . original pianoforte duet , 
 arrangement 

 S. p. D.—The difficulty experience know 
 new song overcome subscribe musical 
 Circulating Library 

