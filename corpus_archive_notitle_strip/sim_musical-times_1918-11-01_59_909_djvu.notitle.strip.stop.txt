picture , hardly employing technical terms 
 unusual words sedate old - wo 
 elegance sound litt 
 chink antique medals 

 remember parallel drew Beethoven 
 orchestration — likened black - - whitt 
 formula , giving consequence scale exquisit 
 grey tints — Wagner , kind mult 

 coloured mastic evenly distributed , whidjf 
 declared distinguish sound ofé 

 masters music 

 isf ’ tke celebrated shade Beethoven . 
 ef ? indifferently flattered 

 MUSICAL TIMES.—Novemser 1 , 1918 

 piher 

 Beethoven sonatas badly written 
 2 . Strictly speaking , , especially 
 ones , orchestral transcriptions ; hand 

 tien missing Beethoven 
 mainly heard , , rate , hope . 
 d wiser left Schumann 

 Chopin : wrote properly 
 piano , slight matter 
 M. Lalo , feel grateful 
 having prepared perfection Dukas — 
 

 MR . CLUTSAM ARTICLES ‘ PRINCIPLES 
 MODERN COMPOSITION 

 Str,—Having read Mr. Clutsam thoughtful series 
 articles columns close consideration , 
 desire attention point . formulated 
 broad bold system chord - classification , 
 entitled ‘ Principles Modern Composition . ’ 
 theorists past — according 
 lights . Musical Times September October , 
 1910 , described elaborate treatises 
 B C music , pointed contents 
 remotest bearing composition . 
 reproduced frontispiece , illustrating popular 
 idea act music - production . Mr. Clutsam 
 patient industry pale work reviewed 
 late Dr. Prout Monthly Musical Record June 
 July , 1895 . seen volume , 
 size score ‘ Tristan . ’ contains , engraved 
 music - type , thousands examples combinations 
 notes , permutations , 2 , 3 ... 12 notes , 
 fact , taken 2 , 3 , 4 . . . # , reference 
 possible desirable . cui dono ? 
 M. Anatole Loquin treatise — , fear , Mr. Clutsam 
 milder classification — leaves art composition 
 . cartload chords 
 ‘ moderns ’ dumping 
 uninterested public particle interest evoked 
 Elgar glorious ‘ Carillon , ’ Beethoven despised 
 Symphonies . clean mental workshop , 
 contents labelled boxes , excellent preliminary 
 creating masterpiece ; 
 composing musical works — masterpieces 

 , Sir , truly , F. Conom 

 SMALL ORCHESTRA 

 S1r,—Amongst interesting _ illuminati 
 articles September issue , attracted » , 
 attention particularly M. Jean - Aubry ‘ Plea t } 
 small Orchestra . ’ admirable article expressed mp 
 clearly views long held , sure » 
 held large number earnest musicians throughoy| 
 music - loving world . Undoubtedly War hx 
 great — indirect — benefit mo 
 musicians : shattered false idols , brought , 
 greater sense self - reliance , caused — chieh 
 involuntarily — revert simplicity place og 
 old , blundering complexity . desired » 
 , view urgent necessity work » 
 , temporarily , necessary spheres activity 
 maintain gigantic orchestras choral organizatioy 
 accustomed War ; ; 
 result adopt simpler means — fry 
 perfunctorily , appreciatively . 
 long time accustom absence th 
 colossal orchestras ‘ Tod und Verklarung ’ ‘ Elektr ’ 
 having eventually , begun realise 
 power ( degenerating mere noise ! ) m 
 necessarily sine gua non beauty art — si 
 possible express infinite variety emotions 
 orchestra Mozart Beethoven calibre , tht 
 ‘ extras ’ celesta vast army wood - wini 
 ( frequently employed Richard Straus 
 , , superficialities 

 people , imagine , agree th 
 blarings hootings super - orchestras Mable 
 variety — spite immensity orchestn 
 required render , amazing complexity th 
 scoring employed — produced impressive effects thu 
 produced beautifully - balanced orchestras ¢ 
 Beethoven . infinitely ‘ thrill , ’ mind , 
 passage Scherzo Beethoven C mina 
 Symphony , low C sustained ban 
 accompanied solely mysterious tappings tk 
 timpani , passage Strauss ‘ Tod wi 
 Verklarung ' — contrast means work 
 present 

 aspect matter : th 
 enormity orchestras required render certas 
 modern works , inner beauty music lat 
 ninety - listeners . simpk 
 orchestras Mozart Beethoven , little ‘ inne 
 melody ’ speaks , unobtrusively unmistakably ; ead 
 instrument individuality gigantt 
 orchestras vogue 1913 . small orchestra litt 
 mental strain imposed listener , th 
 finer shades expression able heard . fancy thi 
 reasons failure orchestral works 
 composers — especially ‘ ’ prentice hands ’ — wer 
 wo nt imagine hugeness spelt greatness 

 , , compelled reduc 
 resources , let learn lesson teaches 
 : great necessarily require vast army 
 instrumentalists , greatest a@ 
 produce great music simplest means.—Yours , & c 

 GLOUCESTER MUSICAL RECORD 

 Dr. A. Herbert Brewer , letter Odserver 
 October 14 , says : ‘ read great interest 
 Mr. Ernest Newman article ‘ ‘ Resurrection 
 Triennial Festivals ” Sunday issue . glad 
 approves continuation 
 Choirs Festivals War , 
 eye - - eye statement towns 
 ‘ ‘ music worth speaking ordinary times . ” 
 mentions immediate needs orchestral 
 music . unaware Gloucester possesses 
 Orchestral Society ( pre - War time numbering 
 members ) , spite War , continued 
 flourish , performed recently , works , 
 following Symphonies : Nos . 1 4 Beethoven , 
 E minor F minor Tchaikovsky , D major 
 Brahms , “ Unfinished ” Schubert . 
 appreciation music proved fact 
 concert numbers music - lovers failed obtain 

 506 MUSICAL TIMES.—NoveMser 1 , 1918 

 fi 
 place 

 Fridays , October 25 , November 29 , February } 
 March 21 . Principal works : ‘ afraid , ’ ‘ Soy 
 Darkness Light , ’ ‘ Alfred Great ’ ( Hurlstone 
 ‘ Samson ’ selection , Beethoven second Symphomy 
 Annual ‘ Messiah ’ concerts , December 13 14 

 Dundealgan Choral Socizty.—Costume - recital   Maritam ! 
 January 

 Miss Lena Ashwell Miss Carrie Tubb gave 
 series recitals October 10 . Miss Ashwell recitations 
 drawn Rupert Brooke , Geofirey Dearmer , Seumas 
 O'Sullivan , Rk . E. Vernede , . Miss Tubb sang 
 fine selection songs young native composers . 
 unusual enjoyable programme 

 October 12 Mr. Mrs. Arthur Williams , pianist 
 ’ cellist , Mr. George Ferguson , vocalist , gave 
 pleasant chamber concert . Mr. Mrs. Williams played 
 Sonatas Brahms Beethoven , Mr. Ferguson sang 
 vigorously , best Bruneau ‘ L’Heureux 
 Vagabond . ’ Mr. de Veroli accompanied 

 October 12 London String Quartet gave final 
 Londen concert Spanish tour , playing Brahms 
 B minor Clarinet Quintet McEwen ‘ Threnody ’ 
 ( Juartet . interesting revival Cantata Tunder 
 ( pupil Frescobaldi ) , voice , strings , pianoforte , 

 WIGMORE HALL 

 Moiseiwitsch attracted crowded audience October 12 , 
 _ fine programme included Beethoven 
 ‘ Appassionata ’ C sharp minor Sonata Brahms 
 ‘ Variations Theme Handel ’ played 
 inimitable manner 

 Miss Amy McDowall choir gave concert aid St. 
 Dunstan Hostel October 12 . choir , small body 
 young ladies , sang varied selection - songs 
 admirable style , far . Miss Héléne Dolmetsch 
 played ‘ cello solos , accompanied Miss G. Underdown , 
 Miss Helen Choisy accompanied choir 

 MUSICAL TIMES.—Novemser 1 , 1918 

 _ — 
 Music Provinces . od strings — doubt suggested comp ser visit force | 
 enice beholding ‘ ! ponte dei Sospiri’—proved J sery & 
 ( CORRESPONDENTS . ) delightful novelty . seit } 
 , Chamber music , auspices Birminghaly | 
 Chamber Concerts Society , furms important featug sisits f 
 BIRMINGHAM . arrangements having hold concerts , th gi 
 executive excellent Catterall combination gf 9 
 musical season swing , few|formerly . concerts given iim Sit H 
 months enormous increase number of| Exhibition Room Royal Society Artists qj patfor 
 concerts calibre . places amusement — October 15 , following items presented 
 theatres , music - halls , corcert rooms , 9nd picture - houses — | Beethoven String Quartet , Op . 18 , . 2 , G ming 
 enjoyed prosperous season , patronised Debussy Quartet G minor , Borodin Quartet in4 
 new public . midst | unanimity artistic ensemble characteris _ 
 thousands strangers employed Government work | performance . wh 
 need relaxation , money plentiful spend } Mark Hambourg appeared Wassell concert gt | 
 freely . better illustration given point | October 9 , Central Hall , programme w b 
 record business Royal Carl Rosa Opera devoted admirably representative pianoforte recity 
 Ccmpany fortnight season October 7 ! containing copious sek ction Chopin pieces , includigy 
 October 19 . Prirce Wales Theatre crowded | Sonata B flat minor . Op . 35 . Berccuse , Studi manag 
 nightly , matinées , who| posthumous works heard previous tickets 
 probably seen grand opera bcfore . | Beethoven represented ‘ Moonlight ’ Sonata , agg " ™ @ ™ * 
 astonishing fascination ‘ ] Trovatore ’ ] Bach D’Albert transcription Organ Prelude af ™ 
 public , life recollection remember Fugue D major . artist excellent form , havi 
 seen enormous attendance . course | creating furore éravura performance Chopia 
 * Tales Hoffmann , ’ ‘ Faust , ’ ‘ Carmen , ’ ‘ Maritana ’ | Study F , Op . 10 , 8 , repeated 4¢ 
 enjoyed great popularity , measure October 16 , Birmingham Festival Choral Socieg * ¥ ° ° Y 
 * Tannhauser . ’ revival Ambroise Thomas | gave Central Hall , Berlioz ‘ Faust , ’ Sir Thoma perfort 
 ‘ Mignon ’ Mozart ‘ Don Giovanni ’ proved welcome | Beecham direction . principals Miss Gladgp Sovem 
 musical feature , Puccini ‘ La Boheme ’ ] Ancrum , Mr.+ Frank Mullings , Mr. Arthur Cranmer , ang & € Mi 
 received , company appeared } Mr , Herbert Brown . Comments performar c2 ms Brown 
 Prince Wales Theatre November lsst , including Miss | deferred issue J / ustcal Times . Miss 4 
 Beatrice Miranda , Miss Clara Simons , Miss Eva Turner , mee acd M 
 Mr. William Boland , Mr. Hughes Macklin , Mr. Arthur epee : : Parker 
 Winckworth , Mr. Hebden Foster . conductors BOURNEMOUTH . ‘ Tann 
 Messrs. Henriquez de la Fuente Herbert Ferrers . month October started th Miss 4 
 chorus orchestra appear numerically | long - established Bournemouth Sympheny Concer Allen , 
 representative , probably owing prevalent } commencement - fourth series thegp Service 
 conditions . Concerts discovers Mr. Dan Godfrey commang Reed , 
 local season opened violin pianoforte |of highly - efficient orchestra , prospect 
 recital given large lecture theatre Midland | successful season wel ! view . times consideral Octobe 
 Institute September 28 , Miss Gertrude Fuller | cl anges personnel orchestra unavoidably Shepp : 
 Miss Beatrice Hewitt , remarkably talented local artists . | Mr. Godfrey apparently able replace wig ¢ 
 programme contained Beethoven rarely - heard Sonata | equally capable substitutes thi se members departed * pprec 
 violin pianoforte G major , Op . 96 , John Ireland | quality band whit inferior v 
 Sonata D minor , violin pianoforte , Rimsky- | previous seasons ; , far violins splend 
 Korsakov Fantasie de Concert B minor , Op . 33 , | concerned , dis'inct improvement , t 
 violin ( ably accompanied pianoforte Mr. Appleby | expert leadership Mr. A. Renges , occupies t handly 
 Matthews ) , pianoforte solos Granados Debussy . | position long held Mr. F. Kirg - Hail — serving gm Consol 
 Miss Gertrude Fuller earnest admirably equipped | Army — discern signs excellent wei disting 
 violinist , produces warm sympathetic tone . Miss | quarter . list novelties season disclesg BUSIC | 
 Hewitt best local pianists , |the fact Mr. Godfrey usual enterprise @ ™ stanc 
 performers exposition Sonatas proved | pinning faith entirely - tried composition © ™ ™ M ! 
 enjoyable musical feast , respective solos | course works known valve predominat Stroud 
 realised perfect performances . British music special feature , maki | 
 excitement caused visit famous | Bournemouth places prese Mr. 
 Royal Italian Carabinieri Band seventy - performers , | moment native works given opportunigg @ acc 
 ) gave concert Town Hall October 1 . | . vacant 
 demand admission large great concert opening concert October 10 Symphony ¥ Mr. J. 
 room filled times . con-| - popular C minor Beethoven , som hon . -s 
 stitution band | extent accounted large attendance , f auditor 
 Banda di Roma , gave matinée evening concert | Bournemouth audiences true steel thag ™ anof 
 Town Hall 1905 . great characteristic feature | Beethoven . performance capital , strg 
 Carabinieri players consists wonderful tone - power | tone especially , suggested , extremely soi Spoke 
 /#//z . precision attack perfect , | . Bruneau * L’Attaque du Moulin ’ Suite ab bed Te 
 pieces demanding cav / adi / e playing clarinets produced | exceedingly played , charming Prelude provig 
 delicious mellow quality tone . Cavaliere Tenente Luigi particularly effective . Massenet ‘ Phedre ’ Overture 2 effort ii 
 Cajoli conducted quiet manner , forces | Elgar ‘ Crown India ’ March compositions @ Li 
 complete control . concert given aid Italian | programme . Concerto new violin Conceti press 
 British Red Cross Societies . W. II . Reed , work remembered bad rai 
 Appleby Matthews Sunday Orchestral Concerts recently produced London Miss Jessie Snow , ® t 
 inaugurated Scala Theatre October 6 } conjunction London Symphony Orchestra . Sé Flying 
 far great success , doubt continued | played solo present occasion . Th Victor 
 Autumn season . orchestra | work abounds effective passages soloist , whit Rockin 
 controls remarkably compact efficient body | Miss Snow disclosed advantage ; ristol 
 performers , excellent array strings , leader | compositions , attractive momett Joyce 
 Mr. Alexander Cohen , Scala Orchestra . pro-|are wanting , appears hold interest Murch 
 grammes varied character suit tastes , special | performer listener . A. 
 feature inclusion classical symphonies . noble success attending opening concert M. 
 performance given Mozart Symphony E flat , | anticipate pleasurable afternoons come . ind sel 
 startling exhilarating reading Smetana lamented death Sir Hubert Parry — fine music@ 
 Overture ‘ Bartered Bride . ’ Elgar impressive ‘ Sospiri ' | lovable men — brought home additiow & . M 

 _ — _ | 
 2Ser Visit 

 including Ethel Boyce ‘ Hymn aviators , ’ | famous vocalist Spring . Describing Miss Harris y 
 students played pianoforte quartet . Belgian artists | having ‘ beautiful contralto voice ’ ‘ singer wi 
 gave chamber concert Dartmouth October 10 | fine artist , given opportunity , ’ Madame Cla 

 Serbian relief . 
 Vieuxtemps Concerto , M. L. Delune pianoforte , 
 playing pieces Liszt ( Rhapscdy . 7 ) , 
 accompanying Madame Delune ( ‘ cello ) ‘ Dragon fly ’ 
 , music Popper Rubinstein . 
 instrumentalists played Beethoven Trio , 
 Madame M. Rizzini sang songs Hunt Massenet 

 music section Plymouth Institution 

 Gunner S. Rex , R.G.A. , organist St. Martin Church , 
 Cardiff , gave organ recital Antony Church q 
 September 10 , assisted Bombardier Gawihrop , vocalig , 
 Bombardier Probyn , ‘ cellist . Rev. J. H. Duerden ; 
 ‘ Sunbeam ’ choir Hayle , September 23 , sang trig 

 encourage subconscious analysis . new Winter | ( * Lift thine eyes ’ ‘ Like asa father ’ ) , duets , chorusg 
 season Plymouth Co - operative Education Department | Mrs. T. R. Hosking organ ; Bugle ‘ 
 opened September 28 performance band | - ’ Ladies ’ Choir ( Miss Meta Hawke ) St. Auste 
 Royal Marines , conducted Mr. S. P. G. O'Donnell , | Ladies ’ Quartet combined concert October 3 fe 
 continued weekly miscellaneous concert | benefit local clay - workers blinded War , Misss 
 band R.G.A. , conducted Mr. R. G. | F , Williams F. Jane giving pianoforte music . 
 Evans . Plymouth Corporation Concerts - opened ; Bodmin Young ‘ Leaguers ’ Union choir bundre 
 October 5 band Royal Marines , | performers , trained conducted Mr. H. Lamerton 
 October 12 visit paid Senhor Edgardo Guerra . | gave performances ‘ Bohemian Girl ’ cleve 
 Promenade Pier week - day concert seasonclosed October 12 , | original revue , ‘ Music , Mirth , Melody , ’ October 
 Sunday band concerts ircluded October 6 con- | 3 , £ 100 raised Nation 
 certs band Welsh Guards ( Mr. Andrew Harris ) . | Children Home Mr. 
 Lifton , September 29 , performance | pianoforte , Overture incidental music wer 
 Red Cross , military band Crownhill played played orchestra led Mr. Symons , 
 Sergeant Tiplady , Miss Kate Bower sang . ; . : 
 Welsh Guards Band ( Mr. Andrew Harris ) successful 
 tour Cornwall week beginning September 28 , 
 opering Newquay paying return visit , proceeds | 
 given St. Dunstan Hostel . local season opened Crane Hall October 2 , 
 large choir , conducted Mr. R. Lang ‘ recital given Mr. Joseph Greene Miss Hilda Cragg 
 supported orchestra led Mr. E. W. Wingate , gave | James , local artists achievements pianist 
 rousing performance * Elijah ’ Plymouth October 6 ; | vocalist respectively , approving mention 
 day Sunday concerts Theatre Mr. Greene commanding technique leaves little t 
 Royal - opened pianoforte recital Mark | desire interpretation varied moods . wa 
 Hambourg , played Bach - Tausig Prelude Fugue , | evidenced playing Schumann ‘ Fantaisiestiicke , ’ 
 C sharp minor Sonata Beethoven , Chopin numbers , | Palmgren dainty ‘ Rococo , ’ Frank Bridge ‘ Vale 
 Ravel ‘ Jeux d’eaux ’ Scriabin Study C sharp . Caprice . ’ Mr. B. Sandberg Lee accompanied songs 
 important event new season October 9 | included tender old ‘ Londonderry Air , ’ beautifull 
 Mr. David Parkes Plymouth Orpheus Choir , | sung commonplace words . ‘ Danny Boy ’ doe 
 occasion seventy voices . interval | duty heroic Cuchulain — ‘ Splendour Ulster 

 LIVERPOOL 

 particulars published Bradford | te 
 Choral Societies 

 _ , | Cathedral September 19 , Dr. Brewer played 
 Halifax Choral Society , Mr. C. H. Moody,|¢}intandia ’ . Andante Beethoven fifth 
 magyar een eg _ s ag et ‘ tior ia andia ’ e ! 
 “ = ove Coleridge - Taylor Hiawatha , addition | Symphony , Miss Murray Lambert gave fine _ per- 
 Messiah , ” concert arranged . | ¢ > ances Handel Sonata D , Andante 

 oy egg yh sete , 2 S S até » & 
 Huddersfield Choral Society Ev eryman ’ | sfendelssohn Violin Concerto , O’Connor Morris ‘ Lament , ’ 
 concert , composer direction , ] 914 < , sno * Old Saga . ’ recital 
 h’c * Sing ve } ’ " y . SUG ou ng é iC ga . Ni 

