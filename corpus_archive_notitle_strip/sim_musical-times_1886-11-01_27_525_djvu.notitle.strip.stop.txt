MADAME CLARA WEST ( Soprano 

 MISS LOTTIE WEST ( Contralto ) . 
 Beethoven Villa , King Edward Road , Hackney 

 MISS MADELEINE WHARTON ( Soprano ) , 
 MDLLE . JOSE D’ARCGONVILLE , R.A.M 

 Thou prayer Thy servant praycth . ” 

 followed Beethoven Funeral March(organ ) ; “ O 

 amateur 

 aspect case presents . 
 purpose gatherings chi efly 
 artistic cultivation , needs 
 promote genuine musical taste 
 masses . Observe character Glouceste 

 programme . Pine wi 
 sion sympathy people , contains 
 illustrious names Haydn , Ha ndel , Mendelssohn , 
 Beethoven , force genius brought 
 ‘ ar , mainly f emotional senpression ns , inevitably , 
 , culture . happy fortune art 

 possess countless works appreciate 

 MAZZINI WAGNER 

 Ir useful , writing art , turn . 
 times aside beaten track specialists 
 technical experts broad highway trodden 
 men lives thoughts animated 
 wider general aims , relieve art 
 splendid barren isolation , history 
 dreary , unintelligible monotony . 
 man moved mid - stream modern 
 European progress Mazzini . , man , 
 typical political literary tendencies 
 age lived ; musician ; 
 niche scooped Sir George Grove 
 Pantheon ; Mr. Gurney dismisses angry 
 sneer speaking Beethoven mystic , 
 Donizetti , young musician , promising great 
 things . Mazzini views music , 
 views substantially 

 inexplicable . “ betters ” 
 performances intended 
 actively exert , careful 
 refrain influencing choice music | 
 general presentation reference tastes . | 
 lies great danger . 
 exercise simplicity humility . 
 desire ‘ ‘ ” 
 ; longing artistic display | 
 sake glory . use| 
 beautiful old figure , performers “ be- | 
 come little child , ” minister child - like | 
 fashion tastes music grown | 
 growth strengthened | 
 strength . keeping cardinal point view a| 
 sure guide success obtained . long | 
 impressed , interested , | 
 attendance poor looked , | 
 lose touch going | 
 appreciation follow , “ Ichabod ” 
 written enterprise . facts 
 lost sight Gloucester , intelligence 
 discretion Cathedral Organist ample 
 surety wise management 

 643 

 music language illustrated passage 
 fom Hegel . ‘ Colour , ’ said Hegel , “ tries , like 
 music , escape externai world , 
 fascinate unsubstantial supernatural 
 character ; difference colour 
 art musical art — far intimately 
 bound mind character , 
 echo voice , accents 
 speak feelings day day , moment moment . ” 
 mystical character ; music 
 nearer akin , comes home 
 tous colour . Thirdly , music , like language , 
 especially adapted express national type 
 character . music , said Mazzini , Italy 
 fulfil mission Italy world ; 
 mission man ( melody ) 
 sacrifice altar country 
 ( harmony ) , sacrifice 
 ereatest thing greatest man . 
 Wagner persistently repeats 
 Beethoven spoke immortal 
 Symphonies , spirit Germany 
 spoke . regards Lessing , Herder , 
 Schiller , Goethe founders otf German 
 drama , fathers German patriot- 
 ism ( Lessing Herder ridiculed patriotism ) , 
 says time come music - drama 
 assert German character , fulfil German 
 mission ( Beruf ) world . Wagner conception 
 character mission fellow - country- 
 men clear cut definite like Mazzini con- 
 ception . time meant ‘ spirit todoa 
 thing thing sake , pleasure 
 ” ( * Kunst und Poli > xi . ) . time , 
 ideal resembles Fichte proclaimed 
 fellow - countrymen [ rench Berlin : 
 “ Germans , ” said Vichte , ‘ possess deep spiritual 
 capacity believe national unity , 
 signs political world flickering , vanish- 
 ing , orextinct . ” * * Itisacharacteristic , ” ’ wrote Wagner , 
 “ German life builds ; ever- 
 lasting God lives long raises trom 
 temple glory ” ? ( Burlingham ‘ ‘ Wagner , ” 
 p. 274 ) . Wagner theory order discover 
 essence , strip attributes 
 national character , order discover pure 
 elernentary German character , revert 
 period German history began , sounds like 
 dim far - echoes philosophic age gone 
 . Lut Wagner pervading dominant 
 theory national ideal rallying point 
 national unity , national ideal 
 vividly impressed nation artists , 
 theory harmony best teachings 
 Schiller German poetry , Stein German states- 
 manship , Fichte German philosophy , finally , 
 Mazzini exponent general drift 
 tendency European cuiture . Seli - reliance , self- 
 assertion indispensable qualities artist 
 character . saps qualities 
 menaces existence Art . Wagner inveighed 
 France source artistic degrada- 
 tion country . Napoleon motto , ‘ * Dépayser , ” 
 fatal relf - respect , artistic 
 creation patriotism . imitative , re- 
 ceptive , undiscriminating Germans looked 
 Paris manners , fashions , - cans , dramas , 
 ‘ ‘ William Tells ” * “ Fausts , ” 
 played hands , hands 
 thought : ‘ reward 
 sport Paris salons , deserve ” ( ‘ * Der 
 Freischiitz Paris , ” ii . ) . Mazzini took 
 parable Parisian influence art ; 
 tnough Italians imported copied 
 Parisian art gave self - help ; denationalised 

 Ky 

 

 Wagner critical writings 
 Wagnerites attach great value , 
 Mazzini mention , 
 mention . ‘ Music art 
 world explains consciousness distinctly 
 profound philosophy , ” & c. ( ‘ ‘ Beethoven , ’ 
 translated Dannreuther , p. 41 ) . , 
 places , Wagner confuses speculative 
 artistic attitudes mind , apparently 
 reason . , dream theory Wagner , 
 borrows Schopenhauer , fine metaphor , 
 equally applicable , , art . ‘ “ Poetry , ” 
 said Keats , ‘ like dream Adam ; weawake 
 find true . ” Schopenhauer , ( Wagner 
 ) , limits music , rides 
 death . , verse voice : Wagner 
 reverses Voltaire ( Addison ? ) dictum , declares 
 essential test good poetry 
 music . regarded 
 merest paradox 

 theories Wagner 
 wild abstract , special applica- 
 tion compels admiration . Thereis , seen , 
 fallacious hankering ‘ free , strong , 
 noble man nature . ” 
 legends myths lived — Sir Henry Mayne , ‘ et d 
 genus omne , ” believed — highly artificial 
 age ; artistic impulse impelled Wagner 
 choose legends myths 
 natural , national import , 
 human nature unmasked , 
 human nature hidden dreamy 
 mystic veil ( cp . ‘ * Music Future , ” p. 38 ) . 
 Hegel — ‘ ‘ subject music 
 collective interest ’ ( die Gemeinde ) . ‘ better 
 subject 1 chosen Enchantment ” ( Zauberei 

 magical art shapeless , subjectless 

 pure colour - painting Hegel dreamed , 
 | Turner tried arrive , religion 
 dogmas ( “ Beethoven , ” p. 33 , translated Dann- 
 reuther ) . development music 
 regarded inspiring entrancing 
 invested deceptive finality . Art lose 
 vitality spirit attained finality form . 
 , Wagner observation poetry soon 
 pass philosophy direct music 
 ( ‘ Music Future , ” p. 30 ) , accepted 
 declaration absolutely necessarily 
 true , betrays profound insight char- 
 acteristic tendencies modern poetry 

 bad philosophy fine artistic 
 instinct Wagner trace 
 found essay Mazzini , 
 subject - matter remarks . writings 
 Mazzini W agner alike noble partiality 
 ‘ man best quality . ” thought 

 brutal — Verdi endows art strong | 
 

 living work , kneaded mud , hatred , 
 blood , let coldly , ‘ Dear Sir , 
 lacks taste ; distingué . ’ Distingué ! Michael 
 Angelo , Homer , Dante , Shakespeare . Beethoven , 
 Cervantes , Rabelais — distingués 

 Subs equently , Bizet discusses classes 
 musical eis : ‘ * * Believe , partial criticism 
 euel , terrible , mortal weapon . pupil 
 friend Halévy , 
 received confidences matter . 
 high position indisputable glory console 
 odious unjust attacks 
 subjected . suffer life 
 ridiculous accusation Wagnerism , 
 particularly sensitive . ” Concerning pedantic 

 2 — — eta , 
 stopped notes slight vibrato , 
 thorough master instrumentation 
 Berlioz carried characteristics 
 treatise , varied according 
 construction manipulation instrument 
 viola cello received 
 slightly different treatment violin , 
 absence E string addition lowe ; 
 string , C , shifts effect open strings 
 chords scales E C important 
 factors 

 flat movement Beethoven C minor 
 Symphony referred key colour 
 advocates . regard construction 
 tonality principal instruments employed , 
 forgiven calling , explanation sake , 
 ‘ Air Variations strings , ” interspersed 
 | short Symphonies happy contrast 
 | wind , Beethoven , , strings view 
 case , chose appropriate 
 key express wanted . hear : 
 played ( pitch natural time ) ] 
 |think sound lovely 
 absence open notes string ; 
 played real pitch time 
 , G — astonish orchestra con- 
 ductor remarks . “ custom 
 time tune string pitch B flat 
 | instead natural , think written 
 ; itin natural produce effect desired . 
 , 
 extent endow acertain key ( instance flat ) 
 ja certain characteristic , played ceriai ) 
 | group instruments , endow key o ! 
 flat music generally abstract 
 characteristic . 
 | guitar evenly constructed , semi- 
 } tones marked firmly open strings 
 cross metal bars nut , | 
 doubt finest ear detect difference 
 character piece played guitar , 
 jand semitone higher 
 | fingering , affixing “ capodaster ” 
 cross bar ( virtually means shifting 
 nut semitone ) , , contrivance , 
 character manipulation keys remain 
 exactly 

 argue marked difference 
 tonality accompaniment strings zither 
 key semitone higher , notes 
 open 

 able , happen apply . 
 shall , hoped , welcome 
 foreign talent — superior , 
 derive benefit therefrom . talent 
 good , better , ranks , mean 
 guided maxim ‘ charity begins home . ” 
 Germany sets example . years ago , 
 foremost violinist , Mr. Carrodus , wished 
 artistic touron Teutonic soil , advised 
 stay England , Germans 
 want . tables apparently soon turned , 
 foreign friends know 
 longer dependent musical 
 supply . Leeds Festival , essentially English 
 , emphasised change taken place , 
 organs public opinion good service 
 , like Globe , draw attention 

 OxE attempt remove 
 disgrace England lain 
 1g0 years suffering greater com- 
 positions greatest compcser remain un- 
 published . Germany given monumental 
 editions Palestrina , Bach , Handel , Mozart , 
 Beethoven , Belgium busy publication 
 works Grétry , Holland lately 
 bestirred revive forgotten Swee!}- 
 nick , England remained satisfied 
 Purcell little . 
 Purcell Society , founded 1876 , 
 published works years 
 existence , 1882 given signs life . 
 circular issued month shows 
 Society slumbered , , 
 members imagined , defunct . According 
 document , continuation publications 
 prevented difficulty finding editors ; 
 surmounted generous 
 offer Mr. W. H. Cummings , willing 
 undertake sole labour preparing Purcell 
 works publication — light undertaking , 
 remembered - seven odes 
 , | - operas , immense 
 Church music , remain published . suc- 
 cess undertaking rests solely 
 public , ought difficulty obtaining 
 sufficient number subscribers willing 
 pay guinea year honour England 
 reputation musical country 

 Dr. Starner Cantata “ Daughter Jairus ” 
 performed Special Festival Services St. 
 Marylebone Parish Church , onthe 2oth 27th ult . , 
 direction composer . brief 
 address delivered Rector , mighty power 
 music kindle religious feelings dwelt 
 eloquent terms . ‘ seed sown Dean 
 Gloucester memorable sermon recent 
 Festival beginning bear rich fruit 

 sition justified anticipatory remarks — 
 > justified , , use author 
 lad - like themes . 1 music , 
 hapt ier , time , 1¢ thought 
 il concerned , bu especially Madame Albani , | carried Mr. Stanford employed 
 Mt , Barton McGuckin , Mr. Watkin Mills , | melodies redolent sea , smacking unmistakably 
 % e solo characters sustained . dwell | forecastle . , needless , 
 efforts artists , better | glorified setting treatment , bear 
 portunity soon present . | process , retaining original character 
 ey sieeivabity . chorus orchestra | serve colouring localising purposes . 
 cellent , contributed share | descriptive parts Choral Ballad , battle , 
 satisfactory result . Following Cantata came | events immediately following , came force gran- 
 tlection pieces Mozart “ Idomeneo ’ * ; | deur , impression secure 
 “ pital - song ‘ days long ago , ” Berthold | Mr. Stanford sympathetic hearing 
 ; Mr. H. Leslie madrigal , “ Thine eyes bright , ” | appeals judgment Leeds Festival public . 
 d Overture “ E uryanthe . ” purely vocal | composer , conducted person , warmly congra 

 lieces conducted Mr. Alfred Broughton , |tulated frank success , effusion 
 ‘ warm reception ac tone ‘ ledgment success | merits case justified . Beethoven Symphony 
 ‘ ainer choir . C minor followed new work ; second 

 work second morning Bach Mass B| programme wholly taken Mendelssohn 
 hinor , imposed sufficiently arduous task } “ Walpurgis Night . ” dismiss familiar 
 dorus recovered fatigues suns things general commendation performance 

 presiding grand organ , discharged 
 task . 
 RICHTER CONCERTS . 
 Tne usual short autumn season Concerts began 

 1 
 St. James Hall 23rd ult . , auspicious con- 
 ditions , estimated size favour- 
 able attitude audience . , true , 
 measure enthusiasm generally shown Mr. 
 Richter supporters works Wagner - Liszt 
 school question ; season begun , 
 time necessary settling 
 groove holidays lifted fanatics 
 music . Concerning opening Con 
 cert , need ; programme having 
 stock pieces played , Mr. 
 Richter bdton , = ad = nauseam , 
 , truth , satiety . sclection began 
 * Kais ? Wagner , came 
 * Faust ’ ? Overture , Liszt Symphonic Poem , ‘ Les 
 Preludes * ’ ; Introduction * * Parsifal , ” * * Ride 
 Valkyrie , ” Beethoven “ Eroica ” Symphony bring- 
 ing rear . fairly representative things , 
 objection repeated rendering . 
 time , little catholicity desirable 
 interest music generally . performance offered 
 little opportunity criticism . Mr. Richter orchestra 
 , probably , played music 
 book , time familiar slightest 
 wish able Conductor . , natural result , 
 perfect ensemble 

 CRYSTAL PALACE 

 obvious reason , 
 musical critic , Leeds London 
 time , unable opening Saturday 
 Concert Palace gathered perusal 
 programme . Spanish ballet ‘ Le Cid ” 
 ( Massenet ) constituted orchestral novelty per- 
 formance , singer new Sydenham audiences 
 heard Mdlle . Ella Russell , choice songs 
 calculated display vocal dexterity 
 won credit Covent Garden . Miss Fanny Davies 
 vas heard Pianoforte Concerto Schumann , 
 interpretation works young lady fails 
 recall style touch great mistress , 
 minor pieces . Symphony Beethoven 
 C ( . 1 ) , Sterndale Bennett welcome 
 “ * Naiads * * Overture served lever duridean . view 
 close proximity anniversary Liszt birth , 
 second Concert took shape memoriam 
 homage Hungarian master , compositions 
 programme entirely , 
 exceptions Siegfried Funeral March 
 “ G6tterdimmerung , ” performance opened 
 appropriate impressive fashion , 
 Vorspiel * Parsital . ” ” lack scenic accessories 
 case piece strikingly 
 minds present thoughts memories 
 suggested occasion surroundings , 
 devotional character famous Verspiel rendered 
 eminently suitable inclusion programme , 
 apart intimate historic connection 
 Liszt Wagner . ‘ “ Symphonic Poems ” 
 given occasion [ . 3 , “ Les Préludes , ” 
 . 12 , ‘ “ Ideals ” ( Schiller ) ] , , 
 episodes tranquil pastoral charm , 
 stormy passion , produced equaliy powerful 
 impression unusually appreciative sympathetic 

 658 MUSICAL TIMES.—Novemser 1 , 1886 

 _ — . 
 previous Monday evening Mr. Watson introduced 
 choir Free Trade Hall , conjunction Dr , 
 J. F. Bridge , skilfully exhibited etfective parts 
 inefiective organ 

 interesting musical doings 
 chronicled Mr. * . Lamond Pianoforte Recital , 
 Wednesday , 13th ult . Mr. Lamond undertook 
 exacting programme , including Beethoven Sonata 
 ( Op . 111 ) , Brahms - Variations Theme 
 Paganini , Schumann ‘ Etudes Symphoniques , ” q 
 ‘ Romance ’ composition , ‘ * Don Juan ” 
 Fantaisie , transcriptions Liszt , varied 
 selection Chopin works . Evidently chief desire 
 young pianist exhibit mastery key . 
 board , remarkable young player . , 
 copying Rubinstein trick plunging 
 piece ( atfording breathing digesting 
 time audience ) , increased experience naturally 

 guide aright Mr. Lamond ; , future , 

 

 d Institute 
 | allowed recent remarkable development teaching 
 functions interfere original work execu- 
 tive department art , Concert season , 
 took place 2nd ult . , , respects , 
 important given . modest title 
 Violin Recital regular Chamber Concert given , 
 attractions instrumental portion sup- 
 plemented vocal performances Madame Valleria . 
 Mr. Ludwig Straus , refined 
 artistic playing admired Beethoven Violin 
 Pianoforte Sonata minor ( Op . 23 ) , 
 joined pianoforte Dr. Rowland Winn . 
 Sarabande Bourrée J. S. Bach Suite B miner , 
 lolique vivacious 
 : striking proofs 
 nique , repeatedly evoked 
 taking Sonata 
 mentioned , Dr. Winn played short pteces — 
 Pastorale Capriccio Scarlatti , Rubinstein Barcarole 
 G minor ( Op . 50 ) , Mendelssohn popular Presto C 
 ( . 34 * ‘ Lieder ” ’ ? ) — exhibiting case perfect 
 taste finished execution . Madame Valleria , 
 | splendid voice , provoked furore expressive 
 singing Mendelssohn Swedish * Winterlied , ” 
 impassioned dramatic rendering Schumann “ Wid- 
 mung ” ’ fairly thrilled audience . Equally charming 
 style rendering Spohr ‘ bird 
 maiden , ” obbligato , arranged violin , 
 played Mr. S S. 
 succession novelties , Festival Choral 
 y season turning attention revivals , 
 old works merit , public danger 
 forgetting , comprised current Concert scheme . 
 Sir Arthur Sullivan “ Light 
 World , ” performed Society Concert 
 | 7th ult . , previousiy heard Birming- 
 |ham 1873 , Festival year 
 | originally produced . large scale undramatic 
 |character work probably responsible 
 |extent neglect , contains charming music 
 | misses great , orchestral writing 
 exceedingly picturesque elaborate . 
 ithe performance speak somewhat qualified 
 ; } terms , parts admirable , fre- 
 | quent evidences insufficient rehearsal , especially 
 obedience invitation 

 violin solo , Nocturn 

 Redeemer . dramatic spirit expressive power 
 performance fine , sang 
 solo , * Daughters Jerusalem , ” fervour 
 splendour voice fairly enraptured audience . 
 concerted numbers , Mrs. Payton Mr. Grice rendered 
 efficient aid . choruses generally admirably 
 rendered , lengthy elaborate chorus “ pour 
 Spirit ” especially effective . Mr. Gaul rendered 
 excellent service organ , Mr. Stockley conducted 
 judgment decision 

 rth ult . , Mr. R. Rickard gave Pianoforte 
 Recital Acocks Green Institute . programme 
 comprised specimens Bach , Handel , Beethoven , Chopin , 
 Gottschalk , Raff , Liszt , rendered 
 unexceptionable taste technical skill . Mr. Rickard met 
 warm welcome large audience . 
 vocalist Miss Preston , possesses pleasing soprano 
 voice sings expressively , accompaniments 
 carefully rendered Miss Jessie Hiley 

 evening Birmingham Sunday School 
 Scholars ’ Festival opened ‘ Town Hall , 
 conductorship Mr. A. R. Gaul , performance 
 composer Cantata “ Ruth ” prominent 
 place . choir numbered 500 voices , 
 principal vocal parts rendered Miss Clara 
 Surgey , Miss Delia Atkins , Miss Lilian Mills , Mr. 
 ‘ Tom Horrex 

 perché . ” Mr. Stockley conducting left 
 desired 

 Messrs. Harrison Concert , 25th ult . , 
 noteworthy departure usage precedent , inasmuch 
 consisted essentially orchestral music instead 
 usual ballad operatic miscellanies public 
 taught look occasions . 
 renowned band Dr. Hans Richter possession 
 orchestra , treated audience perform- 
 ance Beethoven “ Eroica ’’ Symphony 
 composer “ Leonora ’ ? Overture , . 3 , 
 heard Birmingham day . orchestral 
 selection comprised examples Wagner — 
 “ Lohengrin ” Prelude “ Walkirenritt , ” 
 needless band justice . Berlioz 
 arrangement Weber “ Invitation , ” Mendelssohn 
 ‘ Hebrides ’’ Overture remaining items 
 orchestral selection . vocalists Miss Hope Glenn 
 Mr. E. Lloyd , successful 
 Lancelot song Dr. Heap Cantata “ * Maid 
 Astolat 

 MUSIC YORKSHIRE . 
 ( CORRESPONDENT 

 estimation great Russian pianist | 
 held amply shown large audience 

 assembled Victoria Rooms . known 
 opportunity kind 
 years , doubt fact tended 
 increase number auditors . M. Pachmann selected 
 varied interesting programme , played 
 tom memory . Beethoven Sonata ( Op . 101 ) occupied 
 foremost place , received artistic rendering . 
 items follows:—Impromptu ( . 4 ) , 
 Schubert ; Rondo E flat , Field ; Faschingshwank ( Op . 
 % ) , Schumann ; Etude de Concert , Liszt ; ‘ “ ‘ La Fileuse , ” 
 Raff ; “ Gutschwundenes , ” Gluck ; Toccatina , Hen- 
 selt ; Nocturne , Etudes , Barcarole , Scherzo ( . 3 ) , 
 Chopin 

 2oth ult . Madame Norman - Néruda Mr. 
 Charles Hallé gave Violin Pianoforte Recital 
 Victoria Rooms . programme follows : Grand 
 Pianoforte Sonata D minor ( Op . 29 , . 2 ) , Beethoven ; 
 Violin Concerto F sharp minor , Vieuxtemps ; 
 Violin Pianoforte Duets , Heller Ernst ; Pianoforte 
 Solo ( ) , Impromptu F sharp , Chopin ; ( b ) Spinner- 
 lied ( Wagner ) , Liszt ; Violin Solos ( ) ‘ * Berceuse Slave , ” 
 P. Néruda ; 6 . Hungarian Caprice , Raff ; Grand Fantasia , 
 Violin piano C , Schubert 

 YIIM 

 complete prospectus thirteenth 
 | Choral Orchestral Concerts issued 

 meeting 2oth ult . arranged 
 j ; * popular ” Concerts , addition 
 Subscription nights , Thursday 
 evenings , placeof Saturday . word “ popular ” 
 mislead . Let explained signifies lower rates 
 admission , lower character music , respect 
 class music practical difference 
 sets programmes . mentioned month , 
 Symphonies Beethoven per- 
 formed . produced chronological order , 
 Choral Symphony concluding series Vebruary 8 , 
 1887 . selections orchestral music 
 Committee usual judgment , happy 
 | mixture grave gay , lively se 
 | noticeable . works per- 
 | formed time following : — Gade Concerto 
 | violin orchestra , chief wil ! taken 
 | Mr. John # . Dunn , young executant , 
 | highly promising impression Concerts 
 years ago ; Overtures , Auber “ La Siréne , ” Derlioz 
 * * Corsair , ’’ Cherubini ‘ * Lodoiska , ” Cowen * Festi- 
 val ” ; miscellaneous , Orchestral Scene , 
 “ Forest Arden , ” Henry Gadsby ; “ 
 ' Mountain ” “ Village Life ” ‘ Scenes Poe- 
 tiques , ” B. Godard ; Rakoczy March , Liszt ; Selection 
 A. C. Mackenzie opera ‘ * Troubadour " — viz . , Prelude 
 Act I. , “ Jeu de Paume ” Masque music , Entr’- 
 acte Act III . ; Spanish Ballet ‘ “ Le Cid , ” 
 selection Suite ‘ Scénes Pittoresque , ” Massenet ; 
 | Selection Suite “ Aus Aller Herren Lander , ” 
 Moskowski ; Capriccio Italien , grand orchestra , Tschai- 
 kowski ; Interlude Acts III . IV . 
 ‘ “ Tannhauser ” ( Tanniduscr Pilgrimage ) , Wagner . 
 |Goetz F , Mendelssohn minor , Schubert ( un- 
 | finished ) B minor , Schumann . 4 , Sym- 
 | phonies announced addition Beethoven set . 
 Concertos Beethoven , Violin D 
 Pianoforte , No.5 , E flat ; Max Bruch Concerto violin , 
 | No.1 ; Liszt No.1,in E flat , pianoforte ; Mendelssohn 
 E minor , violin ; Schumann Pianoforte , probably 
 lA minor , Pianoforte Concerto proper . Overtures 
 miscellaneous pieces performed need 
 detailed . suffice composers com- 
 prise names Sterndale Pennett , Berlioz , Rossini , 
 Schubert , Wagner , Weber , Haydn , Mozart . 
 understood foregoing inclusive 
 announcements Subscription series Concerts . 
 addition names vocal instrumental 
 soloists mentioned letter , note 
 Miss Carlotta Elliott , Miss Thudichum , Madame Clara 
 Samuell , Madame Marian Mackenzie , Mr. E. Lloyd , Mr. J. 
 Bridson , Mr. W. H. Burgon , vocalists ; Mr. J. F. 
 Dunn , violinist , referred . orchestra 
 number eighty - performers . gave names 
 important choral works month . Concerts 
 \ begin , stated , December 6 

 662 

 d | alor 1¢ fell short hi shest | standard , 
 good orches 

 1 pice 5 | safe Boston w 
 Professor Templeton , Uni vers sity . | tral music New York . nount saying 
 leading artists Herr err od city consummation 
 ( cello ) , Eos Mor las ; ( tenor ) . ! ; flat | long wished , finally brought 
 Mozart B given , violinist played } jue fi instance intelligent 
 Beethoven Romance : binding t hat confi 

 Merthyr , Concerts hav ] — 
 St. David Church 

 Ir interest trace current musical 
 feeling Saxon capital , , , 
 characteristic centre Teutonic Fatherland . 
 operas given month 
 strongly stamped Wagner seal out- 
 siders ex ‘ pect . works great 
 master himse , imitators , recently 
 given ; large house drawn “ Die Folkunger , ” 
 ingenious setting ofan unusually spirited text Mosenthal . 
 interest plot centres Magnus , heir 
 Swedish throne , forced “ wicked uncle ” 
 taking oath deny identity 
 remain perdu cloister . circumstances 
 combine bring light confound con- 
 spirators brought tact , ‘ business ” 
 interest acts . 
 composer special interest English musicians 
 fact deputy , 
 |the successor late Herr Merkel organist 
 | Catholic Cathedral . Dresdeners speak Herr 
 | Kretschmer belonging Wagner school , 
 fact score divided numbers , runs 
 scene scene continuously , occasional 
 |combination wood brass recalls ‘ ‘ Lohengrin , ” 
 | styles diametrically opposed . “ Die Folkunger ” 
 contains Coronation March , figure 
 London Concert programmes 

 work ofa disciple hero Bayreuth , 
 1as given , éclat , 
 ‘ Queen Sheba ” ( Goldmark ) , little recom- 
 mend tne Fraulein Malten , compares 
 unfavourably Herr Kretschmer work , seen 
 artistic point view public . 
 Wagner Trilogy given 
 weeks ; “ Siegfried ’ attractive 
 dramas , * * Gétterdammerung ” ’ best attended ; 
 occasion English language , 
 indigenous transatlantic form , outweighed 
 German house . physical 
 power Herr Gudehus , tenor , probably 
 exceeded analogous effort witnessed 
 operatic stage . crowded house 
 commencement winter season drawn 
 “ Lohengrin . ” title rdle Elsa fell respec- 
 tively lot Herr Gudehus Frailein Malten , 
 yore ; new baritone , Herr Schrauff , showed dignified 
 bearing good presence , combined correct intona- 
 tion , Telramund . converse qualities 
 € conspicuous Herr Reise F¥ean Leyden , 
 ‘ ‘ Prophéte ” Meyerbeer , mutilated finely 
 rendered version recently produced . hearing 
 popular singer attack B flat C natural chest , 
 suspicion break quality tone , 
 sustain notes indefinitely , wished stature 
 — head shoulders Bertha 
 evening — assimilated compass 
 fine organ . Intending visitors interested 
 hearing band plays Sunday evenings 
 known Belvedere , intelligent direction 
 Herr Trenkler , shows marked improvement 
 years . music Catholic Church ( home 
 ‘ * Dresden Amen ” ’ ) shows change ; recent 
 Masses performed o’clock Sunday service 
 having Beethoven C , Mozart E flat , 
 best known Agnus Dei , fine Mass 
 Reissiger D minor 

 OBITUARY 

 Tue Dr. Rea Newcastle - - Tyne Subscription 

 Concerts season 1886 - 7 announced place 
 evening rst inst . , conductorship 
 Mr. Richter , orchestra cighty - _ per- 
 formers — vocalist , Mrs. Hutchinson . second 
 Concert ( December g ) Gounod ‘ “ Redemption ” 
 given — principal vocalists , Mesdames Thudichum 
 Bolingbroke , Mr. Iver McKay Mr. Brereton ; 
 ( february 15 , 1887 ) new work performed , 
 probably prominent novelties Leeds 
 Festival , vocalists engaged occasion Mrs. 
 Hutchinson , Mr. Harper Kearton , Mr. Bridson . 
 chorus orchestra 350 performers . 
 Tut Harvest Festival St. Mary Parish Church , 
 3alham , held Sunday , 3rd ult . service 
 Matins sung Dykes F Woodward E flat . 
 Evensong canticles sung Prout F , 
 Anthem * O men praise Lord ” ( Dr. 
 Joseph C. Bridge , Chester ) , offertory Anthem , 
 Hallelujah , ” trom ‘ Mount Olives ” ( Beethoven ) . 
 Mr. H. W. Weston , A.C.O. , Organist Choirmaster 
 church , presided organ , Evensong gave 
 short Recital , including , pieces , Smart Solemn 
 March E flat , Allegretto B minor ( Guilmant ) , Allegro 
 vivace ( Handel } , Air Variations Sym- 
 phony D ( Haydn 

 Concerts Mr. Henschel series 
 London Symphony Concerts place 17th 
 25th inst . include Brahms Symphony 
 D , . 2 ; Beethoven Concerto pianoforte , violin , 
 violoncello C ( Op . 56 ) ; “ * Good Friday spell , ” ’ 
 Wagner “ Parsifal ’’ ; Prelude tothe act Mackenzie 
 opera ‘ Troubadour , ” & c. second Concert , 
 Beethoven Symphony B flat , . 4 ; Schumann 
 Concerto minor ( Miss Fanny Davies ) ; Saint - Saéns 
 Ballet music , ‘ ‘ Henry VIII . ” ; Idyl , “ ‘ Evening 
 Sea Shore , ” Mr. F. Corder , form principal 
 features 

 Sir ARTHUR SULLIVAN Oratorio “ Prodigal Son ” 
 given Monday , 25th ult . , Stepney Meeting 
 House , choir St. John , Waterloo Road , S.E. , 
 solos taken Master Warren , Miss Tunnecliff , 
 Mr. H. Cooper , Mr. Frederick Winton . Mr. Henry J. 
 B. Dart presided organ . second 
 miscellaneous 

 numbering voices . 
 organ , Mr. Charles O. M. Philps , Crganist 
 Choirmaster church , conducted . conclusion 
 service Organ Recital given Mr. Sidney 
 Hill , appreciated 

 customary Harvest Festival services St. James 
 Church , Knatchbull Road , Camberwell , given 
 Thursday evening , 2 : st ult . , following Sunday . 
 musical portion services rendered 
 manner worthy high commendation , comprised 
 Smart Morning Evening Sevice F , Wesley 
 Anthem “ Wilderness , ” Gadsby ‘ Blessed 
 , ” Goss ‘ * Fear , O Land , ” Hallelujah 
 Beethoven ‘ “ * Mount Olives . ” Mr. R. Felix Blackbee , 
 F.S.Sc . L. , presided organ ability 

 Harvest Festival Parish Church , West Hack- 
 ney , 18th ult . , highly successful , choir 
 100 voices , orchestra performers 
 taking music , included A. R. Gaul 
 Cantata “ Holy City . ” solos sung Mr. 
 Thomas Kempton , Mr. Frost , Mr. Henry Thom , Masters 
 Humm Lewis . Mr. H. Baynton led band , Mr. 
 R. T. Gibbons , F.C.O. , presided organ , Mr. 
 Francis L. Kett , Choirmaster , conducted . Cantata 
 whoie service excellently rendered 

 j | Sunday , roth ult 

 requested state Miss Florence 
 series Pianoforte Recitals Berlin 
 january , 1887 , play selections works 
 Couperin , Rameau , Scarlatti , Alberti , Symonds , Greene , 
 Handel , Bach , Beethoven , Mendelssohn , Weber , Schumann , 
 Pe Brahms , Liszt , Moscheles , Bennett , Bargiel 

 Moszkowski , Florence 

 pti ’ 

 jthe Girzenich Hall Cologne , 
 Willner , season commenced . 
 noteworthy works performed following : 
 : Elijah , ” Bach Passion Music ( St. 
 Berlioz ‘ Roméo et Julictte ’ ? Symphony , 
 ‘ | Brahms Symphony , . 1 , Niels Gade Symphony , 
 |No . 4 , Rubinstein new Symphony , Willner 127th 
 > salm , Symphonies Beethoven . 
 | minor choral orchestral works appear composer 

 names Liszt , Wagner , Raff , Vv olkn mann , Cherubini 

 article * * true condition voice - production , ” 
 contained recent number Cincinnati Musical 
 Standard , writer ( Mr. Edmund J. Myer ) maintains 
 “ English language destined language 
 future song language literature 
 commerce . ” words , English , 
 opinion writer , universal language dreamt 
 . . mean- 
 time , far “ song ” concerned , rival claims 
 distinction unnaturally advanced neigh- 
 bouring nations , Germans . , 
 , severe shock susceptibilities 
 read , , following paragraph 
 Neue Zeitschrift fiir Musik 22nd ult . , viz . : 
 “ performance Wagner ‘ Tannhauser , ’ 
 direction Herr ( unfortunately ignorant 
 exact Danish equivalent Mr. ) Swendsen , 
 Court Theatre Copenhagen , Anton Schott sang 
 title - rdle English language , deference 
 wishes public , declined 
 ears offended hated German tongue . ” Herr 
 Schott , London aware , versatile artist , 
 equally mingled I¢alian version 
 mellifluous Danish coadjutors , 
 preference given Copenhagen audience 
 English language ( interpreted German ) , 
 distinct compliment paid country , preliminary 
 step , doubt , direction indicated Mr. Myer 
 Cincinnati Musical Standard 

 Hubert Ries , distinguished member - known 
 musical family , younger brother 
 Ferdinand Ries , pupil Beethoven , died September 
 14 , Berlin , having attained mature age nearly 
 eighty - years . ‘ deceased artist vears 
 past highly esteemed violinist Berlin opera . 
 author meritorious ‘ t Violin School 
 beginners , ” eminent teacher successful composer 
 instrument , quartet evenings formed 
 time distinct feature musical life Prussian 

 676 

 MUSICAL TIMES.—Novemper 1 , 1886 . 677 

 best pianists heard , competent choir Di iblin Musical Society d veteran 
 5 perform great works Beethoven , Liszt , Chopin , | Conductor , present , example , 
 se Rubinstein , equally home delicate combina- | performance given season 
 ‘ 0 tions stringed trio . mention | society Dr. Stanford “ Holy Children , ” 
 d , Orchestral Union Dublin , think } impossible written 
 success . iI . great deal “ Hibernicus ” 
 Having given idea musical doings | music Dublin , particular 

 St Dublin , W ill permit mi uke remarks | hich level nt city , 
 1 article pen Mr. C. L. Graves , entirely accord . good 
 1S appears October number , subject * * Musi-| fortune boy hear M John Stanford , 
 ly cal talent Ireland ’ Mr. Graves obser- | Lablache Dublin amateurs , fine voice 
 vations concur , think practical acquaint- | style , inimitably rich hum¢ regard 
 ut ance country possessed critics } Moot sre Lover , position , fully stated , 
 10 Ireland ; think written any- | commend correspondent 
 1B thing friendly spirit . , | ‘ omeateien impossible deny 
 m Irishman chooses ridiculously exaggerated | remarkable lyrical gift , , , 
 statements respecting music Ireland , necessary heretical prefer known satirical poems ; 
 undertake task dis paragement thi t , view . lite surroundings , claim 

 CarcuttTa.—Mr . Ernest ‘ later Concert Town Hall , 
 September 7 , decided succe s. Mr. Slater gave ample evidence 
 expression delicacy uch pianoforte selections , 
 ably supportea talented amateurs , Mencels- 
 sohn Hear Prayer rendered choir 
 voices , solo excellently sung Mrs. Davies 

 CuICHESTER.—There fairly numerous audience 
 Assembly Room ‘ Il hursday evening , 14th ult , occa- 
 sion Mr. E. H. Thorne Piarotorte Recital . 
 programme comprised sele ctions works Beethoven — Sonata 
 G , Sonatain F minor , Grand Sonata B flat . second 
 devoted writings Chopin . Mr. Thorne perfor- 
 mance proved great treat , frequently warmly 
 applauded 

 Crewe.—Under auspices Philharmonic Society , even- 
 ing Ballad Concert given Town Hall , Tuesday , 
 September 28 , following artists:—Miss elle , Miss 
 Susetta Fenn , Mr , Sinclair Dunn , Mr. Jcehn Rid 

 Kiver . 6s . 
 7ING , OLIVER.—Twelve Original Voluntaries . 
 NX Composed Organ Harmonium . 1s 

 pe Ptsce ® : ALD , JAMES.—Andante Haydn 
 Symphony , Letter T. Arranged ( Organ . 2s , 
 — Allegretto Beethoven Trio Piano , 
 Violin , Violoncello . Op . 70 , No.2 . Arranged Organ , 2s . 
 OVELL O PUB LICATIONS TONIC 
 SOL - FA NOTATION . Translated iby W. G. McNauacut . 
 HANDEL . — “ Messiah . ” rs 

 SULLIVAN , ARTHUR . — < Golden 
 Legend . ” 2s 

