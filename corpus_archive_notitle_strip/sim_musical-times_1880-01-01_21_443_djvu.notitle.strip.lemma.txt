new foreign publication 

 BEETHOVEN.—Fantasia for Piano , Chorus , and orchestra . s. d 

 Op . 80 . arrange for two Pianos , four hand , by H. v. Bilow 20 0 
 BOHM , TH.—Fantasia on a Sonata of Hummel for flute and 

 BACH , J. L.—Suite , e major 
 BACH , 7 — Fantasia , c minor . 
 BEET ! run . — sonata quasi una Fantasia , cc sharp : minor 

 Op . 27 , no . 2 . 
 SCARLATTI — Cat ’s " Fugue 
 BEETHOVEN.—Sonata , a flat major . 
 HAYDN.—Fantasia , c major ... 
 FIELD , J.—Rondo , e flat major .. ao 
 BEETHOVEN . — thirty - two variation , c minor ; .. 
 HANDEL.—Gigue , g minor 2a 
 10 . BEETHOVEN.—Sonate pathétique , c minor . 
 11 . SCHUBERT , F.—Impromptu . 8 . , no . i 
 12 . MENDELSSOHN . Capriccio . pp . 5 f 
 13 . WEBER.—Momento Capriccioso vis 

 sell at half - price . 
 Nos . 1—5 , in one volume , bind in cloth ... éeo net 7 6 
 6—10 , in one volume , bind in cloth ... es ae aes Se 
 London : NovettLo , Ewer and Co 

 WHEWUNWRWWUW PD 
 oooooo0ooo0o0de 

 BEETHOVEN . oar March from the Eroica Symphony . s. d. 
 Arran 5 by es 20 
 BERLIO he Marche hongroise from " La Damnation de 
 Faust . " arrange by A.Guilmant ... ese a0 6 0 
 GOUNOD , CH.—Marche Solennelle si oe a 's § 
 GUILMANT , A.—Marche Triomphale . op . 34 ... pre of HLS 
 LEYBACH , j. — Prise d’Armes . arche Op . ier es Le | 
 — Souvenirs et Regrets . Marche Funébre bie na ooo 2 6 
 WESTBROOK , W. J.—March in e flat ... * oe a ee 

 London : NovE.Lto , Ewer and Co 

 the Children ’s Bach ( 32 solo , 4 duet ) , with Portrait and ' Biograstt 

 the — — s Beethoven ( 30 solo , 6 duet ) , with Illustrated Bio 

 aa 

 in a recent number of that excellent and useful 
 weekly journal , Replies , we perceive that an attempt 

 have be make by a correspondent ( in whose initial , 
 " C.K. S. , " we recognise a frequent contributor to 
 our own page ) to answer the following question : 
 " can you give I the origin and precise meaning of 
 ' high development , ' as apply to pianoforte play- 
 ing ? " now , of course it be easy enough to give the 
 " origin " of this term , because , as everybody know , 
 it be invent by a knot of artist , either of foreign 
 extraction , or with strong foreign proclivity , to 
 designate a certain school of pianoforte - playing , for 
 the furtherance of which an Academy have be 
 found ; but to describe its " precise meaning " be 
 a task which we have vainly endeavour to per- 
 form , and which " C. K. S. " himself have not more 
 successfully grapple with . true it be that he 
 tell we what it be not , but only in the gentle 
 manner hint at what it be — or rather what it may be . 
 " carry our memory back to a period of more than 
 half a century , " he say , " we recall the great perform- 
 ance of the illustrious master of the pianoforte , " 
 and then he mention a number of the grand 
 artist , amongst whom be native of England , 
 France , Germany , Italy , and Russia . as none of 
 these pianist have term themselves apostle of 
 " high development " what it be not be of course 
 evident . what it be , or may be , be thus shadow 
 forth : " exaggerated reading of the classic work 
 of Beethoven and other great master , illustrate 
 by unauthorised change of time , by substitute 
 Andantes for Adagios , and Prestos for Allegros and 
 allegretto , have be long noticeable in the perfor- 
 mance of some pianist , even of the most exalted 
 fame . shall we call this a high development ? " 
 we pause for a reply 

 HER MAJESTY ’S theatre 

 MONDAY popular concert 

 four evening concert be give by this institution 
 during the past month , previous to the usual adjournment 
 for the Christmas vacation , viz . , on the rst , 8th , 15th , and 
 22nd respectively , the programme offer , as usual , 
 numerous attraction to the lover of classical chamber- 
 music . at the first - name concert Madame Norman- 
 Néruda introduce , as in the previous month , a string 
 quartet by Haydn , which have not be perform here 
 before , viz . , that in e major ( op . 17 , no . 1 ) , which be 
 play in excellent style by the lady refer to , who be 
 assist by MM . Ries , Zerbini , and Piatti . this bright 
 and genial composition , with its somewhat antiquated 
 lead theme in the first movement , its graceful minuet , 
 its busy and vivacious finale , be evidently much appre- 
 ciate by the audience , and will doubtless meet with a re- 
 petition before long , although some thirty - five similar 
 work by this prolific composer as yet await their introduc- 
 tion to the frequenter of these concert . the pianist on 
 this occasion be Mr. Charles Hallé , who play with his 
 well - know lucidity and finish Beethoven ’s Sonata in D 
 major . Signor Piatti receive a perfect ovation after his 
 performance of Locatelli ’s sonata in D major , originally 
 write for the violin and transcribe for the instrument of 
 which Signor Piatti be such a consummate master . he 
 be most ably support by M. Zerbini , who play the 
 pianoforte accompaniment . the concert conclude with 
 an admirable rendering on the part of Madame Norman- 
 Néruda and Mr. Charles Hallé of Beethoven ’s so - call 
 Kreutzer Sonata , for violin and pianoforte , a great favourite 
 with this , as indeed with any other audience , and which 
 elicit the usual round of applause . Herr Henschel be 
 the vocalist , and contribute an Aria from Handel ’s opera 
 * " Rinaldo , " as well as two song by Schubert . the singer 
 accompany himself on the pianoforte , a proceeding ad- 
 mirably adapt to the drawing room , but scarcely to a 
 public performance in a large hall ; for whatever the per- 
 formance under such circumstance may gain in artistic 
 sympathy , be more than counterbalance by the want of 
 directness of appeal to the audience on the part of the 
 singer . we fancy Herr Henschel will not be long in 
 recognise the truth of these remark . his double 
 performance be , however , an admirable one , and result 
 in the usual call for an encore 

 at the second Concert of the month M. Sainton be 
 the lead violinist , in which capacity he play , in 
 conjunction with MM . Ries , Zerbini , and Pezze , Men- 
 delssohn ’s String Quartet in A minor ( Op . 13 ) , and Haydn ’s 
 String Quartet ing major ( Op . 77 , no . 1 ) , both work be 
 zendere in a masterly manner , as indeed the mere mention 
 ef the name of these artist sufficiently indicate . 
 Mdile . Janotha reappear after her recent indisposition , 
 and give a very poetic reading of Beethoven ’s Pianoforte 
 Sonata in E minor ( Op . 90 ) . this be the Sonata dedicate 
 to Count Moritz Lichnowski , consist of two movement 
 only , in which the love - romance of the Count be say to 
 have find its musical illustration . however that may be ( it 
 be Schindler who first relate the circumstance ) , the work in 
 question be one which demand a distinct artistic individu- 
 ality on the part of its exponent , a quality which Mdlle . 
 Janotha possess in a marked degree , and which partly ex- 
 plain the fact of she having so rapidly become a favourite at 
 these concert . Miss Hope Glenn sing Beethoven ’s well- 
 know concert Aria ' " ' in questa tomba , " as well as Stern- 
 dale Bennett ’s ' * Castle Gordon , " there be an absence 
 of artistic warmth in the lady ’s delivery which may , 
 perhaps , have be the result of nervousness . the 
 conclude number of the programme consist of two 
 piece by representative composer of the modern german 
 school , write for two pianoforte , the one entitle " La 

 Belle Griseldis , " call by its composer ( C. Reinecke ) an 
 ' ' Improvisata , " but treat in the regular form ; the other 
 be a " Tarantelle " by Joachim Raff ( Op . 84 , no . 12 ) . 
 these interesting composition be well execute by 
 Misses Hopekirk and Ockleston . Sir Julius Benedict act 
 as accompanist , an office in the discharge of which he 
 greatly excel 

 the third Concert refer to bring a repetition of 
 Haydn ’s genial Quartet in b flat major ( Op . 50 , no . 1 ) , 
 which have be first introduce to the audience in the 
 previous month , and whose interest in the work have 
 evidently gain by the second hearing . Haydn be so 
 talkative , and at the same time he have so much to say that 
 be worth listen to , that his quartet require a repetition 
 in order to be fully appreciate , albeit ' ' good old Haydn " 
 be now - a - days look upon by some as a composer whose 
 work have long since be supersede and may be under- 
 stand at a glance . Mdlle . Janotha be again the pianist , 
 and reap endless applause in her characteristic render- 
 ing of a capriccio in b minor , by Brahms , a romance 
 by Madame Schumann , and the novelette in b minor 
 by Schumann , which be encore . the vocalist be 
 Miss Lillian Bailey , whose bright voice tell well in 
 her delivery of Bach ’s joyous Pfingst - Cantate , ' ' Mein 
 glaubiges Herze , " although her singing be evidently 
 somewhat influence by nervousness . the second vocal 
 portion of the programme be two song write by 
 Herr Henschel and sing with much spirit by the lady 
 just mention , the composer accompany on the piano- 
 forte . Beethoven ’s beautiful and ever - popular Septet — 
 play here for the thirty - fourth time — conclude the 
 programme , the executant be Madame Norman- 
 Néruda , MM . Zerbini , Clinton , Wendtland , Wotton , 
 Reynolds , and Pezze . Signor Piatti at this , as in the 
 previous Concert , be prevent by indisposition from 
 occupy his regular post at the violoncello 

 for the last Concert of the month the follow pro- 
 gramme have be announce : Schumann ’s Quartet in A 
 minor ( Op . 41 , no . 1 ) , a Pianoforte Sonata by Beethoven , 
 Mozart ’s Sonata for pianoforte and violin in F major , 
 and song by Handel and Buononcini ; the executive artist 
 be Mesdames Norman - Néruda and Janotha , MM . Ries , 
 Zerbini , and Pezze ; vocalist , Mr. F. King . we must 
 defer our notice of the performance until our next number . 
 these Concerts will be resume on the 5th inst 

 HERR HENSCHEL ’s CONCERT 

 Special Correspondent of the Daily Telegraph 

 Mr , Bennett be well - know to english musician as one 
 of our able critic . a musician himself , he add to re- 
 . markable insight and critical judgment — intensify by 
 long experience — the gift of a fine , lucid , and nervous 
 english style . . . . as a permanent record of these famous 
 performance , Mr. Bennett ’s little book can not be too highly 
 recommend , and those amateur who wish to have a 
 general idea of Wagner ’s theory will find they describe 
 in language less enthusiastic certainly than in the letter of 
 the composer ’s avowed disciple , but without prejudice , 
 and above all without a trace of intention to ridicule . . . . 
 in the appendix be interesting sketch of Nuremberg 
 and Salzburg , and an account of a visit to the grave of 
 Mozart , Beethoven , and Schubert . ”—Manchester Examiner 

 we find so much which we can cordially indorse that 
 we have great pleasure in recommend the little volume 
 as an interesting record of the important event which it 
 describe . ”—Academy 

 the MUSICAL TIMES.—January 1 , 1880 

 fore be look upon as an important event of her artistic 
 career . that the faultless technique , the artistic earnest- 
 ness and thoughtfulness , which characterise the lady ’s 
 interpretation , should meet with due recognition in an 
 eminently musical country be , indeed , almost a foregone 
 conclusion . nevertheless , the fact must be highly gratify- 
 e to the artist , as well as to her numerous english friend , 
 that from the commencement of her visit in Germany the 
 reception accord to she have be everywhere of the most 
 flattering nature . at Disseldorf , Hamburg , Brunswick , 
 Berlin , Leipzig , where she have hitherto appear at various 
 concert - institution , her success have be most marked , as 
 may be gather from the notice contain in the local 
 organ of the press . an enumeration of the programme 
 of concert in which Miss Zimmermann take part will 
 be find in the column of this journal usually reserve 
 for that purpose . as regard the estimate take by 
 german critic of the lady ’s qualification , it will be of 
 interest to our reader if we offer an extract from a 
 fairly representative notice , publish in the Berlin 
 Tribune of the 4th ult . that journal , refer to a 
 musical soirée give by Miss Zimmermann , with the co- 
 operation of Herren Joachim and Hausmann , say : ' the 
 execution of the pianoforte part of Beethoven ’s Trio in 
 b flat major ( Op . 97 ) be , apart from the unerring pre- 
 cision in the surmounting of its technical difficulty , 
 throughout a combination of exhaustive study , of an in- 
 timate enter into the poetic intention of the composer , 
 and of a refined taste . whoever can play like this have 
 follow the manifestation of genius step by step , until 
 the secret of his realisation of the beautiful have quite 
 reveal itself . the sonata in G minor ( Miss 
 Zimmermann ’s , for pianoforte and violin ) be a grandly 
 conceive work in four movement , which alone 
 should be sufficient to testify to the composer ’s artistic 
 maturity . among the truly remarkable production with 
 which in these latter day ' woman ' have enter into 
 competition with man , this sonata may certainly lay claim 
 to the full recognition , albeit that to our thinking the 
 scherzo , which be most favourably receive , appear , 
 because of its mazurka style , somewhat out of keep 
 with the rest . .. . it be , however , in her rendering of 
 Bach ’s Prelude and Fugue in e minor , where the entire in- 
 dividuality , and the high artistic emotion of the pianist 
 become manifest . . . . as far as we can remember , only 
 Hans von Bilow be in the same degree capable of initiate 
 us into the secret of that composer , and it be in this 
 direction especially where we hope soon to meet the artist 
 again 

 Miss Zimmermann be expect to play at one of the 
 Gewandhaus Concerts at Leipzig during the early part 
 ef this month , and it be needless to add that we shall 
 continue to follow her artistic progress in Germany with 
 much interest 

 31 

 the openig ae Dedication Festival Services at St. Edmund the 
 re Jullien tijgeand Martyr , Lombard Street , be hold on Novem- 
 me ; but r at the choral celebration , at noon , Ouseley ’s 
 n admirabiliiyice in F be sing ; and at evensong Ebdon ’s Evening 
 and Rondiwice , C. E. Horsley ’s " I be glad , " and Ouseley ’s 
 | some vocal icame even to pass " be efficiently render . on 
 Neldon , mmber 27 ( be the octave ) , Handel 's " hallelujah " 
 to what @ijus be sing after the evening service . Miss Kate 
 vell be palligstrop preside at the organ , and Mr. C. E. Tutill 
 e by abowiigducted . 
 ducte , Ww understand that the telephone be now adopt as a 
 of the Posimeans ’ of rapid communication by Mr. Alfred Hays , of the 
 s ’s Hall , qi : Box Office , 4 , Royal Exchange Buildings , Cornhill , 
 some of timestablishment be connect by this medium with a 
 om may Mge number of the most eminent merchant ’ and banker ’ 
 bhart , Migiiges in the City and with all the principal theatre ; thus 
 lace well every facility for the transaction of order . Mr. 
 many — t@mays already possess telegraphic communication , and in 
 ramme ; ng telephonic have well earn the thank of his nume- 
 violin ) , Oli patron . 
 ere watm@iigssrs . BREITKOPF and HARTEL , of Leipzig , the famous 
 it . besidfilisher of the complete - work of Beethoven , Mendels- 
 enedict pe Mozart , and Chopin , be about to add another name 
 > and pian illustrious list of composer . they announce a 

 ire instity 

 enormous difficulty which the mounting of the work 

 sent . 
 iE erren Reinecke and Schradiek be just now engaged 
 yn a concert - tour in Germany have for its special 
 dhject the performance on two successive evening of the 
 wi ple of Beethoven ’s violin sonata , to the number of 
 ten . the undertaking create much interest in musical 
 Hans Richter , the capellmeister par excellence , whose 
 ntract with the Viennese Opera terminate in May next , 
 it be state , enter upon a fresh engagement for ten 
 s as orchestral director at that institution . 
 Mdlle . Marie Wieck , the eminent pianiste , sister of 

 ymn . { Madame Schumann , have make a most successful concert 

 we subjoin , as usual , the programme of Concerts * 
 recently give at some ofthe leading institution abroad 

 Paris.—First Conservatoire Concert ( November 30 ): symphony in 
 C ( Schumann ) ; fragment from " Fernando Cortez " ( Spontini ) ; 
 overture , ' " ' Coriolan " ( Beethoven ) ; two chorus from " Israel in 
 Egypt " ( Handel ) ; overture , ' ' Carnaval Romain " ( Berlioz ) . Concert 
 Populaire ( November 30 ): overture , ' ' Phédre " ' ( Massenet ) ; Réverie 
 ( Schumann ) ; Gavotte ( Lulli ) ; first and second act , ' La Prise de 
 Chatelet Concert ( November 30 ): Reformation 
 Symphony ( Mendelssohn ) ; hymn to " St. Cecilia " ( Gounod ) ; Scénes 
 Poétiques , orchestral suite ( B. Godard ) ; Pianoforte Concerto , E 
 minor ( Beethoven ) ; Pavane du XVie Siécle ( composer unknown ) ; 
 Schiller Marsch ( Meyerbeer ) . Chatelet Concert ( December 7 ): " La 
 Prise de Troie ’' ( Berlioz ) . Concert du Conservatoire ( December 14 ): 
 Symphony , b minor ( Beethoven ) ; Pavane , Chorus from sixteenth 
 century . overture , ' Fingal ' ( Mendelssohn ) ; chorus from 
 " Oberon " ( Weber ) ; Symphony , C ( Mozart 

 Leipzig.—At St. Thomas ’s Church ( November 22 ): prelude and 
 Fugue in F minor ( Handel ) ; Motett ( Zollner ) ; prelude on " Jesu 
 meine Freude " ' ( Bach ) ; Choral Motett ( Mendelssohn ) . Euterpe 
 Concert ( December 2 ): symphony , e flat major ( Haydn ) ; Violoncello 
 concerto ( De Swert ) ; prelude to Opera " Die Albigenser " ( De 
 Swert ) ; cantata for male voice ( Brambach ) . Gewandhaus Concert 
 ( December 4 ): Pianoforte Concerto , e major ( Reinecke ) ; La Farfaletta 
 ( Scarlatti ) ; Toccata , d minor ( Bach - Tausig ) ; Symphony , C ( Schu- 
 bert ) , vocal soli . Conservatorium Concert ( December 1 ): Violin 
 Sonata , c minor ( Beethoven ) ; prelude and fugue ( Bach ) ; Cavatine 
 from " Romeo " ( Bellini ) ; suite ( Bennett ) ; Violin Sonata , d major 
 ( Mozart ) ; two movement from german Requiem ( Brahms 

 Berlin.—Bilse Concert ( November 29 ): overture , " Rienzi ' 
 ( Wagner ) ; polonaise in E ( Liszt ) ; symphony , c minor ( Beethoven ) ; 
 overture , ' ' Dimitri Donskoi " ( Rubinstein 

 Diisseldorf.—Second Concert of the Bach - Verein ( November 20 ) : 
 overture , ' Ossian ' ? ( Gade ) ; ' " Schicksalslied " ( Brahms ) ; third 
 Concerto for violoncello and orchestra(De Swert ) ; ' ' Vorder Klosterp- 
 forte , " soprano and alto solo , with chorus , organ , and orchestra 
 ( Grieg ) ; violoncello solo ( Bach and Piatti ) ; prelude to Opera , ' " ' Die 
 Albigenser " ( De Swert ) ; ' the pilgrimage of the rose " ( Schumann 

 uartett , A minor , Op . 41 ( Schumann ) ; variation for pianoforte on a 

 heme by Handel ( Volkmann ) ; trio , e flat major , op . 70 ( Beethoven ) . 
 Chamber Concert of the same ( December 23 ): quartett , ge ar 
 Pianoforte Sonata , c minor , Op . 111 ( Beethoven ) ; trio , flat 
 major ( Schubert 

 Wiesbaden.—Concert at the Curhaus ( November 28 ): serenade for 
 stringed instrument , violoncello and bass ( Dvorak ) ; Pianoforte 
 Concerto , D minor ( Mendelssohn ) ; Symphony , no . 1 , major 
 ( Mozart ) ; polonaise for Pianoforte ( Liszt ) ; overture " Corsair " 
 ( Berlioz ) . concert at the same ( December 5 ): Symphony , no . 4 , in 
 a ( Mendelssohn ) ; ' Francesca da Rimini , ' Orchestral Fantasia 
 ( Tschaikowsky ) ; overture , ' ' Leonore " ( Beethoven ) ; vocal soli 

 contribution intend for this column should indicate clearly the 
 place and date of performance , as otherwise they can not be insert 

 the MUSICAL TIMES.—January 1 , 1880 

 Miss Agnes Zimmermann ’s Concert - Tournée.—At the Stadtische 
 Tonhalle , Diisseldorf ( November 4 ): sonata for Pianoforte and Vio- 
 loncello , D major ( Mendelssohn ) ; Violin Concerto ( Witte ) ; Etudes 
 en forme de variation , for pianoforte ( Schumann ) ; Barcarole ( Rubin- 
 stein ) ; Mazurka ( A. Zimmermann ) ; Etude ( Mendelssohn ) ; Polonaise 
 Brillante , for pianoforte and violoncello ( Chopin ) ; vocal soli . Hand- 
 werker Verein , Berlin ( november23 ): Trio , C minor ( Mendelssohn ) ; 
 Sticke in Volkston , for violoncello ; Nocturne , D flat major ( Chopin ) ; 
 Gavotte ( A. Zimmermann ) ; Caprice ( Rubinstein ) , for pianoforte ; 
 Barcarole ( Spohr ) ; Hungarian Dances ( Brahms - Joachim ) , for violin ; 
 vocal soli . Concert of the Hof - Capelle ( November 18 ): Symphony , 
 " Landliche $ Hochzeit " ( Goldmark ) ; Pianoforte Concerto , no . 4 
 ( Sterndale Bennett ) ; ' ' abend " ( Schumann ) ; Gavotte ( A. Zimmer- 
 mann ) ; Rhapsodie Hongroise , o. 8 ( Liszt ) , for pianoforte ; vocal 
 solo . musical Soirée , Berlin ( December 2 ): trio , b flat major 
 ( Beethoven ) ; prelude and fugue , e minor ( Bach ) ; rondo piacevole 
 ( Sterndale Bennett ) ; sonata for Pianoforte and Violin ( Beethoven ) ; 
 Etudes Symphoniques en forme de variation ( Schumann 

 Cologne.—Concert Gesellschaft ( December 16 ) ; overture " Leonore " 
 ( Beethoven ) ; air from " creation " ( Haydn ) ; Pianoforte Fantasia 
 ( Rubinstein , play by the composer ) ; " ' Beim Sonnen Untergang " 
 for chorus and orchestra ( Gade ) ; Pianoforte Soli ( Chopin ) ; dramatic 
 Symphony ( Rubinstein ) . m 

 Sr eiengd - eamenst of the Peabody Institute ( November 29 ): 
 String Quartett , e flat major ( Mozart ) ; Quintett , a minor , op . 145 
 ( Franz Lachner ) ; air of the Countess ( ' ' Le Nozze di Figaro 

 Auburn ( New York).—Mr . Richardson ’s Academy Concert ( Novem- 
 ber 12 ): Cathedral Service in A , Cantate Domine ( Bridgewater ) ; 
 Sextett ( Beethoven ) ; Ave Verum ( Mozart ) ; requiem ( Jomelli ) ; String 
 Quartett in G , no . 12 ( Mozart ) ; Cathedral Service in A , Deus 
 misereatur ( Bridgewater 

 CORRESPONDENCE 

 BirmincHaM.—The Carl Rosa Opera Company end their second 
 wsit , for six night , on November 30 . Mignon be perform twice , 
 = on the 27th Goetz ’s Taming of the Shrew be give for the first 

 in Birmingham , and be well receive . the other performance 
 be repetition of familiar works.——Mr . Stratton ’s second popular 
 Chamber Concert take place in the Masonic Hall on November 25 . 
 the programme comprise work by Beethoven , Raff , Sterndale 
 Bennett , & c. , all play with excellent effect . the attendance be 
 great than at the open Concert . at these Concerts encore be 
 abolish , and no one allow to enter or leave the room during actual 

 inknown 
 stitution . 
 one can 

 performance , arrangement which meet with the entire approval of 
 the subscribers.——Handel ’s Messiah be give in the Church of 
 the Saviour on the 4th ult . , by the member of the singing - class 
 gmnecte with the Church . the solo be take by Miss Fraser 
 nner , Mrs. Forrest Currie , Messrs. Banks , Roper , and W. Pearson ; 
 the chorus be generally very well execute . there be a small 
 band lead by Mr. Synyer ; Mr. Stratton preside at the organ , and Mr 

 mson conduct . there be a very large attendance . the 
 tond of Messrs. Harrison ’s Popular Subscription Concerts take 
 ice in the Town Hall on the roth ult . , Mr. C. Hallé ’s band be 
 great attraction . the programme be make up from Beethoven , 
 ini , Svendsen , Leo Délibes , & . Madame Néruda play a Con- 
 trto by Vieuxtemps , and piece by Nardini and Paganini ; Mr. 
 lallé give a Nocturne ( op . 37 , no . 2 ) , and Valse ( op . 34 , no . 3 ) , by 
 pin , and , with orchestra , the Gavotte and Musette from Raff ’s 
 tite for orchestra and pianoforte . the vocalist be Mdlle . Minnie 
 uk ( her first appearance in Birmingham ) , Mr. Maas , and Mr. 
 derick King . their selection meet with great success , and there 
 several encore . a large audience be present.——The Cheap 
 erts for the People still meet with great success , the Town Hall 
 g crowd every Saturday evening . on November 30 there 
 an Orchestral Concert give by the Edgbaston Amateur Musical 
 Union , conduct by Mr. C. J. Duchemin . the vocalist be Miss 
 laude Kelly , who sing with taste and finish , and be encore several 
 two instrumental item also meet with that compliment . 
 the 13th ult . Mr. A. J. Sutton ’s Choir occupy the platform , and 
 tforme Dr. Stainer ’s Daughter of Fairus ; and on the 2oth the 
 Handsworth Philharmonic Society give Macfarren ’s May Day and 
 ¢ Christmas Carols 

 VON 

 erect by Messrs. James Conacher and Sons , of Alfred Street , 
 icess Street , Huddersfield , from a specification draw up by Mr 

 ningham ) , who preside at the instrument throughout the service 
 the instrument 1 admit by those who hear it to be very fine , and 
 equal to any in Bradford . the reed be of particularly good quality , 
 and free from harshness . the organ , which be thoroughly well 
 play by Mr. Rhodes , both in the voluntary and the service , give 
 perfect satisfaction to that gentleman and all connect with the 
 church.——Mr . S. Midgley give the first of two Concerts of Chamber 
 Music in the Church Institute on Friday evening , the 5th ult . , when he 
 be assist by Herr Straus and Herr Daubert . the most interesting 
 item in the programme be Beethoven ’s trio in b flat , Op . 97 , which 
 be well render . among the various solo Herr Straus create the 
 great impression . Herr Daubert play a Sarabande and Gavotte 
 by Popper with fine expression and pure tone . Mr. Midgley , in 
 — to his other arduous labour , contribute three pianoforte 
 solos 

 BricG , LincoLnsHirE.—The Fourth Concert of the Brigg Choral 
 society be give in the Corn Exchange on Tuesday , November 25 , 
 when selection from the Messiah and Samson be render by an 
 amateur band and chorus of eighty performer . the room be 
 crowded , and the Concert a great success . Madame Evison , of Hull , 
 Mr. Hayes , of Boston , and Mr. Dodds , of Leeds , be the soloist . 
 Mr. C. W. Cray conduct , and Miss F. Smith accompany 

 DuruaM.—The third Evening Concert of the Philharmonic Society 
 ( for the benefit of the Durham County Hospital ) be hold in the Town 
 Hall on Wednesday , the 3rd ult . , when Haydn ’s Oratorio , the creation , 
 be perform . the principal singer be Miss Tomlinson , Mr. 
 Verney Binns , and Mr. Grice . Miss Tomlinson be loudly applaud 
 throughout the evening , especially for her rendering of ' " ' with verdure 
 clothe " and " on mighty pen . " Mr. Binns and Mr. Grice sing with 
 fine effect the recitative and air with which the Oratorio abound . 
 the chorus be extremely well render , and the band , with Mr. 
 J. Wood as leader , play remarkably well . great credit be due to 
 Mr. D. Whitehead , the conductor , for bring the Society to such 
 perfection in so short a time 

 EpinBurGH.—Sir Herbert Oakeley give an Organ Recital on the 
 4th ult . , in the Music Class - room , to a numerous audience , include a 
 large number of student . the opening piece , by Corelli , a composer 
 to whom it be say that Handel be greatly indebted , be play chiefly on 
 the diapason . Handel 's air " ' Verdi prati , ’' which be sing at the last 
 Reid Festival , be next give , and introduce the soft stop on the 
 swell organ ; and in the chorus which follow , both wood and metal 
 ' * 32 ’s " on the pedal organ be use with great effect . the original 
 scoring ( four horn and string ) of Beethoven 's Sestet be closely 
 imitate , and amongst other interesting item Kowalski ’s Menuetto 
 and Trio , write in the old style , be very successfully render . a 
 Gavotte be always a favourite movement in the Music Class - room , and 
 Bach ’s no . 2 be irresistibly redemande , and partly repeat . the 
 combination of stop , embrace orchestral oboe , clarionet , and cor 
 anglais , be especially noticeable in this number.—a second 
 recital by Professor Oakeley take place in the Music Class - room on 
 the 18th‘ult . , when an interesting programme be go through . the 
 vocalist be Miss Wakefield , who give four song in a very effective 
 manner 

 Frome.—A very successful Concert be give at the mechanic ’ 
 Hall on the 16th ult . by Mr. W. H. Cox , who be assist by Miss 
 Laura Cox ( grand piano ) , Mr. W. G. Cox ( violin ) , Mr. Andrew Waite 
 ( violoncello ) , and the following amateur vocalist : Miss Archard 
 ( soprano ) and Mr. E. R. Trotman ( baritone ) . the programme , which 
 consist principally of classical music , be thoroughly enjoy by a 
 select and fashionable audience 

 UDOR , REV . T. O. — ‘ Christmas Bells . Carol . 
 8vo , 4d 

 OVELLO ’S OCTAVO CHORUSES . 
 no . . reft of thy son Dr. Crotcn ’s " Palestine " 2d . 
 o . o happy once 2d . 
 . o feeble boast m 
 . hence all his might 
 . in frantic converse 
 . then the harp awake 
 86 . nor vain their hope 
 . Lo ! star - lead chief 
 . daughter of Sion ... 
 . hecome .. ee 
 pa . be peace on earth ... pa : 
 AWRENCE , E. M. — ' * time long past " and 
 " Sunbeams . " four - part song . 8vo , 4d . 
 cHARDY , R.—Hymn of the season . 
 tina . 8vo , 4d . 
 OVELLO ’S part - song BOOK . 
 no . the Vesper Hymn . BEETHOVEN . 
 7 . what though sorrow . NAUMANN 
 . the Swallows . PoHLENz 
 . hope and faith , WEBER 
 . Hark , the lark . Kiticken 
 pa . awalk at dawn . GADE ss 
 OGERS , DR . R. — ' ' the river flow 

 love . " part - song . 8vo , 3d 

 gore composition from the GREAT 
 ter , ee - ' ge Organ , with Pedal Obbligato , by 
 ARTHUR HENRY BROW 
 no . 
 1 . " Wedding March , " by Mendelssohn . 
 2 . March from " Tannhauser , " by Wegner , 
 3 . = — " Scipio , " also from Occasional Oratorio , both by 
 andel , 
 4 . ' Coronation March , " from Meyerbeer ’s " ' Prophéte . " 
 5 . " the dead March " in " Saul " and " see the conquer hero 
 come , " both by Handel . 
 6 . Andantino , from the 11th Grand Symphony by Haydn , and " Waft 
 her , angel , " from Handel ’s " Jephtha . ' 
 7 . " as pant the hart , " from Spohr ’s " Crucifixion , " and " where’er 
 you walk , " from Handel ’s " Semele . " 
 8 . m & . heart ever faithful " ( Mein glaubiges herz frohlocket ) , by 

 9 . Andantino from the 5th and Andante from the 4th Sonatas , by 
 eyel 
 10 , " the * Hero ’ 's March , " by Mendelssohn . 
 11 , " quis est homo ? " from Rossini ’s " Stabat Mater . " 
 12 . Air et Cheeur , from " ' La Dame Blanche , " by Boieldieu . 
 13 . Grande Marche Héroique in C , by Fr . Schubert . 
 14 . Grande Marche Héroique in D , by Fr . Schubert . 
 15 . overture , ' ' Berenice , " by andel . 
 16 , overture , " Sosarmes , " by Handel . 
 17 . Handel ’s Overture , " Alcina . " 
 18 . Gavotte , from Handel ’s overture , " Otho . " 
 19 . " La Carita , " by Rossini . 
 20 , " angel ever bright and fair , " and " pious orgy , " both by 
 Handel . 
 21 . ' Ave Maria , " by Franz Schubert . 
 22 . Aria , by Antonio Lotti . circa 1700 
 a. ° my tag thirsteth for God , " Aria from Mendelssehn ’s 42nd 
 salm 
 24 . " Gloria in excelsis , " from Weber ’s Mass in g. 
 25 . " fac ut portem , " from Rossini ’s ' Stabat Mater . " 
 26 . " Pieta , Signore , " from the Oratorio " San Giovanni Battista , " by 
 Stradella . 
 27 . Handel ’s Overture to " Julius Cesar . " 
 28 . Serenade , by Franz Schubert . 
 29 . Aria , by Cavaliere Gluck . 1765 . 
 30 . Aria , from " Alcina , " by Handel . 
 31 . Aria , from " Artaserse , " by Leonardo Vinci . 1730 . 
 32 , Cantata , by Alessandro Scarlatti . 
 33 . Aria , by Ch . Gluck . 1769 . 
 34 . Aria , by Domenico Cimarosa . 1784 . 
 ss , © Diedi il Coro , " Aria by Handel . 
 36 . Siciliana Long . 
 37 . Andante . 
 38 . Aria , by Padre Martini . 1763 
 39 . " Kyrie Eleison , " from Schubert ’s Mass in g. 
 40 . Aria , by Gluck . 1767 . 
 41 . ' Sanctus " and " Hosanna , " from André ’s mass. op . 43 . 
 42 . last chorus , from Beethoven 's " Mount of Olives . " 
 43 . ' he shall feed his flock , " from Handel ’s " ' Messiah . " 
 44 . pe we tu solus , " by Vincenzo Righini . 1788 , 
 s allelujah " chorus , from Handel ’s " Messiah . " . 
 46 . ' turn thy face , " " then shall I teach , " " I will magnify thee , " 
 from anthem by J. Weldon . 
 47 . " the heaven be tell , " from Haydn ’s " creation . " 
 48 . Andante and Allegretto , from Handel ’s Violin Sonata in A major . 
 49 . slow movement from symphony 36 , by Haydn . 
 [ continue . ] 
 price two shilling each . 
 volume i. and ii . , bind in cloth , be now ready , each contain 
 20 number 

 London : B. WiL.1Ams , 60 , Paternoster Row 

 Folio , cloth , gilt edge , 12s . Octavo , paper cover , 4s . ; cloth , gilt edge , 6s . ei 
 i cheap edition . Horton 
 h Folio , paper cover , 5s . ; cloth , gilt edge , 8s . Octavo , paper cover , 2s . 6d . ; cloth , gilt edge , 4s . 6d . eeeetion 
 see rare ship will 
 ' the only Complete Edition , contain book 7 and 8 . RI 
 i ; " ' the volume before we be , indeed , a model of cheapness combine with elegance and convenient arrangement . it cos 
 Bi contain , in 518 neatly print page , everything Mendelssohn have write for the pianoforte , from the Capriccio in F Jello , hax 
 8 sharp minor , Op . 5 , compose in 1825 , at the age of 16 , to his late work , include several publish after his death . ice 

 a student will find no end of interesting point in the work here collect , but to the more advanced amateur J splicatic 
 it also they will be a source of purest enjoyment . we need not add that the stately volume before we be eminently Jan now 
 ie adapt to serve as an elegant and valuable gift - book at this , or , indeed , at any season of the year . ”—The Times . Street , L 
 re ' * this be a new edition , just issue by the eminent firm in Berners Street , of the complete work of Mendelssohn for NI 
 le pianoforte solo , include the two concerto , and the other piece with orchestral accompaniment . these be com- re 
 prise in one handsome volume , full music size , far less bulky than might be expect from the comprehensiveness of 
 its content . these comprise all the hitherto publish pianoforte work of the composer of the class just specify , in 
 ia include the eight book of ' ' Lieder ohne Worte . " some of these and several other piece be the copyright of } ofMessrs 
 ie Messrs. Novello , Ewer and Co. , hence this be the only complete edition procurable in this country . the advantage of van 24 , 
 ie have all these production of the great master in a single volume be great , especially for the purpose of ready din be tal 
 reference , as in the case of the beautiful one - volume edition of Beethoven ’s sonata issue by the same publisher 

 like it , the Mendelssohn collection now under notice be beautifully engrave and print , and be altogether bring LON 
 out in a style worthy of the content and of the high reputation of the firm by which it be issue . ”—TJJlustrated |[he ™ 
 London News . Sir a 

