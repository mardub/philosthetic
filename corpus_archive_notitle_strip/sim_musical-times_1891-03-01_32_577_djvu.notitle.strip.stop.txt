SatnT PAvL , 
 given world evening Whitsunday , 
 22 , 1536 , composer twenty- 
 sixth year . place Diisseldorf ; occasion 
 Lower Rhine Musical Festival . 
 eagerly - expectant audience memorable occa- 
 sion Carl Klingemann , lerdinand Hiller , J. W. 
 Davison ( journalist ) , Sterndale 
 Bennett , turned . 
 named recorded experiences event- 
 ful day , aid narratives 
 contemporary records , ample 
 material wherewith relate story 
 performance Mendelssohn “ St. Paul 

 , word [ Festival . 
 addition ‘ St. Paul , ’ performed 
 Beethoven Choral Symphony , Overtures 
 “ Leonore , ” . rt ( unknown ) . 3 ; 
 Mozart “ Davidde penitente ’’ ; 
 Handel ( Chandos anthems ) . 
 formance took place Kittersaal , 
 room , ” says Hiller , * * small forthe large audi- 
 ence orchestra ; ‘ Sleepers , wake , ’ 
 blast trumpets trombones gallery 
 low hall overpowering 

 per- 
 “ 

 MUSICAL TIMES.—Marcu 1 , 1891 

 day Festival , 
 usual , called Kiinstler - Concert — chiefly consist- 
 ing solo performances principals . 
 programme Concert altered 
 moment owing illness 
 solo vocalists . Mendelssohn proposed 
 Ferdinand David play Beethoven 
 “ Kreutzer ” Sonata . Unfortunately music 
 found , Mendelssohn said : “ played 
 , course play heart , ” 
 music rehearsal , 
 enormous success 

 interesting prices admission 
 Concerts . ticket evening 
 Concerts cost shillings , single evening 
 shillings , morning Concert shillings 

 distinct melody perceptible , douk 
 express certain point lausl vague 
 aspirations happiness disappointment 
 experiences wearied toils science ; 
 metaphysical stage , 
 properly belongs , repeat , sphere 
 symphony ; , foilowing example ot public , 
 believes fully M. Gounod great talents , | 
 postpone decision till second act 

 confidence abused . course ofthe 
 second act ( superb ! ) M. Gounod pays 
 arrears owing public . 
 act opens choruses equal beauty , linked 
 contrasting rhythm , colour , 
 subject . chorus soldiers , comes 
 chorus old men , lastly chorus girls 
 women . ‘ soldiers sing war love , 
 old men praise good Rhine wine , 
 women pick quarrels girls . drinking , 
 dancing , love - making merrily , till 
 length merry - makers throw 
 fair combatants , rushed 
 tooth nail . composer 
 | tendered scene brilliancy colour 
 jdoes fail moment . old men 
 |chorus encored , good reason ; 
 choruses precede follow fully equal 
 |toit powerand originality . feel uncertain , 
 | speaking individual numbers score , 
 mention proper order ; memory 
 pardoned slips question 
 - act opera . mention instance , | 
 entirely forget point act waltz , 
 sung danced , ovght come , 
 , old men 
 chorus , took fancy audience 
 night . waltz , ! 
 seriously praising man wrote 
 Finale act ‘ Sapho ’ having com- 
 posed waltz ? Yes , certainly . Beethoven 
 wrote waltzes , Weber ; casy , 
 rate , compose household 
 word ink score dry . | 
 hardly dare predict glowing future 
 waltz ‘ Faust , ’ certainly melody 
 popular , — - important 
 small works great — style 

 Margaret issues house way tt 
 church . Faust approaches , pays , 
 different words , compliment Don 7 wan paid 
 |to Zerlina . composer set words , 
 lovers exchange , mezza - voce phrase , 
 sustained flowing accompaniment orchestra . 
 possibly fresher charming 
 |than melody bars long ; steals 
 ; mirth bustle village /fcte like fragment 
 celestial music soaring upward native spheres . 
 forgotten scene act , 
 applauded , , , effect 
 stirring situation music . 
 Mephistopheles plying soldiers liquor , 
 length provokes violence ribald 
 taunts . throw demon , 
 Valentine head , sword hand , 
 blade breaks short struck 
 invisible cuirass . ‘ Satan ! ’ cry ; ant 
 presenting handles swords , forma 
 cross , compel spirit darkness grovel 
 feet 

 Mendelssohn Wagner : 
 tante 

 Beethoven Weber : ‘ attain 
 art pleasing 

 Weber Beethoven : ‘ ready 
 lunatic asylum 

 Handel Gluck : ‘ * knows 
 counterpoint cook 

 tion , St. James Theatre , passed 
 hands Mr. Alexander , selection music 
 nightly played small , thoroughly efficient , 
 orchestra , direction Mr. Walter Slaugh- 
 ter , , instead , usual , accompani- 
 ment buzz conversation , listened 
 pleasure warmly applauded . evenings ago 
 programme included Mozart Overture * * Zau- 
 berflote , ” ” , Moszkowski * * Spanish Dances , ” 
 Minuet Finale Mozart Symphony E 
 flat 

 Mr. Artuur F. Froccatr sent follow- 
 ing Sonnet Beethoven , suggested Sonnets 
 Mr. Bennett pen in‘our issue 

 fitting , fair stream , 
 echoes , waters roll 

 uttered fills heart 

 Tue Daily News gives programme 
 Hereford l’estival present arranged : — September 
 8 , Mendelssohn “ St. Paul . ” evening secular 
 Concert , programme chosen . Wednesday 
 morning , Septemberg , Mozart Requiem , Beethoven 
 “ Eroica ” ” Symphony , new Motet “ Praise 
 Holiest . ” Dr. H. J. Edwards , Barnstable , 
 Vorspiel Wagner ‘ “ Parsifal , ” Sir Arthur 
 Sullivan “ Festival ” Te Deum . Wednesday 
 evening , Stainer St. Mary Magdalen ’ ? Mendels- 
 sohn ‘ * Lobgesang . ” Thursday morning , * De 
 profundis , ” Dr. Hubert Parry , soprano soloist , 
 triple chorus , orchestra ; Bach “ Blessing , 
 Glory , ’ Spohr ‘ Calvary . ” evening 
 “ Elijah . ” ‘ Messiah ” Friday morning , 
 Festival conclude evening 
 Chamber Concert Shire Hall 

 Melbourne Argus , writing Dr. Mackenzie 
 “ Cotter Saturday Night , ” observes : ‘ Dr. A. C. 
 Mackenzie setting Burns homely story , ‘ 
 Cotter Saturday Night , ’ originally intended 
 Birmingham Festival 1858 , pro- 
 duced Edinburgh December 16 , year . 
 written chorus orchestra , 
 owing skilful way chorus 1s 

 picturesque | 
 

 remarkably vigorous performance given 
 * * Tannhauser ’ Overture . suffering illness 
 Madame Néruda played Mendelssohn Violin Concerto 
 customary refinement charm . Beethoven 
 Pastoral Symphony , went exceedingly 
 matter course , delicious Romanza Mozart 
 “ Fine kleine Nachtmusik , ” repeated desire , completed 
 scheme , end performance , 
 Royal party , Sir Charles 
 Hallé recipient immense applause 

 CRYSTAL PALACE 

 Meistersinger , ” beautiful Introduction ‘ Parsifal 

 spirited ‘ * Walkiirenritt . ” ’ vocal depart- 
 ment commend Mr. Henschel 
 firm delivery familiar monologue Hans Sachs 
 Mr. Mrs. Henschel quietly dramatic reading 
 immediately succeeding duet opera 
 Cobbler Poet naive Eva . last- 
 named piece great success . exception 
 teferred ‘ ‘ Eroica ” ? Symphony Beethoven , 
 conducting Mr. Henschel exhibited artistic 
 sympathy work hand compositions 
 Bayreuth master . Stimulated circumstances 
 manifest wonted intelligence un- 
 questioning obedience , members orchestra 

 painstaking care Conductor need told 

 Mr. Stavenhagen solo pianist occasion , 
 delicate , refined , appreciative reading 
 Beethoven Concerto B flat ( . 2 ) added 
 number admirers heard 
 . performance Liszt Hungarian 
 Rhapsody distinguished necessary fire 
 vigour needful complete interpretation . con 

 cluding orchestral work sense novelty 

 MUSICAL TIMES.-—Marcu 1 , 1891 

 152 
 earnest spirit produced remarkable | black fog , sang Sullivan ‘ ‘ Thou’rt passing ” 
 operatic productions ‘ Ivanhoe , ” performed fine | Schubert ‘ Der Schiffer , ” Mr. Schénberger 
 power expression ; Beethoven Symphony F ( . 8) , | completed programme pianoforte trifles , 
 known patrons Concerts , Wagner | Nocturne Chopin played G 
 Overture “ Tannhauser ’’ presented | major , familiar piece G minor quoted fre 
 sympathetic artistic style , usual band | programme . 
 able direction Mr. Manns . great crowd assembled Monday , gth ult . , ml 
 Miss Rosina Isidor , appearance , | greet Dr. Joachim , evidently retains hold set 
 displayed voice fine resonant quality songs | public notwithstanding lapse time appear . ha 
 Donizetti Maggi . ance younger violinists phenomenal ability . ov 
 — _ — , services rendered musical art 
 . 2 , Lite 8 saAwappme |the loftiest character generation Jose ow 
 MONDAY SATURDAY POPULAR CONCERTS , Joachim inestimable , ci = Ar 
 order record complete , | unsurpassed alike soloist leader . - 
 Concert January 26 , interesting novelty | slight faultiness intonation abl 
 commenced programme . Brahms Sonata |his playing romance Hungarian 
 C ( Op . 1 ) , , far aware , | Concerto Brahms Hungarian Dance F , 
 performed London — , Mr. ; gave encore , respect execu- § 0 ' 
 Schénberger Pianoforte Recital Princes ’ | tion grand , remark apply 
 Hall . Schumann enthusiastic opinions Brahms , |to leading Beethoven Septet , Brahms Trio cer 
 written 1853 , led publication |in E flat , pianoforte , violin , horn ( Op . 40 ) . un ! 
 Sonatas ( Op . 2 5 ) . Certainly present work | - named work , added reper . Alt 
 remarkable effort youth , strong individuality | tory Popular Concerts , likely heard frequently , 
 compare early compositions Mendelssohn . | composer thoughtful beav- 
 note movement masculine force | tiful additions chamber music highest class . mn 
 striking subjectiveness conspicuous Brahms | pianist evening , Miss Fanny Davies , modestly 
 riper works , sign immaturity | contented trifling pieces Schumann fro 
 movements , reason believe | Madame Schumann . Madame Bertha Moore displayed 
 Andante variations , based old German |a pretty voice refined method songs Schumann Mr 
 ditty , penned Brahms fourteen years | Mr. Henschel . 
 old , period played piece Concert ! Mr. Max Pauer appeared time season Tri 
 Hamburg . Mr. Schonberger executant | Saturday , ryth ult . , played Chopin Allegro de abil 
 present occasion , masterly performance re- | Concert ( Op . 46 ) executive ability , Alb 
 ceived applause . little pieces Beethoven | succeed making characteristic “ M 
 ended programme , central portion | work wholly interesting . truly magnificent reading es 
 occupied Schubert Octet , performed | Bach Chaconne given Dr. Joachim , con- Mr 
 break . Mr. Braxton Smith contributed airs | certed works Mozart Quintet C Rheinberger 
 Handel Sterndale Bennett . | Pianoforte Quartet E flat ( Op . 38 ) . Mr. Orlando Harley 
 Schumann justly popular Quintet E flat ( Op . 44 ) | remarkably received Mr. Oliver King effective 
 doubt mainly instrumental drawing immense | song ‘ ‘ Northern seas . ” ’ 
 audience following Saturday , work | Scant notice required remaining Concerts T 
 magnificently rendered Miss Ilona Eibenschiitz | took place month . 16th ult . Beethoven n00 
 pianoforte . young lady heard | Quartet E minor ( Op . 59 , . 2 ) , Mendelssohn Trio ony 
 advantage Mendelssohn familiar Prelude Fugue | C minor ( Op . 66 ) , movements Spohr Duet 
 E minor ( Op . 35 , . 1 ) , selected solo , | D , violins ( Op . 67 , . 2 ) , Mr. Straus joined wit ! 
 Madame Néruda simply faultless Gondoliera | Mr. Joachim , concerted pieces , Mr. Max Pauer aa 
 Moto perpetuo Franz Ries Violin Suite G | introduced Toccata G major minor , Rheinberger . Sch 
 ( . 3 ) , extremely pleasing movement . | regard piece , remark programme ( Op 
 Beethoven String Trio G ( Op . 9 , . 1 ) completed | annotator principally designed illustrate fl 
 instrumental programme . Mr. William | composer fine technical musicianship apt . ‘ Star 
 Nicholl songs familiar need mention . | plenty scholasticism , little inspiration , Toccata , intr 
 memory Gade honoured Monday , /and audience glad pianist , response sup y 
 2nd ult . , performance Octet strings , F , | inevitable encore , gave Beethoven familiar , welcome mod 
 heard Concerts thirteen | Andante F , played Mozartean grace : 
 years . early effort Danish composer , | delicacy . Mr. Hirwen Jones sang air Gounod perf 
 described pleasing , certainly | ‘ * Le Médecin malgré lui ’ ? Schubert ‘ Good Night ” upor 
 great . second movement unmistakably written |in agreeable manner . clea 
 influence Mendelssohn , | Brahms comparatively early comparatively simple muc 
 characteristic Scandinavian touches . Miss Ilona Eiben . | Sextet B flat ( Op . 18 ) , Schubert Fantasia C , Imp 
 schiitz improved position interpretation | pianoforte violin ( Op . 159 ) , Beethoven Sonata wen 
 Beethoven Sonata C minor ( Op . 111 ) , |C ( Op . 2 , . 3 ) constituted scheme Saturday , Scht 
 played previous Saturday Concert . _ | 21st ult . Musicians need critical remarks concerning 
 present occasion gave remarkably fine , | works , need said Sextet went es 
 ay 

 inspired , rendering work , share | wonderful spirit , Miss Zimmermann played 
 Schumann Pianoforte Quartet E flat ( Op . 47 ) | Sonata quiet , chaste expression . Mr. Braxton 
 perfectly . Mr. Orlando Harley unable | Smith light tenor voice displayed advantage 

 description , evidently graduated 

 good school . Beethoven * Kreutzer ’ ? Sonata , 
 course joined Mr. Albeniz , carefully 
 certainly powerfully interpreted . Mr. Arbos showed 
 unfailing technical ability Bach Chaconne , Mr 

 Albeniz displayed light , delicate style playing 

 sang place Madame Valda , portion duet 
 * Flying Dutchman 

 second Concert , afternoon 12th ult . , 
 Mr. W. H. Squire added list instrumental 
 executants , good performance given Schubert 
 Trio B flat ( Op . 99 ) . Mr. Squire displayed remarkable 
 ability violoncellist Gavotte Bach , Mr. 
 Albeniz best Beethoven - called 
 “ Moonlight ” ? Sonata . change 
 vocalists , Mr. Courtice Pounds unable sing ; 
 Mr. Hirwen Jones highly acceptable substitute 

 HERR STAVENHAGEN RECITAL 

 Tue materials able pianist Recital after- 
 noon roth ult . , St. James Hall , drawn 
 sources , , purposely accident , 
 semblance chronological order . Beginning 
 Haydn Variations F minor , pr 
 included Beethoven C sharp minor Sonata ( Op . 27 , . 2 ) , 
 Schubert Impromptu flat , Schumann * Papillons ” 
 ( Op . 2 ) , Chopin Nocturne F major Polonaise 

 flat , brought termination pieces 

 ideas formulated earlier generation 

 performances generally amply repay attention bestowed 
 . version Beethoven Sonata 
 clear precise , exhibiting , course essential , 
 poetic feeling delicacy . richly melodious 
 Impromptu Schubert lacking refine- 
 ment . Herr Stavenhagen appeared ease 
 Schumann brisk piece , recalling boisterous mirth 
 Carnival . Chopin pieces played 
 spirit required fail gain Herr 
 Stavenhagen flattering tokens appreciation 

 WIND INSTRUMENT CHAMBER MUSIC 
 SOCIETY 

 dy 
 l 

 complained played entire work . 
 comparisons odious Stock 
 Exchange best amateur orchestral socicties , 
 , certainly best , thanks 
 |admirable training receives Conductor , Mr. 
 George Kitchin . programme Concert 
 | Wednesday , 18th ult . , model class . 
 | Sterndale Bennett Symphony G minor dropped 
 performance late years — high } 
 seasoned present tastes . rendered 
 refinement , better performance 
 Cherubini “ Anacreon ” ’ Overture Francis Thomé 
 pretty humorous Suite ‘ ‘ Les Noces d’Arlequin . ” 
 Gade Violin Concerto D minor ( Op . 56 ) played 
 vigour Miss Emily Shinner . 
 Danish composer best instrumental works , 
 considerable national colouring . saying 
 probably performed London , 
 programme annotator include 
 Crystal Palace , played Mr. John Dunn , 
 November 13 , 1886 . - music rendered 
 male voice choir songs contributed 
 Mrs. Helen Trust . 
 | following Saturday Strolling Players gave 
 second Concert present season . event 
 clashed second Oratorio performance Covent 
 Garden refer briefly programme , 
 included Beethoven Symphony ( . 2 ) , Svendser 
 Rhapsody Norvégienne ( . 2 ) , Cherubini Overture 
 ‘ ‘ Water Carrier , ” Moszkowski Spanish 
 Dances , sufficiently ambitious scheme . vocalists 
 Mr. Reginald Groome Madame Rolla , 
 taking place Madame Pauline Featherby 
 moment notice 

 y 

 ROYAL INSTITUTION 

 Proressor C. Huppert H. 
 Institution , 12th ult , series 
 Lectures entitled ‘ position Lulli , 
 Purcell , Scarlatti history opera . ” 
 | Lecture , numerously attended , 
 | Professor said composers broadly divided 
 classes — practical - minded , wrote 
 finger pulse public , character style 
 | music consequently greatly owing tastes 
 | period music produced ; 
 | Idealists , defied prevalent fashions modes 
 | expression time , struggled come 
 ja self criticising standard . belonged 
 men Handel , Meyerbeer , Mendelssohn ; 
 Bach , Beethoven , Schumann . Lulli interesting 
 | example practical school , circumstances 
 jinfluenced art productions worldly wise 
 temperaments . national characteristics French 
 | opera ballet spectacular display , Lulli 
 choice mythological subjects operas . form 
 | operas founded ‘ “ Mascarade , ” 
 jin Louis XIV , fond taking 
 ; Lulli commenced career Court 
 |composer . Lulli accredited having invented 
 form overture , predecessor 
 } court favour , Cambert , cast precisely 
 | mould , developed . remarkable 
 dignity seriousness Lulli overtures 
 pointed frivolity court fashion 
 outcome shallow natures . vocal 
 writing greatly superior instrumental , 
 remarkable declamatory expressive power 
 | vocal parts dramatic instinct led adopta 
 ) form accompanied recitative frequently 
 set tunes . chiefly reserved ballet music , 
 | operas abounded . good 
 tunes heavy disappointing , 
 admirable , possessed Italian characteristics 
 3y preservation ballet , clear connection 
 | maintained old mascarades dialogue , 
 penultimate step complete French opera 
 /form . Lulli operas showed great constructive skill 
 appreciation climax , Lulli French 
 ‘ opera design object , remained 
 | present day . 
 | second Lecture , delivered roth ult . , 
 | devoted surroundings influence Purcell , 
 | Professor Parry combated opinion held 
 |musicians Puritanical influence _ _ 
 detrimental progress music country , 
 destroyed old English polyphonic school . 
 school , Professor said , attained highest pitch 
 perfection Elizabeth 
 | beginning James I. reigns , greatest 
 representative , Orlando Gibbons , died year 
 | Charles I. ascended throne — viz . , 1625 , - 
 | years Commonwealth . ideas Italian 
 | reformers penetrated England long time 
 Puritans , taste grand old polyphonic 
 style decline beginning seventeenth 
 century . ‘ Puritans , fact , admirably prepared 
 way new school , gave impulse develop- 
 ment retarding progress . Charles II , 
 French tastes preference music ot 
 light expressive character , destroy 
 grand polyphonic style Puritanical 
 repressions . Charles II . quickly surrounded 
 musicians perform write new 
 style , taken - - classes 
 enthusiasm new departure art commonly 
 excited . influences work Henry 

 PARRY commenced 

 155 

 Lecture 4th ult . specially designed 
 students , large number present , subject 
 chosen ‘ Sonata - form . ’’ Form , Professor said , 
 plan design , thought important branch 
 musical art greatly neglected . recently 
 real text book subject . true 
 Macfarren written Sonata , Miss Prescott 
 contributed admirable articles 
 published book form . Bannister 
 chapter Form Harmony Book , Dr. Harding 
 Analysis Beethoven Sonatas , Sir John Stainer 
 Primer , exception 

 treatises , treatises little use 
 student . explanations sonata - form greatly 

 M. Puituipre Rurer , years esteemed 
 Professor Pianoforte Harmony Conservatoire 
 Liege . died town January 30 , aged eighty - 

 Paris died recently aged pianoforte tuner , 
 Bonarpin , proud boast 
 employed tuner number years 
 great Beethoven 

 record death month , Stuttgart , 
 JosErH ABENHEIM , musical director Royal Orchestra 
 residential town , composer number 
 entr’actes , overtures , popular pianoforte pieces , songs . 
 born Worms , 1804 

 Miss Florence Eyre , resident Clifton , gave 
 Annual Concert Classical Chamber Music 2nd 
 ult . lady , studied pianoforte Dr. Carl 
 Reinecke Leipzig Conservatoire , assisted 
 Professor Brodsky , violinist high repute 
 Continent . Miss Agnes Jansen vocalist , Mr. 
 J. H. Fulford accompanist 

 Miss Lock popular Chamber Concert , 16th ult . , 
 attended larger assemblage usual . 
 chief work brought forward Schubert Quintet 
 A(Op . 114 ) , pianoforte , violin , viola , violoncello , 
 double - bass . ‘ executants — Miss Lock , Messrs. Theo . 
 Carrington , Gardener , E , Pavey , Bourke — gave 
 praiseworthy interpretation work . Beethoven 
 Sonata G ( Op . 96 ) , pianoforte violin , 
 couple movements Sterndale Bennett Trio 
 ( Op . 26 ) principal compositions 
 scheme . Mr. Montague Worlock vocalist 

 Mrs. Richardson ( pianoforte ) , Mr. F. Ward 

 MUSIC DUBLIN . 
 ( CORRESPONDENT 

 Saturday , 14th ult . , Dr. Collisson series 
 Popular Concerts resumed . large assembly 
 gathered Leinster Hall evening greet 
 Madame Albani talented Concert party . 
 interesting numbers programme instru- 
 mental Trio Gade ( Op . 29 ) , played 
 skill finish Miss Kate Chaplin ( violin ) , Mr. Ruders- 
 dorf ( violoncello ) , Dr. Collisson ( pianoforte ) . 
 performers gave Presto Beethoven Trio ir 

 G ( Op . 1 ) end Concert , contributed 

 Trio B flat , pianoforte , violin , violoncello 

 2 . Beethoven Trio D , Op . 8 ( Serenade ) , violin , viola , 
 violoncello ; 3 . Raft Quintet minor ( Op . 107 ) , 
 pianoforte strings . executants Messrs. 
 Papini , Bell , Rawlingson , Rudersdorf , Esposito 

 Mr. Alex . Billet commenced series Classical 
 Pianoforte Recitals 5th ult . Lecture Hall 

 sixth Messrs. Paterson Orchestral 
 Concerts large audience listened sympathetic inter- 
 pretation Cliffe interesting Tone - picture ‘ * Clouds 
 Sunshine , ” ’ indifferent performance Mozcart E flat 
 Symphony , ‘ “ Oberon ” ‘ William Tell 

 Beethoven 

 Overtures , characteristics 
 | orchestra fairly brought . pianist Migs 
 | Pauline Hofmann , youth careful technique 
 |won great applause scholastic reading Men . 
 | delssohn G minor Concerto 

 series ordinarily important 
 | season programmes presented keen interest 
 |taken performances . deep debt 
 enterprise secured artists Madame Nordica , 
 | Miss Macintyre , Mr. Mrs. Stavenhagen , Mr , 
 | YsaYe , face certain pecuniary loss engaged 
 ' Choral Union co - operate orchestra jp 
 la splendid performance ‘ * Golden Legend . ” Mr , 
 Paterson annual statement eagerly awaited fre . 
 | quently applauded . announced general success 
 scheme , firm intention carry similar set 
 Concerts season . ‘ smallness Music Hall 
 greatly hampers Mr. Paterson desire improve 
 performances lower subscriptions . pro . 
 grammes , edited large extent written Mr. ; 
 Dibdin , great demand . 
 | great treat hear Sir Charles Hallé 
 |magnificent band Reid Concert ( 13th ult . ) 
 { supplementary Concert following day . 
 ! difficult - estimate value services 
 | orchestra rendered musical taste edu . 
 cation Edinburgh - years ; Sir 
 Herbert Oakeley , appearance 
 capacity Reid Professor , remembered 
 . Symphony ‘ * * Reid ’ Beethoven No.2 , 
 /and fascinating performance silenced 
 wished hear later composition . 
 scholarly ‘ ‘ Anacreon ” romantic ‘ Freischitz ” 
 Overtures showed - sided qualities band 
 perfection . Reid Professor represented 
 ‘ numbers ( Pastorale , Sarabande , Gavotte ) 

 Orchestral Suite , neatly written , beautifully played . 
 |warmly applauded . Lady Hallé unfortunately pre- 
 vented illness fulfilling engagement , 
 place creditably filled Mr. Willy 
 Hess , days ’ notice undert 0k Lady Halle 
 solos — notable feat performed . gained 
 ovation . Madame Nordica vocalist , Donna 
 Elvira great aria ‘ “ Mi tradi ’ ? “ Dich theure Halle ” 
 { ‘ * Tannhauser ” ’ ) showed absolute command 
 , vocal technique , audience . Saturday 
 afternoon ‘ “ monstre ’ ? programme _ presented . 
 | Schubert long Symphony C , overtures , 
 |concertos , smaller pieces ! 
 | Symphony perfection , Overtures 
 | “ Flying Dutchman ” ‘ “ Coriolan ” orchestra 
 jexcelled . Sir Charles Hallé psid usual homage 

 Beethoven sympathetic reading romantic 
 Concerto G. Mr. Hess won warm encore 

 Lady Hallé particular property — Vieuxtemps 
 Fantaisie Caprice . Madame Nordica sang beautiful aria 
 Gounod * * Reine de Saba ” songs Sit 
 | Herbert Oakeley . enthusiastic applause greeted 
 Sir Charles Hallé closing number “ Semiramide ” 
 | expressed distinct hope—‘t Auf Wiedersehen . ” 
 | Atthethird Edinburgh Classical Chamber Concert 
 chief interest centred Brahms fine Trio E minor . 
 | equally played Mr. Della Torre . 
 Madame Hamilton , Mr. McNeill , Trio Gold- 
 mark , E minor ( given time ) , 
 fortunate interpretation inferior 
 work . numbers Schubert Rondo 
 pianoforte violin , played Madame Hamilton ; 
 Moszkowski Berceuse Davidoff ‘ * Springbrun- 
 /nen , ” Mr. McNeill opportunity 
 |his - improving technique style ; Chopin 
 ‘ * Funeral March ” Mr. Della Torre 

 toth ult . Glasgow Quartet gave fifth 

 Concert series , 12th ult . Madame Trebelli 
 nad hearty greeting appearance touring 
 party . Notwithstanding programme rich material , 
 audience attending Quartet Concert large . 
 Schubert famous posthumous Quartet D minor 
 Beethoven Quartet major ( Op . 18 , . 5 ) 
 heard , , said , better 
 conditions regards balance refinement tone . 
 Quartet Party welcome organisation 
 Glasgow musical life , led Mr. Maurice Sons 

 gave remarkably able performance Beethoven 

 Romance F — perfect ensemble 
 question little time 

 Sir Charles Hallé Orchestral Concert band 
 consisted seventy - performers , Mr. Willy Hess 
 solo violin place Lady Hallé , unfor 

 tunately ill appear . programme , carried | 
 admirable style , contained Beethoven major Sym 

 phony , Spohr Concerto minor ( “ * Dramatic ’ ) , 

 MUSIC LEEDS . 
 ( CORRESPONDENT 

 Tue Leeds Subscription Concert current 
 Series given 4th ult . , Sir Charles 
 Hallé ubiquitous orchestra appeared 
 time season . Beethoven Pastoral Symphony 
 occupied place piéce de resistance , accorded 

 satisfactory treatment hands instrumentalists 

 following Thursday elaborate funeral service | Herr MapaME STAVENHAGEN appeared . 
 known Brahms ‘ German Requiem ’ given > Drawing - room Concert 5th ult . 
 time 1874 . Cantata known Leicester Philharmonic Society gave Recital oi 

 need remark plea fairly |Gounod * * Faust ’ roth ult . Great disappointment 
 judged Concert - room , tone treatment | felt absence Mr. Lloyd Miss Damian , 
 solemn portions appear place . ) unable fulfil engagements , 
 sort fashion extol choral fugue thoroughly efficient substitutes found Mr. Iver 
 persistent tonic pedal ; continued | McKay Miss Agnes Jansen . Madame Fanny Moody 
 noise confusion repulsive , | Mr. Charles Manners parts Marguerite 
 movement utterly keeping words ; , | Mephistopheles , Mr. Andrew Black Valeutine , 
 , musical settings Scriptural | eminently successful . orchestra led 
 passages . Mendelssohn welcomed allevia- | Mr. Betjemann , choruses sung 
 tor gloom , * Walpurgis Nacht ’ ? gained | Society , direction Mr. H. B. Ellis . 
 brightness power natural investment.| Herr Ellenberger second Chamber Concert given 
 soloists Mdlle . Fillunger , Miss Alice Walker , Albert Hall , Nottingham , 1Sth ult . 
 Mr. Charles Chilley , Mr. Andrew Black ; } assisted Miss Cantelo , Miss Lilian Tarbolton , 
 worthy note , ably choral move- | Mr , Richardson , Mr. Edwin Thorpe . playing 
 ments sung , voices betrayed fatigue deserves commendation . Beethoven Quartet ( . 18 , 
 close * Requiem ” weeks . 4 ) , Brahms Sonata pianoforte violoncello 
 enormously difficult Mass D , | ( Op . 38 ) , Schumann - welcome Quintet ( Op . 44 ) 
 admirable spirit sustain choir | pieces played . 
 chord . excellent programme rgth ult . , in-}| Mr. E. H. Lemare Organ Recitals continue attract 
 cluding Beethoven - welcome ‘ Pastoral ’ ? Symphony | increasing audiences Mechanics ’ Institution 
 — simple masterly descriptive music | Saturday afternoons , evidence growing taste 
 written — , doubtless , drawn large audience | good music . 
 apart attraction attending Herr Joachim annual 
 visit , regarded festival . 
 Evidently Symphony * * Rhapsodie Hongroise ” ’ | MUSIC SHEFFIELD . 
 ( . 4 ) Liszt specially prepared , per- | 
 formance nearly possible perfect . wildest 
 parts RI hapsody detail clear finished , | 
 Symphony shading exquisitely delicate . 
 interpretation London following evening | sequently past month contained fixtures 
 equal Manchester rendering , surely | interest . ballad students ’ Concerts 
 effort secure frequent visits | given Saturday Popular Concerts 
 Metropolis s # highly - trained band . Herr Joachim | Albert Hall resumed . 
 Concerto G excited great interest , playing | 3rd ult . Sharrow Literary Society gave 4 
 Bach Chaconne immense applause . Madame Rolla | Chamber Concert , performers Miss Dora Bright , 
 vocalist . Mr. J. Peck , Mr. Alfred Giessing , Mr. J. A. Rodgers . 
 Concert Hall , 3rd ult . , Miss Fanny Davies | Miss Kingdon Rev. Mr. Parkin vocalists . 
 gavea Recital , witha bill fare substantially Mendelssohn Trio C minor pianoforte , violin , 
 offered St. James Hall previous Wednesday . | violoncello admirably rendered , Miss Bright 
 Exceedingly clear decided manipulation | played Mr. Peck Suite violin pianoforte . 
 pieces set ; expression lacked warmth , | work previously heard Sheffield 

 CORRESPONDENT 

 167 

 performance aroused considerable interest , Miss Bright 
 native town . 
 Beethoven , Liszt , Grieg , Mr. Giessing played 
 Goltermann Concerto violoncello minor 

 absence years , Dr. Joachim visited 
 town r2th ult . , playing Mendelssohn Concerto 
 , Miss Fanny Davies , Beethoven Sonata ( Op . 
 30 , . 2 ) Hungarian Dances ( Brahms- Joachim ) . 
 Miss Davies played Chopin Andante Spianato 
 Polonaise pieces . Mr. W. Foxon 
 vocalist Mr. J. W. Phillips accompanied 

 12th ult . St. Cecilia Musical Society gave 
 admirable performance ‘ Elijah , ” direction 
 Mr. Wm . Brown . ‘ chorus singing excellent 
 showed improvement previous performances . 
 quartet soloists exceedingly . Mr. Edward 
 Grime , sang Prophet , creating 
 marked impression . Mr. J. Peck led band Mr. J. 
 W. Phillips Organist 

 19th inst . competition Santley Prize 
 guineas , student ( male female ) 
 adjudged best accompanist , place 
 25th inst . , day Louisa Hopkins Prize , 
 female pianists , gift Mr. Edward Lloyd , 
 memory mother , competed . 
 following day , 26th ult . , Sterndale Bennett Prize 
 awarded female pianist judged 
 best player composition Sterndale Bennett 

 Miss Fanny Davies gave Pianoforte Recital Wed- 
 nesday , January 28 , St. James Hall , greatly 
 pleased numerous audience pure , expressive , 
 thoroughly legitimate playing . important 
 pieces programme Beethoven Sonata E 
 ( Op . 109 ) Schumann Fantasia C ( Op . 17 ) , 
 Miss Davies renders exceptionally . 
 best pieces Bach , Scarlatti , 
 Brahms , Sterndale Bennett . pieces 
 Impromptu B flat , Mr. Arthur Somervell . 
 cleverly written piece , showing clearly influence 
 Brahms 

 Miss WINIFRED PARKER gave evening Concert 
 St. James Hall , 3rd ult . audience 
 large wished , prevent 
 Miss Parker greatest justice songs set 
 , gave excellent style , 
 evident delight audience . successful effort 
 ‘ Inflammatus , ” ’ Rossini * * Stabat Mater , ’’ 
 beautifully sung . Miss Parker advan- 
 tage assistance Madame Belle Cole , Miss Rose 
 Williams , Mr. Phillips Thomas . Mr. Hilton Carter , Mr. 
 Plunkett Greene , Mr. Felix Berber ( violin ) , choir 
 200 voices , direction Mr. William Carter 

 FOREIGN NOTES 

 Tue Beethoven Haus , Bonn , enriched 
 portrait Countess Thérése von Brunswick , 
 Beethoven love , presented com- 
 peser secret betrothal . portrait , three- 
 life - size , work Lampi , Viennese painter 
 considerable reputation beginning present 
 century , bears inscription , handwriting 
 lady : ‘ ‘ Dem seltenen Genie ; Dem grossen Kiinstler ; 
 Dem guten Menschen ; von T. B. ” interesting relic 
 years possession Capellmeister 
 Helmesberger , Vienna , presented 
 Bonn Institution 

 MADAME INGEBORG VON BRONSART opera * Hiarne ™ 
 ( referred month Notes ) 
 brought Berlin Opera 14th ult . , per- 
 formance deriving special interest fact 
 composer work lady social eminence . 
 result , Berlin correspondent informs , 
 | satisfactory , apart probability 
 succes d’estime assured . opera , 
 abounding reminiscences , work capable 
 musician , contains interesting moments , 
 | particularly tender situations drama . 
 libretto , experienced pen Herr Bodenstedt , 
 |deserves praise purity diction general 
 |adaptability musical treatment . Emperor 
 | present occasion , new opera likely 
 | remain répertoire time 

 new revised edition Ambros justly esteemed 
 ‘ * * Musik Geschichte ” progress , publisher 
 Mr. C. I. Leuckart , Leipzig 

 eleventh Silesian Music Festival held 
 summer , conducted time Dr. Willner , 
 following works included programme — viz . , 
 Beethoven “ Eroica ’ ? Symphony , Haydn “ Seasons , ” 
 Cantata Bach , portions Schumann “ Faust ” ’ 
 Wagner ‘ Parsifal , ” Concert - Overture 
 late Conductor Festivals , Ludwig Deppe 

 commemorative tablet placed , instance 
 municipal authorities , house . 8bis , 
 Thurmstrasse , Osnabriick , number years 
 Aibert Lortzing , composer “ Czar und Zimmermann 

 

 Newsury.—On Shrove Tuesday Amateur Orchestral Union 
 gave annu al Concert . orchestra , consisting abou t players 
 ( - thirds members Society ) , conducted 
 . Dines Eatwell Mr. J. S. Li idle . Beethoven § 
 D , Wagner Meistersinger Overture , Wallace Maritana ‘ 

 pieces played . Miss Bessie Latham Mr. 
 e vocalists ; Mr. W.C. Hann , principal solo 
 Mr. James Brown , leader solo violin 

