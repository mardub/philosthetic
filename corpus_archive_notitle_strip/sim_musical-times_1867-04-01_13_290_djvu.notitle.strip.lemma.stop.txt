PHILHARMONIC SOCIETY 

 Tue - fifth season Society commence 11th ult . , 
 excellent concert , interest enhance 
 appearance new conductor , place Professor 
 Bennett , resign , reader aware , conclusion 
 season . enter question 
 Mr. Cusins gain election vacant conductorship , be- 
 lieve , , critic duty simply judge 
 fitness office , bind rarely hear 
 band great accuracy , tempi 
 trying symphony , Mendelssohn . 1 , C minor , 
 Beethoven . 7 , , clearly indicate , 
 rigidly adhere . certain coarseness , perceptible 
 listener , , , course , escape conductor ear , 
 remedying radical defect new 
 director real power ; duty 
 artist accept responsible post , endure 
 find difficult cure . symphony men- 
 tione , Professor Bennett charming Overture * Naiads , " 
 Cherubini ' ' Les Abencérrages , " perform ; Herr 
 Joachim play Spohr ninth violin concerto , refinement 
 intellectual appreciation phrase remarkable 
 composition , hearer forgetful extreme 
 difficulty . vocalist Miss Louisa Pyne Mr. W. H. 
 Cummings ; music choose , , , 
 choose , mean effective . Hummel 
 romance , * * l’'ombrosa notte vien , " delicately sing Miss 
 Pyne , " Mr. Cummings effect 
 Mozart " * Costanza , " // Serraglio ; duet Gounod 
 Reine de Saba , afford weak example composer 
 power , ardent admirer , bind supply 
 reason failure , admit work per- 
 form M. Gounod fame 

 musical SOCIETY 

 GENOA 

 Tur promised repetition Mozart 12¢h Mass Maestro Lavag- 
 nino , place Oratorio dei Filippini , spot peculiarly 
 titte , harmonious proportion beauty ornamentation , 
 purpose . Signora Paulina Vaneri sing principal soprano 
 , treble alto chorus supply lady 
 member Signora Beati Philharmonic class . series 
 Classical Concerts Professor Lavagnino Villa Novello , 
 follow second series ; unequivo 
 cal proof success attend spirited attempt 
 introduce taste good music city hitherto 
 reproach care reverse sterling 
 composition , rise similar experiment 
 laudable direction , Professor Bossola announce 
 intention series concert production Sym 
 phony overture Beethoven , Mendelssohn , & c. , 
 Paganini Theatre , Genoa 

 mention italian paper , Arezzo , birth- 
 place Guido Monaco ( know world Guido Arctinus , 
 Guido d'Arezzo , inventor system musical notation ; 
 particular , page 155 , * * Hawkins ’ History 
 Music , " Novello Edition , 1855 ) , contemplation construct 
 spacious street piazza , bear illus- 
 trious inventor . street lead Railway Station 
 piazza , contain monumental sculpture , honour 
 renowned Guido . ' defrayment expense 
 construction , contribution receive 
 civilised world 

 Scuusert Socrety.—The Concert 
 season Beethoven Rooms lth ult . , 
 crowded audience . work Spohr occupy 
 ; second miscellaneous . principal 
 work perform Spobr Trio , Op . 124 , artistically ren- 
 dere Madile . Rosetta Alexandre ( pianist king Prussia ) , 
 Monsieur Vivien ( violin ) , Herr Schubert ( violoncello ) ; 
 pianoforte piece play Madlle . Kinkel ( Schubert . 
 encore ) Benedict Pianoforte Fantasia , ' * 
 Prince Wales . " ' vocalist Madame Sauerbrey , Miss 
 Marie Stocken , Miss Adelaide Bliss , Miss Barry Eldon , Miss 
 Mina Poole , - lady extremely successful 
 Lachner ' oh happy little bird , " violoncello obdligato 
 Herr Schubert . concert proof manner 
 Herr Schubert intend proceed enterprise , 
 fairly entitle good wish desire promotion 
 healthy taste music country 

 Tie Musret Orcan.—On Saturday 16th ult . , 
 instrument exhibit M. Lemmens , 
 perform selection music admirably adapt 
 display extraordinary capability . harmonium 
 good specimen french manufacture ; newly invent 
 mechanical contrivance , * * Dawes patent melody attach 
 ment , " singularly beautiful effect , melody 
 clearly define separate accompaniment 
 play e instrument . great aivantage 

 receive letter Miss Lizzie Wilson 
 complain person singe East - end Music 
 Halls . 
 accident intention , willingly opportunity 
 correct misapprehension arise circum- 
 stance 

 tue hasty line accompany an- 
 nouncement decease Sir George Smart , number , 
 scarcely sufficient justice man bear con- 
 spicuous history musical progress country . 
 , organist Chapel Royal , St. James , 
 conduct musical arrangement coronation King 
 William Queen Victoria ; conductor 
 musical festival Westminster Abbey 1834 , 
 founder , energetic member , 
 Philharmonic Society ; direct Lenten Oratorios 
 Drury - lane Covent Garden Theatres , musical 
 festival London ; , manager musical de- 
 partment Covent Garden Theatre , lesseeship 
 Mr. Charles Kemble , engage Weber compose Oberon , 
 owe untiring zeal artistic 
 judgment , bright example set hold 
 position power world art . 
 merely perform duty carefully honourably 
 ; know career Sir George Smart 
 aware , conductor great work 
 time country , especially Beethoven Ninth Sym- 
 phony , Philharmonic , encounter opposition 
 lead member Society ; 
 mention , direct Haydn Creation , Beethoven 
 Mount Olives , Mendelssohn St. Paul , composi- 
 tion great german master , introduction 
 England ; personal exertion , produce Spohr 
 Opera , Azor Zemira , Covent Garden Theatre ; con- 
 ducte composer Oratorio , Judgment , Nor- 
 wich Festival . personal sympathy artist proverbial . 
 Spohr live house great Portland - street time , 
 Weber guest period engagement 
 Covent Garden Theatre , produce conduct Oberon , hos- 
 pitality unfailing kindness Sir George poor die 
 composer , surprise know gentle nature . 
 hear lately , tear eye , dwell 
 grasp hand Weber retire 
 bed night life , " shall wife , " 
 , morning Sir George fetch bedside 
 servant , unable receive answer knock- 
 ing composer door , Weber find dead bed , 
 tranquil expression face , length 
 find rest death bodily suffering long pre- 
 vent enjoy life 

 remind comparative youth 
 term modern music , fact Sir George Smart having 
 live day Haydn , Mozart , Beethoven ; 
 link time Handel , curious fact 
 hear Joah Bates ( , boy , 
 Handel direct Oratorios ) , circumstance connect 
 precise manner Handel great work per- 
 form country ; authority 
 subject , information highly valuable . honourable 
 conduct , strict integrity , singleness purpose character- 
 ise long professional life Sir George Smart , 
 know , publicly privately , bear ample testimony . 
 , stamp history 
 music England ; kindly notice spring 
 spontaneously dozen pen decease , fit 
 epitaph memory 

 tue late Mr. John Duff , die 7th ult . , 
 founder music - publish business , long know Duff 
 Hodgson , recently Duff Stewart , Oxford Street . 
 enter house Goulding , D'Almaine , Potter , Co. 
 1814 ; leave , 1831 , establish . 65 , 
 Oxford Street . author word popular 
 song ; * brave old Temeraire , " Hobbs ; * * 
 iteel clothe ship England , " Winn ; 

 organ . instrument , fine . contain 26 
 stop . 
 s3eLPER.—On 14th ult . , Concert 

 Belper Glee Madrigal Society , assistance 
 Mr. G. Knowles , Mr. John Naylor , cornet player Matlock . 
 glee , madrigal , chorus 
 evening utmost success ; Mrs. Harrison sing 
 song , receive audience . pianist 
 Mr. Trimnell , ( organist parish church , Chesterfield . ) 
 perform Beethoven ' * Moonlight Sonata ; " Messrs. Knowles 
 Naylor contribute cornet solo . concert 
 extremely attend 

 SLACKBURN.—On Thursday , 28th February , 
 organ lately erect Christ Church , Messrs. Conacher 

 Epinsurcu.—On Monday evening , 11th ult . , 
 Mr. M‘Hardy miscellaneous concert Queen - street 
 Hall . assist Miss H. Lindley Kdinburgh 
 Solfeggio Association , honorary conductor 

 Oberon fairy land , " successful . Miss ® atten 
 Lindley sing song compose Mr. M‘Hardy , " ® chari 
 verdure clothe , " Creation ( encore ) . Mr. M‘Hardy perform iver 
 pianoforte solo satisfactory manner.——On Mon . 
 day evening , 4th ult . , second performance Judas Muccabeus 
 Music Hall , Mr. T. K. Longbottom choir , disp ] 
 chorus nnmbere 100 , band 20 performer , oie 
 lead Mr. A. O. Mackenzie . chorus com- 
 mendable spirit precision ; ' ' disdainful danger , " * Jude , 
 happy land , " especially , deliver telling effect . acho 
 air ' " ' mighty king , " " ' wise man flattering , " sing ey " 
 corsiderable effect Miss Hiles . Mr. Inkersall good pag 
 voice , render successfully try music , 
 Mr. Weiss , , energetic reliable , create tt } 
 usual impression inspirit delivery ' ' Arm , arm , ye ong 
 brave . " Miss Nalton p pleasing contralto voice , Lode 
 sing music allot creditable manner . Mr. T , head 
 K. * ongbottom conducted.——In celebration St. Patrick Eve , Li 
 Saturday evening , 16th ult . , devote entirely irish melo- 
 die , dance , reading . audience large , en - je ° ° ! 
 thusiastic , singing Miss Dunsmore , Miss Kirk , Mr , 
 Inkersall satisfaction song wae ° ° % 
 encore . feature evening recitation pros " 
 Mr. Nevin , feeling visibly adat 
 affect audience . ty 
 Frome.—On Tuesday evening , 5th ult . , Keaf 
 successful performance Handel Messiah : 
 chanic Hail . soloist Miss Rosalie Cox , Mrs. F. Harrold , jus 
 Master Cox , Miss . Crattwel ] , Mrs. Ames , Miss Olive , messr , cot 
 Cogswell , Harvey , Harrold , Greenland , Lewis , Harris . catch 
 chorus exceedingly sing choir number 
 80 voice . concert aid fund raise h pass 
 repewe Christ Church . | 
 Giascow.—On Tuesday evening , 12th ult . , ag ute 
 concert sacred secular music Greyfriars Phil - f : 
 harmonic Society , conduct Mr. Robson . conf hoven ’ 
 siste Beethoven Mass C , perform oyerty 
 considerable effect 60 member society , assist anf 
 orchestra 16 performer . second contain sclectiol ists w 
 song glee , Satisfaction , ref Joachi ; 
 ceive frequent encore . Mendel 
 GranrHaM.—An Amateur Vocal Society lately bag 
 establish town , honorary direction Dr. sleenos 
 Dixon ( Onn ) , organist parish church . society ab Gipsy . 
 ready nu _ r nearly 80 perform member , goodly ' 
 number 0 : .. vnorary member , privilege attend sims p , 
 weekly practice . addition 
 Hanitty . — excellent performance 
 Handel Oratorio , J / essiah , place 6th ult . , inf new , 
 Bethseda Chapel , benefit North Staffordshire Inf Church . 
 firmary . chorus sing chiefly work man ani Shields , 
 woman Potteries ; highly satisfactory manner inf ® Mr. 
 reflect utmost credit carefu ® e 
 training Mr. Powell , believe teach handson 
 Tonic Sol - fa method . professional vocalist engage t ) 
 Madame Lemmens - Sherrington , Miss Nalton , Mr. Leigh Wilson , git 
 Mr. Brandon . band consist performer , : 
 Mr. Hallé orchestra Manchester , addition ox : 
 Mr. H. Walker , organist Free Trade Hall , teady n 
 preside organ , Mr. T , Harper perform trumpet Thursda 
 , Messiah scarcely complete . iti & poric 
 satisfactory find concert realize surplus £ 60 fa m 
 fund Infirmary . Esq . , 
 ‘ Z jo 
 HeAton.—On Shrove Tuesday Annual Soiré Owen . " } 
 Heaton Mechanics , hold Church School - room , @ society . 
 150 sit excellent tea . tea , recitation " o resi 
 Mr. B. Preston , Mr. J. M'Gourlay , Mr , Hardacre , % est : 
 receive . church choir , assist Messrs. Esq , 
 J. Pickles , ’ . Wood , V. Croxall , W. Northrop , sing selection righ 
 song , - song , & c. accompany thei | 
 pianoforte Mr , Mark Widdop , organist St. Barnabas ’ Church , sust 
 Heaton . chor 
 Hexiam.—On Monday evening , 4th ult . , ff hejrur 
 concert aid Abbey Choir Fund Town 
 Hall member choir , assist Miss Annie Penmat eo 
 amateur vocalist Newcastle . ' programm oxtr 
 comprise great number choice selection eminew@ ® gave o 
 composer , occupy upwards hour half . 82 % Fanny Ay 
 piece allot choir , render 9@ L. 8 . pal ; 
 excellent manner forth approval audience # accompan 
 heartily encore . Mr. Nicholson act conductor P 
 ANN 

 accompanyist 

 vocal score , 2s , 6d . ; vocal , 2s . 6d 

 inD. Sanctus ; , , Sanctus Beethoven . 
 score , 6d . ; vocal Ant 6d 

 vocal score 

 Boox I. 
 1 slow movement . Mozart 
 2 holy ! holy ! Spohr 
 3 slow melody . Mendelssohn 

 4 Gloria Mass b flat . | 20 themarvellous work . Haydn | 110 Aria . Mendelssohn 196 Asie , Quartet , 10 , 
 _ 4 . Andre | 21 Andante movement . 111 " fix everlasting | 135 Aria , 4 . 
 5 Benedictus , Mass . 1 . Weber Mendelssohn | ag ke~ Aria . J. @. Herzog 
 6 Andantino , Sonata . Dussek | 22 Agnus Dei . Mozart 112 solemn March . Dr. Boy 137 prelude Rinck 
 7 prelude . C. F. Gluthmann 23 Slow movement . hatz | 113 impromptu , op . 142 . Ped 1 138 Adagio Clarinet Co 
 8 Slow movement 7th| 24 Gloria Excelsis . ( Pedal | p 5 bb . ) P. FP . AW ie 4 certo , Coal 
 Symphony . Haydn Obb . ) Mozart 
 9 Gloria Excelsis , 1st Boor XVIII . 
 Mass. ( Pedal Obb . ) Haydn Boox iv . Boox xv . 7 Arie , Comtabile . Bee 
 agio 
 Boox IL 25 Adagio Movement . Rinck | 114 Aria , Quartett , op.77 . Haydn " Mendel 
 10 air . Haydn 26 Andante Movement . Mozart | 115 " thou shalt bring . " | 141 Aria , Kalkbrenner 
 11 bless arethe depart . Spohr | 27 prelude . anonymous Israel Egypt . Jandel 142 Benedictus , Mass . 1 . 
 12 piece . Rinck 28 Dona Nobis the3rd mass. | 116 Andante . Hummel . Moo 
 13 slow movement Ist Hummel | 117 Larghetto , Symphony D. | 143 Andante . Haydn 
 symphony . Haydn 29 Slow movement . Spohr Beethoven | 144 prelude . J. E. Eberlin 
 14 air semplice . Pleyel 30 Andante movement . Czerny | 118 prelude . A. Hesse 145 voluntary . A. s 
 15 holy ! holy ! Handel 31 Andante quartett . 119 Adagio movement . Rinck 146 Ave Maria . Arcadelt 
 16 air . 82 Aria . Spohr [ Haydn | 120 Aria vary . ( Pedal Obb . ) | 147 Chorale . M. G. Fischer 
 17 slow movement Sonata , | 33 Hallelujah Chorus . ( Pedal | A. Hesse | 148 Hallelujah , Saul . ( Pedal 
 Op . 2 . ( Pedal Obb . ) Beethoven ' bb . ) Handel | 191 Soft Voluntary . < A. Hesse Obb . ) - 
 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDON 

 Boog iii 

 Boox V. 
 84 Aria . Spohr 
 35 Pastorale . Zipoli 
 36 prelude . J. G. Herzog 
 87 introductionand fugue . Rinck 
 88 Adagio movement . Rinck 
 39 aria P.F. Sonata . Mozart 

 41 slow movement . Beethoven 
 42 slow movement . Beethoven 
 43 fugue . J. EZ . Eberlin 

 Pedal Obb . ) Mozart 

 100 romance , symphony . 14 . 
 Haydn 

 101 Aria , Sonata , op.30 . Beethoven 

 102 pastoral air . Geminiani 

 Boox xiv 

 107 Aria . < A. Hesse 
 108 Aria , op . 13 . Beethoven 
 | 109 slow March . Steibelt 

 Boox x. 
 75 voluntary . Anton Andre 
 76 Dona nobis pacem . Mozart 
 77 prelude . C. G. Hoépner 
 78 Aria . Dussek [ Morar 
 79 Andante Sonata , op . 6 ) 
 80 air — ' ' Lord mind 
 ful . " Mendelssohn 
 81 chorale — ' ' sleeper , awake ' 
 Mendelssola 

