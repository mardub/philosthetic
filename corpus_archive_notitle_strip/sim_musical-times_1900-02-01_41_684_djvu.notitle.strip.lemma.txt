viesgeiieiain 10 , at 3 

 4 Brahms . 
 .- Beethoven . 
 Georg Liebling 

 tragic overture .. ee 
 symphony , no . 5 , in C minor | . 
 new Pianoforte Concerto 
 Herr Grorc LigsLinc . 
 Tannhauser ’s Pilgrimage 

 FEBRUARY 24 , at 3 

 overture — Macbeth " . Sullivan . 
 symphony in G minor ( no . 40 , B. & H. 's edition ) .. Mozart . 
 Violin Concerto in D . Beethoven . 
 " Mons . Ysave 

 Overture — ' " Le conte Romain " " es Berlioz 

 Violin Solo .. ; é " a ee 
 " Mons . Ysave . 
 Adagio and Canzona from ninth Sonate for on une 
 Harpsichord ae ea . Purcell . 
 Eine Faust - Ouvertiire .. Wagner . 
 vocalist , Mr. ' Cuartes KNow zs 

 Marcu 1 10 , at 3 . 
 overture — ' Ribezahl " ( Ruler " arti ' Weber . 
 Symphony , no . 8,inF .. . Beethoven . 
 overture — " Idomeneo " A Mozart . 
 Pianoforte Concerto , no . 5 , in e flat ( " Emperor ' . Beethoven 

 err ZWINTSCHER . 
 overture — Tannhauser " os « 3 ae 7 - » Wagner , 
 Siegfried Idyll ee ea ° Wagner . 
 Toccata in F ( orchestrate by Esser ) xs . » Bach 

 August 6 — less than a month later — the overture be 
 finish . Pastor Schubring , Mendelssohn ’s attach 
 friend , tell a pretty story concern its composition . 
 he say : " the weather be beautiful , and we be 
 engage in an animated conversation as we lie in 
 the shade on the grass [ in the Schonhauser Garten ] , 
 when all of a sudden , Mendelssohn seize I firmly 
 by the arm , and whisper ' hush ! ' he afterwards 
 inform I that a large fly have just then go 
 buzz by , and he want to hear the sound it 
 produce gradually die away . when the overture 
 be complete , he point to the semiquaver scale 
 passage of the violoncello which make the modula- 
 tion from b minor to f sharp minor , and say : 
 ' there , that ’ the fly that buzz past we at 
 Schonhauser 

 MENDELSSOHN bring the score of his overture 
 with he to England , on the occasion of his 
 early visit to London , in 1829 . the work be 
 first play in England , appropriately enough , on Mid- 
 summer night , 1829 , at a concert give by Drouet , the 
 flautist , and it be conduct by Mendelssohn . at 
 the same concert he play Beethoven ’s e flat Piano- 
 forte Concerto , then an absolute novelty in this 
 country . after the concert , the score of the overture 
 be leave in the hackney coach by Attwood and — lose ! 
 ' " * never mind , " say Mendelssohn , upon be tell of 
 it , ' I will make another . " he do , and on compare 
 the new score with the band part , no discrepancy 
 could be discover 

 seventeen year elapse between the composition 
 of the overture and the remainder of the ' Mid- 
 summer Night ’s Dream " music ; yet there be not the 
 slight trace of any dissimilarity in style or work- 
 manship — in fact , its perfectness as a _ perfectly 
 complete musical equipment to the play be one of 
 its strong feature . the first performance in 
 England of the entire music be at the Philharmonic 
 concert of May 27 , 1844 , conduct by Mendelssohn . 
 we be enable to give some hitherto unpublished 
 extract from Mendelssohn ’s letter to his english 
 publisher , Mr. Buxton ( Ewer & Co. ) , relate to the 
 english publication of the work . on December 13 , 
 1843 , he send the * ' Scherzo , Notturno , and March " 
 ( i.e. , the famous * * Wedding March " ) , pianoforte duet , 
 and mention fifteen guinea as the price . " if , " he 
 say , ' you wish to have the copyright of the whole 
 for England ( pianoforte arrangement and full score ) , " 
 the price of the complete work ( consist of nine 
 other piece ) ' " ' would be the same again as those 
 three ' --i.e . , thirty guinea for the entire music ! 
 he add that the work be give seventeen time 
 in six week at the Royal Theatre , Berlin . 
 a little later he write to say that the english word 
 will go to the music without any alteration , and 
 propose that some arrangement should be make 
 with Cramer and Co. , who have publish the overture 
 in 1829 , whereby the entire work could be issue in 
 a complete form . Buxton evidently suggest that a 
 pianoforte arrangement for one performer should be 
 make , as Mendelssohn say he be afraid that ' it will 
 be a little difficult . " it be not surprising to find that 
 the last reference to the publication contain the 
 word " alteration . " but Mendelssohn do no 
 alter his mind in regard to his visit to England . 
 " since yesterday , " he write to Buxton on March 5 
 1844 , ' I be certain that I shall go to England , and 
 I need not tell you how great a pleasure I anticipate 
 from a stay in your country . I intend to arrivt 
 towards the end of April and to bring several new 
 thing with I , and to remain several month , and t0 
 be very happy in old England 

 from a photograph take in 1863 

 librarian of the Paris Opera House , and of Herr Felix 
 Weingartner — the former probably take special over- 
 sight of the literary and research aspect of the task , 
 and the latter eminent conductor be responsible 
 for the musical erudition . the full score will be 
 issue in fifteen folio volume at a subscription price 
 of fifteen shilling each ; the opera will not , however , 
 at present be publish , though their overture will 
 appear amongst the instrumental music . a pianoforte 
 edition of the vocal work be also to be issue . the 
 Berlioz publication — of which nearly all the instru- 
 mental music be almost ready — will take rank with 
 the monumental edition of Beethoven , Mozart , 
 Mendelssohn , and other great composer issue by 
 the great Leipzig firm of Breitkopf and Hartel . this 

 Piazza Michel Angelo Buonarotti , Milan . having 
 appoint the administrative council , which include 
 Arrigo Boito , the composer of " Mefistofele , " Signor 
 Negri , the author and senator , and Signor Giulio 
 Ricordi , of the eminent music publish firm , the 
 veteran composer , in a speech last over two hour , 
 explain the principle upon which he desire the 
 Institution to be carry on . finally , the Maéstro , 
 having hand the Deed of gift to the Council , 
 intimate to the latter that he have relinquish the 
 proceed of copyright of all his work , from January 
 i , 1900 , in favour and as part endowment of the 
 Home . truly a right royal gift and a noble 
 monument to perpetuate the memory of Italy ’s 
 Grand Old Man 

 increase the significance of the text . the perfect fitting of 
 the music to the word be , indeed , a special feature of the 
 anthem . in his endeavour to attain this end Dr. Davies 
 have occasionally expand the usual four - part writing into 
 that of seven part , but such passage be few . towards 
 the end there occur a quintet for two soprano , two tenor , 
 and a bass , which greatly add to the effectiveness of the 
 composition . a reference to the opening theme in 
 augmentation and four bar of choral writing of con- 
 siderable originality bring the work to an impressive close 

 Beethoven . by Frederick J. Crowest . 
 Wagner . by Charles A. Lidgey . 
 ( the Master Musicians Series . edit by Frederick J. 
 Crowest . ) [ J. M. Dent and Company 

 these two volume be specimen of mere book - making . 
 it would occupy too much space to point out their short . 
 coming and literary defect , more especially that on 
 Beethoven . but we should be glad to be direct to 
 the source of any biographical information relate to 
 ' " Anhang , " whose name ( ! ) be give by Mr. Crowest as an 
 " author " in his appendix A. what would Beethoven 
 have say to any biographer of his who could perpetrate 
 such a travesty as : ' ' a journey which , unhappily , prove 
 to be the last he undertake ere essay that bourne along 
 which no traveller have yet turn a face . " shade of 
 Shakespeare 

 the National Music of America and its source . by 
 Louis C. Elson 

 113 

 dr. strong one . but he could do something to make the 
 oint- weak one strong , while the strong one could take care of 
 vood , themselves . 
 — " WOMAN as a MUSICIAN . " 
 t te Dr. H. A. Harding ( Bedford ) contribute a paper at the 
 pipe afternoon sit on ' ' Woman as a Musician " " — a subject , 
 " tone by the way , which be ably treat by Mr. Stephen S. 
 have Stratton in an essay , entitle ' ' Woman in relation to 
 low musical art , " " which he read before the Musical Associa- 
 with tion on May 7 , 1883 . Dr. Harding begin his survey 
 opt with four century ago , explain that despite what 
 learn man might say about japanese and greek music , 
 parish music be not an independent art until the fourteenth 
 ante century . his initial reference to the fair sex be ' ' woman 
 . to its as a singer , " in the course of which he interpolate the 
 a Mr. remark that he be not sure that woman be impregnable 
 1 Cups in the world of song , while he add that he ' ' understand " ' 
 vice . " prime donne be ' ' a little trying at time . " the fact 
 - much be that their ( the lady ’ ) knowledge be not equal to 
 in Mr. their great natural gift , and many humble member of the 
 ess the orchestra which accompany they be much well 
 mateur musician than the prime donne themselves . deal 
 leputy- with song for woman , he instance one wherein the word 
 ly upon " Jove " occur twelve time in eight bar ! every child , 
 hat we he contend , should be teach to sing , and faulty voice 
 of the production should be promptly check . now - a - day in 
 rest large class girl be tell to " sing up , " which often 
 enough ruin their voice and completely destroy 
 those charming high note which child possess . 
 after quote Lord Chesterfield ’s remark to his son , that 
 " he would rather see he with a pipe in his mouth than 
 ns . with a fiddle under his chin , " Dr. Harding advert to 
 the enormous increase of female musical executant , and 
 especially violinist , which , he add , would perhaps induce 
 rporate [ " omposer to return to the symphonic form of Beethoven ’s 
 sth ult , time . ' " weare tell that there be woman novelist , woman 
 ete of attist , and so on , " say the lecturer ; ' why not 
 woman composer ? " ' ' I say , emphatically , that there ave 
 iad wel . woman composer ; they do actually exist . " he definitely 
 nd ult ) , state their number to be 489 ; but no woman , he add , 
 ety , read have ever take a high position as a composer , although 
 n of the [ ‘ tey have do so in literature and the sister art . they 
 f500 have fm be splendid executant ; why have they not the genius 
 the con- jg ' compose ? one authority declare that woman ’s strength 
 ciety be of body would hardly endure necessary strain of brain and 
 00 and jm " tve power to compose . he take strong exception to the 
 mination . jg ectionable use of the word " feminine " and " femininity , " 
 ‘ rominent ( @ * apply to composer and to music ; and after allude 
 wasnext the allege femininity of Schubert , he express the 
 ' ascheme fg devout hope that if Schubert really be feminine , woman 
 d in view . @ " ould imitate he . woman as musician have be pre- 
 1 Council , § " tte from come to maturity for want of training and 
 development . although there have not hitherto be a 
 Bridge Seat composer find in the rank of woman , in these day , 
 his Roya fj " ten woman be advance so rapidly , there be no 
 Summings fj son why she should not take a high place in the rank 
 ion of the composer , 
 secondary § during the discussion which follow the reading of 
 this be the paper Sir Frederick Bridge arrive . in wind up the 
 t obstacle ff usiness of the meeting , he express the opinion that 
 he Society Oxford and Cambridge should allow woman to become 
 atisfactory ig " @duate in music . 
 ' © . improve Mr. F. H. Cowen , the president of the second day , 
 yutside the # pen the proceeding with an address on " the 
 Training of Conductors and Accompanists , ' " ' of which we 
 tint a full and authorised report in another column . 
 2 paper on ' " pitch : past , present , and future . " 
 syster ic Mt. W. H. Cummings , the chairman on Thursday ( the 
 pianoforte third , » 
 command ba day of the Conference ) , contribute an essay on 
 ir as due to Pitch : past , present , and future . " Mr. Cummings start 
 ost that ' y correct the mistaken impression that the pitch be 
 ve of finge ! miginally very low and have gradually grow high and 

 e . he review the actual state of affair existent at 
 fhe time of Purcell and subsequently . in Purcell ’s time 
 € be two pitch , one for the church and one for the 
 ber . on the continent , too , there be two pitch ; 
 ind he instance an organ tune a third of a tone above 
 amber pitch , and a stop be add a third low than 

 13th ult . Balfe ’s ' Satanella " ' be revive . the perform- 
 ance be most successful and attract a large audience . 
 Miss Chrystal Duncan take the title - part and Mr. Turner 
 appear as Count Rupert 

 at the Oratory , Edgbaston , the first of three lecture- 
 concert be give on the 3rd ult . the programme be 
 devote to chamber music , with Herr Theodor Werner as 
 violinist , and Mr. W. Sewell ( organist at the Oratory ) 
 as pianist . among the piece give be the Septet for 
 ianoforte , trumpet , and string , by Saint - Saéns , on the 
 8th ult . , after the lecture , Herr Werner give a historical 
 violin recital . the third function be a chamber concert , 
 give at the Edgbaston Assembly Rooms , on the 2oth . 
 the artist be Madame Amina Goodwin , Herr Werner , 
 and Mr. W. E. Whitehouse , with Mr. W. Sewell as 
 accompanist . the programme include Beethoven ’s Trio 
 ( op . 97 ) and Rubinstein ’s Op . 52 

 on the 16th ult . Mr. Halford resume his orchestral 
 concert in the Town Hall . the programme comprise 
 Schumann ’s Symphony , no . 2 , in C ( Op . 61 ) , the prelude 
 to act ii . and iii . of " * Manfred , " by Sir A. C. Mackenzie 
 ( first time here ) , and Wagner ’s Prelude to Act iii . , 
 " Lohengrin . " the whole be finely render , and 
 Mackenzie ’s composition be most interesting . Signor 
 Ronchini appear here for the first time , and give an 
 admirable reading of the solo part in Hans Sitts ’s Violon- 
 cello Concerto ( Op . 34 ) , an excellent composition ; he also 
 play some short solo piece 

 just too late in December last to be notice in this 
 monthly chronicle , the Choral Union give a performance 
 of Dvorak ’s ' * Stabat Mater , " in the McEwan Hall , under 
 Mr. Collinson ’s baton . interest in the beautiful work 
 attract a very large audience , who award unstinted 
 applause to the choir and its capable conductor . the 
 intonation of the tenor be not always above reproach , 
 but , on the whole , the chorus deserve all the praise so freely 
 give . the second part of the concert be devote to 
 Mr. Hamish MacCunn ’s ' * Cameronian ’s Dream 

 Messrs. Paterson ’s orchestral concert be well attend 
 this season than ever . the Scottish Orchestra have settle 
 down to steady and well work , and the accompaniment 
 to the ' ' Stabat Mater ' ? and Brahms ’s Pianoforte Concerto 
 ( no . 2 ) be as carefully and successfully play as be 
 symphony by Goetz ( in F ) , Beethoven ( no . 1 ) , and 
 Dvorak ( ' ' New World " ) . the soloist at the concert 
 mention be Mr. Frederick Dawson ( who be very 
 well receive for his excellent performance ) , on the 8th ult . , 
 and Madame Marchesi ( whose song be enthusiastically 
 applaud ) , on the 13th ult 

 a crowded audience assemble in the McEwan Hall , on 
 the 22nd ult . , to hear Rheinberger ’s ' ' Christoforus ’' and 
 the Ninth Symphony , give at Messrs. Patterson ’s concert 
 by Mr. Kirkhope ’s Choir and the Scottish Orchestra . this 
 splendid choir be in its good form in its rendering of the 
 ' Ode to Joy . ’' the long high note , the terror of choir 
 and conductor , be easily and successfully vanquish . 
 the instrumental movement suffer from an ineffective 
 disposition of the band and a very pointless reading of this 
 noble composition . in ' Christoforus " ' the singer seize 
 the numerous opportunity which the rich and beautiful 
 score offer to show all the quality that have win for 
 they their enviable reputation . Mr. Kirkhope conduct 
 ' " ' Christoforus " ' and Mr. Bruch the Symphony 

 on New Year ’s Day the Choral Union give the annual 
 performance of ' ' the Messiah . " never have the chorus 
 show to well advantage ; the massive chorus be 
 splendidly give , and the florid passage take with a 
 precision and ease which leave nothing to be desire 

 MUSIC IN GLASGOW . fav 
 ( from our own correspondent . ) suc 
 on December 23 the lady pupil of Glasgow Atheneum bee 
 sang Cowen ’s " Daughter of the Sea " and other piece at rs 
 their annual concert . Co 
 the Classical Orchestral concert have be fairly well - 
 attend . Mr. Bruch have give more or less satisfying of ¢ 
 rendering of Tschaikowsky ’s ' ' pathetic " Symphony , je 
 Beethoven ’s no . 4 and no . 8 , Mozart ’s " Jupiter , " and whi 
 Goetz ’s symphony in F ( Op . 9 ) , a particularly interesting brea 
 revival , which be well receive ; also Mackenzie ’s Prelude serv ! 
 to Act II . of " Manfred . " the only soloist of note who at 
 have appear be Mr. Frederick Dawson ( always welcome a arr 
 here ) , who play Brahms ’s Second Concerto ; M. Siloti , = 
 who play the Schubert - Liszt " ' wanderer " Fantasie , = 
 and Madame Blanche Marchesi . inter 
 at the second concert of the Helensburgh Subscription ment 
 series , the Brodsky String Quartet play Schumann 's ie 
 Quartet , Op . 41 , no . 3 , and Beethoven ’s Op . 18 , no . 6 , oh 
 and Miss Nedda Morrison sing . i th 
 at the annual Guards ’ concert , Madame Albani and hg 
 Mr. Edward Lloyd sing . 
 the death be announce of one of the most notable ot wond 
 scottish musician , Mr. J. Roy Fraser , teacher and greate 
 organist , Paisley , who , by mean of great open - air pn 
 concert on the Braes of Gleniffer , raise fund sufficient fiben : 
 to erect statue of Tannahill and Burns in the town . = ie 
 we | 
 special 
 music in LIVERPOOL and DISTRICT . pee 
 ( from our own correspondent . ) aie 
 at the Philharmonic Society ’s concert , on the oth ult , , third re 
 the most interesting feature be Schumann ’s Symphony § the gift 
 in D minor ( no . 4 ) . in Beethoven ’s Pianoforte Concerto J Néruda 
 in C minor the solo part be play with admirable 9 % and th , 
 sympathy and expression by Mr. Leonard Borwick . much § tenderir 
 linterest be add to this concert by fine rendering of evening : 
 Smart ’s part - song , ' ' dream , baby , dream , " and Cooke 's J Brahms 
 " hark ! the lark . " on the 17th ult . the Societa 9 and vio 
 Armonica ’s 119th concert , under the direction of Mr. Vasco & & forte anc 
 Akeroyd , be a great success . the orchestra prove @ clarinet , 
 reasonably equal to Beethoven ’s A major Symphony , @§ ult , Mr. 
 and far exhibit skilful efficiency in Dvorak ’s suite in § admirab ] 
 D ( Op . 39 ) . Mr. Akeroyd , who have work hard and with [ & the Brah 
 excellent result to bring the orchestra of this old Society § the sam 
 up to a reliable standard of efficiency , play the solo & clarinet . 
 part in Max Bruch ’s Violin Concerto in G minor . on ff aquisite 
 December 28 an Eisteddfod choral competition between & modulate 
 the Dwyrwd Male - Voice Choir , of Talsarnau , and the Mf that the « 
 Manchester Prize Glee Society result in the former § § asociatio 
 win an award of £ 20 and a gold medal . and not 
 Gounod ’s ' ' redemption " be admirably perform by ff silfully ¢ 
 the Sunday Society , in St. George ’s Hall , on the r4th ult , Ball con 
 under the skilful direction of Mr. W. I. Argent . the mi successful 
 principal vocalist be Miss Ada Standen , Mr. Ben Bigcin dist 
 Roberts , Mr. Arthur Weber , and Mr. Hargreaves Hudson . Chopin mo 
 Dr. Peace render excellent service at the organ . Mr. La : 
 vatie by t 
 — the 

 MUSIC in MANCHESTER . a ga " 
 ( from our own correspondent . ) tonotonou 
 after a fortnight ’s silence , it be pleasant again to I I MW 
 listen to the Hallé Orchestra , albeit its performance on the 9 % " be 
 11th ult . , even in so well know a work as Mozart 's G 
 minor Symphony , be by no mean so finished as we 
 have be accustom to during the last three year . it 
 must be confess that the interest excite by the 
 " Hiawatha " scene of Mr. Coleridge - Taylor be not ( 
 increase by his Orchestral Ballad in A minor . in the 
 other novelty of the programme — Dr. Stanford ’s concert : 
 variation upon the jovial old air ' down among the dead § as usual , 
 man " ' — there be passage so charming that one be reluctant " clusive att 
 to own that the whole work be render somewhat dis jp*ks of the 
 appointing by the attempt to make the most of isolated mnsider a fi 
 section of the text , instead of endeavour to grasp the Pier . 
 general idea of the whole theme with its irresistible jollityand the gteate 
 genuine english character . the pianoforte part — although FFtds Choral 
 admirably play by Mr. Leonard Borwick , an especial FP0del set by 

 at the meeting of the 18th ult . , besides Sir Hubert 
 Parry ’s Orchestral Variations , originally give here in 
 1898 , there be two work quite fresh to our ear . 
 Glazounow ’s Symphony in b flat ( Op . 55 ) be decidedly 
 interesting , bold , and fresh ; and only in the last move- 
 ment degenerate into that blatant uproar to which russian 
 writer seem prone . the " Sea picture ' ? of Edward 
 Elgar be present by the orchestra under the guidance 
 of the composer , with Miss Clara Butt as the narrator . 
 as when listen to many of the bright fancy of an 
 artist now attract great attention , one could not help 
 wonder whether the young author would not feel 
 great freedom in a purely orchestral work , wherein his 
 imagination could take a wide range and his idea be not 
 merely sketch , but fully work out . Miss Ilona 
 Eibenschutz play Schumann ’s Pianoforte Concerto and 
 several of the Brahms Waltzes 

 we have have quite a festival of chamber music , render 
 specially interesting by the visit of Herr Muhlfeld , of which 
 Mr. Brodsky , Mr. Cohn , and Mr. Carl Fuchs have take 
 advantage in produce , at their respective concert , many 
 work which we may only rarely hear . at Mr. Brodsky ’s 
 third recital , on the 17th ult . , after Beethoven ’s Op . 131 , 
 the gifted clarinettist , with the assistance of Miss Olga 
 Néruda , give we the Brahms Sonata in e flat ( Op . 120 ) , 
 and then join the quartet party in a most finished 
 rendering of Mozart ’s delightful quintet in A major . two 
 evening later , Mr. Isidor Cohn ’s programme include the 
 Brahms Trio in A minor ( Op . 114 ) for pianoforte , clarinet , 
 and violoncello , Schumann ’s Fantasiestiicke for piano- 
 forte and clarinet , and Mozart 's Trio in E flat for pianoforte , 
 darinet , and viola ; and , for his third meeting , on the 22nd 
 ut , Mr. Carl Fuchs , modestly keep himself and his 
 admirable violoncello play in the background , provide | 
 the Brahms Quintet ( op . 115 ) for clarinet and string , and 
 the same composer ’s Duet in F minor for clavier and 
 tatinet . as the result of all this enjoyment of the 
 exquisite playing of Herr Mihlfeld and of the delicately 
 modulate tone he produce , one can not but be sensible 
 that the clarinet may be hear to full advantage only in 
 association with orchestral instrument of varied character 
 ad not with a mere pianoforte accompaniment , however | 
 skilfully that support may be manage . at the Schiller | 
 Hall concert Miss Irene Schaefsberg , who play so 
 successfully a few week back at the Free Trade Hall , 
 again distinguish herself in her interpretation of some 
 Chopin morceau 

 Mr. Lane ’s Ballad concert be always attractively 
 vatie by the intersperse choral work , and Leslie ’s ' * ' how 
 sweet the moonlight " and Battye ’s ' ' how short , sweet 
 low , " serve to give solidity to the programme of the 
 nth ult . , which otherwise might have be somewhat 
 nonotonously fragmentary . chief among the soloist be 
 Madame Marchesi , whose rendering of Schubert ’s " Erl- 
 King ? be simply superb 

 at Bradford , Miss Wehner give a concert on the 3rd 
 ult . , at which the Chaigneau Trio be introduce to 
 Yorkshire . these very clever young lady give a highly 
 finish performance of Saint - Saéns ’s Pianoforte Trio in F , 
 and play some solo very artistically . Miss Wehner 
 sing with great artistic intelligence a most interesting 
 series of song of varied character . the subscription 
 concert , on the 1oth ult . , be of the miscellaneous order , 
 the artist engage be Miss Louise Dale , Miss Butt , 
 Messrs. Grover and Plunket Greene , with Mr. Moszkowski 
 as pianist and Mr. Gérardy as violoncellist 

 the Huddersfield Glee and Madrigal Society , under Mr. 
 Ibeson , give a varied programme of concerted vocal music 
 on December 19 . on the 16th ult . there be another of 
 the Subscription concert , at which Madame Amy Sherwin , 
 with Mr. H. Verbrugghen as violinist , Mr. Willibald 
 Richter as pianist , and the Meister Glee Singers , whose 
 ensemble be as perfect as ever , make their appearance . the 
 very artistic Brodsky Quartet Party be responsible for 
 the Halifax Subscription concert on the roth ult . , and 
 play quartet by Beethoven and Schumann most 
 sympathetically 

 MUSIC in WALES . 
 ( from our own correspondent 

 the fourth international concert give by the Cercle 
 des Etrangers at Monte Carlo be devote to english 
 composer . the programme include Mr. Frederick 
 Cliffe ’s Symphony in C minor , two of Mr. F. H. Cowen 's 
 ' dance in the Olden Style , " and Mr. Edward Elgar 's 
 ' Imperial ' ? March . the concert be , as usual , under 
 the able direction of M. Léon Jehin 

 Tue Musical Festival of the Lower Rhine , in Whitsun 
 week , will be hold this year at Aix - la - Chapelle , under 
 the conductorship of Herren Richard Strauss and F. 
 Schwickerath . amongst the work to be perform art 
 Liszt 's oratorio " Christus , " Beethoven ’s Ninth Symphony , 
 Haydn ’s ' season , " and probably a new work by Richatd 
 Strauss 

 minor ( M 
 tt solo ingt 
 " cert of th 
 Buttykay , 
 CARLSRUH 

 Hutui.—Dr . G. H. Smith give an interesting lecture 
 on Henry Purcell before the Literary and Philosophical 
 Society , on the 16th ult . after refer generally to the 
 neglect of Purcell ’s music and to the condition of musical 
 art in that composer ’s time , Dr. Smith give an historical 
 outline of Purcell ’s opera ' ' Dido and ZEneas , " which work 
 be subsequently perform by a choir of thirty voice , the 
 solo be sing by Miss Kathleen Mayes , Miss Evans , 
 Mr. Charles Ratcliffe , and Mr. Turnbull . Mr. Kirby 
 accompany on the pianoforte and Dr. Smith conduct 

 MELBOURNE ( AUSTRALIA).—A special musical service 
 be hold on December 5 , in St. Paul ’s Cathedral , 
 when Beethoven ’s C minor Symphony and Brahms ’s 
 " german " requiem be perform under the conductorship 
 of Mr. Ernest Wood , the Cathedral organist . a large and 
 capable orchestra have be engage , and the service be 
 commence with a praiseworthy rendering of the Symphony . 
 for the Requiem , which follow , the Cathedral choir have 
 be largely augment , and , throughout the very difficult 
 chorus with which the work abound , acquit them- 
 self creditably , give abundant evidence of careful 
 rehearsal . the baritone solo be sing by Mr. Henry 
 Rofe , of the Cathedral choir , and the soprano solo in no . 5 
 be excellently sing by four of the Cathedral chorister . 
 the service conclude with Bach ’s a minor Prelude and 
 Fugue , perform by the assistant Cathedral organist , Mr. 
 R. J. Shanks . Mr. Ernest Wood , who devote much time 
 and pain to secure an adequate performance of these two 
 masterpiece , must be heartily congratulate on the 
 result of his effort . this be the second occasion upon 
 which , under his direction , Brahms ’s Requiem have be 
 give in the Cathedral 

 PENZANCE.—A very successful concert be give in St. 
 John ’s Hall , on the 5th ult . , by the Penzance Choral 
 Society . the work choose for this occasion be Sir 
 Arthur Sullivan ’s ' the Light of the World . " the oratorio 
 be admirably perform by both choir and orchestra , and 
 the solo be efficiently sing by Miss Mason , Miss E. 
 Rowe , Mrs. Cornish , Mrs. C. L. Taylor , Mr. W. Sampson , 
 and Mr. J. Trebilcock . Mr. R. White preside at the 
 organ and Mr. J. H. Nunn conduct with much ability . 
 during an interval in the performance Mr. and Mrs. Nunn 
 be present by Mr. T. B. Bolitho , M.P. , with a cheque 
 for £ 500 , a silver salver , a silver tea and coffee service , 
 also an album contain the name of the subscriber . 
 the presentation be make in recognition of the long and 
 valuable service render by Mr. and Mrs. Nunn to the 
 art of music in Cornwall 

 SALISBURY.—The member of the Cathedral choir , 
 assist by the Close Ladies ’ Orchestra , give a concert on 
 the evening of New Year ’s Day in the Choristers ’ school . 
 the programme include the , Symphony from Bach ’s 
 " Christmas " Oratorio ( part 2 ) , Minuet and Trio ( Boc- 
 cherini ) , and Elgar ’s Serenade , ' " ' Salut d’amour , " by the 
 orchestra . the following interesting selection of part- 
 music be sing : ' queen of the valley " ( Callcott ) , 
 " this pleasant month of May " ( Beale ) , ' ' the cloud - cap’t 
 tower " ( Stevens ) , ' ' who shall win my lady fair ? " 
 ( Pearsall ) , and ' ' the silver swan ' ( Gibbons ) . violin and 
 violoncello solo be contribute by Miss Carpenter and 
 Mr. J. L. Davis , accompany by the Precentor , Canon 

 the large audience . it be one of the aim of this Society to 
 p its perform a work by a british composer at each concert with 
 o the complete orchestral accompaniment . this aim , so far , 
 ine have meet with an extraordinary amount of support from the 
 good public , the attendance both at this and the first concert 
 d by be large than that at other in the town or neighbour- 
 John hood for many year past . the solo in ' ' Hiawatha " be 
 charmingly sing by Mr. Edward Branscombe , who also 
 nicon , contribute two song to the second part of the programme . 
 of St. other soloist be Miss F. Lane ( harpist ) and Mr. James 
 lerful , Edgar ( violinist ) . ' the miscellaneous selection include 
 t be some sixteenth century madrigal and Eaton Faning ’s 
 it be part - song ' * Moonlight , " unaccompanied ; but perhaps the 
 1 both most popular number be the March and Chorus from 
 duce " Tannhauser " ' and the Soldiers ’ Chorus from ' " Faust , " 
 nd of jg accompany by the orchestra . Dr. W. G. Eveleigh , to 
 and by jg whom the Society owe its existence , be the conductor , 
 every jg and Mr. G. Ely be the accompanist . 
 kettle . BETHESDA.—Mr . E. D. Lloyd give his annual concert 
 when at Bethesda Chapel , on the 8th ult . , when he be assist 
 y. the ff by Mrs. Lloyd and Mr. Emlyn Davies ( vocalist ) , Miss 
 worked — Louie James and Mr. W. R. Watson ( violin ) , and Mr. A. 
 about — § Corrison ( flute and piccolo ) . Beethoven ’s ' ' Egmont ' " ' and 
 distinct JJ Schubert ’s ' ' Rosamunde " ' overture be include in the 
 five in JJ programme and be play by the instrumentalist 
 ind be j name , supplement by Miss Eames and Mr. Morris 
 ; tothe # ( pianoforte ) and Mr. Lloyd ( organ ) . 
 modern BristoL.—Very successful performance of " little 
 Bo - Peep , " ' a fairy operetta for child by C. Egerton Lowe , 
 175 the be give at St. Michael ’s School - room , Two Mile Hill , 
 acreon # @ on December 28 and the rst ult . great pain have be 
 ylinder take in prepare the work , which be receive with 
 etheus much favour , encore be numerous . 
 o , " the Cowes.—An excellent performance of Sullivan ’s ' * golden 
 ars Mt Bi leend " be give before H.R.H. Princess Henry of 
 17 , bas Hi Battenberg , at the Victoria Hall , on the rith ult . , by 
 he te Northwood Band and Chorus , number together 
 had . yo . the orchestra ( lead by Miss Rutland ) perform their 
 rces ( tht hi xduous task with more than efficiency , and the choir 
 rich be Bi ieeme particularly happy in their work , especially in the 
 ischiitz two beautiful unaccompanied chorus . the soloist , Miss 
 roduced , Amy Sargent , Miss Lucie Johnstone , Mr. Henry 
 | I @ ® Bi Tunpenny , and Mr. Charles Tree , be all admirable . 
 i d I _ Mendelssohn ’s ' ' hear my prayer " precede the cantata and 
 ace , 80 " bi vas to have be sing before the appearance of the Royal 
 party , but , on learn that the motet be to be include 
 , reay . blin the programme , her Royal Highness arrive at the 
 vginning of the concert , in order to hear it . at the con- 
 dusion of the cantata , Princess Henry , who be Patroness of 
 he Society , and have select the cantata for performance 
 nthis occasion , express her gratification to the conductor , 
 5 . " Mr. Frederick Rutland . 
 o insert GLOUCESTER . — a very successful performance of 
 es fora -Sat ’s cantata " King Olaf " be give by the Choral 
 wr with @eiety , on the 23rd ult . , in the Shire Hall . it be the 
 ‘ additional tmmendable design of this Society to perform at least 
 wld help modern composition each season , and its rendering 
 = if this important work be distinctly excellent , the 
 e enous hoit singing with precision and confidence and the 
 Prarnishedggectestra ( lead by Mr. E. G. Woodward ) be thoroughly 
 1 " I have ticient . the solo vocalist be Miss Rosina 
 — lammacott , Mr. Henry Beaumont , and Mr. H. Sunman , 
 they kindly ill of whom be thoroughly satisfactory . the cantata 
 aa precede by Mackenzie ’s breezy ' Britannia ' " ’ 
 nformationg , ture , Mr. A. Herbert Brewer conduct , and 
 - in anygeetved hearty commendation for the successful issue 
 aa anyone ‘ te concert . 
 ee Hampton Wicx.—Mr . William Ratcliffe , who have 
 " Triiey . ently resign the post of organist of the Parish Church 

 ms , on the 18th ult . , the recipient of a handsome } 
 hantel - clock , which be present to he by the Vicar 

 WELLINGTON ( N.Z.).—Mr . Robert Parker , organist of St. 
 Paul ’s Pro - Cathedral , give his twenty - first annual concert 
 in the Choral Hall , on December 14 , assist by the Glee and 
 Madrigal Society and the Wellington Liedertafel , of which 
 he be the conductor . the former sing in admirable style 
 the madrigal , " fire , fire , my heart , " ' ' Matona , lovely 
 maiden , " and ' * when love and beauty " ( Sullivan ) ; also 
 a fine selection of modern part - song , which include 
 Parry ’s " Phyllis ' and Stanford 's ' ' Corydon , arise ! " " the 
 Liedertafel sing Myles B. Foster ’s fine ' ' ode to music " 
 for tenor solo and chorus of male voice . two talented 
 pupil of the concert - giver , Miss Barber and Miss Janet 
 Ross , play respectively the Romanza and Finale from 
 Mozart ’s d minor Concerto , and the Rondo from Weber 's 
 Sonata in C major . Dr. Kington Fyffe sing with much 
 refinement Purcell ’s ' ' knot song " and " when I be 
 lay in earth , " and Mr. John Prouse give a splendid 
 rendering of Tschaikowsky ’s ' ' Don Juan ’s Serenade . " 
 the concert be an unqualified success 

 WEYBRIDGE.—A successful concert be give at the 
 Village Hall , on the 11th ult . , by Miss Catherine Low . 
 the concert - giver play Chopin ’s Ballade in A flat , 
 Moszkowski ’s ' Caprice Espagnol , ’' Liszt ’s ' ' Rhapsodie 
 Hongroise , " no . 12 , and other piece , and be assist in an 
 excellent rendering of Beethoven ’s " Kreutzer " Sonata by 
 Mr. Rohan Clensy . the vocalist be Miss Hester Otway 
 and Lieut.-Colonel Craig 

 Woop GreEen.—The Alexandra Musical Union Select 
 Choir and Orchestra , comprise altogether about forty 
 performer , give a very creditable performance of ' " St. 
 Cecilia ’s Day " ( Van Bree ) , at Bradley Hall , on the 5th 
 ult . , under the direction of Mr. E. J. Deason . the solo 
 be sing by Madame Minnie Jones . the chorus , 
 ' * Brooks shall murmur " and ' " ' give way now to pleasure , " 
 and the choral , ' ' fragrant odour , " be especially well 
 sing , the second be encore 

 the MUSICAL TIMES.—FeEsruvary 1 , 1900 

 just publish . 
 a new edition of the finale to 
 BEETHOVEN ’s 
 choral symphony 

 tue english translation revise , and partly RE - write , 
 by 
 NATALIA MACFARREN 

