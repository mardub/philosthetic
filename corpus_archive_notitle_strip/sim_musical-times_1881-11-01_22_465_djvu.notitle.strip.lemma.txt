for any length of time maintain its vital power intact 
 | without the aid of its daughter , Music , who in turn 
 have become its foster - mother , or at all event may 
 be call upon in this capacity at any time 

 how infinitely , therefore , be it to be regret 
 when , with the predominant influence of a party , 
 which have increase immoderately the glittering 
 pomp of the Church , not disdain to admit into 
 it even theatrical element , these latter — i.e. , the 
 theatrical and superficial excrescence — have find 
 their way also into the music of the service . there be 
 a fesuitical style recognisable in music ; and he whose 
 taste have be form upon the eternally true and 
 classical in our art will discern even in Beethoven ’s 
 Grand Mass , as well as in Mozart ’s Requiem , the fact 
 that since the seventeenth century opera have in- 
 vade the Church , and that the strangely fastidious 
 impersonification of the saint of that time be like- 
 wise reflect in the character of the Church music . 
 such be the case as much in Germany as in the 
 country inhabit by latin race , and it be well 
 known to any one who have ever visit Italy that one 
 may hear the late operatic air resound from 
 the organ even in the majestic dome of St. Peter ’s 

 550 the MUSICAL TIMES.—Novemper 1 , 1881 

 string together of unconnected theme , phrase , 
 rhythm , and flourish ( i‘loskeln ) , which taker 

 singly be uninteresting and crude , and join in such 
 a soi - disant whole must make the impression of nun- | 
 chalant musical improvising , nay delirious raving ; 
 and while listen to such a ' Humoreske ' always 
 call to the lip the question , what be the meaning of 
 all this , what be ' the humour of it ' ? if the 
 seriously strive musician , who unfortunately begin 
 to compose when Beethoven have cease , may have 
 be induce by a dim feeling of the necessity of 
 go as far as possible out of the way of the all- 
 crush symphonist to take other path , this feeling , 
 as we can not help sece now , could not lead he 
 far than to first attempt — to weak beginning . 
 thus we see he in small composition , as in some 
 number of the ' Carnaval , ' attain the perfectly 
 adequate musical expression for the respective 
 ' programmatical ' superscription . but in large 
 work , even where the sonata form be abandon , 
 we look in vain for a more comprehensive programme 
 correspond proportionately with their size , that be 
 to say , for an indication at the head of they of the 
 poetico - artistic subject which be to be treat in 
 they . nay , even this abandonment of the sonata 
 form , as the classical fundamental form , be not 
 undertake in consequence of self - conscious artistic- 
 ally thoughtful reflection of the hard - press musician ; 
 it can appear to we only as an act of nebulously 
 indefinite , blindly grope caprice , which at once 
 go back to the sonata or symphonic form as soon 
 as Mendelssohn , who in this respect be certainly 
 far superior to Schumann , courageously set the 
 example 

 let we pause for a moment and consider the last 

 earnestness and energy , would have have at his dis- 
 posal intelligence and skill , could hardly have create | 
 out of such theme piece of music , much less work | 
 of art 

 to begin with the last and crowning assertion of 
 this heap of preposterous statement ; from what 
 insignificant seed - corn have many of the grand | 
 and most famous chef - dceuvre spring ! how the | 
 notion of " idea which fit neither in any of the 
 exist form , nor in any form yet to be discover , " 
 could be conceive by a being endow with reason 
 be a problem which I recommend for solution to the | 
 ingenious reader . and then , I can not help ask : 
 suppose that Schumann be not able to produce 
 anything but fancy ( Einfille ) , would it not be wise 
 to accept than to spurn they ? have not the com- 
 poser ’s aphoristic thought and few - line sketch , 
 loosely and fantastically as they be often string 
 together , as much raison d’étre as the reflection , 
 maxim , epigram , caractére , Xenicn , pensieri , & C. , 
 with which La Rochefoucauld , La Bruyére , Pascal , 
 Chamfort , Vauvenargues , Goethe , Schiller , Leopardi , 
 and other have enrich literature ? ' to be sure , 
 this be an inferior kind of art , or rather a minor 
 branch of art , and not to be compare to work 
 of develop reasoning , to many - membere , artisti- 
 cally construct organism . still , such flash of 
 the intellect , such iridescence of the fancy , such 
 throb of the heart , such furtive peep into human 
 life and character , be far from be worthless 
 and despicable . moreover , be there only one type 
 of beauty , and of this one type be only the 
 high degree of perfectness admissible ? but so 
 narrow - minded and so narrow - hearted a partisan as 
 Joseph Rubinstein can not be expect to have many 
 sympathy . 
 Schumann , but , with one exception , on all composer 
 that have appear in the world up to this time . 
 Brahms , Raff , and Goldmark be pelt to the , 
 utmost with mud and dirt by this gamin of | 
 critic . Haydn , Mozart , Beethoven , and Mendels- | 
 sohn be treat with somewhat more respect : 
 they be , as it be , card which may be plaved|a 
 with good effect against the above - mention small | 
 one , but which be sweep off the table as soon as the | 
 ace of trump , Wagner , turn up . the 

 Beethoven be merely the pedestal upon which 1 th 

 master of master rise in his majesty and sublimity . | 
 in listen to Joseph Rubinstein ’s stricture we shall | 
 do well to keep in mind that he reject all purely 
 instrumental music , absolute as well as programme 
 music , and advise the world to give up its long 

 above - quote weak and trivial italian song without 

 however , be weak or trivial themselves . be it 
 not for one note , 2 minor instead of a major interval , 
 the setting of the word * * Ingemisco tanquam reus ’ 
 from Mozart ’s * * Requiem " might be add as a third 
 example of the same kind . but rosalia occur com- 
 paratively rarely in this form . oftenest there be 
 only one repetition , and frequently the sequence be at 
 a low pitch or the modeis change . Handel 's * * how 
 beautiful be the foot " ( bar 7 and 8) from the 
 " Messiah " furnish an example of a phrase repeat 
 once , a tone high , and in the same mode ; Beet- 
 hoven ’s overture to ' ' coriolan , ' Op . 62 ( bar 15 , 
 & c. ) , of a passage repeat once , a tone low , and 
 in the same mode ; and the same composer 's * ' Sonata 
 appassionata , " ' Op . 57 ( at the beginning of the first 
 movement ) , of a passage repeat once , a tone low , 
 and in a different mode . 
 remind I that Beethoven be notable among com- 
 poser for the effective use he make of rosalia . 
 what can be more impressive than the weird and 
 mighty upheaval in the heroic 

 position take place from C minor to C sharp minor ! 
 it must be patent to the student of this master that 
 repetition have be carry far by he than by 
 any other composer . with what persistence he can 
 fasten upon a melodic or rhythmic motive without 
 become monotonous may be well learn from his 
 symphony — I mention especially the first move- 
 ment of the C minor and the Pastoral , and the 
 scherzo of the Ninth Symphony . in the heroic 
 Symphony the chief phrase of the first subject 
 appear in one place successively ( Joseph Rubinstein 
 would say rosaliter ) in e flat , f , d flat , b flat , and 
 e flat ( in the latter key several time ) . nor do 

 Beethoven confine himself to transposition ; he also 

 introduce freely and with wonderful effect repetition 
 at the same pitch . in the C minor Symphony ( bar 
 63 , & c. ) a four - bar phrase occur three time in im- 
 mediate succession 

 to place this question as to the force and expres- 
 | siveness of iteration and transposition in a clear light 
 | | can not do well than illustrate it by example draw 
 | trom a master highly esteem both by conservative 
 } and radical , and recognise by all as something 
 ; more than a mere composer , a contriver of more or 
 | less euphonious combination of sound — in short , as 

 a true tone - poet . here , then , follow an additional 
 number of rosalia ( take the word in the sense of 
 | the third and most comprehensive of the three defini- 
 [ tion above quote ) and literal repetition at the 
 same pitch cull from various work of Beethoven 's 
 write by he at different period of his life and 
 representative of all his style 

 sonata , op . 2 , no . 1.—the first phrase of the 
 } second subject of the first movement be once com- 
 | pletely and once partially repeat . in the working- 
 |out section the phrase appear in addition to the 
 |same number of repetition twice a tone high , and 
 |after that the melody be take up by the bass . the 
 } second half of the subject contain likewise a repeti- 
 [ tion of a four - bar phrase , and in the coda which 

 and less abusive appellation ) with which the Scherzo 
 of this sonata open put one in mind of the beginni 
 of the Andante favori 

 speak of the less piece of Beethoven ’s piano- 
 forte work , I would call the reader ’s attention to | 
 certain rosalia in the Bagatelles ( op.33 ) . the first 

 nw 
 AS 

 in the Heroic Symphony ( first movement , bar 
 134 , xc . ) an eight - bar phrase which first appear in 
 e minor be at once transpose to a minor . the same 
 passage occur far in ff and e flat minor . but | 
 who could help be enrapture by this ethereally | 
 lovely rosalia ? nor do I think it possible that any 
 be bear of woman should be so insensible as to be | 
 proof against the charm of the playful rosalia at the | 
 beginning of the Allegretto scherzando of the eighth | 
 symphony ( i major ) . as to the transposition from 
 a to d of the mysterious opening passage of the ninth 
 symphony , its effect be beyond the power of descrip- 
 tion : justice can not be do to it by ever so great 
 and choice an accumulation of adjective 

 I have already point out that the theorist as well 
 as musician be generally at variance as to what be 
 and what be not a rosalia . indeed , even by our 
 accept the most comprehensive of the three defini- 
 tion , we shail not escape from be again and again 
 bring face to face with this puzzling question . Tor 
 instance , would you call the second half of the first | 
 nine bar of Mozart ’s G minor Symphony a rosalia , | 
 notwithstanding the modification of the interval of | 
 the melody and the total change of the harmony ? 
 if you would , thename would lose its opprobriousness ; 
 for its opprobriousness be base on the monotony of 
 the thing thus call , and monotony can not exist where 
 there be variety . this defence of rosalia hold also 
 good in the case of repetition of melody at the same | 
 pitch , but with a different accompaniment and perhaps | 
 a slightly alter cadence ( see Beethoven ’s string 
 quartet , Op . 127 — the beginning of the Allegro ) . nay 
 even the mere change of the cadence be enough to 
 produce the desirable variety . it be one of the most 
 common procedure of composer to begin the second | 
 clause of a musical sentence like the first , give it | 
 only towards the end a different turn . in addition to | 
 the example in point to be find in the above note | 
 from Beethoven ’s work , and instead of thousand 
 more let the follow few suffice . Beethoven ’s | 
 sonata , Op . 10 , no . 3 , first movement , first subject ; 
 his Sonata , Op . 22 , last movement , first subject ; his | 
 symphony in a major , first subject of the Vivace and 

 romanticism 

 his passion for Gluck seem to have survive the 
 lapse of vear . 
 12 , 1856 ) he say 

 as for I , I shall never forget that your artistic 
 instinct have , without hesitation , recognise and 
 adore with transport that , for you , new genius . 
 yes , yes , depend upon it , whatever the man of half- 
 feeling and half - science , those who have only part of 
 a heart and a single brain lobe , may say , there be 
 two great superior god in our art , Beethoven and 
 Gluck . ' the one reign over the infinitude of thought , 
 the other over the infinitude of passion ; and , although 
 the first be strong above the second , there be never- 
 theless so much of the one in the other that these 
 two Jupiters make but a single divinity , in whom our 
 admiration and worship ought to be absorb 

 on May 23 , 1856 , we find the master write to his 
 friend Morel , entreat his good office for Louis 
 Berlioz , who desire to leave the imperial navy and 
 enter the merchant service . this letter contain 
 also a reference to his state of health , in which we 
 may see the beginning of the end 

 565 

 s ' Lauda Sion " ( in e natin , the frag- 
 ment from " Christus , " and music to " Athalie , " | 
 Haydn ’s " season , " Spohr ’s ' Calvary , ' Beethoven ’s 
 Mass in D and " Mount of Olives " ' ( with the original 
 libretto ) , Bach ’s " St. Matthew Passion , " Dr. Crotch ’ s 
 ' ' Palestine , ' Benedict 's * Legend of St. Cecilia , " 
 Costa ’s * " Eli ' ? and ' ' Naaman , " Macfarren ’s ' John 
 the Baptist , " and Rossini ’s ' * Moses in Egypt . " 
 but it could scarcely be expect that an institation 
 so perfectly organise -d as the Sacred 
 poe | would be content with merely appeal : 
 1e publ ic to support a series of concert every yea 
 to preach the cause of high - class sacred music out- 
 side , as well as inside , the wall of Exeter Hall become 
 anweh a duty , and a triennial Festival in honour ot 
 Handel be the legitimate offspring of this self- 
 impose task . gradually , however , the idea be 
 develop ; for on the centenary of the composer 's 
 death ( in 185 g ) , a performance of his work be 
 contemplate ( chiefly through the suggestion of the 
 indefatigable ' Treasurer , Mr. R. K. bi wh ) , a pre- 
 liminary Festival be hold two year before , which 
 establish the fact that , with the powwestal aid of the 
 Sacred Harmonic Society , under the experienced 

 Mendelssohn 

 hare in a creditable manner . the trio , * lift thine eye 
 bey Madame Albani , Miss Davies , oe Madame Pater , 
 create a marked effect . the accompaniment by band 
 and organ be very finely play throughout 

 in the evening a miscellaneous concert be give . the 
 chief feature be the magnificent playing by the band of 
 the Overture to ' " ' Oberon , " Beethoven ’ s Symphony in IF 
 major , the overture to ' ' Tannhauser , " Valse lente and 

 Wil 

 e thing for so young a composer . it seem to claim 
 nothing on the score of juvenile authorship , but be uncom- 
 promising in its pretension to rank with the chief of its kind ; 
 largely develop , ambitious in style and character , and 
 rigidly observant of classic form , while redundant in matter , 
 redundancy , in point of fact , together with a certain 
 reckless daring , be its principal failing . each movement 
 contain too much , episode be expand till they be 
 co - ordinate with the principal theme ; nor be the harmonic 
 wealth less profuse than the melodic . such exuberance 
 surprise , and to a certain extent gratify ; but the Concerto 
 lose through it in point of clearness , order , and the con- 
 ciseness that should always prevent the saying of a word 
 more than be necessary to logical and dialectic complete . 
 ness . we might have expect this , since in music , as in 
 other thing , ' youth will be serve . " with this , however , 
 we must recognise a mastery over the resource of art — 
 resource alike of fancy and expression — such as be very 
 rarely meet with in one so young . into the detail of the 
 work we purposely refrain from go . it must be hear 
 again before either description or judgment would be war- 
 rant ; but no little be say for Mr. D’Albert ’s success by the 
 fact that desire to hear it again be strong . the Concerto 
 be admirably play as regard the orches stra , but the solo 
 would have gain in the hand of a strong performer 
 than Mr. D’Albert , and by be e : cecute upon a well 
 instrument than be choose for the occasion . the com- 
 poser be loudly applaud after each movement , and 
 three time recall at the close , amid genuine excite- 
 ment 

 Beethoven 's Choral Symphony end the Concert , 
 generally speak , be render in well style than on 
 any former occasion , the chorus be thoroughly up to 
 its work and singe faultlessly . the solo , intrust to 
 the artist already name , have be hear to great 
 advantage . Saturday 's concert include in its programme 
 3eethoven ’s " Eroica ' ? Symphony and a selection from 
 the work of Richard Wagner 

 and 

 First Symphony , and Massenet ’s 
 ; to which be add an " ' arrange- 
 ment " for string of the little piece entitle * * Traumerei " ’ 
 in Schumann ’s ° « s Kinderscenen " — tho ugh the pretty 
 trifle should have be present save as the composer 
 write it be hard to discover . familiar as all these work 
 be , we need only touch upon the char acter of their per- 
 formance by Mr. Corder ’s small yet well - select orchestra , 
 e piece be play with great care , and in a style 
 that spo ! se no less for the skill of the executant 
 the ability of the conductor . pract moreover , 
 ide up for the limited number of the instrume : nts . i ote 
 aiteentoesn in the conservatory have turn the place fron 

 certo , Beethoven ’s 
 Scenes Pittoresques 

 i d into a good concert - room , and the ¢€ net . n of a 
 iinutive orchestra { s now pretty nearly all at can 

 o in 

 the concerto be ably execute by Mr. Oscar B eringer , 
 wh sympathy aid his executive ability in overcome 
 the difficulty of modern music . Vith Beethoven ’s 

 sentative of the noble — oi 

 al Hall on the sth ult . , the executant bei ng Messrs 

 Speelman , Bernhardt , and ae the 
 programme consist of Quartets Mozart in b flat , 
 Raff in d minor , and Mendels nin E minor . Raff ’s 
 fine but unequal work have not d here before . 
 the performance throt ighout be m « st admirable . Mr. 
 Hallé give his first cc incert of the season on the 27th ult . 
 the programme include Mozart ’s Symphony ( no . 1 ) 
 in D , Brahms ’s Academical Festival Overture , Beethoven ’s 
 Pianoforte Concerto in C , and Schumann ’s * Phantasie- 
 stiicke . " the vocalist be Madame Schuch Proska , from 
 Dresden.——During the season , in addition to the choral 
 work we announce last month , the follow instrumental 
 novelty will probably be introduce : Brahms ’s tragic 
 Overture , Berlioz ’ * * Romeo and Juliet , ' Symphony and 
 Overture to ' * Waverley , ' " Mozart ’s Ballet - music to ' Ido- 
 meneo , " Gluck ’s Ballet - music to " Paris and Helena , " 
 Raff ’s Symphony , " I m Walde , " ' Dvorak ’s Danses Slaves , " 
 Cowen ’s Overture ' " ' Niagara , " and Rubinstein ’s second 
 Concerto 

 he 

 MUSIC in LEIPZIG . 
 Leipzig , October 21 

 or the two musical association which hold the high 
 place in Leipzig , the less — that of the Euterpe Concerts 
 — have not yet begin ; but three of the more famous serie 
 of performance that take place weekly in the Gewand- 
 haus have already be give . the great boast of the 
 Gewandhaus director be that their orchestra be reserve 
 strictly for one single end , hardly any of its member be 
 suffer to degrade their artistic training by occasional 
 engagement of a light kind . res severa est verum gau- 
 dium be the inscription upon the cornice of the hall , and it 
 give the note and the character of its performance . the 
 Gewandhaus be consider as a sort of temple , whereof the 
 minister priest have before all thing to be keep pure , 
 and wherein , unfortunately , the initiate must be equally 
 select . for , owe to the smallness of the building and 
 the renown of the concert , it have become an absolute im- 
 possibility for any new comer to be sure of a place ; there be 
 hardly aseat that have not be subscribe for for year , and 
 for the vacancy of which probably there be several patient 
 applicant . consequently the rehearsal , which take place 
 at the early hour of nine in the morning , be crowd by 
 the student of the Conservatorium and the unlucky 
 multitude to whom the concert themselves be close . 
 distinguished from the Euterpe management , which incline 
 towards the work of Liszt , the aim of Herr Reinecke have 
 be to preserve in the Gewandhaus Concerts a definitely 
 classical character . thus the three performance hitherto 
 give have include Haydn ’s Oxford Symphony , Beet- 
 hoven ’s Pastoral Symphony , and the second ( d major ) 
 Symphony of Brahms — a work which , on repeat hearing , 
 assure one more and more of its author ’s right to be hold 
 as the successor of Beethoven in massive strength , in 
 mastery over the orchestral body , and in the creation and 
 solid manipulation of inspired melody . Herr Reinecke 
 have , however , always seek to be prompt in acknowledg- 
 e the worth of new composition which have the stamp 
 of artistic workmanship on they . accordingly , the classical 

 have only deviate from its high character by produce some 
 english glee . in popular estimation the weekly perfor . 
 mance at Bach ’s own church , St. Thomas ’s , retain their 
 position ; and nothing can excel the purity and precision 
 with which , for instance , the wonderful motett , ' the Spirit 
 also helpeth our infirmity " ( ' Der Geist hilft unsrer 
 schwachheit auf ’' ) , be render last Saturday . the 
 choir be still under the management of the learned editor 
 of the Bach - Gesellschaft , Wilhelm Rust 

 vice be hold in St. Mark ’s Church , Camberwell , it be 

 the Festival of the Harvest Thanksgiving . the ordinary 
 choir be augment by several gentleman from neigh- 
 boure choir . the Magnificat and Nunc dimittis be 
 well sing to Banks ’s setting in e flat , and the anthem 
 be Dr. Stainer ’s ' * ye shall dwell in the land , ' after 
 the blessing Sullivan ’s Te Deum in D be sing , the choir 
 be group round the altar . Mr. Alfred Physick , the 
 organist and choirmaster of the church , preside at the 
 organ , and play as conclude voluntary Beethoven ’s 
 " hallelujah 

 a Harvest Home Concert be give on Thursday , the 
 73th ult . , at Claremont Chapel Schoolroom , by some 
 member and friend of the King ’s Cross Senior Band of 
 Hope . Gounod ’s * Ave Maria " be the chief item in the 
 programme . song be also give by Misses Ethel 
 Harwood , F. Davies , and M. Tensh , Messrs. H. T. Probert , 
 A. Probert and W. H. Mason , vary with a pianoforte 
 solo by Miss Willcocks ( who also accompany during the 
 evening ) , a duet for piano and harmonium , violin solo , and 
 selection by the orchestra , all of which be well receive 
 by a numerous and appreciative audience 

 a series of four Concerts will be give by the Kilburn 
 Musical Association , under the direction of Herr Adolph 
 Gollmick , at the Town Hall , Kilburn , during the come 
 season ; the first , on Wednesday , December 14 , to be 
 devote to Handel ’s " ' Messiah 

 Mr. WaLter Bacur 's Pianoforte Recital take place 
 at St. James ’s Hall to - day . the programme be select 
 from the work of Beethoven and Liszt , and include 
 the latter master ’s ' ' Mephisto - Walzer . " Mr. Bache ’s next 
 Orchestral Concert be announce for February or March , 
 1882 , the programme consist of Liszt 's ' Goethe . 
 Marsch , " " ' Mephisto - Walzer " ' and ' " Faust ' Symphony 

 a concert be give on the roth ult . at the Mission 
 Room of St. Saviour ’s , Fitzroy Square . solo be sing 
 by Miss Filmore , Mrs. Frisby , Mr. Suter , and Mr. W. ) , 
 McLaren . Miss Nellie McEwen also contribute two 
 song with much effect . several glee be well render 
 by member of the choir of St. Edmund the King an 
 Martyr , Lombard Street 

 phous Canon " be particularly happy ; and the final 

 chapter , head ' ' hint to the student , " ' contain some 
 valuable rule for the construction of this species of com- 
 position . it be impossible to speak too highly of the judi- 
 cious manner in which the example have be select 
 throughout the work . it be almost needless to say that 
 very many have be take from the work of John 
 Sebastian Bach ; but the various extract from other com- 
 poser , which in every case most aptly enforce some special 
 point , be of the deep interest , and may perhaps even 
 have the effect of more strongly draw the attention of 
 the pupil to the composition from which they be quote . 
 one especially we may mention from Beethoven ’s Sonata , 
 Op . 28 ' popularly know as the ' Sonata Pastorale " ) , 
 which be too often pass over by mere " finger pianist " 
 without any thought of the passage be a really fine 
 example of double Counterpoint . amongst the canon 

 lfil 

 we approach the study of the " ' Nirwana " with the re- 
 spect due to the composition of one of the most eminent 

 any music which will help to make pupil sing on the 
 pianoforte must be of service ; and transcription of good 
 song , therefore — provide only that they do not degenerate 
 into finger display — should be always welcome , not only 
 for the useful practice which they enforce , but because they 
 make young instrumentalist acquaint with the standard 
 vocal work . in this set of three we have Haydn 's 
 ' ' Mermaid ’s Song , ’' Mozart ’s ' ' Violet , ’' and Beethoven ’s 
 " Mignon ’s Song " ( ' knowest thou the land ? " ' ) . all of 
 these , of course , do not lend themselves equally well to 
 " arrangement " for an instrument ; but Herr Eisoldt have 
 acquit himself of his task with much credit . it need 

 scarcely be say that the pleasing accompaniment to the 

 Mermaid ’s Song , " apart from the melodious character of 
 the theme , will render this the most popular number of the 
 three , but Mozart ’s beautiful vocal gem must also attract 
 young player , and there be sufficient variety in Beethoven 's 
 well - know song to interest even those who hanker after 
 ' pretty ' music . very little fingering be mark , except 
 in the ' ' Mermaid ’s Song 

 for the Pianoforte . by John 

 588 the 

 with reference to acontroversy recently set on foot in the 
 musical world , the Leipzig Signale remark ironically : 
 " " Mozart ’s Requiem be say to have be only just dis- 
 cover ; search be make after a symphony , by Franz 
 Schubert , as yet unknown . and now it be ask , what have 
 become of a tenth symphony and a second oratorio by 
 Beethoven ? the symphony , as be well know , be 
 bespeak and pay for in advance by the London Philhar- 
 monic Society ( Beethoven send his Ninth inits stead , after 
 its first performance at Vienna ) ; the oratorio have be 
 ask for by the Gesellschaft der Musikfreunde in Vienna , 
 and likewise pay beforehand : ergo , where be the two 
 work ? — R.S.V.P 

 we read in Le Ménestre ! : 
 the name of Pére Dupin , 
 author ' ninety - four year of age ' , 
 comic opera , entitle ' Ploch le Soldat , 
 rietti will write the music 

 net ) ; Kapsodie Hon 

 Beethoven ) ; First Suite d’Orchestre ( Ma 
 I ration of the Nouve 

 Ss . Overture , " Oberon " ( Weber 

 Leipzig.—Gewandh : : Fest- Ou iverture ( Volk 

 mann ) ; prelude and fugue , e minor ( Me taokn } 1 ) ; Pianoforte Con- 
 certo , no . 2 ( Sc arwenka ) ; ordanza " ( Liszt ) ; Pastoral Sym- 
 phony ( Beethoven ) ; Vocal soi i ( W sac Kirchner , ae 
 Schumann ) . Ger — idhaus Concert ( Octot 13 ): Overtu - 
 veva " ( Schumann ) ; Violin con to ( Gad " og 

 Handel ) ; " ence , no . 2(Brahr cal 

 Bourne , Linco NSHI A Violin and Pianoforte Recital wa 

 S 
 — Rooms , Angel Hotel , by Messrs. 
 I by Mr. F. Cundy , vocalist . the pro- 
 > work of Beethoven , Schubert , Mozart , 
 e Recital be well attend and highly 

 give on the 15th ult . i 
 Bertolle and Lewis , assis 
 gramme be select from 
 Schumann , Gounod , & c. 
 successful 

 nin the Colston Hall , and be very largely 
 n the principal item in the programme 
 ( no . 5 ) in C minor , W eber ’s Overture 
 to Euryanthe , Rossini 's to Siege of Covint h , and W 
 Overture to Tanuhduser . yd ’s ' ' Funeral March of a Marionette , " 
 Boccherini ’s Minuet in a major for muted string , and Meyerbeer ’ s 
 Coronation March ( Le Pop be also include in the pro- 
 gramme . the vocalist w ‘ ere Madame Evans - Warwick , and Mr. C , 
 Fredericks , Mr. Waite lead the band , and Mr. Riseley conduct . 
 — a Harvest Thanksgiving Service be hold at St. Nicholas 
 Church on September 29 , when the usual choir of the church be 
 augment by the choir of Christ Church , Broad Street , and St , 
 Peter ’s , Peter Street , number altogether sixty voice . the service 
 be Goss in a ( unison ) , and the anthem , " behold I have give you 
 every herb bear seed , " specially write for the occasion by Mr , 
 William Fear Dyer , the Or ; ganist and choirmaster of St. Nicholas , the 
 word be select from var ious passage of scripture by the Rev , Ti 
 G. Alford , M.A. , Vicar . the solo be sing by the member of St , 
 Nicholas ’ choir . after the se rmon , which be preach by the Rev , 
 Ambrose M. Foster , of Wilton , near Taunton , Dr. Stainer ’s anthem 
 " o clap your hand " be sing , the quartet be take by the member 
 of Christ Church . Mr. Brookes , Organist and Choirmaster of St. 
 Peter ’s and lay - vicar of the Cathedral , conduct the service . the 
 offertory be for the Church Missionary Society . — a Union of twenty- 
 eight congregational and baptist Choirs of the city have recently be 

 of the same series be giv 
 patronise . on this occasi 
 be Beethoven 's Symphon 

 agner ’s 

 Volumes i 

 ul s waltz . book 1 to 3 each 4 0 
 zar " ave » waltz . no . 1to3 = » £ ¢ 
 Beethoven ’ s v i to " ~ oi 
 to , as Duet s ¢ 0 
 angel ever br sin see 
 Der Lustig¢ " on 
 Ditto , Duet ... 4 0 
 Nazareth ( Gouno ei 4 0 
 silver be ! ls of memory ( Dale ) as oa 3 « oo 
 \ s his watery nest ( Hatton ) 4 0 
 Spohr ) ; ; one 3 0 
 us , 12th service ( Moz art ) o- yo wee ae 3.0 
 he Lord be mindful ( Mendelssohn 3 
 m. Stabat Mater . r = 4 
 Isis , 12th service ( Mozart 3 0 
 Chinen ette de Boheme ... = 3 0 
 Gavotte i in B fiat ( Handel 4 0 
 IY len 3 ) 
 st " ne ons oo 
 Nelson 's V ictory ( braham 4 0 
 the Harmonious Blacksm sith ( Hand 3 0 
 with verdure clad ( | Haydn ) ee 3 0 
 Caller Herrin ' 3 0 
 Freischitz ... ae 4 0 
 leil ies ena 4 
 service ( Mozart 3 0 
 s tlock ( Handel ) 2 6 
 ( Haydn pic : 3 oo 
 : ( Handel ) : ine aid eee 
 Cornaval de ve enis¢ Sa 5 bas ‘ iis pee wpe 
 the heaven be tell ( hi : 3 0 
 the Bridal March from * L , ie an ae 
 each of the above post - free for half- ' price in stamp . 
 also a cx omplete list of Mr. West ’s compo sition gratis and post - free . 
 London : KRovertT Cocks and Co » . » new Burlingt ston Street . 
 third edition . price be 
 ow to le arn to play the PIANO 
 WELL . by one who have TauGut himseecr 

 here , in the compass of fourteen page , be matter calculate to 
 produce well result than half - a - dozen year ’ teaching . " 
 HE ART of PLAYING at sight . by Oxe 
 WHo have TauGutT Himsetr . price r . 
 " the author believe his system to be infallible ; after a fair trial , 
 we must say we believe so too . " 
 ow to form and train a village 
 choir . by an OrGANISING CHOIRMASTER . price be . 
 HE harmonium , and how to play it . 
 price t 

 PIANOFORTE classic . 
 BACH . — forty - eight prelude and s. d.|MENDELSSOHN.—LIEDER OHNE WORTE 

 FUGUES . be Folio 6 0 ; cheap edition . contain book 7 and 8 . 
 BEETHOVEN.—SONATAS . new and complete | Svo , 102 page , cloth , gilt , 4 . 6d . ; paper cover 
 edition . edit by Agnes Zimmermann . MENDELSSOHN.—OVERTURES ( Soro ' . the 
 Folio , cloth , gilt 21 . 0 } only complete edition ea Folio , cloth , gilt 
 BEETHOVEN.—SONATAS . new and complete MENDELSSOHN.—OVERTURES ( Durr ) . the 
 edition . edit by Agnes Zimmermann . only complete edition ene Folio , cloth , gilt 
 8vo , cloth , gilt , 7 . 6d . ; paper cover 5 0 MENDEI SSOHN.—SYMPHONIES.S pa 
 HANDEL . — * MESSIAH . " arrange for Piano- a ee yop ve 
 Rene : Sindee Maetieald Pours ~ oa . 3 of complete edition os olio , cloth , gilt 
 ; 2NDELSSOHN.—SYMPHONIES ( Dver ) . the 
 MENDELSSOHN . — PIANOFORTE WORKS ee 
 ( include the Lieder ohne Worte ) . an entirely : y Res Aeon aad ilebaiee 
 new and carefully revise edition . Folio , 518 MENDELSSOHN . — * ELIJAH . " — arrange for 
 page . handsomely bind .. Cloth , gilt 21 0 ! Pianoforte Solo by Berthold Tours at 
 MENDELSSOHN . — PIANOFORTE WORKS MOZART.—SONATAS . new and complete edi- 
 ( include the Lieder ohne Worte ) . an entirely tion . edit by Agnes Zimmermann . 
 new and carefully revise edition . 8vo , 518 page Folio , cloth , gilt 
 Cloth , gilt , ros . 6d . ; paper cover 7 6 ! y¥QZART.—SONATAS . new and complete edi- 
 MENDELSSOHN.—LIEDER OHNE WORTE . | tion . edit by Agnes Zimmermann . 
 the only complete edition . contain book 7 | vo , cloth , gilt , 5 . ; paper cover 

 and 8 . elegantly bind . Folio , 145 page ( with Seek Noa ees ee gee oe etd 
 Portrait of the Composer ) .. i a .. a2 o| PLANO ! Onre ALEL MS.—Edited by . Berthold 
 MENDELSSOHN LIEDE > OHNE WORTE | tour . vol . I. , Bach ; Vol . II . , Handel . cloth , each 
 MENDELSSOHN.—LIEDER OHNE WORTE . | PIANOFORTE ALBUMS.—Edited by Berthold 
 the only complete edition . contain Books ae elas ' é 2 ses pe eae 
 tour . nos.1 , 2 , and 3 , Compositions by Bach 

