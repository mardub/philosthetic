FRIDAY EVENING.—Overture , ‘ ‘ Euryanthe ’’ ( Weber ) ; New 
 Cantata , ‘ ‘ Ballad Dundee ” ’ ( Charles Wood ) ; Violin Concerto 
 ( Stanford ) ; Songs Sea ( Stanford ) ; Overture , ‘ ‘ Lustspiel " ’ 
 ( Smetana 

 SATURDAY MORNING.—Symphony B flat ( Beethoven ) ; Mass 
 D ( Beethoven 

 SATURDAY EVENING . — ‘ “ ‘ Golden Legend " ( Sullivan ) ; 
 Sixth Chandos Anthem ( Handel 

 Let know shall sevf7e home 
 man creatures . individually wish 
 compare notes 

 day hour convenient possible 
 engagements , 
 friendship truth affection . © . B. 
 Wesley took fiddle 
 friend ( Jacob ) Chelsea order achieve 
 ‘ Triumph Burney Ignorance 
 Prejudice ’ unknown . Probably , 
 result eighty - - year - old historian 
 evinced Oliver Twistian craving 
 ! events Burney wrote Wesley 
 following funny farrago : 
 DEAR FRIEND 
 French Packet mind , 1 
 time think plan rehearsing 
 quips gqiddities great S. B. best 
 advantage , concerng wch cold 
 ( spite heat weather ) enumeration 
 difficulties , , occurred want 
 Room sufficient 2 large instruments equal force 
 magnitude ; & Time , day , justice , 
 enjoy effects learned ingenious arcana . 
 , allowing old adage , wch quoted , 
 : ‘ ‘ second thoughts best’’—instead 
 sending & Sigt Novello toa P. F. maker find 
 2 Instrumts equal magnitude , nicely tuned ; 
 examining little parlour keeping room ( 
 health & warm weather ) , find , unbe - littered , 
 w4 sufficient space 2 rate 
 Giants lie — & 
 thought sending & friend toa P. F. shop 
 trial 30 comzca/ pieces ( 
 learned , ingenious , & original productions Haydn , 
 Mozart , & Beethoven , said , ignorant 
 vulgar hearers ) , thought sweet 
 & precious self , performance w4 
 inaudible music spheres — intend 
 going open air . 
 , caught fresh cold , 
 2 decayed teeth upper jaw , 
 acute twinge inhale fresh air ; beg , 
 warm weather , performance 

 obtuse ea - shot , acquaint 
 Larve shall meet wth ( post obit . ) wonderful 
 wonders produced pen great S. B. 
 played , game a// fours , zealous 
 indefatigable Messts- Wesley Novello . 
 send Instrument , day , 
 days , & hours , end present month , 
 & hope sinister occasion new procrasti- 
 nation promised pleasure . C. B 

 M. A. B.—(1 ) find useful hints regard 
 octave playing books Mr. Franklin Taylor , 
 * Primer Pianoforte Playing ’ ( Macmillan ) ‘ Technique 
 Expression Pianoforte Playing ’ ( Novello ) . 
 octaves Chopin Polonaise flat ( Op . 53 ) 
 doubtless exacting weak wrist ; 
 extend pages music break , 
 bars change figure bars 
 octaves resumed remaining bars . 
 practise octave passages long time , 
 instead adopting ‘ short frequent ’ method . 
 discouraged — /erseverantia omnia vincit . ( 2 ) 
 special portraits appear Musicat TIMEs 
 sold separately journal . publishers 
 tell numbers require procurable 

 H. G.—-(1 ) authentic particulars concerning 
 - called ‘ Handel organ Edgware ’ found 
 article Dr. W. H. Cummings , entitled ‘ Handel Myths , ’ 
 appeared Musica TiMEs January , 1885 , 
 letters subject issues 
 following months . ( 2 ) music performed Beethoven 
 funeral arranged Vincent Novello , , 
 account obsequies , published Messrs. Novello , 
 ( 3 ) Shepherd Song Evening Hymn Merkel 
 published separately , Organ Album ot 
 composer 

 N. C. M.—It difficult obtain music 
 pupils London iis suburbs crowded 
 localised districts . Professional Diploma 
 help known practical outcome . 
 possible advantage pianoforte recital , 
 invite sundry friends 
 acquaintances , addition playing public 
 possible — fact , making known , course 
 time , sought 

 W. G. McNAUGHT 

 EDITION , REVISED . gg 
 . songs Fathers loved Irish Air . 
 ' 2 . Let hills resound oe " BRINLE y RICHARDS . 
 BEETHOVEN | 3- Long , longago .. se T. A. BAYLEY . 
 | 4 . lass Richmond Hill James Hook 

 

 CLOTH , GILT , SHILLINGS 

 T recognise , smallest hesitation , 
 important valuable recent contributions musical litera- 
 ture . best informed professional musicians learn 
 great de al master - works Beethoven Sir George 

 Grove , wide reading acute perceptiveness enabled | 
 marshal astonishing array facts , intimate 

 satisfied remarks , earnestly recommending 

 recognise Beethoven greatness shown immortal 

 Symphonies obtain Sir George Grove volume , walk 

 Czerny.—In D minor , Op . 740 , . 37 ( Franklin Taylor ’ . s 
 Studies , Book 15 ) ae m Bi 
 MENDELSSOHN.—L fed Ohne Worte E ‘ flat , Op 67 , ‘ . 1 
 ( . 31 ) .. ee ee 
 TSCHAIKOVSKY . — Valse nA flat , Op . 40 , . 8 
 LOCAL CENTRE.—ADVANCED GR : ADE . 
 Bicu.—Fugue C minor . . 2 Book 1 ( Peters , 1a ) j 
 CLEeMENTI.—In D , 147c , Vol . 3 

 . 87 Gradus ( Peters , 
 ( Franklin Taylor ’ s Studie 3s , Book 22 ) . 
 BEETHOVEN.—Sonata C minor , Op . 10 
 ment “ 
 SCHUMANN.—Novelette F , Op . 21 , No.1 
 Bacu.—Fugue G , . 15 Book 2 ( Peters , 11 ) | 
 STEIRELT — F minor , Op . 78 , . 24 ( Franklin Taylor s 
 Studies , Book 15 ) ’ 
 BRETHOVEN . — Sonata , Op . : 4 , . 2 
 Czerny.—In minor , Op . 740 , . 
 Studies , Book 4 ) : 
 Bacu . — Fugue F shé arp , " . 13 Book 1 ( Pe ters , TA ) . 
 BEETHOVEN.—Scherzo ( Finale ) , trom Sonata G , Op . 14 , No.2 
 SCHOOL EXAMINATIONS.—ELEMENTARY . 
 ALoys ScuMitt.—Preparatory Exercises ( far . 20 ) , 
 ( Peters ) 
 Czerny — C , 
 Book 5 ) . 
 Bertini.—In iC ; Op . 
 Book 5 

 SCHOOL EX . AMINATIONS . = " OWE R DIN ISION 

