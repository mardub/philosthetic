that 1 was t 
ditional

size has caused them to be closely imitated by various makers ; and, in 
one instance, the name “ Beethoven Grand,” which I py cue gd used 
asadistinctive trade mark, but have since abandoned, has

ed

EVENTIDE. 
MIDNIGHT. A Gondola Song

s. d. 
No. 1. HENRY VII.’S CHAPEL ... aa ide EQ 
Beethoven's “ Hallelujah,” from Mount basen Olives. 
2. ST. GEORGE’S tha EL.. 3 0 
Jackson's Te Deum i 
3- ST. PATRICK’S CATHEDRAL . 3 0 
Mendelssohn's ‘‘ Come, let us worship.” 
4- MUNICH CATHEDRAL ... 3 0 
Spohr’s “ Last Judgment.” 
5. FREIBURG CATHEDRAL 3 0 
Travers's “ Ascribe unto the Lord.” 
6. nnEee CATHEDRAL .... o

mith’s “‘ How beautiful upon the mountain

utterly prevented the possibility of his singing three 
consecutive notes in tune

Now it will no doubt be said by many that such 
untaught amateurs as I have described have passed 
away; but those who dive beneath the surface will 
see that they still exist, although in a transitional 
state. True, they have felt the force of the musical 
educational movement which has steadily progressed 
for so many years, but “a little knowledge is a dan- 
gerous thing ;” and, failing to reach true art, yet 
dissatisfied with their former ignorance, they effect a 
compromise, and content themselves with “ arrange- 
ments.” Do we not see in the programmes of 
“ Penny Readings,” for instance, Mendelssohn’s Con- 
certo in G minor appear, and does it not turn out to 
be a hash formed by bits of the movements, with a 
little seasoning added by the musical cook? Do we 
not hear at private parties choruses turned into 
songs, and songs into choruses; movements from 
Beethoven’s Symphonies taken as subjects for bal- 
lads, and other distortions from original works, which 
could only be tolerated by listeners whose artistic 
feeling is not sensitive enough to experience a shock 
at such desecrations? Have not words been put tq 
Mendelssohn’s songs without words, and have not 
these exquisite little gems been published in a so- 
called sacred collection, with symphonies patched on 
in various parts by the arranger? Do we not see an 
“ arrangement” of a movement from a Symphony, 
which is not the movement at all, but only just as 
much of it as may be thought will sell to amateurs 
who know no better? Yes, the young lady who 
plays her own bass and variations, and the gentle- 
man who sings a “‘ good song,” are still amongst us ; 
but they call themselves musicians now; and, 
although illumined with but the faintest glimmer of 
the divine light of art, they fancy that they are 
basking in the full mid-day sun

Everybody knows that wherever there is a demand 
there will be a supply; and there can be no wonder, 
therefore, that as long as these half-formed artists 
abound, they will be well provided for. But is it not 
somewhat criminal to allow such garbled versions of 
standard works to grow up around us without a word 
of discontent? Musicians, of course, do not want 
them ; but apathy on the subject becomes selfish- 
ness; for it is the duty of all who would aid the pro- 
gress of art not only to pass by such productions 
themseives, but to use their best endeavours to pre- 
vent others from purchasing them, by leading them 
to an appreciation of the true works of art and 
exposing those which are false. When professors 
teach them without a murmur, and critics review 
them without a protest, a positive harm is done, for 
this tacit recognition of their claims is accepted as a 
proof that, although manufactured for amateurs, they 
are not unwelcome to artists, and thus they flourish 
by the patronage of one class and the toleration of 
another. If those who hold the educational power 
within their grasp would but be true to themselves 
and their art, such ‘‘ arrangements” as I have men- 
tioned would be speedily banished from our drawing- 
rooms ; for, to the credit of many who perform these 
works, I may say, from positive experience, that, 
like children, who cling to a love for their nursery 
tales, they do so from ignorance of the real treasures 
in art which lie ready for them outside their prison 
walls

CRYSTAL PALACE

At the concert of the 16th ult. (the first after the Chm 
mas vacation) Rubinstein’s Overture to his Opera, “Dimi 
Doskoi,” was the novelty; but we question whether 
work so disconnected and diffuse in treatment can 
the sympathies of those who at the same performance ha 
Beethoven's Symphony in A, and even Mendel bn 
juvenile Overture, ‘‘ The Wedding of Camacho,” p 
before them. Schumann’s Pianoforte Concerto in A} 
on the whole well played by Mr. Oscar Beringer ; hissty 
however, being perhaps more acceptable to those who. 
more sympathetic than ourselves with the “higherde 
ment’’ school. The vocalists were Mr. E. Lloyd and Mis 
Sophie Lowe. On the 23rd ult. Herr Wilhelm) playel 
Mendelssohn's Violin Concerto with an effect quite: _ as t

quisite singing ; and amongst the orchestral pieces a We 
come item was the Symphony in G minor of Sit 
Bennett, a work which we are glad to see making its Wf 
to the fame it deserves. Mr. Manns conducted the tot 
certs with his usual efficiency

ROYAL ALBERT HALL CONCERTS

The new series of these concerts commenced on thedl 
ult., with a band increased and improved, and a prograill 
both of vocal and instrumental music, in the highest@ 
gree attractive. The quality of the orchestra was 
and successfully tested in the Overture to “ Guill 
Tell,” Beethoven’s ‘Pastoral Symphony,” G. A. 
farren’s Overture to “ St. John the Baptist” and Wage 
* Kaiser-Marsch,” all of which, under the intelligent 
ductorship of Mr. Barnby, were finely played, the “Pastore 
Symphony,” indeed, being rendered with a frecision # 
variety of colouring which reflected the utmost credit™ 
all concerned. The re-appearance of Herr Wilheim 
eminent violinist, was an event of much interest, 408% 
performance fully proved his right to take rank 
the most accomplished artists of the day. His 1 | 
Mendelssohn’s Concerto was in every respect thomis” 
satisfactory. The first movement—whichis, in ouropi gc 
too often hurried through, even by the greatest viol 
was given with a steadiness and perfect commam

ji

THE result of the Christmas examinations held by the 
Church Choral Society and College of Church Mogg: 
London, is as follows:—Senior Choral Fellows; M, Bi 
Foster, Jesus College, Cambridge (by competition); W.. 
Birch, Ch.F., Amersham Hall School, Reading. Choi 
Fellow : F. G. Cole, St. Mary’s, Staines. Choral Associates: 
(2nd class), J. W. Wilson, Junr., Holy Trinity, Upper 
Tooting; (3rdclass), G, H. L. Edwards, Poplar. H 
Prizeman, F. G. Cole, St. Mary’s, Staines. Examinerg.— 
Sectiona, the Warden and H. J. Stark, S.C.F.; Section$, | 
Gordon Saunders, Mus. B., Oxon., S.C.F.; Section ; 
Walter H. Sangster, Mus. B., Oxon., S.C.F.; Section d 
Edward Dearle, Mus.D.,Cantab.; Registrar, W. J. Jennings, 
B.A., Cantab., S.C.F

On the 13th ult., a concert was given in the large Schoo. 
room of Chelsea Congregational Church, by Miss 
Johnson, Fell. Coll. Org. (Organist of the Church), in aidg 
the Debt Liquidation Fund. Miss Maria Langley and My, 
Alfred Rutland were the vocalists. A feature at the coneer 
was the appearance of Madlle. Franziska Friese (of Berlina 
solo violinist, and Herr H. Véllmar as solo pianist, bothd 
whom met with the most enthusiastic reception, the: 
performance of movements from the: “* Kreutzer Sonata’ 
(Beethoven) being encored. A choir of forty voices, 
conducted by Mr, H. Evans, sang some part-songs with 
good effect. There was a large audience, and the concet 
was most successful

REVIEWS

Short Voluntaries for the Organ, arranged by John Hiles* 
Vol. 4

Tuts volume of 180 pages concludes a work of great 
practical usetochurchorganists. Its contents are selected 
from the works of J. André, A. W. Bach, J. S. Bach, J. 
Barnby, E. Batiste, J. Battishill, Beethoven, Sir J. Benedict, 
A. P. F. Boély, Oscar Bolck, Carissimi, J. B. Cramer, Dr. 
Crotch, Dussek, Dr. Garrett, Gluck, Sir J. Goss, Ch. 
Gounod, Handel, Hasse, Haydn, Dr. H. Hiles, Dr. F. 
Hiller, E. J. Hopkins, Kalkbrenner, R. Keiser, Max Keller, 
Koseluch, Léfebure-Wély, Rev. H. F. Limpus, Dr. J 
Mendel, Mendelssohn, G. Merkel, Mozart, Rameau, €. 
Reinthaler, W. Russell, F. Schneider, Schubert, Spohr, 
Dr. Stainer, A, S. Sullivan, E. H. Thorne, W. Walond, 
Dr. S. S. Wesley, and, we presume, the editor himself, to 
whom we ascribe the two Voluntaries which appear 
anonymously. They consist of German Chorales, English 
Hymn-tunes, Bourrées, Choruses from oratorios, Songs 
from operas, movements from symphonies and sonatas, 
offertories and other pieces. With reference to the word 
‘short ” in the title, it is expressly stated that no piece in

a

Price 2s. 6d. each; or in scarlet cloth, 4s

NOW READY. 
BEETHOVEN’S FIDELIO 
(With German and English words

With the two great Overtures as usually performed; being the only 
Piano Score that has been published agreeing with the Origina 
Score as to the notes and signs for phrasing

We agree fully in the principle here enunciated, and

ugh contrary to more general practice, can feel no 
feason for identity of key in the separated pieces of a 
Church Service, far less in those of the Morning and 
Evening Services. In Beethoven’s Mass in C, the Sanctus 
%$in A; and in his Mass in D, the Credo is in B flat; and 
What holds good in the Roman Service cannot be musically 
@ fault in the Anglican, while what has the authority of 
this mighty master must hold good everywhere. The 
MStances quoted by Mr. Young (of course from the 
Messiah) are all in such closely-related keys that the 
“Mansition from any one piece to that which follows it hasa 
tHectly satisfactory effect, and induces an agreeable

variety; those quoted from Beethoven are in analogous 
but less closely related keys than Handel’s, and being 
divided from the other portions of the composition by inter- 
vening matter with or without music, startle not by their 
diversity, though they charmingly relieve the monotony, 
that is to some extent a consequence of a whole Service 
having one chiefly prevailing key. We only contend, but 
this more for the sake of conscience than of effect, that it 
is preferable for the several numbers of one composition to 
have some tonal affinity, to their being in various keys 
that have either the remotest relationship or no relation- 
ship at all. The nicest of ears are unshocked by the tonal 
diversity of the several pieces in an opera which are 
separated by speaking ; where separated by recitative, the 
modulations in this lead satisfactorily from the key of one 
piece to that of another, which is not always the case with 
the chanting and intoning in a Service; and what is 
musically agreeable in a theatre cannot be offensive in a 
Church. 
THE WRITER OF THE ARTICLE

TO CORRESPONDENTS

DorcHEsTER.—The Vocal Association gave a performance of the 
first part of Elijah on the 23rd Dec. The solo vocalists were Miss 
Matilda Scott (soprano), Miss Margaret Hancock (contralto), Mr. 
Wallace Wells (tenor), and Mr. J. Lander (bass); hon. accompanists, 
Miss M. Wills Lock and Mr. W. Gregory, pupils of Mr. Boyton 
Smith. The parts were most evenly balanced, and the whole ot the 
choruses went exceedingly well. Mr. Boyton Smith conducted

EpInBuRGH.—The fifth of the series of concerts now being given by 
the Choral Union took place on the 4th ult. in the Music Hall, and 
attracted a larger audience than any of its predecessors. There wasa 
band of fifty performers, conducted by Mr Adam Hamilton, with Miss 
Agnes Drechsler-Hamilton for solo violinist,and Madame Campobello- 
Sinico for solo vocalist. The programme was well selected. Sir 
W. Sterndale Bennett’s charming overture, ‘The Naiades,” and 
Beethoven's ‘‘ Eroica” symphony, were done full justice to by the band. 
Miss Agnes Hamilton was highly effective in her violin solo “ Airs 
Hongrois’’ (Ernst), and Madame Campobello-Sinico had an extremely 
cordial greeting on her entry, and her rendering of Beethoven's highly 
dramatic scena “Ah, perfido,” and some lighter songs, gave the 
highest satisfaction to the audience. ——PRoFESSOR OAKELEY gave an 
Organ Recital on the 14th ult, toa crowded audience in the Music 
Class-room. The attendance of students especially was very large. 
Loud applause followed the performance of each number, and several 
pieces were redemanded, but Dr. Oakeley responded in only one in- 
stance by repeating Handel's Gavotte

Gotcar.—On Thursday evening, Dec. 24, the Choral Society, 
assisted by members of the Hudderstield Choral Society, gave a 
concert, consisting of a selection from Handel's Messiah. The prin- 
ag artists were Mrs. Barras, Messrs. Townend, Lunn, and Stocks. 
Mr. J. E. Pearson presided at the harmonium, and Mr. H. Pearson 
conducted. The performance on the whole was satisfactory

LiverPoo_.—The second Soirée of the season, under the auspices ot 
the Jewish Choral Society, took place on New Year's Eve, in the 
Meyerbeer-hall, and proved, like all its predecessors, a very enjoyable 
re-union. Amongst the principal items of the musical portion of the 
entertainment were a duo concertante (violin and pianoforte), excel- 
Jently played by Mr. James J. Monk and Mr. Emmanuel Nelson, the 
honorary conductor; a pianoforte solo, the composition of Mr. Monk; 
an aria of Donizetti, by Mrs. E. Nelson; and several concerted 
pieces, which were admirably rendered by the Jewish Choral Society. 
A new four-part song, entitled “I met my love,” composed by Mr. 
Monk, was also sung with success. ——-THE first performance of the 
present series of concerts, on the plan of the London Monday Popular 
Concerts, which was given in the Philharmonic Hall on Wednesday the 
6th ult., was, as usual, most admirable, and highly appreciated. The exe- 
cutants were, first violin, Mons. Sainton; second violin, Herr L. Ries; 
viola, Mr. Zerbini ; violoncello, Signor Piatti; solo pianoforte, Miss 
Agnes Zimmermann; vocalist, Mr. Santley ; accompanist, Mr. Zerbini

The programme comprised the quartett in E flat major, Op. 44, No, 
for two violins, viola, and violoncello (Mendelssohn); song, “ Tre geal 
son che Nina” (Pergolesi); Sonata pianoforte in C major, Op. 2, No.3 
(Beethoven); songs, ‘‘ Dein angesicht” and ‘‘Widmung” (Schumann), 
two “ Stiicke im Volkston,” from “ Fiinf Sticke im Volkston ” (Sch. 
mann); song, ‘I pray thee by the godsabove” (Alwyn) ; and the'trio iq 
Cc pine, Op. 66, No. 2, pianoforte, violin, and violoncello (Mendel. 
sohn

MANCHESTER.—On the 28th December the Shelley and Old Glos 
hand-bell ringers contested at Belle Vue Gardens for the championship 
of Engiand, and a prize of £50 offered by Messrs. Jennison. The 
Shelley band numbers, including the conductor, eight ringers, the Old 
Glossop twelve, though the former rings quite as many bells as the latter, 
The Glossop ringers’ selection comprised the Kyrie and Gloria (Mozart) 
Haydn's No. 1 Symphony, and a Rondo Brillante. Their opponents 
also played Haydn’s First Symphony and Haydn’s D Symphony, 
and the overture to Zanetta. The judges were Mr. C. Warwick 
Jordan, Mus. Bac., Lewisham, Kent; Mr. L. Goodwin, Organist, 
Church of the Holy Name, Manchester; and Mr. W. J. Young, pro- 
fessor of music, Manchester. These gentlemen decided, after a con. 
test lasting two hours, that the prize should be awarded to the She! 
ringers, though they expressed their opinion that the arrangement and 
execution by the Old Glossop band of Haydn's First Symphony were 
superior to those of the other band

SurewspurRY.—On Tuesday evening the roth ult. Mr. Boucher’s 
fourth subscription concert of classical chamber music was given in 
the Assembly Room at the Lion‘ Hotel, to a select and fashionable 
audience. The artists were Miss Watkis (vocalist), Mr. J. H. Thomas, 
Mr, W. Cover, R.A.M. (violinists), Mr. T. Watkis (viola), Mr. J. B. 
Boucher (violoncello), and Herr John Weingaertner (pianist). Miss 
Watkis gave a clever rendering of two songs, the latter one, ‘ The 
green trees whispered” (Balfe) eliciting anencore. Mozart's duet for 
violin and pianoforte, in D minor, was played with great taste and 
correctness by Mr. Cover and Herr Weingaertner, and the same com- 
poser’s quartett in B flat major was also admirably rendered. The

me was brought to a termination by a very clever perform- 
ance of Beethoven’s quartett in E flat major, for pianoforte, violin, 
viola, and violoncello

SovrHport.—On Friday evening the 15th ult. M. Alexandre Guil- 
mant, the celebrated organist of the Church of La Trinité, Paris, gave 
aRecital on the grand organ in St. Peter’s Church. The largeaudience, 
which included organists and organ connoisseurs from all parts, 
testified the interest excited by the visit of a man whose compositions 
Mr. Best some years ago was the means of introducing into this 
country. The programme was well selected, but the majority of the

Uney, GLoucesTERSHIRE.—The first concert of the Glee Society 
_ on Wednesday the 30th Dec. in the National School-room. 
The Merry Men of Sherwood Forest, an Operetta, by W. H. Birch, had 
the place of honour in the programme, and was exceedingly well 
adored. Mrs. Hudson presided at the piano with great efficiency. 
The local performers were assisted by Miss Wilson, of the Gloucester 
Choral Society, and Messrs. Hunt and Waddams, of the Cathedral 
choir, who also contributed some songs. The choruses generally were 
well rendered, and showed that Mr. Leach, the organist of the 
charch, has devoted considerable time and labour to the training of 
the choir. A prologue and an epilogue, apropos to the occasion, 
Written by a lady in the village, were exceedingly well recited by the 
Rev. J.C. Hudson. 
‘Wetiuncron.—An amateur concert was given at the Town Hall on 
2gth December, on behalf of the Wellington and Rockwell Green 
and Infant Schools. A well selected programme was excellently 
ed, a great feature being the meat singing. A Christmas chorus, 
Up, brothers, up,” (Calkin) “‘O hush thee, my babie” (Sullivan), and a 
r chorus with solo, ‘‘ We'll gaily sing and play” (Pinsuti), 
Creating much effect. The instrumentalists mustered in good numbers, 
and i cluded two professional gentlemen: Mr. Richardson, of Bristol 
(Violinist), and Mr. Cheek of Taunton (Aautist

Warrcnurcn.—On Wednesday evening the 6th ult. a pianoforte 
Violin Recital was given at Apsley House (by the kind permission 
Steer), in aid of the Whitchurchschool fund. The artists were 
Squire (pianoforte), Mr. Squire (violin), and Miss Seymour 
ist). The programme comprised music of the highest class, 
Bverks of Beethoven, Mozart, and Mendelssohn, all of which 
Were well rendered. There was a large attendance

Winpsor.—A musical entertainment illustrative of John Bunyan’s 
's Progress was given in the Iver Church School-room on the 
ult, for the benefit of the Sunday schools, by the Colnbrook Choral 
numbering forty voices, under the conductorship of Mr. R. Rat- 
he Rev. S. Ward, the vicar, presided, and the Rev. Mr. Oliver

Master, Rev. T. H. Taytor. Musical Instructor, E.G. Monk, Esq. 
The School receives Boarders and Day Boys. A superior Musical 
and Middle-class Education, Terms for boarders, £30 per annum ; 
for day boys, £6 6s. per annum. Prospectuses sent on application to 
the Master, Minster-yard, York

CHUBERT SOCIETY, Beethoven Rooms, 27

Harley-street, W. President, Sir Julius Benedict. Founder 
and Director, Herr Schuberth. NintH Season, 1875.—The Concerts 
will commence the 24th February next. The Concerts ot the Society 
afford an excellent opportunity for rising Artists to make their first

