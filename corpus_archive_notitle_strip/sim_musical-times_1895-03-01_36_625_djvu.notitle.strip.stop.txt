MUSICIAN send SPECIMENS ( free ) DINHAM , 
 BLYTH CO . MUSICZAUTO - LITHO PRINTING . Anthems , 
 Kyries , Services , Songs , & c. , printed ( black ink ) , - 
 upwards , trifling cost . 28 , Fenchurch Street , London . Estab 
 lished 1872 

 X J ANTED , SET STRING QUINTET ACCOM- 
 PANIMENTS Beethoven Pianoforte Concerto ( . 3 ) 
 C minor , Op . 37 . G. Wilmot Cooper , Belmont Street , Oldham 

 IOLIN STRINGS.—Send od . samples 
 best ‘ E ” Strings . Alphonse Cary , Clapham Junction , S.W 

 Courvoisier ‘ Technics . ’ , perchance , ran 
 concerto violin bagpipes , Max 
 Vogrich . went mad 
 playing Kreutzer Sonata Karl Klindworth . 
 pianist . case madness 
 followed attempt play Beet- 
 hoven sonatas single sitting . 
 gone crazy combination Chopin 
 Wiirzburger Hofbrau . 
 accompany Scalchi . written 
 concerto . fatal writing 
 concerto . ” explanations , mutatis 
 mutandis , hold good water ; 
 readily suggest 
 persons desirous establishing connection 
 insanity musical studies . example , 
 surprised hear musician 
 having lost reason attempt understand 
 mixed metaphors Latterday Pooh Pooh , 
 reconcile conflicting statements 
 Pellmellikoff Gazetzky 

 Beauty fade , seasons fly , statistics , 
 modern poet tells , die . 
 drawback general futility . earthly 
 purpose served informing people 
 postman gone rounds daily 
 years effect journeyed round world seven- 
 teen half times , number flies 
 killed London restaurant hot summer 
 , carcases laid straight 
 line , reach Land End John o ’ Groats ? 
 statistical time - wasters , Wag- 
 nerian boot , gravely published number 
 bars contained act Wagner 
 operas — howling wilderness figures . 
 wished employ time purpose 
 compiled list 
 curious instructive . Let 
 requisite‘time patience command thou- 
 sand bars common time , , representative 
 works Mozart , Beethoven , Wagner , 
 like ; let figure distinct 
 changes key , 
 changes harmony ; ( calcu- 
 lating crotchets ) tonic harmony , dominant 
 harmony , forth ; common chords , 
 dominant sevenths , diminished sevenths , . 
 result striking comparison 
 old new musical methdds . offer 
 valuable suggestion free gratis statisticians 
 want object expend 
 unhappy powers 

 esteemed collaborator , Mr. J. S. Shedlock , 
 hard work time “ History 
 Pianoforte Sonata , ” shortly 
 published Messrs. Methven Co. , 
 course , traced evolution important 
 instrumental form ; development Pianoforte 
 Sonata present day Clavier Sonate 
 seventeenth century kept view . 
 portion deal chiefly Kuhnau , 
 Emmanuel Bach , Scarlatti ; works 
 Corelli , , earlier composers , 
 certainly prepared way men , 
 forgotten . epoch , course , 
 Haydn Mozart ; glad know 
 Mr. Shedlock proposes deal 
 writers Sonatas eighteenth century 
 names little known 
 students . composers interesting 
 experiments regard contents grouping 

 movements — experiments , 

 probable , Beethoven largely availed . 
 Sonata Beethoven — , , 
 Schumann , Brahms , Chopin — possibly 
 Sonata “ Future ” form , , 
 think , interesting section , 
 volume long wanted ; hitherto 
 comprehensive essay kind available 
 Dr. Parry Grove Dictionary 

 LIVELY series ot articles ‘ ‘ Traditions ” 
 appearing columns Le Ménestrel 
 pen M. Montaux . false traditions 
 true , chiefly 
 writer concerned . regard costumes 
 operatic stages tells curious anecdotes , 
 states Gounod “ Mireille ” 
 produced , Madame Carvalho insisted appearing 
 costume Swiss peasant — proved 
 coloured prints existence — 
 considerable time elapsed prima donna 
 ventured disregard “ tradition ” dress 
 picturesque garb daughters 
 Arles . time , M. Montaux pleads 
 vigorously certain latitude regard costume , 
 protests tradition con- 
 demns Mephistopheles red carnival dress 
 Valentine equally inevitable - . M. Maurel , 
 remembered , defied tradition 
 seasons gave grey Mephistopheles , 
 Madame Nuovina violated tradition 
 appearing black - haired Marguerite . 
 certainly said favour latitude 
 advocated M. Montaux — tends lend 
 element freshness surprise works 
 danger hackneyed 
 invariably mounted scenery 
 dresses 

 Edinburgh correspondent , Mr. Charles Smith , 
 writes concerning suggestion series 
 notices dealing lives eminent English 
 composers Church acceptable 
 readers Musica Times . remarks : “ 
 suggestion widely known , 
 hopes approvers come forward . 
 end interesting information concerning 
 great men , better 
 knowing . ” Mr. Smith good hope 
 task . 
 matter overlooked , 
 , pressing obligations 
 discharged , shall receive attention 

 found notable sentences York- 
 shire paper containing criticism Paderewski 
 Bradford Recital . ‘ playing Beethoven 
 Sonata ( Op . 111 ) , ” says writer , ‘ fascinating 
 facility changed rhythm 
 intuitively phrased beautifully . ” : ‘ Schu- 
 mann ‘ Etudes Symphoniques ’ ( Op . 13 ) gave 
 opportunity showing intensely , 
 magnetically , expression conveyed 
 melody notes , sing witchingly wailful refrains , 
 surrounded intricate harmonies . ” Expres- 
 sion singing witchingly wailful refrains ( alliteration 
 commended Mr. Swinburne ) midst 
 intricate harmonies makes striking novel 
 picture . writer continues : ‘ Liszt ‘ Etude de 
 Concert ’ brilliant , moved 
 audience greatest enthusiasm telling 
 clearness touch note rapid 
 finger passages . ” Grammatically , Etude 
 , , course , Paderewski meant . 
 pass fine example word - slinging 

 Mr. J. C. B. Trrsurr delivering series 
 musical lectures connection University 
 Extension College , Reading , , judging 
 Beethoven read , judicious 
 instructive discourses . 
 necessarily limited time allowed , Mr. Tirbutt 
 gave asketch master life , incidental 
 references works , laid 
 audience , help good artists , 
 examples movements Pianoforte Trio 
 ( Op . 1 , . 3 ) Sonatas pianoforte 
 violoncello ( Op . 5 ) , Violin Romance G , 
 songs . limit advantages 
 lectures like Mr. Tirbutt 

 unlucky reviewer dealt modern 
 edition Johnson Dictionary new work , 
 rated great lexicographer pretty soundly 

 SYMPHONY CONCERTS 

 TuosE persist attributing attractiveness 
 Wagner music caprice fashion , 
 difficulty account overflowing audience , 
 14th ult . , crowded cheaper portions 
 Queen Hall , stalls unoccupied . 
 Qn occasion Mr. Henschel devoted Concert 
 tothe works Bayreuth master , , commemoration 
 death - day , Beethoven Symphony . great 
 work Bonn master received excellent interpre- 
 tation , particularly regard Adagio 
 emphatic Finale . meritorious interpretations 
 given Wagnerian excerpts , consisted 
 Prelude ‘ ‘ Lohengrin , ” - called ‘ ‘ Good Friday 
 music ” act ‘ ‘ Parsifal , ” familiar 
 orchestral arrangement Introduction closing 
 scene ‘ “ ‘ Tristan Isolde . ’’ Greater intensity 
 expression demanded music , 
 performance abundantly satisfied requirements 

 large majority attentive listeners proof 

 masterpiece Brahms , exquisitely beautiful 
 Clarinet Quintet B minor ( Op . 115 ) , played 
 Saturday , 2nd ult . , following Monday , 
 St. James Hall filled occasions . Mr. 
 Mihlfeld displayed perfect command 
 instrument principal . occasion 
 took , Miss Fanny Davies , Weber Duo 
 Concertante E flat pianoforte clarinet , Miss 
 Davies rendering solos Scherzo Impromptu 
 Chopin , perfectly unaffected , expression . 
 Madame Emily Squire gave satisfaction songs 

 4th ult . programme included Beethoven early 
 melodious Trio B flat pianoforte , clarinet , 
 violoncello ( Op , 11 ) Schumann Sonata G minor ( Op . 
 22 ) , magnificently played , alike technique feeling , 
 Mr. Leonard Borwick . Mr. Hugo Becker gave pleasing 
 cantabile movement violoncello César Cui , Miss 
 Fillunger altogether admirable Brahms 
 recently published Volkslieder 

 programme Saturday , gth ult . , 
 stereotyped pattern , small audience . 
 suffice state concerted works 
 Beethoven Quartet ( Op . 18 , . 5 ) Mendelssohn 
 Sonata B flat pianoforte violoncello ( Op . 45 ) ; 
 Miss Eibenschiitz Beethoven Variations 
 C minor , Lady Hallé recitative Adagio , 
 Spohr Violin Concerto G minor ( . 6 ) , Miss Kate 
 Cove - chosen songs Liszt , Miss Maude V. 
 White gave large measure satisfaction 

 Sir Alexander Mackenzie Pianoforte Quartet E flat , 
 work drew attention dawning ability 
 Scottish musician years ago , 
 performed following Monday , freshness 
 vitality recognised heard 
 time . music bears impress youth- 
 fulness joyous life , beautifully rendered 
 Miss Fanny Davies , Lady Hallé , Messrs. Gibson 
 Whitehouse . Miss Davies gave intelligent per- 
 formance Beethoven Sonata D minor ( Op . 31 , . 2 ) , 
 favourite preceptress , Madame Schumann , 
 Mendelssohn Quartet minor ( Op . 13 ) completed 
 instrumental portion programme . songs 
 Mendelssohn , Dvorak , Paderewski , Madame Amy 
 Sherwin showed advanced respect ot 
 vocal refinement 

 features lent special interest Concert ot 
 Saturday , 16th ult . appearance Mr. 
 Emil Sauer Mr. Arthur Chappell audiences 
 Rubinstein Pianoforte Quartet C ( Op . 66 ) 
 performed time . work displays 
 deceased Russian virtuoso ripest manner 
 composer . seldom symmetrical dealing 
 symphonic forms , movements 
 Quartet somewhat disjointed rhapsodical , 
 defects construction atoned 
 impressiveness writing generally . slow 
 movement reminds hearer Beethoven Piano- 
 forte Trio D ( Op . 70 , . 1 ) . Allegro scherzando 
 F bright piquant , Finale well- 
 constructed movement . Rubinstein Quartet virtually 
 pianoforte solo string accompaniment , Mr. 
 Sauer recognise , companion artists 
 “ ” times . gifted pianist selected 
 solo Beethoven penultimate Sonata flat ( Op . rro ) , 
 , highly finished remarkably striking per- 
 formance , gave encore Chopin Nocturne G 
 indescribable beauty touch method . Miss Gwladys 
 Wood agreeable songs Handel Brahms , 
 Mendelssohn perennial Quartet E flat brought 
 Concert conclusion 

 Concert Monday , 18th ult . , 
 notice month , justice 
 words . Mr. Sauer second appearance 
 season , taking Schubert Pianoforte Trio B 
 flat , playing solo Chopin Allegro de Concert 
 ( Op . 46 ) , , hands , wonderfully effective 

 THURSDAY SUBSCRIPTION CONCERTS 

 fourth Concert series , held Queen 
 Hall 7th ult . , composer drawn tor 
 Beethoven ; , considering 
 prescribed limits Thursday scheme , choice 
 pieces scarcely improved . 
 instance , tenor air peerless ‘ ‘ Adelaide , ” ’ 
 sung Mr. William Nicholl refinement style 
 purity expression developed 
 beauty work , secured interpreter 
 compliment double recall . Miss Meredyth Elliott dis- 
 tinguished ‘ ‘ Creation Hymn , ” Miss Anna 
 Kelly obtained warm approval delivery ‘ ‘ Kennst 
 du das Land ? ” Grand Trio B flat ( Op . 97 ) 
 pianoforte , violin , violoncello , efficiently played 
 Messrs. Septimus Webbe , Otto Peiniger , Hans Adolf 
 Brousil , headed list ; violin solo second 
 named artists gave Romance F. According 
 custom , second miscellaneous . Mr. 
 Septimus Webbe acquitted Edward 
 German Concert Study flat , Mr. Brousil played 
 solos Mr. J. H. Bonawitz . specially successful 
 features section Mr. Nicholl finished ren- 
 dering Ernest Lake ‘ “ ‘ love delight , ’’ 
 old song , ‘ ‘ early horn . ” conscientious vocalist 
 associated Miss Kelly Luzzi duettino , 
 “ Insiéme . ” Concert — earlier half 
 devoted Dr. Hubert Parry works — Mr. Nicholl 
 MS . songs ‘ ‘ Weep ye ” “ Thine 
 eyes shine 

 MUSICAL GUILD 

 Amonc smaller conspicuous concert - giving 
 institutions London , easy 
 members struggled energy 
 perseverance , years discouragement com- 
 parative neglect , known 
 Musical Guild , striven seasons — 
 1889 — foster taste best specimens chamber 
 music residents Kensington . meeting 
 held weeks consider steps 
 taken secure continuance excellent 
 Concerts given ex - students Royal College 
 compose Guild . meeting success , 
 judge large audience Concert 
 twelfth series , took place Town Hall 
 evening Monday , 18th ult . , attendance 
 considerably excess seen 
 Guild Concert ; known 
 wealthy amateurs neighbourhood 
 places shilling , , 
 interests Guild , 
 glad occupied . intent draw 
 attention Concerts criticise details , 
 merely chief pieces programme 
 Beethoven Septet Dvorak Pianoforte Quintet , 
 performance works speak 
 terms praise needing little , , qualification . Solo 
 pieces clarinet violoncello admirably played 
 Messrs. C. Draper P. Ludwig , vocalist , 
 Mr. C. Magrath , contributed , Halévy “ Si la 
 rigueur , ” songs Mr. A. Davidson Arnott ( member 
 Guild ) , second , setting Shelley 
 ‘ Time long past , ” admirable composition , rich 
 poetical feeling musicianly treatment . 
 Concert enjoyable 

 SIR A. C. MACKENZIE NATIONAL MUSIC 

 PIANOFORTE VIOLIN RECITALS 

 NuMBER performances taken place 
 past month fairly come heading , 
 , speaking generally , ordinary pattern , 
 dealt briefly . second Recital 
 Miss Florence , Queen Hall , January 31 , 
 included Beethoven ‘ ‘ Waldstein ” Sonata C ( Op . 53 ) , 
 Brahms Variations original theme ( Op . 21 ) , 
 piquant little movements Harpsichord Suite 
 , Henry Symonds , organist City 
 early century 

 _ Miss Recital present 
 given 14th ult . , principal features pro- 
 gramme Mozart Sonata minor Schumann 
 ‘ Faschingsschwank aus Wien , ” crisply 
 powerfully rendered . numbers Brahms 
 set pieces ( Op . 116 117 ) Chopin Fantasia F 
 minor ( Op . 49 ) played , noticeable 

 justice rendered works , Miss Hare possessing 
 excellent technique , pieces Chopin , 
 Schumann ( comiposer represented ever- 
 welcome ‘ Papillons , ” Op . 2 ) , Brahms , Rubinstein , 
 Liszt 

 Master Basil Gauntlett Recital Steinway Hall , 
 Thursday , January 31 , rendered interesting 
 admire juvenile prodigies fact 
 young performer stated years age . 
 grandson Dr. Gauntlett — clever , 
 somewhat eccentric musician , died nearly 
 years ago . displaying phenomenal capacity , 
 Master Gauntlett afforded evidence sound tuition , 
 credit preceptor , Mr. Carlo Ducci . programme 
 include works magnitude , wise ; 
 number minor pieces Byrde , Frescobaldi , 
 Couperin , Domenico Scarlatti , Bach , Mozart , Beethoven , 
 Mendelssohn , Schubert , Chopin gave plenty variety 
 scheme . time , young aspirant 
 recommended continue studies 
 public recitals present 

 excellent artist , Mr. Franz Rummel , gave 
 series Pianoforte Recitals St. James 
 Hall , Friday afternoon , 15th ult . work 
 ability outset prominently displayed 
 Beethoven set Variations original theme 
 F ( Op . 34 ) ; Chopin favourite Sonata B flat minor , 
 Funeral March following ; Schumann enormously 
 difficult Toccata C ( Op . 7 ) vigorously interpreted ; 
 Mr. Rummel rendering ‘ “ Miniatures ” 
 Rubinstein ( Op . 93 ) delightfully refined . 
 pieces Strauss , Mendelssohn , Liszt included 
 programme 

 briefest record needed concerning 
 Pianoforte Recital given Mr. Gustave Pradeau , 
 Princes ’ Hall , Saturday , 16th ult . programme 
 contained names composers , Bach 
 represented “ Italian ’’ Concerto French 
 Suite ( . 5 ) , Chopin Sonata B minor , 
 Schumann minor pieces . Mr. Pradeau 
 commendable music Bach 

 ROYAL COLLEGE MUSIC 

 6th ult . Orchestral Concert given 
 Imperial Institute , programme including delightful 
 Overture Peter Cornelius ‘ ‘ Barber Bagdad ’’ ; 
 Beethoven Fourth Symphony B flat ; scena , ‘ ‘ Ich 
 woh dies Gewand , ” Bruch ‘ “ Odysseus , ’ sung 
 Miss Louise Watson ; Spohr eighth Violin Concerto 
 ( Scena Cantante ) , played Miss Jessie Grimson ; 
 setting soprano orchestra ‘ ‘ Zara Ear- 
 rings , ” Lockhart ‘ Spanish Ballads . ’ 
 work Mr. S. Coleridge - Taylor — _ scholar 
 College , known number com- 
 positions published years . Ballad 
 question displays decided talent , orchestration 
 especially felicitous touches . sung 
 Miss Clementine M. Pierpoint , clear , high 
 soprano told Mr. Taylor music . 
 orchestra best form ; slips infre- 
 quent , heard Professor Stanford young 
 people play greater finish refinement . 
 Concert 13th ult , heard capital performance 
 Dvorak fine Pianoforte Quintet ( Op . 81)—a work 
 stand chance classic . 
 Misses Gwendolyn Toms , Marie Motto , Ruth Howell , 
 Emma Smith , Mr. E. Tomlinson interpreters . 
 Miss Isabella Graham played Chopin Study E ( 
 Op . 10 ) smoothly expressively , plenty 
 energy brilliancy rendering Tschaikowsky 
 peculiar beautiful Scherzo Russe 

 ROYAL ACADEMY MUSIC 

 MR . CHARLES FRY RECITALS . whic 
 reputation Mr. Charles Fry Beet 
 chiefly accredited large audiences attended octal 
 Recitals , Miss Olive Kennett , past month — 
 St. Martin Town Hall , little doubt befor 
 attractiveness evenings increased voice 
 musical arrangements . Beet 
 Recital , 5th ult . , Mr. Clement v 
 Locknane written new incidental choral stren 
 orchestral music ‘ ‘ Macbeth , ” greater r tha 
 lines Witches set excellent fashit 
 effect , probably time . music perso 
 rendered quintet strings ( led Mr. Gatehouse ) migh 
 harmonium ( composer presided ) , editio 
 small choir directed Mr. J. T. Hutchinson , , 
 gentleman singing music assigned Hecate . J 
 new incidental numbers specially composed Mr. fi 
 Berthold Tours ‘ ‘ Romeo Juliet , ” selections editic 
 formed chief portion second Recital , tyran 
 12th ult . Mr. Tours music consists orchestral ah 
 pieces — Andante grazioso , Allegro , Adagio know 
 veligioso , Allegro appassionata , severally designed 
 suggest respective characters ¥ uliet , Mercutio , eee 
 Friar Laurence , Romeo . pieces written 
 strings , described French character , , 0 
 played successively , form effective Suite , found 
 manner heard occasion preceding unfort 
 Recital . Sir Arthur Sullivan piquant incidental music | 
 “ Merchant Venice ” given — _ 
 Recital , roth ult . little artistic importance tionec 
 pertains Mr. Stanley Hawley essentially illus _ — 
 trative music - known dramatic poems , y Se 
 “ Bells , ” ‘ ‘ Soul Music , ” ‘ ‘ Lorraine Lorree , ” 
 “ Story Faithful Soul ” severally cole 
 heard Recitals . Mr. Hawley music follows tn 
 spirit text fidelity suggestiveness th N 
 described masterly , musical ¥ fl 
 intuition Mr. Fry Miss Kennett enabled 
 advantage clever background , maée nl 
 recitations enjoyable effective Dar 
 impersonations . H. Pe 
 MUSICAL ASSOCIATION . Dr. Vi 

 usual interest attached meeting FF en 
 Musical Association 12th ult . , owing th § tiene b 
 reading paper Sir George Grove , entitled “ fer 7 text j 
 words successive editions Beethoven Ninth © ports 
 Symphony , ” 2 subject known > aang 
 great interest . Sir George said remarks wert | ¢ 
 taken musician , simply F tedund 
 intended attention alterations 
 score Ninth Symphony successivé interes 
 publications thirty years . origina Mendel 
 editions Beethoven Symphonies issued questio 
 composer approval , ‘ errors excepted , ” ? meetin 
 taken ascorrect . case , redundant bars introd : 
 Scherzo C minor Symphony , Beethoven wrote t ? Gott y 
 publishers immediately publication , correcting tt B whe 
 mistake ; reason letter acted upot , tune 
 , , noticed thirty years ( 
 , intuition Mendelssohn promp ” / 
 inquiries , resulted pte pi 
 duction letter . Ninth Symphony sonata 
 performed , MS . , Vienna , 7 , 1824 ; thesec sixt 
 performance Philharmonic Society ; teich 

 March 11 , 1825 . score published Mess 

 1 

 bar , engraved folio edition Presto D — 116 , 
 agreed exactly list metronome marks 
 Beethoven letters Schott Moscheles . 
 octavo edition , , minim changed 
 semibreve , exactly doubling speed . bars 
 beginning famous cadence soli 
 voices , words ‘ “ ‘ die Mode streng getheilt , ’’ 
 Beethoven , apparently excited subject , 
 usual love plain speaking , changed word 
 streng , “ strictly , ” frech , ‘ ‘ insolently ’ — i.e. , 
 “ fashion strictly parts , ’ ‘ 
 fashion impudently parts . ’ Surely characteristic 
 personal touch great composer individuality 
 suffered remain , octavo 
 edition Schiller word streng restored — 
 , case , note comment . 
 Finale double bar change signature 
 sharps bars earlier octavo 
 edition folio edition . , Beethoven 
 tyrant copyists . knew exactly 
 meant , intention public 
 know . letter copyist wrote : ‘ ‘ 
 dot note dash , 
 vice - versd . Dashes dots things . ” 
 importance Beethoven attached difference 
 observable folio edition , octavo 
 found constant level dots ; 
 unfortunate , system prevailed Breitkopf 
 Hartel “ critical correct edition ’’ entire 
 series Beethoven symphonies . Sir George men- 
 tioned instances explicit directions 
 Beethoven life ignored , concluded 
 expressing desire edition symphonies 
 Beethoven left , apparent errors 
 omissions suggestions noted margin 
 set footnotes . , hoped vital 
 difference minim — 116 semibreve — 116 Trio 
 Ninth Symphony corrected , 
 excellent horn players secured scramble 
 lately forced 
 - known passage 

 discussion , chairman , Mr. W. 
 H. Cummings , Mr. Otto Goldschmidt , Mr. F. G. Edwards , 
 Dr. Vincent , took , interesting 
 Particulars given concerning MS . copy sent 
 Philharmonic Society , contained emenda- 
 tions Beethoven , pencil translation 
 text Italian , language work 
 performed country direction Sir George 
 Smart . answer Mr. Shedlock , Sir George admitted 
 Schindler Berlioz maintained 
 tedundant bars Scherzo C minor Symphony 
 intended . close , Mr. Goldschmidt 
 interesting communication reference chorales 
 Mendelssohn Organ Sonatas , reply 
 questions addressed Sir John Stainer 
 meeting Association season . chorale 
 Introduced sonata known “ mein 
 Gott ” ( “ God ordains ” ) , originally 
 French popular song date 1529 . Bach 
 pe Cantatas , St. Matthew Passion 
 ( I. . 31 ) . sonata “ Aus tiefer 
 ( “ deep ” ’ ) , appeared Luther 
 - book ( Wittenburg , 1524 ) . chorale fifth 
 Pag probably Mendelssohn . 
 tei Meg | - known “ Vater unser m Himmel 

 ch ” ( “ Father art Heaven ” ) , originally 

 MUSIC BIRMINGHAM . 
 ( CORRESPONDENT 

 important musical event taken place 
 past month second Mr. Stockley 
 orchestral series , given January 31 . Despite Arctic 
 severity weather good audience assembled , pro- 
 gramme offering occasion distinct novelties , 
 pice de resistance , , B flat Symphony 
 ( . 4 ) Beethoven . Great interest centred pro- 
 duction Grieg new Suite “ Sigurd Jorsalfar , ” 
 highly successful rendering varied 
 captivating movements work , characteristic 
 composer — , way , long 
 installed favourite musical public . 
 agreeable innovation presentment 
 important works duo pianoforte ( pianofortes ) — 
 viz . , Mozart Concerto E flat Saint - Saéns 
 Grand Duo . Dr. Rowland Winn Mr. Percy 
 Stranders thoroughly artistic exhibition 
 vastly interesting pieces . Mr. Plunket Greene sang 
 effect set ancient ditties — quaint little 
 strains lilting sort — Irish , Welsh , Cornish respec- 
 tively , considerable impression Dr. H. 
 Parry ‘ ‘ Anacreontic Ode , ” noble piece song - music , 
 perfectly suited , , artist 
 voice 

 accordance customary rule , 
 “ crowned ” ’ works Triennial Festival shall 
 repeated following season , Dr. Parry ‘ ‘ King 
 Saul ” - presented Festival Choral Society 
 Concert 14th ult . important work 
 awaited great expectation , fine audience 
 assembled Town Hall occasion ; Dr. Parry , 
 Festival performance , conducting work . 
 accordance generally - expressed sentiment , 
 score compressed little ; performance 
 month , taking , roughly speaking , hours ahalf . Mr. 
 Ffrangcon Davies , time , appeared title véle , Mr. 
 Iver McKay ( David ) , Mr. J. Sandbrook ( Samuel ) , Miss Anna 
 Williams ( Michal ) , Miss Jeanie Rankin ( Witch 
 Endor ) . Mr. Davies , hardly competing Mr. 
 Henschel respect dramatic force expression , 
 requisite justice exacting music King . 
 close movement , ‘ “ ‘ Saul Dream , ” 
 dialogue Samuel , Mr. Davies applauded 
 enthusiastically . Mr. McKay best effort 
 expressive solo ‘ ‘ Lord trust ” ; Mr. Sand- 
 brook remarkably good impression . Miss Williams 
 heard advantage , ‘ ‘ Lamentation ” solo 
 especially , artistic effort . chorus — 
 Dr. Parry works — exacting work committed 
 , characteristic force composer , , 
 peculiarly manifest finely dramatic choral writing 
 , instance , numbers ‘ ‘ Vain words thou speakest ” 
 ‘ Glory Saul . ” Dr. Parry received deafening 
 ovation close work 

 298 . away ? oe . «   C , Villiers Stanford 

 299 . Summer ... .. « . ‘ ny , Hamilton Clarke 44 
 300 . Tothe woods .. ee eo ee 
 301 . Noble thy life js “ e ee xe " Beethoven # 
 302 . world goes round .. .. « e Marie Wurm 

 303 . Softly moonlight .. F. Iliffe # 
 304 . stole love ( arranged F. Maxson ) W. Macfarren 

 Tue Concert Harrison - Simpson series 
 held 6th ult . , Kinnaird Hall . Mrs. Henschel 
 gave ideal interpretation Liszt ‘ ‘ Loreley ” 
 Purcell ‘ ‘ Nymphs Shepherds . ” Miss Clara Butt , 
 appearance Dundee , successful 
 in“O ma Lyre , ” Gounod ‘ Sapho , ” Handel 
 atia “ Lascia ch ’ io pianga , ” beautiful voice 
 great impression . Miss Pauline Sant - Angelo played 
 Liszt Rhapsodie Hongroise ( . 5 ) Rubinstein 
 Valse Caprice remarkable skill vigour . 
 performers — Miss Marianne Eissler , Mr. Braxton Smith , 
 Dilettante Vocal Quartet — received 

 Messrs. Paterson Scottish Orchestral 
 Concerts given 11th ult . , conductor- 
 ship Mr. Henschel , attracted large audience . 
 programme included Weber Overture ‘ Der 
 Freischiitz , ’ ” Beethoven C minor Symphony , Liszt 
 Symphonic Poem “ Les Préludes . ’ ” ? Max Bruch G minor 
 Concerto Svendsen Romanza admirably played 
 Miss Frida Scotta . Miss Lalla Miranda vocalist 

 Organ Recitals given Dr. Turpin , 
 ist ult . , St. Luke Free Church , Broughty Ferry , 
 occasion opening new organ , 
 attended 

 MUSIC NORTHUMBERLAND DURHAM . 
 ( CORRESPONDENT 

 past month considerably active 
 musical matters predecessor , 
 means busy important populous district 
 asthis . Concert Newcastle Chamber 
 Music Society took place January 29 , proved 
 successful Concerts excellent Society 
 invariably . instrumentalists Mr. Willy 
 Hess ( violin ) , Mr. C. Rawdon Briggs ( second violin ) , 
 Mr. Speelman ( viola ) , Sefior Pezze ( violoncello ) , Mr. 
 J. M. Preston ( pianoforte ) . principal works performed 
 Mozart String Quartet C major ( . 6 ) , 
 Andante Variations Schubert familiar post- 
 humous Quartet D minor , Beethoven Quartet 
 strings C major ( Op . 8 , . 4 ) , Brahms Sonata 
 ( Op . roo ) pianoforte violin . 
 works exceedingly played , particular 
 mention performance Sonata 
 Mr. Willy Hess Mr. J. M. Preston , 
 talented local pianist . vocalist Mdlle . Marie 
 Fillunger , sang songs Schumann , Schubert , Men- 
 delssohn , Brahms artistic manner . 
 final Concert season Chamber Music 
 Society engaged , usual , Miss Fanny Davies , Herr 
 Joachim , Signor Piatti . hoped 
 distinguished violoncellist sufficiently recovered 
 illness appearance 

 r2th ult . Herr Emil Sauer , eminent pianist , 
 appearance Newcastle - - Tyne , gave 
 Recital new Assembly Rooms large 
 enthusiastic audience 

 M. PapErREwskt Recital , January 28 , attracted 
 crowded audience Albert Hall , programme 
 enthusiastically listened , scene close 
 Recital unusual excitement 

 appearance Nottingham Mdlle . Eiben- 
 schiitz Drawing - room Concert January 29 . 
 Nottingham amateurs owe great debt gratitude 
 committee Concerts long list artists 
 renown engaged . Concert 
 maintained high standard interest execution 
 reached . Mdlle . Eibenschitz played 
 Beethoven ‘ ‘ Waldstein ’’ Sonata group pieces 
 Scarlatti , Schumann , Brahms , Liszt great 
 acceptance ; Mr. Gompertz String Quartet played 
 Smetana famous Quartet E minor ( “ Aus meinem 
 Lieben ’’ ) portions quartets Haydn Schubert 
 success 

 7th ult , Sacred Harmonic Society gave 
 miscellaneous Concert , anticipated 
 interest , strong programme promised 
 appearance Miss Ella Russell , Mr. Hirwen Jones , 
 Mr. Andrew Black . choral pieces Mendelssohn 
 42nd Psalm “ Spring ” ( Haydn ‘ ‘ Seasons ” ’ ) , 
 danger forgotten Nottingham . 
 Mr. Adcock direction capitally presented , 
 trust interest awakened Society 
 amateurs revivals justify 
 miscellaneous Concerts . Miss Ella Russell suc- 
 cessful cantatas , dramatic talent 
 displayed ‘ Softly sighs . ” Mr. Andrew Black , 
 usual , carried audience away enthusiasm , 
 especially scene Wagner ‘ ‘ Flying Dutchman . ” 
 Mr. Leyland replaced Mr. Hirwen Jones , unavoid- 
 ably absent . band deserve honour admirable 
 work exacting programme , including Men- 
 delssohn ‘ ‘ Italian ” ? Symphony 

 MUSIC WILTS HANTS . 
 ( CORRESPONDENT 

 SERIES Chamber Concerts given Salis- 
 bury past month , direction 
 Rev. H. W. Carpenter , aid St. Mark Church Building 
 Fund . quartets trios Beethoven , Mozart , 
 Schumann , Gade drawn pro- 
 grammes , executants series Mrs. Regan , 
 Mr. F.L. Bartlett , Mr. Regan , Mr. Ffooks , Miss Hussey , 
 Miss Fussell . Countess Radnor , Mrs. Windley , 
 Miss Bath , Rev. H. W. Carpenter , Rev. H. J. 
 Trueman appeared vocalists 

 Successful Concerts given Town Hall , 
 Devizes , afternoon evening 13th ult . , 

 seen Pianoforte Recital . Mr. Christensen gave 
 Chamber Concerts Leeds , 4th ult . 
 programme included Raff Pianoforte Trio 
 minor ( Op . 155 ) , Concert - giver assisted 
 Messrs. Edward Elliott ( violin ) Arthur Bolton 
 ( violoncello ) . Mr. Elliott clever playing Max Bruch 
 G minor Concerto noteworthy feature Con- 
 cert . Miss Mary Poole sang songs simply 
 pleasantly . rgth ult . Messrs. 
 Haddocks ’ ‘ ‘ Musical Evenings ” ’ took place . Mr. Edward 
 Lloyd ballad singing chief thing Concert , 
 Miss J.illie Wormald , Miss Alice McFarlane , 
 Mr. Stanley Cookson took vocalists . Miss 
 Teodoras pianist Miss Durbridge gave 
 couple harp solos 

 Mr. Edgar Haddock gave Violin Recital Leeds 
 College Music , 12th ult . , programme including 
 Bach Chaconne Beethoven Concerto . Mr. Wallis 
 Vincent accompanist 

 Bradford , like Leeds , boast exceptionally 
 interesting Subscription Concert past month . 
 Advantage taken Mr. Mihlfeld presence 
 England secure assistance clarinettist Brahms 
 Clarinet Quintet , country 
 remain closely associated . superbly artistic qualities 
 playing insisted need 
 bare acknowledgment . Lady Hallé , Messrs. 
 L. Ries , Gibson , Becker completed cast , 
 finish refinement , difficult improve . 
 heard Mozart Quintet 
 combination instruments . Sir Charles Hallé 
 pianist Mrs. Henschel vocalist . 
 Concert , January 26 , Bradford Permanent Orchestra 
 special feature pieces Sir Arthur Sullivan , 
 doubtless contributed greatly popularity 
 event . Mendelssohn D minor Pianoforte Concerto 
 brilliantly played Mr. Frederick Dawson , fine 
 staccato touch dashing octaves remarkable . 
 introducing Waltz Waldteufel committee 
 compromised dignity , generally thought 
 acted wisely . Musicians scrupulous Wagner , 
 Brahms , Bulow hesitated express 
 admiration Strauss waltz . Miss Brigg vocalist , 
 absence affectation excellent enunciation 
 singing enjoyable , spite apparent 
 nervousness . 16th ult . Concert given 
 Society . Dr. Creser , Organist 
 Chapel Royal , came conduct , time 
 public , Symphony founded old English popular 
 tunes . successful movements melo- 
 dious expressive Largo , indebted 
 folk - lore material , Scherzo , 
 excellent use old songs , “ jug 
 ” “ Phillida flouts . ” Mr. John Dunn 
 played Spohr Ninth Violin Concerto masterly 
 fashion showed exceptional command 
 instrument brilliant solos . Mr. Lucas Williams 
 vocalist . band , Mr. G. F. Sewell 
 Conductor , shows steady advance season . 
 strongest proof found 
 extremely delicate careful manner accom- 
 paniments Concerto played 

 _ Huddersfield , Subscription Concerts 
 given notice music Yorkshire . 
 January 29 Scottish Orchestra appeared . pro- 
 gramme similar given Leeds following 
 night , chief difference Symphony . 
 Haydn delightful work B flat ( . 9 Salomon 
 set ) , ideally perfect reading given . 
 finished , sympathetic playing difficult 
 imagine . Sir A. C. Mackenzie “ Britannia ” Overture 
 Liszt “ Les Préludes ” included programme . 
 Subscription Concert , 12th ult . , 
 miscellaneous kind . Madame Bertha Moore Miss 

 tothy Jackson — descendant , believe , com- 
 poser “ ‘ Jackson F — vocalists . pianist 
 Mr. Frederick Dawson , facile execution 
 shown music Beethoven Chopin ; Miss 
 Frida Scotta favourable impression 
 thoroughly artistic violin playing . trombone quartet 

 added variety programme . 1gth ult . 
 Huddersfield Glee Madrigal Society gave enjoy- 
 able Concert , programme consisting type 
 vocal music suggested . Madrigals 
 Palestrina Benet , glees Mornington Bishop , 
 - songs modern composers , sung 
 excellent precision delicacy Mr. J. E. Ibeson 
 able conductorship . Solos contributed 
 members Society 

 musical season activity , 
 interesting Concerts progress , chief 
 given great orchestral societies 
 Sunday . ( 1 ) , Concert Society 
 Conservatoire , devoted classical music modern 
 works undisputed value ; ( 2 ) , orchestra M. 
 Lamoureux , repertory consists chiefly works 
 Wagner generally modern tendency ; 
 ( 3 ) , orchestra M. Colonne 

 great enterprises add 
 Harcourt Concerts , prodigious quantity 
 music consumed , performances 
 finish makes reputation Concerts . 
 chamber music societies numerous 
 principal ones mentioned : quartet parties 
 M. Marsick , M. Géloso ( quartet Beethoven ) , 
 M. Mendels , M. Nadaud ( French music ) , M. 
 Fernandez ( works Schumann ) , M. Parent , M. 
 Lefort , M. Rémy , M. Weingartner , & c. ; Recitals 
 pianists Breitner , Paul Brond , César Géloso , 
 Philipp , & c. concert parties gives 
 Concerts season . 
 mention societies definite objects , 
 Société Nationale ( modern school ) , Société des 
 Compositeurs , & c. , amateur societies 

 important musical event past month 
 production Opéra , 15th ult . , 
 Mdlle . Augusta Holmés lyric drama acts , “ La 
 Montagne Noire . ” ’ Criticism dealt somewhat severely 
 work , probably account disappointed 
 expectations . Mdlle . Holmés known Wag- 
 nerian predilections , successful works given 
 reputation possessing considerable individuality . 
 led expectations greater realisa- 
 tion . , , recognise life 
 movement opening act , 
 Mdlle . Holmés deaJt successfully certain 
 parts required charm . Apart thematic 
 reminiscences , work affinity Wagnerian 
 method . ‘ plot , novel worth describing 

 Following present season come short period 
 Wagnerian opera , Mr. Damrosch bdton , 
 Rose Sucher Alvary stars greater 
 magnitude . Wagnerian music - drama ‘ having heretofore 
 proved unprofitable , interesting watch 
 result coming venture . Mr , Damrosch evidently 
 proceeding caution , ( supposed 
 quarters ) backed Mr. Andrew Carnegie , 
 moderate loss entail disaster 

 far , Oratorio Society left - beaten 
 paths , bring Mr. Damrosch ‘ Scarlet 
 Letter , ” reported . Boston 
 Orchestra duly continued visits , 
 produced Concerto violin , composed Mr. 
 Loeffler , member band , met 
 warm reception . Kneisel Quartet coming 
 Boston stated intervals ; best New 
 York Quartets — Beethoven — 
 usual , violin ( Mr. Gustav Dannreuther ) taken 
 leading orchestras frequently 

 Cincinnati permanent orchestra 
 assembled , Mr. Frank van der Stucken hard 
 work organising . Reports public work 
 encouraging . existence band 
 great measure energy perseverance ladies 
 Cincinnati , worked heart hand secure 
 guarantee fund requisite maintaining 
 company - rate musicians . plan 
 raise sum yield annual revenue 
 40,000 dollars , maintained large band 

 Tue Cricklewood Philharmonic Society gave per- 
 formance Haydn “ Creation , ” 13th ult . 
 soloists Miss Margaret Hoare , Mr. Reynolds Wood , 
 Mr. Arthur Johnson . Mr. A. A. Yeatman presided 
 pianoforte , Mr. Albert Rayment conducted , 
 chorus numbering members . Concert concluded 
 short miscellaneous programme , 
 Cynthia Quartet sang Bridge humorous - song ‘ ‘ 
 Goslings , ’’ Hatton ‘ “ ‘ Letter , ” ‘ ‘ Simple Simon , ” 
 “ Chinese March . ” Miss Winifred Jones contributed 
 violin solos 

 Mr. H. J. T. Woop gave Concert , 8th ult . , 
 Paddington Baths , aid St. Matthew Church , 
 Bayswater . Haydn Symphonies , arrangement 
 strings Harpsichord Suite Purcell , dances 
 Monsigny “ Aline , ” orchestral portion 
 Mendelssohn Pianoforte Concerto G minor ( solo 
 carefully played Miss C. Worship ) creditably 
 rendered Mr. Wood amateur force . Mr. C. Jacoby 
 gave finish Wieniawski Polonaise Brillante , 
 Miss Grainger Kerr effectively sang couple Beethoven 
 Scotch songs 

 annual dinner Westminster Abbey Old 
 Boys ’ Club took place , gth ult . , Victoria 
 Mansions Restaurant , Mr. J. C. Barrett took 
 chair , company numbering seventy . 
 visitors old boys choirs St. 
 Paul Cathedral , Chapels Royal , Windsor St. 
 James . dinner excellent programme , arranged 
 Mr. F. Charlton Fry , given , Messrs. 
 Avalon Collard , Eustace Bryant , H. C. Hutchinson , Thur- 
 good , Baker , S. H. Parry , F.C. Fry , Arthur Fayne 
 took 

 hold responsible opinions expressed thi 
 summary , notices collated local papers 
 supplied correspondents 

 BASINGSTOKE.—On 11th ult . Harmonic Society 
 gave excellent performances Beethoven “ Mount d 
 Olives ” Cowen “ Sleeping Beauty . ” soloists 
 Miss Kate Drew , Miss Mary Reeve , Mr. Charles 
 Butler , Mr. Giffard Wells . Mr. J. S. Liddle led 
 band Mr. W. H. Liddle conducted 

 Beprorp.—Mdlle . de Nolhac gave Matinée musical 
 Town Hall , 2nd ult . Mdlle . de Nolhac , 
 efforts heartily appreciated , assisted Mf 
 Templer Saxe , Miss M. Chetham ( solo violin ) , Mis 
 Ethel Herbert ( reciter ) . Dr. H. A. Harding acted # 
 accompanist 

 POSTHUMOUS SONG 
 PIANOFORTE ACCOMPANIMENT 

 LUDWIG VAN BEETHOVEN 

 Words 9 _ 
 v. J. TroutBeck , D.D 

 rt OC COOH HHH HH ND HH eH HH OOM OOO 
 DAAMOWAAAVAAAACAGVCCCAAGAGAWDCAAaAA 

 26 . blessed child ( ‘ ‘ Athalie ” ) , Mendelssohn ... va 
 27 . Heaven Earth wcgeaile ( “ Athalie ” ’ ) , 
 Mendelssohn .. $ e ee ee 
 28 . Le Désir ( Sehnsucht ) , ‘ Saniabi . ; aa 2 
 29 . afraid ( “ Elijah ” ) , Mendelssohn ae ae « 26 
 30 . Nocturne E flat ( Op . 9 , . 2 ) , — _ L , ae a. oe 
 31 . Romance G , Beethoven wa “ o +6 
 32 . Noél , Adam vr ee ea oe “ oo Sie 
 33 . Nocturne , Tschaikowsky “ er > 8 ee ree 
 WALKER , A. H. , Mus . Doc . 
 Andante Variations , major ee ee Net 2 0 

 London New York : Nove .. o , Ewer Co. 
 Brighton : J. W. CHESTER , 1 , Palace Place 

