Herr FELIX WEINGARTNER 

 ELGAR ‘ ‘ DREAM GERONTIUS " ’ ) 
 BEETHOVEN CHORAL SYMPHONY 

 SYMPHONY CONCERTS 

 EXTRA WAGNER CONCERT : ror ... APRIL Ig , 8.15 . 
 SpeciAL ENGAGEMENT FRAULEIN THERESE MALTEN 
 ( Royal Court Opera , Dresden 

 PARRY ‘ “ * BLEST PAIR SIRENS " ) 
 BEETHOVEN GRAND MASS Dj } 
 ( Missa SOLENNIS 

 SpeciaAL ENGAGEMENT Dr. HENRY COWARD 
 SHEFFIELD CHORUS ( 300 Voices 

 PROGRAMME MUSIC . 
 PROFESSOR NIECks . 
 ( Concluded page 165 

 Programmes diverse natures , 
 , subjects chosen composers 
 approached different standpoints , 
 dealt different ways . programmatic 
 composition general impression 
 complex subject , series pictures , portraiture 
 character characters . 
 purely emotional , extent descrip- 
 tive , allegorical . ‘ examination 
 works single composer , Liszt , 
 sufficiently exemplify variety , wider 
 range examination lead results 
 astonishing . Beethoven ‘ Pastoral ’ Symphony , 
 ‘ Leonore , ’ ‘ Egmont , ’ ‘ Coriolan ’ Over- 
 tures ; Mendelssohn ‘ Midsummer Night Dream , ’ 
 ‘ Hebrides , ’ ‘ Melusine , ’ ‘ Calm Sea 
 Prosperous Voyage ’ Overtures ; Berlioz ‘ Fantastic , ’ 
 ‘ Harold , ’ ‘ Romeo Juliet ’ Symphonies , 
 ‘ King Lear , ’ ‘ Waverley , ’ ‘ Benvenuto Cellini , ’ 
 ‘ Roman Carinval ’ Overtures ; Liszt ‘ Faust , ’ 
 ‘ Dante ’ Symphonies , Symphonic 
 Poems ‘ Ce qu’on entend sur la Montagne , ’ 
 ‘ Prometheus , ’ ‘ Mazeppa , ’ ‘ Die Ideale , ’ 
 ‘ B Saint - Saéns ‘ Phaéton ’ 

 

 fourth period , entirely 18th century , 
 characterized general striving 
 expressiveness instrumental music , spread- 
 ing cultivation programme music . 
 shown ( 1 ) overtures , entr’actes , incidental 
 music plays operas , instrumental 
 ritornelli accompaniments vocal composi- 
 tions : ( 2 ) melodrama , , instrumentally 
 accompanied speech ; ( 3 ) symphonies 
 sonatas 

 fifth period , begins close 
 ithe 18th century , programme music 
 ithe larger classical forms . Beethoven opens 
 | period , principal inspirer 
 | follow 

 sixth period , fourth decade 
 19th century , characterized wider 
 scope subjects , departure classical 
 form . Berlioz , Liszt , Wagner inspiring 
 geniuses period 

 activity different nationalities , 
 sensuous Italians aloof programme music : 
 intellectual French cultivate predilection ; 
 sentimental Germans occupy _ inter- | 
 mediate position . great | 
 musical nations Europe , long | 
 continuous history . ‘ Norwegians Russians | 
 recently come , | 
 begun attract attention exercise influence . | 
 English appear fitfully , effectively 
 16th roth centuries . 
 Slight co - operation , facts 
 overlooked : contributions 
 virginal composers 16th century ; Purcell 
 programmatic tendencies 
 17th century ; Sterndale Bennett classical 
 programme music 19th century . ‘ 
 youngest generation British composers 
 unmistakably strong bias programme 
 music 

 great mass present - day music , 
 vocal instrumental , programme 
 music,—that , music intended mean 
 , merely tickle ears 
 form - sense-——we effort 
 understand problem problems involved . 
 position unintelligent negation 
 justice subject , brings discredit 
 - judge . Tout comprendre Cest tout 
 pardonner . 1 sure understanding 
 nature capacities music , reason- 
 able aims programme music , furnish complete 
 apology gewve , prove conclusively | 
 legitimacy right exist . 
 admit bad good examples , 
 giving case away . 
 fact ,   Beethoven ) Mendelssohn 
 examples legitimize programme 
 music ? 
 , , contributions 
 species composition fair - minded 
 person acknowledge valuable 
 priceless ? , question 
 choosing . possible , 
 easy natural , combine love 
 programme music love called 
 absolute music 

 

 unusually interesting nature . 
 attractive feature appearance 

 London Dr. Coward celebrated Sheffield Chorus 
 309 voices . splendid Sheffield singers | 
 Elgar ‘ Dream Gerontius ’ 
 Beethoven Choral Symphony , Saturday afternoon , | 
 gth inst . ; Parry ‘ Blest Pair Sirens ’ | 
 Beethoven Mass D , evening 2oth . 
 occasions provide feast music 
 London amateurs eager share . | 
 remaining concerts Festival | 
 devoted mainly orchestral music ( 100 performers ) | 
 concertos , played Professor Johann Kruse 
 violin ) Mr. Mark Hambourg ( pianoforte ) . | 
 conductor — Felix Weingartner . | 
 hear 

 Occasional Wotes 

 Mendelssohn . 
 Meyerbeer 

 Overture , ‘ Ruler Spirits ” ... IVPeber . 
 II . 
 Sinfonia ‘ * Eroica ” Beethoven . 
 SR noe eer ( Le Nozze di 
 Recit . : Giunse alfin | Figaro ) 
 Aria | ‘ * Deh vieni , non tardar ” } Mozart . 
 Mademoiselle ArTOT . 
 Overture , “ * Ruy Blas ” .. Mendelssohn . 
 Conductor — PROFESSOR STERNDALE BENNETT 

 Mus . D 

 soon Majesty entered room Professor 
 Bennett gave signal , band executed 
 National Anthem .   Mendelssohn symphony ( 
 especial favourite Palace ) listened 
 marked interest end end , apparently 
 enjoyed strangers phrase 
 familiar . faultless 
 execution genial , spontaneous , masterly 
 composition heard England . 
 times movement indicated 
 unerring judgment precision ; passage 
 hurried , consequently instance refined 
 expression delicate combination lost . applause 
 grew warmer warmer — successive 
 movement symphony , hearty 
 unrestrained conclusion irresistible ? es¢o 

 sa / fare//o ¢arantella worked 
 spirit ingenuity — 
 divert attention ordinary 
 business evening . Beethoven superb Zvo¢ca 
 a//egro , advantage , 
 taken shade quicker — equally played ; 
 scherzo , imperfectly rendered , given ( 
 difficult horn parts trio included ) absolute perfec- 
 tion , pathetic slow movement ( ‘ ‘ J / arcéa funebre sulla 
 morte dune Eroe ” ) impressively 
 wished , /va / e — invention science 
 appear strive victory , come 
 triumphant — ‘ ‘ hitch . ” overtures — 
 splendid examples genius composers , 
 respects alike , widely opposed 
 scarcely satisfactory , Adler Spirits , 
 , bearing palm irreproachably finished 
 execution . members band _ 
 accomplished conductor , Professor Sterndale Bennett , 
 evidently determined maintain reputation 
 Philharmonic Concerts ; assuredly 
 performance briefly described 
 calculated diminish 

 vocal airs — different expressive 
 — sung utmost feeling , thoroughly 
 artistic style Mademoiselle Art6t — , , 
 hoped , appearance season 

 Mr. Chorley appears strenuous 
 anti - Schumannite colleague 77mes 
 JZustcal World . critic 
 Atheneum said 

 Symphony Dr. Schumann considered 
 German admirers reasonable orchestral 
 work ; described detail 
 Atheneum [ , 1312 ] , attempt 
 class new writers called music , 
 Mozart , Beethoven , Mendelssohn kingdom . 
 add character given 
 piece head work,—robust places , places , 
 , ugly , crude colour thick 
 texture . , imagine , wanted 
 ; glad matter set rest 

 earlier criticism referred extract , 
 Mr. Chorley characterized Symphony ‘ dulness 
 laid dulness . ” Philharmonic directors 
 evidently took criticisms seriously 
 heart , work find place 
 programmes - years later . 
 meantime played fewer 
 times — twice month , March , 1860 — 
 Crystal Palace Saturday Concerts , direction 
 Sir ( Mr. ) August Manns , probably , 
 addition , conducted daily concerts 
 given Sydenham . Symphony 
 performed times Philharmonic Society . 
 dates names conductors : 
 June 5 , 1854 , time England ( Costa ) ; 
 April 26 , 1875 ( Cusins ) ; 26 , 1881 ( Cusins ) ; 
 March 8 , 1899 ( Mackenzie ) ; March 2 , 1904 
 ( Cowen 

 remainder concert extended 
 notice . Miss Elizabeth Parkina , American vocalist , 
 sang delightful air ‘ Depuis le jour ’ Charpentier 
 ‘ Louise ’ ; Miss Marie Hall gave usual brilliant interpre- 
 tation Mendelssohn Violin Concerto — flies 
 away tempo movement ; Miss Dorothy 
 Maggs played Tschaikovsky Pianoforte Concerto B flat 
 minor ; Schumann genial Symphony B flat — 
 jubilee performance ( p. 235 present issue)—concluded 
 concert 

 programme concert 24th ult . opened 
 Beethoven frisky Fourth Symphony , closed 
 Strauss tone - poem ‘ Death Transfiguration ’ — 
 symptoms death Symphony . Mr. Leonard 
 sorwick ably interpreted Brahms Pianoforte Concerto ; 
 Bottesini F sharp minor Concerto double - bass served 
 ability agility Mr. Claude Hobday , 
 contrabassists orchestra ; Miss Maria Gay 
 vocalist . Dr. Cowen conducted wonted enthusiasm 

 SYMPHONY CONCERTS 

 audience 12th ult . work probably came 
 nature novelty , testimony 

 advanced character Liszt genius music 
 appeared fresh , particularly regard scoring , 
 remarkably modern . movement 
 choral close satisfactory portions work . 
 balanced , contains striking themes , 
 episode dealing incident Pao / Francesca 
 beautiful . central portion , ‘ Purgatorio , ” suffers 
 contrast vividness ‘ Inferno ’ section , 
 - development indulged listener 
 experiences somewhat Purgatorial weariness described 
 Dante ; music lofty conception orchestral 
 colouring pure , rich , appropriate tothe subject . 
 choral portion excellently sung ladies 
 Mr. Smallwood Metcalfe choir Eastbourne , 
 subsequently choristers gave admirable renderings 
 Dr. Elgar charming - songs ‘ Fly , singing bird , fly ’ 
 ‘ Snow . ” request Mr. Henry J. Wood , 
 composer recently scored orchestra , parts 
 played time occasion . Needless 
 add greatly effectiveness beautiful 
 compositions . added M. Henri Marteau , 
 Professor Geneva Conservatoire , reappearance 
 England Beethoven Violin Concerto , 
 played great finish purity , somewhat coldly 

 London Concerts 

 248 
 fascinating finish , unanimity expression , 
 artistic intuition interpretations 

 Tschaikovsky ‘ Elegiac ’ Trio minor ( Op 50 ) 
 Schubert B _ flat ( Op . 99 ) pleasantly memorable . 
 second ‘ extra ’ concerts 18th 29th 
 ult . entrusted Kneisel String ( Quartet 
 Boston , U.S.A. previous appearance party 
 England 1897 , comprised MM . Franz 
 Kneisel , Karl Ondricek , Louis Svecenski , Alwin , 
 second violin M. J. von Theodorowicz . 
 18th ult . highly - polished interpretations given 
 Beethoven second ‘ Rasoumovsky ’ ( Quartet Dvorak 
 ‘ Negro ’ Quartet F ( Op . 96 ) , regard readings 
 performances somewhat cold 

 RICHTER CONCERTS 

 Mozart Society gave concert benefit 
 founder , Herr J. H. Bonawitz , 19th ult . 
 Portman Rooms . admirable rendering secured 
 musician Requiem Mass , feature after- 
 noon dramatic singing Miss Blanche Gordon 

 Mr. John Dunn gave orchestral concert 4th ult . 
 St. James Hall , direction Mr. Hamish 
 McCunn . Mr. Dunn heard Beethoven Concerto , 
 Tschaikovsky Concerto D , Sir Alexander Mackenzie 
 ‘ Pibroch ’ Suite , - named conducted composer 

 Stock Exchange Orchestral Society gave concert 
 aid North Eastern Hospital Children 
 23rd ult . , playing orchestra , able 
 direction Mr. Arthur Payne , customary excellence . 
 interesting feature début youthful 
 violinist — Sybil Keymer — pupil Herr August Wilhelnj , 
 played [ dag?o /ina / e Mendelssohn 
 Violin Concerto excellent technique sympathetic 
 feeling remarkable young 

 Sir Alexander Mackenzie ‘ Dream Jubal ’ formed 
 chief feature concert given South London 
 Choral Association 18th ult . , work 
 listened rapt attention large audience . 
 poem recited Mr. Richard Temple , efforts 
 met acceptance , Miss Emily Davies 
 Mr. A. Livingstone Hirst gave great satisfaction 
 principal soprano tenor solos duet . Miss 
 Edith Judge Mr. W. E. Soar completed quartet 
 vrincipals ‘ Gloria Excelsis . ’ capital orchestra 
 ( led Mr. T. E. Gatehouse ) gave effect 
 important assigned thereto composer , 
 choruses rendered manner 
 years earned Association high place ranks 
 London Choral Societies . programme included 
 Sir Frederick Bridge ‘ Ballad Clampherdown . ’ Miss 
 Gertrude Venables organist Mr. L. C. Venables 
 conducted experienced skill 

 Reeves Quartet ( Messrs. H. Wynn Reeves , Henry 
 Gibson , H. Goomand J. T. Field ) gave enjoyable concert 
 chamber music West Hampstead Town Hall 
 February 27 crowded audience . Beethoven String 
 Quartet D ( . 3 , Op . 18 ) Brahms Clarinet Quintet 
 ( clarinet played Mr. George W. Anderson ) 
 included programme . < feature special interest 
 excellent interpreted Pianoforte Trio ( MS . ) 
 composed Mr. Henry Gibson . composer , 
 nephew Mr. Alfred Gibson second violin 
 Quartet , played pianoforte Trio , 
 Miss Ethelinay Holbrook contributed songs 
 tasteful manner 

 Inaugural Concert Winchmore Hill Choral 
 Orchestral Society took place main hall New 
 Institute 16th ult . , chief feature programme 
 Mendelssohn ‘ Hymn Praise . ” fact 
 nearly orchestra ( led Mr. G. A. Parker ) 
 amateur worthy note , capable 
 rendering Symphony speaks earnest 
 patient training conductor , Mr. J. Gilmour Laird , 
 successful results choir . solo 
 Vocalists Miss Lucy France , Miss Florence Bunce , 
 Mr. Henry Turnpenny 

 satisfy popular demand . second concert Mr. 
 Wetzler orchestra came grief ‘ Don Quixote ’ 
 Dr. Strauss helm . fortnight 
 work amazingly brilliant performance concert 
 Boston Symphony Orchestra , comparisons , 
 inevitable circumstances , robbed occasion 
 lustre , fantastical 
 work received luminous authoritative reading . Added 
 fact audience pitifully small . 
 announced concert 
 - morrow ( March 9 ) signalized 
 production composer latest work , - discussed 
 ‘ Symphonia domestica ’ ; write announcement 
 reaches Dr. Strauss , conducted concerts 
 Philadelphia Boston , found impossible 
 prepare work , production postponed till 
 concert . concert , set March 16 , 
 postponed date agreed . 
 Strauss Festival woeful financial failure , 
 far yielded little disappointment artistically 

 different experiences foreign 
 visitors . Herr Weingartner , Philharmonic 
 Society set aside traditions decades gave concert 
 outside subscription , profound impression 
 musical life American metropolis effort 
 doubtless bring permanently , 
 term years ; M. Safonoff staid 
 Philharmonic audience feet waving kerchiefs 
 cheering minutes dial Tschaikovsky s * Pathetic ” 
 Symphony Beethoven ‘ Leonore ’ . 3 . 
 conductor vast temperamental elemental puissance , 
 excited players 
 audience 

 set 

 great success songs Hugo Wolf 
 achieved Germany brought desire 
 acquainted works , 
 opera ‘ Der Corregidor ’ important . work 
 given German stages , 
 performances laid aside : reason 
 Opera feel inducement 
 announce ; management yielded 
 pressure numerous admirers Wolfs music . 
 performance regarded honour 
 composer spent life , 
 necessity ; honour paid ¢&/a¢ 
 rendering respect admirable . 
 accepted pious duty discharged song 
 composer tragic end excited feeling melancholy . 
 ‘ Der Corregidor ’ ripe effective work ; 
 shows praiseworthy writing neophyte . 
 commonplace , dramatic traits . 
 , merry story music conceived 
 tragic mood ; scene jealousy , best 
 work , proper effect . 
 signs talent , interest musico- 
 dramatic problems notable points 

 Emil Paur , conductor Boston Symphony 
 Orchestra , given interesting concert . trained 
 Viennese school — father orchestral player took 
 performances given direction Beethoven 
 . brilliant conductor , knows 
 interest magnetize audience 

 unusual musical event performance Bach 
 cantata ‘ QO ewiges Feuer . ” live Catholic country 
 church performances Protestant sacred works 
 permitted , given concert - hall . 
 wonderful aria cantata admirably sung 
 Frau Helene Durigo , choir ‘ Singverein ’ 
 deserve praise participation work . 
 deep impression created Brahms little - known 
 ‘ Begrabnissgesang ’ ( Burial Song ) . performance , 
 time , Tschaikovsky ‘ Winter Dreams ’ 
 Symphony , . 1 , listened marked interest . 
 Lowe , conducted works utmost care 

 MUSIC BIRMINGHAM . 
 ( CORRESPONDENT 

 eighth Halford Concert , held Town Hall 
 Ist ult . , introduced interesting novelty , Symphonic 
 Poem . 4 William Wallace , performed 
 Philharmonic Society , London , March , 1901 . com- 
 poser conducted , work 
 cordially received . Mr. Halford directed fine performances 
 Mendelssohn Italian Symphony Beethoven 
 ‘ Egmont ’ Overture , Miss Kate Cherry successful 
 appearance excerpts Wagner works . 
 ninth concert , 15th ult . , Dr. Cowen conducted 
 performance ‘ Indian Rhapsody , ’ 
 Festival apart , performance Orchestral 
 Poem ‘ Phantasy Life Love . ’ splendidly 
 played , composer came great ovation . 
 Mr. Leonard Borwick gave artistic rendering 
 solo Second Pianoforte Concerto Brahms ( 
 new ) , concert terminated brilliant 
 performance Tschaikovsky ‘ Francesca da Rimini 

 21st ult . Birmingham Amateur Orchestral 
 Society gave interesting concert Midland 
 Institute . programme comprised   Rubinstein 
 Humoreske , ‘ Don Quixote ’ ( time Birmingham ) , 
 Idyl , ‘ Springtime , ’ A. Herbert Brewer , Liszt Symphonic 
 Poem ‘ Les Préludes , ’ Beethoven Violin Concerto , 
 Miss Margaret Holloway soloist . Mr. Granville Bantock 
 conducted 

 Harrison Concert season took place 
 Town Hall 7th ult . vocalists Madame 
 Melba , Miss Carrie James ( local contralto ) , Mr. William 
 Green , Mr. Robert Radford . Madame Melba 
 brilliant voice naturally centre attraction . 
 Successful débuts Mlle . Sassoli , youthful 
 harpist , Miss Kathleen Chabot , clever pianist , pupil 
 Miss Fanny Davies , Mr. Rohan Clensy , Irish 
 violinist 

 3rd ult . pianoforte recital given 
 Masonic Hall Miss Kathleen Arnold , pupil 
 Mics Fanny Davies . _ interesting number _ 
 programme Arensky Suite Pianofortes , played 
 ladies named , time . 
 Mr. Francis Harford vocalist 

 Mr. Max Mossel drawing - room concerts 
 held Grosvenor Rooms Grand Hotel 17th 
 ult . Mr. Philip Halstead , pianist Glasgow , 
 appearance city , Mr. Max Mossel 
 played César Franck Duo Sonata Beethoven 

 Kreutzer ’ Sonata . Madame Kirkby Lunn sang , 
 time England , tragic song - cycle , ‘ Schon 
 Gretlein ” ’ Liibeck ~Kapellmeister , Alexander 

 MUSIC DUBLIN . 
 ( CORRESPONDENT 

 fourth concerts Dublin Orchestral 
 Society given January 28 February 25 . 
 music performed included Beethoven Triple Concerto , 
 played time concerts , soloists 
 Miss Bessie Ruthven ( pianoforte ) , Herr Adolf 
 Wilhelm ] ( violin ) , Mr. Clyde Twelvetrees ( violoncello ) ; 
 Tschaikovsky ‘ Pathetic ” Symphony ; Elgar ‘ Grania 
 Diarmid ’ Funeral March ; Beethoven Seventh Symphony : 
 Mackenzie Orchestral Ballad ‘ La belle dame sans merci ” : 
 Fantasia Pianofortes Orchestra Signor 
 Esposito ( solo parts played 
 Miss Annie Lord Miss Edith French ) ; * Ballet 
 des Sylphes * ‘ Marche Hongroise *   Berlioz 
 ‘ Faust . ’ Signor Esposito conducted usual skill 

 Orpheus Choral Society second concert Dr. 
 Culwick presented fine selection madrigals and_part- 
 songs , including Thomas Morley ‘ Shoot , false love , care 
 , Dudley Buck ‘ Ifymn Music , ” elegy 
 ‘ Lycidas , ” written memory Sir Robert Stewart 

 MUSIC EDINBURGH . 
 ( CORRESPONDENT 

 chief feature Amateur Orchestral Society 
 second concert February 17 Choral Fantasia 
 Beethoven . Society valuable co - operation 
 Edinburgh Choral Union , solo pianoforte 
 brilliantly played Mr. Alfred Hollins . programme 
 included ‘ Jupiter ’ Symphony ‘ Zauberflote * 
 Overture , Sullivan ‘ Merchant Venice ’ music . 
 tactful inspiring beat Mr. Collinson Society 
 showed greatest advantage , concert 
 extremely enjoyable 

 Year year University Musical Society takes 
 important position Edinburgh organizations , 
 reason growing membership sterling 
 worth achievements . performance Society 
 reached high - water mark Hiller ‘ Song Victory ” 
 Goring Thomas ‘ Sun - Worshippers , 
 chief works given annual concert 4th inst . 
 hymn ‘ Morning Evening ’ sung memory 
 late Professor Sir Herbert Oakeley , 
 impressive opening . soloists Miss Evangeline 
 Florence Mr. Ben Davies , accompaniments 
 played admirable taste section Scottish 
 Orchestra Mr. Daeblitz leader , Mr. Collinson 
 pianoforte 

 Vale Leven Choral Union , reorganized _ set 
 - going lapse years , performed Haydn 
 ‘ Creation ’ 2nd ult . tone new choir 
 fresh excellent quality , male voices 
 better usually found small choirs . Haydn 
 tuneful work received fairly good interpretation . feature 
 performance unusually fine singing solo 
 music Madame Bertha Rossow Messrs. Webster 
 Millar Fowler Burton . Mr. William Blakeley con- 
 ducted 

 3rd ult . Dumbarton Choral Union , 
 Mr. Edwin Owston direction , essayed T. Mee Pattison 
 ‘ Song Bell ’ miscellaneous pieces 
 success . 8th ult . Glasgow Sabbath School Union 
 Choir ( conductor Mr. Alec Steven ) gave praise- 
 worthy rendering Haydn ‘ Creation . ’ TheChoir , composed 
 chiefly teachers engaged Sunday School 
 work , numerically strong fairly balanced , 
 choruses sung great spirit good attack . 
 solo music given Miss Jenny Taggart Messrs. 
 Adams Burnett ; efficient orchestra led Mr. Sieg ] 
 supplemented skilfully Mr. Berry organ gave 
 accompaniments effectively . second concert 
 season Glasgow Amateur Orchestral Society 
 14th ult . best given body 
 accomplished amateurs . programme included 
 Overture ‘ Magic Flute , ’ Beethoven Overture 
 ‘ Leonore * . 3 , love scene Berlioz ‘ Romeo 
 Juliet , ’ Liszt Symphonic Poem ‘ Preludes , ’ Bach 
 Concerto D minor violins accompaniment 
 string orchestra . Berlioz Liszt numbers best 
 played , performance high order . 
 solo parts Concerto beautifully played 
 Mr. Henri Verbrugghen , accomplished leader 
 Scottish Orchestra , Master Bertie McGrath , youthful 
 violinist . mark _ performer . 
 special interest attaches concert , completes 
 Mr. W. T. Hoeck majority conductor Society . 
 Miss Barbara Kerr , young contralto vocalist 
 promise , sang songs remarkably 

 Cecilian Orchestra ! Society , combination , 
 understand , originated years ago , 
 gave creditable performance Mr. E. R. Joachim 

 baton 17th ult . Considering /evsonne/ 
 band , programme , included Beethoven 
 Symphony Mendelssohn Capriccio Brillant B minor 
 ( pianoforte soloist , Mr. W. M. Turnbull ) , somewhat 
 ambitious , respects performance 
 remarkably good . Miss Isobel M. Kendrick contributed 
 vocal solos 

 energetic direction Mr. R. L. Reid 
 Choral Institute connected Young Men Christian 
 Association gave Handel ‘ Judas Maccabus ’ 
 18th ult . choruses sung praiseworthy 
 correctness , lost effect 
 taken quickly , performance 
 highly creditable concerned . efficient orchestra , 
 Mr. Hutton Malcolm organist , played accom- 
 paniments , soloists Miss Maggie Jaques bore 
 honours 

 brilliant programme associated 
 Richter concerts , took place Philharmonic 
 Hall 2nd ult . Prominence given Strauss 
 ‘ sprach Zarathustra , ’ performed time 

 Liverpool .   Beethoven ‘ Leonore ’ Overture . 2 , 
 prelude final scene ‘ Tristan und Isolde , ’ 
 Dr. Elgar ‘ Cockaigne ” Overture included 

 programme 

 tenth concert - fifth season Philhar- 
 monic Society occurred February 23 , programme 
 included Grail music ‘ Parsifal , ’ Beethoven 
 Symphony , Strauss ‘ Tod und Verklarung , ’ last- 
 named performed tirne concerts . Miss 
 Muriel Foster sang art finish Saint - Saéns 
 ballade ‘ Le Fiancée du Timballier . ” eleventh concert 
 took place fortnight later , M. Raoul Pugno solo 
 pianist ( Grieg Pianoforte Concerto ) Mr. John Coates 
 vocalist . Dr. Cowen occupied usual post conductor 
 concerts 

 excellent concert given auspices 
 Cymric Vocal Union 12th ult . , Welshmen 
 valued assistance Liscard Orchestal Society . 
 Miss Helen Jaxon Mr. Emlyn Davies sang 
 desirable success 

 February occurred 
 passed unnoticed neighbourhood , 
 believe , unnoticed rest musical England , 
 new departure great importance . Ata 
 meeting Manchester Municipal Council grants 
 educational institutions , 
 grant £ 300 year Royal Manchester College 
 Music . opposed resolution meeting , 
 party opposed unquestionably exists , 
 letters having appeared local press written 
 persons posing champions ‘ profession ’ general 
 raising objection considerthe unfair advantage 
 given College . curious point 
 writers said letters case opposed 
 particular Institution privileges , 
 sort higher education music , ghost 
 attempt having apart 
 College higher education exists 
 neighbourhood . old cry raised injustice 
 Manchester having pay students come 
 distance . far ascertain , case 
 money British municipality voted 
 furtherance musical art — 
 Birmingham musical Midland 
 Institute 

 seventeenth Hallé concert , February 25 , 
 remarkably fine rendering given Elgar ‘ Apostles , ’ 
 performers — orchestra , choir , organist , conductor , 
 soloists — recent Elgar Festival 
 London . following week Mr. Godowsky played 
 consummate mastery Tschaikovsky B flat minor 
 Concerto Chopin C sharp minor Scherzo . 
 purely orchestral pieces Mendelssohn 
 ‘ Meeresstille ’ Overture , Parry Symphonic Variations , 
 Beethoven major Symphony , audience 
 taking care demonstrate special affection 
 - named work appreciation Dr. 
 Richter . monumental rendering . Strauss ‘ Zarathustra ’ 
 repeated nineteenth concert ( 1oth ult . ) 
 received genuine warmth , occasion 
 Strauss 
 received — concerts . Dr. Brodsky played 
 Busoni Violin Concerto , Tschaikovsky ‘ Manfred ’ 
 given probably time 
 neighbe uurhood . Hallé concert subscrip- 
 tion series Beethoven programme ending 
 Choral Symphony , ‘ magnificently 
 orchestra choir , conducted Dr. Richter 
 sacerdotal impressiveness . j 

 seventh Gentlemen Concert , Ist ult . , 
 light agile soprano Miss Lillie Wormald displayed 
 airs * Magic Flute ’ ‘ Figaro ’ David 
 * Couplets du Mysoli ’ voice flute duet , 
 lighter orchestral pieces charmingly played . 
 fourth Schiller Anstalt concert , 12th ult . , Mr. Borwick 
 played Beethoven Sonata selection Brahms 
 Waltzes , taking pianoforte Trio 
 Smetana exemplifying unsatisfactory application 
 Lisztian principles concerted chamber music . singer 
 Miss Schiinemann , unknown London 
 heard , gave fine performance 
 songs Schubert , Brahms , Hugo Wolf , voice 
 rich expressive contralto.——At fifth 
 Brodsky concert roth ult . fine rendering 
 given Dr. Brodsky Messrs. Speelman , Fuchs , 
 Cohn Richard Strauss Quartet pianoforte 
 strings . Mozart B flat major Beethoven second 
 Rasoumovsky string Quartets . 15th ult . 
 Miss Edith Robinson gave interesting recital older 
 violin music , programme including examples Purcell , 
 Senaillé , Leéclair , Corelli , Tartini . Vocal selections 
 sung Mr. Plunket Greene agreeably diversified concert , 
 thoroughly successful 

 _ important feature recent Ladies ’ Concerts 
 Midland Hall playing Moscow Trio 
 8th ult . , considerable impression , 
 especially Haydn Trio flat B major minor . — — 
 series concerts organized Dr. Bradley behalf 

 MUSIC OXFORD . 
 ( CORRESPONDENT 

 February 4 , Town Hall , auspices 
 Musical Club , Schubert * Unfinished ’ Symphony 
 B minor , Beethoven second Symphony 
 played Dr. Allen local orchestra . programme 
 included Mozart - welcome Pianoforte Concerto 
 major ( solo admirably played Mr. 
 Leonard Borwick ) Brahms rarely - heard chorus 
 * Nanie 

 February 17 , Sheldonian , Sir Hubert Parry 
 discoursed * Types Audience influence . ’ 
 lecture appreciated 

 MUSIC SHEFFIELD DISTRICT . 
 ( CORRESPONDENT 

 Shetfield Chamber Music Society scored local 
 success 8th ult . , Mrs. Mountain pianoforte- 
 quartet party plaved Mozart G minor Beethoven E flat 
 ( Op . 16 ) . Mrs. Mountain ( associates Mr. John 
 Peck , Mr. Herbert Parkin , Mr. Maurice Taylor ) 
 co - operated Mr. Peck Mozart Sonata G major 
 pianoforte violin 

 district suburban concerts given 
 earlier days month mentioned excellent 
 perforinance Benedict ‘ St. Peter ’ Rawmarsh 
 Parkgate Choral Union Mr. A. E. Simmonite 
 direction ; successful rendering ‘ Elijah * Ann Road 
 Church direction Mr. Maurice Tomlinson , 
 performance Handel ‘ Samson ’ Wycliffe Congregational 
 Church conducted Mr. Reeves Charlesworth 

 BRADFORD 

 performance ‘ Dream Gerontius ” 
 subscription concert 11th ult . sensation 
 month . Mr. John Coates interpreted 
 identified essayed 
 Worcester sympathetically dramatically . 
 Miss Alice Lakin sang music Age/ conscien- 
 tiously , Mr. Iver Foster performance bass parts 
 marked distinct step advance career . chorus 
 Festival Choral Society sang admirable correctness 
 remarkable spirit , hardly succeeded realizing 
 mystical atmosphere work . Hallé 
 Orchestra course excellent , Dr. Cowen conducted 
 - directed energy . Ist ult . Bradford Old 
 Choral Society wound season quadruple bill , 
 including Brahms ‘ Song Destiny , ’ Elgar ‘ Banner 
 St. George , ’ Parry * Blest Pair Sirens , ’ Stanford 
 * Phaudrig Crohoore . ’ garnish ‘ Egmont ’ 
 Overture , way curious intermezzo , Mendelssohn 
 D minor Pianoforte Trio , place 
 surroundings . choir , Mr. Fitton conductorship , 
 best Elgar work , sung fresh- 
 ness intelligence . Permanent Orchestra 
 concerts recorded . 5th ult . Mozart G minor 
 Symphony Beethoven greatest ‘ Leonore ’ Overture 
 played Mr. Allen Gill direction , Mr. Arthur 
 Payne contributing violin solos , toth ult . 
 popular * Sullivan programme ’ arranged help 
 Festival Choral Society . Miss M. Klepper , 
 Bradford pianist gifted ambitious , gave 
 rth ult . highly interesting Chopin recital , showing 
 great technical powers genuine artistic refinement 
 feeling . occasion promising young 
 soprano appearance , Miss Ethel Lister , fine 
 voice dramatic power , combined attractive 
 presence , speak future 

 HUDDERSFIELD 

 BLACKBURN.—The concert 29th season 
 St. Cecilia Vocal Union given Exchange 
 Hall 4th ult . band — selected Hallé 
 Orchestra — chorus numbered nearly 300 performers . 
 Mendelssohn ‘ hart pants ’ ‘ Song 
 Destiny ’ ( Brahms ) occupied half programme , 
 works given refinement accuracy . 
 chorus sang - songs ‘ Dawn 
 Song ’ ( Bairstow ) ‘ Echoes ’ ( Sullivan ) . Miss Agnes 
 Nicholls solo vocalist . band loudly 
 applauded able renderings Grieg ‘ Peer Gynt ’ Suite , 
 Hungarian Dances Nos . 5 6 ( Brahms ) . Dr. E. C. 
 Bairstow conducted conspicuous ability 

 BoURNEMOUTH.—The Winter Gardens Symphony Concerts , 
 able enterprising direction Mr. Dan Godfrey , 
 continuing successful course . programme 
 February 29 included Beethoven ‘ King Stephen ’ Overture , 
 Grieg Suite , ‘ Sigurd Jorsalfar , ’ Briill Symphony E minor 
 Op . 31 ) , notably Concertstiick minor piano- 
 forte orchestra composed Mr. Tobias Matthay ( 
 solo played Mr. York Bowen ) , achieved great 
 success 

 CAMBUSLANG.—The Choral Society gave performance 
 parts Coleridge - Taylor * Hiawatha ” Elgar 
 ‘ Banner St. George ’ 22nd _ ult . , Institute 
 Hall . choir sang efficiently soloists 
 Miss Mary EF . Alexander , Mr. Tom Child Mr. J. Bayne 
 Young . accompaniments efficiently played 
 Mr. W. H. Cole orchestra . Mr. Herbert Walton , 
 Glasgow Cathedral , conductor 

 rTHE MUSICAL TIMES.—ApRriL 1 , 1904 . 267 
 j Tr - wa 7 
 ANTHEMS ASCENSIONTIDE 

 praise majesty Mendelssohn 14d . * Let ( - arrangement , siege Myles B. Foster 3d . 
 * Achieved glorious work .. Haydn 1d . * Let celestial concerts unite . ats ie Handel 14d . 
 * Achieved glorious work ( Chorus ) Haydn 14d . * Lift heads sa Handel fe : 
 * glory Lamb Spohr 14d . * Lift heads S. 5 . Coleridge -Taylor 3d . 
 Awake glory M. Wise 3d . * Lift heads Pe J. L. Hopkins 14d . 
 * Christ obedient unto death | . vale F. Bridge 14d . O , ye people , clap hands H. Purcell 3d . 
 Christ entered Holy Places . Eaton Faning 14d . | * O clap hands ... J.Stainer 6d . 
 < ane aes .. Oliver King 14d . Oclap hands T.T. Trimnell 3d . 
 * God gone . Croft 4d . * O God , King Glory .. H.Smart 4d . 
 * God , King .. Bach 14d . | * O God , Thou mt . Mozart 3d . 
 Grant , beseech Thee . H. Lahee 14d , | * O amiable : : .. J. Barnby 2d . 
 * Hallelujah unto God Almighty Son Beethoven 3d . | * O Lord Governour ... . H. Gadsby 3d . 
 * excellent Thy , O Lord Handel 14d . ' O Lord Governour ... Marcello 14d . 
 * ye risen Christ . Ivor Atkins 4d . * O risen Lord J. Barnby 14d . 
 ye risen : . J.Naylor 3d . | * Open gates . F.Adlam 4d . 
 ye risen ( Parts ) Myles B. Foster 3d . * Rejoice Lord aes mi Baptiste Calkin 3d . 
 Father house ase H. Elliot Button 3d . ‘ earth Lord ... T. T. Trimnell 4d . 
 Father house ... J. Maude Crament 3d . * Lord exalted . J. E. West 14d . 
 day aes Elvey 8d . Lord King H. Gadsby 6d . 
 * shall come pass B. Tours 14d . Lord King .. H.J. King 4d . 
 * King glorious ... J. Barnby 6d . * Unfold , ye portals .. Ch . Gounod 6d . 
 + Leave , forsake J. Stainer 14d . Thou reignest . Schubert 3d . 
 Let heart troubled ( Double Chorus , unaccompanied ) weak helpless Rayner 2d 

 Myles B. Foster 3d . 
 * . * Anthems marked ( * ) Tonic Sol - fa , 1d . 2d . . 
 ry Thi 
 WHITSUNT es 
 people saw ... , J. Stainer 6d . shall come pass G. Garrett 6d 

 Secretary Guildhall School Music 

 BEETHOVEN . J. S. SHEDLOCK 

 GULLIVER 

 Responses Commandments selected 
 Services composers Myles B. Foster , 
 Dr. Garrett , Ch . Gounod , Dr. E. J. Hopkins , Dr. C. H. Lloyd , 
 Merbecke , H. Smart , John E. West , S. S. Wesley , 

 Sir John Stainer Sevenfold Amen included , Vesper 
 Hymns Beethoven , Sullivan , ; concluding 
 Vestry Prayers S. S. Wesley Rev. Canon Hervey 

 PREFACE 

 s 
 BACH.—Sonata ( C minor ) . Violin Bass . Transcribed 
 Pianoforte Solo B. H. REINHOLD net 3 0 
 BADING , H.—Berceuse(in B ) . Clarinet Pianoforte 

 Ope | x ass aaa dan 
 BEETHOVEN.—Allegro Violoncello Sonata ( Op . 109 ) . 
 Transcribed Harmonium Pianoforte REINHARD 
 net 3 6 

 BRANDTS - BUYS , J.—Quintet ( D major ) . Flute , 
 Violins , Viola , Violoncello 

