ischa Elman play fine Amati violin . play Court , visit 
 repertoire large , e.g. : king queen Spain . King Edward genially 

 rtos.—Bach minor ; Brahms ; Bruch g minor 
 ) minor ; Beethoven ; Glazounoff minor ; Lalo 
 1 ) minor ; Mendelssohn ; Mozart ; Paganini D ; 
 Spohr minor ( Gesangscene ) ; Saint - Saéns b minor ; 
 Tchaikovsky ; Viotti minor , . 22 ; Wieniawski , xc . 
 neertstiicke . — Romances F G , Beethoven ; 
 Mclodies Hongroises Othello Fantasia , Ernst ; Suite , 
 Mackenzie ; Suite minor , Sinding ; Rondo Capriccioso , 
 Saint - Saéns ; Faust Fantasia , Wieniawski , & c. 
 Sonata Bach , Beethoven , Brahms , Corelli , 
 Handel , Mozart , Senaille , Tartini 

 Grieg 

 season succession — unprecedented 
 honour — play Gewandhaus Concerts , 
 Leipzig 

 remarkable thing Mischa 
 playing , especially young , 
 mood interpret composition 
 opposite character . concerto Beethoven , 
 Brahms Tchaikovsky — different 
 atmosphere , young Elman breathe , 
 speak , natural air play 
 marvellous technique , absolute certainty 
 ( memory ) rare poetic insight , 
 masterpiece . play baton 
 great conductor Europe . testimony 
 distinguished quote . 
 Herr Arthur Nikisch enthusiastic 
 close rehearsal Tchaikovsky concerto 
 : ' , dear Mischa , 
 concert play 
 , wish play . ' 
 concert : ' play 
 concerto twice rehearsal ; 
 think play like . ' 
 Dr. Richter regard Brahms 
 concerto ? artist ’ room 
 conduct Strauss tone - poem , rush 
 boy : ' beautiful — 
 proud , dear Elman ; 

 chat boy minute 
 career , evince great interest 
 performance : time play 
 Queen Alexandra . ask hobby , 
 Mischa reply , ' motoring . ' motor car 
 luxury Elman family indulge , 
 live quietly modest house . regard 
 english music , Mischa , ' greatly admire 
 Elgar : regret Elgar write 
 violin . wish compose 
 concerto , great success . ' 
 \lthough gifted youth hitherto 
 play Europe , eagerly await America 
 Australia . continent hope soon 
 , add fame genius 

 22 October , 1877 

 prospectus ninety - sixth season 
 Philharmonic Society issue . 
 actual novelty present : Viola concerto 
 York Bowen , Violin concerto Hubay , 
 vocal scena ( ' bell ' ) Franco Leoni , 
 Symphony C Sibelius , 
 conduct composer . symphony 
 Beethoven C minor , Dvorak ' New 
 World , ' Schubert C , Schumann d minor c , 
 Tchaikovsky e minor . overture 
 play Egmont Leonora . 2 , 
 Beethoven ) , Academic ( Brahms ) , Comedy ( Harty ) , 
 fly Dutchman ( Wagner ) . orchestral music 
 far exemplify Brandenburger 
 concerto string , . 3 ( Bach ) , prelude 
 ' Sappho ' ( Bantock ) , symphonic variation ( Dvorak ) , 
 variation original theme ( Elgar ) , spring 
 Winter , symphonic suite ' season ' 
 German ) , funeral march ( Grieg ) , rhapsody . 1 
 Liszt ) , symphonic poem ' Finlandia ' ( Sibelius ) 
 ' Till Eulenspiegel ' ( Strauss ) , ' death apotheosis ' 
 Strauss ) , introduction closing scene 
 ' Tristan , ' Waldweben ' Siegfried ' ( Wagner ) . 
 addition mention , concerto 
 portion scheme consist Brahms b flat 
 D’Albert e flat , Schumann minor , 
 pianoforte ; Bach violin ; Mozart 
 represent Serenade nocturne . 6 , 
 D , string quartet orchestra . Mendelssohn 
 idol Philharmonic Society , find 
 place prospectus 

 M. Claude Debussy announce conduct 
 orchestral nocturne Queens Hall 
 Symphony concert February 1 . ' , 
 public appearance England , undoubtedly 
 stimulate interest work 
 original fascinating composer day . 
 performance London 
 work M. Debussy know repute 
 ' La Demoiselle Elue ' ' Pelléas et Mélisande ' ; 
 ' L’Aprés - midi d’un Faune ' favourite 
 piece Queen Hall english perform- 
 ance August 20 , 1904 . London 
 critic , comment performance , 
 impression ' faune ' mean fawn , 
 facetiously inquire afternoon thought 

 it| year ago | spend week - end Ripon 

 set forth chronological form , accuse | acquaintance Dr. Crow , 
 date . /i’erhaps Directors | kindly invite round Cathedral 
 ( ueen Hall ( rchestra , Limited , way | close Sunday afternoon service . 
 issue interesting important catalogue in| intelligent young verger cicerone 
 alphabetical order composer . start peregrination Americans , 
 | stay hotel , ask join . 
 | pause verger description , 
 report Vienna letter of| stand Choir , senior american 
 Beethoven ( - number ) , | lady point lectern , " think 
 - seven page manuscript music hand-| cathedral decent bird , 
 writing composer , discover in| miserable crow like ! " nasal - toned matron 
 austrian capital . letter , to| Atlantic idea 
 write year 1816 1823,|the organist ( Dr. Crow ) stand , 
 likely prove specially interesting discovery | perch lectern , organ 

 authenticate . | seat cathedral 

 S 

 History Music England , ' | workmanship violin pianoforte 
 " advantage . accompaniment arrange old figure - bass 
 — _ — deserve high commendation . 
 number series Trio - Sonata 
 N MUSIC . violin , violoncello ( ad /#4 . ) pianoforte celebrated 
 setae , composer immortal ' Rule , Britannia , ' Dr. Thomas 
 ( j 2 . y Alfred Mofiatt . . ~ . 
 ‘ ad ait enalen . tor tn sien Wie ie . Augustine Arne . Sonata probably date year 
 , oe eee ae : " 4 g » Sonata ! 1740 , set seven similar work . 
 d , Henry Eccles : . 3 , sonata b flat ; _ . . , apse 
 bn 4 contain movements—.S7cz / iano , Modera Largo , ar 
 r , William Babell ; . 4 , Sonata g minor , : . " ' " th 
 oe Deets = Amaaies , tr Sob Caltett : prove effective 
 ' Trio - ak , toe violoncello , additional interest 
 : . ee . inclusion instrument . 
 y inc rur 
 . Ltd real pleasure study fascinating old 
 er ancier 1 rich delightful store | work . dull tedious passage beginning 
 ed English Violin Music edit | end , comment pure , healthy 
 Mr. Alfred M number series ous joyous tone display english composer 
 n publist succeed work maintain | 17th 18th century . thank Mr. 
 e sa h level excellence set piece } Moffatt rescue example musty dusty 
 ! f great iulue _ historical | recess museum bring light 
 r Editor remark , considerable attention | present day . stand glare ! 
 er scitation church music , glees 
 ir rga eve sichord music old 
 poser little s 
 ‘ m violin writter vy English ! 4 //istory B Festival ¢ t ] Sock 
 1 cla r ‘ f Purcell close formation 1856 jubilee 1906 . G. F. Sewell . 
 rn en es oii Bradford : G. F. Sewell 
 r rs r unfamiliar author interesting book vetera 
 \ graphic h preface piece , | Yorkshire music - lover wl uny county pr 
 r 710 - 77 , al y Oxford . | original member Bradford Festival 
 et master | Choral Society , fitting write histor 
 l W ; g concer task _ discharg commendable skill , 
 ! " cred year hold a/| ¢ handicapped early 
 Blenheim , | record , cover year , having k 
 ' ) Mar rougl Sonata der t secretary , - ye 
 rr r ° 3 s ' n » origin enthusiasm 
 r bh | rt harpsichord , ' | late Samuel Smit \i ir Bradford 18490 , whe 
 . consi g ro,| Mendelssohn ' Elijah ' perform ( com 
 ( n energe Bradford December 7 ¢ year , wi ler 
 , ! nearly fill factory operative . T 
 S D r Henry Eccles , rn | St. George Hall , st 27 , 1853 , s 
 r King Band fro 7 16.| choral music Bradford , d tl Festival ( ¢ 
 r t r par d r e | duly form 155 
 ! ] 3 ' ited e president Mr. Samuel Smit ! firs 
 t Par wher ! $ n | conductor , William Jack . Masham , 1 fi 
 , Hie wr r wor hilling ars 1 ! 
 t r ! " ¢ e n ind guinea ft 
 ALT 2 ) Society * Coftee n provide 
 \ ¢ ' Society fund member e choir « 
 { r rom stance , « f refreshment year 
 vor mount t e sui 1 i¢ 1858s Soe 
 N S | r W um | l proud dis befor n Victor 
 Dr. | Ww ( rt | Pala r concer 
 rs AW H vs ( r Bread Stree \ rchestra wa ve $ 62 se Bradford singer 
 ( ! Geor Fir 1 th nour r | ral Symp 
 H : \ orkshire Mar } 3 rhe pitfall 
 t ¢ r ! \ r r makir y " V vynen , 2 
 n Burme sador Bradford 1 cencert w 
 ¢ | { r r. ) gran 
 s e , Brita nd * ¢ 
 Air , hor f e soci \ \w 
 r r short | $ r neé cor * 7 
 dr rtive , vear , ' Deliverar ist work ) 
 ' ] S. Bur , » Edw Ile = : ee Bridge , 
 " s r s \ Mr. R. H. Wils pr nt conductor Dr. F. H 
 ! r s Cowe S \ lead contingent singer 
 1 , estival rd , Gl er Chester , d 1 
 ' r t lee year Soc sing , invitation , Lond 
 Collet N lhar ic Soci ncert 17 , 1906 , whe 
 red cernir lly maintainec reputation Bach motet ' sing jy 
 r r t Lord inact panie ) , Beethoven Chor 
 f rt Symphony Society autograph 

 Schubert 
 detail 

 f 

 t inthem ly Bach , editorial skil ] | 42 interesting important book entitle ' Military Music 
 Mr. John P. - cantata ' Gott lob ! | @ history wind - instrumental band , Messrs. Boosey 
 Nun geht Jahr zu ende . ' _ - writing | publish 1894 . 
 evt lv interesting 0 h largely c t f . 
 ee rp eg * = ff aoe te test phouse| December 9 , Mont reux , O1 ro Pi NIGER , aged 
 nelody commonly know ' Old rooth ' Psalm past pom aative " pe — _ = ft — aa ior ~ 
 te aa ea > = enecessively | Paris Leipzig , pupil Joachim . 1572 
 pro ccowe ux seas a. 4 . | violin master Harrow Saas . addition 
 composer subsequently start new theme | ' ® d€!ng excellent violinist teacher , Mr. Deiniger 
 ' great skill -and fluency author - know method violin , 
 1 yn reach . 9 compose piece instrument . 
 , . na r 1 posed pra aan — following - know foreign musician die 
 , ] } . e close vear GAETANO BRA \ , age seventy , 
 gr richly harmonize lay : * ' 
 voice . conclusion impressive composer famous * Serer BAPTIST 
 , , . — . CHARLES DANCLA , Tunis , age t famous 
 ext anthem Sir Edward aaa TAFFER ? " 3 
 Igar Rev. Henry Collins . prayer iesetat aga 2 ; alte d ‘ 
 rgiveness intercession , deep devotional spirit aged seven y seven , listinguisnec writer } e Sica 
 nes echo reverent feeling music , — author Ws ae ve eho ! haley 
 passage intensity fervour . | ° ° " ! ine impolite opprobrious term t y 
 word tl second anthem J ] . Cummins , enemy Wagner 
 ure lie music confident character , 
 al spirit evident , fine climax build 
 wards t close , , , come whisper 
 rever : ae Choral Union ga npressiv 
 performa e Brahms german Requiem December 3 , 
 receive . work hear time t Nort ! 
 d th rna Sir Ge Smart . | Scotland . soloist Miss Betty Booker 
 H. Bertram Co nad C. L. . Co portrait | Mr. Francis Harford , Mr. Arthur Collingwood conduct . 
 le Beethoven canon . i’p . xi . 252 + 105 . 6d . n December 13 University Society concert 
 ans , Green & Co. ) ofits season . addition choral item t rogramime 
 d d Sch mn . select edit | include Schubert ' Fierrabras ' Mackenzie ' | 
 Dr. Karl Storck ; translate Hannah Bryant . pp . ix Minister ’ overture : Elgar ' Sursum Corda ' organ , string 
 , . ( John Murray brass ; cf Mozart G minor symphony ; und Dyvor 
 Ja ta Edward Algernon Baughan . | ' Sclavische tinze , ' set 1 , . 4 Professor C. Sanford 
 b 2 . ( net . ( John Lane . ) Terry conduct \ feature concert recently u 
 y year ’ rien é zan rle lea 1c playing . \berdeen Scottish Orchestra performance 
 Oscar Beringer . pp . 723 ; . 72 Bosworth & Co. ) Max Bruch Violin concerto Mr. G. S. Mackay , 
 lay 2 i//s prove Vice - Chancellor | member ( uec n Hall Orchestra Dr. Frederi 
 ' ut Cambridge , 1501 - 1765 . pp 73 : ss . wet . | Cowen ably conduct fine programme w cl lud 

 Cambridge Hi . Roberts . ) | Mozart ' Splendente Te , deus ' sing Choral Union 

 score 

 complete command orchestra . 
 apply fine performance Tchaikovsky ' pathetic ' 
 symphony overture ' Tannhiuser . ' M. Jacques 
 Pintel great impression performance 
 Schumann minor Pianoforte concerto . considerable 
 fluency delicacy display , hardly 
 poetry charm - know concerto 
 yield . interesting feature concert 
 performance soli wind Beethoven * Kondino ’ 
 oboe , clarinet , bassoon horn — instrument . 
 playing exquisite . concert December 16 , 
 Iluddersfield Choir , separately 

 20 

 young performer . Lengyel , fourteen 

 excit iardly wonder masterly interpretation 
 Bach , Haydn , Beethoven , Mozart , Chopin . 
 second Liszt 

 numerous recital place 
 past month , remarkable Mr. Ysaye 
 December 4 11 ( Jueen Hall . brother , 
 Mr. Théophile Ysaye pianoforte , belgian virtuoso 
 occasion superb interpretation violin 

 mar . hand symphony e flat 
 Carl Ph . Emanuel Bach , hitherto unknown , produce 
 Gesellschaft der Musikfreunde , prove attractive . 
 lover music wind instrument spend pleasant 
 evening chamber concert arrange Ary van 
 Leeuwen , flautist , colleague Opera 
 Bach cantata accompaniment 
 flute , chamber duet Handel oboe , trio 

 pianoforte , flute , bassoon Beethoven write Bonn 

 young clay , Haydn ' Divertimento ' wind 
 instrument — occur ' St. Antoni ' Chorale 
 Brahms write masterly variation -- 
 admirably render . Emil Sauer farewell concert 
 fine 
 display virtuosity , , fact , admire reproductive 
 creative artist 

 MUSIC BELFAST . 
 ( correspondent 

 Brodsky Quartet Manchester chamber 
 concert Queen College Society December 6 7 
 concert Mr. S. Speelman 4 romance 
 viola solo compose brother , perfect 
 master instrument justice composition , 
 tunefui musicianly work . Grieg 
 string quartet play concert , Beethoven 
 ( ) uartet e flat ( Op . 127 ) fiéce de résistance 
 second concert , difficulty place 
 reach ordinary player , splendid playing 
 master reveal beauty time 
 audience . Madame G. Drinkwater Miss F. Moore 
 respective vocalist enjoyable music - making 

 Hallé Orchestra , Dr. Hans Richter , 
 interesting orchestral concert December 10 . pro 
 gramme — ' advanced , ' , audience 

 programme able conductor . choir 
 orchestra acquit praiseworthy manner , care 
 having augment orchestra contingent 
 professional instrumentalist . special mention 
 cf fine tone - quality , attack , phrasing 
 choir . aid render principal artist , Miss 
 Alice Venning , Madame Marguerite Gell , Mr. Edwin 
 Spooner Mr. William Evans , , 
 satisfactory . Mr. C. W. Perkins occupy accustomed 
 post organist . e programme comprise Colerid 
 Taylor ' Death Minnehaha , ' choir 
 orchestra thoroughly familiar . purely orchestral 
 include ven overture ' Egmont , ' 
 Grieg ' Peer Gynt ' suite . 1 

 passing reference 1 
 concert highly artistic nature 
 recently hold . second Harrison concert 
 principally noteworthy account apy 
 ance M. Edouard de Reszke little Vin 
 Chartres , clever violin prodigy . Dr. Hans Richter 
 und Hallé band executive concert 
 season promote Birmingham Orchestral 
 Concerts Society , Town Hall Decem 4 . 
 Strauss tone - poem ' sprach Zarathustra ' receive 
 ideal interpretation , chief feature magnificent 
 rendering Beethoven C minor symphony . _ vocalist 
 Mr. Webster Millar . Birmingham Concerts Society 
 og : ncert Town Hall — November 26 
 December 10 — conduct Mr. George Halford . 
 , Mr. Arthur Cooke , clever young English 
 create sensation Liszt ' pathetic 
 Concerto , ' second Mr. Johann Hock , dutch 
 violoncellist , splendid rendering Dvorak 
 principal orchestral item 
 Tchaikovsky ' italian 

 rf 
 c 

 ioloncello concerto 

 Beethoven Fourth symp 

 apriccio . ' great interest Felix Weingartner 
 concert Grosvenor Room December 6 , gr 
 conductor appear pianist , assist Kebner 
 String ( Quartet , programme devote 

 Lonsdale . Handsworth Orchestral Society 
 artistic concert Town Hall December 5 , 
 conductor Mr. Johann C. Hock . solo violinist S 

 Mr. William Henley , playing create enormous 
 Max Mossel drawing room concert , 
 hold Grosvenor Room December 12 , Miss Fanny 
 Davies M. Zacharewitsch finish perfor 
 ance Beethoven ' Kreutzer ' vocalist 
 Miss Edith Clegg . nineteenth annual scottish concert , 
 iven Town Hall November 30 , auspex 
 f tl ngham Midland Scottish Society , 
 Glasgow Select Choir , conduct 

 supply programme 

 Society Shirehampton commence interesting 
 concert Parish Hall . performer Mr. 
 Hubert Hunt ( violin ) , Mr. Ernest Lane ( viola ) , Mr. RK . Le 
 Duc cknall ( vivloncello ) , Mr. P. Napier Miles 

 inoforte ) . work interpret Beethoven 

 Trio D string ; waltz pianoforte 
 Brahms ( play Messrs. Miles Hunt ) , Mozcart 
 pianoforte quartet G minor . composition 
 play effect , interval Mr. Campbell 
 McInnes sing folk - song modern lyric . 

 blin Orchestral Society concert 
 season November 27 , excellency 
 Lord Lieutenant Countess Aberdeen present . 
 \ good performance Schumann Symphony 
 d minor , able direction Dr. Esposito . 
 programme include Glinka Overture ' Russlane 
 et Ludmila , ' Bach aria , Rameau ' Rigaudon , ' Waldweber 

 Siegfries 1 ' ) , Wagner , Beethoven ' Leonore ' Overture 
 N L 

 Miss Marie Dowse violin recital November 25 , 
 play Tartini ' Trille du diable ' 
 Tchaikovsky ' Sérénade mélancolique . ' gifted lady 
 assist Mr. J. C. Doyle , vocalist , Miss 
 Madeleine Moore , pianist . December 5 Miss Nan Stac 
 vocal recital , assist Mr. E. Gordon Cleather 

 44 MUSICAL TIMES.—Janvary 1 , 1908 

 chamber music recital Molesworth Hall 
 December 18 . programme include 
 quartet Beethoven Goetz , song 
 sing Miss Nettie Edwards 

 December 3 Phibsboro ’ Glee Singers ( conductor , 
 Mr. Peter Walsh ) successful concert Rotunda , 
 Mr. John MacCormack great attraction . 
 gifted tenor — bring notice 
 win gold medal Feis Ceoil competition 1903 

 North City Choral Society ( conductor , Mr. George 
 Harrison ) performance December 13 * Acis 
 Galatea ' Elgar ' Banner St. George , ' orchestral 
 accompaniment . Miss Nettie Edwards , Mr. Robert 
 Harrison Mr. Thomas Marchant soloist 

 Sunday Orchestral concert attract good 
 audience Antient Concert Rooms . 
 item Dr. Esposito conduct Beethoven fifth 
 Symphony , Mendelssohn ' italian ' symphony , Mozart 
 G minor symphony . Royal Dublin Society , 
 chamber music recital Miss Fanny 
 Davies Mr. Frederick Dawson , pianist , Kruse , 
 Nora Clench Brodsky ( ) uartet , London Trio , 
 Mr. Edwin H. Lemare , organist 

 Hallé Band concert December 9 11 , 
 conduct Dr. Richter . Dr. Brodsky play solo 
 Brahms Violin concerto , perform 
 time orchestra Dublin . chief item 
 interest Strauss ' Till Eulenspiegel , ' perform 
 time Ireland 

 MUSIC EDINBURGH . 
 ( correspondent 

 Messrs. Paterson orchestral concert , 
 November 25 , programme consist Mozart 
 Divertimento string horn ( . 15 , b flat ) , 
 Tchaikovsky ' Francesca da Rimini ' overture , March 
 ' Karelia ' suite Sibelius , Saint - Saéns 
 Pianoforte concerto , . 2,in g minor . Miss Fanny Davies 
 soloist , alike concerto group piece 
 Van den Gheyn , Chopin , Rubinstein , display 
 usual musicianly quality . fourth concert , 
 December 2 , conduct Dr. Richter , programme 
 comprise Beethoven second Symphony , overture 
 ' fly Dutchman , ' Dvorak symphonic Variations , 
 Richard Strauss ' Tod und Verklarung , ' Grieg 
 ' Peer Gynt ' suite . fifth concert , December 9 , 
 Herr Felix Weingartner direct performance 
 Edinburgh Symphony . 2 , e flat , Mr. 
 Mischa Elman splendid rendering Tchaikovsky 
 Violin concerto Beethoven romance F. 
 piece — conduct Dr. Cowen — Bach Fugue 
 minor , arrange string Hellmesbeger , 
 overture ' der Natur , ' Dvorak 

 fourth Mr. Simpson classical concert , 
 December 3 , Lady Hallé Mr. Leonard Borwick 
 delightful performance Beethoven sonata minor 
 ( Op . 23 ) Mozart Sonata f major . Lady Hallé 
 play ' Romance ' Joachim ' hungarian ' concerto 
 Brahms - Joachim ' hungarian ' dance . 
 Mr. Borwick arrangement pianoforte 
 organ fugue Bach , Andantino Schubert 
 Chopin valse . Mrs. George Swinton vocalist 
 Mr. Martin Hobkirk able accompanist 

 December 11 Herr Felix Weingartner chamber 
 concert devote entirely composition . 
 composer assist Rebner ( ) uartet ( Messrs. 
 Adolph Kebner , Walther Davisson , violin ; Joseph 
 Natterer , viola ; Johannes Hegar , violoncello ) , Mr. Claude 

 performance ' King Olaf ' December 17 , 
 Choral Union proof ability 
 justice Elgar large choral work . start 
 finish chorus sing rare precision fine 
 effect , notably unaccompanied portion Epilogue . 
 solo music capable hand Miss Agnes 
 Nicholls Messrs. Gervase Elwes Herbert Brown , 
 - exceedingly successful _ 
 appearance concert . accompaniment 
 effectively play Scottish Orchestra , supplement 
 organ , judiciously handle Mr. J. E. Hodgson , 
 Mr. Bradley conduct customary skill 

 Glasgow Amateur Orchestral Society concert 
 season place December 19 , Mr. W. T. Hoeck 
 conduct . programme familiar line , 
 include Weber overture ' Euryanthe , ' Liszt Pianoforte 
 concerto . 1 , e flat , Beethoven Symphony , 
 Délibes ballet suite ' Sylvia . ' Mr. August Hyllested , 
 leading local pianist , play solo Concerto , 
 contribute solo . Miss Jenny Young vocalist 

 Mr. Joseph Bradley , year guide 
 destiny premier choral society great 
 distinction , sever connection Glasgow end 
 present season , having accept conductorship 
 Philharmonic Society Sydney , New South Wales . 
 Choral Union exceptionally fortunate appoint 
 successor Dr. Henry Coward , fame 
 know choral music sing . Dr. Coward 
 duty March 

 favourably hear interesting programme , perform 

 continued 
 appreciation chamber music section public 
 evidence second Schiever concert December 14 , 
 programme contain Beethoven ( ) uartet 
 ( Op . 130 , Op . 18 , . 1 ) Pianoforte 
 Violoncello sonata ( Op . 69 ) , play Madame Marguerite 
 Stilwell Mr. Walter Hatton 

 Mr. Darbishire Jones , young violoncellist merit 

 MUSIC LIVEKPOOL . 
 correspondent 

 ' lady ’ concert ' November 23 ] 
 Orchestral Society play Brahms Symphony D 
 Beethoven ' Leonora ' overture . 3 , interesting 
 novelty Vincent d’Indy symphonic poem ' Wallenstein 
 camp , l'art II . ' Max Thekla , ' melodious | 
 musicianly work . solo Weber ' Concertstiick ' 
 cleverly play Madame Marguerite Stilwell , 
 vocalist Miss Marie Stuart , contralto singer 
 hear Berlioz * captive ' ' english song ' 
 Bertram Shapleigh , interest 
 ywchestral vocal . Mr. Granville Bantock conduct 
 concert , December 7 , 
 Beethoven ' Eroica ' symphony finely play . Mr. 
 Willy Lehmann favourable impression artistic 
 powerful playing RKubinstein minor Violoncello 
 mcerto , Mr. Frank Mullings display tenor voice 
 good quality , courageously use Bach ' Pan 
 master ' artistic advantage song Richard 
 otrauss 

 Elgar ' apostle ' hear time 
 Philharmonic Society concert November 19 . 
 vocal principal Miss Agnes Nicholls , Miss Mabel 
 Braine , Mr. John Coates , Mr. Herbert Brown Mr. 
 Ffrangcon - Davies . Dr. Cowen conduct _ generally | 
 satisfactory performance draw fully choral 
 instrumental resource premier Society , | 
 oratorio hear impressive attention audience | 
 refrain sign applause near th 
 end . forthcoming performance great work 
 Welsh Choral Union Mr. Harry Evans await 
 interest 

 MUSIC MANCHESTER . 
 ( correspondent 

 Hallé concert November 28 devote entirely 
 Wagner . open Grail Scene ' I'arsifal . ' 
 second consist act ' Tannhauser . ' 
 vocalist Madame Ella Russell Mr. John 
 Coates . ' Tannhauser ' music choir sing 
 exceptionally . concert December 5 , 
 Beethoven fourth Symphony perform , 
 Dr. Brodsky play Brahms Violin concerto Bach 
 ‘ Chaconne . Miss Agnes Nicholls sing Mr. Hamilton 
 Harty setting , soprano orchestra , 
 Keats ' Ode Nightingale , ' song ' wild 
 Granville 
 Bantock ' Christ wilderness . ' concert 
 December 12 , Dr. Richter secure magnificent performance 
 Beethoven fifth Symphony ; Mr. Mischa Elman 
 extremely fine rendering composer Violin 
 Concerto , addition Tchaikovsky ' Sérénade 
 Mélancolique . ' Dvorak overture ' Mein Heim * ( Op . 62 ) , 
 Grieg ' Peer Gynt ' suite . 2 , complete 
 programme 

 Gentlemen Concert November 25 , Mr. Arthur 
 Catterall play Mendelssohn Violin concerto ; Miss 
 Currie vocalist ; symphony Mozcart 
 December 11 afternoon recital 
 . Miss Lydia Nervil sing ' ah ! infelice ' air 
 ' Magic Flute , ' vocal waltz Venzano ; 
 Mr. Mischa Elman play Spohr ' Gesangscene ' 
 concerto ( op . 47 ) , Tartini frequently hear Sonata 
 G minor , suite , movement , 
 Sinding 

 ir work 

 London 
 ce Beethoven 
 * academic 

 Brahms Begrabnisgesang " 
 Symphony Orchestra fine perforn 

 Haydn String ( Quartet bear chief burden 
 cause chamber music Torquay . 
 concert season October 9 , mark 
 advance standard performance . Haydn Quartet 
 b flat ( Op . 76 ) Grieg G minor ( Op . 27 ) 
 chief number programme . vocalist 
 Mr. Walter Belgrove . chamber concert 
 record South - west season 

 Torquay Musical Association open fifteenth 
 season thirtieth concert November 27 . choir 
 sing - song , orchestra play 
 movement Beethoven symphony A. force 
 combine _ impressive rendering Dvorak 
 * Te Deum , ' Mr. T. H. Webb obtain excellent 
 result . Mrs. W. H. Mortimer , band , play 
 Scharwenka Pianoforte concerto b flat minor , 
 vocalist Miss Ruth Morrison Mr. Walter 
 Williams 

 amalgamation Exeter Oratorio Society 
 Western Counties ’ Choral Association , 
 definitely place , Society 
 rehearsal ' Judas Maccabeus , ' eventually 
 adopt performance concert 
 new condition November 20 , Miss Emily Breare 

 jchoir acquit , direction 

 L rst point date come performance ns| Dr. H. J. Edwards , - song . Dr. Edwards 
 creation ' Greenbank Choir , conduc | play pianoforte solo , Miss Hilda Pugsley violin solo , 
 Mr. R. Lang , October 6 . choir orchestra | Mr. Percy Lewis violoncello solo , instrumentalist 
 inbere seventy , excel point attack | oine Beethoven trio . Miss Linford Brown sing . 
 nd correctness time tune . soloist | young choral society Tavistock justify formation 
 Miss Nellie Ellis , Messrs. W. Foster H. Smith . ] merit performance ' Banner St. George ’ 
 ly choral event performance ol Dvorak | December 11 , conduct Rev. H. Leigh Murray 

 wide ' Guildhall Choir November 39 

 
 hold 

 Beethoven 

 PARIS 

 Easter H. THORNE 1d . 
 1628 . meek thou livedst . elegy 

 L. VAN BEETHOVEN 1d 

 1629 . blessing , glory wisdom . anthem 
 double chorus J.S. Bacu 3d 

