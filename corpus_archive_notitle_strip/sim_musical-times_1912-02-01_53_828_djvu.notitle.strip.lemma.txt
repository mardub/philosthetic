he to have be not the earnest and scrupulous 
 © innovator depict by sentimental biographer and 
 i tic but a shrewd matter - of - fact writer of music 
 fto suit the taste of director and of the public . 
 | of course , this have nothing to do with the intrinsic 
 | quality of his music ; but nevertheless it suggest 
 ithe propriety of a close critical examination . | if 
 it appear clearly that a certain artist ’s chief ideal 
 be to court the general favour , it become more 
 than likely that in his output will be find certain 
 token of this weakness , certain characteristic flaw . 
 both these point — Bizet ’s deliberate attendance 
 on the director and public , and the result 
 eclecticism of his method — M. Gauthier - Villars , 
 after remark that ' Bizet ’s mentality consist of 
 so strangely contradictory trait that any attempt 
 at analysis will resemble a betrayal , ' emphasize as 
 well by critical remark as by quotation from 
 Bizet ’s letter 

 Bizet , the son of a professional musician , 
 methodically train and coach , an expert crafts- 
 man and the crack candidate to degree and prize , 
 be , say the author , the professional par excellence . 
 this fact must be well understand , as it account 
 well than any other for his idiosyncrasy as a 
 musician . he be the ' model pupil , ' demure , 
 dispassionate , who never rebel against the 
 surround routine nor chafe under the rule , as 
 young artist of imaginative genius be wo nt to do . 
 his favourite master be ' Auber , Halévy , Gounod 
 and Ambroise Thomas . ' he could speak in one 
 breath , and with equal positiveness , of Beethoven 

 and of Meyerbeer ’s ' overwhelming genius . ' he 
 work methodically , without incertitude and 

 and the other on ' Die musica enchiriadis und| the series up to date consist of seventeen volume 
 hr Zeitalter ' ( 1889 ) , the latter be a splendid | no . i. toxiv . be Philipp Spitta ’s work , vol . xv . and xy 
 monograph on medizval music . from 1889 onward | be edit by his brother Friedrich , and vol . xyjj 
 nothing of importance appear from Spitta ’s pen , | appear in 1909 , by A. Schering . 
 except some few less review ; and his death in the musical work of Friedrich the Great , King 
 1894 bring the publication to a standstill . | Prussia , next occupy he , and in 1889 Breitkopf § 
 several of these essay , together with some that be | Hartel publish the two beautifully - execute volume ; 
 new , appear in book form . ' the first series , entitle | which contain the royal musician ’s flute sonata 

 Zur Musik , be a collection of sixteen essay on / concerti , & c. of this work the british Museun 
 subject as far apart as ' Kunstwissenschaft und | possess a copy of the edition de live , present by 
 Kunst ' and ' Beethoveniana . ' the preface be date | the German Emperor . 
 Berlin , February 27 , 1892 . a second set appear in there remain only the ' history of the romantic 

 1894 , under the title ' Musik geschichtliche Aufsatze ' | Opera in Germany , ' which Spitta leave unfinished ; it j 
 musico - historical essay ) , and the preface to this|a thousand pity that death should have rob the 
 work , date March 9 , be probably the last thing | musical world of so interesting a work , especially s 
 Spitta write . in it he explain that the volume under | since it be almost complete 

 it be an almost cruel task , ' say Sir Charles , ' to 
 write a movement , bar by bar , modulation by 
 modulation , figure by figure , exactly the same in all 

 respect , save theme , as a work by another composer ; 
 but it be the only way to get at the root of the matter , 
 md it must be face ? we can only suggest ' entirely ' 
 instead of ' almost . ' the passage be follow by a 
 bar - to - bar analvsis of a Beethoven sonata of the early 
 second period , to help the student to manufacture a 
 work on the same scaffolding . we submit that the 
 process , widely accept as it may be , be fundamentally 
 wrong , and we believe , with all respect to Sir Charles , 
 that the whole point of view which the passage quote 
 connote can account for the second - hand impression 
 convey by the work of so many of our most 
 promising young composer . ' if you want your child 

 to grow into a good man , ' say John Wesley , ' first 
 break his will ' ; but modern soc iety have revolt from 
 the bloodless prig such an edict conjure up . 
 Sir Charles ’s edict seem to we a _ musical 

 parallel,—as if he be to say , ' if you want your son 
 to be a composer , first teach he to write like the 
 early Beethoven . ' we can conceive of no device 
 more calculated to nip criginality in the bud 

 be less directly concern with Mr. Corder , we 
 may be content with say that his method appear 
 dangerous from an opposite standpoint . his school , 
 with their am : ize technical skill , see so clearly how | 
 everything do that they seem eternally to be 
 try , not to sing their own song , but to roar a little 
 loud than the late lion . we recently hear at the 
 same concert two work by two of our foremost young 
 composer , one of each school . the work of Mr. 
 Corder ’s representative , brilliantly clever though it 
 be , be so exactly like Debussy that — on the 
 analogy of the shopwalker who dress too like a 
 gentleman to be one — it be obviously an imitation . 
 the work of the other school , on the contrary , full 
 deep feeling and genuine thought , be cover from 
 beginning to end by the mantle of Brahms , and the 
 high compliment be to say it be nearly worthy of | 
 it model . but in all seriousness we would ask , not | 
 merely whether either work be worth compose or 
 perform , but the more important question whether 
 we be not witness the sacrifice of two great talent , 
 one on the altar of cleverness , the other on that of 
 tradition ? to be one more name in the great succession 
 be the unspoken prayer in the heart of every young 
 composer ; but the goal be not to be reach either by 
 consc iously fetter our impulse in order to link up 
 with the last great name , or by adopt all the late 
 shibboleth lest we be label out - of - date . and that 
 these two alternative be accept by our young 
 composer as their Scylla and Charybdis be , we believe , 
 the present great peril to english music 

 Miss Kimpton ’s first holiday orchestral concert for young 
 people at Steinway Hall on January 8 . 
 symphony be excellently perform under Miss Kimpton ’s 
 direction , Master Sigmund Feuermann play Paganini ’s 
 Violin concerto in D , and Miss Gladys Garnault supply 
 song . the second and last of this holiday series be give 
 m January 15 , when Mr. Percy Scholes be the speaker 

 Mendelssohn ’s ' Scotch ' Symphony be the chief work 
 hosen for explanation and _ performance . Feuermann 
 lay Beethoven 's Violin concerto , and Miss Gwladys 

 Roberts sing 

 La Socicté des Concerts d’Autrefois again appear on 
 the follow evening at -Eolian Hall , on the occasion of 
 a Broadwood . concert . they play Sacchini ’s Ballet music 
 from * Le Cid , ' a suite by Stamitz , and a suite by Christian 
 Bach . M. Desmonts and Mlle . Delcourt play a viola da 
 gamba and harpsichord Sonata by Handel , and M. Mondain 
 and M. Nanny play piece by Boismortier for oboe and 
 double - bass . the exquisite execution for which this Society 
 have gain a reputation be maintain without fail . 
 Mr. Bertram Binyon provide artistic interpretation of song 

 Beethoven ’s second 

 are | 
 most decided success on 

 the London Trio , with Mr. Ivor James in the place ot 
 Mr. Whitehouse , who be still indisposed , give a concert at 
 Eolian Hall on January 8 , and play Brahms ’s Trio in 
 C minor , Op . 101 , with admirable breadth and expressive 

 significance . Signor Simonetti and Madame Amina 
 Goodwin play Beethoven ’s ' Kreutzer ' Sonata , and 
 Madame Goodwin and Miss Wynefred Manby ( vocalist 

 provide solo 

 Lloyd ’s ' hero and Leander ' and Dvordk ’s ' Spectre ’s | he heat § , eat enh ' c in Birmi 
 Bride . ' he well - balance choir of 150 voice maintain | . the last function associate with music in irminghar 

 its accustomed high standard , and take full advantage of the | in the year 191 t be the Festival Choral Society S annua 
 many opportunity for artistic and cultured singing . the | Yule - Tide performance of Handel 's ' the Messiah , ' give 
 soloist be Miss L. Evans - Williams , Mr. Frank Mullings , | ° ® Boxing night , at the Town Hall , before a crowded 
 and Mr. Charles Tree , the very dramatic tenor solo work | assembly , principally compose of people reside in th 
 be admirably sing by Mr. Mullings . the accompaniment | outlying district of the Midlands . it & on such = occasion 
 be well play by a full professional orchestra . Mr. | that one hear the Messiah give on festival line ; indeed , 
 Edwin J. Quance conduct . | the whole interpretation will compare favourably with the 
 tii good tradition , which be say much , if one consider the 
 the Bromley Choral Society , ably conduct by Mr. F. | this be the fifty - sixth annual performance by our premie ; 
 Fertel , give , on December 13 , an admirable performance choral Society . Dr. Sinclair again show himself to be 
 of Coleridge - Taylor ’s trilogy , ' scene from the song of | master in the handling of choral force , conduct a really 
 Hiawatha , ' which attract and greatly please a large | magnificent performance , every number be receive wit 
 audience . the chorus work be distinguish by its spirit | € xtraordinary appreciation . the voice of the chorister 
 and expressiveness . solo part be take by Miss B. M. | be of delightful timbre and evenly balance . in the way 
 Crawford , Mr. Bertram Pearce , and Mr. Graham Smartt . of attack , precision , and vocal technique the utmost be 
 dnauntnesintestin realise . the   principal — all of whom _ distinguishe 
 an excellent performance of ' Elijah ' be give at | themselves — be Madame Le Mar , Miss Mildred Jones , 
 Woodside Hall on December 16 by the Finchley Musical | Mr. William Green , and Mr. Robert Radford . ti 
 Society , under the direction of Mr. H. J. sage . the | organist be Mr. C. W. Perkins . London will short ) 
 principal solo be take by Mrs. William Nollis,| have an opportunity of hear the Brminghan 
 Mrs. A. W. Williams , Mr. George Foxon , and Mr. Charles | Festival Choral Society , for on February 29 they w 
 Martin . give a performance of Bach ’s monumental B minor Mass , 
 4 aenneemen at Queen ’s Hall , under Dr. Sinclair ’s conductorship , 
 rhe Richmond Philharmonic Society give the first concert | among the late local musical association be that of th 
 of its twenty - second season at the Castle Assembly Rooms | Birmingham Diocesan Organists , promote for the purpox 
 on December 19 . the principal choral work be Barnett ’s | of friendly intercourse , for interchange of information and 
 ' Paradise and the Peri . ' the second part comprise | opinion on musical matter , the reading of paper on subject 
 Beethoven 's Symphony in C minor , Tchaikovsky ’s ' Elegy ' | interesting to organist by member of the association , or t 
 for string , and Wagner ’s overture ' Die Meistersinger . ' | expert outside the association . its president be the Lon 

 he soloist be Miss M. Fielding , Miss V. Fielding , | Bishop of Birmingham , and its vice - president be the Rector 
 > ' : | . . pe e 

 aes . during the past month have draw good audience , and the 
 audience , which occupy every available seat . t f 

 performance have reach their usual high standard of 
 , ws _ ; excellence . Mr. Dan Godfrey , adopt his customary 
 the Fulham and District Choral Society give their twelfth impartial attitude , have present we with work that be 
 concert at the Fulham Town Hall , when the concert version representative of various period and school , include 
 of german s ' Merrie England ' be perform before a large | symphony by Xaver Scharwenka ( c minor ) , Schumann 
 and appreciative audience . the work be conduct by ( no . 1 , in b flat ) , Schubert ( the ' unfinished ' ) , César 
 Mr. George Wilby , and the soloist be Miss Dorothy Franck ( his solitary example in d minor ) , and Glazounow 
 Cook Smith , Miss May Peters , Mr. Frank Webster , Mr. John ( no . 5 , in b flat ) . mention should also be make of the 
 Prout , and Mr. Oliver Greenwood . the efficient orchestra | performance of the following interesting composition : 
 be lead by Mr. Edgar Wilby , and Mr. C. A. Finlay act | Beethoven ’s ' Coriolanus ' Overture , Balfour Gardiner ’s 
 a accompanist at the pianoforte . ' * overture to a comedy ' ( conduct by the composer 

 P. A. Bow 
 conduct 
 moulin ’ suit 
 ' Romeo an 
 and Gustav 
 by the con 
 the name 
 concerto ( ] 
 for violin at 
 concerto ( \ 
 concerto in 
 Pianoforte 
 in every ca 
 commendat 
 there ha 
 usual , but 
 Miss Leila 
 Farnell - Wa 
 Madame L 
 the New Y 
 of Anna 
 performanc 
 the seri 
 Mr. T. W. 
 symphonie : 
 lecture ha ' 
 symphony . 
 can have fa 
 outlook co 
 that these \ 
 illustration : 
 fresh zest t 
 organizatio 

 the Wessely Quartet and Dr. Esposito provide the 
 programme Mendelssohn ’s Op . 44 , No . 2 , in e minor ; 
 Brahms ’s violin and pianoforte Sonata , Op . 78 ; and Dvorak ’s 
 ( Juintet , Op . 81 . on January 14 , Dr. Brodsky , Herr Carl 

 Fuchs and Dr. Esposito play the following programme : — 
 Mendelssohn ’s Trio in D Brahms ’ sonata in A , 
 Op . 100 , and Beethoven ’s trio in b flat , op . 97 

 on January 21 the Marie Motto ( Juartett play Schumann , 
 Op . 41 , no . 3 ; Haydn , op . 74 , no . 3 ; Dvorak in 

 on the 

 serve also to display the consummate art of Miss Nicholls , 
 } and of Mr. Hamilton Harty at the pianoforte . on January 
 | 15 , the first of a second series of free chamber concert , 
 seven in number , which have be organize by Mr. S. 
 Midgley , and finance by a few wealthy music - lover in the 
 town , take place . with the aid of Miss Ada Sharp as 
 violinist , and Mr. Drake as violoncellist , good performance 
 be give of Pianoforte trio by Beethoven ( D , op . 70 , 
 no . 1 ) , Schumann ( D minor , op . 63 ) , and three movement 
 from Hiller ’s Serenade trio ( Op . 186 ) . Mr. Charlesworth 
 George be the vocalist , and sing most sympathetically song 
 by Schubert , Brahms , Sibelius , and other 

 the Huddersfield subscription concert on January 9 be 
 give up to Madame Clara Butt ’s concert party , which 
 include Mr. Kennerley Rumford , Mr. Frank Merrick as 
 | pianist ( whose variation on a Somerset song be very 
 charming ) , and Mr. Charles Barré as violinist . the programme 
 be of more interest than be usual at these miscellaneous 
 | concert 

 a lead musical journal —— a ' Wunderkind ' of 

 Ipolyi , have appear at the Beethovensaal . 
 look exactly like a ' pocket edition of Ysaye , and to play like 
 alittle man . ’-——Richard Strauss ’s new opera , take from a 
 comedy by Moliére , be not to be produce in an opera house , 
 but in Reinhardt ’s Deutsches Theater in Berlin . the first 
 performance be to take place next November , and there will 

 e other performance during the season . Eugen 
 Albert will make his re - appearance as pianist on 
 February S with the Philharmonic Orchestra , at a 
 concert in aid of the Orchestra ’s widow and orphan 

 und . he will play concerto by Beethoven and Liszt . 
 — — after be associate with Berlin opera for nearly 
 wenty year , Dr. Karl Muck be to leave at the beginning of 
 ext season for Boston , where he will conduct the symphony 
 Orchestra for a period of five year . it be possible that 

 the miracle , ' now be perform at Olympia , London , 
 il be produce in Berlin at Easter.——The young 
 joloncellist , Hans Bottermund , have give a very successful 
 cital in the Klindworth - Scharwenka Saal . his programme 
 aclude Bach ’s Suite in c major for violoncello alone . 
 Madame Gemma Bellincioni have settle in Berlin , and have 
 pen a school for singing . — — an extraordinary concert 
 jk place at the house of Herr Karl Flesch last month . 
 saye , Kreisler , Mischa Elman , and Karl Flesch all play , 
 ‘ reisler and Eiman plave the Bach double Concerto 

 in the British Museum by Professor Stein , be perform 

 the programme also include the recently discover 
 | Beethoven Symphony . the concert be conduct by 
 Professor Stein 

 LEIPSIC 

 AS NET book 
 the new CATHEDRAL PSALTER , 
 and all other psalter , chant book , and PRAYER book 

 and the follow of their publication : — or 
 the CORONATION SERVICE , with music as perform at the Coronation of King George V. and s. d. 
 Queen Mary oes Paper , 2 . 6d . ; Cloth , gilt , 5s . od . ; Whole Leather , gilt , 7 . 6d . ; edition de luxe 63 9 
 CATALOGUE of the MUSIC LOAN EXHIBITION " by the WORSHIPFUL COMPANY of 
 MUSICIANS . 1904 . illustrate . paper cover ... om aa ws wl sos - G6 . 
 J. S. BACH . his Work and influence on the music of Germany . by Puivipp Spitta . translate by \ SHOR ’ 
 CLARA BELL and J. A. FULLER MAITLAND . 3 vol . reduce price , cloth re _ ia — - 286 VALU 
 FREDERICK CHOPIN . 2vol . by FREDERICK NIECKS sea _ ins ee be oo . 82 § includ 
 programme music in the last four ce nturie . by FREDERICK NIECKS . reduce 
 price , cloth be , ae ad ii i i _ a " i as OE 
 the music and MUSICAL instrument of SOU THERN ‘ INDIA and the DECCAN . 
 illustrate with seventeen plate . by Captain C. R. Day ae eet see a a - — wee 
 ditto ditto Artists ’ Proof copy . japanese paper ail : si : — — te 6 
 CHRISTMAS CAROLS . new and old . Library edition , with historical preface . Roxburgh Binding 7 6 
 CHRISTMAS CAROLS . new and old . beautifully illustrate . series 1 and 2 only . . cloth , gilt ... " 7 6 
 GOD save the king . the Origin and history of the music and word of the National anthem . by ces 
 W. H. CumMMIne Cloth , gilt on wes oe ae one se ‘ ane 3 6 
 the histor\ of MENDELSSOHN ’s ELIJAH . by F. G. Epwarps . cloth ... ais si op 3 6 
 NATIONAL NURSERY RHYMES and song . by J. W. ELtiott . beautifully illustrate and 
 elegantly bind nae ; ; in ai ' ie a des ' a ' : son 7 6 
 the LITERATURE of NATIONAL MUSIC . by Cart ENGEL . cloth ... = a as 5 0 t ' 
 research into the early history of the VIOLIN FAMILY . by Cari ENGEL . cloth 7 6 . 
 BEETHOVEN and his nine symphony . by Sir GeorGe Grove . cloth sin ne " 6 0 
 the BEAUTIFUL in MUSIC . by Epvarp HAnstick . translate by Gustav CoHEN . cloth _ ... 4 0 
 the letter of a L E If ZIG CANTOR . by Moritz HAupTMANN . translate and arrange by 
 D. COLERIDGE 2 vol . cloth , gilt ; be _ bn - nas _ a 
 JENNY LIND . \ record and Anclyeia of the method of the late Jenny Lind - Goldschmidt . by WIT 
 W. S. Rockstro . Cloth , wr _ - ees - awe eee " 20 
 LIFE of MOZART . by Orto JAuN. translate from the German by PAULINE D. TOWNSEND . with 
 five Portraits and Preface by SIR GEORGE GROVE . 3 vol . reduce price . cloth I . oe 
 NOVELLO ’S collection of word of anthem , contain the wo rd of over 2,000 « Anthame . 
 Cloth , § s. od . ; red basil 7 6 
 the appendix only . paper . wa " ee one : sa = 1 0 
 the PARISH CHURCH ANTHEM BOOK ( Words of Anthem ) . cloth oan " a " a oe or 2 0 — 
 a dictionary of musical term . by J. STarNerR and W. A. BARRETT . cloth , gilt ... eee 7 6 or in 7 
 DUFAY and his contemporary . _ fifty composition , with facsimile . by J. STAINER , J. F. R 
 STAINER , C. STAINER , and E. W. B. NICHOLSON _ vee oon a a 5 
 early BODLEIAN MI SIC . Vol . I. , contain 110 ecielien a ms . music ; Vol . IL . , contain their a i 
 transcription into Modern Rast tation . ' by J. STAINER , J. F. R. STAINER , C. STAINER , and E. W. B. . 
 NICHOLSON . the two » volume , large folio , half morocco ... si a ee ‘ ile _ — - 2s t 
 technique and | x pression in pianoforte - playing . by FRANKLIN TAYLOR . cloth , gilt 5 0 | 
 CATHEDRAL organist : past and present . by JoHN E. West . cloth _ ... ose on eo 3 6 
 additional hymn , with tune . for use with any other Church Hymnal — 
 Staff Notation , 2s . 6d . ; Tonic Sol - fa , 2s . 6d . ; word only , is . od . , 4d . , and 3d . Dr. I 
 the COUNCIL SCHOOL hymn book , with tune that ad 
 Staff notation , 2s . 6d . ; the word , with melody only in Staff Notation and Tonic Sol - fa , Is . , om 
 word only , limp cloth , 6d . * ao 
 the general history of the science and practice of music . by " he 
 Sik Joun Hawkins . two vol . Cloth . nen be i i sa 
 - supp ok mentary Volume of Medallion Portraits from the Origin : l Plates . Cloth ean : w 8 @ to | 
 LONDON : Wareh 
 160 , WARDOUR STREET , W. January , 1912 Ward 

 J 
 ™ Ge 
 a 

 by 
 author of " ' the Diversions of a Music - Lover . " 
 extra crown 8vo . 6s . net 

 Westminster Gazette . — ' * a pleasant volume of musical essay .. . 
 the subject discuss range over a wide field — from ' Salome ' to the 
 Ballad Industry , from Blumenthal to Beethoven 's ' Letters , ' and in one 

 i all the same penetrating discernment and polished and amusing 
 terary style be to be detect 

