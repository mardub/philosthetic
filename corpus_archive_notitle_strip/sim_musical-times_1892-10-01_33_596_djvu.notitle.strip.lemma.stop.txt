

 singing - class circular . 
 publish month . 
 . 596.—vol . 33 . OCTOBER 1 , 1892 . price 4d . ; post - free , 5d . 
 register transmission abroad . annual subscription , postage - free , 5s , 
 ama 2S ED ST WES TE te PS CEE SAE TREE oee site TRI OS PAE ASE SE ame 
 LEEDS MUSICAL festival . ROYAL ACADEMY MUSIC , 
 October 5 , 6 , 7 , 8 , 1892 . TENTERDEN STREET , W. 
 = — _ — 4 institute 1822 . incorporate Royal Charter , 1830 . 
 conductor , Sir ARTHUR SULLIVAN . patron : MAJESTY QUEEN RoyAL FamILy . 
 EN principal : Dr. A. C. MACKENZIE . 
 \ band chorus 450 . Michaelmas term commence September 29 . 
 N\ PrincrpALs.—Madame ALBANI , Miss MACINTYRE , Miss ANNA half - term begin November 1c . 
 WILLIAMS , Miss HILDA WILSON , Miss MARIAN McKENZIE , syllabus Metropolitan Examination , Dec. , 1892 ( suc- 
 Mr. EDWARD LLOYD , Mr. HENRY PIERCY , Mr. BEN | cessful candidate create licentiate Academy ) , 
 davy , Mr. NORMAN SALMOND , Mr. ANDREW BLACK , | ready , send application . day receive 
 NEC : Mr , PLUNKET GREENE . principal violin : Mr. J. T. | entry , October 22 . 
 CARRODUS . organist : Mr , A. BENTON . fortnightly concert , October 22 , 8 . 
 serial ticket ( transferable ) , admit concert , £ s.d.| lecture W. S. Rockstro , Esq . , Wednesday , October 5 , 
 Saturday night .. et ee ave ome .. 5 5 0 | succeed Wednesday , 3 . 
 Seats ( morning ) .. é pe aa tito F. W. RENAUT , Secretary . 
 Seats ( evening ) .. pe wi x ee o1§ 0 
 second Seats ( morning ) .. ae ce o10 6 7 
 Second seat ( evening ) .. 0 80 ) GUILDHALL SCHOOL MUSIC . 
 unreserved seat ( morning ) ' ee 05 0 < - : . 
 unreserved seat ( evening ) . . wa = @ § 6 establish Corporation London 1880 , 
 SATURDAY evening . principal : Sir JosepH BARNBY . 
 seat , Gallery Ground Floor ( ) ee + . 015 0 concert place October . 
 . seat , Gallery Ground Floor ( b ) 010 6 ! term commence Monday , September 26 . 
 d Mrs. Second Seats ( ) ee egiin lwaerd tas o 8 © | prospectus particular apply Secretary . 
 Second Seats ( unreserved ) .. e ‘ tp - < x @ 3.6 order Committee , 
 application ticket attend accompany CHARLES P. SMITH , Secretary . 
 r bya remittance ticket require . Victoria Embankment , E.C. 
 programme ( free ) application . 
 HALL . ™ FRED . R. SPARK , Hon . Sec . | HIGHBURY PHILHARMONIC SOCIETY , 
 Festival Office , Municipal Buildings . 
 nT ete thaer sane selhetmtentio ATHENEUM , HIGHBURY NEW PARK , N. 
 ROYAL CHORAL SOCIETY , conductor : Mr. G. H. BETJEMANN . 
 ROYAL ALBERT HALL . 
 patron : Majesty QUEEN . October 17 , 1892 . 
 President : H.R.H. DuKe EpinpurGu , K.G. CONVERSAZIONE . 
 ERBERT Conductor : Sir JoserH Barney . recitation Mr. CHARLES FRY , VOCAL MUSIC . 
 - second season , 1892 - 93 . 
 — ' nk awe November 14 , 1892 . 
 ee series etait hich followi k HAYDH * creation . " 
 tie comprise Concerts , follow work : 
 | D . bp eee fs Mad FANNY MOODY . 
 GSTER . ' perform : — November 2 , Dvorak ' " ' requiem " ; November 23 , Mr. PHILIP NEWBURY . Mr. CHARLES MANNERS . 
 Berlioz ’ " ' Faust " ; December 7 , ' ' GoLpEN LEGEND " ; January 2 , 
 " Messian " ; January 18 , " SoLtemn Mass " ( E. M. Smyth ) January 16 , 1893 . 
 NAUGHT . " creation " ( , ii . ) ; February 15 , ' " redemption sh DR . C. H. H. PARRY " job " 
 March 8 , " IsRAEL Ecypt " ; March 31 , " Messian " : April 19 , Rio ; pe : , ty 
 " St. Pau " ; 1o , " Extyan . " Schumann " ' NEW YEAR SONG . 
 arrangement se vith follow artist ee 
 e wi e follow artist : — ' ' 
 Madame ALBANI | Mr. EDWARD LLOYD Se eee gg 
 isH _ Madame NORDICA Mr. BEN DAVIES Master R. WEIR . - Mr. PLUNKET GREEN E. 
 SH ! Miss ANNA WILLIAMS Mr. IVER McKAY Mr. ALBERT CORNISH . Mr. JAMES LEY . 
 Madame CLARA SAMUELL Mr. WATKIN MILLS 
 R Pupil 7 . ANDREW PLACK March 20 , 1893 . 
 SS 3 } v ' a1 
 Madame BELLE COLE Mr HENSCHEL OND SIR W. G. CUSINS " GIDEON . " 
 Miss CLARA BUTT Walthew ' ' PIED PIPER " ( performance ) . 
 — _ — — _ — _ Mascagni " ' CAVALLERIA RUSTICANA " ( selection ) . 
 ABLE organist , Mr. HODGE . Madame GUILIA VALDA . _ — - Mrs. KATHARINE L. FISK . 
 Band Chorus consist 1,000 performer . Mr. BEN DAVIES . Ms. SAWELEY . 
 . pr enthusiastic reception accord Dvorak " requiem , " 
 Q row oe . ye desirability repeat work 8 , 1893 . ; 
 g Concert season . ' 
 " hy January 18 perform , time , " ' solemn BERLIOZ ' " FAUST . " 
 b4 ass ' ( compose E. M. Smyth ) . work distinguish Mrs. HELEN TRUST . Mr. EDWARD LLOYD . 
 ‘ ied Trio , ) conductor speak high term ; pro- Mr. ANDREW BLACK . Mr. KEMPTON . 
 toa aww ho = member Royal family 
 ; ena deep interest . e performance honour rae ; - ' 
 Trio , presence Imperial Majesty Empress Rentals ane Subscriptions seat : 15s . , 128 . 6d . , 10s . 6d . , 78 . 6d . ; 
 ufthe concert , include Sub- vocal Orchestral Members , Guinea . 
 ER ? " sn pense — Lee ot od oo Concerts : | prospectus information obtain — 
 S , 3 guinea ; Arena , 2 guinea ; Balcony ( reserve ) , 14 guinea . 
 price Concert : Ios . 6d . , " , $ 8.5 435 ag Gallery WILLIAM THORNTHWAITE , ) 
 menade , ts . 14 , Highbury Hill , N. , ; Hon . Secs . 
 seemriber receive , seat secure , Pros- HENRY SCRUTON , 48 , Highbury Hill , N. , 
 ‘ use obtain Royal Albert Hall . 
 opinst Concert Weduecder , November 2 , 8 . Dvorak ESSRS . HANN CHAMBER CONCERTS . — 
 uetiem . " artist : Madame Albani , Miss Hilda Wilson , Mr. Seventh Series , Brixton Hall , Mondays , October 31 , ' 
 ward Lloyd , Mr. Watkin Mills . November ro , 8 - . , haa , pag na ig hay nee 3 : 
 Quartet G major , op . 18 , no.2 ( Beethoven ) ; suite Pianoforte ! 
 OYAL L BERT HALL . — MR . H. C.|ana Violoncello , * m Walde " ( David Popper ) ; Pianoforte Quintet 
 — sy SONKING organist SuNpDAy , October 16 , 1892 , Op . 34 ( Brahms ) ; Pianoforte Solo , ballade G minor 
 Street ( © ‘ ana ( Chopin 

 578 

 MR . ALEXANDER TUCKER ( Basso Profundo ) 
 new address , St. Audries , Enfield , N 

 MADAME BAER ( Pianist ) 
 ( Pupil Academy Music , Berlin ) . 
 home . play Beethoven , Mendelssohn , Liszt , Weber , & c. 
 150 , Hartfield Road , Wimbledon 

 MISS CHRISTINA BRUMLEU ( Violinist ) 
 ( Gold Medalist , L.A.M. , A.L.A.M. ) 
 Concerts , Homes , & c. address , Mossbank , Sidcup , Kent 

 great MUSICIANS executant 
 use prefer Pianos 

 BEETHOVEN HAYDN Mpme . SCHUMANN 
 WEBER CHOPIN WAGNER 
 MENDELSSOHN _ _ LISZT VON BULOW 
 DVORAK HELLER DE PACHMANN 
 CRAMER HENSELT HALLE 
 BENNETT E. SILAS SULLIVAN 

 price list illustration application 

 road Spa Road , lead eastward 
 Bermondsey New Church , meet Spa 
 road derive . chalybeate spring 
 discover seventy year ago ( Knight | 
 " London " issue 1851 ) , place con- 
 verte sort tea - garden ingenious man , 
 exhibit talent painting decorate 
 house entertainment subject 
 pencil . following description , Hughson , com- 
 pare ' * Mount Heclas " ' ' ' Mount Vesuviuses " 
 modern exhibition , doubt 
 " ' new sun . " Mr. Keyse , | 
 proprietor , establish sort Vauxhall Bermondsey 
 Spa , , find succeed , ingenuity " ' suggest 
 improvement , , , entertain 
 public excellent representation siege 
 Gibraltar , consist transparency fire work , con- 
 structe arrange Mr. Keyse ; height 
 rock length foot , 
 apparatus cover acre 
 ground . " x 

 BEETHOVEN SKETCH BOOKS 

 J. S. SHeptock , B.A. 
 ( continue p. 525 . ) 
 sketch 

 connect link time time 
 Mr. E. Dannreuther 
 movement certain Beethoven 
 pianoforte sonata . far conscious , 
 far unconscious working genius pro- 
 bably remain mystery ; process 
 undoubtedly compound . page 
 Notirungsbuch set meditate 
 problem . page ( 50 ) brief sketch , 
 Beethoven try present 

 thought shape . — 
 Allegretto . ze — 
 rp - b- ne @ £ 26 al 
 ja " . ' t _ . ew — _ _ 1 _ 
 b > |-o — — ] J Pe 7 oes — 
 SS Se ee 
 ev ' ol — 
 vir 6 ° cle b 
 ee 
 z ane — alte 
 > ) h | < i4 — ~~ ) 
 " oe log _ — — 
 Sa ps ee eet ne ha 8 = 
 al _ ba eo . 
 e oa 3S 
 ' ' | — _ cl u. s.w 
 — — — a-—e fe- 
 3 — — o—_f 

 

 interval second note 
 different , key measure , 
 likeness , 
 shape unconsciously . certain figure , , 
 writer Beethoven 
 , , stock art 
 composer . let — little turn 
 find Wagner Rienzi Prayer 
 Briinnhilde theme " Ring des Nibelungen . " 
 Beethoven happen . case 
 , figure 

 remind opening theme , 
 -|more subsequent passage Finale 

 hay Ss = ~ 
 eee _ 
 vs oe Cee sees 

 judge place Notirungsbuch 
 Beethoven fond ot jot idea sequential 
 form . , pick instance 

 

 gg ee | et 
 FS se cae S 
 — 
 SS sse SS 
 t oe — _ tt ag br et ae 
 — — — — 
 = = es te ie 
 ie SE festaws 52 sees ose |e fe = 
 ry ) = [ » e a4 2 
 -2@—5 — nf eas 2 — _ > s-|50 — — 
 ee vo ae ‘ ose ee eee ee 
 se ae reemeeees — — o — 2 - 12 - 0200 
 1 — = — — | = = = = = = ra nl 
 b 4 t= | 55 g |S woe 
 geese ot eet 
 — — — — _ — _ ss — _ — _ _ = 
 ee ee ee 
 eae oo Qe — _ — _ _ . — 
 4 oe — — — = = 
 2—2-*2- ? oe @. .@ te . @. 
 — f f — — — e — e — # -e- # oe e—|- * ee — ' * 
 SSS r = i=2= = = — K—- = sa 

 — — _ { -a — ® 
 8 stee paseg eget | = SSS 
 c= | 
 ee ee ee ee | 
 se | — oe oe | ] | ee ie oo 
 - Co = = — = = 
 | ea . s ee oe 
 . . . " go-@ — ] & = = . = ~ 
 = capital technical " Rosalia " es es — ze et 
 study . 
 little series similar study es 2 . 2 
 cull Notirungsbuch . Nottebohm = = = eB Ss 27 ] < r e — = 
 ( Zweite Beethoveniana , ch . 37 ) quote . | ! — seissee- — — — — — 

 il ataiaiantaleaaeiaalil signature quotation ought probably 
 key F , change sixth bar , 
 mark Beethoven , f minor ; bar 

 oe 
 — — - | bass want G flat second 
 group — rate , second . 
 e 2 oy St hee sketch piece , possibly piano- 
 ss forte , occupy page 
 asia oe eae eee " ! | Notirungsbuch , tempt 

 difference ; detail , 
 outline . bar mark 

 hoven . piece conclude follow | point — 
 lively bar — 
 4 — 
 = t — — — s = eo 
 . 2 — z2- zz y — ? 
 das |forscht mein Geist und |sinnt unddenkt und aoe 
 o — ej = e 
 se . _ ! ee lower page . print 
 = oi 0235 - 2 ) | | version — 
 eet ae 
 — — imm | | - — — — _ — — f- — — — t ~ 
 3 . @. @ o o Oba — o- Sa = — — e } tt o- ) 
 sss ae -—- — eo Zz — — — — oe - 
 1 t , " ? ow 
 : intention vague , low note 
 ¢ = f ae kind . Beethoven write g , f , , 
 — — — — et og lastly , d : mental neume point downwards , 
 . exact pitch find experiment . 
 attention Beethoven devote small 
 2 — ae — " — — — — — + | composition deserve attention ; place ot 
 20 0 - 0 eee 0 op } eee — et | Notirungsbuch bar song 
 ne — ole | find 

 

 al 
 tt 

 mention lastly sketch move- 
 ment Symphony C , accord 
 Nottebohm , Beethoven work publish 
 oneinC ( Op . 21 ) . Nottebohm ( Zweite Beethoveniana , 
 p. 228 ) mention sketch short 
 quotation . speak 
 edition " * Beethoven Studien , " describe 
 opening Finale Op . 21 , 
 tell find fugue 

 12th . " Zweite Beethoveniana " mention 
 error . sketch commence 
 

 ete . 
 notirungsbuch ( p. 56b ) come 

 Cantor piece , , , good 
 effort week . far standard work 
 Wednesday morning programme . untried 
 companion Dr. J. F. Bridge setting ' * Lord 
 Prayer , " translate Dante Dean Plumptre . 
 piece turn success . great 
 restraint appear ' ' Nineveh , " altogether 
 solemn dignified composition , seek effect 
 mean legitimate . 
 Prayer accuse sensationalism — 
 eccentric ; place mean 
 end . section composer 
 impressive spirit text , 
 music exist intimate alliance . technically , 
 work product accomplished hand , Dr. 
 Bridge felicitate happy addition 
 store sacred music . add 
 piece offer difficulty , lie 
 scope average choral society . composer 
 come expressly Scotland conduct performance , 
 return north , pleased 
 interpreter reception accord 
 music . evening Wednesday devote 
 secular Concert scheme Festival . use 
 Concerts , , reason creditable 
 public taste , dwindle , soon 
 . survivor friend steward , , how- 
 , propriety Festival exclusively 
 Cathedral solemnity , throw open great church 
 occasion , instead shut 
 people evening dress listen operatic 
 air - song . probably 
 Gloucester secular Concerts derive local distinction 
 fact serve production Cantata , 
 " birth Song , " compose Miss Ellicott , daughter 
 Bishop . , explain month , 
 setting soprano tenor solo , chorus , orchestra ot 
 verse Mr. Lewis Morris answer 
 question environment serve kindle 
 poetic fire . poem particular connection 
 music . train reasoning expression 
 feeling , read sing . 
 approach music , stanza , example , 
 lead Miss Ellicott choice . 
 understand division music chorus 
 solo entirely composer , text 
 invite repelling treatment . want 
 definite musical suggestiveness verse leave 
 composer free respect , account 
 passage vague relevancy . 
 thing , , incidental choice Mr. 
 Morris poem , place composer obvious 
 disadvantage . music , occasionally somewhat 
 awkward singer , creditable fair amateur 
 proceed , strengthen place 
 musician sex . respect , add , 
 challenge comparison masculine work . 
 certainly strength orchestration , occasionally 
 display vigour construction expression , 
 warrant qualification ' manly . ' Miss Ellicott , 
 composer , ambition , choral work 
 likely large dimension " ' Birth Song . " 
 new piece receive , matter course , 
 composer , shake hand conductor , Mr. 
 Lee Williams , bow thank audience 
 platform . Mozart " Jupiter ' ? Symphony new , 
 effective - song Mr. Brewer specially 
 noticeable feature rest programme 

 remain novelty produce 
 Cathedral Thursday : Dr. Hubert Parry " Job " 
 morning , Mr. Lee Williams ' ' Gethsemane " 
 evening . associate Beethoven 
 Symphony C minor , second , Mendelssohn 
 " hymn praise , " Festival manager able 
 scheme . occasion large 
 audience attend ; morning , 
 doubt , tribute personal popularity Dr. Parry 
 county , pride connection Festival 
 management father , late Mr. Gambier 
 Parry , conspicuously contribute year . 
 , numerous attendance recognition 

 success achieve — lofty talent use good 
 manner single eye glory art . reader 
 remark tell " Job " 
 unconventional , add daringly , 
 far Dr. Parry know influential 
 support . person day 
 unconventional dear , simply unconvention- 
 ality . domain music individual 
 allow influence . tend 
 extravagance license , dangerous 
 art , music especially . ' ' Job " feature 
 decidedly new , composer 
 bound . write declamatory bass solo 
 seventeen page long , length special 
 peculiarity . ' chorus equally voluminous 
 reply . work end , chorus , 
 solo — circumstance detract musical 
 effect , accord natural treatment story . 
 music character easily discuss 
 straitened space notice like present . 
 subject separate leisurely discussion , cir- 
 cumstance admit deal tech- 
 nical character tendency . Dr. Parry 
 step know " advanced " music , 
 depart extent solid dignified classic 
 style work immediately precede . 
 piece stand , dispute 
 masterfulness . " Job " popular 
 question shall answer , 
 musician turn page interest , 
 admiration strong sustained flight 
 Dr. Parry capable . admixture 
 lyrical beauty work , , example , naive 
 melodious song Shepherd Boy ; great 
 characteristic dramatic vigour . quality ¥ 0b 
 ' ' lamentation " answer argument 
 almighty ( set chorus ) afford supreme illustration . 
 intimate , Cantata deal 
 , series novelty , shall 
 glad performance London Society course 
 approach season afford early excellent 
 opportunity . musician gather Gloucester 
 hear " Job , " , 
 eye general character meet favour , 
 opinion exist great power 
 page . certainly remarkable production , 
 sufficient memorable Festival 
 adorn . composer conduct capital performance , 
 reason satisfied , there- 
 , sympathetic reception accord 
 music . Beethoven Symphony Mr. 
 Williams orchestra opportunity turn 
 good account , Spohr " fall Babylon , " 
 rarely hear , listen marked attention 
 deserve rendering careful enthusiastic . 
 Cassel master command admiration 
 choral work . Mesdames Nordica , King , Mary 
 Morgan , Messrs. Houghton Mills answer 
 solo , acquit 

 Cathedral Thursday 
 evening , " ' Gethsemane " ' ' hymn praise " ' 
 present diverse powerful attraction ; _ first- 
 local origin successor 
 " Night Bethany , " second love 
 amateur bear great favourite . structure 
 " Gethsemane " describe column 
 month , , , suffice plan 
 new work precisely resemble " Bethany . " 
 musical point view advance Cantata . 
 grasp mean firm , scope broad , 
 detail work great skill . 
 Mr. Williams strike chord devotional 
 feeling strong hand . chorale model ot 
 kind ; air tenderly expressive , bold 
 stirring , subject travel 
 accompany . consider nature theme 
 high praise , high deserve . 
 effect music , hear Cathedral , 
 fine , great audience fall stillness 
 impressive . admirable performance 
 Miss Anna Williams , Miss Hilda Wilson , Mr 

 Eleanor Rees , Mr. Ben Davies , Mr. Ludwig)—did 
 . leave impresssion impending artistic failure — 
 happily destine come 

 following morning surprising improvement 
 refer set . Concert hour 
 Mayor breakfast , hear 
 chorus present exhilarate civic good thing , 
 good thing need . opening number 
 Dvordk ' ' Stabat Mater ' ? proceed little 
 way critic begin rub eye ask " 
 singer yesterday " ? want 
 hear Dvorak choral music sing 
 Cardiff folk , know work . stay 
 moment!—the choir trainer , 
 prepare " Elijah , " ' stabat ' " ? 
 ask know , simply indicate possible solution 
 puzzle . orchestra good 
 Dvorak work . excuse 
 slip performer . soloist 
 ( Miss Anna Williams , Miss Eleanor Rees , Mr. Ben Davies , 
 Mr. Ludwig ) demand 
 exact composition . feature programme 
 morning Sir Arthur Sullivan pathetic Overture 
 " memoriam , " finely play ; Beethoven C minor 
 symphony , Stanford ' revenge , " 
 chorus prove success ' Stabat " 
 fluke . thing necessary speak 

 Sir Arthur Sullivan come Cardiff specially 
 conduct ' ' golden legend , " presence 
 popular musician , aid fame late Cantata , 
 entice public fill seat usual . 
 audience , fact , judge Cardiff Festival standard , 
 good , satisfy propriety occasion . 
 chorus plain normal 
 standard high . sing " evening hymn " 
 splendidly , drop shade , fairly win encore . 
 number , result Sir 
 Arthur Sullivan delight , secret 
 satisfaction . soloist Madame Nordica , Miss 
 Wilson , Mr. Lloyd , Mr. Ludwig , Mr. Mills , 
 discharge familiar task expect . 
 Madame Nordica , , obviously suffer 
 indisposition . superfluous dwell general 
 success performance , pleasure audience , 
 applause direct Sir Arthur Sullivan , hero ot 
 occasion . imagine . 
 Cantata Schumann Symphony B fiat , 
 ' " " Mock Doctor ' ? overture , vocal piece . 
 - work finely play Sir J. 
 Barnby direction 

 town St. Gallen possess choir 
 male voice , mixed chorus , repre- 
 send 500 executant . combine 
 municipal orchestra welcome guest 
 Choral Festival , grand welcome Concert comprise 
 entry scene Minnesainger Wartburg 
 Festival ' ' Tannhauser , " Mendelssohn " 
 Walpurgis Night , " solo , chorus , orchestra ; 
 Contest gathering attend twenty- 
 association , include guest 
 canton famous Zurich Choral Union , head 
 distinguished conductor , Professor Attenhofer . pro- 
 gramme match divide — viz . , 
 - song composition chorus , solo , com- 
 prise work - know composer ot 
 class music , swiss composer 
 Attenhofer , Angerer , Wiesner , Heim , 

 instrumental gathering , place week 
 later , open Welcome Concert , selection 
 Wagner ' " ' Nibelungen " perform St. 
 Gallen orchestra . match , hold 
 spacious St. Lawrence Church , 
 thirty association Switzerland compete , 
 composition perform varied character , 
 range Donizetti , Rossini , Verdi , Kreutzer , 
 Lachner , Wagner ; Concert massed 
 band , number close performer , 
 include Beethoven ' ' Die Himmel rithmen des Ewigen 
 Ehre , " Zwyssig stirring Schweizer Psalm " Trittst ins 
 Morgenroth , " , national hymn 
 ' ' Rufst Du , mein Vaterland , " absent swiss 
 patriotic gathering kind ; finally , composition 
 Professor Wolfenberger , judge , 
 Haubold , conductor St. Gallen orchestra 

 regard " Courts , " ' ' Kampfgerichte , " 
 Festivals compose distinguished swiss 
 musical authority select canton outside 
 St. Gallen view ensure perfect impartiality . 
 principle Courts award 
 lay " Rules Regulations 
 Federal Musical Matches , ' head harmony , 
 rhythm , pronunciation , modulation , execution , general 
 effect choral performance ; instrumental , 
 adequacy composition , purity tone , rhythm , power , 
 conception , general effect . award choral 
 performance divide class , excel- 
 lent downwards , mark vary 
 good fourteen meritorious per- 
 formance ; instrumental award , hand , 
 group class , mark extend 
 eighteen ninety ; prize consist laurel 
 wreath high oak wreath low 
 class , accompany special 
 award shape valuable present 

 know Mr. Behnke highly successful method 
 acquire widow daughter , 
 fully prepared qualified carry life - work , 
 away time 
 begin reap merit reward 

 VicTorR WILDER , frequent 
 occasion mention Foreign Notes , account 
 surprising literary activity service chiefly 
 musical art , die Paris 8th ult . , age - seven . 
 native Ghent Belgium , study 
 local University , proceed , usual course , 
 doctor degree philosophy civil law , 
 pupil Conservatoire 
 town . 1860 abode Paris 
 capacity /itterateur musical critic , soon come 
 identify literary artistic life 
 french capital . excellent linguist , turn 
 attention adaptation consequent populari- 
 sation french language series song 
 Schubert , Schumann , Brahms , , 
 follow number admirable french version ot 
 german italian operatic libretti , " L’Oca del 
 Cairo , " Mozart , ' " Sylvana , " Weber , 
 ponderous wagnerian music - drama . 
 able version Handel ' Messiah , " ' Judas 
 Maccabeeus , " ' * Alexander Feast ’' omit 
 connection . M. Wilder painstaking , 
 , successful translation Wagner drama , 
 ' Lohengrin " ' ' Parsifal , " ' probably 
 consider important achievement ; 
 melancholy reflect existence 
 prematurely cut short eve 
 production Wagner chef - d’auvres Paris 
 Opéra — viz . , ' ' Die Meistersinger , ’' performance 
 realisation able translation book 
 render possible . M. Wilder , 
 author valuable biography Beethoven 
 Mozart , contributor Le Ménestrel Le Guide 
 Musical , year past esteem 
 influential musical critic Paris Gil Blas 

 follow death , record , 
 occur past month abroad , viz 

 612 MUSICAL TIMES.—Ocroser 1 , 1892 

 Messrs. Harrison Birmingham , conjunction Liverpool Orchestral Society , Mr. Rodewald , ad 
 Messrs. J. Muir Wood Co. , announce | hand , thing , Beethoven * * Eroica , " 
 season set Subscription Concerts same|Haydn . 2 , D , movement Schubert ago | 
 lines‘and principle management as|c major , Schumann C major symphony . 
 series centre hardware industry . ] heavy order amateur performer comin 
 need hardly arrangement provide | season , course half - - dozen concert sai 
 appearance Madame Adelina Patti , inaugurate | . direc 
 scheme 26th inst . Messrs. Paterson , Sons Co.| Sunday Society announce seven orchestral ; 
 field , usual , goodly array | Ballad Concerts , Lecture Carols Christmas tl 
 leading artist , chief Madame Nordica ] Day , come session , band | 
 Mr. Sarasate . South Choral Society | increase close performer . hope 
 Handel " ' Samson , " revival amateur cordially | musical afternoon , little later , tran . th 
 welcome , similar recognition enterprise | ferre St. George Hall . project recently 
 Paisley Choral Union . flourish | defeat vote City Council , { 
 organisation season Gounod | body appear steadily grow generousin overc 
 " Redemption " Sullivan " golden legend , " | matter year year . surro 
 Orchestral Concert band Glasgow ] Birkenhead Cambrian Choral Society natura ? ough 
 Choral Union Mr. Manns direction . Glasgow | receive hearty congratulation loca , costs 
 Amateur Orchestral Society , - second year , | friend victory score Rhyl Eisteddfod , Recit 
 model youthful prowess , resume | suggest prize - money 
 season work announce series Concerts . | spend trip Chicago , rumour 
 Glasgow Quartet society buckle | Dowlais Choir think . Birkenhead people — suc 
 armour , safely a|propose produce J. Parry " Saul Tarsus " malle 
 peculiarly active winter store musical Glasgow . shortly . loc 

 richly - stage performance Rossini ' ' Cinderella " | Subscription Concerts Bootle , Liscard , West 
 place Theatre Royal - 5th ult . | Kirby abandon owe scant support — t 
 follow night . pasticcio — avowedly — | extend admirable entertainment , way , 
 , , admirably sing act opera ! local society , , probably district tional 
 company direction Mr. Mrs. Leslie | render season fairly busy . ap 
 Crotty , provide exhilarating } series Subscription Concerts announce 

 rehearsal night clash engagement . | attract crowd people enjoy Sol - fa 
 West Derby Philharmonic Society , Mr. | comparative leisure , certainly , ambitious notatic 
 Cooper , hand Handel " Messiah , " Mozart | speculator rush avail oppot 

 King Thamos , " Dr. W. H. Hunt ' ' Stabat Mater , " and|tunity Mr. Lane relinquishment afford . Mr. 9 . 
 Bennett " Queen . " Rock Ferry , old estab-| Cross , , continue modest Saturday 9 Bridge 
 lishe musical society Mr. Pemberton rehearse | evening entertainment somewhat mixed character Ballad 
 Beethoven ' ' Mount Olives , " Massenet " Narcisse , " |the Y.M.C.A. , probably utilize sem 

 Mozart ' " * Twelfth Mass. " ' result newly grandiloquently 
 Newton - le - Willows Musical Society ' prepare | announce " Manchester School Music , " mi " 
 Cowen " St. John Eve " ? Macfarren ' ' Day , " |}on account confuse real Manchester 

 CaTTERIcK , YorRKs.—A scries Organ Recitals Evening 
 service Sundays commence Parish Church . Mr. 
 Jas . Finlay , Peebles , , appreciate , 
 11th ult . programme include work Haydn , Mendelssohn , 
 Gounod , Widor , & c 

 CREWKERNE.—An Organ recital Parish Church 
 Monday , 12th ult . , Organist , Mr. Louis A. Brookes . 
 follow programme listen large congregation — 
 Allegro , Mozart ; hear prayer , Mendelssohn ; Largo Cantabile , 
 Haydn ; fugue " St. Anne " tune , Bach ; ' gondola song , " 
 Spohr ; Poco Adagio , Kozeluch ; Larghetto , Beethoven ; Marche 
 Militaire , R. H. Pearce 

 EASTBOURNE.—On 15th ult . seventh Classical Concert 
 Devonshire Park , direction Mr. Norfolk Megone . 
 orchestra good account Beethoven Sym- 
 phony piece Berlioz , play accompaniment 
 movement Rubinstein D minor Concerto , pianoforte 
 ably render Miss Margaret Gyde , Saint- 
 Saéns Rondo Capriccioso , violin render 
 Miss Joseph Harmer . vocal piece contribute Miss 
 Lucille Saunders Mr. Albert McGuckin . Miss Gyde play 
 Saint - Saéns Caprice Airs Alceste , gain encore 

 Eppinc.—On 4th ult.a Festival Evensong render 
 new Parish Church St. John , Epping Forest Church 
 Choir Association . Mr. J. W. Ullyett conduct , organist 
 Mr. Henry Riding Mr. Donald Penrose . similar servict 
 hold St. John , Loughton , 14th ult 

 lady ’ voice 

 ae page detailed list application . 
 ally weak Tschaikowsky Opera ‘ ‘ Eugeny Onegin " 5 es ua pri : 
 co ae ora . ROBERT COCKS & CO . 
 — Beethoven Sketch Books — . 5 . sketch ... + . 589 NEW edition 
 EWS Giovanni Benvenuto Cellini " t pc rc ee « + 592 STANDARD classical PI ece , 
 | ¥ occasional note .. .. ooo + + ee 593 | thoroughly revise , - finger , newly engrave , specially 
 se Facts , Rumours , Remarks .. ee oe ee ae + e 596 Associated Board Trinity College Examinations . 
 paper Gloucester Musical Festival ey ae sa ce a3 .. 598|   ' series good musical publication day , highly 
 Cardiff Musical Festival 6a pemame svenenees Se Graphic . 
 - evening " Haddon Hall " Savoy Theatre .. ey ee oo aie aaa : lite te cae 
 rch . mt | " Cigarette " ... . 6or ) " * CLASSICAL MUSIC " ( new series ) 
 ciate , " Wedding Eve " es - 6or : 
 rdelssohn , ; s select , finger , edit 
 National Temperance Choral Union ae « a3 « . 601 

 ADOLPHE SCHLOESSER 

 X J ANTED , country , thoroughly practical 

 experienced tuner ( equal temperament ) , capable 
 ordinary repair , repair Harmoniums american 
 Organs . apply , state age , marry , salary require , testimonial , 
 photo J. Bentley , Beethoven House , Nantwich 

 W ANTED , PIANOFORTE TUNER ( 

 es vo oo friend delight join voice . ”—Birmingham Daily student PIANOFORTE , VIOLIN , ORGAN , 

 s. d. instrument . a. LEFFLER ARNIM , author " Health Maps . " 
 nderness , PartsI.andII . .. .. « ee ef ef e- 1 0 minute ’ daily practice exercise reduce two- 
 -declama- Compiete , paper cover .. + " ae _ Pe & 6 usual necessary hour mechanical practice . addition 
 gh level Ditto , paper board ... ww Swe oo passive exercise assist cultivation refined 
 second ~ scarlet cloth ee ee ee oe ee sere os delicate touch . instrument require . 
 , Numbers ae ne " « . o 2 Sir Chas . Hallé : ' .... hesitation 
 n . " . + . Tonic Sol - fa ( complete ) , 9d . ; , singly , 1d . . think exhaustive useful . " 
 € speaker Mr. Arthur O’Leary : " . . . . confident exercise 
 spire recommend likely good result . " 
 invariabl VOCAL TRIOS school use . Signor Papini : ' ' .... invaluable study 
 y , 
 eal tragic violin instrument require flexibility finger . " 
 | in- _ _ ane eer 
 scene , BIRDS Ox HUMANA V : ° 
 } -—Voice production develop- 
 sty ment , art singi irely igi 
 ging . entirely novel original 
 ry Dt . word EpwarD OXENFORD method study voice . practical artistic . 262 para- 
 y hich graph , 33 illustrative ise . J. W. Bern 
 rds W y Sy Tative music exercise . y j- . E - 
 stone , Sly ae HARDT , F.R.A.M. Crown quarto , stout paper , 5 . ; cloth , 7 . | 
 spiratin ALFRED R GAUL SIMPKIN , MARSHALL , HAMILTON , KENT Co. 
 ) h . . 
 rrength ? " 1 . Robin . 4 . Skylark . \ K jante ( notation ) , copy Mozart 
 2 . Nightingale . 5 . Owl . 12th mass , Mendelssohn " Lauda Sion , " Smart " Bride 
 3 . Dove . 6 . Swallows . sage engl low price , Felix Watkins , Beethoven House , } 
 t. Asaph 

 lly score § " bythe production trio townsman , Mr. A. R. Gaul , s g h t si n gi n g m n u a. s 

 ther piece transpose , Peck , 36 , Southampton Street , | TILBORGHS , J.—Scherzo ( free style ) e " 
 Stand , W.C J. 4 London : Laupy Co. , 139 , Oxford Street , Ww 

 636 MUSICAL TIMES.—Ocroser 1 , 1892 . 
 nr ° ' : ah peer 8 
 Edwin Ashdown selection chorus Treble Voices . | 
 PIANOFORTE accompaniment . price 4d . net . t 
 1 . Steersman , steer bark . - part(Flying Dutchman ) Wagner ) 88 . come away wood . - .. ex C. Gounod 
 2 . wind . - ( Flying Dutchman ) Wagner ! 89 . Miserere Domine . - .. C. Gounod 
 3 . spin chorus . - Sead Sunnie ? .- Wagner| . rest noon . - ( Mountain Maidens)F. Romer 
 4 . singe liketh . - eS .. Sir G.A.Macfarren| 91 . break day . - ( Mountain seaman F. Romer 
 5 . parting . - .. ne ae .. Sir G , A. Macfarren| 92 . haste away . - ( Bride Burleigh ) .. - F. Schira 
 6 . Mermaids . - .. - . Sir G.A. Macfarren| 93 . morn bright . - ( Bride Burleigh ) . - F. Schira 
 7 . raise high song . - ( Lohengrin ) .. Wagner } 94 . Hailtothee . - ( Bride Burleigh ) .. - F. Schira 
 8 . adown wav’ring billow . - ( Sea - Maidens ) 95 . maypole . - ( - tide ) ..   Mazzonj 
 L. Roeckel| 96 . o’er smile meadow . - ( - tide ) P. Mazzonj 
 g. ' ti happy season . - ( Sea- Maliens ) 97 . o ! praise flow’ret sweet . - - F. Rogers 
 J. L. Roeckel ] 98 . sleep . - ( Magic Flower ) .. F. F , Rogers ComPc 
 10 . Barcarolle ( oh ! ' ti pleasant " ) . - part(Oberon ) Weber } 99 . pure , sister tender love . - . eber 
 11 . whispering breeze . - ( Knight Palestine ) Weber| 100 . pleasure . - ( Knight Palestine ) Weber 
 12 je y ! joy ! joy ! - ( Knight Palestine ) - » _ Weber } tor . Oberon fairyland . - .. « + Stevens " 
 13 . theearth clothe flower . - ( Fallof Leaf ) O. Barri } 102 . blow , gentle gale . - .. os + + Bishop predece : 
 14 . heart joy . - ( Fall Leaf ) + » O. Barri } 103 . hark ! apollo strike lyre . three- -part < e + + Bishop " Mus 
 15 . sing , sweet bird . - ( Fall Leaf ) . .. © . Barri ] 104 . wind whistle cold . - ea ee + » Bishor previous 
 16 . haste church ! - ( Bride Burleigh ) .. F.Schira| 105 . chough crow . - oe oe - » Bish / ' 
 17 . chorus angel . - ( Bride Burleigh ) .. F.Schira| 106 , sound loud timbrel . - Avist , 9 dry 
 18 . awake slumber . - ( Magic ) .. F. Abt| 107 . ye shepherd tell ( wreath ) . three- -part . * " Mazzinghi Mendels 
 1 g. o’er flower - bespeckl’d meadow . - ( 108 . Harvest home . three- -part . Bishop Foster J choral si 
 Magic ) .. F , Abt } 109 . pearl - crown’d moon . three- -part E. Aguilar fair shar 
 20 . land wondrous beauty . three- -part ( 110 . O ! finda song . - ( Summer night ) .. --E. Aguilar , 
 Magic ) .. F , Abt } 111 . wind blow . - ae + " « + . Bishop continue 
 21 . come , let wreathe " bridal flower . - 112 . alullaby - . ee ee G. Schmitt thin ! 
 ( Orpheus Eurydice ) Offenbach | 113 . come hill . - ais es - » H. Foster produce 
 22 . music — three- -part ' ( Orpheus Eury- 114 . sower . - " 6 eo - » D’Albano 
 Gite ) .. Offenbach | 115 . home rest . - . ee se ve « -C. Pinsuti lessiah 
 23 . homeward wend . three- " ( - tide ) P. Mazzoni | 116 . memory . - ae « 9 -C. Pinsuti § work ar 
 24 . trip lightly , gaily , brightly . - ( Harvest Queen ) 117 . good - bye , eonsthoart , good- bye . hue J ; L. — _ | 
 Godwin Fowles | 118 . city king . - .. es 0.b pose thos 
 25 . angel peace . - ( Lohengrin ) . - Wagner | 119 . Erin mavourneen . - j. L , Pn composel 
 26 . thee trust . three- ai — _ — chorus , Tann- 120 , blessed poor Spirit . - ( sacred ) « Mozart 
 hauser ) .. ee + . Wagner | 121 . old cloister . - .. ' * Beethoven 
 27 . awake ! awake ! " three- . oe = C. Gounod | 122 . roll , fair orb , - ue ae Beethoven " th 
 28 . music sunshine . three- -part ' & . Gounod | 123 . Thee trust . - ( Sacred ) sa « + Handel ' ee 
 29 . joyous life . - ( Elfin Knight ) I. Gibsone | 124 . ring , sweet bell . - .. ae fe C. Gounod tthe 
 30 . sing , sister , sing . - ( - Hallow Eve ) .. B. Gilbert | 125 . minstrel . - .. " Pe oe C. Gounod pot ef 
 31 . sunrise . - ( Mountain Maidens ) . .. F. , Romer | 126 , evening song . - " aa - C. Gounod wale Da 
 32 . sunset chorus . - ( Mountain Maidens ) .. F , Romer | 127 . wood - nymph home . three- -part eC ee C. Gounod ° 
 33 . old piano . - ..   . .. F. Auger | 128 . song father love . - Pe Wagner Bow 
 34 . home bell . - . ee G. Schmitt | 129 . gipsy chorus . - part(Bohemian Girl ) .. M. W. Balle byan aud 
 35 . dear home . - ( Tannhiuser ) se -- Wagner | 130 . fair . - ( Bohemian Girl ) .. M. W. Balfe work , 
 36 . summer . - ( Tannhiuser ) .. « + Wagner | 131 . droop maiden . - ( Bohemian Girl ) M. W. Bale § possible 1 
 37 . thee hath beauty . - ( Oberon ) - . _ Weber | 132 . deign forgive gipsy maid . - ( Bohemian Girl ) estimatin , 
 38 . remember eve . - ( Elfin Knight ) , Gibsone M. W. Balfe ¥ com 
 39 . vintage time . - ( Elfin Knight ) , Gibsone | 133 . maiden dream . - - » Sir Julius Benedict Mr. 
 40 . odour come quiet spell . three- _ ( Summer 134 . thy starry dwelling . three- -part ahaa wo 
 night ) .. oe E. Aguilar Moses Egypt ) « Rossini remarkabl 
 41 . sad parting . two- -part " ( volkslie ) | . ne .. German | 135 . father Heaven . three- -part ( Prayer , Masaniello ) Auber " hole . " — . 
 42 . griefalone . - .. ye os eos 3 Smart | 136 . sylvan song . - .. " Wagner " disti 
 43 . Christmas song . - * e + s A. Adam | 137 . lady fair ( Villanelle ) . three- -part + e Niedermeyer § f Daily Post 
 44 . hymn peace . - ae " . W.-H. Callcott 138 . stay , prithee , stay . - .. s Bishop " § 
 45- o ! southern wind . - ae ee ' O. Barri | 139 . o ! skylark thy wing . - ve ae Mendes fvourite | 
 46 . dweller spirit land . - .. ae -- O. Barri | 140 . light fairy foot fall . - .. Weber # Daily Gaz 
 47 . ' " Tissummer . - Js + é .. O. Barri | 141 . evening bell . - .. ce » Zamboni abo 
 48 . o ! come grove . alec ae ~ - . O. Barri | 142 . haste thee , boatman . - .. ae ae " F , Kiicken ments V 
 49 . snow . - .. . ee ee o . » QO . Barri | 143 . margin river . - .. - » Henry Smart num| 
 50 . come . - .. ' ae - ee O. Barri | 144 . bird morn . - .. ee .. Henry Smart . 
 51 . Christmas carol . two- -part ie " C. Gounod 145 . wander wind . - .. .. Henry Smart Price , pag 
 52 . midnight bell . - J. L. Roeckel | 146 . song sea breeze . - ee " C . L. Hatton Sol - f 
 53 . scented violet . - ( " Magic ) _ F. Abt | 147 . heavenly music . - .. ' L , Hatton 
 54 . , star ofeve . - ( Elfin Knight ) .. , Gibsone | 148 . adieu , ye woodland . - .. = + % ee = f Abt | 
 55- cuckoo . - ( Elfin Knight ) I. Gibsone | 149 . nightingale singe . ' - oe oe oe _ F. Abt 7 
 56 . come , let . - ( Elfin Knight ) ; I. Gibsone | 150 . sing , gondolier . - .. ne ae C. Gounod 
 57 . seek thee . - ( Mountain Maidens ) F. Romer | 151 . , merry fay . - .. oe = e C. Gounod 
 58 . village maiden . - iain P. Mazzoni | 152 . voice Spring . - .. oe ie . Offenbach 
 59 . John Peel . - part(hunting song ) .. D. Pentland | 153 . haymaker . - .. " # 6 . « Offenbach 
 60 . o ! come hither . - ( Sacred ) oe -- Henry Smart | 154 . meadow . - eR os ae Lotti 
 61 . o ! thank . - ( Sacred ) Henry Smart | 155 . thy service . - ( Sacred ) vs ar J. Blockley 
 62 . o ! praise Lord . - ( Sacred Mendelssohn | 156 . think thee . - .. o ° ce ..   R. Schumann 
 63 . hark ! hunter merry horn . - ae L. Zamboni | 157 . o ! ye sunny hour . -part ee ee .. _ R. Schuman | 
 64 . old english pastime . - .. oe .. J. L. Battmann | 158 . good night . - pa ea $ 5 « .   R. Schumann 
 65 . organ - grinder . - ee oe .. J. L. Battmann | 159 . cradle ieonting billow . - $ s . » H. Foster 
 66 . happy fireside . - ne .. Henry Smart | 160 . Serenade . - .. es ae .. J. B. Wekerlin PRODUCE 
 67 . hark ! yon old abbey . two- os J. Barnett | 161 . sailor return . - oe oe ee Wagner BIR 
 68 . merry . - - . Sir G. A. Macfarren | 162 . song June . -   .. « e .. _ Mendelssohn 
 69 . alpine sister . - f - »   D. Tagliafico | 163 . singe inthe rain . - .. . P. Knight 9 " Exe 
 70 . heap high golden corn . two- -part _ -- Raphael | 164 . ferry boat . - .. ' * e es . Williams Jind 
 71 . lull’d silence . - .. > ea .- Zamboni | 165 . spring descendeth . - .. = feab 
 72 . fairy sister . - ‘ és o6 Kes . F. Packer | 166 . art thou , beam light ? - ne .. Bishop , Bormore int 
 73 . merry . - " o ve " ie .. T. Distin | 167 . far away . - oe eo os ° « . J. R. Thomas ” fi vine , 
 74 . swallow ’ return . - .. aa - . T. Distin | 168 . Italy music . - .. + e ee ar c.e , hom § une 
 75 . Thesea - nymph . .. mee oe oe .. T. Distin | 169 . Nina farewell . - .. ee Pergolesi Shields cho : 
 76 . Merrie gipsy . - .. ie .. T. Distin | 170 . gipsy home . ' - . " s ee | Herbert Ostet bi hisce y 
 77 . hail ! pretty babe . - oe oe .. T , Distin | 171 . vivandiére . three- -part fy ; f , Schira wa 
 78 . sleigh - bell . - oz . T. Distin | 172 . peace . - Sir Julius Benedict toon fay 
 79 . hath Lord . two- -part ( St. Paul ) ; . Mendelssohn 173 . sing , pretty maiden . three- -part ( Maritana ) oe Wallace 
 80 , Paul come congregation . - Mendelssohn | 174 . Angelus . - ( Maritana ) .. os ee .. Wallace paper 
 81 . God high . - ( Chorale , St. Paul ) Mendelssohn | 175 . pretty gitana . - part(Maritana ) .. oe . » Wallace cover 
 82 . o ! thou true light . - ( Chorale ) Mendelssohn | 176 . alas ! chime . - ( Maritana ) .. ~. Wallace ear 
 83 . howlovely messenger . - ( Chorale ) Mendelssohn | 177 . turn , old time , - ( Maritana ) , .. Wallace t cloth 
 84 . away ! away ! - .. i. - + . J. L. Roeckel | 178 . God save queen . - .. .. Nation 9 tue 
 85 . angel ’ song . three- oo ae os C. Gounod | 179 . , sad heart , art thou beat ? - os F. Abt Onhestral P ; 
 86 . farewell . ' - - RN ae C. Gounod | 180 , Eve lamentation . - .. ..   « M. P. King [ hice 7 , 64 , 
 87 . dawn , - ar ae oe Cc . Gounod 181 . thy spirit near . - ve ..   Henrv Sm ite gratui 
 OMPO 
 EDWIN ASHDOWN , LIMITED , - 
 r Lo 
 NEW YORK . HANOVER SQUARE , LONDON . TORON ? » NDON 

 eae 4a 

