MUSICAL time 

 hymn Whitsuntide . Woelfl Rondo , inc 7 206 
 na day ready , hymn 12 Dussek air variation , bs . ot fare 
 " cc te , cheat " uct Yomunlc , eomecned b 13 , Sterkel Andante , e flat 
 st y INCE n ° t NOVELLO , p y 14 Haydn Rondo , op . 17 , G. 
 Voi tag m Satine dha Cuan /1§ Beethoven Rondo , C. , die 
 ‘ voice , Acompanime gan . |16 Mozart waltz 

 London ; J. A. Novello , Thematique Catalogue 

 ( musical publication Dr. Mainzer ) | 
 J. A. Novello , 69 , Dean - street , Soho ; Simpkin | 
 Marshall , Stationers ’ Hall Court , London 

 useful practice , edit R. Barnett . | 
 . ' ae : & 
 1 Steibelt sonata , a. op . 50 2 6 ) instruction . 
 2 Beethoven sonata , G. Op . 49 2 6 } CHURCH psalmody . manual sterling 
 3 Haydn Rondo , inc. .. 2 6|Psalm hymn tune , chiefly Old Church style . 
 4 J.N. Hummel romance rondo , 1g. 2 6 ) select , harmonize , partly compose Cuarzes STEG- 
 5 Steibelt Sonata , G , 2 6 / GALL , Royal Academy Music , Organist Christ 
 6 Clementi Sonata , f. op . : 2 6 / Chapel , St. Marylebone . selection particularly 
 7   Dussek Rondo ( rule Britwania ) , o. 2 6j / adapt Rev. W. J. Hall Selection psalm 
 8 Kalkbrenner Rondo , inC , .. 2 6|hymn , universally use metropolitan 
 9 Dussek Rondo , e flat 2 6 } church . 
 10 © Clementi Rondo , c. op , 21 2367 London : Coventry , 71 , Dean - street , Soho 

 ae ee ee 

 HAYDN MASS , number singly , quarto size . 
 i. 48 . 6.—ii . 48 . — ili . 38 . 6d.—iv . 4s.—v . 5s.—vi . 4s . 6d . — 
 vil . 38.—vili , 28.—ix . 4s.—x . 33.—xi . 1s . 6d.—xii . 38 . 6d . — 
 xiii . 38.—xiv . 38.—xv . 4s.—xvi . 55 

 BEETHOVEN C. Folio , 8 . 6d . ; Quarto , § s 

 BEETHOVEN d , Folio , 14s 

 HUMMEL e flat , 8s ) HUMMEL b flat , 7 

 Ist June commence 

 favorite masse MOZART , HAYDN , BEETHOVEN . 
 Masses form volume similar ' Judas Maccabeus , " " Messiah ; ’' , 
 completion , bind , correspond series , parchment cloth.—in addition original Latin 
 word , english adaptation mass 

 CHEAP oratorios—-work complete 

