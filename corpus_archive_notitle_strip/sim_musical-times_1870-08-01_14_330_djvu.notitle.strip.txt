ITALIAN OPERA, DRURY LANE

M. Amprotse THomas’s Opera, ‘ Mignon,” produced at 
this establishment on the 4th ult., may be pronounced a 
decided success, a result which the composer has reason 
to congratulate himself upon, considering that the incidents 
of the libretto, concocted to suit the palate of the Parisian 
public, have been so tampered with by those well known 
French cooks, MM. Michel Carré and Jules Barbier that, but 
for the familiar title“ Miznon,” few persons wouldimagine 
that Goethe’s “ Wilhelm Meister” was the work selected 
for desecration. We have so often expressed our opinion 
upon this subject, that it is needless to do more on the 
present occasion than record our surprise that those who 
aspire to the rank of literary men, cannot put together a 
simple love story, suitable for operatic purposes, without 
fastening upon the immortal creations of those who, if 
living, would have openly protested against such a viola- 
tion of those laws of honour, by which works of art should 
be protected as the sacred property of the world. Of M. 
Thomas himself we have no hope, for Goethe and Shaks- 
peare are merely considered by him as valuable aids in 
securing the good opinion of the public for his music; and. 
if he boldly grapples with such a subject as “ Mignon ’”— 
which even Beethoven and Schubert have only dared 
lightly to touch—he is perfectly justified, from his own 
point of view, in accepting the applause of an operatic 
audience as his legitimate reward. Dismissing this ques- 
tion, therefore, and looking only at the composer’s share 
of the work, we are bound to record that in “ Mignon” 
we have some of M. Thomas’s best music. We are by no 
means disposed, however, to rate even his best very 
highly; but in addition to a grace and elegance which 
rus through the whole work, there is such a variety of 
colouring, and such an artistic treatment of the orchestra 
is observable, even when the vocal parts are comparatively 
unimportant, that the attention is kept alive, in spite of 
the conviction that, musically speaking, there is little that 
can take a permanent place in the world of real art. The 
overture, which was encored, contains much skilful instru- 
mentation, and the Polacca movement is pretty enough to 
secure the applause of the lovers of light music, especially 
as it forms a good contrast with the andante, with horn 
solo, which precedes it. The first act contains some good 
bustling music, in which the gipsies, by whom Jfignon has 
been stolen, take an important part. A trio for Fi/ina, 
Guglielmo and Laerte is effective and characteristic of the 
situation. Mignon’s song, “Non conosci il bel suol” (better 
known by its French title, “ Connais-tu le pays,”) is by 
far the best solo in the Opera, the tender grace of the 
melody expressing most sympathetically the dreamy recol

in a.” ee e

the second act, and indeed sang the whole of the music| Allegro Vivace, from the MS. Concerto in C ” (excellently 
with a perfect mastery over its extreme difficulties.| played on the pianoforte by the composer, Mr. Shakes- 
Signor Foli was an excellent representative of the Nor-|peare), proved how admirably the training of the institu- 
wegian Skipper, Daland, and Signor Perotti (who lately | tion has been brought to bear upon the natural gifts of this 
made a successful début as Faust), created quite an enthu- | young artist. The “Adagio "is based upon a most winning 
siasm as Lik the Hunter. The Opera was well placed subject; and the “ Allegro Vivace,” which is somewhat 
upon the stage, and Signor Arditi contributed much to | Weberish in character, is freely written and instrumented 
its success by his watchful and intelligent conducting. | with much knowledge of orchestral effect. Mr. Shakes- 
r | peare, on retiring from the orchestra, was most warmly

jand deservedly applauded. There is also much to com. 
PHILHARMONIC SOCIETY. | mend in the Recit, and Air by Mr. Parry, which was 
Tue eighth and last concert of this Society for the pre- | effectively sung by the composer, and a chorus by Mr, 
sent season was given at St. James’s Hall on the 4th ult.,| Brion, and part-song by Miss Crawford, showed much 
the programe being devoted exclusively to the composi- talent in the young composers, the latter piece being espe- 
tions of Beethoven. The directors have indeed a right to cially graceful, and well written for the voices. Besides 
congratulate themselves upon having vindicated the the pianoforte performance of of Mr. Shakespeare, already 
character of England from the charge of neglecting the mentioned, Miss Linda Seates and Mr. Kemp played the 
memory of this great composer, who was born one hun-| Andante and Rondo from Mozart’s Concerto in E flat, for 
dred years ago—for as Mr. Macfarren truly says in his tyo pianotortes, with marvellous precision and effect, Miss 
“ Analytical Programme,” “ Who shall say how much of | ‘Townshend gave an intelligent reading of Hummel’s 
the vast changes in the inward constitution and outward | Fantasia on an Indian air, Miss Westmarland performed 
acceptance of musical art which have been wrought with- | Weber’s Concertstiick with a decision and spirit scarcely 
in this period, are due to the creations and influence of his | to be expected from so young a student, and Miss Chan- 
wonderiul genius?” The task of representing the differ-| nel} displayed much musical feeling and facility of execu- 
ent styles uf Beethoven at the various stages of his career tion in Professor Bennett's Caprice in E. The only other 
was, as may be imagined, an extremely difficult one; but instrumental piece was Beethoven’s Romance in F, for the 
as far as the limits of an ordinary—or perhaps we may say violin, in which Mr. Morley sufficiently proved that he is 
an extraordivary —evening concert would permit, the idea progressing in theright road, Miss Ferrari received quite 
was fairly carried out. Commencing with the first Sym-|an ovation for her very refined rendering of Meyerbeer’s 
phony, aud ending with the niuth, we had certainly the|« En vain j'espere,” and Miss Marion Severn, Miss Mauds- 
two extremes of his style; but considering that, in addi-|Jey, and Miss Rebecca Jewell were also highly effective in 
tion to these works, we had the Dervish Chorus from the |t¢he solos allotted to them. The powers of the choir were 
“ Ruins of Athens,” the Scena, “ Ah Perfido,” the “ Choral | successfully tested in Linley’s Madrigal “iiark, the birds,” 
Fantasia,” the Terzetto, “ Tremate,” and the Overture to | and also in Mendelssohn's two-part song, from “Ruy Blas,” 
“ Leonora,’ there can be no question that the attention of| « Why listen to the Carols?” the brightness of the soprano 
the listevers was too severely taxed to admit of the possi-| voices being especially observable. ‘The prizes were dis- 
bility of that close attention which such music demands. | tributed by Mrs. Gladstone, previously to which an address 
All these compositions were on the whole well executed. | was read by Professor Sterndale Bennett, Principal of the 
We could have desired a better soprano than Miss Arabella| Academy, in which the highly satistactory artistic state of 
Smythe, both in « ‘Tremate” and the Choral Symphony ;| the institution was particularly dwelt upon ; for not only

but Miss Juiia Elton, Mr. Cummings, and Mr. Santley 
gave much strength to the cast, the latter especially sing- 
ing the Bass recitative, which commences the vocal part 
of the Choral Symphony. better than we ever remember to 
have heard it. he great Scena,“ Ah Pertido” taxed 
Madile. Nils-on’s powers to the utmost, but the impassioned | 
portions of it were thrown out with such true dramatic 
feeling that we cannot wonder if some shortcomings in| 
the more expressive parts were forgotten in the burst of 
applause with wich she was enthusiastically greeted at 
the end. Madame Arabella Goddaid played the piano- 
forte part in the Choral Fantasia with the utmost delicacy 
and refivement, and the vocalists sang with much decision 
and firmness. It is not fair to be tuo critical on the ninth 
Symphony. <A perfect performance of this grand work is 
not to be expected with so few rehearsals; and more espe- 
cially at the high pitch which, in spite of repeated failures

An interesting little episode occurred during 
the Annual Examination of the students atthe Royal 
Academy of Music, on Saturday the 16th wt. The pro- 
ceedings being temporarily suspended, Mr. George Mac- 
farren, in the name of the whole body of Professors 
attached to the Institution, presented to Professor Stern- 
dale Bennett, the Principal, an address of congratulation 
on the honour recently conferred upon him by the 
University of Oxford

Miss Erten JARMAN gave a concert at the 
Hanover Square Rooms, on Thursday, the 7th ult. T'he 
bensficiare played standard works by Beethoven and Woelf, 
in all showing herself a pianist and musician of ability. 
She was assisted by Madlle. Delise, Madlle. Santos, and 
Miss Lyndhurst ; Messrs. Walter Reeves, C. J. Bixhenden, 
W. C. Bell, and Madame Rivoldi (from a Scala\, who has 
a voice of great volume and richness. Mr. Lansdowne 
Cottell and Mr. C. F. Weber conducted ; and the concert 
was highly successful

Mr. Laxspownr Corrett gave his fourteenth 
and last Concert for the season at Wornum’s Concert 
Hall, Store Street, on Saturday, the 16th ult., to an over

composition, Truhn’s glee, ‘‘ The Chafers,” was substituted, 
which was so humorously rendered, that a second recall was 
demanded. Mr. H.S. Trego conducted with much ability, 
and must be congratulated on the success of his concert

Mr. ArExanpErR Coopsr’s Concert took place 
at the Beethoven Rooms, Harley Street, on Friday evening 
the 24th June, before a large audience. The programme 
was an interesting one, and included Mozart’s Trio in K, 
Beethoven’s Duet Sonata in F, and Schumann’s “ Blu- 
menstiick” and “ Nachtstiick.” These were all most 
efficiently renaered by the concert-giver, assisted by 
Messrs. H. W. Hill ana ‘Walter Pettit, and received with 
hearty applause. The vocal portion of the programme 
included Handel’s “‘Cangio d’ aspetto” (well sung by 
Miss Marion Severn), two songs by Schumann (given 
with taste and expression by Madame Talbot Cherer), 
“Tn sheltered vale” (sung by Mr. Chaplin Henry, and 
encored), and several of Alex. Cooper’s most recent com- 
positions—“ If I were yonder wave my dear ” (Barcarole 
for tenor), ‘“ Love for Love” (canzonet, also for tenor), 
“ Hope in Tears” (for contralto), and two new M.S. glees. 
The execution of these, with the exception of ‘‘ Hope in 
Tears,” somewhat betrayed the effect of insufficient re- 
hearsal. Haydn’s Trio in G brought the concert to a 
successful conclusion, and the beneficiare received quite an 
ovation

Miss Karnertne Poyntz gave her annual 
evening Concert at the Hanover Square Rooms, on the 
7th ult. The programme was well selected, and Miss 
Poyntz received the warmest applause for her vocal solos, 
which were sufficiently varied to test her powers both of 
execution and expression. The vocalists who assisted the 
concert-giver were Madame Osborne Williams and 
Madlle. Drasdil, Messrs. W. H. Hillier, W. H. Cummings 
and Harley Vinning. The instrumentalists were Mr. 
Richard Blagrove (concertina), and Mrs. Richard Blagrove 
and Mr. W. G. Cusins, pianoforte

The object of the entertainment was to show the value and 
powers of the national instrument of Wales, the triple-stringed 
harp, as well as to give specimens of the unaltered ancient Welsh 
compositions, vocal and instrumental, and the crowning incident of 
the night may be considered as an event in the history of music— 
viz., the performance of a concerto composed by Handel for the 
celebrated Welsh harper Powell, who used to play before George IL. 
This valuable manuscript has been recently discovered in the British 
Museum by Mr. Brinley Richards, and was executed with the most 
marvellous precision from memory on the triple harp of Wales, by 
Herr Sjiéden, himself an eminent performer on the pedal harp, with 
accompaniments for two violins, a viola, violoncello, and double 
bass. It was played from the original score of Handel's MS., and 
consists of three movements, an “Allegro,” which abounds with 
passages difficult enough to puzzle most modern players on any harp; 
a very graceful and melodious ‘Larghetto;” and the “ Finale,” a 
quaint and charming specimen of the great composer in his most 
mirthful humour; and, unlike his other ‘‘concertos,” this one is 
“scored” for stringed instruments only. Herr Sjiden, it should be 
repeated, played the entire work from memory, and, still more 
remarkable, he p!ayed it upon the Welsh triple-stringed harp, the 
instrument for which it was composed. The accompaniments were 
very carefully performed (Mr. Weist Hill, first violin) under the 
direction of Mr. Brinley Richards, whose exertions were well known 
to be a labour of love

Tue Concert of Herr Lehmeyer, the well- 
known pianist, took place on the 18th ult., at the Beet- 
hoven Rooms. The programme was devoted wholly to 
the works of Beethoven and Schubert. The singing of 
the Mesdlles. Doria was much appreciated during the 
evening. The quartet in canon from “ Fidelio” by these 
ladies, with Mr. Stedman and Herr Deck, was especially 
well sung, and received an unanimous encore, Mr. Bene- 
dict’s refined accompaniment contributing much to the 
success of the piece. Mr. Stedman gave “Sei mir gegrust” 
(Schubert), in a highly artistic manner, the same com- 
poser’s “‘ Wanderer” being finely rendered by Herr Deck. 
Herr Lehmeyer was assisted by Sig. Scuderi (violin), and 
Mons. Albert (violoncello). The other artists were Mdme. 
Thaddeus Wells and Miss C. James. Mr. Parker and 
Herr Ganz conducted

Tue last Concert for the season of S. Mark’s 
Choral Association took place at the School Rooms, 
Rawstorne-street, City-road, on the 29th June. The 
performances of the choir were extremely successful, in- 
cluding the “Sanctus” of Bartniansky, the “ Pilgrims” 
(Leslie), ‘Sweet and low” (Barnby), “ Ave Maria” (Henry 
Smart), “ Dawn of Day” (Reay), and “ Hear my Prayer” 
(Mendelssohn), the solo in which was capitally sung by 
Miss Maria Langley. ‘The soft southern breeze,” from 
Mr. Barnby’s new Cantata “Rebekah,” was effectively 
given by Mr. Stedman; and Mr. Paget also sang some 
bass solos. Liszt's arrangement of airs from “ Rigoletto” 
was well played by Miss Florence Martin, and the per- 
formance of Herr Jacoby on the violin was highly appre- 
ciated. Mr. James Robinson conducted as usual, with 
Mr. J. Tunstall as accompanist

To the list of new works promised at the 
forthcoming Birmingham Festival (given in our June 
number), we have now to add a Choral Ode by Dr. 
Stewart, composed expressly for the occasion. We may 
also mention that at the miscellaneous evening concerts, 
Mendelssohn’s Concerto in G minor will be played by 
Madame Arabella Goddard, the “ Kreutzer” Sonata will 
be given by Madame Goddard and M. Sainton, and the 
Oveitures to “Der Freischiitz,” ‘ Zampa,” and “ Guil- 
laume Tell,” will be performed. It is also announced 
that on the Wednesday evening, the second part of the 
concert will consist entirely of selections from the works 
of Beethoven

Mr. Ettts Roserts gave a Concert at St. 
George’s Hall, under the patronage of His Royal Highness

MENDELSSOHN’S Lieder ohne Worte, the 
8 Books complete .. coe

BEETHOVEN’S Thirty- eight Sonatas... 5 0 
PY Thirty-four Miscellaneous

Pieces... ee oa

Piano Solo

BEETHOVEN. Thirty-eight Sonatas

Pianoforte Pieces (Rondos, &ec

Schwanengesang 
Twenty-two Celebrated 
Songs aa ove 
WEBER. Four Sonatas, complete 
— Pianoforte Pieces as tee

Piano Ducts. 
BEETHOVEN. Symphonies arranged by

Ulrich :— 
Vol. I—No, 1—5 
Vol. Il.—No. 6—9 
— Septett 
HAYDN. Celebrated Sy mphonies arranged by 
Ulrich :-— 
Vol. I.—No. 1—6 ee

Songs

BEETHOVEN. Songs, complete

SCHUBERT. Album, containing—Se héne Mil- 
lerin, complete ; Winterreise, complete ; 
Schwanengesang, complete; and Twenty- 
two celebrated Songs, original Edition

Pianoforte Scores, 
(g—German, f—French, i—Italian words

BEETHOVEN. Fidelio, G

Missa Solennis , 
CHE RUBINI. Demophon, g g. J 
—— Mass in ‘ie 
—— Mass in D minor 
—— Massin A . ; 
—— Requiem for male voices 
GLUCK Orphée, GS. 
—— Alceste, 0 ia 
—— Paris and Helena, 9, f 
—— Iphigenia in Aulia, y

ee SOO ee

GLUCK Armide, g, f ae 40 
— — Iphigenia in Tents, gt aes an 28 
0|JOMELLI. Requiem ee eg ea ee 
0|MOZART. Don Giovanni, gi ca ee 
0|—— Figaro,g.i .. . aes sia, 2G 
0| —— Zaubertlite, g, i aaa a «eo 6 
0| —— King Thamos, g Pee at ae Se 
0|ROSSINI. Il Barbiere, 9,7 ... oe ee 
0|}SPOHR. Jessonda, g aa ne ict ee 
0|WEBER. Freyschutz,g _... en ‘cae 
O|SCHUMANN. Faust,g... wm 
0 ad 
Complete Operas arranged for Piano 
: Solos. 
0 AUBER. Maurer und Schlosser 20 
0 BEETHOVEN. Fidelio 2 0 
0 BELLINI. Norma ‘<i 2 0 
Yt ee Sonnambula ... 20 
BOIELDIEU. Dame Blanche aa 2 0 
0 DONIZETTI. Lucia di Lammermoor 2 0 
0 Elisire d’amore ia rr Maes 
9 | HEROLD. Zampa aes rif eee 8 
MOZART. Don Juan i ae iio te 
—— Figaro = ies pee oe 2 0 
Zauberflite ... abe xa 2 0 
ROSSINI. Il Barbiere ae ig 2 0 
_| WEBER. Freyschiitz aaa 2 0 
0 | —— Oberon a oe 2 0 
0}__— Euryanthe... aa 2 0 
0 | _— Preciosa 2 0 
BEETHOVEN. Egmont 2 0 
BELLINI. Puritani ati ‘as is, (ee 
0 | —— Montecchi e Capuletti ... asa «s 2 9G 
0|BOIELDIEU. Jean de Paris 2 0 
CHERUBINI. W ee Deux Journées) 2 0 
GLUCK. Armide.. pe oa em (ae 
0|MEHUL. Joseph ... 2 0 
0} MOZART. Seraglio 2 0 
‘Titus ae 2 0 
0} ROSSINI. Otello ... es re 2 0 
SPOHR. Jessonda ‘a was 2 0 
o| Complete Operas arvanged as Piano- 
forte Duets. 
BEETHOVEN, Fidelio 3.0 
BOIELDIEU. Dame Blanche $ @ 
9) MOZART. Don Juan 4 0 
0|___ Figaro “a, 4 0 
O}___ Zaubertlite ... 3.0 
> 0 ROSSINI. 1 Barbiere 30 
rd WEBER. Freyschiitz 3 :@ 
0| Overtures for Pianoforte Solo 
| MOZ ART. Overtures, complete . = 0 
}BEETHOVEN. Overtures, complete 2 0 
|} CHERUBINL. Eight Overtures, ae 20 
)| WEBER. ‘Ten Overtures, complete... aku: sane 
SCHUBERT, SPOHR, LINDPAINTNER., Cele- 
| brated Overtures 2 0 
| BOLELDIEU, HEROLD, AUBER, SPON TINL. 
| Celebrated Overtures ; neat ee 
| BELLLNI, ROSSINI. Celebrated Overtures 2 0

o| Overtures as = Duets. 
0| MOZART. Ten Overtures, ¢ omplete ... 2 0 
0} BEETHOVEN, Eleven Overtures, compl 3 0 
0} CHERUBINI. Eight Overtures, cope 3 0 
0O| WEBER. Ten Overtures, complete... gow aaa 
0 | SCHUBERT, SPOHR, LINDPAINTNER. Cele- 
0 brated Overtures V0 
0| BOLE LDLEU, HEROLD, AUBER, SPONTINI. 
0 Celebra‘ed Overtures ji ico See 
0} BELLINI, ROSSINI, Cele brated Over tures .. 2

THE MUSICAL TIMES.—Avetst 1, 1870

Finally, Messrs. Novello, Ewer and Co. haye to announce that as they were the 
first to issue Handbooks of the Oratorios in a convenient style and form, they 
have resolved to apply the same principle to popular secular works, by publishing a 
series of Handbooks ofthe Operas, with English words, printed in the same size and 
shape as the Octavo Oratorios; and when it is added that the editorship has been 
undertaken by Mr. G. A. Macfarren, and the translations by Madame Mactarren, 
it will be seen that the Publishers are desirous of making the series unique as to 
accuracy and trustworthiness

It appears to Messrs. Novello hardly necessary to eall attention to the eftorts 
they have made in times past to improve their business, and render their Catalogue 
as nearly complete as possible; but- they may perbaps be pardoned for alluding to 
one or two improvements efiected during the last three years, such as the purchase 
of the business of Messrs. Ewer and Co., thereby bringing the entire collection of 
Mendelssohu’s works into their hands; the acquisition of the extensive premises, 
No. 1, Berners Street; the reduction in price of the greater portion of their 
publications ; the purchase of the Sacred Works of the great French musician, 
Gounod (which works are, by their deep devotional feeling, producing such a 
beneficial effect upon the services of our larger churches); the acquisition of the 
entire compositions. of Mr. Goss, Dr. Wesley, and Mr. Turle; the publication of 
cheap editions of Beethoven’s Graad Mass in D, and Bach's subline Passion ; and, 
lastly, the great and beneficial influence they have exerted upon almost all classes 
of musicians—whether professional or amateur—through the medium of the dZusice! 
Times

London : 1, Berners Street ( W.), and 35, Poultry (E.C.. 
August Ist, 1870

