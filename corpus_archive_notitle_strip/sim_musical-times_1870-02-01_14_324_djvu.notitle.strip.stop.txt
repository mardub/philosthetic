ENGLISH GLEE UNION — Messrs. Ashton 
 Pearson , Clagett Rudkin . terms , & c. , address Mr , z 
 Rudkin , 122 , Culford - road , N 

 ST . CECILIA CHORAL SOCIETY . — 
 Conductor , Mr. C. J. Hareitr.—For Performance 
 Cantatas large works little known country , 
 Cantatas Beethoven , Mendelssohn , Gade , Schneider , Gounod 
 & c. , studied past season . Rehearsals 
 Monday Evening , Grafton Hall , Grafton - street , Fitzroy- 
 square . Periodical Rehearsals complete works held , 
 assisted Eminent Professional Artistes . Subscription : — 
 Ordinary Members , 5s . ; Honorary Members , £ 1 1s . New Members 
 apply Taos . Cutt , Hon . Sec 

 XFORD ST . CECILIA GLEE PARTY 
 Messrs. Rowley , Guy , Robson , Sinkins . terms , & 
 address Mr. A. Rowley , New College , Oxford 

 Tue North London Sacred Harmonic 
 gave concert United Methodist Free Ch 
 Lecture Hall , Charlotte Street , Islington , ‘ Tuesday 
 evening , 18th ult . programme contained selections 
 Mozart Twelfth Mass , Haydn Creation . 
 soloists Misses Pedder Bent , Mrs. Broad , Mr. 
 Marriott , Mr. H. G. Froome , acquitted 
 creditably . choruses W ' 
 rendered . Mr. Bent leader orchestra , Mr. 
 James Boyce conducted , numerous audience 

 fourth “ Monthly Popular Con- 
 certs , ” held Angell Town Institution , Brixton , 
 pieces interest performed , 
 mentioned Beethoven Quartett D , 
 violins , viola , violoncello , Duet piano- 
 fortes , Mr. Cipriani Potter — excellently rendered 
 Mr. Walter Macfarren Mr. Ridley Prentice — ani 

 recognition sterling merits country 

 Beethoven Sonata G ( Op . 30 , . 3 ) played 

 MUSICAL TIMES.—Fesrrvary 1 , 1870 

 Louis Dupuis 

 Turse pieces , like Felix Gantier , reviewed 
 time ago “ Musical Times , ” especially 
 intended young players , , think , found 
 equally acceptable teachers pupils . . 1 
 founded Mozart twelfth Mass ; . 2 , Weber 
 Mass G ; . 3 , Haydn “ Imperial ” Mass ; 
 . 4 , Beethoven Mass C ; . 5 , Gounod 
 Messe Solennelle ; . 6 , Rossini ‘ ‘ Stabat Mater . ” 
 written form little Fantasias , com- 
 mencing short introduction . doubt 
 subjects ‘ ‘ Stabat Mater , ” Weber Mass 
 acceptable juvenile pianists ; themes 
 extremely pleasing , music “ parents 
 guardians ” nod heads . 

 , preferred headings 

 MUSICAL TIMES.—Fesrvary 1 , 1870 . 375 

 pianofortes produces certain effects 
 fnstrument . said , size pianoforte 
 drawback employment concerted music . 
 , large houses , expense frequent 
 tuning great object , pianofortes kept 
 itch found immense economy time teach- 
 ing music , means rendering musical study 
 far interesting instru- 
 ment . girls family , 
 violin , learning overtures 
 symphonies arranged pianoforte quartetts , excellent 
 study . 1 know ladies living way 
 country , seldom hear music 
 concerts London , weeks spend 
 season ; play like artists , contrive 
 + o acquaintance Trios Mozart 
 Beethoven , aid pianofortes , violin 
 violoncello parts played second instrument . 
 brother fond music sit 
 listening ’ evening . better 
 lady learnt violin 
 brother violoncello , pleasure 
 rendering music original language . 
 thongh good translation better , 
 translation . time , 
 composition Mendelssohn grand Quatuor piano- 
 forte , violin , viola , bass , arranged duett 
 janofortes , interesting music play , 
 good pianists , enjoyable hear . 1 
 bring rambling letter end ; entreat- 
 ing wno cultivate music remember “ Union 

 atren gth . ” 
 Iam , obedient ne 

 Notice sent Subscribers payment ( advance ) ex- 
 hausted . paper discontinued Subscription 
 renewed . remind disappointed 
 obtaining numbers , music pages 
 stereotyped , sufficient quantity rest 
 paper printed supply current sale 

 BEETHOVEN.—Apply Mr. J. Gill , Secretary Royal Academy 
 Music , Tenterden Street , Hanover Square , forward 
 prospectus containing desired information 

 A. R. Swarne.—The consecutive fifths mentioned certainly , 
 undoubtedly Handel wrote ; scarcely under- 
 stand correspondent means asking 
 “ accounted 

 Mawstone.—The annual concert given Mr. H. F 

 enniker , R.A.M. , took place 4th ult . principal vocal- 
 sts Madile . Mathilde Enequist Mr. R. West ; violinist , 
 Mr. J. Carrodus ; pianist , Mr. Henniker . 
 efficient orchestra , selected Royal Engineer Marine 
 bands , chorus , numbering voices . Sonata 
 Beethoven ( dedicated Kreutzer ) excellently played Messrs. 
 Carrodus Henniker ; encored Fantasia 
 Scotch airs . orchestra effectively rendered Symphony C 
 major , Mozart , overture opera Admiral 
 Daughter , H. F. Henniker . choruses carefully sung , 
 amply proved great pains bestowed 
 conductor ; “ Ye Mariners England , ” Pierson ; “ Mark 
 Heavens . ” Verdi ; “ List head , ” Henniker ; 
 “ Kermesse ” scene , Faust , especially rendered . 
 concert gave great satisfaction crowded 
 audience ; classical selections better appreciated 
 generally expected country town 

 Mancuesrer.—At Mr. Charles Hallé Concert , 
 6th ult , Beethoven Pastoral Symphony , overtures La Cle- 
 menza di Tito ( Mozart ) , Manfred ( Schumann ) , Mendelssohn 
 Grand March ( pusthumous ) , principal orchestral works , 
 Madame Norman - Néruda created genuine effect Larghetto 
 Finale Vieuxtemps Concerto , Beethoven Romanza 
 G ; Mr. Charles Hallé performed usual excellent style 
 Arabesque , Schumann , waltz Schubert Liszt . 
 vocalist Madame Sinico , warmly applauded 
 operatic airs , Gounod Serenade , violoncello 
 obbligato played M. Vieuxtemps 

 Parrick Brompron , Yorksuire.—Mr , Lax , organist 
 choirmaster Patrick Brompton , gave annual concert 
 school - room , 18th ult . , attended 
 principal families neighbourhood . glees part- 
 songs given choir highly creditable manner . 
 solos , sung amateur ladies gentlemen , gave 
 great satisfaction 

 MENDELSSOHN Lieder ohne , Worte , § Books 

 complete ooo ove ee ove owe £ 4 0 
 * BEETHOVEN Thirty - Bight Sonatas eve ow 5 
 * BHETHOVEN Thirty - Miscellaneous Pieces 20 
 * SCHUBERT Sonatas eco ove 4 @ 
 SCHUBERT Dances , complete tit s h ¢ 
 * SCHUBERT Pieces ... ove ove ~ 20 
 * MOZART Eighteen Sonatas oce ooo ae 3S 6 
 * WEBER Complete Pianoforte Works eco oo & O 
 * SCHUMANN Album , containing 43 Pieces ... ~~ £ 4 
 SCHUMANN ' Forest Scenes . Easy Pieces « = . 3 0 
 Volumes marked * handsomely bonnd cloth 

 gilt edges , 2s . extra . 
 London : Novello , Ewer Co 

 33 Juvenile Vocal Album 

 32 Beethoven Sonatas . Edited Charles Hallé ( . 6 ) . 
 31 Beethoven Sonatas . Edited Charles Hallé ( . 5 . ) 
 30 Beethoven Sonatas . Edited Charles Hallé ( . 4 . ) 
 29 Contralto Songs , Mrs. R. Arkwright , & c 

 28 Beethoven Sonatas . Edited Charles Hallé ( . 3 . ) 
 27 Sets Quadrilles Duets , Charles d’Albert , & c. 
 26 Thirty Galops , Mazurkas , & c. , d’Albert , & c 

 25 Sims Reeves ’ Popular Songs 

 22 - Christy Minstrel Songs . ( Selection . ) 
 21 Pianoforte Pieces , Ascher Goria 

 20 Beethoven Sonatas , Edited Charles Hallé ( . 2 . ) 
 19 Favourite Airs Messiah , Pianoforte 

 18 Songs Verdi Flotow , English words . 
 17 Pianoforte Pieces , Osborne Lindahl 

 16 Sacred Duets , Soprano Contralto Voices . 
 15 Eighteen Moore Irish Melodies 

 14 Songs , Schubert . English German Words . 
 13 Popular Duets Soprano Contralto Voices . 
 12 Beethoven Sonatas . Edited Charles Hallé , . 1 

 11 Pianoforte Pieces , Wallace 

