PERFORMED 

 
 WAGNE ! * LOHENGRIN ” ’ > 6 
 ‘ 5 “ TANNHAUSER ” ’ $ s ae oO 
 zs + FeyvInG ‘ DUTCHMAN ” 3 6 
 ‘ TRISTAN UND ISOLDE ” ie Os 
 BEETHOVEN “ FIDELIO ” ’ .. oe . 6 
 WEBER * * EURYANTHE ” .. ai xe Ff 6 

 London : NoveLLo , Ewrr Co 

 crushing blow removal head . 
 desolate outlook life appear Felix 
 read published letters know 

 Fanny Hensel attended Festival “ St , 
 Paul ” produced 1836 , letters . 
 anent interest . read 
 Beethoven * * Leonora ” ’ overtures 

 Ah ! Rebecca ! heard overture 
 ‘ Leonora , ’ rare piece ! actually 
 played , Beethoven disliked aside . 
 man taste ! know things pretty , 
 charming , interest . Haslinger 
 printed complete edition works in- 
 cluding ; present 
 success 

 Concerning Ninth Svmphony , knew 
 score , Fanny remarks 

 313 

 words express appreciation influence 
 happiness society . 
 great difference Bousquet , 
 calmer , decided preference 
 French classical school ; Gounod , romantic 
 degree passion , upset 
 introduction German music . startled 
 like bombshell , wonder 
 damage . ” place read : “ ‘ Magnus 
 came , [ I’renchmen , , 
 , caprices — Bousquet surnamed 
 Caprice en la , Gounod Caprice cn mi , 
 Dugasseau Caprice en st bemolle . . . . Bousquet 
 { showed cantata work , 
 { contain beautiful . 
 | acquaintance German music unmixed 
 | benefit , believe , far Gounod 
 bewildered upset . strikes 
 |less matured comrade , know 
 music , scherzo played 
 day , asked accept , bad 
 taken account . thought discerned 
 traces German influence init . ” 
 read : ‘ evening Frenchmen dropped . 
 ‘ Wilhelm began portraits , great deal ot 
 fun going , course , meantime . [ 
 | sitter allowed order play 
 chose , manner went nearly 
 ‘ Tidelio , ’ things , ended 
 Beethoven Sonatain C major . Gounod behaved 
 } intoxicated , talked lot 
 nonsense ; came exclaiming , 
 immense enthusiasm , ‘ Beethoven est un polisson | 
 pronounced high time 
 bed , carried . ” — 
 curious , throwing light religious tenden- 
 cies Gounod nature : ‘ * talked 
 Gounod , Bousquet lavished alternate 
 abuse pity having given ! 
 participation delightful days . 
 | told far Gounod allowed 
 drawn engagements religious nature , 
 result feared weak 
 | character . ... Duringthewinter hei Pcre Lacordaire ) 
 tried win Bousquet Gounod , 
 , excitable easily influenced , 
 | thoroughly embraced views Bousquet afraid 
 music cowl . .. . 
 Society St. John Evangelist Paris consists 
 | entirely young artists . bound 
 | motive employing Christian art 
 } convert worldly minded , Gounod said 
 | joined . ” 
 | extract charming letters , 
 /and pass . concerns Irish family 
 named Palisser , described Madame Hensel : 
 | ‘ * consist gigantic daughters — 
 | handsome English faces long , slender flower - stalks , 
 iwho ride horses paint landscapes , talk 
 | German , French , Italian fluently , sing badly 
 | — tall son , number children , pleasant mother 
 |and good - looking father . ” ami- 
 | able friendly astonish Madame Hensel till 
 ishe learned English . .1 fropos . 
 | English bit Madame Hensel mind : — 
 | “ regard English general , 
 | impossible find ruder churlish person 
 { Englishman ac- 
 quainted . vexed day . 
 | live compact mass 
 form nationality , 
 lsomehow place , consequence 
 
 English 

 country . avoid playing 

 } 

 fondly public apt fasten 
 characteristic , disproportionately foster jts 
 crotchet based 
 sound harmony , M. Chopin hardly 5 
 intimately exquisitely graceful , 
 occasion grandiose . . .. 
 attraction M. Chopin muatin¢é : sing . 
 ing Madame Viardot - Garcia , , ip . 
 ienita ’ ly Spanish airs Mdlle . de Mendi , 
 queerly piquant ‘ Mazurkas , ’ gave ‘ Ceneren . 
 tola ’ rondo , graced great brilliancy , song 
 Beethoven , ‘ Ich denke dein 

 strange Mr. Ella shouid absent 
 concert given house great friend 
 Lord Falmouth , case ; 

 MUSICAL TIMES.—June 1 , 1882 

 majority advocates sides fully 
 competent task lies . 
 question time music 
 treated , recognised critics , agree- 
 able relaxation fatigues day , 
 compositions Beethoven Symphonies , ex- 
 ample , scarcely understood sufficiently 
 receive justice hands ; , 
 , art estimated higher 
 standard general public led regard 
 intellectual study pleasant pas- 
 time . Let admit judges 
 formed works judged ; looking 
 think leniently taught according 
 knowledge day , looking forward 
 trust earnestness purpose desire 
 uphold true intent music continue 
 rule writings responsible office 
 criticism confided 

 , , intellectual 
 thought day directed music ; 
 , recollection , opinion 
 sides courted present moment . 
 musical activity year 1882 remark- 
 able doubtless form epoch 
 history art . London , Italian 
 Opera asserts right heard , German 
 music , German executants , surrounds 
 sides ; enthusiast wishes 
 attractions held , 
 perplexed number operas concerts 
 best specimens modern German art 
 heard . memorable perfor- 
 mance Bayreuth , Wagner ‘ ‘ Nibelung Ring ” 
 constantly talked country , 
 representative work repre- 
 sentative composer sung 

 singers language country ; , better| received emissaries foreign land 

 , language music origin- 
 ally written . ‘ * Richter Concerts , ” conducted 
 artist proved right 
 position occupies highly intellectual reader 
 greatest works musical art , remain 
 attractions season ; “ Symphony 
 Concerts , ” able conductorship Mr. 
 Charles Hallé , fine orchestra * * Beethoven 
 Choir 350 performers , ” appeal irresistible 
 force lovers classical music ; 
 excellently organised enterprises patron- 
 ised demonstrate possibility doubt 
 public ready willing listen 
 good music , performed , placed reach . 
 addition Orchestral Concerts , 
 , time , given Mr. Walter 
 Macfarren , numerous Recitals chamber music , 
 mentioned series Herr 
 Franke Marlborough Rooms ; 
 announced competition French 
 Orphéons ( similar given Brighton 
 year ) place present month 
 Royal Albert Hall 

 Extraordinary , , season 1882 
 prove London — remembered 
 renewed exertions characterise performances 
 English Societies — Bayreuth find 
 preparations proceeding vigour pro- 
 duction Wagner latest opera - drama , “ Parsifal , ’’ 
 given July , best solo 
 singers procured , Munich Court 
 Orchestra , secured special 

 spicuously absent 

 task extinguishing old ones . article 
 Symphony , instance , published 
 journal professing represent American opinion , 
 naming modern Symphonic com- 
 posers , read : ‘ * Tschaikowsky , Russian , 
 new school writers , school 
 justly supplanting time - worn obsolete , dry 
 unmusical methods writing 
 essentially German , unmistakably solid , 
 divine aftlatus painfully con- 
 author criticism 
 admits Beethoven Seventh Symphony ( 

 ) “ ‘ rank , ” imagine 
 | consider old - world composer entirely 

 XUM 

 
 movement , somewhat diffuse , contains 
 clever writing ; ‘ * ‘ Romance ’ movement 
 decidedly best . “ R nce ’ 
 y given 
 orchestra pianoforte surrounds exceed- 
 ingly delicate graceful embellishments . Rondo 
 Finale extremely brilliant , passages pianoforte 
 taxing powers executant utmost . 
 composer , , pupil Liszt , proved 
 thoroughly equal occasion , rewarded 
 singing Madame 
 Christine Nilsson Mozart ‘ “ * Mi tradi ” Schubert 
 Serenade highest order ; programme 
 included Beethoven ‘ Pastoral Symphony , ” 
 Wagner Overture ‘ Tannhduser , ” ’ short 
 pianoforte solos Signor Sgambati . concert 
 ably conducted Mr. Cusins 

 del 

 326 MUSICAL TIMES.—June 1 , 1882 

 heard genuine interest . Mr. D’Albert played | opposition fail injure parties . 
 immense spirit facility . learn,| result thin attendances , 
 said executive artist poet,|the Symphony Concerts , rival enter . 
 ‘ ‘ nascitur , non fit . ” , , plenty time | prise . 
 , uses , daysto } Mr. Hallé Manchester Orchestra , Mr. Hallé 
 come , shine pianist composer . The| Conductor , Messrs. Schultz - Curtius 
 audience Concert fairly large , appre- | difficulty arranging series interesting perform . 
 ciative good thing . ances , intended , programmes , 
 second Concert ( Sth ult . ) introduced Tschaikowsky | display solid merits classical school 
 Violin Concerto D. Russian work fitting | newer cult exhibited . managers , . 
 Russian artist , that|ever , set faces entirely opposition 
 nationality appeared person Gospodin Adolf } contemporary composers ‘ “ ‘ advanced ’’ type . 
 Brodsky . refer Concerto terms un- | Concert , instance , produced Rubinstein 
 qualified admiration . long , pretentious , entitled | Pianoforte Concerto G major ( . 3 ) , Madame 
 boast certain original features , genius corres- | Sophie Menter — highly developed pianist — 
 pond precisely adequate measure . de-| play . work new English audiences , 
 ficiency unusual day , composers an|and likelihood sooner 
 idea possible excessive elaboration , use| later accepted good . point amateurs 
 called mechanism art , } slow arrive decision , 
 deficiency subtle spirit manu-|only hastened frequently placing music 
 factured , evades analysis . Gospodin Brodsky an| . Rubinstein Concerto assuredly gains 
 artist mastered technicalities profession . | repeated hearing , obscure 
 plays , , far execution goes , comes | clear , fragmentary assum- 
 amiss tohim . cantabile bravura equally at|ing definite organisation shape . Madame Menter 
 home , thing desirable wanting a|helped process able performance , 
 fuller better quality tone . come | composer applauded . 
 finer instrument ; Russian artist distinctly | Unhappily people hear 
 worthy applause received Richter|admire . Concerto given Cherubini 
 audience , programme Concert , calling | ‘ ‘ Anacreon ” ’ Overture , Brahms Variations Theme 
 mention , Beethoven Sympheny | Haydn , — far ! — Choral 
 , Handel Suite D , Wagner ‘ ‘ Faust ” Overture , | Symphony , Madame Anna Williams , Miss Orridge , 
 solo “ Meistersinger , ” finely delivered Herr | Mr. Vernon Rigby , Mr. Santley soloists , 
 Betz , Berlin Opera . great work tried mettle orchestra chorus 
 Concert ( 18thult . ) Dvordk Symphony D , | severely , ordeal passed success , per- 
 long produced Crystal Palace , occupied | formance deserving rank best heard 
 post honour . criticism appeared in| London . fact served inaugurate new 
 number need added ; | Concerts needful distinction . 
 remains Herr Richter gave somewhat scanty | second performance ( 12th ult . ) interest- 
 audience fine reading work fail to| ing Schubert great Symphony C ( . g ) , 
 interest musician . scena ‘ Softly sighs , ” | Beethoven Music Goethe ‘ ‘ Egmont , ” given con- 
 ‘ * * Der Freischiitz , ” ’ prelude fugue Bach , } nection recitation ‘ ‘ argument ” 
 violin , followed Dvorak ; sung Madame | portions dialogue . < better rendering Sym- 
 Marie Roze , played Herr Rappoldi . | phony tomind . marked extreme 
 gentleman impassioned artist . executes lrefinement , , needful , great power ; 
 mus.c stolidity , correctness , | obviously left hazard whim 
 machine . fugue stolidity matter , | individual performers . single mind taste domi- 
 correctness , astonishing correct- | nated execution work , credit 
 ness found . hard find |result leader followers entitled share . 
 violinist able render work technically faultless | ‘ ‘ Egmont ” Overture usual impression , 
 amanner . Concert ended Brahms ‘ German | interludes entr’actes soon wearisome , 
 Requiem , ” performance use | fault , recita- 
 congratulatory terms . sooth , chorus bad , | tion dialogue Mr. Clifford Harrison reflected 
 orchestra means free blemish . The| dulness . task reciter 
 result great disappointment , looked | occasions difficult , majority 
 forward hearing work . cases dead weight audience 
 programme fourth Concert ( 22nd ult . ) | indifference . Care taken employ 
 new “ Venusberg ” music ‘ ‘ Tannhduser , ” | man equal task . selections programme 
 “ * Siegfried ” ? Idyl , Beethoven Symphony B flat , | Overture ‘ ‘ Euryanthe , ” great scena 
 Pianoforte Concerto E flat , vocal | ‘ ‘ Oberon”’—finely sung Frau Sachse - Hofmeister , 
 ic intrusted Frau Sucher , German company , | ‘ ‘ Nibelungen ” company — Svendsen fifth Nor- 
 ury Lane . familiar things need | wegian Rhapsody , original work Concert 
 dwell . Herr Oscar Beringer played great Concerto | ended . 
 brilliantly , special interpretive merit , programme ofthe Concert , 18th ult . , included 
 purely orchestral pieces given admirable | selections Cherubini ‘ Medea ’ Overture , 
 ensemble Herr Richter seldom - failing baton . Dvorak ‘ Slavische Rhapsodie ” D , Pastoral 
 Symphony Beethoven , Berlioz ’ Overture ‘ ple 
 : ee ele Romain . ” undoubtedly feast orchestral goo 
 SYMPHONY CONCERTS . things , worthy guest . items , , 
 series Concerts , affecting | known require comment , pass 
 continuation Richter Concerts , commenced St. } word praise careful efficient 
 James Hall onthe rst ult . actual season | rendering . occasion Herr Vogl sang songs 
 appears nominally fourth arises circum- | Brahms Handel , — 
 stance joint managers Richter Concerts , | surmised ‘ “ ‘ Nibelung Ring ’ — vocal 
 given year previously , having agreed | artist high culture . adding present series 
 company , claimed credit legitimate successors | Concerts given benefit Royal College 
 old business . matter Herr Franke appears | Music , point fourth programme , 
 best . Herr Richter went , | performed Sth inst . , contain 
 taking , course , ; Messrs. Schultz- | Schumann ‘ “ Faust . ” Amateurs ought lose 
 Curtius reduced somewhat barren expedient | opportunity , promised England , hearing 
 indicated . matter , , public | work entirety . 
 interest , deserves mention explaining active 

 yme 
 ym- 
 eme 
 hile 
 1 
 ymi 

 MR . GANZ CONCERTS 

 Tur second Concert present series took place 
 St. James Hall 6th ult . , attended bya large 
 audience , set programme mainly 
 composed classical works . suffice 
 : Mendelssohn Overture “ Ruy Blas , ” Beet- 
 hoven Pianoforte Concerto C minor ( . 4 ) , Schubert 
 Symphony C ( . 9 ) , Weber Overture ‘ * Eury- 
 anthe . ” Mr. Ganz fond introducing novelty , 
 instance gave old favourites , , 
 usual , repaid selection making Concert 
 enjoyable taste . pianist Herr Ernst 
 Loewenberg , meritorious artist , established 
 right heard exponent Beethoven playing 
 equal intelligence technical skill . Better per- 
 formances purely orchestral works 
 occurred audience , sundry slipshod passages 
 attracted notice ; , things considered , 
 room fault , especially assume 
 energies conductor orchestra centred 
 Liszt Symphony Dante ‘ * Divina Commedia , ” 
 , played previous Concert , announced 
 repetition . proceedings relieved 
 short pianoforte solos , singing Miss 
 Agnes B. Huntingdon 

 idea ‘ ‘ Divina Commedia ” played 
 carried speedily ventured 
 anticipate , appear acquaintance 
 materially affected general verdict merits . 
 ‘ Purgatory ” “ Paradise ” gain acquaint- 
 ance , majority ‘ ‘ Inferno ’ re- 
 volting . Surely , , music carried 
 province , reduced rank burlesque . | 
 Concert Russian pianist , M. Vladimar de Pachmann , 
 début Chopin Concerto F minor , playing 

 relation incident visit King 
 Danish camp disguise harper , - known 
 events followed — introduction betrothed , 
 Alswitha , forming agreeable rclief necessarily 
 warlike tone colours greater work . 
 music Cantata Mr. Prout decided 
 advance “ Hereward , ” choruses 
 evidently threw great strength . ‘ Alfred , ” con- 
 trary , containing effective - planned 
 choruses , distinguished solo music remarkable 
 merit , mentioned songs 
 rivals Danish camp ( extremely 
 melodious , excellently contrasted ) , air Alszitha , 
 ‘ * dream , ” scena Alfred , “ Forest 
 Selwood , ” unexaggerated dramatic power . 
 choruses means elaborate , 
 composer evidently resolved sym- 
 pathetic expression words skill 
 defiance . chorus Saxons , ‘ * Weary 
 war - wasted , ” deserves especial mention truthful 
 musical setting ; , space permit , 
 cite received warm tho- 
 roughly deserved recognition audience . 
 impossible speak highly charming instrumen- 
 tation work — observable delicate 
 light shade solo parts , rich 

 w 
 colour adds intensity choral portions , 
 aid stage accessories 
 realisation effect . Triumphal March , 
 ( purely orchestral movement work ) , 
 claims notice , intrinsic excellence 
 skilful manner instruments treated . 
 composer right congratulate having 
 secured services Miss Annie Marriott , Mr. Shake- 
 speare , Mr. F. King , exerted 
 utmost ensure success Cantata . 
 chorus - singing respect perfect 
 desired , Mr. Prout , laboured 
 hard gain accuracy . conclusion per- 
 formance composer called forward received 
 ovation . remainder programme com- 
 prised Mozart Symphony G minor , Beethoven Scena , 
 “ Ah perfido , ” Benediction Daggers , 
 ‘ * * Les Huguenots 

 GREGORIAN ASSOCIATION FESTIVAL 

 Sebastian Bach “ Passion according St. Matthew ” 
 performed oth ult . , time Birming . 
 ham , members local Philharmonic Union , 
 Dr. Swinnerton Heap , Miss Clara Samuell , Madame 
 Poole , Mr. Welch , Mr. Harrison principal 
 vocalists , local band performers . 
 production excited great interest musical circles , 
 reception general public cordial 
 desired . performance , faultless , 
 generally effective praiseworthy , dramatic 
 spirit choruses admirably realised 

 Madame Sophie Menter played time 
 Birmingham recital 18th ult . , created 
 furore extraordinary performance , 
 especially Liszt difficult ‘ Don Juan ” Fantasia 
 couple pieces Rubinstein : Romance E 
 flat , . 1 ( Op , 44 ' , * * Valse Caprice ” ’ key , 
 noteworthy performances 
 accomplished German pianiste named 
 Beethoven Sonata Appassionata F minor , Op . 57 

 Bach Toccata Fugue D minor | . g Novello 

 MUSIC BRISTOL . 
 ( CORRESPONDENT 

 Monday rst ult . Mrs. Viner - Pomeroy fourth 
 Classical Chamber Concert ( fifth season ) took place 
 Victoria Rooms , executants Messrs. Henry 
 Holmes ( violin ) , M. Rice ( second violin ) , G. Burnett 
 ( viola ) , T. Pomeroy ( violoncello ) , Mrs. Viner- Pomeroy 
 ( pianoforte ) . programme comprised Mendelssohn 
 Trio D minor piano , violin , violoncello , Spohr 
 Quartet E flat , Schumann Noveletten ( Op . 21 , . 1 ) 
 Phantasiestiicke ( Op . 12 , . 7 ) , Beethoven 
 Quartet D ( Op . 18 , . 3 

 People Concert Saturday 6th ult . Mac- 
 farren Cantata ‘ ‘ - Day ” chief attraction 

 programme Monday Popular Concert 
 8th ult . underwent slight alteration , consequence 
 melancholy news received Ireland previous 
 day . Gounod Royal Wedding March , 
 item , changed moment 
 Dead March ‘ “ ‘ Saul . ” ’ performance hall 
 remained semi - darkness , audience standing , 
 conclusion gas turned orchestra 
 broke ‘ God save Queen , ” sudden change prov- 
 ing impressive . followed Beethoven 
 Overture * * Leonora ” ( . 3 , C ) ; Mendelssohn Sym- 
 phony ‘ ‘ Hymn Praise ” Overture ‘ * Ruy 
 Blas , ” Overture “ * Tannhauser , ” A. Dvorak Suite 
 D , Marche Héroique C. Saint - Saéns consti- 
 tuting remainder programme . Madame . Winn 

 pro- 
 lense 
 epa- 
 
 ather 
 S 
 ns 
 ance 

 ee es 
 Miss Mabel Waite vocalists . performance 
 Mendelssohn Symphony Wagner Overture 
 particularly good . Dvorak Suite appeared excite con- 
 siderable interest audience , close 
 Romance applause . rule 
 encores prevails Concerts , 
 Suite undoubtedly redemanded . Mr. 
 Riseley conducted 

 Monday Popular Concert 22nd ult . 
 ( tenth Concert fifth season ) Mr. Riseley band 
 performed following selection : Overture ‘ ‘ Jessonda ” ’ 
 ( Spohr ) , Overture “ Die Felsenmihle ” ( Reissiger ) , Scotch 
 Symphony , Op . 56 ( Mendelssohn ) , Overture 
 “ Merry Wives Windsor ” ( Nicolai ) . chief 
 feature evening Miss Helen Hopekirk per- 
 formance Beethoven ‘ ‘ Emperor ’’ Concerto , piano- 
 forte orchestra . Miss Hopekirk displayed 
 marked talent ability , left - hand passages especially 
 rendered masculine vigour power . 
 gave Chopin Berceuse D flat Polonaise 
 flat , pianoforte solo , playing , 
 Concerto , memory . movement 
 Mendelssohn Symphony somewhat loud 
 pianissimo passages , unusual fault Mr. Riseley 
 band , remaining movements went . Mr. W. 
 Thomas Miss C. Wollaston vocalists 

 

 Tue Gentlemen Concert series given 
 Concert Hall April 24 . chief orchestral work , 
 Mozart ‘ ‘ Haffner ” Serenade , produced Mr. Hallé 
 Concerts year ago , replete melody , 
 like Mozart wrote , ranked great 

 themes , somewhat thin scoring small 
 orchestra , combine render work slightly monotonous , 
 despite melodic beauty . orchestral numbers 
 Schubert Overture ‘ ‘ Rosamunde , ” Brahms 
 “ Hungarian Dance ” . 6 . 
 characteristic rendering Beethoven 
 Concerto C minor , played infinite grace 
 delicacy pieces Schumann “ Phantasiestiicke . ” 
 Mrs. Hutchinson sang songs usually 

 played good contralto voice , commendable method , 
 excellent taste pleasing ambitious selection . 
 Madame Sophie Menter gave Pianoforte Recital 
 8th ult . programme comprised seven 
 ‘ original pianoforte compositions , viz . , Beethoven ‘ ‘ Sonata 
 Appassionata , ’’ Preludes Chopin , pieces 
 Scarlatti , equal number Rubinstein ; rest 

 Liszt ‘ ‘ Don Juan ” Fantasia , Tausig transcription 

 Arabesque ( Op . 18 ) . singing choir highly 
 commendable , soloists , Miss Tomlinson , Miss 
 Riley , Mr. Fred . Foster , achieved considerable 
 success 

 Leeds Amateur Orchestral Society attempted great 
 things Church Institute , 8th ult . pieces 
 performed band Suppé Overture , ‘ * Poet 
 Peasant , ” Beethoven Pastoral Symphony , Auber 
 Overture ‘ Fra Diavolo . ” M. J. P. Bowling 
 Conductor , Mr. W. S. Hall solo flautist , Miss Annie 
 Woods vocalist 

 Dr. Stainer Cantata ‘ * Daughter Jairus ” formed 
 programme Concert given 
 Ilkley Vocal Society gth ult . ; second con- 
 sisting - songs solos . Mr. Jas . Broughton , 
 Chorus - master Leeds Musical Festival Choir , 
 Conductor Society 

 edited , translated Natalia Macfarren . | Frederick H. Bell . [ B. Williams 

 Novello , Ewer Co. bree ; ; 
 ; . Se : . | Composers , like authors , sutier good works 
 Pp . pe drew Beng te — ee * having produced time ; 
 : — | ta h OBES 108 h s ¢ ey Y | remarked despairing writers Shakespeare 
 schubert ; number tor Con- | Beethoven left anybody 
 tralto , acquainted vocal ! ; hem . Mr. Bell evidently fs victims , 
 works composer know | written good Serenade M. 
 beautiful wrote . volume commences | Goynod written “ Sérénade Berceuse ” . 
 r ” WT ee s 
 bi — , W anderer , po — ae pnts reproduced notes popular 
 ese eg = \e ae — _ * -~ ae hi 2 rate French composer , figure pianoforte 
 C hich m oO rg wh th cota | colours composition actu- 
 , hinted notice volume , | a}1y believe commencing Gounod Sérénade . 
 fully revealed listener union | [ ; Sto ig pete . cfect resem : 
 equally sympathetic artists , pianoforte pjance — probably accidental — lessened 
 pdb sd Lp pines : agg — — wn € SEry | change time key middle song . 
 . F “ 4 2 5 OC gS . 
 a4 ° mm : : + , ’ . ” | + la bd . Tames , Tour 
 Flight Time , ” ‘ “ Weary Heart , ” ‘ Sadness , ” “ * } Song Norns . English Version , Lewis Novra . 
 Lyre , ” ‘ ‘ Lay Imprisoned Huntsman , ” | Composed Female Voices , Solo Chorus , Or- 
 “ Death Maiden , ” ‘ ‘ Prometheus , ” | chestra , H. Hofmann . [ Novello , Ewer Co.| 
 - — hse ys book , pana = | Tus characteristic composition hitherto ap- 
 ae 14 Ne Scene struc Pose ; | peared folio size , need scarcely 
 _ — Pe cae oe ope hs ape - ative | publication Novello Octavo Edition largely spread 
 merits songs ; agree volume ts | 4 knowledge claims attention . written 
 priceless treasure contralto vocalists . |for female voices , appeals choral societies 
 7 . oe . wing - room vocalists , doubtless glad 
 Pianoforte Teacher Guide . L. Plaidy . Trans- bette drawing - room vous “ se aig | caus ae 
 lated Fanny Raymond Ritter . [ W. Reeves.| /to acquainted work beauty 
 sbiniinsinia . Pi . ads d | thoroughly executive powers . 
 Tue author book known | pianoforte accompaniment exceedingly arranged , 
 need word introduction . ‘ ‘ Technical Studies ” | numerous indications instrumental score 
 happily familiar pianists | found useful guides performer . 
 country Germany ; cordially wel 

 m 
 m 

 Baltimore.—Peabody Concert ( April 15 ): L’Arlésienne , orchestral 
 suite ( Bizet ) ; Pianoforte Concerto , E minor ( Chopin ) ; Songs ( Gounod ) ; 
 Overture , ‘ ‘ King Lear , ” Fragments ‘ ‘ La Damnation de Faust ” 
 ( Berlioz ) . Students ’ Concert Peabody Institute ( April 22 ): String 
 Quartet , C major , Op , 76 , Pianoforte Trio , D minor , . 19(Haydn ) ; 
 Air , “ Alcina ” ( Handel ) ; Variations , Op . 12(Chopin ) ; Spinning Song 
 Hungarian Storm March ( Liszt ) , Students ’ Concert Pea- 
 body Institute ( April 29 ): String Quartet , C major ( Svendsen ) ; 
 Rakoczy March ( Liszt ) ; Songs(Schumann ) ; Pianoforte Trio , C minor 
 ( Raff 

 New York.—Poughkeepsie Vocal Union , direction Dr. 
 F. L. Ritter ( April 19 ): Symphony , C major ( Beethoven ) ; Fourth 
 Psalm , baritone solo , chorus orchestra ( ’ . L. Ritter ) ; Chorus 
 ‘ * Joseph ” ( Méhul ) ; Scherzo Fourth Symphony ( Ritter ) ; 
 “ Ave verum ” ’ ( Mozart ) ; Air Chorus , ‘ ‘ heavens telling , ” 
 ‘ Creation ’ ( Haydn ) ; March , ‘ ‘ Tannhauser ” ( Wagner 

 uet 

 performers , thirty - amateurs . ery 

 formance consisted Mendelssohn St. Paul . choruses wet } ie ee 
 excellently given , showing marked advance powers ! Mic 
 members . solo vocalists Miss Anna Williams , Miss Wake- } “ 7 ° “ 4s 
 field , Mr. Harper Kearton , Mr , Hilton . Mr. D. J. Wood , Mus ) Lrw1 
 Bac . , Cathedral Organist , conducted . Inthe evening Haydn “ Spring ’ } 2tst ult . 
 ( Seas ) spiritedly executed , Beethoven Septuor E ! Anthem 
 flat marked feature programme . ‘ musical institution ) ¢ 
 West flourishing condition , musically ) ( Mr. B 

 financially . good , 

 PELL VOCAL LIBRARY 

 
 song , ” 
 OL “ Sweet 
 - twilight 
 > ART - SONGS , & c 
 2 ae y na ! 
 4 \ ~ 4 DEN ) C C ° Bells 0 ! 
 Composed Composed Mr. J. 
 arranged PRICI arranged price . Mr. Alt 
 z , G. A. Macfarren 1d . 45 . ‘ hou , ee wer ( Prayer “ Mose Egitto ” ) Rossini 2¢ Tickets 
 2 ” d. 40 . T Rhine . $ .a . T.B. .. G.A.Macfarren 1 usual 
 3 1d . 47 . German Fz land . S\A.T.Bi si 1d 
 te sis ‘ yo 2d . 438 . ree Lord Shepherd ( Quartet ) . s.a.t . B , G. A. Osborne 2d U 
 SAct . 8 . Yr . Rimbault 2d . » 49 . L4 Yeum F oii hie pa os ack Jackson 2d 
 6 . task ended ( Son ck ty ae « . Balfe 4d . 50 > Deum F ies ; Nares 2d Ta 
 7 . sp sum : b : : 51 1 ay , B55 Stine ove eee Reset 4d slg 
 8 . Soldiers ’ Chorus . T. ey o Osborne 4 place al 
 ) Th e Kermesse ( Scene " Faust ’ ) 53 . Sai Se Ww alter Hay 4 EM 
 10 . Quit thy bower . , 54 . Handiaidens ( “ Fridolin ” ’ ) A. Randegger ai } Chace 
 iI ns , a- ‘ 55 . ok fertory S Edmund Rogers 4a ] cd 
 2 . got - binders ’ Chorus 59 . Rec » ss C 2d . 
 13 ; Sviv ae “ sei 8 male voices 57 . Th : ey ving . Sir Hk . ( ishop 3¢ came , 
 4 . Gipsy Chorus ses ove 58 . : Rossini 2 Margar 
 15 . Ave M — : Bass ; 50 , E ann R 1 Edwarc¢ 
 16 . Hark ! t » herald J angels sing . $ s. d. 60 e af por 
 17 . Englan d ( Solo ¢ Ct orus ) . aa 1 . r ES > 4d totned 
 nS J : st L S Colle e 
 S S.A.T.B 1 , 62 , Resurrection C. Villiers Stanford 6¢ " 8 
 29 . 1 , ' 63 , Boys . New P atriotic Song H. J. Byron & W.M. , Lutz 4¢ 
 20 e 2d . '64 . Menof Wales ... « » Brinley Richards 24 R ° 
 es 2d . 65 . Dame Durden .. ae ant cas sie 
 2 io 2d . 65 , little farm tilled eae ia ob Hook 14 ! Hall , 
 ¥ 7 + 2d . | 67 Z ys asas ait le maiden ... anu m Macfarren 1 , Oratori 
 24 , ad . 68 , Fair Hebe Bac ane ei : 13 ) Sopran 
 5 Orp ! heus lute . s.s.s . 2d . | 6 g. rice loved maiden ‘ fair ets ms ae si IC Choir , 
 26 , Lullaby , S.a.a .... * io 1d . 70 , Jovial Man Kent wes ant pon ” monthi : 
 27 , , native la 1d . | 71 . Oak Ash .. , ia ove fa s invited 
 March Men Hari ch . ad . 72 , Heart oak ron ais ae ‘ 6 1 special 
 yi Queen . 1d . 73 . Come tothe sunset tree oes nee Prem seer Py hilpott 42 atthe 
 305 Britannia . $ .4.T.u , 1d.!74 . . s.a . T. W. F. Banks 2 quarter 
 ‘ Retr bat . T.1 3 ad . | 75 . Pp ure , love iyi int inocence ( * * “ TI Red di L ahore ’ ” ) Chorus ot female music , 
 2 , Lo ! soap ae bie 5S ad . voices ee H ae fi J. Massenet | general 
 3 . Weare spirits . s.s.s . dd P56 ) wARooveNayl ): SAA cease macs E.R. Terry 2 % ) Sexton 
 Market Chorus ( “ Masaniell 4d.}77 . Hailto woods . a. T.T.R. ... « J. Yarwood 247 cal Dit 
 j » Prayer ( * * Masaniello ” ) . 1d.|78 . Near town Taunton ... Dean T thomas J. Dudeney 2 } nights . 
 30 . Water Sprites . s.a.7.n . . 2d . 7 g. Merry boys sea . A.T.T.B. ss .. J. Yarwood 24 ) — — 
 37 ° Eve s glittering star . S.A.T.B. eae ame Pe 2d . . caer Iarewell . S.A.1.B. Berlioz 3 % 0 ] 
 38 . primrose . $ .A.T.B .... poh iss 2d . si . sun sets o'er mountains « “ ll Demoni 0 ” ) C 
 39 . O dewdrop bright . : ; j ie d . Rubinstein 32 : 
 40 . 1 5.K01 Rossini 4d . ) $ 2 . Hymn Nature . sé td ae ... Beethoven 34 , : 
 45 ; oNacient X J. Gill ) 2d . | 83 . Michaelmas Day ( Humorous - Songs , No.1 ) W. Maynard 44 | 
 42 . S.A.T.B , . ’ Brink y Richards 2d . } Sy . Sporting Notes ( Humorous - Songs , . 2 ) W. Maynard d , : 
 43 . s fan cy , ocean ‘ spray . S.A.T.B. OG . > Osborne 2d , | 85 . Austrian National Hymn _ ... ra Pes fa Haydn 40 . 95 , G 
 44 . Prayer Sea . s.a . T.5 . ine ” 2d . Nea 

 LONDON | te 
 CHAPPELL . & Co. , Bo , NEW BOND STREET , W. Tune 

