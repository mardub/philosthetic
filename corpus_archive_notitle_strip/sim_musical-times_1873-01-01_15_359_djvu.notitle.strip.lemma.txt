8 . RorrrrpDaM.—Fantasia , on subject from Haydn 's 8rd 
 ( imperial ) mass ... ove ese ove eee 

 4 , Mataca.—Fantasia , on subject from Beethoven 's Mass 

 5 Roven.—Fantasia , on subject from Gounod ’s Messe 
 Solennelle eco ove eco won eee 

 CRYSTAL PALACE 

 Mendelssohn ’s Oratorio , " St. Paul , " be give at the 
 Saturday afternoon concert on the 30th November , and 
 with an effect which lead we to hope that the miscellaneous 
 performance ( good as they invariably are)jmay a 
 be set aside for the presentation of some of the great sacre 
 work . the choir have evidently be well train for the 
 occasion , and especially in the chorale , show evident 
 sign of progress ; although much have yet to be do before 
 that steadiness and balance of tone absolutely demand in 
 the elaborate chorus of the standard Oratorios can be at- 
 taine . the solo vocalist be Madame Lemmens - Sher- 
 rington , Miss Julia Elton , Mr. J. H. Pearson ( for whom the 
 tenor music appear especially well suit ) and Mr. Lewis 
 Thomas . a feature of much interest be the performance 
 by Dr. Stainer of Mendelssohn ’s own organ accompaniment , 
 which although we believe long eo have never before 
 be play in this country . on the follow Saturday 
 two novelty be produce , one be an early Symphony 
 in E flat by Mozart , and the other an arrangement for the 
 pianoforte of Beethoven ’s violin Concerto , in D major , by 
 the composer himself . both these work be listen to 
 a curiosity ; for those who know Mozart ’s Syagoeres 
 write in the zenith of his career , could find but little in 
 this youthful essay to warrant more than a passing recog- 
 nition of its graceful melodiousness , and the violin Concerto 
 ( even adapt for the pianoforte by Beethoven ) could only 
 recall recollection of its excessive beauty in its original form . 
 Miss Agnes Zimmermann ( the pianist ) do all that could be 
 do for the composition ( an additional attraction in the 
 performance be the introduction of Beethoven ’s own 
 cadenza ) , and be receive with warm and well deserve 
 applause . the vocalist be Madame Sinico and Signor 

 722 

 british ORCHESTRAL SOCIETY 

 ir any doubt exist as to the possibility of form a 
 first - rate band exclusively compose of British - bear subject , 
 it must assuredly have be dispel at the first concert of 
 the above - name Society , which take place at St. James ’s 
 Hall on the 5th ult . the principal orchestral work — 
 Beethoven ’s Bas gg in C minor and the overture to 
 " Ruy Blas " and " Oberon”—most effectually test the 
 high power of the instrumentalist , and that they be 
 in every respect equal to the task be amply prove , not 
 only by the solid applause which greet the conclusion of 
 the two Overtures and every movement of the Symphony , 
 but by the frequent subdued murmur of approbation from 
 those to whom every note of these composition be familiar , 
 and who be constantly accustom to hear they interpret 
 by our fine orchestra . the " Ruy Blas " Overture be 
 play with a vigour and decision which fully reveal the 
 minute beauty of this noble orchestral piece ; and the 
 performance of Beethoven ’s Symphony be one of the 
 fine — if not the fine — ever hear in this country . that 
 a great portion of the effect produce be owe to the skill 
 and intelligence of the new conductor , Mr. George Mount , 
 we willingly concede ; but the brightness of the string and 
 the perfection of tone in the wind instrument , even in the 
 most exacting portion of this most exacting Symphony , 
 be the theme of universal admiration ; and the storm of 
 approbation which burst forth after the final chord be 
 indeed most fairly and legitimately earn . Sir Sterndale 
 Bennett ’s Pianoforte Concerto in F minor be render by 
 Madme . Arabella Goddard with a care and artistic conception 
 of the composer ’s meaning in every phrase , which reflect 
 the high credit not only upon her power of faithfully 
 interpret a work of genius , but upon her zeal in en- 
 deavouring , by the bestowal of her good energy on the 
 task , to prove to the world that the composition of an 
 Englishman be worthy to rank with the creation of those to 
 whom ( from the accident of their having be bear in a 
 foreign country ) the palm of superiority have be too often 
 rashly concede . Madme . Goddard be most warmly ap- 
 plaude at the conclusion of the Concerto , and recall to 
 the platform to receive the renew congratulation of the 
 audience . the vocalist be Madme . Lemmens - Sherrington 
 and Mr. Lewis Thomas , the former singe Handel ’s hack- 
 neyed " sweet bird " ( with Mr. Radcliffe ’s excellent flute 
 obbligato ) , and the latter Sir Julius Benedict ’s ' ' rage , thou 
 angry storm , " the two afterwards give with much effect 
 Spohr ’s melodious duet from " Faust , " " Dearest , let thy foot 

 p follow . " the second concert , on the 19th ult . , again 
 contain a most attractive selection of orchestral com- 
 position ; Mendelssohn ’s " Italian Symphony , " Beethoven ’s 
 " Leonora " Overture ( no . 3 ) , Mr. Sullivan ’s " Overtura di 
 Ballo " and Wagner ’s Prelude to " Lohengrin " be not 
 only interesting in themselves , but well select for contrast 
 in style . all go well , if we except a slight tendency in 
 the conductor to hurry the Symphony , which somewhat 
 mar the effect of the stringed instrument passage . 
 Mr. Edward Howell ’s performance of Goltermann ’s Violon- 
 cello Concerto in B minor be a triumphant display both of 
 his executive and expressive power ; and that his effort 

 i 

 Maviie . S. F. Hempron give an evening concert # pe rors 

 St. George ’s Hall on the 2nd ult . , before a thoro " = 
 appreciative audience . two movement from Beethoven Malivoir , 
 ' Allegro Appassionata , " a Polonaise of Chopin , Li solos b 

 Valse de Faust , " and two short solo ( one of her own com tong by 
 position ) , test most powerfully the young pianist ’s variél song o 
 capability ; but in every piece she be highly suce teflected 
 and the applause with which she be receive be Bicknell 
 legitimately earn . Miss Heilbron be assist by several t 

 british ORCHESTRAL society . 
 to the EDITOR of the MUSICAL TIMES 

 dear Sir,—The outcry of " Straniero " in your last 
 number against the above Society on account of its con- 
 fesse national character be base on the usual lame story 
 of all good music having be write by Germans and other 
 continental composer . that Beethoven , Mozart , & c. , write 
 music of the good kind , all the world know and acknow- 
 ledge . the foreign music of the present day be , however , 
 quite as remarkable for its inferiority to the english as the 
 latter be , perhaps , to the former when those two composer 
 live — that be , for the sake of argument , admit that this 
 be the case . but because we admit that they produce 
 good music there be noreason why that of Purcell , Battishill , 
 Arne , Sir J. Stevenson , Sir Henry Bishop , Onslow , Balfe , 
 and Wallace should be forget , and what be much more to 
 the purpose at the present time , why we should hear trashy 
 " Savoyard ’s Dreams " by Lumbye , " symphonic poem " by 
 Liszt , wearisome " serenade " by Brahms , and incomplete 
 scrap of Wagner ’s opera and other indifferent orchestral 
 composition by various german ( would - be ) composer so 
 pie often than the fine work of Bennett and Macfarren , 
 not to mention other , lest this should be consider an 
 attempt to advertise anyone . of one english composer we 
 do hear a great deal , and the more the well . from the 
 " prodigal Son " to " Cox and Box , " from the " Tempest " 
 to the Masque in the " ' Merchant of Venice , " his music be well 
 than that of any of the modern Germans . I have mention . 
 of course " Straniero " would put I down something , 
 which , judge by his letter , he very strongly resemble , 
 but my impression be not the result of stay in Englan 
 and hear what he would possibly consider as " ' nothing 

 but of pass three year in Germany , study music there 

 Beprorp.—The sixth season of the Bedford Amateur Musical Society 
 be conclude by a concert at the Assembly Rooms , on Tuesday , the 
 10th ult . , when Mendelssohn 's Elijah be perform . Mrs. Sydney 
 Smith , Miss Alice Barnett , Mr. Leslie Braham , and Mr. J. Lander , be 
 highly effective in the solo part ; ' ' hear ye Israel , " by Mrs. Sydney 
 Smith ; ' ' o rest in the Lord , " by Miss Alice Barnett , and the two tenor 
 song by Mr. Leslie Braham , be particularly deserving of mention . 
 Mr. J. Lander , too , be thoroughly efficient in the trying part of the 
 Prophet ; and the chorus , especially ' ' thank be to God " and " be 
 not afraid , " be render with a precision and power which well 
 merit the applause they receive . Mr. P. H. Diemer conduct , Mr. 
 R. Rose preside at the harmonium , and Mr. T. J. Ford at the piano- 
 forte 

 BrrMincnau.—At St. George ’s Parish Church , the organ . originally 
 build by Elliott , have be re - build and enlarge by Mr. Banfield , of 
 Birmingham . it contain 43 stop , with 8 coupler , and 8 composition 
 pedal ; of the stop , 7 be at present without pipe . about half the 
 stop , and the entire action , & c. , be new : the rest , with the case , 
 form part of the original instrument . the organ be open on 
 November 24th and 27th . sermon be preach by the Lord 
 Bishop of Worcester , by the Rev. Samuel Minton , M.A. , and the Rev. 
 Canon Wilkinson , D.D. the service be Dr. Clarke Whitfeld ’s " te 
 deum laudamus " and " Jubilate " in E , and G. A. Macfarren 's 
 " Cantate " and " Deus Misereatur " in G. the anthem be ' ' sing , 
 o daughter of Zion , " by the Rev. Sir F. A.G. Ouseley , Bart ; ' o praise 
 God in his holiness , " by Clarke Whitfeld ; and " ' hallelujah to the 
 Father , " by Beethoven . the service include a hymn write for the 
 opening of the organ by the Rector , " Triune God , thy praise to sing , " 
 set to music by Dr. Belcher , who preside at the organ , which be cer 

 z 
 corresp 

 BovurneMouTH.—Miss Ella Morgen ’s concert take place on the ty 
 November . the bénéficiare , whose executive power be of 
 order , play selection from Beethoven , Mendelssohn and J , 
 Miss Butterworth ( of the Royal Academy of Music ) , who pobsehaeg 
 beautiful contralto voice , sing " ' Ben e ridicolo " ( Randegger ) and " , 
 roam in the Forest " ( O'Leary ) , the performance of both Youg 
 ladies be warmly applaud 

 Bory St. Epmonps.—The first concert for the season , 
 St. Mary ’s Choir , Glee , and Madrigal Society , prove very g 
 the five - part glee , ' queen of the valley , " evidence the 
 training of the choir . the Society have the valuable assistance of 
 E. Spiller from London , and Miss Bacon from Ipswich . an in| 
 feature in the programme be a pianoforte duet from Euryanthe , 
 by the conductor , Mr. T. B. Richardson , and his little daughter 
 11 ) , which be enthusiastically re - demand . Mr. T. B. Ri 
 eonducte with his usual ability , and the accompaniment be 
 render by his pupil , Mr. G. Whitehead , the assistant organist of § 
 Mary 's Church 

 be especially successful in the air " rejoice greatly " and " I know 
 that my Redeemer liveth , " the latter of which be unanimously 
 encore ; and Messrs. Kennedy , Spencer , and Allen be also highly 
 efficient in the music allot to they . the chorus be render 
 with much precision , perhaps the most effective be ' ' for unto we " 
 and " ' lift up your head . " the performance be under the able con- 
 ductorship of Mr. W. B. Armstrong and Mr. W. Smallwood 

 LiveRPOOoL.—On the 8rd ult . , the Philharmonic Society give its 
 eleventh subscription concert for the year . principal artist : Madile . 
 Marie Marimon , and Signor Foli . the chief orchestral work be 
 Beethoven 's " Sinfonia Eroica , " which go splendidly . the over- 
 ture be those to Lurline ( W. V. Wallace ) , to William Tell ( Rossini ) , 
 of which the March be encore , and that to Za Fiancée ( Auber ) . 
 the Notturno , in a Midsummer Night ’s Dream ( Mendelssohn ) , give 
 great pleasure . two chorase by Weber , from Preciosa and Oberon , 
 be very well sing and play . and the applause throughout the 
 concert be most enthusiastic.——Tue 12th and last Subscription Con- 
 cert for the year , of the Philharmonic Society , be devote to a very 
 fine performance of Sir Julius Benedict ’s Oratorio , St. Peter . prin- 
 cipal artist : Madame Florence Lancia , Madame Patey , Mr. Sims 
 Reeves , Mr. Edward Lloyd , and Mr. Santley . the chorus go 
 excellently ; the quartet and three of the song be encore , and the 
 solo performance be what might be expect from the well - know 
 characteristic of the vocalist . Mr. Sims Reeves , though evidently 
 suffer much from the effect of his late severe illness , sing with 
 exquisite sweetness and expression 

 Lypnry.—The first concert for the season be give by the member 
 of the Harmonic Union on Friday , the 13th ult . the programme con- 
 siste of selection from Samson and Mozart 's Figaro . principal 
 vocalist : Miss J. Jones ( pupil of Mr. J. A. Matthews ) , Miss Sainsbury . 
 Mrs. Vivian , and Messrs. Morgan , G. Halford , and Gillman . the 
 chorus be well sing , and several solo be re - demand . there 
 be asmall band lead by Mr. Woodward ; Mr. J. Hooper preside at 
 the pianoforte , and Mr. J. A. Matthews conduct , and fill up the 
 wind part on the harmonium 

 ScarBporoveH.—The sympathy extend towards the captain of the 
 unfortunate vessel ' E.J.D. " ( which be recently strand on the beach 
 at Scarborough , and have since become a total wreck ) , have be almost 
 universal in the town ; and amongst the laudable endeavour to aid 
 he , a classical and musical entertainment be give on Saturday 
 evening the 30th November , at the Prince of Wales Assembly - room , 
 by . Dr. Sloman , organist of St. Martin ’s . the entertainment consist 

 reading from Shakespere ’s Julius Caesar , two Gentlemen of Verona , 
 and Hamlet , and of a few vocal and instrumental piece , very agreeably 
 and judiciously varied in character . the reading be deliver by 
 Dr. Sloman with excellent dramatic effect , and with much grace and 
 ease of manner . the instrumental portion of the music consist of a 
 Pianoforte solo , comprise illustration of Bach ( a fugue in D minor ) , 
 Mozart ( Sonata in a major ) , and Beethoven ( Andante from Sonata in G 
 major ) , all of which be admirably play and warmly applaud 

 SLovcH.—A concert be give at Slough , on the 18th ult . , by some 
 member of the Eton College Choir , assist by Mr. H. Nicholson ( solo 
 flute ) and Madame T. Wells , who be very successful in the music 
 allot to them.——TueE Slough Glee Club give its first concert on the 
 19th ult . Miss Agnes Larkcom be well receive , and bid fair , with 
 study , to become a valuable acquisition to the profession . the mem- 
 ber of the Club give Kiicken ’s ' ' Young Musicians " and ' the 
 Chafers " with great spirit . Mr. John Gower be solo pianist and 
 accompanist . the concert be under the direction of Mr. O. Christian 

 choir . second series for male and female voice . 
 — no . 2 . blessed be the soul . 8vo . , 6d 

 ELL , H.—O God , thou art my God . Anthem 
 $ vo . , 8d . 
 MITH , R. F.—The Lordis nigh unto they . Anthem 
 8yo . , 6d . 
 WOVELLO ’s OCTAVO chorus : — 
 no , 349 . where ishe ? Engedi . Beethoven . 3d . 
 350 . Jehovah , Lord God of Hosts . Spohr . 4d . 
 $ 51 . o God . who in thy heavenly hand ( Joseph ) . Handel . 3d . 
 $ 52 . come with torch . Walpurgis Night . Mendelssohn . 4d . 
 353 , I wrestle and pray . Motett . Bach . 4d . 
 WELLO ’s edition of the music for 
 Lae NATIONAL MUSIC MEETINGS at the CRYSTAL 

 class 1 . price 1s . 6d . 
 class 2 . price 1s . 0d 

 MASS in G , with latin and eng- 
 lish word ie E16 
 MASS in E flat , ditto . 1 6 ° aa 

 Hlozart , Haydn , and Beethoven 

 OF , se TWELFTH MASS 2 o 

 HAYDN ’S third , or 
 IMPERIAL MASS 

 BEETHOVEN ’S MASS inC 

 the above Three Masses , bind in one volume , whole scarlet 

 to these Masses be add Mr. E. Hotmes ' ’ Critical Essays , 
 extract from the Musical Times 

 MOZART ’S LITANIA DI VENE- 
 RABILE ALTARIS ( in e flat ) 1 
 MOZART ’S LITANIA DI VENE- 
 RABILE SACRAMENTUM , BP 1 
 BEETHOVEN ’S MASS in d ... 2 
 BEETHOVEN ’s ENGEDI ; or , 
 David in the Wilderness ( Mount 
 of Olives ) ai ji 

 Iu paper 
 cover 

