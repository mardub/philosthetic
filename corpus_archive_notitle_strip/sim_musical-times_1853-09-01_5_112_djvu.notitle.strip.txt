HANDEL’S Messiah (by Bishop), folio, 15s. 
HANDEL'S Israel in Egypt (by Bishop). Folio, 35s. 
Octavo Edition of ‘‘ Messiah” (by Bishop). 6s. 6d. 
HAYDN’S Creation (by Bishop). Folio, 15s. 
Octavo Edition of ‘¢ Samson” (by Dr. Clarke). 
HORSLEY’S Vocal Harmony. 6 Vols., £5 8s

BEETHOVEN’S 17 Quartetts. £6 6s. 
Also, his 4 Trios and 4 Quintetts (all by Rousselot). £3

BEETHOVEN'S 9 Symphonies (for Piano), £3 3s. 
- Ropert Cocxs & Co., New Burlington-st., London

10s

ees

ingle *Vol. 1. First Mass in B flat ana 4 6{ Vol. 10. Tenth MassinC minor ... 3 0 
h, 188 #,, 2. Second Mass in C 4 6! ,, 11. Eleventh Mass in F ” 1 6 
’, Bust, *,, 3. Third Mass in D 3 6] ,, 12. Twelfth Mass in E flat (om obli-} 6 
*,, 4. Fourth Mass in B flat 4 0| gato) us J 
ewly *,, 5. Fifth Mass inC a we 5 OO] 9 18 Thirteenth MassinC ... «e- 8 @ 
niaat *,, 6. Sixth Mass in B flat Po . 4 6| » 14 Fourteenth Mass —- and nd Gloria 0 
1 5 7. Seventh Mass in G 38 0 | E only) 
+, 8 Eighth Mass in B flat 2 0| » 415. Fifteenth Mass i in c 9 
f the +, 9. Ninth Mass in C 4 0|*» 16. Sixteenth Mass in B flat 5 0 
Bust, 
MOZART’S MASSES, from Vol. 17 to 34, at various prices, viz. :— 
oultry. Or complete in 3 Vols., half-bound and lettered, £2. 14s. 6d. 
_ #Vol. 17. First Mass in C Ee - 8 0} Vol. 26. Tenth Mass in B flat 2 €* 
ts of T 5, 18. Second Mass in C 3 0! ,, 27. Eleventh Mass in C 20 
nbridg. x, 19. Third Mass in F 8 O|* ,, 28. Twelfth Mass in G aaa awa (Cg 
of the », 20. Fourth Mass in F 2 O}f ,, 29. Kyrie and Gloria(13) in E flat —- ae 
» 21. Fifth Mass in C “a an BD GL aa. ee de Regal on aie ; : 
) publitt », 22. Sixth Mass in D # ee 2 Gs 31. Celebrate ae 
7 se ¥ ,, 23. Seventh Mass in B flat .. 3 0 Fi» 82. ac - Be aaah and — (16) is) 3 0 
ea pside; »» 24. Eighth Mass in C , 1 6/+ ,, 33. Seventeenth Mass in C 6 
*,, 25. Ninth Mass inG ea -- 2 Oj*,, 34. Short Requiem Mass aad “eee 
—— (With English and Latin Words.) 
TIONS, The Vocal Parts of the remainder of Mozart's and Haydn's Masses are in eee: 
— *Vol.35. SPOHR’S ORATORIO, “ The Last Judgment” 6 0 
— » 36. WEBBE’S COLLECTION OF MOTETTS AND ANTIPHONS (first published i in 
18. 6d, 1792) for one, two, three, and four voices, or chorus 5 6 
ane », 3872 WEBBE’S COLLECTION OF SACRED MUSIC, as used in wn Chapel of the King 
of Sardinia, in London ee, &G 
ae The two Masses from this collection ma nay be had separate, price 1s. 6d, each. 
— * ,, 38. MOZART. An English adaptation of Mozart’s celebrated Requiem rc oo & 6 
—— * ,, 39. ROMBERG’S TE DEUM, English words, composed for four voices ae osenk. @ 
sigh * ,, 40. MENDELSSOHN’S PSALM (115th), “ Not unto us, O Lord’”’ 2 0 
_ * ,, 41. ROMBERG’S HARMONY OF THE SPHERES, adapted to English oun for the 
ent, I$ use of the Choral Harmonists’ Society of London .. 20 
£36 * ,, 42. BEETHOVEN’S MASS IN C. The ‘ys aranged and meee simplified 
—— by Mr. Vincent NovELLo as <8 $a Gee 
— * ,, 48. HAYDN’S SEASONS. Part 1, olen eas 4 0 
ae * 4, 44. Part 2, Summer ‘<a Bound in One Vol. 38 0 
mg 40: Part 3, Autumn eas Cloth, 14s, ian Ae 
ae * ,, 46. Part 4, Winter sa wceve 
TE WEBBE’S SIX EASY MASSES, 
Teel For small choirs (originally composed for two voices), with an Alto and Tenor Part (ad. lib.) by V. ~—— 
er Vol 47. First Mass, in A ee .» 1 6] Vol.51. Fifth Mass, in F P 
—— » 48. Second Mass, in B flat .. oe a6 » 02. Sixth Mass, in F, known : as “*F i 0 
x5 49. Third Mass, in C ae eo 2 -O1 three parts” 
£5 % », 50. Fourth Mass, in D re ie Bee 
= Vol. 58. GALLIARD’S HYMN OF ADAM AND EVE — seats with a ae 
On j Accompaniment by W. Crorcu, Mus. Doc. - 2-0 
(To be continued.) 
London Sacred Music Warehouse, J. ALFRED NOVELLO, Music Seller by Appointment to Her Majesty, 69, Dean 
Street, Soho, and 24, Poultry; also in New York, at 389, Broadway

XUM

As the Hart pants (do.) 1s. éu

MOZART, HAYDN, & BEETHOVEN

The Three Favorite Masses, with the Latin words, and an 
English adaptation by R. G. Loraine, Esq.—viz., 
Mozart’s Twelfth Mass (paper) 3s. 6d. 
Haydn’s Third or Imperial (paper) 2s. 6d. }(bound) 8s. 6d

Beethoven’s Mass in C (paper) 2s. 6d

peel Folio Editions of Oratorios

The Deliverance of Israel from 
Babylon (boundincloth) 21s. 
a Isaiah (bound in cloth) 24s

BEETHOVEN, Engedi; or, David in the Wilderness 9s 
ELVEY, Dr

Each Song, Duet, Trio, Quartett, or Chorus, may be had singly, 
from 6d. to 2s, each.—Upwards of 400 are now ready

Beaumont Institution.—A literal error occurred in 
our notice of a performance last month at this institution, 
which, we are assured, is of some importance to the gentle- 
man in whose name it caused an alteration: Mr. Alfred 
Carder’s name was misprinted Carter. We are sorry to 
have been the innocent cause of any discomfiture to this 
rising young professor, and are glad his friends have 
called our attention to the matter

Musicat Unron.—Including four Musical Winter 
Evenings, and two private receptions for the trial of new 
music and débits of artists, this day’s performance 
makes altogether a series of fourteen entertainments of 
chamber classical music given by us during five months, 
and at which were produced for the first time the following 
classical works, besides various solos and vocal music of 
merit. Trio, in E flat. No. 2.Op. 100 (Schubert); Theme 
and Variations, Pianoforte, Op. 34 (Beethoven); Quintet, 
in G, Op. 83 (Spohr); Quintet, Pianoforte, E flat, Op. 44 
(Schumann); Pianoforte Sonata, in D, Op. 10 (Beethoven); 
Quartet, B flat (6-8), No. 3 (Mozart); Quartet, E major, 
No. 59 (Haydn); Sonata, No. 2 (Tartini); Quartet, B 
minor, No. 68 (Haydn); Pianoforte Sonata, Op. 47 
(Hiller); Duet 44 mains, F minor, Op. 22 (Onslow); Sestet, 
in C. Op. 140 (Spohr). The following list of performers 
is printed as they successively appeared in our programmes 
of the Musical Winter Evenings, and Musical Union 
Matinées :—Messrs. Molique, Vieuxtemps, Bazzini, Mel- 
lon, Goffrie, Hill, Webb, Henry Blagrove, Piatti, Liitgen, 
F. Pratten, Bottessini, R. S. Pratten, Barret, Lazarus, 
Wuille, Bauman, Jarrett, C. Harper, Hallé, Pauer, Mdlle. 
Clauss, Haberbier, Hiller, Blumenthal, Mdlle. Staudach, 
and Arthur Napoleon. At two evening receptions in 
the Concert Room of the Réunion des Arts, the following 
artists performed for the first time in England:—Violin, 
Graf; Viola, Ries (pupils of Vieuxtemps); Violoncello, 
Jacquard and Drechsler; Pianoforte, Mdlle. Graever. 
‘Summary—14 Germans; 3 French; 3 Italians; 2 Belgian ; 
9 English, and 1 Portuguese.—Ella’s Musical Record

Sivort.—-By the upsetting of a carriage in Switzerland, 
this well-known violinist has received such injuries as to 
prevent him, for some weeks, appearing in public

BrapDrorD Musica FestivaAL.—This event opened at 
the St. George’s Hall, Bradford, on Wednesday, the 31st, 
and terminates September the 2nd. The Earl of Hare- 
wood is the President. The principal vocalists consist of 
Madame Clara Novello, Miss Louisa Pyne, Mrs. Lockey 
(late Miss M. Williams), Mrs. Sunderland, Miss Freeman, 
and Madame Castellan; Mr. Sims Reeves, Mr. Lockey, 
Mr. Weiss, Mr. Winn, Herr Formes, Signor Tagliafico, 
and Signor Gardoni. Mr. J. L. Brownsmith is appointed 
organist, and Mr. W. Jackson, of Bradford, will officiate 
as chorus-master. Mr. Costa is the conductor. The band 
consists of sixteen first violins, sixteen second violins, ten 
tenors, ten violoncellos, ten double-basses, two harps, two 
flutes, two oboes, two clarionets, two bassoons, four horns, 
two trumpets, three trombones, one ophicleide, one double 
drum, one triangle and side drum, and one bass drum, 
making in all a force of eighty-five. The chorus is very 
powerful, and numbers close on two hundred and twenty 
members. They are, for the greater part, selected from 
the choirs of Leeds, Bradford, Halifax, and Huddersfield

Uxzxince.—The members of the Choral Society gave 
their first entertainment on the 20th June, under the di- 
rection of Mr. J. T. Birch: the programme embraced 
a selection from the works of the best sacred composers— 
including those of Handel, Haydn, Moscheles, Mendels- 
sohn, Beethoven, &c. The performance, we are informed, 
gave general satisfaction: one of the regulations of this 
new society enjoins a meeting for practice weekly—a rule 
that must be attended with a good result

Rocupar.-—An Organ Performance was given in the 
Assembly Room on August 10th, by Mr. R. Hacking, jun. 
The selection comprised the works of Handel, Mozart, 
Bach, and Mendelssohn

Jiles. A Progressive Introduction to 
PLAYING on the ORGAN, consisting of Fifty-five 
| Preludes, Fugues, Airs, &c., in two, three, and four parts, from 
ithe works of Dr. Arne, A. W. Bach, Barthélémon, Dr. Call- 
|cott, Cherubini, Clementi, Corelli, Dussek, Fenoglio, Graun, 
|Dr. Greene, Handel, M. Haydn. Herz, Keeble, Keisewelter, 
Max Keller, Knecht, Lauska, Lawes, Dr. Mendelssohn Bar- 
tholdy, Miiller, Naumann, Novello, Paganini, Pergolesi, Pinto, 
Reber, Reisseger, Richardson, Rink, Rousseau, Scarlatti, F, 
Schneider, Steibelt, Weber, Werner, Winter; to which is added 
some account of the Instrument itself; a notice of its various 
Stops, and the manner of combining them ; with Directions 
and Exercises for the use of the Pedals. By Joun Hires, 
Organist of the Music-hall, of St. Julian’s Church, and St, 
John’s Chapel, and Conductor of the Choral Society, Shrews- 
bury. Price ros. 6d

HILES. SHORT VOLUNTARIES, arranged by J. Hitzs, 
Organist of the Music-hall, of St. Julian’s Church, and of St, 
John’s Chapel, Shrewsbury; from the works of Abel, Dr. 
Arne, Beethoven, Cherubini, Clementi, J. B. Cramer, Defesch, 
Dussek, Fenoglio, Geissler, Gluck, Handel, Haydn, Herz, 
Hesse, Himmel, Hummel, Keisewelter, Keller, Knecht, 
Lauska, Lawes, Long, Mendelssohn, Marcello, Mozart, 
Miiller, Naumann, Onslow, Paganini, Pinto, Reber, Reissiger, 
Rink, Rousseau, Spohr, Weber, and Winter. In Nive Booxs, 
price 1s. 3d. each

These Voluntaries occupy only four minutes each in their 
performance

Selected) for the Organ, intended principally for the 
Soft Stops, and inscribed to the Venerable Philip Jennings, D.D., 
Archdeacon of Norfolk. 
In 36 Numbers, In 6 Books, In 1 Volume, 
Is. 3d. each, 6s. each. 31s. 6d

Each Book contains 50 Melodies from the following Au- 
thors :—Albrechtsberger, Attwood, Bach, Beethoven, Bellini, 
Bononcini, Battishill, Berger, Boyce, Cramer, Cherubini, 
Clementi, Couperin, Cooke, Dr. Croft, Donizetti, Dragonetti, 
Dussek, East, Florimo, Eliza Flower, Gollmick, Geminiani, 
Gluck, Graun, Goudimel, Greene, Handel, Haydn, Herold, 
Hesse, Hummel, Juvin, Kalkbrenner, Keeble, Klose, Krufft, 
Mozart, Mendelssohn, Minoia, Neukomm, Novello, Natividad, 
Onslow, Pinto, Paxton, Purcell, Pleyel, David Perez, Paradies, 
Rousseau, Russell, Rossini, Romberg, Rosa, Reading, Reinagle, 
Righini, Seeger, Spohr, Stokes, Schneider, Steibelt, Travers, 
Turnbull, Viner, Webbe, Weber, S. Wesley, Winter, Woelfl, 
and Wranizky

oo Complete Theoretical and 
h Practical ORGAN SCHOOL, containing Instructions 
for Playing the Organ, with numerous Exercises for acquiring 
the use of the Pedals; translated by Charles Flaxman, and 
edited by J. G. Emett. Price ros

