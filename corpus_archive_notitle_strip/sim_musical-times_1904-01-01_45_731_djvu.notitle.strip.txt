studied composition and orchestration under 
Professor Prout with the greatest possible 
advantage. The Principal, Sir George Macfarren, 
gave him every encouragement to compose. 
German soon began to make his mark at

Tenterden Street, first as a violinist. He joined the 
ensemble class, then under genial Prosper Sainton, 
and played viola in the Posthumous Quartets of 
Beethoven, his colleagues being Winifred Robinson, 
H. C. Tonking, and J. E. Hambleton. Although 
not a pupil of his, Sainton lent him his Guarnerius 
violin (worth £800) to play the De Beriot and 
Mendelssohn concertos upon at an Academy 
concert and annual examination respectively. 
Within the first year of his pupilage at the 
Academy, German won the Tubbs prize bow. 
THE MusicaL Times of the years 1881 to 1887 
(inclusive) records various appearances and achieve- 
ments of the young man from Whitchurch. As a 
composer we find his name in public performances, 
against a Pizzicato movement (1883) and a Bolero 
for violin and orchestra (1884). He tried very 
hard for the Potter Exhibition, but failed. ‘To 
qualify for this in regard to organ playing he used 
to walk at six o’clock in the morning from his 
lodgings in Hampstead Road to Westminster Chapel 
in order to practise the organ, the long walk to 
and from and the practice creating a keen appetite 
for breakfast. The year 1885 brought him much to 
the front, when he gained the Lucas silver medal 
for the composition of a Te Deum (in F) for 
voices and organ. In 1886 he produced his 
operetta (performed by the Operatic Class of the

Academy) entitled ‘The Two Poets,’ afterwards

A London newspaper, in the course of a dramatic 
criticism, says: ‘ Mr. - has recently developed 
a passion for the flute, on which he accompanies 
himself in a very touching little ditty.’ A 
correspondent asks: ‘How can a man, though an 
actor, play the flute and sing at the same time? 
Orpheus with his lute would seem to be nothing 
compared with Mr. — with his flute

A reputed saying of Rubinstein’s:—‘ X plays 
Beethoven’s Sonatas with velocity, and Czerny’s 
Studies with feeling

As we go to press, we have received, by the 
courtesy of Messrs. Paling, of Sydney, N.S.W., the 
following interesting cablegram

MEMORIAL CONCERT TO MR. NEWMAN

Of the numerous orchestral concerts held at Queen's 
Hall during the past month, one of the most memorable 
was that given on the 2nd ult. by the Queen's Halli 
Orchestra and its conductor, Mr. Henry J Wood, to 
Mr. Robert Newman, in recognition of his services in 
promoting and fostering a love of orchestral music. The 
day was appropriate, for it was the tenth anniversary of 
the opening of Queen's Hall to the public. The 
programme consisted of Beethoven's ‘Egmont’ Overture, 
Tschaikovsky’s ‘Pathetic’ Symphony, and Wagner 
excerpts

There is no need of detailed criticism concerning the 
Queen's Hall symphony concerts which took place on 
November 28 and the 12th ult. At the former, the chief 
feature was the revival of Borodine’s Symphony in 
B minor, No. 2; Bach's ‘ Brandenburg’ Concerto in G 
(No. 3) was splendidly played by the strings; and the 
soloists were Miss Muriel Foster and Mr. Jean Gerardy, 
both of whom were heard at their best. At the concert 
on the r2th ult. was presented Tschaikovsky’s Fourth 
Symphony, which was magnificently interpreted, and 
Madame Carrefio played the solo part of Grieg’s 
Pianoforte Concerto. Madame Schumann-Heink sang 
superbly the recitative and rondo ‘Non pit di Fiori’ 
from Mozart's last opera ‘Ila Clemenza di Tito,’ the 
elaborate obbligato for basset horn being beautifully 
played by Mr. F. Gomez on a new model specially made 
for Mr. Wood by M. Selmer, of Paris

The Dulwich Philharmonic Society, conducted by 
Mr. Arthur Fagge, gave an excellent performance at the 
Crystal Palace on the 12th ult. of Berlioz’s ‘ Faust,’ 
before a large and enthusiastic audience. The occasion 
was of more than ordinary importance, for it was 
the first interpretation of the new English version 
prepared by Mr. Paul England and recently published 
by Messrs. Novello. It may in truth be said that 
the very enjoyable English adaptation of the French 
libretto will certainly contribute to the longevity 
of the work—one that presents Berlioz at his best and 
indubitably shows his right to a prominent niche in the 
temple of fame. The performance was admirable, the 
choristers singing with great intelligence and the solos 
being effectively rendered by Miss Eunetta Truscott and 
Messrs. Herbert Grover, Haigh Jackson and Peter 
Dawson. Mr. Arthur Fagge conducted

The first concert of the fourteenth season of the 
Richmond Philharmonic Society was successfully given 
on the 17th ult., before a large and appreciative audience, 
at the ‘Star and Garter,’ Richmond. Sullivan's ‘ Martyr 
of Antioch’ occupied the first part of the programme, in 
which the soloists were Miss Winifred Parker, Miss 
Edith Lane, Mr. Frederick Norcup, and Mr. Stewart 
Gardner. The second part included Beethoven's 
‘ Leonora’ Overture (No. 3), and the first performance in 
England of the arrangement for chorus and orchestra of 
Grieg’s ‘ Herbstiirm.’ The concert was ably conducted 
by Dr. C. E. Jolley, who had the efficient co-operation of 
a capital band and chorus

The West Ham Choral Society, under the direction of 
Mr. W. Harding Bonner, gave a concert of gems from 
Handel s works on the rst ult., the programme including 
an important selection from ‘Judas Maccabzus’; the 
other works drawn upon being ‘Acis and Galatea,’ 
‘Solomon,’ ‘L’Allegro,’ ‘Samson,’ and the operas 
‘Serse ’ and‘ Alcina.’ The ‘ Occasional’ Overture was 
played by the orchestra, which also accompanied the 
Organ Concerto, No. 4 in F, in which the solo part was 
undertaken by Mr. Henry Riding. The solo vocalists

MUSIC IN BIRMINGHAM. 
(FROM OUR OWN CORRESPONDENT

At the Halford Concert of November 24, Mr. Fritz 
Kreisler made his second appearance in Birmingham, when 
his playing in Beethoven's Violin Concerto and Tartini’s 
‘Il Trillo del Diavolo’ (with accompaniment of strings 
and organ) evoked extraordinary enthusiasm. Rutland

Boughton’s symphonic poem ‘A Summer Night’ was| J

First Symphony and the ‘Carnaval Romain’ Overture 
of Berlioz. Herr Fritz Kreisler played in magnificent 
style the G minor Violin Concerto of Max Bruch anda 
Fantasia of Paganini

The first concert of the Amateur Orchestral Society 
showed that Mr. Collinson has brought his forces toa 
still greater degree of excellence than before, and that 
their resources of technique and expression are greatly 
increased. The programme was an extremely interesting 
one, and reached its climax in a fine rendering of the 
Eighth Symphony of Beethoven. The vocalist was Mr. 
Scott-Macpherson. By-the-way, in mentioning the forth- 
coming performance by this Society of the Choral 
Fantasia of Beethoven we, by a very regrettable slip of 
the pen, gave the name of Mr. Della Torre as the soloist

instead of that of Mr. Alfred Hollins

The Choral and Orchestral Union's season opened on 
| November 24 with a magnificent performance of Elgar's 
‘ Dream of Gerontius ' by the Choral Union and Scottish 
Orchestra. Seldom has any choral novelty aroused such 
genuine interest and enthusiasm, and the initial perform- 
ance of Elgar’s noble work in Glasgow attracted one of 
the largest and most keenly appreciative audiences ever 
seen in St. Andrew’s Hall. Nothing but praise can 
be bestowed upon the chorus and semi-chorus (an 
admirably chosen body) for their excellent singing. Mr. 
Joseph Bradley, the conductor, must be congratulated on 
securing one of the best performances yet given of this 
work. The soloists, Miss Muriel Foster and Messrs. 
John Coates and Walter Harvey, the last-named being a 
rising local baritone, left nothing to be desired

The second classical concert, on the 1st ult., was notable 
chiefly for Signor Busoni’s refined and musicianly 
reading of the solo part in Henselt’s Pianoforte Concerto 
in F minor, and Dr. Cowen’s first-rate handling of the 
‘Eroica’ Symphony. Two novelties—Wagner’s ‘ Die 
Feen’ Overture and Cowen’s ‘ Reverie’ for orchestra 
(most sympathetically played)—lent additional interest to 
the programme. An enormous audience assembled to 
welcome Dr. Richard Strauss, who occupied the con- 
ductor’s desk at the third Classical Concert on the 
8th ult. Two of the composer-conductor’s works were 
included in the programme—viz., his Symphonic 
fantasia ‘Aus Italien’ (first performance here) and the 
humoresque ‘Till Eulenspiegel.’ In these, and in 
Beethoven’s overture ‘ Leonora No. 3,’ the band played 
magnificently

The Glasgow Glee and Catch Club, which has this 
season been strengthened and improved by the addition 
of some fresh voices, made a successful appearance on 
November 30, A well-arranged selection of glees and 
part-songs was submitted, but the Club’s best effort was 
in Mr. William Wallace’s clever setting of Professor 
Aytoun’s humorous ballad ‘The Massacre of The 
Macpherson.’ Reversing the usual order, Mr. Wallace's 
music is a kind of commentary on the words, and for 
this purpose he makes use of several well-known Scottish 
melodies in addition to some familiar excerpts from 
Wagner. The Club sang with great gusto and full appre- 
ciation of the humour of the piece, and the ballad was 
evidently much enjoyed by the audience. Mr. George 
Taggart conducted, and Mr. A. M. Henderson lent 
valuable aid with the accompanients

Society, ably

Thanks to the elaborate care with which the works had | conducted by Mr. W, T. Hoeck, gave a most meritorious 
been previously rehearsed, the performance was striking | performance on the roth ult. Beethoven's Symphony in D, 
and brilliant in the extreme. So also was that at the| Berlioz’s ‘King Lear’ Overture, and Vieuxtemps’ Violin 
third concert, when the chief items were Schumann’s| Concerto No. 4 in D minor were the leading items on the

asiiaad

MUSIC IN LIVERPOOL AND DISTRICT. 
(FROM OUR OWN CORRESPONDENT

Dr. Richter appeared to direct the concert of the 
Orchestral Society which was given Jn Memoriam Alfred 
Rodewald on the 5th ult., and under his care the orchestra 
entered fully into the pathos and significance of the 
occasion. The scheme included Mozart’s ‘ Masonic 
Dirge,’ Brahms’s Rhapsody founded upon Goethe's 
‘Harzreise im Winter,’ the Prelude to ‘ Parsifal,’ the 
Prelude and ‘ Angel’s Farewell’ from ‘The Dream of 
Gerontius,’ Strauss’s tone-poem ‘Death and Trans- 
figuration,’ and Beethoven's ‘ Eroica’ Symphony. In

sympathy, members of the Philharmonic Society came 
forward to render the chorus portion of the Brahms 
work, whilst Miss Muriel Foster sang the solo in that, 
and also the ‘ Angel’s Farewell’ with rare power and 
vocal introspectiveness

The first ‘Gentlemen's Concert’ of the same Society was 
given in the Gymnasium on the 12th ult., when Mr. Alfred 
toss was at the first desk, and Mr. V. V. Akeroyd con- 
ducted. The programme included Schubert’s C major 
Symphony, an Adagio by Bargiel for Violoncello (played 
by Mr. E. Hutton) and Orchestra, Max Bruch's Violin 
Concerto, the solo part being very well rendered by 
Mr. J. S. Bride, and the ‘Midsummer Night’s Dream’ 
Overture

Two concerts of the Liverpool Philharmonic Society 
have occurred since my last letter, that on November 24 
being characterized by a singularly fine rendering of the 
solo part of Liszt’s Pianoforte Concerto in A by M. Busoni. 
The Prelude to the first act of Richard Strauss’s 
‘Guntram,’ that to Elgar’s cantata ‘Lux Christi,’ 
and Beethoven's Symphony in D were included in 
Dr. Cowen's programme, and Madame Blauvelt was the 
vocalist

The concert on the 8th ult. brought with it Mr. 
Fritz Kreisler, who also played Bruch’s G minor 
Concerto. The vocalists were Miss Louise Dale and 
Mr. Hamilton Earle, and the orchestra revelled in 
Haydn’s Symphony in C, ‘ L’ours

The Newcastle Amateur Vocal Society, conducted by 
Mr. J. E. Jeffries, organist of the Cathedral, has given 
Dr. Prout's ‘Hereward the Wake’; the Philharmonic 
Society (Mr. Geo. Dodds) has performed Gounod’s 
‘Faust’; and the Postal Telegraph Choral Society 
(Mr. T. Hutchinson) Gaul’s ‘ Joan of Arc

At the concert of the Auckland Musical Society, under 
the conductorship of Mr. N. Kilburn, Stanford’s 
‘Revenge’ and Jensen's ‘Feast of Adonis’ were sung, 
and Mr. Louis Pecskai played Beethoven’s Violin 
Concerto. The ‘Creation’ has been performed by the 
Middlesbrough Musical Union, also under the direction 
of Mr. Kilburn, by the Gateshead Vocal Society 
(Mr. N. Laycock), and at North Shields, conducted by 
Mr. H. Y. Dodds. The South Shields Choral Society, 
under Mr. M. Fairs, gave on the 16th ult. Mendelssohn's 
‘Hymn of Praise’ and Dr. Elgar’s ‘Coronation Ode,’ 
the latter for the first time in the district

A highly interesting lecture was delivered by 
Mr. W. H. Hadow in the theatre of the Literary and 
Philosophical Society on the 14th ult., the theme of 
his discourse being Beethoven's Sonata in D (Op. 28

MUSIC IN NORWICH. 
(FROM OUR OWN CORRESPONDENT

The programme was enriched with some valuable 
analytical notes of historical interest, written by Dr. A. H. 
Mann, the conductor. The additional accompaniments 
added to some of Handel’s works by later composers 
were not used, the works being performed in the original 
Handelian form. The choir sang with good attack, 
great precision and power, and due attention to light and 
shade, and showed that it had benefitted by the careful 
training received at the hands of Dr. Mann

At the concert given by the Norwich Philharmonic 
Society on the 17th ult., the Norwich Choral 
Society—under their conductor, Dr. Bates—introduced 
several madrigals and part-songs, which were sung in a 
finished style. The instrumental part of the concert was 
supplied by a trio consisting of Miss Gertrude Peppercorn 
(pianoforte), Mr. Dettmar-Dressel (violin), and Mr. Bertie 
Withers (violoncello), who performed two Trios, the 
‘Op. 21 in D’ of Dvorak, and the ‘Op. 3 in C minor’ 
of Beethoven. Miss Gertrude Peppercorn contributed 
two pianoforte solos by Chopin,—Etude in A flat and 
Fantasia (Op. 49)—while Mr. Plunket Greene, the sclo 
vocalist of the evening, delighted his hearers with two 
groups of songs of varied character

MUSIC IN NOTTINGHAM AND DISTRICT. 
(FROM OUR OWN CORRESPONDENT

THE MUSICAL TIMES.—Janvuary 1, 1904. 49

Philharmonic Orchestra gave us a delightful concert, 
the principal items being Beethoven’s Symphony in F 
(No. 8), Wagner’s ‘Siegfried Idyll,’ and Schumann’s 
Overture ‘ Genoveva

On November 18 our genial Professor of Music, 
Sir Hubert Parry, gave an admirable lecture in the 
Sheldonian on ‘ Realistic Suggestion and Programme,’ 
the illustrations to which were excellently given by two 
young ladies from the Royal College of Music

Though during the past month there have been a good 
many concerts at Leeds, there has been little of general 
interest. Quite the most noteworthy event was the 
appearance, on the 5th ult., at one of the Municipal 
Orchestra's concerts, organized by Mr. Fricker, of 
Mr. Josef Holbrooke, who conducted his exceedingly 
clever orchestral variations on ‘Three Blind Mice.’ 
Their difficulty is considerable, and to this, and their 
novelty, may be ascribed some want of clearness in the 
performance. In point of construction, the composer has 
not quite overcome the difficulty of avoiding monotony 
that is inevitable in writing a series of variations 
on a short and not very distinguished theme. Of 
its suggestiveness, however, he has fully availed 
himself, and his devices in counterpoint and colouring, 
and in the introduction of snatches of other 
tunes, show remarkable skill. In Mozart’s G minor 
Symphony, the ‘ Casse Noisette’ Suite, and other things, 
the-orchestra, under Mr. Fricker, showed its efficiency 
and really good quality, and all musicians in the town 
are hoping that this really artistic venture may meet with 
the success it deserves. The local orchestra, which 
Mr. Edgar Haddock conducts, has also done much to 
popularize orchestral music, and on the roth ult. it gavea 
concert at nominal prices, when the programme, follow- 
ing a precedent set just a year ago, was devoted to 
Handel and Wagner, a singular contrast, and indeed so 
striking a one that comparisons were happily impossible

There have been two choral concerts of interest. On 
the gth ult. the Leeds Choral Union gave Gluck’s 
‘Orpheus’ with excellent effect, Miss Giulia Ravogli 
singing her famous part, as she has done with this 
Society on two previous occasions, at Leeds and in 
London. Elgar’s ‘Bavarian Highlands’ Suite was the 
other work performed, under the conductorship of 
Mr. Alfred Benton. The enterprising suburban society 
which Mr. H. P. Richardson conducts at Headingley, 
gave a programme of anything but hackneyed character 
on the 14th ult. Neither Bach’s cantata, ‘ Bide with us,’ 
Beethoven's ‘ Mount of Olives,’ nor Mackenzie's ‘ Bride,’ 
has been heard much in the West Riding of late, and all 
deserved revival. With efficient principals—among 
whom the tenor, Mr. Brearley, deserves special mention 
for his artistic singing—and a small but well-selected 
orchestra of local musicians, a series of really adequate 
and highly interesting performances were given, and one 
only wishes the example may be followed

The Leeds Musical Union gave an exceptionally 
interesting programme of male-voice music on the 7th ult. 
Palestrina’s Mass ‘ Aeterna Christi munera’ beaded a list 
of pieces which meandered gently through the centuries 
till it ended with Sullivan's ‘The long day closes,’ a

roused the singers to a greater display of animation and | 
vigour than they have been in the habit of showing

while Madame Sobrino, Mr. William Green, and 
Mr. Herbert Brown assisted in a very good all-round 
performance. The Keighley Orchestral Society, of which 
Mr. Summerscales is the conductor, gave a creditable 
performance on the oth ult. of Beethoven's ‘ Pastoral’ 
Symphony, among other less important things, and 
Mr. W. H. Squire contributed violoncello solos. 
‘St. Paul’ was given at Pudsey on November 30, 
under Mr. H. Pickard's direction, and ‘ Jephtha’ 
was the subject of the Batley Society’s concert 
on the ist ult., Mr. J. Fearnley being the conductor. 
The Dewsbury Choral Society broke in upon established

custom by giving at its concert on the Sth ult. chamber | 
music, the work of the chorus being confined to Franz’s | 
‘ Liebeslieder’ under Mr

Equally energetic is the York organist, Mr. T. Tertius 
Noble, whose creation the York Symphony Orchestra 
may truly be styled. Its prowess was shown on Novem- 
ber 23, when Mr. Edward German conducted a number 
of his compositions, the chief being his symphonic poem 
|‘ Hamlet,’ which was most creditably played. On the 
| 15th ult. the York Musical Society, of which Mr. Noble 
is also the conductor, gave C. H. Lloyd’s ‘ Hero and 
Leander ' and Parry's ‘ Blest Pair of Sirens,’ the chorus 
singing with much spirit. The principals were Miss Dow 
and Mr. Nitschke

At a very enjoyable concert given by the Hull Philhar- 
monic Society on the 4th ult., under Mr. Hudson, the 
orchestra gave a good performance of Beethoven's Eighth 
Symphony, and the artistic singing of a local vocalist, 
Miss Dorothy Wiley, contributed to the interest of the 
proceedings. At Scarborough Messrs. Cass and Owen 
Williams have been continuing their excellent chamber 
concerts, Arensky’s Pianoforte Quintet in D (Op. 51) 
being the feature of the concert on the 14th ult. Mrs. 
Burrell’s Subscription concerts at Malton also deserve 
;mention. On the 12th ult. she had the assistance of 
| Miss Agnes Nicholls in a most enjoyable programme

Miscellaneous

society under the direction of Professor Iniese which

has just given its two-hundredth, or jubilee concert. 
Beethoven, Liszt, Wagner, and Siegfried Wagner were 
the four composers represented : the first by his Seventh 
Symphony, the second by his Pianoforte Concerto in A, 
the third by his ‘ Tannhauser’ Overture, and the fourth 
by a vocal excerpt from ‘ Herzog Wildfang,’ given under 
the composer's own direction. 
BERLIN

The fifth Symphony Concert was devoted to the 
memory of Berlioz, the programme including the Fantastic 
Symphony, magnificently performed under the direction 
of Weingartner, the ‘ Rob Roy’ Overture, the dramatic 
scena ‘Cléopatre,’ recently heard in London, and the 
Marche from ‘Les Troyens.’ It is curious that the 
programmes of the three Berlioz concerts given in 
London did not contain a single excerpt from the last- 
named work, and all the more so considering that 
Berlioz arranged it for concert performance

4th ult. According to the latest news the performances are, 
however, interrupted owing to the illness of M. Delmas, 
who impersonated L'Firanger. The role of Vita 
was taken by Madame Bréval.—— The 23rd ult. 
was the date fixed for the production of ‘La Keine 
Fiammette,’ by MM. Xavier Leroux and Catulle 
Mendés, at the Opéra-Comique. —— It has long ago 
been stated that the hundredth anniversary of the 
birth of Berlioz on the 11th ult. deserved to be worthily 
celebrated in this city. At the Conservatoire, Colonne 
and Chevillard concerts, Berlioz has certainly been 
largely represented, but the only notice taken of the day 
itself was the placing of flowered wreaths round the 
statue of the composer in the Place Vintimille, the 
Mayor of Cote-Saint-André, M. Bourgault-Ducoudray, 
and M. Eugéne d'Auriac, professor of the Faculty of 
Letters, and representatives of various societies being 
present. A few brief words were spoken by the three 
persons named, M. Ducoudray remarking that ‘le silence 
de nos voix rendra mieux le regret de nos cceurs.’ 
PRESSBURG

At the cathedral service for the festival of St. Cecilia 
(November 22), Beethoven's ‘‘ Missa solemnis’ was 
performed under the direction of Gabriel Franck, of Kaab. 
The Mass was first given here in the year 1835 under 
Kumlik (1801-1869), and since 1891 it has formed part of 
the service on St. Cecilia’s day. In thé invitations 
issued this year, attention was called to the fact that only 
when presented in conjunction with the service can the 
music of this ‘ noblest of all sacred works’ produce its 
proper impression

ROME. 
The opera season commences immediately after 
Christmas. Ten works are announced, among the

CESTRIAN.—Braham's song ‘The Death of Nelson’ 
was written for, and probably sung by him in his opera 
‘The Americans,’ produced at the Lyceum Theatre, 
April 17, 1811. As Nelson did not die until the year 1805, 
the song must have been composed after Méhul’s‘ Chant du 
départ’ in 1794 (see Grove’s Dictionary i, 440b). We 
cannot say if the first part of Braham’s song is a 
conscious or an unconscious plagiarism of Méhul’s air

A. F. T.—Miss Fanny Davies made her first appearance 
in England, after she had completed her studies in 
Germany, at the Crystal Palace Saturday Concert of 
October 17, 1885, when she gave a fine performance 
of Beethoven's Pianoforte Concerto in G. We regret 
that we cannot give any information concerning the lady 
vocalist you mention

M. H. V. M.—(1) The Pastorale and Capriccio (Nos. 
g and 20) are contained in Breitkopf and Haertel’s 
‘Sixty Sonatas’ of Scarlatti. Quite right. No. 300f the 
same set is ‘ The Cat’s Fugue.’ (2) Theterm ‘ Biroulki’ 
is equivalent to Bagatellen—small pieces. See the 
pieces by Liadoff in No. 41 of Novello’s Pianoforte 
Albums

i, A f YA De 7

Viola, Violoncello, and Bass. Music (from Beethoven to By F. A. CHALLINOR, Mus. Doc. (Dunelm). P:. 
German's Dances), arranged for Strings and Harmonium; 60 lots. Staff, 1s.; Tonic Sol-fa, 4d. (Band Parts on hire). Gi 
Offers wanted. Packham, Havant. scenic Gi

TORMAN and BEARD have three good Second- FOR MALE-VOICE CHOIRS. Gs 
M hand ORGANS, . and — — by good “rm “ THE MASSACRE OF MACPHERSON a

The Responses to the Commandments have been selected from the 
Services of the above composers as well as those of Myles B. Foster, 
Dr. Garrett, Ch. Gounod, Dr. E. J. Hopkins, Dr. C. H. Lloyd, 
Merbecke, H. Smart, John E. West, S. S. Wesley, and others

Sir John Stainer’s Sevenfold Amen is also included, as well as Vesper 
Hymns by Beethoven Sullivan, and others; concluding with two 
Vestry Prayers by S. S. Wesley and the Rev. Canon Hervey

PREFACE

THE COLLECTION nxciupeEs

SYMPHONIES 
By BEETHOVEN, HAYDN, MENDELSSOHN, MOZART, SCHUBERT, 
SCHUMANN, etc

OVERTURES 
By AUBER, BEETHOVEN, CHERUBINI, DONIZETTI, GLUCK, KREUTZER, 
LORTZING, MENDELSSOHN, MOZART, ROSSINI, SCHUBERT, 
WAGNER, WEBER, etc

BALLET MUSIC 
By CHERUBINI, GLUCK, SCHUBERT, etc

MARCHES 
By BEETHOVEN, BERLIOZ, CHOPIN, MENDELSSOHN, MEYERBEER, 
WAGNER, etc

MISCELLANEOUS PIECES 
By GRIEG, REINECKE, GRETRY, evc

