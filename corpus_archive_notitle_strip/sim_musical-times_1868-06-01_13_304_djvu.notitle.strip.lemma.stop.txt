bes organist Carlisle Cathedral want 
 art{cle pupil . term obtain - applica 

 Music , Old Harpsichord Music , Pianoforte Studies , & c. , complete 
 Works Great Composers Pianoforte , Overture : , & . , 
 arrange Quintetts Pianoforte Accompaniments , Septetts , 
 Quintetts , Quartetts , Trivs , duet ; Violin Music 
 Pianoforte Accompaniments ; Fiute Music ; Glees , Madrigals , 
 Songs , & c. ; Old Works Harmony Composition , 
 History Music , Opera Church Music ; modern 
 ditto . Mendelssobn 13th Psalm , 2 . 6d . Spohr 128th psalm , 
 1s . 6d . Beethoven Pianoforte Sonatas , 4s . 10d . musical anti- 
 quarian Society Publications , 19 vol . , complete , £ 6 6 , Purcell 
 Ayres , 1697 . £ 2 12s . 6d . choron . Principes de Composition des 
 Ecoles d’Italie , 3 vol . folio , scarce , £ 2103 . Seidel Con- 
 struction Organ , 4s . 6d . descriptive Catalogue , enrich 
 critical Biographical Notes , send post stamp . 
 Alfred Whittingham , 45 , Frith - street , Soho - square , London , W 

 EV . R. HAKING - song 
 8.a.7.b. , Male Voices . 3d . . 
 song old Bell . 
 Violet . 
 wind Errand . 
 Christmas Holly . 
 Knight falchion gleam . 
 song wavelet . 
 ring - - ding - - ding 

 MUSICAL TIMES.—Jvne 1 , 1868 

 , beneath criticism , im- 
 possible , limited space provide review , 
 state reason ; 
 alternative , , . 
 reader , instance , surprised 
 tell half compo- 
 sition forward , 
 gross plagiarism ; merely contain 
 passage come old 
 value friend , literally copy 
 previous song piece known . ' , 
 inane ballad — , way , 
 bit bad preserve contempt 
 " royalty " system — multiply 
 extent begin wonder , 
 thing buy , good music sell . 
 list come ambitious attempt 
 rest contented maudlin 
 trifle ; having hear ' " Beethoven 
 understand , " dash hetero- 
 geneous mass chord mysterious passage , 
 form wild disconnected piece , 
 usually christen romantic title , believe 
 destine deeply philo- 
 sophical aim music , carry art 
 great german composer leave . sacred 
 work , evidently consider necessary 
 original idea ; dry succession 
 chord fit religious word 
 think sufficient purpose , provide 
 officious critic discover glaring 
 defect musical grammar 

 swell catalogue crudity 
 great extent ; mention 
 onerous duty 
 reviewer . , motive kindness , pass 
 work , declare unjust 
 partial ; notice expose weak- 
 ness , term bitter , ungenerous rise 
 composer . course , , open — 
 true art ; regard ail composition be- 
 fore abstract bearing pro- 
 gress . ' , bad work known composer 
 remain unnoticed , whilst good work unknown 
 composer , review length . pursue 
 system , satisfaction 
 feel duty accord 
 good ability ; 
 expect composer like 
 pass crowd , , , 
 seize collar publicly 
 abuse 

 MR . HENRY LESLIE concert 

 Tue eleventh Concert series highly attractive 
 admire genuine unaccompanied choral music , 
 madrigal , include " sweet honey - suck bee , " 
 " bonny Lass , " & c. , agreeably mix 
 composition modern school . Mr. Sims Reeves 
 Mr. Santley principal vocalist ; clever 
 pianist , Mr. Frederick H. Cowen , highly favourable 
 impression piece Henselt , Liszt " Krl King , " 
 Chopin Ballade inG minor . twelfth Concert 
 orchestral , include Mendelssohn Italian Symphony 
 Beethoven Choral Fantasia , pianoforte 
 ably play Madlle . Mehlig . choral 
 music fine " hymn Bacchus , " trom Antiyone , 
 selection @dipus . solo vocaiist . Miss 
 Kellogg . excellent Concert 20th 
 ult . , Director benefit , Madlle . Titiens Mr. 
 santley principal singer , Mendelssohn 
 Reformation Symphony form chief orchestral feature 
 programme 

 CRYSTAL PALACE 

 Tue series Concerts title 
 St. James Hall Ist ult . , 

 Handel Alezandey Feast , Beethoven Ruins 

 Athens perform . large chorus excel- 
 lent orchestra , music ; 
 insufficiency rehearsal , want 

 St. Paul Cathedral . chorister select 
 Majesty Chapels Royal , St. Paul , West- 
 minster Abbey , St. George Chapel , Windsor , & ¢. , num- 
 bere 200 voice . regret 
 choral service include composition 
 talented organist Mr. Goss , judicious selection 
 work Mr. Henry Smart Dr. 
 8.5 . Wesley , foremost 
 modern writer Church . Mr. Smart service 
 F , canticle , 
 fine specimen truly religious music , unfettered 
 dry pedantic conventionalism , 
 acquainted ; glad find merit 
 able conscientious composer begin 
 fairly acknowledge , Festival 
 year conservatism sacred music reign triamphant . 
 fine anthem " ascribe unto Lord , " Dr. Wesley , 
 notice performance Gloucester 
 Heretord ; repeat grandeur 
 conception boldness design stand 
 good itsaccomplished composer work . 
 collect , Mendelssohn Anthem , " judge , 
 God , " ; sermon , Dr. Croft Anthem , 
 " cry aloud shout . " execution work 
 mean reproach ; effect 
 exceedingly fine ; acknowledge 
 difficulty combine choir place 
 time fur large number rehearsal , performance 
 highly creditable concern . vocalist 
 direction Mr. Winn , prove him- 
 self occasion efficient successor late 
 Mr. Henry Buckland , year associate 
 Festivals . ' prayer intone 
 Rev. J. H. Coward , M.A. , lesson read 
 Rey . B. M. Cowie . Mr. Goss 
 organist ( assist Mr. George Cooper ) 
 imagine efficiently duty responsible 
 Office fulfil . collection St. Paul 
 £ 115 ; dinner , place atter service 
 Merchant Taylors ’ Hall , £ 925 

 tuz immense number Pianoforte Recitals 
 London season appear 
 slight degree diminish attraction Mr. Walter 
 Macfarren annual series , place 
 highly appreciative audience Hanover 
 Square Rooms 2nd ult . Bach Preludes 
 Fugues B flat minor major , Beethoven sonata 

 flat ( Op . 12 ) , pianoforte violin , Sehumann 
 " Fantasiestitcke " ( Op . 12 , & e. , 
 Concert - giver composition , form _ highly 
 attractive programme instrumental music , 

 execution Mr. Macfarren special character- 
 istic . piece select com- 
 mendation elegant Romance " Mariana , " 
 beautiful cuntabile theme . treat 
 graceful simplicity true keeping nature 
 subject . Galop Tarantella favourable 
 specimen composer light vivacious writing ; 
 find useful study piece , 
 Tarantella especially , effect , 
 drawback immoderate difficulty . omit 
 mention Mendelssohn pianoforte duet , 
 ( Andante variation b flat ) , Mr. Macfarren 
 ably assist Miss Emma Buer ; Mr. Henry 
 Holmes prove Concert 
 violinist high order . second recital - 
 28rd ult . , equally classical selection . 
 prominent piece Mozart Solo 
 Sonata minor , Beethoven sonata C minor ( Op . 
 90 ) , pianoforte violin , selection Professor 
 Bennett " Suites de Pieces , " Mendelssohn pianoforte 
 duet , " * Allegro Brillante " ( Op . 92 ) , Mr. Mac- 
 farren join pupil Miss Linda Scates , young 
 pianist truly refined classical playing equal 
 honour master Royal Academy Musie , 
 Institution hold scholarship . player 
 recall performance , receive 
 congratulation audicnee , justly 
 entitle . Mr , Macfarren romance , piano- 
 forte violin , composer join Mr. Henry 
 Holmes , playing instinct musicianlike 
 feeling work . 1 , " Serenade , " 
 . 3 , " * Canzonet , " encore . Mr. Macfarren 
 play piece , 
 favourably receive , especially entitle ' " Chanson 
 d ' Amour , " perform time , , 
 think destine popular . 
 Recital , 6th inst . , Mr. Macfarren play Sth 
 book Mendelssohn Li - der ohne Wovte 

 Concert St. Mark Schools , 
 Rawston Street , Goswell Road , Tuesday evening , 
 5th ult . , following lady gentleman : — Miss 
 Carpenter , Miss kdwards , Miss Merryweather , Rev. 
 W. H. Hyde , Mr. A. C. Hunter , Mr. R. Weekes , 
 Mons . F. Duboc . programme average 
 amateur concert ; piece receive 
 utmost favour . pianoforte violin duet 
 render , violin playing Mons . Duboe 
 highly creditable . Miss Carpenter accompaniment , 
 violin song , mark good taste 
 judgment 

 lecture deliver Mr. Andrew 
 Ashcroft " English Church Musie , " 30th April , 
 St. Paul Church , Burdett Road , E. , aid fund 
 restoration west window . lecturer 
 trace , fully time permit , history 
 cathedral music early stage present posi- 
 tion ; illustrate period Anthems , & ec . , 
 different composer . ' illustration 
 Asheroft - vans Tonic Sol - fa Choir , Stepney Meeting , 
 assist amateur quartette string har- 
 monium 

 Miss Marran Burts ( pupil Mr. W. G. 
 Cusins ) , Evening Concert Beethoven 
 Rooms 22nd ult , play choose 
 selection pianoforte music , display 
 performance real artistic power intellectual 
 conception author lead hope 
 day high rank profession . 
 ‘ l'rio C minor , Beethoven ( Op . 1 ) , pianoforte , 
 violin , violoncello ( join Herr 
 Straus Mons . Paque ) Miss buels introduce 
 composition , receive 
 favour . ' vocalist Miss Robertine Henderson 

 Miss Fanny Holland , Madlle . Mela , Mr. Renwick 

 Miss Emma Busby morning Concert John Plummer , set music 
 Hanover Square Rooms , Monday , 18th ult . , Dr , Spark . Mr. Congreve composition differ Dr. 
 perform interesting pianoforte piece | Spark essential particular — 
 utmost effect , successful | piece follow , stoppage , bass 

 Mendelssohn posthumous work 
 ( Prelude , Lied Etude ) Schumann interesting 
 sketch ; pianoforte Beethoven trio 
 E flat ( ably assist Mr. Henry 
 Holmes ( violin ) Signor Pezze ( violoncello ) , 
 vocalist Miss Robertine Henderson , Signor 
 Ciabatta ; conductor Mr. Walter Macfarren 

 Madame Puzzi Annual Morning Concert 
 St. George Hall , 22nd ult . , 
 usual success . wasa long list vocalist 
 sing long list music utmost satisfaction 
 large audience ; programme agreeably 
 intersperse instrumental solo . 
 respect Concert thoroughly fashion 
 taste annually respond invitation 
 Madame Puzzi 

 concert - giver composition effect 

 Miss Aenes ZiIMMERMANN Concert , 
 place Hanover Square Rooms , 22nd ult . , 
 contain programme varied style , 
 ordinarily attractive . Beethoven Trio pianoforte , 
 violin , violoncello ( Op . 70 , . 2 ) , Chopin 
 Grande Polonaise , pianoforte violoncello , 
 choose represent distinct school writing ; whilst , 
 pianoforte solo , contrast 
 Gavotte Bach , wonderfully imaginative 

 song , proceed end inter- 
 \mission . Concert attend ; varied 
 | programme appear utmost satisfaction 
 | audience 

 Serenade " ( Hatton 

 Mr. Water Bacue gaye Annual Concert 
 ' St. George Hall , 25th April , programme 
 provide , evidently intend contrast modern 
 music " music future . " Mr. Bache 
 highly successful Beethoven pianoforte variation ; 
 ( Mr. Klindworth ) Liszt « « Potmes Sym- 
 phoniques , " arrange piano composer . 
 Berlioz , Wagner , Schumann , represent 
 occasion ; , respect , Concert highly 
 interesting . Miss Westbrook , Miss Lucy Franklein , Mr. 
 W. H. Cummings , Mr. J. B. Welch , contribute 
 vocal solo ; choral music effi- 
 ciently render Mr. Joseph Heming choir 

 performance Elijah 
 Tonic Sol - fa Association , St. James Hall , 27th 
 April , direction Mr. Thomas Gardner . 
 solo vocalist Madame Rudersdorff , Miss Charlier 

 pianoforte recital Misses 
 Kingdon , Willis Rooms , 4th ult . , 
 utmost success . power concert - giver 
 severely tax music select occasion ; 
 piece prove fully equal 
 task . effort ably second Mr. Blagrove 
 violin . series per- 
 formance classical modern pianoforte music 

 Tur West London Sacred Choral Society 
 complete successful series performance 
 Rooms Welbeck Street , Cavendish Square . 
 work produce Concerts Llijah ( Mendelssohn ) , 
 Isaiah ( W. Jackson , Masham ) , Messiah ( Handel ) , 
 Lobgesang ( Mendelssohn ) , Engedi ( Beethoven ) , Creation 
 ( Haydn ) , Judas Maccabeeus ( Handel ) . principal 
 singer , soprani , Mrs. Burgess , Miss Marie Stocken , 
 Miss Blanche Reeves , Miss Anna Isaacs ; contralti , 
 Mrs. Brooks , Miss Pond , Miss Adelaide Newton , 
 Madame Rocelli ; tenori , Mr. Greenhill , Mr. Wallace 
 Wells , Mr. Albert James ; bassi , Mr. 8 . Kilbey , Mr. 
 G. E. Coleman , Mr. Denbigh Newton , Mr. W. Owen . 
 band chorus , number eighty performer , 
 , usual , direction Society con- 
 ductor , Mr. H. C. Freeman . performance 
 numerously attend ; result highly 
 satisfactory 

 perceive Choir Benevolent Fund 
 announce " * Grand Choral Festival , " King College 
 Chapel , Cambridge , Thursday Morning , 28th ult . 
 ( late mention performance present 
 number ) , conduct Professor Sterndale Bennett . 
 choir select Majesty Chapels Royal , St. 
 James St. George , Windsor , St. Paul Cathedral , 
 Westminster Abbey , Temple Church , Lincoln Inn , 
 London , Cathedrals Lincoln , Ely , Peterborough , 
 Cambridge Collegiate Choirs . selection music 
 inclydes Dr. S. S. Wesley Anthem , " blessed God 
 Father , " ' Professor Bennett beautiful Quartet 
 Woman Samaria , ' * God Spirit , " Mr. Goss 
 Anthem , " praise Lord . " sincerely trust 
 fund noble charity materially benefit 
 festival , extensive scale 
 necessitate considerable outlay 

 form ; utterly impossible crowd intoa 

 pianoforte adaptation half passage 
 occur score , convey hearer varied 
 colouring obtain difference tone quality 
 instrument employ . good arranger 
 , frankly admit fact , seek 
 seize salient point score ; , whilst 
 broad outline , fill minute 
 detail distinctly bring per- 
 , clearly comprehend listener . 
 - handed arrangement Beethoven Symphonies , 

 Czerny , admirable ina purely musical point 

 1 . Schumann Vocal Album . 
 2 . Schumann Album Young Pianists ( Op . 68 ) . 
 3 . ' * forest scene . " R. Schumann ( Op . 82 

 , quarto form , cheap price , 
 neatly engrave print edition 
 exquisite piece , vocal instrumental , com- 
 poser long , ignorantly wilfully , 
 subject misconception misrepresentation 
 country . , instance , produce 
 rapidly , occasionally ambitious imitation 
 vast unapproachable genius Beethoven , 
 concede . grant , , 
 large work diffuse , occasionally laboured ; 
 , exception , , late pro 

 duction , scarcely find hand 

 lication popular form — large number similar , 

 equal , merit , remain alike - production 
 publisher find expedient . begin 
 song , thirty include selection 
 — compare similar pro- 
 duction Beethoven ( incomparable 
 " Adelaida " ' stand kind ) , Schubert 
 Mendelssohn — word , great 
 class . song Schumann refer 
 setting lyric popular german 
 poet — Heine — original text 
 edition english translation , 
 accomplished pen Mr. John Oxenford . 
 impossible specify piece - pro 
 duced ; point specimen justification 
 opinion . " Der Nussbaum , " 
 graceful phrase vocal melody echo pianoforte , 
 support delicate sprinkling arpeggio chord ; 
 " Sonntags Rhein , " strain pure religious 
 beauty , masterly accompaniment , simple 
 , fail impressive charming 
 imperfect unsympathising interpretation . 
 similar earnest beauty " den Sonnenschein , " 
 melody accompaniment simple asa hymn tune , 
 bearing unmistakable impressof poetical genius . " Wid- 
 mung " ( ' * devotion " ) , elevation 
 beauty , high degree elaboration . accom- 
 paniment song , alternation arpeggio 
 chord , admirable specimen rich framework 
 master surround vocal melody . 
 " Abendlied " hymn expressive feeling 
 thankfulness poetical religious mind 
 conceive close peaceful - spend day . 
 theme use composer 
 set pianoforte duet ( op . 82 ) , " Fiir grosse 
 und kleine Kinder . " suppose 
 song character — 
 light vein " Der arme Peter , " " Die beiden 
 Grenadiere , " " ' Trinklied , " & c. value lieder 

 Beethoven , Schubert , Mendelssohn , 
 infect unreasoning prejudice Schumann , 
 increase intellectual enjoyment 
 knowledge volume , content 
 know 

 second Album refer consist forty- 
 small piece , avowedly compose young pianist . 
 piece , , small extent 
 duration — like minor pianoforte piece Beethoven 
 Mendelssohn , impress dis- 
 tinctness character completeness thought 
 master compress small compass . 
 piece illustrative special train 
 thought indicate distinctive title attach — 
 early simple 
 play mere beginner , demand , 
 power execution , appreciative perception 
 style character music 
 real significance worthily interpret . , 
 song , indicate 
 beautiful piece collection . . 10 offer good 
 illustration absurd popular misconception Schu- 
 mann composer , style turgid , gloomy , 
 incoherent . little piece cheerful 
 grace specimen Haydn genial mood — 
 child play , profound musician listen 
 delight . . 19 , 20 , 22 , 24 , 25 , 28 , 30 , 
 lieder ohne worte Mendelssohn write . 
 . 12 , 23 , 29 , 31 , 37 , 38 , 39 , exquisite little 
 picture romanticism ; . 41 vigorous Volkslied , 
 chorale ( . 42 ) , simple , shew 
 figurative inner accompaniment Schumann 
 earn student old Sebastian Bach 

 forest Scenes " series pianoforte piece 
 great length important 
 precede movement , distinctive 
 title . , , somewhat elaborate 
 construction , intend mature player . 
 exquisite beauty variety character 
 piece surprise know Schumann 
 prevalent adverse criticism . airy 
 mystery breathless apprehension express . 4 , 
 « Verrufene Stelle " ' — freakish capricious grace 
 . 6 , entitle ( somewhat unintelligibly , admit ) 
 ' " Vogel als Prophet " ( Prophetic Birds ) , refinement 
 tender expression final " Abschied ; " 
 vigorous elastic energy . 8 , " Hunting Song " — 
 , charming characteristic piece 
 specify , sufficiently prove , compass 
 - shilling book , Schumann possess high poetic 
 faculty real musical genius 

 iss Frankford , Miss Cox , Miss Jenny Pratt , Mr. A.|to member ; instrumental accompaniment 
 James , Mr. Waterson , Mr. Lawler , select choir ) in-| play , room improve- 
 clude admirable example composers| ment . valuable addition orchestra 
 consideration . Miss Cox sing Spohr " maiden| introduction harmonium , skilfully play 
 bird , " Mr. Southgate play odbdligato clarionet| Mr. G. W. Lyon , Exeter . soloist Miss 
 Evans recently invent orchestrina . Mr.| Sophie Foote , soprano ; Mrs. Charles Michelmore , con- 
 Carder perform taste movement | tralto ; Mr. W -H. Cummings , tenor ; Mr. Drayton , bass . 
 pianoforte sonata Clementi ; movement of|mr . Michael Rice leader band , Mr 

 Beethoven Sonata Pathetique . concerted vocal 
 music sing , especially Dr. S. Bennett trio , 
 " Hawthorn Glade , " Sir H. Bishop , 
 " day retire lamp 

 Curron.—A reading Sophocles ’ tragedy 
 Antigone , Mrs. Scott - Siddons , place Victoria 
 Rooms , Friday evening , 8th ult . , Men- 
 delssohn music , furnish highly intellectual treat 

 second great taste skill . Mr. Powell 
 ' conductor . audience large 
 previous Concerts Society 

 Lrverpoor.—A. second Concert aid 
 fund new organ S. Cleopas ’ Church , Toxteth 
 | Park ( build Messrs. Gray Davison ) , 
 hold Ebenezer Hall , 16th April . choir 
 |S. Cleopas sing selection - song great pre . 
 |cision ; party gentleman glee 
 satisfactory manner . Mr. ‘ Thompson , organist 
 church , conduct Concert ability ; 
 pianoforte solo , Ascher " Fanfare Militaire . " 
 | proceed Concerts respectively 
 £ 49 £ 21.——Tue sixth Subscription Concert 
 |the Philharmonic Society year 1868 , place 
 ‘ 28th April , great interest ; pro- 
 |gramme extremely varied attractive 
 character . principal vocalist Madame Trebelli- 
 Bettini Signor Bettini ; singing 
 ithe evening highly appreciate . principal 
 [ instrumentalist brother Willi Louis Ther , 
 ‘ Pesth , perform delicacy 
 Duet Concerto Mozart e flat ( pianoforte ) , 
 | ' curious unisonal rendering " Etude , " 
 jby Chopin , " Marche Turque , " Beethoven , 
 remarkable feature perfect ensemble 
 performance . chief orchestral work 
 Beethoven Sinfonia , . 7 , play 
 vigour . overture Der Berggeist , Spohr , 
 Zampa , Herold , spirit 
 orchestra usually aceustom audience . choral 
 member Society sing - song 
 G A. Maefarren , Wallace , addition " Night- 
 ingale Chorus , " / omon * bridal Day , " 
 William Tell —— Tue Societa Armonica hold open 
 rehearsal 2nd ult . , perform , ina 
 creditable manner , Symphony D Ries , work 
 know amateur . ' programme in- 
 elude " e ouverture , " Nicolai , " Adagio , " 
 Mendelssohn , march Athalie 
 composer . ' t'wo song contribute Miss Fanny 
 Armstrong , Mr. Armstrong , usual , conductor 

 Lonesigut.—On Monday evening , April 27th , 
 performance sacred music Longsight 
 Independent Chapel , choir number 30 voice . 
 programme consist entirely 
 selection Judas Maccabaus . chorus 
 . second anthem , 
 consist fugue subject , cleverly work , 
 compose Mr. C , B. Grundy , talented young 
 organist Oxford Koad Wesleyan Chapel , 
 render . organ playing Mr. Lowe 
 exceedingly good . accompaniment judiciously 
 perform , " o , lovely peace , " 
 especially satisfactory . rendering / legretto 
 movement Symphony " hymn praise , 
 organ solo programme , charac- 
 terise great taste . performance conclude 
 * HaJlelujah Chorus 

