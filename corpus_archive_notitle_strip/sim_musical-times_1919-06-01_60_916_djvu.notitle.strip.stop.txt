Interludes 

 Musical Times February , 
 making timid débit heretic regard 
 departments Beethoven work , remarked 
 ‘ devout Beethovenite lookin 
 round missile . ’ devotees lately 
 showered verbal bricks , 
 risk tedious , endeavour hand 
 . far concerned , 
 subject drops 

 , word reply Dr. Froggatt . 
 said seen claim 
 forward Beethoven failure 
 song - composer , stated fact . 
 quotations alter fact , 
 instruct modify opinion . ‘ 
 extracts Parry Ritterdo . Failure 
 course comparative term , success 
 smaller man failure Beethoven . 
 swallows needed 
 summer , Parry remark 
 ‘ Beethoven times actually attained 
 ideal modern song ’ sufficient 
 establish Beethoven song - composer 
 sense Schubert , Schumann , Brahms , Wolf , 
 Grieg , Parry , Stanford , good , 
 song - composers . Ritter positive , 
 remember history written 
 half - - century ago , intervening 
 years seen good deal winnowing , 
 especially far songs concerned . 
 Dr. Froggatt pained shocked 
 daring express opinion having 
 read Parry ‘ Summary ’ Ritter ‘ History . ’ 
 cheerfully plead guilty 

 opinion songs , right wrong , 
 . based examination music , 
 echo somebody judgment . 
 excellent thing art 
 pronounced opinions - hand . 
 plenty mistakes , 
 balanced 
 scrapping vast pile old middle - aged 
 music owes vogue lazy habit 
 opening mouth shutting eyes , 
 taking German classics send . Mean 
 , opinion backed hard fact 
 Beethoven orchestral , chamber , 
 pianoforte works constantly performed , 
 songs rarely heard . writing 
 Saturday , 10 , order verify , | 
 turn page Daily Telegraph 
 date . dol find ? page contains 
 programmes vocal recitals , Mesdames 
 D’Alvarez , Muriel Foster , Melba , Astra Desmond , 
 Megan Foster , Mr. Kingsley Lark , 
 song Beethoven performance . 
 page gives programmes concerts 
 Beethoven Festival Queen Hall . 
 Beethoven successful song composer 1s 
 single example work 
 field included scheme ? turn 

 purest form English folk - song 

 273 

 R.A.M. Syllabus current year , 
 note ( surprise ) thirty - 
 songs set L.R.A.M. candidates 
 Beethoven . academical institutions 
 rule lacking loyalty classics , 
 omission significant . Facts , like women , 
 stubborn things . far 
 convincing Herr Ritter said 
 years ago . Beethovenites 
 misgivings , enthusiast Mr. Pitcher 
 said letter month Musical Times 
 ‘ world knows Beethoven 
 song composer 

 ‘ world , ’ proved 
 letter signed ‘ Ein Fest Burg . ’ correspondent 
 point , think . says 
 condemnation Beethoven songs ‘ revival 
 old cry , indicate return 
 mental inertia mid - Victorian era . ’ 
 mid - Victorian era 
 capable appreciating sentiment style 
 songs . sung , 
 failed completely 
 imagined . ‘ Ein Fest Burg ’ thinks 
 difficult popular , suggest 
 knows little type modern 
 song favour recitals . , 
 Beethoven child play singer 
 accompanist 

 Ein Fest Burg ’ asks critical analysis 
 dozen selected songs . analysis need 
 space spared , 
 little interest average reader . man work 
 particular field judged . 
 composer Beethoven standing writes 
 seventy songs , expect find dozen 
 able bear strict scrutiny . 
 reason faith — unfaith — , 
 allowed works 
 bulk fail . Instead examining 
 dozen mentioned ‘ Ein Fest Burg , ’ 
 waded seventy , best 
 composer fame , 
 judging music merits . ‘ examina- 
 tion confirmed poor impression songs 
 ine - years ago , , 
 expectation , bought volume . 
 days ardent young Beethovenite 
 ‘ Ein Fest Burg ’ wish meet , 
 remember disappointment . taking 
 works afresh , thing strikes 
 extraordinarily uninteresting vocal . 
 blame modern composers unvocal , 
 unpleasing , rule charge 
 writing uninteresting voice parts . ‘ 
 generally present kind problem 
 singer , vocal line 
 appears unsatisfactory sung 
 charged significance 
 taken conjunction accompaniment . 
 ‘ accompaniment ’ lack better word . 
 best songs duets voice 
 pianoforte . ‘ singer 

 8 

 ee ee ee 
 — - — = po et fe 
 clouds shines . sun 
 worse example occurs 

 Liederkreis ’ — usually regarded composer 
 best work song - writer . . 2 find 
 second verse — thirteen bars 
 - — set monotone , pianoforte 
 plays tame series tonic dominant chords . 
 Beethoven meant subtle , 
 effect miserably weak 

 melodies constructed 
 arpeggios common chords . extract 
 ‘ Neue Liebe , neue Leben , ’ Op . 75 , . 2 

 Ex.3 . Andante espressivo . 
 ee 
 Dry , dry , 
 Andante espressivo . fee 
 fe — = — — 
 — _ + _ © ea , — _ ) | — — — — — = 
 Fee ee Se 
 | SS 
 7 ee _ _ _ _   — e 
 = — } — _ _ _ — _ _ } -___F ee om EE . eee . 
 = ¢ — _ SS — _ _ _ — _ — _ _ _ — — . — f 
 = fs — = = — — — _ 

 strikes convincing setting 
 ‘ Dry , Tears love everlasting . ’ 
 Beethoven pleased little scale 
 ( represents tears ) ends 
 song 

 Ex , 4 

 r Dry ! siete 
 | 
 : = = = } 2S = f 
 4 SS SS SE 
 oe ae 

 Ein Fest Burg ’ evidently likes sort 
 music . use . 
 curious thing feeble writing 
 dates Beethoven best period . 
 , composing songs , considered 
 scrap passage - work series notes 
 good . passages 
 extent justify 
 laid regard sonority . 
 keyboard writing generally 
 elementary description . favourite 

 Adelaide ’ spoilt complacencies 

 right hand doubling voice , 
 left ‘ tum - tums 

 easy fill pages 
 journal similar examples writing worthy 
 Hummel Hiller , Beethoven 

 uncongenial task , dwelling 
 weaknesses great man work , merely 
 draw attention points . , far 
 cases Beethoven content 
 music duty verses , regardless 
 fitness . chiefly fact 
 bulk songs type 

 _ 

 popular Germany — kind half . 
 way house folk - song moder 
 song . Hiller - rate men turned 
 things score . straightforward tune , 
 stock phrases folk - song , 
 rhythmical modal peculiarities — 
 melody . accompaniment 
 modest unconventional , setting du 
 number verses . odd man 
 develop instrumental forms 
 little song . rarely 
 realise importance making 
 music faithfully reflect sentiment verse , 
 easygoing methods respect 
 marked contrast vivid touches Bach 
 vocal writing , especially cantata solos . Note 
 Bach habit seizing salient words 
 ‘ death , ’ ‘ bitter , ’ ‘ sighs , ’ ‘ tears , ’ ‘ trust , ’ ‘ joy , ’ 
 ‘ sleep , ’ increasing significance 
 striking harmonic melodic feature . Compare 
 accompaniments Beethoven , 
 older music far 
 modern 

 irritating habit Beethoven 
 repeating line verse , chiefly order 
 round musical form . ‘ result usually 
 anti - climax 

 chief reason Beethoven 
 failure song - writer lay fact 
 genius dramatic epic lyrical . 
 , peculiar excellence Beethoven — 
 skill development — finds little 
 scope smaller vocal forms . insig- 
 nificant phrases songs , occurred 
 big instrumental work , doubt 
 developed splendid effect . form 
 confines narrow , spontaneity 
 vital importance , Beethoven spread 
 . ‘ modern song established years 
 later composer equipment 
 opposite lines . Schubert little skill develop 
 ment , Beethoven lacked — 
 inexhaustible fund lyrical invention 

 Ein Fest Burg ’ request list songs 
 good dozen selections Beethoven 
 hardly taken seriously . 

 find scores finer examples Schubert , 
 ‘ . | Schumann , Brahms , Wolf , Dvorak , Grieg , Strauss , 
 | Debussy 

 Mallinson , Parry , Stanford , Ireland , 
 Frank Bridge , Martin Shaw , Moussorgsky , Cui , 
 Tchaikovsky , Charles Wood — list 
 longer — fear 
 help . Purcell furnish 
 handful fresher vital 
 Beethoven . ‘ laid earth , ’ 
 ‘ knotting song , ’ ‘ Nymphs Shepherds , ’ 
 ‘ Fairest isle , ’ ‘ attempt love sickness 
 fly , ’ ‘ shall love 
 , ’ ‘ Arise , ye subterranean winds , ’ ‘ O let 
 weep , ’ ‘ Muses , bring roses hither , ’ ‘ Come , 
 dare , ’ , Dr. Froggatt 
 ‘ Ein Fest Burg ’ seventy 
 Beethoven keeps 

 ee ee ae 

 275 

 Ein Fest Burg ’ convincing 
 takes task orchestral question . 
 songs matter taste . 
 Burg ’ likes kind music quoted , 
 evidently kind thing likes . Far 
 grudge simple pleasures . 
 taste - day , . 
 assume reasons liking 
 songs , disliking . 
 point matter arguable . 
 comes comparison modern 
 orchestra Beethoven day , 
 dealing facts . condemnation 
 ‘ vulgarity ’ modern orchestra left | 
 speak . gives away ) 
 completely left 

 Mr. Cooper letter calls words . ) . 
 need point - scoring } 
 necessarily lead music noisy . } 
 matter fact , result | 
 reverse . example , passage -quoted 

 Finale seventh Symphony , scored | 
 Beethoven , decidedly noisy , reasons 1 
 suggested - scoring 

 gave 

 ‘ Ein Fest | 7 

 strident . expression ‘ jolly good row ’ 
 applied section Beethoven marked 
 want tremendous power , 
 employ unusual direction ? Mr. 
 Cooper , like ‘ Ein fest Burg , ’ think 
 ‘ power ’ ‘ vulgarity ’ synonyms 

 , criticisms Beethoven 
 songs suggestions - scoring 
 certain passages orchestral works 
 forth powerful arguments 
 correspondence far received , 
 heretical way hardened unrepentant 

 FESTE 

 P.S.—Bearing mind indignation Dr. 
 Froggatt expressed suggestion 
 Schers ? Beethoven Symphonies 
 played detached works , 
 interested following item programme 
 | Beethoven Festival concerts 

 PIANOFORTE SOLOs 

 succeed , poetry best unsur- 
 passable tonal grandeur emotional beauty , 
 matched rhythmic balance verbal nuance 
 Bible works ‘ Religio Medici , ’ 
 equalled general power moving force 
 grander passages music . Whitman , 
 , pre - eminently musician poet ; 
 think eventually art inexhaustibly 
 influence music larger forms lyric poetry 
 hitherto influenced smaller 

 describe Whitman musicianship analyse 
 ‘ philosophy ’ music great matter 
 undertaken . describe musical 
 experiences history music 
 New York 1835 1861 . impossible 
 moment . suffice 
 exceptional opportunities hearing music 
 observing musicians audiences ( far music 
 publicly practised New York 
 Europe generally rise Wagner ) 
 sixteenth - year . brief 
 exceptions placed newspaper man 
 printer free entry public 
 performances music New York 
 Brooklyn year 1861 . 1862 
 1873 military hospitals 
 Government Offices Washington ( town 
 books music ) , 
 1873 year death ( 1892 ) 
 invalid Camden gentle traveller West 
 North . observed military music 1862 
 1865 , natural sounds — particularly songs 
 birds — years 1875 1879 thereabouts . 
 occasion served , went concerts 
 years invalidism ( 
 February 11 , 1880 , heard concert 
 Philadelphia , Beethoven Septet ) . mind 
 musically formed fixed new 
 modern music Liszt Wagner entered 
 America , likely circumstances 
 brought music life fully 
 thirty years brought , 
 changed times . Writing 
 1891 says : ‘ experts musicians 
 present friends claim new Wagner 
 pieces belong far truly ’ Italian 
 composers Italian operas 1800 - 50 , ‘ 
 , likely . fed bred 
 Italian dispensation , absorb’d , doubtless 
 . ignorant 
 developing music later years , 
 unsympathetic . ‘ Poetry - day 
 America , ’ written 1881 , quotes 
 passage musical writer shows 
 adequately understood older Italian music 
 formed newer French , German , 
 Italian music escaping — ‘ music 
 present , Wagner , Gounod , later 
 Verd . , tends free expression poetic 
 emotion , demands vocalism totally unlike 
 required Rossini splendid roulades , Bellini 
 suave melodies . ’ opera 
 influenced — Italian opera , French Auber , 
 German Flotow , English semi- 
 ballad type . Instrumental music effect 
 . ‘ Absolute ’ music moved 
 slightly ; cou d decisively 
 exhaustive consideration 
 implied musical references 1890 prose 
 passage entitled ‘ Death - Bouquet 

 Whitman deeply permanently 
 affected definite movement tangible life 
 opera , received ‘ magnetic , electric thrill ’ 
 music essential chiefly 

 MISTAKEN PRONUNCIATION , Ere 

 S : k,—Your correspondent Manchester 
 number Musical Times makes mistake saying 
 Catalani opera ‘ La Wally ' , naming , 
 pronounced ‘ Vallée ’ French . pronounced 
 * Vallee ’ ( course accent ) English . 
 called Italy . wonder Mr. O'Mara 
 come comiposer best opera , 
 superior way discussion ? refer 
 * Loreley . ’ dramatic libretto music , 
 containing beautiful vocal melody 
 charming ballet music frequently included high- 
 class orchestral concerts . scope 
 beautiful scenic effects . given times Italy 
 ‘ La Wally . ’ Alluding discussion going 
 paper Beethoven song - writer , surely 
 * Feste ’ speaks great Bonn master ‘ 
 failure song composer ’ heard Beethoven 
 Creation Hymn , ‘ Ah ! Perfido , ’ fine ‘ questa 
 tomha , ’ scarcely think ! — , & c 

 CLAUDE TREVOR 

 BEETHOVEN 

 51r,—I know stimulating writer musical 
 matters ‘ Feste , ’ 
 agree . month look forward perusal 
 remarks , rarely find 
 makes think . April 
 ‘ Interludes ’ disagree , 
 deep penetrating truths expressed 
 command respect . regrets 
 thought fit excuse temerity appealing 

 article raises important question 
 taste . literature , man takes delight reading 
 drama , essays , lyrical poetry , 
 biographies , blessed man finds pleasure 
 . rare man ! 
 find fault taste devoted line ; 
 taste perfect special department 
 interested . forgotten music 
 , , expressive temperament 
 literature , consequently music 
 best appeal , taste 
 fault found , simp'y temperaments 
 composer listener compatible 

 doubt , , changes time . , 
 instance , change political opinions ? | 
 held years ago ; experience altered views . 
 frankness convinced 
 experience . music . man ought 
 develop life , year ought able 
 mark different year . 
 way life rich , pity 
 dead precluded reviewing year enthusiasm . 
 year find devouring music Byrd 
 Farnaby , year specially enthusiastic modern 
 Britons , year hope lunatic 
 group . reached stage tire 
 Beethoven bored Haydn Mozart , 
 ecstasies Purcell , hope 
 shall . , , kaows 

 Feste ’ hits root matter . 
 fashions art , fashions entirely artificial 
 pundits believe . time time 
 great movements thought leave 
 entirely unaffected expression tastes . 
 20th century prevalent great dissatis- 
 faction complicated social system , product ofa 
 mechanical age . People filled aspirations 
 know define hopes vague chaotic . 
 , looking Russian art — literature 
 music — do¢s perceive foreshadowing anarchist 
 revolution ? observe symptoms oi 
 chaotic emotional outburst , somewhat disordered , 
 arts — literature , painting , music alike ? 
 art good , point unsentimental time 
 verdict 

 April 13 , 1919 

 S1x,—May venture challenge statement 
 ‘ Feste ’ effect Beethoven failure 4 
 song composer . revival old cry indicate 

 return mental inertia mid - Victorian era — 
 songs easy , good 

 leads ask , ‘ Feste ’ studied Beethoven 
 songs , sung played — studied 

 ters 
 — 
 “ nthe 

 299 

 sonata symphony , reveal beauties ? 
 studied , right dogmatise 
 subject , studied , ask 
 formulate critical analysis , showing songs 
 failures . render task difficult , 
 list songs Beethoven , ask ‘ Feste ’ 
 reasons failure achieve greatness , 
 parallel list songs composer 
 reach high standard : ‘ die Hoffnung , ’ Op . 12 ; 
 ‘ Adelaide , ’ Op . 48 ; ( Cycle ) ‘ Sechs Lieder von Gellert , ’ 
 Op . 48 ; ‘ Mignon , ’ Op . 75 , . 1 ; ‘ Wonne der Wehmut , ' 
 Op . 83 ; ‘ Der Wachtelschlag ’ ; ‘ Abendlied 

 Symphonies , surely ‘ Feste ’ 
 wishing exchange austere purity Beethoven 
 scores vulgarities modern orchestra ! fact 
 fever modern life revealed modern orchestral 
 ( ) music prove feverish state 
 preferable health . Beethoven achieved colossal effects 
 ( spiritual aural ) limited orchestra . Strauss 
 achieved colossal noise dint straining resources 
 modern orchestra 

 ‘ Feste ' desirous arguing noise synonymous 
 power , humbly suggest - score bars 
 Symphony . 5 timpani G E 
 flat . surely colossal noise , sole raison - d'étre 
 modern orchestral works 

 E1n Fest Bure 

 S1r , — ‘ Feste ’ entirely missed point letter , 
 dealt , question opinion , 
 fact . stated ‘ seen claim 
 forward Beethoven failure song 
 composer . ’ gave quotations contrary books 
 Ritter Parry — books ‘ Feste ’ ought 
 acquainted . reply , gives extract book 
 Sir Henry Hadow , prove poor 
 opinion Beethoven composer songs . ( second 
 quotation serve mistaken purpose . ) 
 opinions , case 
 better . man unacquainted , 
 forgotten , writings - known historians 
 Ritter Parry , unwise makes sweeping 
 assertion above.—Yours , & c 

 5 , Richmond Mansions , ARTHUR T. FrocGatr 

 Denton Road , Twickenham , 
 4 , 1919 

 Sirk,—In humble opinion Beethoven controversy , 
 ‘ Feste ’ conducting ably , significant musica ! 
 event . Sooner later going inestimable value 
 cause music , hope wili let 
 drop soon . quarrel ‘ Feste ’ lenient 
 Beethoven matter limitation instruments 
 time , reason : Surely 
 astounding fact musical history Weber 
 died Beethoven . Weber limitations 
 imposed Beethoven , think 
 orchestral works need - scoring . 
 opening ‘ Oberon ’ Overture , instance , sounds 
 modern - day played . 
 ‘ Feste ’ discuss point future article , 
 oblige ? — , Xc . , Rosert E. Lorenz 

 14 , Craven Hill , W.-2 

 Sir , — ‘ Feste ’ wishes - score Beethoven , 
 rate - scored—‘a matter letter versus 
 spirit ’ : says , ‘ care passage 
 stands ; obviously miscalculation 
 Beethoven . Let - score symphonies — bring 
 line modern orchestral requirements ; let 
 spirit Beethoven letter 

 Unfortunately spirit Beethoven elusive . 
 sure result titivation 
 spirit ‘ Feste ’ Sir Hugo Burls 
 learned collaborators 

 , going ? adding instrumental 
 parts ? — increasing volume sound ? — making 
 ‘ jolly good row ’ ? , ‘ spirit Beethoven ’ 
 produced sacrificing Jumbo 

 know sad fate Bach . day week 
 hear pianist straining muscle body 
 thumping thundering ‘ Bach - - piano- 
 forte arrangement , ’ plodding 
 Preludes Fugues approved bag - pipe manner — 
 graces , sz / ences d’articulation , 
 appoggiature left played short , , 
 doubtless , arrangers editors thought 
 reproducing spirit Bach 

 Grosvenor Square 

 unable print letters received 
 subject . majority writers decidedly 
 views suggestions ‘ Feste . ’ opinions , 
 , carried weight 
 apparently impression best way 
 defend Beethoven attack modern composers 
 modern orchestra . requests 
 continuation discussion , view present 
 great musical activity , consequent demand 
 space , close . ‘ Feste ’ deals 
 letters ‘ Interludes . ’.—Ep . , A/.7 

 Obituary 

 early months 1914 London 
 acquainted Mr. Albert Coates leading personality 
 English musicians . came Russia conduct 
 Covent Garden Winter season opera German — 
 memorable season brought ‘ Parsifal ’ London . 
 mastery conductor art evident 
 Royal Opera Syndicate engaged following 
 Summer season , conducted German Italian 
 opera . beginning war found Russia . 
 short account Mr. Coates life work given , 
 portrait , issue April , 1914 

 glad welcome Mr. Coates England 
 protracted trials war - time . - introduced 
 concert familiar music Queen Hall April 29 , 
 Beethoven seventh Symphony principal work 
 hand . doubt mastery 
 orchestra ability draw meaning 
 music , room question — recurred 
 ‘ Siegfried Idyll’—on subject semi . 
 prone dwell lovingly passage slow 
 music bent squeezing dry allowing 
 natural flow . Tchaikovsky * Romeo 
 Juliet ’ Mr. Coates asserted gifts loophole 
 questioning . great interpretation , finely designed 
 eloquent . Atthe second concert , 8 , Mr. Coates 
 conducted Tchaikovsky ‘ Pathetic ’ Symphony 
 special insight expect , Wagner 
 excerpts , Saint - Saéns ‘ Africa ’ Fantasie , Miss 
 Hilda Dederich solo pianist . concert , given 
 Mr. Coates 16 , devoted Russian music . 
 Tchaikovsky loomed largely scheme , 
 represented ‘ Romeo Juliet ’ B flat minor 
 Pianoforte Concerto , played Miss Katharine Goodson , 
 interesting hear Rimsky - Korsakoff Suite 
 ‘ Legend Tzar Saltan ’ invigorating rhythms , 
 Scriabin ‘ Poéme de l’extase . ’ work 
 heard London , memory serves 
 correctly , strong impression richly 
 woven design . Intervening years enabled perceive 
 strands texture clearly , know 
 harmonies , considered exotic , 
 typical Marylebone Kensington . skill 
 Scriabin weaving open 

 instruments — double - bass , violins , flute , clarinet , horn 

 new Violin Sonata ( Op . 165 , . 2 ) , Sir Charles 
 Stanford played Miss Murray Lambert Mr. 
 Hamilton Harty Wigmore Hall 7 , proved 
 characteristic composer merit , tending 
 unusually simple style 

 Mr. Frederic Lamond great popular gifts 
 displayed ( Queen Hall Beethoven 
 * Emperor ’ Concerto Tchaikovsky B flat minor . 
 gratifying observe British - born pianist , 
 makes appeal strength purely - musical value 
 interpretations , display , big following , 
 attract large interested audience . 
 accompanied occasion ( ueen Hall 
 Orchestra , Sir Henry Wood , conducted 
 performance Bantock characteristic picturesque 
 tone - poem , ‘ Fifine Fair 

 new Violin Sonatas , E. J. Moeran Miss Mary 
 Barber , performed Wigmore Hall 10 
 M. Edgardo Guerra , accompanied composer , 
 occasion joint recital M : José de Moraes ( vocalist 

 CAMBRIDGE 

 Messrs. AlLert Sammons William Murdoch gave 
 recital Guildhall 7 crowded house . 
 programme consisted Bach Violin Pianoforte 
 Sonata E major , Beethoven ‘ Kreutzer ’ Sonata 
 Romance G , Mozart Rondo G , pieces Debussy . 
 week concert University Musical Society 
 takes place June 6 , items performed 
 Bach ‘ Sing ye Lord , ’ ‘ Emperor ’ Concerto 
 Beethoven , ‘ Wasps ’ Overture Vaughan Williams , 
 ‘ L'Aprés - m di d’un faune , ’ Debussy 

 membership Musical Club continues increase , 
 committee contemplating acqui - ition larger 
 premises . weekly concerts held usual , 
 standard programmes keeping 
 traditions Club 

 choir voices formed G.W.R. men 
 Newport , sang enthusiasm , rich tone - quality , 
 fine precision Exeter , 10 , Mr. W. H. Bryant 
 conducting 

 Mr. Vladimir Cernikoff , Torquay , April 16 , played 
 pianoforte music Bach , Schumann , Beethoven , Liszt , 
 Chopin , introduced Cradle Song ‘ Scaramouche ’ 
 Holland . Madame Alice Montague showed special skill 
 songs Purcell . Easter week found band 
 R.M.L.I. Plymouth giving days ’ season 
 Pavilion , Mr. P. S. G. O'Donnell conducting . 
 concerts given Miss Margaret Cooper party , 
 Madame Melba Miss Katharine Goodson gave vocal 
 pianoforte recital Saturday afternoon 

 organ recital Budleigh Salterton 3 , Mr. 
 F. J. Pinn played Andante Variations Rea , 
 music Cooke , Widor , Callaerts , Silas , Merkel . Mr. 
 S. J. Bishop sang beautiful Prayer Kalinnikov , 
 air ‘ 

 LEICESTER.—An exacting programme adopted 
 Mr. Vincent Dearden Ladies ’ Choir concert 
 Edward Wood Hall 5 . included 
 Coleridge - Taylor ‘ green heart waters ’ 
 ‘ lambkins ? ’ Vaughan Williams ‘ Sound 
 sleep , ’ Colin Talyor ‘ Slumber Song Madonna , ’ 
 Percy Fletcher ‘ Follow Carlow 

 NorwicH.—The Philharmonic Society brought _ 
 series concerts successful conclusion 
 present season Thursday , 8 . chief attraction 
 Elgar Violin Concerto , heard 
 time Norwich . Albert Sammons soloist , 
 played magnificently . ably supported 
 orchestra . work splendid reception 
 crowded audience . orchestral works 
 Tchaikovsky fifth Symphony Beethoven ‘ Egmont ’ 
 Overture . vocalist Miss Minnie Searle , sang 
 Mozart ‘ Non mi dir ’ ‘ Don Giovanni , ’ group 
 songs pianoforte accompaniment complete success . 
 Mendelssohn ‘ Hymn Praise , ’ given evening , 
 marked fine sirging choir . Miss Searle , Mr. 
 A. E. Benson , Chorister Wilson soloists , 
 work received enthusiasm audience . Dr. 
 Frank Bates conducted concerts 

 SurroLk — multitude military East Anglia 
 war , remained musical talent busily 
 employed , expected energies 
 choral socigties musical bodies remained dormant . 
 aspect war ended , evidence 
 lacking general awakening , Autumn find 
 enthusiasts alive . Spring 1915 - known 

 COSMOPOLITAN AUDIENCE 

 Mr. Frederick Kitchener gave pianoforte recital 
 Shepherd Hotel , Cairo , April 27 , playing Prelude 
 Fugue Bach , Beethoven C sharp minor Sonata , selection 
 Schumann ‘ Carnaval , ’ studies Chopin , 
 pieces Moszkowski , Paderewski , Cyril Scott.and Liszt . 
 audience included Sir Edmund Allenby Lady Allenby , 
 French , American , Spanish Ministers . 
 audience 160 nationalities — 
 English , Australian , American , French , Italian , Spanish , 
 Greek , Russian , Danish , Egyptian , Turkish , Armenian , 
 Albanian , Syrian , Jewish . programme printed 
 French . chance Esperanto 

 announced Carnegie United Kingdom Trust 
 connection music publication scheme 
 - works sent year compared 
 seventy - year , quality variety 
 works submitted encouragingly good . adjudicators 
 unanimously recommend following publication : 
 Rhapsodies String Quartet , George Dyson ; 
 * Hound Heaven , ’ solo baritone , chorus , 
 orchestra , William H. Harris ; ‘ Hymn Jesus , ’ 
 chorus orchestra , Gustav Holst ; Sextet 
 Strings , G minor , P. H. Miles ; Sir C. V. Stanford 
 Symphony . 5 , ‘ L ’ Allegro ed il Pensiercso 

 Majesties King Queen Majesty 
 Queen Alexandra graciously signified willingness 
 accept copy ‘ Westminster Pilgrim 

 Beethoven Festival Queen Hall , 19 - 24 , 
 occurs late notice month . account 
 appear July number 

 Hnswers Correspondents 

