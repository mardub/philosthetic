Arranged for the Organ or Harmonium by JOHN OWEN. 
Each Part One Shilling, net. 
PART I

Andante (Heller), Lied (Mendelssohn), Schlummerlied (Schumann), 
March (Schumann), Andante (Oesten), Andante Moderato(Schumann), 
Andante Cantabile (Beethoven), Allegretto (Mendelssohn), Traimerei 
(Schumann), Andante (Beethoven), Wedding March (Mendelssohn

PART II

Angels ever bright and fair (Handel), Lied (Mendelssohn), Andante 
(Russell), Adagio Cantabile (Beethoven), Cujus Animam (Rossini), 
Faith (Glover), Hope (Glover), Charity (Glover), Sanctus (Bartniasky), 
Caprice (Blumenthal), Gloria, from Twelfth Mass (Mozart

To be continued

No. 51. 
pa

given in its complete form, and leads to an Andante 
in B flat minor, ‘* Requiem xternam,” which allots to 
the voices precisely the music, with some expansion, 
heard at the beginning of the Mass from the orchestra 
(see Nos. 2 and 3). The device is a happy one, 
and secures unity in a most effective way. With 
this it may be supposed that Verdi ends, as Beethoven 
closes his Mass in C, but once more the recitative 
prayer is heard, leading to an Allegro risoluto (C 
minor), which takes the form of a fugue upon the 
following subject

No. §4. 
_Allegroris

HER MAJESTY’S OPERA

Mr. MAPLeEson seems resolved to support his character 
as a zealous upholder of high class works, for he opened his 
season on the roth ult. with Beethoven’s ‘ Fidelio” 
(Madlle. Tietjens sustaining the part of the heroine with 
even more than her usual effect), and has definitely an- 
nounced his intention of devoting an evening in each week 
to the performance of an Opera by one of the great classical 
composers. Signor Bignardi, who made his début on the 
occasion, produced but small effect in the music of Florestan, 
but it is possible he may do better in parts of less pretension. 
Madlle. Elena Varesi’s performance of Gilda, in “ Ri- 
goletto,”” proved that she possesses a really good voice 
which has been carefully trained, and there is every reason 
to believe that she will be a valuable acquisition to the 
company during the season. The greatest success has 
been created by Madlle. Anna de Belocca, who comes to us 
heralded by the highest encomiums from the Continental 
press. Her singing of the trying part of Rosina, in “ Il 
Barbiere ” proved her to be not only an excellent vocalist, 
but a refined and intellectual actress, and her reception at 
once evidenced how thoroughly she had won the good 
opinion of her hearers. As Susanna, in ** Le Nozze di 
Figaro,” Madlle. Pernini made a promising appearance, but 
judgment upon her merits must be deferred until they have 
been more severely tested. Signor Campanini has already 
appeared in some of his best parts, and Madlle. Singelli, 
who made so favourable an impression last season, has 
returned to the company

PHILHARMONIC SOCIETY

MUSIC IN LEIPSIC. 
FROM A CORRESPONDENT

At the nineteenth Gewandhaus Concert the yearly 
production of Beethoven’s ninth Symphony took place, but 
owing doubtless to the absence of Herr Reinecke, the per- 
formance was not a great success

At the last Concert, in addition to the Eroica Symphony 
and the Athalie Overture, Schumann’s “Concert Allegro” 
was played by the Capellmeister, this being his only 
appearance this winter as soloist at these Concerts

EXCELLENT as are the histrionic powers of Mr. Charles 
E. Fry—who forwarded us tickets for a ‘‘ Shaksperian 
Recital” at the Quebec Institution on the 23rd ult.,—we 
could scarcely have given him a notice in these columns 
had he not so woven in some well chosen musical 
illustrations with his reading as to put in a plea for our 
support. Mr, Fry’s highly dramatic recitation of many of 
the most effective scenes from Shakspere’s plays elicited 
the warmest and most deserved applause from a large 
audience; and we can scarcely doubt that, were he so 
inclined, he might command a legitimate success in an 
arena which would afford him freer scope for the display of 
his versatile talents. The musical artists who appeared on 
the occasion were Mr. E. H. Thorne and Mr. W. H. 
Thomas (pianoforte), who played four-handed arrange- 
ments of Mendelssohn’s Overture to the Midsummer 
Night’s Dream, and a selection from Sullivan’s Tempest 
music, and Misses Emily Spiller and Dones, who gave 
some appropriate vocal pieces, nearly all of which were 
most enthusiastically encored. The Recital was listened 
to with the utmost interest throughout

Tue two Pianoforte Recitals given during the past 
month by Dr. Hans von Biilow, previous to his departure 
for America, were attended by large and thoroughly ap- 
preciative audiences. The programme of the first concert 
was exclusively devoted to the compositions of Chopin, 
and the second was miscellaneous, a marked effect being 
created by his performance of Beethoven’s thirty three 
variations on a waltz by Diabelli

INTELLIGENCE has reached us of the recent death of 
Madame Pleyel, who held a high reputation as a refined 
and intellectual pianist. Her performances were received 
with the utmost favour in England before the love for 
classical works had become so general; and we may, 
therefore, regard her as one of the many pioneers of true 
art to whose teachings we are so much indebted for the 
cultivation of a healthy musical taste in this country

WE are requested to state that the article entitled 
‘Bach in Soho,” reprinted in our last number from the 
Guardian, was from the pen of Mr. John Crowdy, 
occasional musical critic to that paper

THE St. Mary’s Choral Society, Southwark, in connec- 
tion with St. Mary’s, Old Kent Road, gave a concert in 
the School-room, Clarence Street, on the 1st ult. The 
Society, under the excellent guidance of Mr. H. F. Frost, 
the choir-master at the Chapel Royal, Savoy, has of late 
made considerable progress. The choruses included the 
opening of Spohr’s Last ¥udgment, Mendelssohn’s ‘ I 
waited for the Lord,” Beethoven’s “‘ Hallelujah,” and part- 
music by Bennett, Mendelssohn, and Macfarren. The 
programme also contained vocal solos by Miss E. Reimar, 
R.A.M., Miss M. Duval, R.A.M., the Misses E. and H 
Tedd, and Messrs. Milsom, Gerald Gadsby, and W. 
Clifford, and pianoforte solos by the conductor. Mrs. 
Pawsey was an efficient accompanist. The Rev. Wm. 
Hall, vicar, presided

Ir is with much regret that we record the decease, 
during the past month, of Mr. Joseph Williams, the cele- 
brated clarionet player, and one of the Directors of the 
Philharmonic Society. His tone and execution upon the 
instrument which he had made his especial study brought 
him vefy early in life into the highest positions, not only 
in our best orchestras, but in Her Majesty’s private band. 
A clarionet Concerto by Mr. Williams was an important 
feature at one of the Hereford Festivals, and he also pub- 
lished an instruction book for the instrument, besides 
several graceful original melodies. Although in his 80th 
year at the time of his death, Mr. Williams retained his. 
mental faculties to the last, and his loss will be long felt 
by the many who enjoyed the privilege of his friendship

AT the second concert of the British Orchestral Society, 
on the 31st March, Mr. Alfred Holmes’s Symphony in C 
minor, ‘‘ Robin Hood,” occupied a conspicuous place in the

rogramme, and was received with sufficient favour to make 
its composer believe that he has fairly taken his place 
amongst the writers of orchestral ‘‘ descriptive music,” a 
school of composition evidently alarmingly on the increase. 
That there are gleams of talent in this work is undeniable ; 
but the want of defined plan, the commonplace nature of 
the themes, and the absence of colour in the instrumentation 
produced a sense of weariness in the listener. The best 
movement is the Serenata, and the worst the Finale; but, 
throughout the Symphony, as we have already said, there 
are many points which seem to indicate that, were their 
author less ambitious, he might be more successful. Mr. 
Randegger’s Scena ‘‘Saffo” (finely sung by Madame 
Lemmens-Sherrington) is thoroughly dramatic, and full of 
that truth of expression in every phase of the poetry which 
might be expected from the composer of “ Fridolin.” The 
instrumentation is masterly in the extreme, and although 
sufficiently independent, never obtrudes itself to the 
detriment of the vocal part. The applause was as warm as 
it was well deserved, and Madame Sherrington was 
enthusiastically recalled to the platform. At the third 
concert, Spohr’s ‘‘ Power of sound’”’ was well given, and 
Miss Emma Barnett was highly successful in her 
interpretation of Beethoven’s Concerto in E flat

TueE pupils of the North-East London Academy of 
Music gave their annual concert at the Manor Rooms, 
Hackney, on Tuesday evening the 30th March. The pupils 
most worthy of notice were Miss Clements, Miss Hewett, 
Miss Ellen Smith, Miss Lottie West and Mr. C. E. Smith, 
pianists; and Miss Cassell, Miss Rogers, Miss Helling, 
Mr. C. Prickett and Master J. E. West, vocalists

THE pieces in these two Numbers are chosen from sacred 
and secular sources, those from the latter being happily 
likely to conjure up thoughts, wherever heard, of home 
and its influences. There is a pleasant specimen of the 
talent of Charles Wesley, who was uncle of the present 
eminent organist of Gloucester, and was organist of Mary- 
lebone Church in the Marylebone-road when it was first 
opened, in the Pastorale from one of his Voluntaries. For 
how much of the organ distribution the author and the 
editor are respectively responsible may not be surmised ; 
but it is not a good effect, whoever devised it, to double

the bass part with the pedals, an 8th below the left hand, 
so as to have the passage sounding in three several octaves, 
which is here conspicuous. A short Anthem by Attwood 
and an Introit by J. B. Calkin are harmonious in character 
and nicely arranged. A Melody with one of its variations, 
from a pianoforte piece by C. M. von Weber, follows. An 
Abendlied by F. Spindler is divertingly overmarked with 
lines to indicate the progression of the parts, which, if 
they-succeed in diverting the reader’s attention from the 
successions of 8ths and 5ths, cannot disguise their effect to 
the hearer. The charming Allegretto in E, from the Nuits 
Blanches, by Stephen Heller, comes out well in its trans- 
lation for the organ. A Song of Sacrifice, by Beethoven, 
is welcome as a little known specimen of the master. A 
delicious movement from Schumann’s Album fiir die Jugend 
is welcome as one of the best known pieces in that 
delightful work. This is followed by the ‘ Hosanna,” from 
Macfarren’s 52 Introits. One of Beethoven’s settings of 
the poems of Gellert, ‘‘ The praise of God,” will be another 
novelty to many who hear it. Mendelssohn’s charming 
songs without words in E is far more appropriate to the 
organ than it is to Lyte’s poem, ‘‘ Abide with me,” to 
which it has been irreverently adapted—fitted we cannot 
say—and is sometimes sung. The last piece is a Prelude 
and Fugue by Dr. Maurice Greene, the venerated Cathedral 
composer, which is not a most favourable specimen of his 
power. The reprinting of this with one flat too little in 
the signature, according to a custom that is fortunately 
obsolete, is as injudicious as it would be to republish 
Shakspere with the antiquated orthography; it perplexes 
the reader who is accustomed to forms now in use, and, if 
we mistake not, may have misled the editor to omit or to 
insert some accidentals where the propriety of D natural 
or D flat is doubtful. The practical difficulty of all these 
arrangements is within the reach of every player of moderate 
attainments

Organ Pieces for Church use. Sets 1 and 2, by Boyton 
Smith

Transcriptions Classiques. Morceaux de Concours, pour 
le Piano, par Ch. Neustedt

On ty three of the six numbers already published in this 
series have been sent to us for notice; but we presume 
that they may be accepted as fair specimens of the entire 
set. Why No. 2 should be called ‘‘ Sonatine de Beethoven ; 
op. 6,” considering that it is the last movement of the well- 
known Duet in D, arranged as a Solo, we cannot conceive. 
It is true that musicians are aware of this, but students are 
not; and seeing that the title-page informs them that it is 
a * Sonatine,”’ and says nothing about its being originally 
written for four hands, many young players will receive a 
wrong impression regarding the composition which it may 
take years to remove. The other numbers are “La 
Romanesca; Air de Danse du XVIe Siécle,” and the 
“ Allegretto un poco Agitato” from Mendelssohn’s ‘* Lob- 
gesang.” All the pieces are carefully transcribed, lie well 
under the hand, and will be found extremely useful for 
teaching

Three Classical Duets, from Haydn's Celebrated Overture 
in D. Arranged by John Pridham

MEDDLING WITH THE OLD MASTERS. 
TO THE EDITOR OF THE MUSICAL TIMES

Sir,— An “ Old Subscriber” deserves my best thanks for 
explaining what I attributed to the changing mania of 
some modern meddler. Beethoven’s own words settle the 
matter of course; unfortunately I find the question only half 
solved, because it is not stated whether the bar marked

4 @-_F 
ae ae ma Se me SS me whee! oe Se 
pea | foe a | t t

ro Te sana

is to be admitted also, in case the prolongation of rhythm 
is to be taken out. Perhaps-an “old subscriber” would 
be able to explain why there exists such an important 
difference in the following passage from Beethoven’s Sonata, 
‘Op. 81, as printed in an old French edition (Launer, Paris

Launer's edition

Bristo..—A very successful concert was given at the Athenzum 
Hall, on the 6th ult., by the St. Peter’s Church Choir, under the direc- 
tion of Mr. Alfred Brookes, organist. The great attraction of the 
programme was the production of Mr. Arthur Sullivan's dramatic 
Cantata, On Shore and Sea; solo vocalists: Miss Pennington (Mrs. 
W. Quick) and Mr. Kidner (tenor). The careful training the choir 
had undergone was shown in the able interpretation given to the 
many and difficult choruses. Miss Pennington and Mr. Kidner gained 
a fair share of applause. Herr Janeck played the accompaniments in 
excellent style. The remainder of the programme was composed of 
glees and. part-songs, with solos for pianoforte, by Herr Janeck, who 
with Mr. Pomeroy (violoncello) also gave a couple of Sonatas. Miss 
Pennington, Mr. H. J. Dyer, and Mr. Kidner contributed songs, &c

Burstem.—Mr. Arthur Crook, organist of St. Giles, Newcastle- 
under-Lyne, has given a series of organ recitals (varied with vocal 
music) during the winter months at the Town Hall, at which selections 
from the compositions of Bach, Mendelssohn, Smart, Batiste, and 
Wely, and arrangements from the works of Handel, Beethoven, &c., 
have been performed

Bury St. Epmunps.—A concert in aid of funds for the erection of a 
new organ was given by the choir of St. John’s Church, on Tuesday in 
Easter week, under the direction of the organist, Mr. Thomas Smith. 
The choir was assisted by the Rev. E. H. and Mrs. Littlewood, and 
Mrs. Tooley. The concert was a great success, the room being crowded 
in every part.——On the 15th ult., through the enterprise of Mr. Smith, 
Mr. Charles Hallé and Madame Norman-Neruda gave a pianoforte and 
violin recital, at which the celebrated “Kreutzer” Sonata of Beethoven 
bean played. The audience, although a very appreciative one, was not 
arge

CamBripGE.—The University Press Musical Society, assisted by 
several ladies, gave a miscellaneous concert, at the Alexandra Room 
on the 31st of March. Birch’s Merrie Men of Sherwood Forest 
formed the first part of the programme. Mr. Robson, of King’s College 
Choir, sang the music allotted to Robin Hood. The choruses were 
well rendered, especially in the second part, which consisted of part- 
songs, &c, Mr. T. Beale conducted.——On the 16th ult. Mr. Charles 
Hallé and Madame Norman-Neruda, gave a pianoforte and violin 
recital at the Guildhall, before a numerous and appreciative audience. 
The playing of these well-known artists was much admired

CHELMSFoRD.—An evening concert was given in the Shire Hall on 
the r4th ult., by the church choir, assisted by Miss Bolingbroke, R.A.M., 
and several amateurs of the neighbourhood, who kindly gave their

services. Mr. Harold E. Stidolph conducted with his usual skill and 
ability. Auber’s overture to Gustave,as a quartett for two piano- 
fortes, was well played by the Misses Ford, Miss Pilgrim, and 
Mr. Stidolph. Mrs. S. Baker was very successful in ‘‘ Robert, toi que 
j'aime,” and the Rev. W. Blow contributed violin solos. Miss Boling- 
broke’s début before a Chelmsford audience was a marked success. 
The quartett, “‘ Integer Vita ” (Flemming), by Messrs. Harris, Cleale, 
Gepp and Stidolph, was well balanced in tone. Mr. Stidolph, in 
Beethoven’s “ Sonata Pathétique,” proved himself a true artist. The 
part-songs were well sung, especially Sullivan’s “O, hush thee, my babie.” 
Miss Ford ably accompanied the Rev. W. Blow in his violin solos, on 
the pianoforte

ConGcLeton.— Mr. Eyre gave his annual concert.on the 2oth 
March, in the Assembly Room, assisted by an excellent quartett party. 
The executants were Signor Risegari and Herr Witte (violins), Mons. 
Speelman (viola), and M. Auguste Van Biene (violoncello). The high- 
class music selected for performance was thoroughly appreciated by 
the audience. M. Van Biene’s solo on the violoncello (a Sarabande 
and Gavotte, by Popper) was encored. The performance closed with 
Haydn’s celebrated Serenade. Madame Rovina Arnold and Miss Edith 
Clelland were the vocalists

CRrEWKERNE.—A performance of fudas Maccabeus was given by the 
Choral Society on Tuesday evening, the 2oth ult. The principal 
vocalists were Miss Ellen Glanville and Miss Perry (soprano), 
Mr. Mathews and Mr. Price (tenor), and Mr. Cross (bass), all of whom 
acquitted themselves much to the satisfaction of the audience. The 
band and chorus numbered nearly seventy performers, and Mr. Loaring 
the parish organist, conducted with great care and judgment

Dersy.—The Choral Union gave a successful performance of 
Beethoven’s Engedi, and Rossini’s Stabat Mater, on the 13th ult. In 
the Beethoven music Miss Katherine Poyntz, Mr. Cummings, and 
Mr. Henry Pope were the vocalists, and in the Stabat Mater Miss Helen 
D’Alton sang the contralto part. Mr. Neville Cox presided at the organ

Derry.—The Easter services at St. Columb’s Cathedral were most 
effectively rendered by the choir, which has attained to a high state of 
efficiency under the skilful tuition of Mr. Turpin, the preceptor and 
organist of the Cathedral. The anthem from the Messiah was espe- 
cially admired

HuDDERSFIELD.—The Choral Society gave its last concert this 
season on the 16th ult., when St. Paul, was the Oratorio performed. 
The vocalists engaged were Mrs. Hodgson, of London; Miss Cros- 
land, of Huddersfield; Mr. Edward Lloyd; and Mr. H. Rickard, of 
Halifax, a new bass singer, who made a very favourable impression. 
Mr. Rickard is a pupil of Mr. W. R. Eckersley, of Halifax

HicH WycomsBe.—The Choral Society gave its second concert of 
the season on the 7th ult. The first part consisted of secular music, 
the feature being Beethoven’s “ Kreutzer” Sonata for violin and piano- 
forte, played by Messrs. Mountney and Biggin. The second part con- 
tained selections from Samson, several numbers of which were 
encored. “Then free from sorrow” was charmingly sung by Miss 
Hibbert; “Return, O God of Hosts,” by Mrs. Webb; and “Let the 
bright seraphim” by Miss Gilbert. Mr. Mountney, of Chesterfield, 
led the band, and Mr. Biggin, organist of the Parish Church, conducted

JepBuRGH.—A fine organ, built by Messrs. Foster and Andrews, 
Hull, from specifications by Professor Oakeley, Edinburgh, for the 
new Parish Church, was opened on the 3rd ult. The organ is placed in 
a gallery at the end of the church, where it is heard to the best ad- 
vantage. The design of the front is in keeping with the architectural 
style of the church, and is of polished pine varnished; the front pipes 
are tastefully illuminated, and the keys and stops are fitted*up in 
polished walnut. The pedal board is radiating, and all the most 
modern improvements are adopted in the construction of the instru- 
ment. The great organ contains 728 pipes, the swell organ 436, and

the pedal go—in all 1254. The capabilities of this instrument were 
finely displayed under the masterly hand of Professor Oakeley, The 
selections were from Bach, Handel, Haydn, Mozart, Beethoven, Men- 
delssohn, Schumann, Gluck, Merkel, and Oakeley

Lerps.—On Monday, the 5th ult., four vocalists who have associated 
themselves under the title of the “ Yorkshire Concert Party,” enter- 
tained a fairly numerous audience at the Church Institute by selections 
from Wallace’s opera of Maritana.. The party consists of Miss 
Pauline Grayston (Mrs. Taylor), who showed herself to be thoroughly 
at home in the soprano music; Miss Emma Kennedy (contralto), Mr, 
George Nunns (tenor), and Mr. Dodds (bass), with Mr. John Shaw as 
pianist. For four persons to sing through the greater portion of an 
opera was no light task; but the vocalists fulfilled it with considerable 
spirit, and, considering the absence of all dramatic accessories, with 
good effect——Mr. J. W. Syxes gave his third violin recital at the 
Leeds Church Institute, on the 8th ult. The selections were from De 
Beriot, Bach, Spohr, Steveniers, and Sainton. Mr. Sykes sustained the 
interest throughout by his masterly playing, and the instrumental 
music was relieved by Miss Tomlinson’s vocal selections, which were 
as well chosen as they were sung. Mr. C. W. Young accompanied

MANCHESTER.—M. Riviére’s Promenade Concerts have been con- 
tinued with an intelligent enterprise, and upon a scale of liberality 
that entitle the Conductor and the management to great praise. The 
musical selections are, speaking generally, very judicious. M. 
Riviére's orchestra is rich in soloists, but many other high-class pro- 
fessional artists have been engaged, including Mr. Aptomas, the Welsh 
harpist, his brilliant playing rousing the audience to positive enthu- 
siasm. The vocalists have been Miss Blanche Cole, Mr. Thurley 
Beale, Madlle. Bunsen, and Herr G. Werrenrath

MARLBOROUGH.—In order to increase the fund which is now being 
raised to purchase a new organ for the College Chapel, Mr. Bambridge 
has recently given three ey recitals, assisted by Mr. John 
Roberts (vocalist), Mr. J. S. Liddle (violin), and Mr. W. H. Aylward 
(violoncello). Mr. Bambridge played selections from the works of 
Bach, Moscheles, and Heller, and his own Meditation in B flat. 
Beethoven's Trio in C minor, by the three instrumentalists, was rap- 
turously applauded. Mr. Liddle as violinist, was highly successful, as 
also was Mr. Aylward, in his ’cello solo. Mr. Roberts sang several 
songs, which formed an agreeable variation to the performances

Maryport.—The second concert of the Philharmonic Society took 
place in the Athenzum, High Street, on the 7th ult., under the con

Oswestry.—The performance of Samson by the Handel Society on 
the 16th ult. was a genuine success. Mr. Bywater proved an admirable 
tenor. The choruses were exceedingly well sung, and the excellent 
way in which the band accompanied was a feature in the concert. The 
other vocalists were Miss Harriette Leders, Miss Clara Couvane, Mr. 
Chisholm, and Mr. Glave. Mr. George Pugh presided at the har- 
monium, Miss Farmer at the pianoforte, and Mr. Whitridge Davies 
conducted

PatsLey.—A concert of glees, part-songs, &c., was given by the 
Tonic Sol-Fa Institute, in the Abercorn Rooms, on the gth ult. 
Miss Thomson played Beethoven’s Sonata, Op. 31, No. 3 in E flat, 
and a fantasia, ‘‘Ye banks and braes,” and also accompanied. Mr. 
J. A. Brown conducted

ParkGATE.—The Rawmarsh and Parkgate Sacred Harmonic Society

PiymMoutH.—The Vocal Association closed the season with a per - 
formance of Judas Maccabeus, in the Guildhall, on the 16th ult. The 
solo vocalists were Madame Blanche Cole, Madame Alice Fairman, 
Mr. Thurley Beale, and Mr. Bernard Lane. The orchestra was un - 
usually strong; Mr. R. Blagrove led, Mr. Sidney Naylor presided at 
the harmonium, and Mr. Lohr conducted

RamsGATE.—On Wednesday evening, the 7th ult., the members of 
the Choral Society gave a musical entertainment at the Granville Hall, 
under the direction of Mr. Pollard. Mendelssohn’s Hymn of Praise 
was well given by the choir; the programme also contained a good 
selection. of music, all of which was successfully performed. Mac- 
farren’s part-song, “‘ Up, up, ye dames,” and the trio, ‘“ Ti Prego,” were 
much admired. A trio by Beethoven was exceedingly well played by 
Mr. Morley (violin), Mr. C. Foster Cooke (violoncello), and Mr. Pollard 
(piano). Miss Duval and Mr. H. Guy contributed several songs. The 
accompanists were Mrs. Stride and the Rev. R. G. Osborne

Reap1nG.—The Choral Society gave its first concert on Wednesday 
evening, the 17th ult.,in the Town Hall, under the conductorship of 
Mr. John Old. A band of twenty instrumentalists played several 
overtures. Mr. Old’s dramatic chorus, ‘ The Battle,” was one of the 
features of the concert. Madame Goodhind and Mr. Hunt were the 
solo vocalists, and Mr. G. Webb was well received for his performance 
on the clarionet

Toronto.—Two excellent performances of Randegger’s Cantata, 
Fridolin, have been given by the members of the Philharmonic 
Society, the chorus and orchestra numbering 250 performers. The 
solo singers were Mrs. Dow (Boston), Mr. Simpson, and Mr. Baird, 
both of New York, and Mr. Murray Scott (Toronto). Mr. F. H. 
Torrington conducted with his usual ability

UxsripGE.—The last concert of the season was given by the Choral 
Society on the rst ult. The solo vocalists were Mrs. Hanson, 
Miss Joyce Maas, Mr. Frank Elmore, and Mr. H. Pyatt, all of whom 
were well received. The choir sang several part-songs with good 
effect, Pinsuti’s “In this hour” and Barnby’s “ Silent night” being 
particularly well rendered. The great attraction, however, was the 
fine performance by Madile. Gabrielle Vaillant, of the andante and 
rondo from De Beriot’s Second Concerto for violin, and (with 
Mr. A. D. Miles) of Beethoven's Sonata in G, Op. 30, No. 3, for violin 
and pianoforte. Mr. Walsh was an efficient accompanist, and 
Mr. Miles conducted

WELLINGTON.—A lecture on Mendelssohn was delivered at the 
Town Hall, on the 2nd ult., by Mr. C. H. Fox. There was a large 
attendance. The lecturer gave a sketch of the life of the great com- 
poser, with the proximate dates when the different works were written. 
The lecture was illustrated by the Harmonic Society, of which Mr. 
Fox is honorary secretary, and the choruses and other pieces were 
selected with judgment and taste. Efficient assistance was rendered 
by Mrs. Gregory (Halberton), Miss Kidgell, Miss Manley, Miss Warren, 
Miss C. Elworthy, and Messrs. Toms, E. Knowling, S. H. Sparkes, 
Ebdon, Crowe, and Master Toms, as well as the members of the choir. 
The programme included the overture to the Midsummer Night's 
Dream, part-songs and duets, with several selections from the Orato- 
rios of St. Paul and Elijah, all of which were rendered effectively 
and in good taste. Mr. Manley conducted

