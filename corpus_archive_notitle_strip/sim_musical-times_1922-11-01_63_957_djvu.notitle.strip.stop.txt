methods , far novel , 
 | preferable 

 run results 
 |innovatory tendencies . , tune old 
 | satisfy intone 
 willingly . noticed interest excerpts 
 old judgments Beethoven Miss 
 Muriel Silburn reproduced month / usica/ 
 Times . students musical history familiar 
 countless instances kind , 
 refer Bach Wagner , 
 Monteverdi , Brahms , Gounod . 
 , compile list composers 
 treated anarchists 
 kind , shorter 
 compile good composers 

 standards judgments kind 
 originate founded faith certain fixed 
 rules ( alleged laws ) , certain trends 
 habits thought . ‘ opposite attitude 
 people willingly use , reference 
 composer works happen admire 
 despite infringements rules laws notice- 
 able , words Edgar Allan 
 Poe applies Shelley : “ disdained Rule 
 emanation Law , 
 soul Law . ’ needless 
 emphasise attitudes equally 
 uncritical 

 comprehension music , works|each generation certain parts 
 art , depends apprehension } heritage past . ‘ lime time 
 relationship elements — twofold relation-| happened , happens , genera- 
 ships , concrete positively deter-| tion evinces keener interest certain old works , 
 minable hand , purely spiritual other.|and lesser , foregoing 
 merely aspect the| evinced 
 relationships significant point view| evince . Similarly tendencies given 
 art . periods differ ( limits , sharply 

 listener dream contending | ) country country , regards 
 relationship more|old music new . amplitude 
 terms music Bach Beethoven sett told , great ; 
 sense , perfectly aware that|their duration . Withal know works 
 music imitators outwardly similar | survived test varia- 
 relationships mere shells not|tions kind endure despite 
 sense worth making , reached | temporary variations . 
 stage able perceive spiritual relation- reason confidently 
 ships , tangible relationships go|—and sake paradox — musical 
 making certain idiom style . For| works age . true freshness 

 Fc 

 painters . think 
 pictures musical subjects | 
 poetical 

 pictorial subjects . refer portraits , 
 class picture musicians 
 hold marked advantage poets painters . 
 lack certain — influence , 
 affluence , like — makes 
 unsuitable sitters eminent painters 
 epoch . refer imaginary 
 portraits musicians , branch 
 greater popularity begins noticeable . 
 know picture child Handel 
 astonishing parents playing — 
 modern pianoforte technique ? 
 know picture 
 Beethoven playing friends ? called 
 ‘ Symphony Beethoven , ’ sub - title 
 ‘ Picture men , trying 
 awake performance Symphony 
 Beethoven . ’ record 
 Symphony playing , certain 
 eighth , seventh , fifth ; 

 mean painted music . 
 lare pictures 

 picture Liszt extemporizing given 
 theme ( presumably , - line notation , 
 Gregorian tone ) miniature pianette , 
 left foot jammed sustaining pedal 

 poetical pictorial analogies 
 imaginative portraits ? true 
 picture , photograph , Charles Dickens 
 reading daughters , Milton 
 dictating Paradise Regained daughters ( 
 happy artist dutiful daughters ! ) , 
 picture Browning making rough 
 drafts — poetical equivalent extemporizing — 
 ‘ Red - Cotton - nightcap country , ’ Swinburne 
 reciting newly - finished poem Watts - Dunton ? 
 reason non - depiction 
 Watts - Dunton time listen , having ‘ 
 work hand . ’ 
 picture Raphael painting ‘ Madonna , ’ 
 Fra Angelico painting ‘ Annunciation ’ ? 
 truth painters hesitate enshrine 
 | pictures pictures lest work suffers 
 comparison lack unity style ; 
 composer hesitate use melody 
 Beethoven new work lest invention 
 sound poorer 

 /abu / arum genera 
 refer 
 subjects , 
 musicians music , music prisoned 
 |within bars pen man , music 
 native freedom . judge 
 | character expression produced 
 | performers ’ faces . Sir Joshua Reynolds 
 picture , “ Cherub Choir , ’ need 

 FRANK LIEBICH 

 crowd notes , freed rules bound | 
 stave — sort fancy - dress ball | musician Richard Wagner short 
 notable employées great composers.| story , 4 ” End Paris . death - bed 
 Minims dance bacchanalian frenzy , their|last words : “ believe God , Mozart , 
 large , vacant faces supported frail,| Beethoven . ’ tale written eighty years 
 semiquavering bodies ; demisemiquavers , hitherto|ago pot boiling Wagner 
 chained like Roman prisoners bands , | domiciled French capital . 
 caper ecstatically momentary freedom ; | interval elapsed printed 
 sharps , flats , naturals , hitherto | faith Mozart fluctuated . 
 waited patiently sides relentless| 19th century Mozart Society 
 masters , demand right considered as|assemblies concerts _ trysting - place 
 free independent citizens musical/| lovers Salzburg composer . 
 world . Rests , wanted|a man works public favour societies 
 rest Hampstead Heath ? | propagation preservation faith 

 , , music - day possess visible required . significant Mozart 
 charm ? tendency audiences drift | Society needed closing years 

 ‘ ap , ; thoughts , dancer ; 

 lhe unceasing exploitation musical sound tones , | musician . . . . | place value 
 unending possibilities harmonic ! op man praise censure . . . . | continue 
 combinations , adjustment ! follow express feelings . ’ 
 instruments musical ideas mee : ee 
 consequent newer appraisements functions straightforward simplicity sums 
 amalgamated timbres qualities charm pore Ho = . 
 strain hearing carefully | MUSIC : Beethoven outstrips “ ce 
 followed manifestations develop- | 4 ™ 4ster form , unrivalled genera- 
 ment progress modern music the| tion structural extension free varied 
 thirty years . ‘ Che ear wants rest|P ° lyphony . plastic moulds , 
 relaxation . better Mozart | hammered themes sketch- 
 turn respite refresh and| book rapped 
 - sensitise . | physical agony endured process 

 worked demoded composition . superb volcanic passion ; 
 outworn forms methods day , a| interpreter age Napoleon ; 
 harmonic system transcended touch Romantics , 
 recognition , elimination | forces working late world - wai 
 superfluous details , avoidance intro-| 204 heroism displayed 
 spection appeal emotion , his| ! . impersonal modern musician 

 ill - suited confront rough usage ! ‘ ' ® , | aim straightforward 
 subjected Archbishop of| ¢xPression simplest form . use 
 Salzburg courtiers ; bear poverty | working dramatic npate - aeaie . 
 : - ’ | essential ¢0 feel convey 

 hardships later years . | feelings . ’ : 
 wrote father compose | 
 cheerful , happy| easy deride idea simplicity 
 moods music reflected sorrow joy.| connected Stravinsky 
 Wistful regretful poetic } termed ultra - modern . unfamiliar 
 Adagios Andantes Sonatas , Symphonies , | sonorities disconcert unskilled hearer : 
 Quartets , touch morbidity ! familiar resources modern 
 passionate resentment despairing cries as| harmony 18th century , 
 sentimental persons discover appreciate in| recognise modern composer 
 Beethoven . direct simple profusion 

 perfect Symphony inG minor reveals| means disposal Mozart 
 nature . tremblingly alive delicate | circumscribed confines straightened harmonic 
 transient manifestations beauty } resources . 
 world . ‘ free un-| constant use labels musical 
 practised jealousy malevolence was/ history crippling 
 sensitive finger - tips good bad| recognises unbroken evolution art . 
 natures came contact , | terms classic , romantic , modern , induce partisan- 
 kept sane brave midst trouble } ship camps . 
 delightful sense fun humour . ) succeed ages , proceed 
 toguish countenance Salzburg Hanswurst| surely musical sounds , 
 peeps Rovdos Fina / es :| material , derived 

 Unorthodox , | Z7¢t / e Night Music Mozart 

 contemporary , asked sum creed 
 trenchant terms Wagner imaginary hero , 
 begin eliminating Beethoven 

 verbal brickbats statement provoke , 
 explanatory reservations , possible 
 reveal affiliations 
 modern heretical notions summing 
 concise form : believe Mozart , 
 Schubert — , , Mendelssohn 

 gem protests Principal 
 Domestic Workers ’ Bureau , Ltd. Like 
 Kensington Mayoress , ( , 
 protest ‘ woman’—not ‘ cat’—in 
 line ) , , , hastily assumes 
 object classes enable girls sing 
 work . ‘ mistaken impression gives 
 opening little light badinage , , 
 preliminary explosive * Preposterous ! ’ 
 withering , calling alliteration artful 
 aid 

 taxed order Carmen 
 cook Pagliacci pageboy , Beethoven 
 butler 7wo Zyes Grey 
 tweeny maid 

 Roars laughter ! aching sides 
 moved carry good work ‘ Charpentier 
 chauffeur , Handel housemaid , 
 Gluck governess , Franck footman , 
 Grieg gardener , Thomas Haynes 
 Bayley boot - boy 

 La Musique et les Nations . G. Jean - Aubry . 
 [ London : J. & W. Chester 

 live age passports , frontiers 
 bristling , tendency 
 requiring z / sa journey 
 Paddington Penzance . Piague Antwerp , 
 Carnarvon Cork , citizen proudest 
 boast speaks language understood 
 number fellow - men . Music time 
 lagged merry craze mutual incom- 
 prehension . quickly changing . 
 years ’ time Bach Beethoven 
 doubt respected position Cicero prose , 
 contemporary musical productions thenadays 
 like Welsh newspapers Catalan poetry- 
 local consumption . Music clearly 

 passed sway nationalism 

 pile books pamphlets special interest 
 teachers dealt briefly , 
 cases authors ’ names _ sufficient 
 guarantee discerning educators 

 Growth Music , H. C. Colles ( Clarendon 
 Press , 10s . 6d . ) , reprint work 
 established standard guide . 
 originally appeared volumes , dealing 
 respectively ‘ Troubadours J. S. 
 Bach ’ ; ‘ Ageof Sonata , C. Ph . E. Bach 
 | Beethoven ’ ; ‘ Ideals 19th century . ’ 
 publishers issued volumes 
 | — stout , handy book pages , 
 delightfully clear attractive style . fact 
 |its described ‘ Study Musical History 
 | Schools ’ justice ; emphati- 
 cally study lots left school- 
 | days far . Books ‘ musical appreciation 
 | poured forth late . best 
 |of , happily bears shibboleth 
 |on title - page . helpful features 
 | frequent use cross - reference text . , 
 copious index , synopsis 
 chapters , makes book convenient hunting- 
 | ground data . numerous musical 
 | examples , considerable length . 
 | Percy A. Scholes Second Book Great 
 Musicians ( Oxford University Press , 45 . 6d . , gilt , 55 . ) 
 | course find public waiting . Mr. 
 | Scholes adept simple exposition , 
 |is chatting kiddies usual , persuading 

 ook gives 
 ances , 
 ( Country 
 forte solo 

 MUSIC LETTERS 

 centenary César Franck ( born December 10 , 
 1822 , Liége ) calls forth leading article the| 
 October Music Letters ( 22 , Essex Street , 55 . ) , 
 W. Wright Roberts , excellently judicious 
 readable , straying sheer idolatry ( 
 Franckian idolatry palls irreverential 
 generation ) , picking great things 
 sounding praise . Egon Kornstein , Hungarian 
 Quartet , writes thoughtful way ‘ 
 Practise String Quartet , ’ won 
 quartets Barték , , post - Beethoven 
 quartets , greatest resemblance 
 Beethoven period 

 Eugéne Goossens discusses ‘ String Quartet 
 Brahms ’ ; recognise 
 vivacious , ranging Mr. Goossens 
 formal observations . Prof. Henry J. Watt 
 article ‘ Rule Law Music ’ 
 missed interested philosophy art . 
 Lady Dean Paul , , pages ( ‘ Musings ’ ) 
 wsthetics . ‘ Life interlude creation 
 disintegration , art expression man 
 yearning constancy . ’ takes line 
 results depreciation Mahler ( ‘ conceived 
 intellectually gives vast effigies 
 philosophy spiritual memorials ’ ) appre- 
 ciation Berlioz . ‘ Jonsonian Masque ’ 
 described Jeffrey Mark 

 lot ) ; beautiful miniature strings 
 F. Lawrence , 77is#is ; Frank Bridge impression 
 Summer ( clever essay objective- 
 contemplative mood ) ; Gerrard Williams 
 fragrant Pot - Pourri — worthy welcome items . 
 W. R. 

 QUEEN HALL SYMPHONY CONCERT 
 [ Symphony Schumann D minor 
 Concerto Beethoven E flat , 

 L , S.Q 

 Verdi Reguiem received exceedingly fine 
 interpretation , Italian vein , , 
 efface recollection Nikisch memorable 
 performance 1913 , suffered little comparison . 
 Grail Scene /arsifa/ suffered 
 chorus big , near hand , bring 
 mystical effect . copious selection 
 Die Meistersinger happier , afforded 
 brightest spots brilliant Festival . 

 798 MUSICAL TIMES — Novemser 1 1922 
 doubted music al British composer . Mr. McEwen , like Sir Dan , 
 finer interpretation , said , with| , man 
 greater confidence , Brahms Schicksa / s / ied,| striven represent British music worthily , , 
 inspiration fully realised | Mr. McEwen written British idiom , 
 conductor , choir , orchestra . instrumental | quality music 
 Introduction Epilogue reached great height | fashion hard fighting , every- 
 refined beauty . supremely fine instrumentalists | body right people credit , 
 appeared soloists : M. Cortét Beethoven fifth| brilliant scribes beginning discover 
 Pianoforte Concerto Franck Symphonic / . certainly class representative 
 Variations , Mr. Albert Sammons Elgar Violin ! British composer . shall turn work 
 Concerto , acknowledged exponent . | time come , revel national flavour . 
 orchestral works Scriabin two| ‘ programme ’ work , fact regret , | 
 symphonic works , ? oeme de ? -xtase Prometheus , | think composers command sufficient 
 furnished hard nuts audience crack , | right material enable supply absolute 
 left impression marvellously fine perform- | music free nature guide 
 ances . questioned were| meaning . Symphony entitled / way , 
 judicious programme , } composer deals actualities . provides literary , 
 fact Mr. Coates close touch Scriabin | , , poetic basis , entitles move- 
 music affords sufficient reason . ments ‘ Spring Tide , ’ ‘ Moonlight , ’ ‘ Sou’-West 
 choice principals adversely | Wind . ’ areall mood pictures , isa strong 
 criticised vocalists world- | feeling poetic aspect things . second 
 wide fame included . matter policy , } composer wholly true land birth . 
 artists names } given hint ‘ hills 
 decorate poster , extent fees | Highlands love , ’ cleverly keeps 
 subscribers feel having money’s-| Scottish characteristics fore . , 
 worth , said , ] second movement , insistent melodic figure , 
 performances suffered efficiency . Mr. John| freely suggests revelry night going 
 Coates , Mr. Robert Radford , Mr. Norman Allin|on mountains , strains , wafted 
 , course , head profession , and| , graphically depicted . 
 Miss Dorothy Silk recent years risen a| listen ‘ programme ’ music freedom 
 similar height ; Miss Margaret Balfour , who| mind music leaves find 
 appearance Leeds Festival , | story , impressed 
 showed strides late art , | hearer thoughtful , individual piece 
 left pleasant impression beautiful | work lightly dismissed . pleasant 
 voice artistic singing . soprano ! example ‘ atmospheric ’ work , , happily , 
 charming voice musicianship a|an atmosphere likes linger 
 worthy colleague artists mentioned | desire , ‘ atmospheric ’ pieces , 
 Bach Magnificat , finished Eva the| outside soon possible . Sir Dan Godfrey gave 
 Meistersinger Quintet , Miss Elsie Suddaby , a| careful presentation hands excellent 
 Leeds singer fame rapidly extending | orchestra , audience paid composer 
 borders , engagement showed | compliment calling platform . 
 honour county.| rest programme testified 
 Miss Eleanor Paget , newcomer , powerfully | pliability orchestra , 
 sustained trying soprano Chora/| \eistersinger Overture Ravel quaint 
 Symphony , Miss Edith Clegg , Mr. John Adams , | fairy Suite 1/¢7e 7Oye Saint - Saéns Pianoforte 
 Mr. Raymond Hartley , Mr. Percy Heming ( } Concertoin F , Mr. Arthur Shattuck soloist . 
 excellent Amfortas ) efficient } ; Bournemouth learned Easter 
 respective tasks . Mr. Percy Richardson useful ] Festival established fact , save year 
 work organ pianoforte ( comfinuo | . 
 Bach Cantatas ) deserves mention . Mr. Albert } , I. F. E. B. 
 Coates best , putting great vitality his| 
 conducting , altogether Festival z oe . 
 roused general enthusiasm adi 
 recollect occasion 1886 , ecstatic | Church Organ Music 
 chorus girls pelted Sullivan roses 

 conducted Golden Legend . ORGAN 
 October issue quarterly journal shows 
 falling variety interest . organs Bristol 
 J. B. MCEWEN SYMPHONY Cathedral discussed Rev. Andrew Freeman , 
 BOURNEMOUTH St. Paul Cathedral Mr. Somers Clarke . 
 “ . . ; Mr. Stuart Archer writes Cavaillé - Col organ , 
 Sir Dan Godfrey began - eighth season ! yy , W. F. Muckle Table Organ William Mace , 
 Symphony .Concerts Bournemouth onjand Mr. James Matthews organ built 
 October 12 , beating bush . | Pretorius model Freiburg University . Dr. Eaglefield 
 chief number programme new/| Hull continues frank discussion organ tutors , old 
 Symphony , British , work J. B.|new . articles Mr. Meyrick Roberts ( 
 McEwen . fact frank acknowledgment reg weer tt : paths herBemoagh gm 
 cy ~e ~ » bea u ustre ’ 
 policy Sir Dan pursued long , Rouen , Chartres ) , Dr. Audsley , Mr. Abdy Williams , 
 good audience spite weather relates experience temporary maestro dt 
 tempted sands sunshine } -apsel / Capri . Correspondence , reviews , specifica- 
 symphonies seriousness , showed won| tions balance capital number , , 
 public character champion | usual , illustrations strong feature 

 Music Provinces 

 BIRMINGHAM DtstrRict.—At series 
 symphony concerts City Birmingham Orchestra , 
 given Wednesday , October 4 , Town Hall , Mr. 
 Appleby Matthews conducted stimulating , unconven- 
 tional , performance César Franck Symphony . 
 programme included Bantock Sappho songs , 
 Orchestral Prelude cycle . 
 composer conducted , vocalist Miss Dorothy 
 d'Orsay , sang admirable control artistic 
 insight . beautiful contralto voice , finding 
 complement variety colouring intelligence 
 interpretations . — — Sunday evening concerts 
 City Orchestra , regular feature winter 
 musical season , provided interesting music . 
 Mozart /upiter Beethoven fifth Symphonies 
 given , ard , lesser - known works , Gustav Holst 
 Marching Song orchestra proved extremely enjoyable 

 Miss Marjorie Sotham introduced mid - day 
 concert Birmingham , venture promises 
 successful . vocal recital Dr. Tom Goodey included 
 songs Wolf , Martin Shaw , charming folk - songs . 
 Miss Sotham co - operated Messrs. Frank Venton 

 HUDDERSFIELD.—At concert September 30 , 
 Huddersfield Phiiharmonic Society presented orchestral 
 music including Scherzo finale Dvorak 
 New World Symphony , Ruy Alas Don 
 Giovanni Overtures , lighter items Moszkowski 
 Délibes Mr. Wilfred Miller , orchestra , 
 played Dream piece Delafosse , clarinet solo 

 Leeps.—An account Leeds Musical Festival 
 appears thisissue . September 23 , Haydn 
 Creation sung Oxford Place Chapel , 
 conductorship Mr. Robert Pickard.——-The aseries 
 special orchestral concerts held Belgrave 
 Central Hall October 7 , section Leeds 
 Symphony Orchestra , Mr. Bensley Ghent , played 
 Beethoven Egmont Symphony . 1 ; 
 Old English Suite Cowen.——During week 
 beginning October 9 , Adrian Beecham musical setting 
 Merchant Venice staged Leeds Theatre Royal . 
 — — Leeds Committee British National Opera 
 Company entertained October 13 luncheon , 
 Mr. Mrs. Percy T. Leigh . stated 
 Leeds subscriptions B.N.O. 
 £ 1,000 £ 3,000 £ 4,000 
 Bradford . Mr. Percy Pitt , artistic director Company , 
 said understood visiting Leeds Grand 
 Theatre week , commencing February 206 . 
 — — tThe winter season Leeds Saturday Orchestral 
 Concerts opened October 14 , Borodin second 
 Symphony , B minor , performed time 
 Leeds , Mr. Eugéne Goossens ccnducting . ZS 3 
 Hoggett , lecturer Music University Leeds , 
 gave series discourses dealing ‘ 
 beginnings Modern Music , ’ October 16 , 
 special subject ‘ Beethoven later works 

 LIVERPOOL.—Mr . Frank Roscoe opened course 

 AMSTERDAM 

 week intervened summer season — 
 came effective close Scheveningen 
 Beethoven Ninth — commencement winter 
 season . Mengelberg appeared subscription 
 concert , absence unwonted duration . 
 older pieces , Mahler Symphony , gave 
 hearing Henri Rabaud Eclogue , work breathes 
 tender emotions . concert 
 conspicuous Bronislaw Hubermann appearing soloist 
 time subscription concerts , 
 audience counts kind hall - mark . 
 played Concerto Brahms superbly . Small wonder 
 announcement solo recital days later 
 caused big hall filled seat . 
 occasion Hubermann played Concertos ( Bach 
 minor Goetz F ) , Beethoven Aventse ? 
 Sonata 

 French Musical Festival great event . 
 alleged objective survey present state 
 modern French music . realisation plan , 
 , hampered , inasmuch momentary conditions 
 operatic life permit performances French 
 opera scheme . Works César Franck 
 excluded time , entire concert reserved 
 celebrate Franck Centenary December 7 

 strange fate befell E. Th . W. Hoffmann opera , 
 Undine , July 29 , 1817 , successful run 
 - nights , theatre burned 
 costly decorations material necessary 
 performance . composer refused staged 
 opera - house , large , 
 refusal pay dearly , , apart 
 unsuccessful performance Prague 1821 , opera 
 lost world . Ninety years passed 
 performance unique work Hans Pfitzner 
 published pianoforte score text ( C. F. Peters , Leipsic ) . 
 hundredth anniversary Hoffmann death , 
 German stages performed Undine , romantic 
 opera , great success . remains seen 
 success 

 addition event , Messrs. I. Engelhorn , 
 Stuttgart , published book 
 pages , containing Hoffmann literary works , z.e . , 
 musical novels portions musical criticisms 
 value . 
 recognised real greatness Beethoven , musical 
 critics written enthusiastically , 
 time correctly , Hoffmann essay , 
 * Beethoven Instrumentalmusick 

 NEW WORKS 

 Mischa Elman opened concert season Carnegie 
 Hall absence America 
 years . played , , 1920 , 
 announced abroad rest , study , 
 composition . compositions 
 materialised , confessed rest study 
 wrought changes Mr. Elman methods 
 mannerisms . returns virtues 
 faults marked career . tone 
 large generally pure , technique excellent , 
 affectations sentimentalisings continually 
 evidence , mar attempts work 

 real music played thoroughly artistic way 
 return New York , time 
 year ‘ Festivals ’ parts 
 country . Chief Mrs. Coolidge annual Festival 
 chamber music held charming home 
 Berkshire Hills . opening programme given 
 Wendling String Quartet , Stuttgart , Germany . 
 leader , Carl Wendling , concert - master Boston 
 Symphony Orchestra Gericke . concert 
 devoted Beethoven , Schumann , Reger , second 
 Brahms . Sonata . 2 , E flat , Op . 120 , 
 pianoforte viola , superbly played Mr. Ernest 
 Hutcheson Mr. Firestone ( viola player 
 San Francisco Orchestra ) . novelty afternoon 

 concert Trio C minor Gabriel Pierné , 

 Augusteum visit promised Stokovski , 
 known New York , visits Italy 
 time , probable Sibelius come Rome 
 year . season inaugurated end 
 November Verdi Mass 

 Accademia di Sta . Cecilia informs Capet 
 ( Juartet , Paris , visit Rome season , 
 interpret Quartets Beethoven 

 Finally , December 10 centenary birth 
 César Franck , Amici della Musica organizing special 
 Franck commemoration date 

 number resident Vienna conductors season 
 considerably enlarged . Apart advent 
 Paul von Klenau , alluded , Anton von Webern , 
 ultra - radicai composer disciple Arnold Schinberg 
 — String Quartet caused uproar 
 recent Salzburg Chamber Music Festival — 
 newcomer conductors . assumed 
 directorship Konzertverein Sunday afternoon 
 popular orchestral concerts , débfit surprisingly 
 orthodox . work Clemens Krauss , new 
 young conductor , quickly established promising 
 operatic Kapellmeister , symphonic débit confirmed 
 favourable impression . programme , consisting 

 conducting distinguished repose , absence 
 mannerism . pianistic style eminently suited 
 works modern ultra - modern school , lends 
 readily classic works Beethoven Scarlatti , 
 suffered , things , excessive use 
 pedal 

 sixtieth birthday Emil Sauer , revered pianist , 
 occasion great festival concert honour , 
 - known professional pupils 
 participants . programmes included 
 Pianoforte Concertos — grateful legitimate music 
 innocent sort . eve birthday Sauer , 
 recital , played number familiar composi- 
 tions programmes 
 vears . return ‘ romantic ’ pianist , Germaine 
 Schnitzer , native city social 
 musical event . sister art , young Erica Morini , 
 developed remarkably year absence , 
 mentally technically 

