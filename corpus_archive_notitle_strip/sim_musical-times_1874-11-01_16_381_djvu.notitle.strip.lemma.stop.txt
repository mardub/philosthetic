n ia 
 School Rooms , & c 

 London , find 
 number imitation celebrate SHORT IRON 
 GRAND PIANOS offer public genuine Instru- 
 ment , think musical Profession , Amateurs , 
 Trade , following statement : — year ago , 
 thought labour , succeed complete short 
 Grand , adopt cross - string principle Steinway , lessen 
 size ) improvement sound - board 
 action ; know musical world Germany 
 originator Pianos . time ad- 
 ditional improvement effect , sale gradually 
 increase , present time 250 work- 
 man engage exclusively manufacture . acknowledge 
 superiority Pianofortes description 
 size cause closely imitate maker ; , 
 instance , ' " ' Beethoven Grand , " originally use 
 asa distinctive trade mark , abandon , adopt- 
 e ; unexampled modesty , public invite 
 accept copy original , consider genuine spurious . 
 numerous testimonial receive ( include 
 medal ) , [ permit mention Majesty King 
 Saxony , great interest Art Manufactures , 
 addition excellent musician , honour manufactory visit 
 February 25th , pleased express entire satisfaction . 
 Depot Pianofortes London Mr. FLAVELL , 26 , 
 North Audley Street , W. , great variety . — 
 ERNST KAPS , Sole Importer — CuHar_Les RussELL . 
 March 6 , 1874 

 FABRICATION SUPERIEURE ARTISTIQUE . 
 MILLEREAU CO . , PARIS 

 J 
 NEW PIECES , sy LOUIS DUPUIS . 
 gem SACRED ART 

 s. d. 
 . 1 . HENRY VII . CHAPEL ... aaa « = . 3 @ 
 Beethoven " hallelujah , " Mount Olives . 
 2 . ST . GEORGE CHAPEL ... die ow 7 © 
 Jackson te deum F. 
 3 . ST . PATRICK CATHEDRAL ... 3 0 
 Mendelssohn " come , let worship . " 
 4 . MUNICH CATHEDRAL ... « ae 
 Spohr " Judgment . " 
 5 . FREIBURG CATHEDRAL ps ae oe 
 Travers " ascribe unto Lord . " 
 6 . RHEIMS CATHEDRAL ... ... ... 30 

 R. A. Smith " ' beautiful mountain . " 
 beautifully illustrate view Interiors Sacred Edifices 

 year past , conductor 
 equally free 

 liability censure securing , soloist , 
 Mdlle . Albani , Miss Wynne , Mdme . Patey , Mr. 
 Sims Reeves , Mr. E. Lloyd , Herr Behrens , Mr. 
 Santley , Miss Dora Schirmacher ( piano ) ; Herr 
 Straus Mr. Carrodus ( violin ) . orchestra 
 instrument , lead M. 
 Sainton , perfect " wind " depart- 
 ment , fairly necessary mark , 
 chorus 300 voice 
 train Mr. James Sanders , challenge notice . 
 far , , , artistic 
 point view , engagement Mdme . Adelina 
 Patti sing concert £ 800 lie open 
 question . join outcry 
 Mdme . Patti large sum 
 Festival fund . Diva talent are.as 
 marketable commodity waggon load turnip , 
 value fetch . 
 appear , , statement recently issue 
 committee , Mdme . Patti engagement 
 good stroke business , bring con- 
 siderable sum . question profit 
 loss set fact engage 
 popular singer day hackneyed song 
 evening concert depreciate 
 important performance raison d'etre 
 occasion . Liverpool people flock crowd 
 hear Mdme . Patti sing ' ' mile Edinbro ' 
 town , " stay away " St. Paul , " , 
 ordinary condition , probably 
 choose oratorio . committee , know 
 public , act good intention , 
 good deal worldly wisdom , admit . let 
 hope , occasion , con- 
 sider necessary secure success great 
 musical festival foremost attraction 
 bring money 
 week ago . reason , probably , dictate 
 Mdme . Patti engagement , programme consist , 
 main , work past stage novelty . 
 manager appear distrust local 
 amateur , think good offer familiar 
 bill fare ; , strange inconsistency 
 point view , leave programme 
 oratorio — ' " Messiah " * Elijah , " — 
 usually depend draw " house . " look 
 circumstance case , 
 grave fault find Liverpool 
 scheme . reason , apart beauty 
 work , " St. Paul " ; 
 Liverpool interest , arise 
 performance 1836 . ' creation " 
 ( ) spare , true , 
 handelian selection , Gounod Mass 
 " SS . Angeli Custodes , " new town , 
 Sullivan ’ s " Light World . " respect 
 evening concert , fault find 
 choice Mozart Symphony G minor , 
 Beethoven " Pastoral , " Gounod ' " Jeanne d’Arc , " 
 ( time Liverpool ) , Mendelssohn " italian " 
 symphony ; Professor Oakeley march 
 " Edinburgh , " Mr. G. A. Macfarren festal Overture , 
 Mr. J. F. Barnett descriptive piece ' ' Lay 
 minstrel , " important element abso- 
 lute novelty provide . committee , 
 , , deny 
 

 Festival begin Philharmonic Hall 
 Tuesday , September 29 , performance " St. 
 Paul ; " Royal Highness Duke Edin- 
 burgh act president . artist familiar 

 
 , Herr Straus , Cavatina Dances , 
 Mr. Oakeley March conduct com , 
 poser person , set » 
 successful . original sense jt 
 recall memory , original Mare , 
 , , vara avis terra . capacity 
 form exhaust ; wherefore , let Mr. Oakeley 
 credit belong , familig 
 model , turn workmanlike thing . bee , 
 object March heavily score , 
 music character look pomposity 
 noise , , , means wholly 
 consist . trioin subdominant key 
 special credit composer skill ang 
 fancy , work , , 
 creditable , justify Duke Edinburgh 
 personally compliment Mr. Oakeley 
 success . decline express opinion 
 Mr. Macfarren overture till beep 
 hear condition suitable . perform . 
 ance good , audience ip . 
 attentive , thing fall flat . happily 
 work remain , entitle careful attention 
 subsequent occasion . attention 
 assuredly , receive , time weigh 
 merit hardly absent 
 write distinguished composer " St. John 
 Baptist 

 pass briefly work 
 second morning . Haydn " creation , " 
 solo Mdlle . Albani , Mr. Bentham 
 Behrens , follow Gounod Mass " s§§. 
 Angeli Custodes , " selection seven 
 piece ' " ' Messiah " " Israel Egypt , " 
 Miss Wynne , Mdme . Patey , Mr. Lloyd 
 . M.Gounod Mass having 
 discuss column need dwell 
 ; , regard entire concert , 
 suffice add Liverpool people express 
 indifference send select representa : 
 tion " assist . " evening entertainment open 
 ' ' Pastoral " symphony , , , 
 - dress crowd wake appreciation 
 great music . praise 
 account . " Pastoral " symphony 
 story tell child 
 interest , tell manner simple 
 infantine comprehension . Liverpool folk , 
 yawn Mozart " ' G minor , " hear 
 piping , rustic revelry , bird note , storm noise , & € y 
 Beethoven work obvious pleasure , 
 applaud symphony nearly half 
 energy , minute , applaud 
 Mdme . Patti " mile Edinbro ’ town . 
 Gounod exquisitely droll ' Funeral March & 
 Marionette " " Jeanne d’Arc " 
 important feature programme , upoa 
 requisite dilate , merit having 
 long ago receive adequate attention . 
 March , strange , excite little interest- 
 audience sense humour ? — 
 Cantata come end long concert , 
 hear attention necessary appt 
 ciate character . performance , , lie 
 open criticism , reason , , 
 concerned weary . Mr. Sims Reeves 
 appearance occasion ( prevent 
 illness " St. Paul " ) , sing 
 Benedict " charming girl love , " 
 Blumenthal " Requital , " old beauty voice 
 style 

 morning devote Mr. Arthur 

 general remark leave Prospectys 
 speak , confident enterprise jt 
 heralds judge fairly merit set forth 
 confident don 
 deserve success 

 desirous attention couple 
 excellent photograph forward 
 , represent eminent live 
 decease musician . , entitle ' Heroes 
 german Music , " 1740 1867 , ex . 
 ceedingly effective picture ; Bach , finger 
 organ , surround Beethoven , Gluck , 
 Handel , Haydn , Mozart , Schubert , & c. , appear 
 orchestra , whilst celebrity , male 
 female , artistically pose beneath . 
 group , " Heroes Music Italy , 
 France , England , " & c. , 1450 1868 , include 
 worthily represent music 
 country , german point view ; 
 inclined question 
 England produce artist 
 worthy place Purcell , Burney Field . 
 likeness exceedingly striking ; 
 cordially commend interesting picture 
 professor amateur , reliable portrait 
 , composer executant , 
 shed lustre art 

 large space invariably 
 devote review , find impossible 
 small selection music 
 forward . unnecessary state 
 obvious fact inundate 
 letter urge notice piece send " 
 month ; " occasion 
 politely request composer 
 review certain piece receive , 
 desire " alteration " 
 allude matter 
 column , lest b 
 accuse discourtesy reply numer 
 ous request . future , , 
 counsel send work review 
 remember — letter drop 
 post - box property Postmaster 
 General — composition forward notice 
 consider undisturbed possession 
 custody voluntarily 
 commit 

 result careful study execution Wallace 
 second Grand Polka de Concert . Mr. Stedman hear 
 advantage ' Tom Bowling , " 
 Miss Scott duet Balfe ' ' Talisman , " ' ' 
 ring , " Herr Oberthiir play usual masterly 
 manner , highly appreciate . Mr. Henry Parker , 
 great favourite , accompany careful 
 artistic way . theatre overflowing 

 ' meeting new Association ' 
 Investigation Discussion subject connect 
 Art Science Music , " announce place 
 2nd inst . , Beethoven Rooms , Harley Street , 
 Cavendish Square . o’clock , Paper ' ' extend- 
 e compass increase tone stringed instru- 
 ment , " Dr. W. H. Stone , M.A. , F.R.C.P. , read 
 author ; quartett stringed instrument , 
 fit Dr. Stone Mr. Meeson Elliptical Tension 
 bar , double Bass string CCC , ex- 
 hibite play . R.H.M. Bosanquet , Esq . , M.A. , 
 St. John College , Oxford , read Paper 
 " " temperament , division octave 

 University INTELLIGENCE.—Oxford , Ociober 24 . 
 examination degree Bachelor Music . 
 follow satisfy Examiners:—Bentley , John , 
 New College ( St. Ann Street , Manchester ) ; Birch , 
 Edward H. , New College ( Notting Hiil , London , W. ) ; 
 Bradley , Joseph , New College ( High Street , Staly- 
 bridge ) ; Cay , Francis , New College ( bed . Middle 
 Class School ) ; Cole , H. Cardini , New College ( 
 Hampstead Road , London , N.W. ) ; Gower , John H. , New 
 Inn Hall ( Windsor ) ; Hartmann , Albert , F.O. un- 
 attach ( bandmaster H.M. 17th Lancers , Dundalk ) ; 
 Hill , Andrew T. , St. Mary Hall ( Cheam , Surrey ) ; 
 Holloway , Arthur S. , Worcester College ( Heming- 
 ford Road , London , N. ) ; Howard , Samuel , New College 
 ( Rochdale Road , Manchester ) ; Hullett , Charles H. , 
 St. Mary Hall ( York Street , Portman Square , Lon- 
 don , W. ) ; Hunt , H. G. Bonavia , Christ Church ( 
 Middle Temple , London , E.C. ) ; Lister , Henry , New 
 College ( Islington , London , N ) ; Léhr , George , S.L. , 
 New College ( Woburn Square , London ) ; Lott , 
 John B. , New College ( Assistant Organist , Canterbury ) ; 
 Morland , John , New College ( St. Martin , 
 Leicester ) ; Palmer , Walter H. , New College ( 
 Lindfield , Weston - Super- Mare ) ; Righton , John H. , New 
 College ( Faringdon ) ; Riseley , Thomas , Christ 
 Church ( Cheltenham College ) ; Strah , Frederick K. , 
 New College ( Rosemount , Selkirk , N.B. ) ; Troman , 
 Thomas , New College ( Smethwick ) ; Williams , 
 Charles L. , New College ( St. Columba College , 
 Rathfarnham , Dublin ) ; Wrigley , James G. , New College 
 ( Church Street , Blackpool ) . examiner — 
 Sir F. A. Gore Ouseley , Bart . , M.A. Mus . Doc . , Christ 
 Church , Professor Music ; Charles W. Corfe , Mus . Doc . , 
 Christ Church , Choragus ; Edwin G. Monk , Mus . 
 Doc . , Exeter College.—Second examination degree 
 Bachelor Music . examination hold early 
 Easter Term , 1875 . candidate examination , 
 addition necessary subject , require 
 critical knowledge score Beethoven 
 " symphony B flat ; ' " ? Handel ' ode St. Cecilia 
 Day ; " Mozart ' " ' requiem . " text book 
 Ouseley ' ' treatise harmony counterpoint , " 
 Berlioz , Kastner , ' ' instrumentation , " Burney 
 Hawkins " ' History Music . ' exercise 
 candidate propose offer exami- 
 nation , send ( approval examiner ) 
 Professor Music , residence , St. Michael , 
 Tenbury , time February 1 , 1875 

 Harvest Festival Services hold St. Stephen , 
 South Kensington , the.4th 11th ult . church 
 beautifully decorate corn , fruit , flower , & c. , 
 font west end special feature . 
 service fully choral , evensong , 
 anthem , ' heaven tell , " ' 
 thank ' " ' ( Mozart ) , effectively render 
 choir . organ orchestra ; , introduce 

 le Athens fate noble hero 
 d country againt countless host invade Persians , 
 anxiety dispel joyous word outrunner 
 fellow arm , fall dead utterance — " 
 victory ! people break shout victory , 
 check priest , admonish offer thank heaven , 
 ensue movement placid religious character . fugal 
 movement succeed , boastful advance foe 
 ibe , contrapuntal character display power 
 voice . number close brilliantly amplification 
 ofthe open choral movement , " victory ! victory ! " no.2 isa bass 

 akg . 3 , song Egina , betrothed athenian 
 leader , lament loss beloved . simple highly 
 tpressive Air . . 4 , bass announce approach 
 wetorious army ; . 5 kind choral pol : bid wel- 
 come hero , piece strongly marked accent son- 
 orous vocal distribution secure impressive effect . 
 Recitative , enraptured Egina recognise 
 form victor , lead , course , Duet , con- 
 siste delighted exclamation happy reunite pair . 
 difficult situation musical rendering ; admirable 
 composer fall short attempt paint note 
 testacy faithful heart meet joy separation des- 
 pair anguish , fact Beethoven perfect success 
 wondrous piece G , Fidelio , add difficulty 
 Mmiters ; set comparison aside , high praise 
 mstance far unsuccessful , certain good 
 tect . final number chiefly choral , intersperse 
 strain solo voice . spirit , generally 
 t character vary touch tenderness , wiil 
 hearer favourable recollection work . 
 ir . Brion oung musician extensive 
 © important Cantata appear . let en- 
 souragement successful accomplishment arduous task , 
 aim high mark grow 
 tertainty practised hand 

 Choir 

