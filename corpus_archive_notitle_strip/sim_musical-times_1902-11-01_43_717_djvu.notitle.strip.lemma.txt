while listen to this section that the scene might 
 well be depict orchestrally . ) and who will 
 forget that glorious high b natural of the soprano 
 near the end of Part I. ? Mr. John Coates as 
 Gerontius repeat the success he have make at 
 Worcester ; and Mr. Ffrangcon - Davies impressively 
 sing the music assign to the bass voice . Miss 
 Muriel Foster worthily replace Miss Marie Brema 
 ( absent through illness ) by her rendering of the 
 Angel music in Part II . this young singer be a true 
 artist , and the possessor of a beautiful voice . her 
 rapid rise in public estimation be well deserve , and 
 her refined singing always give pleasure . chorus 
 and orchestra spare no pain to do justice to a work 
 that have become epoch - make in the history of 
 english music 

 follow the Prelude to Act III . of ' Lohengrin ' 
 and Beethoven ’s Violin Concerto ( soloist , M. Ysaye ) 
 come Dr. Elgar ’s ' Coronation Ode , ' a setting of some 
 fine verse by Mr. Arthur C. Benson . this work be 
 to have be sing by the Sheffield choir at the gala 
 opera performance in London as part of the Coro- 
 nation celebration , but the King ’s illness necessitate 
 the abandonment of the function , to the intense dis- 
 appointment of the Sheffield singer . no trace of 
 this disappointment be , however , noticeable in their 
 interpretation of Dr. Elgar ’s melodious strain . the 
 rendering of the unaccompanied chorus * daughter 
 of ancient Kings ’ prove to be the perfection of 
 refined chorus - singing . the purity of tone , the ideai 
 enunciation of the words—.g . , ' pure , stately " — no 
 less than the perfect blending of the voice , reach 
 the high point of choral technique . the final 
 chord of this movement contain a low c for the 
 bass ; this note , sing softly and of good tone 
 and absolutely in tune , produce a wonderful effect 
 — ' successful sounding of the deep C ’ , ' as 
 one of the audience nautically observe . the 
 principal in the ' Ode ' be Miss Agnes Nicholls , 
 Miss Muriel Foster , Mr. John Coates , and Mr. 
 Ffrangcon - Davies ; and Dr. Elgar conduct a 
 performance which must have afford he as much 
 pleasure as his music give to the audience 

 upon reassemble in the evening we be favour 
 with the ' Wandrer ’s Sturmlied ' of Richard Strauss , 
 perform for the first timein England . this setting 
 of Goethe ’s word , for six - part chorus and orchestra , 
 be an early work of the most modern of german 
 composer , and hardly represent the true Strauss . 
 contrast , not unwelcome , be afford in Mozart ’s 
 delicious G minor Symphony , in which Mr. Wood 
 give evidence of his genius as a conductor of orches- 
 tral music . the work be most beautifully play . 
 Strauss be again in evidence when two song from 
 his Opus 33 , entitle ' Hymnus ' and ' Pilgers Morgen- 
 lie , ' be sing by Mr. Bispham . Dr. Cowen then 
 conduct his setting of Collins ’s ' ode to the 
 passion , ' a work which have attain great popularity , 
 and to which all concern do ample justice . a 
 selection from ' Israel in Egypt ' conclude the day ’s 
 music . suffice it to say that in old Handel ’s stately 
 strain Sheffield again triumph gloriously 

 DR . JOACHIM 
 PROFESSOR KARL 

 MR . BERTHOLD TOURS 
 Dr. G. M. GARRETT 
 MR . W. T. BEST 
 * REV . SIR F , A. 
 BEETHOVEN aie 
 MOZART ( at the age of 7 ) 
 * DR . WILLIAM POLE 

 KLINDWORTH 

 the weak saint upon his knee , 
 show we rather a mild and innocuous devil , as compare 
 with the fiend of ' the golden Legend ' or of ' Eden , ' to 
 say nothing of the Mephistopheles of Berlioz , Gounod , or 
 Boito . possibly a more experienced actor than 
 Mr. Watkin Mills might have make Dr. Parker 's Satan 
 seem more vividly imagine ; nothing in the Act be as 
 beautiful as its conclusion , in which the choir take a 
 prominent part . of the beautiful third part , with its 
 splendid sequence of ecclesiastical chorus , and its fine 
 realization of Saint Christopher ’s feat in bear the 
 Christ - child through the flood , it be not necessary to 
 speak again in this place : it be only to be record that 
 Mr. Andrew Black sing ' it with much vigour , that Miss 
 Agnes Nicholls be successful as the queen of the first 
 Act and the Angel of the last , and that Mr. Saunders be 
 good as the king turn hermit 

 in the first part of the same concert come Dr. Elgar ’s 
 new ' coronation Ode , ' first hear the week before in 
 Sheffield . it be curious that the little chorus ' daughter 
 of ancient Kings , ' which provoke the only encore of the 
 festival at Sheffield , fall almost flat at Bristol . still the 
 work as a whole delight the audience , and the usual 
 wagging of head testify to the popularity of the big tune 
 on which the final number be base . between this and 
 ' Saint Christopher , ' both of which be conduct by their 
 composer , Miss Adela Verne play Liszt ’s ' hungarian 
 Fantasia , ' with orchestra , in admirably finished style . I 
 seem to be rain eminent pianist , for not only this 
 clever young lady appear , but on the first night of the 
 Festival , when Dr. Grieg be to have be present , 
 Mr. Borwick play the norwegian composer 's single 
 pianoforte concerto to perfection ( give also Saint - Saéns ’s 
 brilliant ' Africa ' jater on ) , and on the last night of the 
 meeting , M. Paderewski play in Beethoven 's ' Emperor ' 
 concerto , and his own beautiful , poetic , and immensely 
 difficult ' polish Fantasia . ' he be hear at his good in 
 the latter work , and be require to play an encore 
 give Mendelssohn 's ' song without word ' in b fiat , 
 with the strong flavour of the chase about it 

 the splendid male - voice organisation of 350 voice 
 which , under Mr. Riseley ’s direction , have attain such 
 eminence , take part in Mendelssohn 's ' Antigone ' music 
 and Grieg ’s ' Landerkennung , ' the vocal soloist in each 
 be Mr. Plunket Greene , and the reciter in the former 
 Mrs. Brown - Potter and Mr. Rudolph de Cordova . 
 Mrs. Brown - Potter also declaim the ballad ' Bergliot , ' 
 for which Grieg write music ; she would come near the 
 ideal of the elocutionist ’s art if , when recite ' through 

 the NORWICH MUSICAL FESTIVAL . 
 ( by our special correspondent 

 the twenty - seventh triennial Festival at Norwich , and 
 the eighth which have be under the artistic direction of 
 Mr. Randegger , take place on the 21st to the 24th ult . 
 for the general public the interest of a festival lie 
 chiefly in its relation to current art , so I shall not 
 apologise for baldly record performance of such 
 well - know work as ' Elijah , ' ' the golden Legend , ' 
 Verdi 's ' Requiem , ' ' the redemption , ' and Beethoven 's 
 c minor symphony , and concentrate my criticism on 
 the less hackneyed feature of the Festival 

 the first to be hear be Sir Hubert Parry ’s ' ode to 
 Music , which be , of course , not a novelty in the strict 
 sense of the word , having be hear on two occasion 
 at Royal College concert . but its first public appearance 
 be at the open concert of the Festival . suffice it to 
 say that the noble music fit Mr. A. C. Benson 's fine 
 poem ' like a glove . ' the solo part be not prominent , 
 but there be five of they , which be take by Madame 
 Albani , Miss Kate Moss , Miss Ada Crossley , Mr. Ben 
 Davies , and Mr. Robert Radford 

 result of Mr. Carnegie ’s benefaction 

 at the Palette Club ’s usual monthly concert , on the 
 Ist ult . , the orchestra of the Club , under Dr. Hardy , 
 give a highly creditable performance of Berlioz ’s ' King 
 Lear ' Overture , Beethoven ’s C minor Symphony , and 
 German 's ' Henry VIII . ' dance . vocal solo lend variety 
 to an interesting programme 

 it be much to be regret 

 although the musical work of our Training Colleges 
 here do not come under public notice in the usual way , 
 it may be of interest to mention the choral music select 
 for study during this session . at Nétre Dame College , 
 they have in hand Wilfred Bendall 's ' the Lady of 
 Shalott , ' Mendelssohn ’s ' laudate pueri , ' and Schubert 's 
 ' Serenade ' for alto soloist and chorus ; at the Church of 
 Scotland College , Arthur Somervell ’s ' ode to the Sea , ' 
 Part iii . of Coleridge - Taylor ’s ' Hiawatha , ' and Elgar ’s 
 ' the Banner of St. George ' ; at the United Free Church 
 College , Haydn ’s ' Spring ' and Gounod ’s ' Gallia 

 Mr. Philip E. Halstead , a lead local pianist 
 associate with the Verbrugghen Quartet ( Messrs. 
 Verbrugghen , Freeman , Haigh and Foulds ) give the first 
 of a series of four chamber concert on the 2oth ult . 
 the programme include Beethoven ’s fourteen variation 
 for pianoforte , violin , and . violoncello , Schubert ’s string 
 quartet ' death and the maiden , ' and Dvorak ’s quintet 
 for pianoforte and string ( Op . 81 ) . with regard to both 
 ensemble and general intelligence the performance be 
 one of great merit 

 MUSIC in GLOUCESTER and DISTRICT 
 ( from our own correspondent 

 Professor Horatio Parker be present at the first 
 rehearsal of the Gloucester Orpheus Society , and 
 promise to write a part - song especially for the Society . 
 an item of the programme for the Society ’s annual 
 concert on January i , 1903 , be a setting of a Coronation 
 Ode , write by Mr. H. Godwin Chance ( a member of 
 the Society ) and set to music by Sir Hubert Parry , its 
 President 

 the programme for the season issue by the Chelten- 
 ham Musical Festival Society ( under the direction of 
 Mr. J. A. Matthews ) include Elgar 's ' coronation Ode , ' 
 ' the hymn of praise , ' ' the golden legend , ' the 
 ' Messiah , ' and Beethoven 's ' Eroica ' Symphony 

 the Cirencester Choral Society be rehearse Sir 
 Charles Stanford 's ' revenge ' and Bennett 's ' Mav 
 Queen 

 music in LIVERPOOL and DISTRICT . 
 ( from our own correspondent 

 the first concert of the sixty - fourth season of the 
 Philharmonic Society take place on the r4th ult . M. 
 Kocian make his first appearance in Liverpool , and the 
 talented young violinist achieve a large measure of 
 success . Mr. Andrew Black appear in the place of 
 Mr. Van Rooy , and be in particularly good voice , his 
 singing of ' Wotan ’s Abschied ' ( ' Die Walkiire ' ) be 
 excellent . the orchestra , under Dr. Cowen , and with 
 Mr. A. W. Payne as principal violin , play Beethoven ’s 
 C minor Symphony with much care . the chorus be 
 let off lightly with one item , to wit , Sullivan 's ' Song of 
 Peace ' from ' On‘Shore and Sea 

 M. Godowsky make a great impression on his first 
 appearance in Liverpool , when he play at the Small 
 Concert Room , St. George 's Hall , on the 16th ult . ; anda 
 large audience welcome M. Paderewski after an absence 
 of several year , at his recital at the Philharmonic Hall , 
 when he achieve all his usual success 

 MUSIC in MANCHESTER . 
 ( from our own correspondent 

 though the string quartet form in Manchester by 
 Dr. Brodsky have only be in existence a few year , it may 
 now be regard as firmly establish among the institu- 
 tion of musical Manchester . not having hear his early 
 Leipzig quartet — which include Hans Becker , Novacek , 
 and , I think , Klengel — I can draw no comparison . but 
 that the Manchester combination can hold its own among 
 the fine quartet in existence be now generally recognise . 
 at the first Brodsky concert of the present season , which 
 take place on the 8th ult . , Mr. Simon Speelman , 
 the unsurpassed viola- player , be absent from 
 his usual place , in consequence of recent family 
 bereavement , but a remarkably efficient substitute 
 have be find in Mr. Catterall — Dr. Brodsky ’ 's 
 young pupil — who be rapidly win recognition as a 
 string - player of first - rate ability ( having already become 
 a member of both the Hallé and Bayreuth orchestra ) . 
 in other respect the quartet be constitute as usual — 
 second violin , Mr. Rawdon Briggs , violoncello , Mr. Carl 
 Fuchs . the programme consist of Volkmann 's G minor 
 and Haydn 's B flat major Quartets , and Beethoven 's 
 third Sonata for pianoforte and violin . the last - name 
 work , which naturally occupy the middle place in the 

 programme , afford an opportunity of hear a youn 
 pianist name Edward Isaacs,—like Mr. Catterall , 4 
 pupil of the Royal Manchester College — who in the 
 main acquit himself very satisfactorily , play 
 with neatness and good taste , and fall short 
 of masterly style only by reason of a slightly dry 
 tone . the single point that in any way detract from 
 the success of these concert be the want of a satisfactory 
 hall 

 with Dr. Brodsky ’s first concert , the Manchester 
 musical season may be say to have open , though one 
 or two song recital and the first performance here of 
 German 's ' Merrie England ' come early . for the 
 great public the real opening ceremony be , however , 
 the Hallé concert of the follow Thursday , when , with 
 four masterly work in as many different and sharply 
 contrast style , Dr. Richter prove that the Hallé 
 orchestra maintain in nearly all respect the extremely 
 high standard to which he have bring it by the end of last 
 season . buti begin to fear that some occasional blatancy 
 in the heavy brass instrument be an ineradicable fault of 
 the present combination . here one trace the influence 
 of those brass - band contest so prevalent in the North of 
 England . it be true that Dr. Richter have very greatly 
 mitigate the fault in question , but it be still liable to crop 
 out again early in the season . the programme consist 
 of the ' Meistersinger ' Overture , Strauss ’s ' Tod und 
 Verklarung , ' Elgar ’s variation , and Beethoven 's seventh 
 Symphony . though there be no soloist the concert be 
 extremely well attend , and Dr. Richter ’s reception be 
 most enthusiastic 

 at the concert give on Monday , the 2oth ult , 
 by that most ancient — and at the present time 
 wonderfully flourishing — institution the Gentlemen 's 
 Concert Society , the same world - famous conductor 
 preside over a small orchestra , and charm a more 
 exclusively fashionable audience with his interpretation 
 of Sterndale Bennett 's ' naiad ' Overture , Schumann 's 
 Overture , Scherzo and Finale , the two new dream piece 
 by Elgar , and Handel ’s ' partenope ' Overture , the final 
 piece take from the programme of a concert give by the 
 same Society , or its lineal predecessor , about the middle 
 of the eighteenth century — I mean the eighteenth , not the 
 nineteenth ! the singer be Miss Alice Nielsen , whose 
 youthful and vivacious manner lead to happy result in 
 the cavatina from ' Rigoletto ' than in Gounod ’s ' Jewel 
 Song . ' in the Verdi air the audience have the unwonted 
 pleasure of hear Dr. Richter play a pianoforte accom- 
 paniment , which , in spite of finger cramp by 
 overlong holding of the baton , he do not fail to bring 
 out with exquisite rhythmical elasticity 

 MUSIC in YORKSHIRE . 
 ( from our own correspondent 

 it must be that Yorkshire musician , though energetic 
 enough when they do get to work , be slow in put on 
 their harness , for at the time of write I findi have only 
 one concert of first - rate importance to record . there be , 
 however , the pleasure of hope to indulge in , for the chief 
 concert - give society have issue programme of a 
 fairly interesting nature . the Leeds Philharmonic and 
 Subscription Concerts , for example , will have give 
 ' Israel in Egypt ' before this appear in print , and 
 promise also Stanford 's ' Eden , ' Richard Strauss ’s 
 ' Wanderer ’s Sturmlied , ' C. Wood 's ' dirge for two 
 veteran , ' the ' Vatergruft ' of Cornelius , and Elgar 's 
 pleasant choral suite , ' from the Bavarian Highlands . ' 
 the Leeds Musical Union be give Elgar 's ' Caractacus ' 
 and ' Coronation Ode , ' Dr. Parker 's ' Star Song , ' Gluck ’s 
 ' Orpheus , ' and Parry ’s ' Song of Darkness and Light , ' 
 while they be pleasantly vary the invariable Christ- 
 ma ' Messiah ' concert by give a selection from that 
 work , together with portion of an equally venerable , but 
 less hackneyed composition , the ' Christmas Oratorio ' of 
 Bach . at the Bradford Subscription Concerts it be not 
 propose to give any choral work of peculiar interest , 
 but Beethoven 's Choral Symphony and the third Act of 
 ' Lohengrin ' will take up one programme , and ' St. Paul ' 
 a second 

 the Bradford Festival Choral Society , besides supply- 
 e the chorus on these occasion , will give on its own 
 account Dr. Cowen ’s ' Coronation Ode , ' and be to 
 perform , on the 24th ult . , too late for notice , ' Israel in 
 Egypt , ' which , after be shelve for some time past , 
 seem to be enjoy a ' run 

 HamBurG.—Massenet ’s remarkable operatic legend , 
 ' Le Jongleur de Notre - Dame , ' with its poetical and 
 dramatically effective libretto , be produce at the 
 Stadt Theater for the first time in Germany , on 
 September 25 , under capellmeister Gille ’s direction , 
 and with very efficient artist in the solo part 

 Leripzic.—A series of subscription concert , give by 
 the excellent Chemnitz Municipal Orchestra , be open 
 on the 6th ult . , under Herr Weingartner ’s direction . 
 the programme be devote entirely to Liszt 's com- 
 position , and include the symphonic poem ' Tasso ' 
 and ' Hungaria , ' and the Pianoforte Concerto in A major , 
 Herr A. Reisenauer be the pianist . the first 
 Gewandhaus concert of the season , uader Herr Nikisch ’s 
 conductorship , be dedicate to the memory of the late 
 King Albert , and include vocal number from Handel 's 
 oratorio ' Deborah ' and Bach ’s cantata ' Ich habe genug , 
 as well as Wagner ’s funeral dirge from ' Gétterdam- 
 merung , ' and Beethoven 's ' Eroica ' symphony . it wasa 
 deeply impressive performance . — — Herr Stephan Krehl 
 have be appoint to the senior professorship of com- 
 position at the Conservatorium , in the room of the late 
 Professor Jadassohn 

 Lunp(SWEDEN).—The doyen of scandinavian organist , 
 M. Delan , chapelmaster of the cathedral , recently cele- 
 brate his eighty - eighth birthday . he have pursue his 
 avocation without interruption for over sixty year 

 March , by Halversen . Mr. D. Sinclair play a move . 
 ment from Weber ’s Concerto in E flat asa clarinet solo , 
 and Miss E. Barber and Mr. W. A. Bowring be the 
 vocalist 

 Winpsor.—The programme of Messrs. Thomas Dunhill 
 and Edward Mason ’s third concert of Chamber Music 
 at the Royal Albert Institute on the 9th ult . be very 
 interesting . it comprise Beethoven 's Quartet for 
 string ing major ( Op . 18 , no . 2 ) , a MS . quintet for 
 string and horn in F minor by Mr. Dunhill , and 
 Mendelssohn ’s Concert variation for pianoforte and 
 violoncello , Op . 17 . the artist include the Grimson 
 Quartet and Mr. Hale Hambleton , in addition to the 
 concert - giver , with Miss Perceval Allen as vocalist 

 Correspondence 

 in non - apostrophise the place where the remain of 
 Dr. Crotch lie bury , we follow the form of its name 
 as state in the legal notice affix to the door of the 
 church at the time of our visit ; and as the various issue 
 of so accurate a book of reference as Kelly 's Directory of 
 Somerset and , indeed , ' the Clergy List , ' give the same 
 form , we certainly err in very good company.—ep . M.T 

 JBEETHOVEN ’S HOUSE 

 dear Sir,—Will you permit I to correct a slight 
 error which find expression in last month ’s Musicat 
 Times ? it be there state that the Schwarzspanierhaus — 
 the Sterbehaus of Beethoven — bear no commemorative 
 mark of the fact that the great master draw his last 
 breath within those wall 

 visitor who approach this interesting spot from the 
 Beethovengasse , which lead into the spacious courtyard , 
 will look in vain for such a sign ; but the outside of the 
 building , which form part of the Schwarzspanierstrasse 
 — and here be situate the room which Beethoven 
 occupy — bear a small tablet , which state in _ the 
 briefest term the fact , together with the date of the 
 great composer 's death 

 the inscription , time and weather - wear , be none too 
 clear , and may easily be overlook . still , there it 15 , 
 and in justice to the music - lover of Vienna , I shoul 
 like to testify from personal observation that they have 
 not overlook the sombre day towards the end of 
 March , 1826 , whose record make sad the heart of 
 musician . — yours faithfully 

 A. R. C. O.—The work here enumerate be without 

 soprano and contralto solo : ' Festgesang ' ( Mendelssohn ) , 
 ' Rock of Ages ' ( Bridge ) , ' Zion ' ( Gade ) , ' song of 
 Thanksgiving ' ( Cowen ) , ' the procession of the Ark ' 
 from the ' Rose of Sharon ' ( Mackenzie ) . 
 _ ScopuLus.—(1 ) there be much to be say fro and con 
 in regard to the sonatina in F and G ascribe to 
 Beethoven , but perhaps the balance of opinion be in 
 favour of the con . ( 2 ) try Naumann ’s ' History of Music ' ; 
 the chapter on english music be by Ouseley 

 portion.—(1 ) Mr. J. Brotherhood , of New York , may 

