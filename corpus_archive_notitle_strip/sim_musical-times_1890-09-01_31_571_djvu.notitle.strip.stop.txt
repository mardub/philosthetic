T ° Pr ANOF ORTE REPAIRE RS TUNERS . — 
 WANTED , MAN thoroughly acquainted Repairs 

 Pianofortes , Harmoniums , American Organs ; fair 
 Pianoforte Tuner , thoroughly competent repairing work ; 
 represent Proprietor Salesman , & c. , 
 absence home , able try Pianoforte . man 
 experience , gentlemanly address , ¢ strictly : sober habits find 
 . ¢ » mfort able position . sal : ary small , situation perma- 
 ent healthful city . “ Address , Beethoven , Messrs 

 ‘ ello , Ewer Co. , 1 , Berners Street , W 

 NOVELLO , EWER CO . MUSIC PRIMERS . 
 . 34 . 
 Epitep sy SIR JOHN STAINER 

 ANALYSIS FORM 
 DISPLAYED 
 BEETHOVEN 

 THIRTY - 

 Edinburgh 

 calls Art - Ballad . Loewe fore- 
 runners domain vocal music , gives special 
 prominence Mozart , Beethoven , Reichardt , Zelter , 
 Zumsteeg . , - named 
 led comprehensive nature genius 
 seek wider field expression musical 
 thoughts afforded isolated songs — majora 
 canebant . adhered closely 
 essentially national song , accompani- 
 ment plays purely subordinate , con- 
 sidered originators new species vocal 
 music . service reserved Schubert 
 Loewe , Mr. Bach describes 
 greatest song composer , con- 
 siders greatest writer ballads . 
 difference Volkslied Art - Song 
 carefully considered , radical distinguishing 
 mark words Volkslied 
 tunes , Art - Song words 
 generally capable achieving separate immor- 
 tality account . , how- 
 , Volkslieder exerted wonderful influence 
 alike poets composers , greatest 
 symphonic music grown simple national 
 dance tunes , finest specimens Art - Song 
 idealised national songs . 
 field literature calculated stimulate 
 creative powers song writer forcibly 
 ballad , combining limited compass 
 epic , dramatic , lyric elements , suffused , 
 , supernatural mystical atmosphere . 
 impetus given lyrical poets 
 Germany publication German dress 
 ‘ Percy Reliques ” ’ amazing 

 draughts - springs 
 inspired Birger , Goethe , Uhland , 
 world wealth splendid subjects 
 musical illustration . deriving 
 origin anonymous productions early 
 minstrels , later ballads demanded 
 elaborate musical treatment hitherto 
 accorded songs people , 
 genius Schubert Loewe grasped 
 situation , developed new form art . old 
 strophic form gave place “ - composed ” ’ 
 musical framework . simple harmonies , 
 previously regarded sufficient , 
 superseded richer accompaniment , enhancing 
 reinforcing voice individuality 
 appropriateness . composers 
 named realised char- 
 acter took action poem , music 
 find distinct accents foreach . lyrical , 
 dramatic , descriptive turns . achieve 
 demands talent common order , 
 realise exigencies beset 
 composer ballads constitutes slight claim 
 remembrance . Loewe , according Mr. Bach 
 — means opinion — 
 credit attaches having taken new 
 departure . “ elevated ballad 
 musical drama miniature ; 
 indicating esthetic principle unity 
 epic , lyric , dramatic elements , 
 created model future productions 
 kind 

 easily mistaken . poor 
 fellow undergoing painful tedious treat- 
 ment hospital . know process , | 
 use sermons antique instrument | 
 called barrel organ , believe 

 glad hear collection per- | 
 formances Beethoven C minor Symphony | 
 complete . procured spectro- | 
 phonograph given certain Mons . Jullien , | 
 amusing , — gem collection — | 
 performance work theatre | 
 Vienna 1808 . hardly imagine | 
 days audience listened | 
 vile performance . save | 
 particular friends ; stagger faith 
 Beethoven 

 danger having , 
 strike musical profession . certain entre- 
 preneury lately took concert - party touring 
 provinces , persuaded accept sharing 
 terms — illegal arrangement days . 
 lost heavily , artists naturally grumbled . 
 Unfortunately included company 
 promising debutante , paid bring . 
 excuse , com- 
 municated Vocal Union , Violinists ’ 
 Union , Pianists ’ Union , powerful 
 bodies instantly came manager 
 peremptory command dismiss * black - leg ” 
 ( , know , isthe expression non - unionist ) , 
 pay heavy fine illegal agreement . 
 rashly refused , having , tact , lost money , 
 general turn - Greater London im- 
 pending . Managers boast depend | 
 amateurs fill vacant places , 
 operatic stage wooden puppets , phonographs 
 inside , , course , form efficient substitute ; 
 know artists conquer 
 end , bloated capitalists forging | 
 chain 

 Royal Institution yesterday curious new | 
 machine exhibited inventor . calls | 
 Musical de - composer , piece music | 
 minutes resolved com- | 
 ponent phrases , traced ] 
 origin . cruel invention , laying bare | 
 unpleasant truth greatest 
 masters real originality scarcely said | 
 existed . Poor Beethoven came singularly badly , 
 theme phrase * Eroica , ” instance , 
 referred till lost 
 echoing ages . experiment , regret 
 , caused accident machine , 
 proved unequal strain , cracked right 
 . work , stubbornly refused 
 decomposed , “ King Lear ” Overtufe | 
 Berlioz . machine doubtless improved | 
 long 

 , cylinder nearly , 
 conclude . sending electric post pair | 
 improved microphones fit invisibly 
 inside ears , means hear | 
 coming sounds nearly minute 
 emitted . look confidently forward day 
 valuable invention shall developed | 
 means hear music otf 
 far future ease revive 
 sounds remote past . shows true 
 door communicate dead 
 gone ancestors . shall copy 
 phonogram , hope day science 
 enable send centuries 

 F.C 

 vexed question connection 
 music morals - opened 
 described temperate article 
 recent number Universal Review . Mr. H. 
 Arthur Smith , writer article question , 
 takes granted connection . 
 disputants author American 
 novel “ Janus , ’ upholds view 
 influence music immoral , 
 writers argue non - moral — zi.e . , 
 music , apart associations , mora ! 
 immoral — Mr. Smith think worth hi : 
 reckon . concerns need 
 showing far musical ethics formulated . 
 questions interested : 
 ‘ * musical Zolas Rabelais , Don Juans 
 Cencis expressed mere sound ? — works , 
 given higher level culture comprehension , 
 little think introducing 
 families read aloud ‘ Les Conies 
 Drolatiques ’ ‘ ] Decamerone . ’ ” Mr. Smith 
 proceeds adduce certain analogies 
 considers ‘ sufficiently illustrate faculty 
 ear , unaided intellect evoke emotion , ” 
 asks question — * emotion 
 aroused suchcharacter degree definiteness 
 leave distinctly ethical aspect ? ” 
 affirmative answer Mr. Smith evidently 
 considers possible interrogatory 
 inspires hope “ rudiments 
 branch moral science hitherto unobserved ” 
 — Plato — ready hand , 
 consequence morally incumbent masters 
 guides art exercise special care 
 discrimination prescribing lines study thei 
 pupils . order justify contention , Mr. Smith 
 takes - known instrumental works , dealing 
 theme — Death — 
 judgment hearer disturbed 
 association words , proceeds 
 impression convey mind . 
 Handel Dead Marchin “ Saul , ” Funeral 
 March Beethoven flat Sonata ( Op . 26 ) , 
 Wagner ‘ “ Trauermarsch , ” Chopin * Marche 
 Funébre . ” , according Mr. Smith , breathes 
 spirit solemn Christian resignation mixed 
 ‘ feel death 
 majestic dreadful , exclaim ‘ 
 thy sting ? thy victory ? ’ ” ’ Beethoven 
 March , contrary , suggests Mr. Smith 
 view Death identical enunciated 
 hero Homer , said 
 poorest slave earth reign dead . 
 Unmitigated hopeless sorrow brood 
 . ‘ “ Trauermarsch ” finds heroic , pas- 
 sionate , Pagan — defiant despairing ; 
 Chopin famous march labels sickly 
 realistic sensuous — death Morgue . 
 test Mr. Smith applied , ingenious 
 , serves display precarious 
 fluctuating basis new science 
 musical ethics . candidly admits , com- 
 parisons speak one’s - self . ‘ * 
 impression inevitably suggest different trains 
 thought feeling different hearers . ” 
 , impressions , cultivated 
 hearers , diametrically opposed . 
 Count YTolstoi proclaiming presto 
 ‘ ‘ Kreutzer ’’ Sonata direct incentive basest 
 passions . ‘ criticism , ” writes Mr 

 53 

 favourite pieces , encore acknowledgment 

 applause interfere development plot . 
 delightful little - act opera , Berlioz , ‘ ‘ Beatrice 
 Benedict , ’ newly arranged stage , preceded ballet 
 17th , 18th * * Lohengrin ” played 
 2ooth time Vienna Opera—6o times old 
 house , 14o present beautiful temple . 
 stir Festival gaiety , generosity 
 spared expense , gorgeously mounted opera , 
 mind musical pilgrim turns irresistibly , 
 quiet reverence , little sanctuary Heiligenstadt , 
 Beethoven offered worship God Nature ; 
 upper room Schwarz Spaniergasse , 
 great spirit winged flight storm ; 
 chamber death , Mozart gentler spirit 
 attained ‘ ‘ Requiem AZternam . ” present triumph 
 - living genius makes grudge rest 

 nr 

 formed present day , introduction played 
 viole d’amour , remainder obbligato 
 given viola . matter regret 
 instrument longer extensively studied 
 , attempt revive use 
 welcomed encouraged far possible . 
 present compositions double value , 
 available viola viole d'amour , 
 “ * Deux Morceaux Lyriques ” ’ Andante 6 - 8 
 time key D major . features 
 piece writing undesirable . 
 somewhat careless mistake notation twenty- 
 eighth bar accompaniment . bar commences 
 | C sharp , followed beat C 
 natural , followed fourth beat C 
 sharp . harmony requires B sharp , C natural . 
 | like fault repeated recurrence phrase . 
 | Andante followed bright commonplace 
 | melody B minor , marked alla marcia . com- 
 | positions , unpretentious , found useful 
 | additions scant répertoire performer 
 viola viole d’amour . firm publishes 
 ‘ * Solitude , ’’ reverie viole d’amour violin , 
 “ Sur le lac , ” serenade viole d’amour 
 violins . good intentions , 
 somewhat marred consecutive fifths 
 found - sixth - seventh bars 
 accompaniment . melodic phrase 
 rhythmic tuneless nature distinguishes 
 - called ‘ music hall ’ songs present day . 
 | , ‘ Sur le lac , ” popular 
 solo viole d’amour violins , 
 care evidently taken arrangement 

 Passages Works L. van Beethoven . 
 pianoforte solo wellas accompaniment . Collected , 
 | fingered , brought form ‘ “ Daily Exercises , ” 
 Giuseppe Buonamici . 
 ‘ Florence : G. Venturini ; London : Novello , Ewer & Co 

 idea suggested compilation work 
 | altogether happy . reality 
 student actual passages taken works 
 | master preparation complete study 
 works , devised 
 hand able skilful . knowledge 
 work study based plan large 
 valuable knowledge formed accumulated 
 serve best incentive pupil . 
 increase interest works 
 passages belong , actual compositions 
 entered . course practice , , 
 { peculiarities construction likely 
 iclearly displayed , higher reverence genius 
 | composer likely inspired . exercises 
 |also form excellent introduction writings 
 |of musicians modern school Beethoven 
 lis point departure . , passages 
 form excellent schooling , 
 |studied advantage aspire attain 
 | freedom finger solidity knowledge techni- 
 | calities pianoforte . ‘ book divided seven 
 | sections — ‘ ‘ bundles , ” frequently called — 
 | progressive character . treats scales 
 | keys , second arpeggios , staccato 
 | passages , fourth repeated notes , thirds , fourths , 
 sixths , broken sixths ; fifth shakes , octaves , 
 | broken octaves ; sixth mixed passages , seventh 
 |of rhythmical contrasts . comprehensive character ot 
 | book commend interested 
 |the advance - founded pianoforte playing . 
 | compiled pianoforte classes Royal Academy 
 lof Music , worthy come general use . 
 title - page instructions set forth 
 languages , French , Italian , German , English , 
 |valuable nature design prove Signor 
 Buonamici good friend students particular 
 music general 

 Pianoforte Fingering , English Foreign . 
 Oldershaw . [ Edwin Ashdown 

 thirds sixths , . . 10 

 Analysis Form displayed Beethoven Thirty - | ‘ * Presto , ’ described moto 
 Pianoforte Sonatas , description form | pe rpetuo , remarkably effecti Altogether 
 movement , use Students . H. A. Harding , | pieces , reason tic originality shown , 
 Mus . Doc . , Oxon . Novello , Ewer Co. Music Primers . | form valuable ad : ic present 
 . 34 . | Novello , Ewer Co. | | day 

 Tue student music grateful author 
 little work making easy 
 learned personal labour trouble . ies Fee Aye 
 dissection movements Sonatas | Novello , Ewer Co. 
 patiently , laid lines| Iw instalment Messrs. Bridge Higgs valuable 
 clear adopted late J W. Davison | edition bach organ works , grand 
 masterly descriptions Sonatas | old master finest elaborate compositions 
 time time performed Monday Popular Con- | instrument . mention superb Toccata 
 certs . author present work careful | Fugue C ( g-5 time ) , intermediate Adagio 
 toavoid savour poetical description , } minor , utterly variance Bach usual organ 
 task undertaken guidance cf students . | style ; Prelude Fuesue D minor ( 
 Properly , considerable educational value , | arranged Violin Sonata G minor ) ; grand 
 value increased proportion it| Toccata Fugue F major , examples 
 develops power observation analysis , | scarcely value . previous instalments 
 knowledge gained means pages | suggestions registering calculated prove great 
 profitably applied like manner concerns works | assistance student , play 
 elaborate fuxue varying stops , 
 chanves utterly alien spirit 

 FOREIGN NOTES 

 performances Berlin opera resumed 
 ist inst . 147 representations 
 beginning current year ; Wagner 
 represented thirty - , Verdi - , Mozart 
 Meyerbeer fourteen , Bizet seven , Nicolai , 
 Nessler , Beethoven , Weber , Hofmann perform . 
 ances works respectively 

 projected second opera house Berlin , 
 directorship Herr Angelo Neumann , approaching 
 realisation , necessary concessions having granted 
 municipality , required capital 
 subscribed . building erected 
 Potsdamer Strasse 

 Pauline Lucca , - known prima donna , 
 finally retire operatic stage , henceforth 
 | devote vocal training specially gifted 
 | pupils sexes . 
 | Anew comic opera , ‘ Die Fliichtlinge , ” Herr Raoul 
 | Mader , professor Vienna Conservatorium , 
 accepted performance coming autumn 
 Vienna Hofoper 

 Interesting hitherto unpublished details concerning 
 Beethoven romantic attachment Therese , Countess 
 Brunswick , Martonvasar ( Hungary ) , , appears , 
 great musician secretly betrothed period ot 
 years ( 1806 - 1810 ) , found small volume 
 entitled ‘ ‘ Beethoven unsterbliche Geliebte , ” ’ published 
 Bonn ( Peter Neusser 

 German Emperor presented leading musical 
 institutions conservatoriums Fatherland 
 copy recently published musical compositions 
 ancestor , Frederick Great 

 n gan cital given Mr. Lyle , Ors 

 ye Abbey . programr inc tad 1 pieces yur 
 Beethoven , Bach , Mozart , Har , Schubert , Batiste , Pleyel , Spinne 
 nd Chinnery 

 OINTMENTS 

 MONTH . 
 Published NOVELLO , EWER & CO . 
 RIDGE , J. FREDERICK . — ‘ Repentance 

 Nineveh . ” dramatic Oratorio . words selected 
 Holy Scripture , JoserpH BennetT. Paper cover , 2s . 6d . ; paper 
 boards , 3s . ; cloth gilt , 4s . Vocal parts , 1s . . 
 ENDL , KAREL . — ‘ “ Water - Sprite 
 Revenge . ” Cantata , Female Voices . English version 
 Rev. J. TRoutseck , D.D. ts . 
 AUL , A. R. — ‘ “ ‘ Virgins . ” sacred 
 Cantata , Solo Voices Chorus . Paper cover , 2s . 6d . ; 
 paper boards , 3s . ; cloth gilt , 4s . Tonic Sol - fa edition , 1s . Vocal 
 parts , 1s.each . Words , 5s . 100 , 
 ARDING , H. A.—Analysis Form , displayed 
 Beethoven Thirty - Pianoforte Sonatas ; descrip- 
 tion form movement . use students . ( . 34 , 
 Novello , Ewer Co. Music Primers . Edited Sir Joun 
 STAINER . ) Paper cover , 2s . ; paper boards , 2s . 6d . 
 \ JHITE , A. C.—The Double - Bass . ( . 32 , 
 Novello , Ewer Co. Music Primers . Edited Sir Joun 
 STAINER . ) Paper cover , 3s . ; paper boards , 3s . 6d 

 MONTH — continued . 
 ELVILLE , C. E.—‘‘Adestes Fideles 

 COMPOSED 

 L. VAN BEETHOVEN 

 English Version Rev. Dr. TrourBeck 

