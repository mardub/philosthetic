part I. 
 UNDAY evening at the HARMONIUM . 
 a selection of piece from the work of the most celebrated 
 composer , arrange by E. F. Ruspavtt . engrave and print in 
 the good manner . price 1 . net . post free , 13 stamp . 
 ART ii 

 SUNDAY evening at the HARMONIUM . a selection 
 of piece from the work of Handel , Beethoven , Bach , Mozart , 
 Gounod , & c. , arrange by E. F. Rimpavtr . price 1 . net ; post 
 free , 13 stamp 

 Metzler and Co. , 37 , Great Marlborough - street , W 

 the MUSICAL TIMES.—Junz 1 , 1869 

 MARSHALL HALL BELL ’S evening 
 concert , at the Beethoven Rooms , 27 , Harley - street , on 
 Thursday , 3rd June . Miss Roserting HENDERSON , Miss Anna 
 JEWwELL , Mr. Witsye Coorer , Mr. Henry Biacrove , Mr. J 
 Batsrr - CHATTERTON , and Mr. WaLTER MacFaRREN . 33 , Grove 
 End Road , St. John ’s Wood 

 Z 

 T have be at length fortunate enough , during last Lent , to get 
 possession of Mozart ’s score of the whole die /rx , as far as the 
 Lacrymosa , from a friend . the Requiem and Kyrie , which I copy 
 for myself from the original score , consist of five sheet , each leaf 
 number by Mozart separately , from 1 to 10 inclusive . the MS . 
 of the Dies /re , now in my hand , consist of eleven sheet , num- 
 bere 11 to 32 . the Lacrymosa begin at no . 33 . the Domine , 
 Quam olim , Hostias , and Quam olim da capo , be number from 34 
 to 45 , and be inthe hand of the Hofkapellmeister , Joseph Eybler . ' » 
 he then go on to describe their content , and 
 afterwards add 

 as soon asi obtain this Dies Jre , I show it to person 
 thoroughly acquaint with Mozart 's handwriting , who recognize 
 it at the first glance , and admire Mozart 's precision in the develop- 
 ment , the figuring , & c , rejoice heartily over the discovery , and 
 bore testimony that all relate to it which I have assert in my 
 ' defence , ' be exactly true . these person be Beethoven , 
 Eybler , Giinsbacher , Von Mosel , Kiesewetter , Doppelhof - Dier , 
 Smezcall , Streicher , Treitschke , Gyrowetz , Haslinger , Carl and 
 Joseph Czerny , Leidesdorf , Kandler , Sechter , Assmayer , and , 
 among other , Mozart ’s son , Wolfgang Amadeus , now here 

 Weber notice this second statement in the Cecilia , 
 no . 22 , but his article contain nothing worth 
 record 

 ment of a passage in the Kyrie fugue ] , and when we add to this 
 such specimen of Weber 's own manufacture as these [ quote 
 passage of Weber 's own setting of the Requiem ] , we be remind , 
 by Weber 's astounding knowledge of harmony and melody , of 
 the depart celebrity , Sterkel , [ an illegible name ] Kalkbrenner , 
 André , the father ( not the other one . * quite unlike he ) , & c. 
 Requiescant in paee . I especially thank you again , my revere 
 friend , for the pleasure your communication have give I . I 
 have always reckon myself among those who most honour 
 Mozart , and shall do so to my late breath . 
 " honoured Sir , give I your blessing soon . 
 " with great respect , 
 * * BEBTHOVEN 

 t the place where this early work could be 
 pow Stadler conceal this letter till after Beethoven 's 

 Weber notice André ’s edition in no . 23 of the | death ( March , 1827 ) , when he give it to a certain 
 Cecilia , 1827 , and reprint his preface entire . he ) Herr Schlosser , who straightway publish it , in fac- 
 admit its confirmation of the testimony of Stadler simile , in a short biography of Beethoven ( Prague , 
 and Siissmayer , but call attention to the passage 1828 ) . Weber copy the facsimile in the Cecilia 

 in Madame Mozart ’s and Nissen ’s letter , throw | 4nd answer some of Beethoven ’s remark ; but 
 direct an angry reproof to Stadler 

 doubt on some of the copy . but what principally | rar cite act ak aa etait 
 iversy at this time , be write at the end of 1828 , by 

 and have recognize as Mozart ’s composition . Weber 
 be overjoy at this discovery ; he take it for 
 grant that Mozart , in write the Requiem , know 
 the object for which it be intend , and he con- 
 clude that , although he give the Count enough , and 
 more than enough , for his money , he do not write it 
 with any care for his own reputation , or any view 
 to its publication under his own name 

 in No . 29 of the Cecilia ( Feb. 1828 ) , Weber com- 
 plain of the bitter and unfair personal attack make 
 on he in consequence of his article ; and especially 
 comment on a further provocation by Stadler , 
 which , as it introduce the name of a great man into 
 the dispute , be worth record . it seem that 
 Stadler send a copy of his brochure to Beethoven . 
 Weber have , some year before , sharply criticise the 

 have clear up much that be then obscure ; but it 
 li essential to notice the inference from they as to 
 |the extremely unsatisfactory state of the discussion 
 ‘ at that time 

 but now come an event in the history which pro- 
 mise to be of greaterimportance and interest than any 
 that have occur since Mozart ’s death . it will have 
 be observe that , during all the discussion raise 
 by Weber , which have not only agitate the entire 
 musical world for year , but which affect seriously 
 the fame of Mozart and the character of those be- 
 long to he , the person of all other most com- 
 petent to clear up the matter , namely , Madame 
 |Mozart , have remain obstinately silent . except by 
 ‘ the communication to André , we do not hear of her 
 lin any way . Nissen have die in 1826 ; but the widow 
 | be still live , surround by friend , in Salzburg , 
 and it be impossible either she or they could have be 
 ignorant of discussion which affect she so nearly . 
 in 1828 , however , the musical journal announce a 
 new Biography of Mozart , compile by Nissen and 
 edit by his widow ; and , of course , everybody believe 
 that the revelation withhold for a third of a century 
 would now take place ; and , indeed , the appearance of 
 the work at this peculiar time , naturally lead to the 
 conclusion that they have only be keep back so long 
 with the object of clear they up the more ex- 
 plicitly and conclusively in this way . . Oulibicheff 
 ( whose admirable book on Mozart and his work 
 we shall have occasion to notice hereafter ) give 
 an amusing account of how his own expectation 
 be raise ; the satisfaction with which he notice 
 the large size of the book when it be bring to 
 he ; and the eagerness with which , neglect food 
 and sleep , he devour its content , read through 

 Schlacht von Vittoria , and Beethoven , who , no doubt , 
 have this still rankle in his mind , answer in a 
 letter , of which the follow be a translation 

 6 Feb. , 1826 

 tue third and last of these Concerts be give at the 
 Hanover Square Rooms , on the 11th ult . , when Miss Zim- 
 mermann again prove her right to be rank in the first 
 class of intellectual pianist , play through a programme 

 of the most exacting work with an intelligent apprecia- 
 tion of her author ’s meaning , and an executive power 
 which can not be too highly praise . the selection in- 
 clude Beethoven ’s Trio in C minor , for pianoforte , violin , 
 and violoncello ; Mendelssohn ’s Duo , for pianoforte and 
 violoncello ( op 58 ) , and three " Pensées Fugitives , " for 
 pianoforte and violin , by Heller and Ernst . in the per- 
 formance of these composition Miss Zimmermann have the 
 valuable co - operation of Mr. Henry Holmes and Signor 
 Piatti . the one solo choose by the Concert - giver on this 
 |occasion be Schumann ’s Sonata in G minor ( ( ) p 22 ) , the 
 ‘ difficulty of which she surmount with the utmost ease ; 
 |and , indeed , we may say that we have rarely hear Schu- 
 |mann ’s music render so intelligible to an audience , the 
 |interpretation of the work be obviously not only a 
 | labour of love , but the result of careful and earnest study . 
 the solo vocalist be Madame Lemmens - Sherrington 
 | who sing a graceful song , by Miss Zimmermann , call 
 he oh ! that we two be maye " ( encore ) , and a trifling 
 ballad by Molloy , which should never have have a place in 
 a concert of this pretension . several part - song be ex- 
 | cellently give by Mr. Barnby ’s choir , and receive with 
 | the utmost favour 

 MR . HENRY LESLIE ’s concert 

 Tue last Subscription Concert of the season take place 
 on the 6th ult . a good selection of part - music be sing 
 by the choir with that perfection to which Mr. Leslie have 
 | now accustom his audience ; and amongst the rest the 
 | Concert - giver ’s Prize Madrigal , ' ' Thine eye so bright , " 
 which receive , as it deserve , the most enthusiastic 
 applause . Mr. Sims Reeves , who be to have sing a 
 number of his most popular song , be ill ; and Mr. Vernon 
 Rigby , who be send for , be not forthcoming ; so that 
 Mr. Massey ( a member of the choir ) have an opportunity 
 of singe " the Village Blacksmith , " which please so 
 much that he be encore . Miss Edith Wynne and Miss 
 Elena Angéle also contribute some song which be 
 highly successful ; and the Quartet Glee Union sing some 
 glee with much effect . the instrumentalist be Mr , 
 Blagrove ( violin ) , Mr. Brinley Richards ( pianoforte ) , and 
 Mr. De Jongh ( flute ) ; Mr. Blagrove and Mr. Brinley 
 Richards play Beethoven ’s sonata in F ( Op . 24 ) , which 
 be much applaud , and Mr. Richards perform a 
 brilliant arrangement of his own on " Weber ’s last 
 waltz , " the Concert be well attend 

 ROSSINI ’S MESSE SOLENNELLE 

 Tue right honourable Lady Elizabeth Low- 
 ther , who die in February last , have leave a legacy of £ 100 
 to the Royal Academy of Music 

 Tae New Polyhymnian Choir ’s Public re- 
 hearsal for May be another proof of the success attend 
 this rise choir . the programme comprise piece ap- 
 propriate to the season , include " now be the month , " 
 " come , bounteous May , " " Flora give I , " & c. , an 
 finish with Beethoven ’s " Hallelujah Chorus . " Dr. 
 Bennett ’s quartet , " God be a Spirit , " be also include 
 in the selection . the Rehearsal give great satisfaction 
 to a large audience . the rehearsal for the present 
 month will be entirely devote to the work of Men- 
 delssohn 

 we have be request to state that the 
 number of composition send in to the Secretary of the 
 Ely Diocesan Church Music Society , in competition for 
 the prize offer in our advertising column last Novem- 
 ber , for the good musical arrangement of the Nicene Creed 
 for parish choir , and the good organ accompaniment to 
 the creed when monotone , be so large , that a considerable 
 time must elapse before the adjudication of the prize can 
 take place . ' he competitor be not confine to England ; 
 India and America have both contribute . the Com- 
 mittee be engage in take such step as they trust will 
 give general approval for the decision to which they may 
 come 

 Mr. Joun Brinsmeap , of Wigmore Street , 
 Cavendish Square , have lately patent an invention for 
 perfect the check action of pianoforte , without in the 
 slight degree affect the repetition . the touch be 
 very sure , light , and elastic , give the performer every 
 facility for produce gradation of tone . so important 
 an improvement be of the utmost interest to pianist 

 Mavpame Evernr Oswatp ’s Morning Concert , 
 on the 17th ult . , at St. George ’s Hall , be attend by a 
 thoroughly appreciative audience . we have before men- 
 tione the claim of this pianist toa high rank in her 
 profession ; and have now to say , that on the present 
 occasion , throughout a well choose programme of classical 
 music , she fully sustain her reputation . her execution 
 of the Waldstein Sonata , of Beethoven , be alone a 
 convincing proof of her exceptional power , the last 
 movement especially , be throw off with such brilliancy 
 a to elicit the warm applause 

 Tue Concert of the Misses Gottschalk take 
 place on Wednesday evening , the 19th ult . , at Willis ’s 
 Rooms , before a large audience . Miss Clara Gottschalk ’s 
 quality as a well train pianist be successfully test 
 ina Waltz , of her own composition , and a Fantasia , 
 call " Flowers of Erin ; " ' and Miss Gottschalk also 
 receive marked applause for her performance of a duet 
 with Signor Regondi . Miss Blanche Gottschalk be equally 
 successful as a vocalist , especially in Berger ’s song , " ' the 
 Syren . " the conductor be Mr. Franz Berger and Mr. 
 Osborne Williams 

 Ly 

 from Schwerin ( bass ) ; Herr Vogl , from Munich ( tenor ) ; 
 Herr Grutzmacher , from Dresden ( violoncello ) ; and Herr 
 Joachim ( violin ) . |the conductor be Rietz , Hof . 
 kapelmeister at Dresden , and Tausch , music director of 
 Dusseldorf . the principal work perform be Handel 's 
 Oratorio Joshua ( the chorus singing in which be describe 
 as far superior to any ever yet hear in England ) , Mendel . 
 ssohn ’s Lodgesang , and the sublime " Magnificat , " by Sebag . 
 tian Bach , with modern instrumentation by Robert Franz , 
 besides these important composition , there be an ex . 
 cellent selection of first - class music , include Beethoven 's 
 Violin Concerto , exquisitely play by Herr Joachim , 
 Ata Matinée give to the eminent artist assemble in 
 Dusseldorf , a youth of fourteen , Julius Réntgen , perform 
 on the pianoforte three prelude and fugue of his own 
 composition ; and a duo for violin and viola , by the same 
 youthful composer , be also play by Herr Joachim and 
 Réntgen , sen . , both work manifest a precocious genius 
 which will no doubt shortly assert itself to the world 

 rebieln 

 accommodate the lapse of thirty - one day ( as do the usy 
 of the Musical Times ) , between the rendering of any ong 
 piece and the next ; and any hearer , therefore , who js 
 sensitive to the effect of musical transition , will ty 
 shock or delight , accord to the conservatism 
 republicanism of his proclivity , by the extraordi 
 change from the key of f to the key of e , without ong 
 chord of kindly intervention , in passing from the " Glorig 
 in excelsis " to the " Credo in unum deum 

 it be remarkable in the present setting of the text , that 
 s|the word " Credo " be make to recur as the heading of 
 every article of belief ; whereas , many purist maintain 
 $ | that the text of the Church isa inflexible as her doctrine , 
 3|and that though a word , and even a phrase may be iterate 
 when no other phrase occur between its repetition , the 
 recurrence to any word or phrase from a previous sentence , 
 involve the transposition or inversion of the text , ij 
 quite unorthodox . there be , indeed , the precedent of the 
 3| " Credo " in Beethoven ’s Mass in D , wherein the same 
 word be in like manner , though not to the same extent , 
 repeat ; but the composer of this wonderful work be 
 scarcely so revere an authority for ecclesiastical as for 
 musical propriety . there be also the precedent of the 
 english version of the Nicene Creed , wherein the corres . 
 ponde word , ' I believe , " thrice recur after the com . 
 mencement ; but it be doubtful if a good Romanist would 
 accept any instance of anglican heresy as valid authority 
 for tamper with the veritable wording frame by a 
 Council of the Church . even these two authority in 
 art and in doctrine , however , Beethoven and our Book of 
 Common Prayer , afford no example of recurrence to any- 
 thing but the one general declaration of belief , which , as 
 it apply to the entire Creed , so it may be applicable to 
 each of its several article ; but Rossini do more than 
 this , in the recurrence , at the close of the piece and else- 
 where , to the initial phrase , " Credo in unum deum " " — 
 not for the sake of bring back a musical idea , as Mozart 
 do in his Litany in e flat , and Beethoven in his Mass in 
 C , though both without dislocate a single word of the 
 text,—but seemingly , since no other object be discernible , 
 for the purpose of yive a new reading to the time 
 honour summary of the decision of the Council of 
 Nice . shall we then suppose the idler of forty year to 
 have wear but a mask of gaiety in that free commerce 
 with the world which win for he countless admirer of 
 his witticism and his courtesy , and to have have con- 
 stantly behind this an undercurrent of seriousness in his 
 contemplation of the Church , which may yet win for he 
 admirer of his modification of her ordinance 

 the first movement comprise all the word of the 
 « Credo " down to the important " et homo factus est . " 
 the chorus and the quartet of solo voice be employ 
 in alternation , apparently for the sake of musical variety , 
 since with no obvious reference to any more or less per- 
 sonalism in one or another article of the text , or to any 
 particular expression of one or another passage . — the 
 many and the extreme modulation that variegate 
 this Allegro Cristiano — a novel , if not a significant 
 definition — seem also to have no other than a technical pur- 
 pose ; at least , itis beyond I to connect any of they 
 with the expression of the word . as little can I trace 
 any meaning that can join sound with sense in the several 
 recurrence , in different key and to different portion of 
 the text , of the very ordinary musical phrase , first set to 
 the word ,   factorem ceeli et terre . " perhaps the most 
 extraordinary passage in the piece be that set to the word 
 « et incarnatus est , " repeat a semitone higher to " de 
 spiritu sancto , " and again repeat another semitone higher 
 to " Ex Maria Virgine ; " it isextraordinary in its harmony 
 extraordinary in its notation ( have a progression from 
 f flat to d sharp , for instance ) , extraordinary in its part- 
 writing , extraordinary in its voicing , and very extraordinary 
 in its effect ; and its pertinence to the purport of the 

 ee 

 word , physical and metaphysical , be to I as\imperceptible 
 as its construction be extraor : 
 possibly the writer have no intention of particular 
 ssion in his rendering of any one or another de- 
 daration of faith or utterance of sentiment , but aim 
 rather at the general embodiment of some idea of the 
 text a8 . a whole , and of its signification in the abstract ; 
 and thus he may have emulate the practice of those 
 ‘ mitive Church musician who give to their music a 
 maling character of solemnity , but regard not the special 
 getting of any of the wordsor idea . thus , perhaps , may 
 be account for Rossini ’s always appropriate a discord 
 to the word " Credo , " whether in the accompaniment 
 qhen the voice be in unison , or among the voice when 
 they sing the harmony ; since by all principle of musical 
 ion , if he have any esthetical design in this treat- 
 ment of the text , it can but be interpret as represent 
 the act of faith to be one of pain , or of discomfort at 
 least , to the believer . to meet he on his suppose 
 ground of technical and not « sthetical purpose , however , 
 it must be protest that this french habit of dart into 
 the remote recess of tonal affinity , induce none of the 
 solemnity of effect that characterise the early music of , 
 the Church , but cause a ceaseless vexation of the ear 
 and irritation of the whole nervous system , which be 
 antipathetic to the gravity and repose usually associate 
 with religious reflection . the movement be strongly 
 coloured in the orchestration , of which the same gener- 
 alitie may be say as of any of the previous movement 
 wherein all the instrument be bring into use . once 
 for all may here be mention the employment of a third 
 bassoon , after the manner , for the most part , of that in 
 which Haydn and Beethoven sometimes write for the 
 double bassoon ( Contra fagotto ) , but without the chance 
 of the same effect of rich sonority that this fine instrument , 
 whose use have be recently restore to our orchestra 

 produce 

 tion of composer seldom make way , even at Festal- 
 ora , for the mere vigorous work of Mozart , Haydn , and 

 Beethoven . 
 [= ss — q — 
 griginal correspondence . 
 | to the editor of the MUSICAL TIMES 

 sin,—referre to a letter , sign S. Galindo , in the 
 April Number of the Musical Times , 1 beg to repudiate 
 the assertion that " you rarely hear the word of a 
 singer , in whatever language he may sing , " it be 
 uite the reverse of undeniable , as any one who have 
 hear Signori Mario or Mongini , Mr. Sims Reeves , or 
 Mr. Santley , or Monsieur Faure ( not to merition many 
 other ) in Italian , French , or English , must surely admit 

 Hiles ’s organ solo be also thoroughly appreciate ; and the con- 
 cert afford the utmost satisfaction to a large audience . 
 CampripGe.—The opening of the St. John ’s College 
 New Chapel , on Wednesday , the 12th ult . , be mark by a most 
 imposing ceremony . the sentence of consecration having be 
 read by the Rev. E. B. Sparke , it be sign by the Bishop , after 
 which the 100th psalm be sing by the whole congregation . at 
 the conclusion of the sermon , an Anthem , compose expressly for 
 the occasion by Professor Bennett , be give with much effect , Dr. 
 Garrett most ably preside at the organ . as might be expect 
 from the reputation of its composer , this work be write in the 
 purest ecclesiastical style , the sacred character of the word be 
 reflect with the utmost fidelity in the music . the service be 
 conclude by avery excellent rendering of the * hallelujah " Chorus , 
 at the evening service , Professor Bennett 's Anthem be repeat 

 Canrersury.—Mr . Longhurst ’s annual concert , on the 
 26th April , in the Music Hall , be fully and fashionably attend . 
 the principal vocalist be Miss Edith Wynne , Miss Fanny 
 Holland , and Mr. Vernon Rigby , all of whom be highly effective 
 in their vocal solo . a feature in the instrumental selection be 
 Beethoven 's Sonata in G ( Op . 30 ) , for pianoforte and violin , which 
 be excellently give by Mr. H. Weist Hill and Mr. Longhurst ; and 
 Mr. T. Harper 's trumpet be hear to the utmost advantage in the 
 obbligato to * * let the bright Seraphim , " which be sing by Miss 
 Edith Wynne . Mr. Longhurst ( who conduct with much ability ) 
 may be congratulate on the success of this , his twentieth annual 
 concert in Canterbury 

 Currroy.—The Royal Tyrolese Singers give three 
 Concerts , under the auspex of Mr. James C. Daniel , at the Vic- 
 toria Rooms , on Monday evening and Wednesday afternoon and 
 evening , the 10th and 12th ult . the performance be all well and 
 fashionably attend 

 Coxnor.—On Whit Monday , the opening of a new 
 organ in the Primitive Methodist Chapel , be inaugurate by Mr. 
 John Lambert , of Durham , who show the capability of the in- 
 strument in a well - choose selection from the work of Handel , 
 Haydn , Mozart , Beethoven , and Mendelssohn . in the evening there 
 be a grand concert of sacred music . Mr. John Lambert preside 
 at the organ , and also join the other member of the Cathedral 
 choir in several vocal piece . there be a very full attendance 

 Crayrorp.—On the 30th April an amateur Concert 
 be give , at the new School Room , under the conductorship of the 
 Rev. J. B. Ward , who also accompany most of the piece on the 
 pianoforte . the programme contain a selection from the Mac- 
 beth music ( usually , but erroneously , ascribe to Locke ) , the solo 
 in which be excellently render by Miss Ida Thorne , Messrs. 
 Lloyd and Hudson . in the miscellaneous part pianoforte solo 
 be well play by Mr. E. Lewis and Miss Louisa Horner ; and 
 song be give with much effect by Miss Helieler and Miss Ida 
 Thorne , the last - name lady create a marked impression in 

 Kanpy , Ceyton.—The annual meeting in connection 
 with the " ' Musical Volunteer Club " be hold , in the United Service 
 Library , on the evening of Monday , the 22nd March . James Van 
 Langenberg , Esq . , the Acting District Judge , occupy the chair . 
 the secretary ’s report show the operation of the Club during 
 the two past year . the band , which at present number twelve 
 performer , be highly efficient ; and the character of the music 
 play be gradually raise in proportion to the progress show by 
 the player . the finance , from yarious cause , be not in a satis- 
 factory state ; but it be hope that if the member will continue to 
 evince their interest in it and the public to encourage it , the 
 ' Musical Volunteer Club " will begin its third year with an 
 increase prospect of success . in the absence of Capt . H. Byrde , 
 the adoption of the report be propose by Mr. W. 8S. Le Feure , 
 Secretary of the Municivality , and second by Mr. N. Brohier . 
 able address be also deliver by the President and several 
 other , and the officer for the current year be appoint , & vote 
 of thank be pass to the chair , and the proceeding terminate 

 LeEEps.—Three special Organ and popular Concerts be 
 give in the Town Hall , at Whitsuntide , under the direction of Dr. 
 Spark , which attract large audience . the first , on Saturday , 
 the 15th ult . , consist of vocal music and organ solo , the vocalist 
 be Misses Myers and Kennedy ( soprani ) , Misses Anyon , and 
 Hamilton Smith ( contralti ) , Mr. Longbottom ( tenor ) . and Mr. 
 Dodds ( bass).—ar Dr. Spark ’s Organ Recitals , on Whit Monday 
 and Whit Tuesday , the programme include work by Bach , 
 Handel , Beethoven , Mendelssohn , and two very interesting piece 

 lmirably rendére by Miss Anna Jewell , Mr. Gedge , Mr. Minns 

 IMMERMANN , AGNES.—March , arrange for 
 the Organ by John Stainer , 1s . 6d 

 EST , W. T.—Arrangements from the Scores of 
 the Great Masters for the Organ . no . 72 , price 2 . , contain : — 
 Aday;io from the Tiio , Op . 8 ( Beethoven ) , and March in C minor 

 F. Spindler 

 TAINER , JOHN.—Arrangements for the Organ . 
 no . 1 , price 2s . , contains:—Andante from Quintett in e flat 

 Op . 4 ( Beethoven ) , and Minuetto ( Handel 

 ESTBROOK , W. J.—The Young Organist ; a 
 collection of piece of moderate difficulty . no . 9 , price 
 1s . 6d . , contain : — Larghetto , from the Violin Duets ( Spohr ) ; Trio 

 the MUSICAL TIMES.—Jvne 1 , 1869 

 elers Cay row " aig 
 : 4 5 a 
 import and sell by NOVELLO , EWER and CO . 
 PIANO SOLO . Nett . | PIANOFORTE score . Nett . 
 BEETHOVEN . thirty - eight sonata , ne oon oe of ( g — German , f — french , # — italian reagag 
 — — Pianoforte Pieces ( Rondos , & c. ) .. ns .. 2 0 BEETHOVEN . Fidelio , g pas 4 
 — variation . os wk 4 0 } — — Missa Solemnis . a eee be 4 ° 
 CLEMENTI . twelve Sonatines ooo ik .. 2 0;}CHERUBINI . Demophon , ' at - sis 5 8 6 
 HANDEL . Suites I — VIIL . ve a " 3 0| — MassinF ... os = as = ¢e 
 — Suites IX — XVI . _ ... it # 3 " 3 0/ — — MassinD minor ne aa 
 — — Chaconne , Lecons , Fugue es ne ... 3 0| — — MassinA ... we fe er . 
 HAYDN . ten celebrate Sonatas ... we on ee — — requiem for male voice ese eee be 4 ; 
 MOZART . eighteen Sonatas , complete wee w. 4 0 } GLUCK . Orphée , g , f , i ... ns owe iy 3 
 — — Pianoforte Pieces ( Rondos , & c. ) .. ee 2 — Alceste , 9 , f px ise in a 4 . 
 SCHUBERT . Pianoforte Pieces , Op . 15 , 78 , 90,94 , 142 .. 3 0 } — — Paris and Helena , g , f ... * a 4 
 — ten Sonatas , complete ve . 4 0| — — Iphigenia in Aulis , 7 , / ae by 49 
 — dance ... we w- 2 0 ) — — Armide , g , f wis ' ~ as 
 — — song arrange : — Schiine Miillerin on .. 2 0| — — Iphigenia in Tauris , 9 , 7 aa be 4 
 Winterreise ... eco w. 2 0| JOMELLL requiem = 4 4 
 Schwanengesang « . 2 0|MOZART . Don Giovanni , ~ ons ove a 40 
 twenty - two celebrated song owe 3 0| — — Figaro , 9,4 . eve eve 1 40 
 WEBER . four sonata , complete ... ' le « . 2 0| — — Zauberilite , gt ans sa aan 7 30 
 — Pianoforte piece ... " a ae .. 2 0| — — King Thamos , g ae ni a ae 
 ROSSINI . II Barbiere , g , i = ne ~ € " 
 PIANO DUETS . | SPOUR . Jessonda , g _ — on on o- 40 it i 
 BEETHOVEN . om ~ rermeage 8 by Ulrich : — | SCHUMANN , " Foust ’ g ore ere oo 20 mer 
 — no . 1 eli 0| ‘ aust , g .. eee 8 0 ss 
 Vou IL.—No . 6—9 a 5 ca nt 
 — ~ oa — — pear arrange for ) PIANO ‘ S010 , of t 
 HAYDN . celebrate Symphonies arrange nr ' Ulrich : — | ue chlosser — we 20 of | 
 Vol . L — no . 1 - 6 = 4 0| BEETHOVEN . Fidelio ... oo ove 20 intr 
 vol . IL — no . 7—12 — 4 0 ) a eee eee ove oe 2 0 the 
 m ' 3 é ae \ — u we on wo F 
 MOZART . — symphony arrang ze by Ulrich : | BOIELDIEU . Dame Blanche xs eh 3 ; whi 
 « + £ 0 ) DONIZETTI . Luci a ont 
 — Sonatas and all other composition oo & OI Eli d ’ mcia @t Lammermoor az ~ 20 mal 
 SCHUBERT . original Compositions in3vols . ,each ... 4 0| pk iy Sma a 9 of " + lee 
 La — pan ove ove an 
 PIANO and VIOLIN . a. Don Juan ove eee eos oe 2 0 and 
 BEETHOVEN . ten Sonatas , complete ane we AOA Zenberfitte 5 may hac a be 
 MOZART . Highteen Sonatas , complete 2 10 0 } ROSSINI I Barbiere o I " te fen 
 SCHUBERT . three Sonatines and Rondo , Op . 70 -- 8 0 ! WEBER . Freyschiitz 2 ~~ £ ° ln 
 — — song arrange : — Schine Miillerin ap be 0 Oberon 4 bla oe : es = 4 he 
 Winterreise ... ie ee 2D Euryanthe be ie tet ; : ye 
 Schwanengesang 3 0 ! Preciosa a ae os a : Py 
 twenty - two Celebrated Songs 3 0 BEETHOVEN . " Egmont " ® a i " 90 ve 
 | BELLINI .   Purit : ra yen ie 
 PIANO and VIOLONCELLO . Ln tend a Cnpeneall = = aa > 
 SCHUBERT . song arrange : — | BOIELDIEU . Jean de Paris wit chi 
 Schéne Miillerin ... ow « . 8 0 , CHERUBINI . W maaan ( les deux Sournses ) 20 we 
 Winterreise . 3 0 ' GLUCK . Armide ine ~ £ s sin 
 Schwanengesang .. 3 0 ' MEHUL . Joseph " a Me : fom « a lig 
 twenty - two celebrated Songs we 3 0 MOZART . Seraglio ove ~ oon sf his 
 | } — — Titus ‘ ss ie eve one ott | 
 PIANO TRIOS . | ROSSINI . Otello - - s 20 s 
 SCHUBERT . Trion Op . 90 and 100 , for Pianoforte , Violin | SPOHR . Jessonda on 20 th 
 d 
 aon Vishenane a 4 ° | complete opera arrange as PIANOFORTE i 
 quartett . duet , pa 
 HAYDN . eighty - three String Quartetts , complete .. 40 0 BEETHOVEN . Fidelio oe " ove we 3 0 wi 
 MOZART . ' Ten celebrate String Quartetts ... .. 12 0 | BOIELDIEU . Dame Blanche ra att ® af 
 — — seventeen String Quartetts a cam .. 12 0|MOZART . Don Juan ove ose eve on ew fo 
 } — — Figaro pees oe ie eee ee a 
 quintett . i — Zauberflite ae ae ei a i 
 MOZART . five Celebrated String Quintetts ... we 6 9| ROSSINI li Barbiere   ... ws os - $ ? bo 
 — five String Quintetts .. be .. 6 0 WEBER . Freyschiitz pt 
 SCHUBERT . celebrate Forellen Quintett , Op . 114 , f | pe 
 Pianoforte and String tr te ong a 3 0 overture for PIANOFORTE 5010 , be 
 MOZART . overture , complete 26 mn 
 song . | BEETHOVEN . overture , complete one a 2 m Pi 
 BEETHOVEN . song , complete a ta o | CHERUBINI . eight overture , complete _ oe kn el 
 SCHUBERT . album , contain — Schine Miillerin , com- WEBER . | ten overture , comple BO . ccs dd - 
 stele ; Wameiaiie aemaiein Gbeannngnanien . sour SCHUBERT , SPOHR , LINDPAINTNER . celebrate Over- as - 
 plete ; and twenty - tw lebrate song z u 
 oo aaa ea a a BOIELDIEU , HEROLD , AUBER , SPONTINI . celebrate th 
 — — ditto , ditto , for deep voice sap 4 0 overture . ei a 7 ° q 
 — — Schine Miilierin , original edition 5 0 BELLINI , ROSSINI . celebrate overture ... 20 . 
 — — ditto , ditto , for deep voice - pis a # 
 — — Winterreise , pote . edition ... coe ooo 1 ° overture as PIANOFORTE duet . ' 
 ditto , for deep voice ... 2 9 MOZART . ten overture , complete .. pm - 20 ¥ 
 a Schwanengesang , original e dition 20 BEETHOVEN . eleven overture , complete ~~ § % . 
 Ditto , for deep voice ... 9 9 CHERUBINI eight overture , complete on on 5 8 0 
 — — twenty - two celebrated song , original edition 2 9 , WEBER . ten overture . complete ... die ' 
 Ditto , dite , for deap velco 2 9| SCHUBERY , SPOHR , LINDPAINTNER . celebrate Over- ' t 
 — Second Album , contain — s ty - f ; : 
 om Gui event ) y- five " celebrate < i BOTELDIE : U , HEROLD , AUBER , SPONTINI . ' celebrate A : 
 — — ditto , ditto , for dee @ verture .. , " e e , 
 ' ina 4 © BELLINI , ROSSINI , celebrate overture - .. ae 
 FULL score . study . i 
 BEETHOVEN . Pianoforte Concerto , 7 son - . 4 0 BERTINI . study , op , 100 , and little piece on 2 0 
 — — ditto , ditto , in b flat . ies -- 4 0 — — study , op . 29 and 32 3 on o 2 0 i 
 — _ — dis aus ou 4 0 — — study for four hand , op . 97 ... ove w- 2 0 
 — = ' I ree i ( IMEN ‘ 20 
 ditto , ato , in Bet - ma pa : : CLEMENTI . gradus em , vo . oe he 
 — Violin Concerto 40 ditto Vol . IL . ... o & e 
 WEBER . Concertstiick , op . 79 4 0 ' rode . caprice for Violin ( edit . by David ) ... ov ae 

 eae see se 

 33 the Juvenile Vocal Album 

 32 Beethoven 's sonata . edit by Charles Hallé ( no . 6 ) . 
 31 Beethoven 's sonata . edit ky Charles Hallé ( no . 5 . ) 
 30 Beethoven 's sonata . edit by Charles Hallé ( no . 4 . ) 
 29 ten Contralto song , by Mrs , R. Arkwright 

 28 Beethoven 's sonata . edit by Charles Hallé ( no . ,3 . ) 
 27 five set of Quadrilles as duet , by Charles d'Albert , c. 
 26 thirty galop , Mazurkas , & c. , by d'Albert , & c 

 25 Sims Reeves ’ most popular song 

 22 twenty - one Christy Minstrel Songs . ( first selection . ) 
 21 nine Pianoforte piece , by Ascher and Goria 

 20 Beethoven 's sonata , edit by Charles Hallé ( no . 2 . ) 
 19 favourite air from the Messiah , for the Pianoforte 

 18 twelve song by Verdi and Flotow , with english word . 
 17 nine Pianoforte Pieces , by Osborne and Lindahl 

 16 twelve sacred duet , fur Soprano and Contraito voice . 
 15 eighteen of Moore 's irish melody , 
 14 ten song , by Schubert . engiish and german word . 
 13 twelve popular duet for soprano and Contralto voice . 
 12 Beethoven 's sonata . edit by Charles Hallé , no , 1 . 
 11 six Pianoforte Pieces , by Wallace 

 10 nine Pianoforte Pieces , by Brinley Richards 

