OUTLINE OF THE PERFORMANCES

Turspay Morninc: ELIJAH. Tuespay Evenine: Brahms’s 
SONG OF DESTINY; Mr. Edward German’s NEW ORCHES- 
TRAL WORK (composed expressly for this Festival); Beethoven's 
C MINOR SYMPHONY, No. 5; Wagner’s MEISTERSINGER 
OVERTURE; Scene 3, Act III., of DIE WALKURE; Schumann’s 
MANFRED OVERTURE

WepNEsDAY MorninG: Professor Stanford's new REQUIEM 
MASS (first time of performance); Bach’s Cantata O LIGHT 
EVERLASTING; Brahms's SYMPHONY, No. 1. WeEpDNESDAY 
EveninG: Purcell’s KING ARTHUR MUSIC (as specially edited by 
Mr. J. A. Fuller Maitland for this Festival); Cherubini's MEDEA 
OVERTURE; Beethoven’s LEONORA OVERTURE, No. 3

TuHurRspAy Morning: MESSIAH. Tuurspay Evenine: Gluck's 
IPHIGENIA IN AULIS OVERTURE; Arthur Somervell’s new 
Cantata, ODE TO THE SEA (composed expressly for this Festival) ; 
Wagner’s SIEGFRIEDIDYLL; Mozart’sG MINOR SYMPHONY 
Dvorak’s CARNIVAL OVERTURE

Sept. 15.—‘A Stronghold Sure” (Bach), Magnificat (Hubert Parry), 
Selection from “ Parsifal’’ (Wagner). “Last Judgment” (Spohr). 
Evening: “ Elijah

Sept. 16.—Mass in D (Beethoven), Symphony, B minor (Tschai- 
kowsky), ‘“‘ Creation”’ (Part I.). Evening: ‘‘ Redemption

Sept. 17.—‘* Messiah.” Evening: Chamber Concert in Shirehall

ANALYTICAL PROGRAMMES

In the Musical World of December 2, 1836, 
there is a letter written to the Editor by Mr. 
Charles H. Purday, in which he complains that 
the public are ‘‘as ignorant (generally speak- 
ing) of the true character, design, and end of 
those stupendous efforts of genius (i.e., the 
works of Mozart, Haydn, and Beethoven) as 
are the Hottentots of their existence.” But he 
is not content to complain; he makes a pro- 
position, which if carried into effect would, 
he thinks, make the public not only more 
interested in the works they hear, but enable 
them to understand somewhat of their true 
character and design. Mr. Purday’s proposi- 
tion is

That a prologue, if I may use the term, should preface 
every performance of the works of the great masters, 
giving a brief and pithy analysis of the composition to be 
performed, showing its relative character to the mind of 
the musician, the feelings by which he was actuated in the 
production of his work, and the circumstances (where 
known) under which it was brought out: this would, by 
the novelty, in the first place, attract attention, and, by 
frequent repetition, keep that attention alive and lead us in 
tracing the mind of the author through the productions of 
his pen; and the design of his work would thus be made 
clear to the understanding of the amateur, and infuse in 
him the desire to become more intimately acquainted with 
the classical compositions of the great masters

No notice appears to have been taken of this 
letter

The programme-book of the first College 
Concert in memory of General Reid, given at 
Edinburgh University, Friday evening, Feb- 
ruary 12, 1841, contained—as humbly described 
by Professor Thomson—“ brief notices of the 
music,” and this, there is every reason to 
believe, was the first experiment of the kind in 
this country. The concert was so brilliant a 
success that it was repeated on the morning of 
the next day. The demand for tickets was 
altogether unexampled. On the Friday the 
audience numbered 1,300; ‘so great,’’ says a 
newspaper of the time, “ was the anticipation 
of the musical treat, that all the tickets were 
bought about a fortnight before the concert 
took place.”” Many applications for seats had 
to be refused, and hence the Senatus Academicus 
ordered the repetition as above mentioned. 
The programme was a long one, and although 
the performance commenced at seven o’clock, 
it was near twelve o’clock before the ‘ enter- 
tainment,” as it was called, concluded. The 
programme was divided into three parts, with 
two intervals, one of ten, the other of fifteen 
minutes. Besides overtures by Mendelssohn 
and Weber, Beethoven’s C minor Symphony, 
and other instrumental music by Corelli, Bach, 
and Haydn—the last-named was represented 
by the “God save the Emperor” Variations 
from the Quartet (Op. 80, No. 2)—there were 
many vocal solos, duets, trios, quartets, also 
choruses. Among the violins of the orchestra 
is to be found the name, ‘“ A. Mackenzie,” the

father, we presume, of Sir Alexander Mackenzie

now Principal of the Royal Academy of Music. 
The orchestra consisted of fifty-six performers, 
the chorus of 126 members. The music was 
under the direction of Professor Thomson

The analytical notes on Beethoven’s C minor 
Symphony deal principally with structure. The 
form now known as first-movement, which our 
Professor prefers, however, to call symphonic, 
is clearly and succinctly described; he speaks 
of the Coda as consisting ‘‘of a few brilliant 
chords from the whole orchestra,” a definition, 
however, which scarcely applies to the closing 
bars of the particular Allegro in question

The story of the reception of this movement 
by the Philharmonic orchestra in 1814 is thus 
told by Professor Thomson

The programme opened with Mendelssohn’s 
overture to ‘St. Paul,” and the analyst calls 
attention to the difference between the instru- 
mental introduction to an oratorio and to an 
opera. The latter kind is illustrated in the 
third part of the programme, which included 
the overture to Professor Thomson’s ‘‘The 
Shadow on the Wall’’—an opera which, in 
April, 1835, had a successful run of nearly fifty 
nights at the English Opera House—and 
Weber’s ‘‘ Oberon” overture

Besides the words, there are helpful notes 
of comment, in some cases fairly lengthy, to all 
the vocal pieces of the programme; at the 
present day it is most rare to give anything 
beyond the words. Not only does the Professor 
point out the general character of the music in 
each case, but here and there calls attention to 
details of structure, harmony, or orchestration. 
For instance, he gives not only a clear outline 
of the “ Hallelujah” chorus from the ‘‘ Mount 
of Olives,’’ but refers to certain chords and 
special passages. And then he contrasts this 
chorus with the “ Hallelujah’ in Handel’s 
‘* Messiah’; while acknowledging the nobility 
of Beethoven’s music, he yet feels that it ‘‘must 
yield in breadth of design, simplicity of detail, 
and sublimity of effect to the elder inspira- 
tion

For the above brief description of the con- 
tents of this Reid programme-book, I am 
indebted to Professor F. Niecks, who kindly 
placed his copy at my disposal

started in 1845. 
thing of the kind was attempted

Beethoven Quartet Society (MM. Sivori, 
Sainton, Hill, and Rousselot), which gave a 
series of five concerts, or Meetings, as they were 
called, in the course of which all the master’s 
quartets were given. Three quartets were 
played each time, excepting at the fourth 
concert, when there were four. The works 
were not performed in chronological, or rather 
opus order. The first programme will give 
a general idea of the scheme :—Op. 18, No. 1; 
Op. 59, No. 2 (in C); and Op. 127. One or’ 
more stanzas of poetry are followed—in the 
case of all the quartets—by the title and opening 
bars of each movement, after the manner of a 
thematic catalogue, and then come short 
comments: historical, esthetic, and sometimes 
critical. At that time Nottebohm had not 
written his ‘‘ Beethoveniana”’ nor Sir G. Grove 
his ‘* Beethoven” article in the “ Dictionary of 
Music and Musicians.’’ The sources whence 
information respecting the works could then be 
obtained were not free from errors. We find, 
for instance, Beethoven’s Quartet (Op. 18) 
described as composed in 1791 and published 
at Vienna in1792. The programme of the fifth 
and last concert, which included specimens of 
the three styles of the composer (Op. 18, No. 5; 
Op. 59, No. 3, and Op. 132), closes with the 
following : 
Our task is ended. 
Let the echoes of the world be raised with one sound

and that 
Honour To BEETHOVEN

Then came the programme-books of the New 
Philharmonic Society, which commenced its 
concerts in 1852, followed by those of the 
Crystal Palace (with which the name of Sir 
George Grove is so.pleasantly associated), the 
Sacred Harmonic Society, and the Monday 
Popular Concerts, written by the late Mr. J. 
W. Davison. It was not, however, until 
1869 that the Philharmonic Society followed 
suit. The books, with analyses, were first 
prepared by the late Sir G. A. Macfarren; 
afterwards by the late Dr. F. Hueffer; the 
analyst at the present time is Mr. Joseph 
Bennett. Analytical programme-books came 
more and more into vogue. Now they are 
provided for all orchestral, and most chamber 
concerts of any note

PrAYER—“ Gran Dio” - Guglielmi

Cuorus—‘ Glory to God” sc me ) 
QuartetT— O Lord, have mercy ’’ Beethoven, 
Cuorus— God is great in battle

Part II. 
OVERTURE .. Vogler. 
Air—“ But, O sad Virgin” a Handel. 
ConceErTO for Pianoforte.. Sebastian Bach. 
Air—‘‘ Total Eclipse” Handel. 
MapricAL— Lady, when I behold ” Wilbye. 
A1r—‘ Lord, remember David ” Handel. 
QuINnTET—“ Sento, O Dio”’ P Mozart. 
Cuorus—‘ He gave them Hailstones ” ‘ Handel

PHILHARMONIC SOCIETY

Part I. 
SympuHony IN C (No. 6) Mozart. 
A1r—‘‘ O God, have mercy” Mendelssohn. 
ConcerTo in E flat for Pianoforte . Beethoven. 
Ronpo—‘ I] cielo, la terra’ Winter. 
OvERTURE—“ L’Apparition” .. xe ae Ries

Part II, 
Sympuony in G (Letter V.) ne oe Haydn. 
Cantata—* The Quail’”’.. Ne .. Beethoven. 
STRING QuaRTET in G (Op. 18) .. .. Beethoven, 
TERZETTO—“ Soave conforto”’ .. Rossini. 
OveERTURE in D Romberg

SocieTA ARMONICA

Part I. 
SympHony—“ Pastoral’’.. .. Beethoven. 
DustT—* Dove vai” Rossini. 
PravyER—“ Cielo” Lindpaintner. 
A1r—“ Resound, ye hills ” Calcott

LARGHETTO AND ALLEGRO for Pianoforte ae Weber

I have taken these specimen programmes 
quite at random, and there is much to be 
learned from them, especially as regards the 
points common to all. Each, for example, 
agrees with the rest in its evidence as to the 
limits of public taste and appreciation in the 
matter of orchestral music, and the proof is 
absolute that instrumental “ bread” needed to 
be washed down by an unconscionable deal of 
vocal “sack.” Of eighteen pieces in the 
Antient Concerts list only three are orchestral. 
In that of the Philharmonic, the instrumental 
works are six out of ten. The Societa Armonica 
shows five out of fourteen, and the Society of 
British Musicians six out of fourteen. The 
inference from these figures is clear as to the 
lack of robustness in the public appetite; but, 
on the other hand, the orchestral selections, 
though in a minority, are, as far as I know 
them, of unimpeachable character. Lovers of 
such music, it would appear, demanded the 
best

Further interesting testimony with reference 
to the question just indicated arises out of the 
revival by the Philharmonic Society, in 1837, 
of Beethoven’s Choral Symphony. That work 
was first performed, by the same Society, in 
1825, and then so badly rendered that far- 
reaching consequences followed. Unable to 
comprehend it in hearing (the score was not 
then published), some of the critics attributed 
its apparent defects to the composer. One 
styled the work ‘the most extraordinary 
instance of great powers of mind and wonderful 
science wasted upon subjects infinitely beneath 
its strength.” Another said it was “ without 
intelligible design,’ and so on. These opinions, 
according to the late Dr. Gauntlett, “ were 
copied from one publication into another, and, 
in consequence, that which had originated in 
misapprehension and ignorance was received 
as the dictate of sound criticism. A prejudice 
was excited against the work, and although the 
Symphonies, Nos. 7 and 8, were repeated and 
recognised as glorious specimens of the com- 
poser’s matured powers of mind, few dared to 
set aside the verdict passed against the Sym- 
phony, No. g.” For the hostile critics in this

case there is really something to be said. Their 
worst fault lay in giving a blind and hasty 
judgment. In effect they never heard the work, 
being witnesses only to a travesty which they 
had no means of recognising as such. They 
should have suspended judgment; they pre- 
ferred to condemn out of hand, albeit the com- 
poser’s name was Beethoven

The performance of 1837 was directed by 
Moscheles, and in this connection we get our 
first glimpse of the modern conductor—or, 
rather, of his prototype—in England. From 
an enthusiastic writer in the Musical World I 
quote the following: ‘‘ Mr. Moscheles conducted, 
and every attitude testified how completely 
he was absorbed in the beauty of the scene; 
how his spirit bowed down and worshipped 
the mighty genius of his master. For the way 
in which he led the band to draw out some few 
points, we thank him with feelings of gratitude 
and admiration; and we really think, now our 
enthusiasm has in some measure subsided, 
that, had we met him coming out of the 
concert-room, we should have knelt to him, 
and, through him, done homage to the memory 
of the magician whose mighty conception he 
had been so instrumental in developing.’’ So 
might some perfervid worshipper of Richter or 
Mottl speak now of his idol. The more sober 
Times delivered itself to the same effect: ‘If 
music is ever to rise in this country to the 
dignity of an art, instead of being a mere play- 
thing for indolence or affectation, it must be 
when such media as these are found for bringing 
forth its hidden treasures. The wonder of the 
evening was that a work which has appeared 
at every preceding attempt harsh, complicated, 
and unintelligible, proves in reality, when 
played as it should be, when the performers 
have caught some spark of the composer’s fire, 
to be at once simple, grand, and impressive. It 
has established a truth which cannot be too 
widely circulated—that good music will always 
please the public if it is well played ; and that 
orchestras in general are much more in fault 
than those who listen to them.” How did those 
who listened to the Moscheles performance 
behave? A critic tells us that they bestowed 
enthusiastic applause and were absolutely over- 
whelmed with ecstasy and astonishment at the 
marvellous beauty of the work. ‘* We never 
saw a more unanimous feeling of approbation, 
or one demonstrated with greater cordiality at 
any meeting of this society.” It must be 
understood that the Philharmonic audience of 
that day was a very select body, but we have 
found them, twelve years before, accepting 
meekly the dictum of two or three writers who, 
being bound to say something magisterially, did 
so, and influenced the mass till an adequate 
rendering of the work made the crooked so 
straight and the rough places so plain that 
ordinary men could find their way to the goal 
of truth unaided. In this sketch of an event 
sixty years ago we may see a warning to the

On June 20 Mr. W. A. Reid gave an organ recital at St. 
Luke’s Church and played with great ability the following 
programme: ‘‘ Occasional ’’ Overture (Handel), ‘‘ Coro- 
nation March”? (Meyerbeer), ‘* Coronation March ” 
(Lesueur), Handel’s ‘‘ Zadok the Priest” and ‘The King 
shall rejoice,” the Hallelujah chorus from ‘‘ The Messiah,” 
and ‘God save the Queen,” with variations (Adolphe 
Hesse). Special services were held at all the churches 
throughout the city on Accession day

On June 17 Mr. Maughan Barnett’s musical society gave 
a most successful concert at the Opera House, in com- 
memoration of our Queen’s long reign. The first part 
consisted of Cowen’s ‘Song of Thanksgiving” and 
Beethoven’s E flat Pianoforte Concerto, the solo in the 
latter work being played by Mr. Maughan Barnett and 
conducted by Mr. Lawrence Watkins. The second part 
consisted of an original cantata, entitled “A Song of 
Empire,” the words by Mr. Arthur H. Adams, the music 
by Mr. Maughan Barnett. The cantata, which is a

favourable example of colonial talent, was enthusiastically 
received. The chorus numbered 220 and the orchestra 
thirty-five performers. Mr. M. Barnett conducted

ALEXANDER WHEELOCK THAYER

THE gteat biographer of Beethoven, ALEXANDER 
WHEELOCK THAYER, died at Trieste, on July 15, having 
nearly completed his eightieth year

The name of Thayer will go down to posterity as the 
author of the ‘‘ Life of Beethoven,” every line of which 
bears eloquent testimony to his boundless enthusiasm and 
self-denying energy. Unfortunately, he never completed 
his great and self-imposed task; but his work is a truly 
monumental one, deserving the highest praise. Thayer 
cleared away many erroneous statements made by former 
Beethoven biographers. He ascertained many important 
and hitherto unknown facts, and he unearthed numerous 
unpublished letters. Without being a blind worshipper, he, 
by dint of the true biographer’s love and sympathy, brought 
the complex and storm-tossed individuality of the master 
humanly nearer to us than any other writer. For this 
achievement he has laid the entire musical world under the 
deepest obligation. His investigations were of the minutest 
and most painstaking character. He took nothing for 
granted and neglected nothing which could in any way 
have some bearing upon his exalted theme; every fact 
he stated may be regarded as absolutely authentic

Although an American by birth, and writing his book in 
English, Thayer was averse to its being issued in that 
language. The volumes as they were completed were 
published in Germany, in a German translation by Dr. 
Deiters, the biographer of Brahms. Here again the author’s 
anxiety to render his work as complete and accurate as 
possible manifested itself in a striking manner; his reason 
for the course he adopted being that the German publication 
would doubtless bring forward well-informed corres- 
pondents, and elicit corrections as weil as new matter

thus rendering the eventual English version of the work the

more complete and reliable. The first volume of ‘* Ludwig 
van Beethoven’s Leben”’ appeared at Berlin in 1866, the 
second in 1872, and the third (which brought the life story 
of the great composer up to the year 1816) in 1879. The 
most important and eventful period (in regard to creative 
activity) thus remains to be dealt with—though, alas! by 
some other hand—in the remaining volume contemplated 
by the author

Concerning Thayer’s own life, it may be summed up in 
a few lines. A lineal descendant of the Pilgrim Fathers, 
he was born at South Natick, near Boston, U.S., on 
October 22, 1817. He studied at Harvard University, 
where he took the degree of Bachelor of Laws, and for a 
few years was employed in the University Library. In 
1849 he paid his first visit to Europe, staying about two 
years, for the purpose of studying German and of collecting 
material for a life of Beethoven. On his return to 
Boston he became a highly valued contributor to Dwight’s 
Fournal of Music, then but recently started. A severe 
and able review of Marx’s “ Beethoven” in the Atlantic 
Monthly, republished in Germany by Otto Jahn, had made 
him known in Germany. In 1854 and subsequent years 
Thayer continued his Beethoven studies in Berlin and 
Vienna, conferring with Schindler, Wegeler, and other 
friends of the master. He also visited London, where the 
help of Charles Neate, Cipriani Potter, George Hogarth, and 
In 1862 he was

Vienna, a post which he afterwards exchanged for that of 
United States Consul at Trieste, where he died. Amongst 
his minor published writings, besides numerous articles con- 
tributed to American newspapers, are ‘“‘ Signor Masona’”’ 
and ‘Ein Kritischer Beitrag zur Beethoven Literatur,” 
both published in Berlin. In the pursuit of his life’s one 
great object, Thayer was no stranger to occasional priva- 
tions, which he bore with heroic fortitude. We may add 
that at one such period, towards the close of his life, when 
his failing eyesight would only allow him to work for some 
two or three hours a day, an offer of material assistance 
from a firm of London music publishers was conveyed to him 
in delicate terms through a friend. His reply in declining 
the offer was at once characteristic and decisive: ‘If no 
new misfortune befalls me,”’ he says in his letter, “ I think 
I can continue to live within my income by not leaving 
Trieste in the summer, and by denying myself all sorts of 
pleasures and luxuries to which long experience has 
rendered me habituated.” Such was the man. Was he 
not a true hero-worshipper

REVIEWS

A new edition of Félix Clément’s “ Dictionnaire Lyrique,” 
edited by M. Arthur Pougin, has just been published by the 
Librairie Larousse. This important book of reference, the 
last supplement to which appeared as long ago as 1880, 
has, in the present edition, been entirely brought up to 
date, and a vast amount of new matter added to it by its 
industrious editor

A CONCERT by students of the London Academy of 
Music, at St. George’s Hall, on Juiy 24, brought forward 
quite the average amount of ability expected at displays of 
this description. In several instances proof was afforded 
of natural qualifications having received careful develop- 
ment. Balance between instrumental and vocal pieces 
was pretty evenly maintained, both with respect to the 
number and the efficiency with which they were executed. 
As pianists Miss Maude Smithers and Miss Rosalind 
Borowski earned approval for their smooth and artistic 
rendering of Mendelssohn’s Prelude and Fugue in E 
minor and a ‘‘ Valse Romantique” respectively. There 
were also a couple of solo violinists. Miss Evelyn Tyser 
played Vieuxtemps’s “ Air Varié’’ with firmness and feeling, 
and Miss Zeta Mason acquitted herself satisfactorily in one 
of the Brahms-Joachim Hungarian dances and an air by 
Bach. Among the vocalists, Miss Alice Tristram (with a 
refined rendering of Goring Thomas’s ‘‘A Summer 
Night’’), Mr. Gilbert Denis (who sympathetically sang 
Willeby’s ‘‘ Wake not”), and Miss Mary Hulburd (in her 
tasteful singing of ‘‘Connais-tu le Pays,” from Ambroise 
Thomas’s “* Mignon’’) were prominent. Some variations 
on a string quartet by Beethoven, an Andante Religioso for 
strings by Hiller, and other pieces, effectively played by 
the orchestral force conducted by Mr. A. Pollitzer, varied a 
programme which concluded with the presentation of 
diplomas and medals gained at the recent examinations

At Trinity College, London, Professor E. H. Turpin, on 
July 28, distributed the diplomas and certificates gained at

subscribers) had towards him, as well as an expression of 
their resentment at the treatment he had received at the 
hands of the clerical authorities, who had deprived them of 
a friend and the church of one of its most distinguished 
members. The wardens of the church had previously 
given Dr. Abernethy one hundred guineas

Tue Beethoven House Society, of Bonn, proposes to 
distribute the proceeds of the recent Beethoven festival 
in the form of three prizes, of 2,coo marks each, for 
new chamber compositions — viz., one for stringed 
instruments only, another for stringed instruments with 
pianoforte, and a third for wind instruments only or 
for wind instruments with either pianoforte or stringed 
instruments. Competitors must have been born previous 
to 1876 and their MSS. (score and parts) should be sent 
not later than December 17 next, addressed to Professor 
Joachim, in Berlin. Professors Rheinberger, of Munich ; 
Von Herzogenberg and Joachim, of Berlin; Drs. Reinecke, 
of Leipzig ; Wolff, of Bonn ; and Mandyczewski, of Vienna, 
are the members of the jury

At the recent annual meeting of the Church Orchestral 
Society, held in the Church House, Professor Sir Frederick 
Bridge, who presided, drew attention to the remarkable 
progress made by the society during the past year, a

Mr. N. Vert, the well-known concert agent, announces 
that he has already made the following fixtures for the 
coming season. At St. James’s Hall: his own three grand 
morning concerts, on October 16, 23, and 30. At Queen’s 
Hall: two Richter concerts, on October 18 and 25; a Scotch 
concert, on November 30 (St. Andrew’s Day); and Mr. 
Willy Hess’s Quartet concert, on December 8. The dates 
of Herr Grieg’s recital at St. James’s Hall and the Bach 
Choir concerts have yet to be fixed

THE prospectus of the Royal Choral Society for the 
approaching season is not yet ready for publication; but we 
are enabled to state that, in addition to the usual familiar 
and popular works, the novelties, or pseudo-novelties, will 
include Beethoven’s ‘‘ Ruins of Athens,’ adapted to a new 
English version, and a new oratorio, entitled ‘‘ The Gate of 
Life,” by Franco Leoni, which will be its first performance. 
Sir Frederick Bridge’s “‘ Flag of England” will be repeated 
“by desire

THE Normal or Teachers’ course of the Virgil Piano 
School, which has just closed (28th ult.), has been 
eminently successful, about eighty pianoforte teachers 
having attended the session. An interesting and enjoyable 
feature of the course has been the playing of Herr Felix 
Dreyschock, of Berlin, who has given twelve “ Recital 
Lessons’ during the session. We understand that a two

BerLin.—A new acting version of the second part of 
Goethe’s ‘' Faust,” by Herr Aloys Prasch, is being mounted 
with much care at the Berliner Theater, and the coming 
event is looked forward to with much interest here. A 
special feature of the performance will be the new 
incidental music written for the occasion by Herr Max 
Karpa, the talented conductor of the theatre, who has 
followed very closely in his score the hints and indications 
furnished by the poet himself in letters to his friends 
and in his conversations with Eckermann.——A new 
work, which cannot fail to excite much curiosity and 
interest, is about to be submitted to leading German 
operatic managers. It is nothing less than a dramatised 
version, set to music, of Immermann’s ‘‘ Miinchhausen”’ ; 
not, indeed, the idyl, entitled ‘‘ Der Oberhof,’’ which 
forms so charming an episode in that famous novel, 
but the politico-social satire itself, with that arch-humbug 
Miinchhausen, somewhat brought up to date, for its 
central figure. The authors of the new work, who 
carefully conceal their identity under the pseudonym 
of Hans Ferdinand Hans, are said to occupy prominent 
positions in the literary and musical world.——At the 
Royal Opera a young soprano, Fraulein Reinisch, engaged 
last year pending the completion of her studies, made an 
excellent début recently in “‘ Les Huguenots,” and will, in 
the opinion of many competent critics, soon attain a 
prominent position. —— The Meyerbeer Prize of 5,000 
marks has this year been awarded to Herr Bernhard 
Koehler, a pupil of the Cologne Conservatorium.—— 
Madame Carrefio, the well-known pianist, has returned to 
her residence here after a tour in the United States of two 
months, during which she gave no less than sixty recitals

BrussELS.—The first performance of M. Edgar Tinel's 
drame musicale ‘* Sainte Godelive,”’ the libretto by Hilda 
Ram, took place in the concert hall of the Exhibition, 
before a numerous audience, on July 22. The new work 
by the composer of ‘‘ Franciscus”’ is intended both for the 
concert-room and for stage representation, and embodies 
some effective dramatic writing together with general 
excellency of workmanship. It proved, however, to be some- 
what of a disappointment to the musicians present, being 
considered wanting in style and personality. The defective 
acoustic conditions of the hall, moreover, prevented its being 
heard to the best advantage. The composer, who conducted, 
was accorded a most hearty reception. A second perform- 
ance had been announced in the following week, but was 
countermanded.—Performances at the Théatre de la Mon- 
naie will be resumed on the rst inst., among the principal 
works to be produced during the season being M. Massenet’s 
“ Hérodiade,”” MM. Zola and Bruneau’s ‘‘ Messidor,”’ and 
Wagner’s ‘Die Walkire’” and ‘“ Die Meistersinger.” 
Among the artists announced to appear during the 
season are Miss Brema, who will be the Marceline in 
‘“L’Attaque du Moulin,” Frau Mottl (E/sa in ‘‘ Lohengrin,” 
Elizabeth in “ Tannhauser,” and Marguerite in ‘‘ Faust’), 
and M. van Dyck.——M. Paul Gilson, the composer, a 
former laureate at the Conservatoire, has gained the prize for 
a poem to be set to music by this year’s candidates for the 
Belgian Prix de Rome. The professional librettists who 
competed with the musician are said to be furious, vowing 
that they will take their revenge by entering the ranks 
of composers at the next competition !—_—Beethoven’s 
‘* Missa Solemnis” was performed for the first time in the 
Belgian capital on the 8th ult., at the Exhibition Hall, by 
the ‘“‘ Legia” Choral Society of Liége, under the direction 
of M. Sylvain Dupuis, with Mdlles. Nathan and Haas

an event in the musical life of Brussels and created a pro- 
found impression. An interesting analytical study of the 
work, from the pen of M. Marcel Rémy, has been published 
in connection with the performance

Care Town.—The third of the series of chamber music 
concerts took place in the Good Hope Hall on July 2r

he programme included the following concerted pieces : 
Schumann's Pianvforte Quintet in E flat and the slow 
movement from Beethoven’s Pianoforte Trio (Op. 2). 
Excellent solo performances on the violin and violoncello 
Were given by Mr. Percy Ould and Herr Windisch respec- 
tively, and Mrs. Tennyson Cole proved herself to be an 
acceptable vocalist in her rendering of Goring Thomas's

My heart is weary.” The other performers, in addition 
to those already mentioned, were Mrs. Barrow- Dowling 
and Mrs. Leader (pianoforte), Miss Neave and Mr. Prati 
(violins), and Mr. Riegelhuth (viola). Mr. Barrow-Dowling 
was the accompanist at this enjoyable concert

Haniey.—Mr. J. A. MacGregor has been presented 
with a reading lamp and dressing case from the choir and 
congregation of Trinity Church, on his retirement from 
that church

HarroGATE.—The programmes of the daily concerts 
given by the Town Band, under the direction of Mr. J. 
Sidney Jones, deserve notice because of their originality. 
The first part of the six performances for the week ending 
the 21st inst. were arranged for as follows: ‘ Half-hours 
with” (1) Ed. German, Cowen, and Sidney Jones; (2) 
Gounod, Strauss, and Weber; (3) Beethoven, Sullivan, 
and Mendelssohn; (4) Schubert, Grieg, and Wagner; (5) 
Mozart, Meyerbeer, and Balfe; (6) Verdi, Donizetti, and 
Offenbach

Honotutvu.—A special Diamond Jubilee Thanksgiving 
service was held at St. Andrew’s Cathedral, on July 23, 
which was attended by Her Majesty the Queen Dowager, 
Prince David Kawananakoa, and other eminent personages. 
A choir of eighty voices, under the able direction of Mr. 
Wray Taylor, is said to have well rendered ‘‘ music of a 
class never before heard in Honolulu.” The service 
included J. M. W. Young’s Te Deum in G and a setting of 
the Jubilate in the same key by Mr. Taylor, who played 
the National Anthem as a concluding voluntary

NortH Berwick, N.B.—Mr. Charles H. Nutton, the 
newly appointed organist of St. Baldred’s Church, gave his 
first organ recital on the 11th ult. Selections were given 
from Haydn, Saint-Saéns, Wély, Tartini, Schubert, and 
Handel. Mr. Nutton contributed two violin solos, the Rev. 
I. Dyer presiding at the organ, and Herr Hochstein gave 
two violoncello solos. There was a large audience of 
residents and visitors

OxFrorp.—An organ recital was given in the Town Hall, 
on July 24, in aid of the Linton Memorial Schools, by Mr. 
W. L. Biggs, organist of St. Peter-le-Bailey Church, when 
the programme included pieces by Theo. Stern, Batiste, 
Guilmant, Wagner, Bach, Schumann, and Beethoven. The 
vocalists were Miss Gwendolen Hayes and Mr. John Lomas

6, at the York Street Centenary Hall, under the direction 
of Mr. W. L. B. Mote. The conductor had composed for 
the occasion a dignified Jubilee hymn, ‘Sixty years of 
service splendid,” which was well received, and Master 
Arnold Mote’s organ solo, an original ‘‘Queen’s Com- 
memoration March,” also won acceptance. The orchestra 
played with spirit J. E. West’s march “ Victoria—our 
Queen.” ‘The Queen’s Song,” by Eaton Faning, proved 
one of the best numbers on the programme, and was effec- 
tively rendered by the choir and orchestra. Solos were 
contributed by Miss Millie Wynn and Mr. H. T. Gordon

Select Movements from the Works of the Great Masters, arranged 
from the Full Scores as Voluntaries for the Organ

No. ad, 
1. Hallelujah (‘ Mount of Olives”) Beethoven 1 6 
2. Slow Movement in G (First Symphony) .. Mozart 1: o 
3. And the children of Israel (‘‘Israelin Egypt’) .. Handel xr o 
4. Hallelujah (‘Time and Truth”) oe ae .. Handel o 6 
5. Slow Movement (First Symphony) Beethoven 1 0 
6. Ere to dust is changed (“‘ Time and Truth”) . Handel o 6 
7. Amen Chorus (‘‘ Alexander Balus”) .. oe -» Handel o 6 
8. Slow Movement (Op. 34) A ae ee .. Mozart 1 o

O Father, Whose almighty power (‘‘ Judas”) - Handel

13. Movement (Quartet, Op. 75) .. oe ee -- Haydn 1 o

14. Introduction (“ Mount of Olives”) .. ate Beethoven o 6 
15. Sing, O ye heavens (‘‘ Belshazzar”).. -- Handel i 0

London and New York: NoveE.tto, Ewer and Co

CANTATAS, ORATORIOS, AND OPERAS

BACH.—" Passion” (St. Matthew) .. ae Ke << oe 
BEETHOVEN.—“ Mount of Olives” .. (Paper boards, 1s

Tonic Sol-fa) 
BENNETT.—“ The May me" -. (Paper boards, ts. 6d.) 
ELGAR.—“ King Olaf”. F (Tonic Sol-fa) 
GLUCK.—" Orpheus” ‘ ‘ ‘ (Tonic Sol-fa) 
GRAUN, C. H.— The Pusiin" i Der Tod Jesu”) . 
H ANDEL., — Messiah” (Paper ve Is

LONDON & NEW YORK: NOVELLO, EWER AND CO

Slow Movement (Pianoforte Ae iaessold| a 
Schumenn G.C. Martin 1 
Minuet (Twelve Menuets for es mers : 
Beethoven 
Andante (Pianoforte Sonata, pal 147) a 
Schubert . 
= Handel G.C. Martin 1 
As pants the hart (# Calvary”) Spohr 
ol Agnus Dei(MassinG) .. oo) 
Overture (‘‘ Acis and Galatea”’) Handel G. C. Martin 1 
> | Albumblatter No.1, Op.g9) Schumann 
{ Réastoieanta, , Op.2, No.1) Beethoven ‘ 
4.4 The Cat's Fugue .. . Scarlatti + G. C. Martin 1 
{ Albumblatter (No. 5, Op. 99) Schumann) * 
leony and iain °° Sym- 
5.1 phony) .. . Schumann } G.C. Martin 1 
( Air (Overture ( (or Suite) i in D) Bach) 
Allegro (Quartet, Op. 18, No. 2) 
Beethoven ; 
6. Menuetto (Pianoforte Sonata, Op. =. G.C. Martin 1 
Eia M Stabat M Dror 
( Eia Mater (‘‘Stabat Mater”) Dvorak) . 
he Romance .. ‘a Mozart) ** G.C. Martin 1 
8. Minuet and Allegro - Handel .. B. Luard Selby 1 
9. { Funeral March (“ = of ete} .. GC, Mattin x 
10. ‘eee in F major Purcell A. W. Marchant 1 
11, Adagio in B minor Mozart A.W. Marchant 1 
12, Adagio (Sextet, Op. 81) ... Beethoven A. B. Plant 1 
13. In Elysium (“ Orphée”) .. Gluck E. J. Bellerby 1 
14. Judex (“Mors et Vita”). Gounod .. John E. West 1

I

London and New York: Novetto, Ewer and Co

a 8. 
ARENSKY, A.—B siieaiiea For V Sceensite and Pianoforte.. 4 
For Pianoforte ‘ 3 
BEETHOVEN.—Thema con Variazioni. For “Harmonium, 
Pianoforte, and Violin. Op.12. No.1 .. 6 
“Coriolan.” Overture. For Harmonium and Pianoforte 5 
“Egmont.” Overture. For Harmonium and Pianoforte 5 
“Fidelio.” Overture. For Harmonium and Pianoforte.. 5

BERGER, W.—Prelude and Fugue (G minor). For Pianoforte 
Solo. Op. 42 .. pw - a 
—— Scherzo (D minor). “F or Pianoforte Solo. Op. 43, No.2.. 6 
BRUHNS, J. L.—Fifty Pieces. For Pianoforte Solo. To be 
used as Second Pianoforte Part to “ Czerny’s Fifty Studies” 
(Kunst der Fingerfertigkeit). Op. 740. In Six Books. Each 
net 4 
For. Pianoforte, Violin, and 
Violoncello (or Viola

LOESCHHORN, A.—Menuet. For Strings. Score and Parts 4 
ae we _E. — Second Mazur. For Violin and Piano

forte. p- ee es ee wa § 
POORT iN = ‘Etude de Concert. For Violoncello and 
Pianoforte. Op. 15, No. 2 4 
— "“L’Aube.” Melodie. For V ioloncello and Piano. “Op. 16 3 
REBIKOFF, W.—Chant sans paroles. For Violoncello and 
Pianoforte os “a ea ‘nn ith a aa wo 3 
REINECKE, C.— Studien und Metamorphosen. For Piano- 
forte Solo. Op. 235 :— 
Yo. 1. HAYDN .. aa ‘i ea ps wa ae 
‘“ . Mozart Pe «e ue “ ae 
. BEETHOVEN 4 
RIMSKY- ‘KORSAKOW, —“ Chanson du Berger. ” For. Vv iolon- 
cello and Pianoforte . ee 3 
SCHYTTE, L. —Allegretto. For Harp (or Piano) and Strings: — 
Score and Parts .. ee 6 
—— Valse capricietto. Op. 102, ‘No.1 “a 3 
—— Valse noble. Op. 102, No. 2 3 
STANGE, M.—Suite. For Violin Solo - es ae <a 
STENHAMMAR, W.—Sonata (in A flat). For Piano. Op.12 9 
SVENDSEN, J.—Romanze. For Viola and Pianoforte a4 
TSCHAIKOWSKY, P.—Catalogue général et thématique des 
ceuvres .. net 20

WEBER, C. M.— “Adagio and Rondo. For Organ and Strings 5

Myles B. Foster| 6. Judex, from ‘Mors et Vita” Gounod

1. The Good Shepherd i 
2. Andante.. an W. Warder Harvey, 7. Soft Voluntary .. H. A. Harding 
3. Andante con moto es A. R. Gaul; 8. Slow March 2 aieaiita anes Woods 
4. Christmas Bells G. J. Elvey| 9. A Song of Praise : i . J. Stainer 
5. Minuet - Philip Hayes | 10. Andante in G minor .. E. H. Fellowes 
BOOK 2. 
1. Allegretto ..F. Cunningham Woods | 6. Tempo di Minuetto .. Myles B. Foster 
2. Larghetto ; J. Barnby 7. Andante con moto, from Quartet in 
2. Adag io W. Warder Harv ey D minor : ae Schubert 
4: Someiadlag Voluntary. . ae A. R. Gaul 8. Adagio William Sterndale Bennett 
o Introductory Voluntary E, F. Rimbault g. ‘ntroductory Voluntary .- Daniel McIntyre 
BOOK 3. 
1. Andante Tranquillo C. H. Lloyd! 4. Gavotte, from ‘‘ Semele” Handel 
2. The Village March a Ferris Tozer|5. An Evening Prayer .. Alfred W. Tomlyn 
3. Romance, from Serenade for |6. Heaven and the earth display, from 
Strings Mozart “ Athalie”’ : -» Mendelssohn 
BOOK 4. 
1. Meditation .Battison Haynes | 4. Concluding Voluntary.. . Cuthbert Harris 
2. Allegro moderato : W. John Reynolds 5. Introductory ne E. H. Turpin 
3. Funeral March, from Pianoforte 6. Solemn March . C. H. Lloyd 
Quintet (Op. 44) ; : Schumann 
gy 5. 
1, Agnus Dei a .F. Cunningham Woods Allegro con brio Frederick A. Keene 
2. Minuet, from gth Pianoforte Sonata.. Mozart é. Communion W. Wolstenholme 
3: Jerusalem Celestis, from ‘“ Mors |7. Allegro, from Finale to the oth Piano- 
et Vita” AC : ; Gounod | forte Trio .. Haydn 
4. Andante Grazioso Kate Boundy 8. Concluding Voluntary . . Cuthbert awa 
BOOK 6. 
1. OSalutaris Hostia . . Myles B. Foster | 6. Forsake me not, Duet from 
2. Slow March, from the 4th Sonata Boyce | “The Last Judgment” .. Spohr 
3. O great is the depth, from ‘St. Paul”? Mendelssohn | 7. Allegro moderato - Warder Harvey 
4. Processional March J. Warriner | 8. Minuet ‘ Samuel Ould 
5. Largo, from ‘* Xerxes ”’ Handel 
BOOKS 7, 8, and 9 fUST PUBLISHED. 
BOOK 7. 
1. Impromptu R John E. West | 5. Sunday Song is ee Max Oesten 
2. Minuet, from Pianoforte Sonata '6. Minuet and Trio, from Quartet 
(Op. 10, No. 3) Ae Ba : Beethoven | (Op. 9, No. 1) ; : . Haydn 
ce Introductory ee B% S. J. Rowton! 7. Pastorale ‘0 Alfred Ww. Tomlyn 
4. March, Oliver O. Brooksbank | 8. Religioso . T. L. Southgate 
BOOK 8. 
1. Elegy : C. H. Lloyd| 6. Andante in F, from String Quartet in 
2. Rondo, from No. 6 “of Piéces de D minor ee a .. Mozart 
Clavecin nf Couperin} 7. Meditation , W. Wolstenholme 
3. Andante Grazioso Frederick A. Keene|8. F inale, from Pianoforte Trio (Op. 88) Schumann 
4. Soft Voluntary .. ; Ferris Tozer|9. Canon .. Battison Haynes 
5. Allegretto Pastorale W. John Reynolds 
BOOK 9. 
1. Larghetto ..F. Cunningham Woods! 6. Andante con moto John Francis Barnett 
2. Recessional March .. E. H. Fellowes, 7. Andante Religioso Alfred W. Tomlyn 
3. All Men, all sii from * Hymn 8. Evensong : . Cuthbert Harris 
of Praise ” ‘ Mendelssohn g. Minuet, from Quartet i in G minor .. .. Schubert 
4. Aliegro poco maestoso. . W. G. Cusins | ro. Melody i inA ae Ne W. H. Callcott 
5. Communion ; Alfred R. Gaul

To be contin

