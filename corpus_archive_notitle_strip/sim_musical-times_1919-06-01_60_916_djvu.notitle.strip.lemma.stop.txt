interlude 

 musical time February , 
 timid débit heretic regard 
 department Beethoven work , remark 
 ' devout Beethovenite lookin 
 round missile . ' devotee lately 
 shower verbal brick , 
 risk tedious , endeavour hand 
 . far concern , 
 subject drop 

 , word reply Dr. Froggatt . 
 claim 
 forward Beethoven failure 
 song - composer , state fact . 
 quotation alter fact , 
 instruct modify opinion . ' 
 extract Parry Ritterdo . failure 
 course comparative term , success 
 small man failure Beethoven . 
 swallow need 
 summer , Parry remark 
 ' Beethoven time actually attain 
 ideal modern song ' sufficient 
 establish Beethoven song - composer 
 sense Schubert , Schumann , Brahms , Wolf , 
 Grieg , Parry , Stanford , good , 
 song - composer . Ritter positive , 
 remember history write 
 half - - century ago , intervene 
 year good deal winnowing , 
 especially far song concern . 
 Dr. Froggatt pain shock 
 dare express opinion having 
 read Parry ' summary ' Ritter ' history . ' 
 cheerfully plead guilty 

 opinion song , right wrong , 
 . base examination music , 
 echo somebody judgment . 
 excellent thing art 
 pronounce opinion - hand . 
 plenty mistake , 
 balance 
 scrapping vast pile old middle - aged 
 music owe vogue lazy habit 
 open mouth shut eye , 
 german classic send . mean 
 , opinion hard fact 
 Beethoven orchestral , chamber , 
 pianoforte work constantly perform , 
 song rarely hear . write 
 Saturday , 10 , order verify , | 
 turn page Daily Telegraph 
 date . dol find ? page contain 
 programme vocal recital , Mesdames 
 D’Alvarez , Muriel Foster , Melba , Astra Desmond , 
 Megan Foster , Mr. Kingsley Lark , 
 song Beethoven performance . 
 page programme concert 
 Beethoven Festival Queen Hall . 
 Beethoven successful song composer 1 
 single example work 
 field include scheme ? turn 

 pure form english folk - song 

 273 

 R.A.M. Syllabus current year , 
 note ( surprise ) thirty - 
 song set L.R.A.M. candidate 
 Beethoven . academical institution 
 rule lack loyalty classic , 
 omission significant . fact , like woman , 
 stubborn thing . far 
 convincing Herr Ritter 
 year ago . Beethovenites 
 misgiving , enthusiast Mr. Pitcher 
 letter month Musical Times 
 ' world know Beethoven 
 song composer 

 ' world , ' prove 
 letter sign ' Ein Fest Burg . ' correspondent 
 point , think . 
 condemnation Beethoven song ' revival 
 old cry , indicate return 
 mental inertia mid - victorian era . ' 
 mid - victorian era 
 capable appreciate sentiment style 
 song . sing , 
 fail completely 
 imagine . ' Ein Fest Burg ' think 
 difficult popular , suggest 
 know little type modern 
 song favour recital . , 
 Beethoven child play singer 
 accompanist 

 Ein Fest Burg ' ask critical analysis 
 dozen select song . analysis need 
 space spare , 
 little interest average reader . man work 
 particular field judge . 
 composer Beethoven standing write 
 seventy song , expect find dozen 
 able bear strict scrutiny . 
 reason faith — unfaith — , 
 allow work 
 bulk fail . instead examine 
 dozen mention ' Ein Fest Burg , ' 
 wade seventy , good 
 composer fame , 
 judge music merit . ' examina- 
 tion confirm poor impression song 
 ine - year ago , , 
 expectation , buy volume . 
 day ardent young Beethovenite 
 ' Ein Fest Burg ' wish meet , 
 remember disappointment . 
 work afresh , thing strike 
 extraordinarily uninteresting vocal . 
 blame modern composer unvocal , 
 unpleasing , rule charge 
 write uninteresting voice . ' 
 generally present kind problem 
 singer , vocal line 
 appear unsatisfactory sing 
 charge significance 
 conjunction accompaniment . 
 ' accompaniment ' lack word . 
 good song duet voice 
 pianoforte . ' singer 

 8 

 ee ee ee 
 — - — = po et fe 
 cloud shine . sun 
 bad example occur 

 Liederkreis ' — usually regard composer 
 good work song - writer . . 2 find 
 second verse — thirteen bar 
 - — set monotone , pianoforte 
 play tame series tonic dominant chord . 
 Beethoven mean subtle , 
 effect miserably weak 

 melody construct 
 arpeggio common chord . extract 
 ' Neue Liebe , neue Leben , ' Op . 75 , . 2 

 ex.3 . Andante espressivo . 
 ee 
 dry , dry , 
 Andante espressivo . fee 
 fe — = — — 
 — _ + _ © ea , — _ ) | — — — — — = 
 fee ee se 
 | SS 
 7 ee _ _ _ _   — e 
 = — } — _ _ _ — _ _ } -___F ee om ee . eee . 
 = ¢ — _ SS — _ _ _ — _ — _ _ _ — — . — f 
 = fs — = = — — — _ 

 strike convincing setting 
 ' dry , tear love everlasting . ' 
 Beethoven pleased little scale 
 ( represent tear ) end 
 song 

 ex , 4 

 r dry ! siete 
 | 
 : = = = } 2s = f 
 4 ss ss se 
 oe ae 

 Ein Fest Burg ’ evidently like sort 
 music . use . 
 curious thing feeble writing 
 date Beethoven good period . 
 , compose song , consider 
 scrap passage - work series note 
 good . passage 
 extent justify 
 lay regard sonority . 
 keyboard writing generally 
 elementary description . favourite 

 Adelaide ' spoilt complacency 

 right hand double voice , 
 left ' tum - tums 

 easy fill page 
 journal similar example writing worthy 
 Hummel Hiller , Beethoven 

 uncongenial task , dwelling 
 weakness great man work , merely 
 draw attention point . , far 
 case Beethoven content 
 music duty verse , regardless 
 fitness . chiefly fact 
 bulk song type 

 _ 

 popular Germany — kind half . 
 way house folk - song moder 
 song . Hiller - rate man turn 
 thing score . straightforward tune , 
 stock phrase folk - song , 
 rhythmical modal peculiarity — 
 melody . accompaniment 
 modest unconventional , setting du 
 number verse . odd man 
 develop instrumental form 
 little song . rarely 
 realise importance 
 music faithfully reflect sentiment verse , 
 easygoing method respect 
 marked contrast vivid touch Bach 
 vocal writing , especially cantata solo . note 
 Bach habit seize salient word 
 ' death , ' ' bitter , ' ' sigh , ' ' tear , ' ' trust , ' ' joy , ' 
 ' sleep , ' increase significance 
 striking harmonic melodic feature . compare 
 accompaniment Beethoven , 
 old music far 
 modern 

 irritating habit Beethoven 
 repeat line verse , chiefly order 
 round musical form . ' result usually 
 anti - climax 

 chief reason Beethoven 
 failure song - writer lie fact 
 genius dramatic epic lyrical . 
 , peculiar excellence Beethoven — 
 skill development — find little 
 scope small vocal form . insig- 
 nificant phrase song , occur 
 big instrumental work , doubt 
 develop splendid effect . form 
 confine narrow , spontaneity 
 vital importance , Beethoven spread 
 . ' modern song establish year 
 later composer equipment 
 opposite line . Schubert little skill develop 
 ment , Beethoven lack — 
 inexhaustible fund lyrical invention 

 Ein Fest Burg ’ request list song 
 good dozen selection Beethoven 
 hardly seriously . 

 find score fine example Schubert , 
 ' . | Schumann , Brahms , Wolf , Dvorak , Grieg , Strauss , 
 | Debussy 

 Mallinson , Parry , Stanford , Ireland , 
 Frank Bridge , Martin Shaw , Moussorgsky , Cui , 
 Tchaikovsky , Charles Wood — list 
 long — fear 
 help . Purcell furnish 
 handful fresh vital 
 Beethoven . ' lay earth , ' 
 ' knotting song , ' ' nymph Shepherds , ' 
 ' fairest isle , ' ' attempt love sickness 
 fly , ' ' shall love 
 , ' ' arise , ye subterranean wind , ' ' o let 
 weep , ' ' muse , bring rose hither , ' ' come , 
 dare , ' , Dr. Froggatt 
 ' Ein Fest Burg ' seventy 
 Beethoven 

 ee ee ae 

 275 

 Ein Fest Burg ' convincing 
 task orchestral question . 
 song matter taste . 
 Burg ' like kind music quote , 
 evidently kind thing like . far 
 grudge simple pleasure . 
 taste - day , . 
 assume reason like 
 song , dislike . 
 point matter arguable . 
 come comparison modern 
 orchestra Beethoven day , 
 deal fact . condemnation 
 ' vulgarity ' modern orchestra leave | 
 speak . away ) 
 completely leave 

 Mr. Cooper letter word . ) . 
 need point - scoring } 
 necessarily lead music noisy . } 
 matter fact , result | 
 reverse . example , passage -quote 

 Finale seventh Symphony , score | 
 Beethoven , decidedly noisy , reason 1 
 suggest - scoring 

 gave 

 ' Ein Fest | 7 

 strident . expression ' jolly good row ' 
 apply section Beethoven mark 
 want tremendous power , 
 employ unusual direction ? Mr. 
 Cooper , like ' Ein fest Burg , ' think 
 ' power ' ' vulgarity ' synonym 

 , criticism Beethoven 
 song suggestion - scoring 
 certain passage orchestral work 
 forth powerful argument 
 correspondence far receive , 
 heretical way hardened unrepentant 

 FESTE 

 P.S.—Bearing mind indignation Dr. 
 Froggatt express suggestion 
 scher ? Beethoven symphony 
 play detached work , 
 interested follow item programme 
 | Beethoven Festival concert 

 PIANOFORTE SOLOs 

 succeed , poetry good unsur- 
 passable tonal grandeur emotional beauty , 
 match rhythmic balance verbal nuance 
 Bible work ' Religio Medici , ' 
 equal general power moving force 
 grand passage music . Whitman , 
 , pre - eminently musician poet ; 
 think eventually art inexhaustibly 
 influence music large form lyric poetry 
 hitherto influence small 

 describe Whitman musicianship analyse 
 ' philosophy ' music great matter 
 undertake . describe musical 
 experience history music 
 New York 1835 1861 . impossible 
 moment . suffice 
 exceptional opportunity hear music 
 observe musician audience ( far music 
 publicly practise New York 
 Europe generally rise Wagner ) 
 sixteenth - year . brief 
 exception placed newspaper man 
 printer free entry public 
 performance music New York 
 Brooklyn year 1861 . 1862 
 1873 military hospital 
 Government Offices Washington ( town 
 book music ) , 
 1873 year death ( 1892 ) 
 invalid Camden gentle traveller West 
 North . observe military music 1862 
 1865 , natural sound — particularly song 
 bird — year 1875 1879 thereabouts . 
 occasion serve , concert 
 year invalidism ( 
 February 11 , 1880 , hear concert 
 Philadelphia , Beethoven Septet ) . mind 
 musically form fix new 
 modern music Liszt Wagner enter 
 America , likely circumstance 
 bring music life fully 
 thirty year bring , 
 change time . write 
 1891 : ' expert musician 
 present friend claim new Wagner 
 piece belong far truly ' italian 
 composer italian opera 1800 - 50 , ' 
 , likely . feed breed 
 italian dispensation , absorb’d , doubtless 
 . ignorant 
 develop music later year , 
 unsympathetic . ' poetry - day 
 America , ' write 1881 , quote 
 passage musical writer 
 adequately understand old italian music 
 form new french , german , 
 italian music escape — ' music 
 present , Wagner , Gounod , later 
 Verd . , tend free expression poetic 
 emotion , demand vocalism totally unlike 
 require Rossini splendid roulade , Bellini 
 suave melody . ' opera 
 influence — italian opera , french Auber , 
 german Flotow , english semi- 
 ballad type . instrumental music effect 
 . ' absolute ' music 
 slightly ; cou d decisively 
 exhaustive consideration 
 imply musical reference 1890 prose 
 passage entitle ' Death - Bouquet 

 Whitman deeply permanently 
 affected definite movement tangible life 
 opera , receive ' magnetic , electric thrill ' 
 music essential chiefly 

 mistaken pronunciation , ere 

 S : k,—Your correspondent Manchester 
 number Musical Times mistake 
 Catalani opera ' La Wally ' , , 
 pronounce ' Vallée ' French . pronounce 
 * Vallee ' ( course accent ) English . 
 Italy . wonder Mr. O'Mara 
 come comiposer good opera , 
 superior way discussion ? refer 
 * Loreley . ' dramatic libretto music , 
 contain beautiful vocal melody 
 charming ballet music frequently include high- 
 class orchestral concert . scope 
 beautiful scenic effect . time Italy 
 ' La Wally . ' allude discussion 
 paper regard Beethoven song - writer , surely 
 * Feste ' speak great Bonn master ' 
 failure song composer ' hear Beethoven 
 creation hymn , ' ah ! Perfido , ' fine ' questa 
 tomha , ' scarcely think ! — , & c 

 CLAUDE TREVOR 

 BEETHOVEN 

 51r,—i know stimulating writer musical 
 matter ' Feste , ' 
 agree . month look forward perusal 
 remark , rarely find 
 think . April 
 ' interlude ' disagree , 
 deep penetrating truth express 
 command respect . regret 
 think fit excuse temerity appeal 

 article raise important question 
 taste . literature , man delight read 
 drama , essay , lyrical poetry , 
 biography , blessed man find pleasure 
 . rare man ! 
 find fault taste devote line ; 
 taste perfect special department 
 interested . forget music 
 , , expressive temperament 
 literature , consequently music 
 good appeal , taste 
 fault find , simp'y temperament 
 composer listener compatible 

 doubt , , change time . , 
 instance , change political opinion ? | 
 hold year ago ; experience alter view . 
 frankness convince 
 experience . music . man ought 
 develop life , year ought able 
 mark different year . 
 way life rich , pity 
 dead preclude review year enthusiasm . 
 year find devour music Byrd 
 Farnaby , year specially enthusiastic modern 
 Britons , year hope lunatic 
 group . reach stage tire 
 Beethoven bored Haydn Mozart , 
 ecstasy Purcell , hope 
 shall . , , kaow 

 Feste ' hit root matter . 
 fashion art , fashion entirely artificial 
 pundit believe . time time 
 great movement thought leave 
 entirely unaffected expression taste . 
 20th century prevalent great dissatis- 
 faction complicated social system , product ofa 
 mechanical age . People fill aspiration 
 know define hope vague chaotic . 
 , look russian art — literature 
 music — do¢s perceive foreshadowing anarchist 
 revolution ? observe symptom oi 
 chaotic emotional outburst , somewhat disordered , 
 art — literature , painting , music alike ? 
 art good , point unsentimental time 
 verdict 

 April 13 , 1919 

 s1x,—may venture challenge statement 
 ' Feste ' effect Beethoven failure 4 
 song composer . revival old cry indicate 

 return mental inertia mid - victorian era — 
 song easy , good 

 lead ask , ' Feste ' study Beethoven 
 song , sing play — study 

 ter 
 — 
 " nthe 

 299 

 sonata symphony , reveal beauty ? 
 study , right dogmatise 
 subject , study , ask 
 formulate critical analysis , song 
 failure . render task difficult , 
 list song Beethoven , ask ' Feste ' 
 reason failure achieve greatness , 
 parallel list song composer 
 reach high standard : ' die Hoffnung , ' op . 12 ; 
 ' Adelaide , ' op . 48 ; ( Cycle ) ' Sechs Lieder von Gellert , ' 
 Op . 48 ; ' Mignon , ' op . 75 , . 1 ; ' Wonne der Wehmut , ' 
 op . 83 ; ' Der Wachtelschlag ' ; ' abendlie 

 symphony , surely ' Feste ' 
 wish exchange austere purity Beethoven 
 score vulgarity modern orchestra ! fact 
 fever modern life reveal modern orchestral 
 ( ) music prove feverish state 
 preferable health . Beethoven achieve colossal effect 
 ( spiritual aural ) limited orchestra . Strauss 
 achieve colossal noise dint strain resource 
 modern orchestra 

 ' Feste ' desirous argue noise synonymous 
 power , humbly suggest - score bar 
 Symphony . 5 timpani G E 
 flat . surely colossal noise , sole raison - d'étre 
 modern orchestral work 

 E1n Fest Bure 

 s1r , — ' Feste ' entirely miss point letter , 
 deal , question opinion , 
 fact . state ' claim 
 forward Beethoven failure song 
 composer . ' quotation contrary book 
 Ritter Parry — book ' Feste ' ought 
 acquaint . reply , extract book 
 Sir Henry Hadow , prove poor 
 opinion Beethoven composer song . ( second 
 quotation serve mistaken purpose . ) 
 opinion , case 
 . man unacquainted , 
 forget , writing - know historian 
 Ritter Parry , unwise sweeping 
 assertion above.—Yours , & c 

 5 , Richmond Mansions , ARTHUR T. FrocGatr 

 Denton Road , Twickenham , 
 4 , 1919 

 Sirk,—In humble opinion Beethoven controversy , 
 ' Feste ' conduct ably , significant musica ! 
 event . soon later inestimable value 
 cause music , hope wili let 
 drop soon . quarrel ' Feste ' lenient 
 Beethoven matter limitation instrument 
 time , reason : surely 
 astounding fact musical history Weber 
 die Beethoven . Weber limitation 
 impose Beethoven , think 
 orchestral work need - scoring . 
 opening ' Oberon ' overture , instance , sound 
 modern - day play . 
 ' Feste ' discuss point future article , 
 oblige ? — , Xc . , Rosert E. Lorenz 

 14 , Craven Hill , W.-2 

 Sir , — ' Feste ' wish - score Beethoven , 
 rate - scored—‘a matter letter versus 
 spirit ' : , ' care passage 
 stand ; obviously miscalculation 
 Beethoven . let - score symphony — bring 
 line modern orchestral requirement ; let 
 spirit Beethoven letter 

 unfortunately spirit Beethoven elusive . 
 sure result titivation 
 spirit ' Feste ' Sir Hugo Burls 
 learn collaborator 

 , ? add instrumental 
 ? — increase volume sound ? — 
 ' jolly good row ' ? , ' spirit Beethoven ' 
 produce sacrifice Jumbo 

 know sad fate Bach . day week 
 hear pianist strain muscle body 
 thump thunder ' Bach - - piano- 
 forte arrangement , ' plod 
 Preludes Fugues approved bag - pipe manner — 
 grace , sz / ences d’articulation , 
 appoggiature leave play short , , 
 doubtless , arranger editor think 
 reproduce spirit Bach 

 Grosvenor Square 

 unable print letter receive 
 subject . majority writer decidedly 
 view suggestion ' Feste . ' opinion , 
 , carry weight 
 apparently impression good way 
 defend Beethoven attack modern composer 
 modern orchestra . request 
 continuation discussion , view present 
 great musical activity , consequent demand 
 space , close . ' Feste ' deal 
 letter ' interlude . ’.—Ep . , a/.7 

 Obituary 

 early month 1914 London 
 acquainted Mr. Albert Coates lead personality 
 english musician . come Russia conduct 
 Covent Garden Winter season opera German — 
 memorable season bring ' Parsifal ' London . 
 mastery conductor art evident 
 Royal Opera Syndicate engage follow 
 Summer season , conduct german italian 
 opera . beginning war find Russia . 
 short account Mr. Coates life work , 
 portrait , issue April , 1914 

 glad welcome Mr. Coates England 
 protract trial war - time . - introduce 
 concert familiar music Queen Hall April 29 , 
 Beethoven seventh Symphony principal work 
 hand . doubt mastery 
 orchestra ability draw meaning 
 music , room question — recur 
 ' Siegfried Idyll’—on subject semi . 
 prone dwell lovingly passage slow 
 music bend squeeze dry allow 
 natural flow . Tchaikovsky * Romeo 
 Juliet ' Mr. Coates assert gift loophole 
 questioning . great interpretation , finely design 
 eloquent . atthe second concert , 8 , Mr. Coates 
 conduct Tchaikovsky ' pathetic ' Symphony 
 special insight expect , Wagner 
 excerpt , Saint - Saéns ' Africa ' Fantasie , Miss 
 Hilda Dederich solo pianist . concert , 
 Mr. Coates 16 , devote russian music . 
 Tchaikovsky loom largely scheme , 
 represent ' Romeo Juliet ' b flat minor 
 Pianoforte Concerto , play Miss Katharine Goodson , 
 interesting hear Rimsky - Korsakoff Suite 
 ' Legend Tzar Saltan ' invigorating rhythm , 
 Scriabin ' Poéme de l’extase . ' work 
 hear London , memory serve 
 correctly , strong impression richly 
 woven design . intervene year enable perceive 
 strand texture clearly , know 
 harmony , consider exotic , 
 typical Marylebone Kensington . skill 
 Scriabin weaving open 

 instrument — double - bass , violin , flute , clarinet , horn 

 new Violin Sonata ( op . 165 , . 2 ) , Sir Charles 
 Stanford play Miss Murray Lambert Mr. 
 Hamilton Harty Wigmore Hall 7 , prove 
 characteristic composer merit , tend 
 unusually simple style 

 Mr. Frederic Lamond great popular gift 
 display ( Queen Hall Beethoven 
 * Emperor ’ Concerto Tchaikovsky b flat minor . 
 gratifying observe British - bear pianist , 
 appeal strength purely - musical value 
 interpretation , display , big following , 
 attract large interested audience . 
 accompany occasion ( ueen Hall 
 Orchestra , Sir Henry Wood , conduct 
 performance Bantock characteristic picturesque 
 tone - poem , ' fifine Fair 

 new Violin Sonatas , E. J. Moeran Miss Mary 
 Barber , perform Wigmore Hall 10 
 M. Edgardo Guerra , accompany composer , 
 occasion joint recital M : José de Moraes ( vocalist 

 CAMBRIDGE 

 Messrs. AlLert Sammons William Murdoch 
 recital Guildhall 7 crowded house . 
 programme consist Bach Violin Pianoforte 
 Sonata E major , Beethoven ' Kreutzer ' Sonata 
 Romance G , Mozart Rondo G , piece Debussy . 
 week concert University Musical Society 
 place June 6 , item perform 
 Bach ' sing ye Lord , ' ' Emperor ' Concerto 
 Beethoven , ' Wasps ' overture Vaughan Williams , 
 ' L'Aprés - m di d’un faune , ' Debussy 

 membership Musical Club continue increase , 
 committee contemplate acqui - ition large 
 premise . weekly concert hold usual , 
 standard programme keeping 
 tradition Club 

 choir voice form G.W.R. man 
 Newport , sing enthusiasm , rich tone - quality , 
 fine precision Exeter , 10 , Mr. W. H. Bryant 
 conduct 

 Mr. Vladimir Cernikoff , Torquay , April 16 , play 
 pianoforte music Bach , Schumann , Beethoven , Liszt , 
 Chopin , introduce cradle Song ' Scaramouche ' 
 Holland . Madame Alice Montague special skill 
 song Purcell . Easter week find band 
 R.M.L.I. Plymouth day ’ season 
 Pavilion , Mr. P. S. G. O'Donnell conduct . 
 concert Miss Margaret Cooper party , 
 Madame Melba Miss Katharine Goodson vocal 
 pianoforte recital Saturday afternoon 

 organ recital Budleigh Salterton 3 , Mr. 
 F. J. Pinn play Andante variation Rea , 
 music Cooke , Widor , Callaerts , Silas , Merkel . Mr. 
 S. J. Bishop sing beautiful Prayer Kalinnikov , 
 air ' 

 LEICESTER.—An exacting programme adopt 
 Mr. Vincent Dearden Ladies ’ Choir concert 
 Edward Wood Hall 5 . include 
 Coleridge - Taylor ' green heart water ' 
 ' lambkin ? ' Vaughan Williams ' Sound 
 sleep , ' Colin Talyor ' slumber Song Madonna , ' 
 Percy Fletcher ' follow Carlow 

 NorwicH.—The Philharmonic Society bring _ 
 series concert successful conclusion 
 present season Thursday , 8 . chief attraction 
 Elgar Violin Concerto , hear 
 time Norwich . Albert Sammons soloist , 
 play magnificently . ably support 
 orchestra . work splendid reception 
 crowded audience . orchestral work 
 Tchaikovsky fifth Symphony Beethoven ' Egmont ' 
 Overture . vocalist Miss Minnie Searle , sing 
 Mozart ' non mi dir ' ' Don Giovanni , ' group 
 song pianoforte accompaniment complete success . 
 Mendelssohn ' hymn praise , ' evening , 
 mark fine sirging choir . Miss Searle , Mr. 
 A. E. Benson , Chorister Wilson soloist , 
 work receive enthusiasm audience . Dr. 
 Frank Bates conduct concert 

 SurroLk — multitude military East Anglia 
 war , remain musical talent busily 
 employ , expect energy 
 choral socigtie musical body remain dormant . 
 aspect war end , evidence 
 lack general awakening , Autumn find 
 enthusiast alive . Spring 1915 - know 

 COSMOPOLITAN AUDIENCE 

 Mr. Frederick Kitchener pianoforte recital 
 Shepherd Hotel , Cairo , April 27 , play Prelude 
 Fugue Bach , Beethoven C sharp minor Sonata , selection 
 Schumann ' Carnaval , ' study Chopin , 
 piece Moszkowski , Paderewski , Cyril Scott.and Liszt . 
 audience include Sir Edmund Allenby Lady Allenby , 
 french , american , spanish Ministers . 
 audience 160 nationality — 
 english , australian , american , french , italian , spanish , 
 greek , russian , danish , egyptian , turkish , armenian , 
 albanian , syrian , jewish . programme print 
 French . chance Esperanto 

 announce Carnegie United Kingdom Trust 
 connection music publication scheme 
 - work send year compare 
 seventy - year , quality variety 
 work submit encouragingly good . adjudicator 
 unanimously recommend follow publication : 
 Rhapsodies String Quartet , George Dyson ; 
 * hound Heaven , ' solo baritone , chorus , 
 orchestra , William H. Harris ; ' hymn Jesus , ' 
 chorus orchestra , Gustav Holst ; sextet 
 string , G minor , P. H. Miles ; Sir C. V. Stanford 
 Symphony . 5 , ' L ’ Allegro ed il Pensiercso 

 Majesties King Queen Majesty 
 Queen Alexandra graciously signify willingness 
 accept copy ' Westminster Pilgrim 

 Beethoven Festival Queen Hall , 19 - 24 , 
 occur late notice month . account 
 appear July number 

 hnswer correspondent 

