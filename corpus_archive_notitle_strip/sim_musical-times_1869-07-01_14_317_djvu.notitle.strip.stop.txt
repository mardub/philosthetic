MUSICAL TIMES.—Juny 1 , 1869 

 INCIDENTS LIFE BEETHOVEN . 
 R. M. HAYLEY . 
 ( Concluded p. 44 

 Tne following description Beethoven , drawn 
 Englishman visited period 
 life arrived , affords 
 interesting details 

 28th September , 1823 , re- 
 membered dies faustus ; , 
 know passed happier day . Early 
 morning , accompanied intimate 
 friends , paid visit Beethoven village 
 Baden , near Vienna , time 
 residing . ‘ difficulty obtaining ad- 
 mittance . seeing , Beethoven stared , 
 immediately shook heartily hand , 
 old acquaintance . remem- 
 bered distinctly previous visit paid 
 1816 , short duration ; 
 proof excellent memory . observed 
 occasion , great regret , perceptible 
 change appearance ; immediately struck 
 expression countenance betokened 
 great unhappiness . subsequent conversation 
 friend confirmed fears . appre- 
 hensive able hear word 
 said . , , mistaken , com- 
 prehended . spoke loud slowly 
 , use speaking - trumpet ; 
 evident replies word 
 lost . , , observe , 
 played piano , generally touched notes 
 s0 violently , thirty strings 
 suffered consequence . in- 
 tellectual , animated , , use expression 
 ‘ isso applicable symphonies , 
 canbe energetic conversation 
 good humour . in- 
 judicious question , ill - timed advice , especially 
 reference deafness , sufficient 
 estrange . wished ascertain 
 highest possible compass trumpet , piece 
 music engaged composing , 
 asked Herr H — — ; answer , how- 
 , appear satisfy . told 
 , general , sought informatiun respecting 
 , nature , compass principal in- 
 ‘ struments , different makers 

 establish , fact , itis inconceivable 
 : adhered firmly . soon 
 pressed , wavered ; fact non - completion wrung 
 reluctantly publishers , allowed Siiss- 
 mayer public declaration pass single dissenting 
 contesting word ! , communications , strict 
 confidence , Stadler André , hesitation 
 ( positively corroborating Siissmayer story . statement . 
 made.almost death - bed , positive conclusive 

 little set overwhelming con- 
 currence.of evidence , argument gathered music 
 . argument strong prove presence 
 ( Mozart ideas Mozart genius 
 Requizm , appears generally conceded ; . enoreh 
 prove entire score proceeded pen 

 introduced nephew , handsome 
 young man eighteen , relative whon 
 lived friendly terms . presenting , 
 said , ‘ try puzzle Greek , 
 like ; ’ intimating youth familiarity 
 language . history young man placeg 
 Beethoven goodness heart favourable 
 light . affectionate father haye 
 greater sacrifices Beethoven ip 
 case ... .. 
 hour , agreed meet o'clock , 
 romantic valley close , dine , 
 visited baths , objects interest , 
 returned Beethoven house , 
 waiting , set valley , 
 Beethoven excellent pedestrian , delights 
 long walks , especially amidst wild romantic 
 scenery . assured passes 
 nights excursions , absent 
 home days time . way tothe 
 valley stopped suddenly point 
 beauties prospect , remark defects 
 buildings passed . time 
 absorbed thought , merely hummed 
 indistinctly walked forward . told 
 manner composing , 
 wrote note till formed definite plan 
 piece . day uncommonly 
 fine , dined open air ; , appeared 
 particularly Beethoven , 
 guests hotel , interrupted 
 day . ‘ repast prepared 60 
 luxurious , Beethoven help remarking 
 . ‘ different dishes ? ’ said , 
 ‘ Man raised little animalsif 
 chief enjoyment consists pleasures 
 table . ’ similar observations 
 meal . likes fish , trout 
 favourite . Restraint kinds utterly abhors ; 
 think man Vienna : 
 speaks , political subjects , little 
 reserve Beethoven . hears difficulty ; 
 speaks remarkably ; observations 
 characteristic original music . 
 interested , course con- 
 versation table , remarks respecting 
 Handel . Isat : , heard distinctly 
 , German , ‘ Handel greatest composer 
 lived . ’ describe feeling , 
 , enthusiasm , spoke 
 Messiah , masterpiece immortal genius . 
 feel emotion added , ‘ 
 uncover ‘ head kneel grave . ’ 
 endeavoured repeatedly lead conversation 
 Mozart , vain . merely heard , ‘ Ina 
 monarchy know ; ’ expression 
 refer subject sought intro- 
 duce . heard Beethoven 1s 
 inexhaustible praise Mozart 

 Tt remarkable Beethoven bear 
 hear earlier works commended ; told 
 surer way making angry 
 praising . prefers -works . — 
 second Mass considers best . 
 present employed writing new opera , called 
 Melusina , text pen 
 poet Grillparzer . Beethoven great admirer 
 ancients . prefers Homer ( especially 
 Odyssey ) Plutarch allothers . poets vf 

 country 

 d pianoforte , violin , violon- 
 cello , whilst manuscript . appeared 
 beautiful , , hear , soon pro- 
 duced London . relate 
 extraordinary man , , seen 
 heard , inspires profound 
 admiration . kindness treated 
 , friendly manner bade 
 farewell . left impression 
 long life 

 reception Beethoven gave brother 
 artist , Carl Maria von Weber , year 1823 , 
 described celebrated composer 

 called times , 
 bad humour , shunned society . 
 lucky catch favourable moment 
 shown room , found seated 
 writing - table , rise 
 welcome . Beethoven known 

 ears , enter conversation 
 tim , Suddenly sprang , stood upright 
 , laying hands shoulders , shook 
 kind rough cordiality , said : ‘ 
 capital fellow . ’ 
 words embraced affectionately . 
 allthe marks distinction received Vienna , 
 affected deeply fraternal embrace 
 Beethoven 

 physical sufferings 
 wholly exempt , intense 
 years life , added mortifica- 
 tion finding forgotten public 
 Vienna , time intoxicated sweetness 
 Rossini melodies . circumstances , 
 true lovers art presented address 
 Beethoven , , expressing high 
 admiration genius , earnestly requested 
 tpeedy production works , viz . , 
 Ninth Symphony , andthe Missa solennis . 
 concert took place , pieces per- 
 formed . author incapacitated 
 hearing ; , , listening 
 rapturous boisterous applause 
 greeted . asked turn round , 
 seen delight pictured 
 countenance , hearty manner 
 manifested . Strangely , repetition 
 performances house nearly ; 
 showing capricious breath popular 
 favour . ear populace captivated 
 strains rival artist , Beethoven catered 
 vain 

 determined offer Mass 
 referred , manuscript , 
 courts Europe ducats ; 

 peror Russia , Kings France , Prussia 

 Beethoven wrote Cherubini , received 
 answer . years life received 
 publishers fair remuneration 
 works . Sonatas Quartetts 
 paid eighty ducats , 
 received little . ‘ , 
 , wanting instances 
 robbed - earned fruits labour . 
 Russian Prince , ordered 
 Quartetts , agreed pay 150 ducats , 
 withheld money ; , frequent demands 
 Beethoven , 
 sent 

 Bitter disappointments , Beethoven 
 endure severer trial base ingratitude 
 | nephew , years 

 great sacrifices , subjected 
 | privations . visiting brother 
 ; John , returned Vienna nephew 
 |an open carriage , 2nd December , 1826 . 
 ‘ weather time unusually inclement , 
 ithe exposure severe season produced 
 Beethoven disastrous consequences . 
 attacked inflammation lungs , 
 dropsy supervened . unable obtain 
 advice physician , till 
 days elapsed , Dr. Wavruch , having 
 heard chance illness , 
 medical assistance , hastened . 
 , nearly months , joined 
 physician , friend Beethoven , 
 malady progress , operations 
 relieve patient found necessary time 
 time . Whilst deplorable condition , 
 sufferings aggravated apprehension 
 soon deprived merest necessaries , 
 funds command 
 100 florins . occurred apply 
 Philharmonic Society assistance ; accord- 
 ingly , wrote Moscheles , London , 
 answer evinced painful feelings 
 drawn forth tale distress . letter 
 accompanied sum £ 100 , Society 
 forwarded alleviation wants , request 
 application 
 stand need additional assistance 

 warnings approaching end Beethoven 
 bore resignation ; , little 
 life happy , prospect death 
 beset terror 
 anticipated . hastened arrange worldly 
 affairs , bequeathing unworthy nephew every- 
 thing possessed , exception original 
 scores later works , wrote 
 hand , left , especially 
 latest period life , given 
 abundant proof sincerest disinter- 
 ested friendship . time planning 
 new works , including Oratorio en- 
 titled , Triumph Cross ; , 
 severest sufferings , length paid debt 
 nature , whilst surrounded brother John 

 Saxony , accepted offer . Beethoven pri- 
 vately asked , atthe instance Russianambassador 
 Vienna , order knighthood 

 acceptable money ; decided 

 ess , words , Donné par le roi|March , found countenance entirely changed 

 M. Beethoven , 

 occasion|and weak greatest 

 MUSICAL TIMES.—Jtty 1 , 1869 

 exertion able articulate words . Dr. 
 Wavruch came soon . looked 
 afew moments , said ‘ Death 
 far distant . ’ day 
 , great desire 
 reconciled heaven , world 
 died true Christian . Dr. Wavruch begged , 
 names friends , receive rites 
 Church ; , perfect 
 calmness , replied ‘ . ’ Rector 
 Parish came midday , Beethoven received 
 Sacrament edifying devotion . 
 think end near , for| 
 scarcely Clergyman left room , 
 said ‘ Plaudite amici comedia finita est . Tawards 
 evening unconscious , began wander 
 conversation . continued state till 
 evening 25th , evident 
 dissolution fast approaching . lingered , 
 , till following evening , died a| 
 quarter past 

 arrangements funeral super-| 
 intended friends Stephen von Brenning A. | 
 Schindler . took place 29th March.| 
 ‘ bier followed large concourse persons | 
 rank condition , house where| 
 died neighbouring Church his| 
 obsequies celebrated . earthly remains 
 conveyed Cemetery , Anschutz 
 actor , pronounced funeral oration composed 
 Grillparzer . silver medal struck memory , 
 bust soon adorned halls re- 
 sounded music 

 following brief description Beethoven 
 personal appearance uninteresting . 
 ‘ + feet , inches height , strong 
 compact build , muscular . head 
 large , covered long , shaggy gray 
 hair , unfrequently rested shoulders . 
 forehead high broad , 
 laughed small brown eyes disappeared 
 altogether . soon flash 
 , pupils invariably turned 
 upwards , idea struck , remain fixed 
 motionless long abstracted gaze . 
 moments underwent sudden change , inspi- 
 ration , transformed appearance , 
 imparting imposing grandeur , 
 diminutive stature assume proportions 
 giant 

 , seen Beethoven character 
 evident largeness soul beamed forth 
 acts ; , diminutive stature , 
 great men generation sees . 
 thoroughly kind hearted , generous 
 degree , abhorring sort ostentation . 
 ‘ sympathy mean 
 unjust , hated false dealing . 
 stranger worldly wisdom , knowledge 
 mankind ; exhibited credulity child 
 matters caution experience direct . 
 musician , combined profound 
 knowledge music science happy 
 talent invention , melodies possess 
 utmost originality sweetness . earlier 
 works followed essential points practice 
 instrumental music guided Haydn 
 Mozart . genius sympathised alike 

 genial flow distinguished compositions 

 

 characterise works . sub . 
 sequently , Haydn devoted principally 
 Church music , Mozart laid foundation 
 endyring fame dramatic compositions 
 Beethoven followed entirely different path . 
 seclusion world led region 
 instrumental music ; cherished art 
 sake , pianoforte sole - sufficient 
 companion . compositions instry . 
 ment cycle creative powers 
 wo nt . better adapted treatment , 
 entering deeply nature 
 capabilities favourite instrument , Beethoyen 
 soon left great predecessors immeasurably 
 

 Beethoven took subject hand 
 expand mighty touch . absorbed 
 idea pursue train 
 thought insatiable avidity . enlarged 
 form chosen , conformity 
 rules art , brought wonderful 
 combinations excite utmost admiration , 
 initiated . Speaking Beethoven com- 
 positions , Mr. Schindler says , ‘ ‘ feelings 
 respect Beethoven music undergone 
 variation , save warmer . half . 
 score years acquaintance works , 
 repulsive attractive . 
 , felt mind fascinated pro- 
 minent idea , enthusiasm kindled 
 flashes genius , unlooked episodes , shrill 
 dissonances , bold modulations , gave u- 
 pleasant sensation . soon 
 reconciled ? appeared hard 
 soon found indispensable . gnome - like 
 pleasantries appeared distorted , 
 — stormy masses sound , found 
 chaotic — times learned love 

 piano , orchestral works grander . 
 bold genius freer loftier 
 flight . felt home , speak , instru- 
 mental world , unfortunate deafness having 
 debarred communion species , 
 revelled mass harmonious murmurs 
 evoked masterly genius . states 
 mind compositions pourtray reveal depth 
 fervour feelings . composer 
 qualities tenderness force happily 
 blended ; long music charms ears , 
 beauties understood appreciated , 
 Beethoven stand con- 
 temporaries successfully cultivated 
 untiring ardour , rarest natural gifts 
 probability fell lot man possess 

 ROYAL ITALIAN OPERA 

 MUSICAL TIMES.—Juny 1 , 1869 

 exponent highest class sacred music . 
 - known air , ‘ “ Waft , angels , ” genuine 
 success ; audience scarcely rest contented 
 mere acknowledgment overwhelming 
 applause greeted . Herr Carl Stepan 
 fully sustained reputation bass ; Miss 
 Julia Klton sang music Storge dramatic 
 power expression , passages lying thoroughly 
 register ot Madlle . Drasdil , 
 occasion representative . 
 Miss Banks desired 
 music Jphis ( beautiful air , « Farewell , ye limpid 
 springs , ” sung utmost pathos ) , Miss 
 Annie Sinclair gave recitatives allotted 
 intelligence expression . 
 spoken success choir fine choruses 
 Oratorio abounds ; 
 occasion effect Handel massive writing 
 — especially overwhelming chorus , “ 
 loud voice,”—was greater , audience 
 prolonging applause perseverance worthy 
 better object , considering shat object effect 
 repetition entire chorus , utter destruction 
 general effect work . glad find 
 Concerts successful 
 given , commencing December . 
 works performed , Bach Passion 
 Music , Spohr Judgment bBeethoven Mass 
 D , Choral Sympho - y , Handel Dettingen Te 
 Deum , Acis Galatea , lesser known 
 Oratorios named , promise showing 
 enterprise highly praised . Mr. 
 Barnby earned position ; fur apart 
 having produced , utmost success , seven great 
 musical works months , 
 thank proving practical advantage adhering 
 diapason normal , steadily resisting 
 system encores , reforms , desirable 
 considered , 
 unflinchingly carried 

 PHILHARMONIC SOCIETY 

 Ar Sixth Concert , took place St. James 
 Hall , 31st , Symphonies Haydn , 
 B flat , known “ La Reine de France ” — work 
 somewhat weak , historically interesting , having 
 = celebrated , composed 

 ndon — Beethoven , B flat ( . 4 ) . orches- 
 tral novelty Prelude Wagner Lohengrin , 
 produced decided effect audience 
 unanimously encored ; proving 
 indefinite period named “ future ” 
 commenced . specimen com- 
 positions innovative writer Wagner crept 
 programme conservative Society like 
 Philharmonic , impossible conjecture ; certain 
 original thoughtful piece orchestral 
 eolouring , resembling dreamy beauty glowing 
 — daring artist Turner — 

 time works ‘ future ” — inevitably 
 assert , virtue innate power human 
 sympathies defies canons criticism 
 laid artistic law - givers . _ M. Vieuxtemps ’ inter- 
 pretation Mendelssohn Violin Concerto , scarcely 
 realised intellectual beauties work fullest 
 extent ; facility execution , perfect command 
 mechanical difficulties contains . 
 performance entire composition 
 bedesired . Weber Voncert - Stiick played 
 utmost delicacy , dramatic feeling . 
 Fraulein Mehlig , tho : oughly enter 
 spirit little story object 
 composer illustrate . vocalists Madame 
 Volpini Mr. Santley , gave.a Canto 
 Infernale , « Lucifero , ” A. Graffigna , , spite 
 excellent singing , produced littleeffect . seventh 

 Concert took place 14th ult . , Protessgr 
 Bennett Symphony G minor , composed expressly fog 
 Society 1864 , performed marked succesg 
 movement played , received 
 warmest applause , Minuet thoroughly charmi 
 hearer - demanded enthusiasm , 
 end Symphony Professor Bennett called 
 forward receive congratulations sq 
 thoroughly earned . Symphony Beethoven 
 “ Kroica , ” Mr , Cusins ’ careful direction , 
 went admirably , Madame Norman - Nerudg 
 delighted everybody ’ exquisite violin playing , 
 ad : itionally pleased hear- 
 better music Rode Seventh Concerto . 
 vocalists Madame Monbelli Signor Verger . 
 extract Wagner works , vigorous March 
 Zannhéuser , played audience 

 CHARITY CHILDREN ST . PAUL 

 - Celebrated Songs man 
 WEBER . Sonatas , complete ... ans 
 — Pianoforte Pieces 

 PIANO DUETS , 
 BEETHOVEN . ee 3 arranged Ulrich 

 BACH . - Preludes Fugues . Vols.1&2 . 4 
 BEETHOVEN Thirty - Sonatas , eo 
 — Pianoforte Pieces ( Rondos , ms - os 
 — Variations . i. ove 
 CLEMENTI . ‘ Sonatines 2 
 HANDEL . Sui'es I.—VIII . ae = oon 
 — Suites IX.—XVI . Boe ves 
 — Chaconne , Lecons , Fugue eee eae os ® 
 HAYDN . Celebrated Sonatas 
 MOZART . Eighteen Sonatas , complete - 4 
 — Pianoforte Pieces ( Rondos , & c. ) .. ‘ > 
 SCHUBERT . Pianofo : te Pieces , _ 15 , 78 , 90 , 4 , 142 3 
 — Sonatas , complete | 
 — — Dances ove oon ee 
 — — Songs arranged : — Sehine Miillerin ae 
 interreise ... ao 2 

 2 

 2 

 . l — woo 6 
 Vol . IL.—No . 9 ae 5 
 — " Sep tett uw 
 HAYDN . Celebrated Symphonies arranged ws Ulrich : — 
 Vol . IL — , 1—6 
 Vol . 1.—No.7—12 4 
 MOZART . _ — Symphonies arranged Ulrich : — - 
 o. 1—6 ies ooo 
 — Sonatas bevy Compositions “ ae 
 SCHUBERT . Original Compositions 3 vols . , soo 
 PIANO VIOLIN . 
 BEETHOVEN . Sonatas , complete ase ae 
 MOZART . Eighteen Sonatas , complete os WO 
 SCHUBERT . Sonatines Rondo , Op . 70 3 
 — Songs arranged : — Schine Miillerin aes ae ® 
 Winterreise ... os 38 
 Schwanengesang om ie 
 - Celebrated Songs aE 
 PIANO VIOLONCELLO . 
 SCHUBERT . Songs arranged : — 
 Schéne Miillerin .. ove oe 8 
 Winterreise coe OS 
 Schwanengesang awe 8 
 - Celebrated Songs oe 3 
 PIANO TRIOS . . 
 SCHUBERT . Trios , Op . 90 100 , Pianoforte , Violin 
 Violoncello ae ove 4 
 QUARTETTS , 
 HAYDN . Eighty - String Quartetts , complete ow 40 
 MOZART . Celebrated String Quartetts ... ooo 12 
 — Seventeen String Quartetts sab ose eve 12 
 QUINTETTS . 
 MOZART . Celebrated String Quintetts ... uw 
 — String Quintetts ... 6 
 SCHUBERT . Celebrated Forellen Quintett , Op . 114 , 
 Pianoforte String Instruments 3 
 SONGS . 
 BEETHOVEN . Songs.complete _ ... 4 
 SCHUBERT . Album , containing — Schine Miilierin , com- 
 plete ; Winterreise , complete ; Schwanengesang , com- 
 plete ; - aac ante original 
 Edition ... oo 4 
 — — Ditto , ditto , deep voice ove & 
 — — Schine Miillerin , original Edition 2 
 — — Ditt » , ditto , deep voice ooo eve ow 32 
 — _ Winterreise , original Edition ... ine ao 3 
 — Ditto , deep voice ... eee 2 
 _ — Schwanengesang original Edition 2 
 — — Ditto , deep voice ... 2 
 — Twenty- -two celebrated Songs , original Edition 2 
 — — Ditto , ditto . deep voice 2 
 — 8econd Album , containing — Seventy - “ celebrated 
 Songs , original Edition ie - _ @ 
 — — Ditto , ditto , fur deep voice ove ae oo & 
 SCORES . 
 BEETHOVEN . Pianof»rte Seeman ; int . ow . 4 
 — Ditto , ditto . B flat . bk oe 
 — — Ditto , ditto , C minor nike oo . 4 
 — — Ditto , ditto , G = ar ses oe 4 
 — Ditto . ditto , E flat . ooo oss 4 
 — Violin Concerto aie wee ose 4 
 WEBER . Concertsttick , Op . 79 ia aw 

 PIANO sOLO . Nett 

 oocoooo 

 PIANOFORTE SCORES . Nett . 
 ( g — German , f — French , Italian wat . 
 BEETHOVEN . Fidelio , g es owe G10 
 — Missa Solemnis ove ove ~ 40 
 CHERUBINI . Demophon , g , f ~ 8 0 
 — MassinF ... ue ow 40 
 — — Mass D minor ove ons pan - 40 | 
 — MassinA ... eco ose oe 4 0 
 — — Requiem male voices ooo eco ow 40 
 GLUCK . Orphée.g , f , ... ae ove oe 3 0 | 
 — Alceste , g , f ove aes ow 40 | 
 — Paris Helena , g , roe pie eee ow 40 
 — Iphigenia Aulis , g , f ee aa ww 40 
 — Armide . g , f aan cia wo 40 
 ae Iphigenia Tauris , af - ow 40 
 JOMELLL Requiem “ ow 490 
 MOZART . Don Giovanni , g , ¢ pam . - . 40 
 — — Figaro , 9 ... ase - eco oo . 40 
 — Zauberflite , g , 7 Pat jit eco ow 3 0 
 — King Thamos , g ovo sve ove - 40 
 ROSSINI . Il Barbiere , g , ¢ ae ove ow . 40 
 SPOWR , Jessonda . g oon ose ove - 40 
 WEBER . Freyschutz , g eee eee ow 2 0 
 SCHUMANN . Faust , g ... oo - 8 0 
 COMPLETE OPERAS ARRANGED PIANO ary 
 AUBER . Maurer und Schlosser nae oe 2 0 
 BEETHOVEN . Fidelio ... ae wee wo . 2 0 
 B LLLINI . Norma ove ov eee ow . 20 
 — — Sonnambula oe ene ow . 2 0 
 BOI LDIEU . Dame Blanche = ww . 2 0 
 DONIZEVTI . Lucia di Lammermoor ose 9 
 — — EKlisire d'amore “ soe 2 0 
 HEROLD . Zampa ie oe ow . 2 0 
 MOZART . Don Juan “ -- 20 
 — - Figaro ome oat pom 29 
 — Zruberflite aa aia 8 0 
 ROSSINI . Il Barbiere ... ae oo B ® 
 WEBER Freyschiitz woe sa ow 20 
 — ( ) beron aco ae oe oe - . 20 
 — — Eurvanthe ove ses ow SH 
 — Precioxa ... ae eas oe wo Om 
 BEETHOVEN . Egmont ce haa 
 BELLINI . Puritani oe ae ne 2 0 
 — Montecchi ¢ C.puletti eee ose ow . 2 0 
 BOIELDIEU . Jean de Paris aw OH 
 CHERUBINI + ate ( les Deux Journées ) wa Oe 
 GLUCK . Armide “ ie pen w « 8H 
 MEHUL . Joseph ads ee ove wow SH 
 MUZART . Seraglio ism aa vo wow 2 0 
 — Titus aap oe nia ow Se 
 RO - SINI . Otello Hs ee « 2 0 
 SPOHR . Jessonda “ 20 
 COMPLETE OPERAS ARRANGED PIANOFORTE 
 DUETS , 
 BEETHOVEN . Fidelio tee ven ooo - 30 
 BOIEKLDIEU . Dame Blanche eco mae .. 80 
 MOZART . Don Juan 2 “ ww 40 
 — — Figaro ase ono ane aw £ 0 
 — Zauberflite ane - 3 0 
 ROSSINI . Ll Barbiere _ ... ove pees uw CF 
 WEBER . Freyschiitz xe ihe - 3 0 
 OVERTURES PIANOFORTE SOLO . 
 MOZART . Overtures , complete — _ ian w. 2 0 
 BEETHOVEN . Overtures , complete aa ww Ae 
 CHERUBINI . Overtures , gee ia ott 
 WEBER . Overiures , complete 20 
 ee SPOHR , LINDPAINTNER . Celebrated Over- 
 BOIELDIEU , _ HEROLD , AUBER , SPONTINI . “ Celebrated oa 
 Overtur ove ooo 
 BELLINI , ROSSINI . Celebrated Overtures ... « wo £ 8 
 OVERTURES PIANOFORTE DUETS . 
 MOZART . Overtures , complete ... sad « 32 
 BEETHOVEN . Overtures , complete ... w. 3 0 
 CHERUBINI . Overtures , complete sa ~. 3 0 
 WEBER . Overtures . complete ... - 20 
 ane SPOHR , LINDPAINTNER . Celebrated Over- sa 
 BOIELDIEU , HEROLD , AUBER , SPONTINI . " Celebrated 
 Overtures . aes - 3 0 
 BELLINI , ROSSINI . Celebrated Overtures wo ae 
 STUDIES . 
 BERTINI . Studies , Op . 100 , Little Pieces ... 39 
 — — Studies , Op . 29 32 en Se < 
 — — Studies hands . Op . 97 ... ae wae 
 CLEMENTI . Gradus - ad Parnassum , Vol . I. _ ... oe 
 oe Vol . II . ... ee Oe . 
 Vol . IIL . ... aid 

 RODE . Caprices Violin { edit David ) ... ove 

 _ — _ — _ 

 trial - evening movement , Adagio , 
 Finale Haydn Quartette ( Op . 96 ) ; 
 movement , canzonette , Finale Mendels- 
 gohn Quartett ( Op . 12 ) ; movement , 
 Andante , Finale Beethoven Quintette ( Op . 4 ; . 
 place , selection movements 
 work , instead giving work entirety , 
 hardly judicious mode trying taste inclination 
 new hearers special kind musical composition ; 
 place , reserve affected 
 proclaimed putting forth experi- 
 ment establishment quartutte society , 
 hardly wondered attendance remark- 
 ably stinted . claim exclusiveness exercise chariness 
 matter , like wilfully limiting number 
 hearers select ; consequence 
 expected,—the seats room 
 sparsely occupied , merely 
 known constitute seventy eighty 
 special lovers instrumental chamber - music Genoa 

 Tue Concert Miss Emma Buer , 
 given 16th ult . , atthe Hanover Square Rooms , 
 contained programme interest , proved 
 Concert - giver right occupy high position 
 interpreter best pianoforte music . Beet- 
 hoven Trio D ( Op . 70 ) , pianoforte , violin violon- 
 cello ( ably assisted Mr. Henry Holmes 
 Signor Piatti ) , Cipriani Potter fins Duo 
 pianofortes ( enjoyed co - operation 
 instructor , Mr. Walter Macfarren ) , Mendelssohn 
 Duo pianoforte violoncello . Andun‘e variations 
 D ( violoncello played usual masterly 
 style , Signor Piatti ) , Miss Buer displayed exception 

 best native vocalists need assur- 
 ance occasion’she sang 
 usual purity intonation refinement feeling . 
 ably assisted vocal department Miss 
 Watts , Madame Rudersdorff , Madame Sainton - Dolby , 
 Miss M. Severn , Messrs. W. H. Cummings , Vernon Rigby , 
 L. Thomas , Signor Bossi . Instrumentalists : — Miss 
 K. Roberts Mr. W. G. Cusins ( pianoforte ) , Messrs. 
 J. B. Chatterton J. Thomas ( harp ) , Signor Piatti 
 ( violoncello ) , M , De Jongh ( flute 

 Mrs. Jonn Macrarren Concert , 4th 
 ult . , St. James Hall , displayed excellent pianist 
 qualifications utmost advantage . 
 pieces performed ( valuable co - operation 
 M. Sainton ) , Beethoven Sonata E flat ( Op . 12 ) , 
 Romances Walter Macfarren . new duet 
 pianof.rtes , themes Der Freischiitz ( com- 
 posed Mr. Benediét ) , played suc- 
 cess composer Mrs. Macfarren . vocalists 
 Madlle Liebhart , Misses Banks , Henderson , Sinclair , 
 Emmett , Edith Wynne , Mesdames Patey Sainton- 
 Dolby , Messrs. W. H. Cummings , Garcia Patey . 
 new song Randegger , new duet Miss Gabriel , 
 new songs Mori H. Holmes , sung Madame 
 Sainton - Dolby , Mr. Madame Patey , Miss Rober- 
 tine Henderson respectively . Concert 
 attended 

 Tue Annual Concert Miss Marian Buels ; 
 young pianist rapidly rising notice , given 
 4th ult . , Beethoven Rooms . principal 
 feature prograntme new Sonata , 
 composition , pianoforte violoncello , work 
 ordinary promise , excellently performed 
 composer M. Paque . solos played 
 Concert - giver success ; 
 joined Mr. W. G. Cusins Schumann Andante 
 variations , pianos . assisted Herr 
 Straus , violin , Misses Banks , B , Emmett 
 Fairman , Mr. 8 . Betjeman Herr Deck 
 vocal department 

 Mr. Henry Lestir benefit Concert , 
 present season , given St. James 
 Hall , 4th ult . , highly attractive programme 
 provided . pieces Choir — 
 included Samuel Wesley Motet , ‘ exitu Israel , ’ 
 Mendelssohn Psalm , “ Judge , O God”—the services 
 Mr. Sims Reeves secured , effective 
 solos , Beethoven “ Adelaida ” ( Madame Arabella 
 Goddard pianoforte accompaniment ) Blumenthal 
 “ Message , ” selected occasion . 
 interesting features Concert Weber 
 Sonata pianoforte clarionet , finely performed 
 Madame Goddard Mr. Lazarus , Thalberg Fan- 
 tasia “ Rose Summer , ” played per- 
 fection Madame Goddard . principal vocalists , 
 Mr. Reeves , Miss Edith Wynne ( sang 
 solo Mendelssohn “ Hear prayer , ” 
 excellent feeling ) , Miss Marion Severn Mr. Frank 
 Massey 

 MavaMe TurrEse LEUPOLD gave private 
 Soirée Musicale 5th ult . , Hanover Square 
 Rooms , distinguished patronage . Unfor- 
 tunately Concert - giver unable play piano- 
 forte works set programme , con- 
 sequence having hurt hand ; accompanied 
 vocal music sung pupils utmost 
 delicacy refinement . teacher , Madame Leupold 
 deservedly holds high reputation : easy 
 produce good singers pianists ; 
 able shew number trained equally 
 satisfactory results , proves definite system 
 applied , fail utter incapacity 
 pupils receive . singing Madlle . Tourrier 
 Glitck “ Ché fard , ” respect excellent 

 servedly applauded . Concert - givers aided 
 artists eminence 

 excellent Amateur Concert , aid 
 Clergy Orphan Schools , given Hanover 
 Square Rooms , 18th ult . , carefully - selected 
 highly « ffective programme provided . Etiquette 
 forbids entering detailed account 
 efficient manner pieces per- 
 formed ; refrain mentioning truly 
 artistic style pianoforte Beethoven 
 Trio B flat ( pianoforte , violin violoncello ) 
 played lady , generally 
 vocal music rendered care judgment 
 . room filled ; sincerely 
 trust noble Charity aid perform 
 ance given , materially benefit result ; 
 hear Institution effected , 
 effecting , real good , house neces- 
 sary organization available larger number 
 inmates 

 Mr. Lanspowne Corretn gave Morning 
 Concert 29th , Store Stre - t Rooms , be- 
 fore large audience . principal vocalists Miss 
 Rosabella Shackell , Madame Alfiardi , Miss Emilie Blanche , 
 Mics Ida Wilmot , Madame Bishop , Madame Montserrat . 
 Mr. Walter Reeves , Mr. Sianley Betjemann , Mr Row- 
 land , Herr Angyalfi . highly suc- 
 cessful . youthful instrumentalists , Master Surtees 
 Corne ( piano‘orte ) , Master Louis D’Egville ( violin ) , 
 gave pleasure solos ; Miss Jarman 
 Miss Helen Meredith , proved pianists 
 ability , - named lady Weber Concert- 
 Stiick , second piece called « L’Ecosse 

 Month ( Continued 

 EST , W. T.—Arrangements scores 
 ‘ iGreat “ Masters Organ . . 74 , » price 2s . , con- 
 tains : — Harmonious Blacksmith . .Air,.with Variations , 
 5th ‘ ‘ Suite de Piéees , pour le clavecin " ( Handel ) . Andantino , 
 « Pensée Musicales , ” Pianoforte , Op . 94 , . 2 ( F. Schubert ) . 
 Largo , ‘ Pianoforte : Sonata , major , . 2 , Op . 2 
 ( Beethoven ) . 
 JESTBROOK , W. J.—The Young Organist , 
 ‘ . 10 . price 1s.6d . , eontains:—Allegretto ( S. Sechter ) . 
 Andante , Sonata , Op . 14 ( J. B Cramer ) . Andantino , Sonata , 
 Op . 37 ( D. Steibelt ) . relude ‘ Fugue Organ ( Dr. 
 Boyce ) . Kind Jesus ward zur We't gebracht , Der kinder christa- 
 pend , Op . 36 ( N. W. Gade ) , Theme Op . 10 ( F. Schubert ) . 
 TAINER , JOHN.—<Arrangements Organ , 
 . 2 , price 2s . , contains;—Andante Pianoforte Duet 
 ( Mozart ) . Overture , ‘ ‘ Semele ” ( Handel ) . 
 OOPER , GEORGE . — Organ Arrangements , 
 . 30 , price 2s . , contains : — Adagio Sestetto , Op . 110 
 ( Mendelesohn ’ . Larghetto Sonata Concertante , Op . 115 
 ( Spohr ) . Andante Quintett ( Mozart ) . 
 ARMER , JOHN.—The Harrow Glee Book , 
 . 22 . sake somebody . 3d . 
 ALMER , L. S.—A Young knight met lady 
 ‘ fair . - Scng . 8vo . ,.1s . 
 London : Novello , Ewer Co 

 ready , price 2s . @d . , bound cloth , 
 PSALTER , Pointed Adapted 
 rAncient Ecclesiastical Chant ; Canticles , Proper 
 Pealms . Creed St. Athanasius , W. T. Best 
 Chants arranged Short Score immediately . 
 words , sung usual - harmony , 
 exactly : as'written . unison - singing preferred , har- 
 monization available Organ Accompaniment . 
 “ Venite ” printed Psalms foreach Day Month 
 Bovk . order remove al ! doubt manuer 
 ofpointiny words varying length Chants , 
 tosecure » ppropriate keys succeeding Psalms Morning 
 Offiee . * ‘ Gloria Patri ” » ppears place end 
 Psalm , pointed rhythm Chant require , 
 separately , 
 CANTICLES HYMNS Church , Creed 
 St. Athanasius , 3d . 
 London : Novello , Ewer Co. , 1 , Berners - street , 35 , Poultry 

 40 Thirty - cight Valses d’Albert , eminent composers 

 9 Christy Minstrel Song Book . ( Selection ) , 
 38 Fashionable Dance Book , Pianoforte . 
 37 Country Dances , Reels . Jigs , & c. , Pianoforte , 
 36 Christy Buckley Minstrel Airs Pianoforte . 
 35 Christy Minstre ! Songs . ( Second Selection ) . 
 34 Christmas A'bum Dance Music , 
 33 Juvenile Vocal Album . 
 32 Beethoven Sonatas , Edited Charles Hallé ( . 6 ) . 
 31 Beethoven Sonatas . Edited Charles Hallé ( . 5 . ) 
 30 Beethoven Sonatas , Edited Charles Hallé ( . 4 . ) 
 29 Con : ralto Songs , Mrs. R. Arkwright , & . 
 28 Beethoven Sonatas . Edited Charles Hallé ( . 3 . ) 
 27 Sets Qu : drilles ag Duets , Charles d’Albert , ec . 
 26 Thirty Galops , Mazurkas . & c. , d’Albert , & c , 
 25 Sims Reeves ’ Popular Songs , 
 24 Thirteen Popular Songs , Barker , Linley , & e. 
 23 - Juvenile Pieces Pianoforte . 
 22 Twen y - Christy Minstrel Songs . ( Selection . ) 
 21 Pianoforte Pieces , Ascher Goria . 
 20 Beethoven Sonatas , Edited Charles Hallé ( . 2 . ) 
 ly Favourite Airs Messiah . forthe Pianoforte . 
 18 Songs Verdi Fiotow , English words , 
 17 Pianoforte Pieces , Osborne Lindahl . 
 16 Sacred Duets . Soprano Contralto Voices , 
 15 Eighteen Moore Irish Melodies . 
 14 5.ngs , Schubert . English German Words . 
 13 ‘ Popular Duets Sopran > Contralto Voices . 
 12 Beethoven Sonatas . Edited Charles Hali¢ , . 1 . 
 1L Pianoforte Pieces , Wallace . 
 10 Pianoforte Pieces . Brinley Richards 

 9 Valses , C. d’Albert , Strauss . & c 

