WEDNESDAY MORNING , 11.30 , Concerto ( Lloyd ) , New 
 Work ( Hubert Parry ) , ‘ ‘ Requiem ' ’ ( Brahms 

 THURSDAY MORNING , 11.30 , ‘ ‘ Apostles ' ’ ( Elgar ) , 
 Symphony F ( Beethoven 

 THURSDAY EVENING , 8 , ‘ ‘ Holy Innocents " ’ ( Herbert 
 Brewer ) , ‘ ‘ Hymn Praise 

 Books , 1s . 

 title sufficiently indicates scope Albums , 
 volume consists Diabellis easy Duets , followed 
 progressive order Pianoforte Solos celebrated classical 
 composers , : Bertini , Kuhlau , Czerny , Clementi , Schmitt , 
 Haydn , Mozart , Dussek , Beethoven , Schubert , Schumann , & c. 
 folk melodies countries included , pupil 
 interested instructed . favourable 
 opinions expressed utility work , 
 found invaluable supplement Instruction Book 

 BOSWORTH CO . , 5 , PRINCES ST . , OXFORD ST . , W. 
 Letpzic , Paris , VIENNA 

 EX.11 . < = sf ees 
 Sf sh . Sh . ~ ° > 
 Oe ee Se 
 ( SS SSS 

 investigations necessary , 
 point specially clear : weighty 
 thematic material movement 
 ( including introductory Avdan / e ) drawn 
 theme ‘ Dresden Amen ’ ; , developed , 
 metamorphosed variety perfectly 
 astonishing ; composer stand- 
 point . works Mendelssohn 
 manifold use theme , , 
 important , master Mendelssohn 
 degree * ; Beethoven , 
 attempt similar transformations 
 theme . technique movement , 
 leitmotive , 
 Mendelssohn stretching hands directly 
 Berlioz Liszt , artistic grounds assigned 
 treatment 

 remainder article Herr Heuss argues 
 ‘ Dresden Amen ’ certainly 
 Mendelssohn Roman Catholic symbol ; 
 supposition considers ‘ impossible . ’ words 
 Catholic Evangelical version cum 
 spiritu tuo ’ ‘ und mit deinem Geiste ’ ) clearly 
 explain meaning use : typifies 
 Spirit God , armour struggle 
 Reformation , Protestantism , carried 
 . Space prevents giving writer 
 interesting explanation regard twofold 
 appearance ‘ Amen ’ theme struggle 
 truth depicted development section . 
 close notes ‘ wished - ideal 
 reached ; theme unable rise 
 dominant , characteristic interval “ Dresden 
 Amen . ” culminating point Mendelssohn 
 placed movement Luther 
 chorale brings work victorious end 

 OVERTURE Meistersinger ... Wagner . 
 SUITE D ae Bach . 
 OVERTURE Zauberflote Mozart . 
 SYMPHONIC VARIATIONS Elgar . 
 HUNGARIAN RHApsopy F Liszt 

 SYMPHONY C minor ( . 5 ) Beethoven 

 Magnificent interpretative excellence 
 representative programme . man 
 mettle,—strings brass — 
 result triumph concerned , 
 good augury success venture 
 watched sympathetic interest 

 444 MUSICAL TIMES.—Jvty 1 , 1904 

 proud premier choir trainer , 
 Dr. Henry Coward , fain believe 
 unique infectious enthusiasm tremendous 
 time trouble takes rehearsing 
 difficult choral work . rival 
 Professor Siegfried Ochs , conductor Phil- 
 harmonic Choir Berlin . occasion Professor 
 Ochs held fewer 117 chorus rehearsals 
 Beethoven Mass D . satisfied . 
 Assuming intelligence natural aptitude 
 Berlin Sheffield singers equal , 
 think Herr Ochs performance 
 gigantic work truly memorable . 
 wealthy ‘ friend music ’ arrange 
 provide funds friendly tournament song , 
 Berlin Philharmonic Choir 
 finest choirs compete proud 
 position ‘ choir world 

 reference performance said Mass 
 Sheffield chorus recent Kruse Festival 
 Herr Felix Weingartner , special corres- 
 pondent expressed Professor Ochs intense 
 disappointment effect produced _ 
 fugal movements ‘ gloria Dei 
 patris , bass trombones start subject 
 ZF ben marcato choral basses , tenor trom- 
 bone follows # choral tenors , alto 
 trombone contraltos , similar 
 stereotyped fashion , loud fugato 
 started . passages trombones 
 fail ‘ mix ’ choral tone , absolutely ruin 
 splendid effect rolling masses vocal 
 sound produce - meant 
 crude help . staunchest purist 
 pretend Beethoven use trombones 
 conditions specimen matchless orchestral 
 scoring . . choral parts 
 sung accuracy splendid tone 
 Sheffield stalwarts , doubling brass 
 insult singers need rude crutches 
 cover ground . annoyance 
 listener , feels trombonic doubling 
 vocal interferes utterly destroys 
 enjoyment superior instrument , human 
 voice . Similar instances works naturally 
 occur , ¢g . ‘ Rex tremende , ’ ‘ Confutatis , 
 ‘ Quam olim abrahx ’ Mozart Requiem . ( 
 aware trombone parts movements 
 Siissmayer ! ) points chorus singer feels 
 inclined argue : voice drowned 
 blatant trombones , use singing 

 true , suggests question : 
 Beethoven Mozart , 
 relied choruses large com- 
 petent accustomed ? 
 venture think altered 
 scores respect . 
 doubt old masters ’ crude use trombones 
 passages merely safe way backing 
 small , weak , incapable chorus . 
 necessity expedient passed , 
 performances competent choirs 
 time come spirit composer 
 considered letter slightly ‘ corrected . ’ 
 works shine greater beauty , 
 design movements way spoiled , 
 trombones turn generally 
 ‘ doubled ’ lower strings organ , blend 
 human voice . view 
 leading conductors important subject ? 

 meantime Professor Ochs taken bull 

 soldier sailor good authority 
 calling , thrilling song 
 oratorio ‘ Elijah , ’ reads , ‘ Lord man 
 war 

 Herr General - Musikdirektor Fritz Steinbach 
 born 1855 Griinsfeld Baden . studying 
 years Leipzig Conservatoire — 
 gained Mozart Prize — pupil 
 Nottebohm Vienna . ‘ owe every- 
 thing , ’ says Herr Steinbach , evident delight 
 recalls happy time spent famous 
 Beethoven scholar contrapuntist , especially 
 hours recreation , lessons , 
 teacher introduce eager pupil 
 deep fascinating mysteries Beethoven 
 Sketch Books , Nottebohm 
 fathom describe . Young Steinbach displayed 
 unmistakable talent composition , like 
 great colleague , Hans Richter , soon abandoned 
 creative interpretative art . 1880 
 appointed conductor Mainz Municipal Theatre , 
 post held distinction till 1886 , 
 followed Meiningen , Court 
 Conductor succession Hans von Biilow . 

 capacity laid solid foundation 
 fame conductor works classics , 
 especially Brahms . 
 acquaintance Brahms time previously , 
 recommended Steinbach 
 Nottebohm . close friendship great 
 master young conductor continued end 
 Brahms life . year passed 
 /meeting . summer Steinbach visit 
 ‘ revered friend Vienna Ischl , winter 
 | Brahms travelled Meiningen hear Court 
 lorchestra discourse Bach special delectation . 
 | assume Steinbach peculiarly uncon- 
 ventional readings , life feeling , 
 Brahms Symphonies works 
 composer fullest approval . Herr Steinbach 
 | title General - Musikdirektor given 

 Mr. Donald Francis Tovey lectured June 14 
 Musical Association subject ‘ Permanent 
 Musical Criteria . ’ following synopsis 
 discourse 

 individual work art microcosm.—Unsound- 
 ness abstract prior ? reasoning Art 
 kind general science.—True artistic laws 
 concrete , applicable work art 
 similarity material.—Principles 
 organic unity universal artistic laws.—Applica- 
 tions principles music , é.g . , laws part- 
 writing deduced organization types 
 music , choral instrumental , showing identities 
 principle beneath diametrically opposite results 
 different cases.—Relation organic laws 
 historic progress . — Different oposite meanings 
 term ‘ ‘ progress ” shown Monteverde , Bach , Haydn , 
 Beethoven , Wagner.—Classification composers 
 according grasp organizing principles 

 Meeting held 11 , Chandos Street , 
 Cavendish Square 

 JOHANNES BRAHMS 

 Johannes Brahms ! man like 
 music , broadly euphonious , strong dignified . 
 composer produce 
 trivialities . thinks matter , 
 certain close connection composers ’ 
 names music ? bearer 
 weightiest , noblest , greatest music , Johann 
 Sebastian Bach , provided ‘ additional numbers ’ 
 ‘ musical comedies ’ lived London 
 twentieth century ? Imagination boggles 
 thought . units Hans von Biilow musical 
 trinity — Bach , Beethoven Brahms — pretty 
 accurately summed stern noble names 
 bore 

 Herr Kalbeck great biography Brahms — 
 volume 500 pages recently 
 published — bids fair addition 
 series exhaustive biographical _ critical 
 works German writers noted . 
 shall disappointed eventually prove 
 valuable way Spitta ‘ Bach , Pohl ‘ Haydn , 
 Jahn ‘ Mozart , ’ Niecks ‘ Chopin . ’ author 
 possesses qualifications required 
 task — enthusiasm unbounded ; knowledge 
 adequate ; love painstaking 
 research touching , candour 
 embarrassing refreshing . 
 partisan goes saying . Admirers Liszt 
 devotees , lovers programme - music generally , 
 wince terrific strokes Herr Kalbeck two- 
 edged sword goes forth doughty deeds 
 hero greater glory . Good hater programme- 
 music @ /a Liszt , wearies 
 reading meanings programmes Brahms 
 sonatas , serenades , quartets , 
 hand knows distinguish Liszt 

 omposer , affection , Liszt 
 .oble , captivating man undisputed king 
 pia . sts . Taken , making allowance 
 author bias occasional indiscretions , 
 boo great value uncommon interest . 
 Lei ‘ thre Wanderjahre young lion - hearted 
 ‘ ion - maned genius described great detail 
 anc fascinating newness information . 
 ‘ plo 1 Johannes ’ shown lovable creature . 
 Mu known Brahms man 
 mas.cr , redounds credit . 
 child case father man . 
 Gre.ily man sterling character 
 adm red , impossible , reading Herr Kalbeck 
 volume , feel similar admiration youth 

 Biographies hitherto silent 
 Brahms mother — born Johanna Henrika 
 Christiane Nissen . learn 
 senior husband seventeen 
 years , having born Hamburg 1789 ; 
 married sister kept small shop — 
 - called ‘ Dutch warehouse’—in Ulrikus Street , 
 Hamburg , buttons , needles , thread , linen 
 goods sold ‘ ladies ’ lowly neighbour- 
 hood . Christiane blessed good looks , 
 pair beautiful , soulful blue eyes excepted . 
 weakling childhood , compelled , 
 result disease , limp life . 
 frail body dwelt sturdy soul strong 
 character . supervised household 
 arrangements ‘ establishment , ’ undertook 
 responsibility looking rooms 
 upper floor , let ‘ single gentlemen . ’ Hither 
 came Johann Jakob Brahms ( born June 1 , 1806 , 
 Heide ) , fliigelhorn player Hamburg 
 Civil Militia , quest board lodging . 
 comfortable , learnt admire 
 blue - eyed , sweet - natured Christiane womanly 
 qualities , committed foolishness , 
 quote author , eventually turned 
 stroke genius — militia musician married 
 Christiane June g , 1830 . daughter 
 born , little household quitted 
 shop Ulrikus Street moved 
 Bickerbreitergang ; abode proving 
 expensive , papa Brahms content 
 small rooms darkest , narrowest 
 lanes lowest , dirtiest quarters old 
 Hansa town . , 7 , 1833 , near 
 haunts thieves vagabonds , room compared 
 Beethoven humble birthplace Bonn 
 airy , comfortable hostel , genius 
 born brought greater deeper happiness 
 lives genuine Brahms - lovers know 
 appreciate , com- 
 poser , Bach Beethoven excepted . 
 rooms actually occupied Brahms family 
 unfortunately ascertained . great master him- 
 self point left tenement floor 
 ramshackle abode midst misery , 
 testimony contemporary dwellers house 
 confirm view . , floors 
 building similarly divided - roomed 
 tenements , Herr Kalbeck hesitates accept 
 suggestions rumours facts 

 Frau Brahms , learn , gifted remarkable 
 memory , enabled old age 
 learn heart Schiller poems . 
 devoted poetry , quick - witted inventive , 
 hobby taking form designing original patterns 
 needlework ; adept making 
 finest embroideries , generally 
 superior intelligence young husband . 
 doubt little 
 ‘ Hannes ’ ( called home ) inherited 

 Herr Kalbeck gives interesting particulars 
 boy teacher , Otto Friedrich Willibald Cossel , 
 reproduces facsimile earliest known 
 example Brahms writing . New Year 
 letter - year - old ‘ dutiful pupil ’ sends 
 ‘ beloved teacher ’ good wishes 1842 , thanks 
 kindness getting 
 music , apologises occasional laxity , promises 
 better future 

 thank Cossel frustrating 
 project completely ruined 
 career budding genius . lad 
 created sensation , concert 1843 , playing 
 difficult pianoforte Study Herz joining 
 father colleagues Beethoven Wind Sextet , 
 enterprising concert agent proposed taking 
 prodigy tour toAmerica . unnaturally boy 
 parents greedily swallowed golden bait 
 dangled eyes , cost Cossel great 
 real sacrifice avert knew 
 worst possible danger threaten gifted 
 pupil future . decided reluctantly 
 boy greatly attached , 
 place worthier hands . pretence 
 teach , pressed 
 little Hannes attention famous 
 Eduard Marxsen , , fruitless attempts , 
 finally extracted desired promise 
 excellent unwilling master parents 
 come terms - ‘ manager . ’ 
 result , boy people recognized 
 importance allowing Hannes study 
 distinguished teacher , proposed tour land 
 dollars came naught 

 rs el pl 

 regret place record death , age 
 - , Mr. William Augener , took place 
 Tunbridge Wells June 19 . eldest son 
 Mr. George Augener , - known music - publisher , 
 sympathy felt 

 death took place Baden , near Vienna , 30 , 
 age eighty - , Dr. Hermann Rollett , author 
 pamphlet ‘ Beethoven Baden , ’ author 
 reference Mr. C. A. Barry MusIcat 
 TiMEs February , 1903 , p. 102 

 Baume ( Manx ) Scholarship Royal Academy 
 Music competed September , 
 awarded promise branch music candidates 
 ages 14 22 years , subject certain 
 conditions . successful candidate entitled 
 years ’ free instruction Academy 

 Unanimously join 

 interesting find love mother - land showing 
 . instance , ‘ public concert 
 vocal instrumental music ’ given benefit 
 Mr. Flagg ( patriotic , - - way ) Boston 
 1769 , find advertisement information : ‘ 
 vocal performed Voices , conclude 
 Avztish Grenadiers . Bostonians 
 enterprising recorded ‘ order oratorio ’ 
 given Beethoven Boston banker . 1823 . 
 Mr. Elson tells : * time Beethoven 
 begin work entitled ‘ t Victory Cross ” 
 Viennese society , determined send work 
 ( abandoned ) Boston . ’ 
 surprised learn Beethoven delighted 
 commission ocean 

 Considering dominating influence Opera New 
 York present day , interesting turn 
 accounts early days . real beginning Opera 
 New York dates ‘ fall 1825 , ’ ‘ came 

 SIXTEENTH CINCINNATI FESTIVAL . 
 ( CORRESPONDENT . ) 
 New York , June 4 , 1904 

 Sixteenth Biennial Music Festival Cincinnati 
 took place days 11 15 inclusive . 
 Like predecessors , held 
 1873 ( interregnum 1877 allow time 
 erection Springer Hall , built 
 Festivals ) , Festival notice conducted 
 Mr. Theodore Thomas , like predecessors 
 distinguished participation English artists . 
 quartet singers — Miss Agnes 
 Nicholls , Miss Muriel Foster , Mr. William Green , 
 Mr. Watkin Mills , gave good account 
 , especially Elgar ‘ Dream Gerontius , ’ 
 Mr. Thomas central feature 
 meeting . choral works — Bach 
 Mass B minor , Beethoven Solemn Mass D major , 
 composer Choral Symphony — contralto 
 solos sung Madame Schumann - Heink instead 
 Miss Foster 

 Festival choir numbered 423 voices , divided 
 follows : sopranos , 166 ; contraltos , 136 ; tenors , 46 ; 
 basses , 75 . figures indicate choir 
 perfect balance , ; disproportion 
 men voices women 
 great effect thought , reason 
 male choristers decidedly superior 
 females training experience . ordinary purposes 
 orchestra numbered ninety men , 
 increased 130 players Bach Mass , Mr. Thomas 
 repeating significant modifications experiment 
 years ago instrumental parts work , 
 multiplying wind instruments choral accom- 
 paniments . nucleus Festival band 
 Chicago Symphony Orchestra , 
 Mr. Thomas direction thirteen years , , 
 threatened disruption , finally 
 hoped prove permanent basis , 
 explained previous letters . Cincinnati permanent 
 Symphony Orchestra , Mr. Thomas 
 bring believe purpose 
 Festivals promote local institution chorus , 
 nearly reinforcements brought 
 Chicago . circumstances scarcely necessary 
 ageing Mr. Thomas ( 7oth 
 year ) periodically provokes speculation future 
 Festivals . Personal equation plays important 
 management 

 programmes Mr. Thomas prepared 
 Festival uncompromising severity . Beethoven 
 Mass Symphony occupied evening ; ‘ Dream 
 Gerontius ’ associated - acts music 
 ‘ Grania Diarmid ’ Edward Elgar ; Richard Strauss 
 ‘ Death Transfiguration ’ ; great scexa ‘ Fidelio , ’ 
 sung Miss Agnes Nicholls ; ‘ Imperial Hymn , ’ 
 written Berlioz function Paris Exhibition 
 1855 . instrumental introit Bach Mass 
 Suite B minor strings flute , 
 circumstances peculiarly lame impotent 
 beginning Festival . miscellaneous 
 afternoon concerts symphonies ( Mozart 
 E flat Beethoven F , . 8) , 

 462 

 matter fact heard good deal excellent 
 music , ¢.g . , phenomenal performances 

 Beethoven Seventh Brahms Fourth Symphonies , 
 maligned work especially received 
 reading life feeling , strength beauty . 
 Herr Steinbach famous Brahms conductor , [ | 
 heard symphony year London . 
 present occasion , orchestra 150 
 performers , surpassed , feel inclined 
 agree German critics 
 greatest symphony Beethoven . heard 
 glorious performance Bach ‘ Brandenburg ’ Concerto , 
 . 3 , played thirty - violins , - violas , 
 eighteen violoncellos , _ basses . 
 movement actually encored ! wonder , | 
 repeat phenomenal , score 
 jumboism suggested number executants , 
 artistic grounds , viz . , tone , finish , _ spirit . 
 audience delirious delight , Bach ! 
 great Sebastian represented humorous 
 cantata ‘ Der Zufriedengestellte AZolus , ’ candidly 
 confess bored long succession ‘ comic ’ airs 
 endless recitatives . work requires special 
 treatment hands , mouths singers , 
 treatment forthcoming . Brahms ‘ Triumphlied , ’ 
 lovely quartets ( Op . 92 Op . 64 ) solo 
 voices ‘ Emperor ’ Concerto , superbly played 
 M. Paderewski , completed programme second 
 concert , noted , devoted 
 great musical B’s — Bach , Beethoven , 
 Brahms 

 Dr. Richard Strauss ‘ Taillefer , ’ ballad soli , 
 chorus , orchestra , great attraction 
 concert . laid - popular lines . 
 voice parts comparatively straightforward diatonic , 
 dramatic situations orchestral writing 
 complex . music bounds 
 strepitous swing ‘ Don Juan ’ ‘ Heldenleben ’ 
 climax , poem evidently chosen 
 composer loves work Ercles vein . 
 scene ‘ Battle Hastings . ’ try describe 
 , content remark , compared 
 ‘ Battle Hastings ’ battle Charlottenburg — 
 Strauss studio — ‘ Heldenleben ’ merest 
 lullaby . Max Bruch ‘ Sanctus ’ soprano soloists , 
 double chorus orchestra ( Op . 35 ) fine 
 scarcely inspired piece sacred music , massively planned , 
 richly scored , dignified impressive . sung 
 warmly received . original version ‘ Lohengrin 
 Erzihliing ’ aninteresting novelty . additional twenty- 
 lines poetry contain beautiful music style 
 precedes , doubt Wagner 
 wisdom cutting , climax , dramatically 
 musically , Lohengrin declaration ‘ Sein 
 Ritter , ich - bin Lohengrin genannt . ” Herr Knote sang 
 Scena magnificently . Ernst von Wildenbruch striking 
 poem ‘ Das Hexenlied ’ ( Witch Song ) recited 
 Dr. Ludwig Wiillner , intensely dramatic 
 exquisitely modulated interpretation 
 perfect , certainly moving , efforts 
 Festival . err Max Schillings accompanying 
 melodramatic music betrays hand earnest 
 talented artist . creates requisite atmosphere , 
 helps intensify effect affecting 
 situations . , scored dark , gloomy 
 colours . Weber ‘ Euryanthe ’ Overture , Haydn E flat 
 Symphony ( B. H. . 3 ) , final scene 
 ‘ Die Meistersinger ’ completed gigantic programme 

 solo Sir Charles Stanford Clarinet . Concerto 

 \ minor ( Op . 80 ) , Mdlle . Annie de Jong violin 
 playing Dvordk seldom - heard Concerto ( Op . 53 ) , 
 reappearance Miss Muriel Foster 
 American - Canadian tour , sang Brahms Rhapsody 
 contralto solo , male choir , orchestra . 
 chorus parts - named work sung 
 gentlemen Alma Mater Choir , consisting past 
 students Royal Academy Music , general finish 
 interpretation testifying excellent training . 
 Tschaikovsky Fantaisie , ‘ Francesca da Rimini , ’ Dr. 
 Cowen Indian Rhapsody , Brahms Symphony D 
 ( . 2 ) , presented effective contrasts concert June 16 , 
 , exception , rendered manner 
 bore witness versatility instrumentalists 
 insight conductor . 
 memorable performance thai M. Raoul Pugno 
 Beethoven Pianoforte Concerto C minor , elicited 
 enthusiastic applause . refreshing music , 
 delightful delicacy pianist touch 

 charm interpretation 

 , keen recollection electrifying effect 
 Parry ‘ Come , pretty wag , ’ rendered crack 
 choirs , felt ‘ Magpie ’ audience 
 thronged hall realised poten- 
 tialities remarkable piece . reluctance 
 exhibit high colour deprived modern 
 pieces legitimate effect , acknow- 
 ledged ‘ artistic ’ restraint admirably adapted 
 exhibit charms old madrigals , 
 daintily delightfully performed . Palestrina wasrepresented 
 ‘ Non son le vostre mani . ’ Giovanelli ‘ M1 sfidate , ’ 
 Orazio Vecchi ‘ Il bianco dolce cigno , ’ Antonio 
 Scandelli ‘ Von einem Hennlein . ’ piece 
 piquantly sung intensely amused audience . 
 English version , believe , curiously 
 modern - song ( Scandelli died 1580 ) welcome 
 addition repertory choral societies country . 
 Highly painstaking expressive performances 
 unaccompanied - songs ( Op . 104 ) Brahms given . 
 ‘ m Herbst ’ especially impressive . concert 
 advantage assistance Mr. Plunket Greene . 
 sang solo ‘ Die Vatergruft ” 
 recently issued ‘ English Lyrics , ’ Sir Hubert Parry . 
 Miss De Angelis , young highly promising singer , 
 sang solos . Mr. Lionel Benson usual conducted . 
 high repute Society largely 
 gentleman fine taste skilful training 

 MR . B. HOLLANDER ORCHESTRAL SOCIETY . 
 M. YSAYE LEADS BEETHOVEN C MINOR SYMPHONY 

 second concert Mr. B. Hollander Orchestral 
 Society took place Kensington Town Hall June 1 . 
 programme included Beethoven C minor Symphony , 
 Mozart ‘ Zauberflote ’ Overture , Max Bruch Second 
 Violin Concerto ( Op . 44 ) , works conductor 
 orchestra completely justified high opinion 
 qualities expressed issue . concerto 
 played grandest manner M. Eugene Ysaye . 
 heard ‘ Chant d’hiver , ’ strange effusion , 
 chill , melancholy loneliness vast , snow- 
 clad expanse moorland suggested means 
 vague , meandering melodic phrasesand vaguer , far - fetched 
 harmonicdevices . piececannot called pleasing , 
 haunts memory reason pensive poetry pervading 
 . response great display enthusiasm , M. Ysaye 
 played Beethoven Romances encore , 
 enthusiasm broke forth redoubled vigour took 
 place desk lead Symphony , standing 
 like rest violinists . ‘ led ’ purpose , 
 movements went impetuosity 
 absolutely startling . memorable performance . 
 Mr. Percy Pitt Symphonic Prelude , ‘ Le Sang des crépus- 
 cules , ’ completed programme , extraordinary , 
 immensely clever work , gloomy weird 
 prevailing mood , heavily scored , confess 
 inability form opinion qualities 
 bewilderingly resonant performance . short season came 
 close June 22 , important works 
 performed time England , viz . , M. C. Saint- 
 Saéns Second Violoncello Concerto ( Op . 119 ) , Mr. 
 Hollainder ‘ Dramatic ’ Symphony ‘ Roland ’ ( Op . 24 ) . 
 French master work effectively written 
 instrument , caztadz / e passages gives soloist 
 opportunity displaying choicest singing tone . 
 solo instrument stands background rich 
 orchestral texture ; thematic material interesting , 
 handled M. Saint - Saéns usual mastery , altogether 
 effective Concerto proves versatile hand 

 distinguished Frenchman lost cunning . Mr , 
 Joseph Hollman played sclo dash 
 brilliancy , large warm tone 
 famed . blame Mr. Hollander having neglected 
 provide audience synopsis Symphony , 
 doubt Englishman thousand 
 intelligent account legendary lore woven 
 round giant figure ( feet high ! ) 
 redoubtable paladin nephew Charlemagne , 
 asked hand Aude , daughter Sir Gerard 
 Lady Guibourg , reward slaying Angoulaffre , 
 Saracen giant , gaining object desires 
 fell ambush valley Roncesvalles , 
 Pyrenees , killed , flower French 
 chivalry ( A.D. 778 ) ; lady - love died 
 broken heart . Mr. Hollander gave audience titles 
 movements work . 
 Symphony programme music , closely details 
 story followed . 
 instance , Roland famous deed blow 
 enchanted horn ere died . blew loudly 
 Charlemagne heard leagues away . paladin 
 heroic bugling effort broke veins neck , fatal 
 results . terrific blasts Mr. 
 Hollander brass , meant describe 
 tragedy . Leaving aside , , question 
 appropriateness undisclosed programme , judging 
 work abstract music , find lack continuity ; 
 big , broad sweep symphonic movement felt 
 splendid sehervzonamedafter Roland trusty sword , ‘ Durandal , ’ 
 movement consequently far effective . 
 remaining portions halting - starting , 
 losing picking threads marked 
 feature Liszt symphonic works , tends break chain 
 attention secured . stand 
 Mr. Hollander credit , , excellent assets 
 long - spun themes true symphonic importance , impressive 
 climaxes dynamic emotional ebullition , excellent 
 scoring , especially brass , deft workmanship 
 allows carried away 
 ideas wide river ‘ rolling rapidly , ’ 
 finally rare quality individuality felt 
 thematic material scoring . 
 Symphony splendidly played - received . 
 wonderfully brilliant , rhythmically perfect performance 
 Berlioz ‘ Carnaval Romain ’ Overture , short Wagner 
 selection songs sung Spanish contralto , 
 Madame Marie Gai ; completed _ highly interesting pro- 
 gramme . glad hear Mr. Hollander 
 second series concerts autumn . success 
 attend artistic endeavours 

 MUSICAL TIMES.—Juty 1 , 1904 . 467 

 interest pertained Miss Winifred Christie 
 orchestral concert June 21 St. James Hall , 
 young artist studying past years 
 Royal Academy Music , pianoforte 
 pupil Mr. Oscar Beringer succeeded gaining Liszt 
 Scholarship . Miss Christie attainments surmised 
 fact elected heard Pianoforte 
 Concertos Beethoven G Tschaikovsky B flat minor , 
 Liszt ‘ Hungarian ’ Fantasia . young artist 
 successful Beethoven work , pure , 
 sympathetic legitimate style heard greatest 
 advantage . present Miss Christie readings vigorous 
 passages deficient boldness , respects 
 playing pleasure - giving . orchestra conducted 
 Dr. Cowen , secured effective support concert- 
 giver , excellent performance vivacious Overture 
 ‘ Butterfly Ball . ’ beautiful singing Miss Muriel 
 Foster increased enjoyment large audience 

 Mr. Joseph Holbrooke series ‘ Modern Chamber 
 Concerts ’ merit generous support , 
 hearing works British composers . 
 scarcely wise entire programme novelties , 
 strain attention great . concert 
 given June 8 , selection opened Pianoforte 
 Quintet E flat Mr. Joseph Speaight , contained 
 strong - knit movement . set Lyrical 
 Pieces String Quartet , Mr. Richard H. Walthew , 
 worthy attention amateurs ; best work 
 brought forward String Sextet ( Op . 16 ) Mr. 
 Holbrooke — terse , bright , interesting com- 
 position , merits publication . new 
 songs , ‘ come ’ ‘ Bonjour , ’ 
 Mr. Norman O'Neill , proved charming rendered 
 Mrs. Henry J. Wood 

 MUSIC BIRMINGHAM . 
 ( CORRESPONDENT 

 close session School Music connected 
 Birmingham Midland Institute marked 
 series performances afforded evidence good 
 work year . concert chamber 
 music given June 11 programme _ comprised 
 Beethoven String ( Quartet G ( Op . 18 , . 2 ) , 
 Pianoforte Quartet ( Op . 16 ) , arranged Quintet 
 Pianoforte Wind Instruments ; Schumann Pianoforte 
 Quintet ( Op . 44 ) . dozen students took 
 creditable rendering pieces . addition , 
 Maurer Concertante Solo Violins ( pianoforte ) 
 brightly given Misses Wadeley , Hodgkinson , 
 Burman , Fuller ; Miss K. G. Thomas played Suite 
 Poétique René de Boisdeffre , Mr. H. Caville gavea 
 brilliant rendering Rode Seventh Violin Concerto . 
 pianists Mrs. Charles Gaunt Mr. G. H. Manton , 
 young professor Institute . June 15 
 Choral Orchestral Concert took place Town 
 Hall . Students ’ Choir ( Ladies ) sang Berlioz Ballad 
 ‘ Ophelia ’ ( ‘ Tristia , ’ Op . 18 ) , chorus Gaul 
 ‘ Virgins , ’ Mackenzie ‘ Come , sisters , come . ’ 
 Mr. Gaul conducted performance music , 
 great reception . orchestra , mainly composed students , 
 aided professional players departments , gave 
 admirable rendering Elgar ‘ Froissart ” Overture , 
 accompanied choral solo numbers . Miss Winifred 
 Morris created like sensation interpreta- 
 tion solo Beethoven Pianoforte Concerto 
 EF flat , showing extraordinary promise . Mr. P. L. Dyche , 
 young violoncellist , gave good account Max Bruch 
 ‘ Kol Nidrei , ’ playing good tone neat execution 

 468 

 stage Tyne Theatre possession 
 Newcastle Amateur Operatic Society week 
 beginning June 6 , adequate creditable per- 
 formances given Gilbert Sullivan operas , 
 ‘ Trial Jury ’ ‘ H.M.S. Pinafore . ’ Madame Mimi 
 3eers , Misses Elsie Downing , Margaret Herries , 
 West , Messrs. Maurice Pearce , W. D. Spark , G. K. 
 Vietch , J. R. Liddell , E. J. Potts , W. FE . Storey , 
 Kk . Pearson entrusted principal characters , 
 Mr. R. Smith conducted 

 usually expect good music receive 
 attention popular seaside resorts , 
 pleasant note Tynemouth Palace - - - Sea 
 variety entertainments entirely exclude 
 interests . orchestra thirty - performers , 
 Handel , Wagner Gounod evenings recently given , 
 Beethoven Fifth Schubert ‘ Unfinished ’ Symphony 
 included programmes . hoped 
 lead developments artistic 
 direction 

 successful pianoforte recital given Sunderland 
 June 13 Mr. Henley Pratt , organist St. George 
 Presbyterian Church . recitalist assisted ” 
 Mrs. Alfred Wall Mr. Alexander Webster ( vocalists ) . 
 Mr. Alfred Wall ( violinist 

 concert ‘ ‘ Eights ” took place Balliol 
 College Hall 21 , Miss Fanny Davies 
 pianist , Herr Frau Dulong vocalists , Dr. Walker 
 accompanist . pieces charmingly played 
 Miss Davies Canons flat B minor 
 Schumann , Bach minor Fugue . played 
 pieces older Netherland writers . 
 Swiss folk - songs nicely rendered singers 

 Exeter College Concert took place 24 , 
 important items Beethoven ‘ Prometheus ’ Over- 
 ture Mozart ‘ Jupiter ’ Symphony , excellently given 
 band Mr. J. S. Heap direction . Miss 
 Bridson played good style Paganini Violin Concerto 
 D minor , songs given Mr. Charles 
 Saunders ‘ beats rapturous thrill ? ’ 
 cantata ‘ Maid Astolat , ’ late Dr. Swinnerton 
 Heap , father conductor concert 

 Keble Concert 26 consisted chiefly 
 songs - songs interspersed orchestral pieces . 
 Miss Beatrice Dunn Mr. McInnes soloists , 
 successful ‘ Mignon Song ’ Goring 
 Thomas , ‘ Boot , saddle , horse 
 away , ’ Graham Peel . thoroughly excellent rendering 
 Weber ‘ Oberon ’ Overture given baton 
 Mr. F. Shaw 

 E. S.—(1 ) late Herr Carl Oberthiir , distinguished 
 harpist , born Munich , March 4 , 1819 , died 
 London — settled year 1844 — November 8 , 
 1895 . Ile pupil Elise Brauchle G. V. Roeder . 
 time played orchestra , Costa , Covent 
 Garden , achieved fame teacher 
 solo performances concerts England abroad . 
 compositions ( upwards 200 number ) included 
 opera ‘ Florio von Namur ’ ( produced success 
 Wiesbaden ) ; Grand Mass ‘ St. Philip de Neri ’ ; Overtures 
 ‘ Macbeth ’ ‘ Riibezahl ’ ; trios harp , violin 
 violoncello ; ‘ Loreley , ’ legend harp orchestra ; 
 quartet harps ; vast number original 
 compositions arrangements instrument . 
 ( 2 ) harp compositions Hasselmann Verdaile 
 obtained Messrs. Novello 

 C. F.—Beethoven ‘ Mount Olives ’ English 
 version oratorio ‘ Christus Oelberge . ’ word 
 ‘ Hallelujah ’ occur original ( German ) version 
 noble chorus C , thoroughly Halleluiac 
 sentiment . work performed country 
 February 25 , 1814 , direction Sir George Smart , 
 Lenten oratorio performances Drury Lane 
 Theatre . , , English versions 
 text : Arnold ( ? ) , Oliphant , Bartholomew , 
 Troutbeck ; Dr. Hudson , Dublin , 1842 , 
 story changed David , title 
 « Engedi 

 LreIGH.—A teaching connection generally 
 bought . course obtained church organ 
 appointment post bring work pianoforte 
 tuition ; good qualification 
 Licentiateship Royal Academy Music , ‘ 
 good deal teaching away home , prepared successfully 
 examinations , splendid testimonials _ press 
 notices concert work , ’ surely right track 
 making teaching connection . steadily , putting 
 plenty enthusiasm work , time 
 reap reward - . wary agents 

 CANTATAS , ORATORIOS , OPERAS 

 BACH . — “ Passion " ’ ( St. Matthew ) aus ‘ ia 
 BEETHOVEN . — “ Mount Olives " ... ( Paper boards , 1s 

 Tonic Sol - fa ) 
 ( Paper boards , 1s . 6d 

 EDITION , REVISED 

 BEETHOVEN 

 

 PRICE , CLOTH , GILT , SHILLINGS 

 T recognise , smallest hesitation , 
 important valuable recent contributions musical litera- 
 ture . . . . best informed professional musicians learn 
 great deal master - works Beethoven Sir George 
 Grove , wide reading acute perceptiveness enabled 
 marshal astonishing array facts , intimate 
 acquaintance spirit master qualified throw 
 light pages , , obscure ... . 
 satisfied remarks , earnestly recommending 
 recognise Beethoven greatness shown immortal 
 Symphonies obtain Sir George Grove volume , walk 
 luminous paths ready conduct trust 
 guidance . "'"—Daily Telegraph 

 London : NovELLO Company , Limited 

 Responses Commandments selected 
 Services composers Myles B. Foster , 
 Dr. Garrett , Ch . Gounod , Dr. E. J. Hopkins , Dr. C. H. Lloyd , 
 Merbecke , H. Smart , John E. West , S. S. Wesley , 

 Sir John Stainer Sevenfold Amen included , Vesper 
 Hymns Beethoven , Sullivan , ; concluding 
 Vestry Prayers S. S. Wesley Rev. Canon Hervey 

 PREFACE 

 Thesame . Violoncello Pianoforte ... wa 

 BEETHOVEN.—Allegro , Violoncello Sonata . Op . 69 . 
 Arranged Harmonium Pianoforte A. REINHARD 

 BRANDTS - BUYS , J.—Quintet . Flute , Violins , 
 Viola , V iolonce llo 

