of Handel in his purely instrumental music 
 it be say that ' it would be difficult to discover 
 one less open to suspicion of be a writer of 
 programme music . 
 to vocal music he demonstrate power to paint 
 tone - colour vividly . Haydn in ' the creation , ' 
 ' the and other work be claim 
 as a programmist , and it be state that even 
 in compose his sonata and symphony he 
 be dominate by unrevealed romance and 
 programme . a curious case of what may be 
 term programitis be that of Dittersdorf ( 1739 

 1799 ) , who in twelve symphony endeavour to 
 illustrate subject from Ovid ’s ' Metamorphoses . ' 
 Professor Niecks say that these symphony ' be 
 full of beauty and point of interest , ' and that 
 ' they well deserve to be read and play . ' after 
 deal rather fully with the powerful effort of | 
 Beethoven to express and moods , the 
 Professor conclude ' that the time can not be far | 
 off when he will be regard as the chief founder 
 and the great cultivator of programme music . ' | 
 nineteen page be devote to Mendelssohn , whose 
 remark that ' since Beethoven have take the step 
 he take in the Pastoral symphony , it be impossible | 
 for composer to keep clear of programme music 

 be quote 

 without be so mean as to pull all the plum 
 out of Mr. Michotte ’s very tasty pie , I may venture 
 to give some sample of its toothsome content 

 on receive Wagner , Rossini at once assure 
 he that he have not be so impolite to a brother 
 musician as to say the unkind thing gratuitously 
 attribute to he . ' as for despise your music , ' 
 he go on , ' I ought in the first instance to know 
 it , and to know it I ought to hear it at the theatre , 
 for it be only in the theatre , and not simply by 
 read the score , that it be possible to render a 
 just judgment of music intend for the stage . 
 the only thing of Wagner ’s he have hear be , he 
 say , the ' ' Tannhauser ' March , which he have 
 after thus 
 clear the ground , intercourse become easy and 
 pleasant , and many interesting topic be broach 
 and discuss during this short visit . on Wagner ’s 
 express a fear — only too well justify in the 
 event — lest the success of ' ' Tannhauser ' should be 
 spoil by a cabal , Rossini express his lively 
 sympathy , and tell how he have suffer in similar 
 fashion , notably on the production of his master- 
 work , the ' Barber , ' and even in later year in Paris 
 and Vienna after his world - wide reputation have 
 be make . in the latter case it be Weber who 
 fulminate against rossinian opera . Wagner 
 admit Weber ’s intolerance , but his admiration 
 for the composer who , more than any other , may be 
 say to have prepare the way for he , lead he to 
 excuse he on the ground of his patriotism , though 
 this , by the way , could not have be cite as an 
 apology for his attack on Beethoven , for Weber ’s 
 article ridicule the fourth symphony be a 
 remarkable instance of how one composer may fail 
 to appreciate another whose ideal be different 
 from his own 

 the mention of Weber lead Rossini to describe 
 how that composer pay he a complimentary 
 visit when ( in 1826 ) he be pass through Paris 
 to produce ' Oberon ' in London . the circum 
 stance be in curious contrast to those of 
 Wagner ’s visit , for while Wagner hesitate because 
 he believe Rossini have ridicule he , Weber 
 hesitate because he know he have severely 
 criticize Rossini . however , Rossini ’s édonhomt 
 soon put Weber at his ease , though his condition 
 be at this time pitiable , the disease which be so 
 soon to carry he off , show its ravage in his 
 livid complexion and extreme emaciation , as well 
 as in a consumptive cough 

 then the conversation turn to another of 
 Wagner ’s idol , Beethoven , of whom Rossini have 
 some vivid recollection , having pay he a visit in 
 1822 . what seem to have strike Rossini most 
 be the exceed sadness express in Beethoven 's 
 face . he be receive kindly but with characteristic 
 brusqueness , beethoven open the conversation 
 by compliment he on the ' Barber , ' and tell 
 he with delightful candour that he should stick 
 to opera buffa , and not attempt anything serious 

 E. Michotte . 
 détails inédits et commentaires . 
 1 fr 

 natural instinct in composition — a modest and 
 very just claim — and avow that his slight 

 musicianship be chiefly derive from study 
 the score of german master . he name 
 particularly ' Creation , ' ' Figaro , ' and ' the magic 
 flute , ' to which he might have add ' the 
 season ' ( ' the impatient husbandman ' ) , and 
 Beethoven ’s eighth Symphony ( 4//egretto ) , to 
 both of which he owe some definite suggestion . 
 on his express his regret that he have not 
 enjoy a more thorough training on german 
 line , Wagner show his appreciation of what 
 Rossini have accomplish by cite the ' Scene of 
 the darkness ' in ' Moses in Egypt , ' that of the 
 conspiracy in ' Guillaume Tell , ' and , in another 
 order , the ' ( Quando Corpus , ' as example which he 
 could hardly have better , and these the veteran 
 admit be among the ' happy moment ' of 
 his career 

 for Bach , Rossini express a lively admiration 

 _ 

 he have so clearly express . for his own part he | 
 be ( 00 old — ' be at the age when one be not so 
 much inclined to compose as liable to decompose ' 
 — to turn his eye to new horizon , but he be very 
 willing to ac knowledge that Wagner ’s idea be of 
 a nature worthy of the serious consideration of 
 young composer . ' of all the art , ' he conclude , 
 ' music be that which be , by reason of its ideal 
 character , most subject to transformation , and to 
 these there can be no bound . who , after Mozart , 
 could have foresee Beethoven ? or , after Gluck 

 Weber ? and , after these , why should there be an 
 end to progress ? ' 
 as they come away Wagner acknowledge to 

 an interesting sale of autograph take place at the 
 — Drouot , Paris , during the last week of February 

 short note by Chopin fetch 45 ; a receipt by| 
 Beethoven for 1,200 florin from the . Archduke Rudolf | 
 ree ilize £ 4 2s . 6d . ; two bar in score from| 
 ' Lohengrin , ' write in } spo , £ 5 4 . ; a sheet of 
 sketch by Beethoven , 47 4 . ; a sonata by Gounod 

 12 8 . ( purchase by St Charles Malherbe ) ; the | 
 score of Massenet ’s — — £ 20 4 . ; a Chopin 
 Mazurka ( op . ph no . 1 ) , £ 28 45 . ; and a Schubert | 
 song ( i page ) , 452 

 Chopin ’s pupil , Madame Dubois ( 7¢e O’Meara ) , who | 
 have be a sufferer for many year , recently die at | 
 Paris . she study with the composer for five year , 
 and Prof. Niecks , in his ' Life of Chopin , ' furnish an | 
 interesting list , give to he by Madame Dubois 
 herself , of the study and piece which she study 

 under Chopin ’s direction . after name Bach , | 
 Clementi , Beethoven , Weber , Mendelssohn and| 
 Liszt , & c. , she conclude : ' and of Schumann 

 nothing 

 by six Hubert Parry . 
 | ORCHESTRAL : symphony in C minor and Leonora 

 overture ( no . 3 ) , Beethoven . Romeo and Juliet 
 symphony , Jeriioz . symphony in E _ minor , 
 | Tchaikovsky . Don Juan , symphonic poem , s / rauss 

 Overture , the butterfly ’s ball , Cowen . overture , 
 ' Summer , Hervey . * two Norfolk rhapsody , 
 no . 2 in d minor , no . 3 , A//a marcia in G minor 
 and G major , Vaughan Williams 

 thirty - seven bar ) with which the overture open , 
 the drummer come to the nineteen bar ’ rest of the 
 mdantino movement . ' I will miss those , ' say 
 the drummer . ' no , no , count they , please , ' say the 
 maestro 

 Holz tell I that it be your intention to publish a_| 
 large size of the engraving represent Handel ’s | 
 monument in St. Peter ’s Church , London [ Westminster | 
 Abbey ] . this afford I extreme pleasure , indepen- | 
 dent of the fact that I be the person who suggest | 
 it . accept my thank in anticipation.—BEETHOVEN 

 from a letter to Gottfried Weber , April 3 , 1826 

 the history of music show a steady and continuous 
 progress towards more passionate emotional expression . 
 in the work of the old composer there be subtle 
 emotional effect which < difficult to label . the effect 
 produce on the hearer be akin to the emotion which a 
 grand landscape excite and suggest . we be conscious 

 and noble kind , but we can not easily 
 give they verbal expression . the emotional appeal become 
 in course of time more and more pronounced , until in some 
 of the work of Beethoven we find join to extreme beauty 

 form , a passionate emotion . it be by no 
 mean necessary for the hearer to be place in possession 
 of the stimulating cause , if any , of this pa peal . a Composer 
 of a strong emotional nature will produce work that reflect 
 external circumstance ; the 

 s four movement in which brightness come only as 

 leam of sunshine on an April day , but the music be 
 y understand , | ¢ } oroughly mozartian in idiom . Mr. Wood be greatly to be 
 the work recall the } praise for revive this interesting work , which it be to be 
 good . for the present hope will be hear uin during the Promenade 
 we can not yoke M. Enesco } Concert season . Beethoven ’s Rondino in e flat for wind 

 from when he be about twenty year 

 g haikovsky ’s ' Mazeppa , ' the love scene from Strauss ’s 

 Stanford ’s melodious ' irish rhapsody No . 1 , ' and end opera ' Feuersnot ' and Beethoven ’s E flat Concerto , the 

 with Cherubini ’s ' Les Abencérages ' overture . Miss Marie | pianoforte part of the last - name being beautifully play by 
 Brema , the vocalist of the evening , be more successful ] Mr. Richard Bublis ; - 
 in the song by Weingartner than in those by Purcell 3 — 
 the concert give on March 13 have special interest , if | LONDON symphon oncert . 
 nly because it incidentally demonstrate that new british } at the ninth concert give at ( Jueen ’s Hall on March 11 

 isic could be favourably pit against new foreign music . | interest centre chiefly in the performance of Strauss ’s 
 the first item be the prelude to the as yet unperformed | ' Ein Heldenleben . ' probably no fine interpretation have 
 ypera ' Ione ' by Mr. Arthur Hervey . this be conduct | be give of this remarkable work . no attempt be make 
 by the composer and be well receive . the opera have a|to mitigate the realism of the Battle section . if we must 
 tragic plot in which love , jealousy and revenge be motive , | feed on Strauss , there be no doubt it be well to have he serve 
 and the drama . the theme have ; hot . Mr. Arthur Payne play the violin solo with tell 
 character and they be often effectively orchestrate . the } beauty of tone and execution . Mr. Harold Bauer give 

 rown ' and brooding section seem proportionately too | highly effective performance of the b flat minor Pianoforte 
 long to maintain interest , but the poetry of the treatment | concerto by Tchaikovsky and the ' Todtentanz ' by Liszt . 
 be unmistakable . another novelty in the programme was| the concert open with Mozart 's beautiful symphony in 
 the Violin concerto in A ( Op . 45 ) compose and conduct | e flat , and close with one of the most brilliant performance 
 y Christian Sinding , who appear for the first time in| of Beethoven ’s ' Leonora ' no . 3 overture that we have have 
 England . the concerto do not make a very favourable| the good fortune to hear . Dr. Richter conduct and 

 pression . it lack charm and often seem laboured . | seem throughout at his good 

 engender a feeling of monotony 

 the MUSICAL TIMES.—Aprit 1 , 1907 . 255 
 M Theodora Macalaster , assist by Mr. Stoney | chale ( Mr. Wm . Kendall ) . church choir ’ ( man and boy 
 in Beauchamp ( tenor ) and Mr. Robert Buchan ( violin ) , give a ] prize , St. Paul 's , Wimbledon Park ( Mr. G i. Dean ) . 
 i very successful concert at Steinway Ilall on March 6 . | church choir ’ ( mixed ) prize , Clapham High Street 
 oa Th dy have a soprano voice of musical quality , and her Wesleyan Church ( Mr. Wesley Hammet Men s choir 
 sis interpretation of an admirable selection of song by british | prize , Wren male choir , Camberwell Mr. F. C. French ) . 
 i d ind tinental composer attest to considerable dramatic | ' the Gentlewoman Competition for lady ’ choir , 
 . intelligence and sensitive feeling . [ ler singing be much | first prize , Mrs. Mary Lavyton ’s choir ; second prize , Aristotle 
 o appr ciate by a large audience . Road E.C.S choir ( Mr. A. G. Gibbs ) . Elementary 
 ' : : | school choir , first prize , Aristotle Road school choir , 
 ry the choir and orchestra of the City of London ¢ ollege | Clapham Mr. A. G. Gibbs this choir also gain the 
 athe perform , on March 7 , Schubert ’s ' the song of Miriam , ' first prize for sight - singing . second prize , Bolingbroke Road 
 ing and Gade ’s ' Spring ’s message . ' the chorus sing throughout | L.C.C. school , Old Battersea ( Mr. T. Maskell Hardy ) . 
 a with excellent attack and much attention to detail of | two concert be give by prize - winner . Sir Walter 
 expression . the solo vocalist be Miss Ethel Williams , | Palmer distribute the prize to the junior . 
 Miss Annie McBride , Mr. W. H. Walter and Mr. George } the adult competition attract a large audience , 
 ot s. the orchestra be ably lead by Miss Maude]and , without depreciate any of the performance , all of 
 il Swepstone , and the pianoforte accompaniment be in the | which be excellent , it be only fair to make special mention 
 on hand of Miss Gertrude Smith and Mr. Russell Bonner . | of Mr. Arthur Holford ( bass ) , the Hampstead Prize 
 lly Mr. W. G. Rothery conduct . Silver Band , and Mr. Percy W. Lawton ( tenor ) , the 
 t ’s ° , , i gold medallist of the festival . the Hon . Mrs. Talbot , 
 " x the South London Choral Ass ciation perform Hamish support by the Bishop of Southwark , present the prize , 
 li MacCunn ’s too rarely - hear work ' the lay of the last } , 4q Lord Alverstone propose the vote of ee . 
 as minstrel ' on March 20 , and give an adequate rendering of ! Mrs. Talbot . the massed choir of lady sing charmingly 
 e the dramatic and declamatory a with which | the work under the conductorship of Dr. Madeley Richardson . the 
 abound . : a capable orchestra do justice to the picturesque adjudicator be Dr. F. N. Abernethy , Mr Oscar Beringer , 
 accompaniment , and the vocal solo be ably sustain by Mr. | lr . Field , Dr. C. J. I rost , Mr. Alfred Gibson , 
 Miss Mabel Manson , M adame ( ecile Vicars , Messrs Dr rt ! | Huntley , Dr W. G. MeN wught , Mr. Dan Price , 
 Bernard Turner and Herbert Tracey . Cowen ’s choral ballad , | py , 4 , Madeley Richardson , and Mr. Fred E. Weatherly 
 8 , ' John Gilpin , ' and Faning ’s ' vagabond ' be include in the secretary of the festival te Mr. T. Lester Tooes 
 " e the second part of the concert , which be ably conduct by : palsentsat e 
 nl Mr. Leonard C. Venables . OAKHAM ( RUTLAND ) . 
 . an enjoyable concert be give by the Hampstead | , rhis be the second = = this typically countryside 
 : : estival . the Hon . Mrs. Charles Fitzwilliam be the chief 
 ve Conservatoire Orchestra on March 21 , under the able ; ' anh ba Gale meee cae oes ee 
 ly lirection of Mr. René Ortmans . excellent performance | promore ! tr a prog ba rat - oe ay = ' ele > 
 be give of Beethoven ’s eighth symphony , the overture Gistrict . | tin pomcesg a lid " — i e oe 
 to ' Die Meistersinger , ' the first movement of Tchaikovsky 's sch ols have enter , but two do not atte a he rcapaneaneat 
 < r > a . ; village be represent by adult choir . there be a 
 Pianoforte concerto in B ilat minor — the solo part in which gs es tea ™ nein a 
 be play by Miss Ada Petherick — and Arthut Hervey ’s | px si sc oe ee ee rem cae 
 , - s ' . 7 . * n flow’rv meadows , ' Palestrina ) , an anthem class 
 clev and effective concert overture ' Youth . ' the vocalist | > , meta . " hae , ee gga -1 pane : 
 be Madame Windsor Loche and Miss Gertrude Walton , | ( " , what be these ? " Stainer ) , a man s - voice class ( " lovely 
 , night , " Chwatal } , a female - voice class the shepherd , 
 k II . Walford Davies ) , a mixed quartet class ( * ' when 
 5 , ae . hand meet , ' Pinsuti ) , and class for sight - singing and voice 
 Musical Competition Festivals . | production . ' virst place be secure by Wing , Exton , 
 Reker Ridlington , Burley ( two ) , Oakham ( three ) , and Uppingham . 
 STRATFORD musical . FESTIVAL . the choir combine to sing two glee at the evening 
 this old - establish festival have this year celebrate its| concert which be successfully give on the second 
 ’ silver jubilee Special and very successful effort have | evening of the festival , March 7 
 be make to give the event due importance . fourteen 
 adult choir and eighteen junior choir appear . there be KENSINGTON COMPETITIONS 
 : about 400 solo instrumentalist and 173 solo vocalist . formerly this organizition appeal only to lady ’ 
 altogether it be estimate that upwards of 2,000 performer | choir . it be now open to orchestra and mixed - voice 
 take part in the festival . the result in the chief choral | choir in addition . e test - piece for the orchestra be 
 class be as follow : first - prize winner — lady ’ choir , | Tchaikovsky 's ' Elegie ' and a short sight - test . the Church 
 Madame Hands ’s choir ; man ’s choir ( two class ) , | Orchestral Society ( Dr. Huntley ) , the London Diocesan 
 St. Columba ’s ( first in both ) ; girl ’ choir , Stratford | Orchestra ( Margaret Ilaweis ) , andthe Linnet String Orchestra 
 Co - operative Junior ( Mr. Alfred Sears ) ; boy ’ choir , | Miss Ida Hyett ) compete , and the challenge banner , 
 St. Saviour ’s , Walthamstow , choir - boy ; elementary school | offer as a prize , be win by the Diocesan Orchestra . in 
 s choir ( boy ) , Godwin Road , Forest Gate , Council School ; ] a sight - test competition ( adjudicator , Mr. Joseph Ivimey 
 n ( girl ) , Farmer Road , Leyton , Council School ; choral | the Church Orchestral Society gain full mark 
 n Societies , Mr. G. Day Winter 's choir . the adjudicator } in the choral section nine choir compete in seven 
 ) be Dr. Percy Buck , Mr. George Oakey , Mr. James ] section . Miss Lisa Gibson ’s choir , which be a highly 
 1 Bates , Mr. Oscar Beringer , Mr. G. WH .   Betjemann , | train one , gain first - prize in three section . Mrs. Mary 
 l Mr. Ernest Fowles , Mr. Dan Price and Mr. A. L. Cowley . | Layton be successful in two section . the Essendine 
 e the competition take place on March 9 , 11 , 13 , 14 , 15| choir ( Mr. W. Kendall ) be first in the female - voice choir 
 , and 21 . Mr. J. Graham be the indefatigable secretary . | sight - test , and be only two mark below the win 
 S the concert give by the prize - winner in Stratford Town | choir in the mixed - choir section . the singing generally 
 Hall on the afternoon of March 21 be honour by the | be of a high standard . Dr. McNaught adjudicate in all 
 presence of the Duchess of Albany . in addition to ] the choral section . the competition be hold on March 14 . 
 distribute the prize and certificate gain by the successful nee 
 . candidate , Iler Royal Highness present , on behalf of the| the Morley ( Yorks ) Vocal Union hold a competition in 
 subscriber , a silver salver and a photographic album to| the Town Hall on February 23 . seventeen choir compete 
 é Mr. J. Spencer Curwen , the founder and president of the | Mr. C. H. Moody , of Ripon Cathedral , adjudicate . the 
 : festival . Baptist Tabernacle Choir , Crosland Row Wesleyan , and the 
 Nelson Arion male - voice choir be prize - winner in various 
 | Soutu LONDON MUSICAL FESTIVAL . | section . 
 the second annual festival , heldin the Town Hall , Battersea , | _ — _ — — 
 on March 2 , 4 , 5 , 6 , 7 and 14 be a great success . there on March 15 , at a meeting of the Concert Goers ’ Cluls 
 be nearly 400 entry , and 1,350 performer . the standard | hold in Langham i[lotel , Miss Wakefield give an address 
 be generally satisfactory . the ' Lady Palmer ' Competition | on the Competition Festival Movement . Mr. W. II . Leslie 
 for choral Societies be win by the Essendine ( Paddington ) | preside , Dr , McNaught and Mr , A. Kalisch speak 

 KUM 

 iford ’s fine 

 three 
 Beethoven ’s Choral f 
 vequitte t 

 about 160 voice : all the = 
 by the 

 MO N CORRESPONDENT . ) 
 the Dublin Orchestral Society give its first concert of the 
 m Febrt the programme be as 

 Beethoven ’s 
 s ’s ' Le Rouet d’Omphale ' ; 
 ' Tristan and Isolde 

 there be a good attendance , and the hand 

 the series of orchestral concert be brilliantly round 
 off by that which take place on February 20 . Dr. Cowen 
 and the Messrs. Paterson deserve unstinted praise and 
 congratulation on the success of the season . the programme 
 be a plebiscite one , and include the ' Pathetic ' 
 symphony , the ' Midsummer Night ’s Dream ' Overture 
 and the ' ride of the valkyrie ' — all beautifully play 
 Miss Marie Hall make quite a sensation by her violin 
 play 

 charming concert , which be worthy of much well 
 support than it receive , be at give by the Verbrugglien 
 ( Juartet on March 7 , and the third concert ( February 26 ) of 
 the Edinburgh String ( Juartet be equally interesting , 
 include as it do work by Beethoven , Dvorak and Haydr 
 the sixth classical concert ( March 16 ) bring the Brussels 
 ( ) uartet , assist in one number ( the Pianoforte quintet of 
 Cesar Franck ) by Mr. Denhof . quartet by Glazounow 
 and Beethoven be render with perfect finish and 

 homas ’s 

 Opera have be much in evidence during the month . 
 T Royal Carl Rosa and the Moody - Manners company 

 each give a fortnight ’s performance . < a feature of t ] 
 Carl Kosa season be the fine presentation of Beethoven ’s 
 > amateur performance have be give by the 
 eus Club ( * Merrie England ' ) and the Glas ; - 
 Music Opera Class ( Offenbach ’s ' Grand Duchess ' ) . in 

 latt work the distinguished appearance of Miss 
 Jenny Young , a very promising Glasgow vocalist , merit 

 special mention 

 successful pianoforte recital have be give by 
 Madame Carreho , Mr. Frederic Lamond ( a _ Beethoven | 
 programme ) and Mr. August Ilyllested . \mong other 
 performance have be Spohr ’s * last Judgment ' by tt 
 Sunday School Union Choir ( Mr. Alec Steven , conductor 
 Costa ’s * Eli ' by the Clydebank Choral Union ; and the 

 the Coatbridge Choral Union , the last two 
 Mr. W. J. Clapperton 

 Mr. Coleridge - Taylor conduct the Symphony Orchestra 's 
 performance , on February 25 , of his symphonic variation on 
 an african theme and the £ v / racte from ' Lierod . ' ? the 

 rchestra , under Mr. Akeroyd , also give fine performance 
 4 Tchaikovsky 's I and Beethoven ’s 

 pathetic ' 
 * Leonora ’ overture 

 r the of the Orchestral Society , include 

 Beethoven ’s Eiglith symphony 

 concert , give 

 evening , sit 
 suis Titania ' and Weber 's 

 penultimate 
 March 11 , Beethoven ’s 
 ballet suite ' La Belle at 
 * Le Rouet d’Omphale ' 
 he vocalist of the 
 * Infelice , Ambroise Th 
 * softly sigh . ' 
 the Welsh ¢ 
 Bach ’s * Passion ' 
 March 16 . the 
 devotional and showed 

 be 

 from our own correspondent . ) 
 concert 3 
 e performance of the Mass in 
 this third etiort of the choir in this 
 I le . the principal be 
 Lakin , Mr. William Green 
 at the concert of February 21 

 Beethoven 
 Bach 
 minor on March 14 
 work be the good it 
 Miss Agnes Nicholls , 
 and Mr. Hjalmar Arlberg . 
 there be one more repe the ' pathetic ' 
 and Dr. Brodsky play Beethoven ’s Violin concerto . 
 Bruckner ’s Symphony No . 3 , in D minor , little 
 impression at the follow concert , but Madame Carrejio 
 score a great success in a fine performance of Beethoven ’s 
 e flat symphony be gloriously 
 play at Miss Nervil , the brilliant 
 \ustralian the vocalist , and Mr. Albert 
 Spalding , the play the Violin 

 h not a little 

 Mr. Dalton Baker be the 

 the programme of the Brodsky of 
 February 27 include Mozcart ’s G , 
 Beethoven ’s Quartet in and 

 e minor 

 L TIMES.—ApriL 1 , 1907 

 Messr Franz S i h D she , Paul Miry and | ' Lohengrin . ' Mr. Charles Knowles be accorde 
 } . Gaillard , the cultured lful Brussels String ( Quartet , | enthusiastic reception for his solo — Henschel ’s ' \ g 
 pr rogr t he Schiller - Anstalt | Dietrich ' and Sullivan ’s ' Thou art pass hence . ' ¢ 
 i i r i hree s ann | conce both artistically and financially , be 
 ( Juar { \ i s i t | } the v ciety have give , 
 O ( Mr. ¢ I ; s i d Th , Sacred Harmonic Society conclu 
 ) Juar ( r(op . 1 y - firs s itl performance of G 
 Mr. | fF Pr , rts , | * Reden 22 the artist be Miss 
 i S Thr Perceval ladys Roberts , Mr. William ¢ 
 ’ ' r February \ 1 Mr : 
 Mr. C1 \ i rts ¢ ] al ] t : 
 M St. Patr lead ore att officiate at the or 
 i gr s i : f on M 
 M t S ur 4 dichamber « 
 lg i | ll , a 2 , di lude 1 t < 
 due rt r Dr. Henry Watsor by Brodsky y 
 \ ! rps rd , be | performance n sturne inC minor and Balla I 
 : c uc pa nt , al G minor , and also accompany Mr. Brodsky in Bach ’s \ 
 Dr. W é rps rd Toc concerto in A m this the only opportunit 
 Henry Pur I I rami be mos dience have throughout the 1 of hear Mr. Br y 
 " soloist , and his magnificent breadth of tone in b 5 
 . polyp ! be thoroughly appreciate . the c rt 
 conc ] 1 fine rendering of Brahms ’s Pianoforte 
 MUSIC in NEWCASTLE and DISTRICT juintet . 
 at concer f the Leicester Symphony or : stra 
 O , , sn : I February 28 the rogramn include M rt ’s 
 ( ( ts February 2 I rte ncer in C ( no . 21 ) , Beethoven ’s Symphony 
 en M } \ r. Carl W , rave a|in D d new Festival March by Mr. | Mar ll 
 y = natas by | Ward f N um , conduct by tl composer 
 Mozar : I ; we ule | Jessie Adc ] nd Mr. J. A 
 f Mr. Borw ' Pia ae Ade 
 M S wel erfor by the Leicester lharmonic Societys performan 
 S n M S v 's cer Elgar ’s the | m n Mar 21 , despite some 
 . I Mr. Alf W shor n . pon W he Society and its con i 
 , Br rt ' ure W ly « t uted phe ir be hear 
 ( y 1 grea ad e i Fag © ye priest , nm 
 Gr wfos ‘ tr nan | 5 i d s lly in the Lord ’s Prayer 
 lerr | d ( r fr at a s s be m in 5 r , Miss Gwladys kk 
 | . é Mr. Webster M r and Mr. Dalton Baker , each of w 
 l S S or S do splendidl special menticn ma e mad 
 . ! n rogra rat * the sun goeth down , ' the ' arrest ' s t 
 } ' r M | ~ y il 1 | I olo ' unto yo that r 
 ' S rst S ot acl Mr. Ellis , ib ] ( leserves cre for 
 | ( rtu LAr : é ' nterpris¢ 9 r ' I ur ’s late orator 
 Mr LA I ( I S y 1 Leices’er audien 
 , r , Gre \ new ral s y have be form at Loughbor 
 rforn ' | ( Mar : e fron work select for 1ts first concer 
 t t Mr. M. Fair a ) . : ng | fres ! " 1 pr $ d celle : work e 
 . Choral S Armst : ( rformance on March 5 contain 
 New ano Mozart d Walford Davies ’ g Thomas ’s ' Sun - worshippers ' and 
 i t man , r Mr. W. G. Whittaker praise . ' the chorus be support 
 [ i ! it 1 Granvil I play the first 

 i r d | pl ind a piece i 
 i Spr , be Miss | 
 i n c ( r s am r } thborough ) , and 
 | uN ' ry re nder Birmingham ) . Mr. Frank Storer 
 ‘ r Mr. J. } lefiries it } companie . 
 rmer r y \ , separate the Long Eaton Choral Society perform ' Elijah ' i 
 r 14 t . concert on March 1 the s t Miss Ethel Lister , 
 Mr. ( D ; Sulliva ' Martv1 atin Miss Ma ters , Mr. Gwily 1 Mr. Dan Pr 
 Vas ] M LD S$ 1 retir y from Th chor 1d r str the directi 
 e mucl ood and| Mr. J. S. Derbyshi a rendering of 
 ' wor imiliar worl 
 O AY | 2 I or ( r Saris On Mare ( Chor Society sing G 
 I ) ~ ' Br Parr | Lloly Citv ’ w ul pr S10 ! the solo be al 

 1907 261 

 on |anuary 31 in the same building , and under the auspex | 
 ff the Musical Club , a chamber concert be give , its chief | 
 feature be Brahms ’s Serenade for smal ! orchestra in A 
 ( Op . » ) , Beethoven ’s Pianoforte concerto in G — Mr. | 
 Dona Tovey play the pianoforte part in the latter 
 work—-and Bach ’s Concerto for flute , oboe , violin , trumpet 
 nd string , in F , under the conductorship of Dr. Allen . 
 rt be very enjoyable . 
 Professor of Music , Sir Hubert 
 ebruary 20 his terminal lecture in the Sheldonian 

 Parry , give on 
 theatre 

 auspices of the Musical Club , Miss Fanny Davies , Herr 
 eisler and Mr. Whitehouse give a delightful concert , the 
 Brahms ’s Sonata for pianoforte and 

 programme include 
 violin in G ( op . 78 ) and Beethoven ’s pianoforte trio in D 
 op . 70 , no . 1 ) , in addition to very 
 harmingly play by Miss Fanny Davies 

 on March 4 in the Examination Schools 
 tet , under the auspex of the Musical Union , give an 
 ste concert , the principal piece be Beethoven ’s 

 in F minor ( Op . 95 ) , Mozart 's Divertimento for 
 , viola and violoncello in e flat and Dvorak ’s beautiful 
 yartet ( ' Aus der neuen Welt ' ) in F ( op . song 
 and Legrenzi be contribute by Mr. T. C 

 last , thougl 
 ‘ horal Soc ety and Bach Choir be , on 

 rmonic and ( 
 March 14 , concentrate upon 
 Town Hall , of Beethoven ’s Mass in 

 enthusiastic baton of Dr. Allen , for tl 

 Mendelssohn ’s 13th Psalm , Bach ’s church cantata ' Bide 
 with we ' and Spohr ’s ' last judgment ' constitute the 
 programme 

 at the last concert of the 
 Society , Stanford ’s quartet \Op . 64 ) in D minor , Beethoven ’s 
 quartet ( Op . 18 , no . 3 ) and Max Reger ’s Pianoforte trio 
 in B minor ( op . 2 ) be perform by Messrs. James and 
 J. W. Sharpe ’s quartet party 

 among other interesting event announce to be give in 
 the closing day of be concert by the 
 Sheffield Vhilharmonic Orchestra ( conductor , Mr. J. H. 
 Parkes ) and the Shefiield Amateur Instrumental Society 
 conductor , Mr. J. Duffell ) , also performance of ' Acis and 
 Galatea ' by the Hillsbro ’ Choral Society ; of Mendelssohn ’s 
 42nd Psalm , ' at St. Mary ’s Church ; Tozer ’s ' Way of the 
 Cross , ' at Zion Congregational Church , and Maunder ’s 
 * Olivet to Calvary ' at Peter ’s ( Abbeydale ) Church 

 stvie 

 whe mn ted , ured to secure a very effective per 
 formance of Beethoven 's seventh Symphony . on Mar 5 
 the Philharmonic Society , of which Mr. J. W. Hudson 1 
 conductor , give a concert the principal feature of which be 

 Tchaikovsky 's ' pathetic ' symphony , and on 

 Sonata for violin and pianoforte in F be play by Messrs. 
 G. E. Cathie , who lead the orchestra , and Mr. F. Lewis 
 Thomas , and the vocalist Miss Edith Miller and 
 Mr. Julien Henry 

 BURTON - ON - TRENT . the Musical Sociely give a concert 
 in the Town Hall on March 11 , when the 
 consist of ' * Hiawatha ’s Wedding - feast , ' Cowen ’s * John 
 Gilpin , ' and Beethoven 's C minor Symphony . the choir 
 and orchestra , number 170 , under the direction of 
 Mr. T. E. Lowe , give an artistic rendering of the first two 
 and the orchestra be hear to advantage 
 in the symphony . Mr. John Harrison be the solo vocalist 
 and Mr. Charles Collier play the solo part in Gabriel 
 Pierne ’s Concertstiick for harp and orchestra 

 CaTrorp.—The 
 conductorship of Mr. C. W. 
 ' Redemption , ' at St. Dunstan ’s College , on 
 the oratorio be eflectively render by a choir and orchestra 
 number nearly 160 performer . the singing of the choir 
 reflect great credit upon the member themselves and upon 
 the conductor , the music of the ' celestial choir ' be 
 render by a select body of boy and man belong 

 Chapel , Islington , on march,19 

 Beethoven 

 MUSICAL TIMES.—Apri- 1 

 1540 . the Toast 

 1541 . Spring time . part - song for T.'r . B. B. 
 L. VAN BEETHOVEN § 1d . 
 1543 . glory , honour , praise and power . 
 Motet , no . 3 - Mozart 2d 
 1545 . Ye that do live in pleasure plenty 

 Madrigal for s s. a.1 t b. 
 1550 . come , let we join our 
 Anthem for Easter E Vine HALL 14d . 
 saviour , thy child keep . Anthem 
 for Evening Service a. SULLIVAN Id. 
 ii URS , BERTHOLD — * dream . " arrange for 

