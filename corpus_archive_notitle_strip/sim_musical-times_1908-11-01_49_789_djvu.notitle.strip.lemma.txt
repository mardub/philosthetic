music - making which , at the midnight hour , be 
 conclude with ' God save the King , verse and 
 chorus , all stand . ' the meeting take place at 
 the White Hart , the identical hostelry at which 
 Mr. Pickwick stay on his memorable visit to 
 Bath , on which occasion Mr. Sam Weller remark 
 that he have ' never hear a bile leg o ' mutton 
 swarry afore . ' it be interesting to record 
 that in the year 1794 the name of the landlord of 
 the White Hart be P - ckwick 

 in 1822 - 23 Sir George Smart give a series 
 of nine subscription concert at Bath , when 
 Beethoven ’s first and fifth Symphonies be play , 
 also the overture ' Edvardo e Cristina ' and 
 ' Les Voitures Versées , ' by Rossini and Boieldieu 
 respectively , for the first time in England , and 
 Moscheles produce one of his _ Pianoforte 

 Sir George ’s own set of the programme 
 of these concert be preserve at the british | 
 Museum , together with the following receipt , bear | 
 1 four shilling stamp , of the erection of an organ 

 the King Edward Professor of Music , Sir Frederick | 
 Bridge , will deliver a course of five lecture during the | 
 season at the University of London , the subject of his | 
 discourse be ' Composers of Classical Songs . ' | 
 admission to the lecture , which be open to the| 
 public without fee , be by ticket , for which application | 
 should be make to the Academic Registrar , University | 
 of London , South Kensington 

 millionaire musical - autograph collector may gratify | 
 their desire by acquire three treasure now be | 
 offer for sale by a Leipzig antiquarian bookseller . | 
 the first be Beethoven ’s ' Bagatellen , seven short 
 pianoforte piece ( Op . 33 ) , consist of nineteen folio , 
 for which the sum of 22,000 mark ( £ 1,100 ) be ask . 
 the second ' Lot ' consist of the original of the same | 
 ' thirty - three variation on a waltz by| 
 Diabelli ' ( Op . 120 ) , compose in 1823 . it may be } 
 acquire — without any copyright advantage to| 
 recompense the buyer — for 42,000 mark , or £ 2,100 , | 
 which be equal to , say , £ 49 for each of the forty - three 
 folio constitute the little volume . thirdly , there be 
 the original score of Wagner ’s ' love - feast of the | 
 Apostles , ' value by its present owner at 12,500 mark , 
 or £ 625 , a mere bagatelle for so elaborate a work and 
 a compare with the sum demand for the| 
 Beethoven trifle 

 master ’s 

 the old country about Christmas - time 

 viennese paper announce the discovery , by Herr 
 \lexander Hajdecki , of twenty - six hitherto unknown | 
 ietter of Beethoven . they be all address to the | 
 master ’s friend Bernhard , editor of a Vienna paper | 
 and author of sundry operatic libretto . even more | 
 important than the letter seem a long document of | 
 forty - seven page , write throughout in Beethoven ’s | 
 almost illegible handwriting . this take the form of 
 a pro memoria , and be also address to Bernhard , | 
 to enable he to prepare a petition in proper form , to | 
 the court of appeal , in connection with the unsuccessful 
 action bring by Beethoven to obtain the guardian- | 
 ship of his nephew Carl , after the death of that| 
 unworthy youth ’s father . the document be state to| 
 be a further proof of Beethoven ’s nobility of character , | 
 and of a degree of culture remarkable in one who 
 never receive the benefit of any scholastic training 

 this from a Yorkshire newspaper 

 the famous collection of old musical instrument 
 formerly belong to Herr Paul de Witt , the 
 proprietor - editorof the Zez¢schrift fiir Instrumentenbau , 
 have be acquire and present to the city of 
 Cologne by Herr Commerzienrath W. Heyer , to form 
 the nucleus of a new historico - musical museum . to 
 this have be add the collection form by the well- 
 know firm of pianoforte manufacturer , Messrs. 
 Ibach , of Barmen , as well as a number of instrument 
 specially purchase . thus Cologne will soon be 
 able to boast of one of the most representative 
 museum of the kind in existence 

 Messrs. Schuster & Loeffler , of Berlin , announce 
 the early publication of a new and complete edition of 
 the literary work of Carl Maria von Weber . the 
 volume , edit by Herr Georg Kaiser , will contain no 
 less than forty article not include in any previous 
 collection . it will be interesting to see whether the 
 new editor will make an attempt to ' explain away ' 
 those passage in which Weber as conclusively proved 
 himself a dad critic — zde his remark on Beethoven ’s 
 fourth Symphony — as in his opera he demonstrate 
 the fact that the say bad critic be by no mean a 
 bad composer 

 german musical paper record the production of a 
 ' satyrspiel ' compose by Herr Gustav Wied and 
 bearing the fascinating title 

 Eighth Concert , Monday , July 8 , 1844 

 Part I. 
 Sinfonia Eroica~ - : . - - - Beethoven . 
 Song , MS . , ' Ach Herr , ' Herr Staudigl - - Nivolat , 
 trio , two violoncello , and double bass , 
 Mr. Lindley , Mr. Lucas , and Mr. Howell Cor 

 scene from ' as you like it , ' Miss A. Williams , 
 Miss Dolby , Mr. Allen , and Herr Staudigl , 
 with chorus 

 the experiment be its approach to success , 
 and , it may be hope , its suggestion to non - opera goer that 
 they should hear the masterpiece complete on the stage . the 
 performance be a fine one . , there be fourteen soloist 
 all well - know singer , and the chorus be excellent 

 the last day be devote to Bach ’s ' St. Matthew ' Passion , 
 Beethoven ’s choral syimphony , and a _ miscellaneous 
 selection . the supremacy of Bach be never more con 
 vincingly exhibit than on this occasion . the performance 
 be regard by all as the culminate feature of the festival . 
 immense pain have be take to rehearse the work . Mr. 
 Wood write an organ part , and the orchestra be specially 
 constitute to give effect to the composer ’s colouring and 
 method . there be eight flute , eight oboe , eight 
 bassoon ( for which Bach do not write ) , two oboi d’amore , 
 two oboi da caccia , and a solo viola da gamba . the chief 
 vocal soloist be Mr. Dalton Baker , Mr. Webster Millar , 
 Mrs. Henry J. Wood , Miss Jessie Goldsack , Mr. H 
 Witherspoon , Mr. Robert Charlesworth , and Mr. Joseph 
 Lycett . although all be competent , a special word of 
 praise be due to Mr. Millar for the able way in which he sing 
 the arduous part of the Evangelist . the tone - colour secure 
 in the accompaniment be peculiarly beautiful and haunting . 
 the chorus - singing be sublime — there be no other word to 
 describe it . one might feel that with all its tremendous , 
 thrilling force the famous chorus ' have lightning and 
 thunder ' be too staccato , and that the lovely last chorus 
 be somewhat too daintily treat , but the overwhelming 
 grandeur and expression of the interpretation generally will 
 live long in the memory when all else have be forget . 
 the performance be a triumph for Mr. Wood and 
 Dr. Coward , and a noble tribute to the genius of Bach 

 at the evening concert , which conclude the festival , the 

 panie piece be efiective , if not specially so . the 
 Cornelius piece call for more mood . the trio would 
 have be well as music with half the number of voice , 
 but all the same the tone be beautiful . the motet go 
 splendidly . 1 t be evident that the Shetfield choralist have 
 take to Bach 

 Beethoven ’s Choral Symphony be the last work perform . 
 the instrumental movement display the orchestra at its 
 good , and the choir unexhausted by its previous great effort 
 again blaze forth triumphantly . the soloist be : Miss 
 Jenny Taggart , Miss Edna Thornton , Mr. Lloyd Chandos 
 and Mr. Frederic Austin 

 the attendance be good , but the seat be rarely all 
 fill . about five - seventh of the audience at any time 
 consist of lady , a fact that may not imply lack of interest 
 on the part of the man , but simply that they c yuld not spare 
 the time to attend . the service of Mr. J. W. Phillips at 
 the organ throughout the festival deserve special mention . 
 it remain only to add that Mr. J. A. Rodgers contribute 
 many able and interesting analytical note to the programme , 
 and that the official , from the secretary , Mr. EF . Willoughby 
 Firth and Mr. Noel W. Burbidge , downwards , be 
 particularly courteous and attentive 

 ighly favourable impression which this group of — 
 piece make at their production at the recent Worcest 
 festival , be fully confirm on the occasion of their 
 initial hearing in the metropolis . of the six number 
 form the suite , those most favourably receive at 

 1een ’s Hall be the open March , the 
 and the Finale , the vigour the last - name prove 
 very acceptable after the sustained delicacy of the precede 
 three number . another feature of the concert be the re 
 appearance of Mr. Eugene Ysaye , who revive Corelli ’s 
 Concerto Grosso No . 8 , for two solo violin , string orchestra , 
 and organ . in this extremely interesting example of 17 t 
 century sic the belgian violinist be most ably assist by 
 Mr. Maurice Sons , who gore the second violin solo part , 
 and Mr. Frederick B. idle at the organ . Mr. Ysaye 
 be also hear at hi A. good in Beethoven ’s concerto 

 enthusiastic applause that he be rec d 

 success 

 be no doubt that in his case we have 
 talent which have also progress far in its development and 
 especially surmount all the great difficulty of technique 
 with victorious ease . at the same time , the performance of 
 the wonder - boy have amply demonstrate that his appearance 
 be premature ; at least he be as yet unable to sound the 
 emotional depth of Beethoven ’s concerto . Colbertson , who 
 be fourteen year old , be bear in Roumania and train by 
 Prof. Sev€ik at Prague 

 RICHARD VON PERGER 

 Wesley aloud with 
 Lloyd ’s eight - part chorus 

 mposition be perform 
 Messrs. T. I ’ . Morris , 
 Wiltshire and Ivor 
 Beethoven ’s Quintet 
 1 , and Messrs. Morris 

 ively play the Cavatina 

 idgment , Mr. Friskin prove himself to be a pianist of 
 outstanding ability , his technical power be of the high 

 order and his reading , especially of Beethoven , give 
 evidence of ripe musicianship 

 under the direction of Mr. A. M. Henderson , a local 
 pianist of distinction , two very enjoyable chamber concert 
 be give on ( ctober 20 and 27 . the programme 
 include trio for pianoforte , violin and violoncello by 
 Mozart and Arensky , in which the concert - giver be 
 associate with Miss Bessie Spence ( violin ) and Mr. John 
 Linden ( violoncello ) , and quartet by Rheinberger and 
 Dvorak , play by Miss Bessie Spence ( violin ) , Mr. John 
 Daly ( viola ) , Mr. George Bruce ( violoncello ) and Mr. A. M. 
 Henderson ( pianoforte ) . some solo piece be als 
 contribute by the various performer . among other 
 music - making have be chamber concert by a new 
 combination , ' the Glasgow Trio’--Miss Maggie Horne 
 ( violin ) , Mr. John Linden ( violoncello ) , and Mr. Wilfrid 
 Senior ( pianoforte ) ; and by Messrs. Philip Halstead and 
 Henri Verbrugghen . also a pianoforte recital by Madame 
 Carrefio ; a violin recital by Mr. Holroyd Paull ; and a 
 week ’s performance of opera by the Moody- Manners 
 Company 

 MUSIC in LIVERPOOL . 
 from our own corresionden!i 
 T pening concert of the seventieth season of the 

 ilharmonic Society take place on October 13 , when a fine 
 vance be give of Beethoven ’s fourth Symphony 

 Carrefio play in a brilliant and virile inter 
 Grieg ’s Pianoforte concerto , and Signor 

 volution of the string quartet , ) by Mr. H. McCullagh 

 Mendelssohn : a centenary celebration , ' and ' Beethoven 
 i his music , ' Rev. H. H. McCullagh ; ' irish fairy song 

 1 tale , ' Miss Madelaine 

 MUSIC in MANCHESTER . 
 ( from our own correspondent 

 a great and enthusiastic audience welcome Dr. Hans 
 Richter on October 15 to conduct the first of the twenty 
 weekly concert of the Hallé Society . the programme 
 nclude the ' Meistersinger ' overture , Strauss ’s ' Tod und 
 Verklarung , ' and Beethoven ’s C minor Symphony . Madame 
 Carreiio play Grieg ’s Pianoforte concerto , Liszt ’s sixth 
 rhapsody , and , as an encore , the late Edward MacDowell ’s 

 dainty little piece , ' Ilexentanz . ' at the second concert 
 Dr. Brodsky play the Violin concerto in D of his fellow 

 countryman and friend , Tchaikovsky . the _ orchestral 
 selection include Beethoven ’s ' Egmont ' overture ; 
 Debussy ’s Prelude , ' Aprés - midi d'un Faune ' and 

 TIMES.—NoveMBER 1 

 for the season on October 21 , the performer , as from the 
 commencement , be Dr. Brodsky , Mr. Rawdon Briggs , 
 Mr. Simon Speelman , and Mr. Carl Fuchs Haydn ’s 

 Juartet in C ( Op . 76 , no . 3 ) ; Schubert ’s first pianoforte 
 Trio in b flat ; and Beethoven ’s ( uartet in a minor 
 ( Op . 132 ) be in the programme . in the trio Mr. Max 
 Mayer be at the pianoforte , greatly help Dr. Brodsky 
 and Mr. Carl Fuchs to secure for the work an inspiring 

 interpretation 

 he opening of a new organ , buil 

 by Mr. A. Keates , of Sheffield . Mr. C. W. Perkit 
 Birmingham , give the first public recital on new 
 nstrument 
 +34 ed 
 Foreign Wotes . 
 the Koyal Oratorio Society ’s programme for the come 
 season include Handel ’s ' Joshua , ' Beethoven ’s Chora 

 Symphony , Vincent d’Indy ’s ' Lay of the bell , ' an ' Ode t 
 friendst > by T. Wager ' i ' the Resu et of ( set 

 I n 

 he first genuine sensation of the new musical season « 
 from an unexpected quarter , viz . , the first concert of the Roya 
 Orchestra , give in the Koyal Opera House on October 2 
 the programme contain nothing more ' sensational ' thar 
 hree old little - know work in | 
 Haydn , an equally unhackneyed masterpiece in A by Mozart , 
 ( b. h. no . 29 ) , and Beethoven ’s ' Eroica . ' but on 
 conduct rostrum stand Germany ’s foremost ving 
 musician , Dr. Richard Strauss , to his debut as 
 conductor of the Royal Orchestra , the successor o 
 Weingartner in the coveted office , and he give performance 
 » f these classical piece so unconventional in every way a 

 symphony — a 

 at the Symphony Concerts conduct by Prof. Karl Panzner , 
 in the Mozartsaal , the novelty will include Felix Woyrsch ’s 
 C minor and Hans Huber ’s ' heroic ' Symphonies ; the 
 symphonic poem ' the Nun , ' by Leo Blech , and ' hero and 
 Leander , ' by Paul Ertel ; an * overture toa shakespearean 
 comedy , ' by Paul Scheinpflug , and the prelude ( ' harvest 

 Festival ' ) to Act iii . of Max Schillings ’s ' Moloch . ' 
 yet another symphony orchestra have be form . under 
 he direction of Herr Oskar Fried the member thereof , 
 n October 11 , give their first very successful concert at 
 the Bliithner - Saal , after which the institution be call 
 ' Bliithner - Orchestra . ’-——In the Beethoven - Saal , Herr Fritz 
 r give a concert October 10 , at which he 
 woduce a work of his own composition , viz . , an 
 Introduction ( 77 ti i = Rectitativo ) and Scherzo 
 esque ' for violin alone 

 on 

 FRANFURT - on - MAIN 

 the old and most distinguished local concert society , the 
 Frankfurter Museumsgesellschaft , celebrate on October 2 
 the hundredth anniversary of its foundation by give a 
 festival concert , with Eugen d’Albert as soloist in Beethoven ’s 
 ' Emperor ' concerto 

 HANOVER 

 Th ussical Concer soctety ( formerly th 
 Concert Committee ) will give eight concert , seven 
 consist of chamber music , at Bechstein Hall , and or 

 of orchestral and choral music , at ( ) ueen ’s Hall ( December 9 ) . 
 string quartet by Haydn , Beethoven , Schumann and Brahm 
 be announce for performance , also Beethoven ’s Strit 
 in G major , Mozart ’s Trio for pianoforte , clarinet and viola , 
 Dvorak ’s Pianoforte trio in F minor , Mozart ’s ' ) uintet for 
 pianoforte and wind instrument , Brahms ’s Pianoforte quintet , 
 and other work . Schubert ’s ' Grand duo ' ( op . 140 ) , for 

 pianoforte duet , arrange for full orchestra by Joachim , will 
 be play at the ( Jueen ’s Hall Concert ( it have not be 

 J. T.—The following be suggest rate of speed 
 Haydn ’s Pianoforte sonata in EF flat — first movement , 
 rotchet- 80 ; second movement , quaver- So ; last move 

 nent , minim 84 . Beethoven ’s Pianoforte sonata in D 

 NOVEMBER 1 , 1908 , 735 
 ( op . 28)—a 0 , dot minim 60 ; Andank , iver 
 84 ; Scherzo , dot minim 96 ( Zrio 8S ) ; ndo , dot 
 crotchet s4 

 DURHAM you will find the shake write out 
 the Cotta edition of Beethoven ’s Rondo in G 

 or pianoforte . the dante section may 

 cal score vs N 

 gift sician , S. ] of Leipzig ) SOU 
 fn t yt ing ut unplay s fre he instrume 
 4 f e Roy College of Music ) : th a taste 
 ] t eve ess he por 1s t Holy Scriy 
 \ ' i w i analyt | Ger 
 e have , price 
 BACI Mote pr y Lord English Version by HENry J. Woo ! use at Sheffield and Norwich 
 | vocal Score 6d , 
 Ilumor Cantat ' ¢ Pj und Pan . " sed at Norwich Festival oa 7 ” be 1 . 
 m a use at Norwich Fest 98 ° i 1 
 BEETHOVEN . ( ¢ ral Symy ] version by REv . Sovu1 ARD . sed at Sheffield , Bristol and 
 Norw Fes S Vocal Score 2 . od . 
 BERLIO Te De 1 at Sheffield Festival be " a 2s . od . 
 DELIUS , FR * * Sea Drift . use at Shettield Festival - 99 4 1 . 
 RANCK , CESAR ' the Beatitudes . " use at Shetheld Festival Vocal Score . net $ s 1 . 
 HARRISON , [ . a * * Cleopatra . " ' write specially for the Norwich Festival Vocal Score 2s . od . 
 ALESTRINA * * lamentat I use at Sheffield Festival " " 1 . 
 PART - song . 
 BAINTON , | AR L. the Miracle . choral song for Mixed Voices . ( s. A ) 1 . 
 BANTOCk , GRANVILLI Elfin Musi Poem by SHELLEY . trio for female voice . ( Ss . MEZZO . 1 . 
 BERGER , FEDOI Song of the Pied Piper . english version by Rev. Canon GorTON . for man ’s voice 5d 

 s t » p 7 ; 
 R 
 ELY , THOMAS . ' * home , they bring she warrior , dead . " for mixed voice 
 GI MA alm - S ay Morning Poem by G Englisk version by HELEN F. BAntt 
 I 
 t l i r 
 p | y who 
 \ ( ig 
 \ rs a Song of Cheer for our Dear Lad English wor y | ENGLAN for 
 \ score 
 SHA EI TRAM . O I Fir Tree a Br Vords by HELEN Hunt J 
 art 4 
 PIANOFORTE SOLO . 
 278 SIBELIUS , JEAN . Al the Song now still’d . 2 . Andantino from ' ' Kyllikki . " 3 . romance 
 nd } . 3rd movement from the Sonata , op . 12 . 5 . dance intermezzo . 6 . Alla Marcia , 
 ron * * Carelia ” ’ suite . 7 . war song Tyrtcus . S. at Eventide , Finnish Song 3s 
 ¢ : frer tk 1 ; f e 
 . s 
 a t ! 
 I 
 " w 
 v pis tt rep 
 st 
 preci w 

