439 

 hand , fact theory insist 
 sufficiently pattern , length , tone 
 motive predetermines working - . 
 motive Beethoven fifth Symphony calls , inside 
 general pattern classical movement , 
 methods working 
 suit motive Beethoven sixth ( 
 modern music , illuminative comparison 
 sections Borodin 
 second _ — unfinished — Symphonies re- 
 spectively ) . Creative faculty displayed alike 
 inventing theme working , possibly 
 working - . _ inferior 
 composer expose thoroughly 
 treatment ‘ pure , abstract ’ motive 
 piece ‘ materially descriptive ’ music , despite 
 semblance style achieve 
 closely adhering standard rules 

 limitations purely descriptive cr imitative 
 music obvious soon overlooks 
 played — freely played — creative 
 imagination ; begging question , 
 remains demonstrate , instance , 
 M. Ravel ‘ Jeux leau , ’ founded 
 imitative motives , afford 
 elaborate display purely musical imagination 
 wish 

 44¢ MUSICAL TIMES.—JuLy 1 , 1913 

 called conventional association , ! emotional interest fact entries 
 hero left ground , borrowing its}in seconds aim reproducing ‘ fashion 
 justification ‘ analogy founded } timorous singers . ’ , effect js 
 term . ’ | excellent : Kuhnau attempted straight . 
 ‘ pure ’ music variation | forwardly express Gideon anguish , 
 work under- overlaid | question achieved better 
 holding note . classical fugue , , | harsh , tremulous seconds — daring 
 holding note appears — view merely|effect time , long ag 
 abstract musical effect — assert return | passed sphere pure music . 
 original tonality emphasise conclusion . find separate questions 
 , M. Ravel ‘ Le Gibet ’ — poetic | deal ‘ crossing recrossing 
 music , descriptive programmatic — | territory . ’ keeping apart 
 holding note obtains beginning end , its|enabled study point turn 
 obvious purpose produce effect | suppress causes ambiguity . 
 harping dreariness obsession . written remaining 
 comparisons suffice | point ( actual programme controlling 
 student told materially | ‘ order phrases appear way 
 descriptive music music lowest sort , is| played ) 
 asked blindly endorse dogmatic principle , | Wagner ‘ Letter tone - poems 
 postulate quibbling has| Franz lI.iszt ’ ? Mr. Newman essay 
 reduced ; closer study | ‘ Programme - music , ’ words summing . 
 matter proves , proofs adduced in| suffice . 
 favour principle poetic music . Briefly , , imitative , descriptive 
 stimulus afforded suggestions , material | poetic dramatic elements suggest motives 
 immaterial , intervened cases than| pregnant novel 
 aware . instance , told that| merely formal , abstract motives , 
 Beethoven imagined motive Scherzo | programme suggest new plastic forms . 
 ninth Symphony whilst watching afar | gifted composer , satisfactory 
 lamp - lights Vienna flare through|forms derives programmes 
 mist ; anecdote true , } forms stand close relationship wih 
 find little reason accepting for|the ‘ regular ’ forms — combination int 
 rejecting . lone , use certain deviation 
 true , instance suffices uncommon occurrence — 
 useful material suggestion hands | acknowledged type . 
 composer genius , | perennity law constructions 
 ends ‘ pure ’ music . likewise | great superstition dogmat 
 note , contrary current belief , | principles student 
 abstract data , ‘ loftier , ’ | cautioned . ma 
 useful composer poetic music . | abstract , general definitions artistic beauty 
 inclined think high - sounding philo-| offered , pot ! 
 sophic symbolic scheme suit purposes | Diderot . ‘ sense Beauty , Fren 
 musical art better ripple brook | philosopher said , originates notion relation 
 ( exceptis excipiendis ) lighting rows lamps . | ship — work art independent . 
 leads everlasting confusion | purport , 
 mind feeling . Music expresses | parts stand obvious satisfactory connectio . 
 emotion mediums sound |and artistic pleasure consisting perfect appre 
 rhythm . material starting - points | hension connections purport . 
 imitative descriptive music suggest musical| predetermined scheme like classic 
 elements good proportion | symphony , , great help heare . 
 intrinsically beautiful apt convey | hand , contain suff 
 emotion . abstract notion suggests | total possible relationships , preclude 
 sound rhythm ; abstract | satisfactory schemes . Artistic education consis ’ 
 , emotional . properly|in acquiring capacity apprehending © 
 appeal , , stimulus composer’s| relationship parts work t0 
 imagination , indirect profitable | understand message conveyed work 
 wise . ‘ holding - note Dr. Strauss ‘ Don|Why people blindly adhere 
 ( Juixote ’ mere freak , puerile material ltenets past ? Simply 
 imitation bleating sheep another|incapable , exactly mat ! 
 section work . remains | writers music incapable going 
 permitted judge musical point | routine fixed methods preordained schemes 
 view pure simple . | Diderot definition helps explain — 
 case point afforded ‘ Gideon’s| art judges believe pure music stand highe 
 doubts ’ Kuhnau Bible Sonata . * can|level poetic music . having 

 ially 
 |how absolutely independent — 
 S.e Musical Times , , 1913 , ‘ Problem Discord . | descriptive strictly programme - music remas 

 flattery music . broad , majestic , 
 proud spirit , course fine specimen technical 
 power . Weber ‘ Der Freischiitz ’ Overture 

 Beethoven fifth Symphony completed programme 

 concert June 2 , Herr Mengelberg gave 
 Tchaikovsky fifth Symphony , 
 employed individuality , sense climax , 
 art bringing musical design . ably - wzitten 
 Pianoforte concerto Mr. Haydn Wood ( produced 
 recent Patron Fund Concert ) played skill 
 enthusiasm Miss Tina Lerner . ‘ Meistersinger ’ 
 Overture Concerto Grosso Handel added 
 enjoyment evening 

 June 9 privilege 
 choral incursions Yorkshire periodically 
 remind deficiencies South . Leeds 

 Philharmonic Chorus co - operated Orchestra 
 performance Elgar ‘ Music- Makers ’ Beethoven 
 Choral Symphony . singing : cantata highly 
 pointed shaded , formed suitable choral 

 background high artistry Miss Muriel Foster 

 Mr. Thorpe Bates 

 M. Paderewski doubtless chiet attraction 
 June 16 , hall practically , 
 performance Beethoven ‘ Emperor ’ Concerto 
 ideal . lacked spontaneity charm , 
 dominated personality exponent expense 
 music . concert memorable , , 
 splendid interpretation ‘ Eroica ’ Symphony , 
 given Herr Nikisch direction 

 MUSICAL TIMES.—Juty 

 MUSICAL TIMES.—JuLy 1 , 1913 . 469 

 unequalled Trio concerts given | Brahms ‘ Kreutzer ’ Beethoven . 
 MM . Thibaud , Casals , Bauer Queen Hall | songs included Lieder Mr. Harty ‘ bivouac 
 2 . programme consisted Trios Brahms in| fitful flame ’ ( Whitman ) . , 

 C minor ( Op . 101 ) , Saint - Saéns F major ( Op . 18 ) , | admirably performed . 
 Tchaikovsky minor . eure 
 Sees interesting concert Beecham Orchestra 

 Misses Elsa Cecilia Satz , Bechstein Hall , June + 
 Sonata D pianofortes , J / ozart 

 Mr. Louis Edger , Steinway Hall , June 4 — Sonata jp 
 major , Beethoven 

 Mr. Eugen d’Albert , ( Queen Hall , June 5 — Sonay 

 appassionata , Beethoven . 
 Miss Katherine Goodson , Bechstein Hall , 
 flat , Op . 110 , Beethoven 

 June 5 — Sonan 

 Mr. Arthur Rubinstein , Bechstein Hall , June 6- 
 * Carneval , ’ Schumann . 
 Herr Ernst von Dohnanyi , AZolian Hall , June 7 — Sonau 

 Op . 111 , Beethoven 

 Miss Kate Friskin , .Zolian Hall , June 11 — ‘ Davidsbindler , 
 Schumann 

 Miss Isolde Menges ( violinist ) , ( Queen 
 Sonata D minor , Brahms 

 Miss Beatrice Harrison ( violoncellist ) Mr. Enget 
 d’Albert ( pianist ) , Bechstein Hall , June 6 — Sonatas 
 Beethoven ( major ) , Brahms ( E minor ) , Saznt - Sam 
 ( C minor 

 Mr. Mischa Elman 
 Concerto F sharp minor 

 pia anist- 
 Bechste 

 Mr. David Levine ( pianist)—Sonata . 3 , C , 
 Beethoven ; Miss Marguerite Le Mans _ ( vocalist ) — 
 ‘ Chansons de Miarka , ’ 4/7 . 4 . George ; Mr. Philip Levine 
 ( violinist ) — Sonata , Brahms ; Molian Hall , 27 

 Madame Sobrino ( vocalist ) — ‘ Kling , ’ Strauss ; Senor 
 Sobrino ( pianist)—Sonata G minor , Schumann ; 
 Bechstein Hall , 28 

 Miss Myra Jerningham ( pianist ) Miss Marjorie Hayward 
 ( violinist ) , Aolian Hall , June 17 — Sonata G major , 
 Nicholas Gatty 

 Mr. Bronislaw Huberman , Queen Hall , June 17 — Sonata 
 major , Beethoven 

 Mr. David Madame Clara Mannes ( violinist pianist ) , 
 Bechstein Hall , June 17 — Sonata , César Franck 

 orchestra memory late Lord Ashbourne ( 
 Lord Chancellor Ireland patron Society ) . 
 Madame Borel , Mr. William Lewin , Mr. T. W. Hall 
 soloists . Dr. Charles Marchant conducted 

 Sunday Orchestral Concerts Woodbrook came 
 end present season concert June 1 , 
 programme included Beethoven seventh Symphony 
 Dr. Esposito sé cond Irish Rhapsody violin orchestra 
 ( Signor Simonetti soloist ) . Mr. Arthur MacCallum 
 vocalist . 25 chiefitem programme 
 Schumann ‘ Andante Variations ’ 
 pianofortes , beautifully played Miss Fanny Davies 
 Dr. Esposito . Miss Davies played group short 
 pieces Scarlatti , Brahms , Liszt . 
 Haydn , B flat 

 LIVERPOOL . 
 names season ‘ 
 definite arrangements 
 Philharmonic Society , added Mr. Max Fiedler 
 Mr. Hamilton Harty . Elgar ‘ Caractacus ’ 
 selected performance ‘ Messiah . ’ Sir 
 Frederic Cowen conduct works , 
 advantage chorally prepared Mr. Harry 
 Evans . Composed Leeds Festival 1898 , 
 strange long interval elapse 
 performance Elgar ‘ Caractacus ’ Liverpool . 
 belated recognition merits interesting 
 attractive work , Philharmonic Society 
 wise choice , ‘ Caractacus ’ quasi - novelty 
 commend subsequently occasion 
 acrimonious discussion daily press . 
 choral novelties selected produced ensuing 
 Leeds Festival October , Mr. Hamilton Harty 
 ‘ Mystic Trumpeter 

 report Hallé Society June 2 
 | pleasant reading . loss Manchester concerts 
 | £ 400 , concerts centres 
 showed deficit £ 316 ; debit balance 
 | reduced £ 5 necessitating £ 3 guarantor . 
 | appeal public special funds 
 guarantors pay balance guarantee 
 |together resulted - persons promising , 
 instalments , aggregate £ 1,952 . , £ 1,340 
 | invested , yielding £ 55 annum , 

 pay extra rehearsal . net proceeds 
 Annual Pension Fund Concert £ 161 . coming 
 winter , arrangements completed bulk 
 | players engaged weekly terms instead pei 
 engagement , provision concerts 
 extra rehearsals addition customary 
 day concert . 
 | Arrangements Scotch tour 
 spring ; executive impressed 
 character Edinburgh Glasgow press criticisms 
 Beethoven Festival Concerts Edinburgh March , 
 taken unusual course printing 
 brochure form sending copy subscribers ! 
 think Balling men appreciated 
 true worth Manchester , 
 knocked heads Edinburgh Glasgow critics 
 executive action 
 comprehensible 
 distinctive character critiques 

 season , Mr. E. J. Broadfield announced 
 repetition ‘ Parsifal ’ evening , Verdi ‘ Requiem ’ 
 honour centenary ( work heard Manchester 
 past - years ) , Brahms * Schicksalslied , ’ 
 Walford Davies ‘ Song St. Francis ’ ( setting 

 474 MUSICAL TIMES.—JvuLy 1 , 1913 

 desirous singing ‘ Atalanta Calydon ’ , | Orpheus Choir exists bond real fellowship T 
 definite known head ; wonders | born recognition Choir supreme artistic qualities P 
 Bantock new choral work movements , called | celebrations German national occasions usually fing DI 
 * Vanity Vanities ’ considered ‘ possible ’ ; | Choir singing Kaiser Consul heart $ e 
 February 14 Mr. Harry Evans Liverpool . voic sage time German ! ) . return Frankfurt 
 orchestral novelties arranged include Max|in , Captain Schlagintweit brought Heégar new m 
 Reger ‘ Concerto ancient style ’ ; Sinigaglia | work ‘ 1813 ’ ( commemorative Napoleonic debacle w 
 ‘ La Baruffe Chiozzotte ’ ; Strauss ‘ Aus Italien ’ and|a years ago ) Orpheus men study . 
 Holbrooke ‘ Queen Mab ’ Scherzo ( legacies | festivities June 13 inaugurated Mr. Nesbitrs ar 
 season ) ; Rachmaninoft Pianoforte concerto , “ played | men singing Adolf Frey rousing lines , wondered sh 
 composer ; Bantock ‘ Helena F. B. ’ Variations , a| fared piece huge ne 
 Bruckner Symphony . Rachmaninoff , | competition Frankfurt 4 . banquet 
 hear Siloti , Cortot , Irene Scharrer , Isolde Menges , | sang German Strauss ‘ Liebe ’ ‘ Brauttanz 
 Brodsky , numerous vocalists . } Brahms ‘ Wiegenlied ’ ; later evening 5 
 recent years attempts , | Consul daughter Felix Fleischer ( Bremen , bat bu 
 success , bring home executive necessity | Carl Rosa ) repeated Wolf Ferrari ‘ Susannens sai 
 introducing fresh blood body . executive } Geheimniss , ’ produced occasion o Ey 
 elected , , guarantors £ 100 ( | Kaiser Birthday Banquet 1912 . Dr 
 case subscribers ) instead main body — — — _ f int 
 subscribers ; consequence executive | senkeaieiieaiis m . . — 
 drawn restricted - a08 g conceivably ablest | NEWCASTLE DISTRICT . ‘ = 
 persons duties find disqualified } event importance month bee aa 
 inability assume guaranteeship . year recital modern song given Mr. Frank Maullings , 
 deaf ear turned requests . | June meeting Northern Section LSM 
 June 2 executive admitted idea groups songs chosen works Hug 
 receiving consideration ; pressed vague reply | Wolf , Richard Strauss , Granville Bantock ( ‘ Ferishtah 
 chairman hinted course | fancies ’ ) Roger Quilter ( Shakespeare songs ) respectively , 
 certainly taken ‘ meeting ’ ( presumably in| Mr. Mullings sang rare ability insight , M. § 
 June , 1914 ) . Bishop Welldon , persistent | W. G. Whittaker accompanied . Mr. T. Henderson , hon 
 point democratic — receiving | secretary Northern Section contributed explanator 
 reply indicated promptly suggested names | notes songs , devoting particular attention 
 Dr. J. Kendrick Pyne Mr. S. El . Nicholson ( past | Prof. Bantock . c 
 present organists Cathedral ) , produced | — — - — -- Mu 
 rejoinder guarantors , OXFORD . Chi 
 specialists | ’ , 
 executive . | 3 , delightful pianoforte recital given pee 
 outside observer Assembly , Room Town Hall Mr. Harold Bauer cree 
 pressing need choice men principal items Bach ‘ Italian ’ Concerto bow 
 acknowledged business capacity , qualities unite oo Sonata F major , Op . 78 . ; th 2 
 keen musicianship , wide outlook world music , 10 , Town Hall , complimentary concert ‘ 7 
 sound ideas , catholicity taste , artistic judgment | given ies benefit Mrs. Sunman ( widow oe 
 relied conductor hisown . Men | late Mr. Henry Sunman , years member Mr 
 type found movements achieving | Cathedral choir ) , nearly £ 60 realised . 2 
 great ends . Liverpool possessed men combined lay - clerks Colleges & H 
 late Alfred E. Rodewald Mr. J. Dudley Johnston ( | sincerely congratulated excellent concert . 
 removed London ) . Men like frequently better * Eights - week ’ concert given Balliol 
 posted current musical matters _ professional 18 , items Ackroyd Sinng Mr. 
 musicians , simply passion music leads | ( Juartet gave excellent interpretations Smetana ( Quartet cons 
 totake live interest things — amateurs in|in E minor , Brahms Quintet F minor , Op . } H 
 fundamental sense word ; sure | days later interesting concert : J 
 unflinching fidelity root principles , couple | Exeter , chief item Stanford * Revenge , whic Sulli 
 persons type executive worth half - - dozen | ably c nducted organ scholar , Mr. HH . S. Price , 
 professional musicians . | songs - songs given miscellaneous 
 writer J / anchester Courier recently said , | . derfc 
 truth : ‘ striking act supreme intelligence might| 21 came Keble concert , tha cred 
 cancel past instant . Courage , high sense |a thousand people enjoyed . orchestra sale 
 adventure , willingness face strenuous period criticism , provided , gave good account Prelude Mrs , 
 adverse generation touch | Act ‘ Die Meistersinger , ’ Berlioz ‘ * Hungaman Mr. | 
 flow things musical — needed order | March , ’ Grieg ‘ Peer Gynt ’ Suite ( Op . 46 ) , Schubert ’ Bowe 
 tackle problem fill seats abating | ‘ Rosamunde ’ Overture , Op . 26 . rem : rining items wer . Mr. 
 artistic ideals . ’ |songs - songs given conductorship @ 
 Saturday , June 21 , choristers| Mr. D. G. A. Fox ( organ scholar ) Mr. r. F ot qr 
 assembled Mr. Nicholson Cathedral | ( Jueen followed day ane interest Xccon 
 Diocesan Festival , large numbers leaving little room | programme , consisting sets songs ear yor succe . 
 congregation . Wesley ‘ Ascribe unto Lord , ’ | nicely sung Fraulein Diestel , - songs , tw § 
 Brahms ‘ Requiem ’ chorus ‘ lovely Thy dwellings , ’ instrumental Trios flute , violin , piano forte , tt 
 Bach ‘ Come , ye thankful ’ ( ‘ Christmas | Kuhlau César Cui . . 
 Oratorio ’ ) , Smart ‘ Magnificat , ’ constituted chief concert term , given auspices © Mr. 
 Festival work . Musical Club , took place 28 , Town Hall . om 
 girl - operatives Miss Ashworth Ancoats } Dr. Allen directed programme eat wr Mar | 
 Institute Choir leave Manchester August 4 en route | excellence . consisted Beethoven seventh Symp phony ; 
 ( vid Folkestone Boulogne ) Zurich , , ] Elgar ‘ Enigma ’ Variations , ‘ Tristan ’ Prelude ant trio , 
 Tonhalle , conjunction orchestra , concerts | Liebestod , César Franck poem pianoforte 
 given August 6 7 ; proceed | orchestra , * Les Djji om Mr. Egon Petri _ Mn 
 Lucerne , appearances Kursaal . } Mr. Balfour Gardiner ‘ Shepherd Fennel ¢ — ( Violin 
 remain days , generous conductor | pleased audience portion ™ 
 hopes , ‘ best time lives . ’ | repeated way encore . Music ‘ 
 Manchester celebration - fifth June 10 , Sir Walter Parratt , Professor _ ~ pees . 
 anniversary Kaiser accession throne | gave usual terminal lecture Sheldonian — w 
 took place June 13 befitting manner . large audience , subject * orches 

 beginning end 

 AMSTERDAM 

 - fifth anniversary foundation 
 Concertgebouw Orchestra celebrated days ’ 
 musical festival Herr Mengelberg direction . 
 programmes included Mahler ‘ Das Lied von der Erde ’ 
 eighth Symphony , works Beethoven 

 ANTWERP . 
 Peter Benoit choral works ‘ Noél ’ ‘ Alma 
 Redemptoris , ’ Concert - overture E. Wambach , 
 ‘ Elegie ’ Sokolow ‘ Hlamlet ’ Overture 

 LEIPSIC 

 Wagner celebrations elaborate 
 master native town . Performances given 
 |the Municipal Theatre , operas ‘ Rienzi ’ 
 | ‘ Gotterdimmerung . ” 22 , 10 - 30 a.m. , 
 foundation - stone monument Max Klinger laid . 
 | followed 12 o’clock festival concert 
 | Gewandhaus , Beethoven ninth Symphony 
 |given direction Prof. Arthur Nikisch . 
 | evening festival performance ‘ Die Meistersinger ’ 
 | took place Municipal Theatre —— 23 
 | extensive Wagner exhibition opened , concert 
 given Alberthalle 24 , ‘ Das Liebesmahl 
 | der Apostel ’ performed 

 LILLE 

 MINUET.—{Sonata E Frat ) . ‘ ia 31 , iii 

 BEETHOVEN 
 A. C. MACKENZIE 

 8 . PRELUDE . — ( “ Cotomsa 

 English Version » Rev. J. Trovu SCK . 2 | ‘ 
 yy ee Sy Oe - TRCTIER . Tee ( 1813 - 1865 ) . Edited W. A. BARRETT . Price x 

 BEETHOVEN.—Twenty - Soncs . Vol . I. English MOZART , W.A.—NINETEEN SonGs . English Ge 
 German words . English Version Rev. words . English version Rev , J. Trou Tan } 
 J. TROUTBECK . Price ts . 6d . D.D. Price 1s . 6d 

 BEETHOVEN.—SEVENTEEN Soncs . Vol . II . English 
 German words . English Version Rev. 
 J. TROUTBECK . Price ts . 6d 

 BEETHOVEN.—TweEnty - Soncs . Vol . III . English 
 German words . English Version Kev . 
 J. TROUTBECK . Price 1s . 6d 

 BENNETT , W. STERNDALE 

