Fripay Morninc : Bach ’s MASS in B minor 

 Fripay Eveninc : Bruckner ’s te DEUM ; Dvorak ’s symphonic 
 variation ; Brahms ’s ALTO RHAPSODIE ; Beethoven 's . 
 choral symphony 

 Mesdames ALBANI , AGNES NICHOLLS , MURIEL FOSTER , 
 KIRKBY LUNN , and CLARA BUTT 

 MR . HENRY PLEVY 

 TENOR ) 
 ( of the Beethoven Festival , Queen 's Hall Choral and Crystal Palace 
 Concerts . ) 
 Solo Tenor , Christ Church , Lancaster Gate . 
 Banpury PHILHARMONIC SociETY . — ' ' HIAWATHA . ' ' — ' Mr. Henry 
 Plevy hasan excellent voice , with good compass and style , and he give 
 Onaway ! awake , ' with the great acceptance , his singing be 
 characterize by feeling and expressiveness throughout . it be an 

 admirable effort his other number be also well deliver , 
 and his high note ring out well in ' then the black - robe chief , ' 
 which be ably sing . ""—Guardian 

 LARGE BOX of Orchestral Society ’s surplus 

 MUSIC , 50s . list : 4 dozen Flute and Piano duet and 
 trio , 15 . ; Haydn , 66 String Quartets , 20s ; Chamber Music , 
 symphony ( Reissiger , Beethoven , Mozart ) , 18 volume , 3c . studio , 
 12 , Gipsy Hill , London 

 the MUSICAL TIMES.—Septemper 1 , 1903 

 Old Tycho Brahe , and modern Herschel 
 have something in ’em ; but who be Purcell ? 
 the devil with his foot so cloven 

 for aught I care , may take Beethoven ; 
 and if the bargain do not suit 

 I will throw he Weber in to boot 

 the performance of Port Talbot be distinguish 
 by a beautiful blend of tone , rich and resonant , and 
 unified as to vowel production . it be a wonderful 
 rendering on the technical side , and on the 
 spiritual side equally remarkable . it make a pro- 
 found impression . pentre be very little behind . 
 the opening measure be not striking , but later 
 the performance become intensely interesting because 
 of its moving expression . Khymney also display 
 fine tone , magnificent execution , and deep , thrilling 
 expression . the verdict be give in favour of 
 Port Talbot , apparently with the enthusiastic assent 
 of the vast audience 

 orchestral and chamber music be not a speciality 
 in Wales . as artistic progress be to a great extent 
 dependent upon the development of instrumental 
 practice , it be gratifying to record that on this occasion 
 two orchestral band give respectable performance 
 of no less severe a test than Beethoven ’s ' Leonora ' 
 overture no . 3 . the Llanelly orchestra under Mr. 
 David Thomas be award the prize . a remarkable 
 rendering of Hummel ’s Pianoforte Trio , Op . 22 , no . 2 , 
 by the ' Vagabonds ' ( Miss Marion Lloyd and other , 
 of Newport ) be describe by Mr. C. Francis Lloyd 
 as one of the fine performance of chamber music 
 ever give in Wales by native performer . another 
 interesting section be that for choir of lady ’ voice , 
 in which a choir from Carmarthen under Mr. H. F. 
 Ellingford ( a Londoner train at the Royal College 
 of Music ) be triumphant . a choir from Nantlle 
 Vale under Mr. J. Jones Owen be very little inferior 
 in merit 

 some comparatively small prize of £ 2 or £ 3 for 
 original song , anthem and part - song fail to bring 
 forward any special talent . a prize of £ 20 offer 
 for a cantata to certain welsh word succeed 
 in attract only two competitor . as one of 
 the composition be not of sufficient merit , 
 and the other , although display commendable 
 talent , be not nearly finish , the prize be withhold . 
 by far the most notable result of the composition 
 competition be the unearthing of remarkable talent 
 in two of the six string quartet submit . we , the 
 adjudicator , be all strong in our admiration of 
 these interesting proof of welsh power in composi- 
 tion . we be so impressed with the comparative 
 inadequacy of the £ 5 prize offer , that Mr. Lloyd 
 on our behalf use his influence with the Eisteddfod 
 Association , and get they to agree readily — an act 
 very greatly to their credit — to add £ 10 to the prize , 
 and Major Bythway contribute a further £ 5 . we 
 be thus enable to award substantial first and 
 second prize respectively to Mr. John Williams ( who 
 I believe be study in London ) and Mr. W. H. 
 Dean , Mus . Bac . , of Llandrindod Wells , who be 
 announce as the successful competitor 

 new and exalted mood . where else than in Wales 
 could such an incident happen 

 the evening concert must have at least brief 
 record . a concert choir of 350 singer have be 
 gather locally and assiduously rehearse , and an 
 orchestra of fifty competent player lead by Mr. E. G. 
 Woodward have be engage . ' Israel in Egypt ' 
 be perform at the first concert . the chorus 
 show that they have be well train by their 
 conductor Mr. John Thomas , and the whole 
 performance be smooth if not stirring . there be 
 hardly a slip from beginning to end . the soloist , 
 Madame John Thomas , Madame Hannah Jones , 
 Mr. Thomas Thomas and Mr. David Hughes 
 be all fully competent . the second concert 
 be miscellaneous . Madame Sobrino sing with her 
 usual brilliancy . the overture to ' a Midsummer 
 Night ’s Dream ' be not very well play , and 
 the orchestral accompaniment to the song be 
 sadly lacking . a fair performance of Beethoven ’s first 
 symphony be give . but on the whole it must be 
 say the handling of the orchestra betray in- 
 experience . the third concert produce an oratorio , 
 ' the Legend of St. David , ' the composition of a 
 native musician , Mr. David Jenkins , of Aberystwith . 
 this work be first produce some year ago at 
 an Eisteddfod , and it be hear in London on a 
 later occasion . much pain have be spend by 
 Mr. John Thomas in order to secure a good 
 performance . the work be well but not enthusias- 
 tically received . it favourably exhibit Mr. Jenkins ’s 
 fluency , but it would gain greatly by considerable 
 compression . the programme of the last concert 
 consist entirely of welsh composition . I regret 
 that I be unable to be present 

 although there be the usual and apparently 
 inevitable unpunctuality , the general arrangement . 
 show foresight and be carry out with tact and . 
 exemplary good temper . the official be numerous 
 and indefatigable . Mr. W. H. Protheroe , the General 
 Secretary , deserve special mention for his never- 
 fail courtesy and singular capacity for the work . 
 perhaps his buoyancy be help by the fact that the 
 enormous audience ensure the financial success of 
 the Eisteddfod . next year the ' National ' will be 
 hold at Rhyl , and in 1g05 at Mountain Ash . it will 
 be difficult for either place to beat the Llanelly 
 record , either as to musical result or as to 
 attendance . the receipt amount to over £ 5,000 . 
 a surplus of £ 500 be leave at the disposal of the 

 Elgar . 
 ( compose expressly for the Festival 

 Elijah eG we ' Mendelssohn . 
 the Voyageof Maeldune .. = e Stanford . 
 the golden legend .. Sullivan . 
 Messiah .. ae Handel , 
 Psalm xiii . Liszt . 
 bl pair of Sirens Parry . 
 Mass in b minor . Bach . 
 te deum be Bruckner . 
 choral Symphony Beethoven . 
 ORCHESTRAL work . 
 symphony in G minor .. Se . Mozart . 
 symphony — Harold in Italy .. . Berlioz . 
 Symphonic Variations ‘ vorak 

 orchestral Poem — a phantasy of Life and Ginn 
 Love se es seca 

 the PROMENADE concert 
 at QUEEN 's HALL 

 a huge audience assemble at the familiar rendezvous 
 in Langham Place on the evening of Saturday , the 
 22nd ult . , when another season of autumn concert , under 
 Mr. Robert Newman 's experienced management , be 
 inaugurate . as Mr. Henry J. Wood make his way to 
 the conductor 's rostrum , there break forth a terrific 
 storm — of thunderous applause , which give proof , if 
 proof be need , that the popular chef d@’orchestre reign 
 supreme in the esteem of Promenaders who stand still in 
 listen to the excellent and varied fare provide for 
 their aural delectation . we may well say varied , as the 
 most eclectic of taste must surely be satisfy in , for 
 instance , a programme ( announce to be give during 
 the opening week ) that be to begin with a symphony in 
 e flat by the divine Mozart and conclude with the 
 Washington Post March by Mr. Sousa , with a Beethoven 
 Symphony and a Madame Angot fantasia throw in 

 the programme of the opening night cover the 
 well - tread ground of a Promenade Concert . we have 
 a overture : ' Tannhauser , ' ' William Tell ' ( its ever- 
 beautiful violoncelli opening most delicately play ) and 
 ' 1812 . ' with these be intermingle two hungarian 
 Dances , the ' Peer Gynt ' Suite , and three orchestral 
 excerpt from the ' Faust ' of Berlioz , the whole con- 
 stitute the first part of the programme , with the addition 
 of song contribute by Miss Rose Ettinger , Mr. Lloyd 
 Chandos . and Mr. William Ludwig . with the exception 
 of a little roughness noticeable here and there in the 
 playing of the band — a little defect doubtless due to the 
 holiday interval , but which will disappear when the 
 wood - wind and ( may we say ? ) the Wood - string have 
 settle down to regular work — the enjoyable evening 's 
 music may be regard as a foretaste of many other 

 miscellaneous 

 the Chamber Concert at the Hovingham Musical 
 Festival to be hold on the 23rd and 24th inst . , and to 
 which we call attention in our last issue ( p. 527 ) , be to 
 have the co - operation of Professor Johann Kruse , in 
 addition to that of Miss Fanny Davies , Mr. Herbert 
 Withers and Miss Agnes Nicholls , the programme 
 will include Beethoven ’s ' Waldstein ' Sonata for 
 pianoforte and Bach ’s Chaconne for violin , Canon 
 Pemberton , the originator and conductor of the Festival , 
 be to be congratulate on an excellent scheme which be 
 sure to be efficiently carry out under his able direction 

 the sixth annual general meeting of the Incorporated 
 Staff - Sight - Singing College take place at the Guildhall 
 School of Music on July 25 , Dr. W. H. Cummings in 
 the chair . the annual report show a gradual and 
 uninterrupted extension of the College examination and 
 of the adoption of its publication , and a steady increase 
 in its membership ; while the improvement in its financial 
 position have be great than in any previous year . the 
 year ’s total of certificate and diploma grant amount 
 to 191 . various lecture have also be organize , all of 
 which have prove uniformly successful , and special 
 mention be make of the debt of gratitude which the 
 College owe to its esteemed president , Dr. W. H. 
 Cummings , Principal of the Guildhall School of Music . 
 the Hon . Treasurer , Dr. Warwick Jordan , and Hon . 
 Secretary , Dr. Hamilton Robinson , be specially 
 thank for their value service and re - elect to their 
 respective office , and after an informal discussion of 
 matter connect with the College , a vote of thank to 
 the chairman bring the proceeding to a close 

 X. Y. Z.—In order to prepare for the Mus . B. 
 examination you have well seek the advice of a 
 reliable coach . it be difficult to say how long the period 
 of preparation would be necessary to one who be ' not 
 much at theory ' ; but as you be a good practical 
 organist and pianoforte player , there be no reason why 
 you should not become a good theorist . do not let 
 your coach drive you at motor - car speed 

 MEZzzo - Soprano.—There be a dozen setting , at least 
 of Adelaide Procter ’s poem ' the lost chord . ' one of 
 they be a most terrible atrocity in the way of tinkering 
 to think of the lovely slow movement of Beethoven 's 
 Pianoforte Sonata in e flat ( Op . 7 ) maltreat thus 

 seat - e one day at iT gan , Xc 

 of April last , have recently be issue by Messrs 
 D. Appleton and Company , under the title of ' Musical 
 Education 

 StuDENT.—Yes , you be quite right , the Beethoven F 

 Pianoforte Sonata in e flat to which you refer be Opus 7 

 St. Andrew ’s Church , Wells Street , all in London , where 
 they will hear ' some of our good church choir 

 J. G.—You can obtain an interesting photogravure F 
 portrait of Beethoven ( size 19 } x 16 inch ) from the fF 
 Berlin Photographic Company , 133 , New Bond Street 

 Messrs , Breitkopf and Haertel , Great Marlborough 
 Street , also publish one or two portrait of the master 

 F * musical 

 Beethoven 

 is Opus 7 
 m position 

 108 number 
 one shilling each 

 select from the work of 
 Abel , Adams , Albrechtsberger , André , Astorga , Attwood , 
 Bach , Battishill , Beethoven , Bierey , Bishop , Bonno , 
 Buononcini , Brownsmith , Cafaro , Casali , Carissimi , 
 Cherubini , Clari , Colonna , Corelli , Danby , Dietsch , 
 Dietenhoffer , Dupuis , Durant , Fasch , Fesca , Flowers , 
 Gibbons , Gluck , Graun , Handel , Haslinger , Haydn 
 ( Joseph and Michael ) , Himmel , Horncastle , Hummel , 
 Jomelli , Keeble , Koslovsky , Leo , Lotti , Majo , Martini , 
 Mendelssohn , Mozart , Novello , Neukomm , Palestrina , 
 Perez , Pergolesi , Perti , Pescetti , Portogallo , Porto , Quarle , 
 Ricci , Righini , Rink , Russell , Sala , Sarti , Scarlatti , 
 Schicht , Spohr , Steffani , Stokes , Stradella , Vogler , 
 Walmisley , Watts , Webbe , Weber , Wesley , and Winter , 
 with several adaptationscontribute by eminent organist 

 select and arrange by 

 each book contain 

 fifty melody by the follow composer . 
 Albrechtsberger , Attwood , Bach , Battishill , Beethoven , 
 Bellini , Berger , Boyce , Buononcini , Cherubini , Clementi , 
 Cooke , Couperin , Cramer , Croft , Donizetti , Drago- 
 neiti , Dussek , East , Florimo , Eliza Flower , Gollmick , 
 Geminiani , Gluck , Goudimel , Graun , Greene , Handel , 
 Haydn , Herold , Hesse , Hummel , Juvin , Kalkbrenner , 
 Keeble , Klose , Krufft , Mendelssohn , Minoia , Mozart , 
 Natividad , Neukomm , Novello , Onslow , Paradies , Paxton , 
 David Perez , Pinto , Pleyel , Purcell , Reading , Reinagle , 
 Righini , Romberg , Rosa , Rossini , Rousseau , Russell , 
 Schneider , Seeger , Spohr , Steibelt , Stokes , Travers , 
 Turnbull , Viner , Webbe , Weber , S Wesley , Winter , 
 Woelfl , and Wranizky 

 select and arrange by 

 third edition , revise 

 BEETHOVEN 

 NINE symphony 

 PRICE , CLOTH , GILT , SIX shilling 

 t recognise it , without the small hesitation , as one of the most 
 important and valuable among recent contribution to musical litera- 
 ture ... . the well inform of professional musician may learn a 
 great deal about the master - work of Beethoven from Sir George 
 Grove , whose wide reading and acute perceptiveness have enable 
 he to marshal an astonishing array of fact , and whose intimate 
 acquaintance with the spirit of the master have qualify he to throw 
 light upon page which , to many , be still obscure . . . . I must be 
 satisfied with the remark already make , earnestly recommend all 
 who recognise Beethoven ’s greatness as show in his immortal 
 Symphonies to obtain Sir George Grove 's volume , and walk in the 
 luminous path . through which he be ready to conduct all who trust 
 his guidance . ’’"—Daily Telegraph 

 I.ondon : NovELLo and Company , Limited 

 ORGAN ACCOMPANIMENTS to the LORD ’s PRAYER , APOSTLES ’ and NICENE creed 

 the canticle , with the Cathedral Psalter Pointing , have be set to appropriate chant by Sir John Stainer : 
 Sir George C. Martin , Sir Joseph Barnby , Sir George Elvey , and other well - know church composer . 
 rriterol the response to the commandment have be select from the service of the above composer as well as 
 to fini p those of Myles B. Foster , Dr. Garrett , C. Gounod , Dr. E. J. Hopkins , Dr. C. H. Lloyd , Merbecke , H. Smart , 
 n . [ John E. West , S. S. Wesley , and other . 
 1e that Sir John Stainer ’s sevenfold amen be also include , as well as Vesper Hymns by Beethoven , Sullivan , and 

 e f ts S 
 4 y other ; conclude with two Vestry Prayers by S. S. Wesley and the Rev. Canon Hervey . 
 lip of 4 
 ent o 7k > 
 ns Bs Bel PREFACE . 
 the real Fe Organists , choirmaster , and librarian of choir often have occasion to wish for a choral service - book of a compre- 
 tart of be hensive nature . this desideratum the ' ' Parish Choir Manual " ' seek to meet by supply within one cover the Canticles 
 hymn . be for Morning and Evening prayer , response ( ferial , festal , and to the Commandments ) , Final Amens , Vesper Hymns , 
 ‘ and Vestry Prayers , in addition to organ accompaniment to the Lord ’s Prayer and the A , ostles ’ and Nicene Creeds . 
 eserves b the constant usefulness and the practicability of such a compilation be obvious , and a glance at the name of the 
 rive to b- composer will assure confidence in a publication that should commend itself for use ' tin quire and place where they 
 i sing . " ' 
 TIN . vaveees = S ee wate oe . : , 
 Loxpon : NOVELLO and COMPANY , LimitTEep , snp NOVELLO , EWER and CO . , New York . 
 — ' m 
 es now ready 

 price twelve shilling 

 this masterly treatise and accept classic on a subject of 
 which Berlioz be so consummate a master , be copiously 
 illustrate by excerpt from the full score of work by 
 Gluck , Mozart , Beethoven , Weber , Spontini , Meyerbeer , 
 and especially from those of the author 

 London : NOVELLO and Company , Limited 

 each , net 

 BEETHOVEN.—-Twelve Menuets inédits ( ceuvres posthumes 

 for Pianoforte .. . each , net 1 . ; complete , net 

 STEIBELT . — G , op . 78 , no . 10 ( Franklin Taylor ’ s Studies , 
 book 2 

 BEETHOVEN . lite ' in b fi at , " from Sonata no . 13 , Op . 22 

 Cuopin.—Mazurka in C , op . 33 , no . 3 

 book 16 ) . 
 Mayer.—In a minor , op . 168 , no . 18 

 BEETHOVEN . — -Rondo ( Finale ) from Sonata in dd ; no . 7 1 op . 10 , 
 no . 3 om 
 Cuopix.—Nocturne in a fiat , op . 32 , " no . 2 

 MENDELSSOHN.—Lied ohne 
 ( lie , no . 36 ) . sas Aer 
 Moscuetrs.—In d minor , op . 70 , no . 6 ' 
 Bacu.—Italian C oncerto , first Movement ... 
 ScHUMANN.—Romance in f sharp , op . 28 , no . 
 a ERGER . — die jag d , ’' Op . 5 , no . 1 ( Pianoforte Albums 

