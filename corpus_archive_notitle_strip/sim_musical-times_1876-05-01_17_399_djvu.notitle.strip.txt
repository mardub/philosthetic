ISS ALICE FAIRMAN’S MORNING CON- 
CERT, ST. GEORGE’S HALL, W., WEDNESDAY, 
May 31st, at Three o’clock.—Vocalists: Mesdames Edith wee and 
Blanche Cole, Badias, and A. Fairman; Messrs. E. Lloyd, N. Varley, 
Wadmore, andL. Thomas. Instrumentalists: Madame Varley-Liebe, 
Messrs. Ganz, Mattei, Richard Blagrove, and F. Chatterton. Con- 
ductors: Messrs. Ganz, F. Kingsbury, F. H. Cowen, and S. Naylor. 
Tickets, 10s. 6d., 5s., and 2s.,at Messrs. Schott and Co., 159, Regent- 
street, and at 18, St. Peter’s-square, Hammersmith

BC RUBERT SOCIETY, Beethoven Rooms, 27

Harley-street, W.—President, Sir Juttus BenEepict; Founder 
and Director, Herr SchuserTH.—Tenth Season, 1876.—The THIRD 
CONCERT will take place on Thursday, May 11th. The Society's 
Concerts afford an excellent opportunity for young rising Artists to 
make their first appearance in public. Particulars on application to 
H. G. Hopper, Hon. Sec

THE series of Saturday Concerts at the Crystal Palace 
is now concluded, the interest having been well sustained 
to the last. Taking up our record where we left it last 
month, we have first to notice the very fine performance 
at the concert of the rst ult., of Raff’s violoncello concerto, 
by Signor Piatti, Joachim Raff, one of the most prolific 
of living composers, is gradually obtaining in this country 
the recognition which he has long since received in 
Germany. Three of his symphonies have already been 
heard here, and a fourth (No. 2, in C major) is among the 
promises for this season at the Philharmonic Concerts. 
His concerto for the violoncello is, like many of his other 
works, remarkable for cleverness of construction rather 
than for decided originality of invention. It is, however, 
so melodious, and so admirably written both for solo 
instrument and orchestra, that with such a rendering as it 
received from Signor Piatti, its success could not be other 
than certain. At the same concert an overture entitled 
‘Les Muses,” the last composition, we believe of the 
recently deceased Alfred Holmes, was brought forward for 
the first time, but without producing any great impression

A very excellent performance of Beethoven’s ‘‘ Mount 
of Olives,” the solo parts being given by Madame Osgood, 
Mr. W. H. Cummings, and Signor Foli, occupied the 
greater part of the afternoon on the 8th; but the special 
novelty of this concert was the first production at the

Gore Ouseley has “finished an oratorio, ‘ Hagar

PHILHARMONIC SOCIETY

PRESUMING that a funeral service is a legitimate com- 
position to place before a Philharmonic audience at St. 
James’s Hall, we may congratulate the Society upon the 
second presentation of Brahms’s ‘“ Requiem,”’ which was 
given on the 6th ult., and applauded with a vehemence most 
complimentary to everybody concerned in its execution, 
however such audible expression of satisfaction might 
shock the sensitive nature of those who felt the religious 
purport of the work. The originality displayed in this 
‘* Requiem” prevents anything like comparison with those 
masterpieces which have been bequeathed to us by the 
great composers who have passed away ; it must be judged 
only as the earnest outpouring of an artist who feels the 
importance of his mission and dares to think for himself. 
Many of the movements are masterly specimens of con- 
structive power; and the orchestral colouring, although in 
parts somewhat overladen, is generally in excellent tone 
with the subject of the text. The baritone solo, with 
chorus, ‘‘ Lord, make me to know the measure of my days,” 
may be cited as one of the most impressive numbers of the 
work; andthe choral pieces are written with due regard 
to the solemnity of the words, and a careful avoid- 
ance of mere display, The fault so observable in 
modern German music, of over elaboration, is less apparent 
in this than in many other of the works of this composer ; 
and, save where restlessness of tonality induces restlessness 
with the listeners, we have but little fault to find with the 
modulations so frequently occurring throughout the work. 
In the solos, Mrs. Osgood and Mr. Wadmore were exceed- 
ingly effective, the lady’s rendering of the somewhat difficult 
music allotted to her evidencing the result of careful and 
intelligent study. Another important feature in the pro- 
gramme of the evening was the fine performance of Spohr’s 
violin Concerto in E minor, by Herr Joachim; and when 
we add that the purely orchestral works were Beethoven’s

Ruy Blas,” it will be seen that the concert was in every 
respect of the highestinterest. Mr. Cusins, who conducted 
as usual, deserves every credit for the excellent manner in 
which he directed Brahms’s ‘‘ Requiem

THE MUSICAL TIMES.—May 1, 1876. 461

Supper’? (Wagner), and Mass, No. 1, in C (Beethoven) ; 
Friday evening, ‘St. Paul” (Mendelssohn). The chief 
novelty of the Festival will be the Oratorio of Mr. Mac- 
farren, in which the principal solos will be sung by Mr. 
Santley. The Danish composer, Niels Gade, will be repre- 
sented by a new sacred Cantata, and by a secular Cantata, 
“The Crusaders.” Mr. F. H. Cowen’s new Cantata, 
“The Corsair,” is based on Byron’s poem; and Richard 
Wagner’s sacred Cantata, ‘‘ The Last Supper,” is described 
as a grand composition for voices and orchestra. Engage- 
ments with most of the principal singers have already 
been concluded ; the choral rehearsals are proceeding with 
regularity, and the band is now in process of organisation

At the second and third of Mr. Willem Coenen’s 
**Chamber Concerts of Modern Music,” the selection of 
works was extremely interesting. Brahms’s Quartett, for 
strings, in C minor, produced at the second concert, will 
assuredly assert its right to be again and again heard at 
performances devoted to chamber compositions, for we are 
satisfied that increased familiarity with its many beauties 
will render it a favourite both with audiences and executants. 
The “ Adagio” is a charming movement, and its popular 
character of course ensured it a warmer welcome than was 
accorded to the rest of the composition, the elaborate 
nature of which could not be appreciated by the most 
attentive listener at a single hearing. Schumann’s Trio 
for pianoforte, viola, and clarionet, in which the concert- 
giver was ably supported by Messrs. Zerbini and Lazarus, 
produced a marked effect, the melodious third movement 
displaying Mr. Lazarus’s exquisite tone and power of ex- 
pression to the greatest advantage. Mr. A.C. Mackenzie’s 
quartett—played at these concerts, and commented upon 
by us last season—was ,well received, the brilliancy and 
certainty of touch exhibited by Mr. Coenen in the difficult 
pianoforte part being the theme of universal admiration. 
One of the most interesting features in the programme of 
the third and final concert, on the 13th ult., was Raff’s 
Sonata in D major, for pianoforte and violoncello, rendered 
to perfection by Messrs. Coenen and Daubert. The second 
movement, marked “ Vivace,” is a perfect gem, and fairly 
took the audience by surprise, the two artists so thoroughly 
sympathising with each other throughout as to cause a 
murmur of satisfaction at the conclusion, which, to the 
credit of the executants, was not seized upon as an encore. 
Schumann’s string quartett in A major (Op. 41, No. 3), and 
Gernsheim’s pianoforte quartett in C minor (Op. 20), were 
also important items in the programme, and were received 
with the warmest marks of approbation. Mr. Coenen, with 
the modesty of a true artist, has but rarely put himself 
forward alone at these concerts, but on this occasion he per- 
formed a solo of his own composition, ‘‘ Caprice Serenade,” 
with a delicacy of touch and brilliancy of finger which made 
us long to hear more of his playing. The refined and 
melodious subject upon which this little sketch is based— 
alternately sung with the right and left hand, and surrounded 
by the most graceful passages of ornament—will recom- 
mend it to all who cultivate the highest order of drawing- 
room music; for should those who attempt it not be gifted 
with the composer’s executive powers, even moderately 
played, we are satisfied that the piece would make its way. 
Before closing our notice, we must not forget to make 
special mention of Mr. Wiener’s violin solos, which elicited 
the most enthusiastic applause, and also to award a word 
of praise for the vocal pieces contributed at the second 
concert by Miss Sophie Ferrari, and at the third by Miss

THE Passiontide and Easter services at St. Stephen’s, 
Lewisham, have been marked by some special features. 
On Thursday evening, in Passion week, Mendelssohn’s 
‘Christus ” was performed with excellent effect. On Good 
Friday evening a special service was held, which included 
a short selection from the “ Messiah,” and the solo and 
chorus “ With Jesus I will watch and pray,” from Bach’s 
‘“ St. Matthew Passion,” solo by Mr. G. F, Carter, of 
Westminster Abbey. The Rev. Canon Gregory preached. 
On Easter Sunday morning the English version of Gounod’s 
‘Messe Solennelle”” was sung. The above were given 
with instrumental accompaniments, and were under the 
direction of Mr. C. Warwick Jordan, Mus. Bac., Oxon

Miss Amy Stewart (a pupil of Herr Sauerbrey), gave 
a pianoforte recital, at Langham Hall, on the 5th ult., when 
she played with much success selections from Chopin, 
Schubert, Beethoven, Schumann, and Heller, and two 
compositions by Herr Sauerbrey. Madame Sauerbrey 
sang Schubert’s Serenade and Mozart’s “ Voi che sapete.” 
The room was well filled

Art the sixth trial of new compositions by the ‘ Musical 
Artists’ Society,” held at the Royal Academy of Music on 
the rst ult., amongst the many interesting works given may 
be mentioned a trio in D, for pianoforte, violin, and violon- 
cello, by Mr. F. E. Gladstone; a romance, for pianoforte 
and violoncello, by Mr. E H. Thorne; Ferdinand Hiller’s 
“Suite Moderne” (excellently played on the pianoforte 
by Mr. George Wheeldon), and two pianoforte solos by 
Mr. J. B. Calkin, effectively rendered by the composer. 
Two well-written songs by Mrs. -O’Leary-Vinning—the 
first sung by Miss Marion Severn, and the second by Miss 
A. Butterworth—elicited warm applause. Mr. Arthur 
O’Leary and Mr. Eaton Faning presided at the pianoforte

Gavotte (Festivale). By Alfred B. Allen

THIS piece was composed for and performed at Mr. 
Kuhe’s Brighton Festival during the present year. We 
should imagine that, being originally intended for an 
orchestral work, its effects are much enfeebled in the 
pianoforte transcription before us; but the theme is melo- 
dious, although we cannot say that the “ Trio”’ is much to 
our mind, the three consecutive fifths between the inner 
part and bass, in the 8th bar, being, to us, particularly 
disagreeable. By the way, we cannot understand why the 
diminished 7th on G¥ should be written as a {6-5 when it 
resolves to a 6-4 on the dominant. Surely the root is the 
same whether it resolve to a 7th or a 6-4, and varying 
notation like this is very apt to perplex a performer. 
Should Mr. Allen doubt how this chord ought to be 
written, let him refer to Beethoven’s Sonata in E flat (Op. 
31, No. 3), where in the 5th bar of the first movement, he 
would be rather surprised, we think, to find FZ instead of 
Gp. i

The Silent Land. Part-song for unaccompanied Choral 
Singing. The words from Longfellow’s ‘‘ Hyperion.” 
Composed by Alfred R. Gaul

BERWICK-UPON-TWEED.—On the 28th March the Messiah was 
performed by the Berwick Choral Union, numbering upwards of one 
hundred and twenty voices, assisted by an orchestra from Edinburgh 
and Glasgow, including the Drechsler-Hamilton family. This is the 
first time an Oratorio has ever been given in Berwick, and the per- 
formance was a great success. Mr. Anderson conducted, and Mr. 
Barker presided at the harmonium

BIRMINGHAM.—Mr. R. H. Rickard, gave a Pianoforte Recital at the 
Masonic Hall, on the 30th March, previous to his departure for Leipzig, 
to pursue his musical studies. The programme included two preludes 
and fugues by Bach, Beethoven's Sonata Appassionata, in F minor, 
Op. 57, Schubert's Impromptu, in A flat, No. 4, Op. go, Liszt’s trans- 
cription of the Tannhduser March, a selection from Schumann's 
Fantasiestuck, Chopin’s Ballade “ La Favorite,” Reinecke’s Ballade, 
Op. 20, and Weber's “ Concertstiick,” which were interpreted in a 
manner worthy of high commendation. In the last-named piece the 
orchestral accompaniments were given with judicious care and pre- 
cision by Dr. Swinnerton Heap. Mr. E. Allely and Mr. Randell 
contributed vocal music, selected from the works of Handel, Mozart, 
Verdi, Blumenthal, and Hatton

Botton, LANCASHIRE.—A Musical Service was given on Wednesday 
evening, March 2gth, in the Wesley ehapel, Bradshawgate. The 
Anthem “I was glad” (Sir G. Elvey), the “Credo” (Haydn’s First 
Mass), ‘O Father whose Almighty power” (Handel's udas), a selec- 
tion from the Last }udgment,and Beethoven's “ Hallelujah” (Engedi), 
were efficiently rendered by the chapel choir. Solos by Mr. H. Taylor 
(choirmaster), and Miss Fallows, were well sung, and the performance 
on the organ of Mendelssohn’s Trumpet Overture in C, Guilmant's 
“ Marche Funébre et chant Seraphique,” and the German Hymn, varied, 
by Mr. J. T. Flitcroft, the organist, were highly appreciated. A most 
interesting address was given during the interval by the Rev. John 
Rhodes, upon the subject of “ Ancient and Modern Music compared

CAMBRIDGE.—The members of St. Paul’s Musical Society gave their 
third Concert on the 28th March, when selections from Mendelssohn's 
St. Paul, and songs and part-songs were well rendered. A pianoforte 
solo, by Miss Dennis, the able accompanist, was a special feature of 
the evening. The Concert was a success, and the profits were, as 
usual, devoted to the Cambridge Industrial School——A Special 
Service was held in the church of All Saints, on the Wednesday 
before Easter, consisting chiefly of that part of the Messiah which 
relates to Christ’s Passion. The choir (not increased beyond its 
ordinary strength) was accompanied (besides the organ) by a band of 
twenty-two instruments. The chorus “ Behold the Lamb of God,” 
was followed by the contralto air, ‘‘ He was despised,” sung with great 
feeling by Mr. E. J. Bilton, a feature being that the second part of the 
air, “‘ He gave His back to the smiters,” not usually given, was included. 
In the choral of Bach’s “ O sacred head surrounded,” the congregation 
joined. This, as well as the other chorals was accompanied by the 
instruments alone, (as arranged by Mr. W. C. Dewberry, organist of 
All Saints), two verses being sung unaccompanied. The choruses 
“Surely He hath borne our griefs,” “ And with His stripes,” and “ All 
we like sheep,” were effectively sung. The choral ‘O sinner lift the 
eye of faith,” came next; and after the address and offertory the recit- 
ative, “ All they that see Him,” the chorus “ He trusted,” and the solos, 
“ Thy rebuke,” “‘ Behold and see,” and “ He was cut off,” which were 
given with taste by two regular members of the choir. After an inter- 
val for silent prayer, the service was concluded by the choral “O 
Love who formedst me to wear,” which was joined in by the large 
congregation. The organ was played by Mr. Mountain, of S. 
Saviour’s, Hoxton, and the service was conducted by Mr. W. C. 
Dewberry, A.R.A.M., organist and choirmaster of the church, to 
whose zeal and judgment the success of this choir is due

DunHAM Massey.—On the 22nd ult., a new organ, built by M. 
August Gern (late foreman of the works of Messrs. Cavaillé-Coll, 
of Paris), was opened at the church of St. Margaret, by Mr. ] 
Matthias Field, the organist, in the presence of a large congregation. 
The service was full choral, the permanent choir being considerably 
augmented. The Rev. Geo. London, of Altrincham, read the first, and 
the Very Rev. Dean Howson, of Chester, the second lesson, and the 
responses were given by the Rev. H. Hignett, of Ringway, Altrin- 
cham. The Rev. T. A. Stowell, of Salford, preached, and the Rev. 
Canon Gore, of Bowdon, pronounced the benediction. The organ, 
which will cost about £1,000, stands in the north transept, and is 
divided by a large window. Ithas been entirely re-built, only the pipes 
of the old instrument, which it is to replace, having been used

EAstTBOURNE.—On Tuesday, the 11th ult., Mr. J. H. Deane, organist 
of Trinity Church, gave a lecture at the Friendly Societies’ Hall, on 
the works of J. S. Bach and the later works of Beethoven. Com- 
mencing with Bach's Passion (St. Matthew) Mr. Deane described the 
most important features of the work, and in illustration, four of the 
Chorals were sung by the Trinity Church choir, and a Soprano air 
rendered with much taste by Miss Douglas. Mr. Deane, in the course 
of his lecture, endeavoured to contradict the general assumption that 
the compositions of Bach, especially his fugues, are dry, laboured, and 
expressionless, and gave the following apt illustration in support of 
their real musical value and artistic merit, viz., that Bach considered 
his four parts as four friends holding an amicable conversation, and 
that no one was to speak unless he had something to say, and that any 
one versed in them must readily admit how characteristic this is of 
the part-writing in all Bach’s fugues. Mr. Deane chose for an illus- 
tration the grand organ Fugue in A Minor, which he played on the 
piano, assisted by his daughter, who took the Pedal part. The other 
composition introduced was the Soprano air, ‘‘ My heart ever faithful,” 
being brought forward as a composition of a more jubilant character, 
in contrast with the Passion music. Before the second part of his 
lecture Mr. Deane introduced Moscheles’s piano duet, ‘“ Hommage a 
Handel,” and Cherubini's “ Ave Maria.” The performances illustra- 
tive of Beethoven were the first movement from the C minor 
Symphony, the Allegretto and Scherzo from the Symphony in A, and 
the song “ Knowest thou the land?” Mr. Deane pointed out the 
grandeur and beauties of Beethoven’s Symphonies, and in summing 
up, advised his hearers who had hitherto neglected Bach and Beethoven 
at once to set about the study of their works

Ecc.ies.—The Eccles Amateur Choral and Orchestral Society gave 
the last Concert of the season on Wednesday, March 2gth, in the 
Co-operative Hall. The programme consisted of glees, solos, selec- 
tions from operas, &c. The vocalists were Miss Lowe, Mr. George 
Barlow, and Mr. Ashe. Mr. De Jong played a couple of solos on the 
flute with his usual taste and finish. Mendelssohn’s Rondo BriRante, 
Op. 22, for pianoforte with orchestral accompaniments was well played 
by Mr. Lowe, the conductor of the Society. The ‘‘ Shepherds’ chorus,” 
from Schubert's Rosamunde, with orchestral accompaniments, a choral 
March, written by Mr. C. B. Grundy, Bishop's ‘‘ Echo song,” by Miss 
Lowe, with flute obbligato, anda violin solo by Miss Mather, amember 
of the orchestra, were features of the programme. The overture to Le 
Nozze di Figaro, Haydn’s No.13 Symphony, and selections from Lucia 
di Lammermoor were very creditably performed by the orchestra

The second part was miscellaneous. The accompanists were Mr. 
— Hill, Mrs. Hadley, and Mr. Walter Lain, organist of the parish 
church

Norwicu.—The first of a series of classical concerts, promised by 
Mr. Darken, took place in St. Andrew's Hall, on the 21st ult. Beethoven's. 
Septuor, arranged as a quintett for pianoforte, violin, viola, violoncello, 
and contra-basso, was well rendered by Miss Kate Griffiths, Messrs. 
C. Griffiths, H. Channell, J. Griffiths, and G. R. Griffiths. Miss 
Griffiths also played Chopin’s Scherzo in B flat minor and three 
sketches by Schumann, Henselt, and Chopin. Mr. Griffiths contributed 
a violoncello solo by Goltermann, and Messrs. C. Griffiths and H. 
Channell a duet of Kalliwoda’s for two violins. The concert con- 
cluded with Hummel's Trio in F, for pianoforte, violin and violoncello. 
The vocalists were Mdlle. Sophie Lowe and Mr. Vernon Rigby

NotrincHaM.—The report of the Sacred Harmonic Society, just 
issued, announces that the season has been very successful in a musical 
point of view; but owing to depression of trade, a trifling financial 
loss has been sustained, and with a view to liquidating this, the 
members of the Society have given, during the past month, an extra 
Concert of glees, part-songs, madrigals and solos by the members 
themselves. During the next season, four grand performances are to 
take place, the subscription to which will be one guinea, as before. 
Mr. Henry Farmer remains the able choir-master and conductor, and 
Mr. Marriott, hon. sec. It has now been decided to commence an 
elementary instruction-class, under Mr. John Adcock, as a useful 
branch, and as a nursery from which to draw a supply for the chorus, 
which a large new Hall will enable the Society greatly to increase

RamMsGATE.—On Wednesday evening, in Lent, a special service was 
held at St. Mary’s Church, a feature at which was the performance of 
Gounod’s “ There is a green hill far away,” arranged as Solo and 
Chorus by Mr. J. Finch Thorne, the organist and choirmaster. At the 
Easter services the following compositions were amongst those 
performed: J. Baptiste Calkin’s Te Deum in B flat; Dr. Stainer’s 
Magnificat and Nunc Dimittis in E; Sir John Goss, in A, and 
the Anthems “ They have taken away my Lord” (Dr. Stainer), “ Since 
by man came death,” “‘ Worthy is the Lamb,” and the “ Hallelujah” 
chorus. The mid-day celebrations on Easter-day and also on the 
Octave were fully choral, Marbeck’s music being used on both occasions

Rock Ferry.—On Wednesday, the roth ult., a Concert of sacred 
music was given in the Hotel Assembly-rooms. Mendelssohn's 
Hymn of Praise formed the first part, and the second part consisted 
of various selections from oratorio music, including Costa's “ I will 
extol Thee O God;” Handel’s ‘* Why do the nations?” Mendelssohn's 
“Cast thy burden upon the Lord,” &c.; Beethoven's “ Hallelujah” 
from the Mount of Olives, being the concluding piece. The principal 
soloists were Mrs. Billinie Porter, Miss Armstrong, and Mr. Alfred 
Brown, and the choir numbered forty voices. Mr. Billinie Porter was

accompanist, and Mr. Armstrong conducted

wee! g. 
Agnus Dei (Mass) Bes ove 
Dona Nobis, in D (Sixth Mass) « ila --- Mozart 
Introduction ... nts =e -. Rink 
Lord, remember David... wx see «» Handel | Freuet euch alles (Tad Jesu) 
Introduction +. Goss | Kyrie, in G et 08 b axpen 
Fugue, in C, on the Letters of his Name .. C.P.E. Bach Gloria,inF ... 
Etregeeos(Te Deum) _... .. Graun 
Introductory Voluntary, in F oa he .. Rink | O Lord our Governor (I Salmi) ... x . Marcello

No. 2. | We will rejoice (Anthem) a ae -. Dr. Croft 
The marvellous work (Creation) ... pan «- Haydn | Lauda Sion, in B flat ss Ay a ... Naumann 
Amplius lava me wmnigagd eb oe eo =Sarti Solemn March, in E flat... Sep .. Dr. Boyce 
Introduction ... ee i one igs +. Goss | O, thou eternal God (Crucifixion) Ay ... Spohr 
Fugue, in E flat «. Albrechtsberger Lead on, lead on (Judas Mepiate) me ... Handel 
Prepare the Hymn (Oceasional Or Oratorio) «» Handel Slow Mov ement, in C = haz ... Beethoven 
Dead March in Sau +» Handel “No. 15. De Cea

Dr. Croft

oan 0 J. & Bach Sancta Maria, inF ... - As -.. Mozart 
O, how sweet ... ose os a ent Introduction and Fugue in ‘E flat Rink ; 
Introduction Rink Et expecto resurrexionem and Et vitam (Fugue) Cherubini

But thou didst not leave (Messiah) ws S. Handel No. 16. 
Credo, in B flat (First Mass)... =...» Haydn When the ear heard (Funeral Anthem), ... Handel 
Introductory Voluntary, in F ate és «-- Mozart How are the mighty fallen 
No. 4. He delivered the poor jogos Anthem) Handel 
Teach me, O Lord (Mount Sinai) hep -- Neukomm Sanctus in A (Mass i in C) .. ss af ... Beethoven 
ae oo before the Mee beams Sas ... Haydn Benedictus, in F Caldara 
anctus Dominus, in D ayia So «- Mozart ee ats ay aie 
Holy, holy (Redemption) ... a eal Qual anelante (I Salmi) ave eso- veo coo Marcello 
How excellent (Saul) ws _ ‘ a «. Handel Benedictus, in B flat (Third Mass) 
Prai thie’Eord } 0. 5 Slow Movement, in E 
raise ye the ~ord | (Mount Sinai) _... .. Neukomm Kyrie in B flat (First Mass). 
Glorify the Lord.. Introduction 
Fac ut portem (Stabat Mater)... a «-» Boccherini Donaaohis (Third Mass) .. 
Quoniam, in B flat (First Mass) .. wae +. Haydn Introductory voluntary, in E 
Agnus Dei, in G ... Naumann Voliatar ’ 
Angels, ever bright and fair (Theodora) .. Handel 4 re i No: 18. 
Introductory Voluntary, in D 0 . «. Rink Recordare, in F (Requiem) s 
ae: st 
Then round about (Samson) ous --» Handel we a ome \ (Judas Masih) Handel 
Benedictus, in E flat (Tenth Mass) nig ++ Mozart Perché m’ hai derelitto (Sev. en Last Words). Haydn 
Introductory Voluntary,inD  ... M. Haydn And if to death (Susanna) .. .. Handel 
Forty-two aa si 9 ex in the principal major Cum Sancto Spiritu 4 
keys Es Bs Goss In gloria Dei (Fugue) ji tas oa . Haydn 
fg 0. 19. 
roy ig “}(Te Deum) i. se one) Baga Virgin Madre ye on Last Wests) sie «. Haydn 
we > There is a river (I Salmi) apa ... Marcello 
Benedictus, in B flat — ne? vee Eybler Overture to Saul (1st movement) «ee ~Handel 
Et vitam venturi ‘int me «» Righini Adagio, in E (from Op. 2) Beethoven 
Benedictus,inG . MM eS «- Righini Et gi virdbus (Fugue) wee sie cy RC seas 
Concluding Voluntary, i inc ves ae «» Drobs q sess 4 
No. 8. : Andante, in D (from a Sinfonia) ... ... Mendelssohn 
Introductory Voluntary, in G : + Rink Achieved i is the glorious work (Creation) -. Haydn 
Benedicta et venerabilis (Graduale) «+ oe =M. Haydn Hallelujah, Amen (Judas Maccabeus) . Handel 
Unendlicher! Gott unser Herr } Ss And J iis 
woe pe .. Spohr ndante, in F . see «- Mozart 
——a ist dein Nam’ . 5” Cherubini Gloria Patri(Service in A). eee aid .. Dr. Croft 
Benedictus, in G (First Mass) ae Introductory Voluntary, in c Seika .. Rink 
Around let acclamations ring (Athaliah) | -. Handel Andante. in-A-minor ey Mendelssohn 
“ey r se sas an is 
Benedictus, in A ion aati = Sea -+» Mozart ye. 4 God (Anthem) ... <i ee — aeons 
lagvednction.. ... — -- Goss Introduction and Air (Des Heilands Letze 
Fugue, in G re 7 sn ii ee ... Albrechtsberger Stund e) Spohr 
uoniam tu solus ... a es a = me 
; é ae a .. Righini Larghetto in A (Sinfonia i in 'D) 3. ... Beethoven 
Cum Saneto Spirits J Lae | Pergolesi Occasional Overture (1st two movements) .. Handel 
Leicht ist das Grab das Weltgericht ie ... Schneider Kyrie in G (Fugue from Man: 22. pear 
0. 10. Tie i ugu a Mass)... naa on 
The trumpet shall sound (Messiah) i ... Handel Slow Movement in C (from Op. 13) aH .. Beethoven 
Gloria in excelsis, in C (Second Mass) ... ... Mozart De torrente in via (Dixit Dominus) ie pr po 
With verdure clad (Crestion) im" ne ... Haydn March from the Occasional Oratorio ... Handel 
Fugue,inA ... e we aee.:~“Padre Martini Concluding Voluntary, in G (from a Fantasia) J. S. Bach 
Quoni — No. 11. . No. 23. 
uoniam, in E flat... Cry aloud and shout ° Dr. Croft 
} (Second Mass) . rr errs Slow Movement, in D fteoae a Sinfonia in G)... Haydn

Cum Sancto Spiritu 
I know that my Redeemer liveth (Messiah) -. Handel Blest are the departed (Last Judgment) -.- Spohr

