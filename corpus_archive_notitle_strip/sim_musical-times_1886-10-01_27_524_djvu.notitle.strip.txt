THURSDAY EVENING.—New Work, for Chorus and Orchestra AND

THE REVENGE, by C.V. Stanford; SYMPHONY IN C MINOR ‘ 53 Pr 
No.5 (Beethoven); THE FIRST WALPURGIS NIGHT (Mendcl< MESSE SOL E NNELLE 
ohn’. Principals: Miss DAMIAN, Mr. IVER McKAY, and

BRERETON. CH. GOU NOD

cluding Saturday nigh ) . +5 C I. H. fe iideaeai 
SINGLE TICKET.—Morning—Front Scats and Gallery

Reserved b¢ << — nD x’ 
i Evening—Front. ‘Seats and Gallery, BEETHOVEN’S CHOR. AL SYMPHON) 
Reserved = a a A —__———_ ? 
” ” Morning—Second Se ats , Reserv xi ae) TUESDAY E* VI NG, MARCH 1 
"s Evening—Second Seats, "Reserved... 7 The Sacred Oratorio

ets st re 
Morning Performances begi in at 12.30. C . I y set eS 
Eveniog Performances begin at 7.30 (Saturday Eveni 7). os ae ix ~ 
Applications must be accom panicd by a re emittanc the ful _ BY 
coor of the Tickets required. L. SPFOHR

following year he divided the 
lirst prize with another. He thus rapidly mounted to

the highest place, his playing being spoken of by 
his master, Marmontcel, in terms of special admi- 
ration. Bizet made equa! progress on ihe 
organ, under Benoit, winning the second prize 
in 1854, and carrying off the first in 1855. 
His organ studies hh brought him under the 
influence of Sebastian Bach, of whom he was a fervent 
and passionate disciple. For Beethoven, also, he 
cherished a profound admiration, but the older 
master was his special idol. He would often gather 
his friends around the pianoforte and play to them 
the Cantor’s fugues. At such times, it is said, 
had the air, with his sympathetic physiognomy 
charming profile, of a young pontiff initiating 
jaithful into the mysterious rites of some new cul: 
the cilt of Art, most 
religions

Zimmermann dying in 1853 (Gounod took his class 
during illness), Bizet pass sed under the instruction of

While engaged upon “ Les Pécheurs” Bizet’s 
name came twice or thrice before the public. The 
Sah rzo of his Suite was performed at the Cirque

Napolcon, under Pasdeloup (January 11, 1863), who 
ventured to associate his young countryman with 
Haydn, Mozart, and Beethoven, and was mercilessly 
baited for his pains. The audience hissed; the sub- 
the press criticised. Who was 
Bizet that he should be forced unon such illustrious 
company, and his music be thrust in where only 
classic art should reign? The same Scherzo was 
better received at a Concert of the Société Nationale 
des Beaux Arts, through whom, also, his ‘* Vasco de 
Gama” obtained a hearing, but concert-going Pari- 
sians were not, on the whole, favourable to the new 
man, the less because he was charged with a leaning 
to Wagnerism. M. Pigot makes a spirited defence 
of his hero against this accusation, claiming that he 
never ceased to be absolutely individual, that he be- 
longed to the great race of Weber, Mendelssohn

and their fellows, and had nothing in common with

It turned out to be a hoax, and was promptly contradicted by the great 
artist herself, who since then has, moreover, inherited a considerable 
fortune from a Piccolomini, a distant cousin

iso in the early 
development and subsequent cultivation of music has 
Siena taken a much larger and more important share 
than has hitherto been supposed. Native talent, 
native training, and the constant and invigorating 
influence of Cathedral services and popular testivals 
early formed a distinct school of which, as a modern 
artist, Marietta Piccolomini may be said to be pre- 
eminently representative. As far as composers, 
properly speaking, are concerned, that school, no 
doubt, produced neithera Bach, nor a Beethoven, nor 
a Cherubini; but it was a school thoroughly 
characteristic of Siena and the Sienese; in short, it 
was—as Lanzi says in speaking of Sicnese painting 
and sculpture—*a bright school grown up in the 
midst of an_ essentially and festive 
people.” C.F. S

say-he artes] 
say-heartea

Instruments, Les Musiciens), and fifth (a Critique, | C. V. Stanford—both for the first time in London

Le Symbolisme et la Philosophie de la musique au | and Liszt’s ‘13th Psalm.” : 
In the last chapter (A/1887, F. H. Cowen’s Cantata ‘‘ Sleeping Beauty” 
endeavours to}and Beethoven’s Choral Symphony

XIITe siécle) chapters

Handel’s ‘ Messiah” concluded the Cathedral per. 
formances on Friday morning, the principal singers being 
Madame Albani, Miss Anna Williams, Madame Patey, 
Miss Hilda Wilson, Mr. Winch, Mr. Watkin Mills, and 
Mr. Santley. Again the choir, in spite of the week’s hard 
work, gave the whole of the choruses with undiminished 
spirit, and Mr. C. L. Williams, as Conductor, strengthened 
the favourable impression he has so legitimately earned 
throughout the Festival

The free evening service in the Cathedral was very fully 
attended. When the clergy had taken their seats the 
orchestra played Spohr’s second Overture in the * Last 
Judgment.” The responses were again those of Tallis. 
The Magnificat and Nunc dimittis were composed for 
the occasion by Mr. C. L. Williams, both being written 
in the undying style of the best church composers, and in 
true sympathy with the sacred text. In Handel's sixth 
Chandos Anthem the bass solo was well sung by Mr. 
Watkin Mills, the service concluding with Beethoven's 
* Hallelujah ” Chorus, from the ‘* Mount of Olives,” which 
was given with impressive effect

We have already borne testimony to the admirable con- 
ductorship of Mr. C. L. Williams, and have now only to 
acknowledge the good service rendered by Dr. Langdon 
Colborne at the organ, in the morning Cathedral per- 
formances, by Mr. Done in the Cathedral, on Wednesday 
evening, and by Mr. C. H. Lloyd in the pianoforte 
accompaniments at the Secular Concert on Tuesday 
evening. The issue of cheap tickets has materially widened 
the feeling in favour of these Meetings; and we are glad 
to record that the number of persons attending the 
performances shows an increase over the last Gloucester 
Festival of more than 3,000. The collections amount to 
£532 12s. 2d. (£32 more than in 1883) which, with the 
contributions of the Stewards, make a total of £1,507 2s. 34. 
It will be thus seen that the present Festival, both artisti- 
cally and financially, has been a most gratifying success

prehensive view of the work, Iam bound to express a doubt 
whether it serves more than a limited and personal purpose. 
It cannot be said that music is the richer for it, and in that 
case, as far as music is concerned, it may just as well 
never have been written. ‘The time has come to discourage 
the composition, or, at any rate, the production of merely 
respectable works, which are nothing more than witnesses 
to musicianship. Such things are multiplying vastly, in 
response to an indiscriminating demand for novelties, and 
are diverting executive resources and public attention from 
matters much more profitable. I do not apply these remarks 
to Dr. Heap’s Cantata in a special degree, but there is no 
getting away from the fact that the composer has little 
to say that has not been said before, that he lacks inspiration 
and originality, and that—fatal fault—his art is unable to 
sustain interest through along work. The performance, con- 
ducted by the composer in person, enjoyed every advantag 
that circumstances permitted. The solos were taken by 
Mrs. Hutchinson (Elaine), Madame Trebelli (Guinevere), 
Mr. Lloyd (Lancelot), Mr. Grice (Sir Bernard), and Mr. 
Watkin Mills (Arthur). With these artists there was lack 
neither of skill nor good will. As for the band and chorus, 
it will be assumed that they were enthusiastic on behalf of a 
Conductor whom they justly esteem. A few miscellaneous 
selections brought the Concert to an end

The second day of the Festival opened with a Concert 
in which three classic works were performed, namely, 
Dvorak’s “ Stabat Mater,” Beethoven’s Symphony in C 
minor, and Mendelssohn’s *“* Lauda Sion.” There is, of 
course, nothing that need be said about these masterpieces, 
and their rendering may be briefly summarised. The solos 
in the “Stabat’’ were taken by Mrs. Hutchinson, who 
was her old self again, Madame Trebelli, Mr. Lloyd, and 
Mr. Watkin Mills; each doing justice to a sublime text, 
while, considering the difficulties of the music, it is possible 
to applaud the band and chorus for a plucky and fairly suc- 
cessful effort. The work they did sufficed, anvhow, to 
make a deep impression, which is proof that the music was 
not inadequately interpreted. In the “C minor” the 
orchestra played its very best, while the ‘* Lauda Sion’ 
came nearest of all to a satisfactory hearing. ‘The soloists 
in Mendelssohn’s piece, were again those mentioned above. 
Dr. Heap conducted throughout, and this is, perhaps, the 
moment to acknowledge the energy and success with 
which he presided over the general musical doings

The final Concert took place on Friday evening, when 
another novelty was produced—a Cantata, ‘* The Bridal of 
Triermain,” by Mr. Frederic Corder. Very much by Mr. 
Frederic Corder was this work, as given at Wolverhampton, 
for not only did the composer of the music adapt the words 
from Walter Scott, but wrote an analysis for the pro- 
gramme-book: and conducted the performance. He was 
the Atlas of the occasion; the whole world of the Cantata 
resting on his shoulders. Comprehensiveness of this sort 
runs a risk in proportion to its extent, but Mr. Corder did 
not over-estimate his powers. In all four capacities he 
acquitted himself well. ‘ The Bridal of Triermain ” wili 
assuredly be heard in London on no distant day, and when 
it forms his sole text the critic may hope to discuss it 
without restriction. Here I can only deal in generalities, 
which, however, will suffice to excite curiosity and interest 
in anticipation of closer acquaintance. Mr. Corder has 
wisely respected the poem whence he took his subject and 
his words. That is to say, there are no material altera- 
tions in the text, and the ** argument ”’ is as closely followed 
as the necessity of much abridgment allowed. The main 
incidents of the poem are, of course, in the Cantata. Hence 
we have Sir Roland’s dream, and the bard Lyulpi’s 
explanatory story in the first part, the second being taken 
up with the Knight's successful attempt to free Gyneth 
from the spell of Merlin’s enchantment. The whole, we 
need scarcely say, is given in narrative form—not the besi 
form by any means, but the composer has contrived to 
minimise this difficulty, and secure far more dramatic force 
and interest than, under such conditions, it was reasonable 
to expect. Let me confess that the musical handling of 
the subject came as a pleasant surprise. Mr. Corder is 
accounted a Wagnerian of the reddest hue—if the violent 
colour be that of the sect—and it was anticipated, not un 
naturally, that his Cantata would illustrate the teachings 
of “the master” in aspecial degree. it turns out that our

anc

tral Concerts under the 
umences in the Philhz 
vith such artists at the 
lame Norman-Néruda, and 
the entrepreneur himself, it is evider nt that the season will! 
be well inaug ed. The list of Symp! includ 
Beethoven’s ** Eroica,”” Me aesyre yhn’s A Italian at

rene 02's iy apniy oe "in C, in addition to a first pro 
uction, as Li ncerned, of Schuma 
in A major, Dvorak’s

The Bristol Musical Festival Society 
Concerts for November 26 and 27, when Berlioz’s “ Faust

256 
Beethoven’s ‘ Engedi,” and Rossini’s ‘“ Stabat Mater

together with miscellaneous items, will form the programmes. 
The last two works are old favourites with the Festival | 
| services, with band and augmented choir, should take place

the theatre were exhibited by Mr. Myles, who is by 1 ro- nmes of the ten Concerts before Christmas being as 
the 5 j yf

fession an architect. -At the first Concert Beethoven’s Symphony

mus 0. in C, Massenet’s Spanish Ballet from ‘* Le Cid

XUM

The ideal,” 
Concerto 
and vocalists, 
third 
Symphony, 
Tannhauser ” 
“ Coriolan”’ (Beethoven) and

Volckmann’s Violoncello Con- 
certo, to be played by Herr Julius Klengel; vocalist, Miss 
Ella Russell. The iourth Concert (November 6) wili be 
devoted to Dvorak’s Leeds Festival Oratorio, ‘“ Saint 
Ludmila,” the composer; principal vocalists

xcred T : The Childhood of C vill

be given (for the firs 1¢ at these Concerts), Miss Mary 
Davies, Mr. Henry Piercy, Mr. Robert H ton, and Mr. 
Santley, being the pri: neip al vocalists. At the seventh Con- 
cert (N laweniine 27)F. ‘ > mphon ic Fantasia” 
(first time), a selection from ickenzie’s Opera ‘* The 
Troubadour,” and Beethoven's Violin Concerto, performed 
by Pan F. Ondricek; vocalist, Mdlle. hatonnutti Trebelli

At the eighth Concert (December 4) Sir Arthur Sullivan’s 
Leeds Festival Cantata “The Golden Legend” will be 
given, conducted by the composer ; pare vocalists, 
Madame Albani, Madame Patey, Mr. Edward Lloyd, and 
Mr. Frederic King, the choruses by Novello’s Choir. At 
the ninth Concert (December 11), conducted by Sir Arthur 
; an, Overture * The Sapphire Necklace ” and incidental

ys 
violin

have been requested to publish the following. This 
yn is most important for piano-dealers who send out 
their goods on this system :—* Birmingham Police Court, 
September 14, 1586..—-The Pianoforte Case.—Dale, Forty | 
and Co.,of Cheltenham, re Brawn. Before Mr. Kinnersley, 
stipendiary magistrate. This was an application by Mr. 
C. J. Chesshyre, of Cheltenham, solicitor, on behalf of 
Messrs. Dale, Forty and Co., for an order of restitution of | 
the piano (for the stealing of which the prisoner Brawn | 
had been convicted), under the 24th and 25th Victoria, 
c.g6, sec. 100, which piano was, before conviction, bond fide | 
purchased by one Stephen Ainge, of Birmingham. Mr. 
Hebbert, jun., appeared for Mr. Ainge. Order to restore | 
the piano to Messrs. Dale, Forty and Co. was at once made 
by the learned mavistrate.” 
The prospectus of the Sacred Harmonic Society announces | 
that five Concerts will be given during the coming season 
as follows: December 3, ** Judas Maccabzeus,’’? December 
17, The Messiah,” January 2i, 1887, * Elijah,” February 
25, Rossini’s ‘* Moses in Egypt,’ and March 25, Costa’s 
“Eli.” Engagements have already been made with many | 
of the most eminent solo vocalists. Mr. W. H. Cummings 
retains the post of Conductor, and Mr. Fountain Meen is 
Organist. The council in the prospectus make an earnest 
appeal to all friends of the Society to use their utmost 
endeavours to extend the number of subscribers, and 
draw attention to the fact that in reducing the number of 
Concerts to five they have considered what they feel to be 
the best interests of the Society. | 
Tue Highbury Philharmonic Society promises for the en- | 
suing season four Concerts, the first of which will take | 
place on November 29, when ‘“ Judas Maccabeeus ”’ will be | 
given. At the second Concert, on January 31, 1887, Ran- 
degger’s * Fridolin’? and a miscellaneous selection. 
March 21, Mendelssohn’s Italian Symphony, Bach’s 
wrestle and pray,” Beethoven’s ‘A calm sea and a pros- 
perous voyage,” and a selection from “ William Tell

and at the last Concert, on May 16, Dvorak’s ‘ Stabat

A series of weekly Organ Recitals has been instituted 
at St. John’s, Waterloo Road, on Tuesday evenings, those 
during the past month having been given by Mr. Henry 
J. B. Dart. All the Recitals have been largely attended, 
and the programmes have included compositions by Bach

Handel, Mendelssohn, Meyerbeer, Beethoven. Rubinstein, 
On | Guilmant, Smart, Hopkins, and Stainer. It is contem

oe

THE MUSICAL TIMES.—Ocropser 1, 1886

Songs. Composed by L. Van Beethoven. Vol. I. The 
English version by the Rev. Dr. Troutbeck. 
| Novello, Ewer and Co.} 
No more effectual protest against the meretricious vocal 
works of the day can be devised than the publication, in a 
cheap form, of this volume of songs. We should, of course

not think of enlarging upon the exceptional beauties of

Dr. Hans Von Biilow will give a series of four "Con reerts

devoted to Beethoven’s pianoforte works, 
chronological order, in the leading German tow 
winter season

Mr. Goring Thomas’s opera ‘* Esmeralda,” having met 
with so favourable a reception last year both at Hamburg 
and Cologne, the same composer’s ** Nadeshda” has been 
accepted at the Stadt-Theater of the latter town for per- 
formance during the coming winter

doubt that many other more exacting vocal and instrumen- | 
- nb

tal, choral and orchestral works, will be found palatable. 
As to the above-mentioned songs and pianoforte pieces, 
even the most classical of the classicists, unless he be at 
the same time an out and out pedant, cannot but admit 
their beauty. And though he may at first be taken aback 
by the divergence of forms in the symphonic poems, he 
must before long recognise, besides other excellences, their 
easy intelligibility. Several of Liszt's Rhapsodies have 
become great favourites with the public. If they 
are sometimes treated by critics de haut en bas, 
I think the view taken is a mistaken one. They 
ought not to be compared with Beethoven's sym- 
phonies and similar works. They are something very 
different, something sui generis, and something well worth 
having. Even were they nothing more than mere jeux 
d’esprit—and they are more—they would have a raison 
Wétre. But enough of this

A friend of mine has accused me of having made a 
scornful allusion to Berlioz, of having glorified Liszt at his 
expense. Iam unable to see how this is borne out by my 
words. Certainly nothing was further from my mind than 
the intention of saying anything in disparagement of the 
great French master. I wished only to point out that, in 
my opinion, the totality of Liszt’s works which are likely 
to live would turn out to be more valuable (considered 
quantitatively as well as qualitatively) than that of Berlioz’s. 
1 shall not quarrel with anyone whose calculation differs 
from mine. That I selected the long-neglected and only 
lately-appreciated French master for comparison can hardly 
surprise, seeing that his and his contemporary’s fortunes as 
composers offer some points of resemblance. And the 
great number of diverse works composed by Liszt— 
Berlioz’s are few and less diverse in kind—seems to me to 
justify my supposition

THE MUSICAL TIMES.-—Ocroper 1, 1886

THE HIGHBURY 
SOCIETY. 
NintH SEASON, 
Conductor, Mr. GILBERT H. BETJEMANN, 
Assistant Conductor, Mr. DAVID BEARDWELL. 
FOUR CONCERTS will be given at the Highbury Athenzum: 
November 29, 1886, Handel’s JUDAS MACCABA:US; January 31 
i887, Randegger’s FRIDOLIN, and a Miscellaneous Selection; 
March 21, Mendelssohn’s ITALIAN SY MPHONY, 
WRESTLE AND PRAY. Beethoven's A CAL} 
PROSPEROUS VOYAGE, and a Selection from 
WILLIAM TELL, &c.; May 16, Dvorik’s STABAT MATER and 
Mendelssohn’s W ALPU RGIS NIGHT

a: mbered and Reserved Sea xts for the Four

