extremely rare specimen Sebastian 
 Bach extraordinary musical genius , Editor 
 indebted obliging politeness kind friend 
 Mendelssohn Bartholdy , frequently played 
 , memory , time 
 copy manuscript obtained England , 
 visit Germany year ( 1833 ) , 
 Mr. Mendelssohn kind procure copy , 
 obligingly allowed transcript iito 
 Editor work , 
 expressed admiration composition . 
 writer present note gladly avails 
 opportunity expressing best acknowledgments 
 gentleman considers greatest 
 ornaments musical art present age , 
 gratifying proofs 
 liberal friendly sentiments 

 important Beethoven event took place 
 London Christmas Eve , 1832,—before 
 Christmas card microbe germinated — 

 Pe ee ee 

 great Mass D performed 
 England . home zealous amateur , 
 Mr. Thomas Massa Alsage , Queen Square , 
 Bloomsbury , assembled 
 band chorus , conducted Moscheles . 
 Novello played organ , daughter son 
 ( Clara Alfred ) soprano bass 
 soloists respectively , daughter son - - law 
 ( Mr. Mrs. Cowden - Clarke ) sang 
 chorus , son Edward Novello , 
 painter . copy programme 
 interesting event preserved Fitzwilliam 
 Museum , Cambridge 

 days Beethoven performance 
 Choral Harmonists Society sprang 
 existence . association amateurs , devoted 
 performance important choral works 
 orchestral accompaniment , held 
 meeting New London Tavern , Bridge 
 Street , Blackfriars , January 2 , 1833 . Novello , 
 pioneers Society , shared 
 duties conductor Charles Lucas , Charles 
 Neate , Henry Westrop . 
 complete set programmes , word - books , & c. , 
 Society ( 1833 - 1851 ) , presented leader , 
 late Mr. J. H. B. Dando , 
 possession present writer , 
 able form opinion energy 
 enthusiasm London amateurs seventy years 
 ago , pass judgment 
 excellence music performed . 
 instance , Purcell ‘ King Arthur ’ given 
 London Tavern ( place concerts 
 removed ) June 26 , 1835 . social 
 element appears feature 
 pleasant music - makings , find injunc- 
 tion printed programmes 

 Committee request Members Visitors 

 resume seats tea occupied 
 performance 

 eclectic nature music performed 
 added greatly enjoyment 
 meetings . Madrigals , Oratorios , Masses , in- 
 cluding Credo Bach B minor Mass 
 ( 1838 ) , & c. , sung , Overtures played , 
 - day , 1838 , performed 
 ‘ Grand Concerto E flat ( Piano - forte obbligato ) 
 Beethoven . ’ ‘ piano - forte obbligato ’ amusing . 
 setting word - book concert 
 printer uses figure 6 flat sign ( p ) — 
 ‘ Grand Concerto ... ( E6 ) ... Beethoven . ’ 
 February 25 , 1839 , Choral Harmonists 
 gave public performance Beethoven 
 Mass D country . 
 hasten rapidly survey remaining years 
 Vincent Novello career 

 1834 family removed 69 , Dean Street , 
 thirty - years — till removal 
 Berners Street 1867 — business 
 Novello firm rapidly developed . Phil- 
 harmonic concert March 17 , 1834 , dramatic 
 cantata , ‘ Rosalba , ’ voices , composed 
 expressly Society Vincent Novello , 
 performed . Later year officiated 
 organists Royal Musical 

 biographers . living 
 years Craven Hill , Bayswater , Novello , 
 owing delicate health wife , removed 
 Nice , resided 1849 1854 . 
 death Mrs. Novello 
 ( July 25 , 1854 ) returned England , 
 remained London years , 
 27 , Porchester Terrace , Bayswater . 1856 
 sought genial climate Italy , 
 died Nice August 9 , 1861 , month 
 eightieth birthday 

 word ‘ industry ’ writ large 
 estimating life - work Vincent Novello . 
 editing achievements remarkable , 
 especially taking consideration constant 
 claims busy teacher music 
 regular engagements fulfilled . 
 grudge credit 
 making known strains music 
 remained hidden 
 unpublished , day . 
 principal publications mentioned 
 course sketch . 
 added editing eighteen Masses Mozart 
 sixteen Haydn , 
 printed 
 time ; Psalmist , large collection Hymn- 
 tunes ( 1836 ) ; Croft Greene Anthems 
 ( vols . ) ; Boyce Anthems ( vols . ) ; 
 Masses Beethoven , large number 
 Oratorios Handel composers , & c. 
 Keenly interested literary art , 
 assiduous collector manuscripts , 
 presented British 
 Museum departure Italy 1849 . 
 presented Museum large collec- 
 tion letters , , example , written 
 master epistolary humour , 
 Samuel Wesley , special interest 

 personality Vincent ’ Novello 
 given words eldest daughter , 
 late Mrs. Cowden - Clarke , records 

 Performances ‘ Dream Gerontius ’ 
 taken place past month Sheffield 
 ( conductor , Dr. Henry Coward ) , Manchester 
 ( Dr. Richter ) , New York . ticket 
 sold long concerts took place 
 cities , Oratorio received 
 extraordinary enthusiasm . Manchester 
 composer , present interpretation 
 work , received overwhelming ovation 

 view house — indicated flag — 
 Beethoven drew breath . 
 historical habitation soon thing past , 
 following account closing scene , 
 Atheneum atst ult . , read interest 

 houses great men born died 
 naturally held special veneration . , like 
 things human , pass away time . 
 remarkable house lately doomed demolition , 
 time lines published 
 partly entirely ceased exist . 
 Schwarzspanierhaus Vienna Beethoven 
 breathed March 26th , 1827 . Sunday , 
 November 15th — seventy - eighth anniversary , 
 way , death brother , Caspar Carl , 
 son cause anxiety 
 sorrow composer — gathering 

 HOUSE BEETHOVEN DIED 

 WINDOW ROOM INDICATED FLAG 

 rooms occupied Beethoven autumn 
 1825 death . Dr. Lueger , burgomaster 
 city Vienna , prevented indisposition 
 present , represented 
 Dr. Neumayr , delivered address . Herr 
 Reimers , Hofburg Theatre , recited poem 
 specially written occasion . 
 impressive ceremony performance . 
 said admirable , Prill 
 quartet players Beethoven Quartet F , 
 Op . 135 , actual recom master 
 died . occupied rooms house , 
 , numerous guests 
 assembled , , , work 
 composed . autograph movement 
 ( Allegretto ) belongs Dr. Heinrich Steger , 
 present , brought . move- 
 ment bore superscription “ Muss es sein ? Es 
 muss sein , ” words explanation 

 792 

 MUSICAL TIMES.—Decemper 1 , 1903 

 given . sketch wonderfully 
 pathetic peaceful /ento marked “ Siisser Ruhe 
 Gesang . ” Beethoven bad health 
 time wrote quartet , possible 
 interpretation ‘ “ Muss es sein ? Es muss sein ” 
 contemporaries Beethoven , allusion 
 death , correct . , “ sweet rest ” 
 thoughts grave , 
 long painful experience knew 
 longer hope earth 

 sequel history Hovingham 
 Musical Festival given issue ( p. 739 ) , 
 append list principal works performed 
 Yorkshire music - making inception 1887 
 present time . Accompanying list — 
 reason space reduced longer document — 
 photograph enthusiastic Founder 

 793 

 historic house Broadwood 
 remove Great Pulteney Street Conduit 
 Street . year 1732 founder 
 business , Burkardt Tschudi , maker harpsi- 
 chords , opened premises forsaken , 
 location traditions seventeen years 
 uprooted . John Broadwood , young 
 Scot , born Cockburn Path , village 
 Berwickshire coast , came London , Scotchmen 
 , seek fortune . entered factory 
 Tschudi , daughter , 
 matrimonial process Barbara Tschudi evolved 
 Barbara Broadwood . 1783 John Broadwood 
 sole proprietor , Tschudi 
 ( Shudi , Anglicised form ) dropped 
 title firm . Tschudi , intro- 
 duction Handel , supplied double harpsichord 
 Prince Wales , harpsichord 
 maker privileged use Prince Wales 
 crest sign Great Pulteney Street house , 
 known ‘ Plume Feathers ’ 
 time , 1767 , houses began num- 
 bered London . think great masters 
 music passed portal 
 old house Great Pulteney Street ! Let recall 
 names : Handel , Mozart ( boy 
 years old ) , Haydn ( said composed ) , 
 Clementi , Hummel , Mendelssohn , Liszt , Chopin . 
 Beethoven greatly treasure pianoforte 
 presented , 1818 , Mr. Thomas Broadwood ? 
 Beethoven allow touch 
 instrument Stumpf , trusted tuner . 
 acoustic apparatus attached 
 Broadwood pianoforte order hear 
 sounds brought forth touch 
 genius hand . deafness Beethoven 
 pathetic things music history . 
 Knowing , help feeling sense 
 regret change ; 
 sentiment way business . old 
 associations , , remain 
 interesting memories , Broadwood House 
 - wishers continued prosperity 
 new surroundings . , conclusion , 
 refer Punch drawing — inimitable 
 Charles Keene ? — depicted landlady 
 lodging - house showing rooms prospective 
 occupant . ‘ , ’ said lady enquiring 
 apartments , ‘ havearosewood piano . ’ ‘ , 
 mum , ’ indignantly replied landlady , ‘ 
 Broadwood 

 Dr. Elgar ‘ Dream Gerontius ’ produced 
 Darmstadt October 19 local Musikverein , 
 direction Hofkapellmeister Willem de 
 Haan , Fraulein Bengell ( mezzo- 
 soprano ) , Herr Oskar Noé ( tenor ) , Herr 
 Alexander Heinemann ( bass ) soloists . 
 Darmstidter Zeitung course long notice 
 concert says 

 QUEEN HALL SYMPHONY CONCERTS 

 firmly established widely appreciated 
 Queen Hall Symphony Concerts emphatically 
 shown large attendance performance 
 present series October 31 , 
 second concert 14th ult . occasion 
 Symphony E minor Brahms , 
 received fine interpretation Mr. Henry J. 
 Wood direction . Miss Adela Verne gave legitimate 
 vivacious rendering solo Schumann 
 Pianoforte Concerto , performance London 
 given Mr. Arthur Hervey tone poems ‘ 
 heights ’ ‘ march , ’ written , produced 
 , Cardiff Musical Festival year . concert 
 14th ult . chiefly memorable purity , 
 expressiveness , brilliancy Herr Kreisler violin 
 playing Brahms Concerto D , rarely 
 received fine interpretation . Mozart delightful 
 Symphony E flat beautifully played , 
 Beethoven ‘ Coriolan ’ Overture completed 
 ideal programme 

 ALEXANDRA PALACE CHORAL SOCIETY . 
 PERFORMANCE PARRY ‘ JUDITH 

 gradually custom open musical 
 season October . Concert Society , 
 improve public taste , commenced 
 popular Sunday Thursday concerts , 
 classical music performed , 
 greater attendance ; shows 
 concerts supply generally wanted . 
 symphonic concerts ( Tuesdays , 
 Wednesdays ) given Society special import- 
 ance . novelties given mentioned 
 symphonic poem ‘ L’apprenti sorcier , ’ Dukas , 
 Elgar ‘ Cockaigne ’ Overture . work , spite 
 excellent technical qualities , objective 
 subjective ; suggest true deep content 
 Goethe poem . Elgar Overture impresses 
 brilliancy , impression created deep 
 produced year ‘ Orchestral Variations 

 Philharmonic concerts unpleasant 
 experience . Shortly concerts commenced , 
 Hellmesberger , conductor , resigned post , 
 management left lurch . 
 committee sought services foreign conductors , 
 Ernst von Schuch , achieved great 
 success Haydn - Mozart - Beethoven programme . 
 conductor Sofonoff , director 
 Moscow Conservatoire 

 ‘ Vienna Capella Choir ’ given programme 
 old music — Bach motet , works Josquin de Prés , 
 Orlando Lasso , & c.—under direction diligent 
 conductor , Eugen Thomas 

 5th ult . Mr. Max Mossel began new series 
 drawing - room concerts Grosvenor Rooms . 
 M. Zacharewitsch , violinist new Birmingham , 
 heard pieces Spohr , Tschaikovsky Sarasate , 
 M. Benno Schonberger gave refined rendering 

 Beethoven Sonata C minor ( Op . 30 , . 2 ) .. 
 Pianist principal solo Schumann ‘ Etudes 
 Symphoniques 

 Mr. William Sewell Male - Voice Choir Ladies ’ 
 Choir gave successful concert Town Hall 
 October 24 ; week later Mr. Joseph H. Adams 
 conducted performance cantata ‘ Song 
 Thanksgiving , ’ produced 1899 , 
 7th ult . Midland Musical Society , conducted 
 Mr. A. J. Cotton , revived Rossini ‘ Stabat Mater , ’ 
 bracketed Coleridge - Taylor ‘ Hiawatha 
 Wedding - Feast 

 MUSIC BRISTOL . 
 ( CORRESPONDENT 

 Meetings held Imperial Hotel jp 
 order form musical club professional amatey ; 
 musicians . leading professionals city 
 members , arranged , jp 
 addition social intercourse , shall periodical 
 performances music . meeting held 
 roth ult . String Quartet Beethoven Dvorak ’ ; 
 Pianoforte Quintet given 

 oth ult . concert given choir 
 Eastville Chapel , Mr. George Riseley organ , 
 careful direction Mr. F. Stone , anthems 
 choruses rendered , solos contributed 
 Miss Amy Perry Miss Maude England 

 Dublin Orchestral Society reconstituted , J 

 bids fair successful organizatiot . 
 large number annual subscribers secured 
 annual donors sums . fits 
 orchestral concert given 18th ub 
 Special interest attached performance tht 
 ‘ Burleske ’ pianoforte orchestra Richard Strauss , 
 time orchestral work 
 composer played Dublin . Mr. Archy 
 Rosenthal pianist acquitted 
 difficult solo . Beethoven Symphony 
 ( movements ) , Notturno Scherzo 
 Mendelssohn ‘ Midsummer Night Dream ’ music , até 
 ‘ Meistersinger ’ Overture completed programme . 
 attendance good , promises 
 success Society season commence 

 Mr. RK 
 attracted 
 proved 
 step : 
 progr 
 Ha : 
 Granville 
 perf 
 local pia 
 exce 
 finished 
 comp 
 accompa 

 MUSIC GLASGOW . 
 ( CORRESPONDENT 

 Mr. August Hyllested , recently - appointed principal 
 professor pianoforte Athenaeum School Music , 
 gavea successful recital 3rd ult . Mr. Hyllested , 
 studied Liszt , achieved distinction solo 
 pianist Continent America , initial 
 performance city fully justified high reputation . 
 programme included somewhat stereotyped pieces 
 Bach , Beethoven , Schumann , Chopin Liszt , 
 Chopin numbers Mr. Hyllested probably 
 best . Mrs. Hyllested fine singing songs Gluck , 
 Schumann Brahms added greatly evening 
 enjoyment . enjoyable concert given 
 t2th ult . Madame Clara Butt Mr. Kennerley 
 Rumford , assisted - rate instrumental soloists . 
 addition - known items , programme 
 included vocal novelties , best 
 Mr. W. H. Squire duet ‘ Harbour Lights 

 careful direction Mr. J. K. Findlay , 
 choir St. John United Free Church performed 
 Handel ‘ Samson ' 18th ult . lacking 
 somewhat volume tone , choruses sung 
 commendable accuracy steadiness , solo 
 Music received adequate interpretation Misses 
 Dixon Dykes Messrs. Adams Bain . small 
 string band , ably supplemented Mr. Thomas Berry 
 organ , gave accompaniments effectively 

 Gentlemen Concerts indicated 
 committee present suffering great 4 
 sense responsibility representing oldest concert 
 institution kingdom . minds 
 running obsolete obsolescent , 
 concerts interesting usual . best 
 features ‘ Oberon ’ Overture singing 
 Miss Agnes Nicholls , gave beautiful Micaela air 
 success Leeds years ago . 
 acoustics new Midland Hall proved deplorable 
 orchestral choral music . day October 
 Mr. Brand Lane gave concert season 
 Philharmonic Choir , madrigal - singing showed 
 improvement delicacy good tone singing 
 forte , long array London stars . ‘ Elijah ’ 
 performance fortnight later packed Free Trade 
 Hall imposing mass humanity . soloists 
 Madame Albani , Mr. Santley , Mr. Ben Davies , 
 young local singer ( pupil Mr. Lane ) named 
 Dora , fairly successful début 
 contralto soloist , showing self - possession 
 novices , generally , certain 
 harshness register — D F nearest 
 middle C 

 concert Brodsky 
 Quartet , , held 4th ult . , 
 interesting masterly rendering 
 Quartets Haydn Schubert , D minor , 
 popular Schubert 
 Quartets , ‘ Death Maiden ’ variations , 
 opportunity Manchester hearing 
 Mr. Arthur Friedheim , new professor pianoforte 
 Royal Manchester College . association 
 Dr. Brodsky Mr. Fuchs , Mr. Friedheim gave sound 
 level - headed performance Beethoven B flat Trio 
 — later , course ( Op . 97 ) . Quartet 
 Schubert played following Saturday 
 Schiller concert Verbrugghen Quartet , 
 rendering plenty technical 
 power , display repose . 
 Mr. Isidore Cohn pianoforte , Mr. 
 Verbrugghen gave concert enjoyable 
 rendering ‘ Rondo Brillante ’ Schubert,—formerly 
 popular , neglected — Mr. Kenneth 
 Carne Ross ( baritone ) sang effectively songs 
 Alessandro Scarlatti , Elgar , Reynaldo Hahn . 
 concert ended fearful wonderful Septet 
 Saint - Saéns pianoforte , trumpet , violins , viola , 
 violoncello double - bass , curious 
 extreme case incongruous styles 
 incongruous tone - values offered con- 
 tribution musical art . interesting pianoforte 
 vocal recital given r4th ult . , course 
 vocal pieces instrumental , 
 Graham Peel , introduced , giving impression 
 young composer pretty small talent 

 performance Preston Choral Society 
 4th ult . Sullivan ‘ Golden Legend ’ Elgar 
 ‘ Coronation Ode ’ interesting showing 
 progress Society conductorship 
 Dr. Coward , Sheffield , appointed 
 year ago . excellent work Southpott 
 Orchestral Society , conducted Mr. R. H. Aldridge , 
 exemplified 2oth ult . , orchestra 
 containing - cent . amateur 
 thirty - cent . professional talent gave enjoy- 
 able rendering Dvorak ‘ New World ’ Symphony , 
 Weber ‘ Oberon ’ Overture , Liszt Rhapsody , . 2—- 
 nearly identical . 12 Pianoforte Series 
 — pieces , audience large 
 enthusiastic 

 MUSIC SHEFFIELD DISTRICT . 
 ( CORRESPONDENT 

 important musical developments taken place 
 past month . establishment 
 representative influentially - supported Society 
 devoted cultivation chamber music . 
 Sheffield Chamber Music Society outcome 
 movement started months ago provide local 
 lovers chamber music opportunities 
 hearing best class music performed 
 leading players . scheme season consists 
 concerts , instrumental parties 
 engaged led Mr. Kruse , Dr. Brodsky 
 Mr. Josef Holbrooke . concerts 
 programmes performed local musicians . 
 membership Society , numbering 150 music - lovers , 
 speedily filled , concert given 
 roth ult . Kruse Quartet ( Messrs. Kruse , Haydn 
 Inwards , Alfred Hobday Percy ) gave fine 
 performance Brahms C minor Quartet ( Op . 51 ) , 
 Beethoven B flat ( Op . 18 , . 6 ) . Professor 
 Kruse played Adagio Spohr Ninth Violin 
 Concerto , Mr. Percy contributed move- 
 ments Boccherini Sixth Sonata accompani- 
 ment Mr. J. A. Rodgers 

 event alluded acceptance behalf 
 Sheffield University committee offer 
 Mr. Charles Manners provide festival week high- 
 class opera bare expenses , proceeds devoted 
 University building fund . Moody - Manners 
 Company giving successful series per- 
 formances , offer arose consequence 
 suggestions local newspaper respecting 
 company repertoire visit . proposal 
 warmly taken city . forces 
 largest Mr. Manners ’ touring companies 
 combined , Wagnerian operas figure prominently 
 scheme 

 interest Municipal Orchestra concert , 
 mentioned month ago , fully sustained 
 second concert 7th ult . , Mr. William 
 Wallace appeared conduct couple compositions , 
 cycle ‘ Sea - Songs , ’ ably sung Mr. Dan 
 Billington , owing little effectiveness 
 careful playing picturesque orchestral accompani- 
 ment . capital piece musical humour , 
 setting Bon Gaultier ‘ Massacre Macpherson ’ 
 male - voice choir ( Leeds Musical Union ) 
 orchestra . burlesque character music , 
 comical appropriate plagiarisms , exact keeping 
 amusing verse , , spite spiritless 
 performance , effective . novelty wasaset 
 pretty dances orchestra , entitled ‘ Faerie 
 Suite , ’ Mr. Bernard Johnson . Mr. Fricker conducted 
 Schubert ‘ Unfinished ’ Symphony pieces 
 marked ability , concert exceedingly 
 enjoyable —— roth ult . , Miss Clara Winder , 
 young Leeds soprano , gave concert , 
 good impression agreeable singing . — — 
 second concert Philharmonic Sub- 
 scription series 17th ult . , Mr. H. J. Wood 
 Queen Hall Orchestra appearance 
 Leeds , excited great interest enthusiasm 
 brilliant performance programme 
 important feature Tschaikovsky Fifth 
 Symphony . Richard Strauss ‘ Sturmlied ’ sung 
 admirable refinement Philharmonic chorus , 
 justice rhythmic subtleties nuances 
 expression . 
 BRADFORD 

 October 30 Bradford season said 
 begun Subscription series 
 concerts . Dr. Richter conducted brilliant performance 
 Berlioz ‘ Harold Italy , ’ feature 
 concert linger longest memory 
 superb playing Beethoven Violin Concerto 
 Mr. Kreisler , triumphant début West 
 Riding occasion . Tartini ‘ Trillo 
 del diavolo ’ showed great interpreter 
 brilliant virtuoso , musicianship 
 , addition , displayed admirable cadenzas 
 written works . Like D’Albert 
 pianoforte , forget executant 
 think Beethoven , surely praise high 
 bestowed interpretative artist : 
 following day Permanent Orchestra began opera- 
 tions season programme 
 noteworthy feature Mackenzie Scottish 
 Concerto , movements played . 
 soloist Miss Ethel Bird , careful player , 
 forceful . Mr. Allen Gill conducted . 
 oth ult . Miss Marie Lummert , assisted Mr. Ellenberger , 
 ( violin ) , Mr. Carl Henrich ( pianoforte ) , gave vocal 
 recital , programme showing breadth excel- 
 lence taste musicianship . 
 roth ult . Old Choral Society , Mr. J. W. Fitton , 
 ave interesting performance Gluck ‘ Orpheus , ’ 
 Miss G. Lonsdale chief rdle , Miss Ada 
 Beecroft Miss Bradbury parts . 
 Beethoven Choral Fantasia solo efficiently 
 played Mr. E. J. Pickles 

 YORKSHIRE TOWNS 

 ARNHEIM 

 musical festival held October 17 18 
 direction Martin S. Heuckeroth . 
 works performed Bach ‘ God Lord 
 sun shield , ’ Handel ‘ Ode St. Cecilia , ’ 
 Beethoven ‘ Choral ’ Symphony , Mahler 
 Symphony , , way novelty , Jan von Gilse 
 ‘ Sulamith ’ soli , chorus , orchestra 

 BERLIN 

 Professor Siemering Haydn - Mozart - Beethoven 
 monument erected near Gold Fish Pond 
 Thiergarten unveiled 

 BRUNSWICK 

 festival concert recently given Musical 
 Society honour Willem de Haan , 
 conductor - years . programme 
 included choral ballad ‘ Arpa , ’ Dr. Elgar 
 ‘ Dream Gerontius ’ solo vocalists Mesdames 
 Bengel Minna Obsner , Messrs. Oscar Noé 
 Alexander Heinemann 

 HEIDELBERG . 
 - days ’ Festival ( October 24 - 26 ) held 
 new Town Hall direction Dr. Wolfrum , 
 day programme commenced 
 Bach Organ Fugue E flat , performed Dr. Wolfrum 
 new powerful organ , included 
 works Liszt ‘ Dante ’ Symphony 
 Richard Strauss ‘ Tod und Verklarung . ’ second 
 concert devoted chamber music . Petrj 
 Quartet Dresden played quartets Mozart 
 Beethoven , Dr. Wolfrum Professor Julius Buths 
 Diisseldorf performed Rheinberger transcription 
 pianofortes Bach ‘ Goldberg ’ Variations , 
 works mentioned Dr. Wolfrum 
 ‘ Festmusik zur Zentenarfeier der Universitat Heidel- 
 berg ’ ; Bruckner Ninth Symphony D minor , 
 ‘ Te Deum ’ finale , Richard Strauss 
 conductor ; composer new work ‘ Taillefer ’ 
 soli , chorus orchestra 

 HELSINGFORS 

 BRIEFLY SUMMARIZED 

 Bo.ton.—The Philharmonic Society opened season 
 11th ult . Victoria Hall miscellaneous 
 programme including good performances Haydn 
 Symphony . 7 , C , Gavotte ‘ Mignon ’ 
 ( Ambroise Thomas ) . - songs ‘ Waterlilies ’ ( Sachs ) 
 ‘ love dwelt ’ ( Elgar ) - named composer 
 ‘ Scenes Bavarian Highlands ’ sung 
 choir admirable effect . Miss Edith Robinson 
 gave successful interpretation Beethoven Violin 
 Concerto , vocalist Miss Lillie Wormald . 
 Mr. C. Risegari skilful conductor 

 DarLaston.—The Choral Society , conducted 

 
 VIOLIN ( Position ) PIANOFORTE 

 L. van BEETHOVEN 

 Price Shilling Sixpence 

