string , and double wind , have bee 

 BEETHOVEN ’S gtn ( Cuorar ) SY pg 
 o 

 together , side by side ; 
 increased Orchestra of 130 
 artist : Miss ANNA WILLIAMS , Mi 
 Miss HILDA WILSON , Mr. IVER McKAY , 
 MILLS . organist , Mr. HODGE . Prices Sta ll , ros . 6d . ; Arena 
 -s . ; Balcony ( reserved ) , 5 . ; unreserved , 4 

 Tvuespay Mor " ELIJAH . " 
 TuEspDAY evening 

 DR . A. C. MACKENZIE 'S " VENI , CREATOR SPIRITUS ’ 
 BEETHOVEN 'S VIOLIN CONCERTO ( Dr. Joacuim ) 
 BRAHMS 's third symphony , & c 

 WEDNESDAY morning . 
 ST . MATTHEW ” passion music . 
 WEDNESDAY evening . 
 STANFORD 's new dramatic oratorio " EDEN , " & c. 
 THURSDAY Morninc . — MESSIAH . ' 
 THURSDAY evening . 
 PARRY ’ 's " BLEST PAIR of SIRENS . " 
 Solo Violin , Dr. JOACHIM , & c. 
 Fripay MorNING 
 DVORAK ’s new RE qu IE 
 BEETHOVEN 's SEVENTH symphony 

 Fripay Evenina . 
 BERLIOZ ’S " faust 

 INGING.—A gentleman ( light baritone ) will give 
 his service at charitable Concerts , & c. address , M. A. S. , care 
 of Mrs. Matthews , 329 , High Road , Kilburn , N.W 

 R. WILLIAM BENNETT ( Basso Cantante ) . 
 4 for Oratorios , Ballads , & c. , address , 61 , Wynn St. , Birmingham . 
 " Messtau . " ( Birm . Town Hall ) . — " the bass air , ' why do the nation , ' 
 receive a splendid exposition " Birmingham Daily Gazette , March 
 31 , 1891 . engage : Madeley , October 19 ; Birmingham Town Hall 
 ) , October 20 , 21 , 22 , 23 , and ( Beethoven 's mass ) Nov. 14 

 N R. S. J. BISHOP ( Solo Bass , Exeter Cathedral ) 
 a be now book engagement for Season 1891 - 2 . press notice 
 or , Cathedral , Exeter 

 name of a few of the great musician and executant 

 who have use and prefer their Pianos : — 
 BEETHOVEN HAYDN R. SCHUMANN 
 | WE BER CHOPIN WAGNER 
 |ME NDELSSOHN — LISZT VON BULOW 
 | DVORAK HELLER DE PACHMANN 
 | CRAMER HENSELT HALLE 
 | BENNETT Mpme . SCHUMANN : SULLIVAN 

 price list and illustration on application , 
 33 , GREAT PULTENEY STREET ( near Regent Street ) , 
 LONDON , W 

 the story have now be tell . in lay down the pen 
 there be sadness in the thought that so soon after the 
 strain of * Elijah " have die away in Exeter Hall — in 
 a little more than six month — the genius - brain that 
 have conceive that glorious work be for ever calm 
 in death . no more fitting conclusion could be find 
 for this record than that supply by Jenny Lind , who , 
 in write to the composer ’s widow on her irreparable 
 loss , say : ' ¢ his ' Elijah ' be sublime ! 
 he never write anything fine ; and assuredly could 
 not have write anything lofty in the future ! with 
 what solemnity we all stand there ( to perform it ) ; and 
 with what love do the people still speak of he ! — how 
 the good English have understand and absorb this 
 particular music ! 
 special mood ! " ' to this tribute of one great artist 
 to the memory of another we would subscribe a fervent 
 " amen 

 att educated musician know that the title 
 * * Moonlight , " as apply to Beethoven 's Pianoforte 
 Sonata in C sharp minor , be all moonshine so far as 
 the composer be concern . an equally absurd 

 false title have be give in England to Mendelssohn 's 

 Mr. Wiegand , as a new comer , may perhaps be 

 excused if , up to the present , he have adopt the 
 european estimate . he may , however , be inform 
 that the public of Sydney have be teach to appre- 
 ciate and to reverence music ’s master and master- 
 piece , and that we neither desire nor approve the 
 mutilation of great work . if he , personally , do 
 not share that feeling of reverence , he may be 
 entreat to respect our prejudice — indeed , it would 
 be preferable that programme should not contain 
 the name of the acknowledge ' great ' composer 
 unless their work be perform accord to their 
 author ’ intention . these remark perforce include 
 reference to the item on yesterday 's programme 
 ascribe to Handel and to Beethoven , for in neither 
 case be the composition render in their in- 
 tegrity . " criticism of this sort would be very useful 
 near home . Hy 

 the ultra - wagnerian Musical Courier of New York 
 be disgusted with some of the recent proceeding at 
 Bayreuth , and seem a little anxious lest decadence 
 should have set in . we cite our contemporary ’s 
 out - spoken , and , by all account , not uncalled - for 
 remark : ' * our german brother be not slow to 
 appreciate the situation , and as chance for skinning 
 in the wholesale style be rare in the land of the 
 Kaiser , they simply make the most of the opportunity . 
 Bayreuth have become the centre of all that be 
 musically aristocratic , but we venture to suggest to 
 Cosima Wagner and her satellite that she may run 
 a good thing into the ground , both by neglect to 
 place the business management of the scheme in less 

 Dr. Mackenzie 's ' Rose of Sharon " have be 
 perform at Wellington , New Zealand , with success , 
 notwithstanding the fact that , if the critic of the 
 New Zealund Times must be believe , the libretto be a 
 ' sensational novel , ' into the text of which word 
 of scripture have be " twist . " even a New 
 Zealander should be careful in write about what 
 he fail to understand 

 Sik GrorGe Grove ’s demand for photographic 
 copy of the autograph score of Beethoven ’s 

 Symphonies be one to be support in every way , 
 reproduction in that manner bring with it so many 
 advantage to student . the matter , we suppose , 1 
 simply one of expense , and there can hardly be 
 doubt that subscriber would come forward in sufh- 
 cient number to cover all possible risk 

 a meeting of subscriber interested in the formation of 
 the propose Scottish Orchestra be hold on the 16th ult . , 
 when it be resolve to launch a joint - stock company to | 
 carry out the scheme of the promoter . the prospectus , 
 memorandum , and article of association be adopt , 
 and a board of director and other official appoint 
 More than one speaker emphasize the desirability of 
 cordial co - operation with exist musical organisation . 
 the chief society refer to be , of course , the Glasgow 
 Choral Union , an institution which have bear the brunt of 
 the battle for little short of twenty year in the good 
 interest of orchestral music . the Council of that society 
 be approach some month ago with a view to settle 
 a basis of co - operation , but for good and sufficient reason 
 which then exist nothing practical result from the 
 conference , though sympathy be express in favour of 
 the propose new scheme . the chairman of the meeting 
 under brief notice state , by the way , that to ensure success 
 acapital of at least £ 50,000 should be secure . of this 
 amount £ 22,000 have already be subscribe , and the 
 directorate be hopeful that the balance can be raise before 
 any serious outlay be incur . an orchestra of eighty 
 performer be aim at , the headquarters would be in 
 Glasgow , and during a season of twenty - six week concert 
 might be give in all the leading Scotch town 

 meanwhile the Glasgow Choral Union , which have under- 
 take responsibility before the conference just refer to 
 take place , have send out its prospectus . amongst other 
 matter of no mean interest it be gratifying to note that the 
 guarantee fund — £ 4,101 — be the large on record , and 
 augur well for the success of this the eighteenth series of 
 concert . encourage , moreover , by the large measure of 
 support during the past season , the committee have decide 
 to strengthen the string contingent of the orchestra by the 
 addition of eighteen performer , make a total band of | 
 nearly ninety executant . the string be distribute as 
 follow — viz . , fourteen first violin , fourteen second , ten | 
 viola , nine violoncello , and nine double bass . many old 
 friend return as member of the orchestra , which will 
 again be lead by Mr. Maurice Sons . Mr. August Manns 
 will also have a cordial welcome , so , likewise , Mr. Joseph 
 Bradley , who have give so much satisfaction as a choral | 
 conductor . the list of solo artist include Mesdames 
 Fillunger , Clara Samuell , Belle Cole , Henschel , Marriott , 
 Fanny Davies , and Adelina de Lara ; Messrs. Edward 
 Lloyd , McKay , Lely , Henschel , A. Black , Foli , Mills , 
 Ysaye , César Thomson , Joachim , Piatti , Schénberger , 
 Lamond , and Sapellnikoff . unusual care have be 
 bestow upon the orchestral programme , which will , inter 
 alia , comprise Beethoven ’s Choral Symphony , the same 
 master ’s Violin Concerto , his no . 5 Pianoforte Concerto , | 
 Paganini ’s Fantasia for violin , the overture to ' " ' Tann- | 
 hauser , " ' Parsifal , " ' and the " Magic Flute , ’' Haydn ’s | 
 " Oxford , ' ? Mozart ’s " Jupiter , " and Schumann ’s ' ' Rhenish " 
 Symphonies , Couldery ’s romance in a flat , Hiller ’s Scherzo 
 inAminor , MacCunn ’s ' ' the Ship o ' the Fiend , " and Gluck ’s 
 ballet air from " Orpheus and Eurydice . " ' the choral 
 work have already be refer to in Ture Muvsicat | 
 Times — viz . , Berlioz ’s ' Faust , " Handel ’s ' Messiah , " | 
 Mendelssohn ’s ' " hymn of praise , ' and Mr. MacCunn ’s 
 new dramatic Cantata ' Queen Hynde . ’' it ought to be 
 mention that at the guarantor ’ meeting , hold on the 

 i8th ult . , the season ’s arrangement be regard with 

 De Profundis ( psalm cxxx . ) . Set to music for Soprano 
 Solo , Chorus , and Orchestra . by C. H. H. Parry . 
 | Novello , Ewer and Co 

 Mr. Gatenouse ’s annual Recital at the Birkbeck | 
 Institution , on the 2nd ult . , be a remarkable performance , 
 if merely on account of the fact that only a few day prior 
 to the Recital he unfortunately meet with a very serious 
 street accident ; but notwithstanding his disabled con-| in add to the many setting of the 130th psalm " De 
 dition , he accomplish his task in a manner that be | profundis clamavi ad te domine , " Dr. Parry have not only 
 evidently to the entire satisfaction of a numerous audience , | assert his own claim to consideration as a composer of 
 and greatly to the enhancement of his reputation as a| eminence , but he have also take stand as a typical english 
 violinist . the programme include Beethoven ’s romance | musician . he have arrange his music so that it be practt- 
 in F , Ballade and Polonaise of Vieuxtemps ( which the | cally in twelve - part writing throughout , though not always 
 audience insist upon encoring ) , and the " Kreutzer " | for a choir of twelve voice . the opening chorus 1 for 
 Sonata , which be undoubtedly the piece of the evening and | three choir of four part each ; the second chorus , 
 met with a splendid recept‘on . Mr. Alfred Izard preside at | ' ' Sustinuit anima mea , " be for two choir of six voice 
 the pianoforte throughout and materially assist in the ! each , resolve in the double fugue at the end into 

 some 
 Cooper 

 foreign note 

 we extract the follow interesting paragraph from the 
 Neue Zeitschrift fiir Musik : — * there be still live , at 
 Baden , near Vienna , the female servant who use to attend 
 on Beethoven , and who be in fact still engage in service 
 at the identical house where the great master write his 
 stupendous Ninth Symphony . the house be private pro- 
 perty , and be occupy at present by an establishment of 
 sempstress . some Germen artist recently visit 
 the place and make the acquaintance of the somewhat 
 rough - spoken old lady , who , however , must have be 
 rather pretty in her youthful day . she remember the 
 ' uncouth , crazy musician ' she use to wait upon very 
 well . ' ifpeople be not so dull , ' she remark to her 
 questioner , ' they would be quite sure that none of the 
 portrait that be about be like he . he never trouble 
 about brush his hair , and look much fierce and savage- 
 like . " there be still preserve here the slip of paper whereon 
 Karl Beethoven write the word , * I must see you . your 
 brother Karl , house proprietor , ' along with the composer 's 
 memorable reply : ' I call on you , but do not find you at 
 home . IL . van Beethoven , brain proprictor . some 
 memorial tablet ought surely to be place against a house 
 so interesting to all music - lover 

 two complete performance of Wagner 's gigantic 
 tetralogy ' Der Ring des Nibelungen " ' be most success- 
 fully give during August and September at the Dresden 
 Hof - Theater 

 Bripiincton.—On Friday , the rth ult . , at All Saints ’ Church 

 improve their new premise , and all the appointment be of the 
 hand over by the eminent virtuoso to the Conservatoire | accommodate a much large number of pupil than the restricted 
 ys ° 2 ley phase brink tcisinae eee pe es : see 
 of the town . the programme include Beethoven ’s | SPace at the former building would allow . Cork be the only city in 
 . ' fa tea a ; of | Ireland which contain a school of this nature support by the city 
 Sonata ( Op . r11 ) , Schumann ’s " Fantasiestiicke " and | rate , : 
 re Carnaval , " and piece by Chopin , Liszt , and the pianist- | parrrorp.—on the 16th ult . a vocal and instrumental concert be 
 composer himself . give at the Conservative Club to a numerous audience . Miss Ada 
 the forthcoming centenary of the death of Mozart be to | Loaring , Mr. J. Millbourne , and Mr. George Schneider be engagid 
 es : soll it ‘ thie Semana Chea f Vi " ith as vocalist . Mr. C. J. Wilson play a couple of mandolin solo , and 
 € commemorate a 1 € imperia pera oo enna with a | Mr. H , Squires , of Maidstone , prove a most efficient accompanist . 
 performance of the seven great operatic work of the master , | wise he cishin can punleieattealee the Memwene Gori 
 f ae ; ' " 5 r A > elg uz : Z 5 i , urch 
 and in addition thereto of such historically interesting early | Choral Union be hold in St. Michael 's Church , Workington , on 
 production as ' ' Bastien und Bastienne ( 1768 ) and " La | Thursday , the roth ult . I he voice number 300 ; the choir fourteen . 
 finta giardiniera " ( 1775 ) . a similar series of performance | Mt. P. T. Freeman , of Keswick , be the conductor . 
 be also in course of preparation at the Royal Opera of } M : rye ) ele ipa a member of te Liedertafel , at a 
 Dresden | Special general mecting rel att lelr rooms ON ¢ ugust 6 , unanimously 
 one i , ns ey ) , | elect Mr. Henry John King as their conductor , and , on the motion 
 e the centenary of the composition of the ' Marseillaise of Mr. Marshall , the whole choir , with the president ( Colonel Turner 
 is to be celebrate in a special manner next year ( April , | and vice - president ( the Baron von Mueller ) , repair to the masonic 
 1892 ) . a committee , of which President Carnot be the | Hall where Mr. King be conduct the final rehearsal for the great 
 - . > 2 ’ ora oncer o congratulate 1 on his a 4 . te 
 h hair 1 all the Ministers of State | Choral c t — t sratulate he on his appointment . TI 
 onorary chairman , and all the Ministers of state be | announcement be receive with loud cheer . 
 member , have be form , for the purpose of arrange | NantwicH.—The annual Festival of Choirs connect with the 
 the detail , at Choisy le Roi , where the remain of the | Church Choir Association take place in the Parish Church , on Thurs- 
 composer of the National Hymn , Rouget de Lisle , be | day , the roth ult . there be sixteen choir represent , make a 
 bury r : | total of 385 voice . the service be very well sing , the word in the 
 a Psalms be unusually distinct . the anthem be " this be the day " 
 |(turle ) . the conductor be the Rev. C. Hylton Stewart , and Mr. 
 | Arthur J. Smith , Organist of the Church , be at the organ 

 TO correspondent 

 a , PerTH ( WESTERN AUSTRALIA).—-The Concert of the Musical Union 
 * , * notice of concert , and other information supply by our friend ae ee ag eg hice > 
 in the country , must be forward as early - possible after the a oe Reece se = ee : 
 occurrence ; otherwise they can not be insert . our correspondent the fir t nart mith he the ea bane ala part of Eli th and in the 
 must specifically denote the date of each concert , for without such pleted ae a " i re aera i - : - ian CPA re 
 date no notice can be take of the performance . | second part selection from some ot er work whic the Society have 
 | already perform . the Union consist of about one hundred and 
 our correspondent will oblige by write all name as clearly as pos- | forty perform member , and of these twenty act as an orchestr 
 sible , as we can not be responsible for any mistake that may occur . | in the past the work perform have include the Wesstah , Creati 
 correspondent ave inform that their name and address must | Elijah , hymn of Praise , Acts and Galatea , Israel in Egypt , Rossini 
 accompany all communication , | Stabat Mater , & c. , and the next to be take in hand will be Sf . Pau ! . 
 we can not undertake to return offer contribution ; the author , PortsmMoutTH.—On the 12th ult . Mr. Albert Mellot ( Assistant - Music- 
 therefore , will do well to retain copy . master at Eton College ) give two Organ Recitals on the fine organ in 
 notice be send to all subscriber whose payment ( in advance ) be ex- | the Town Hall . Messrs. Rowe and Howard , of Eton College Choir , 
 hauste . the paper will be discontinue where the subscription be | be the vocalist , and the violinist at the evening recital be Miss 
 not renew . we again remind those who be disappoint in Kathlee n Thomas . the programme be make up from the work 
 obtain back number that , although the music be always keep | of Bach , Handel , Haydn , Rossini , Lemmens , Elvey , Vieuxtemps , 
 in stock , only a sufficient quantity of the vest of the paper be | Batiste , Beethoven , and Wagner , and the several piece be well 

 print to supply the current sale , | receive 

 we do not hold ourselves responsible for any opinion express in 
 this summary , as all the notice ave either collate from the local 
 paper or supply to we by correspondent 

 ADELAIDE.—The large attendance at the last Concert give by Sir 
 Charles and Lady Halle , with Mdlle . Fillunger , on August 24 , in the 
 Town Hall , give evidence of the appreciation of the Adelaide musical 
 public cf true art . the Concert include Beethoven 's Sonata 

 Pathétique , Grieg ’s ' norwegian bridal Procession , " and one of 
 Weber 's well - know piece . Lady Halle ’s contribution be the 
 Andante and Finale from Mendelssohn 's Concerto in E , a Larghetto 
 by Nardini , the D minor Sonata by Mozart , Vieuxtemps 's Reverie 

 have write for chorus in twelve part , but the music 

 though splendid from a scientific point of view , be far from 
 be mere science . it be beethovenish in breadth of out- 
 line and wealth of expression 

 BRISTOL TIMES and mirror . 
 notwithstanding its scholarly and deep character , there 
 be a wealth of melody in the work which win the admira- 
 tion of the general public , while the masterly construction 
 inspire the homage of the musician 

 13 . theearth be clothe in flower . four- part ( Fallof the leat ) O. Barri | 102 . blow , gentle gale . three - part .. we 
 . with heart of joy . three - part ( Fall of the Leaf ) . » O. Barri | 103 . hark ! apollo strike the lyre . three - part 

 15 . sing , sweet bird . three - part ( Fall of the Leaf ) .. .. © . Barri | 104 . the wind whistle cold . three - part 
 16 . haste tothe church ! three - part ( Bride of Burleigh ) .. F.Schira | 105 . the chough and crow . three - part 
 17 . chorus of angel . four - part ( Bride of Burleigh ) .. F.Schira | 105 , sound the loud timbrel   three - part a ie 
 18 . awake from slumber . three - part ( the Magic Weil ) .. F , Abt | 107 . ye shepherd tell I ( the wreath ) . three - part " " Mazzinghi 
 19 . o’er the flower - bespeckl’d meadow . three - part ( the 108 . harvest home . three- part . Bishop and Foster 
 Magic Well ) .. a i aa f. Abt | 109 . but now the pearl - crown'd moon . three- -part Bok 
 20 . I see a land of wondrous beauty . three - part ( the | 110 , O ! finda song . three - part ( Summer Night ) 
 Magic Well ) .. s ve F , Abt ) 111 , when the wind blow . three - part 3 
 21 . come , let we wreathe ' the bridal flower . three - part 112 , alullaby three - part . : 
 ( Orpheus and Eurydice ) Offenbach | 113 . come over the hill . three- part 
 22 . now on music ’s wing . three- part ( Orpheus and E ury- 114 . the sower . three - part a band 
 dice ) i Offenbach | 115 home and rest . three - part . as nie a .C. Pinsuti 
 23 . homeward we be wend . or hree - part ( May - tide ) P. Mazzoni | 116 . memory . three part os ee - » C. Pinsuti 
 24 . trip itlightly , gaily , brightly . three - part ( Harvest Queen ) 117 . good - bye , sweetheart , good- bye . three - part 
 Godwin Fowles | 118 . the city of the king . three - part ns 
 25 . angel of peace . three - part ( Lohengrin ) .. Wagner | 119 . Erin mavourneen . three - part 
 20 . in Thee we trust . three- aa ( pilgrim ’ chorus , Tann- 120 blessed be the poor in Spirit . t hree- -part ( Se wcred ) « 
 hauser ) . ' ne va . Wagner | 121 . through the old cloister . three part .. 
 27 . awake ! awake ! " thre " e : -part sf . ae 2 fe C. Gounod | 122 . roll on , fair orb ) three - part ve a Beethoven 
 28 . music and sunshine . three - part .. " s C. Gounod | 123 . in thee I trust . three - part ( ( Sacred ) + a " .. Handel 
 29 . joyous our life . three - part ( Elfin Knight ) ° I. Gibsone | 124 . ring on , sweet bell . three - part .. ; ee > 
 30 . sing , sister , sing . four - part ( All Hallow Eve ) . B. Gilbert | 125 . the minstrel . three - part 
 31 . sunrise . three - part ( the Mountain Maidens ) .. .. F. Romer | 126 , an evening song . three - part 
 32 . sunset chorus . three - part(the Mountain Maidens ) .. F. Romer | 127 . the wood nymph ’s home . three - part 
 33 . our old piano . three - part .. ae ae : I .. F. Auger | 128 , the song our father love . three - part 
 34 . home beli . three - part : oe as G. Schmitt | 129 . gipsy chorus . three - part ( Bohemian Girl ) 
 35 , Dearhome . three- part ( 6 i annhauser ) a ae .. Wagner | 130 . now to the fair . three - part ( Bohemian Girl ) .. M. W. Balie 
 36 . summer . three - part ( Tannhauser ) ee .. Wagner | 131 , yet droop the maiden three - part ( Bohemian Girl ) M. W. Baliz 
 37 . for thee hath beauty . three - part ( Oberon ) . .. Weber | 132 . deign to forgive the gipsy maid . three - part ( Bohemian Girl ) 
 38 . I remember one eve . three - part ( Elfin Knight ) I. Gibsone M. W.1 
 39 . vintage time . three - part ( Etfin Knight ) I , Gibsone | 133 . the maiden ’s dream . three - part . Sir Julius Bene 
 40 . odour come with quiet spell . three- — ( a Summer 134 . from out thy starry dwelling . three- part aaa 
 night ) ... fs st ; 2 E. Aguilar Moses in Egypt ) . 
 41 . sad be the parting . two - part ( Volkslied ) . vs re .. German | 135 . Father of Heaven three- part ( Pray er , Masan illo ) 
 42 . not in our griefalone . two - part .. ar .- Henry Smart | 136 . a sylvan song . three - part 
 33 . a Christmas song . two - part de ini A. Adam | 137 . a lady fair ( Villanelle ) . three- -part 
 44 the hymn of peace . two - part < a 5 .. WH . Callcott 138 . stay . prithee , stay . three - part 
 45 . o ! southern wind two - part ' om a .. O. Barri ! 139 . o ! skylark for thy wing . three - part 
 46 . dweller of the spirit land . two- part Sis as .. O. Barri | 140 . light as fairy foot can fall . three - part 
 47 . ' ti summer . two - part a ' ae = ! .. O. Barri ) 141 . the evening bell two - part . 
 48 . o ! come tothe grove . two - part .. ee he .. O. Barri | 142 . haste thee , boatman . two- part x 
 49 . the snow . two - part .. a ‘ i sd as .. O. Barri | 143 . on the margin of that river . two- part 
 50 . come back . two - part .. es bs ie ee .. O. Barri | 144 . bird of the morn . two - part .. 
 51 . a Christmas carol . two - part as ss be C. Gounod | 145 . wandering wind . two - part .. oy ne .. Henry Smart 
 52 . the midnight bell . two - part J. L. Roeckel | 146 . song of the sea breeze . two- ane ne a | : L. Hatton 
 53 . where the scented violet . two- part ( the " Magic Well ) — F. Abt | 147 . heavenly music . two - part .. ' a ne . L , Hatton 
 54 . see , the star of eve . two : part ( Elfin Knight ) .. I. Gibsone | 148 . adieu , ye woodland . two - part .. ae a " 4 f , Abt 
 55 . the cuckoo kept call . two - part ( Elfin Knight ) I. Gipsone | 149 . nightingale be singe . two - part oo F. Abt 
 56 . come , let we go . two - part ( Elfin Knight ) : I. Gibsone | 150 . sing to I , gondolier . two - part .. a as C. Gounod 
 57 . we will seek thee two - part ( the Mountain Maidens ) F. Romer 151 . now , merry fav . two - part .. i " , C. Gounod 
 58 . we be only village maiden . two - part ( May - tide ) P. Mazzoni | 152 . the voice of Spring two - part . Offenbach 
 59 . John Peel . two - part ( Hunting song ) ae : D. Pentland | 153 . the haymaker . two - part . Offenbach 
 60 . o ! come hither . two - part ' sacred ) oe .. Henry Smart | 154 . now the meadow . two - part ee 
 61 . o ! give thank . two - part ( sacre ) Henry Smart | 155 in thy service . two - part ( s acred ) . 
 62 . o ! praise the Lord with I . two - part ( sacre ) . Mendelssohn 156 , I think of thee . two - part .. > ee 
 63 . hark ! the hunter ’s merry horn . two - part oe L. Zamboni | 157 . o ! ye sunny hour . two en . 
 64 . old english pastime . two - part .. J. L. Battmann | « 58 . good night . two- part . . 
 65 . the organ - grinder . ' two - part oe es .. J. U. Battmann | 159 . cradle by the heave billow . two - part 
 65 . the happy fireside . two - part sia er .. Henry Smart | 160 . serenade . two - part se ke 
 67 . hark ! from yon old abbey . en pe = i J. Barnett | 161 , the sailor 's return three - part ee , 
 6 % . merry May . two - part : ite .. Sir G. A. Macfarren | 162 . the song of June . three - part Mendelssohn 
 69 . the alpine sister . two- part ne .. D. Tagliafico | 163 . singe in the rain . two - part PP .. J. P. Knight 
 > o. heap high the golden corn . two- part oe ae -- Raphael | 164 . the ferry boat . two - part .. " 7 ae ee L. bag 
 . Lull'd by the silence . two - part .. m8 vs .. Zamboni | 165 . when spring descendeth . two - part . " so 
 . fairy sister . two - part " ee fol . ee .. F Packer | 166 . where art thou , beam of light ? three - part .. - Bishop 
 3 . merry May . two - part i h 0 % pm soo DDiStini one MANaWay , AWOspanGns " oe es   -<o j " R. Thomas 
 the swallow ’ return . two - part .. es ee .. T. Distin | 168 . Italy 's music . three - part .. + fe rr C. E. Hor 
 75 . the sea - nymph . two part .. om se va .. T. Distin | 169 . Nina 's farewell . two - part . Pergolesi 
 76 . Merrie gipsy . two - part a a os ae .. T. Distin | 170 . the gipsy home . two - part .. 
 77 . hail ! pretty babe . two - part ‘ a ar oe as : Distin | 171 three vivandicre . three - part ; 
 78 . sleigh - bell . two - part ae ‘s - si . Distin | 172 . peace . two - part ; ie " | Sir ir Julius Be -nedist 
 79 . for so hath the Lord . two - part ( St. Paul ) x. I - ‘ Issohn | 173 . sing , pretty maiden . three - part ( Maritana ) allace 
 8 . and Paul come to the congregation . two - part Mendelssohn | 174 Angelus . three - part ( Maritana ) pe w allace 
 81 . to God on high . three - part Chorale , St. Paul ) Mendelssohn | 175 . pretty gitana . three - part(Maritana ) .. se .. Wallace 
 82 . o ! thou true and only lig t. three - part ( Chorale ) Mendelssohn | 176 alas ! those chime . thr - part ( Maritana ) .. .. Wallace 
 83 . howlovely be the I senger . three - part ( Chorale ) I - ‘ Issohn | 177 . turn or , old time three - part ( Maritana ) .. Wallace 
 84 . away ! away ! three - part . . an J. L. Roeckel | 178 . God save the queen . three - part ee ay .. National 
 85 . the angel ’ song . three- part . be cd oP C. Gounod | 179 . say . sad heart , why art thou beat ? two - part .. Fam 
 86 . a farewell . three - part os ve 5 I C. Gounod | 180 , Eve 's lamentation . two - part ey & Wie M. P. king 
 8 > . the dawn , three - part es C. Gounod | 181 . thy spirit be near . two - part ' * Henry Smartt 

 LONDON : E EDWI in ASHDOWN ( Limitep ) , HANOVER SOU ARE 

