music - making , midnight hour , 
 conclude ' God save King , verse 
 chorus , stand . ' meeting place 
 White Hart , identical hostelry 
 Mr. Pickwick stay memorable visit 
 Bath , occasion Mr. Sam Weller remark 
 ' hear bile leg o ' mutton 
 swarry afore . ' interesting record 
 year 1794 landlord 
 White Hart P - ckwick 

 1822 - 23 Sir George Smart series 
 subscription concert Bath , 
 Beethoven fifth Symphonies play , 
 overture ' Edvardo e Cristina ' 
 ' Les Voitures Versées , ' Rossini Boieldieu 
 respectively , time England , 
 Moscheles produce _ Pianoforte 

 Sir George set programme 
 concert preserve british | 
 Museum , following receipt , bear | 
 1 shilling stamp , erection organ 

 King Edward Professor Music , Sir Frederick | 
 Bridge , deliver course lecture | 
 season University London , subject | 
 discourse ' Composers Classical Songs . ' | 
 admission lecture , open the| 
 public fee , ticket , application | 
 Academic Registrar , University | 
 London , South Kensington 

 millionaire musical - autograph collector gratify | 
 desire acquire treasure | 
 offer sale Leipzig antiquarian bookseller . | 
 Beethoven ' Bagatellen , seven short 
 pianoforte piece ( Op . 33 ) , consist nineteen folio , 
 sum 22,000 mark ( £ 1,100 ) ask . 
 second ' Lot ' consist original | 
 ' thirty - variation waltz by| 
 Diabelli ' ( Op . 120 ) , compose 1823 . } 
 acquire — copyright advantage to| 
 recompense buyer — 42,000 mark , £ 2,100 , | 
 equal , , £ 49 - 
 folio constitute little volume . thirdly , 
 original score Wagner ' love - feast | 
 Apostles , ' value present owner 12,500 mark , 
 £ 625 , mere bagatelle elaborate work 
 compare sum demand the| 
 Beethoven trifle 

 master 

 old country Christmas - time 

 viennese paper announce discovery , Herr 
 \lexander Hajdecki , - hitherto unknown | 
 ietter Beethoven . address | 
 master friend Bernhard , editor Vienna paper | 
 author sundry operatic libretto . | 
 important letter long document | 
 - seven page , write Beethoven | 
 illegible handwriting . form 
 pro memoria , address Bernhard , | 
 enable prepare petition proper form , | 
 court appeal , connection unsuccessful 
 action bring Beethoven obtain guardian- | 
 ship nephew Carl , death that| 
 unworthy youth father . document state to| 
 proof Beethoven nobility character , | 
 degree culture remarkable 
 receive benefit scholastic training 

 Yorkshire newspaper 

 famous collection old musical instrument 
 belong Herr Paul de Witt , 
 proprietor - editorof Zez¢schrift fiir Instrumentenbau , 
 acquire present city 
 Cologne Herr Commerzienrath W. Heyer , form 
 nucleus new historico - musical museum . 
 add collection form well- 
 know firm pianoforte manufacturer , Messrs. 
 Ibach , Barmen , number instrument 
 specially purchase . Cologne soon 
 able boast representative 
 museum kind existence 

 Messrs. Schuster & Loeffler , Berlin , announce 
 early publication new complete edition 
 literary work Carl Maria von Weber . 
 volume , edit Herr Georg Kaiser , contain 
 article include previous 
 collection . interesting 
 new editor attempt ' explain away ' 
 passage Weber conclusively proved 
 dad critic — zde remark Beethoven 
 fourth Symphony — opera demonstrate 
 fact bad critic mean 
 bad composer 

 german musical paper record production 
 ' satyrspiel ' compose Herr Gustav Wied 
 bearing fascinating title 

 Eighth Concert , Monday , July 8 , 1844 

 I. 
 Sinfonia Eroica~ - : . - - - Beethoven . 
 Song , MS . , ' Ach Herr , ' Herr Staudigl - - Nivolat , 
 trio , violoncello , double bass , 
 Mr. Lindley , Mr. Lucas , Mr. Howell Cor 

 scene ' like , ' Miss A. Williams , 
 Miss Dolby , Mr. Allen , Herr Staudigl , 
 chorus 

 experiment approach success , 
 , hope , suggestion non - opera goer 
 hear masterpiece complete stage . 
 performance fine . , fourteen soloist 
 - know singer , chorus excellent 

 day devote Bach ' St. Matthew ' Passion , 
 Beethoven choral syimphony , _ miscellaneous 
 selection . supremacy Bach con 
 vincingly exhibit occasion . performance 
 regard culminate feature festival . 
 immense pain rehearse work . Mr. 
 Wood write organ , orchestra specially 
 constitute effect composer colouring 
 method . flute , oboe , 
 bassoon ( Bach write ) , oboi d’amore , 
 oboi da caccia , solo viola da gamba . chief 
 vocal soloist Mr. Dalton Baker , Mr. Webster Millar , 
 Mrs. Henry J. Wood , Miss Jessie Goldsack , Mr. H 
 Witherspoon , Mr. Robert Charlesworth , Mr. Joseph 
 Lycett . competent , special word 
 praise Mr. Millar able way sing 
 arduous Evangelist . tone - colour secure 
 accompaniment peculiarly beautiful haunting . 
 chorus - singing sublime — word 
 describe . feel tremendous , 
 thrilling force famous chorus ' lightning 
 thunder ' staccato , lovely chorus 
 somewhat daintily treat , overwhelming 
 grandeur expression interpretation generally 
 live long memory forget . 
 performance triumph Mr. Wood 
 Dr. Coward , noble tribute genius Bach 

 evening concert , conclude festival , 

 panie piece efiective , specially . 
 Cornelius piece mood . trio 
 music half number voice , 
 tone beautiful . motet 
 splendidly . 1 t evident Shetfield choralist 
 Bach 

 Beethoven Choral Symphony work perform . 
 instrumental movement display orchestra 
 good , choir unexhausted previous great effort 
 blaze forth triumphantly . soloist : Miss 
 Jenny Taggart , Miss Edna Thornton , Mr. Lloyd Chandos 
 Mr. Frederic Austin 

 attendance good , seat rarely 
 fill . - seventh audience time 
 consist lady , fact imply lack interest 
 man , simply c yuld spare 
 time attend . service Mr. J. W. Phillips 
 organ festival deserve special mention . 
 remain add Mr. J. A. Rodgers contribute 
 able interesting analytical note programme , 
 official , secretary , Mr. EF . Willoughby 
 Firth Mr. Noel W. Burbidge , downwards , 
 particularly courteous attentive 

 ighly favourable impression group — 
 piece production recent Worcest 
 festival , fully confirm occasion 
 initial hearing metropolis . number 
 form suite , favourably receive 

 1een Hall open March , 
 Finale , vigour - prove 
 acceptable sustained delicacy precede 
 number . feature concert 
 appearance Mr. Eugene Ysaye , revive Corelli 
 Concerto Grosso . 8 , solo violin , string orchestra , 
 organ . extremely interesting example 17 t 
 century sic belgian violinist ably assist 
 Mr. Maurice Sons , gore second violin solo , 
 Mr. Frederick B. idle organ . Mr. Ysaye 
 hear hi A. good Beethoven concerto 

 enthusiastic applause rec d 

 success 

 doubt case 
 talent progress far development 
 especially surmount great difficulty technique 
 victorious ease . time , performance 
 wonder - boy amply demonstrate appearance 
 premature ; unable sound 
 emotional depth Beethoven concerto . Colbertson , 
 fourteen year old , bear Roumania train 
 Prof. Sev€ik Prague 

 RICHARD VON PERGER 

 Wesley aloud 
 Lloyd - chorus 

 mposition perform 
 Messrs. T. ’ . Morris , 
 Wiltshire Ivor 
 Beethoven Quintet 
 1 , Messrs. Morris 

 ively play Cavatina 

 idgment , Mr. Friskin prove pianist 
 outstanding ability , technical power high 

 order reading , especially Beethoven , 
 evidence ripe musicianship 

 direction Mr. A. M. Henderson , local 
 pianist distinction , enjoyable chamber concert 
 ( ctober 20 27 . programme 
 include trio pianoforte , violin violoncello 
 Mozart Arensky , concert - giver 
 associate Miss Bessie Spence ( violin ) Mr. John 
 Linden ( violoncello ) , quartet Rheinberger 
 Dvorak , play Miss Bessie Spence ( violin ) , Mr. John 
 Daly ( viola ) , Mr. George Bruce ( violoncello ) Mr. A. M. 
 Henderson ( pianoforte ) . solo piece als 
 contribute performer . 
 music - making chamber concert new 
 combination , ' Glasgow Trio’--Miss Maggie Horne 
 ( violin ) , Mr. John Linden ( violoncello ) , Mr. Wilfrid 
 Senior ( pianoforte ) ; Messrs. Philip Halstead 
 Henri Verbrugghen . pianoforte recital Madame 
 Carrefio ; violin recital Mr. Holroyd Paull ; 
 week performance opera Moody- Manners 
 Company 

 MUSIC LIVERPOOL . 
 corresionden!i 
 T pening concert seventieth season 

 ilharmonic Society place October 13 , fine 
 vance Beethoven fourth Symphony 

 Carrefio play brilliant virile inter 
 Grieg Pianoforte concerto , Signor 

 volution string quartet , ) Mr. H. McCullagh 

 Mendelssohn : centenary celebration , ' ' Beethoven 
 music , ' Rev. H. H. McCullagh ; ' irish fairy song 

 1 tale , ' Miss Madelaine 

 MUSIC MANCHESTER . 
 ( correspondent 

 great enthusiastic audience welcome Dr. Hans 
 Richter October 15 conduct 
 weekly concert Hallé Society . programme 
 nclude ' Meistersinger ' overture , Strauss ' Tod und 
 Verklarung , ' Beethoven C minor Symphony . Madame 
 Carreiio play Grieg Pianoforte concerto , Liszt sixth 
 rhapsody , , encore , late Edward MacDowell 

 dainty little piece , ' Ilexentanz . ' second concert 
 Dr. Brodsky play Violin concerto D fellow 

 countryman friend , Tchaikovsky . _ orchestral 
 selection include Beethoven ' Egmont ' overture ; 
 Debussy Prelude , ' Aprés - midi d'un Faune ' 

 TIMES.—NoveMBER 1 

 season October 21 , performer , 
 commencement , Dr. Brodsky , Mr. Rawdon Briggs , 
 Mr. Simon Speelman , Mr. Carl Fuchs Haydn 

 Juartet C ( Op . 76 , . 3 ) ; Schubert pianoforte 
 Trio b flat ; Beethoven ( uartet minor 
 ( Op . 132 ) programme . trio Mr. Max 
 Mayer pianoforte , greatly help Dr. Brodsky 
 Mr. Carl Fuchs secure work inspiring 

 interpretation 

 opening new organ , buil 

 Mr. A. Keates , Sheffield . Mr. C. W. Perkit 
 Birmingham , public recital new 
 nstrument 
 +34 ed 
 Foreign Wotes . 
 Koyal Oratorio Society programme come 
 season include Handel ' Joshua , ' Beethoven Chora 

 Symphony , Vincent d’Indy ' Lay bell , ' ' Ode t 
 friendst > T. Wager ' ' Resu et ( set 

 n 

 genuine sensation new musical season « 
 unexpected quarter , viz . , concert Roya 
 Orchestra , Koyal Opera House October 2 
 programme contain ' sensational ' thar 
 hree old little - know work | 
 Haydn , equally unhackneyed masterpiece Mozart , 
 ( b. h. . 29 ) , Beethoven ' Eroica . ' 
 conduct rostrum stand Germany foremost ving 
 musician , Dr. Richard Strauss , debut 
 conductor Royal Orchestra , successor o 
 Weingartner coveted office , performance 
 » f classical piece unconventional way 

 symphony — 

 Symphony Concerts conduct Prof. Karl Panzner , 
 Mozartsaal , novelty include Felix Woyrsch 
 C minor Hans Huber ' heroic ' Symphonies ; 
 symphonic poem ' Nun , ' Leo Blech , ' hero 
 Leander , ' Paul Ertel ; * overture toa shakespearean 
 comedy , ' Paul Scheinpflug , prelude ( ' harvest 

 Festival ' ) Act iii . Max Schillings ' Moloch . ' 
 symphony orchestra form . 
 direction Herr Oskar Fried member thereof , 
 n October 11 , successful concert 
 Bliithner - Saal , institution 
 ' Bliithner - Orchestra . ’-——In Beethoven - Saal , Herr Fritz 
 r concert October 10 , 
 woduce work composition , viz . , 
 Introduction ( 77 ti = Rectitativo ) Scherzo 
 esque ' violin 

 

 FRANFURT - - MAIN 

 old distinguished local concert society , 
 Frankfurter Museumsgesellschaft , celebrate October 2 
 hundredth anniversary foundation 
 festival concert , Eugen d’Albert soloist Beethoven 
 ' Emperor ' concerto 

 HANOVER 

 Th ussical Concer soctety ( th 
 Concert Committee ) concert , seven 
 consist chamber music , Bechstein Hall , 

 orchestral choral music , ( ) ueen Hall ( December 9 ) . 
 string quartet Haydn , Beethoven , Schumann Brahm 
 announce performance , Beethoven Strit 
 G major , Mozart Trio pianoforte , clarinet viola , 
 Dvorak Pianoforte trio F minor , Mozart ' ) uintet 
 pianoforte wind instrument , Brahms Pianoforte quintet , 
 work . Schubert ' Grand duo ' ( op . 140 ) , 

 pianoforte duet , arrange orchestra Joachim , 
 play ( Jueen Hall Concert ( 

 J. T.—The following suggest rate speed 
 Haydn Pianoforte sonata EF flat — movement , 
 rotchet- 80 ; second movement , quaver- ; 

 nent , minim 84 . Beethoven Pianoforte sonata D 

 NOVEMBER 1 , 1908 , 735 
 ( op . 28)—a 0 , dot minim 60 ; Andank , iver 
 84 ; Scherzo , dot minim 96 ( Zrio 8S ) ; ndo , dot 
 crotchet s4 

 DURHAM find shake write 
 Cotta edition Beethoven Rondo G 

 pianoforte . dante section 

 cal score vs N 

 gift sician , S. ] Leipzig ) SOU 
 fn t yt ing ut unplay s fre instrume 
 4 f e Roy College Music ) : th taste 
 ] t eve ess por 1s t Holy Scriy 
 \ ' w analyt | Ger 
 e , price 
 BACI Mote pr y Lord English Version HENry J. Woo ! use Sheffield Norwich 
 | vocal Score 6d , 
 Ilumor Cantat ' ¢ Pj und Pan . " sed Norwich Festival oa 7 ” 1 . 
 m use Norwich Fest 98 ° 1 
 BEETHOVEN . ( ¢ ral Symy ] version REv . Sovu1 ARD . sed Sheffield , Bristol 
 Norw Fes S Vocal Score 2 . od . 
 BERLIO Te De 1 Sheffield Festival " 2s . od . 
 DELIUS , FR * * Sea Drift . use Shettield Festival - 99 4 1 . 
 RANCK , CESAR ' Beatitudes . " use Shetheld Festival Vocal Score . net $ s 1 . 
 HARRISON , [ . * * Cleopatra . " ' write specially Norwich Festival Vocal Score 2s . od . 
 ALESTRINA * * lamentat use Sheffield Festival " " 1 . 
 - song . 
 BAINTON , | AR L. Miracle . choral song Mixed Voices . ( s. ) 1 . 
 BANTOCk , GRANVILLI Elfin Musi Poem SHELLEY . trio female voice . ( Ss . MEZZO . 1 . 
 BERGER , FEDOI Song Pied Piper . english version Rev. Canon GorTON . man voice 5d 

 s t » p 7 ; 
 R 
 ELY , THOMAS . ' * home , bring warrior , dead . " mixed voice 
 GI MA alm - S ay Morning Poem G Englisk version HELEN F. BAntt 
 
 t l r 
 p | y 
 \ ( ig 
 \ rs Song Cheer Dear Lad English wor y | ENGLAN 
 \ score 
 SHA EI TRAM . O Fir Tree Br Vords HELEN Hunt J 
 art 4 
 PIANOFORTE SOLO . 
 278 SIBELIUS , JEAN . Al Song still’d . 2 . Andantino ' ' Kyllikki . " 3 . romance 
 nd } . 3rd movement Sonata , op . 12 . 5 . dance intermezzo . 6 . Alla Marcia , 
 ron * * Carelia ” ’ suite . 7 . war song Tyrtcus . S. Eventide , Finnish Song 3s 
 ¢ : frer tk 1 ; f e 
 . s 
 t ! 
 
 " w 
 v pis tt rep 
 st 
 preci w 

