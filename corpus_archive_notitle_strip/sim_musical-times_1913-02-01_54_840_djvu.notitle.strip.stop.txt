Lecture - Recital Organ Music Karg -Elert given | MUSIC { ZL 1 , EDI . VBURGH 

 Eaglefield Hull , Esq . , Mus . Doc . Oxon . , F.R.C.O. , Wednesday , — ARES PS 
 eteah epen ‘ BEETHOVEN ORCHESTRAL FESTIVAL 

 4 Special Course Lecture - Lessons Professional Music Teac noe 

 drama limited senses view 
 framing theory . proposition 
 I. correct mean music 

 interesting enterprising| , Italian opera , occupy 
 sterviewer shades procure / ear exclusion worth story 
 \agner opinion course events in| psychological interest characters . 
 qusic general opera particular during| sense opera acceptable men 
 te thirty years elapsed death . | heads hearts ears , Wagner 
 probably cling characteristic | certainly drama end , music 
 wmacity views held lifetime ; but|}the means . viewed broadly , work 
 ‘ candid admit ! greatest glorification music 
 d problems taken new aspect . | theatre seen ; enormously 
 theories expounded eagerly prose| increased expressive scope , cut 
 wks illustrated eloquently music-|out drama half elements 
 iamas passed fire thirty| word meaning apart music . 
 ars ’ criticism suffering loss } Drama , , meant analysis little 
 ulity . Supposing brain comprehensive , as| best possible text stage music . 
 wiously gifted , forceful to| wuuld denied interpretation 
 uke problem opera , seeing afresh | theories practice , 
 s Wagner , combining , like , the| upshot . ‘ Word - speech , ’ says , 
 wtencies best instrumental operatic } merely organ intellect , 
 music day vast synthesis , / right entry music — emotional art 
 wuld new form strike — | ar exce//ence — far necessary 
 tata new form necessity evident both| coherence indeterminate flood feeling 
 na prort posteriori grounds . Music | music pours ; music , ought , 
 wmore stand Wagner Bach | ally words 
 : Beethoven ; new humanity find new ] emotional content . reason 
 apression reading life . a|he rejected historical political subjects , 
 uvey opera Wagner death leaves| found ideal ‘ stuff ’ opera ‘ purely 
 w room doubt emotions aspirations | human ’ legends folk ; ‘ Communi- 
 ‘ new humanity found form | cation Friends ’ traces close detail 
 mst appropriate . Wagner more| gradual growth perceptions respect . 
 ucceeeded making special type musical } hidden , , , 
 ‘ ama norm later generations Bach } persistently denies , evident 
 succeeded imposing forms 47s music | else,—that change theories practice 
 art epochs followed . In| musician slowly asserting 
 case spirit endures , form.|himself greater greater urgency , 
 elements Wagnerian form of| finally demanding imperatively form text 
 use , far judge , permanent| allow gift musical expression 
 ators opera general,—the use leading|the utmost possible freedom . 
 temes , example , system entrusting|be borne mind Wagner theory 
 } melodious , flowing , quasi - symphonic develop-| unification arts art - work 
 unt orchestra . these|the product brain comparatively little 
 ‘ ements recognised indispensable consti-| sympathy , understanding , art 
 ‘ ents opera : Debussy , instance , | music . hard saying , 
 “ ards greater his| proof found declarations 
 ‘ teas Melisande . ’ rest , the|his prose works , letters , ‘ Mein Leben . ’ 
 * partures Wagner precepts noticeable| painting , prose 
 “ ough , especially regards poetic basis of|drama , poetry , sculpture , precisely 
 ya . Apart negligible work ! painters , dramatists , poets , sculptors saw . 
 “ ond - rate imitators , hard point to| seriously thought ‘ spoken form 
 ‘ single opera man genius follows play ’ ( die Schauspielform ) ‘ necessarily vanish 
 ‘ ner reliance myth clearest |in future ’ ; painters 
 “ ¢ fundamental expression ‘ purely | ‘ egoistic ’ filling little canvases content 
 “ man , planning subject to| devote powers contributing , 
 “ tice toa minimum musical matter in| poet , musician , rest , 
 “ text , opera , far | ‘ united art - work future . ’ Clearly 
 * 4 pure expression ‘ soul - states . ’ |the musician dominated 

 3 ¥ ERNEST NEWMAN 

 X 

 attitude artistic mind 
 future drama , think , find | 
 utterance form quasi - dramatic music in| 
 shall rid mere 
 scaffolding narration action serves 
 present simply intellectual support 
 music opera . Wagner not| 
 painfully conscious times fact 
 music , matters great deal , diluted | 
 turbid quantity baser matter | 
 function clear 
 particular people 
 particular moment , 
 ? reiterated 
 music alive form art 
 music enters . Facts lose force 
 repetition ; artistic emotion 
 born anew die . 
 feels glow rapturous 
 anticipation notes ‘ Liebestod ’ 
 Wotan ‘ Abschied ’ sounded ? 
 heard times , 
 know note heart ; 
 new wonderful inevitable 
 hundredth hearing . 
 groan depths spirit Wagner 
 care moment kindle 
 great music tell great length , 
 hundredth time , certain mere facts 
 long lost absorbing interest ! | 
 compact work — ‘ Tristan ’ — | 
 great deal , highest point 
 view , superfluous ? bear hear 
 glorious music time number ; 
 bear told time number 
 Tristan Isolde Marke Morold , 
 Tristan slew Morold , Isolde 
 nursed Tristan health , rest 
 . imagine ‘ Tristan ’ things 
 kind assumed matters 
 common knowledge 
 audience , characters motives 
 Tchaikovsky ‘ Romeo Juliet ’ ‘ Francesca da 
 Rimini ’ assumed common knowledge , 
 Strauss ‘ Macbeth ’ ‘ Till Eulenspiegel , ’ 
 Beethoven ‘ Coriolan ’ ‘ Egmont ’ 
 Overtures ‘ Leonora . 3 , ’ 
 Dukas ‘ L’Apprenti sorcier . ’ 
 composer time audience attention 
 devoted musical exposition 
 protagonists ’ soul - states 
 Wagner avowed ideal music - drama , 
 virtually impossible ideal long 
 opera compelled utilise actors 

 man - — stage , occupy 

 period . left Conservatoire flying| major Sonata violoncello pianoforte , 
 colours , chief apparent occupation | work famous . 
 virtuoso appearances pianoforte — for| instigation Herr Carl ] Simon , 

 possessed amazing clavier technique | Berlin music publisher , Karg - Elert specialised 
 obsessed craving composition,|on ‘ Kunst Harmonium , ’ a_highly - finished 
 occupied waking hours . on| instrument orchestral type 
 clavier , worked way Liszt and| manuals . province Karg - Elert speedily 
 Chopin Schumann , Fantasiestiick has| continually playing chiei 
 exercised continual hold . Through| German cities writing large number 
 Schumann Brahms , went two| pieces specially . rhe famous 
 immortals , Beethoven Bach , age | Passacaglia E flat minor organ firs 
 - appointed advanced | appearance way . mere mention 
 pianoforte teacher Magdeburg Conservatoire . | titles pieces lofty view 
 introduction Edvard Grieg proved be| took possibilities new instrument — 
 chief signposts career . From| Aquarellen , Monologue , Scénes _ _ pittoresques 
 learned true importance com-| Improvisation , Madrigal , Sonata B _ mina , 
 position ‘ expression own| Partita movements , Fantasie Fugue , 
 independent personal feelings . ’ Norwegian | Sonata B flat , double fugue B.A.C.H. , & 
 master pointed timidity Karg earlier | , literary way , proved strenuous 
 attempts , unsuspected plagiarisms , directed | propagandist novelty , 
 broader style greater clearness | ‘ Methods , ’ ‘ Studies Harmonium , ’ 
 finish . study Professor Teichmiiller | erudite work ‘ Die Kunst des Registrierens 
 produced greater reputation clavier-| Ein Hand- und Nachschlagebuch fiir Spieler alle : 
 virtuoso , period some| Harmoniumsysteme , ’ throw light 
 enterprising evfreprenevr , astute initiative , | freshness views organ colour , late 
 insisted special value double - barrelled | evinced strongly . 
 . ‘ Elert ’ added Karg , great admiration king o 
 Karg - Elert , willy - nilly . | instruments dared express 
 marvellous powers agents possess , | , submitted rigorous discipline 
 usurping office priest ! | technical studies , enabled act 
 Composition virtuosity fought hard with| accompanist soloist St. John , Liepst 
 Karg - Elert place , was| ‘ came flood organ compositions whic 
 destined victory . Harmonic knots delighted | sufficient te perfect insig " 
 infinitely , hopeless knot | instrument great technical attain 
 liked . conceivable contrapuntal | ments time . rearrangement , extensi0 ? , 
 device attempted , like Max Reger a|and altogether free translation o ! 
 similar period , temperament gifts mastery | successful harmonium pieces — E iis 
 led extravagance likewise | minor Passacaglia , Variationen , Improvisation inE 
 enormous output . songs| Interludium F sharp minor B 
 appeared lists Continental | Sonata , Phantasie Fugue D major , te 
 publishers time . is|Canzona G flat — followed 
 mistaking mastery tonal art | Choral Improvisations , wonderful group pie 
 power express pure noble thoughts . | following Bach lines procedure , hut 
 lhe pianoforte works period less| ancient modern style form . sine 
 equal value , subject | canonic treatment followed modern Fantasie 

 oy > ) = 

 | 
 younger British composer grim mood 
 years gay style - day . } proceeds quote instances music 

 choir excellent , capable assistance | Schumann , Handel , Beethoven , Tchaikovsky 

 Dunhill Mr. Josef } composers occur , vocal music , ! 
 Mr. Dunhill arranged concerts stressed word rhythmically weakes 
 Steinway Hall February 21 28 , March position , fourth eighth quaver ® 
 7 , special feature performance | - time , wedge , speak , wit 
 
 new works ! commonplace mechanical . 
 produced Pianoforte Phantasy - Quintet Mr. 
 James Friskin , Violin sonatas Mr. Nicholas Gatty 
 Mr. 
 Mr. Holbrooke , organized concerts 
 February 28 , 
 music 

 Mozart Operas . critical study . 3y Edward J. Dent , 
 Pp . xv . + 432 . ( London : Chatto & Windus 

 Edinburgh Review—/January , 1913 . Containing 
 article , ‘ New light Beethoven , ’ H. Heathcote 
 Statham . Price 6s . ( London : Longmans , Green & Co 

 Choir . plea beauty refinement Church 
 music . C. J. Viner . Pp . 36 . Price 2d . 
 ( Southbourne - - Sea : W. Harris 

 country short years . 
 Richter 
 playing British orchestras , imparting 
 noble dignified readings masterpieces 
 conducted . 
 thank Sir 
 possesses unique gift training orchestra 
 conveying clearly meaning 

 conductorship . 
 comic - opera seaside bands afford ; 
 came conducting ‘ begin Strauss , 
 Debussy , Tchaikovsky , Wagner , let 
 beware Bach , Beethoven , Brahms 
 end things , years 
 game , sufficiently master 
 forget technique 

 readings scripture . 
 Matins Epistle Mass. 
 psalm sung originally , , course 
 time , shortened , verse remained . 
 
 probably cause shortening , 
 melodic elaboration refrain , presumably owing tts 
 transferred congregation choir trained 
 singers . 
 place 450 550 

 M. Safonoff , famous conductor , heard 
 Bechstein Hall January 15 pianist , 
 capacity earned considerable repute younger 

 days . co - operated M. Belooussof , violoncellist 
 great expressive powers , performance 
 Beethoven Sonatas 

 - heralded Daniel Melsa , violinist , 
 appearance England Steinway Hall January 15 , 
 showed reports brilliance largely 
 based truth . programme gave opening 
 exceptional powers interpretation , sufficed exhibit 
 advanced technique refined , sensitive style . 
 violinist new London , M. Alexandre Sebald , 
 distinguish recital Bechstein Hall , 
 January 17 , performing Paganini - Caprices 
 violin . certainly justified stress laid 
 technical attainments . Miss Floriel Florean 
 gave evening South African Folk - Songs Bechstein 
 Hall January 21 

 complete cycle given , large 

 momentous feature o'clock 
 Concert given olian Hall January 9 
 performance , Misses Verne , Reger Variations 
 Fugue pianofortes theme Beethoven . 
 pleasant Miss Edith Clegg singing . 
 concert series , January 16 , provided rare | 
 pleasure hearing Brahms Trio horn ( Mr. A. E. 
 Brain ) , violin ( Miss Marjorie Hayward ) , pianoforte 
 Miss Mathilde Verne ) , finely played . Mr. Mostyn Bell 
 contributed sonys 

 Miss Dorina Zingari , appearing Steinway Hall 
 January 14 , showed greater capacity composer , pianist , 
 ind orchestral conductor usually expected } 
 girl sixteen 

 leadership Mr. Albert Geloso , London 
 appearance stein Hall January 20 , revealed 
 exceptional powers performance Cesar Franck 
 ( Quartet . produced musicianly Quartet written 
 Mr. B. Hollander . 
 excellently managed popular series concerts 
 progress Saturday evenings , Central ( Wesleyan ) 
 ITall , Westminster . January 18 , principal items 
 programme supplied Band H.M. Irish 
 Guards , conducted Mr. Charles Hassell 

 Mr. Russell Bonner concluded series 
 weekly pianoforte recitals Metropolitan Academy 
 Music , Forest Gate . recital , programme 
 vote audience 133 
 pieces played previous recitals . public choice 
 follows : Prelude , Rachmaninoff ; Sonata latheétique , 
 Beethoven ; ‘ Spring Song , ’ ‘ Duetto ’ ‘ Bee Wedding , ’ 
 Mendelssohn ; ‘ La Campanella , ’ ‘ Liebestraume ’ 
 * Tannhauser ’ March , Liszt ; ‘ Papillon , ’ Grieg ; Fantasia 
 ) minor , Mozart ; Rhapsodie B minor , Brahms 

 Becl 

 Knightly , 

 String Orchesin , 
 conducted Mr. Sydney Robjohns , gave concer 1 
 Streatham Hall December 17 . 
 works Bach , Beethoven , Coleridge - Taylor , Tchaikowk 
 solo artists Miss Violet Pem 
 Mr. Harold Wilde , Mr. Mark Hambourg , Miss Eite 
 Robjohns Miss Gladys Daniel played solo pati 
 Bach Concerto violins 

 programme inclote 

 firm hold public favour 1s retained 
 | weekly Symphonyand Popular Concerts , audiences 
 | Christmas season ranging high level numerically 

 exceptionally good performances Symphony 
 | Concerts fifth Symphonies Beethoven 
 } Tchaikovsky ; cases fine playing manifested 
 great care Mr. Dan Godfrey taken 
 preparation music , conductor nobly 
 backed instrumentalists . Kalinnikoff Symphony 
 } G minor , Dvorak F major Symphony , 
 | disappointment substituted Sibelius latest 
 | Symphony , Brahms second Symphony figured 
 recent programmes . interesting works 
 forms presented , calling special 
 mention Weingartner ‘ Joyous Overture ’ ( per- 
 formance England ) , orchestral version Steinberg 
 Bach Chaconne ( performance country ) , 
 Percy Grainger ‘ Molly shore ’ Mock Morris 
 | strings , Dr. Walford Davies new Suite ( suggested 
 | poesy Wordsworth ) . Turning soloists , pride 
 | place given delightful performance Messrs. 
 | Philip Levine Cedric Sharpe ( lately scholars 
 Royal College Music ) Brahms Double Concerto violin 
 violoncello ; reference omitted 
 performances Hans von Bronsart Pianoforte concerto , 
 Lalo ‘ Symphonie Espagnole ’ violin orchestra , 
 Grieg Pianoforte concerto , Mendelssohn Violin 
 concerto , Mrs. Davan Wetton , Mr. Anton Maaskoff , Miss 
 Myrtle Meggy , Miss Leila Doubleday , respectively 

 Monday ‘ Pops . ’ , , afforded enjoyable 
 means uninstructive afternoons . instance , 
 | concerts deal rise development 

 performance ‘ Messiah ’ New Year Day 
 ‘ popular ’ performance work January 16 . 
 Young Men Christian Association Choir , conducted 
 Mr. R. L. Reid , gave annual ‘ Messiah ’ concert 
 January 3 

 Choral Orchestral Union ninth Classical 
 Concert January 2 , Mr. Philip Halstead , clever Glasgow 
 musician , solo - pianist Schubert ‘ Wanderer ’ 
 Fantasia , orchestra . Symphony Beethoven 
 . 8 , lighter music included Grieg ‘ Peer Gynt ’ 
 Suite Dvorak ‘ Carneval . ’ programme 
 tenth Classical Concert January 7 offered striking 
 contrasts , example Overture ‘ ( Edipus 
 Colonos , ’ Bantock ( given time ) , 
 Haydn ‘ Oxford ’ Symphony , Strauss ‘ sprach 
 Zarathustra , ’ Schubert Overture ‘ Rosamunde . ’ 
 playing Scottish Orchestra concert 
 reached possibly highest level . main feature 
 eleventh concert January 14 remarkably fine 
 playing Mr. Arthur de Greef , Belgian pianist , 
 Grieg Pianoforte concerto minor Saint - Saéns 
 Pianoforte concerto . 2 , G minor . Mozart ‘ Jupiter ’ 
 Symphony Liszt Symphonic - poem ‘ Orpheus ’ completed 
 programme 

 Wagner Centenary celebrated Popular 
 Orchestral Concert , January 18 , _ orchestral 
 programme wholly drawn master works , 
 vocal numbers songs Mozart , Korbay , Cowen ! 
 Similarly Scottish Celebration Concert , January 25 , 
 orchestral programme suitable occasion , 
 vocal items Verdi Dunkels ! 

 results Hallé Treasurer appeal t 
 guarantors subscribers probably ko 
 end season , indications wanting tk 
 response desire . Dumy 
 year - end recess letters guarantors appeue 
 columns Guardian , plainly told ! 
 executive useless appeal help tot 
 outside public retain control 
 public pays voice voices 
 governing body . point appears l 
 divided opinions hesitating pronounceme 
 special meeting December . 
 executive unwilling unable realise 
 elementary fact sooner perishes better ; ide 
 asking public - spirited citizens help finan 
 privately - controlled society unnaturally leads 
 responses accompanied restrictive conditions . Ther : 
 way dilemma , far execu 
 hesitated permit admission supplement 
 public opinion . logic facts , , inexorable 
 end season witness deft 
 - modelling business . reais 
 Balling difficult task hand . finding 
 impossible adhere bold scheme new works ; 
 reason conjectured , compe 
 jettison cargo . time writing ! 
 remain orchestral concerts , app 
 small likelihood dozen promised works dm 
 performed . Societies 

 find 
 possible adhere programmes outlined 
 beginning season , , apart choral mign 
 hardly Hallé programme season 
 accordance prospectus published October . 
 tale recent Halle concerts soon 
 Mendelssohn ‘ Midsummer Night Dream ’ musi¢ 1 
 entirety , ‘ Messiah , ’ comment . * 
 early New Year concert ( January 9 ) brought Achille River 
 Beethoven Violin concerto Bach Chacot 
 Balling introducing Roger Ducasse Suite prove ! 
 little modernised Délibes ; , heart , 
 work young English school Birmingham ‘ 
 January 3 4 , comparing o ! Mase . 
 Bleyle , Klose , Charpentier , Volkmann , Ducasse , pays 
 Balling October , said te 
 Continental men hunt freshness idee 

 1S 

 Miss Isobel Hirschfeld played movement 
 Rubinstein fourth Pianoforte Concerto , Mr. Dalton 
 Baker sang songs Mr. Easthope Martin , accompanied 
 composer 

 CHRISTCHURCH ( N.Z.).—At concert Musical 
 Union , given November 26 , Beethoven ‘ King Stephen ’ 
 Overture Larghetto second Symphony , 
 Mendelssohn G minor Pianoforte concerto ( Miss 
 Rima Young soloist ) , movements 
 Schumann Pianoforte quintet , chief features 
 programme . conductor orchestral pieces 
 Mr. W. S. King 

 HANLEY.—The performance ‘ Messiah ’ given 
 Glee Madrigal Society December 19 surpassed , 
 opinion , choir previous achievements 
 interpretation work . notable purity 
 voice - quality , fluency vocalisation , power 
 sustaining tone . legitimate individual effects 
 Mr. John James reading score added attraction 
 interest . solo parts sustained Miss Eva 
 Rich , Miss Florence Taylor , Mr. Frank Mullings , 
 Mr. Robert Burnett . Mr. Ernest Hammond 
 organ 

 Joseph Holbrooke , played . Press accorded 
 venerous — Cases enthusiastic — praise 
 compositions , conductor , excellent orchestra , 
 ‘ , ’ says Sigva / e , ‘ man , 
 young , artist . "-—The famous Russian Ballet 
 number successful appearances . well- 
 known works répertoire , new ballets , ‘ Der 

 yw Igor Stravinsky , given.——Herr Weingartner 
 concluded concerts Fiirstenwalde performance 
 Beethoven ninth Symphony . public banquet 
 given honour Berlin , élite classes 
 Berlin society present . year concerts 
 given Firstenwalde . ‘ Schauspielouverture ’ 
 Erich Wolfgang Korngold played time 
 fifth Philharmonic Concert , conductorship 
 Professor Arthur Nikisch.——Under direction 
 Fritz Steinbach , Weingartner new   ‘ Lustige- 
 Ouverture ’ performed time Berlin 
 second concert ‘ Gesellschaft der Musikfreunde 

 programme included Mozart Serenade B flat 
 major thirteen wind instruments.——At Herr Oskar 
 Fred symphony concert , E. N. von Reznicek 
 ymphonic - poem , ‘ Schlemihl , ’ work considerable 
 complexity , produced composer direction , 
 proved interesting . — — Hadley orchestral rhapsody , 
 ‘ culprit Fay , ’ Suite Alfredo Casella , 
 vere included programme symphony 

 BRUSSELS 

 Théatre Royale de la Monnaie , Beethoven 
 ‘ Fidelio ’ recently performed - fifth time 
 revival season — record credit 
 taste Brussels public.——Pursuing scheme 
 performing classical operas original form ( inaugurated 
 year ) , Messrs. Kufferath Guidé presented 
 Mozart ‘ Magic flute , ’ musical direction Herr 
 Otto Lohse . concert Société J. S. Bach , 
 secular cantata ‘ Mer han en neue Oberkeet ’ heard 
 great pleasure . occasion Professor Julius 
 Buths played Pianoforte concerto F minor , 
 string orchestra , excellently 

 CHICAGO 

 Germany , 
 HALLE 

 Sgambati ‘ Requiem ’ performed Robert 
 Franz - Singakademie . concert given 
 choir devoted smaller choral works , programme 
 included Humperdinck ‘ Wallfahrt nach Kevelaar , ’ Max 
 Bruch ‘ Flucht nach Aegypten , ’ ‘ Wanderers Nachtlied ’ 
 Karl Klanert , ‘ Elfenlied ’ ‘ Christnacht ’ Hugo 
 Wolf , performed time — 
 Beethoven rarely heard Triple Concerto pianoforte , 
 violin , violoncello , orchestra played 
 Winderstein concert 

 HAMBURG 

 Gesangverein . feature concert 
 performance number old English madrigals . Max 

 Reger conducted ‘ Symphonic prologue tragedy ’ 
 second concert Verein der Musikfreunde , 
 occasion took Herr Kunsemiiller 
 performance ‘ Variations pianofortes ona 
 theme Beethoven 

 KREFELD 

 answer question depends date cf 
 publication . work published July 1 , 1912 , 
 copyright years author death . 
 years , - period absolute 
 copyright — second - years period 
 print work ( subject Board 
 Trade regulations ) paying proprietor 
 copyright royalty equal 10 cent . published 
 price work — z.c . , price chooses 
 publish . work published July 1 , 1912 , 
 copyright continued day , copyright 
 years author death , 
 period divided thirty years years instead 
 equal periods - years . answer 
 second question , — special exercise 
 Royal Prerogative 

 A. B.—The average metronome rates studies 
 pieces Associated Board Higher Division Pianoforte 
 Examination , 1913 , taken follows : Z#s¢ — 
 Loeschhorn .=-80 ; Pauer # 2138 ; Bach o=104 ; 
 Duncan » .-116 ; List B — Loeschhorn # = 126 ; Heller 
 @-120 ; Farjeon # .=76 ; Burgmuller @-126 ; Lzst C — 
 Czerny @ 152 ; Steibelt # = 144 ; Beethoven o.-69 ; 
 Spindler = .=72 . |The quick movements taken 
 slower tempo given technique 
 inadequate 

 FInGERS.—Everything depends ‘ method ’ 
 prefer . - - date book Tobias Matthay 
 ‘ act touch ’ ( Longmans ) . heavy 
 digest begin ‘ principles pianoforte 
 playing . ’ excellent works , ‘ Technique 
 Expression ’ Franklin Taylor ( Novello ) , ‘ 
 Leschetitzky method ’ ( Curwen 

