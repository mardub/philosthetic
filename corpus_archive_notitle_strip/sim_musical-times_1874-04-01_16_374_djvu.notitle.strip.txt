Monday, 20th April, 1874, 
SIR Jj. BENEDICT’S “ST. PETER

Goa Best SOCIETY, Beethoven Rooms, 27, 
Harley-street, W. President, Sir Julius Benedict. Director, 
Herr ScHuBerTH. Eighth Season, 1874. The SECOND CONCERT 
will take place on WEDNESDAY, 29th ApRIL, 1874. The first part of the 
Programme devoted to R. Schumann’s Vocal and Instrumental Com- 
positions. The Society affords an excellent opportunity for young 
rising artists to make their appearance in public

LOUCESTER COUNTY ASYLUM, near 
Gloucester.—_MALE ATTENDANTS WANTED. Wages to 
commence at {24 per annum, with Board, Lodging, and Washing. 
(Musicians preferred.) Previous Asylum experience not necessary. 
Applications, stating age, with musical capabilities, to be sent to the 
Superintendent at the Asylum

CRYSTAL PALACE

Tue Symphony, by Mr. Ebenezer Prout, given for the 
first time, on the 28th February, achieved a success, which 
proves that there is always an audience ready to appreciate 
and acknowledge worth, wherever it may be found; and 
much credit is due to Mr. Manns, whose love for the music 
of his own countrymen is well known, for affording a hear- 
ing to awork of suchimportance byan Englishman. Upon 
so carefully planned a composition as Mr. Prout’s Sym- 
phony it would be unjust, on a single hearing, to say more 
than that it impresses us with a conviction that it is the 
production of a profound thinker upon his art, for every 
movement has a defined plan; the subjects are melodious, 
and skilfully treated; and the instrumentation shows an 
intimate knowledge of orchestral effect. The début of Miss 
Emma Barnett, who played Beethoven’s pianoforte con- 
certo in G, was another marked feature in the concert, her 
performance being remarkable throughout for intelligence, 
fluency of execution, and rhythmical accuracy. The two 
cadences, written by her brother, Mr. J. F. Barnett, were 
excellently played, and elicited a well-deserved burst of 
approbation. At the following concert Beethoven’s fine 
music to ‘‘ Egmont” was given, and received with en- 
thusiasm. Brahms’s variations, for the orchestra, on a 
theme by Haydn, which was also performed, is a remark- 
able work, the originality and variety of the composition, 
and the masterly instrumentation of each variation, keep- 
ing the attention of the hearers alive to the last note. It 
was loudly applauded, and will, we trust, frequently have a 
place in these excellent concerts. Of Schubert’s Otett, 
which was played, on the r4th ult., with all the stringed 
instruments of the orchestra, and the wind instruments 
doubled, we prefer not to speak, as we have on more than 
one occasion freely expressed our opinion upon this desecra- 
tion of a fine work. Mr. Sullivan’s Overture to “‘ The 
Sapphire Necklace’ opened the concert, on the 21st ult., 
with much spirit; and after the hackneyed ‘“ Qui la voce,” 
well sung by Madame Sinico, came Mendelssohn’s ever- 
welcome violin concerto, which was played as only Herr 
Joachim can play it. The only absolute novelty in the 
programme was the “Song of Destiny” (Schicksalslied), 
for chorus and orchestra, by Brahms, which shared the fate 
of most novelties, in being indifferently performed, the first 
movement demanding, on the part of the chorus, an amount 
of refinement, which it hardly received at the hands of the 
Crystal Palace choir. Without entering into an analysis

448 THE MUSICAL TIMES.—Aprit 1, 1874

of its merits, we may mention that it was recognised by its 
hearers as a work of genius of the highest order, and in this 
opinion we cordially agree. The verdict which had been 
given by nearly the whole of musical Germany upon the 
composition was on this occasion unanimously endorsed by 
perhaps the most critical audience ever assembled at 
Sydenham ; and Herr Brahms may reckon at least upon a 
cordial reception being accorded to any future work from 
his pen. The English translation of the text is by the 
Rev. J. Troutbeck, M.A. Beethoven’s Symphony (No. 4, 
in B flat) received such a rendering as can hardly be looked 
for outside the Crystal Palace—the Adagio narrowly escap- 
ing an encore, which, but for the extreme length of the 
concert, would have been inevitable. The next item of 
interest was to a great extent spoiled, by being placed at the 
end of this too long programme. When will English 
people become alive to the fact that high-class music can- 
not be properly listened to without some fatigue to the 
brain; and that, if one great work follows close upon 
another at the same concert, the attention must after a 
time become wearied? Who has not experienced a sense 
of fatigue produced by the regulation two Overtures, two 
Symphonies, and a Concerto, which form the ordinary 
orchestral programme of most of our classical concerts? 
In this, if in nothing else, a good example was given 
at the ‘* Exhibition Concerts” of last year, where an Over- 
ture, a Symphony or Concerto, some ballet music, and a 
March, with two vocal pieces, formed a programme which 
satisfied without satiating. To return—the orchestral 
variations of Brahms produced less effect than they had 
done on their first performance at these concerts, and the 
reason is not far to seek: they came after two hours anda 
quarter of classical music without a break

HER MAJESTY’S OPERA

WAGNER SOCIETY

THE fifth concert, on the 13th ult., again contained those 
popular extracts from Wagner’s works which can but 
increase the longing to hear them in their only legitimate 
place—the Operatic stage. The chorus of Messengers of 
Peace, from the early Opera “ Rienzi,’ almost made us 
wish that the so-called development of the composer’s 
genius had been arrested, so thoroughly legitimate are the 
vocal effects, and so pure and attractive is the melody. 
The Overture to “ Die Meistersinger von Nurnberg,” was 
finely played and received with enthusiasm ; and the choral 
song ‘* Wachet auf,” from the same Opera, was encored. 
A warm welcome was accorded to the specimens from 
“Lohengrin,” the Prayer and Finale to the first Act 
eliciting the most solid marks of approval. Beethoven’s 
“‘ Choral Fantasia” did not go well, notwithstanding that 
the pianoforte part was ably rendered by Mr. Walter 
Bache; but the same composer’s Overture to ‘“ King 
Stephen,” although, as we conceive, out of place at these 
concerts, received ample justice. Ina song by the Abbé 
Liszt, Miss Sterling, created a marked effect, and she also 
gave with good expression Rubinstein’s ‘‘ Die Waldhexe,”’ 
both of which were accompanied on the pianoforte by Mr. 
Dannreuther, who conducted the concert with his usual 
energy and skill

BRITISH ORCHESTRAL SOCIETY

public opinion : ‘‘ Thus far shalt thou go, and no farther.” 
The addition of a seventh string to the lyre aroused 
opposition. Similar obstructiveness attends every pro- 
gressive effort. The very strife yields fruits of peace. 
This is expressed in your recent review of a “letter” 
from Wagner, as well as tacitly acknowledged by the 
spirit of Mr. Joseph Bennett’s carefully worded contribu- 
tion on the same subject, in the Number of your ably 
edited Journal for March. The leading article temperately 
approaches the very pith of the whole matter, and candidly 
sets forth the new views. Yet permit me to add Ido not think 
he has made a single hit at the great musician. All that 
he says—and says so eloquently and so well—only rivets 
in my mind the continuously growing and deepening 
conviction that Wagner is right, and that his opponents 
do not understand him. It is not my province to re- 
introduce invective. But let me be allowed simply to 
State that in my view all your conclusions are foregone. 
It is to meas clear as sunlight that music must grow. To 
utter over again the oft repeated words of -adverse parties 
would be irksome; still, I cannot help saying, in briefly 
stating the case as it occurs to my mind, where I deem 
Wagner’s opponents in error

All he says of Beethoven is truly said. The composers 
dismissed as having “nothing to tell,” are justly dismissed. 
To the project of separating the ‘‘man’”’ from his 
‘theory,’ I say, ‘‘No.’? That music without poetry is 
but half an arch, “ Yes.’”’ Our programme makers know 
this fact so well, that they will, if they cannot find a legend 
attached to an instrumental piece, invent one and tack it 
on. It may be the “Lion in love,” to a concerto of 
Beethoven; or the ‘“‘ Shake of the evil spirit,” to a violin 
solo. Some line of poetry, legend, story, they must and 
will have. Illustrations flow ad infinitum. Do composers 
work ina comatose state? ‘‘ Yes.” Beethoven knew what 
he did? ‘“No.’? Beethoven left no record of his struggles? 
I say, “Yes, he did.” Why not write more choral 
symphonies? ‘ Straight jacket would have resulted.” 
Instrumental music incomplete? ‘ Certainly

Other allusions I am compelled to pass by, though I 
could, with perhaps more pleasure to myself than to others, 
expatiate on each head of the question at considerable 
length. I should not now have written did I not believe 
the subject of paramount importance in the pursuit of art 
to every one, as well as to

This is a logical definition from my definitions. But the 
public, who in such matters must not be put altogether 
aside, afford a practical proof of it. The same public who 
would sit four hours, and enjoy every note of one of 
Meyerbeer’s Operas, at the end of one hour of instrumental 
music would be tired—at the end of four hours would wish 
they might never hear a note of music again as long as 
they lived

It may be retorted that we are not to pander to the 
people—true. Still a musical audience that can appreciate 
Mozart, Meyerbeer, and Beethoven’s Operas, may be taken 
as a fair criterion of a healthy state of ear, which is not 
the case with all musicians

Instrumental performers, professional and amateur, 
particularly players on stringed instruments, become 
passionately fond of their instruments from the difficulties 
they have surmounted in acquiring them (often, indeed, let 
us charitably suppose, mostly unconsciously), and become 
an intolerable bore to others not so circumstanced. 
Indeed (while omitting the works of the great masters, who 
excelled in everything they took in hand) I think we may 
refer the very origin and continuance of most of the string 
quartetts and quintetts to this peculiarity. In some cases 
this enjoyment is shared by the hearers ; thus, in hearing 
that execrable combination of runs, strums, and trills, 
Thalberg’s ‘‘ Home, Sweet Home,” a hearer is struck by 
the labour required to learn it, and is startled and 
interested at the ‘‘ sleight of hand” required by it. In other 
cases the difficulty produces pleasure in both hearer and 
player; in another way, take for example the fugue in D 
minor (No. 4) by J. S. Bach, for violin alone ; everybody: 
knows how much better it would be on four instruments, 
indeed a person would say, d priori, that a fugue for one 
instrument was an impossibility. But the masterly way he 
gives the principal parts out in his ingenious devices for 
stretto, pedal, &c., constitute an excellence which will 
commend itself to the cultured musician to the end of 
time. The Opera is, and must be, the highest excellence, 
as I shall further show farther on when I treat of the first 
thesis temporarily omitted. Therefore I admit that ‘‘Instru- 
mental music is incomplete, and needs the aid of words

Let us now see how Wagner is the first to contradict 
himself. In his Opera, “‘ Le Vaisseau Fantéme,” he says 
his Overture (instrumental of course) is in itself the com- 
plete development of the libretto. Thus (sec. 1) is a 
description of a ship cast among rocks, (sec. 2) the rocks 
don’t suffer it to strike, (sec. 3) the crew sign a compact 
with the evil one in presence of captain, &c

What melody, chord, or rhythm can give the idea of the 
substantives printed in italics? I leave to the reader to 
discover ; I cannot help him in any degree. Here, then, we 
have him wilfully rushing into ‘“‘ Beethoven’s error,” with- 
out Beethoven’s excuse of ignorance, and “ yearning” 
after truth

Let us farther see how Wagner embodies this error in 
his doctrines. He says music (instrumental, presumably 
from the context I take this from) should produce in one 
the same effect as a quiet walk at sunset in a wood far

461

Of the others, suffice it to say that they all lack the regular 
phrased style (the very soul of true music) of Mozart, 
Gluck, Meyerbeer, &c., and even of the Italian school, the 
fervent energy of Beethoven, ana the bold harmonies of 
Handel

I am, Sir, yours, &c., 
Joun W. Hinton, A.B., Mus.B

ueen was a most powerful rendering of a very trying part. One of 
the main excellences of the performances lay in the chorus singing. 
It was always good, and at times most artistic. Altogether, the opera 
in all its departments—grouping of the supernumeraries, gipsies, 
soldiers, and courtiers—was the most completely satisfactory this 
company has ever given

EpinBuRGH.—In connection with the visit of Mr. Mapleson’s Italian 
Opera Troupe, two excellent concerts were given by the Edinburgh 
Choral Union to crowded houses. The first, on February 21st, com- 
prised in the first part a selection of solos and choruses from Handel's 
works, and in the second part, operatic and other solos. The prin- 
cipal artists were Mdlle. Titiéns, Mdme. Sinico, Mdme. Trebelli- 
Bettini, Mdlle. Macvitz, Signori Bettini, Borella, Perkin, and Agnesi. 
Mr. F. H. Cowen presided at the pianoforte. At the second concert, 
on the r4th ult., Handel’s Messiah was given, with complete orchestra, 
and Madlle. Titiens, Mdme. Trebelli-Bettini, Signor Fabrini and Signor 
Agnesi as principals. The chorus, which numbered about 250 voices, 
sang throughout with great care and intelligence; and Mr. Adam 
Hamilton, who conducted at both concerts, deserves great credit forthe 
high state of efficiency to which he has brought them. The organ- 
ist was Mr. T. Hewlett, Mus.Bac., Oxon, whose playing contributed 
greatly to the success of the concerts——PRoFESSOR OAKELEY gave 
an Organ Recital in the Music Class Room, on the 12th ult., when an 
excellent programme was performed to alarge audience. Bach's prelude 
and fugue in E minor were remarkably well played by astudent. Pro- 
fessor Oakeley’s song, ‘‘’Tis not alone that thou art fair,” was encored, 
when he played his little canzonet, “Sempre pili t’amo.”——THE 
seventh annual concert of the University Musical Society was given on 
Wednesday evening, the 18th ult., in the Music Hall, before a large 
audience. The number of choristers, amounting to 200, have attained 
a standard of singing far beyond that of former years, and an equal 
improvement is observable in the orchestra. Professor Oakeley, on 
coming forward to assume the conductor’s place, was greeted with 
demonstrative applause from the audience and the choristers. The 
concert, which was miscellaneous, opened with the students’ song, 
“‘Condiscipuli Canamus,” composed by Professor Oakeley for the 
concert of 1869 to Latin words by Professor Maclagan. Mr. Galletly, 
a student, played, with Mr. Carl Hamilton, Beethoven's sonata in F, 
Op. 17, for pianoforte and violoncello, originally written for piano and 
horn. Beethoven's first Symphony commenced the second part of the 
programme; and another promising young student-pianist, Mr. 
Coates, played Schumann's “ Schlummerlied,” and Schubert’s Im- 
promptu in A flat with great neatness, light touch, and clear articula- 
tion. The concert, which was highly successful, ended with the 
“ National Anthem

EvertTon.—The annual concert of the Choral Society was given on 
Tuesday evening, the roth ult.,in the Schoolroom, which was well filled 
by a large and fashionable audience. _Under the conductorship of Mr. 
Hamilton White, the organist of the Parish Church, East Retford, the 
Society has made great progress, and the choral pieces were given with 
general excellence. Songs and duets were contributed by Miss Annie 
Williamson, Miss Naylor, the Misses Postlethwaite, and Miss Roe, 
assisted by Mr. Dimock of Retford; and Mr. White delighted his audi- 
ience by playing a pianoforte solo

This is a logical definition from my definitions. But the 
public, who in such matters must not be put altogether 
aside, afford a practical proof of it. The same public who 
would sit four hours, and enjoy every note of one of 
Meyerbeer’s Operas, at the end of ove hour of instrumental 
music would be tired—at the end of four hours would wish 
they might never hear a note of music again as long as 
they lived

It may be retorted that we are not to pander to the 
people—true. Still a musical audience that can appreciate 
Mozart, Meyerbeer, and Beethoven’s Operas, may be taken 
as a fair criterion of a healthy state of ear, which is not 
the case with all musicians

Instrumental performers, professional and amateur, 
particularly piayers on stringed instruments, become 
passionately fond of their instruments from the difficulties 
they have surmounted in acquiring them (often, indeed, let 
us charitably suppose, mostly unconsciously), and become 
an intolerable bore to others not so circumstanced. 
Indeed (while omitting the works of the great masters, who 
excelled in everything they took in hand) I think we may 
refer the very origin and continuance of most of the string 
quartetts and quintetts to this peculiarity. In some cases 
this enjoyment is shared by the hearers ; thus, in hearing 
that execrable combination of runs, strums, and trills, 
Thalberg’s ‘‘ Home, Sweet Home,” a hearer is struck by 
the labour required to learn it, and is startled and 
interested at the “ sleight of hand” required by it. In other 
cases the difficulty produces pleasure in both hearer and 
player; in another way, take for example the fugue in D 
minor (No. 4) by J. S. Bach, for violin alone ; everybody. 
knows how much better it would be on four instruments, 
indeed a person would say, d priori, that a fugue for one 
instrument was an impossibility. But the masterly way he 
gives the principal parts out in his ingenious devices for 
stretto, pedal, &c., constitute an excellence which will 
commend itself to the cultured musician to the end of 
time. The Opera is, and must be, the highest excellence, 
as I shall further show farther on when I treat of the first 
thesis temporarily omitted. Therefore I admit that ‘‘Instru- 
mental music is incomplete, and needs the aid of words

Let us now see how Wagner is the first to contradict 
himself. In his Opera, “ Le Vaisseau Fantoéme,” he says 
his Overture (instrumental of course) is in itself the com- 
plete development of the libretto. Thus (sec. 1) is a 
description of a ship cast among rocks, (sec. 2) the rocks 
don’t suffer it to strike, (sec. 3) the crew sign a compact 
with the evil one in presence of captain, &c

What melody, chord, or rhythm can give the idea of the 
substantives printed in italics? I leave to the reader to 
discover; I cannot help him in any degree. Here, then, we 
have him wilfully rushing into ‘* Beethoven’s error,” with- 
out Beethoven’s excuse of ignorance, and “ yearning” 
after truth

Let us farther see how Wagner embodies this error in 
his doctrines. He says music (instrumental, presumably 
from the context I take this from) should produce in one 
the same effect as a quiet walk at sunset in a wood far

461

Ofthe others, suffice it to say that they all lack the regular 
phrased style (the very soul of true music) of Mozart, 
Gluck, Meyerbeer, &c., and even of the Italian school, the 
fervent energy of Beethoven, and the bold harmonies of 
Handel

I am, Sir, yours, &c., 
Joun W. Hinton, A.B., Mus.B

greatly to the success of the concerts.——ProFESsOR OAKELEY gave

an Organ Recital in the Music Class Room, on the 12th ult., when an 
excellent programme was performed to alarge audience. Bach’s prelude 
and fugue in E minor were remarkably well played by astudent. Pro- 
fessor Oakeley’s song, ‘‘’Tis not alone that thou art fair,” was encored, 
when he played his little canzonet, “Sempre pid t’amo.”"——THE 
seventh annual concert of the University Musical Society was given on 
Wednesday evening, the 18th ult., in the Music Hall, before a large 
audience. The number of choristers, amounting to 200, have attained 
a standard of singing far beyond that of former years, and an equal 
improvement is observable in the orchestra. Professor Oakeley, on 
coming forward to assume the conductor’s place, was greeted with 
demonstrative applause from the audience and the choristers. The 
concert, which was miscellaneous, opened with the students’ song, 
‘‘Condiscipuli Canamus,” composed by Professor Oakeley for the 
concert of 1869 to Latin words by Professor Maclagan. Mr. Galletly, 
a student, played, with Mr. Carl Hamilton, Beethoven’s sonata in F, 
Op. 17, for pianoforte and violoncello, originally written for piano and 
horn. Beethoven's first Symphony commenced the second part of the 
programme; and another promising young student-pianist, Mr. 
Coates, played Schumann’s “Schlummerlied,” and Schubert’s Im- 
promptu in A flat with great neatness, light touch, and clear articula- 
tion. The concert, which was highly successful, ended with the 
“ National Anthem

EvertTon.—The annual concert of the Choral Society was given on 
Tuesday evening, the roth ult.,in the Schoolroom, which was well filled 
by a large and fashionable audience. _ Under the a of Mr. 
Hamilton White, the organist of the Parish Church, East Retford, the 
Society has made great progress, and the choral pieces were given with 
general excellence. Songs and duets were contributed by Miss Annie 
Williamson, Miss Naylor, the Misses Postlethwaite, and Miss Roe, 
assisted by Mr. Dimock of Retford; and Mr. White delighted his audi- 
ience by playing a pianoforte solo

The first part of the programme was miscellaneous, comprising 
Overtures, excellently performed by the military band, under the 
direction of Mr. J. J. Murdoch; songs and duets, by Miss Joubert, the 
Rev. C. Darroch, and Mr. Julius Carey; and pianoforte solos by Mr. 
and Mrs. Brion. The second part was devoted to Mr. Brion’s Cantata, 
Marathon, the solo parts in which were admirably sustained by Miss 
M. G. Sheppard and Messrs. G. Sheppard and C. Korner. The 
choruses were exceedingly well rendered by the choir; and the entire 
performance was most successful. The Cantata, which is scored for 
a military band (there being no strings on the island), was very well 
accompanied

LreaMINGTON.—Mr. Julian Adams gave a very successful Chamber 
Concert on Saturday, February the 28th. Mendelssohn’s pianoforte 
trio, in D minor, in which Mr. Adams had the co-operation of Herr 
Otto Bernhardt (violin) and Mr. Turner (violoncello), was rendered 
with great vigour and marked emphasis. Another important work of 
the same class, Mayseder’s trio in B flat, was also admirably played by 
the above-named executants. The vocal portion of the concert was 
sustained by Miss Katharine Poyntz and Mr. Henry Pyatt, the latter 
being encored in Rockstro’s song, ‘“‘ The Reefer.” The great attraction, 
however, was the solo performance of Mr. Julian Adams; his bravura 
style of playing and mechanism being fully displayed in a Fantasia on 
Russian Airs, which was vociferously encored, when he responded 
by playing a musical sketch, of his own composition, ‘“‘ Les Patineurs.” 
The Sonata in G, for pianoforte and violin (Beethoven), was also 
performed with finished execution

Ler.—An amateur concert, in aid of a local benevolent case, was 
given on the 27th February in the Belmont Park Rooms, under the 
conductorship of Mr. Charles J. Frost. The programme consisted of 
part-songs, trios, and songs. Mr. Frost’s new part-song, ‘‘ The Winds,” 
was rendered by Miss Maberley, Miss Austin, Mr. Law, and Rev. John 
Kempthorne; and Sir F. A. G. Ouseley’s charming little trio, “In 
the sight of the unwise,” by the Misses Bumpus and Miss Austin. 
Mr. Frost’s playing (from memory) of Beethoven’s Sonata, Op. 10, 
No. 1, in C minor, and Schubert’s F minor Impromptu, gave un- 
qualified satisfaction. The concert was very successful

LeicesTER.—The Leicester Musical Society gave the last concert 
for the season 1873-4, in the Temperance Hall on Tuesday evening, the 
roth ult., before a large audience, when Mendelssohn’s Oratorio Elijah 
was performed. The principal vocalis&$ were Madame Thaddeus Wells, 
Miss D’Alton, Mr. Henry Guy, and Mr. Santley. Madame Wells was 
very efficient in the part of the Widow. Miss D’Alton was also very 
successful, her rendering of ‘‘Oh, rest in the Lord,” eliciting the 
warmest plaudits. Mr. Santley was in magnificent voice, and he sang 
the part of Elijah with all his well-known power and vigour, his de- 
clamation in the recitatives, and his exquisite interpretation of the airs 
being characterised ‘by all those varied excellences which have made 
his name so famous. Mr. Guy executed his share of the solos in a 
highly satisfactory manner. In one or two of the trios, as well as in 
the double quartets, the principal vocalists were ably assisted by the 
Misses. Deacon and Clowes, and Messrs. Adcock and F. M. Ward. 
The various choruses were exceedingly well executed by the Society. 
The performance of the band, which was especially strengthened for 
the occasion, was equally satisfactory, the accompaniments being on 
the whole as finished as they were judicious. Mr. Nicholson officiated 
as conductor

YaRMouTH.—A concert was given at the Masonic Hall, on Wednes- 
day evening, the 4th ult., by the members of Mr. Deane’s choral class. 
The first part consisted of Haite’s Cantata, Abraham’s Sacrifice, 
a work never before produced in this vicinity, and which deserves 
to be better known. The music allotted to Abraham was effec- 
tively rendered by Mr. C. Panchen (Bass). Miss Botwright, Miss 
E. Cole, and Messrs. Bly and Smith, also acquitted themselves most 
creditably. We must not omit to mention the instrumental piece, 
“The ascent of the mountain,” which was beautifully given by the 
band. The second part was miscellaneous. The instrumental 
quartet from Robert le Diable was warmly encored. Miss Hulley 
played first violin, Mr. omg second violin, Mr. Deane violoncello, 
Mr. A. Howard bass; Mrs. Panchen accompanied; and Mr. Deane 
conducted

Yorx.—The last of the series of winter concerts was given on 
Tuesday night, the roth ult., when an instrumental programme was 
erformed in a manner that gave the utmost pleasure to all present. 
osart’s charming quartet in D minor was a special feature of the 
evening. Mr. Hallé played Beethoven's E flat Sonata, Op. 31, in his 
usual facile style and finished manner; and Madame Norman-Neruda 
contributed a solo on the violin. The quintet of Schumann’s, in E flat, 
ended the programme. Miss Ferrari was the vocalist

OrcAN APPOINTMENTS.—Mr. Geo. W. R. Hoare, to St. James’ 
Church, Clapham Park.——Mr. George Kitchin, to Holy Trinity, 
Sydenham.——Mr. E. F. Seppings, to St. James’, Littie Heath, Chad- 
well, Essex.——Mr. R. Peel, assistant organist to St. Mary’s, Wigan, 
Lancashire.——Mr. Frederick G. Cole, to St. Mary’s, Staines. ——Mr. 
E. J. Mummery (assistant organist of Christ Church), to Castle Church, 
Stafford.——Mr. R. Nottingham, organist and choir master to Winder- 
mere Parish Church, and choir master to the Choral Union, Winder- 
mere.——Mr. E. J. Griffiths, organist and choir master to the Parish 
Church, Broadstairs, Kent. Mr. Frederick G. Edwards, to Surrey 
Chapel (Rev. Newman Hall’s), Blackfriars Road, S.E-——-Mr. Thomas 
Edward Trotter, to St. Saviour’s, Brockley Road, Forest Hill.——Mr. 
H. Walmsley Little, organist and choirmaster to Christ, Church, 
Woburn Square, Bloomsbury

