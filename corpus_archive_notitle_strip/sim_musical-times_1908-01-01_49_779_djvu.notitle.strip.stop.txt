ischa Elman plays fine Amati violin . played Court , visit 
 repertoire large , e.g. : King Queen Spain . King Edward genially 

 rtos.—Bach minor ; Brahms ; Bruch G minor 
 ) minor ; Beethoven ; Glazounoff minor ; Lalo 
 1 ) minor ; Mendelssohn ; Mozart ; Paganini D ; 
 Spohr minor ( Gesangscene ) ; Saint - Saéns B minor ; 
 Tchaikovsky ; Viotti minor , . 22 ; Wieniawski , Xc . 
 neertstiicke . — Romances F G , Beethoven ; 
 Mclodies Hongroises Othello Fantasia , Ernst ; Suite , 
 Mackenzie ; Suite minor , Sinding ; Rondo Capriccioso , 
 Saint - Saéns ; Faust Fantasia , Wieniawski , & c. 
 Sonata Bach , Beethoven , Brahms , Corelli , 
 Handel , Mozart , Senaille , Tartini 

 Grieg 

 seasons succession — unprecedented 
 honour — playing Gewandhaus Concerts , 
 Leipzig 

 remarkable thing Mischa 
 playing , especially young , 
 moods interprets compositions 
 opposite character . concertos Beethoven , 
 Brahms Tchaikovsky — different 
 atmosphere , young Elman breathes , 
 speak , natural air plays 
 marvellous technique , absolute certainty 
 ( memory ) rare poetic insight , 
 masterpieces . played baton 
 great conductors Europe . testimony 
 distinguished quoted . 
 Herr Arthur Nikisch enthusiastic 
 close rehearsal Tchaikovsky concerto 
 said : ‘ , dear Mischa , 
 concert play 
 , wish played better . ’ 
 concert said : ‘ played 
 concerto twice rehearsal ; 
 thought played like . ’ 
 Dr. Richter regard Brahms 
 concerto ? Going artists ’ room 
 conducted Strauss tone - poem , rushed 
 boy said : ‘ Beautiful — 
 proud , dear Elman ; 

 chatted boy minutes 
 career , evincing greatest interest 
 performances : times played 
 Queen Alexandra . Asked hobbies , 
 Mischa replies , ‘ motoring . ’ motor car 
 luxury Elman family indulge , 
 live quietly modest house . regard 
 English music , Mischa says , ‘ greatly admire 
 Elgar : regret Elgar written 
 violin . wish compose 
 concerto , great success . ’ 
 \lthough gifted youth hitherto 
 played Europe , eagerly awaited America 
 Australia . continents hopes soon 
 , add fame genius 

 22 October , 1877 

 prospectus ninety - sixth season 
 Philharmonic Society issued . 
 actual novelties presented : Viola concerto 
 York Bowen , Violin concerto Hubay , 
 Vocal scena ( ‘ Bells ’ ) Franco Leoni , 
 Symphony C Sibelius , named 
 conducted composer . symphonies 
 Beethoven C minor , Dvorak ‘ New 
 World , ’ Schubert C , Schumann D minor C , 
 Tchaikovsky E minor . Overtures 
 played Egmont Leonora . 2 , 
 Beethoven ) , Academic ( Brahms ) , Comedy ( Harty ) , 
 Flying Dutchman ( Wagner ) . Orchestral music 
 exemplified Brandenburger 
 concerto strings , . 3 ( Bach ) , Prelude 
 ‘ Sappho ’ ( Bantock ) , Symphonic variations ( Dvorak ) , 
 Variations original theme ( Elgar ) , Spring 
 Winter , symphonic suite ‘ Seasons ’ 
 German ) , Funeral march ( Grieg ) , Rhapsody . 1 
 Liszt ) , Symphonic poems ‘ Finlandia ’ ( Sibelius ) 
 ‘ Till Eulenspiegel ’ ( Strauss ) , ‘ Death Apotheosis ’ 
 Strauss ) , Introduction closing scene 
 ‘ Tristan , ’ Waldweben ‘ Siegfried ’ ( Wagner ) . 
 addition mentioned , concerto 
 portion scheme consists Brahms B flat 
 D’Albert E flat , Schumann minor , 
 pianoforte ; Bach violins ; Mozart 
 represented Serenade nocturne . 6 , 
 D , string quartet orchestra . Mendelssohn 
 idol Philharmonic Society , finds 
 place prospectus 

 M. Claude Debussy announced conduct 
 orchestral nocturnes Queens Hall 
 Symphony concert February 1 . ‘ , 
 public appearance England , undoubtedly 
 stimulate interest work 
 original fascinating composers day . 
 performances given London 
 works M. Debussy known repute 
 ‘ La Demoiselle Elue ’ ‘ Pelléas et Mélisande ’ ; 
 ‘ L’Aprés - midi d’un Faune ’ favourite 
 piece Queen Hall English perform- 
 ance August 20 , 1904 . London 
 critic , commenting performance , 
 impression ‘ faune ’ meant fawn , 
 facetiously inquired afternoon thoughts 

 it| years ago | spending week - end Ripon 

 set forth chronological form , accuse | acquaintance Dr. Crow , 
 date . /i’erhaps Directors | kindly invited round Cathedral 
 ( ueen Hall ( rchestra , Limited , way | close Sunday afternoon service . 
 issue interesting important catalogue in| intelligent young verger cicerone 
 alphabetical order composers . started peregrinations Americans , 
 | staying hotel , asked join . 
 | pause verger descriptions , 
 reported Vienna letters of| standing Choir , senior American 
 Beethoven ( - number ) , | lady pointed lectern said , “ think 
 - seven pages manuscript music hand-| cathedrals decent bird , 
 writing composer , discovered in| miserable crow like ! ” nasal - toned matron 
 Austrian capital . letters , said to| Atlantic idea 
 written years 1816 1823,|the organist ( Dr. Crow ) standing , 
 likely prove specially interesting discovery | perch lectern , organ 

 authenticated . | seat cathedral 

 S 

 History Music England , ’ | workmanship violin parts pianoforte 
 “ advantages . accompaniments arranged old figured - basses 
 — _ — deserves high commendation . 
 number series Trio - Sonata 
 N MUSIC . violins , violoncello ( ad /#4 . ) pianoforte celebrated 
 setae , composer immortal ‘ Rule , Britannia , ’ Dr. Thomas 
 ( j 2 . y Alfred Mofiatt . . ~ . 
 ‘ ad ait enalen . tor Tn sien Wie ie . Augustine Arne . Sonata probably dates year 
 , oe eee ae : “ 4 g » Sonata ! 1740 , set seven similar works . 
 D , Henry Eccles : . 3 , Sonata B flat ; _ . . , apse 
 bn 4 contains movements—.S7cz / iano , Modera Largo , ar 
 r , William Babell ; . 4 , Sonata G minor , : . “ ‘ “ th 
 oe Deets = Amaaies , tr Sob Caltett : prove effective 
 ‘ Trio - ak , toe violoncello , additional interest given 
 : . ee . inclusion instrument . 
 y inc rur 
 . Ltd real pleasure study fascinating old 
 ers ancier 1 rich delightful store | works . dull tedious passage beginning 
 ed English Violin Music edited | end , comment pure , healthy 
 Mr. Alfred M numbers series ous joyous tone displayed English composers 
 n publist succeeding works maintain | 17th 18th centuries . Thanks Mr. 
 e sa h level excellence set pieces } Moffatt rescuing examples musty dusty 
 ! f great iulue _ historical | recesses museums bringing light 
 r Editor remarks , considerable attention | present day . stand fullest glare ! 
 er scitation church music , glees 
 ir rga eve sichord music older 
 poser little s 
 ‘ m violin writter vy English ! 4 //istory B Festival ¢ t ] Sock 
 1 cla r ‘ f Purcell close formation 1856 jubilee 1906 . G. F. Sewell . 
 rn en es oii Bradford : G. F. Sewell 
 r rs r unfamiliar author interesting book vetera 
 \ graphic h preface pieces , | Yorkshire music - lovers wl uny county pr 
 r 710 - 77 , al y Oxford . | original members Bradford Festival 
 et masters | Choral Society , fitting write histor 
 l W ; g concer task _ discharg commendable skill , 
 ! “ cred years held a/| ¢ handicapped earliest 
 Blenheim , | records , covering years , having k 
 ‘ ) Mar rougl Sonata der t secretary , - ye 
 rr r ° 3 s ‘ n » wes origin enthusiasm 
 r bh | rt harpsichord , ’ | late Samuel Smit \I ir Bradford 18490 , whe 
 . consis g ro,| Mendelssohn ‘ Elijah ’ performed ( com 
 ( n energe Bradford December 7 ¢ year , wi ler 
 , ! nearly filled factory operatives . T 
 S D r Henry Eccles , rn | St. George Hall , st 27 , 1853 , gave s 
 r King Band fro 7 16.| choral music Bradford , d tl Festival ( ¢ 
 r t r Par d r e | duly formed 155 
 ! ] 3 ’ ited e president Mr. Samuel Smit ! firs 
 t Par wher ! $ n | conductor , William Jack . Masham , 1 fi 
 , Hie wr r wor hillings ars 1 ! 
 t r ! " ¢ e n ind guineas ft 
 ALT 2 ) Society * Coftee ns provided 
 \ ¢ ‘ Society funds members e choir « 
 { r rom stance , « f refreshments year 
 vor mounting t e sui 1 I¢ 1858S Soe 
 N S | r W um | l proud dis befor n Victor 
 Dr. | Ww ( rt | Pala r concer 
 rs AW H vs ( r Bread Stree \ rchestra wa ve $ 62 se Bradford singer 
 ( ! Geor Fir 1 th nour r | ral Symp 
 H : \ orkshire Mar } 3 rhe pitfalls 
 t ¢ r ! \ r r makir y “ V vynen , 2 
 N Burme sador Bradford 1 cencert w 
 ¢ | { r r. ) gran 
 S e , Brita nd * ¢ 
 Air , Hor f e Soci \ \W 
 r r short | $ r neé cor * 7 
 dr rtive , vear , ‘ Deliverar ist works ) 
 ' ] S. Bur , » Edw Ile = : ee Bridge , 
 " S r S \ Mr. R. H. Wils pr nt conductor Dr. F. H 
 ! r S Cowe S \ led contingents singers 
 1 , estivals rd , Gl er Chester , d 1 
 ' r t lee year Soc sang , invitation , Lond 
 Collet N lhar ic Soci ncert 17 , 1906 , whe 
 red cernir lly maintainec reputation Bach motet ‘ Sing jy 
 r r t Lord inact panied ) , Beethoven Chor 
 f rt Symphony Society owns autograph 

 Schubert 
 details 

 f 

 T inthem Ly Bach , editorial skil ] | 42 interesting important book entitled ‘ Military Music 
 Mr. John P. - taken cantata ‘ Gott lob ! | @ history wind - instrumental bands , Messrs. Boosey 
 Nun geht Jahr zu ende . ’ _ - writing | published 1894 . 
 evt lv interesting 0 H largely c t f . 
 ee rp tens eg * = FF aoe te test phouse| December 9 , Mont reux , O1 ro Pi NIGER , aged 
 nelody commonly known ‘ Old rooth ’ Psalm Past pom aative " pe — _ = fT — aa ior ~ 
 te Aa ea > = enecessively | Paris Leipzig , pupil Joachim . 1572 
 pro given ccowe ux seas A. 4 . | violin master Harrow Saas . addition 
 composer subsequently starts new theme | ‘ ® D€!ng excellent violinist teacher , Mr. Deiniger 
 ‘ great skill -and fluency author - known method violin , 
 1 yn reached . 9 composed pieces instrument . 
 , . na r 1 posed pra aan — following - known foreign musicians died 
 , ] } . e close vear GAETANO BRA \ , aged seventy , 
 gr richly harmonized laid : * ' 
 voices . conclusion impressive composer famous * Serer BAPTIST 
 , , . — . CHARLES DANCLA , Tunis , aged t famous 
 ext anthems Sir Edward aaa TAFFER ? “ 3 
 Igar Rev. Henry Collins . prayer iesetat aga 2 ; alte d ‘ 
 rgiveness intercession , deep devotional spirit aged seven y seven , listinguisnec writer } e Sica 
 nes echoed reverent feeling music , — author Ws ae ve eho ! haley 
 passages intensity amounts fervour . | ° ° " ! ined impolite opprobrious terms t y 
 words tl second anthem J ] . Cummins , enemies Wagner 
 ure lied music confident character , 
 al spirit evident , fine climax built 
 wards t close , , , comes whispered 
 rever : ae Choral Union ga npressiv 
 performa e Brahms German Requiem December 3 , 
 RECEIVED . work heard time t Nort ! 
 d th rna Sir Ge Smart . | Scotland . soloists Miss Betty Booker 
 H. Bertram Co nad C. L. . Co portrait | Mr. Francis Harford , Mr. Arthur Collingwood conducted . 
 le Beethoven canon . I’p . xi . 252 + 105 . 6d . n December 13 University Society gave concert 
 ans , Green & Co. ) ofits season . addition choral items t rogramime 
 d d Sch mn . Selected edited | included Schubert ‘ Fierrabras ’ Mackenzie ‘ | 
 Dr. Karl Storck ; translated Hannah Bryant . Pp . ix Minister ’ overtures : Elgar ‘ Sursum Corda ’ organ , strings 
 , . ( John Murray brass ; cf Mozart G minor symphony ; und Dyvor 
 Ja ta Edward Algernon Baughan . | ‘ Sclavische Tinze , ’ Set 1 , . 4 Professor C. Sanford 
 Bs 2s . ( net . ( John Lane . ) Terry conducted \ feature concert recently given u 
 y years ’ rien é zan rle lea 1c playing . \berdeen Scottish Orchestra performance 
 Oscar Beringer . Pp . 723 ; . 72 Bosworth & Co. ) Max Bruch Violin concerto Mr. G. S. Mackay , 
 lay 2 i//s proved Vice - Chancellor | member ( uec n Hall Orchestra Dr. Frederi 
 ' ut Cambridge , 1501 - 1765 . Pp 73 : ss . wet . | Cowen ably conducted fine programme w cl lud 

 Cambridge Hi . Roberts . ) | Mozart ‘ Splendente Te , Deus ’ sung Choral Union 

 score 

 complete command orchestra . 
 applied fine performances Tchaikovsky ‘ Pathetic ’ 
 symphony overture ‘ Tannhiuser . ’ M. Jacques 
 Pintel great impression performance 
 Schumann minor Pianoforte concerto . Considerable 
 fluency delicacy displayed , hardly 
 poetry charm - known concerto 
 yield . interesting feature concert 
 performance soli wind Beethoven * Kondino ’ 
 oboes , clarinets , bassoons horns — instrument . 
 playing exquisite . concert December 16 , 
 Iluddersfield Choir took , separately 

 20 

 young performer . Lengyel , fourteen 

 excit iardly wonder masterly interpretations 
 Bach , Haydn , Beethoven , Mozart , Chopin . 
 second Liszt 

 numerous recitals took place 
 past month , remarkable Mr. Ysaye 
 December 4 11 ( Jueen Hall . brother , 
 Mr. Théophile Ysaye pianoforte , Belgian virtuoso 
 gave occasion superb interpretation violin 

 marred . hand Symphony E flat 
 Carl Ph . Emanuel Bach , hitherto unknown , produced 
 Gesellschaft der Musikfreunde , proved attractive . 
 Lovers music wind instruments spent pleasant 
 evening chamber concert arranged Ary van 
 Leeuwen , flautist , colleagues Opera 
 Bach cantata accompaniment 
 flutes , chamber duet Handel oboes , trio 

 pianoforte , flute , bassoon Beethoven wrote Bonn 

 young clays , Haydn ‘ Divertimento ’ wind 
 instruments — occurs ‘ St. Antoni ’ Chorale 
 Brahms wrote masterly variations -- 
 admirably rendered . Emil Sauer farewell concert 
 fine 
 display virtuosity , , fact , admired reproductive 
 creative artist 

 MUSIC BELFAST . 
 ( CORRESPONDENT 

 Brodsky Quartet Manchester gave chamber 
 concerts Queen College Society December 6 7 
 concert Mr. S. Speelman 4 Romance 
 viola solo composed brother , perfect 
 master instrument justice composition , 
 tunefui musicianly work . Grieg 
 string quartet played concert , Beethoven 
 ( ) uartet E flat ( Op . 127 ) fiéce de résistance 
 second concert , difficulties place 
 reach ordinary players , splendid playing 
 masters revealed beauties time 
 audience . Madame G. Drinkwater Miss F. Moore 
 respective vocalists enjoyable music - makings 

 Hallé Orchestra , Dr. Hans Richter , gave 
 interesting orchestral concert December 10 . pro 
 gramme — ‘ advanced , ’ , audience 

 programme able conductor . choir 
 orchestra acquitted praiseworthy manner , care 
 having taken augment orchestra contingent 
 professional instrumentalists . Special mention 
 cf fine tone - quality , attack , phrasing 
 choir . aid rendered principal artists , Miss 
 Alice Venning , Madame Marguerite Gell , Mr. Edwin 
 Spooner Mr. William Evans , , 
 satisfactory . Mr. C. W. Perkins occupied accustomed 
 post organist . e programme comprised Colerid 
 Taylor ‘ Death Minnehaha , ’ choir 
 orchestra thoroughly familiar . purely orchestral 
 included ven overture ‘ Egmont , ’ 
 Grieg ‘ Peer Gynt ’ suite . 1 

 passing reference 1 
 concerts highly artistic nature 
 recently held . second Harrison concert 
 principally noteworthy account apy 
 ance M. Edouard de Reszke little Vin 
 Chartres , clever violin prodigy . Dr. Hans Richter 
 und Hallé band executive concert 
 season promoted Birmingham Orchestral 
 Concerts Society , given Town Hall Decem 4 . 
 Strauss tone - poem ‘ sprach Zarathustra ’ received 
 ideal interpretation , chief feature magnificent 
 rendering Beethoven C minor symphony . _ vocalist 
 Mr. Webster Millar . Birmingham Concerts Society 
 og : ncerts Town Hall — November 26 
 December 10 — conducted Mr. George Halford . 
 , Mr. Arthur Cooke , clever young English 
 created sensation Liszt ‘ Pathetic 
 Concerto , ’ second Mr. Johann Hock , Dutch 
 violoncellist , splendid rendering Dvorak 
 principal orchestral items 
 Tchaikovsky ‘ Italian 

 rf 
 c 

 ioloncello concerto 

 Beethoven Fourth symp 

 apriccio . ’ great interest Felix Weingartner 
 concert Grosvenor Room December 6 , gr 
 conductor appearing pianist , assisted Kebner 
 String ( Quartet , programme devoted 

 Lonsdale . Handsworth Orchestral Society gave 
 artistic concert Town Hall December 5 , 
 conductor Mr. Johann C. Hock . solo violinist S 

 Mr. William Henley , playing created enormous 
 Max Mossel drawing room concert , 
 held Grosvenor Room December 12 , Miss Fanny 
 Davies M. Zacharewitsch gave finished perfor 
 ance Beethoven ‘ Kreutzer ’ vocalist 
 Miss Edith Clegg . nineteenth annual Scottish concert , 
 iven Town Hall November 30 , auspices 
 f tl ngham Midland Scottish Society , 
 Glasgow Select Choir , conducted 

 supplied programme 

 Society Shirehampton commenced interesting 
 concert given Parish Hall . performers Mr. 
 Hubert Hunt ( violin ) , Mr. Ernest Lane ( viola ) , Mr. RK . Le 
 Duc cknall ( vivloncello ) , Mr. P. Napier Miles 

 inoforte ) . works interpreted Beethoven 

 Trio D strings ; waltzes pianoforte 
 Brahms ( played Messrs. Miles Hunt ) , Mozcart 
 Pianoforte quartet G minor . compositions 
 played effect , intervals Mr. Campbell 
 McInnes sang folk - songs modern lyrics . 

 blin Orchestral Society gave concert 
 season November 27 , Excellencies 
 Lord Lieutenant Countess Aberdeen present . 
 \ good performance Schumann Symphony 
 D minor given , able direction Dr. Esposito . 
 programme included Glinka Overture ‘ Russlane 
 et Ludmila , ’ Bach aria , Rameau ‘ Rigaudon , ’ Waldweber 

 Siegfries 1 ’ ) , Wagner , Beethoven ‘ Leonore ’ Overture 
 N L 

 Miss Marie Dowse gave violin recital November 25 , 
 played Tartini ‘ Trille du diable ’ 
 Tchaikovsky ‘ Sérénade mélancolique . ’ gifted lady 
 assisted Mr. J. C. Doyle , vocalist , Miss 
 Madeleine Moore , pianist . December 5 Miss Nan Stac 
 gave vocal recital , assisted Mr. E. Gordon Cleather 

 44 MUSICAL TIMES.—Janvary 1 , 1908 

 chamber music recital Molesworth Hall 
 December 18 . programme included 
 quartets Beethoven Goetz , songs 
 sung Miss Nettie Edwards 

 December 3 Phibsboro ’ Glee Singers ( conductor , 
 Mr. Peter Walsh ) gave successful concert Rotunda , 
 Mr. John MacCormack great attraction . 
 gifted tenor — brought notice 
 winning gold medal Feis Ceoil competitions 1903 

 North City Choral Society ( conductor , Mr. George 
 Harrison ) gave performance December 13 * Acis 
 Galatea ’ Elgar ‘ Banner St. George , ’ orchestral 
 accompaniment . Miss Nettie Edwards , Mr. Robert 
 Harrison Mr. Thomas Marchant soloists 

 Sunday Orchestral concerts attracting good 
 audiences Antient Concert Rooms . 
 items Dr. Esposito conducted Beethoven fifth 
 Symphony , Mendelssohn ‘ Italian ’ symphony , Mozart 
 G minor symphony . Royal Dublin Society , 
 chamber music recitals given Miss Fanny 
 Davies Mr. Frederick Dawson , pianists , Kruse , 
 Nora Clench Brodsky ( ) uartets , London Trio , 
 Mr. Edwin H. Lemare , organist 

 Hallé Band gave concerts December 9 11 , 
 conducted Dr. Richter . Dr. Brodsky played solo 
 Brahms Violin concerto , performed 
 time orchestra Dublin . chief item 
 interest Strauss ‘ Till Eulenspiegel , ’ performed 
 time Ireland 

 MUSIC EDINBURGH . 
 ( CORRESPONDENT 

 Messrs. Paterson orchestral concerts , 
 November 25 , programme consisted Mozart 
 Divertimento strings horns ( . 15 , B flat ) , 
 Tchaikovsky ‘ Francesca da Rimini ’ overture , March 
 ‘ Karelia ’ suite Sibelius , Saint - Saéns 
 Pianoforte concerto , . 2,in G minor . Miss Fanny Davies 
 soloist , alike concerto group pieces 
 Van den Gheyn , Chopin , Rubinstein , displayed 
 usual musicianly qualities . fourth concert , 
 December 2 , conducted Dr. Richter , programme 
 comprised Beethoven second Symphony , overture 
 ‘ Flying Dutchman , ’ Dvorak symphonic Variations , 
 Richard Strauss ‘ Tod und Verklarung , ’ Grieg 
 ‘ Peer Gynt ’ suite . fifth concert , December 9 , 
 Herr Felix Weingartner directed performance 
 Edinburgh Symphony . 2 , E flat , Mr. 
 Mischa Elman gave splendid renderings Tchaikovsky 
 Violin concerto Beethoven Romance F. 
 pieces — conducted Dr. Cowen — Bach Fugue 
 minor , arranged strings Hellmesbeger , 
 overture ‘ der Natur , ’ Dvorak 

 fourth Mr. Simpson classical concerts , 
 December 3 , Lady Hallé Mr. Leonard Borwick gave 
 delightful performances Beethoven Sonata minor 
 ( Op . 23 ) Mozart Sonata F major . Lady Hallé 
 played ‘ Romance ’ Joachim ‘ Hungarian ’ concerto 
 Brahms - Joachim ‘ Hungarian ’ dances . 
 Mr. Borwick gave arrangements pianoforte 
 organ fugues Bach , Andantino Schubert 
 Chopin valse . Mrs. George Swinton vocalist 
 Mr. Martin Hobkirk able accompanist 

 December 11 Herr Felix Weingartner gave chamber 
 concert devoted entirely compositions . 
 composer assisted Rebner ( ) uartet ( Messrs. 
 Adolph Kebner , Walther Davisson , violins ; Joseph 
 Natterer , viola ; Johannes Hegar , violoncello ) , Mr. Claude 

 performance ‘ King Olaf ’ December 17 , 
 Choral Union gave proof ability 
 justice Elgar larger choral works . start 
 finish choruses sung rare precision fine 
 effect , notably unaccompanied portion Epilogue . 
 solo music capable hands Miss Agnes 
 Nicholls Messrs. Gervase Elwes Herbert Brown , 
 - named making exceedingly successful _ 
 appearance concerts . accompaniments 
 effectively played Scottish Orchestra , supplemented 
 organ , judiciously handled Mr. J. E. Hodgson , 
 Mr. Bradley conducted customary skill 

 Glasgow Amateur Orchestral Society concert 
 season took place December 19 , Mr. W. T. Hoeck 
 conducting . programme familiar lines , 
 included Weber overture ‘ Euryanthe , ’ Liszt Pianoforte 
 concerto . 1 , E flat , Beethoven Symphony , 
 Délibes ballet suite ‘ Sylvia . ’ Mr. August Hyllested , 
 leading local pianist , played solo Concerto , 
 contributed solos . Miss Jenny Young vocalist 

 Mr. Joseph Bradley , years guided 
 destinies premier choral society greatest 
 distinction , severs connection Glasgow end 
 present season , having accepted conductorship 
 Philharmonic Society Sydney , New South Wales . 
 Choral Union exceptionally fortunate appointing 
 successor Dr. Henry Coward , fame 
 known choral music sung . Dr. Coward takes 
 duties March 

 favourably heard interesting programme , performed 

 continued 
 appreciation chamber music section public 
 evidenced second Schiever concert December 14 , 
 programme contained Beethoven ( ) uartets 
 ( Op . 130 , Op . 18 , . 1 ) Pianoforte 
 Violoncello sonata ( Op . 69 ) , played Madame Marguerite 
 Stilwell Mr. Walter Hatton 

 Mr. Darbishire Jones , young violoncellist merit 

 MUSIC LIVEKPOOL . 
 CORRESPONDENT 

 ‘ Ladies ’ concert ’ November 23 ] 
 Orchestral Society played Brahms Symphony D 
 Beethoven ‘ Leonora ’ overture . 3 , interesting 
 novelty Vincent d’Indy symphonic poem ‘ Wallenstein 
 camp , l'art II . ‘ Max Thekla , ’ melodious | 
 musicianly work . solo Weber ‘ Concertstiick ’ 
 cleverly played Madame Marguerite Stilwell , 
 vocalist Miss Marie Stuart , contralto singer 
 heard Berlioz * captive ’ ‘ English songs ’ 
 Bertram Shapleigh , interest 
 ywchestral vocal . Mr. Granville Bantock conducted 
 concert , given December 7 , 
 Beethoven ‘ Eroica ’ symphony finely played . Mr. 
 Willy Lehmann favourable impression artistic 
 powerful playing RKubinstein minor Violoncello 
 mcerto , Mr. Frank Mullings displayed tenor voice 
 good quality , courageously Bach ‘ Pan 
 master ’ artistic advantage songs Richard 
 otrauss 

 Elgar ‘ Apostles ’ heard time 
 Philharmonic Society concert November 19 . 
 vocal principals Miss Agnes Nicholls , Miss Mabel 
 Braine , Mr. John Coates , Mr. Herbert Brown Mr. 
 Ffrangcon - Davies . Dr. Cowen conducted _ generally | 
 satisfactory performance drew fully choral 
 instrumental resources premier Society , | 
 oratorio heard impressive attention audience | 
 refrained signs applause near th 
 end . forthcoming performance great work 
 Welsh Choral Union Mr. Harry Evans awaited 
 interest 

 MUSIC MANCHESTER . 
 ( CORRESPONDENT 

 Hallé concert November 28 devoted entirely 
 Wagner . opened Grail Scene ‘ I'arsifal . ’ 
 second consisted act ‘ Tannhauser . ’ 
 vocalists Madame Ella Russell Mr. John 
 Coates . ‘ Tannhauser ’ music choir sang 
 exceptionally . concert December 5 , 
 Beethoven fourth Symphony performed , 
 Dr. Brodsky played Brahms Violin concerto Bach 
 ‘ Chaconne . Miss Agnes Nicholls sang Mr. Hamilton 
 Harty setting , soprano orchestra , 
 Keats ‘ Ode Nightingale , ’ song ‘ wilder 
 Granville 
 Bantock ‘ Christ wilderness . ’ concert 
 December 12 , Dr. Richter secured magnificent performance 
 Beethoven fifth Symphony ; Mr. Mischa Elman gave 
 extremely fine rendering composer Violin 
 Concerto , addition Tchaikovsky ‘ Sérénade 
 Mélancolique . ’ Dvorak overture ‘ Mein Heim * ( Op . 62 ) , 
 Grieg ‘ Peer Gynt ’ suite . 2 , completed 
 programme 

 Gentlemen Concert November 25 , Mr. Arthur 
 Catterall played Mendelssohn Violin concerto ; Miss 
 Currie vocalist ; symphony Mozcart 
 December 11 afternoon recital 
 given . Miss Lydia Nervil sang ‘ Ah ! infelice ’ air 
 ‘ Magic Flute , ’ vocal waltz Venzano ; 
 Mr. Mischa Elman played Spohr ‘ Gesangscene ’ 
 concerto ( Op . 47 ) , Tartini frequently heard Sonata 
 G minor , Suite , movements , 
 Sinding 

 ir worked 

 London 
 ce Beethoven 
 * Academic 

 Brahms Begrabnisgesang ” 
 Symphony Orchestra gave fine perforn 

 Haydn String ( Quartet bears chief burden 
 cause chamber music Torquay . gave 
 concert season October 9 , showing marked 
 advance standard performance . Haydn Quartet 
 B flat ( Op . 76 ) Grieg G minor ( Op . 27 ) 
 chief numbers programme . vocalist 
 Mr. Walter Belgrove . chamber concert 
 recorded South - west season 

 Torquay Musical Association opened fifteenth 
 season thirtieth concert November 27 . choir 
 sang - songs , orchestra played 
 movements Beethoven Symphony A. forces 
 combined _ impressive rendering Dvorak 
 * Te Deum , ’ Mr. T. H. Webb obtained excellent 
 results . Mrs. W. H. Mortimer , band , played 
 Scharwenka Pianoforte concerto B flat minor , 
 vocalists Miss Ruth Morrison Mr. Walter 
 Williams 

 amalgamation Exeter Oratorio Society 
 Western Counties ’ Choral Association , 
 definitely taken place , Society 
 rehearsal ‘ Judas Maccabeus , ’ eventually 
 adopted performance concert 
 new conditions November 20 , Miss Emily Breare 

 jchoir acquitted , direction 

 L rst point date came performance ns| Dr. H. J. Edwards , - songs . Dr. Edwards 
 Creation ’ Greenbank Choir , conduc | played pianoforte solos , Miss Hilda Pugsley violin solos , 
 Mr. R. Lang , October 6 . choir orchestra | Mr. Percy Lewis violoncello solos , instrumentalists 
 inbered seventy , excelling point attack | oining Beethoven trio . Miss Linford Brown sang . 
 nd correctness time tune . soloists | young choral society Tavistock justified formation 
 Miss Nellie Ellis , Messrs. W. Foster H. Smith . ] merit performance ‘ Banner St. George ’ 
 ly choral event performance ol Dvorak | December 11 , conducted Rev. H. Leigh Murray 

 wide ’ Guildhall Choir November 39 

 
 held 

 Beethoven 

 PARIS 

 Easter H. THORNE 1d . 
 1628 . Meek Thou livedst . Elegy 

 L. VAN BEETHOVEN 1d 

 1629 . Blessing , glory wisdom . Anthem 
 Double Chorus J.S. Bacu 3d 

