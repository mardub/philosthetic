iven to the young débutante , Miss Strangways , who 
 deliver the fine Recitative , ' glad tiding of great joy , " 
 with such true artistic feeling as to elicit a round of 
 applause ; and Miss Sydney have also a right to be com . 
 mend , as a new comer , for her singing of the Recitative 
 ' " « arise , Jephtha . " the band be everything that could 
 be desire ; and Mr. Arthur Sullivan ’s excellent additional 
 accompaniment give much vitality to the work . Mr , F. 
 A , W. Docker acccompanie the recitative on the 
 ( a judicious innovation ) with much judgment , and Mfr 
 Barnby conduct with his usual skill and precision . the 
 next concert will take place on the 6th inst . , when Men- 
 delssohn ’s Elijah will be perform , the part of the 
 Prophet be sing by Herr Stockhausen for the first 
 time in London 

 Tar Monthly Popular Concerts at the Angell- 
 Town Institution , Brixton , be proceed with even 
 increase success during the present season . ' the first 
 concert include Beethoven ’s ' ' Sonata Pastorale " and 
 three sketch by Scarlatti , and the second Woelfl ’s Fugue 
 and Sonata in C minor , all of which display Mr. Ridley 
 Prentice ’s power as an accomplished and intellectual 
 pianist to the utmost advantage . besides the solo already 
 mention , the piece which produce the great effect 
 werea Trio by Haydn , for pianoforte , violin and violoncello , 
 and Schumann ’s Sonata in A minor for pianoforte and 
 violin , the allegretto of which be encore . the execu- 
 tant be Messrs. Holmes and Weist Hill ( violin ) , Mr. 
 Webb ( viola ) , and Signor Pezze ( violoncello } , and the 
 vocalist , Mdme . Poole , Mdme . Dowland and Mr. Hillier 

 Tue Sixth Annual Concert in aid of the fund 
 of the Orphanage of the Out - door Officers of H. M. 
 Customs , be give on the 16th ult . , at the City Terminus 
 Hotel , Cannon Street . the large room be inconveniently 
 crowd by a fashionable and appreciative audience ; and 
 the entertainment ( which be under the direction of 
 Messrs. W. Phillips and A. L. Cope ) be thoroughly 
 successful . the artist engage be Miss Blanche Cole , 
 Madame Poole , Mr. Frank Elmore , Mr. Seymour Smith , 
 and Mr. Thurley Beale , with whom be associate Miss 
 Cavanagh , Mr. F. C. Cope , Mr. Churchill , and the 
 gentleman of the Westminster Glee Union . Miss Blanche 
 Cole elicit deserved applause in " Levey ’s " Esmeralda , " 
 and Allen 's " Far down a valley " ; and Madame Poole ’s 
 rich contralto voice be hear to the great advantage 
 in « the Lady of the Lea " and " the unwise choice . " 
 Mr. Elmore sing " the Irish Emigrant " and " ' the Nor- 
 mandy Maid " extremely well ; the remainder of the party 
 be alsohighly effective in the piece allot to they . a 
 prominent feature of the evening be the very excellent 
 playing of the band of ' the A division of Police . the 
 accompanist be Mrs. Seymour Smith , Mr. Meen , and 
 Mr. W. Miller , jun 

 Tae North London Choral Association give a 
 concert in the Town Hall , Shoreditch , on Tuesday evening , 
 the 14th ult . , before a crowded audience . the programme 
 include the principal portion of Haydn ’s ' ' creation " and 
 a miscellaneous selection , under the able direction of 
 Mr. Bassett , the Oratorio be excellently perform . the 
 solo vocalist be Miss Rycroft , Mr. Horatio Perry , and 
 Mr. R. De Lacy ( of St. Paul ’s Cathedral ) . the choir and 
 band number nearly 400 performer . the chorus 
 be excellently sing throughout , " the heaven be 
 tell , " « « achieve be the glorious work , " and " sing the 
 Lord ye voice all , " be especially worthy of praise . Miss 
 Rycroft and Mr. Horatio Perry , give their solo music with 
 care and judgment . Mr. R. De Lacy ’s singing of ' ' roll 
 in foam billow , " and « ' now heav’n in full glory 
 shine , " be highly successful , the latter be redemande . 
 in every respect the concert be thoroughly satisfactory 

 thosge very excellently organise concert , 
 under the title of " Musical evening , " have be highly 
 attractive during the past month . St. George ’s Hall be 
 well suited for the interpretation of the delicate chamber- 
 music select , and the audience at each performance have 
 be a thoroughly appreciative one . at the second con- 
 cert , on the 8th ult . , Schubert ’s String Quartet in A 
 minor , and Mendelssohn ’s , in D major , be give . Sir 
 W. Sterndale Bennett ’s charming trio in A ( Op . 26 ) , for 
 pianoforte and string , be also include in the pro- 
 gramme . the executant be Mr. Walter Macfarren 
 ( piano ) , Messrs. H. Holmes ( first violin ) , F. Folkes 
 ( second violin ) , A. Burnett ( viola ) , and Signor Pezze 
 ( violoncello ) . Mr. Macfarren deserve the thank of all 
 Beethoven lover for rescue almost from oblivion , his 
 Fantasia ( Op . 77 ) , the excessive beauty of which he so 
 ably display as to take by surprise the many listener 
 to whom the work be unknown . at the end of his 
 performance , he receive a well deserve tribute of 
 applause 

 tue private performance upon Messrs. John 
 Broadwood ’s Pedallier Grand Pianoforte , give by M. Dela- 
 borde at the Hanover Square Rooms , on the 16th ult . , fully 
 test its capability , and amply demonstrate to the 
 many artist present that it may prove of the high 
 value for the interpretation of work originally write 
 for the « ' Cembalo con Pedale , " especially when so able an 
 executant as M. Delaborde can be engage for the occasion . 
 the programme contain composition of various style , 
 all of which be make highly effective ; and some vocal 
 music , by Madlle . Cecile Fernandez and Mr. Bentham , 
 agreeably relieve the instrumental selection 

 ASHDOWN and Parry 

 Popular Classics , for the Pianoforte . 
 and finger by Walter Mactarren . 
 Wuen Mr. Macfarren resolve to call these excellent 
 specimen of a pure and true style of composition 
 " Popular Classics , " we presume that " the wish be 
 father to the thought ; " and as he might reasonably con- 
 clude that the sympathy of all real artist would be 
 heartily with he in his task , there can be little doubt of 
 his ultimately justify the title he have select , even if 
 the ' ' pretty ’' music of the day should be find at first a 
 formidable rival . the fact be that what a conscientious 
 teacher have to struggle against in his endeavour to spread 
 a love for the classical work be not a want of appreciation , 
 but a want of knowledge , of they ; and the issue of such 
 composition as those select by Mr. Macfarren , be 
 therefore a matter of the high importance , as render 
 accessible , at a moderate price , many little gem which 
 have long remain unnoticed , not only by pianist but by 
 publisher . the series have already extend to twelve 
 number ; and although we have not space to enlarge 
 separately upon their merit , we can assure our reader 
 that , even if they order one at random , there can be little 
 danger of regret their purchase . the collection 
 include three of Clementi ’s Sonatinas , twoof Beethoven ’s , 
 three Sonatas of Haydn , a Bourrée in a minor , and 
 " Echo , " from the Partita in b minor , of Bach ( the latter 
 a perfect treasure in its way ) , Dussek ’s fine Sonata in D , 
 ( which , both for practice and performance , will be find 
 invaluable ) , and Mozart ’s posthumous Rondo in b flat , a 
 work less know than any other of this composer ’s rondo , 
 but assuredly one of the most perfect and attractive of all . 
 we need scarcely say that these piece be most carefully 
 edit ; and to those who be not familiar with this school 
 of writing the fingering will be of the utmost value . we 
 have every hope that the sale of the " Popular Classics , " 
 will be sufficiently large to amply warrant a continuance 
 f the series 

 select , edit 

 ORGANISTS ’ salary . 
 to the editor of the MUSICAL TIMES 

 Sm,—Will you give I space to reply to the railing 
 accusation make by " Clericus " against Organists in 
 general and Country Organists in particular ? firstly , as 
 to the Organists ’ salary . ' ' Clericus " in effect say : 
 organist be pay for the time they be engage in 
 church work in an equal ratio to the payment they receive 
 for their time when engage in teaching . be that 
 so ? from a calculation I have make I find that my 
 duty require of I at least 272 attendance during the 
 year , of an average duration of an hour and a halt each 
 attendance , exclusive of time spend in copying , & c. , for 
 the choir . the stipend be £ 30 . the service choral — 
 two anthem every Sunday . now £ 30 for these attend- 
 ance will give 2 , 2}d.and asmaller fraction each 
 attendance . surely that can not be in an equal ratio to 
 what an average teacher get for the same time ! for while 
 I be make one attendance at church I could have 
 take three pupil at my own residence , which at the low 
 fee of 1 . 6d . per lesson would have yield 4 . 6d . so 
 far then the assertion of " Clericus " be not support by 
 figure ; but the basis on which he would found his scale 
 of remuneration be bad , if not inconsistent . let we see 
 how his theory work . I only get 7s . 6d . for give a 
 lesson on one of Beethoven ’s Sonatas ; therefore I be 
 amply pay if I play that same sonata to the public for 
 7s . 6d . ! secondly , as to position — w hatisit ? frequently 
 the unenviable position of a shuttlecock throw from the 
 clergy to the churchwarden , and from the churchwarden 
 to the clergy . as an organist of some year ’ experience I 
 have no recollection that my position ever bring I 
 half - a - dozen pupil ; on the contrary , my experience be 
 that the most prosperous teacher be he who have no church 
 work to do . thirdly , as to the voluntary country 
 Organists play . admit " Clericus " to be right , be not 
 his assertion but a reiteration of the complaint ? the 
 Organist be pay so badly that a musician find it more 
 profitable to study pianoforte playing than organ playing . 
 in fact , he get more for one evening ’s attendance at a 
 concert or a quadrille party than he do for a month ’s 
 church work ; but I do not admit that country Organists 
 possess so few qualification as " ' Clericus " assert . here 
 be a specimen of what be require at a country compe- 
 tition : — candidate to play soft voluntary , his own 
 selection ; a MS . chant as tothe Venite ; a MS . hymn 
 tune ; a chant and hymn tune , MS . , from figured bass ; to 
 harmonise.while play a give melody ; to play from 
 score an anthem supply ; and a conclude voluntary — 
 the candidate to write a short exercise in five part . to 
 the successful candidate be offer the stipend of £ 35 . 
 his duty would require about as many attendance as 
 name above . Ceriainly there be some hint of an 
 increase if candidate suit , but nothing say as to 
 repay the extra expense to which he would necessarily 
 be put in move to the neighbourhood , and remove if 
 he do not suit . ' he above example ( except perhaps the 
 short exercise ) be not above the average of competition , 
 and if they mean anything , the Organist select must 
 have a sufficient knowledge of harmony to enable he to 
 escape the false progression for which " ' Clericus " give 
 he credit , and therefore I can scarcely believe that 
 " Clericus " have meet the average Organist . fourthly 

 Clericus " complain that organist do not assist the 

 Biackpoot.—On the 6th ult . an entertainment , con- 
 siste of music and reading , be give at the Assembly Rooms . 
 Miss Clelland ( from Manchester ) sing several song with much 
 effect , and Misses Webbe and Crindrod , Messrs Gardner , Kirtland , 
 and Stanton also contribute some vocal piece , which be most 
 warmly receive . Mr. Grindrod be the accompanist . the 
 attendance be very large , and the concert highly successful 

 Brienton.—Mr . John Hiles , organist of All Saints ’ 
 Church , Buckingham Place , give an Organ recital on Monda 
 afternoon , the 30th October . the Church be well fill , and 
 would doubtless , but for the unpleasant state of the weather , have 
 be crowd , as it have be on former occasion , for Mr. Hiles 's 
 rephtation as an organist stand very high . the Recital commence 
 with a toccata and Fague , by John Sebastian Bach , which be 
 beautifully execute . the second piece be Mendelssohn 's Notturne . 
 the arrangement of this ( doubtless from the pen of Mr. Hiles him- 
 self ) and the Larghetto in A , from Beethoven 's 2nd Symphony , 
 which be give afterwards , effectively bring out the part of 
 the various orchestral instrument , show the player ’s power of 
 management . this be follow by a vocal piece from Fraulein 
 Mehlhorn , a lady whose excellent voice and expressive style 
 be well known in Brighton . her first solo , ' ' I know that 
 my Redeemer liveth , " be well give , and the air from St. Paul 
 ( " Jerusalem " ) , produce even more effect . Cherubini ’s " Ave 
 Maria " and Mendelssohn 's ' ' hear my prayer " be also sing 
 with equal success . Mr. Hiles ’s other performance , include a 
 Concerto , by Handel ( no . 2 ) , Barcarolle in F ( 4th concerto ) , by 
 Sir Sterndale Benuett ( in which the flute part be very beautiful ) , 
 the well - know Andante , by Batiste , and a March in G , by Dr. 
 Henry Hiles ( who , we believe , be the performer 's brother ) , display 
 the same excellency in the organist that we have before mention , 
 and the varied change and combination of the organ , to great 
 advantage 

 Bury Sr . Epmunps.—An excellent concert be give 
 on the [ 0th ult . by the St. Mary ’s Choir , Glee , and Madrigal 
 Society , under the conductorship of Mr. T. B. Richardson . there 
 be a large and fashionable audience , and all the choral music be 
 warmly applaud . amongst the most successful piece may be 
 mention Hatton 's ' " Jack Frost , " and W. G. Cusins ’ ' ' venetian 
 boat Song , " the latter be loudly encore . Mr. T. B. Richardson 
 most ably conduct , and preside at the pianoforte 

 Leamineton.—An operetta be produce on the 15th 
 ult . , at the Music Hall , describe in the programme as a piece of 
 absurdity ' " ' in three whim , " the dialogue be by Captain E. 
 Spencer , and the music by Signor Aspa . the local press speak in 
 the high term of the song and duet in the piece , all of which 
 be thoroughly successful with the audience , several be re- 
 demand . the principal part be sustain by Miss Julia 
 Lawson , Miss Robertha Erskine , Miss Beverley , Mr. Desmond 
 O'Callaghan , and Mr. C. A. Mott 

 Liverroot.—The seventy - fourth anniversary dinner of 
 the Liverpool Apollo Glee Club take place on the 2nd ult . , Mr. Skeaf , 
 the President for the season , occupy the chair , and William 
 Laidlaw Esq . , the vice . a number of glee by Callcott , Bishop , 
 Webbe , Farmer , Evans , & c. , be well render by the perform 
 member , Messrs. Haswell , Evans , J. B. Clarke , Brough , R. Evans , 
 J. Jones , T. J. Hughes , Carroll , and Armstrong , Mr. Skeaf pre- 
 side at the piano . the club be report to be in a prosperous 
 condition , and in possession of a library of between three ahd four 
 thousand glees.——-tue ninth concert for the present year of the 
 Philharmonic Society take place on the 7th ult . , the principal vocal 
 artist be Madlle . Jeanne Devries and Signor Mendioroz . solo 
 violin , Mr. Henry Holmes . the leading orchestral feature of the pro- 
 gramme be Mozart 's Sinfonia in D major ( no.5 ) , and the overture 
 to Melusine ( Mendelssohn ) , and Zannhduser ( Wagner ) . the march 
 in the latter opera be also give with much precision and effect . 
 Mr. Henry Holmes perform Spohr 's ' ' Dramatic Concerto " , with 
 peculiar delicacy and feeling , and be warmly applaud . he also 
 play , with equal effect , Schumann 's Abendlied , and a Tempo di 
 Bourrée ( Bach ) , which be encore . the singing of Madlle . 
 Devries and Signor Mendioroz be greatly admire . the duet 
 from Verdi 's Traviata ' Dite alla giovane , " and the baritone song , 
 " Di Provenza il mar , " be especially successful . the choral 
 member sing , with much spirit , two part - song ; and the chorus 
 from Alexander 's Feast , ' ' the many rend the sky . " — — Tue thirty- 
 sixth open rehearsal ( the first this season ) of the " Societa 
 Armonica " be give on Saturday , the 11th ult . , in the hall of the 
 Liverpool Institute , which be crowd in every part . the pro- 
 gramme be an excellent one , and be well execute throughout . 
 the soprano solo be entrust to Mrs. Billinie Porter ( a 
 daughter of the able conductor , Mr. Armstrong ) , who for some time 
 have be engage in the metropolis . she have a voice of much 
 purity , and in the song " ' softly sigh " ( Weber ) and " the ray of 
 hope " be highly successful , and receive at their conclusion the 
 warm applause . the other solo vocalist be Mr. Forrester , who 
 sing with much taste the solo in Rietz ’s " Laudate Dominum . " 
 the band be all that could be desire , and the performance of the 
 overture to Masaniello , Romberg ’s Symphony in D , the second 
 ballet music from Schubert ’s Rosamunde , and the turkish March 
 from the Ruins of Athens ( Beethoven ) be particularly effective . 
 the chorus be all judiciously sing , and the concert be in 
 every respect thoroughly satisfactory.——Tue tenth subscription 
 concert of the Philharmonic Society , on the 21st ult .. be devote to 
 an excellent performance of Mendelssohn 's first Walpurgis Night . 
 and a miscellaneous selection . the principal artist be Madame 
 Rudersdorff , Madile . Drasdil , Mr. Arthur Byron , and Herr Carl 
 Stepan . the overture be Weber ’s Preciosa and Auber ’s Fra 

 Diavolo . _ the Sinfonia , Mendelssohn ’s " Italian Symphony , " 
 Madame Rudersdorff declaim , with great energy and expression 
 Randegger ’s fine scena , ' ' Medea . " Madlle . Drasdil , Mr. Byron , and 
 Herr Carl Stepan be highly effective in their various solo per . 
 os and the concert be , in every respect , thoroughly 
 succe 

 Sanpown , Ist — e or Wieut.—Mr . Fred . G , . Baker 
 having resign the Organistship of Sandown Parish Church , 
 which post he have fill for nearly eight year , have be present , 
 by the member of the choir , with Best ’s arrangement , from the 
 Scores of the great master , for the organ—4 vols.—and Handel 's 
 Organ Concertos . Mr. Baker be appoint to St. Saviour ’s on the 
 Cliff , Shanklin , Isle of Wight 

 SHEFFIELD.—On the 6th ult . , the Queen Street Har- 
 monic Choir give a performance in the Music Hall , Surrey Street , 
 before a large audience . the choir , although the young in the 
 town , have , under the able direction of Mr. H. Coward , attain 
 considerable proficiency . Romberg ’s cantata , the lay of the Bell , 
 which form the first part of the programme , be . exceedingly 
 well perform , the principal part be most effectively give by 
 Miss Twigg , Mr. A. Kenningham , and Mr. J. H. Sutcliffe . the 
 band be lead by Mr. C. Stokes , Mr. Moxey preside at the piano- 
 forte , and Mr. Coward be the conductor.—mr . Crowther Alwyn ’s 
 concert at the Music Hall , on the 23rd ult . , be attend by a large 
 audience . Mr. Alwyn be assist by Mr. Henry Holmes ( violin ) , and 
 Signor Pezze ( violoncello ) , Miss Sophie Ferrari be the vocalist . 
 Mendelssohn 's Trio in D minor , and Beethoven 's in C minor , for 
 pianoforte , violin and violoncello , be most artistically give by 
 the above - name instrumentalist , and Mr. Alwyn ; and Schubert 's 
 duet for pianoforte and violin , play by the concert - giver and Mr. 
 Henry Holmes , be also an important feature in the programme . 
 the pianoforte performance of Mr. Alwyn include a selection 
 from Schumann ’s Kinderscenen and a piece of his own composi- 
 tion , all of which be receive with warm and deserved applause . 
 the solo of Mr. Henry Holmes and Signor Pezze , and the singing 
 of Miss Ferrari , contribute much to the success of a very excellent 
 concert 

 Srateysrivce.—The fifty - seventh annual grand evening 
 concert of the Staleybridge Old Band take place on the 26th 
 October , in the great hall , which be crowd by an enthusiastic 
 and highly appreciative audience . the concert be under the 
 patronage of the mayor , councillor , and principal nobility and 
 gentry of the county . the following vocalist be engage 

 Wuitrnaven.—Mr . Hamilton White give his first 
 concert of the season , on Tuesday evening , the 31st October , in the 
 Oddfellows ’ Hall . the artist engage be Mesd Rudersdorff 
 ( soprano ) and Drasdil ( contralto ) , Mr. Arthur Byron ( tenor ) , Mr. 
 Whitney ( Bass ) , and Mr. Oscar Beringer ( pianist ) . owe to 
 severe indisposition , Madame Rudersdorff be unable to appear . 
 Madile . Drasdil ’s " * in questa tomba , " Mr. Byron 's ' draw the 
 sword , Scotland , " and Mr. Whitney 's ' Possenti Numi , " be 
 amongst the vocal piece most admire ; : and Mr. Beringer ’s 
 brilliant playing attract considerable attention . the hall be 
 crowded in every part 

 Wiean.—On ' Thursday , the 16th ult . , an evening 
 concert be give in the Public Hall by the choir of the Parish 
 Church of All Saints . the chief feature in the programme be 
 Sullivan ’s Cantata , on Shore and Sea , the execution of which be , 
 in every respect , thoroughly satisfactory . Mr. Walter Parratt 
 play selection from two of Beethoven ’s Pianoforte Sonatas , in 
 which he display much musical ability and taste 

 Wrnpsor.—Mr . Orlando Christian give a concert , in 
 Dr. Hayne ’s Music Room , Eton College , on the 1st ult . , which be 
 well patronise by the Head Master , Assistant Masters , and 
 Students . Madame Thaddeus Wells be very successful in * Lo , 
 here the gentle lark " ( Bishop ) , flute obbligato , Mr. H. Nichol- 
 son . Mr. Christian sing ' ' the Yeoman ’s Wedding " ( encore ) and 
 " the Scout " ( Campana ) , Randegger ’s trio , ' ' the Mariners , " be 
 well give by Madame Wells and Messrs. Christian and Mellor 

 ord 

 MADAME SAINTON - DOLBY 's VOCAL ACADEMY . 
 ADAME SAINTON - DOLBY beg to announce 
 that her Academy for thetraining of Professional Vocalists 
 ( lady only ) for Oratorio and the Concert Room , will open shortly 
 after Christmas . prospectus on application at her residence , 71 , 
 Gloucester - place , Hyde Park , W. , or at Mr. George Dolby ’s office , 
 52 , New Bond - street , W. 
 onthly popular concert , Brixton , 
 director , Mr. Riptey Prentice.—Third Season.—Third 
 concert , on Tuesday evening , December 12th . Herr Straus , Miss 
 Rosa Black , Mr. Ridley Prentice and Mr. Minson , Miss Kate Marie 
 Nott and Miss Hann . Violin Sonata ( Rubinstein ) , Kreutzer Sonata 
 ( Beethoven ) , three sketch for P. F. ( Sterndale Bennett ) , and 

 Novelette ( Schumann ) , P. F. Duet in A ( Mendelssohn ) . ticket 
 | s. , 28 . 6d . and 1 . of Mr. Ridley Prentice , 9 , Angell - park Gardens , 
 | Brixton , S.W 

