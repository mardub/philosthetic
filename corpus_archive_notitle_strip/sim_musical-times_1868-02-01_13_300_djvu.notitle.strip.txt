SECOND SUBSCRIPTION CONCERT, 
on 
WEDNESDAY EVENING, MARCH 11, 1868. 
THE PROGRAMME WILL INCLUDE 
GOUNOD’S “MESSE SOLENNELLE,” 
A new Morert, “ King all Glorious,” by J. Barysy, 
MENDELSSOHN’S 114m PSALM, 
“ When Israel out of Egypt came

BEETHOVEN'S PIANOFORTE CONCERTO

IN E FLAT, &e

It is now to protest, and that most vehemently, 
against the custom of appropriating as anthems 
selections from larger works that never were de- 
signed for such a purpose

Worst among these, that have come within my 
knowledge, are some movements from instrumental 
compositions, which have been more or less distorted 
to render their vocalization possible, and to misfit 
them to the texts which are desecrated by the unholy 
alliance. Be it an overture of Arne, be it a quartet 
of Haydn, be it a trio of Beethoven, the fact is the 
same, of injustice to the music, profanation of the 
words, distraction of the hearer’s thoughts from the 
place of worship to the theatre or the concert-room, 
and sacrilege of the temple

Next to be deprecated, is the association of pieces 
from Masses, and other compositions to Latin words, 
with English texts of purport and sentiment different 
from the original. ‘The case is quite otherwise with 
literal translations or close imitations of the Latin 
sense; in these we have a lesson on the catholicity 
of those tenets which are common to the Roman 
and the Fnglish Church, and the latter becomes 
illuminated by master-thoughts that were conceived 
for the service of the former, but aggrandize both by 
explaining and enforcing their points of unity. I 
have already quoted a clerical impression of the 
powerful annotation upon the text, embodied in the 
setting of the Credo in Beethoven’s Mass in C ; every 
auditor may not be so sensitive as he I quoted, to 
the deep significance of this great commentary on 
the Christian’s belief; but every one may, in some 
degree, be helped by Beethovén’s music to the just 
comprehension of the text, and all the more so for 
his own language being employed for its enunciation. 
If, however, the same notes were to be twisted 
round any chapter of David’s Psalms or Jeremiah’s 
Lamentations that had syllables enough to sustain 
them, the sound and the sense would be in 
antagonism, truth of expression would be annihi- 
lated by their explosion, and such abuse of art 
would have tendencies as evil as its proper ap- 
plication is good. Yet this very thing, this fraud 
upon the musician and upon his audience, has been 
practised with movements from Mozart’s Masses and 
other works of the kind; and some of the pieces so 
despoiled of their expression, which is their chief 
element of merit, are even now in the working 
repertory of some of our Cathedrals

Least objectionable—but, ah! how very objection- 
able in this least degree—of these usurpations for the 
Church, of music not designed for its use, is the 
appropriation of pieces from Oratorios to the purpose 
of Anthems. Now, in the first place, the effect of 
these pieces is materially injured by the omission of 
the parts written for the orchestra, which are as 
integral to the whole as those written for the voices, 
and are at best misrepresented by any arrangement 
of them for the organ. Again, it is seriously injured 
by the enormous disparity between the number of 
singers in a Church Choir and the number we are 
accustomed to hear in Oratorio performances, even 
performances on the smallest scale that are ever 
brought before the public; since this disparity pro- 
vokes comparison of what we hear and what we have 
heard, and the character of the music is changed ac

more permanently exemplified by the very valuable and

linteresting collection here referred to, Most instructive 
jalso is it, to the inquiring musician, to trace the changes 
lin the style of passage writing here exhibited. The early 
luse of formal sequences and runs; the grand, sustained, 
church-like sublimity of Sebastian Bach, changed by that 
inventor, his second son Emmanuel, to a light and graceful 
melodic style that marks the transition from the school of 
Harpsichord music, with i:s formalities and excessive use 
lof the arpeggio, to that expansion and symmetry of form 
developed by Haydn and Clementi, ennobled by the rich 
and romantic imagination of Mozart, to culminate in the 
symphonic grandeur of the Sonatas of Beethoven—the 
different stages of progress which have preceded these last 
and greatest developments of pianoforte music, are fully 
illustrated in the above-named collection of well chosen 
ispecimens by composers of the various schools of Italy, 
England, France, and Germany, the earliest examples 
| being those charming pieces of antique quaintness “ The 
| Carman’s Whistle,” and “'lhe King’s Hunting Jigg,” by 
jour Elizabethan composers, Dr. Byrd and Dr. Bull; and 
the latest consisting of various Italian and German speci- 
mens of the close of the last century. Several admirable 
pieces by various sons of Bach are given, as well as a 
Fantasia and Fugue by John Ernst Bach worthy of the 
great Sebastian himself. Emmanuel Bach (the second son 
of the great contrapuntist) with whom, as already said, 
commences the period of transition from the old formal

modern pianoforte music, will receive illustration in a

No branch of the musician’s art has been so recently 
and so largely developed in England as organ playing. 
In all mere musical respects—beauty, grandeur, and 
variety of tone, the instrument itself has not advanced 
beyond, indeed has scarcely sustained, the excellence 
reached by the great German builders of a century and a- 
half since, Those who have heard the organs of Silber- 
mann, such for instance as that in the Catholic Church of 
Dresden, will scarcely hesitate to admit the supremacy of 
their combined brightness and liquid sweetness, and the 
almost human quality of tone in the softer stops. ‘hese 
high merits are scarcely to be paralleled even in the 
» instruments of that excellent living French artist, M. 
Cavaillé-Col, or those of our best English builders. To 
this superiority of the organs of Germany must likewise 
be added their early application of those important ac- 
cessories—the pedals,—which have for more than two cen- 
turies been there held as indispensable almost as the 
claviers for the fingers of the performer. The construe- 
tion of the German pedals, hinged towards the heel of the 
performer, admits of a facility and rapidity of execution 
that confer the advantage of a third hand,—while the 
clumsy imitation adopted later by the French organ 
builders—now almost obsolete, and known as French 
pedals, being short projecting levers hinged towards

music than of that elevation and special application that 
distinguished Bach’s organ playing and characterise 
his works for that instrument. ‘Doubtless, however, 
Handel had chiefly in view the pleasing his public by a 
lighter style, in strong constrast to the solemnity of his 
oratorios, with which his organ performances were usually 
associated, It was not until after the commencement of 
the present century that organ playing in England began 
to assume that character which had long distinguished it 
in Germany. The late Samuel Wesley and Thomas 
Adams, who flourished in the early part of this century, 
were the earliest pioneers in this school; but even 
those great artists at times interspersed their admi- 
rable improvisations by episodes in the light ad 
captandum style of the English organists of the pre- 
vious century. In the extemporaneous performances of 
both these artists it was no uncommon thing to hear an 
admirable movement in the alla Capella style, or a fugue 
treated with masterly clearness and skill, alternated, with 
trillings and secular prettyness quite out of keeping 
with such associations. To the late Samuel Wesley belongs 
the merit of having first made extensively known here 
the forty-eight preludes and fugues of Bach (Das Wohl- 
temperirte Clavier) by publication and performance. It 
was, however, only with the later knowledge of the great 
organ works of Bach that the special use of the pedals in- 
dependently of the hands, obtained here; one of the 
earliest artists to introduce this important feature of the 
best school of organ playing being the present Dr. 
Samuel Sebastian Wesley, whose skilful performance of 
the grand organ works of Bach, with the pedal part 
written in a third line as independent of the claviers 
as though it were for a third hand —and whose 
admirable improvisations on his instrument, gave a 
great impetus to organ playing in England; an impetus 
which largely helped to produce the many skilful per- 
formers whose names are now too numerous for mention. 
Among these, Mr. Best, organist of St. George’s Hall, 
Liverpool, has long been eminent for his powers as a_per- 
former whose command of finger-board and pedals is alike 
unbounded. ‘This gentlemen has for some time past been 
contributing most valuable additions to thelibrary of organ 
music by a series of arrangements, the latest numbers of 
which are referred to above. These excellent transcrip- 
tions are drawn partly from sacred and partly from 
secular sources; including some orchestral pieces, to 
which Mr. Best contrives to give much of their 
original effect by his skilful and carefully indicated combi- 
nations and changes of the various stops of the in- 
strument. The pieces are arranged, as all organ music 
should be, in three lines, the pedal line being obbiigato, 
and independent of the hands. Only by such means can 
the true effect of organ playing, and the comprehensive 
adaptation of a score, be obtained. The sacred pieces 
contained in the numbers referred to are a magnificent 
chorus from one of Bach’s church cantatas, Mendelssohn’s 
overture to Athalie, and motett, ‘‘ Hear my prayer ;” the 
secular pieces being Handel’s overture to Porus, the 
Allegretto from Beethoven’s Seventh Symphony ; a por- 
tion of Mozart’s Divertimento for wind instruments; a 
Gavotte and Rondo by Bach, and Spohr’s overture to

the point of the performer’s toot—allowed only of an 
occasional holding note, and scarcely admitted of any pas- 
sage playing. These were the pedals first adopted in 
this country, and only superseded by the German pedals 
some half century or so since. It is not a little remark- 
able that Handel, a cotemporary of Bach, and belonging 
to the same period and locality, in which the grandest 
style of organ playing reached its highest development, 
should without protest have submitted to the inferiority 
of the English organs, in size and compass, and especially 
in the absence of pedals, to the use of which he must have

with lodges 989 and 1612, was given on the evening of

the 15th ult., at the new Orange Hall, York-street. 
During the evening some appropriate songs and choruses 
were sung by Miss Murphy, Miss Mahon, and Brothers 
Craig and Dyas; the proceedings concluding with the 
National Anthem.——Tuer Second Series of Monthly 
Popular Concerts was inaugurated at the Ancient Concert 
Room on the 13th ult., under the fairest auspices. Mr. 
Charles Hallé’s Pianoforte Recital in the afternoon was 
listened to by a crowded audience with the utmost atten- 
tion; and every piece—especially Beethoven's ‘ Pastoral 
Sonata,” and the eighth book of Mendelssohn’s « Lieder 
ohne Worte ”—appeared to be thoroughly appreciated. 
At the afternoon concert, Beethoven’s string Quartett in 
F (Op. 18), was excellently played by Messrs, R. M. Levey, 
Liddell. Halton, and Elsner; and Mr. Charles Hallé’s 
pianoforte performance was again an attractive feature in

the concert. Mr, Arthur Barraclough made his first 
appearance as a vocalist on the occasion ; and.in Gounod’s| 
song, “ The Valley,” displayed a good baritone voice, of 
considerable compass, and a legitimate style of vocalisation | 
which augurs well for the future. He afterwards joined | 
Miss Minnie Hodges in Mozart’s duet « Crudel perche,” | 
with the utmost success. Every credit is due to Messrs. | 
Elsner and Gunn for the spirit in which they have en- | 
tered into a speculation which has for its object the| 
elevation of the musical taste in Dublin; and we cordially 
wish them all the success they deserve

Institution for the Blind, Nottingham, Mr. F. M. Ward, 
teacher of music at the Institution, presiding at the piano. 
The hall was nearly filled with a highly respectable 
audience, who evidently thoroughly appreciated the 
amount of talent displayed by the performers. ‘he pro- 
gramme was an excellent one, consisting of a selection of 
favourite glees, songs, &e. “The death of Nelson,” by 
Mr. W. Lock, received a well-merited and enthusiastic 
| encore. Miss Armitage, a lady possessing an excellent 
jtreble voice, was especially happy in her rendering of 
“ Beautiful May,” and Miss Jeffries was much applauded 
in “I cannot sing the old songs’ Messrs. Gibsun (bass) 
and Lock (tenor) sang the duet “O Albion!” with remark- 
ably good effect. The glees were strengthened by Messrs, 
| Bennett and Shepherd, and Miss Cope. We understand 
that the same company will pay another visit to G:imsby

mendatory sentence is due in conclusion to the orchestra, }in the summer, when they intend devoting the proceeds 
which was numerous and efficient—the Jarghetto from |of the performance to the benefit of their old schoolfellow, 
Beethoven’s ‘ Second Symphony in D,’”’ which introduced | Mr. J. Radman, of this town, who was educated in the 
the miscellaneous selections in the morning, and the/above Institution

piquant, if somewhat trite, overtures to Masaniello and | Hampron.—The Choral Society gave a Con- 
Zampa at the evening entertainment, being highly pleas- | enc TB

Supsury.—A Concert, by the members of the 
Sudbury Amateur Musical Society, was given at the 
Town Hall, on the evening of the 2nd ult. With one 
exception, the programme was composed entirely of vocal 
pieces, all of which, considering that they were wholly 
sung by amateurs, were very creditably performed. The 
choir contains some excellent female voices ; but is some. 
what weak in tenors. The only instrumental piece wag 
Mendelssohn’s “ Andante and Rondo Capriccioso,” well 
executed on the pianoforte by Mr. Orlando Steed, who 
also officiated as conductor; and to whom the society is 
mainly indebted for the state of vocal proficiency exhi- 
bited on this occasion. Mrs, Lishman presided at the 
pianoforte

Torney, CamBripGEsnire.—Mr. Thacker’s 
Concert.—The weather, unpropitious though it was, had 
little or no effect upon this annual entertainment, which is 
always looked forward to with a large amount of interest, 
and which received as much patronage this year as for. 
merly. The programme was highly interesting; andthe 
professional talent engaged, although Mr. Sturge did not 
appear, more than satisfied Mr. Thacker’s numerous 
patrons. Miss Clara Wight was principal soprano, and 
sang with much delicacy and taste during the evening, 
receiving several encores. Mr. Otto Booth played three 
violin solos with great brilliancy, and was encored in the 
celebrated “Carnival of Venice.” He also joined Mr, 
Thacker in Beethoven’s Sonata, known as the “ Kreutzer,” 
for violin and piano, which was played with great taste 
and precision. Several pieces were well sung by the 
members of the Choral Soviety. Mr. J. Baker, of 
Whittlesey, assisted Mr. Thacker at the pianoforte, and, 
with his daughter, played the Overture ‘“ Bohemian Girl” 
very effectively

Wiean.—The Messiah was given by the Wigan 
Choral Union, on the 27th December, in the Public Hall, 
The instrumentalists were selected from Mr. Charles. 
Hallé’s band, and the conductor was M. Gustave Steig- 
meier, The professional vocalists engaged were Mis 
Hall, Miss Marie Gondi, Mr. W. H. Cummings, and Mr. 
Orlando Chrisiian. Miss Hall gave “ Rejoice greatly,” 
with excellent effect; and Miss. Gondi sang the music 
allotted to her in a very pleasing style. Mr. Cummings’ 
singing was much admired ; and Mr. Christian, who pos 
sesses a fine voice, was thoroughly efficient in the bass 
part. The choruses generally were well sung by the 
members of the Union

