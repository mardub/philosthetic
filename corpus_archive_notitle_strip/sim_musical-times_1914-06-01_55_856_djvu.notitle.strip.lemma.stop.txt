HENRI VERBRUGGHEN 

 Mr. Daniel Mayer conceive idea 
 hold Beethoven Festival Queen Hall 
 . spring , consider 
 chief direction important musical 
 enterprise entrust . scheme 
 concert include Symphonies , 
 Pianoforte concerto , Violin concerto , 
 vocal work , 
 performance London Symphony Orchestra , 
 leeds Philharmonic Choir , great 
 live pianist , soloist engage . 
 - know Mr. Mayer choice fall 
 Henri Verbrugghen , , solo violinist , quartet 
 leader , conductor , devote considerable 
 artistic life study Beethoven 
 important instrumental composition 
 pianoforte sonata 

 isa pleasure record Mr. Verbrugghen 
 prove fully equal occasion , 
 probably important successful 
 artistic career . special interest create 
 interpretation Symphonies , 
 know introduce certain bold device 
 order adapt orchestration 
 balance tone large modern orchestra . 
 detail device explain 
 , 
 conceive carry spirit reverence 
 respect great composer , 
 idea improve masterpiece 

 1873 . child belgian 
 merchant . mother considerable musical 

 culty , find happy vent devoted 
 application Beethoven chamber music . 
 home resort - know artist , 
 bias boy classical 
 master initiate . Henri eagerly 
 violin , public appearance soloist 
 year age . later 
 parent desire shape education 
 medicine music , age 
 thirteen decision , advice 
 Joseph Wieniawski , Gevaert ( director 
 Brussels Cénservatoire ) , seek , 
 result music win day , Henri enter 
 Conservatoire pupil 
 Hubay , , later , Ysaye , unusual 
 mere musical development . whilst 
 Conservatoire win prize , 

 sixteen important step 
 forward turn special attention study 
 orchestration . soon admit 

 join Scottish Orchestra , 
 found George Henschel . 
 summer lead _ orchestra 

 Jules Riviere Llandudno , later 
 member Lamoureux Orchestra 
 Paris . follow year 
 deputy - conductor Llandudno , 
 experience finally determine adopt 
 career conductor . director 
 music Colwyn Bay ( position retain 
 |for year ) , return scottish 
 Orchestra , Wilhelm Kes 
 ( founder Concertgebouw Orchestra 
 Amsterdam , conduct Mengelberg ) , 
 1902 appoint leader deputy - conductor 
 Scottish Orchestra , Dr. ( Sir 
 | Frederic ) Cowen . summer year 
 succeed year leader 
 | Queen Hall Orchestra Promenade 
 season . period centre attention 
 } Glasgow , Athenzum , 
 ! important musical educational 
 institution Scotland , chief violin professor , 
 } 1911 succeed Dr. Coward conductor 
 | Glasgow Choral Union . recent year 
 |he conduct symphony concert Brussels , 
 | Berlin , Munich , St. Petersburg . notwith- 
 | stand varied widespread activity , 
 form string quartet party , 
 appear place Scotland 
 jelsewhere . present speciality party 
 li sixteen Quartets Beethoven , 
 lately perform chamber 
 music concert Glasgow Edinburgh 

 feature Athenzeum music 
 school orchestral class string player . 
 Mr. Verbrugghen find perfectly possible train 
 class high degree perfection 
 reach amateur player wind instrument . 
 3ut think desirable player 
 restrict repertory piece compose 
 string , purpose study 
 string symphony work write 
 orchestra study miss 
 fill pianoforte . year 
 operatic class success , 
 abandon find absorb dispro- 
 portionately energy school 

 BEETHOVEN symphony 

 idea double wind Beethoven 
 symphony order balance mass 
 string employ modern orchestra 
 new . obvious apparent escape 
 difficulty ; equally obvious 
 apply crudely lead difficulty 
 remove . ' numerous passage 
 symphony clearly play properly 

 amember Society Conservatoire 

 MUSICAL TIMES.—June 1 , 1914 

 condition , scope _ individual 
 temperament . ' consideration 
 matter deal scientifically . 
 sufficient rule extra 
 instrument use Zxééis . 
 Mr. Verbrugghen far . 
 study physical convenience principal 
 wind player whoimmediately characteristic 
 gust forfe sforsando , Beethoven 
 indulge , play delicately : 
 transition effort embarrass 
 skilful performer 

 example Mr. Verbrugghen 
 method treatment 

 public appearance Richard Coeur de Lion 
 original production Sullivan ' Ivanhoe ' 1891 

 follow conductor painstaking inspiring Si 
 Henry Wood , compel duty 
 relinquish post ; Mr. Balling soon acquire th 
 confidence singer , concert bee 
 exhilarating uniformly enjoyable . princip 
 choral work Bach Cantata , ' sleeper , wake , ' 
 double - chorus , ' Nun ist das Heil , ' Brahms ' Song destiny , 
 Dvorak ' Spectre bride , ' Hamilton Harty : 
 * Mystic Trumpeter ' ( conduct composer ) , whit 
 sing spirit force slacken , whil 
 volume tone produce choir 350 voi 
 ' ; ; |(a different body occasion ) excellent . th 
 performance * Der Rosenkavalier . programme confine choral music , coulé 
 AMBROSE AUSTIN , 14 , age eighty - seven . | realise value object - lesson chor 
 thirty - year manager old St. James Hall , | listen ( listen attentive 
 Piccadilly , Mr. Austin - know personality . Beethoven eighth Symphony , ' Oberon ' a0 
 JAMES ALEXANDER BROWNE , April 25 , seventy- ' Meistersinger ' overture , ' Thalassa ' Symphony 

 assistant Sir Arthur Sullivan Dr. A. L. Peace , 
 think highly gift 

 enjoyable programme 

 Beethoven sev 

 style modern , requirement modern opera | Sir Frederic Cowen ' veil ' Neat 

 popular CLASSICAL CONCERTS SWINDON : 
 experiment musical education 

 April 20 concert Mechanics ’ Institute 
 Swindon bring end successful series 
 Classical Concerts 
 past winter genuinely popular price genuinely 
 working - class audience . concert organize 
 Mr. Felix Schuster , similar series year 
 220 , request Swindon branch Workers ’ 
 Educational Association . season 
 audience average , 
 gympathy intelligent appreciation music 
 hear remarkable . 
 programme include Violin sonata Beethoven , 
 Handel , Tartini , César Franck , Ludwig Thuille , 
 Brahms Pianoforte quartet major ( Op . 26 ) , Dvorak 
 Concerto violoncello ' Dumka ' Trio , Bach 
 Concerto violin . vocal item 
 Arias Mozart Verdi , song Brahms , 
 Schumann , Couperin , Gluck , Vaughan Williams 
 song - cycle ' Wenlock edge . ' Beethoven Pianoforte 
 snata flat ( Op . 110 ) meet _ peculiarly 
 enthusiastic reception . March g Mr. Percy 
 String Orchestra play , programme consist 
 Mozart Eine Kleine Nachtmusik Concerto pianoforte 
 e flat ( . 9 ) ; Bach Concerto violin e major ; 
 W. Richter Abendgesang ; Percy Grainger Mock 
 Morris 

 Mr. Mrs. Schuster , performer 
 generously help series concert include 
 Miss Letty Maitland , Mr. Mrs. Percy , Mr. Percy 
 Sharman , Mr. Steuart Wilson , Mr. A. P. Fachiri . 
 altogether experiment successful , 
 earnestly hope repeat year , 
 example Swindon follow 
 

 London Concerts 

 BEETHOVEN FESTIVAL 

 concert excellent series organize Mr. 
 Daniel Mayer report issue . 
 concert , afternoon April 22 , fourth fifth 
 Symphonies , Herr Max Pauer play fourth 
 Pianoforte concerto , Mr. Paul Reimers song- 
 cycle ' die ferne Geliebte . " symphony 
 follow evening April 23 ; Zimbalist play 
 Violin concerto Herr van Rooy sing Aria ' Mit 
 madeln sich vertragen . ' need extol per- 
 formance , descend high artistic 
 grade . climax Festival reach 
 evening April 25 , Leeds Philharmonic Choir 
 200 voice magnificent assistance Choral 
 Symphony choral work ' calm sea prosperous 
 voyage ' ( conduct Mr. H. A. Fricker ) . Miss Ada 
 Forrest , Miss Tilly Koenen , Mr. Reimers , Herr 
 van Rooy soloist help sustain vitality 
 attraction performance . Pianoforte 
 concerto , Mr. Arthur Rubinstein soloist , 
 eighth Symphony complete programme . Mr. Henri 
 Verbrugghen , life work deal 
 leading article , conduct Festival 
 insight masterly ability 

 QUEEN HALL ORCHESTRA 

 Symphony Concert April 25 
 distinguish superb playing Herr Kreisler 
 Brahms Violin concerto , work 
 fit interpret live violinist . 
 Symphony Schubert ' unfinished , ' Sir Henry 
 Wood conduct individual performance , exquisite 
 tone - grading . Beethoven ' Coriolan ' Overture , 
 Bach ' Brandenburg ' Concerto 
 programme 

 programme unusual character choose 
 Endowment Fund Concert Orchestra 
 Sir Henry Wood 9 . include ' dance sun ' 
 Arnold Bax , Percy Grainger ' Shepherd hey , ' 
 Debussy ' L’Aprés - midi d’un   Faune , ' Stravinsky 
 ' firework , ' Schumann Pianoforte concerto , superbly 
 play Herr Dohnanyi . pianist appear 
 composer conductor excellent suite F sharp 
 minor , good season * Promenade ’ novelty 

 Herr Kreisler concert assistance 
 Sir Henry Wood Orchestra , Queen Hall , 
 14 . keen pleasure renew acquaintance 
 beautiful interpretation Elgar Violin concerto . 
 concerto Beethoven 
 unbounded delight 

 new SYMPHONY ORCHESTRA 

 Mendelssohn ' St. Paul ' choose performance 
 Handel Society ( Jueen Hall 12 , unds 
 Dr. Henschel direction attractive choral singing 
 Miss Eleanore Osborne , Miss Phyllis Lett , Mi 
 Gervase Elwes , Mr. Thomas Farmer soloist 
 Mr. E. G. Croager 

 large audience attend concert Wilbeis 
 Queen Hall 
 Beethoven Violin concerto , Erna Schulz solo . 
 chief feature programme . ; 
 Mr. Walter Hyde , hear London 
 Orchestra play wit 
 good tone capability Herr Sachse direction 

 13 , wher 

 concert season 5 , Elgar * dr 
 ' Sea - picture ' ( Miss Alice Laki 
 * Leonore ’ Overture , . 3 , ¥ 
 audience . 

 Gerontius ’ 
 soloist ) , Beethoven 

 iven crowded soloist bes 

 Miss Elena Gerhardt recital Lieder Torquay 
 Pavilion April 25 ; ballad concert 
 sat town date aid Boy Scouts ’ 
 fand Miss Ethel Hook , Messrs. Hastings Wilson 
 . Hugh Peyton ( vocalist ) , Miss [ Isabel Hirschfeld 
 lpianoforte ) performer . party appear 
 t Seaton April 28 aid charity 

 programme play Teignmouth Orchestral Society 
 n April 23 include Beethoven eighth Symphony . Mr. 
 \ , ] . James conduct , vocalist Miss Kate 
 Rostock . occasion benefit Mr. Basil 
 Hindenberg , conductor Torquay Municipal Orchestra , 
 Sir Henry Wood visit Pavilion April 30 
 snducte work Tchaikovsky , Schubert , Wagner . 
 Mr. Josef Holbrooke play solo Poem 
 ianoforte orchestra , ' Gwyn - ap - Nudd , ' conduct 
 isSuite ' Dreamland . ' Mr. Harold Bonarius , leader 
 mhestra , soloist Max Bruch Concerto , 
 wealist Miss Carrie Tubb Miss Ethel Toms . 
 Madame Blanche Newcombe recital german 
 english song Exeter 1 , assist 
 y Mr. Charles Fry Miss Maude Dixon 

 Exeter Orchestral Society score big success 
 concert 5 , conduct Dr. Wood . Mrs. Kenyon 
 Miss Dorothy Wood ) soloist Mendelssohn 
 Pianoforte concerto , hand play interesting 
 piece Debussy , Hamish MacCunn , Elgar , Glinka . 
 fr . Sutton Jones vocalist . Plympton Wesleyan 
 ichestra , conduct Mr. W. M. Wickett . concert 
 6 , Trio play Mr. Wyatt ( clarinet ) , 
 Mr. Long ( flute ) , Miss Chubb ( pianoforte ) 
 Mr. H. Watt - Smy1k conduct concert Ilfracombe 
 Orchestral Socicty , band thirty performer lead 
 Miss A. Clark 

 district performance 7 , Tywardreath 
 Choral Society ' Hiawatha ' trilogy . Mr. W. Brennand 
 Smith conduct 

 4 Penzance Orchestral Society , capable body 
 artistic aim , play Schubert ' unfinished ' 
 Symphony Beethoven ' Egmont ' Overture . Mr. 
 Walter Barnes conduct player , lead 
 Miss Stewart 

 DUBLIN 

 College Choral Society performance 
 Cowen ' St. John Eve ' Dr. Marchant direction . 
 solo member Society . 
 connection Feis Ceoil interesting pianoforte 
 song recital Mr. Frederick Dawson 
 Mr. Gordon Cleather 5 . Mr. Cleather ( 
 year resident Dublin ) sing great refinement 
 artistic feeling . Mr. C. W. Wilson accompanist 

 Sunday , 10 , Dr. Esposito start summer 
 series orchestral concert Woodbrook Concert Hall , 
 Beethoven Symphonies perform 
 order . Signor Simonetti play Mendelssohn Violin 
 concerto extremely , orchestra great 
 advantage accompaniment . 17 , 
 second Symphony , programme include Wagner 
 | * Waldweben ' ' Meistersinger ' Overture . Miss 
 Nita Edwards , return Dublin tour 
 round world Quinlan Opera Company , 
 solo vocalist , sing great success 

 Feis Ceoil publish collection ' irish 
 Airs ' ( eighty - number ) , edit Arthur Darley 
 P. J. McCall . consist selection large 
 number contribute time time Festivals hold 
 |s nce 1897 , understand hitherto unpublished . 
 prove interest 

 Walton Philharmonic Society , conduct Mr. 
 Albert Orton , deserve honourable mention performance 
 Cowen ' sleep Beauty , ' close Society fifth 
 season April 22 . tuneful work afford grateful 
 opportunity intelligent choir , singing 
 generally mark precision expression . vocal 
 soloist Miss Margaret Hadfield , Miss Helen Strain , 
 Mr. Lloyd Moore , Mr. J.C. Brien . wise provision , 
 - class string - player timpanist assist Dr. 
 Stanley Dale pianist , orchestral scope 
 accompaniment usefully suggest . aid 
 small orchestra admirable performance Mozart 
 EK flat major Pianoforte concerto , 
 Mr. Orton solo player 

 estimable local organization , aim 
 instrumental choral , Anfield Orchestral 
 Society . direction Mr. William Faulkes 
 Society continue prosper , programme 
 performance reflect credit concern . 
 close concert sixth season , April 29 , 
 orchestral movement Beethoven , Mendelssohn , 
 Weber play ; Beethoven Pianoforte concerto 
 E. flat , Op . 73 , soloist Mr. E. K. Harrison . 
 vocalist Mrs. Howard Stephens 

 April 18 , number local singer entertainer 
 combine performance , Hope Ilall April 18 , 
 aid widow daughter late Mr. John Henry , 
 especially - know welsh community 
 long resident city singer teacher 
 singing 

 L. Whittake 

 inkerron ’ wf Meresting . : , 
 April 21 , & Société des Concerts du Conservatoire close 
 \. < n performance Bach ' Johannes 
 perform : fasion . " 
 totes te song recital ii , Madame Vallin 
 [ r. Lichtore Hekking introduce interesting number 
 " Grassiand Roland Manuel . 
 M. Robert Schmitz 12 , 
 — ff itteresting pianoforte recital , devote modern french 
 russian music . 
 concert Société Musicale Indépendante 
 roduce wi ( omprise interesting number contemporary Spanish | 
 Mile . yom ff composer , greek song Emile Riadis . 
 M. message \lbert Roussel ' Evocations , ' Paul le Fleur * Crépuscules | 
 mmende . fAmor , ' ' Symphonic Suite ' Darius Milhaud , 
 d ' Mami Mf ' Madrigal lyrique ' O. Klemperer 
 tale tem fiteenth concert Association des Concerts Schmitz . 
 . Itsi le _ 
 aracter , k & sia enin - ornaia 
 ' . | 
 ae Foreign Wotes . 
 wo work 
 nd ball BERLIN . 
 direction Madame Heyman - Engel 2 french 
 a. theis pera - buffe evening Hochschule fiir Musik . | 
 sal @ Tbe duet ' Der Kapellmeister ' ( Paér ) Masse one- | 
 ape # operetta ' Les Noces de Jeanette ' principal | 
 e de Josep ! ature evening — Karg - Elert evening recently | 
 majority gen Harmonium [ prove great success . 
 rs havedal J Mleresting meeting Society Protection Art | 
 ic 2 hold recently . programme 
 HF " onsiste address Herr Metzler disfiguration 
 mposet , # af master - work 17th 18th century violin music 
 warm y unstylish modern arrangement . concert chamber 
 music 16th 18th century follow . 
 ernment & | 
 FRANKFORT - - MAIN . | 
 consist highly interesting concert french music recently | 
 -Korsiors Sen Palmgarten . work Aubert , Dubois , Bizet , 
 humons ff " Massenet , Saint - Saéns , Thomas , Bourgeois hear . | 
 aes ! HAMBURG . 
 inal pat ; : | 
 " Nerina ae great Festival German Brahms Society | 
 nducte Hy ‘ hold Hamburg ( Brahms birthplace ) June 4 - 8 , 
 1915 . Brahms Festivals place respectively 
 ‘ cal Soci M1909 Munich 1912 Wiesbaden . 
 te tow JENA . 
 t , incladm Th , = ; 
 ailles . 0 y € recently discover variation Beethoven | 
 une 100 m * s english horn ( violin viola ) 
 howt rete publish fortunate finder , Prof. Fritz 
 h memmbss ara ata discover ' Jena ' Symphony . 
 sedy - fast lation , theme Mozart ' Don Juan , 
 1 ( * ancie " em write 1795 , Beethoven 

 compose 

 400 ) , single 

 second Beethoven Festival arrange 

 Bach Brahms programme 

