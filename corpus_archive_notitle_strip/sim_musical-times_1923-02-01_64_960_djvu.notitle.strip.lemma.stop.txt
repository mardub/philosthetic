MOUSSORGSKY song writer 
 ERNEST NEWMAN 
 

 time critic historian 
 wish task simplify 
 destruction music 
 century matter ; think , 
 instance , minor work Mozart , Haydn , 
 Beethoven conscientious historian 
 feel read , hardly 
 worth perform - day . 
 long musicologue consider 
 seriously question wholesale sacrifice 
 second- - rate music , simple 
 reason soon impossible 
 man superficial knowledge 
 write lead 
 composer . day , doubt , international 
 committee form decide 
 work past worth reprint , 
 allow lapse oblivion 
 humanity penny bad 

 point view . ' , 
 , critic , groan 
 magnitude task , equally sympathy . 
 feel understand composer 
 thoroughly know intimately 
 composer write . ' psychologist 
 strong esthete . 
 enjoy bad piece art art , 
 profoundly interested 
 revelation artist ; Oscar Wilde 
 Browning , ' process fool arrive 
 folly dear ultimate 
 wisdom wise . ' man mind 
 piece , critic feel dare neglect 
 manifestation , poor piece work 
 throw good deal light good . 
 , , study artist 
 weakness understand strength ; 
 , reading inferior work 
 composer critic esthetic pleasure 
 — like try meal 
 tares wheat lie hand 
 table — good deal 
 pleasure possible 
 composer , defect 
 quality , act 
 react 

 sa 

 Scherzo section great originality , , 
 neatly play sharp pace ( 
 « = 120 ) right light registration , 
 effective . prefer omit , , 
 climax develop } need hesitate , portion 
 good example three-| Andante join satisfactory 
 harmony manual , great | . 
 tone . power music| Franck lead iza / e pass 
 , wide - range left - hand | review subject use . inevitably 
 gradual ascent keyboard . unfortunately | remind Beethoven use device . 
 Franck let movement pause | ninth symphony , comparison leave 
 page 22 23 , string tame | advantage Beethoven . , 
 chord end page 22 . mere | doubt kind thing 
 registration halt indication ! successful help word , 
 accompany . need not|of discordant interruption Beethoven 
 hesitate omit bar follow third| use , fact significant 
 pause page 22 . similarly , are|as word . aid marshalling 
 enthusiastic letter spirit,|of number theme produce merely 

 104 MUSICAL TIMES — FeEsruary 1 1923 

 Tiersot divide manuscript 
 examine group : firstly school 
 task , consist exercise harmony , counter- 
 point , fugue ( 1883 - 40 ) ; _ original 
 composition write childhood , youth , 
 1847 onwards 

 group comprise - Fugue 
 subject , ' worthy [ Tiersot ] 
 transcribe keyboard perform , ' 
 Franck win prize 1840 . second 
 mention ' variation pianoforte aria 
 Le Pré - avx - Clercs , César Franck , age 
 vear half , Op . 5 , ' song O Salu / aris , 
 Pianoforte Sonata ( style Beethoven early 
 work ) , Symphony orchestra , Op . 13 
 perform , accord note pencil MS . , 
 Orleans 1841 , probably write far early ) , 
 second Pianoforte Sonata ' cyclic ' 
 principle apply 

 beginning ot 1848 Franck complete 
 tone - poem inspire Hugo Ce gu’on entend 
 sur la montagne , , judge   Tiersot 
 description example quote , 
 worth know . ( Tiersot contention 
 strength work , Franck consider 
 having forestall Liszt invent vere 
 tone - poem altogether admissible : Liszt 
 Mazeppa Etude , instance , final form 
 ( 1837 ) , adduce proof contrary 

 Monde Musical ( December ) report address 
 Vincent d’Indy member Société 
 Frangaise de Musicologie certain early work 
 César Franck 

 1837 1847 , Franck music reveal , 
 melodic point view , influence Monsigny , 
 Méhul , Gluck , Beethoven ; , regard 
 writing , Liszt , Thalberg , Alkan . 
 imitation Liszt particularly obvious Chant du 
 Litre pianoforte , , , 
 characteristic Franck individuality . 
 early pianoforte piece shape — 
 Allegro exposition theme , 
 introduction 

 FO = th 

 violoncello record Maurice 
 Dambois Chopin nocturne D flat 
 Canzonetta Du Port . phrasing 
 Chopin strike matter - - fact , 
 lacking dreamy elegance 

 Lamond hear H.M.V. 12 - . d.-s . 
 Minuet Beethoven E flat Sonata , Op . 31 , 
 . 3 , Glinka Z’A / ouette . , 
 good reproduction , need comment . 
 Glinka brilliant enjoyable . 
 excellent pianoforte record ' Rachmaninov 
 transcription minuet 
 L’Arlesienne Suite . 1 . ( H.M.V. - . ) . 
 fault pianoforte record 
 occasional effect jangling overgrown 
 musical - box 

 English Singers fore fine 
 H.M.V. 12 - . d.-s.—Gibbons t life ? 
 Tomkins David hear . 
 advance previous record respect . 
 balance blend ( 
 high note bass occasionally cut 
 texture edgy tone ) 
 word clear . madrigal happen 
 type suited 
 largish choir single voice . big 
 way Handel chorus , carry 
 tone . good performance 
 soloist , inevitably sense effort 
 exacting passage . , 
 splendid Gibbons work sombre weight 
 realise sing choir 

 fae 

 charge organ — 
 exception Bach great master write 
 . hard nowadays know great 
 master , . use 
 think pretty Beethoven , present time 
 favourite target brick throw 
 young critic . meet follow day 

 item Beethoven interesting quartet 

 Op . 130 . wewish composer confine 

 apart pious wish , epithet ' interesting ' 
 delightful . word people use 
 acknowledge receipt new work 
 find hard 

 , let use term ' prominent composer , ' 
 run time Bach . Hande ! 
 know great organist , think 
 english organ time possess pedal 
 accustom , write organ work 
 instead concerto . know 
 miss , Handel instrumental music compare 
 badly Bach , choral work . 
 Handel Bach style music change — 
 Bach forget nearly century , irresistible 
 attraction swell orchestra expand opera 
 powerful counter - attraction . musician ne 
 long necessarily church organist , generally 
 , musical boy happen belong ' 
 choir learn organ , exist 
 church . allthe ' prominent composer ' learn clavier 
 pianoforte , write 
 incidentally ; write organ 
 learn ? Haydn study clavier 
 violin , sufficient occupy . 
 Weber Wagner absorb opera , 
 fine pianist . Schubert little 
 organ , principal interest violin . Beethoven 
 play organ boy Bonn , 

 quality , Hopkins 

 exceptionally interesting recital organ music base 
 Christmas theme Mr. Archibald Farmer 
 Presbyterian Church England , Muswell Hill . 
 programme include Maleingreau Vers /a Creche ( Vent 
 Redemptor ) ; piece old french Carol tune Le Bégue , 
 d’Aquin , Boely , Franck , Ropartz ; Christmas hymn- 
 tune Buxtehude , Pachelbel , Buttstedt , Bach , Reger , 
 round Karg - Elert fine Improvisation /2 dudci 
 jubilo . Mr. Farmer announce series historical 
 recital fridaysat8 p.m. place January 
 19 , remain recital February 9 ( French ) , | 
 March 2 ( English ) , March 23 ( general 

 excellent music hear St. Martin - in- 
 - Fields Saturdays January : Parts 1 2 
 Christmas Oratorio , Holst Songs Voice Violin , | 
 song Farnaby , Movzart , Vaughan Williams , | 
 String quartet Beethoven Elgar , & c. singer | 
 Miss Beatrice Hughes Pope , Miss Elsa West , Mr. | 
 Norman Stone , Mr. Eustace Belham ; string | 
 player , Mesdames Elsa West , Elsie Bernard , Emily | 
 Wingfield , Hildegarde Arnold . Messrs. Bernhard Ord , | 
 L. Stanton Jefferies , Rev. G. Sydenham Holmes | 
 organ 

 Mr. A. M. Gifford , Hunstanton , write ask 
 reader identify organ work Lemmens , 
 open 

 . 3 

 Beethoven 

 choirmaster 

 
 Bermondsey 

 dreadful prelude Lohengrin . . 
 Adelaida Beethoven . — Darius Milhaud 

 opera libretti general write class 
 people Thompson - Bywaters hanging 
 providentially follow mystery lock - 
 tailor . — /ercy A. Scholes 

 series lecture french musical history , 
 M. Louis Bourgeois Institut Francais du Royaume 
 Uni , progress 2 , Cromwell Gardens , S.W.7 . | 
 subject remain lecture : ( February 9 ) | 
 Gabriel Fauré ; ( February 23 ) Charpentier Reynaldo 
 Habn ; ( March 3 ) Messager ; ( March 9 ) d’Indy 

 annua ! West - End Festival Sunday School | 
 Choir place Royal Albert Hall on| 
 February 17 . programme perform 
 choir orchestra thousand Mr. W. H. Scott 
 include selection Beethoven ruin Athens | 
 Spohr Judgement 

 Bristol 

 BARNSTAPLE.—At Musical Society concert 
 December 11 choir sing Parry 7here zs old Belief , 
 Sterndale Bennett come , dive , part- 
 \ Sonata flute pianoforte Barnett , 

 song . 
 ’ cello pianoforte Beethoven , 
 instrumental item . Dr. H. J. Edwards Mr. Sydney 
 Harper conduct 

 BIRMINGHAM.—A Sunday night concert Futurist | 
 theatre introduce Leeds Trio Birmingham . 
 Mr. A. Cohen leader , Messrs. Hemingway 
 Herbert Johnson colleague . Rachmaninov 7?7e 
 Elégiague beautifully play , desire hear 
 , Franck early F sharp minor Trio , 
 play , offer Franck find | 
 . Mr. William Heseltine , Zhe Zmmortal Hou 
 cast London , hear song . — — 
 Christmas season bring concert Madame Elma 
 Baker . seasonal appropriateness secure draw 
 Christmas music Bax , Vaughan Williams , Holst . 
 Boughton choral arrangement * Holly Ivy ' | 
 carol Aeth / ehem programme.——Sir Henry | 
 Wood conduct Festival Society usual Boxing - Day | 
 performance 7he AMessiah.——Two day later | 
 work Walsall newly - form Free Church | 
 Choirs Society Mr. Graham Godfrey.——At 
 recital December 22 Miss Rebe Hillier sing Chausson | 
 Chanson Perpetuelle , Paul Beard Quartet Mr. 
 Michael Mullinar co - operate instrumental music . 
 occasion appearance Quartet , | 
 member Messrs. Beard , Cantell , Venton , Dennis , | 
 City Orchestra.——Tchaikovsky Trio 
 mid - day concert year Miss Marjorie | 
 Sotham Messrs. Paul Beard Johan Hock . | 
 second Miss Dorothy Silk sing Brahms song | 
 group old english song exquisitely refined manner . | 
 — — Kunneke light opera , Zhe Cousin , | 
 wide success continent , | 
 english performance Prince Wales Theatre ! 
 Boxing- Day 

 combine choir orchestra Oxford Cambridge 

 perform Beethoven Mass D. programme 
 probably include Vaughan Williams 7owarads Unknown 
 Region , Cyril Rootham Arown earth ( chorus , semi- 
 chorus , orchestra ) , Stanford /r7sh Nhapsody . 
 June 2 - 8 Festival british music hold 
 Cambridge , include kind music 
 16th , 17th , 18th , tyth , 20th century . 
 choral orchestral concert , concert chamber music , 
 unaccompanied choral work , 1isth - century opera , folk 

 dancing , possibly river - procession accompani- 
 | ment Handel Water J / ust-.——Prior Albert Hall 

 concert , C.U.M.S. choir orchestra perform 
 Beethoven Mass D , Cambridge , February 9 , 
 soloist ' English Singers . ' 
 CarRpDIFF.—Madame Tetrazzini , Mr. Lauri Kennedy , 
 Mr. John Amadio , Signor Baggiore , M. Bratza , 
 Mr. Ivor Newton ballad concert December 16 . 
 — — Park Hall concert December 17 
 orchestra play Saint - Saéns Ze Nouet ’ Omphale , 
 vocalist Mr. John Perry Mr. Cuthbert Pardoe . 
 — — Capitol date chief feature 
 Schubert Unfinished Symphony.——At Park Hall 
 concert December 3 ! Litolff Overture A’ohespierr 
 Coleridge - Taylor Ballade minor play 
 orchestra . Mr. Adolphe Hallis , south african 
 pianist , play , Miss Beatrice Miranda sing . 
 CuATHAM.—The Royal Marine Orchestra play new 
 Suite de Ballet , J / y Lady Dragon - fly , Finck , 
 December 11.——On date annual 
 concert Medway School Music Mr. Leslie 
 Mackay Choir , singe Svs / er , awake ( Bateson ) , 
 Elgar /¢ come Misty Ages Love tempest , 
 Austin lady-/ove ( female voice ) , hey nonny 
 ( Armstrong Gibbs ) , Lo ! country sport ( Weelkes ) ( 

 male voices).——On January 2 band Royal 

 LES — FEBRUARY | 1923 133 

 Mr. Lloyd Hartley ( pianoforte ) play Beethoven 
 Ravel . — — subscription Concert January 7 , Bach 
 Concerto violin perform Miss G. Davey 
 Mrs , J. S. Hartley 

 CRAWLEY.—Parry Pied Piper Hamelin 
 Crawley Ifield Musical Dramatic Society 
 January 4 , Mr. Courtenay Robinson conduct 

 information construction history 

 piece play it.——at Patterson orchestral 
 concert , January 8 , programme include Beethoven 
 C minor Symphony , Strauss Don Juan , Berlioz Carnaval 
 Romain Overture , close scene Gotterdimmerung , 
 Miss Florence Austral Brunnhilde 

 EX&tTeER.—At annual concert , December 29 , 
 Isca Glee Singers sing interesting arrangement 
 W. J. Cotton ( alto party ) / # sheltered vale , 
 baritone solo trio accompaniment , Schafer 
 come away , pretty maiden , Uatton Summer Eve , 
 Horsley Celia arbour.——the central feature 
 programme perform Cl.amber Music Club 

 Elgar Variations.—It work high value 
 beautiful style . pseudo - classic expression , remark- 
 able disposition light shade , brilliancy , 
 rich tone - relief , highly complex 

 Symphony.—The principal work _ 
 programme Mr. Dunhill Symphony . whilst 
 direct line succession , lead Beethoven 
 Bruckner , Brahms , Mahler , Symphony 
 free modern exemplification sonata - like style 
 standard symphonic composition . Mr. 
 Dunhill construct characteristic theme 
 suitable development , employ plenty contrast . 
 breadth rhythmic fizesse . work 
 enormous success , Mr. 
 Dunhill composer conductor . lead 
 player quiet moderate persuasive gesture , 
 chef Worchestre reveal _ skill _ fund 
 experience enable hold complete ensemble 
 hand . conductor accomplish 
 rehearsal difficult programme 
 music unfamiliar player 

 reception . Franz Schreker Chamber Symphony 
 programme , repetition bring 
 conviction composer allege superiority 
 present - day composer . concert December 20 
 Corelli famous Concerto Grosso Bach B minor Suite 
 item hearer glad able 
 enjoy . occasion excellent parisian violoncellist , 
 M. Gérard Hekking , masterly reading Schumann 
 seldom - hear Violoncello Concerto . Mengelberg having 
 unfortunately undergo operation , concert 
 conduct M. Dopper , direct concert 
 January 4 , scheme comprise Berlioz 
 Overture Carnaval Romain , Goudoever Suite 
 violoncello orchestra ( youthful composer 
 sustain solo ) , Strauss Don /uan , 
 Saint - Saéns B minor Concerto , play superior style 
 M. Zimmermann . concert Prof. Carl Fiedler 
 fine reading Brahms fourth Symphony . 
 concert January i1 — symphony 
 concert conduct Mengelberg prior departure 
 America — include repetition C. R. Mengelberg 
 Symphonic Elegy Mahler fourth Symphony . 
 vocalist evening Mlle . Mia Peltenburg , 
 win assured position 

 subscription concert Madame 
 Berthe Seroen M. E. Cornelis , January © , prove 
 remarkably successful way . 
 chamber concert Concertgebouw 
 Sextet , player hear Chamber Sonata 
 Handel , Scherzo Dopper , Beethoven Trio , Op . 11 , 
 original version ( clarinet ) , interesting 
 novelty Amsterdam , Josef Holbrooke Sextet , Op . 33 , 
 pianoforte wood - wind , gain cordial 
 reception . W. HARMANS 

 MUSICAL TIMES — FEBrRuaryY 1 

 Mr. Kurt Schindler , Schola Cantorum , 
 Christmas concert present material cull 
 field . number programme , 
 half novelty New York listener . 
 old italian Christmas hymn ; old french Christmas 
 song ; catalonian song ; russian child folk- 
 tune , anthem Rachmaninov , basque folk - tune 
 arrange Mr. Schindler . number boy ’ 
 voice assist choir , song sing 
 original language , Latin , French , catalonian , Russian , 
 Basque . Mr. Schindler attention fact 
 catalonian song good example 
 ' migrate folk - song , ' melody sing 
 country 

 recital field indebted Mr. Ernest 
 Hutcheson series great master 
 pianoforte music ( Bach , Beethoven , Schumann , Chopin , 
 Liszt ) , afternoon devote composer ; 
 Mesdames Gerhardt Frieda Hempel 

 remarkable new singer hear 
 Metropolitan year Miss Elizabeth Rethberg , 
 young girl Germany , advance press - agent . 
 freshness beauty voice , wide range 
 abundant power , great surprise audience 
 débiit Aida . 
 successful Sieglinde , simply 
 fully ease , sing role 

 Augusteum season open Verdi 
 Requiem Mass , hear Rome 
 1913 . repeat time , magnificent 
 success 

 worthy note time write 
 Parisian Quartet Capet visit Rome , perform 
 entire series Beethoven Quartets Santa Cecilia 

 LEONARD PEYTON 

 Philharmonic Orchestra , Vienna boast } little substance . Hindemith late work , String ( Quartet , 
 organization rival superb playing Prague ! Op . 22 , hear private production , considerably 

 Philharmonic Orchestra , Vaclav Talich , recently 
 pay short visit . concert 
 form Vienna - Prague Exchange 
 Concert scheme , provide number 
 concert Prague Vienna Symphony Orchestra 
 February , direction Franz Schalk . frankness 
 compel statement hardly fair deal 
 Vienna Symphony Orchestra , decidedly second - rate 
 hardly suited convey correct idea Vienna 
 orchestral status . hand , Vienna pro- 
 gramme Prague Orchestra , lay particular stress 
 present exclusively czech music , hardly permit 
 judgment player ’ capacity classic , 
 definite pronouncement quality withhold 
 pende opportunity hear play Beethoven 
 Brahms . doubt , , power 
 interpret music compatriot , e.g , 
 Smetana Josef Suk . big Symphony last- 
 , entitle Asrac/ , scholarly work 
 dedicate memory composer wife 
 father , Anton Dvorak , form programme second 
 concert . distinguish quality common tothe conductor , 
 M. Talich , member Orchestra , 
 abandon enthusiasm , temperament 
 racial inheritance . hold true great 
 degree Bohemian String ( Quartet , Suk 
 prominent member . fame ( Quartet long 
 stand , present reorganize form artist 
 live standard excellence 
 traditional . important work present 
 new String ( ) uartet , Op . 35 , Vitezslav Novak , 
 composition moderately modern character 
 entirely free german influence 

 Oscar Nedbal , Furtwiingler predecessor Vienna 
 Tonkiinstler Orchestra prominent figure 
 rumanian musical affair , return Vienna 
 long absence . display old ' athletic ' 
 method conducting , sound musician- 
 ship . selection comic opera /easant /acob — 
 work strongly influence Puccini 
 memory Nedbal operetta — overture 
 Marionette Play Jaromir Weinberger , novelty 
 programme . - piece , write 
 czech composer age seventeen , witty 
 clever counterpart Korgnold ballet , 7%e Snowman , 
 contemporary 

