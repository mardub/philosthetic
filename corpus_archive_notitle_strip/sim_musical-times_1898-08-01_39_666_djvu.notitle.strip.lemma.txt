of Praise " ( Mendelssohn ) . OcToBerR 29 ; NOVEMBER 12 , 26 ; DECEMBER 10 . 
 | THURSDAY , at 11.30 , New Cantata ( Parry ) ; " Eroica ’' Symphony 18 

 Beethoven ) ; ' ' Christmas " Oratorio ( Bach ) . 99 

 4 " FRIDAY , at 11.30 , I cagencsin ™ " eee 
 ; N the SHIRE HALL : — a te . ARTHUR W. PAYNE . 
 £ WEDNESDAY evening , at 8 , Orchestral Works — Coleridge- |   Gramah’t and Accompanist et oeRCY PITT . 
 e io Taylor and Wagner ; Choral Ballad ( Rosalind F. Ellicott ) ; ' ' Golden 
 fo me i ( Sullivan ) . ain sic nacelle tain MR . ROBERT NEWMAN ’s 
 eo mission : reserved seat , 15s . to 1 . 6d . ; Serial ticket , £ 4 each . 7 
 : 2 for programme , ticket , & c. , apply Partridge and Robins , SUNDAY afternoon CON cert , 
 3d . Westgate Street , Gloucester . conductor : Mr. HENRY J. WOOD , 
 | 3d , will be resume in the autumn . 
 : ROYAL ACADEMY of MUSIC , the QUEEN 's HALL ORCHESTRA of go . 
 4 TENTERDEN STREET , W. eonmcrneniae 
 3d . ; Principal Violin .. Mr. ARTHUR W. PAYNE . 
 6d , institute 1822 . incorporate by Royal Charter , 1830 . _ organist and accompanist Mr. PERCY PITT . 
 a patron : her Majesty the QUEEN and the RoyAL FaMILy , the LONDON MUSICAL FESTIVAL , 
 president : H.R.H. THe DuKE oF SAxE - CoBURG and GOTHA . 8 
 — s , principal : Sir A. C. MAcKENz1E , Mus . Doc . 1099 . 
 under the management of Mr. ROBERT NEWMAN . 
 Michaelmas term begin Monday , September 26 . entrance exami- — _ — — 
 nation therefor , Thursday , September 22 , at 10 . QUEEN ’S HALL : — 
 prospectus , entry Forms , and all information may be obtain from | Monpay , May 8 ' 4 " a as ' ae + » at 3 and 8.30 p.m. 
 the Secretary . . W. RENAUT , Secretary . TurEspay , Mayg . at 3 and 8.30 p.m. 
 Wepnespay , May to at 3 and 8.30 p.m. 
 the ROYAL COLLEGE of ORGANISTS . THURSDAY , May : ee at 3 and 8.30 p.m 
 4d . — Fripay , May 12 at 3 and 8.30 p.m. 
 = Pi pe Loans , | be - daily from 10 a.m. to 5 p.m. , also on Saruapay , May 15 .. oe 
 4 uesday an ursday from 5 p.m. to 7 p.m. on Saturdays the 
 # College be open from 10 to 1 o’clock . the College will be close from the QUEEN ’s HALL ORCHESTRA of 103 
 A July 30 to August 3 . conduct by Mr. HENRY J. WOOD 
 at member desire practice on the College Organ may obtain par- 
 3d ticular on application . AnD 
 3d . se and sundry small room , may be hire for concert , the LAMOUREUX PARISIAN ORCHESTRA of 100 
 meeting , & c J 
 at : E. H. TURPIN , Hon . Secretary . conduct by Mons . CHARLES LAMOUREUX 
 3d . Hart Street , Bloomsbury , W.C. TOGETHER with 
 > : J ' J LOISTS 
 3d . HE GUILDHALL SCHOOL of MUSIC . — EMINENT VOCAL and ya itaiaeaaas so 

 Karl Ludwig Klindworth be bear at 
 Hanover , September 25 , 1830 . his father be 
 a clever engineer and optician , a lover of all 
 form of art , a player on several instrument , 
 and a good singer . young Karl be self- 
 teach in regard to the pianoforte . ata party 
 give on his mother ’s birthday he play the 

 overture to the " ' Caliph of Bagdad " ( Boieldieu ) 
 when he be six year old . but the violin be 
 the instrument of his boyhood . study it 
 under a careful master , he attain to a con- 
 siderable degree of virtuosity . he play the 
 concerto of Rode , Molique , and Vieuxtemps , 
 the Fantasias and Studies of Paganini and 
 Ernst , and in all the quartet by the great 
 master . he be passionately fond of singe 
 ( he sing all the ballad of Léwe ) and especially 
 of play the pianoforte score of opera ; he 
 would pass the whole day , from morning till 
 night , in this engross occupation . Karl first 
 became acquainted with the music of Wagner 
 in atypically boyish way . when he be fifteen 
 he be attract to a folio volume by reason of 
 its unusual bulkiness ! ' what ’s this ? " he 
 enquire with youthful curiosity . it prove 
 to be ' Rienzi , ' which , as usual , he eagerly 
 play and study . " as a boy , " say the 
 Professor , ' I be always fond of score . not 
 be able to get any from the library , I copy 
 no end of they — ' Der Freyschiitz , ' ' Don Juan , ' 
 ' the Huguenots , ' the Ninth Symphony , and 
 the quartet of Beethoven from the part , all 
 such work be of the great advantage to 
 me in after life . ' why do I leave the violin ? ' 
 well , I want to study under Spohr ; but my 
 father ’s large family , with his reverse of 
 fortune at the time , compel I to apply 
 to King August , of Hanover , for money to help 
 me . but my request be refuse 

 OPERA conductor at seventeen 

 life at WEIMAR 

 my companion at Weimar , " say pro- 
 fessor Klindworth , ' be Peter Cornelius , 
 Ferdinand Laub , and Bernhard Cossmann . 
 Liszt ’s visitor include the two Wieniawskis , 
 Marx , and Moscheles ; Ferdinand David and 
 Brendel from Leipzig ; Brahms , who bring 
 his three sonata and other composition to 
 show Liszt , previous to his submit they to 
 Schumann ; Bettina von Arnim ( the young 
 friend of Goethe and Beethoven ) and her two 
 charming daughter ; Berlioz , whose visit be 
 the occasion of a Berlioz Festival , when per- 
 formance take place of his ' Benvenuto 
 Cellini , " ' Faust , " and " Romeo and Juliet . " 
 there be a difficulty in find sufficient 
 orchestral player , we young fellow place 
 ourselves at Liszt ’s disposal — Hans von Bilow 
 play the big drum , Dionys Pruckner the 

 triangle , myself the cymbal , and we obtain 
 high commendation on our infallibility from 
 no less a personage than the great Hecto 
 himself 

 MR . J. W. DAVISON ’S criticism 

 Professor Klindworth make his first public 
 appearance in London at one of John Ella ’s 
 " musical Winter Evenings . " 
 March 30 , 1854 , the place Willis ’s Rooms , 
 when the ' pupil of Liszt " play Beethoven 's 
 sonata in C ( op . 2 , no . 3 ) and Liszt 's 
 Fantasia on Mendelssohn ’s ' Midsummer 
 Night ’s Dream . " ' this young man , " say 
 Ella in the previous programme , ' come with 
 good testimonial from Joachim , and _ his 
 master — the hungarian magnate , Liszt , under 
 whom Klindworth have study two year at 
 Weimar . " Mr. J. W. Davison , the great critic 
 of the day , who be not particularly genial in 
 the reception he accord to pianist not of 
 english make , thus criticise Herr Klindworth ’s 
 performance in the musical World . ( the 
 date of the paper , the 1st of April , 1854 , should 
 be note 

 a new pianist , Herr Klindworth , a pupil of M. Liszt , 
 make his début on this occasion . he play twice — 
 Beethoven ’s Sonata in C ( no . 2 of the Haydn set ) , and a 
 fantasia by M. Liszt , on the two theme of Mendelssohn 's 
 Wedding March in a Midsummer Night ’s Dream . Hert 
 Klindworth be evidently young , and at present his talent 
 do not look very promising . he exhibit the fault of 
 his master with . none of his beauty . he thump the 
 instrument with right good will , and be by no means exact 
 in his execution . his mechanism , indeed , be very defective ; 
 and this be even more clearly show ( less clearly , if you 
 please ) in the fantasia of Liszt , which be one of the most 
 difficult , and at the same time one of the most incoherent 
 and unmeaning bravura piece to which we ever listen . 
 Liszt must have have some spite against Mendelssohn when 
 he write it . there should have be some heavy tax 
 upon these fantasia - maker , to prevent they from mangle 
 and caricature the work of great master 

 we do not like Herr Klindworth ’s reading of Beethoven ’s 
 Sonata . it be exaggerated in expression and incorrect 

 the good point be the scale in sixth and third , which 

 effect of Klindworth ’s pianoforte score of the 
 ' Ring ' with Bilow ’s version of the score of 
 ' Tristan , ' Tausig ’s of the ' Meistersinger , 
 and Joseph Rubinstein ’s of ' Parsifal . ' Klind- 
 worth ’s version be perhaps even hard to play 
 than any other , but it be more efficient ; it 

 Liszt ’s transcription of Weber ’s overture , 
 Berlioz ’s and Beethoven ’s symphony and 
 concerto . Liszt ’s transcription be Klind- 
 worth ’s model 

 wo nt to express great admiration for Biilow ’s 
 ' Tristan , ' which he describe as ' the impossible 
 make possible ' — still , he declare that the 
 effect at the pianoforte be not always satis- 
 factory — ' Die Basse sind zu jung ' ( ' the 
 bass be too thin ' ) . with Klindworth the 
 player ’s left hand have exceptionally hard work 
 to do , but the bass stand forth grandly . 
 Tausig ’s method be too subtle , too pianistic . 
 Tausig confess that he have not realise 
 Wagner ’s tempo from the MS . score before he , 
 and that at the true speed his virtuoso con- 
 trivance be rather in the way . much the 
 same thing may be say , and more emphatically , 
 of Joseph Rubinstein ’s version of the score of 
 ' Parsifal ' and the ' Siegfried Idyll , ' the first 

 reflect the orchestra score as closely as do 

 once only I hear Klindworth play in 7 
 public , when he take part together with the 7 
 late Walter Bache in the performance of an 7 
 arrangement for two pianoforte of one of © 
 in private | 7 
 have hear he play , week after week , and for 7 
 year , a good deal of Liszt , Beethoven , and 7 
 Chopin , and the whole of his own pianoforte 7 

 sitet 

 Episode de la vie d’un artiste ’' and " Retour } Suites , " some of Schumann ’s pianoforte work , § , , oe 

 a la vie " symphony , all his overture , and|and all Beethoven ’s pianoforte sonata . Alg ies 

 Faust " ( twice ) ; Beethoven : Ninth Symphony| ' 24 grosse Etiiden , ' be a selection he ane 

 three time ) and the great mass in d;|has make from Clementi ’s ' " Gradus aq _ 

 receive the benefit of Professor Klindworth ’s 
 editorial supervision . the work be now in the 
 press and will shortly be publish by messr , F 
 Novello . a perusal of the proof sheet not 
 only reveal that critical insight and character . 
 istic acumen which one naturally look for in 

 his work , but every page give evidence of his the 
 wide experience as a teacher and complete f ended 
 knowledge of the art of pianoforte playing , dining 
 phrasing and fingering have receive minute | queer 
 attention , and the preface help to demonstrate telegr 
 the thoroughness with which the Professor have f " yo 
 discharge his task . it will cause no surprise f " y | 
 if , as in the case of the work of Chopin , the — s@ ™ 
 Klindworth edition will become the edition of | " th : 
 Mendelssohn ’s " ' song without word . " oa 
 LOVE of ENGLAND . an in 
 " « six week of a most happy time , one of the — 4 loc 
 most enjoyable I have have in my life . I shall & place . 
 always think of you all with deep gratitude ,   eavel 
 and the remembrance of it will always remain F shone 
 a source of great pleasure tome . " thus write f spark 
 the Professor to the present writer immediately § curios 
 after the conclusion of his recent visit to jp telegr 
 London . " I shall perhaps come back again Ft " 
 in the autumn to conduct a Symphony concert § dour 
 of my own , and another concert for Frederick — mood 
 Dawson , " he remark in the course of conver : § when 
 sation . " if I be only a ' capitalist ' I would § few 
 build a cottage at Hampstead Heath , where ! 9 andt 
 could live very happily . " > ia 
 a recital before an audience of one . [ 7 ~y 
 Professor Klindworth do not play in public 9 holde 
 during his sojourn in London in the leafy month / ) week . 
 of June . but he give a ' strictly private " | ) be , 
 pianoforte recital ( by invitation ) on the day © nothi 
 that he leave . the hour be the unconventional © frien : 
 one of 8 a.m. : the only listener be the writer } ) enou , 
 of this biographical sketch . piece by Liszt , © nong . 
 Chopin , and Beethoven be play as only a © in go 
 great artist can playthem . vigour , refinement , — deyic 
 accentuation , technical facility , self - effacement , — that 
 and , above all , true musicianship be combine , he co 
 as they rarely be in the same individual , with — carm 
 ideal result . plea 
 there be a felicitous appropriateness in the | his « 
 Professor ’s choice of Beethoven ’s sonata " Les " Bu 

 Adieux , l’Absence , et le Retour . " as the 

 the MUSICAL TIMES.—Avucust 1 , 1898 

 the same day Herr van Rooy give a humorous 
 recital at St. James ’s Hall , and Herr Mottl 
 give a magnificent reading of the part of the 
 Prophet in " Elijah ' at the Alexandra Palace . 
 Dr. Joachim ’s embodiment of the part of 
 Siegfried be lack in youthful exuberance , but 
 he imitate the " Bird ’s Song " very effectively 
 on a violin which he make out of a few stage 
 reed . the following day M. Jean de Reszke 
 reproduce this incident at his recital with 
 Mr. Leonard Borwick of five of Beethoven ’s 
 symphony , which have be specially arrange 
 by : M. Korderowski for violin and flute . 
 Mr. E. F. Jacques ’s analytical programme on 
 this occasion be very edifying . M. Paderewski 
 have be specially engage for the part of 
 Loge in ' Das Rheingold " on account of his 
 hair , which give a remarkably realistic effect 
 to the character . early in the season 
 M. Paderewski ’s appearance in the title - rdle 
 of " Orfeo " must be regard as the crowning 
 triumph of the Covent Garden Syndicate , for 
 no one could doubt that his pianoforte playing 
 would secure his passing safely through Hades 
 into the well - woode pasture of the Elysian 
 fields , whereas the efficiency of the lyre 
 hitherto see have always be open to question . 
 his descent be very cleverly manage . three 
 stage manager from Bayreuth have _ be 
 engage to bring the apparatus use in the 
 first scene of ' Das Rheingold , " and it be a 
 beautiful sight to behold Paderewski and his 
 resonator float in space , while scale and 
 arpeggio stream forth in all direction . 
 there be many other embodiment that will 
 long be remember . Sir Henry Irving sing 
 exquisitely as Romeo . Miss Ellen Terry as 
 Briinnhilde be one of the surprise of 
 the season , and Miss Marie Brema on 
 one occasion nearly clear the stage by 
 her dauntless horsemanship on a bare backed 
 steed who possess a singularly wild eye . it 
 may not be generally know that the one aim 
 of the Royal Opera Syndicate have be 
 educational . for this purpose Madame Melba 
 be engage to instruct the audience how to 
 sing scale ; Madame Eames to show the effect 
 of emotion control by a constant desire to be 
 graceful ; Madame Calvé to teach the art of 
 fascination ; Fraulein Ternina to inculcate 
 appropriate expression ; Miss Adams _ to 
 encourage youth ; and M. Saléza to expose the 
 evil of exaggeration . the most successful 
 artist of the season at Covent Garden be 
 Mr. Neil Forsyth , who in his thought reading 
 interview display an acuteness that be 
 marvellous . having gain possession of 
 Wagner ’s original tarnhelm , his sudden dis- 
 appearance be no less wonderful than the 
 celerity with which he appear when want , 
 restore diamond brooch to disconsolate 
 lady , and find wander stall and private 
 box . he perform every night . I fear this 
 article be not quite as clear or comprehensive 
 as it should have be , but it be useless my 

 mention what my reader remember , and 
 they would be sure not to forget anything they 
 wish to recollect , and therefore I may say 
 logically that my chronicle of the season be 
 complete . a. ¥ . Z 

 those of our reader who may be take their 
 holiday within reach of Dresden during the last week 
 of this month , or in September , may find the follow 
 information ( kindly supply to we by a correspondent 
 in Germany ) useful 

 date of the performance of Wagner ’s opera in Dresden : 
 August 24 , ' Rienzi ' ; 27 , ' Der fliegende Hollander " ; 
 30 , * Tannhauser " ' ; September 2 , ' ' Lohengrin " ; 6 , ' Die 
 Meistersinger ' ; 8 , ' ' Tristan und Isolde " ; 13 , ' Rhein- 
 gold " ; 14 , " Die Walkie " ; 16 , ' Siegfried ' ; 20 , 
 " " G6tterdammerung . " on the intermediate evening will 
 be give : Gluck ’s " Iphigenie in Aulis " and " Iphigenie in 
 Tauris " ; Beethoven ’s ' ' Fidelio " ; Mozart ’s " * Don Juan " ; 
 Méhul ’s " Joseph in Egypten " ; Berlioz ’s ' ' Benvenuto 
 Cellini " ; Verdi ’s ' " ' Amelia " — i.e. , ' Ballo in Maschera " ; 
 Rossini ’s ' ' Barber of Seville . " the price will be as 
 usual — i.e. , from 7 mark to 1 mark , accord to the 
 position of the seat 

 a Fresu proof of the attention now bestow upon 
 worship - music by nonconformist be afford in the 
 recent issue of the " Bible Christian Sunday School 
 Hymnal " ( 26 , Paternoster Row , or Novello and 
 Company , Limited ) . this hymnal , contain up- 
 ward of 500 tune , on the commendable fix - tune 
 system , have be edit by Mr. J. R. Griffiths with 
 conspicuous success , the editor having discharge 
 his responsible duty with sound judgment combine 
 with a full knowledge of practical requirement . 
 a very unusual feature , but one of general 
 interest in such book , be to be find in the 
 " alphabetical index of tune , " which contain 
 the early know " first appearance " of the tune 

 a ’ Almaviva , Basilio , and Bartolo respectively 

 we probably have to thank Fraulein Ternina for the 7 
 only performance this season , on the gth ult . , of Beethoven 's © 
 " Fidelio , " in which it be not too much to say that she ~ 
 present one of the fine embodiment of the devoted 

 wife see on the Covent Garden stage 

 533 

 e 
 tility , and and it be doubtful if the work ever receive a more brilliant Henniker , Mr. Kennerly Rumford , and M. Joseph Hollman . 
 1 for be and sympathetic interpretation . Mozart ’s Sonata in A|Miss Hewitt ’s style reflect in a measure that of her 
 of delight ( no . 17 ) be follow by Tartini ’s familiar sonata ' Il | teacher , and when she have gain great confidence and 
 Catherine trillo del diavolo . " a very fine interpretation be give of | command of expression , her playing will be still more 
 which she Beethoven ’s sonata in c minor ( Op . 30 , no . 2 ) , but the | attractive than it be at present . she have acquire an 
 ment will climax of the evening may be say to have be the | admirable /egato , which be advantageously show in her 
 > artist in performance of Brahms ’s beautiful composition in g/| rendering of Thomé ’s ' " L’Extase , " and her fluency in 
 ten for 2 ( Op . 78 ) , which conclude the selection and be interpret | rapid passage be display in Hubay ’s ' Echo des 
 at to the with rare keenness of perception of the spirit of the music | Alpes " and in Wieniawski 's " ' Air Russe 

 with the and consummate executive ability 

 asic with command of the keyboard which entitle he to be rank | from Wagner 's work , because " ' nothing could be chip 

 m sing | amongst the so - call musical prodigy of to - day . he | off the wagnerian block " ; but from Verdi ’s opera a very 
 ira , but | be hear in the Presto from Bach ’s " italian " Concerto , | happy choice be make in Jago ’s Credo from ' Otello . " 
 art wert ; Schubert ’s Impromptu in e flat ( op . 90 , no . 2 ) , Chopin ’s | Beethoven ’s ' ' Adelaide , " and some modern french song , 
 bert , and q Impromptu in a flat , and other piece , all of which be | together with several pianoforte duet , which be 

 play with marvellous executive fluency and taste for one vivaciously i by the Mdiles . Douste de Fortis , be 
 ich be of such tender year . some violin piece be contribute | also include in the programme 

 MUSIC in LIVERPOOL . 
 ( from our own correspondent 

 although no definite or official pronouncement have 
 yet be make in regard to the approach season , some 
 come event have cast their shadow before . thus the 
 Philharmonic Society contemplate the production of Verdi ’s 
 ' Stabat Mater ' ? and Te Deum , of Beethoven ’s ' " * ruin of 
 Athens , " and of Handel ’s ' Israel in Egypt " this side of 
 Christmas , while Cowen ’s ' ' Water Lily " ? and Rossini ’s 
 " * Stabat Mater " be talk of respectively for the ninth 
 and twelfth concert . Mr. F. H. Cowen , of course , retain 
 his position as conductor , and Mr. H. A. Branscombe 
 that of chorus - master 

 a new chief have be find for the Musical Society in 
 the person of Mr. F. H. Crossley , who have do excellent 
 work during many year past as conductor of choral 
 society at Warrington , Runcorn , and Newton - le- Willows . 
 no information be to hand with regard to the programme 
 for the coming season , but , judge by what Mr. Crossley 
 have achieve elsewhere , a good one may be look for 

 voice and musical ability , and none but thoroughly com . 
 petent chorister have be admit , and all receive 
 definite emolument for their service , the two first choral 
 concert be to be devote to Mendelssohn ’s " Elijah " and 
 the last act of Wagner ’s ' Tannhauser 

 Mr. Granville Bantock have be give some Capital 
 concert at the Tower , New Brighton , and at the time of 
 writing be just midway through a cycle of eight Beethoven 
 Symphonies , which commence on Sunday , the 3rd ult , 
 and have continue weekly since . it be at an adjacent 
 place of recreation , by the way , that the first Sunday con . 
 cert ever give in the North of England — viz . , at the New 
 Brighton Palace — be exploit in 1883 ; but they be 
 crush out of existence by the same local authority which 
 have now accord a license to their lineal descendant , 
 it be well to see that some progress be be make in the 
 district in question , albeit that its movement be somewhat 
 in the nature of an Adagio 

 MUSIC in MANCHESTER . 
 ( from our own correspondent 

 Leo Smith ( Mr. Fuchs ) , the Vieuxtemps Concerto in E by 
 young Arthur Catterall , the highly gifted pupil of Mr. 
 Brodsky ; the Saint - Saéns Concerto in G minor , play by 

 Helen Brown , and the open movement of Beethoven 's 

 Concerto in b flat by young Edward Isaacs , both pupil of 
 Miss Olga Néruda ; and the performance of Mr. Dayas ’s 
 student , Irene Schaefsburg ( d’Albert ’s Concerto in E ) , 
 Edith Webster ( Chopin ’s Concerto in e minor ) , Muriel 
 Blydt ( the Weber - Tausig ' invitation to the dance " ' ) , and 
 Mary Spencer ( Bach 's fugue in g sharp minor ) . among 
 the singer Ruby Keighley and Milly Jones have greatly 
 advance under Mrs. Hutchinson ’s care 

 MUSIC in NOTTINGHAM . 
 ( from our own correspondent 

 Tue Sacred Harmonic Society have issue the prospectus 
 for its forty - third season . in importance it far exceed 
 any previous effort and afford a most pleasing contrast 
 to the dark time , now happily long past , when fund be 
 low , supporter few , and imminent collapse continuously 
 threaten the Society . the committee have again 
 secure the service of Mr. Henry J. Wood as conductor , 
 and include in their scheme two orchestral concert , with 
 the intention of encourage the formation of a strong 
 local orchestra . with Mr. Wood as conductor , the 
 orchestra have a promising beginning . the Midland 
 Orchestral Union , which be so pluckily venture upon 
 two season ago by Mr. Allen at his own risk , only 
 partly reveal the artistic possibility of such an 
 undertaking ; and it be certain that , with only moderate 
 financial backing , a good band will soon firmly establish 
 itself in this populous district . the concert announce 
 be a recital of Gounod ’s opera " ' Irene , " for November g , 
 and an orchestral concert , on December 8 ( for which we 
 be glad to notice the engagement of Miss Cantelo as 
 solo pianist in Beethoven ’s ' ' Emperor " Concerto ) . " the 
 Messiah " be down for Boxing Day ; Sullivan ’s ' Martyr 
 of Antioch " for February 9 ; the second orchestral concert 
 1 put down for March 2 ; and Mendelssohn 's " Elijah , " with 
 Madame Ella Russell , Miss Hilda Wilson , Messrs. William 
 Green and Andrew Black as soloist , will close the season 
 on March 23 . the spirited policy of recent year , which have 
 bring the Society to its present strong position , have be 
 greatly assist by the reserve fund of about £ 1,000 
 establish by alegacy from the estate of the late Mr. Alsop 

 MUSIC in PARIS . 
 ( from our own correspondent 

 Miss Maup AGNes WInTER at her concert in Queen ’s 
 ( Small ) Hall , on the 12th ult . , justify the hope of her 
 professor at Trinity College , London , and of her private 
 friend . 
 exceptional command of technique for a youthful executant 
 and perception of the varied spirit pervade the work 
 interpret . she play with correctness and fervour 

 Beethoven ’s " Sonata Appassionata , " the opening Allegro 

 be specially well render ; Schumann ’s " Papillons 

 549 

 after each of these solo Miss Winter be warmly compli- 
 mente . with Messrs. L. Szczepanowski and Hans 
 Brousil ( respectively violin and violoncello ) she contribute 
 her share towards an excellent rendering of Beethoven ’s 
 Trio in C minor . the vocalist be Madame Zippora 
 Monteith 

 Tue Alexandra Palace choir again display fitness for 
 the interpretation of standard sacred composition on the 
 23rd ult . , when ' " the hymn of praise ' ? be successfully 
 perform , under the baton of Mr. Henry J. Baker . the 
 chorus be sing with spirit and impulse , and the 
 rendering of the orchestration be generally meritorious . 
 this always acceptable work be precede by the overture 
 to " Athalie " and by Mr. W. Augustus Barratt ’s cantata 
 " Lancelot and Elaine . " the latter , not previously per- 
 form in London , have some effective point without be 
 specially striking in character . the composer receive 
 justice from all engage . Miss Marie Elba and Mr. 
 William Higley respectively give the soprano and baritone 
 solo expressively , whilst the execution of the choral and 
 instrumental feature be adequate 

 BarcELonA.—The first performance here of Berlioz ’s 
 Requiem take place on the 8th ult . , at the Palace of Fine 
 Arts , under the very able conductorship of Sefior Nicolau . 
 the grand work — the choice of which , under the prevail 
 circumstance of the country , be but too appropriate 
 — create a profound impression upon the numerous 
 audience 

 Ber.in.—After a very successful performance of the 
 " ' Nibelungen " cycle , the Royal Opera close its door at 
 the beginning of last month , the great part of the lead 
 member depart to enjoy a well earn rest , while some 
 few remain to support the personnel of the New Royal Opera 
 House ( formerly Kroll ’s ) , where performance of opera be 
 carry on during the summer month and before crowded 
 audience . here Madame Arnoldson continue to be a 
 great attraction in favourite part , while M. Lasalle , the 
 famous Paris baritone , also have to add several appearance 
 to those originally contemplate . at the West - end 
 Theatre Mdlle . Prevosti have be draw full house 
 in ' * Carmen , ' " ' Trovatore , " and ' Il Barbiere , " as 
 well as Frau Sedlmair , whose appearance in the part 
 of Fidelio and of Norma create much enthusiasm . this 
 theatre be to be entirely devote to opera in the future , anda 
 number of interesting work , both old and new , be promise 
 during the coming Autumn season.——A new concert hall 
 be shortly to be open here , intend chiefly for orchestral 
 and chamber concert . it will be call ' ' Beethoven Saal " 
 and will contain over a thousand seats.——The new 
 catalogue which have just be publish by Herr Liep- 
 mannsohn , the antiquarian bookseller , be devote exclusively 
 to Wagner , Liszt , and Berlioz , and contain 483 number , 
 379 of which appertain to wagnerian literature . — — 
 M. Jacques van Lier , formerly lead violoncellist of the 
 Philharmonic orchestra , have be appoint to a professor- 
 ship at the Klindworth - Scharwenka Conservatorium , where 
 he will also conduct a chamber music class 

 CARLSBAD.—Under the auspex of the Carlsbad Musik- 
 verein , a2 commemorative tablet have be place at the 
 house , in the Hirschensprung Gasse , where Johannes 
 Brahms reside during his sojourn here in 1896 . the 
 ceremony of unveiling take place on the roth ult . , and 
 include musical performance by the Cur Orchestra and 
 the local choral society , as well as an able address by 
 Musik - director Alois Janetschek 

 MiLan . — Umberto Giordano , the successful composer of 
 " André Chénier , ' ? have complete the score of a new 
 operatic work , ' ' Fedora , " the libretto found on the well- 
 know french drama , which be to be bring out at the 
 Teatro Lirico , of Milan , in October next . Signora Gemma 
 Bellincioni and the young tenor , Signor Caruso , will sustain 
 the principal part . — — owe to the activity display on 
 the part of an influential syndicate of amateur , ample 
 subscription have be forthcoming to ensure the 
 resumption of operatic performance at La Scala in the 
 coming season . under these circumstance , the Municipal 
 authority have , it be say , likewise consent to renew 
 their annual subvention of £ 6,000 . Signor Gatti - Casazza , 
 formerly manager of the Municipal Theatre , Ferrara , have 
 be appoint director of La Scala 

 Municu.—The thirtieth anniversary of a memorable first 
 performance — that of Wagner 's exquisite musical comedy 
 of ' * Die Meistersinger ’' — be celebrate , on June 21 , at the 
 Royal Opera by an excellent special performance of the 
 work , under Court - Capellmeister Fischer 's direction , and 
 with Fraulein Hofman , Herren Bertram , Mikorey , and 
 Friedrichs in principal part . on the occasion of its original 
 production the master have himself superintend the 
 rehearsal , Dr. von Biilow be the conductor and Dr. 
 Richter the director of the chorus , and the enthusiastic 
 reception accord to the work on the part of the great 
 majority of those present make some amend for the 
 unworthy intrigue which three year previously have cause 
 Wagner to give up his residence in the bavarian capital . 
 as regard the original interpreter , two , Herren Bausewein 
 and Schlosser , be still active member of the Munich opera , 
 while Herr Nachbaur , who " create " the part of Walther 
 von Stolzing , remain attached to the institution as an 
 honorary member . Herr Betz , of Berlin , who be the Hans 
 Sachs , have be present by the Prince Regent with the 
 gold medal for Arts and Sciences in honour of this notable 
 anniversary.——the new three - act comic opera ' Zinober , " ' 
 by Herr Siegmund von Hausegger , be bring out on 
 June 19 , at the Royal Theatre , under Herr Richard 
 Strauss ’s direction , and obtain a complete success , to 
 which an excellent interpretation contribute its share . 
 the libretto , from the pen of the composer , be found upon 
 Hoffmann ’s fanciful story , which have be effectively 
 dramatise , and the music be the work of a gifted and 
 frequently highly original composer . Herr von Hausegger 
 be only in his twenty - fifth year , and a native of Graz , — _ — 
 the now annually recur performance of Beethoven 's 
 nine symphony by the Kaim orchestra be announce to 
 commence on the 22nd ult . , under Professor Loewe ’s 
 direction 

 PRAGUE.—A new operetta , ' ' Der Opernball , " by Herr 
 Richard Heuberger , an esteemed viennese musical critic 
 and composer , be bring out at the German Theatre 
 here , on June 25 , with great success . the libretto be an 
 operatic version of the well - know comedy ' the Pink 
 Dominoes 

 a native of Taunton , be for nine year organist of her 
 > Majesty ’s private Chapel at Osborne , and the composer of 
 the " Osborne " March , write in 1887 for the Queen ’s 
 a Jubilee and frequently perform by the Queen ’s command . 
 © _ thedeath be announce , on June 24 , at Leipzig , where he 
 : have live in retirement for some year past , of HEINRICH 
 ~ Wotrr , the doyen of german violinist , at the age of eighty- 
 a on arrive in Vienna to give his first concert there 

 be surprised to find the city in mourning : it be the 
 | day of Beethoven ’s funeral ! Wolff subsequently make 

 the acquaintance of Paganini , with whom he be fre- 
 quently associate in the performance of chamber music . 
 amongst other treasured reminiscence of Wolff 's artistic 
 career be the friendship he form with Mendelssohn , 
 with whom he try over a number of the latter ’s com- 
 position from manuscript , amongst they the quintet 
 — two viola ) , in which Mendelssohn play the second 
 viola 

 arrange by 

 Slow Movement ( Pianoforte Quintet ) 8 . d , 
 " chumann G. C. Martin 3 . 24 . ' oe con saat < 4 > " * . Arthur B. Plant 2 5 
 Minuet ( twelve menuet for Orchestra ) ets a 
 Beethoven 25 . i " aie to va ea on St. Cecilia 's i Macpherson 1 § 
 Andante ( Pianoforte Sonata , a B ... ih é " - es Handel 
 chuber ' : arghetto ymp ) ony in a ) 
 = 0 aha bgt Handel G.C. Martin 1 6 ) 26 , { 8 tit 4 W. Marchant 1 9 
 s pants the hart " Calvary pohr Warum ? .. - rao 
 Agnus Dei(MassinG ) .. Schubert al { liebeslied as os } A , W. Marchant 1 9 
 ( os ture ( " Acis and Galatea ' ' ) Handel G.C. Martin 1 0 ad : Ss Oo 6 , 
 verture cis oe » \. tet 

 Albumblatter ( no . 1 , op.g9 ) Schumann } 28 . { " n = . eeu ue , ta ya a A. W. Marchant 1 9 

 Adagio(Sonata , op.2 , no.1 ) Beethoven 

 4 { theCater Fugue e ' jel G.C. Martin 1 6 x , { introduction and Fugue i ine fat iam A. W. Marchant 1 0 

 11 , Adagio in b minor Mozart A.W. Marchant 1 o jae " Lord , to thee my heart I profier ° 1d. ee 

 12 , Adagio ( Sextet , op.81 ) .. Beethoven .. A.B. Plant 1 0| 4 % Matthew Passion 

 13 . in Elysium ( ' Orphée " ) .. Gluck .. . I yond t Ol gr , { Andante con moto ( Symphony in inpfag ) W. Cruickshank 1 6 

 book a 
 Marche Funébre . oe 
 Moderato ( Quintet , Op . 18 ) . 
 Adagio ( Sextet , Op , 111 

 oe Bach . 
 + » Spohr . 
 Beethoven . 
 oo   aminck , 
 Schumann . , 
 luck . 
 Mendelssohn . 
 eo Bach . 
 Beethoven 

 Mendelssohn 

 Beethoven 

 Mendelssohn . 
 Mendelssohn 

 Kalliwoda . 
 Beethoven . 
 Kulak 

 Beethoven . 
 Mendelssohn . 
 Mendelssohn 

 celebrate marche 

 content 

 BEETHOVEN.—March from ' " ' Egmont . " 
 BEETHOVEN.—Funeral March . 
 CHOPIN.—Funeral March ( op . 35 ) . 
 HANDEL,—Dead March from " Samson . " 
 HANDEL.—Dead March from " Saul . " 
 HANDEL.—March from " Scipio . " 
 MENDELSSOHN.—Cornelius March ( op . 108 ) . 
 MENDELSSOHN.—Wedding March . 
 MENDELSSOHN.—War March . 
 MENDELSSOHN.—Funeral March ( op . 103 ) . 
 MEYERBEER.—March from " Le Prophéte . " 
 MOZART.—March from " Idomeneo . " 
 SCHUBERT.—March in b minor ( op . 27 ) . 
 SCHUBERT.—Marche Solennele . 
 SCHUBERT.—Grand March 

 Price four shilling and Sixpence net 

 compose BY 

 L. VAN BEETHOVEN 

 if 
 i 
 t 

 NOVELLO , EWER and CO . , NEW YORK 

 book 1 , | book 7 . 
 the Good Shepherd . . ee we ) we , Myles B. Foster | 2 + impromptu .. John E. West 
 i W. Warder Harvey ! 2 : Minuet , from Pianoforte Sonata ( Op . 10 , * no . by ) .. Beethoven 
 Andante " se . " . Warder Harvey 
 Andante con moto .. a ' 3 o « on oo Moe Gaus ; 3 introductory ay se ee S. J. Rowton 
 Christmas Bells ... be as é oo oo G. J. Bivey| 4 arch .. . . * . Oliver O. Brooksbank 
 Minuet ae " be ' Philip Hayes | 5 : Sunday song --Max Oesten 
 Judex , from " Mors et Vita " sy tae * ghee ee ounod| 6 . Minuet and Trio , from n Quartet ( Op . 9 , no . ) .. Haydn 
 Soft Voluntary as é ite as H. A. Harding | 7 Pastorale wa ' " a ia clAlfred Ww . Youlen 
 , slow March .. ne ae ue ae Be Cunningham Woods | 8 Religioso . see + . T.L. Southgate 
 ASongof Praise .. .. « 2 of oe J. Stainer | book 8 
 , Andante in G minor « e ee 3 ] ELH : Fellowes | , 
 | 1 . elegy + » « CC . H. Lloyd 
 | 2 po from no . 6 ( Piéces de Clavecin ) . Couperin 
 book 2 | > aa . " ee ee ee oe Frederick A. i 
 - ae « ee eo ee s Tozer 
 ; ow oe ee we - F. Cota | | 5 . Allegretto Pastorale .. W. John Reynolds 
 a ee oo a ee " w. Warde ; mene | | 6 . ee in F , from String Quartet i in D1 iapaenl ™ Sane er 
 we a aa 7 . meditation . e . Wolstenholme 
 Foaleding Voluntary " s oe ee A. R. Gaul | 8 finale , from Pianoforte Trio ( Op . ' 88 ) oe ee - . Schumann 
 ee Voluntary 22 62 we we eae : 2 teen | @ Camis aa 4c + » « + « + Battison Haynes 
 . Tempo di Minuetto . oe yles B. Foster 
 po sad con moto , from Quartet i in D minor . a | BOOK 9 . 
 agio " es 7 William Sterndale Bennett | . Larghetto a ‘ ie ry .. F. Cunningh 
 ; naalanery Voluntary a oe au « - Daniel McIntyre | ct at el March .. pa ee ayy hey 
 | 3 . = man , all thing ( " Lobgesang " * ) ae ae eon 
 book 3 . | 4 . Allegro poco maestoso ee ee ee ee . G. Cusins 
 | 5 . communion .. oe oe ee ee ee _ Alfred R. Gaul 
 . Andante Tranquillo .. 4d ae 3 us C. H. Lloyd| 6 . Andantecon moto .. « - + . + » John Francis Barnett 
 | the Village March . I Ferris Tozer | 7 : Andante Religioso .. .. « + « « Alfred W. Tomlyn 
 . romance , from Serenade for Strings ( . 0 f .. Mozart | 8 . evensong + + « « Cuthbert Harris 
 Gavotte , from " Semele " ... 3 Handel | 9- minuet , from quartet in Gminor 2 ... ) Schubert 
 . an evening Prayer . * " alfred W. Tomlyn | 10 . melody i ina .. oe ef ef ce eo W.H. Callcott 
 . Heaven and the earth display ( Athalie Py ee Mendelssohn BOOK 10 . 
 1 . Allegro moderato .. ae ee ee we .. E. Bunnett 
 BOOK 4 . 2 . Opening voluntary . oe Ferris Tozer 
 . meditation .. ve we oe ia - . Battison Haynes 3 . while my watch Tam keep , ( ' Choral from he 
 . allegro moderato .. W. John Reynolds wee son Xi = oe Gounod 
 . Funeral March , from Pianoforte Quintet ( Op . 44 ) _ .. Schumann | 4 ° Slow Air , from Suite de Pieces es zp me l ed 
 . conclude Voluntary xe es ae . » Cuthbert Harris z Allegretto Dextarate = i he J. Campbell 
 sag od aaa Sey er ke Osis C. gyi 7 . Allegretto Grazioso , from the last Movement of ath 
 » solemn Marc ne oo eo " oe ee oy Pianoforte Sonata Mozart 
 8 . Hallelujah Chorus , from " ' the Messiah " < .   ° : Handel 
 book 5 
 book 11 . 
 Agnus Dei ... + » « + f , Cunningham Woods | 1 , Pastorale .- _ Battison Haynes 
 Minuet , from 9th Pianoforte Sonata .. ..   « e Mozart | 2 . Gavotte , from the 12th sonata for 2 violin and Cello Boyce 
 . Jerusalem Ceelestis ( ' Mors et Vita " ) .. ee ee Gounod | 3 , Evensong Kate Boundy 
 Andante Grazioso .. « swe we . Kate Boundy | 4 , Minuet , from the Organ Concerto in B fat ( no . % 
 . — _ con brio oe ee ee ee " Frederick A. ee and Set ) a ae pe se : Handel 
 ommunion .. a ° u — olme } « , ‘ aia aa " Oli 
 . Allegro , from Finale to gth Pianoforte Trio ee aydn ; mg ( op . 72 no . 2 ): ys iver a 
 conclude Voluntar ee ° ' Cuthbert Hans E 2 
 g y + oe 7 . Communion .. + " uw . ' Wetuahelon 
 8 . prelude in e minor and chorale .. ee ° « - J.S. Bach 
 book 6 . g. Andanteconmoto .. .. « « G. A. Macfarren 
 » OSalutaris Hostia .. - » Myles B. Foster book 12 . 
 » slow March , from the 4th sonata . ee os oe Boyce| 1 . Berceuse ( op . 77 , no.3 ) .. ee - Alexandre Guilmant 
 » o great be the depth , from " St. Paul " . oo Mendelssohn 2 . Introductory Voluntary .. « ' aa - » Hamilton Clarke 
 . penowel ema oe oe ee ee » . J. Warriner } 3 . ae ad ar ee oe ee ee c Bruce Steane 
 » Largo , from ' ' Xerxes " P Handel | 4 . elody ee oe ee ee ee * S. Colerid e - Taylor 
 forsake I not , Duet from " the last judgment " . " Spohr } 5 . eventide ee ee ee ee eo ee Clowes Bayley 
 , Allegro moderato ee p os os Ww . Warder pr at 6 . postlude ee ae a " " e Josiah Booth 
 inuet oe ee oe ee ee oe Samuel Ould 7 . Jubilant March . ee ee ve W. John Reynolds 
 to be continue , 
 Lonpon : NOVELLO and COMPANY , Limitep 

 572 the MUSICAL TIMES.—Avcust 1 , 1898 

