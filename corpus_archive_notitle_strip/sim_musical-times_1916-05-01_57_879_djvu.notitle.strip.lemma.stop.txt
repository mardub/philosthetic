middle year 1851 , Mr. Ingram , 
 proprietor ///ustrated London News , 
 conceive idea issue series illustrate 
 supplement newspaper subject 
 ' recommend especially lady 
 family . ' suggestion George Hogarth , 
 act musical critic 
 Iilustrated London News , Mr. Ingram determine 
 supplement form 
 series national english melody , 
 number , word write edit 

 QD : Charles Mackay , music arrange 
 Sir Henry Bishop , illustration Gilbert , 
 Birket Foster , Kenny Meadows , William Harvey , 
 . number appear December , 
 1851 , series short time , 
 eventually discontinue complete . 
 Dr. Mackay , ' year ' recollection life , 
 literature , public affair , 1830 1870 ' ( 1877 ) , 
 acknowledge success , variety 
 reason : awkward size paper , 
 simplicity accompaniment , & c. ; 
 hit probably important 
 reason , viz . , word music ( 
 particular ) severely edit alter 
 fail appeal public begin 
 appreciate national music genuine primitive 
 state . Bishop , 1851 old man , enter 
 enthusiastically Mr. Ingram scheme ; 
 life spend alter , adapt , 
 tinker music bring 
 level consider taste public . 
 sacred , instead develop 
 real talent , labour incessantly 
 disfigure masterpiece composer 
 Beethoven , Mozart , host man , 
 addition alteration . spirit 
 approach task propose Mr. Ingram 
 display follow letter Dr. Mackay , 
 mass correspondence subject 
 preserve British Museum 

 Cambridge Street , 
 Hyde Park , 
 June 23d , 1851 . 
 Dear Sir , 
 intend pleasure 
 Mr. Ingram day ; , find 
 office ( Strand ) hope basis 
 arrangement commence oz 
 labour delay 

 QUEEN HALL ORCHESTRA 

 March 25 M. Sapelinikov play Liszt Pianoforte 
 Concertos ( e flat ) , remarkable feat concert . 
 Elgar Introduction Allegro string finely play . 
 Tchaikovsky fourth Symphony Moazart ' magic flute ' 
 overture item . Sir Henry Wood conduct . 
 final concert series , April 8 , 
 novelty . ' Pathetic , ' ' Brandenburg ' 
 Concerto item . Mr. Arthur Rubinstein 
 appear , detain Spain . Mr. William 
 Murdoch , surely artistic pianist 
 midst , play solo Beethoven E flat Concerto . 
 Sir Henry Wood conduct occasion . 
 orchestra 115 concert 
 month 

 LONDON SYMPHONY ORCHESTRA 

 XUM 

 final concert season London 
 String Quartet March 24 , Sir Charles Stanford 
 attractive Quartet D minor , Beethoven Op . 18 , . 2 , 
 G , Brahms Pianoforte Quintet , perform . 
 able party , lead our- fine 
 violinist , Mr. Sammons , earn gratitude 
 Se fine performance support 

 ritish composer 

 MUSICAL TIMES.—May 1 , 1916 

 choose , comprise orchestra Beethoven 
 * Egmont ’ Overture , Glazounov ' Carnival ' Overture , 
 Scherzo Tchaikovsky Symphony . 4 , 
 movement ( ) Rimsky - Korsakov Symphonic 
 Suite ' Antar , ' Prelude Act 3 ' Lohengrin 

 Miss Jane Strachan , local amateur endow 
 charming voice style , sing Bishop ' Lo ! gentle 
 lark ' ( flute obbligato Mr. Vincent Needham ) , 
 song S. Liddle . Mr. Webster Millar sing song 
 Bizet Roger Quilter . gifted young violinist , 
 Mr. Horace Ayckbourne , play portion Mendelssohn 
 e minor Concerto , accompany orchestra 

 Hallé Orchestra Concert jn 
 Town Hall March 22 , conduct Sir Thomas 
 Beecham . programme include César Franck ’ ; 
 symphonic - poem , ' Le Chasseur Maudit ' ; Wagner . 
 ture voluptuous new ' Venusberg ' music tp 
 ' Tannhiuser ' ; Delius ' Sea - drift ' poem baritone solo 
 chorus , orchestra ; Stravinsky Suite Ballet , 
 * L’Oiseau de Feu ' ; Fenney Tone - poem ' dawn , ' Op . 16 : 
 Chabrier rhapsody , ' Espana . ' trying 
 baritone solo ' Sea - drift ' Mr. Powel 
 Edwards , choir , specially train occasion 
 Mr. Appleby Matthews , sing efficiently . f 

 Birmingham Chamber Concerts Society concert 
 March 29 , executive 
 Catterall String Quartet , superb reading 
 Mozart Quartet D minor , Beethoven Quartet iu } 
 C minor , Op . 131 . Mr. J. C. Hock play ability 
 Bach unaccompanied Violoncello Suite . 5 , C minor 

 Birmingham Festival Choral Society concert 
 current series Town Hall March 3p , 
 Dr. Sinclair . principal feature revival 
 Dr. Walford Davies * everyman , ' hear Leeds 
 Festival 1904 , year follow Birmingham { 
 time . Dr. Sinclair compliment fine 
 performance . solo work Miss Mabel Manson , 
 Miss Phyllis Lett , Mr. F. J. Webster , Mr. Thorpe Bates 
 add distinction performance . Cantata 
 follow Wesley - Motet , ' Exitu Israel , ' 
 Sir Edward Elgar ' Carillon , ' poem recite 
 Miss Phyllis Lett . organist Mr. C. W. Perkins 

 TORQUAY 

 Torquay Municipal Pavilion scene 
 interesting event . Melsa play Tchaikovsky Concerto 
 violin Municipal Orchestra March 18 , 
 Mozart Concerto D admirably play 
 March 23 , Miss Daisy Kennedy soloist . 
 latt occasion Orchestra perform Symphony . 1 
 Beethoven , Svendsen episode , ' Carnéval de Paris . ' 
 March 30 Symphony play Brahms , 
 F , Russian ’ cellist , M. Fedor Otscharkov , play 
 Saint - Saéns Concerto band . César Franck 
 Symphony D minor April 5 , 
 Mr. Edgar Heap collaborate Orchestra 
 Schumann Pianoforte Concerto . April 10 , 
 management try experiment free concert 
 public admitie charge . 
 Orchestra play popular number , 
 entertainer contribute ; fresh face 
 audience , Pavilion fill 

 PLYMOUTH , ETC 

 conduct Mr. Albert Orton , Walton Philharmonic 
 Society sing Mendelssohn ' St. Paul ' March 23 . 
 vocal principal Miss Bessie Lang , Madame Annie 
 Beattie , Mr. Lloyd Moore , Mr. Charles Leeds . 
 small orchestra lead Mr. J. E. Matthews , Dr. 
 Stanley Dale pianoforte . Mr. Orton choir 
 Walton Parish Church sing choral evensong H.M. Prison 
 Walton April 9 

 fourth close chamber concert Rodewald 
 Club March 20 , Brodsky Quartet play Haydn 
 Quartet D , Op . 64 , . 5 , Tchaikovsky Quartet F , 
 Op . 22 , Beethoven Quartet b flat , op . 18 , . 6 
 annual meeting Club , hold April 13 , Dr. 
 Pollitt hon . -treasurer announce balance hand , 

 Mr. 

 monthly meeting Northern Section 
 I.S.M. , Mr. Whittaker read paper Bach motet , 
 follow illustration Bach Choir : ' ah ! God 
 mercy , ' - motet ' Spirit helpeth ' 
 ' come , Jesu , come 

 March 21 London String Quartet concert 
 Darlington Chamber Music Society Polam Hall , 
 excellent performance Beethoven C minor 
 Quartet , Op . 18 , . 4 , McEwen ' Bagatelles , ' 
 Schumann quartet minor . March 22 visit 
 Bishop Auckland , play Beethoven E minor Quartet , 
 short english piece , movement Tchaikovsky 
 Quartet D. March 23 appear chamber 
 concert Middlesbrough Music Union , play quartet 
 Beethoven ( e minor ) , Ravel ( f ) . 
 concert , - song sing choir , conduct 
 Dr. Kilburn 

 NOTTINGHAM DISTRICT 

 correspondent particularly request enclose programme 
 forward report concert 

 DuMFRIES.—The select orchestra town 
 enterprising organization . set good example 
 Scotch town . concert year , 
 programme include ' unfinished ' 
 Symphony , ' Casse Noisette ' Suite , ' Land mountain 
 flood , ' ' Hebrides ' overture , ' Jena ' Symphony 
 ( Beethoven ) , concerto D minor ( Bach ) , ' Menuet d’Orfée ' 
 ( Gluck ) , ' Peer Gynt ' Suite , . 1 . Mr. W. J. Stark 

 conductor 

 PERTH ( WESTERN AUSTRALIA).—Mr . Sydney F. Pick , 
 principal baritone Cathedral Church , Perth ( late 
 Glasgow Cathedral ) , gain diploma Licentiate 
 Associated Board , Royal Academy Music Royal 
 College Music , London , male vocalist 
 obtain distinction Western Australia 

 SouTHPORT.—The Orchestral Society conduct Mr. 
 William Rimmer banner fly . orchestra 
 performer good programme March 31 , 
 Mr. Arthur Catterall play Beethoven Violin 
 Concerto 

 West TARRING ( near Worthing ) . — ' Woman 
 Samaria ' ' Rebekah ' perform orchestra 
 Parish Church April 5 , Mr. W. Birstead 

 Almight 
 Almight 

 MUSICAL TIMES.—May 1 , 1916 . 259 
 
 ANTH EMS ASCENSIONTIDE . 
 * praise majesty . oe Mendelssohn 14d . | letnotyour heart troubled(DoubleChorusunac . ) M.B. Foster 3d . 
 achieve — work . Haydn 1d . let ( - arrangement , organ ) Myles B. Foster 3d . 
 ) , cachioved | glorious work ( end Chorus ) . Haydn ic : let celestial concert unite .. Handel 14d . 
 RK , " al Jory Lamb se . " Spohr 14d . | * lift head os Handel 4 L. Hopkins , 14d . 
 oe e , glory ee oe M. Wise 3d . | * lift head ee . Ss . ars " Taylor 3d . 
 Valse Crist dbedient unto death .. es J. F. Bridge 1}$d.| lift head 7 - oe Turner 2d , 
 Christ enter aed aes _ Eaton Faning 14d . | * look , yesaints .. - » Myles B. Foster 3d . 
 nts ( 4d , come , ye child oe .. Henry John King 3d . o ye people , clap hand v oe H , Purcell 3d . 
 ) ee - Oliver King 14d . | * o clap hand ws oe J. Stainer 6d , 
  ( 2s . ) , Godisgoneup .- e * * Croft , 4d . ; W. B. Gilbert ad . o clap hand ve oe oe T. T. Trimnell 3d . 
 " ouf - * God , king ... oe ss - Bach * o God , King glory " ee ae H. Smart 4d . 
 Grant , beseech Thee . ae os H. Lahee 14d . | * o God , thou copenen ee os Mozart 3d , 
 Grant , beseech thee ( collect ) ina A. R. Gaul 3d . | * ohowamiable .. ~~ " o - Barnby 3d . 
 2s , * hallelujah unto God Almighty Son . es Beethoven 3d . * o Lord Governour .. oe = oe . Gadsby 3d . 
 * excellent thy , o Lord .. ° Handel 14d . o Lord Governour .. ad es os Marcello 1d . 
 , ' ye rise Christ . Ivor Atkins 4d . | * o rise Lord ae ea és se J. Barnby 14d . 
 ye rise r . Osmond Carr J. Naylor , ea . 3d . | * open tome gate ... ae F , Adlam 4d . 
 ye rise ( par ts ) .. Myles B. Foster 3d . * rejoice Lord = én ai os J. B. Calkin 1}d . 
 rth Father house H. Elliot Button nand J. Maude Crament , ea . 3d . | * sing unto God os os _ _ F. Bevan 3d . 
 10 centg , wt day . ow Elv ey 4d . | * thousand time _ thousand os ee E. Vine Hall 3d . 
 day ( open ye ' gate ) ae os C. Maker 3d . earth Lord .. ee - ‘ ie T. T. Trimnell 4d , 
 " shall come pass ee es B. Tours 14d . | * Lord exalt ee ee John E. West - 
 = = | leave comfortless _ oe oe W. Byrd 3d . Lord King ~~ ae . Gadsby , 6d . ; H. J. King 4d . 
 repair , ' King - glorious .. J. Barnby 6d . | thou art priest . es S. Wesley 3d . 
 os , Fine King - glorious ( chorus arr , voice ) J. Barnby 4d . * unfold , ye portal es - oe Ch . Gounod 3d , 
 o Raed * leave , forsake J. Stainer   @ * thou reignest es oe Schubert 3d , 
 - let ' heart - . Eaton Faning G. Gardner , } weak helpless ve Rayner ad . 
 ISTEL anthem W HITSUNTIDE . 
 W , le saw .. e ee J. Stainer Father house .. - . J. Maude Crament 3d . 
 — — — * suddenly come ° ee - » Henry J. Wood m shall come pass ... - oe oe G. Garrett 6d . 
 » — Pentecost os oe C. W. Smith 3d . * shall come pass ae ee - _ ee B. Tours 14d . 
 \les * pant hart . , ee oe Spohr = let God arise ... se es os oe Greene 6d , 
 * hart pant .. os + Mendelssohn 14d . let God arise ws se T. T. Trimnell 4d . 
 behold , send promise - ; Varley Roberts 4d . " let heart trouble . oe -- H.G. Trembath 14d . 
 * come , Holy Ghost ° es T. Attwood 14d . look , Holy Dove . - oe B. Luard - Selby 3d . 
 come , Holy Ghost . . E Ivey J. L. Hatton , 4d .   * o clap hand ee - J. Stainer 6d . 
 come , Holy Ghost { ¢ Lee Williams Palestrina , 2d . * o thank " - om G. Elvey 3d . 
 concert . come , thou Holy Spirit : - : J. F. Barnett 3d . .*O Holy Ghost , mind .. es .. G , A. Macfarren " ig 
 ticket . fill heaven earth . ' - Hugh Blair 3d . * oh ! fora close walk God os - . Myles B , Foster rd . 
 * eye hath ( - setting ) .. .. Myles B. Foster 3d . o taste .. * goss ; A. H. Mann , 3d . 
 * Eye hath ( four- a. — 2 .. Myles B. Foster 3d . " * Otasteandsee . es - _ Sullivan 14d . 
 aa fear thou os ° - Josiah Booth 14d . o thou , true light ee oo — — ~- 2d . 
 thank unto God .. + asi Spohr 4d . o shall wisdom find _ os Bo 6d . 
 glorious powerful God os - .. Orlando Gibbons 3d . * bl Redeemer . ee es os V. Hall 3d . 
 ' God come Teman .. nis - - C. Steggall 4d .   * praise 1 ord daily es - ee J. * , ' Callin 14d . 
 = ' Godisa Spirit .. " . " s = W.S. Bennett 1$d . sing tothe Lord .. - os Smart 1s . 
 4 ' great Lord . os es W. Hayes 4d . " spirit mercy , truth , love . ee ee B. Luard - Selby 14d . 
 - ' grieve Holy Spirit - os ee J. Stainer 3d . " spirit mercy , truth , love -- H.A. Chambers 14d . 
 hail ! breath life ' ~ .- Thomas Adams 14d . eye wait thee ee es Gibbons 4d . 
 happy man . os os E. Prout 8d . * glory God Israel os oe T. Adams 3d , 
 dwelleth secret place ee Josiah Booth 4d .   * Lord come Sinai oe _ oe John E , West 3d . 
 " Holy Spirit , come , o come ( ad eeeg Sanctum ) G. C , Martin 14d . Lord descend R oe ee Hayes 14d . 
 spirit . . ae Blow 6d . Lord Holy Temple ue oe , Stainer 4d . 
 nm ' magnify thee es os . J. H. Parry 3d . Lord Holy Temple ae se H. Thorne 14d . 
 _ * leave comfortless - os - Bruce Steane 2d . love God shed abroad ° S. Reay 14d . 
 ™ ' pray Father .. o - .. G.W. Torrance 14d . med oe - H. S. Irons 3d . 
 ifi away .. os es os Thomas Adams 14d . Spirit God ra - Arthur W. Marchant 3d . 
 ifi gonot away .. = es " e A. J. : " aldicott 3d . * wilderness . . " John Goss , 2d . ; * S. S. Wesley 6d . 
 ye love Heap 1}d . * God old come Heaven .. E. V. Hall 3d . 
 ' ye love " W. H. " Monk , Tallis , R. P. mt , ak isd . weill rejoice .. - ee éa Croft 4d . 
 ye love Bruce Steane ad . Day Pentecost ee oe e A. Kempton 3d . 
 ifye love Herbert W. W areing ' W. J. Westbrook , 3d . whosoever drinketh oe oe J.T. Field rd . 
 ' almighty everlasting God . - _ os Gibbons d. * Jewry God know .. .. J. Clarke - Whitfeld 14d . 
 — Almighty God , hast promise - .. H. Elliot Button ie sweet consent .. es ee os oe E. H. Thorne 3d . 
 . " angel spirit , bless - Tchaikovsky 2d . fear Lord .. ~ oe .. J. Varley Roberts 3d . 
 ) ascribe unto Lord .. ae - om S.S. Wesley 4d . let peace God rs - _ _ os J. Stainer 4d . 
 ee " behold , God great - " E. W. Naylor 4d . " let thy mercifulear .. oo ar A. R. Gaul 14d . 
 nt . belove , God love . hee " 4 J. Barnby 14d . * light world wr es E. Elgar 3d . 
 beloved , let love .. Gerard F. Cobb 14d . Lord power ov wn E. T. Chipp 3d . 
 ye ofone mind .. .. Arthur E. Godfrey 3d .   * Lord power .. " William Mason 14d . 
 " Blessed man os " oe John Goss 4d . Lord poe ( man voice ) . J. Barnby ad . 
 blessing glory ee ae = ' Boyce 14d . * Lord , pray thee ae os $ s oe es Jarley Roberts 14d . 
 " blessing , glory .. = iz ie Bach 6d . o Father Biest ae de - j. Barnby 3d . 
 come , ye child - oe - ve Josiah Booth 3d . o God , hast prepare _ oe se A. R. Gaul 2d . 
 = " God come Teman .. os " s C. Steggall 4d . joyful light .. ae oe B. Tours 4d . 
 j pod love world .. os : Matthew Kingston 14d . * o Lord , trust . ee King Hall 14d . 
 vrant , o Lord .. os r Mozart 14d . o taste ' ... - iJ. Goss A. H. olen 3d . 
 grant ' , Lord . " 6 . Elliot Button 1d . * o taste os A. Selives 14d . 
 Hail , gladdening Light es Ss z. Field , " ad . OG . C. Martin 4d . o shall wisdom find ? " le 
 ' holy , oly , holy .. Crotch 3d . ponder word , o Lord ' - .. Arnold D. Calley 14d , 
 holy , Lord God Almighty os - x T. Bateson 4d .   * praise awful .. ae ee os Spohr 2d . 
 ow goodly thy tent aa wm F. Ouseley 14d . rejoice Lord ' " - - G. C. Martin 6d . 
 " lovely thy dwelling . ais Spohr 14d . * love hath Father + 7 - Mendelssohn 14d . 
 — — ' hymn tothe Trinity .. oe - os Tchaikovsky 14d . sing tothe Lord .. ae se ee Mendelssohn 8d . 
 lam Alpha Omega .. - ia Ch . Gounod 3d . * stand bless - Goss 4d . 
 ' lam Alpha Omega = J. Stainer 14d . teach thy way " W.H. Gladstone , Hd . Frank L. Moir 3d . 
 lam Alpha Omega . oe " sit .. J. Varley Roberts 3d . * Lord hath mindful .. S. Wesley 3d . 
 /1oms behold , lo ! .. ee se _ Blow 6d .   * Lord Shepherd - fn .. G. A. Macfarren 14d . , 
 ’ know Lord great - oe aie F. Ouseley 1}d . * Lord Shepherd ee oe oe J. Shaw 3d . 
 reach Isawthe Lord .. . Cuthbert Harris , 3d . ; * J. Stainer 6d . Lord comfort Zion ... en ae H. Hiles 6d . 
 magnify ' - ie J. Shaw 3d . thou art worthy , o Lord .. F. E. Gladstone 3d . 
 W. sing Thy power . , ae se Greene 4d . thou shalt shew path life * . oe Alan Gray . 14d . 
 " sing Thy power . . _ = si A. Sullivan 14d . humbly beseech thee ° se .. H. Elliot Button 1d . 
 sing unto Lord " s _ H. W. — 3d . whatsoever bear God os es oe H. Oakeley 3d . 
 ' humble faith .. mn ei ; G. Garrett 14d . ' whocancomprehend thee .. os ee Mozart 3d . 
 ) ° f \ authen mark ( * ) Tonic sol-/a , 1d . 2d . , 
 Lonpon : NOVELLO COMPANY , LimiTep 

 musical 

