Monday , 20th April , 1874 , 
 SIR Jj . BENEDICT " ST . PETER 

 Goa Best SOCIETY , Beethoven Rooms , 27 , 
 Harley - street , W. President , Sir Julius Benedict . director , 
 Herr ScHuBerTH . eighth season , 1874 . second concert 
 place WEDNESDAY , 29th ApRIL , 1874 . 
 Programme devote R. Schumann Vocal Instrumental Com- 
 position . Society afford excellent opportunity young 
 rise artist appearance public 

 LOUCESTER COUNTY ASYLUM , near 
 Gloucester.—_MALE attendant want . wage 
 commence { 24 annum , Board , Lodging , Washing . 
 ( musician prefer . ) previous Asylum experience necessary . 
 application , state age , musical capability , send 
 superintendent Asylum 

 CRYSTAL PALACE 

 Tue Symphony , Mr. Ebenezer Prout , 
 time , 28th February , achieve success , 
 prove audience ready appreciate 
 acknowledge worth , find ; 
 credit Mr. Manns , love music 
 countryman know , afford hear- 
 ing awork suchimportance byan Englishman . 
 carefully plan composition Mr. Prout Sym- 
 phony unjust , single hearing , 
 impress conviction 
 production profound thinker art , 
 movement define plan ; subject melodious , 
 skilfully treat ; instrumentation 
 intimate knowledge orchestral effect . début Miss 
 Emma Barnett , play Beethoven pianoforte con- 
 certo G , marked feature concert , 
 performance remarkable intelligence , 
 fluency execution , rhythmical accuracy . 
 cadence , write brother , Mr. J. F. Barnett , 
 excellently play , elicit - deserve burst 
 approbation . follow concert Beethoven fine 
 music ' ' Egmont " , receive en- 
 thusiasm . Brahms variation , orchestra , 
 theme Haydn , perform , remark- 
 able work , originality variety composition , 
 masterly instrumentation variation , keep- 
 e attention hearer alive note . 
 loudly applaud , , trust , frequently 
 place excellent concert . Schubert Otett , 
 play , r4th ult . , stringed 
 instrument orchestra , wind instrument 
 double , prefer speak , 
 occasion freely express opinion desecra- 
 tion fine work . Mr. Sullivan overture " ' 
 Sapphire Necklace ' open concert , 21st ult . , 
 spirit ; hackneyed ' " Qui la voce , " 
 sing Madame Sinico , come Mendelssohn ever- 
 welcome violin concerto , play Herr 
 Joachim play . absolute novelty 
 programme " song Destiny " ( Schicksalslied ) , 
 chorus orchestra , Brahms , share fate 
 novelty , indifferently perform , 
 movement demand , chorus , 
 refinement , hardly receive hand 
 Crystal Palace choir . enter analysis 

 448 MUSICAL TIMES.—Aprit 1 , 1874 

 merit , mention recognise 
 hearer work genius high order , 
 opinion cordially agree . verdict 
 nearly musical Germany 
 composition occasion unanimously endorse 
 critical audience assemble 
 Sydenham ; Herr Brahms reckon 
 cordial reception accord future work 
 pen . english translation text 
 Rev. J. Troutbeck , M.A. Beethoven Symphony ( . 4 , 
 B flat ) receive rendering hardly look 
 outside Crystal Palace — Adagio narrowly escap- 
 e encore , , extreme length 
 concert , inevitable . item 
 interest great extent spoil , place 
 end long programme . english 
 people alive fact high - class music can- 
 properly listen fatigue 
 brain ; , great work follow close 
 concert , attention 
 time wearied ? experience sense 
 fatigue produce regulation overture , 
 Symphonies , Concerto , form ordinary 
 orchestral programme classical concert ? 
 , , good example 
 ' * Exhibition Concerts " year , Over- 
 ture , Symphony Concerto , ballet music , 
 March , vocal piece , form programme 
 satisfy satiate . return — orchestral 
 variation Brahms produce effect 
 performance concert , 
 reason far seek : come hour anda 
 quarter classical music break 

 MAJESTY OPERA 

 WAGNER SOCIETY 

 fifth concert , 13th ult . , contain 
 popular extract Wagner work 
 increase longing hear legitimate 
 place — operatic stage . chorus Messengers 
 Peace , early Opera " Rienzi , ' 
 wish - development composer 
 genius arrest , thoroughly legitimate 
 vocal effect , pure attractive melody . 
 overture " Die Meistersinger von Nurnberg , " 
 finely play receive enthusiasm ; choral 
 song ' * Wachet auf , " Opera , encore . 
 warm welcome accord specimen 
 " Lohengrin , " prayer finale act 
 elicit solid mark approval . Beethoven 
 " ' Choral Fantasia " , notwithstanding 
 pianoforte ably render Mr. Walter 
 Bache ; composer overture ' " King 
 Stephen , " , conceive , place 
 concert , receive ample justice . ina song Abbé 
 Liszt , Miss Sterling , create marked effect , 
 good expression Rubinstein ' ' Die Waldhexe , " ' 
 accompany pianoforte Mr. 
 Dannreuther , conduct concert usual 
 energy skill 

 BRITISH ORCHESTRAL SOCIETY 

 public opinion : ' ' far shalt thou , far . " 
 addition seventh string lyre arouse 
 opposition . similar obstructiveness attend pro- 
 gressive effort . strife yield fruit peace . 
 express recent review " letter " 
 Wagner , tacitly acknowledge 
 spirit Mr. Joseph Bennett carefully word contribu- 
 tion subject , number ably 
 edit Journal March . leading article temperately 
 approach pith matter , candidly 
 set forth new view . permit add ido think 
 single hit great musician . 
 — eloquently — rivet 
 mind continuously grow deepen 
 conviction Wagner right , opponent 
 understand . province re- 
 introduce invective . let allow simply 
 state view conclusion forego . 
 meas clear sunlight music grow . 
 utter oft repeat word -adverse party 
 irksome ; , help , briefly 
 state case occur mind , deem 
 Wagner opponent error 

 Beethoven truly . composer 
 dismiss " tell , " justly dismiss . 
 project separate ' ' man ' " ' 
 ' theory , ' , ' ' . ' ? music poetry 
 half arch , " yes . ' " ' programme maker know 
 fact , , find legend 
 attach instrumental piece , invent tack 
 . " Lion love , " concerto 
 Beethoven ; ' " ' shake evil spirit , " violin 
 solo . line poetry , legend , story , 
 . illustration flow ad infinitum . composer 
 work ina comatose state ? ' ' yes . " Beethoven know 
 ? ' " . ' ? Beethoven leave record struggle ? 
 , " yes , . " write choral 
 symphony ? ' straight jacket result . " 
 instrumental music incomplete ? ' certainly 

 allusion compel pass , 
 , pleasure , 
 expatiate head question considerable 
 length . write believe 
 subject paramount importance pursuit art 
 , 

 logical definition definition . 
 public , matter altogether 
 aside , afford practical proof . public 
 sit hour , enjoy note 
 Meyerbeer Operas , end hour instrumental 
 music tired — end hour wish 
 hear note music long 
 live 

 retort pander 
 people — true . musical audience appreciate 
 Mozart , Meyerbeer , Beethoven Operas , 
 fair criterion healthy state ear , 
 case musician 

 instrumental performer , professional amateur , 
 particularly player stringed instrument , 
 passionately fond instrument difficulty 
 surmount acquire ( , , let 
 charitably suppose , unconsciously ) , 
 intolerable bore circumstanced . 
 ( omit work great master , 
 excel hand ) think 
 refer origin continuance string 
 quartett quintett peculiarity . case 
 enjoyment share hearer ; , hear 
 execrable combination run , strum , trill , 
 Thalberg ' ' Home , Sweet Home , " hearer strike 
 labour require learn , startled 
 interested ' ' sleight hand " require . 
 case difficulty produce pleasure hearer 
 player ; way , example fugue D 
 minor ( . 4 ) J. S. Bach , violin ; everybody : 
 know instrument , 
 person , d priori , fugue 
 instrument impossibility . masterly way 
 principal ingenious device 
 stretto , pedal , & c. , constitute excellence 
 commend cultured musician end 
 time . Opera , , high excellence , 
 shall far far treat 
 thesis temporarily omit . admit ' ' instru- 
 mental music incomplete , need aid word 

 let Wagner contradict 
 . Opera , " ' Le Vaisseau Fantéme , " 
 overture ( instrumental course ) com- 
 plete development libretto . ( sec . 1 ) 
 description ship cast rock , ( sec . 2 ) rock 
 suffer strike , ( sec . 3 ) crew sign compact 
 evil presence captain , & c 

 melody , chord , rhythm idea 
 substantive print italic ? leave reader 
 discover ; help degree . , , 
 wilfully rush ' " ' Beethoven error , " with- 
 Beethoven excuse ignorance , " yearning " 
 truth 

 let far Wagner embody error 
 doctrine . music ( instrumental , presumably 
 context ) produce 
 effect quiet walk sunset wood far 

 461 

 , suffice lack regular 
 phrase style ( soul true music ) Mozart , 
 Gluck , Meyerbeer , & c. , italian school , 
 fervent energy Beethoven , ana bold harmony 
 Handel 

 , Sir , , & c. , 
 Joun W. Hinton , A.B. , Mus . B 

 ueen powerful rendering trying . 
 main excellence performance lie chorus singing . 
 good , time artistic . altogether , opera 
 department — grouping supernumerary , gipsy , 
 soldier , courtier — completely satisfactory 
 company 

 EpinBuRGH.—In connection visit Mr. Mapleson italian 
 Opera Troupe , excellent concert Edinburgh 
 Choral Union crowded house . , February 21st , com- 
 prise selection solo chorus Handel 
 work , second , operatic solo . prin- 
 cipal artist Mdlle . Titiéns , Mdme . Sinico , Mdme . Trebelli- 
 Bettini , Mdlle . Macvitz , Signori Bettini , Borella , Perkin , Agnesi . 
 Mr. F. H. Cowen preside pianoforte . second concert , 
 r4th ult . , Handel Messiah , complete orchestra , 
 Madlle . Titiens , Mdme . Trebelli - Bettini , Signor Fabrini Signor 
 Agnesi principal . chorus , number 250 voice , 
 sing great care intelligence ; Mr. Adam 
 Hamilton , conduct concert , deserve great credit forthe 
 high state efficiency bring . organ- 
 ist Mr. T. Hewlett , Mus . Bac . , Oxon , playing contribute 
 greatly success concert —— PRoFESSOR OAKELEY 
 Organ Recital Music Class Room , 12th ult . , 
 excellent programme perform alarge audience . Bach prelude 
 fugue e minor remarkably play astudent . pro- 
 fessor Oakeley song , ' ' ’ ti thou art fair , " encore , 
 play little canzonet , " Sempre pili t’amo . ”——THE 
 seventh annual concert University Musical Society 
 Wednesday evening , 18th ult . , Music Hall , large 
 audience . number chorister , 200 , attain 
 standard singing far year , equal 
 improvement observable orchestra . Professor Oakeley , 
 come forward assume conductor place , greet 
 demonstrative applause audience chorister . 
 concert , miscellaneous , open student ’ song , 
 " ' Condiscipuli Canamus , " compose Professor Oakeley 
 concert 1869 latin word Professor Maclagan . Mr. Galletly , 
 student , play , Mr. Carl Hamilton , Beethoven sonata F , 
 Op . 17 , pianoforte violoncello , originally write piano 
 horn . Beethoven Symphony commence second 
 programme ; promising young student - pianist , Mr. 
 Coates , play Schumann " Schlummerlied , " Schubert Im- 
 promptu flat great neatness , light touch , clear articula- 
 tion . concert , highly successful , end 
 " National Anthem 

 EvertTon.—The annual concert Choral Society 
 Tuesday evening , roth ult . ,in Schoolroom , fill 
 large fashionable audience . _ conductorship Mr. 
 Hamilton White , organist Parish Church , East Retford , 
 Society great progress , choral piece 
 general excellence . song duet contribute Miss Annie 
 Williamson , Miss Naylor , Misses Postlethwaite , Miss Roe , 
 assist Mr. Dimock Retford ; Mr. White delight audi- 
 ience play pianoforte solo 

 logical definition definition . 
 public , matter altogether 
 aside , afford practical proof . public 
 sit hour , enjoy note 
 Meyerbeer Operas , end ove hour instrumental 
 music tired — end hour wish 
 hear note music long 
 live 

 retort pander 
 people — true . musical audience appreciate 
 Mozart , Meyerbeer , Beethoven Operas , 
 fair criterion healthy state ear , 
 case musician 

 instrumental performer , professional amateur , 
 particularly piayer stringed instrument , 
 passionately fond instrument difficulty 
 surmount acquire ( , , let 
 charitably suppose , unconsciously ) , 
 intolerable bore circumstanced . 
 ( omit work great master , 
 excel hand ) think 
 refer origin continuance string 
 quartett quintett peculiarity . case 
 enjoyment share hearer ; , hear 
 execrable combination run , strum , trill , 
 Thalberg ' ' Home , Sweet Home , " hearer strike 
 labour require learn , startled 
 interested " sleight hand " require . 
 case difficulty produce pleasure hearer 
 player ; way , example fugue D 
 minor ( . 4 ) J. S. Bach , violin ; everybody . 
 know instrument , 
 person , d priori , fugue 
 instrument impossibility . masterly way 
 principal ingenious device 
 stretto , pedal , & c. , constitute excellence 
 commend cultured musician end 
 time . Opera , , high excellence , 
 shall far far treat 
 thesis temporarily omit . admit ' ' instru- 
 mental music incomplete , need aid word 

 let Wagner contradict 
 . Opera , " Le Vaisseau Fantoéme , " 
 overture ( instrumental course ) com- 
 plete development libretto . ( sec . 1 ) 
 description ship cast rock , ( sec . 2 ) rock 
 suffer strike , ( sec . 3 ) crew sign compact 
 evil presence captain , & c 

 melody , chord , rhythm idea 
 substantive print italic ? leave reader 
 discover ; help degree . , , 
 wilfully rush ' * Beethoven error , " with- 
 Beethoven excuse ignorance , " yearn " 
 truth 

 let far Wagner embody error 
 doctrine . music ( instrumental , presumably 
 context ) produce 
 effect quiet walk sunset wood far 

 461 

 ofthe , suffice lack regular 
 phrase style ( soul true music ) Mozart , 
 Gluck , Meyerbeer , & c. , italian school , 
 fervent energy Beethoven , bold harmony 
 Handel 

 , Sir , , & c. , 
 Joun W. Hinton , A.B. , Mus . B 

 greatly success concerts.——ProFESsOR OAKELEY 

 Organ Recital Music Class Room , 12th ult . , 
 excellent programme perform alarge audience . Bach prelude 
 fugue e minor remarkably play astudent . pro- 
 fessor Oakeley song , ' ' ’ ti thou art fair , " encore , 
 play little canzonet , " Sempre pid t’amo . ”"——THE 
 seventh annual concert University Musical Society 
 Wednesday evening , 18th ult . , Music Hall , large 
 audience . number chorister , 200 , attain 
 standard singing far year , equal 
 improvement observable orchestra . Professor Oakeley , 
 come forward assume conductor place , greet 
 demonstrative applause audience chorister . 
 concert , miscellaneous , open student ’ song , 
 ' ' Condiscipuli Canamus , " compose Professor Oakeley 
 concert 1869 latin word Professor Maclagan . Mr. Galletly , 
 student , play , Mr. Carl Hamilton , Beethoven sonata F , 
 Op . 17 , pianoforte violoncello , originally write piano 
 horn . Beethoven Symphony commence second 
 programme ; promising young student - pianist , Mr. 
 Coates , play Schumann " Schlummerlied , " Schubert Im- 
 promptu flat great neatness , light touch , clear articula- 
 tion . concert , highly successful , end 
 " National Anthem 

 EvertTon.—The annual concert Choral Society 
 Tuesday evening , roth ult . ,in Schoolroom , filled 
 large fashionable audience . _ Mr. 
 Hamilton White , organist Parish Church , East Retford , 
 Society great progress , choral piece 
 general excellence . song duet contribute Miss Annie 
 Williamson , Miss Naylor , Misses Postlethwaite , Miss Roe , 
 assist Mr. Dimock Retford ; Mr. White delight audi- 
 ience play pianoforte solo 

 programme miscellaneous , comprise 
 overture , excellently perform military band , 
 direction Mr. J. J. Murdoch ; song duet , Miss Joubert , 
 Rev. C. Darroch , Mr. Julius Carey ; pianoforte solo Mr. 
 Mrs. Brion . second devote Mr. Brion Cantata , 
 Marathon , solo admirably sustain Miss 
 M. G. Sheppard Messrs. G. Sheppard C. Korner . 
 chorus exceedingly render choir ; entire 
 performance successful . Cantata , score 
 military band ( string island ) , 
 accompany 

 LreaMINGTON.—Mr . Julian Adams successful Chamber 
 Concert Saturday , February 28th . Mendelssohn pianoforte 
 trio , d minor , Mr. Adams co - operation Herr 
 Otto Bernhardt ( violin ) Mr. Turner ( violoncello ) , render 
 great vigour marked emphasis . important work 
 class , Mayseder trio b flat , admirably play 
 - executant . vocal portion concert 
 sustain Miss Katharine Poyntz Mr. Henry Pyatt , 
 encore Rockstro song , ' " ' Reefer . " great attraction , 
 , solo performance Mr. Julian Adams ; bravura 
 style playing mechanism fully display fantasia 
 russian Airs , vociferously encore , respond 
 play musical sketch , composition , ' " ' Les Patineurs . " 
 sonata G , pianoforte violin ( Beethoven ) , 
 perform finished execution 

 Ler.—An amateur concert , aid local benevolent case , 
 27th February Belmont Park Rooms , 
 conductorship Mr. Charles J. Frost . programme consist 
 - song , trio , song . Mr. Frost new - song , ' ' wind , " 
 render Miss Maberley , Miss Austin , Mr. Law , Rev. John 
 Kempthorne ; Sir F. A. G. Ouseley charming little trio , " 
 sight unwise , " Misses Bumpus Miss Austin . 
 Mr. Frost playing ( memory ) Beethoven Sonata , Op . 10 , 
 . 1 , c minor , Schubert f minor impromptu , un- 
 qualified satisfaction . concert successful 

 LeicesTER.—The Leicester Musical Society concert 
 season 1873 - 4 , Temperance Hall Tuesday evening , 
 roth ult . , large audience , Mendelssohn Oratorio Elijah 
 perform . principal vocalis&$ Madame Thaddeus Wells , 
 Miss D’Alton , Mr. Henry Guy , Mr. Santley . Madame Wells 
 efficient Widow . Miss D’Alton 
 successful , rendering ' ' oh , rest Lord , " elicit 
 warm plaudit . Mr. Santley magnificent voice , sing 
 Elijah - know power vigour , de- 
 clamation recitative , exquisite interpretation air 
 characterise ' varied excellence 
 famous . Mr. Guy execute share solo 
 highly satisfactory manner . trio , 
 double quartet , principal vocalist ably assist 
 Misses . Deacon Clowes , Messrs. Adcock F. M. Ward . 
 chorus exceedingly execute Society . 
 performance band , especially strengthen 
 occasion , equally satisfactory , accompaniment 
 finished judicious . Mr. Nicholson officiate 
 conductor 

 YaRMouTH.—A concert Masonic Hall , Wednes- 
 day evening , 4th ult . , member Mr. Deane choral class . 
 consist Haite Cantata , Abraham Sacrifice , 
 work produce vicinity , deserve 
 know . music allot Abraham effec- 
 tively render Mr. C. Panchen ( Bass ) . Miss Botwright , Miss 
 E. Cole , Messrs. Bly Smith , acquit 
 creditably . omit mention instrumental piece , 
 " ascent mountain , " beautifully 
 band . second miscellaneous . instrumental 
 quartet Robert le Diable warmly encore . Miss Hulley 
 play violin , Mr. omg second violin , Mr. Deane violoncello , 
 Mr. A. Howard bass ; Mrs. Panchen accompany ; Mr. Deane 
 conduct 

 Yorx.—The series winter concert 
 Tuesday night , roth ult . , instrumental programme 
 erforme manner utmost pleasure present . 
 osart charming quartet D minor special feature 
 evening . Mr. Hallé play Beethoven E flat Sonata , Op . 31 , 
 usual facile style finished manner ; Madame Norman - Neruda 
 contribute solo violin . quintet Schumann , e flat , 
 end programme . Miss Ferrari vocalist 

 OrcAN APPOINTMENTS.—Mr . Geo . W. R. Hoare , St. James ’ 
 Church , Clapham Park.——Mr . George Kitchin , Holy Trinity , 
 Sydenham.——Mr . E. F. Seppings , St. James ’ , Littie Heath , Chad- 
 , Essex.——Mr . R. Peel , assistant organist St. Mary , Wigan , 
 Lancashire.——Mr . Frederick G. Cole , St. Mary , Staines . — — Mr. 
 E. J. Mummery ( assistant organist Christ Church ) , Castle Church , 
 Stafford.——Mr . R. Nottingham , organist choir master Winder- 
 mere Parish Church , choir master Choral Union , Winder- 
 mere.——Mr . E. J. Griffiths , organist choir master Parish 
 Church , Broadstairs , Kent . Mr. Frederick G. Edwards , Surrey 
 Chapel ( Rev. Newman Hall ) , Blackfriars Road , S.E-——-Mr . Thomas 
 Edward Trotter , St. Saviour , Brockley Road , Forest Hill.——Mr . 
 H. Walmsley Little , organist choirmaster Christ , Church , 
 Woburn Square , Bloomsbury 

