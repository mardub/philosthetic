Conductor 
 Vocalist 

 BEETHOVEN CONCERT . 
 TUESDAY , June 11 , 3 

 Mr. HENRY J. WOOD 

 MISS MARIAN JAY 

 A.R.A.M. ) . ( SOLO VIOLINIS 
 “ Miss Marian Jay Recitat.—That violin recital consist 
 | interesting series trashy - pieces 
 | proved night Miss Marian Jay , accomplished violinist , , 
 | possessing technical skill 
 | astonishing years ago , disdain include 
 | programme pieces real music . Mr. Bird , began 
 | excellent performance Beethoven early Sonata G , 
 | Op . 30 ( . 3 ) , played Lalo characteristic “ Symphonie 
 | Espagnole ” Tartini Sonata G minor , leaving 
 things Vieuxtemps ‘ ‘ Réverie ” Sarasate “ Bolero ” ’ 
 cared hear end concert . Beethoven 
 sonata showed intelligence musical instinct , Lalo fine 
 work given faultless brilliance ; tone excellent , 
 execution difficult passages wonderfully neat accurate . 
 | merely virtuoso , artist . ”—The Times , 14 , 1go1 . 
 | Concerts , & c. , address , 5 , Hollywood Road , South Kensington 

 NR 

 LONDON EDINBURGH CRITICS 

 fame new viclinist soon spread 
 walls Covent Garden Theatre . 
 November 26 , 1866 , appeared , 
 time , Monday Popular Concerts , 
 led Mendelssohn Ottet , played 
 Beethoven Romance F , took , 
 Charles Hallé Piatti , Mendelssohn 
 C minor Trio . late Prince Leopold ( . 
 wards Duke Albany ) present 
 performance . Concerning Wilhelmj 
 concert , Pall Mall Gazette ( 
 Mr. Davison influence regard musical 
 criticism ) , said : ‘ Romance ( F ) 
 Beethoven [ Wilhelmj ] proved con- 
 summate master simply “ expressive ” 
 Mendelssohn Ottet “ grandiose ” 
 chamber music . ’ Morning Post , 
 notice concert , approve 
 spelling new violinist . 
 ‘ Wilhelmi simple intelli- 
 gible English eyes , ’ recorded opinion 
 critic . Saturday following 
 appearance St. James Hall , Wilhelm ) 
 played Crystal Palace concert 
 December 1 , 1866 , acceptance , 
 solos occasion Lipinski Concert 
 Militaire Vieuxtemps ‘ Reverie . ’ Subse- 
 quent appearances ‘ Pops ’ confirmed 
 good impression initial performances 
 , fame extended Edinburgh , 
 worked feelings 
 critic Auld Reekie journals 
 described ‘ gentleman burst 
 ( ! ) English public lately , 
 took stand performers 
 age 

 performances Paris attended 
 enthusiasm . Pasdeloup 
 concerts populairs pour la musique classique , 
 given Cirque Napoléon , January 20 , 
 1867 , called critics ‘ le nouveau 
 Paganini , ’ Parisians 
 tones admiration , ‘ Inconnu hier , le voila 
 célébre aujourd’hui . ’ visited Italy 
 autumn Florence performances 
 classical music earned title 
 Protector Societa di Quartetto . 
 January , 1868 , accepted invitation 
 art - loving Grand Duchess Helena Pawlowna 
 Russia visit St. Petersburg . 
 Russian capital resided Palais Michel 
 Hector Berlioz , said : ‘ 
 heard violinist tone grand , 
 enchanting , noble August 
 Wilhelmj 

 TECHNICAL ATTAINMENTS 

 violinist Professor Wilhelmj excels 
 equally performance solos 
 quartets . execution later quartets 
 Beethoven — true test executant ' 
 musical intuitiveness — works 
 modern composers especially praiseworthy . 
 great interpreter Bach . 
 Wagner heard play Chaconne 
 time embraced tears 
 eyes said : ‘ dear Wilhelmj , 
 scarcely speak , feel deep 
 impression . 
 perfect artistic interpretation 
 heard . ’ music Paganini , 
 plays great finish , fine outlet 
 display technical attainments . , 
 eclectic tastes , Bach 

 unusually interesting . general impression { Paganini appeal strongly 

 tones , beautifully pure withal , extracts 
 instrument . long - drawn bow 
 portrayed caricature sketch Mr. 
 Charles Lyall , , artist kind 
 permission , reproduce . tone , 
 left - hand technique , Wilhelmj , 
 common consent , unrivalled . double- 
 stopping skill manifests ease | 
 plays thirds , sixths , octaves , | 
 tenths . , Ferdinand Laub 
 said : ‘ believe August Wilhelmj 
 play tune , wanted . ’ 
 peculiar treatment double - stopping 
 result entirely original thought - 
 system , day 
 world detailed form . surprising | 
 recipient orders 
 distinctions . numerous violins 
 chief treasure Guarnerius , ‘ finest 
 

 composer Professor Wilhelmj 
 written chiefly instrument 
 eminently associated . 
 include solos violin orchestral 
 pianoforte accompaniments , transcrip- 
 tions — specialty — 
 string quartets , & c. edited 
 classics violin , including Beethoven 
 Mendelssohn concertos . furnished 
 Beethoven concerto cadenza 
 Biilow declared ‘ best . ’ Compositions 
 orchestra songs prove Wilhelmj 
 narrowed interests field 
 productive activity 

 LETTERS WAGNER LISZT 

 Schubert 

 8th Symphony .. ce ws .. Beethoven . 
 7 . Ballet Airs ‘ Faust ’ .. ee Gounod . 
 8 . Peer Gynt Suite “ f oe .. Grieg . 
 g. Grand Operatic Selection , ‘ Reminiscences 

 Rossini ' j aa F. Godfrey . 
 10 . Overtura di Ballo .. Sullivan 

 London . , order 
 named : — 
 1 . Miss — — played violin solos Tscharknosky 

 Wienramski . 
 Symphony , ‘ Cellinor ’ ( Beethoven ) . 
 3 . test pieces taken Sullivan 
 Wagner ‘ Puritani 

 nr 

 MUSICAL TIMES.—June 1 , rgor 

 opportunity calling attention 
 excellent work Mr. Dan Godfrey , Jun , 
 music Bournemouth . appears 
 sixth series symphony concerts concluded , 
 223 different works given , 
 including fourteen performances England , 
 eighty - pieces time popular 
 watering - place . , - works British 
 composers played , having 
 conducted composers . ‘ 
 fact symphonies Beethoven , Mendels- 
 sohn , Schumann , Dvorak , Brahms , Tschaikowsky 
 included , highest degree gratifying . 
 enthusiastic Bournemouth conductor 
 examination ungrudgingly award 
 marks — 

 PurcELL ‘ Fairy Queen ’ produced 
 Queen ( Dorset Garden ) Theatre 1692 , 
 repeated following year additions . 
 1701 , years Purcell death , advertise- 
 ment appeared London Gazette ( October 9 - 13 ) 
 offering reward guineas ‘ score , 
 copy thereof . ’ advertisement repeated 
 times , vain . ‘ Fairy 
 Queen ’ songs published life - time 
 composer , instrumental music 
 ‘ Ayres Theatre , ’ published widow , 
 fair , certainly idea 
 original work . complete score , 
 years , found library 
 Royal Academy Music , evidently 
 copy performance performances 
 work 1693 . special interest attaches 
 score music handwriting 
 Purcell . Atheneum 25 
 letter Mr. W. Barclay Squire , 
 British Museum , attesting fact . ‘ Ihat paper 
 states volume contains names 
 R. J. S. Stevens , - known glee composer 
 Gresham Frofessor , William Savage . Stevens 
 pupil Savage , Dr. Pepusch . 
 , , score came way 
 possession Handel predecessor ' 
 Cannons 

 financial aspect festival took 
 place Queen Hall week beginning 
 Monday , April 29 , , 
 presume solid considerations induce Mr. 
 Robert Newman continue festivals year year . 
 regards artistic question , 
 inclined doubt necessity orchestral festival 
 Metropolis , found answer hosts 
 provincial musicians present , smaller 
 towns , largest centres population 
 London . hear week round concerts , given 
 highly disciplined orchestra , 
 famous living conductors , makes worth 
 country organist repair London , brush cobwebs 
 provincialism , freshen ideals , country 
 town routine apt obscure 

 leading characteristic year festival 
 undoubtedly appearance eminent conductors — 
 , precise , eminent conductors 
 famous musician . Dr. Saint - Saéns , things , 
 - round musician , suppose care 
 pose virtuoso baton . concert con- 
 ducted consisted , single exception , 
 works , best performance 
 symphonic poem , ‘ La Jeunesse d’Hercule , ’ 
 conducted Ysaye . great Belgian violinist appeared , 
 , particular advantage familiar 
 conductor second concert series . 
 strong , nervous beat , fine sense breadth phrasing , 
 complete command resources . M. Colonne , 
 appeared opening concert , vivacious , 
 poetic . brings detail utmost 
 clearness , polish smartness 
 praise performances . , 
 breadth tenderness feels Ysaye . 
 finest orchestral performances festival , , 
 conductorship Herr Weingartner , 
 gave superb reading C minor Symphony 
 Beethoven . combined warmth , dramatic force , 
 vivacity ina remarkable degree , force character 
 Beethovenish symphony realised 
 perfection . titanic power music 
 brought , performance polished 
 smallest detail , details forced expense 
 breadth . Finally Mr. H. J. Wood conducted 
 concerts , stood test comparison 
 satisfactorily , giving characteristic reading 
 ‘ Pathetic ’ Symphony Tschaikowsky , 

 XUM 

 . said , , 
 symphonies allotted conductors typified 
 inaccurately respective individualities : Beethoven 
 Eighth Symphony , vivacious , taken 
 Colonne ; broad warmly coloured ‘ Eroica , ’ 
 Ysaye ; forceful dramatic C minor , Weingartner , 
 strongly emotional ‘ Pathetic , ’ Wood 

 absolutely new programmes , 
 quasi - novelties given . M. Colonne 
 introduced Symphonic Prelude II . César 
 Franck ‘ Redemption , ’ beautiful example refined 
 tender polyphony , orchestrated brilliantly . 
 manuscript Adagio C minor strings , Guillaume 
 Lekeu , Belgian composer , died 1894 , 
 threshold - fifth year , given M. Ysaye . 
 showed Wagnerian influence , 
 expected , tender somewhat elegiac 
 mood unlike Franck Adagio . M. Saint - Saéns 
 played , time London — piece 
 heard Sydenham — brilliant ‘ Africa ’ Fantasia 
 pianoforte orchestra , introduced 
 country graduated Cambridge 1893 , 
 sparkling character solo proved , needless 
 add , suited incisive style , , 
 intimated , happiest vein 
 occasion . Herr Weingartner appeared composer , 
 medium symphonic poem , ‘ Das Gefilde 
 der Seligen ’ ( Op . 21 ) , composition sonorous 
 rich orchestral colouring , displaying marked 
 individuality thematic material . Finally , samples 
 native art , Dr. Cowen new fanciful 
 overture ‘ Butterfly Ball ’ Dr. Elgar 
 famous Variations , known need 
 criticism 

 PHILHARMONIC SOCIETY 

 M. BasiL SaPELLNIKOFF appeared fourth concert , 
 oth ult . , played Tschaikowsky Pianoforte Concerto 
 B flat minor , work performed 1889 
 composer direction . M. Sapellnikoff , 
 , great executant , matter 
 technique wanting ; appears , , 
 gained power brilliancy . 
 reading work showed intelligence high order , 
 fact having personally acquainted 
 Russian composer strong guarantee 
 understood intentions . 
 wanting . Tschaikowsky concerto makes , 
 ways , strong appeal senses ; heard 
 inner voice speaking composer self , 
 personal character music fully 
 revealed , possibly fully felt pianist . , 
 achieved brilliant success , recalled 
 times ; grant encore redounds greatly 
 credit . rest programme 
 detailed notice . Lady Hallé played Mendelssohn 
 Violin Concerto better London Festival 
 fortnight previously . Dr. Cowen gave refined 
 sympathetic rendering Sterndale Bennett ‘ Paradise 
 Peri ’ Overture . performance Beethoven 
 Symphony B flat thoroughly good ; 
 Adagio , , created strongest impression . Miss 
 Ada Crossley , vocalist , sang arid aria Max 
 Bruch ‘ Odysseus , ’ consequently achieve 
 usual success . concert took place , usual , 
 Queen Hall 

 JOACHIM QUARTET CONCERTS 

 tempted appear public , enfeebled 
 powers prevented justice 
 music great masters . , 
 hand , public naturally indulgent old 
 favourites , strongly inclined ignore perceptible 
 shortcomings . Dr. Joachim shares common lot 
 humanity , years active service cause 
 high art passed leaving trace . 
 false flattery 
 quarter century ago ; strength 
 fire manhood wise declined , 
 truthfully said Dr. Joachim master 
 art , signs advancing age 
 gave counterbalanced wonderfully 
 pure dignified readings music masters . 
 , , players perfect unity 
 ; Dr. Joachim — use common 
 expression — leading concerted music 

 programmes contained works , 
 quartets . concert devoted 
 entirely Beethoven , quartets , Op . 18 , . 5 , 
 Op . 95 , Op . 130 , represented composer 
 stages art career ; programmes 
 great master instrumental music duly represented . 
 Haydn Mozart came share notice , 
 renderings works delightful ; 
 performance Mozart Quintet G minor ( 
 Mr. Alfred Gibson capable second viola ) , , 
 superb 

 Schubert D minor Quartet omitted 
 scheme . Special honour paid Brahms , life - long 
 friend Dr. Joachim , performance Quartets 
 minor ( Op . 51 , . 2 ) B flat ( Op . 67 ) , Sextet 
 G ( second viola , Mr. A. Hobday , second violoncello , 
 Mr. Percy ) . long year Dr. Joachim 
 persevere Brahms music ; public cold 
 critic hostile , daunted . 
 Brahms ranks classics . work 
 programme Schumann Quartet ( Op . 41 , 
 . 3 ) , recalls fight coldness 
 prejudice , victory Dr. Joachim 
 widow composer leading spirits . 
 fine work played rare sympathy 
 artists 

 MR . D. F. TOVEY PIANOFORTE RECITALS 

 PIANISTS ’ recital programmes run , rule , 
 groove : transcription Bach organ fugue , familiar 
 Beethoven sonata , group Chopin solos , 
 showy modern pieces way conclusion . Mr. Tovey , 
 , opens new paths . selected 
 present series recitals works public 
 means familiar . St. James Hall , Thursday , 
 oth ult . , played ‘ Goldberg ’ Variations Bach , 
 admirably clear thoughtful rendering 
 deserves praise . wonderful work , 
 requires study ere properly appreciated . 
 admire Mr. Tovey courage , fear 
 audience , iike man gallery Beet- 
 hoven ‘ Eroica ’ produced Vienna , heartily wished 
 blessed thing stop . second recital , 
 following Thursday , Mr. Tovey performed , 
 skilfully , ‘ Diabelli ’ Variations Beethoven . 
 elaborate works interpreter contributed , 
 form essays , detailed analysis . 
 reading thought , intelligent insight 
 music ; bulky , , probably 
 prove serviceable . , , fully admit 
 difficulty describing long works short space , 
 Mr. Tovey aim . 
 recitals played short , refined , ably written 
 compositions ; , pieces 
 violin pianoforte , fortunate 
 having Dr. Joachim associate ; , second , 
 Bagatelles pianoforte solo 

 400 

 encouraging sign singing numerous 
 school choirs took , evidence 
 care bestowed teaching music 
 elementary schools . competitions 
 invaluable . furnish zest labour prac- 
 tising , means testing comparing results , , 
 short , practical object lesson 
 greatest possible value competitors 
 teachers . advantage heard criticised 
 judge competent Dr. J. C. Bridge , Chester , 
 obvious , fault found 
 judgments , instances , 
 detailed 

 instrumental competitions , Mr. A. Bent 
 adjudicator , produced exceptionally good 
 results . pairs heard Beethoven Sonata 
 F violin pianoforte , exceedingly 
 played case , sight test 
 accompanied showed competitors consider- 
 able musicianship , winners giving finished 
 performances pieces placed . 
 pianoforte trios satisfactory , playing 
 Gade Trio F party prize 
 awarded best performance 
 competitions . , , remarkably 
 free amateurishness . Incidentally , added , 
 performance furnished interesting instance 
 heredity art , young violoncellist descendant , 
 sixth generation , Camidges 
 organists York Cathedral 

 r = e © - fll Se ae ee ae 

 choral classes familiar lines . More- 
 cambe Madrigal Society conspicuously successful , 
 winning open competition mixed choirs 
 highly finished performances , realised spirit 
 music , technically excellent , especially 
 matter keeping closely pitch ; 
 supplementary test sight - reading wonderfully plucky 
 attempt unpublished - song , 
 presented abnormal difficulties modulation 
 like . singing , high 
 standard adhered , mixed choirs sang 
 class ; fidelity 
 St. Helen Choir stuck pitch largely contributed 
 success . male - voice choirs Manchester 
 Orpheus , precision approached mechanical , 
 easily . competitions culminated Saturday 
 evening spirited performance Stanford 
 ‘ Phaudrig Crohoore , ’ chief choirs , accom- 
 panied Nelson orchestra , conducted 
 Dr. McNaught , , spite lack ofrehearsal , managed 
 effective ensemble heterogeneous force 

 concerts choral orchestral music took 
 place sumptuous hall known ‘ Pavilion , ’ 
 Blackpool Tower , magnificent 
 comfortable concert hall country , Rococo 
 decorations artistic elaborate . 
 chief choral works Bach cantata ‘ O Light ever- 
 lasting , ’ Beethoven ‘ Choral ’ Symphony , Brahms 
 ‘ German Requiem , ’ Parry ‘ Blest Pair Sirens , ’ 
 Stanford ‘ Post . ’ chorus , 
 mentioned , local , nucleus Glee Madrigal 
 Society , augmented singers district 
 total number 250 , proportion 
 orchestra , customary , 
 accord excellent principles recently enunciated 
 Professor Prout . balance tone , 
 , satisfactory , chorus 
 advantageously placed weight tone tell , 
 nearly level band . Considering , , 
 hitherto concerned great 
 choral works ‘ glees madrigals ’ 
 Blackpool Society , success 
 promising future Festival . soloists 
 Madame Blanche Marchesi , Miss Agnes Nicholls , 
 Miss Helen Jaxon , Miss Ada Crossley , Miss Florence 
 Oliver , Messrs. W. Green , Harold Wilde , Watkin Mills , 
 Fowler Burton , 
 known need commendation ; exception 
 favour Mr. Burton , young Manchester singer , 
 practically début Brahms ‘ Requiem , ’ 
 singing baritone solos fine voice considerable 
 intelligence . Dr. Richter band , , 
 guidance , developed surprisingly 
 expected , particularly good form , 
 gave superb performances , 
 singled Dr. Elgar poetic fanciful Variations , 
 ‘ Tristan ’ ‘ Meistersinger ’ Preludes , ‘ Hebrides ’ 
 Overture — taken brisker tempo usual , 
 excellent effect — Richard Strauss ‘ Don Juan ’ Fantasia — 
 masterly performance brilliant fascinating work 
 — , , ‘ Choral ’ Symphony , Richter 
 seen best . Capital organisation 

 policy judicious enterprise certainly marked 
 festival Blackpool , little doubt 
 strong effort perpetuate event 

 404 MUSICAL TIMES.—Junz 1 , 1go1 

 Tue Ysaye concert 18th ult . , Queen Hall , M. MICHEL DE SIcARD , Russian violinist , gave , th 
 afforded proof Belgian artist musical | 11th ult . , recitals successive Saturday Si 
 talents . gave superb readings solo parts of| afternoons , Steinway Hall . artist , enjoys ce 
 Bach Violin Concerto E Beethoven , was|a considerable reputation country , produced SC 
 heard clever composition , entitled |a remarkably powerful tone instrument , ol 
 ‘ Chant d’Hiver . ’ described symphonic | style commanding assertive . readings G 
 poem miniature , having programme deadening | chiefly remarkable energy , intensity , passion ; te 
 effects winter , melancholy subject , gracefully treated , | music tender sentiment lacked sympathy , g 
 individuality . intonation true . 

 Tue Royal Amateur Orchestral Society , ; Miss Amy LLEwe.tyn Jones showed results good : 
 direction Mr. Ernest Ford , gave enjoyable | training master , Professor Wilhelmj , violin 

 Tue London Octuor , ’ new body instrumentalists 
 formed purpose performing larger chamber 
 works , gave concert 16th ult . , Steinway 
 Hall . members comprise Messrs. Wallace Sutcliffe , 
 W. A. Boxall , W. Sewell , H. J. Poole , Claude Hobday , 
 W. H. Hall , E. Hall , A. E. Brain — goodly company , 
 able excellent performances 

 pianoforte recitals given month , 
 Mr. Harold Bauer , 15th ult . , St. James Hall , 
 memorable , , time , promising . Mr. 
 Bauer comes , years study , 
 reputation acquired Continent America 
 artist rank . Wehaveno hesitation endorsing 
 thisachievement . Schumann Sonata G minor ( Op . 22 ) , 
 Beethoven ( Op . ror ) , Chopin Scherzo C 
 sharp minor ( Op . 39 ) requires exposition 
 complete executive command keyboard , 
 individuality treatment , man successively 
 play works satisfactorily meet requirements 
 entitled placed leading pianists 
 day . Need said 

 Mr. WADDINGTON COOKE gave pianoforte recital 
 St. James Hall , 6th ult . , interpretations 
 Beethoven Sonata E ( Op . 109 ) Chopin 
 selection showed intelligent trianed 
 musician 

 Miss Hore Squire , young Yorkshire pianist , 
 favourable impression recital , 7th ult . , 
 Salle Erard . Miss Squire successful 
 uture 

 SUBURBAN CONCERTS 

 concert Crystal Palace Amateur 
 Orchestral Society took place April 30 , programme 
 including Schubert ‘ Rosamunde ’ Overture Entr’acte 
 pe 2 ) , incidental music ‘ Merchant Venice ’ 
 Sullivan ) , Beethoven Symphony ( . 1 ) C , 
 Mackenzie ‘ Benedictus , ’ arranged violins small 
 orchestra . Crystal Palace Choir assisted , singing 
 Mendelssohn motet ‘ Judge , O God , ’ - songs , 
 chorus Sullivan cantata ‘ shore sea . ’ 
 Miss Ethelwynn Weager successful 

 appearance solo violinist , playing Nocturne Chopin 
 Zarzycki ‘ Mazur . ’ orchestra advantage 
 veteran Mr. August Manns conductor 

 Tue Marlborough Place Amateur Orchestral Society 
 gave seventh annual concert , Hampstead 
 Conservatoire , 11th ult . , traditional success . 
 programme included commendable performances 

 Schubert ‘ Rosamunde ’ Overture , Godard ‘ Berceuse 
 de Jocelyn , ’ movements Beethoven Seventh 
 Symphony , Moszkowski Spanish Dances . Mr. 
 Aldebert Allen played solo Hofmann Concerto 
 flute great brilliancy , Mr. H. Geehl , 
 Mozart Pianoforte Concerto ( . 23 ) , Mr. R. J. 
 Stephens , De Beriot Ninth Violin Concerto , 
 excellent respective interpretations . Miss Margaret 
 Ruby vocalist , Mr. Paul Oppenheimer , 
 energetic alert conductor , orchestral forces 
 control . Mr. H. H. Hanes proved 
 , heretofore , capital leader 

 Maze Pond Choral Society closed successful 
 season performance Mendelssohn ‘ Elijah , ’ 
 oth ult . , Maze Pond Chapel , Old Kent Road . 
 choir maintained reputation good training 
 heartiness , sang vigour , accuracy , 
 evident love work rarely heard , 
 large choirs . praise given Miss Maggie 
 Purvis soulful rendering Widow 
 soprano music , Mr. Arthur Barlow dramatic 
 singing Elijah . Miss Frances Lake Mr. Harry 
 Stubbs sang , word commendation 
 subsidiary quartet , comprising Miss Edith Potter , Miss 
 Lydia Hopkins , Messrs. Alfred Sears T. Harry Bull . 
 accompaniment played small band ( 
 leadership Mr. T. Harry Smith ) pianoforte 
 organ , respectively played Miss M. Tyrer Mr. 
 Edward Partridge . Mr. W. Dexter Miller conducted 
 usual care efficiency 

 MUSIC VIENNA . 
 ( SPECIAL CORRESPONDENT . ) 
 Vienna , 19 

 TOURING musicians times , 
 travelling orchestra innovation days . Bilow , 
 respects , set example Meiningen 
 orchestra . subjectivity modern conductor 
 developed degree , order exhibit 
 skill properly , carry instrument — i.e. , 
 orchestra — goes . , 
 , apt forget highest aim 
 executive artist place ability 
 service creative artist , instead , happens , 
 merely utilising work purpose 
 converting . Objectivity , 
 fact , entirely ceased factor interpre- 
 tation German conductors present day . 
 notable illustration phenomenon furnished 
 Herr Arthur Nikisch , making tour , 
 Berlin Philharmonic orchestra , Austria , Italy , 
 Spain , France , Germany . gave concert 
 , programme including ‘ Leonora ’ 
 Overture Beethoven , ‘ Pathetic ’ Symphony 
 Tschaikowsky , Tannhauser ’ Overture Wagner , 
 smaller pieces , Herr Nikisch 
 presented , works actually 
 , , opinion , ought . 
 impossible entirely acquit excellent 
 conductor charge undue 
 inartistic exaggeration sake effect . 
 performances carefully thought , 
 prepared , tears tempi pieces deals 
 passages outré fashion , 
 incomprehensible . deliberate exaggeration 
 outcome spontaneous musical feeling , par- 
 ticularly place works classical masters , 
 , connection , Wagner 
 classed . case Tschaikowsky , , 
 altogether misplaced , somewhat superficial art 
 admits certain elasticity interpretation — 
 nay , demands times . treated , 
 frequently discovers , , profundity 
 thought Tschaikowsky music appeared . 
 , listening - elaborated eminently 
 subjective interpretation ‘ Pathetic ’ Symphony 
 Herr Nikisch , help feeling 
 importance work trifle overrated . 
 produced impression piece , 
 composer conceived solemn spirit 

 interesting concert given choir 
 church connected Russian Embassy , , 
 rebuilding , enlarged 
 converted artistically efficient body mixed voices . 
 performance occasion question consisted 
 number sacred compositions Russian composers — 
 Bortniansky , Glinka , Lwoff , — 
 rendered specifically Russian manner , strictest 
 attention minute effects regard dynamic 
 gradations , certain spontaneity rhythm , 
 apparently guided words music . 
 Somewhat monotonous uninteresting , purely 
 musical point view , compositions , 
 rendered extremely impressive 
 beauty voices unconventional , highly 
 artistic , interpretation choir 

 MUSIC MOSCOW . 
 ( FRomM CORRESPONDENT 

 opening new Concert Hall Imperial 
 Conservatoire Music , Moscow , Grand Duke 
 Constantine , took place April 7 ( 20 ) . evening 
 season Symphonic concerts given , 
 directorship M. Safonoff . 
 programme entirely devoted works 
 Russian composers — Glinka , Tschaikowsky , Borodine , 
 Rubinstein ; second great ‘ Choral ’ 
 Symphony Beethoven . chorus consisted pupils 
 Conservatoire , soloists artists 
 Imperial Opera 

 M. Widor , gave recitals magnificent 
 new organ , built Cavaillé - Coll ( presented Con- 
 servatoire M. von Dervas ) , showed capabilities 
 instrument organ music styles , Bach 
 present day 

 MUSIC BIRMINGHAM . 
 ( CORRESPONDENT 

 NEW musical organisation public 
 appearance close season , Birmingham 
 String Quartet , consisting local artists — Messrs. H. T. 
 Freeman D. Reggel ( violins ) , A. J. Beard ( viola ) , 
 R. B. Day ( violoncello ) . gave chamber concert , 
 Masonic Hall , April 27 . programme 
 comprised Haydn String Quartet D minor ( Op . 76 , 
 . 2 ) , Tschaikowsky Quartet ( Op . 11 ) , movement 
 Smetana Quartet E minor . Miss Rosina 
 Buckmann sang Beethoven Gellert songs ( Op . 48 ) 
 songs Schubert , Mr. Wymark Stratton ably officiating 
 accompanist . good attendance , 
 executants established claim recognition 
 support 

 Court Alley Concert Society starting 
 work bringing light sweetness slums 
 City , sound music beginning heard 
 parks . rest , recent musical doings 
 confined churches . April 30 Silver 
 Jubilee Roman Catholic Church St. Catherine 
 celebrated , new organ opened Rev. 
 H. McDonnell , organist Oscott College . 
 Sunday , 5th ult . , church , Paladilhe 
 ‘ Messe Solennelle de la Pentecote ’ soli , chorus , orches- 
 tra , organ , performed time England 

 MUSIC LINCOLN . 
 ( CORRESPONDENT 

 places advanced rapidly musical matters 
 late years city Lincoln , thanks mainly 
 enthusiasm Dr , George J. Bennett , Cathedral 
 organist , said leading spirit 
 movement , admirable support accorded 
 ladies gentlemen local Musical Society , 
 hon . conductor establish- 
 ment . latest Society achievements 
 orchestral concert given Volunteer Drill Hall , 
 17thult . orchestral class connected 
 Society rendered excellent service , members 
 supp'emented able body professionals festival 
 renown , Mr. H. Lyell Tayler principal violin . 
 Finished performances given Beethoven ‘ Leonora ’ 
 Overture ( . 3 ) , Mendelssohn ‘ Italian ’ Symphony , 
 Tschaikowsky ‘ Casse - Noisette ’ Suite , ‘ Ride 
 Valkyries . ” Mendelssohn Capriccio Brillant B 
 minor pianoforte orchestra performed , 
 pianoforte played great executive facility 
 Miss Grace Ellis . choral portion programme , 
 excellently rendered , way , consisted Stanford 
 ‘ Revenge ’ Schumann ‘ Grenadiers , ’ 
 sung Rev. H. A. Tapsfield , 
 priest - vicar Lincoln Cathedral , minor canon 
 St. Paul 

 Society gave Cathedral , baton 
 Dr. Bennett , fortnight later , impressive performance 
 Spohr ‘ Judgment . ’ fine Willis organ , 
 Mr , N. S. Trevitt presided , supplemented 
 band performers , Mr. Edward O’Brien 
 leader . choir attained high level excellence 
 , solos , taken Mr , E. Dunkerton , 
 Mr. C. Woodward , Mr. J. Orange , Masters Webb 
 Gooder , Cathedral choir , sung great 
 refinement expression . Dr. Bennett conducted 
 great tact skill . ‘ thank God , ’ 
 accompaniment scored orchestra Dr. Bennett , 
 sung heartiness vast congregation 

 Chester - le - Street Choral Society , 
 conductorship Canon Anson Firth , Durham , closed 
 successful season , 7th ult . , performance 
 Mr. J. F. Barnett cantata ‘ Building Ship , ’ 
 Co - operative Society New Hall , Chester - le - Street . 
 soloists Miss Janet Reed , Miss Edith Armstrong , 
 Mr. Edward Horn , Mr. W. Peacock . Mr. F. Lonsdale 
 principal violin , Mr. John Young presided 
 organ , Canon Firth conducted . choruses 
 sung performance respect 
 reflected credit concerned 

 13th ult . Durham Amateur Orchestral 
 Society gave ninth concert Town Hall , Durham . 
 principal work performed Beethoven Fourth 
 Symphony B flat , creditably played . 
 conductor occasion Herr Sigmund Oppen- 
 heim , succeeded position held time 
 Mr. Arthur Wallerstein . Mr. Alfred Oppenheim played 
 movements Mendelssohn Violin Concerto C 
 minor , Mr. John Nutton , Durham Cathedral , 
 vocalist 

 final concert season , 2nd ult . , 
 Newcastle Chamber Music Society engaged Joachim 
 Quartet , played Beethoven fine Quartet C sharp 
 minor ( Op . 131 ) Schubert Quartet minor ( Op . 29 ) 
 magnificent style , evoking heartiest applause 
 audience means easy enthusiasm . 
 Mr. J. C. McInnes sang songs Handel Brahms , 
 Mr. J. M. Preston admirable accompanist 

 MUSIC SHEFFIELD DISTRICT . 
 ( CoRRESPONDENT 

 ANSWERS CORRESPONDENTS 

 BEETHOVENIST.—(1 ) Browning ‘ Abt Vogler ’ published 
 Messrs. Smith , Elder Co. ( 2 ) Mozart 
 Masses , , second , twelfth ( - called ) , 
 Requiem obtained Messrs. Novello . 
 ( 3 ) believe greatest organ world 
 Town Hall , Sydney . ( 4 ) question ‘ 
 greatest organist greatest musician 
 world ? ’ like riddle 
 

 THANKFUL.—If earning good living 
 present avocation advise stick 
 use voice pleasure 
 friends . savings soon disappear 
 ‘ going routine ’ order ‘ 
 concert singer , , high class . ’ 
 advice , better consult 
 qualified musician city habitation 

 Wales ‘ ae ee ee ee ‘ “ < ee 409 
 Foreign Notes ee ee ee 410 
 Obituary ( Portrait Mr. R. » Redhead ) .. pe 
 Correspondence . , ee e ae aa oe 
 Miscellaneous “ xe eo 412 
 Brief Summary Country Colonial News .. ee eo 412 
 Answers Correspondents ee ee ee oo 414 
 Music published Month ’ ee oe os 414 
 - Song — “ Lilian . ”—S. P. Waddington « + 393 

 Creation Hymn . * — L , van Beethoven . 
 - aida “ Chase . ”’—Edward German . 
 _ ( Extra Supplements 

 Extra Supplements given 
 number : ( 1 ) Portrait Professor August 
 Wilhelmj , A. H. Fry , Brighton ; ( 2 ) Four- 
 Song , ‘ Creation Hy mn , ’ L. van 
 Beethoven ; ( 3 ) - Song , ‘ Chase , ’ 
 Edward German 

 MUSICAL TIMES 

 Gavotte .. « “ es Elvey . 
 Gavotte .. ee ee ee ee es - . Handel . 
 Gipsy March . ee ee - . Weber . 
 Hohenfriedberger " March “ ns ae 

 March “ Judas Maccabeus ” .. « .. Handel . 
 March “ Le Nozze di ~ — + » « + Mozart . 
 Marche Militaire ‘ es .. Schubert , 
 Marseillaise , La . . Rouget de Lisle . 
 Merry Peasant , Schumann . 
 Turkish March ’ . ‘ “ ae .. Beethoven . 
 Ye Mariners England « ee ee .. Pierson , 
 Wedding March PF Mendelsschn 

 Price Shilling Book 

 ORGAN . 
 VILLAGE ORGANIST.—Boox 18 

 1 , Funeral March ( Sonata , Op . ” ere ee - Beethoven , 
 2 . Ditto ( Sonata , Op . 35 ) .. ci ee ie “ la Chopin 

 3 . Dead March ( ‘ ‘ Saul ’’ ) ‘ Handel . 
 4 . Funeral March ( ‘ ‘ Story Sayid ’ » " vie o Cc . Mackenzie . 
 5 . Ditto ( ‘ Lieder ohne Worte,’’No.27 ) .. Mendelssohn 

 

 FORM TONALITY 
 BEETHOVEN PIANOFORTE SONATAS 

 FORMING 

 Orchestra .. os Score , net , 583 Parts , net 
 — Elegie . Harmonium .. Ge ee : eo ee 
 — Pages d’Album . Harmonium f ae ee oe 

 Meditation . Violin Pianoforte ee ee 
 BEETHOVEN.—Cadenza Concerto , Op . 61 , AvER , net 
 BOELLMANN . L.—Menuet Gothique . Wind Instruments . 
 Score Parts , net 
 BUSSER . = o au jardin des Hesperides . Pianoforte 
 Duet . 18 , ee ‘ e oe © 6et 
 CATHERINE , A. - " Barcarolle . Flute Pianoforte , , 
 — — Tarantelle . Flute Pianoforte ee 
 — — Serenade Melancholique . Flute Pianoforte net 
 DAYAS , W.H.—Sonata(F major ) . Cello Piano . bw % ws 
 FIELITZ , A. von.—Ballade . Pianoforte . Op . + 
 FONTENAILLES , H. de.—Elegie . Violin raf Piano 
 — . Violoncello Pianoforte . 
 FOOTE , A.—Quintet inA minor . Pianoforte , Violins , 
 Viola , Violoncello . Op . 38 
 FRANCK , C.—Deux Offertoires . Violin Violoncello hoor 
 Organ .. = és ee ee ‘ aa e. eac 
 — Ave Maria . Mezzo - Soprano Baritone , Accom- 
 paniment Violin Violoncello Organ 
 — Thesame . Tenor Soprano , ‘ Accompaniment 
 Violin Violoncello Organ ... “ ee 
 GELOSO.—Berceuse . Violin Pianoforte e 
 — Serenade espagnole . Violin Pianoforte .. os 
 GODARD , B.—Suite de Trois Morceaux . Flute . Op . 116 . 
 complete , net 
 . 1 . Allegretto , 4s . ; . 2 . Idylle , 5s . ; . 3 . Valse , 7s . 
 GOENS , D. van.—Invocation . Violin Pianoforte . Op . 36 

 No.1 . eo No.2 . Menuet . Violia Piano- 
 forte . Op . .. 
 GOLDMARK — p. Sakuntala . ” Harmonium 
 Pianoforte net 

 BACH.—"Passion ’’ ( St. Matthew ) .. . oo . Behold Lamb God , Foruntous ’ .. + » Messiah 

 fs 
 BEETHOVEN . — “ Mount Olives ” .. ( Paper boards , 1s . ) 2 . Worthy Lamb , Amen Chorus oo ee | RNORBIOR . 
 ( Tonic Sol - fa ) 3 . excellent Thy , Welcome , mighty King .. Saul . 
 BENNETT . — " Queen ” .. ( Paper boards , ts . 6d . ) 4 . praise Thee , O God .. - » Dettingen Te Deum . 
 ELGAR . — “ King Olaf ” 7 , 4d Sol fa 5 . rash intruder te Ae - » Solomon . 
 ai"thkin   # i. - ( Tonic Sol - ia ) 6 , HappyPair .. 02 « + Alexander Feast . 
 ‘ p leus tees aa ete ( Tonic Sol - fa ) 7 . Themany rend skies .. ae oe Alexander Feast . 
 ene H. — “ Passion " ( ‘ Der Tod Jesu ” ) ... 8 . divine Cecilia came agp cate “ Alesandes Posst . 
 M . — Messiah " ” ee g. willsing untothe Lord .. ie « » _ « + ‘ Israel Egypt . 
 — “ Solomon ” .. ( Paper boards , rs . 2d . ) 10 . Mourn , ye afflicted , Zion lamentation 
 EE 0 ey ee Ce ee ee ee Judas Maccabeus . 
 Sa + + ee ( Paper boards , rs . 2d . ) 11 . O Father almighty , Ah ! wretched Israel 
 — — Van Set Sele ? ( ado awh Meee mor vaete . \ ies Judas Maccabeus . 
 — “ Hercules ” . , . ois ee s os 12 . py array , Leadon .. fudse oo . 
 ome “ TAN ; : ” EP 13 . Hear , ord mn ee ae udas Maccabeus . 
 cero , il Pensieroso , ed il Moderato ” .. 4 M4 Fall'n foe ae udas Maccabeus 

 15 . Zion head shall raise , Tune harps 

 s. d. 
 37 . Musical Ornamentation . I. .. E.Dannreuther 5 0 
 37A. Musical Ornamentation . II ... .. E. Dannreuther 5 o 
 38 . Transposition .. < aa e J. Warriner 2 0 
 39 . Art Training Choir Boys G.C. Martin 3 o 
 3QA . . ( Exercises ) .. G.C. Martin 1 0 
 40 . Biographical Dictionary Musicians   W.H.Cummings 2 o 
 41 . Examplesin Strict Counterpoint . PartI. GordonSaunders 3 0 
 4IA . . II . ( Press . ) 
 42 . Summary Musical History . C.H.H. Parry 2 0 
 43 . Musical Gestures wa ae J. F. Bridge 2 o 
 434 . Rudiments Rhyme .. aa Pe ‘ aa J. F. Bridge 0 9 
 44 . Basses Melodies .. eo .. Ralph Dunstan 2 6 
 45 . Steps Pianoforte ae Francesco Berger 2 6 
 46 . Dictionary Pianists Composers Pianoforte 
 E. Pauer 2 0 
 47 . Organ Pedal Technique . PartI. .. B. W.Horner 2 o 
 474 . Organ Pedal Technique . II ... B. W. Horner 2 o 
 48 . Trios Albrechtsberger .. .. A. W. Marchant 1 6 
 49 . - Studies ( Notations ) J.E.Vernham 1 6 
 50 . Choral Society Vocalisation .. ee J. Stainer 2 0 
 50A. . Exercises Adapted Arranged 
 Female Voices ‘ < « A. W. Marchant 1 6 
 51 . - Solfeggi é - . James Higgs 1 o 
 52 . History Pianoforte aa .. A.J. Hipkins 2 6 
 53 . Scalesand Arpeggios .. Franklin Taylor .2 o 
 54 . Sonata Form oe “ W.H.Hadow 2 6 
 55 . Dictionary Violin Makers ee Pc C. Stainer 2 6 
 56 . Analysis Bach 48 Preludes Fugues ( 
 Parts , 1s . ) ‘ .. F. Iliffe 

 57 . 350 Questions Form Tonality Beethoven 
 Pianoforte Sonatas ( Appendix ‘ Analysis Form ” ) 
 H. A. Harding o 6 

 58 . Steps Harmonisation Melodies 
 J. E. Vernham 1 0 

 ORPHEUS ( New Series 

 J. Robinson 2d . 
 H. W. Davies 3d . 
 - . Beethoven 2d . 
 J. Barnby ad 

 A. Herbert Brewer 4d 

 MUSIC COMPOSED 

 L. VAN BEETHOVEN . 
 ARRANGED E. SACHS 

 Lonpon : NOVELLO COMPANY , Limitep ; anpD NOVELLO , EWER CO . , New York 

