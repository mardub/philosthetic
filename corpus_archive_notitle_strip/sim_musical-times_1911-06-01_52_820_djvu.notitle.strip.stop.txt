world . ’ opening ‘ Freischiitz ’ overture 
 took imaginative lad straight land 
 magic , piece strong 

 impression ‘ Fidelio ’ overture ( E ) . 
 story musical dramatic 
 development interesting , long 
 reproduced : introduction 
 Beethoven Symphonies hearing 
 Seventh , physiognomy means 
 lithographed portraits , sympathy 
 kindled learning deafness 
 retired life ; admiration Mozart , beginning 
 acquaintance ‘ Requiem , ’ 

 raised enthusiasm second finale ‘ Don 
 Giovanni’—these circum 
 |stances helped influence . 
 tragedy , ‘ Leubald und Adelaide , ’ written 
 , minute particulars . Shake 
 speare ‘ Hamlet , ’ ‘ Macbeth , ’ ‘ Lear , ’ 
 Goethe ‘ Goetz von Berlichingen , ’ contributed 
 , title indicated enthusiasm 
 composer ‘ Adelaida , ’ desire 
 provide music fashion 

 Beethoven ‘ Egmont ’ music . knowledge 
 acquired haphazard fashion : 
 quick erratic scholar , learned 
 easily liked , . Philology 
 interested , mathematics look 
 , wonder despair teachers , 
 practically expelled public school 
 Leipsic , virtually self - taught things 
 save music , luckily found sympathetic 

 teacher Weinlig ( ‘ Weinlich ’ spells 

 gather violin performances 

 striking excellence , adds , quiet humour , 
 family seriously urge 
 prosecute studies . influence 
 Schréder - Devrient art known , 
 recollections singing 
 published separately , 
 extracts relating performance 
 ‘ Liebesverbot , ’ removal Germany 
 Weber body , account Dresden per- 
 formance Choral Symphony , recollections 
 Spontini possibly portions 
 reappear proper places 
 story . ‘ impression Schréder - Devrient 
 Wagner singing ‘ Fidelio ’ 
 factors 
 Beethoven - worshipper ; , 
 potent , Choral Symphony , ‘ 
 focus fantastic musical ideas 
 aspirations 

 score studied diligently , 
 careful copy , arranged 
 pianoforte sent transcription Schott 

 70 MUSICAL TIMES.—Jvune 1 

 cenciidniemndaisiatailaten . ee 
 viven yearly Gewandhaus , , according | money belonged mother , till came 
 custom , instrumental movements played | thaler , staked whol 
 conductor , ‘ wentoff smoothly asa_| future . lost fled , dishonoures 
 Haydn Symphony , ’ Pohlenz , ‘ typically | penniless , fortune turned , ang 
 genial , fat music - director , ’ appeared conduct | won lost , suffi ien : 
 choral portion fashion Wagner | pay debts . went home ear ) 
 wonder , , Beethoven written | dawn , night rest enjoye 
 nonsense . ‘ experiences , especially | long time , woke different man . 
 Schréder - Devrient performance Bellini ‘ Romeo | clean breast misdeeds mother , , 
 Juliet ’ Magdeburg , Wagner direc- | thanking God , told son felt assured hy 
 tion , combined fancy Italian | fall course lif 
 music lay warmth spirit found | experienced temptation 
 German School . illusion | 

 wrote early opera , ‘ Das Liebesverbot , ’ 
 hich describes frivolous character , 
 ‘ Rienzi , ’ planned ( characteristic 

 wudacity ) scale suitable resources ; BEETHOVEN ‘ UNSTERB 

 Paris Opera . LICHE GELIEBTE . ’ 
 end period , : “ 
 | ERNEST NEWMAN , 
 speak , sowing wild oats musician , came 
 , Paris , heard Habeneck conduct love affairs great men fascinat 
 Choral Symphony Conservatoire concert . } ing , touch impertinent 

 operatic music| curiosity interest . Th 

 conducting Magdeburg Riga , | fascination increased uncer 
 turned Beethoven like repentant | tainty matter ; man 
 Prodigal Son , earnest repentance | tell gives joy discover 
 ‘ Faust ’ overture , set to| ing . ‘ amateur detectiy 

 ealised 

 work , soon gave proof his| concealed . excuse , 
 renewed allegiance fatherland ‘ Flying | , prying dead man secrets 
 ) utchman , ’ planned time . want know sak 
 outward circumstances Wagner early } simply pleasure ferretin 
 career told wealth detail far exceeding | ; hunter pursues fox 
 lengthy biographies Glasenapp Ellis . | exercise ’ sake , feeling 
 impossible , fair , to| animal , needs fi od . 
 attempt refer matters } anybody detective instinct ther 
 nterest , public| better subject practise wits 
 property time . , however,}upon question identity 

 instanced . student career Leipsic| Beethoven ‘ immortal beloved . ’ 
 short stormy . bellicose individual | remembered death letters , 
 soon found conflict fragments _ letter — wer 

 authorities , comrades , was| found Stephan von Breuning secret drawer 
 speedily involved duels ] composer room . far 
 renowned fire - eaters day , ! ardent Beethoven expressions 
 , , providentially removed incapaci-| affection ; incoherent 

 ed appointed day , Wagner—|through sheer heat haste . 
 , admits , satisfaction — escaped | indication addressed , 
 hurt honour person . He| place written , 
 describes force student - riot | year ; fragment headed ‘ 6th July 
 involved , description tumult | morning ’ ; second ‘ Monday evening , July 6th , 
 grew , took it|and ‘ Good morning July 7th . ’ 
 knowing fighting , | problem ‘ immortal beloved ’ exer 
 gives impression seek | cising minds scores Beethoven investi 
 origin Hans Sachs soliloquy meaning-| gators past seventy years . According 
 fury popular tumult , ‘ Wahn , Wahn , | Schindler , Nohl , Marx , Kalischer , 
 iberall Wahn . ’ source actual street-| immortal beloved Countess Giulietta 
 row ‘ Mastersingers ’ point dis- | Guicciardi . Thayer Grove held 
 turbance witnessed Nuremberg night , | Countess Therese von Brunsvik . Frimmel 
 personal disagreement grew rapidly | puts forward theory Magdalena 
 like tumult , abated rapidly as| Willmann . Recently Wolfgang Thomas 
 arisen , Wagner ‘ saunter home arm-|a ingenious attempt prove 
 n - arm brother - - law , laughing joking , | Amalie Sebald . good deal , course , depends 
 moonlit streets . ’ ‘ pleasant } year letters written ; 
 episode student life sudden happily | point pleasing 
 fleeting passion gambling , tells variety conjecture . Fora long time Guicciardi 

 ft 1 > > - . 9 
 frankly madness gambled away | theory adherents , Kalischer book 

 37 

 yo Unsterbliche Geliebte Beethovens ’ ( 1891 ) | 
 ‘ ing convinced waverers . Max 
 hemann translated Grove ‘ Beethoven 
 ne Symphonies ’ German threw 
 hors theory Therese von Brunsvik , pro- | 
 « ing convert reasoning 
 cjischer . play founded Beethoven | 
 » Sir Herbert Beerbohm Tree produced 
 yr ago , Giulietta Guicciardi , think , 
 figured immortal beloved . Mariam 
 enger gallant attempt , generation | 
 prove identity beloved Therese 
 » Brunsvik ; | 
 rely imaginative demonstration 
 easy prey Kalischer . ‘ 

 mplete edition Beethoven letters boldly 
 nd reprehensibly — heads letters ‘ 
 yuntess Giulietta Guicciardi , ’ prints 
 nong correspondence 1801 . Recently , 
 wever , discussion taken new turn , | 
 Guicciardi theory said 
 credited . ‘ notable contributions | 
 subject late years La Mara book | 
 Reethovens unsterbliche Geliebte : Das Geheimnis 
 : Grifin Brunsvik und ihre Memoiren ’ ( 1909 ) , | 
 ifgang Thomas ‘ Die unsterbliche Geliebte 
 ethovens : Amalie Sebald ’ ( 1909 ) , Max 
 nger ‘ Auf den Spuren von _ Beethovens 
 asterblicher Geliebten ’ ( 1910 ) . addition | 
 san excellent summing Hugo Riemann 
 new volume , issued , ‘ Thayer 
 Life Beethoven . ’ interested 
 ithe subject , time the| 
 atter fully , following summary | 
 evidence useful . 
 egregious Schindler , answerable 
 rso errors connection Beethoven , 
 ho floated Guicciardi theory . 
 mposer spoken Giulietta 1823 , | 
 language showed affectionate terms 
 years . | 
 nough good Schindler ; promptly dated | 
 letters 1806 , surmised 
 mitten Hungarian watering - place . | 
 Thayer showed Giulietta married Count | 
 ( allenberg November 3 , 1803 , gone| 
 0 live Italy . heroic invention of| 
 Schindler baffled mere fact ; 
 edition book altered 1806 1803 , | 
 ind smiled blandly . know , how- | 
 , Beethoven spent summer 1803 
 iderddébling , near Vienna , Hungary . Thayer 
 eld Beethoven visited Count von Brunsvik 
 nthe summer 1806 ; subsequently letter | 
 om composer Breitkopf & Hartel 
 liscovered , bearing date ‘ Vienna , 5th July , 
 i806 , ’ makes impossible 
 een Hungary 6th . , 
 n 1803 1806 July 6 Monday . | 
 La Mara , pursuit evidence theory 
 lherese von Brunsvik , obtained access , years 
 igo , lady Memoirs . [ died unmarried | 
 n 1861 , having devoted greater her| 
 time income founding institutions 

 care children . } Memoirs La Mara 
 publishes book indicate Beethoven 
 Therese terms usual 
 friendship ; clear 
 summer 1806 Therese Siebenbiirgen . 
 La Mara suggested letters 
 belong 1807 ; inconsistent July 6 
 Monday , Beethoven notoriously 
 careless matter dates impossible 
 mistake day 
 month day week . 
 hand remembered writes 
 ‘ 6th July ’ twice — morning evening — 
 7th July , hardly likely 
 error ; day week 

 days likely confused 

 Obviously thing clear 
 muddle adopt Thomas plan seeing 
 Beethoven year July 6 
 Monday . ‘ years 1795 , 1801 , 
 1807 , 1812 1818 . ‘ ruled 
 . Beethoven Moédling 
 time , case evidence 
 love - affair ; year Beethoven 
 . Frimmel decides 1795 , support 
 theory Magdalena Willmann . 
 Beethoven watering 
 place summer ; Thomas pertinently points 
 reason , 
 malady having developed . 1801 , 
 evidence sojourn 
 watering - place . , letter Wegeler 
 Kalischer dates June 29 , 1800 , 
 dated June 29 , 1801 , probable , 
 makes practically impossible visit 
 taken place . [ letter details 
 Beethoven illness . November 16 , 1801 , 
 gives information , evidently reply 
 Wegeler inquiries . incredible , 
 grounds , letters separated 
 interval seventeen months 

 1807 Beethoven 
 Hungary Brunsviks , Baden . 
 lines , , 1812 remains 

 going noted 
 early investigator followed Schindler 
 theory letters written 
 Hungarian watering - place . curious illustra- 
 tion sheep - like docility human mind . 
 atom evidence 
 watering - place Hungary ; 
 mention Prince Esterhazy letters 
 sufficient confirm original error . 
 letters long given , 
 passages essential inquiry 
 quoted . [ use Mr. Shedlock version 
 translation Kalischer complete edition , 
 vol . i. , p. 47 

 MUSICAL TIMES.—JuneE 1 , 1911 

 morning o’clock , short of|the remark ‘ till - morrow roon 
 horses , mail - coach selected route , | definitely engaged ’ looks chan , ged 
 awful road ; stage I| quarters 7th , come und 
 warned travelling night ; they|the eye compilers List . t 
 frightened wood , spurred | confirms correctness Beethoven con 
 — wre mg , coach needs|makes unnecessary admit oy 

 break , road dreadful , swamp , } calculations year July 6 
 mere country road ; postillions had|a Monday . [ Prague , way , 1s , 
 stuck way . Esterhazi,| miles north - west Vienna crow flies 

 horses . . . shall } direction , Carlsbad miles soutt 
 probably soon . ’ west Teplitz . ] Varnhagen von Ense Goeth 

 Bohemia time , 
 diaries , letters , & c. , Beethoven ow 
 correspondence , thoroughly connected 
 record composer movements . th 
 | evidence points love - letters dating 
 period . Beethoven speaks road 
 swamp ; know summer 1812 
 | exceptionally wet . speaks needing quiet 
 | steady life age ; far likel 
 lhe letters clear ( 1 ) Beethoven’s|to think 1812 , - , 
 ardent love fully returned ; ( 2 ) was|/in 1795 , 1801 , 1803 . handwriting 
 place far Vienna post | letters Beethoven middle period 
 horses required ; ( 3 ) } , Riemann points curious similarity 
 reaching destination / idea phrasing passage 
 company beloved . | letters , ina letter written little friend 
 Brunsviks country seat / Emilie M. July 17 , 1812 . ‘ pocket 
 Korompa , Hungary . La Mara surmised | book , ’ says , ‘ shall preserved 
 K stands Korompa , suggests | tokens esteem men , 
 letters written 1807 Pystian,| deserve . ’ Compare following 
 small watering - place near , Beethoven having | sentences love - letter : * Persecuted 
 come visit Therese brother|and kindness men , little 
 Franz . seen , 6th July year | deserve , little care deserve . ’ Max Unger 
 Monday , theory works fairly|has discovered old publication , ‘ Der 
 point . Memoirs Therese | Badegast Teplitz , ’ * Reichspost Prag , 
 distinctly says mother spent July , | Karlsbad Eger went Monday mornings 
 1807 , Carlsbad , having gone June.| . agrees Beethoven remark 
 rhis ae s La Mara desperate expedient | second letter , written Monday evening — * ‘ 
 supposing error ‘ Therese dates . | found letters posted ver } 
 makes Therese journey place | € rly Mondays , Thursdays , days 
 July instead June . Inany case , Thomas says , | post goes K. ’ morning 
 Beethoven guest Brunsviks | writes ‘ heard post goes ) 
 early July known of| day . ’ , fact , read end 
 migration Carlsbad , have| ‘ Badegast , ’ found , 
 written Therese Korompa . |enumeration routes , ‘ 15 
 1807 rejected , year left us—1812;| 15 September post arrives morning 
 doubt | Austrian territories , goes daily 
 year letters written , | 11 a.m. ’ 
 K stands Karlsbad,—Beethoven } doubt , , letters 
 habit spelling word way . | belong year 1812 , effectively puts 
 rhayer objection Beethoven wrote | Giulietta Guicciardi theory court . Wolfgang 
 Vienna June 28 , 1812 , been|'Thomas , belongs credit 
 Hungarian watering - place July 6 loses | clearing subject , holds 
 point soon watering - place located | ‘ immortal beloved ’ Amalie Sebald . 
 Bohemia . know Prague | Beethoven met young lady , , 
 July 2 . 17th writes Breitkopf & | way , fascinated Weber , Teplitz 1511 . 
 Hartel Teplitz 5th . | intimate circle Tiedge 
 Visitors ’ List appears date | Countess von der Recke . certainly 
 7th , shown lists Teplitz September , 1812 , possess 

 Monday evening , July 6 

 found letters 
 posted early Mondays , ‘ Thursdays — _ 
 days post goes K , 
 love happiest , | 
 time unhappiest men — 
 age need quiet , steady life . .... 
 heard post goes day 

 wrong extent day . number letters addressed composer 
 Beethoven tells Breitkopf & Hartel ‘ } month . possibly 
 5th July . ’ agrees reed 

 says letter dated July 6—‘I arrived 
 vesterday morning o’clock . ’ 

 sam 

 August , July . 
 surmises Beethoven seen 
 Prague shortly departure July 4 . 
 14th writes Varnhagen von Ense : 
 ‘ | sorry able spend 
 evening Prague . felt 
 right thing , circumstance 
 foresee prevented 
 . ’ ] learn Fanny Giannatasio del 
 Rio diary September , 1816 , Beethoven 
 spoke having unhappy love affair 
 acquaintance 
 years , hoped marry . 
 [ Thomas takes confirming Amalie 
 Sebald theory . Fanny Giannatasio merely 
 reporting conversation held father 
 Beethoven ; attach 
 credence remark composer having 
 ‘ acquaintance ’ 1811 . — Kalischer 
 says Amalie Sebald married 1815 . 
 date correct , 
 Beethoven speaks 8 , 1816 ( letter 
 Ries ) ‘ probably ( ! ) 
 ’ ? Fanny Giannatasio says , * 
 longer thought | marriage ] , 
 impossibility . ’ Beethoven Amalie 
 1812 ; Thomas thinks 
 love died , 
 ‘ unsterbliche Geliebte * ‘ entfernte 
 Geliebte ’ beautiful song - cycle composer 
 wrote 1816 

 Altogether evidence Amalie Sebald 
 strong . struck 
 cooler tone authentic letters ; 
 friendly , affectionate , 
 Beethoven ordinary correspondence 
 people liked . fiery passion letters 
 July trace . clear 
 composer love returned warmly ; 
 Sebald letters , indicate 
 lovemaking , Amalie 
 holding aloof Beethoven . 
 Riemann , weighing new facts 
 judicially , decides Amalie Sebald , 
 inclined favour Therese von Brunsvik , 
 admits certainty possible . know 
 relations Beethoven 
 Brunsvik family 1812 cordial . 
 Therese living Wittschap , far 
 Prague . uncle Prague ; 

 

 visiting early July , 

 met Beethoven , result 
 revival old passion ? Family pride , 
 Beethoven precarious circumstances , 

 account refusal ‘ Therese mother 
 allow marriage place . Memoirs 
 tell 1814 refused offer 

 marriage , ‘ previous passion having wasted 
 heart . ’ problem complicated 
 Beethoven amazing comprehensiveness 
 regard women . 
 love . 
 Guicciardi , Frau von Frank , Bettine Brentano , 
 Countess Erdédy , daughters 
 tailor Ries lodged , Therese Malfatti 

 friends tell | 
 Giulietta 

 Therese von Brunsvik , Amalie Sebald 
 doubt , attracted time time . 
 recent - dating number letters 
 puts practically doubt marriage 
 project 1810 reference Therese von 

 Brunsvik Therese Malfatti . certainly 
 sincere loves , capable 
 swift transitions . finally 
 love letters undoubtedly belong 1812 , 
 number considerations 
 likely addressee Therese von Brunsvik 
 . impossible , course , 
 letters written 
 know ; view 
 fulness record Beethoven life 
 month month extremely improb 
 able . looks Wolfgang Thomas right 
 dates wrong inferences , 
 Thayer , Grove , La Mara right 
 inferences merely wrong dates . 
 case Guicciardi theory longer tenable 

 CRITICISM 

 Occasional Wotes 

 following works performed 
 Choirs Festival held Worcester Cathedral , 
 | September 10 September 15 : — Tuesday morning : 
 | ‘ Elijah . ’ Tuesday evening : New choral work , ‘ 
 sayings Jesus , ’ Dr. Walford Davies ; ‘ Coronation 
 Te Deum , ’ Parry ; Motet , ‘ Throne mercy , ’ 
 Cornelius ; Choral Symphony , Beethoven . Wednes- 
 day morning : ‘ Parsifal ’ ( Act 3 ) , Wagner ; ‘ Stabat 
 | Mater , ’ Palestrina ; new Symphony E flat , Elgar . 
 Wednesday evening ( Public Hall Miscellaneous 
 concert , including new work orchestra , Prelude 
 | ‘ CEdipus Colonnus , Granville Bantock . Thursday 
 morning : ‘ St. Matthew Passion , Bach . ( 
 | performance new edition prepared Sir 
 Edward Elgar Mr. Ivor Atkins . ) Thursday 
 evening : New work baritone solos chorus , 
 ‘ mystic songs , ’ Dr. Vaughan Williams ; Violin 
 : Requiem , Mozart . Friday : ‘ 

 conductor festival 

 16 , annual oratorio services took place 

 ly Cathedral . morning service included Bach ‘ 
 held sure . ” Beethoven Symphony C minor . 
 fternoon performed Parry * Blest pair 

 rens nd Mendelssohn ‘ Hymn praise . ” choir 

 dis Herr Kreisler chief soloist 

 MM . Ysaye Pugno , inimitable interpreters 
 Beethoven , gave « rts ( Jueen Hall afternoons 

 April 26 , 3 , 1 10 , played series 
 f Beet ] 1 Sonatas violin pianoforte 

 Der Hid 

 alg 
 Konald Nicholson , 
 Dupare . 
 Miss Irene 
 Debussy . 
 Mr. Charles Victor , Bechstein Hall , 8 . 
 Feuer ’ ‘ Schnitterlied , ’ A@z/ Ha//wachs . 
 Mr. Alan MacWhirter , Steinway Hall , 8 . 
 Miss Evangeline Florence , Bechstein 
 * Panis Angelicus , ’ César Franck . 
 Fraulein Povla Frisch , Bechstein Hall , 9 . — ‘ Erlkénig 
 Beethoven . ’ . 
 Mr. Ernest Groom , 
 Lieder , ’ Deorah . 
 Miss Lilian Bowen 
 Hall , . 
 Mr. Lorne Wallet , ® olian Hall , 12 . 
 * Dichterliebe , ’ Schumann . ° 
 Miss Susan Metcalfe , AZolian Hall , 12 . 
 und Leben ’ Cycle , Sch 
 Madame Clara Butt Mr. Kennerley Rumford , Alber 
 Hall , 15.—- ‘ Songof Asia , ’ * John Kelly , ’ Stanford 
 * Der Nussbaum 

 Eolian Hall , 6 

 aAnN 

 Schuman , 
 Mr. Hul Bromilow , -“olian 
 et lagneau , ’ Garcta - Mansi / la . 
 Miss Elena Gerhardt , Bechstein Hall , 16 . 
 Lieder , ’ Juli Wertssh . 
 Miss M ry Wynne - Hulm , Steinway Hall , 16 , 
 songs sorrow , Colertdge- Taylor . 
 Miss Elise Grosholz , .olian Hall , 16 . 
 und Leben , ’ Schumann . 
 Misses Salter , Leighton House , 16 . 
 presidium ” ( specially composed ) , Sa / nf - Saczs . 
 Miss Winifred Ponder , AZolian Hall , 17 . 
 Cyril Scott . 
 Miss Maggie Teyte , olian 
 Dome , Fevrter , Rk . Hahn . 
 Mr. Reimers , Bechstein 
 liebte , ” Beethoven . 
 de Hubbard , Bechstein 
 ciate mi morire , ’ J / onteverde . 
 Henri Maal , Bechstein Hall , 18 . 
 * Pagliacci , ’ Leoncava//o . 
 Madame Julia Culp , Bechstein Hall , 20 . — * Adelaide , 
 Beethoven 

 folk - song quartet , olian Hall , 20.—()uartets 
 Op . 92 , Brahms . 
 Mr. Frank Gleeson 

 Miss Vera Brock , -Eolian Hall , April 24 . 
 minor , Chopin . 
 Miss Myra Hess , Bechstein Hall , April 
 Tone - Poem , ’ Arnold Bax ( performance ) . 
 Signor Paolo Martucci , Bechstein Hall , April 26 . 
 G minor , Schumann 

 Miss Marie Dvorak , Bechstein Hall , April 26 . 
 Appassionata , ’ Beethoven 

 Misses Truman , Steinway Hall , April 27 . 
 pianofortes , J / ozart 

 Bechstein Hall , 12.—Chromatic 
 » , Bach 
 teen Hall , 13.—A Chopin 

 13.—Sonatas 
 inanuel , ~ * Reflets dans 
 Y. 
 sevy , 
 E minor , .J / end 
 Miss Adela Hi umaton , 
 Cosa . Godeenaian 
 Mr. Ernest S chelling , 
 Op . 111 , Beethoven 
 Miss Marion Phillips , 
 Va Dowel te 
 M. Godowsky , ( Queen 
 gramme . 
 Mr. Perciv 

 16.—Prelude fugue 

 Miss Maude Niner , Queen Hall , 4 . — ‘ Kreutzer 

 Beethoven 

 Sonata 

 

 M. Zacharewitsch , 46 , Berners Street , 18.—Sonata 
 E flat , Beethoven 

 Miss Dorothea Walwyn , .Eolian Hall , 20.—Sonata | 
 dD , Corellt 

 Steinway 23 

 Bechstein Hall , April 27.—Sonata , 
 Beethoven . 
 Mr. Paulo Gruppe , Bechstein Hall , 5.—Symphonic 

 Variations , Poé//mann 

 combination 

 comprising Messrs. Arthur Catterall , Ernest R. O'M ’ 
 David Reggel , Johan ¢ Hock . string quartets 
 chosen performance Beethoven Op . 18 , . 2 , 
 G major , Dvorak * Nigger quartet , ’ Andante 
 variations Schubert famous ( Quartet D minor 

 Perfect unanimity beauty tone characterized 

 rtitude 

 unter reely eX 
 Pickin M - Se ee eee tne tie mene melee tractinne ath cl . ‘ oll reel ! 
 16 , W reported tl u recent perform- | attractions , Claiming following . T peakers . 
 es ‘ Merrie England ’ produced net profit £ 162 , | management considering plans p Cowen 
 w 155 vot rital nstitutions . season , expected revival ncerts . 
 interest support scheme y ‘ tinguis 
 d * : ; worthy city like Glasgow . year ire having naninoft . 
 4 ¢ nwalla numb ¢ ] * horal . . . . ¢ nS ut 
 Phere ¢ Cornwall amber small ¢ aoa hy noral | unusual experience second musical season caused 4 mo 
 soc rk wit ithusiasm considerable difi- | opening National Scottish Exhibition , y e late . 
 cultic   dista nd 1s blade u ese ar de serving | continue till October . scope Exhibit deve 
 S } t. c ladron class oi filty voices " xcee ‘ e origi 1 ' : : 
 tag ' _ class ty . : far exceeding riginal plan prom » rmatiol 
 sang \cuc ennett ) , assisted small ) musical attractions special feature . | Rodew : 
 orcnest cnn Apemea-—pexalgies | Mr. A. H. Thorne , | addition appearance stars magn inter Sé 
 Ba ume — sang Gounnd ’ « * Fanet ’ Ans ol w — : ° vin 
 wi Marazi ang Gounod Faust ’ April 26 ; | Kubelik , Pachmann , & c. , choral music repre er tienen 
 \ 9 } ) pres ! 
 ~- Merifield ( ae ral ey ne opening week Queen Hall Orchestra , unde ; w 
 SOI E+ , \. Gr usehole , April 30 , | Sir Henry J. Wood , gave series performances , eave Li ' 
 eI 1 . M.C.A M “ ' 12 , Choral Union sang Mr. Hamish Mac nd r 
 “ 2 W * lay minstrel . ” work , believe nd hon ’ 
 Mr. R. N composed produced Choral Union 18 % rchestt 
 ( L ry . charm novelty t 4s don ! 
 ‘ \pril 3 uudience . Save occasional lapses pitcl usic . 
 t satisfactory performance given baton t Amateut 
 cos cus composer . soloists Misses Jenny Young s fam 
 ; ay , 1 ) Picken , Messrs. John Jamieson Rober nto 
 y Rev. ¢ Corfe , received vocal assistance ett , capable band led Mr. Verbrugghen sup , ( 
 | Edit | t ' ° } + } fall 1 } t 4 bd 
 ; prano ) ; € following day | accompaniments . Great interest evinced t Mr. Apj 
 Truro $ ty , tuct y Rev. Canon Corfe , sat 5 & | inauguration Competitive Festival n erform : 
 progr ev art - songs . 3 , Mr. Alan | takes place xl n June 23 24 . T anofor 
 Pho : $ s gave excellent pe entries var competitions numerous , Mrs. St 
 ¢ iH trilogy , s introducing | promising exceedingly success ! sho sin } 
 | ‘ M de opening | Jaynching movement “ 
 / > : 4 | UACcHINg ‘ vement , ntentiot 
 ! Harrowbarrow United Methodist Church | 
 \f 
 M } — , nalle 
 GLOUCESTER ser 
 ennai 
 . om » af success ! 
 EDINBURGH . Gloucester Orchestral Society held ¢ er = r 
 : — T bom oi hans } Ses mecatidintniat alk tie Cilia . 
 f Orp | Rcemcented Gasee April 21 , assistanc f slouce onetle 
 . “ | Orpheus Society . large attendance , ; 
 Musicians , series Tableaux Vivants , symbolic * . age ... : ise . 
 r “ 5 ‘ ‘ concert successful character . Dr. A. Herber 
 Europe ries , orchestral illustrations , | ; , , ae : y Miss 
 SS GE ta Sener - paeeeiiedl tes Brewer conducted Societies items . T ‘ ar 
 na il Gar 5 , n iri > pe songs , presente , | * aad & 
 1 ) ; contributions f instrumentalists eethover 
 Music Hallon 19and20 . countries represented = : rumental - » 
 oo d . oh vege t Symphony , . 7 ; Weber ‘ Oberon ’ Overture ; ar gre 
 Italy ( Corelli rebuking Cardinal conversation | 7 : 29 , Ch é om ) Api 
 eer , $ j Dvorak Slavonic Dances , Nos . 1 3 . playir wits 
 rr l erformance music ) ; France ( Minuet il ; M f : O Willian 
 , . -eptionally good . ost items given th ph 
 Frenc!l ) } ; Spain ( Bolero ) Germany ( Beethoven Seeks ad | ' ; pemaged ... aia . gtiorg enthusi 
 t < 4 oc ; ld favourites , bu . . ‘ rv chos 
 nature ) ; Slavonic Nations ( Polish Mazurka ) ; ciety Onl fa wens » Gut nstrum 
 : varied talents Orpheonists 1p 
 Si ivia ( Norwegian wedding procession ) ; England ™ , ps : und 
 : : ar possessed . selections ‘ Toast ’ ( Brewer ) ; * Star f 
 ( Morris dance ) ; Scotland ( Scottish harp ; connection | F- ae ? si % eail . - perforn 
 ’ ' : ; Summer night * ( Cruikshank ) ; ‘ phantom host men 
 W Miss Margaret Kennedy , accompanied 2 . ; , : cert 
 , \ . PD : 1 ? Gaal ( Hegar ): ‘ Analogy ’ ( C. H. H. Parry ): * true loi 1 
 harp Irs . Sherwood Begbie , sang old Gaelic song ; ‘ = aie _ : ge Dawsol 
 1 ) . ; 4 hath heart ’ ( C. Lee Williams ) ; MacDowell * Dane \ 
 Highland interior , Scottish reel danced : ; ; “ ohie ™ 
 ‘ } 1 1 gnomes . Mr. W. H. Reed , protessiona iain 
 ) n sible choir , conducted Mr. Tohn > . . . leserve 
 , pyc ute | | instructor Orchestral Society , contributed interesting oe 
 Kir sa lrigals - songs delightfully , lac viol : : aster 
 . . ¢ selection violin solos . ‘ 
 t stra , unseen , led Mr. Winram conducted | S © 'SC"10 ! , 1 % e Efi nA ot 
 \t 1 ) eee . concert Gloucester Choral Society fittet M 
 y Mr. T. HH . Collinson , provided instrumental music . held M , ‘ tr . 
 , ; ’ season held M : , cus ary 1 las 
 rranged Mr. William Hole , Mr. | ™ 7 pads 7 Agen . = arbathesees es ' afer annual 
 Tat Paterson , Mr. fohn Duncan , Mr. John Menzies . ps rformance eac — ha gee misce 
 , ‘ description . 1e choralists , goo sa 
 Mr. H. J. Lintot , Mr. Graham Glen ; dances | @2¢0 % “ % * Bach ’ ok nied t f ~ — able hor cea 
 Madame Marie Maclennan , Miss E. Gray - Macfarlane , voice , sang bac unacce mpanied otett r doul . ( : hem ti 
 Vs . “ epee ee : : ‘ shall Grace ’ ; Mr. P. Napier Miles ‘ Ode t \ 
 ir . Maclennan Mr. Graeme Goring , character . : . + c Ir . 
 weer snout hI ] Maia , ’ beautiful unaccompanied - song ; Sit “ 
 herald , rodt aq ti ea appropriate quatrain . . o ‘ — ; . ree | 
 ‘ > } _ Edward Igar * Banner St. George . ee 
 manager Mr. Duncan Rhind . successful | * /~ 
 mniieathen c + , = , - . | given necessary vigour volume . Mr. A. “ y 
 productio varied tableaux , | > : . ot 7 ’ ? - : 1 Th aria 
 ' rkabh tifel sreuntth wiowel tw audience Porter played organ , Dr. Brewer conducted . 
 ~ ' Se ; ‘ soloists concert Miss Gladys Honey , Miss Lucy Ster 

 Nuttall , Mr. W. H. Squire . Hall o 

 los cleverly played Mr. Horace Cropper 

 Anfield Orchestral Society closed season 
 n April 26 concert conducted Mr. 
 William Faulkes , - known organ composer . 
 enthusiasm shared earnest body amateur 
 nstrumentalists , heard ‘ Oberon ’ Overture , 
 ud Beethoven Symphony . < _ highly interesting 
 performance given Schumann Pianoforte 
 mcerto , solo played Mr. William 
 Dawson , veteran local pianist , organist composer , 
 instrumental works print manuscript 
 leserve better - known . played executive 
 astery striking examples Adagio F , Valse 
 n chromatic basis 

 Mr. Legge , City Director Education , presided 
 innual meeting Liverpool Village Choir 3 

 Ila Sy ) v Orchest 4 % performers gave 

 ilists Mrs. Wiggins , 
 Ihe chief numbers 
 Finale Beethoven Symphony 

 Interm ) nd ‘ Finlandia . Mr 

 season exce 

 Symphony flutes string orchestra proved mor 
 historical interest . 
 BERLIN . 
 Kénigliche Kapelle ( conductor , Dr. Richard Strauss 
 finished excellent performan 
 Beethoven Choral Symphony , conduct 

 season 

 rHE HAGUE . 
 Willem Hutschenruyter ( supported wealthy Di 
 Hoogehauser ) , conceived project building idea 

 hall performance Beethoven music . art 

 YLIM 

 
 MUSICAL TIMES.—Jvune 1 , 1911 405 
 LL — 

 emple designed Herr H. P. Berlage , Union Graduates Music held annual 
 erected Diinen ( sandhills ) vicinity | meeting dinner Criterion Restaurant 
 jarlem . assist undertaking , Beethoven Festival | 11 . Professor Percy Buck took chair 
 colossal dimensions given recently . | gatherings . interesting remarks 
 1oonder } ceedings , lasted fortnight , inaugurated | existing conditions teaching counterpoint . Sir 
 itst perfon performance * Fidelio ( mdse - en - scéne specially Charles Stanford , Sir Walter Parratt , Dr. McClure , Dr. 
 " Messe ’ signed occasion ) , programmes included Pearce , Dr. T. Lea Southgate , , 

 corded , enine Symphonies , ‘ Missa solemnis ’ ; Violin numerous speakers banquet . 
 = ncerto , Pianoforte concerto G major ; School Music Review June contains reports 
 anoforte sonatas Op . 27 . 2 , Op . 31 . 2 , Op . 57 , | demonstration practical musicianship given Steinway 

 LEIPsIC , 
 7 , new - act comic opera * Monsieur 
 Bonaparte , ’ composed Bogumil Zepler libretto 
 Hans Hochfeldt Hans Brennert , produced 
 nsiderable success Neues Theater . 
 Sonata recital given Professor Julius Klengel Herr 
 Leonid Kreutzer , new Sonata violoncello pianoforte 
 oA major ( Op . 71 ) , Siegfried Karg - Elert , * Sonate 

 Ballade ” Russian composer Michael Gnessin , 
 ittons roduced . 
 es Kw LAUSANNE . 
 sky direction M. Carl Ehrenberg , late Gustav 
 arm Mahler fourth Symphony ( soprano solo ) played 
 tenth subscription symphony concert . 
 teresting works recently performed Rimsky- 
 Fe Korsakoft symphonic poem ‘ Antar , ’ Granville Bantock 
 Str ture ‘ Pierrot minute , ’ Hugo Wolf 
 : Italian Serenade . ’ 
 MILAN . 
 novelty season ai Scala Theatre 
 2 Paul Dukas ‘ Ariane et Barbe - bleu , ’ given 
 — vith great success . 
 Messt PARIS . 
 ee April 26 - act operas , viz . , ‘ Le voile du 
 - r heur , ’ composed Charles Pons libretto Paul 
 yest Ferrier ( adapted drama Georges Clémenceau ) , 
 ee ‘ La Jota , ” text music Raoul Laparra , 
 oo roduced Opéra - Comique . Beethoven Festival 
 “ nducted Herr Felix von Weingartner took place 
 ee 2 , 5 , 8 10 , programme including 
 e Symphonies . Chabrier interesting opera * Gwendoline ’ 
 revived Grand Opera . Theatre- 
 Sarah - Bernhardt Russian opera season commenced 
 y 2 performance Dargomyjski * Roussalka . ’ 
 resting 6 , Rubinstein ‘ Demon ’ given great 
 liners success 

 PRAGUE . 
 Czech National Theatre , Franz Picker - act 
 ft pera * Maler Rainer ’ ( libretto Karl Naschek ) 
 produced composer direction 

 RIDGE , J. FREDERICK.—‘‘A Song Englist 

 ho s Beethoven ‘ Unsterbliche G bte . ’ AY ae ggpee 
 W Beethoven nsterbliche Geliebt Violin 1 , 1s . ; Violin 2 , 1s . ; Viola , . ; " Cello 

 Ernest Newman 37 ° | Bass , 1s . ; Wind Parts , tos . 6d 

 SOPRANO MEZZO - SOPRANO . 
 extol Thee , O Lord ( Novello Sacred Songs s. d. | CHAMBER MUSIC 

 a.—I w 
 Soprano , Set I. ) 2 os . 
 M iat aceite : Wiehe AGOSTINI , M.—Op . 17 . Trio Pianoforte , 6 
 \ Costus Utevelis Sone ta Dea CG AMBERG , J.—Op . 12 . Fantasiestiick Clari 
 j ' ’ ianoferte : ¢ 
 ( Page Song ( Romeo Juliette ) . » 6| AULIN , T.—Four Aquarellen Violin Pianoforte . Com- 
 Recitative Air de Lia ( L’Enfant Prodigue ) piste 6 
 ( et . “ S told love » @ | BARNEKOW , C.—Op . ) Idyll f String Orchestra . 
 yf Rect . “ ' Tist ” 1 ) yer Wo : traded , 
 \ Air , ‘ ‘ Heream ” j 8 " ° | BEETHOVEN . — C ; e 
 CONTRALTO . | SS t , ‘ Cello 
 : Pianoforte ; Oboe 
 i. brideg m ” ) Christmas Oratorio Pianoforte 1 
 Air , ‘ * Prepare thyself , Zion I. ) } * 
 se da caine BRZEZINSKI , F. fort 
 Strauss , R. — ‘ ‘ Lover Pledge 1 7 
 Costa.—Morning Prayer ( Eli ): ( Novello Sacred Songs | CARO , P.—Op . 42 . 
 Contralto , Set Ih ) . DAVID , K. H.—Op . 7 . Trio Pianoforte , Violin ‘ Cello 

 rENOR . | } GLIERE , R.—Op . 49 . Duos Violins eacl 
 Siegmund Love Song ( Die Walkiire ) } Op . 51 . Albumblatter ‘ Cello Pianoforte 

