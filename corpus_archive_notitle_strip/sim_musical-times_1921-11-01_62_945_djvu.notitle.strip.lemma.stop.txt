requirement Teachers ’ Registration Council , institute 

 lecture Beethoven PrincrpaL 

 wednesday , November 2 9 , 3.15 

 rehearsal Mondays . membership | 
 apply hon . secretary , 200 , Munster Road , Fulham , S.W. 6 

 want special musical service 
 Islington Church October , November , December 
 , help small orchestra provide 
 illustrative music address Rossini , Haydn , 
 Beethoven.—Mr . F. SALMON , 58 , Berwick Street , 
 we2 

 pianist ’ cellist ( young man ) like meet violinist | 
 regular practice . ( Nottingham . ) large library 

 Mr. Wallace G. Breach , St. John Evangelist , Clapham 
 Rise — Mairigal , Lemare ; Andantino G _ minor , 
 franck ; triumphal March , 4 / cock 

 Dr. R. Walker Robson , Christ Church , Crouch End ( 
 recitals)—Introduction fugue ( sonata b fiat ) , 
 Rheinberger ; Légende , Vierne ; prelude b minor , 
 Bach ; Maestoso , / acdowe// ; Beethoven pro 

 Lancaster 

 
 medieval Church music ( ritual ) good musical world , english orchestra , 
 correct thing . mention ' handsomely , ' | thank school music Royal College 
 ' fine melody plainsong ' — other| music , compose entirely native - bear 
 hand , course , good deal era plainsong | composer 

 fit waste - paper basket — Church Mr. Arthur Bliss wonder place Beethoven 
 music - day . revival descant be| Brahms symphony programme . 
 desire man community , unpractical , , | lose admiration master , judge 
 repeat , mixed congregation ' vain attempt acclaim public , fancy 
 clock . ' frequently difficult congregation | England feel 

 , refrain listen the| hear composition Mr. Bliss , 
 choir . introduction descant | know class 
 dificult prevent listen . ought to| disciple ' ugliness music ' 
 transfer interest choir congregation , | young school worship.—your , xc 

 venture far discuss vocal 
 instrumental branch , evil 
 apparent , operatic excerpt 
 curiously arrange mutilate , 
 doubt voice thing , vehicle 
 secondary importance . instrumental item 
 comparatively satisfying , large extent - 
 selection , judicious , 
 long work recently leave untouched , 
 mercifully , far | 
 escape fate orchestral work . field | 
 chamber music proper case hetter . recording 
 work nature manner date 
 somewhat later period , spite excellence 
 good deal music render form , 
 term theexperimental stage 
 entirely leave 

 true Beethoven ( Quartets 
 available , shortened form ! composer 
 course , suffer like manner , Mr. Schuster suggest 
 Mozart , instrumental , orchestral 
 work , review , 
 fate time come , recording 
 music consider duty , leave individual 
 taste idiosyncrasv , advertising particular 
 record . instead , uncommon find 
 isolated movement , impression , 
 unintentional , complete work worth | 
 hearing ; , muny case , time - hallowed composition | 
 attempt , futile the| 
 interpreter , know , responsible snippet 

 correspondent subject 
 large disc 

 practical illustration principle involve 
 afford simple compound pendulum suspend 
 roof Duke Hall ; polyphonic 
 wave syren ; ' ripple tank , ' exemplify 
 propagation reflectionof wave motion . large number 
 micro - photograph gramophone curve 
 screen ; interesting figure draw Mr. F. 
 Corder harmonigraph , exempl fye combination 
 motion . number special record repro- 
 duce characteristic tone - colour wind 
 instrument orchestra play -£olian- 
 Vocalion 

 course lecture ' history Music ' 
 Wednesday afternoon . 
 place October 19 , Dr. Shinn lecture Haydn 
 Mozart . brief sketch condition 
 instrumental music previous time Haydn , 
 description social political condition 
 Germany Austria 18th century , lecturer 
 pass review chief advance 
 composer domain pure instrumental 
 music andin development orchestra . selection 
 movement quartet Haydn Mozart 
 play illustration . follow lecture 
 Principal deal work Beethoven 

 TRINITY COLLEGE MUSIC , LONDON 

 798 MUSICAL TIMES — Novemper 1 1921 

 superficial idea . place Mr. Anderson Tyrer,}| September 27 contest brass band thinj 
 prevent play Schumann Concerto } section hold Crowlas , Mr. Edwin William ; 
 owe toa sprain wrist , Miss Dorothea Vincent | ( Camborne ) adjudicate . test - piece Fantasia , 
 soloist occasion , choice Beethoven | ' Maid Orleans ' ( Laurent ) , place win 
 Pianoforte Concerto C minor . playing proved| Fraddon . result event new band 
 exceedingly neat expressive , tone | form Ludgvan , direction Mr. R. T 

 trifle thin , manage present fine work a| Williams . 
 considerable insighi requirement . 
 = COVENTRY DISTRICT 
 BRISTOL om 
 apart chamber concert local 

 ee 5 Deol ' . - , : : . 
 Beggar Opera ' Prince Theatre | instrumentalist Coventry Warwickshire Society 
 hearer . stage perform cleverly 

 s 2 Artists ’ annual exhibition Corn Exchange , 
 M. Kubelik appear ( \ctober 3 opening concert | Rover Orchestra , conductorship Mr. J. Clarke 
 ' international celebrity series . varied opinion ! inaugurate ' music 
 express playing , musician found|;¢h . season . Albany Road Hall , September 23 , 
 wanting expression tone Te haikovsky Concerto organization , string considerably 
 D ( = — cog meagre — augment , present distinction programme 
 piece play grace eeling , include movement Beethoven C minor Symphony , 
 evening tone violin | -y . interpretation item reflect credit 
 2@ 2 ac ; doa u 4 > bd | ™ - 
 lack richness . Mr. Percy Kahn draw warm praise | performer , amateur 
 understand art accompany 

 local . 
 pianoforte . Miss Leila Megane , w ith dramatic | short season opera English Royal Carl 
 singing , real success evening . 2 

 great day month Saturday , October 8 , 
 Mr. Albert Coates , London Symphony Orchestra , 
 M. Rosing Signor Ticciati , open season 
 subscription series , fine attendance ai 
 evening , afternoon . Mr. Moiseiwitsch Torquay , September 15 , 

 Scriabin * Poem ecstasy ' evening event play Beethoven Sonata C sharp minor , introduce 
 hour , discussion rage hot strong . | s¢veral piece modern pianoforte music hear 
 wisely , Mr. Coates ’ programme example | ' " Devon , Etude ( F sharp ) Stravinsky 
 camp . Beethoven Symphony . 7 follow ' awakening ' nature poem Eugene 
 Pianoforte Concerto ( Signor Ticciati soloist ) , | Goossens , dedicate pianist , 
 srahm ’ second Symphony , adequately balance Scriabin | close programme come Palmgren beautiful ' 
 Swan . ' Chopin group follow seldom - hear 

 meeting announce intention concert 
 present winter , November , 
 New Year 

 dramatic power 

 CHATHAM 
 concert winter season hold Royal Carl Rosa Opera Company week 
 Chatham Town Hall October 12 . inaugurate a/|répertoire performance Torquay week 
 series bring Cortdt , Siloti , Thibaud , | begin September 26 , week 
 Anne Thursfield , John Coates , Granville Bantock , } Plymouth . advantage , Mr. Glover open 
 - know artist district . four| new series Sunday concert Plymouth October 2 , 
 concert - giver October 12 — Rosina Buckman , Angelo | engage operatic artist — Miss Kate Campion , 
 Rosselli , Adila Fachiri , Jascha Spivakovsky — last-| Miss Constance Willis , Mr. Gerald O’Brien , 
 especially successful . | Mr. Appleton Morris , Madame Turato violinist 
 Rochester Choral Society start rehearsal , | Madame Culp pianoforte . 
 chief item programme ' Hiawatha’s| Choral Society form Plymouth , 
 Wedding - Feast ' Beethoven ninth Symphony , given| sufficient choral 
 conjunction London Symphony Orchestra . organization . pity amalgamate 
 Society perform ' Hiawatha ' music Coleridge- | form pick choir , lead way West 
 Taylor conductor , abvut year ago , | choral music . late Society organize Mr , Percy 
 Rochester Symphony Orchestra increase member- | E. Butchers , year train choir 
 ship season . member begin weekly| female voice good standard . intention 
 practice Dvorak Symphony , * New World , ' | secure voice performance ' 
 outstanding performance month Festival | Redemption . " Mr. Butchers organist choirmaster 
 arrange local Free Church Choirs ’ association | Mutley Baptist Church . choral society 
 October 26 , work order . Exmouth ( Mr. 
 CORNWALI Raymond Wilmot ) , close season deficit , 
 — — decide perform ' creation ' autumn , 
 Mr. H. S. Middleton , young organist Truro|a view recuperate . Exeter Oratorio Society 
 Cathedral , enthusiasm , appeal young man | reorganization , fix programme . Mr. 
 musical life district join | Denis Read , recently appoint organist St , Edmund 
 Musical Association , choir ' Messiah ' } Church , Exeter , ancient church stand 
 Brahms ’ * Requiem ' rehearsal . Mr. W. J. Bayeley , | ' wall , ' organize series sacred concert 

 Penzance , energetically advocate founding 
 school music town , hope scheme 
 practical support . choir fisherman assemble 
 Penzance occasional purpose December 
 form permanent body voice 

 christmas - somg chorus , per- 
 form early December 

 Exeter Chamber Music Club ( initiate Dr. Ernest 
 Bullock ) start mew season vigorously large 
 influx new member , total 
 . annual meeting September 28 
 financial position find excellent . effect 
 Club existence increase support 
 chamber music venture district . instance , 
 audience October 11 attend 
 new series Philharmonic Concerts organize 
 Miss Mabel Bleby Mr. W. F. Crabb , large 
 Exeter concert kind . 
 Birmingham String ( Quartet ( Messrs. Percival Hodgson 
 Frank Venton , Misses Grace Burrows Joan 
 Willis ) artistic interpretation perfect ensemble 
 fine performance Borowski Quartet . 2 
 D , Herbert Howells ’ * Lady Audrey Suite ' ( new 
 West ) , Mozart G , ' Dohnanyi d flat , Beethoven 
 Op . 74 . Howells ’ Suite arouse great enthusiasm , 
 , fascinating beauty humour . Miss Mary 
 Hamlin , youthful vocalist , good gift 
 musician instinct 

 DUBLIN 

 secure support British National Opera 
 Company public meeting hold Glasgow 
 October 5 . aim object scheme 
 explain Mr. Robert Radford , Mr. Percy Pitt , Mr. 
 Walter Hlyde , advisory local committee 
 appoint . surprising Glasgow afford 
 support project offer great possibility 

 chief event month Orpheus week 
 chamber concert ( October 3 8) London String 
 ( Quartet , Miss Myra Hess solo pianist . chamber 
 music past support 
 , persistent effort Orpheus Choir 
 likely bear fruit . Choir 
 large following afford face financial risk — 
 deficit # 100 year — sign 
 want point concert self - support . 
 audience large , enthusiastic , appreciative 
 degree seldom similar event . programme 
 irreproachable , range composition 
 Haydn , Mozart , Beethoven , 
 ' modern ' present - day writer , include 
 performance Scotland Kreisler ( Quartet minor . 
 interpretation London Quartet 
 high level , Miss Myra Hess , play contrast 
 group pianoforte solo ( juintet 
 César Franck , Brahms , Elgar , greatly enhance 
 high reputation Glasgow audience 

 Madame Tetrazzini strong company provide 
 programme ' international celebrity ' concert 
 October 5 

 LIVERPOOL 

 Philharmonic Society season open brilliantly 
 October 11 programme judiciously blend old 
 new . M. Koussevitzky new life * Oberon ' 
 Overture Beethoven seventh Symphony , 
 impart suggestion russian glow glitter . 

 gramme , cover wide range 

 season concert . annual meeting } . - 
 M0 . EB . W. Kieedl eo pena president | include Bach , Mozart , Dvorak ; modern , Howells , 
 aa | Frank Bridge , Grainger 

 Mr. Alfred Agate vice - president hon . conductor , q . : 
 presentation silver Queen Anne teapot oi aagpeee ' — — — - se 
 Mr. T. A. Chignell , hon . secretary ae ae _ ia agpaniet . nage 
 Society year . ' Hiawatha Wedding - Feast ' attendance good music acceptable . 
 concert . Lady Fitzwygram October — Bae nag Theatre , _ Angle — — — 
 president Havant Society , able to| petforme Beethoven Symphony C minor , 
 retain service Mr. Mrs. Canaway hon , | 5¢Ptember 18 australian baritone , Mr. Harold Williams , 
 conductor accompanist respectively . Mr. R. Y. Fisher | # ppeare Park Hall support Mortimer 
 appoint hon . secretary . Society | y—_aguees f Rev. ES " : 
 practise Stanford ' revenge ' ' Songs Fleet ' | _ . rhe comtenaty os Mev . & . : tephen , know 
 opening concert December 14 | Tanymarian , hand . composer 
 : s 7 ee . oo . 9 rare 
 Borough Portsmouth Philharmonic Society | W elsh oratorio , Storm Tiberias . ' Welsh Congregational 
 concert season Town Hall . October 13 , | Union decide event celebrate 
 specially noteworthy . place mark | | denomination ( old os 
 Mr. Hugh A. Burry return leadership Society | minister ) . sue suggested programme include ven 
 3 2 | " > 7 » , 
 season absence ill - health , effective tune _ - epee Pannen ‘ agony er 
 control performer occasion reveal lack : ioe rey wit } t pay = t aap 
 quality bring Philharmonic | ag * - AL oye , Tlenidhe gp os poggel - 
 forefront local musical society , place , J. Mills Llanidloes , 

 practically , place . programme | regard pioneer music Wales , especially 

 entirely classical , occasion Mr. Burry } music ragerses 4 os 

 departure custom draw attention fine ensemble playing Birmingham String 
 audience work horn , bid player ' Juartet ( Mr. Percy Hodgson , leader ) Lecture - hall - 
 stand order doubt | ardiff \ -M.C.A. October 13 , audience 
 refer . separately phrase | high satisfaction . programme consist quartet 
 score , familiarise audience | Beethoven , Haydn , Dohnanyi . 

 > " tie , e 
 sound . prior play prelude act 3 | Opening concert series hold t 
 % | season 

 compose walk aged person sit 
 turn road . believe 
 appropriately define composition ' art 
 avoid stop . ' particular Bruckner 
 true splendid climax , 
 lead , feel ultimate 
 necessity . indication movement approach 
 termination Bruckner settle tonic 
 chon ! great number bar . majesty 

 mvincing power Beethoven . 5 , succeed 
 Bruckner work concert notice , tend 
 place shortcoming strong light . great 
 enthusiasm greete ! appearance Herr Carl Friedberg 

 work fall short 

 ee 

 , concert October 9 , splendidly sustain 
 solo Schumann Pianoforte Concerto , like 
 Mr. Albert Spalding , american violinist , ap 
 Amsterdam favourite pre - war time . Spalding gaye 
 recital , prove splendid 
 form , gratifve audience highly - finishea 
 execution Georges Enesco Violin Sonata . j 
 likelihood artist regain 
 popularity . series 
 chamber music concert Hollandsch 
 String Quartet , confront work new 
 u , viz . , Sonatine , - fledged _ . 
 movement string quartet , Pierre Menu , Franco 
 | Alfano String ( Quartet D major . work meet 
 |a cool reception , appearance feeble 
 success lay door performer . far 
 | hearing ( second hardly 
 | hope ) , composition prove 
 | addition chamber music literature . unfortunately 
 prove impossible procure score verify 
 | impression ; confident iam mistaken 
 belief . stagger succession 
 | dissonant harmony , come hearer gets 
 bewilder consonant chord surely 
 | wrong . , resemble real musical 
 | strain head moment general 
 | turmoil , tempt wonder earthly 
 reason composer continue snatch . 
 |above apply equally new work . Beethoven 
 | Op . . 2 , come comparative balm , 
 , likely owe exertion performer 
 sustain , performance find lack 
 respect . novelty 
 record , Franz Schreker Chamber Symphony , 
 time work composer 
 hear Holland . shall shortly afford 
 occasion fora second hearing , deem advisable reserve 
 opinion till month letter . Mahler ' Lied 
 von der Erde ' complete programme ( October 13 ) . 
 work , feel tighten grip 
 audience , hear time Madame Bauer von 
 Pilecka ( Vienna Staatsoper ) exponent 
 significant contralto . create favourable 
 impression , forget Madame Cahier 
 Madame Durigo previous performance . 
 hand , master - tenor Jacques Urlus good , 
 doubt performance possibly rival . 
 work , appeal strongly Mengelberg 
 | individuality , hear finished way . 
 orchestra live fame . 
 W. HARMANS 

 59 

 musically end summer dead . 
 exception nightly concert Belvedere Park 
 importance occur , thought 
 turn Theater Konzert Und Kino Messe , 
 hold September 4 25 . 
 Messe conjunction Wiener Inter- 
 nationale Messe , open September li 17 . 
 musical Messe divide , 
 principal comprise opera , concert , theatre , 
 cinema , voice - training , dramatic - training , music publication , 
 theatre lighting , & c. concert portion 
 interesting . principal event hold connection 
 follow : Bruckner D minor Symphony , 

 direction Hans Wagner ; Strauss ' Alpen ' Symphony , 
 direction Reiner ; concert Company 
 Musical Friends ; Philharmonic Orchestra , 
 direction Felix Weingartner ; massed orchestra 
 Vienna , direction Weingartner ; choir 
 State Opera , Mozart ' requiem ' ; mass 
 orchestra , direction Fred Lowe ; Mahler 
 Symphonies . 7 8 ; Schubert evening Duhan ; 
 Beethoven ninth Symphony 

 historical chamber concert , September 
 12 , 14 , 17 Ceremonial Hall Hofburg , 
 stand prominently . programme comprise 
 selection work Mozart , Bach , Nardini , Stamitz , 
 de Hervelois , Sacchini , Grétry , Hasse , Matheson , Handel , 
 Scarbeatti , Lach , Salmhofer , Mittler 

 Philmoniker , Tonkunstler , Volksoper orchestra , 
 direction Felix Weingartner . programme 
 follow : ' 
 Largo ... sink wit é Handel 
 Air os Bach 

 Beethoven 
 Wetngartnes 
 Wagner 
 Wagner 

 March * Ruins Athens ’ 
 symphonic Prelude , * King Lear ’ 
 Prelude , * Lohengrin ’ ... e 
 Overture , * Tannhauser 

