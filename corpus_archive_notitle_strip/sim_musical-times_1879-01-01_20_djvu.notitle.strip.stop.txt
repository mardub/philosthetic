MUSICAL TIMES 
 SINGING - CLASS CIRCULAR . 
 JANUARY 1 , 1879 

 BEETHOVEN TENTH SYMPHONY . 
 Lupwic Nou . 
 — TWIN - CHARACTER BEETHOVEN 
 SYMPHONIES 

 Tue fable “ fable ” Tenth Symphony 
 Beethoven , told Musikalisches Wochenblatt 
 1875 , found way immediate circle 
 readers journal . attempting , , 
 state real facts case ina connected form , 
 founded authentic sources , animated 
 merely desire expose ignorance 
 - biographical authorities , , 
 , conviction , purely his- 
 torical interest attaching , subject 
 greatest importance concerning artistic 
 esthetic development Beethoven , 
 artistic intellectual life general 

 intimation existence Tenth 
 Symphony Beethoven given Schindler , 
 - tried companion . time , 
 reduced condition absolute material want 
 wearisome illness , master , repeated 
 application , received sum pounds 
 sterling London Philharmonic Society ‘ 
 account forthcoming concert . ” letter 
 dated March 24 , 1827 , addressed Moscheles , 
 chief promoter concert question , 
 undertaken aid composer , Schindler writes : 
 ‘ days receipt letter 
 greatly excited , wanted 
 sketches Tenth Symphony , design 
 spoke good deal . 

 mind Philharmonic 

 Society . ” letter discovered 1864 Mann- 
 heim Schindler valuable Beethoven papers 
 ( Bohemia ) . published fol- 
 lowing year , subsequently given place 
 ‘ Musikalisches Skizzenbuch ” ( Munich , 1866 ; 
 p- 282 ) . place found passage 
 Beethoven letter Moscheles , intended ac- 
 company Schindler , runs : “ Tell 
 worthy men God 
 restored health endeavour realise 
 feeling gratitude works , 
 leave Society choice write 
 . entirely sketched - Symphony lies 
 desk ” ( Eine ganze skizzirte Symphonie liegt 
 meinem Pulte ) . words , forming 
 original manuscript dictated Beet- 
 hoven , crossed Schindler , 
 consequently found letter 
 actually sent . , substantial accu- 
 racy confirmed - quoted remark 
 Schindler , words “ entirely 
 sketched ” , , applied 
 literal sense . insert state- 
 ment Karl Holz , years 
 confidential friend companion table 
 composer , expressly declares 
 citing ‘ ‘ Beethoven words , ” viz . : ‘ shallin 
 future , manner grand - master Handel , 
 content writing oratorio con- 
 certo year — provided completed 
 Tenth Symphony ‘ Requiem . ’ ” conversa- 
 tion dates year 1826 , shall hear anon 
 Holz obtained actual know- 
 ledge work . quote 
 conversation , carried - known manner 
 deaf composer sick bed friend 

 youth , Gerhard von Breuning , January 
 February , 1827 , published ‘ ‘ Berliner Con- 
 versationshefte : ” ‘ * better pleased 
 - day before.—Are vocal parts 
 Symphony ? — entirely new idea ! ” , 
 fact , work conceived matured years pre- 
 viously , according decisive plan , 
 dealing , , , embodied ‘ 
 entirely new idea . ” ‘ suppressed passage 
 letter referred , add , 
 published 1865 ‘ ‘ Beethoven Briefe ” 
 ( Stuttgart : Cotta ; p. 341 ) . fact in- 
 tended Tenth Symphony gigantic Ninth — 
 appears - day keystone Beet- 
 hoven monumental creations , regarded 
 symphonies , 
 development hoped new 
 forms — considered historically established . 
 endeavour idea 
 symphony natural out- 
 growth composer strivings , forming , 
 fact , entire artistic development 

 publication sets , songs , motetts , can- 
 tatas , time - honoured custom doubtless 
 originated exigencies music - cultivating 
 public . find Philipp Emanuel Bach 
 writing symphonies stretch ; Ditters- 
 dorf produced 1786 Vienna , 
 occasion , ; 
 instance composer avowed object 
 “ illustrate Ovid ‘ Metamorphoses ’ 
 characteristic symphonies , ” , 
 hand , example Father Haydn , true originator 
 species , world - famed ‘ London 
 Symphonies ” form complete series . Mozart , 
 genius infused deeply poetic 
 psychological element hitherto comparatively 
 trifling Sonata form , highly susceptible 
 spiritual development , restricted 
 , especially symphonies , regards 
 mere numerical grouping , concentrating efforts 
 works . 
 universally admired Symphonies E flat , G 
 minor , C , written year 1788 
 space weeks , form , , 
 like quartetts , separate set , albeit set 
 stars forming brilliant constellation 

 similarly traditional practice , 
 undefined artistic want , led Beethoven 
 observe , symphonic creations , 
 grouping , soul 
 creative artist , whilst influence 
 inspirations genius , required equilibrium 
 powerfully stimulated activity successive 
 production works entirely dissimilar 
 character technical construction . 
 second Beethoven Symphonies 
 relationship traced ; 
 works grown , , depth 
 composer individuality , especially 
 merely confident trial skill existing groove . 
 , , “ Eroica ” produces 
 untraditional monumental art - form ; 
 work , known , owes origin 
 concrete historical suggestions , intended 
 memorial Napoleon I. Notwithstanding fact , 
 , justified looking sym- 
 phony followed ‘ “ ‘ Eroica ” forming 
 pendant . Born , sup- 
 plies ethos pathos supposed under- 
 lying poem . urged , matter 
 fact , Fourth Symphony completed 
 1806 , years production 
 “ Eroica ; ” shall period 
 placed origin fundamental germs , 

 

 MUSICAL TIMES.—Janvary 1 , 1879 

 mental disposition ideal contemplation 
 called existence ? 
 hand , certainty assumed 
 mind composer , touched innermost 
 strained unusual degree giant effort 
 produced great heroic poem , kept 
 state tension find relief 
 contemplation serene pictures Fourth 
 Symphony — result nume- 
 rous intermediate compositions different class 
 brought forward . Symphony 
 , symphonic poet par excellence , 
 Beethoven , conveys “ juice speedily 
 inebriates . ” writes year 1810 
 Bettina Brentano : “ ( symphony ) wine 
 inspires new creations , 
 Bacchus mortal men produces glorious 
 wine intoxicates minds . , 
 , sober , fished 
 carry away 
 dry land ; ” know 
 concentrated music , instrumental 
 , , symphonic , ( 
 master expression ) ‘ elicit sparks 
 soul 

 fact worthy notice principal 
 * ‘ motive ” C minor Symphony dates origin 
 time ‘ fate knocking 
 door , ” “ seize fate jaws ” 
 — period approaching deafness , 
 1798 - 1800 . “ motive ” referred , 
 melody Adagio symphony , 
 appear previously sketches 
 Quartetts ( Op . 18 ) dating end century . 
 symphony form , 
 , commenced 1805 , 
 time final elaboration Fourth longer 
 engaged attention , actual 
 completion year 1807 . Simultaneously 
 sixth - called ‘ Pastoral 
 Symphony ” grew existence ; 
 criginally intended fifth order , cir- 
 cumstance point anterior 
 conception work . , 
 certain completed long 
 actual elaboration C minor Symphony — , 
 year 1802 , period composer , 
 tortured illness , , dawning 
 consciousness incurably deaf , 
 subjected terrible inward struggles 
 revolutions , , time , 
 seek support outside . message conveyed 
 Pastoral Symphony regained peace , 
 comforter advent Beethoven , 
 hard summer 1802 , - nigh despaired , 
 thinking perish conflict him- 
 self , prevented laying hands 
 beloved art consciousness 
 high mission tofulfilinit . 
 days master penned touching document 
 , reference death - contemplating 
 period emanated , called 
 * Heiligenstadt Testament , ” concluding words 
 form “ ‘ programme ” Pastoral Sym- 
 phony . ‘ , O Providence , vouchsafe 
 pure day joy ; long , alas ! 
 stranger intense emotions true happiness . 
 , oh , , O Deity , temple nature 
 mankind , shall experience ? ” ex- 
 claims conclusion pathetic utterances . 
 painful emotion adds ‘ ? — , 
 cruel ! ’ gather 
 “ Reminiscences Rural Life , ” sym- 
 phony intended illustrate , Nature 
 merely dumb witness 

 anguish , ministering mother , soothing 
 comforter ; expressly defines 
 character work words , * rathér expres- 
 sion emotion mere painting . ” terrible 
 battle fate , , like Faust , ready 
 “ lay world ruins , ” fought , 
 found powerful representative Fifth Sym- 
 phony , completion Beethoven 
 contemplated composition Goethe im- 
 mortal drama . ( sufficient allude 
 scene “ Faust , ” headed “ forest cave , ” 
 commencing “ Erhabner Geist , du gabst mir , gabst mir 
 alles , warum ich bat , ” point striking ana- 
 logy prevailing state mind com- 
 poser . ) self - consuming combat , 
 consequent superhuman exultation , 
 expressed Finale , reaction claims , 
 composer finds individual self 
 contemplation infinity creation . , 
 ethos pathos , 
 repose powerful effort , slackening wings 
 heavenward flight , descent 
 serene enjoyment world , , contiguity 
 fellow - beings , individu- 
 ality reflected , “ mysterious wonders 
 breast ” reveal . 
 religious character imparted hymn - like 
 finale Pastoral Symphony natural 
 outcome sinking composer indivi- 
 duality universal existence 

 arrive Op . g2 93 , marked like 
 fifth ( Op . 67 ) sixth ( Op . 68 ) twin pair , 
 judge merely outward appearances . 
 seventh baptised ‘ ‘ 13 , 1812 , ” 
 eighth “ month October , 1812 , ” 
 hour birth offsprings 
 ; interval months counts 
 hour , conception maturing , 
 accident final elaboration 
 determined interval material exist- 
 ence . numerical order , , evidently 
 fixed , seeing original 
 sketch - book ( possession Russia ) 
 clearly indicated 

 

 embryonic idea retained Beethoven . 
 symphonies , course , ninth tenth , 
 completed following 
 year . commission new composition , pre- 
 sented Rochlitz declined master , 
 pending completion works , 
 setting music Goethe “ ‘ Faust ; ” shall 
 able trace anon inner relationship existing 
 , greatest tragic poem Germany , 
 Ninth Symphony , entering 
 history origin gigantic 
 twin pair Beethoven symphonic creations . 
 ( continued 

 LITERATURE NATIONAL MUSIC . 
 Cart ENGEL . 
 ( Continued page 657 . ) 
 INSTRUMENTS 

 7th ult . , Raff “ m Walde ” Symphony ( 
 prolific composer published ) 
 produced time Concerts . 
 Continent work generally considered Raff master- 
 piece , opinions country probably 
 effect . Symphony composer 
 characteristic works , showing strong weak 
 points . named flow 

 melody — , admitted , highly 
 original kind — perfect mastery form , rich 
 diversified instrumentation . weak points work 
 occasional tendency vulgarity , great 
 prolixity . finale Symphony ( score 
 occupies 137 pages ) especially noticeable . 
 movement , long , rich 
 beauties impression diffuseness felt . 
 slow movement scherzo excellent . 
 impression produced work , aided 
 splendid performance , favourable ; 
 Symphony probably , deserves , heard . 
 afternoon Mdlle . Janotha , pupil Madame 
 Schumann , appearance Crystal 
 Palace . played Beethoven Concerto G , 
 Madame Schumann cadenzas , manner called 
 forth heartiest admiration , ofthe public 
 numerous connoisseurs present . young pianist 
 possesses excellent technique , real musical 
 feeling ; satisfactory rendering Beethoven 
 work desired . vocalists 
 Concert Madame Sherrington Mr. Bridson ; 
 , heard Sydenham , pro- 
 duced favourable impression 

 Concert Christmas given 
 14th , Beethoven Choral Symphony brought 
 forward , soloists Miss Emma Thursby , Miss 
 Redeker , Messrs. E. Lloyd Santley . needless 
 speak - known work ; add 
 programme included Benedict clever Overture 
 ‘ * Das Kathchen von Heilbronn , ” “ ‘ Dance Nymphs 
 Reapers , ” Sullivan ‘ ‘ Tempest , ” vocal 
 pieces Ambroise Thomas , Handel , Henschel 

 MONDAY POPULAR CONCERTS 

 Ow1nc customary adjournment Christmas 
 festivities , Concerts held 
 month , comprising evenings 2nd , gth , 
 16th ult . , posts pianist leading violinist 
 occasion admirably filled ladies , viz . , 
 Mdlle . Janotha Madame Norman - Néruda . pro- 
 gramme Concert referred presented novelty 
 production , time institution , 
 Spohr Quartett major ( Op . 93 ) stringed instru- 
 ments , received excellent interpretation 
 lady violinist mentioned , Messrs. Ries , 
 Zerbini , Piatti . Quartett belongs class , 
 composition Spohr inducement cul- 
 tivate , fact styled “ virtuoso - quar- 
 tett , ” principal violin merely leads 
 simply domineers companions . appa- 
 rent object composer ( consummate master 
 instrument ) , afford opportunity special 
 display virtuoso qualities similar compositions , 
 somewhat disturbs unity classical art - form 
 demands ; , despite manifold beauties , 
 revival occasional . Mdlle . Janotha 
 initiated favour audience 
 effective performance Beethoven ‘ “ Thirty - varia- 
 tions ” theme C minor , gifted 
 lady opportunity prove versatility 
 talent grace brilliancy execu- 
 tive skill . recalled twice , concert- 
 frequenters signifies encore , Mdlle . Janotha added 
 Chopin Mazurka E minor solo performances 
 evening . Brahms Quartett major ( Op . 26 ) con- 
 cluded programme . Mdlle . Redeker gave 
 usual refined style arias Giordani Stradella , 
 songs Henschel Jensen , Mr. Zerbini playing 
 pianoforte accompaniments 

 second Concert notice opened Beet- 
 hoven incomparable Quintett C major ( Op . 29 ) , 
 second work kind owe 
 genius composer ; mere mention 
 suggest musical amateur world revealed beauty , 
 tender passion , fantastic humour , like painting 
 Titian art - student , Shakespearean drama 
 literary enthusiast . masterpieces emanating 
 “ sister arts ” named universally recog- 
 nised uninitiated , help regretting 

 albeit midst crowded appreciative audience 
 — musical art country remain 
 somewhat subordinate position ; great majo- 
 rity educated title masterpiece 
 inspired genius like Quintett C major fail 
 convey meaning . Quintett worthily 
 rendered Madame Norman - Néruda , Messrs. Ries , Zer- 
 bini , Schrzeurs , Piatti , having - fourth 
 occasion performance Concerts . Médlle . 
 Janotha , solo performance Chopin Scherzo B 
 minor , proved artist considerable attain- 
 ments , technically intellectually ; spirited 
 execution work Polish tone - poet ( 
 Scherzos terribly earnest ) having deservedly 
 encored . lady - pianist associated 
 Madame Norman - Néruda Signor Piatti Schumann 
 Trio D minor , execution , entrusted 
 excellent artists , left desired . Miss 
 de Fonblanque , vocalist occasion , con- 
 tributed materially enjoyment evening 
 singing air , ‘ * Lento il pié , ” Mozart , Bennett 
 ‘ * - dew , ” pieces cultivated style 
 fine mezzo - soprano voice showed great advantage . 
 Concert concluded second performance 
 Popular Concerts Bach Sonata E major piano- 
 forte violin , brilliant technique Mdlle . 
 Janotha Madame Norman - Néruda combined produce 
 excellent rendering work real beauties 
 fully appreciated careful individual study 

 Concert month Haydn beautiful 
 Quartett C major ( Op . 20 ) opened performance , 
 rendered Madame Norman - Néruda , Messrs. Ries , 
 Zerbini , Piatti perfect finish precision 
 artists long accustomed . 
 Mdlle . Janotha Signor Piatti gave admirable reading 
 Rubinstein Sonata D major ( Op . 18 ) pianoforte 
 violoncello , lady responding persistent encore 
 playing valse Leschitizki , brilliant 
 executive powers displayed best advantage . 
 instrumental solo performance announced 
 programme Adagio Spohr Seventh 
 Violin Concerto , pianoforte arrangement 
 orchestral score , given Madame Norman- 
 Néruda true refinement complete 
 mastery capabilities instrument 
 lady rivals . Concert concluded 
 Beethoven Trio E flat ( Op . 70 ) pianoforte , violin , 
 violoncello , performance Mesdames 
 Janotha , Norman - Néruda , Signor Piatti took . 
 Miss Clara Merivale vocalist , contributed 
 air Handel song Gounod numbers 
 altogether interesting programme . Mr. Zerbini 
 Conductor . Concerts resumed 
 6th inst 

 MADAME VIARD - LOUIS ’ CONCERTS 

 chapter “ Symphony Germany ” 
 valuable book . Commencing Haydn , 
 M. Lavoix justly remarks , com- 
 poser invented instrumental colouring ; 
 gave modern orchestra present constitu- 
 tion , discarding instruments 
 time use , modern system 
 instrumentation encumbrance 
 advantage . Mozart orchestration studied 
 grand operas symphonies ; M. 
 Lavoix quotes Kéchel curious specimens 
 instrumental combination . Mozart 
 fully appreciated resources 
 clarionet , treatment instrument 
 subsequently surpassed Weber 

 Beethoven author says 
 individualize instrument orchestra . note , 
 read , inaccuracy statement 
 Symphony ( . 7 ) contains parts trombones 
 ( p. 296 ) . French musicians general ( far 
 experience goes ) apt underrate Mendelssohn , 
 glad find enthusiastic apprecia- 
 tion orchestration given pages 307 , 308 , 
 , space allow , willingly quote . 
 surprised find Schubert altogether 
 omitted . instrumentation characteristic 
 individual , infer M. Lavoix 
 know Symphonies B minor C major , 
 ‘ * Rosamunde ” ’ music , Mass E flat . author 
 strictures Schumann orchestration , 
 characterises heavy dull , adding , 
 great truth , “ ‘ justice rendered 
 talent Schumann , inspired poet , bold 
 happy harmonist , certainly colourist , 
 rank great composers modern 
 school 

 notice , , truly fascinating volume 
 extended length pass 
 hastily chapters remain noticed . 
 excellent analysis orchestration operas 
 Gluck Mozart followed remarks 
 influence composers modern music . 
 sketch progress orchestra France 
 Rameau Rossini . list composers 
 works noticed detail chapter 
 help readers form slight idea 
 labour involved preparation present volume . 
 M. Lavoix speaks ( evidently personal knowledge ) 
 scores Grétry , Monsigny , Dalayrac , Nicolo , Philidor , 
 Gossec , Salieri , Sacchini , Cherubini , Méhul , Lesueur , 
 Catel , Spontini , Berton , Boieldieu . quote 
 point . mentions ( p. 342 ) Spontini 
 introduce modern system orchestrating 
 large masses , doubt results greater sonority , 
 long run inevitable monotony . follow- 
 ing chapter , Italian orchestra Rossini , chiefly 

 MUSICAL TIMES.—January 1 , 1879 

 proves Fow little skill instrumentation larger 
 number Italian composers possessed . simple ex- 
 planation found fact looked 
 orchestra instrument 
 accom anying voice , relegated 
 subordinate position . interesting analysis 
 Beethoven ‘ ‘ Fidelio ” operas Weber , remarks 
 colour romanticism dramatic music , reach 
 elaborate chapters work — 
 devoted Rossini Meyerbeer . occupies forty- 
 pages , details orchestration 
 ‘ * Guillaume Tell , ” grand operas Meyerbeer , 
 value student . chapter modern 
 dramatic orchestra France ( Auber , Hérold , Halévy , 
 Félicien David ) succeeds ; remark 
 M. Lavoix hardly appears fully appreciate 
 exquisitely polished piquant instrumenta- 
 tion Auber . works Berlioz examined 
 detail , justice rendered original 
 thinkers , undoubtedly greatest colourists 
 modern times ; final chapter contempo- 
 rary instrumentation , careful unprejudiced examina- 
 tion Wagner scores found . Frenchmen 
 entertain bitter hatred composer ‘ * Tann- 
 hauser ” ’ pleasant read M. Lavoix opinion 
 . words ( p. 456 

 musican powerful passion , possessing highest 
 degree science effects harmony instrumentation , endowed , 
 , remarkable richness melody , Richard 
 Wagner incontestably musician age 

 experience M. Massé ‘ Paul et Virginie , ” 
 appear superfluous ask attention work 
 pen . composer 
 marred single effort . true “ Paul et 
 Virginie , ” deficient striking numbers , 
 radical defects , little chance obtaining 
 permanent place lyric stage . 
 reason M. Massé productions in- 
 cluded wholesale condemnation . 
 incline patient impartial examination 
 claims , hope finding able redeem 
 credit composer , prove measure 
 reputation enjoys rests basis fact . 
 preamble , , turn M. Massé setting 
 libretto founded Lamartine touching romance 

 Readers French literature remember ‘ Gra- 
 ziella , ” ” recognise materials 
 charming pastoral opera strongly dramatic situa- 
 tions , actuated purest feeling . True story 
 , elements , old — old woman unselfish 
 devotion man loves . presents 
 ' * * Fidelio , ” illustrates strongly Beethoven 
 Opera justice high Heaven , guards innocent 
 punishes guilty . Massé 
 work familiar , scenes , incidents , details 
 fresh charming welcome old friend 
 new attractive face . beginning 
 Opera delightful rustic sweetness joy . 
 overture reproduces significant 
 themes body work , including Saltarello , 
 cottage Fior d ’ Aliza dwells 

 parents , Antonio Magdalena , shadow 
 huge favourite chestnut - tree . , maiden 
 singing gleefully ; , Geronimo , young 
 peasant associated footing 
 brother , answers kindred strains . follows 
 simple air Fior d ’ Aliza , ‘ ‘ O mon doux ami , ” based 
 entirely tonic dominant pedal , distinctive 
 musette accompaniment . young people meet , 
 important duet ensues matter course . 
 speak mutual happiness unaffected strains , 
 till length Geronimo ventures tell companion 
 glad . Andantino cantabile , 
 ‘ * Sais tu pourquoi tout rayonne en ton 4me ? ” marked 
 considerable grace fervency expression 
 melodic beauty . ‘ love , ” says young man , “ 
 lights thy soul ; ’’ naively answers , ‘ Love , 
 sayest thou ? ” wholly comprehending purport 
 remark . , need scarcely pointed 
 , anticipation ‘ Paul et Virginie . ” 
 duet comes air Hilario , good monk 
 collects alms poor neighbouring monastery . 
 worthy brother sings ‘ ‘ house ’ duties 
 considerable unction appropriate sedateness . 
 good terms ; 
 happy state things music , easy flow , 
 reflects . Arrived house Antonio , monk 
 bestows like blessing venerable 
 chestnut - tree , commencing quintett inmates 
 cottage , loth join agreeable 
 atheme . ‘ ‘ O vieux chataignier ! arbre centenaire , ” ’ exclaims 
 Hilario , answering , “ Puisses - tu , respecté de la 
 hache et du temps , reverdir encore dans cent ans ? ” 
 quintett short unpretending , harmony 
 occasion subject . far Opera idyll 
 redolent flowers Eden , finale Act I. , 
 nowreach , genius evil makeshis appearance . 
 woodmen enter , armed axes , announce 
 errand cut tree . purpose 
 state brusque rhythmic - chorus , 
 close Antonio pleads favourite true 
 spirit - known English song , wife invokes 
 Madonna . woodmen 
 orders , simply repeat chorus . Geronimo 
 speaks effect worse man 
 lifts axe venerable trunk . crisis 
 diversion occurs . Fior d ’ Aliza , wandered away , 
 shrieks help ; Geronimo runs direction 
 voice , shot heard ; presently young fellow re- 
 turns , avowing killed man dared 
 offer insult ‘ sister . ” victim captain 
 gendarmes , love rejected Fior d ’ Aliza , 
 revenge purchased cottage Antonio 
 , landlord , rightfully cut tree . 
 Geronimo act excites greatest alarm 
 friends , reason . gendarmes appear 
 scene ; act closes long ensemble express- 
 ing wrath officers loss leader , 
 dismay onlookers , anguish culprit 
 parents , mutual devotion lovers . 
 concerted piece M. Massé takes care 
 depth . music complicated , aim 
 original effects , time fairly expresses 
 conflicting emotions scene , brings 
 curtain éclat 

 Second Set Voluntaries . Arranged 
 Harmonium ; J. W. Elliott . [ Novello , Ewer Co 

 selection , arrangement , 
 Voluntaries ensure extensive sale 
 amateurs instrument 
 especially designed . Mr. Elliott legiti- 
 mately earned right respected 
 authority subject Harmonium music 
 performers safely trust guidance ; 
 writers drawn collection include 
 names composers Beethoven , Mendelssohn , 
 Mozart , Handel , Sir Sterndale Bennett , Sir John Goss , 
 Joseph Barnby , G. A. Macfarren , Sir Julius Benedict , 
 Arthur Sullivan , Berthold Tours , Charles Gounod , Dr. 
 Stainer , equally high reputation , book 
 conscientiously recommended desire 
 possess variety short good Voluntaries 
 shall severely tax performer 

 Baby Bouquet . Fresh Bunch Old Rhymes 
 Tunes . Arranged decorated Walter Crane . 
 Cut printed colours Edmund Evans 

 subjoin , usual , programmes concerts re- 
 cently given leading institutions abroad 

 Paris.—First Concert du Conservatoire ( December 1 ): 
 Symphony , D ( Beethoven ) ; Fragments ‘ Elijah ” ’ 
 ( Mendelssohn ) ; Andante Intermezzo Symphony 
 minor ( A. Holmes ) ; Fragments ‘ * Seasons 

 Haydn ) ; Overture , “ ‘ Le Carnaval romain ” ( Berlioz ) . 
 Concert Populaire ( December 1 ): Symphony , ‘ Eroica ” 
 ( Beethoven ) ; ‘ ‘ Sadko , ” Symphonic Poem ( Rimsky - Korsa- 
 koff ) ; Symphony , E flat ( Mozart ) ; Andante con varia- 
 zioni , Op . 47 ( Beethoven ) ; Scherzo pianoforte 
 orchestra ( H. Litolff ) ; Invitation 4 la valse ( Weber ) . 
 Chatelet Concert ( December 1 ): Oratorio , ‘ Paradis 
 Perdu ” ( Dubois ) . Concert Populaire ( December 8) : 
 Symphony , F ( Beethoven ) ; Ave Maria ( Righini ) ; 
 Fragments ‘ Manfred ” ( Schumann ) ; Andante 
 ‘ “ * Kreutzer - Sonata ” ( Beethoven ) ; Minuet ‘ L’Arlé- 
 sienne ” ( Bizet ) ) ; Overture , ‘ “ ‘ Sigurd ” ( Reyer ) . Festival 
 Concert Hippodrome ( December 17 ): Overture , 
 “ Oberon ” ( Weber ) ; Marche Hongroise ( Berlioz ) ; 
 Prayer ‘ ‘ Moise ” ( Rossini ) ; Marche religieuse 
 “ Gallia ” ( Gounod ) ; March ‘ Orient et Occident ’’ 
 Carnaval ‘ ‘ Timbre d’Argent ” ( Saint - Saéns ) ; 
 Fragments ‘ ‘ Le Roi de Lahore ” ( Massenet ) . Con- 
 cert du Conservatoire ( December 15 ): Symphony , E 
 flat ( Schumann ) ; Paternoster , unaccompanied chorus 
 ( Meyerbeer ) ; Andante Finale Violin Concerto 
 ( Taudou ) ; Choruses ‘ ‘ Oberon ” ( Weber ) ; Overture , 
 ‘ * Leonore ” ’ ( Beethoven ) . Concert - Populaire ( Decem- 
 ber 15 ): Symphony , B minor ( Haydn ) ; ‘ “ * La Jeunesse 
 d’Hercule , ” ” Symphonic Poem ( Saint - Saéns ) ; Andante 
 Tarantella Violin ( Lancien ) ; Fragments “ * Roméo 
 et Juliette ’ ? ( Berlioz ) ; Fragments ‘ Prometheus ’ 
 ( Beethoven ) ; Overture , ‘ ‘ Euryanthe ” ( Weber 

 Leipzig.—Conservatorium Concert ( November 21 ) : 
 Quartett , D major ( Haydn ) ; Hommage Handel ( Mo- 
 scheles ) ; Preludes Fugues ( Bach ) ; Pianoforte 
 Solos ( Beethoven , Chopin , Kirchner ) . Euterpe Concert 
 ( November 27 ): Overture , ‘ ‘ Medea ” ? ( Cherubini ) ; Con- 
 certo , D minor ( Rubinstein ) ; Symphony , D major 
 ( Brahms ) ; Pianoforte pieces ( Haydn , Raff ) . Con- 
 servatorium Concert ( November 29 ): Fugue ( Bach- 
 Liszt ) ; Duet Violin Viola ( Spohr ) ; Sonata 
 Pianoforte ( Richter ) ; Violin Solos ( Raff , Wieniawski ) . 
 Euterpe Concert ( December 3 ): Overture , ‘ ‘ Faust ” 
 ( Wagner ) ; Scherzo Orchestra ( Goldmark ) ; Cantata , 
 ‘ “ ‘ Frithjof ’ ” ’ ( Bruch ) . Gewandhaus Concert ( Decem- 
 ber 8) : Memoriam ( Reinecke ) ; Violin Concerto 
 ( Mendelssohn ) ; Serenade ( Holstein ) ; Violin Fantasia 
 ( Lalo ) ; Overture , ‘ ‘ Ruy Blas ” ( Mendelssohn ) . Gewand- 
 haus Concert ( December 12 ): \Symphony , C minor 
 ( Haydn ) ; Ballet Music ‘ ‘ Feramors ’ ” ’ ( Rubinstein ) ; 
 Violoncello Concerto ( Saint - Saéns ) ; Overture , ‘ “ Ruy 
 Blas ” ( Mendelssohn 

 Berlin . — Concert Stern’sche Verein ( Novem- 
 ber 29 ): Overture , Op . 124 ( Beethoven ) ; Violin Concerto 
 ( Bruch ) ; Entr’acte ‘ ‘ Manfred ” ( Reinecke ) ; Ave 
 verum ( Mozart ) ; Violin Fantasia ( Lalo ) . Bilse Concert 
 ( November 13 ): Overture , ‘ ‘ Coriolanus ’’ ( Beethoven ) ; 
 Rhapsody ( Liszt ) ; Symphony , D minor ( Raff ) ; Gavotte 
 ( Bazzini ) ; Overture , “ * Rienzi ” ( Wagner 

 CORRESPONDENCE 

 Laurels , Gipsy Hill 

 Beethoven pupil said , in- 
 correctly.—H. H. S 

 QUADRUPLE CHANT . 
 EDITOR ‘ ‘ MUSICAL TIMES 

