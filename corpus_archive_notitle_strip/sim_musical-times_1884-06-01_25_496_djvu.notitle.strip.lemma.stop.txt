familiarity Oratorio natural effect 
 lead Costa try hand form 
 composition ; inducement great 
 certainty produce work 
 good possible condition . accordingly ' Eli ' 
 bring 1855 * * Naaman " 1864 " — 
 Birmingham , amid enthusiastic 
 applause . work frequently perform 
 England composer life - time ; Costa 
 ready preside execu- 
 tion . destiny ultimately await 
 idle speculate . 
 equally reason deny Oratorios 
 contain good deal - design , melodious , 
 effective music , write , alike voice 
 orchestra , consummate skill , want- 
 e measure indefinable 
 quality know genius . certain 
 look vain successor Mendelssohn 
 Oratorio nearly approach 
 standard " Elijah " " Eli . " fact 
 honour means small 

 Costa certain reach posterity 
 quality conductor , let record 
 strong pcint fame . 
 speak palace truth , hold 
 admiration immaculate . wasa 
 musician limited sympathy , work 
 dislike successful , 
 honestly try . true italian 
 ardour love hate , ardour 
 frequently overcome prompting judgment . 
 music country Costa passionately 
 worship . happy 
 conduct ' ! Barbiere " ' * Semiramide , " 
 italian operatic music prefer , 
 matter country , present broad 
 strong , sensational , effect . 
 pleasure Handel reason ; 
 german composer later period Beet- 
 hoven , Meyerbeer favourite ; 
 melodic grace perfect form Mendelssohn 
 charm . Beethoven , hand , 
 find Costa " heart heart . " 
 respect great master , 
 understand sufficiently term 
 confidence , alter 
 vocal Mass D , tender 
 performance symphony 
 little perfunctory . Bach musical 
 language ' unknown tongue " 
 Costa . conduct Matthew ' " Passion " 
 , , present 
 forget , note , satiety 
 destroy appetite , result , far 
 concern Sacred Harmonic Society , fully 
 attain . , deceased conductor 
 permit score 
 master alteration in- 
 defensible . idea thing 
 , resolute , imperious nature 
 carry great length cautious 
 musician possibly approve . men- 
 tione matter necessity , silence 
 regard convey false impre 

 whira 

 country conspicuous record 
 civilization find intellectual life| 
 chequer alternate sunshine shade , like a| 
 forest path . splendour , man look | 
 mingled envy admiration ; anon 

 obscurity — glory depart blaze elsewhere.| land Haydn , Mozart , Beethoven , 

 " island story " supplic illustration . Mark | 
 augustan age Elizabeth follow hard | 
 dark terrible period witness | 
 old order society change place new . | 
 ' * , , " Mr. Froude , magnificent | 
 passage , ' " ' change come world , | 
 meaning direction hide 
 — change era era . path 
 tread footstep age break ; old 
 thing pass away , faith life | 
 century dissolve like dream . | 
 chivalry die ; abbey castle | 
 soon crumble ruin ; ) 
 form , desire , belief , conviction old world | 
 pass away return ... floor | 
 heaven , inlay star , sink an| 
 infinite abyss immeasurable space ; firm | 
 earth , unfixed foundation , 
 small atom awful vastness 
 universe . " i’rom ruin 
 cataclysm spring glorious flower elizabethan 
 literature poetry — aye , music ; 
 country , musical good , 
 good , challenge Italy 
 Netherlands , France creep 
 light , Germany remain darkness 
 heathendom . - - splendour 
 wane . come time gloom , relieve 
 bright particular star Milton genius , 
 turn way second augustan age 
 Anne . unhappily music revive . 
 delicate sensitive art smother 
 frippery furbelow Restoration . 
 Purcell die soon music fame 

 ine 
 n 

 easily extend article nearly double 
 actual length , recourse ' padding . " 
 trust , , amply establish 
 proposition lay commence — Han- 
 del score mean want variety 
 colour , find foreshadowing 
 nearly modern effect . course , 
 maintain work Handel stand 
 level , regard instrumentation , 
 Mozart Beethoven ; instrumentation , 
 understand , modern branch art ; 
 Handel 
 prophetic insight future possibility 
 orchestra ; score 
 indicate , dimly , direction 
 subsequent development . believe 
 prove , absolute demonstration , impossibility 
 reproduce Handel work exactly 
 ; necessity modifica- 
 tion admit , question additional 
 accompaniment resolve 
 ' * " " . " propose 
 enter present ; future occasion , possibly , 
 attempt analysis chief score 
 Handel publish additional 
 accompaniment — viz . , Mozart , 
 recently issue Robert i’ranz , 
 suggest important question art , 
 worthy discussion . inconclude present 
 series paper , add reading 
 prove half interesting musician 
 write shall amply 
 reward labour spend preparation 

 GREAT COMPOSERS . 
 JosernH BENNETT . 
 . XIV.—MEYERBEER ( conclude page 253 

 MUSICAL TIMES.—June 1 , 1884 

 german Emperor Joseph II . , die 1790 , 
 accession throne 
 successor Leopold II . , reign 1700 1792 . 
 believe pure conjecture , Mr. 
 Thayer , great authority event 
 Beethoven life , mention existence 
 Cantata compose Beethoven time , 
 merely hazard guess theme death 
 german Emperor , Dr. Wegeler , 
 allude Cantata young composer , 
 mention subject . Brahms , , 
 assert composition 
 intrinsic worth stamp mind 
 work Beethoven | 
 appear title - page , remarkable | 
 way merit ; | 
 future time pronounce | 
 forgery , ought respect affect enjoy- | 
 ment , depreciate | 
 commercial value 

 

 piece create unsatisfactory impression 
 different hang . Brahms 
 ‘ ' song fate ' far likely 
 favourite country . poetical subject unpleas- 
 e , music gloomy hue . 
 present study sombre harmony , clever 
 effective way , want essential element 
 charm . end art gratify sense stimu- 
 late imagination pleasant exercise . case , 
 art Brahms . , , suit 
 pessimist , , apparently , desirous turn 
 enjoyment suffering . different impression 
 altogether Mr. Mackenzie masterly orches- 
 tral Ballad ' " ' La Belle Dame sans Merci . " " composer 
 good taste good sense 
 reconcile claim musical form requirement 
 " poetic basis , " Sterndale Bennett 
 overture " ' Paradise Peri . " result 

 piece hear pure apply music , 
 accord listener inclination , case 
 absolute pleasure . know orchestral movement 
 modern date satisfactory Mr. Mackenzie 
 Ballad . grow interest familiar . 
 Schumann " Rhenish ’' Symphony , long - know 
 Crystal Palace , form second the| 
 Concert , ably play . | 
 programme 12th ult . obviously intend 
 appeal wide circle amateur . contain a| 
 good deal vocal music , draw Handel ( ' ' love in| 
 eye ’' ) hand Wagner ( ' * Probe - Lieder , | 
 & c. ’' ) ; selection safe | 
 hand Mr. Lloyd . contain ' ' Egmont " 
 overture Beethoven ; overture " Oberon " ona | 
 Wagner " Siegfried Idyll ' — ' " ' thing 
 know . — need detain — 
 present , time England , new 
 Symphony ( . 3 ) Brahms . , course , lie 
 attraction connoisseur , élite musical London 
 care present important premicre . 
 undertake analysis work 
 hearing , guide 
 arrangement pianoforte . 
 publication score , 
 duty pleasure examine work length 
 column devote purpose . present | 
 offer impression , knowledge | 
 experience cause modify . advise , | 
 place opening Allegro good thing Brahms | 
 . theme , strike , happily 
 contrast , work accord art , 
 interesting manner . , , absence 
 mere verbiage disfigure modern music . 
 " argument " ' concise , complete — car- 
 dinal virtue . style movement , word 
 noble describe . composer come 
 lofty height proper symphonist . | 
 Andante value . theme , repeat " song | 
 form " adopt Haydn , fall ear ' " like 
 tale tell , ' ingenuity accom- | 
 plished orchestral writer sustain connoisseur interest 

 public generally movement , despite | s 

 rest W agner , " Nibelung 
 ring , " gigantic work following selection 
 : Siegfried ’ s " Gang zu Brinnhilde ’ s Fels , " * Tages . 
 grauen , " Siegfried ’ s ' * Rheinfahrt , " * " * Trauer- Marsch , " 
 " Der Ritt der Walkiiren . " fragment , 
 suffer separation drama , hear 
 great interest , applaud , — 
 " Walkiren - Ritt’’—encored . perform 
 need tell , respon- 
 sible good result 

 programme Monday Weber Over- 
 ture " Ruler spirit , " Beethoven Violin Concerto 
 ( solo Herr Heermann ) , wagnerian selection , 
 Brahms new Symphony , repeat " general desire 

 BACH CHOIR 

 variety afford singing Mrs. Hutchinson 

 Signor Foli , playing Madame Essipoff . 
 pianist free rendering Beethoven 
 Sonata Pathetique , hear advantage 
 minor item Mendelssohn , Schubert , Chopin , 
 composer . Mrs. Hutchinson vocal selection 
 soe commendation render . 
 include Purcell ' ' nymph Shepherds , " " Sol- 
 vejg lie , " Grieg . Choir appear somewhat 
 lessened number , music 
 render Mr. Randegger direction 

 Concert , Saturday afternoon , 17th ult . , 
 include item mark " time . " 
 order performance - song , ' ' daybreak , " 
 Mr. A. R , Gaul , Longfellow familiar line 

 instrumental pupil appear , amateur orchestra 

 choir connect Institution lend valuable 
 assistance . important item programme 
 Mendelssohn " Lauda Sion " Beethoven 
 ' * Pastoral Symphony , " work render 
 manner unqualified satisfaction 
 crowded audience . effort individual performer 
 questionable taste comment detail , 
 congratulate Mr. Weist Hill high average 
 efficiency student , Corporation 
 extraordinary success attend effort 
 cause music . step provide 
 suitable building School , present accommoda- 
 tion grossly inadequate subject 

 justifiable caricature comic journal 

 MUSICAL TIMES.—Jvne 1 , 1884 

 ought impress individuality music 
 ‘ interpret . decade elapse 
 remarkable executant visit country , 
 tendency criticism 
 liberal . generally recognise great work 
 admit varied reading , long player 
 wilfully distort composer meaning text , 
 individuality interpretation preferable dulness 
 mediocrity . recent performance Herr von Bilow 
 , , excited little acrimony , , 
 truth , public regard phenomenon . 
 recital largely attend , evidently 
 general interest awaken M. de Pachmann 
 month ago . programme form 
 considerable extent music outside ordinary répertoire 
 pianist , , , attractive public 
 musician . Recital , April 29 , 
 important item Brahms sonata ’ minor 
 ( Op . 5 ) , early fine work , design large 
 scale individual touch . Scherzo 
 audience 
 somewhat unwisely respond . remarkable 
 demand repetition Raff fugue e minor 
 ( suite , Op . 72 ) , piece play 
 exquisite finish clearness approval 
 audience true spirit discrimination . 
 afternoon Doctor admirable 
 form , piece Raff Rubinstein 
 great power intellectuality style . 
 programme include Beethoven rarely hear Varia- 
 tion russian dance , Rondo Capriccio G , 
 bagatelle b minor ( op . 126 ) , term Bourrée , 
 authority know . second 
 Recital , 6th ult . , Raff , Brahms , Beethoven 
 represent , 
 Rubinstein . composer Toccata ( op . 12 ) , 
 dedicate pianist , encore , 
 Rheirberger piece left hand , equal 
 effect . Beethoven sonata ( Op . 110 111 ) , Dr. 
 Hans von Bilow special quality display 
 good . work require broad masculine style 
 playing , time admit variety treatment 
 matter detail . rendering Sonata C 
 minor particularly admire , vigour display 
 movement contrast 
 beauty delicacy wonderful Arietta , 
 worthily conclude Beethoven pianoforte composition . 
 Raff Suite d minor ( Op . 91 ) contain 
 original effective , , , prefer 
 early Suite e minor . minor piece Brahms 
 complete scheme fault 
 extreme length 

 recital place 15th ult . 
 unable worthy climax 
 series , Herr von Bilow fail satisfaction 
 item programme , devoid 
 interest music . 
 Liszt - legend , ' St. Francis Assisi preach- 
 e bird , ' " St. Francis Paula wave , " 
 pianist fond play , unen- 
 lightened vision appear consist common- 
 place theme surround meaningless trill filagree 
 passage high note instrun nent , " 
 succession chromatic progression 
 melody , sense , form . bathos , Sterndale 

 improve player exaggerated expression 

 - indulgence rubato style . , Beethoven 
 sonata e flat , ' ' Les Adieux , ’' Dr. von Biillow - exert 
 , tone produce consequently hard 
 unpleasant . good performance afternoon 
 Brahms fine rhapsody b minor G minor 
 ( Op . 79 ) . Recital bring close Brahms 
 variation theme Haydn , familiar | 
 musician orchestral piece . keyboard arrange- 
 ment pianoforte , Mr. Oscar Beringer 
 able assistant occasion . recent performance 
 Dr. Hans von Biilow prove great , 
 unequal , pianist . trust 

 away impression country 

 MADAME ESSIPOFF recital 

 Irv curious indisputable fact difference 
 sex cause general distinction quality 
 characterise respectively male female pianist . 
 example , M. Viadimir de Pachmann technique 
 feminine masculine attribute , converse 
 concern Madame Essipoff . 
 hear advantage music require tenderiess 
 delicate touch ; work demand vigour 
 exercise physical power . recital 
 Madame Essipoff St. James Hall 
 past month . , Friday goth ult . , rely 
 entirely , play selection com- 
 poser . important work , 
 length classic dignity , Beethoven Sonata Appas- 
 sionata , oddly . 57 instead Op . 57 . 
 interpret great manipulative strength , 
 undeviating clearness phrasing . 
 piece Chopin , Schumann , 
 Mendelssohn , Handel , Rubinstein , Silas include 
 programme . arrangement Schubert 
 waltz Liszt omit , 
 indicate young pianist avoid . trifling 
 piece Edward Schiitt , promising young russian com- 
 poser , complete scheme . second recital , Wed- 
 nesday , 21st ult . , commence Schumann Sonata 
 inG minor ( Op . 22 ) .. sorry unable endorse 
 favourable opinion Madame Essipoft perform- 
 ance work elicit . ' 
 absence poetical feeling , 
 blur general style coarse un- 
 finished , plenty vigour . piece 
 small calibre admirably play , notably theme 
 Rameau , variation , Liszt Spinnerlied , 
 Leschetizsky Valse Chromatique . Madame Essipoff 
 assist M. Brandoukoff , violoncellist , presumably 
 nationality . movement 
 Rubinstein duet Sonata D ( Op . 18 ) , solo 
 Godard , Dav Tschaikowsky , Popper , dis- 
 tone , 

 idoft , 
 powerful , 
 execution 

 nesday , April 30 , alarge audience . Sehor Sarasate 
 principal solo Mendelssohn Concerto , appear 
 , 
 violinist . exquisite purity tone unfail- 
 e accuracy intonation arduous 
 passage impart charm playing impos- 
 sible resist . hand , little 
 intellectual breadth style , cheap effect gain 
 adoption break - neck speed move- 
 ment serve dissolve spell , rate cultured 
 listener . Caprice Ernest Guiraud , french composer 
 comic opera , consist Andante C 
 Allegro appassionata minor , commend 
 elegant tastefully write piece . orchestra , 
 | Mr. W. G. Cusins , fair performance Mozart 

 Jupiter " Symphony , Beethoven ' ' Egmont " ' Overture , 
 | fantasia russian air Glinka bein g 

 eee 

 MUSICAL TIMES.—Jvne 1 , 1884 . 339 

 include programme . second Concert , ! state , addition Mr. Maas , soloist 
 Saturday afternoon , roth ult . , St. James Hall was| Madame Marie Klauwell , Miss Ellen Atkins , Miss Evelyn 
 thronged , great enthusiasm pre-/| Gibson , Miss Florence Monk , Mr. Frank Ward , Mr. 
 vailed . Herr Max Bruch Concerto , g minor , Bridson . 
 exhibit ponderous german composer ability 
 favourable light . movement ratherin | « 7 
 style m ; tion , second | 
 beautiful , suit Sefior Sarasate style nicety . | 
 audi find admire Fantasia 
 " Carmen " - piece play 
 occasion ; , music class art value , 
 criticism place . orchestral 
 work Mendelssohn * Scotch " Symphony , Weber 
 " Der Freischiitz " Overture , Liszt grotesque 
 " " Mephisto Walzer . " Concert , 21st ult . , 
 violinist essay Beethoven Concerto , great 
 work write instrument , conclu- 
 sive test power executant . Herr Joachim ’ 
 ece Nas 
 London audience hypercritical , justic 
 Sarasate compel acknowledge é 
 ciency b rendering regard 
 power , sion almos 
 transform 
 atone lu 
 especially upper octave , marve 

 technical perfection display , par- | ° f 5e ae 
 compose | individual religious servic 

 composer , satisfactory , elicit loud 
 hearty applause . ' little work score 
 orchestra 

 annual Concert Edgbaston Amateur musical 
 Union , 7th , usual interest 
 merit , selection comp « , addition minor item , 
 Mendelssohn early symphony C minor ( Op . 11)—com- 
 pose 1524 , perform time Philhar- 
 monic Concert London , 1829 , Composer 
 appearance country ; Beethoven 
 " man Prometheus ' ? Overture ; Sterndale Bennett 
 * * Naiades " Overture ; anda Concert Finale Miss Oliveria 
 Prescott . playing band , number 
 amateur , include half - - dozen lady violinist , 
 creditable , skill Con- 
 ductor , Mr. Sutton , occasionally severely tax 
 branch executive . Mr. Piddock , 
 member Mr. Hallé orchestra , perform phe- 
 nomenal feat flute , couple local vocalist 
 contribute pleasing song 

 Carl Rosa Opera series , reduce occasion 
 customary night , open 
 Igth ' * Carmen , " Madame Marie Roze 
 resume 7véle capricious heroine Mr. Barton 
 McGuckin love - sick sergeant Yose . doth 
 assumption reveal noteworthy improvement 
 previous essay artist , 
 performance opera altogether exceedingly 
 spirited effective . following evening Dr. 
 Villiers Stanford new Opera * " * Canterbury Pilgrims " ' 
 va produce time London , 
 success repeat follow 
 Friday . cast substantially , 
 exception , London , need review 
 performance . exception allude substitution 
 Mr. Leslie Crotty Mr. Barrington Foote 
 Hal o ' Chepe , assumption , enter 
 invidious comparison , generally admit 
 able effective performance . ' opera splen- 
 didly mount , performance evoke little 
 enthusiasm , produce deep impression 
 cultured section audience , recognise 
 score evidence earnest _ original 
 thinker ripe musical scholar . Miss Clara Perry 
 Mr. B. Davies carry vocal honour evening , 
 considerably advance reputation 
 excellence singing acting respective 
 Cicely Hubert Level . follow even- 
 e Donizetti ' * La Favorita ’' perform 
 time Birmingham , form , meet 
 cordial reception house . Madame Marie 
 Roze , Leonora , produce deep impression 
 audience , Fernando Mr. McGuckin 
 reap fresh laurel . new comer , Mr. Mills , 
 Monk Baldassare , reveal possession power- 
 ful train organ , little stage experience 

 MUSIC WEST . 
 ( correspondent 

 season " Monday popular 
 Concerts attract immense audience , April 23 , 
 Colston Hall crowd . programme 
 interesting , comprise symphony 
 hymn praise , Beethoven Concerto D major , 
 violin orchestra , overture ' ' Zanetta " 
 ' Rosamunde . " " Symphony ver } finely render , 
 trombone play spirited man 
 string extremely good allegro . lovely 

 Allegretto delightful smoothness , | 
 adagio temarkable good expression . 

 d Festivals 
 symphonic , interpret orchestra 
 150instrument . principal singer engage 
 year Mdme . Nilsson , Mdme . Materna , Miss Emma 
 Juch , Mrs. Hartdegen , Miss Winant , Herr Winkelmann 

 fr . Toedt , Mr. Victor Lindau , Mr. Remmertz , Mr. Heinrich , 
 Herr Scaria Mr. Herman J. Gerold . large work 
 be'given seven Concerts Festival 
 Gounod * " * redemption , " Beethoven C minor D 
 minor Symphonies , Handel " Israel Egypt , ’' Brahms 

 trumental music p 

 le | ° } 7a . ° . , 
 1s | write work * * mors et vita " Wednesday 

 morning , new Cantata Mr. ' Thomas Anderton , 
 new instrumental piece Mr. E. Prout , Scena 
 | Massenet , Beethoven Violin Concerto , jizale 
 act Wagner " Parsifal " ' follow 
 evening . ' Messiah " restore rightful 
 | place Thursday morning , evening programme 
 include Mr. Villiers Stanford new Cantata * 
 | holy child , " new Violin Concerto pen 
 Mr. A. C. Mackenzie , excerpt " Tristan und 

 impulse exercise irresistible spell Isolde , " Liszt hungarian Rhapsody . Friday 
 listener , means lack dramatic colouring ! morning bring performance Herr A. Dvorak 
 characteristic individualising person represent . | new Cantata Beethoven Choral Symphony , 
 particularly efiective demonstrative , , | evening Festival bring fitting close 
 polyphonous resource composer ensemble | repetition Gounod * * Mors et Vita . " ail , 
 finale , grandly conceive admirably sustain | new work seven number , 
 elaboration fail create correspond | native musician , Birmingham , think , 

 impression . T ° y , comment masterly 
 orchestration , , overweighte , exhibit 
 rare splendour local colouring reveal 
 practise master orchestra . conspicuous 
 act pathetic ' Vocero ' Lydia duet Orso , 
 ' Wohl denk ich Eurer sanften Worte noch ' ( ' ah ! 
 mind gentle word ' ) , especially 
 contain passage exquisite beauty . second act 
 fascinate graceful ballet music , 

 , mental malady . Smetana write bohemian _ — _ 
 tion , Mr. Willem Coenen , aid Netherland 
 1 

 important orchestral piece , tw 
 « Mein Vaterland , " ' symphonic poem , ' yeubead , " | Benevolent Fund , St. James Hall , onthe rst 
 perform Crystal Palace , | ult . , anumerous audience . ‘ fhe indisposition Mr. 
 meet little favour . , Smetena know ; Sims Reeves , 
 inthis country . interesting add solemn | prevent appearance ; 
 march pen perform Prague Shake- | song set dow n Mr. Sims Reeves e€ exqui- 
 speare tercentenary .   sitety sing Mr. W. H. Cummin gs — tt é pure 
 WILHELM von Lrnxz.—The death | specimen religious music , ' come unto , " accomp : anied 
 announce gentleman , russian Councillor | composer , Mr. Coenen , sece ) 
 State , OCC : asional writer music Fournal| know * Tom Boy sf 
 de St. Petersburgh . Lenz position mt usical man | song , place ot tw 
 y authorship work | Herbert ves , everythi 
 publish French St. Petersburgh | cumstance , 
 itle * * Beethoven et ses Trois Styles 

 son , Mr. Herbert Reeves 

 Peal Mr 

 
 small volun 
 ( 1852 ) ex 
 qucte 
 cerne t ! roduction : " ar 
 come original edition 
 selle des Musicier s , ' t 
 style t lustrious composer correspond thrce 
 epochs | observation , truth w hich 
 genera need , br ono Von Lenz hand 
 occasion buff e eccentricity . 
 Nees ime 
 absent fron é 
 Beethoven t q 
 indiscrim inate thap od 
 prepare ge 

 addition , 
 Kunststudie . " ' mes L 
 critical article u 
 forte Virtuosen un 

 174 ( Hutchings ) ; " Les Organistes Contem-| attend numerous high lative 

 porain , " ' £ 301 ( Romer ) ; Hopkins " * select movement | audience . varied character 
 Organ , " £ 47 ( Novello ) ; Blumenthal ' * requital , " | imagine Beethoven ’ sonata 
 £ 67 ( Hutchings ) ; ' Chorister sap gig £ 1,440 | b flat ( op . 106 ) , prelude Fugue Bach , 
 ( Novello ) ; ; Pinsi iti ' Vocal duet , " £ 43 ( Novello ) ; | selection work Schumann , Chopin , Thailberg , 
 Smart " king sey daughter , " £ 335 ( Novello ) ; | Mozart , Walter Macfarren include init . 
 Wallace Pees hp ( Hutchings ) ; Mattei " Grande | technical difficulty composition 
 Valse de Conc 137 ( B. Williams ) ; Hullah " | surmount consummate ease accomplished 
 

 Wallace ' * Maritana , " £ 1,064 player expect room 
 lp s execu- 
 Pe assage 

 nw J 
 Music , " £ 282 ( Novello ‘ ie 
 ( Hutchings ) . total < day ’ sale acquaint w ith Miss Gyde ex 
 £ 25,000 . tion ; grasp , 
 Monday , 5th ult . , Annual Concert ] finger , clear rendering elaborate 
 Hospital Society , Town| contrapuntal writing Beethoven great w ork , 
 institution benefit | quality rarely e idence performance young 
 occasion Dalston German Hospital . artist | artist , applause greete e 
 Miss Clara Saniuell , ' " . _ " Helen D ' : aon Miss Hilda | gratifying fairly earne audience 
 Coward , Miss Sherrington , } r. Joseph } Taas , Mr. Egbert | tire recalli ng ; 
 Roberts , Mr. Sant ! " sing | minor piece selection especially men- 
 accustom excellence , pile generously respond } tion Schuma nn ’ s * Nachtstiick " ' Traumeswirren , " 
 enthusiastic demand repetition . Messrs. Coward | Chopin Ballade G minor , Thalberg Study minor 
 sang opening closing quartet , instru- | ( repeat note ch : arm rly pla iyed ) , 
 mentalist Miss Schumann ( violin ) , Mr. Rawlins ] composer popular Fantasia * * Mose Egitto , " 
 ( cornet ) , Mr. Foumee Meen Miss L. Schumann | warmly gg vocalist 
 ( pianoforte ) ; Mr. F. M. Wenborn , usual , } Mr. William Shakespeare , , refined singing , 
 musical director . expect net result the| secure enthusiastic encore . add 
 charity approach £ 200 , Miss Gyde perform entire programme memory 

 Haggerstone 
 Hall , Shoreditch , ' th 

 m 

 disfigure 
 irpe Eolienne , " 
 sketch ; oom ek Beethoven — viz . , complete Cz 

 10 

 s stay 
 lia Venez 
 personality poet - composer , publish 
 Venice . title " " Wagner Venezia , " 
 author , Dr. G. Norlenghi 

 Beethoven " Videlio”—if continental journal rightly 
 informed — produce time St. Peters- 
 burgh season — i.¢. , seventy year 
 production sublime masterpiece Vienna . 
 announcement appear scarcely credible , certainly 
 reflect favourably artistic life 
 leading capital Europe , , , annually 
 lavish enormous sum favourite prime 
 donne 

 musical Dictionary , 
 language , publish Amsterdam . 
 author M. H. Viotta 

 April 23 

 selection , include Beethoven quintet wind instrument 
 ( op , 16 ) wn new song , " old bridal dress , " Conductor 

 solo vocalist Miss Fusseile 
 highly successful . T 
 Dunster , Washford , Porlock branch 
 efficient , effective band - , lead 
 Dr. Mackenzie , Tiverton . Conductor Mr. J. Warriner , 
 L. Mus . , T.C.L. , Organist Parish Church 

 Suc- De Fonblanque , Miss Faulkner , Mr. Gilbert Campbell | fairly bear comparison . work dram ic form , 
 miss vocalist , Mr. Sidney Naylor able accompanist.—the | Mr. William Grist , writer libretto , divide narra tive 
 ee Musical Society Orchestral Concert place Theatre | sce , " Buildi 2 ' Invocation 
 t , St. Royal , Wednesday , 21st ult . band complete | Departur * Medeia ' love , " " 

 detail . programme comprise Beethoven Symphony C | Conflict , " ' * triumph . ' ry se , ef pisode 
 cond ( . 1 ) , overture Son Stranger ( Mendelssohn ) , Nosa- | successful voyage Argonauts . ar nt orchestr ra 
 ig munde ( Schubert ) , // Barbiere ( Rossini ) , hungarian March conduct Dr. Armes , Mr. Alderson C 
 tion , Berlioz Faust ; Air de Ballet , " Sylvia , " Piszicato ( Délibes ) ; choir hear good , 
 Guy , Mendelssohn Pianoforte Concerto D minor , solo | work ith precision 

 play Conductor Society , Mr. Frank Sp y. Mis nod Mr Ts ny ve Whatford ( : fe Joba 
 Hey- Boilie Lloyd vocalist , , song , | 1 Mr. r rong , og ) . Cantata war 
 > .0 , finish rendering Gounod " green hill far away , " wit h certainly t 

 J- F. BRIDGE , anp JAMES HIGGS 

 no.1 . Slow movement e+ ose sos « . » Schumann 
 Mus . Doc . , Oxon . Mus . Bac . , Oxon . Minuet ( 12 minuet ) . ae « . Beethoven . 
 , 2 , Andante ( Pianoforte Sonata , Op . 147 ) cen « « Schubert . 
 Largo ... p ine ... Handel . 
 Booz — SHORT PRELUDES FUGUES 2 . € d. pant hart . coo ote wr es Spohr . 
 ajude IGUES : oP . 3 . Agnus Dei aa ae ie « . Schubert . 
 — PRE 4d , s ND ooo oes 5 . od . 
 » & vinpunnaaie S , PUGUES , TRIO overture , ' " Acis Galatea " . oe -- » Handel . 
 » IIL — FANTASIAS , PRELUDES , FUGUES .. , 3s . od . Al bumblatter , NOE csc ie eee « . Schumann 

 London : NovELto , Ewer Co. London : Novetto , Ewrr wis Co 

 s. d. | s. d , 
 CLEATHER , N.—Lullaby 4 0 } MORI , N.—Two Fantasias Macfarren " Robin Hood " ' 
 — - — Tarantella ove 0 . ; : . es " 3 6 
 CRAMER & PERIER — March , Soldiers ’ Chora , Goun d ~~ Femtonis Gounod Opera " Faust 40 
 opera " Faust " _ . « + 3 6 ! RAFF , J.—Cavatina 30 
 DUSSEK.—Sonata , . 1 , b fat ® © ) ROECKEL , J. L.—Souvenirs Pittoresques , 3 book ... 40 
 — Sonata , . 2 , g ... ve : 6 0 
 ; book 1 . Douce Tristesse , romance , adieu Cocthamertha . 
 des HENRY.—Fantasia Plan tte " Rip Van » 2 Danse Campagnarde , Repos du Soir , l'Espagnole . 
 Wink : . . 5 0 » 3 . Walsette , Kéve de Bonheur , Gavotte Joyeuse . 
 — F noel Sulliv ’ lolanthe ' sid ; ? 5 oo 
 — — Fantasia Sullivan " Pirates Penzance " 5 o| SAINTON , P.—Un Souvenir , Fantaisie ( Op . 26 ) - 60 
 — — fantasia Sullivan " Patience " 5 o| — — Fantaisie Ecossaise ( op . 27 ) ove eae jh — 
 GODFREY , D.—Selection Gounod " ' Faust " ' OO — - " ' ; ; 
 HAMILTON , A.—Twelve scottish melody wow Jf O Tarantelle E » £ % 
 HANDEL , G. F.—Sonata D major . edit Cuanies _ — _ - 40 
 HALLE ... ne ar 3 ) MO eae 
 — Valse ... - 40 
 LINDSAY , L ADY . oun song 4 0 — Reverie ay 
 F 

 - SONGS , KC . 
 compose | compose 
 arrange PRICE | arrange Price 
 1 . Dulce domum . s. A.T.B. : . SirG. A. Macfarren 1d.| 48 . Lord veuiineney en 6 S.A.T.B , G. A. Osborne 2d , 
 2 . dead man . S.A.T.B. - 1d.| 49 . Te DeuminF .. , . Jackson 2d , 
 3 . girl leave . s. A.T.B. ss 1d.|50 . Te DeuminF ... ai ove Nares 2d , 
 4 . British Grenadiers . s.a . T.B. ‘ eo 1d . | 51 . Charity ( La Carita ) , s.s. .... = Rossini 4d , 
 5 . long live England future Queen . " s.act.n . Dr. Rimbault 2d . 52 . Cordelia , A.T.T.B. G. A. Osborne 4d . 
 6 . task end ( song chorus ) . A.T.B.B. o Balfe 4d . | 53 . know . s.a . t.p . Walter Hay 2d . 
 7 . spake summer day . S.A.T.B. ... . .. Abt 2d.| 54 . Chorus Handmaidens ( " Fridolin " ) A. Randegger 4d , 
 8 . soldier ’ chorus . T.T.B.B. ... Gounod 4d.|55 . Offertory Sentences ... « Edmund Rogers 4d . 
 g. Kermesse ( scene " Faust " ) 6d . | 56 . Red - Cross Knight ne Dr. Callcott 2d , 
 10 . , quit thy bower . s. a.7.b. ; Brinley Richards 4d . 57 . Chough Crow Sis : Sir H.R , Bishop 3d . 
 11 . maiden , - woo . S.S. rT T.B. Sir G. A. Macfarren 2d.|/ 58 . " Carnov . ale " Rossini 2d , 
 12 . faggot- binder ’ chorus e Gounod 4d./ 59 . softly fall moonlig ‘ ht Edmund Rogers 4d , 
 13 . Sylvan hour ( female voice ) Joseph Robinson 6d . 60 , Air Himmel ... a4 ave pee oe Henry Leslie 2d . 
 14 . Gipsy Sheces + . Balfe 4d . 61 . Offertory Sentences oe ‘ E. Sauerbrey 4d . 
 15 . Ave Maria * s wen Arcadelt 1d . | 62 , Resurrection Cc . Villiers Stanford 6d . 
 10 . Hark ! herald igel sing . SATB ... Mendelssohn 1d . | 63 . Boys , New Patriotic Song H. iJ. — & W.M. Lutz 4d . 
 17 . England ( solo chorus ) . s. A.T.B. Sir J. Benedict 2d . | 64 , Men Wales > aaa 2d . 
 18 , Shepherd Sabbath day . S.A.T.B. ove J. L. Hatton 2d . 65 . Dame Durden ... 1d , 
 1 g. thought childhood . s.a . T.B. . . Henry Smart 2d.| 66 , little farm till " Hook 1d 
 20 . Spring Return . s. A.T.B. ... se ass + 2d.|67 . simple maiden ... Sir G. A. Macfarren 1d , 
 21 . Anold Church Song . s. A.T.B. nr " 2d . | 68 , Fair Hebe 5 cee e 1d , 
 22 . Sabbath bell . s.a . t.38 . o ae nt . 2d . | 69 . loveda maiden fair ... ae 1d . 
 23 . Serenade . S.A.T.B. ... oes ois oe " 2d.| 70 . Jovial Man Kent sss ae 1d , 
 24 . Cold Autumnwind . s.a . T.B. ast os 2d.|71 . Oak Ash . il ~- ba 1d . 
 25 . Orpheus lute . s.s.s . . Bennett Gilbert 2d . | 72 . Heart oak 2 1d . 
 26 . lullaby . s.a.a . - 1d . | 73 . come sunset tree W. A. Philpott 4d . 
 27 . , " native land . s. a.7 . B. SirG. A.Macfarren 1d . 74 . Way , S : a.t : Bi ose W.F. Banks 2d . 
 28 . March Men Harlech , .a . T. B. Dr. Rimbault 2d./ 75 , pure , lovely innocence n di di Lahore " ) . " chorus female 
 29 . God save queen . s. A.T.B. aaa aes - 1d . | voice .. ons . Massenet 4d . 
 30 . Rule , Britannia . s.a . T.B , ... ove eee 1d.| 76 . Love Idyl . s. A.T.B , Ro eee .R. Terry 2d . 
 31 . retreat . T.T.u . B. oi ai s06 de Rille 2d . 77 . hail tothe wood . a. 1.7 . ee Yarwood 2d , 
 32 . Lo ! morni break . S.S.S. « . Cherubini 2d.| 78 , Nearthetown Taunton ... Dean Thomas J. Dudeney 2d . 
 33 . weare spirit , s.s.s Sir aes A. Macfarren 4d . | 79 , merry boy sea .. J. Yarwood 2d . 
 34 . Market chorus ( " Masaniello " ) . S.A.T.B. ' " Auber 4d . | 80 . Christ rise ( e aster anthem ) . S.A.T.B Berlioz 3d , 
 35 . prayer ( ' ' Masaniello " ) . s. A.T.B. 1d , | 81 . sun set o'er mountain ( ' ' Il Demonio 
 36 . Water sprite . s.a . T.B. - Kiicken 2d . j A. Rubinstein 3d . 
 7 . Eve glitter star . s.a . T.B. ae ies eas ne 2d . | 82 . hymn Nature Beethoven 3d . 
 38 . primrose . s. A.T.B .. ner san * 2d . | 83 . Michaelmas Day ( humorous | part- song , ' . 1 ) W. Maynard 4d . 
 39 . odewdrop bright . s.a . T.B. vee 1d , | 84 . Sporting note ( humorous part- rai . 2 ) 4d . 
 40 . Sanctus , ' ' Messe Solennelle . " " s.a.t.z . Rossini 4d . | 85 . austrian National Hymn Hay dn 4d . 
 41 . kyrie , ancient modern . , Gill 2d . | 86 . Carol . s.s.c . ... aie | Jose ph Robinecs 4d . 
 42 . Sunofmysoul . s.a.7.B .. sea Brinley Richards 2d . | 87 . bright - hair’d Morn . A.T.T.B. Theodor L. Clemens 3d . 
 43 . ’ twas fancy , ocean ny s. A.1.B , GA . Osborne 2d . | 88 oh , rest ( ' ' Velleda " ) .. , ie ov Ge Pe Lenepveu 4d . 
 44 . prayer Sea . s.a . T.B. 2d . | 89 . love reigneth . T.T.B.B. C.G. Elsisser 6d . 
 45 . OThou , power(pray : erfrom " ! ic Egitto ’ ’ ) Rossini 2d.|go . Joy Waltz , 1.7.b.n . ... 6d . 
 46 . Guard Rhine . - s.a.1.2 . Sir G. A. Macfarren 1d.|g91 . Star Bethlehem ( Christmas Carol ) ' Theodor L , Clemens 24 . 
 47 . german fatherland . s.a.7.8 . " 1d , ! 92 . Busy , curious , thirsty fly , 1.a . T.B. " 3d 

 LONDON : 
 CHAPPELL & CO . , 50 , NEW BOND STREET , W 

