Karg - Elert break away tradition , good 
 example usually short , hardly 
 creation 
 extended development choral theme hint 
 occasionally Bach 

 leave César Franck . 
 ' choral ' - know England 
 need little mention . 
 remark , . , clear 
 construction ( especially . 1 ) 
 Franck disciple Beethoven Bach . 
 suggestive choral prelude 
 fantasia . source , look 
 large variation form late Beethoven . second , 
 pity work announce 
 english recital programme ' Choral Preludes . ' 
 french title hardly adequate , 
 misnomer 

 Franck follower idea 
 developed ( distinct figured ) choral , 
 produce work broad , hymn - like theme 
 basis , relieve - contrast matter free 
 agitated character,—a design - suit 
 organ , far ecclesiastical effect 
 average ' bright ' postlude 

 auspex Edinburgh Society Organists 
 aconference Society Ministers Edinburgh 
 district hold February 14 . meeting largely 
 attend representative denomination , 
 frank exchange view place undoubtedly 
 understanding relationship 
 ought exist clergy organist . 
 Society fortunate discussion open 
 lead clergy city . Rev. Dr. A. Wallace 
 Williamson , St. Giles Cathedral , inspiring 
 lead short address ' Christian Year ' ; Rev. 
 Dr. Drummond speak ' Ministry Choir ' ; 
 Dr. W. B. Ross criticise ' Presbyterian Service 
 frequently ' ; Mr. Arthur Curle 
 ' historical Sketch Presbyterian Church Music ' ; Mr. 
 T. H. Collinson speak ' Influence Clergy 
 choir . ' far learn occasion 
 meeting place city , 
 prospect similar meeting near 
 future 

 new organ St. Margaret , Durham , erect 
 amemorial parishioner die war , 
 dedicate January 31 Venerable Archdeacon 
 Durham . manual - stop , 
 build Messrs. Harrison & Harrison . oak carving 
 case work gift Mr. A. V. Yockney , 
 headmaster St. Margaret Day School . opening 
 recital Mr. William Ellis , sub - organist 
 Cathedral , play Bach E flat Fugue , Parry Prelude 
 ' martyrdom , ' Finale Reubke Sonata , 
 Beethoven romance G , Chopin Funeral March , 
 Guilmant Grand Chceur D. chief item 
 subsequent series recital - know player 
 find organ recital column 

 solemn celebration Holy Communion place 
 St. Martin - - - Fields February 16 , memory 
 member S.P.G. fall war . 
 musical , occasion interest 
 great possibility simple music . service 
 congregational character , setting Merbecke . 
 body eighty mixed voice organize . 
 sit nave , large congregation , lead , 
 sang impressive effect . service Tallis Funeral 
 music play . Mr. Martin Shaw ( responsible 
 musical arrangement ) organ 

 Bach ' Funeral Ode ' receive undoubtedly 
 performance Australia December 12 , St. John 
 Church , Toorak . soloist Miss Anne Williams , 
 Miss Mary Thirlwall , Mr. Percy Blundell , Mr. Wilton 
 leech . Mr. Arthur E. H. Nickson conduct . 
 choir congratulate enterprise 

 Mr. Bernard Page good work Wellington , 
 N.S.W. , Saturday evening organ recital 
 weekly February December . list piece 
 play past year note Bach good 
 work , Franck , representative selection 
 Saint - Siens , Karg - Elert , Handel , Lemare ( 16 ) , Wolsten- 
 holme ( 11 ) . orchestral performance rare , arrange- 
 ment rightly important feature , 
 Wagner ( 15 ) , MacDowell ( 9 ) , Debussy ( 8) , ard Beethoven ( 6 ) 
 head list . glad Mr. Page find room 
 piece Byrd , Felton , Greene , Keeble , Russell , Wesley , 
 old worthy 

 ORGAN recital 

 accompany Pianoforte . Edwin Evans , sen . 
 172 music example . ( William Reeves , Charing 
 Cross Road . ) handy volume , pp . 231 . price 
 

 Beethoven sonata . vol . i. ( nos . ii ) . revise 
 M. Moszkowski . ( Société Francaise d’édition des Grands 
 Classiques Musicaux , Paris , Enoch & Sons , London . ) 
 price 4s . 6d . - print folio volume . finger , 
 note metronome rate distinguished 
 editor 

 correspondence 

 s town scour early morning 
 breakfast - time deed 

 write ( //ereford Times , February 17 ) 
 friendship mean possess , 
 reason probably prevent develop 
 great gift composition . 
 enjoy musical career conducting . love 
 large force , command , strong , clear 
 beat inspiration sing . 
 fine organizer , year work preparation 
 Choirs Festival , note 
 London rehearsal note Festival 
 work conductor sheer joy . great 
 teacher fine interpreter great work . 
 fine achievement conductor 
 reading B minor Mass Beethoven Mass D 

 truly loss english Church music , 
 Choirs Festivals , colleague , heavy 

 keen interest music , little difficulty 
 concert promoter push british music , 
 ultimate resort nearly large concert scheme govern 
 power purse . baldly state , concert - 
 perform work public payto hear . present , 
 small section care british music pay 
 , thousand thousand people subscribeto 

 hear Bach , Beethoven , Brahms , Handel 

 moment suggest great compose 
 nation exclude programme ; 
 horrible calamity lose . @ 
 contend earnestness heart capable , 
 english music equal share foreigt 
 work land home . j 

 , base - - spirit ¢ Lo 

 Patriotism ' succeed ban Wagner 
 Beethoven ancient wall imperial tomb . 
 occasion second concert direct illy 

 trious french conductor , Réné - Baton , Sunday , january2,§ gopra 

 Symphony , ' New World ' Dvorik 
 adequately perform . Miss Scharrer 
 good form , Symphony play ease 
 freedom come familiarity technical 
 demand 

 Berlioz 
 Wagne ) 
 Beethoven 

 QUEEN HALL ORCHESTRA 

 January 27 , Beethoven Rondino e flat wind 
 instrument play singular beauty tone 
 perfect ensemble . Suite , ' legend Tsar Saltar , ' 
 Rimsky - Korsakov , perform time 
 country . consist Entr’actes opera 
 . movement charming , 
 effective . Suite easily 
 intelligible , scoring fine craftsmanship 
 instinctive gifted composer . audience 
 quick realise beauty music , feel 
 sure welcome early repetition . ' New World ' 
 Symphony , perform great attention 
 finish , successful item . interest 
 feel - appearance Miss Kathleen Parlow 
 Brahms Violin Concerto . play fine 
 feeling , generally certainty technique 

 concert February 10 , novelty 
 Overture drama ' Tsar Boris , ' Kalinnikov 
 ( 1866 - 1900 ) . turn interesting 
 orthodox composition , period arrest 
 special attention , effect strikingly 
 impressive . symphony G composer 
 exemplification undoubted talent . 
 item programme prelude ' Le Déluge , ' 
 Saint - Saéns , Brahms second Symphony — mag- 
 nificently play — Schumann minor Pianoforte Concerto , 
 solo adequately perform Miss 
 Irene Scharrer , Prelude ' L’Aprés - midi d’un Faune , ' 
 Hamish MacCunn Overture ' Land Mountain 
 Flood . ' Sir Henry Wood conduct occasion 

 Madame Alys Bateman organize excellent concert 
 January 27 , 500 convalescent soldier 
 sailor . programme - design 
 purpose , enthusiastically receive 

 London String Quartet , February 2 , play 
 new Debussy Sonata F , write harp , flute , viola . 
 elicit opinion . quartet play 
 Brahms minor Dvordk F. February 16 
 Ravel Quartet , ' Rasoumovsky ' ( Quartet 
 Beethoven , programme , Vaughan Williams 
 beautiful song - cycle , vocal sing Mr. 
 Gervase Elwes good style 

 Miss Edith Abraham Miss Ethel Hobday 
 attractive recital February 3 . ' little Sonata , ' 
 J. B. McEwen novelty , cheerful 

 Miss Gertrude Peppercorn concert February 10 , 
 programme specially arrange interest 
 child . Schumann ' Kinderscenen , ' Op . 15 , 
 selection , suite ' Peter Pan sketch , ' Harry 
 Farjeon , novelty 

 concert London Trio February 14 , 
 programme include Beethoven Grand Trio , Op . 97 , 
 Leclair D major . Miss Goodwin ( pianoforte ) 
 play interesting selection usual skill , 
 Miss Margaret Champneys sing agreeably 

 Miss Daisy Kennedy ( violin ) recital February 15 . 
 great achievement remarkable performance 
 Paganini Concerto D 

 Central London Choral Orchestral Society draw 
 large audience commodious Central Hall , 
 Westminster , occasion concert 
 January 20 . Elgar chorus ' come misty age , ' 
 ' Banner St. George , ' Percy Fletcher 
 ' song Grey sea ' appropriate item . Mr. D. J. 
 Thomas conduct 

 Royal College Music concert February 1 , 
 student remarkably good performance Brahms 
 Quartet , Op . 51 , . 2 , César Franck ( Juintet F 
 minor . interesting item playing Doris 
 Fell Kathleen Connah Saint - Saéns variation 
 pianoforte theme Beethoven 

 Leighton House Chamber Concerts bring Mr. 
 Arthur Catterall quartet party London play 
 February 8 . prove fine body 
 artist . modern music represent Taneiev Quartet 
 minor ( Op . 11 ) . Miss Jean Waterston sing 
 Cyril Scott song accompaniment composer 

 need ' - Breakfast Concerts ' complete 
 present musical output , future shall 
 Birmingham concert time day . 
 ' - tea Concerts ' , concert hold 
 o'clock , , irrespective evening 
 concert , crown Mr. J. C. Hock , local violoncellist , 
 series * Dinner - Hour Concerts ' 
 Midland Institute 

 new venture organize Mr. Stevenson , 
 jun . , conductor New Philharmonic Society , 
 , ' Town Hall Promenade Concerts , ' 
 January 18 , attendance 
 mean encouraging . match enterprise 
 far early ’ , time Crimean 
 War , famous Julien inaugurate popular Town 
 Hall Promenade Concerts , famed vocalist 
 instrumentalist time appear , Ernst , 
 Wieniawski , Madame Pleyel , Reichardt , Koenig , Madame 
 Gassier , Liebhardt , & c. year later late Mr. 
 Ffrench Davies essay Promenade Concerts 
 Town Hall , similar venture fail 
 draw large audience . Mr. Stevenson 
 beat fairly large orchestra amateur pro- 
 fessional , spirited enjoyable performance 
 strictly popular orchestral selection , comprise 
 ' Peer Gynt ' Suite . 1 , Sibelius famous ' Valse 
 Triste , ' Moussorgsky russian Dance , ' Gopak , ' 
 Barcarolle ' tale Hoffmann , ' Prelude 
 ' Carmen , ' overture ' 1812 . ' - 
 need large rank file , especially 
 sonorous array string . Mr. Norris Stanley , 
 young talented local violinist , play Beethoven 
 Concerto finish technical skill 
 Town Hall year . vocalist Miss Vera 
 Horton , sympathetic contralto . second Promenace 
 Concert February 8 , attendance 
 come expectation , concert 
 continue patronage bestow . 
 programme advance 
 previous concert , contain Overture ' Merry Wives 
 Windsor ' ' Meistersinger , ' ' Finlandia , ' Polovtsienne 
 Dances Borodin ' Prince Igor , ' hungarian 
 March Berlioz ' Faust . ' excellent interlude 
 prove Miss Bessie Clarke scholarly interpretation Grieg 
 Pianoforte Concerto minor , Op . 16 , accompany 
 orchestra . vocalist Mr. Herbert Simmonds , 
 artistic cultured baritone gift sympathetic 
 voice . ' concert distinct advance 
 predecessor 

 connection Catholic - union Birmingham , 
 January 30 , excellent concert arrange Mr. Gervase 
 Elwes Grosvenor Room , Grand Hotel , 
 attend high dignitary Roman Catholic Church . 
 addition Mr. Gervase Elwes appear Miss Louise 
 Dale , Miss Dilys Jones , Sefior Gomez , spanish violinist , 
 Miss Dorothy Howell ( pianoforte ) , Mr. B. Kiddle 
 accompanist 

 considerably half 1916 - 17 series 
 Symphony Concerts , 
 likelihood interest successful proceeding 
 steadily maintain finish season . 
 certainly point number recent audience 
 satisfactory , safe assume Thursday 
 function constitute sound proposition 
 policy Winter Gardens . , Mr. Dan Godfrey 
 know public provide . | Tchaikovsky , 
 instance , sure draw average audience , 
 , January 18 , programme entirely 
 work composer present highly appreciative 
 assembly . excellent playing hear follow 
 week diverse composition Berlioz ' Carnaval 
 Romain ' Overture , Mozart (; minor Symphony , 
 portion Suite Symphonique construct 
 Charpentier opera ' Louise ' ( time ) , item 
 particular receive flexible , emotional , 
 polished performance . occasion genuine 
 pleasure derive revival notable work 
 Borodin B minor Symphony , Dvorik ' Carnival 

 Overture , Glazounov sixth Symphony , , , 
 Beethoven example major . composition previously 
 unheard concert comprise Overture 
 introduction Act 3 Goldmark opera , ' 
 cricket hearth , ' ' ancient Scots Tunes ' set 
 string Sir A. C. Mackenzie , H. Orsmond Anderton 
 * Spring Idyll 

 solo performer Symphony Concerts 
 Miss Daisy Kennedy , reading Tchaikovsky 
 Violin Concerto individuality extremely 
 artistic ; Miss Ruby Holland , , playing 
 reveal good quality , exhibit sutflicient 
 strength work Scriabin Pianoforte Concerto 
 ( performance ) ; Miss Mary Law , 
 conspicuous success Wieniawski Violin Concerto 
 d minor , reason charming expressive 
 interpretation music ; Mr. Ioan Lloyd - Powell , 
 surpass performance Bournemouth 
 scholarly thoughtful playing Beethoven ' Emperor ' 
 Pianoforte Concerto ; Miss Joan Willis , inter- 
 pretation Saint - Saéns minor ' Cello Concerto 
 refined , little lack authority 

 owe writer inability attend ' Monday 
 special ' Concerts impossible furnish detail 
 series , accord account programme 
 exactly suited inclined little 
 overawed big musical form represent 
 symphony concerto 

 CAMBRIDGE 

 London String Quartet pay visit Cambridge 
 February 8 , concert term 
 connection University Musical Society . 
 player attract large number people , size 
 audience concert popularity 
 fully maintain . programme consist Beethoven 
 Quartet F minor ( Op . 95 ) , McEwen ' Biscay ' Quartet , 
 familiar G minor Debussy . owe 
 ill - health Dr. Rootham residence term , 
 consequence Society programme recast . 
 hope performance sacred music St. John 
 College Chapel , announce March 15 , 
 Easter term 

 February 13 chamber concert 
 Newnham College Hall aid Cambridge Serbian 
 Relief Fund . player Misses Margery 
 Thelma Bentwich ( violin violoncello ) , Mr. Raymond 
 Jeremy ( viola ) , Dr. Rumschyski , contribute 

 Brahms Pianoforte Trio B major ( op . 8) , Beethoven 
 Trio C minor violin , viola , violoncello ( op . 9 , 
 . 3 ) , Dvordk Pianoforte ( ) uartet 

 Mrs. Haydn Inwards pianoforte recital 
 February 14 Guildhall work J. S. Bach , 
 Chopin , Liszt , Brahms , César Franck , Debussy 

 chamber - music matinée Torquay Pavilion 
 February unusual interest . string quartet 
 consist Mrs. Lennox Clayton , Miss Jessie Bowater , 
 Miss Ethel Pettit , Mr. Lennox Clayton , Mr. Edgar 
 Heap collaborate pianoforte . Schumann Op . 44 
 ( Quintet Haydn op . 77 , . 1 , Quartet receive 
 excellent interpretation . success classical pro- 
 gramme assured series similar matinée 
 arrange . occasion cite , Mr. John Buckley 
 sing song Handel Henschel . February 3 , 
 Municipal Orchestra play afternoon , 
 evening band Reserve Battalion London 
 Regiment , Bandmaster Crane , gavea concert instru- 
 mental vocal music 

 Dr. Rumschisky , Rassian pianist , play 
 Beethoven Sonatas ( opp . 81 31 ) , piece 
 Chopin , Torquay , February 9 , Municipal 
 Orchestra perform Schumann Symphony e flat 
 Haydn D. Miss Bessie Bingham sing song 
 Henschel , Coleridge - Taylor , Tchaikovsky 

 Barnstaple Parish Church performance 
 February 11 12 sacred cantata soli , chorus , 
 organ , subject Epiphany , music 
 compose Dr. H. J. Edwards word write 
 select Rev. Thomas Russell . work , exalt 
 fervently devotional spirit , sing member 
 Festival Society church choir ; Dr. Edwards 
 provide organ music invariably inspire skill , 
 Mr. S. Harper , sen . , conduct . Church 
 fill listener , singer occupy chancel . 
 scheme cantata comprise — ' prophecy , ' 
 ' journey , ' ' adoration ' --and music , parallel 
 text , develop power spiritual enthusiasm climax 
 . episode Magi centre 
 interest , motif passage star , frequently 
 recur work , assist particular 
 significance . imaginative suggestiveness sense 
 rapture prelude ( organ ) ' Bethlehem ' 
 scene , form basis . revert 
 second , notice beautiful chorale , ' o thou 

 DUBLIN 

 Royal Dublin Society , chamber music recital 
 Brodsky Quartet ( January 29 ) , play 
 Schumann , op . 41 , . 1 , Tchaikovsky , op . 11 , 
 Beethoven , Op . 59 , . 3 ; Dr. Esposito ( February 5 ) , 
 play Bach chromatic Fantasia Fugue , 
 Beethoven Op . 110 , Grieg ' ballade , ' Chopin 
 nocturne b major , Op . 62 , Barcarolle Etude , Op . 25 , 
 . 12 , small piece Arne Scarlatti . 
 February 12 string orchestra , conduct Dr. Esposito , 
 play Handel Concerto Grosso b flat , Bach 
 ' Brandenburg ' Concerto G , Grieg ' Holberg ' Suite , 
 piece Scarlatti , Esposito , Gernsheim . 
 recital present series come conclusion 

 Feis Ceoil announce week commence 
 7 , hold Mansion House , kind per- 
 mission Lord Mayor Dublin . adjudicator 
 Mr. Bantock Pierpoint , Mr. H. Wessely , Signor Carlo 
 Albanesi , Mr. Sydney H. Nicholson . year com- 
 petition hold Marlborough Street School building 
 July , circumstance exceptional , 
 late date unsuitable , school close 
 end June , people absent Dublin 
 July August 

 Burns anniversary celebrate usual . 
 concert January 26 27 , 
 direction Mr. James C. Lumsden , father 
 conduct event . Mr. Lumsden enhance 
 popularity secure good artist inter- 
 pretation scottish song , amateur professional 
 alike flock feast Scotch music 

 Prof. Donald Tovey fifth Historical Concert 
 January 31 . programme consist Sonata 
 F , Op . 10 , . Sonata e minor , Op . 90 ; 
 thirty - variation Diabella Waltz , Op . 120 ; 
 sonata flat , Op . 110 . mention 
 Professor beginning play work 
 memory , feeling intimacy simply 
 delightful . sixth rehearsal series 
 February 14 , precede lecture 
 February 13 , discuss Beethoven later style . 
 programme Sonata Op . 13 , Variations ' La Stessa , ' 
 Sonata Op . 28 , Fantasia Op . 77 , Sonatas Opp . 81 11 

 concert nominally promote young people , 
 equally interesting old folk , February 10 . 
 Mr. Fuller Maitland harpsichord selection , Miss 
 Patupa Kennedy Fraser sing celtic song celtic harp 
 accompaniment 

 Sir Henry Wood conduct admirable programme 
 eighth Philharmonic Concert January 23 , 
 include ' Fidelio ' . 4 Overture , Coleridge - Taylor 
 strenuous Ballade minor , Elgar noble ' Enigma ' 
 variation , interest grow successive 
 hearing . draught crystal spring Sir 
 Henry Orchestral Suite . 6 , excerpt Bach 
 clavier - music , include ' caprice departure 
 beloved brother , ' Preludes ' - , ' 
 Scherzo Partita , Gavotte Musette 
 sixth english Suite , Preludio 
 Partita solo violin . musician 
 familiar piece appeal fresh delight new 
 medium , question favourably 
 receive general audience . 
 purist complain transference , 
 forgetful Bach lead way direction 
 transcribe accommodate Vivaldi Violin Concertos 
 organ harpsichord . 
 complain irreverence Bach clavier organ- 
 music play modern grand pianoforte 
 concert - organ . conclusion Sir Henry Wood 
 notable service orchestral transcription 
 Bach ancient master Moussorgsky modern . 
 ' Rhapsodie Espagnole ' Ravel strong meat , 
 impossible describe line . suffice 
 create definite desire hear . fine singer , 
 Madame Kirkby Lunn , sing ' Che faro ' song 
 MacDowell Quilter great acceptance , 
 Berlioz Chorus ' Ophelia ' female voice orchestra 
 interesting , effectively perform 

 Sir Frederic Cowen receive flattering reception 
 appear ' guest - conductor ' ninth Philharmonic 
 Concert February 6 . long association 
 Society , 1896 - 1913 , assure welcome 
 rostrum , performance conduct c minor 
 Symphony good tradition 
 Society , reputation interpreter 
 Beethoven great music . programme 
 conventional order , contain Tchaikovsky ' Marche Slave , ' 
 heroic , theatrical , trivial turn , Gluck- 
 Mottl ' Suite de Ballet , ' Grainger ' Shepherd Hey . ' 
 violoncello solo Madame Guilhermina Suggia 
 great executive skill accurate , delicate , tone 
 Boéllmann ' variation , ' Schumann uninterest- 
 e Violoncello Concerto ; vocalist , Madame Licette , 
 sang brilliantly - wear ' ah ! fors ’ é lui , ' 
 appropriate feeling Charpentier ' Depuis la jour 

 Mr. Joseph Holbrooke a_highly - favourable 
 impression series chamber concert , 
 commence beautiful Crane Hall January 22 , 
 power composer pianist , especially 
 interpret music , exhibit 
 chamber work bear unmistakable stamp 
 original force distinct individuality . example 
 quality , include noticeable avoidance 
 commonplace conventional , carry occasionally 
 verge eccentricity , offer ' Sonata 
 Romantic ' violin pianoforte , Pianoforte Quintet 
 G minor , whimsical ' Valse Diabolique ' 
 vigorous Finale , Pianoforte * Etudes , ' 
 ' Valse Romaneske , ' ' Scéne Bacchanale 

 concert Rodewald Concert Society , 
 January 29 , Mr. R. J. Forbes pianist join 
 Catterall String Quartet splendid performance César 
 Franck Pianoforte ( Juintet , satisfactory 
 strongly rhythmic Pianoforte Quartet Fauré , 
 fine modern work medium . Mr. Arthur Catterall 
 Mr. Forbes play Delius Sonata violin 
 pianoforte , music ' atmospheric ' order , 
 hearing convey message 

 attractive programme draw large audience 
 Symphony Orchestra Concerts Philharmonic Hall . 
 January 20 , Pachmann play exquisitely 
 orchestra Chopin Andante Spianato 
 Polonaise Brillante , detached solo demon- 
 strate incomparable delicacy touchand toneand reveal 
 usual irresistible drollery . Schubert B minor Symphony , 
 Glazounov acceptable ' paraphrase hymn 
 ally , ' Tchaikovsky ' Casse Noisette ' Suite Mr. 
 Akeroyd fine orchestra plenty opportunity . 
 February 3 , Beethoven seldom - hear Symphony . 4 , 
 attractive ' Festivo ' ' Scénes 
 Historiques ' ( Sibelius ) , chief orchestral item , 
 Mr. Albert Sammons masterly performance 
 solo Saint - Saéns Violin Concerto . 3 , 
 b minor . vocalist Miss Ida Kidder , accomplished 
 singer revive singular charm Sullivan song 
 ' Birds night . ' excellent pianoforte accompaniment 
 play Mr. T. Nevison 

 Shakespeare Theatre Harrison - Frewin Opera 
 Company January 25 performance 
 province french composer Edmond Missa opéra- 
 comique , ' Muguette , ' base Ouida famous ' 
 little wooden shoe . ' principal sustain 
 Miss Nora D’Angel , Miss Beatrice Waycott , Mr. Walter 
 Hyde , Mr. Llewys James . music , generally 
 melodious , possess indication dramatic power . 
 revival Puccini ' La Boheme , ' 
 February 16 . owe success attend 
 Company visit , season extend hy 
 possibly week 

 domain orchestral music cause | Latin rule convey impression fe . 
 occasioned departure advertise programme | grasp significance text . performance fies 
 event , fresh zest musical appetite | exception general rule , strong 
 open morning paper late operation | poetic technical aspect . " lo 
 jocularly term ' substitution board , ' ] Goossen handling Debussy ' blessed Damozel ' ( - 
 Mr. Aikman Forsyth rise superior difficulty } February 15 ) sympathetic , small choir aa 
 occasion illness , distance , submarine menace , naval | - lady fit scheme triki 
 military duty , learn , , | previous occasion November , 1915 ; — 
 practically unlimited extent native rich . matter ] soloist instance attain ideal presentation ve 
 Paderewski , Mlynarski , Rénée Chemet find impossible | unforgettable performance , oe ol 
 visit , — experience | possibly Manchester experience thos bthe 
 January 25 , instead Paderewski Glinka | exquisite sensation compound poet , musician , 
 ' life Czar ' ( cut ) sing English , | personality . nicht 
 hear July , 1888 , Edward Garcia 
 Moscow Operatic Company perform . Russian . , : _ ' 
 bass choralist oon fine efiect opening NOTTINGHAM DISTRICT . . 
 chorus , opera April } annual musical Festival Congregational Church , Sheff 
 heavy - toned choir . performance | Hucknall , place January 28 . music select C 
 people enjoy enthusiastic . Miss Mignon | include Te Deum b flat ( Stanford ) , ' King glorious Fant 
 Nevada exceptional gift Antonida . | ( Barnby ) , ' Lord hath great thing ' ( John E. West ) , ' Path 
 Manchester Operatic Chorus , train Mr. W. A. | ' Babylon Wave ' ( Gounod ) . orchestra . 
 Lomas , alert tolerably efficient , numerous ] Schubert ' unfinished Symphony , ' Walford " 
 compete heavy brass - wind ; period | Davies ' solemn Melody . ' organist Mr. C. E. hi 
 preparation curtail detention | Blyton Dobson , Mr. J. Munks conduct . sscite 
 score London performance prior Christmas . | Bingham Choral Society twentieth annual Sen 
 probably operatic performance instant | concert February 7 , Gaul ' Joan Arc ' receive topi 
 appeal general public , , ' Boris Godounov , ' | successful performance . soloist , hear ~ 
 doubt instruct musical public pass ] miscellaneous selection , Miss Lilian Clayton , Mr. Mi 
 favourable verdict . Franklin Pearson , Mr. A. Farnsworth . Mr. Pilling Dale 

 far enforce policy substitution having | conduct , Mr. C. Doncaster responsible § 
 prove disservice Manchester public , produce | accompaniment . Albert Hall organ recital speci 
 positive gain force attention| February 4 , Mr. Bernard Johnson play Bach Fugue 
 uncommonly rare combination talent Eugéne| B minor , co - operation Miss Emily Roseblade ; 
 Goossens ( jun . ) . recent article comment } pianoforte , hear piece Debussy 
 young conductor willingness step breach | Arensky , Guilmant Scherzo Capriccioso 
 moment , carry programme exceptional | pianoforte organ . Mr. Johnson Pianoforte Suite , ' 
 difficulty . February 1 15 fill gap cause | deserted waterway ' item admirably interpret L 
 Mlynarski absence Sir Thomas Beecham illness , | Miss Roseblade . Chamber Concert week 
 carry advertise scheme , | Albert Hall hold February 5 February 9 . ite 
 date include new work Spaniard , Turina , a| effort popularise chamber music , Mr. Bernard 1 
 march Chabrier , mention Bantock ' Hebridean ' | Johnson solely responsible , ' week ' include 
 Symphony , hitherto conduct Mlynarski . Goossens | afternoon evening performance . programme lead 
 obviously master situation , | announce subjoin : aa : shoul 
 performance unusual fluency point . year ago , Monday , February 5 : Pianoforte Quintet F minor , th 
 Gloucester Festival , late Arthur Johnstone César Franck ; String Quartet C minor , Op . 18 , . se 
 city writer , ' Bantock Berlioz - like | ° 4 , Beethoven ( Mr. F. Mountney , Mr. W. Whitehead , whe 
 vastness conception , wonder } Mrs. Marshall , Mr. E. Thorpe , Miss Cantelo ' ome 
 idea compassed,’—and memory this| pianoforte ) . Tuesday , February 6 : variation — y 
 utterance came listen Goossens reading . | fugue russian Folk - Song , Knorr ; concerto ia th 
 suggestive power opening grip sea-| flat pianoforte orchestra ( organ ) , Mozart 
 music — elemental , vast , tireless } ( Misses Irene Una Truman , Mr. B. Rims 
 heave mighty water , reveal overwhelm } Johnson ) . Wednesday , February 7 : Clarinet Quintet » e 
 imaginative force . little later impressive| b minor , Brahms ; Fantasie f minor , Frank Bridge 

 XUM 

 IQI7 . 137 

 String Quartet F , Dvorak ( performer 
 Monday , Mr. Boak , clarinet ) . Thursday , 
 February 8 : Pianoforte solo — ' Waldstein Sonata , ' 
 Beethoven , ' variation Serieuses , ' Mendelssohn ; organ 
 solo , Prelude fugue b minor , Bach ; Pianoforte 
 Trio d minor , Schumann ( Miss Cantelo , Messrs. 
 Mountney , Thorpe , andJohnson ) . Friday , February 9 : 
 Pianoforte ( Quartet c minor , Brahms ; suite Violin 
 Pianoforte , York Bowen ; trio d minor , 
 Mendelssohn ( Miss Alice Hogg , Mrs. Marshall , Messrs. 
 Mountney Thorpe 

 performance ' Messiah ' aid St. Dunstan 
 Hostel blind soldier sailor Wesleyan Chapel , 
 Beeston , February io 

 Mr. Herbert Ellingford appearance 
 Sheffield solo organist , - attend recital 
 Cathedral Church . varied programme include Liszt 
 Fantasia BAC H , Finale Tchaikovsky 
 ' pathetic ' Symphony , Wagner ' ride valkyrie . ' 
 Mr. Wolstenholme month hear 
 4 recitalist organ Albert Hall , play 
 graceful composition . attractive organ 
 recital St. Mary Church Mr. C. E. J. 
 Hornsby . programme Sonata D minor 
 Topfer , Bach Fantasia fugue G minor , 
 movement Borowski Sonata 

 Miss Fanny Davies Mr. Albert Sammons , Miss Louise 
 Dale Mr. Boris Bornoff , chief performer 
 fourth Sheffield Subscription Concert . Mr. Sammons 
 specialise antique music , illuminating 
 exponent , Miss Davies Beethoven 

 YORKSHIRE . 
 LEEDS 

 entertainment , ' important thing 
 programme ; - play , receive 
 tepid enthusiasm 
 characteristic Leeds 

 Leeds Bohemian Chamber Concert , January 24 , 
 Mr. Cohen String Quartet hear work 
 Schubert ( d minor ) , Tchaikovsky ( D , op . 11 ) , 
 Walford Davies,—the charming ' Peter Pan ' ( ) uartet . 
 concert , hand , warm 
 enthusiasm , audience consist élite 
 society élite musical people , , 
 fashionable enjoy sensation 
 provide brilliant virtuoso , solely entirely 
 want enjoy music . similar function 
 supply Sonata Recitals , institute Mr. Cohen , 
 January 26 , Prof. Rogers , 
 exceptionally efficient amateur pianist , play Violin 
 Sonatas Bach Schumann , short piece 
 Catoire Medtner . second recital Mr. Percy 
 Richardson pianist , programme consist 
 sonata Beethoven ( /of ' Kreutzer , ' sonata 
 C , Op . 96 ) , Medtner ( b minor ) , Fauré ( 

 Miss Edith Robinson Mid - day Conccrt , January 23 , 
 quartet lady , Mr. H. Mortimer clarineitist , 
 play Mozart Clarinet Quintet refinement finish 
 series , February 6 , Mr. Mrs. 
 Rawdon Briggs play Bach Concerto D minor 
 violin , Spohr effective duet . concert 
 meet support 
 entitled , possibly necessarily shorten 
 time allow lunch ; intend set 
 firm financial basis , hope endeavour 
 prove successful 

 Saturday Orchestral Concert January 27 , 
 series prove popular good sense 
 word , Gilson Symphony — Symphonic - Poem — ' La Mer , ' 
 introduce Leeds , Vaughan Williams 
 ' Norfolk Rhapsody . ' performance 
 hardly adequate detail , necessity 
 rehearsal ; work , regard series 
 orchestral tone - picture , effective , high 
 order , handling orchestra certainly 
 able . ' Norfolk Rhapsody ' enjoyable , 
 César Franck ' Les Djinns , ' Miss Kathleen Frise - Smith 
 solo pianist , Mr. Frank Mullings interesting vocal 
 solo , feature programme , , 
 rich . Mr. Fricker conduct . Leeds 
 University recital February 6 Mr. 
 Herbert Johnson , play interesting series 
 pianoforte piece , include Schumann G minor Sonata 
 transcription Bach Organ Toccata D minor , 
 brilliant style . Mr. Hoggett , teach music 
 University , series lecture - recital 
 Beethoven Sonatas , begin January 30 
 exposition Sonatina G ( Op . 79 ) , clear line 
 serve purpose analysis . event 
 common importance visit Mr. Alfred 
 Mallinson native town , , co - operation 
 wife vocalist , , February 7 9 , recital 
 song , manifest extraordinary gift 
 possess impart vivid expression verse 
 varied sentiment type . pianoforte — ' accom- 
 paniments ' hardly word — wonderfully suggestive , 
 fail require atmosphere , 
 especially play exceptional 
 subtlety power 

 BRADFORD 

 J. Harrison 3d 

 true love hath heart ( 4 v. ) 
 W. A. C. Cruickshank 3d . 
 ° R. Schumann ad . 
 night Hamish MacCunn 3d . 
 night music H. W. Wareing 3d . 
 . * night sinkson wave Henry Smart 2d 
 Night song ( 2 v. ) Martin Roeder 3d . 
 * Nightingale , 
 Nightingale , ( Arr . H. Leslie ) 
 % . Weelkes 2d . 
 night , G , Roberti 3d . 
 noble thy life ( Canon 6 v. ) 
 Beethoven 3d . 
 northern love song , H. Hofmann 2d . 
 rose ( 4 v. ) Brahms 3d . 
 ( 4 v. ) Mendelssohn 2d . 
 sleep crimson ( 4 v. ) 
 G. von Holst 
 * Nan , ( 4 v. ) ... « » J. Brahms 
 Nurse Song ( 2 v. ) B. Luard - Selby 2d . 
 Nymphs Rhine , Marschner 4d . 
 o beautiful violet ( 2 v. ) C. Reinecke ad . 
 4 clap hand E. H. Thorne 6d . 
 rateful evening . C. Reinecke 2d . 
 Siew fair ( Arr . 
 Henry Leslie ) Shield 3d 
 o Lord , thou hast search ( Surrexit 
 Pastor Bonus ) , 4 v. Mendelssohn 
 o memory - H. Leslie 
 o love . Hamish MacCunn 
 57 . " o praise Lord ( laudate pueri ) 
 Mendelssohn 
 * s0 sing God ( noél ) ... Ch . Gounod 
 oh , Skylark , thy wing H. Smart 
 ans . oh , Spring Marie Wurm 
 364 . * o swallow , swallow G. von Holst 
 430 . " oh , merry P. E. Fletcher 3d . 
 228 . othou breeze Spring King Hall 3d . 
 258.§ o thou divine ( 2 v. ) A. C. Mackenzie 
 105 . o , thou art Hauptmann 
 289 . o worship Lord ( 4v . ) 
 S. S. Wesley 
 oak thy mournful bier ' prepare 
 C. Reinecke 
 456 © Ona fade violet Hamish MacCunn 
 52 . * ondeparture ... . Franz Abt 
 197 . land afar extend G. Bartel 
 142 . day Franz Abt 
 » day come Lochlin 

 Nanie 

