inferred 
 Hatton compositions come notice 
 Society British Musicians , met 
 approval experienced musician 
 ‘ dear old Attwood 

 Hatton instrumentally inclined 
 productive period career evidenced 
 collection old programmes belonged 
 late Mr. H. J. B. Dando , 
 possession present writer . 
 programmes , printed pink card , concert , 
 presumably private , given ‘ Thursday 
 evening , 18th April . ’ Unfortunately yearnor 
 place given ; selection — included 
 Beethoven second symphony string quartet 
 , ‘ Zauberfléte ’ overture — contained 
 ballad John Barnett opera ‘ Farinelli , ’ 
 produced early 1839 , year probably 
 concert given . events 
 programme contains Quintett pianoforte , 
 violin , tenor , violoncello , double bass , 
 Hatton , mentioned 
 biographers . Efforts trace place giver 
 concert far failed 

 find composer Drury Lane 
 Theatre . July , 1842 , engaged 
 Macready chorus - master salary 
 guineas week . season opened October 1 . 
 February 23 , 1843 , new operetta act 
 produced entitled ‘ Queen Thames , , 
 Anglers . ’ composer 
 withheld representation ( March 6 ) , 
 chorus - master ( /.e . , Hatton ) received 
 publicity . successful piece 
 ran , intervals , nights , 
 probably longer career incident 
 presently mentioned . songs 
 ‘ Alas ! unwed - ’ ‘ Blind man 
 buff , ’ madrigal , ‘ merry bridal bells , ’ 
 described ‘ good deal better 

 Mr. J ] . L. Hatton returned London Saturday 
 [ April 6 , 1844 ] Vienna , super- 
 intending representation opera , 
 composition , highly successful ; Mdlle . Lutzer 
 Herr Staudigl sustained principal characters , 
 took warmest interest production 
 countryman 

 Philharmonic concert 27 , 1844 , 
 duet , ‘ Stung horror , ’ ‘ Pascal Bruno , ’ 
 sung Miss Rainforth Herr Staudigl . 
 concert conducted Mendelssohn , 
 Wedding march performed time 
 country , Joachim , aged fourteen , astonished 
 everybody wonderful interpretation 
 Beethoven Violin concerto . words 
 duet doggerel ‘ 
 Revenge . ’ began 

 Pascal . Stung horror , shame , anguish 

 sets , making eighteen 
 songs , followed ‘ order ’ Mr. Oliphant 
 ‘ exclusive property , ’ 
 ‘ Simon cellarer , ’ , said , 
 remunerated composer £ 10 note 

 excellent pianist , Hatton successful 
 appearances Melodists ’ Club London , 
 member , Hereford 
 Musical Festival 1846 . occasion , 
 miscellaneous concerts 
 Shire Hall , played pianoforte 
 Beethoven early Quintet E flat pianoforte 
 wind instruments , favoured audience 
 ‘ Le Savoyard , ’ described ‘ humorous 
 chansonette composition sung 
 accompanied . ’ second concert 
 played solo Mozart Pianoforte 
 sania D minor . ‘ works 
 immortal master ’ ( said Zhe Times ) , ‘ 
 enriched repertory piano greater 
 extent composer , 
 universal Beethoven , neglected 
 performers , Mr. J. L. Hatton deserves 
 credit endeavouring bring notice . ’ 
 Hatton ‘ Czapek ’ * Chapel ’ 
 ‘ shepherd winter song’—were sung 
 festival . Concerning compositions , 
 Times critic — internal evidence , Mr. J. W. 
 Davison — said 

 songs 

 Handel , Mozart , 
 sentimental 

 Beethoven , & c. 
 

 sang songs comic , | 
 levied contributions | 
 nations compositions , kept audience 
 alternately moved delight excited laughter . | 
 Mr. Hatton modest obtrude | 
 works visitors . ‘ Adventures | 
 Robinson Crusoe 

 f | characteristics happily caught 
 o > — = f — . . r . . Bw 
 5 — ag — = = F |excellent caricature Mr. Charles Lyall , 
 - - kindly allowed reproduce . 
 — — § o- Hatton nautical proclivities musically reflected 
 e—~—_—s — = - . « @ » ? oe 4 ; 
 ne ES SS ‘ Songs sailors . ’ Freemason 

 Beethoven . like sound 5ths — look 

 é ores 
 = = s 

 Wednesday , 26th August , 1846 , conducted | soldier diplomat , ’ ‘ Dictionary 
 ‘ Elijah , ’ 25th ° ) , performance | National Biography ’ states . succeeded 
 Friday ( 28th ) , long chat 

 S 3 sh 5 j 45 > 
 subject 3 quartetts spoke writing , - — title , , = — ahager oo 1822 
 parted time , alas ! spend days e formation - = e ~ woyal é nea . Music , 
 evenings came England . took tive interest 
 visits delight advantage leading remainder life . highly esteemed 
 chamber music guidance Mr. Alsager Berlin — resident minister 1541 
 house Queen Square , Bloomsbury , | 1851 — news death reached 
 occasions , , played tenor parts | capital , principal military bands assembled 
 chamber works . played presence Prince Regent distinguished 
 juite , stop , ‘ , want| company , performed Beethoven Funeral march , 
 ; , laugh , play / q favourite piece Earl . voluminous 
 notes , _ gene r ar [ | composer , output included seven Italian operas , 
 way Teed hardly tll T ad bette |chree cantatas , masses , cathedral services , anthems 
 tenor playings called hymns , madrigals , ‘ vocal MUSIC , 
 ‘ colleague . ’ | printed manuscript . 
 attempts arrange night | forgotten Richard Fitzwilliam , seventh 
 Quartett concerts [ Crosby Hall ] , success . | \ iscount ( 1745- 191 6 ) , founded Fitzwilliam Museum . 
 arrived 1844 fixed concert . | Musical Times March April , 1903 , 
 glad tidings announced subscribers| contained illustrated article important 
 verily besieged tickets , having space | treasure - house . 
 concert - room , landing staircase | 
 crowded listeners . gave magnificent impromptu ; _ _ , : : 
 performance forgotten who| _ peer survey obtained world - wide 
 fortunate hear it.—Yours truly , fame father great Duke Wellington . 
 J. H. B. Danpo . music , Garrett Colley Wellesley ( Wesley ’ , 
 | Earl Mornington ( 1735 - 1781 ) , widely favourably 
 |known composer ‘ cool grot 
 mossy cell ’ ( prize glee ) , excellent double 
 chants . musical sensibilities Lordship 
 displayed aremarkable manner whilst nurse 
 co “ sgh . arms , long speak . elected 
 ey peg ‘ aan mw 4 — s | professor music University Dublin 

 1764 ) , conferred degree 
 encore extemporised earlier numbers | doctor music 

 Rome , 1630 

 Turning Department Manuscripts , 
 satisfactory learn regard Catalogue 
 Music ‘ revision descriptions vol . iii . 
 Instrumental Music ) completed , 
 text printed . ’ early issue book 
 reference expected . Foremost 
 acquisitions bequest Miss Harriet 
 C. Plowden Beethoven Sonata violin 
 pianoforte G ( Op . 30 , . 3 ) , String 
 quartets Mozart described / usica/ 
 Times September , 1907 . Owing question 
 doubts ownership treasures delivered 
 Museum November . acquisi- 
 tions described 

 Messe Solennelle ( Ste . Cécile ) , score , Charles 
 Gounod ; 1852 . Autograph 

 place , increasing accommodation . ’ Sir George 
 Smart , conducted , received Chevalier 
 fee £ 157 105 . Miss Stephens ( ‘ Kitty ’ Stephens 
 John Braham paid £ 189 , Mr 
 Henshaw , organist Durham Cathedral , received 
 £ 12 assisting Mr. Thompson , organist 
 church - festival ; gentleman 
 gave fee £ 10 charities 

 regard music performed festival 
 1824 , marked improvement shown pro- 
 grammes evening concerts . Symphonies 
 Haydn , Mozart ( Jupiter ) , Beethoven ( key stated 
 played , following overtures : ‘ Die 
 Zauberfléte , ‘ Der Freischiitz , ’ ‘ Anacreon . ’ 
 lighter fare programmes included ‘ Charlie 
 darling’—which ‘ gave great offence , 
 substituted Chevalier song 
 consulting committee — ‘ Scots wha hae ’ 
 sung Braham ) ‘ Fantasia Mandolin ; 
 composed performed Signor Vimercati , 
 ‘ curious astonishing . ’ 
 complete oratorio given church 
 ‘ Messiah ’ ; selections ‘ Israel 
 Egypt , ’ ‘ Judas Maccabieus , ’ ‘ Creation , ’ ‘ Seasons ; 
 ‘ Requiem Mass ’ ( Mozart ) , & c. ‘ Grand chorus 
 fugue ’ Mozart ‘ arranged orchestra , 
 Sir George Smart , ’ words ‘ O heavenly Lord ! 
 Almighty amidst mightiest . ’ Madame 
 Catalani , occasion , condescended allow 
 Mr. Braham sing opening solos ‘ Messiah , 
 retained ‘ despised , ’ transposed 
 key G ! Furthermore , ‘ accommodate Madame 
 Catalani ’ , ‘ Lord shall reign ’ ( ‘ Israel Egypt ’ 
 transposed B flat 

 festival held Newcastle September , 
 7842 . previous occasions occupied days , 
 Sir George Smart conducted . sacred 
 performances , given church , largely 
 inevitable ( days ) ‘ selection ’ nature — e.g , 
 ‘ Israel Egypt , ’ ‘ Mount Olives , ’ ‘ Creation , ’ c. 
 ‘ Messiah ’ performed entire , Kossini 
 ‘ Stabat Mater , ’ comparatively new England . 
 ‘ Stabat went new English 
 words , selected adapted , 
 principally church liturgy ’ : records 
 Musical \World long notice festival . 
 programmes evening concerts 
 commend . symphonies performed 
 Haydn E flat , Mozart E flat , Beethoven 
 C minor . overtures played — Die Zauber- 
 fléte , Fidelio , Euryanthe , Der Berggeist , Mid- 
 summer Night Dream , addition violin 
 violoncello concerto , septets found places 
 scheme , Neukomm Beethoven . 
 ‘ grand fancy dress ball ’ set seal festival 
 1842 

 Sir George Smart , punctilious punctual , 
 annotated copies word - books , gives 
 amusing sidelights festival . records 
 * Merry - faced Lindley [ violoncellist ] -took 
 accustomed pinch snuff amid cheers 
 audience . ’ Concerning Duke Cambridge , 

 Miss Agnes Nicholls Mr. John Coates 

 Mass D Beethoven , - work frequently 

 morning . trying choral writing strained powers | 
 choir somewhat , singers } 
 commended courage , result | 
 successful securing effect Beethoven desired causing | 
 work awaken religious feelings singers 
 asin hearers . Dr. Sinclair interpretation tempi 
 liberal , dragged . solo quartet , consisting | 
 Miss Perceval Allen , Madame Ada Crossley , Mr. Gervase | 
 Elwes , Mr. Robert Radford , chosen | 
 performance weak spots festival . | 
 artistic atmosphere restored second | 
 magnificent interpretation Sir Edward Elgar symphony | 
 baton composer . best | 
 performances given , work , heard | 
 cathedral time , exercised new appeal . 
 Motet Bach , ‘ afraid , ’ double choir , | 
 attacked , served reveal deficiency choir 
 important quality incisiveness 

 _ evening programme consisted | 
 Sir Hubert Parry oratorio ‘ Job , ’ heard | 
 meeting seventeen years ago . work well| 
 entitled consideration present day . choral | 
 writing best Sir Hubert Parry , | 
 originality modernity | 
 long monologue Job , delivered | 
 eflect Mr. Frederic Austin . Mr. Walter Hyde | 
 good impression music Satan , Master 
 Tidmarsh gave music Shepherd , | 
 case articulation words clear . 

 forecasting season events , place honour 
 assigned Birmingham Festival Choral 
 Society ( conductor , Dr. Sinclair ) , intend 
 subscription concerts addition Christmas perform- 
 ance ‘ Messiah . ” works given include 
 ‘ Creation , ’ ‘ Samson Delilah , ’ ‘ Hiawatha ’ 
 cycle 

 Midland Musical Society , conducted Mr. A. J. 
 Cotton , announce Cowen ‘ Sleeping beauty , ’ ‘ Messiah , ’ 
 ‘ Hiawatha ’ cycle , Dvordk ‘ Stabat Mater , ’ Beethoven 
 Mass C 

 Birmingham Choral Orchestral Association 
 , Mr. Joseph H. Adams direction , German 
 ‘ Merrie England , ’ Brahms ‘ Song Destiny , ’ Schubert 
 ‘ Song Miriam , ’ Elgar suite ‘ Bavarian 
 Highlands , ’ Handel selection 

 Sine Nomine Choral Society , conductor Mr. R. 
 Simmons , practising Gade ‘ Crusaders 

 Bath 
 Heymann , resumed September 25 . 
 Symphony Concerts performance 
 Beethoven , Brahms Tchaikovsky 
 chronological order 

 MUSIC GLASGOW 

 cathedral . addition cathedral organist , 
 performers Dr. Basil Harwood Mr. C. H. Moody . 
 following forecast shows , coming 
 promises interest . Choral 
 Orchestral Union scheme includes - concerts . 
 Dr. Cowen conductor - - chief Scottish 
 Orchestra , conductors Dr. Richter 
 Messrs. Wassili Safonoff Henri Verbrugghen . 
 choral works performed ‘ Messiah , ’ ‘ Acis 
 Galatea , ’ ‘ St. Paul ’ ( I. ) , Cliffe ‘ Ode 

 North - east wind , ’ Bach Mass B minor Beethoven 

 , - named , 
 Pollokshields 

 
 important step adopting French pitch , coming 

 Nl 
 | Teachers ’ Choral Society ( Mr. Alec Steven , conductor ) 
 MacCunn ‘ Lord Ullin Daughter , ’ Mendelssohn 
 ‘ Hear prayer , ’ miscellaneous pieces . 
 Athenzum School Music Operatic Choral Societies , 
 joined Mr. Henri Verbrugghen direction , 
 announce formidable programme , viz . , ‘ Cavalleria 
 Rusticana , ’ ‘ Lohengrin , ’ ‘ Les noces de Jeanette , ’ 
 Mendelssohn ‘ Hymn Praise , ’ Beethoven ‘ Kuins 
 Athens . ’ 
 | University Choral Society , conducted Mr. A. 
 M. Henderson , perform Dunbhill ‘ Tubal Cain , ’ 
 Somervell ‘ Earl Haldan daughter , ’ Grieg ‘ Kecognition 
 land , ’ selection madrigals - songs . 
 concert Greenock Choral Union ( Mr. W. T. 
 Hoeck , conductor ) prepare parts 1 2 Coleridge- 
 Taylor ‘ Hiawatha , ’ Dumbarton Choral Union ( Mr. 
 |E Owston , conductor ) concert - performance 
 | * Maritana ’ ‘ Bohemian girl 

 MUSIC LIVERPOOL 

 Methodist Choral Union , conductor Mr. Percival H. 
 Ingram , announces concerts Handel ‘ Samson ’ 
 performed Bootle , ‘ Messiah ’ 
 ‘ Creation ’ Philharmonic Hall 

 Chamber music represented Schiever 
 classical chamber concerts Mr. Rawdon Briggs string 
 quartet , Mr. Egon Petri play Beethoven 
 recitals pianoforte sonatas master 

 Mr. Donald Francis Tovey initiate discourses 
 connection Music Lectures Association 
 University , October 5 

 works performed , audiences receive valuable 

 assistance excellent analytical programmes prepared 
 Mr. A. Corbett - Smith . addition masterpieces 
 music — e.g. , Beethoven Mozart symphonies — 
 works British composers receive attention , names 
 Elgar , Sterndale Bennett , Sullivan , Stanford Edward 
 German included past programmes . prospectus 
 forthcoming season comprises Elgar Variations , 
 Zantock ‘ Helena ’ variations , German ‘ Seasons ’ suite , 
 Parry Symphonic variations . Special programmes 
 provided time time illustrating particular 
 composer period musical history . cordially wish 
 success enterprise , trust excellent 
 example induce municipal authorities ‘ anc 
 likewise 

 works gi ' 
 ! 
 
 rforma ! 
 Biegfried 
 ‘ Ring , ’ | 
 particular 
 solo s 
 high 
 Lohengri 
 ( Kundry 
 gooc 
 Dr. Hs 
 desk 

 MUNICH 

 festival performances Mozart operas given 
 Residenztheater included ‘ Le nozze di Figaro , ’ ‘ Die 
 Entfiihrung aus dem Serail , ’ ‘ Don Giovanni , ’ ‘ Cosi fan 
 tutte , ’ skilfully conducted Herr 
 Felix Mottl , Mozart performances followed 
 ‘ Wagnerfestspiele ’ Prinzregententheater , 
 following operas given : ‘ Tannhaiiser , ’ ‘ Die 
 Meistersinger , ’ ‘ Tristan und Isolde , ’ ‘ Ring des 
 Nibelungen , ’ respective batons Messrs. 
 Mottl , Rohr , Franz Fischer . Konzertverein , 
 conductorship Ferdinand Léwe , gave series 
 symphony concerts devoted works Beet- 
 hoven , Anton Bruckner , Brahms . programmes 
 comprised Beethoven symphonies , 

 performed 

 result labours 

 Thieriot ) ; Danses pseudo - classiques en forme de quadrille | F aA ’ 
 d’aprés des themes de J. S. Bach ( Delaborde ) ; Gaudeamus| CAPE Town.—The Philharmonic Society gave second 
 igitur ( Liszt ) ; O du lieber Augustin ( Max Reger ) ; | concert season City Hall August 18 . 
 S ’ kommt Vogel ( Ochs E. Scherz ) . | programme comprised Beethoven ‘ Egmont ’ overture 

 CH R , ‘ cial | Mendelssohn ‘ Scotch ’ symphony , Handel ‘ Largo 

 pupus f - 1 
 Vol teste F advi free . Wi ite appointment 

 M D 
 M Ust ' STUDIOS , Teaching Practice . 
 1 f ' apply , 7 ( Beethoven House , Georg 

 H House , H Street , Sutt Surre 

 Ascher , Vieuxtemps , Marchant , Lefebure Wely 

 Merkel , Leybach , Beethoven , Liszt , Ernst , Bazzini , Klein , 
 Humperdinck , Nevin . 
 Votume III . 
 GREEN ORGAN ALBUM 
 FAMOUS PIECES ( OriGInaL ARRANGED ) 
 ELECTED EDITI Y 
 REGINALD GOSS - CUSTARD . 
 Price Shillings net . 
 ScuotTr & Co. , 
 157 , Regent Street , 48 , ( sreat Marlborough Street , London , W 

 RECITAL WORKS ORGAN 

