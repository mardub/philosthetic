School Rooms, &c

GRAND PIANOS are offered to the public as the genuine Inst — 
ments, I think it due to the Musical Profession, Amateurs, and 
Trade, to make the following statement :— Nine years ago, 
much thought and labour, I succeeded in completing 
Grand, adopting the cross-string principle of Steinway, 
the size, and making many 
and action; andit is well known to the musical world of G 
that I was the originator of these Pianos. 
ditional improvements have been effected, the sale has 
increased, and at the present time I have more than 250 work- 
men engaged exclusively on their manufacture. The acknowl 
superiority of these Pianofortes to any other description of the same 
size has caused them to be @losely imitated by various makers ; in 
one instance, the name “ Beethoven Grand,” which I originally used 
as a distinctive trade mark, but have since abandoned, has been: 
ed; and with unexampled modesty, the public is now invite 
accept the copy as the origina , and to consider the genuine as spurious 
Among the numerous testimonials I have received 
medals), I may be permitted to mention that his Majesty the King 
Saxony, who takes great interest in Art and Manvfactures, and i¢i@ | 
addition an excellent musician, honoured my manufactory witha v

on February 25th last, and was pleased to express his entire satisfaction 
The Depét for my Pianofortes in London is at Mr. FLAVELL'S; 2 4 
North Audley Street, W., where they may be seen in great varicly— 
ERNST KAPS. Sole ImporterCuHar.Les RussELL. me

NEW PIECES, sy LOUIS DUPUIS

GEMS OF SACRED ART :— s. d. 
No. 1. HENRY VII.’S CHAPEL ... -~ 3:0 
Beethoven's “ Hallelujah,” from Mount of Oliv es

2. ST. GEORGE’S ee EL.. oa 3 0 
Jackson's Te Deum in

COVENT GARDEN THEATRE

On Saturday the 8th ult., a series of Promenade 
Concerts, under the direction of M. Hervé, was inaugu- 
rated at this establishment with every prospect of a 
success which must be infinitely more gratifying to the 
proprietors than to the lovers of art. M. Hervé’s Sym- 
phony, ‘‘ The Ashantee War,”’ appeals not to the criticism 
of a musical journal, and we leave it therefore to the 
tender mercy of those for whom it was manufactured. 
The violinist, M. Wieniawski, created a decided effect by 
his performance of Bach’s Prelude to his violin Sonata in 
E major, but the patchwork nature of the concert may be 
imagined when we state that for the encore of this piece, 
he substituted a Caprice on ‘* Willie, we have missed you.” 
Another feature in the programme was the trombone playing 
of Mr. J. Harvey, which created genuine and deserved 
applause. Madlle. Benati, Madlle. Bianchi, Mr. Pearson, 
Mr. Carlton, and Mr. Lewis Thomas have been the prin- 
cipal vocalists. The fact of the Allegretto from Beethoven’s 
Symphony in F (No. 8) and the Overtures to ‘ Guillaume 
Tell” and “‘ Masaniello”’ being taken out of the bills after 
the first evening will sufficiently indicate the taste of the new 
Conductor. We may say, however, that ‘‘ Classical nights” 
are included in the programme of the season

THE opening of the magnificent new Guildhall at 
Plymouth, by His Royal Highness the Prince of Wales, 
which took place on the 13th ult., was followed by a 
Musical Festival, which reflected the utmost credit upon 
Mr. F. N. Lohr and the members of the Plymouth Vocal 
Association under his direction, upon whom the success of 
the undertaking mainly depended. On the first day, 
Mendelssohn’s “Elijah” was selected for performance, 
the principal parts being sustained by Mdlle. Corani, 
Mdile. Mathilde Enequist, Miss Julia Elton, Miss Helen 
D’Alton, Mr. E. Lloyd, Mr. Kerr Gedge, Mr. W. Drayton 
and Signor Agnesi. 
“Hear ye Israel,” was highly effective, Mdlle. Corani 
created a decided impression by her singing in the duet of 
the Widow with Elijah, Miss D’Alton was successful in 
the air, ‘‘ Woe unto him,” and Miss Julia Elton’s render- 
ing of ‘‘O rest in the Lord” was so excellent as to cause 
an evident desire on the part of the audience to hear it 
ence more. Mr. Lloyd’s two tenor solos were delivered 
with true musical expression, and the part of the 
Prophet was sung by Signor Agnesi with such a reverential 
feeling for the excessive beauty of the music as to con- 
vince us that he will take a foremost rank as an Oratorio 
singer. The choruses were given with much precision, 
and with such judicious alternations of light and shade as 
to evidence the care with which the work had been pre

610 THE MUSICAL TIMES.—Sepremser 1, 1874

able to recognise his own melody.” He also devotes 
some portion of his essay to the consideration of the 
‘‘portamento,”’ and endeavours to separate this from that 
fatal habit of crawling up to a note, so often heard in the 
psalm-singing of a mixed congregation, and even (as he 
gently hints) occasionally in our public concert-rooms, 
from some of our recognized solo vocalists. The amateur 
will derive much benefit from reading attentively what 
Mr. Cooper says about the necessity of fully understanding 
the intention of the composer in every work to be per- 
formed ; for facility in reading notes too often leads to the 
habit of considering that mere accuracy in time and tune 
is all that is demanded. ‘ Why is it,” writes our author, 
“that Joachim delights us more than any other violin 
player? Not because he produces greater tone, but be- 
cause he displays greater refinement of feeling and con- 
ception of melodic form and phrasing; so that, whether we 
listen to his playing either a quaint melody of Bach, or 
one of the grand compositions of Beethoven, he leaves 
nothing to desire.” This observation is as applicable to 
vocal as to instrumental music; and, indeed, perhaps even 
more so, for the human voice, if properly trained, exercises 
a more potent spell over the listener than any artificial in- 
strument, and Herr Joachim himself is but a singer on the 
violin

AUGENER AND Co

Buxton.—A special concert was given on Thursday evening the 30th 
July, which was an immense success. The Pavilion was filled, and

sented a most animated appearance. The band, led by Mr. Julian 
toes, and augmented for the occasion, commenced the concert with 
Weber's Overture to Der Freyschiitz, which was played in magnificent 
style. Madame O. Williams sang Smart's “ Sailor’s Story,” with an 
expression and taste which at once gained the good opinion of her 
audience. Beethoven's Sonata for pianoforte and violin, by Mr. 
Adams and Mr. Otto Bernhardt, was rendered to perfection. The 
appearance of Mr. Sims Reeves was greeted with a storm of wel- 
come; and his singing of Beethoven's “ Adelaide” was applauded in 
a manner which brought him again on the platform to bow his 
acknowledgments. A similar compliment was paid to Madame 
Williams when she sang “The Skipper and his Boy” (Virginia 
Gabriel). Weber's ‘‘Concert-stiick,” well played by Mr. Julian 
Adams, concluded the first part. In the second part Mr. Sims Reeves 
, ‘Come into the garden, Maud,” and was so energetically and 
heartily encored that he repeated the last verse. Haydn’s “ Surprise” 
Symphony was played with great precision, and was followed by 
t the audience evidently considered the vocal gem of the evening. 
“My Pretty Jane” by Mr. Sims Reeves, which produced rounds of 
ise, but failed to bring forward Mr. Reeves. The concert closed

with Gungl’s grand march, “ Alexandre

SNEFFIELD.—On the 4th ult. Mr. Charles Harvey gave a concert 
and organ recital inthe Albert Hall; vocalist, Miss Bolingbroke, violin, 
Mr. J. Peck, and organ, Mr. F. Archer. Mr. Peck played with his 
accustomed ability, and Mr. Archer in his fantasia on Scotch airs and 
a selection from I] Talismano, especially, gave great satisfaction, 
a concerto by the two instrumentalists being also given in a masterly 
style-—On the 6th ult. Mr. William Pyatt, of Nottingham, gave a 
concert, at which Miss Emma Beasley, Madame Williams, Mr. Sims 
Reeves, and Signor Foli were the vocalists, and Herr Otto Bernhardt 
solo violinist. The reception given to the artists was of a highly 
gratifying character. Mr. Sims Reeves sang “‘ Adelaide” and “‘ My 
Pretty Jane,” in response to an encore for which he gave “ Tom 
Bowling.” Signor Foli, in the duet “All's Well,” with Mr. Reeves, 
as well as in his songs, “ The Diver,” Pinsuti’s “ Raft,” and “ The 
Vicar of Bray,” sang magnificently. Miss Beasley, in ‘‘ Bid me dis- 
course,” gave great satisfaction, and Madame Williams was encored 
for her rendering of ‘‘ Huntingtower.” Two solos by Herr Bernhardt 
were well-executed, and greatly appreciated. An excellent choir of 
16 voices, under the direction = | Mr. G. H. Smith, gave a good selec- 
tion of part-songs in a most able manner. Mr. S. Naylor was accom- 
panist

SKELMORLIE.—On Tuesday evening, the 11th ult., an Organ Recital 
was given in the Parish Church, by Mr. John E. Senior, the organist. 
The programme, selected from the works of Bach, Weber, Beethoven, 
Spohr, Batiste, and Sir G. Elvey, was rendered in a highly satis- 
factory manner. Solos by Gounod, Handel, and Haydn were ex- 
cellently sung by two amateurs

Toronto, CANADA.—The Philharmonic Society, under the able 
conductorship of Mr. F. H. Torrington, has during the last season 
given the Messiah twice, and Elijah three times. The next series of 
concerts will include the Creation, St. Paul, the Messiah, and 
probably Acis and Galatea, or Signor Randegger’s Fridolin

Deutschland and Freedom evermore " (for voices only), the words 
translated from a German Poem by Ferdinand Freiligrath. The music 
composed for and performed at the Leeds Musical Festival, October 
3874. 8vo. 1s

ORDAN, C. WARWICK.—A short Paper on tha 
Construction of the Gregorian Tones, read at the.General Meeting 
of the London Gregorian Choral Association at Sion College, Nov. 
27, 1874. 
EST, W. T.—Arrangements for the Organ. No. 
99, price 2s., contains :—Baptismal Song, Meyerbeer; Air with 
variations from Symphony, in D, Haydn; Romance for Violin and 
Orchestra, F major, Op. 50, Beethoven. No. 100, Allegretto"(Gratu- 
lations Menuet), Beethoven (Posthumous Work); Allegretto, from 
the Violin Sonata in A major, Handel; Prelude and Fugue, on the 
name of “ Bach,” J. S. Bach. 
London : Novello, Ewer and Co., 1, Berners-street, W

Israel

Price 2s. 6d. each; or in scarlet cloth, 4s

NOW READY. 
BEETHOVEN’S FIDELIO

With German and English words

