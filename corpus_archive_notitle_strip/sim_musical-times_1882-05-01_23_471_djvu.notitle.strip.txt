lral, together

WEDNESDAY, June 7 = 2d 30 a.m., Mendelssohn's ‘ JAH.” 
THURSDAY, jun ae | a.m., Sullivan’s “ P PRODIG AL 
SON 1 a Beethoven SYMPHONY

THURSDAY, at 2.45 p.m., Spohr’s “ LAST JUDGMENT

s. d

WAGNER’S “ LOHENGRIN” .. aie aa Se 6 
7” “ TANNHAUSER”? ma ae Se 16 
es ‘Frying DuTcHMAN” aa § 6 
“ ‘¢ TRISTAN UND ISOLDE” 10 0 
BEETHOVEN'S “ FIDELIO”’.. ee < 3 6

London: NovetLo, Ewer and Co

large and| 
vivifying influence of continental art, and it is only 
inter

be studied and encouraged here as much as it is in 
France and Italy. This is the only country in Europe 
where opera is still regarded as something exotic, 
and ours is the only literature in which it is almost 
universally treated in a spirit of mingled flippancy 
and unbelief. Much of this spirit of irreverence is 
due to the old and favourite assumption that any- 
thing in the shape of a libretto was good enough for 
musical treatment. Hence the degradation of the 
once honoured name of opera. With the complaint 
that opera had become degraded arises the question, 
when and by whom was the downward movement 
towards complete degeneracy detected? Gluck, 
Beethoven, and Weber—all three—made successful 
efforts towards its elevation and regeneration. The 
former, particularly, not only in his remarkably dra- 
matic compositions, but in his writings and letters, 
shows a clear sense of the vastness of the unexplored 
future for the music-drama. ‘There need be no fear 
that the coming composers of dramatic music in 
England will necessarily be dependent upon the form 
and. structure of the Wagnerian operas, although they 
will be exponents of the so-called W agnerian theories, 
For the chief of these theorics—those essential to 
the highest development of the drama in music—were 
articles of faith with Gluck and Beethoven, and 
were with them the recognition of certain great 
fundamental truths, the unnecessarily clamorous 
annunciation of which by the followers of Wagne1 
alone has prevented their universal acceptation as 
truisms. ‘The pamphleteering army, in the guise oi 
pioneers, have scaled certain well-marked heights 
and taken possession of them in the name of Wagner, 
making them the h-adquarters of a vigorous pro- 
paganda and the bas: of operations in the future, 
apparently oblivious of the fact that these heights 
had been previously occupied by Gluck and others, 
and that Mozart was not unacquainted with them. 
But whatever the degree of influence to be exercised 
on English Opera in the future by the teaching and 
example of the great master of modern music-drama, 
be little doubt that through it a great 
stimulus is given to composers, and that a revival 
lof English Opera is no longer to be regarded as 
visionary

THE NIBELUNG’S RING” 
AN ANALYSIS OF RICHARD WAGNER'S TRILOGY 
By F. CorDer. 
THE VALKYRIE (continued from page 190

teristic

alities—as Russia and Hungary—the music of 
which is chiefly remarkable for strangeness of 
rhythm, or—as Scandinavia—for strongly marked 
and erratic forms of melody. This preference 
may be either the result of inclination or policy, but 
in any case it illustrates one of the tendencies 
of the present musical age, which, in its restless 
search after novelty, leaves no hole unexplored, 
no corner unswept. The great masters of eighty 
years ago were satisfied to introduce a national 
melody once or twice in a lifetime, by way of compli- 
ment—as Beethoven—or for the indulgence of a whim 
—as Haydn. But now we have symphonies and 
concertos named after a half-dozen countries, while 
in the lower walks of Art classic themes and rhythms 
are being elbowed out by strong and uncouth barba- 
rians. ‘There is nothing in all this to excite alarm, 
but rather hope. The ancient hero who, struck to 
the earth, sprang up with renewed strength gained 
from the Great Mother, is a type of many things, 
and every form of Art would become effete 
were it not invigorated at intervals by inoculation 
with the sap of nature in its untutored naturalness. 
Thus it is emphatically with music, and when, in 
course of time, culture has taken off the roughness of

254

_|derives from it greater enjoyment when free to make

his own interpretation—granted that he is musi- 
cally more robust than another who cannot thus stand 
on his own legs, yet the craving for a poetic designa- 
tion or application has its roots somewhere in poetic 
instinct. At any rate, composers and publishers 
must reckon with it, and, within due bounds, may 
not only gratify it, but use it as a means for popu- 
larising art. Beethoven's Sonata in C sharp minor, 
had it never found another name, would have ranked 
among the choicest gems of music, but when Cranz 
of Hamburg called it the ‘ Moonlight ” he gave an 
impetus to the work which, even now, carries it easily 
where access would otherwise have been difficult. 
The title—we will not stop to discuss its propriety— 
invests the music with a poetic significance, and 
arrests the attention in thousands of cases where, 
without it, neither the one result nor the other would 
have been possible. At the same time this characteri- 
sation of music may be carried too far, and easily 
becomes exaggerated andcontemptible. The noblest 
and purest music is always that which appeals 
directly and unaided to fancy and feeling, moving 
both without stimulus from, or guidance by exterior 
things. Among the writers of characteristic music, 
Hofmann, as we have said, is a very indus- 
trious and comprehensive labourer, dividing his 
attention, like Schumann, almost equally between 
objective and subjective themes. Schumann, we 
cannot but fancy, has had considerable influence 
over him in this respect—not, perhaps, as materially 
affecting his method, but as determining the nature 
of his work. Some recognition of the fact may have 
been made by Hofmann himself, when he dedicated 
the twelve pieces entitled ‘‘ From my Diary” to the 
widow of the illustrious man. The very names of these

pieces suggest Schumann: ‘ Discourse,” “ Hunting 
Scene,” ‘The Nightingale sings,” ‘*The last Fare- 
well,” ** Snowflakes "—these all come to us like Schu- 
mann echoes. Let us add that they are not unworthy 
of the connection. Schumann could never have writ- 
ten them, it is true, any more than Hofmann could 
write the dead master’s “Carnival” scenes; but, 
in their way, the twelve pages from the “ Diary” 
are as charming in fancy and as graceful in

Hofmann is

alive and the spirit buoyant, but the subtler quality 
|in which Beethoven, Mozart, and especially Haydn, 
| were so rich, seems to us wanting. It must be said 
|for Hofmann that he rarely places himself in a 
position to make his deficiency of humour con- 
spicuous. His natural habitat is among the gravities 
and gentler graces of nature and humanity, of which 
, he may rightly claim_to rank as at once expositor 
; and poet

Referring again to the ‘Italian Love Tale,” evi- 
dence that Hofmann knows how to utter the musical 
| language of the divine passion is foundin an Andante 
|; sostenuto which seeks to represent a dialogue, after 
| the fashion of Mendelssohn's well-known * Song with- 
|out Words.” In essential respects this effort is a 
|complete success. The joy and fear of love mingle 
| in true proportion. Peace and agitation succeed 
}each other as they should, while the music is not 
| without the climax which seems eager, in the abun- 
| dance of its feeling, to transcend the power of art. We 
look for this in No. 3 of six pieces designated “* The 
Trumpeter of Sakkingen,’”’ an Andante sostenuto 
founded upon the lines

MR. GANZ’S CONCERTS

A NEW season of these Concerts began in St. James’s 
Hallon Saturday, the 22nd ult., under encouraging circum- 
stances, the attendance being large and the work done 
interesting. As usual, the manager and conductor sought 
to please varied tastes in the selection of his pieces, and 
did so, we are glad to admit, with entire success. Ad- 
mirers of the classical school, for example, had a rich as 
well as abundant feast in Beethoven's ‘‘ Egmont’ Over- 
ture, and B flat Symphony, to say nothing of Mendelssohn’s 
Violin Concerto, which was made specially attractive 
by the appearance of a new soloist, Herr Ondricek. 
This gentleman, who is, we believe, of the same 
nationality as Herr Joachim, cannot boast many years’ 
experience in art, but he may flatter himself upon the 
possession of very considerable ability. [lis tone lacks 
the roundness and fulness which may come with time and 
a better instrument; it is, however, pure in quality, and 
‘* carries” well to all parts of a large hall. As an execu- 
tant Herr Ondricek shines most in cantabile passages, 
which, we need scarcely add, show the strength or weak- 
ness of a violinist more decisively than any other. The 
young Hungarian can make his instrument sing charmingly. 
Indeed, nothing could have been better than his rendering of 
the theme of Mendelssohn's slow movement. It was brought 
out with intense yet not exaggerated expression, and witha

finish that bespoke an accomplished artist. In bravura pas. 
sages Herr Ondricek is not quite so satisfactory, owing to 
occasional lack of clearness and ease. Nevertheless, his suc. 
cess with Mr. Ganz’s audience was complete, and we are cer. 
tainly not disposed to complain of the fact. The novelty 
in composition produced at this Concert bore the name of 
Liszt, and belongs to the roll of the great virtuoso’s so-called 
symphonic poems, having as its themes the “ Hell,” “ Purga. 
gatory,” and * Paradise” of Dante's ‘‘ Divina Commedia

a 
5

Beethoven), Chromatic Fantasia (J. S. Bach), Sonata in

D minor, Op. 31, No. 2 (Beethoven), Troisitme Ballade in 
A flat (Chopin), I] moto continuo (arranged by Brahms), 
Sonata, E flat, Op. 27, last movement (Beethoven), 
Troisitme Ballade in A flat (Chopin), Sonata in C minor, 
Op. 111, first movement 
(Handel), ‘‘ Masaniello” (Thalberg), Nocturne in C minor, 
No. 12 (Chopin), Toccata (Schumann), Prelude and Fugue 
in E minor (J. S. Bach), Barcarole (Chopin), Fugue in A 
flat (J. S. Bach), Ballade in G minor (Chopin), Sonata in 
C, first movement (Paradies), Aufschwung (Schumann

Italian Concerto, last two movements (J. S. Bach), Study

inF minor, No. 4 (W. Sterndale Bennett), Sonata in

minor, Op. 5, last two movements (Beethoven), Selection

from Etudes Symphoniques (Schumann), Organ Fugue in 
G minor (J. S. Bach, arranged by Liszt), Fugue in I° (J. 
S. Bach), Allegro Grazioso (W. Sterndale Bennett), 
Capriccio in B flat minor, Op. 33 (Mendelssohn), Fugue in

THE second Concert this season of the St. Stephen’s 
Musical Society was given on Tuesday evening, the rsth 
ult., at the Athenzeum, Shepherd’s Bush, when a large 
audience assembled to hear Hofmann’s Cantata ‘* Melu- 
sina.” Considering the result, Signor Dinelli has reason 
to feel proud of his choir and band. The soloists, too, 
were fairly good; for, although Miss Rose Trevor was 
carcely equal to the important part of Melusina, Mr. 
James Sauvage was all that could be desired as the im- 
petuous Raymond, the music suiting him admirably; and 
the minor parts were exceedingly well given by Miss Kate 
Baxter, Mr. Lindeman, and Mr. Stewart Beekley. Seeing 
that ‘*Melusina”’ has proved so successful, it might cer- 
tainly be advisable for the Society to select the same com- 
poser’s ** Cinderella ’’ for one of their concerts. The second 
part was miscellaneous. Signor Dinelli conducted with 
skill and judgment

Beethoven), Fugue in E minor

their great appreciation of the manner in which he has ) 20 2 
| of a compicte and t

season

the Leipzig, 
h for many 
to the 
The Ninth Symphony of Beethoven, on March 30

ed the Concerts in the Gewa and Bach's

Dt. Matthew ” Passion was given, for the benefit of} 
the band, at the Thomaskirche on Good Friday. This } 
year the former Concert had some teatures of special

interest, since the Choral Symphony was preceded by the 
Choral Fantasia, the prelude (in thought) to Beethoven's 
masterpiece. In the Fantasia the piano was taken by | 
Herr Reinecke with sensitive appreciation. The per- | 
formance of the Symphony, throughout excellent, wz as | 
signalised by a remarkably pure and exact rendering of | 
the scherzo, so hard to produce without confusion. At the | 
beginning of the Concert a Sanctus, and Agnus | 
Dei of Max Bruch was produced for the first time. It i 
written for

Kyrie

Festival Association is announced to be given on four 
consecutive davs, commencing the 23rd inst. The we

to be performed are Handel’s ** Messiah’”’ and l tre 
Te Deum and Jubilate; Beethoven’s Symphony in C min 
and Choral Symphony; one of Bach's Cantatas; selec. 
tions from Wagner's ‘ Lohengrin”; Berlioz’ ‘“ Fall of 
lroy”; and a Mass by Schumann, ‘The musical Director 
s Mr. Theodore Thomas; and the vocalist 
engaged include the names of Madame Amalia Materna

Mrs. Osgood, Miss Annie Cary, Miss Emily Winant, 
Signor Campanini, Mr. Theodore J. Toedt, Mr. Whitney, 
and Herr Henschel. Mr. H. Clarence Eddy is the organist

E fe

Hall on Thursday aft ernoon, the 25th inst., when excerpts Joachim, L.. Ries, Zerbini, and Piatti) > Chopin's Nocturne 
from the * Nibelung’s Ring” will be performed, under the | in F minor, and Polonaise in A flat, for pianoforte alone 
conductorship of an Seidl, (Herr Bonawitz:!; Spohr’s Scena Cantante, for violin with 
as : , : : pianoforte accompaniment | Herr Joachim); and Beethoven's 
ru rmances of Bach's ‘‘ Passion according | Sonata in C minor (MM. Bonawitz and Joachim). Each 
ia om. i : age s Chi urch,, Soho, hav e been re-| item w s excellently performed, and elicited much applause. 
peated pad first fi : ‘VPriday she of t! 1e past Lent, | | Mademoiselle Kufferath sang lieder by Schumann, Rubin- 
me rie ee Friday afternoon, under the cuseon of Mr. ; stein, and Brahms with success. It is to be rex vretted that 
Barnby. On each occasion the shortened form of Evensong ithe failure of these excellent Concerts, from a financial 
was employed, and the Magnificat and Nunc cimittls Were | noint of view, renders another scries uncertain. 
this year sung to Mr. Barnby’s setting in EF. flat c pm pose d 
last Festival of the Sons of the Cl The| IN consequence of a severe domestic bereavement sus- 
of the church, Mr. W. Hodge, p1 most | tained by Herr Alfred Griinfeld, Herr Franke has cancelled 
at an at each service, and the orchestra | rsa contract for Six yeanaronte Recitals which were to be

performers

Friedrich Wilhelm Kicken, the well-known composer of 
songs, died at Schwerin, on the 3rd ult., at the age of 
seventy-one

282 THE MUSICAL TIMES.—May 1, 1882. 
“Be nes 
W ve subjoin, as usual, the programmes of concerts* re- | mined up to specified standards. Now, if this principk pes 
cently given at some of the leading institutions abroad :—|be right with regard to general education—and as thier’ C 
Paris.—Gounod Festival at the Cirque d’Hiver (April 2): Symphony public purse pays for musical instruction—it would sure 145° 
D major, Chanson et Stances from “ Sapho,” Divertissement from | not be wrong to insist on its application to the Subjecy 
**Cinq Mars,” ao i Cecili: ay” for ey Nase soe pa of Music. But now arises the question: ‘On why 
from "La Reine dé Saba," Funeral aces of a Marionette,” Medi- principle are we to determine the standard?” }, 
tation on a Prelude by Bach, with violin solo (Gounod). Concert | answering this question we must be guided by the Opinio 
Spirituel of bg ee gg Nog gg si ber RA mercer gin rnind of eminent composers. But, it may be said, as most 9 
irom Septet (Beet, ie ; Vocal dust (Gounod); ‘Andante religioso et them have made themselves partisans of this or thy 
Allegretto (Mendels: ohn); Chorus (Palestrina); Overture, “‘ Oberon” |] System of teaching, how can it be safe to be guided b 
(Weber). Lamoureux (Good Friday): “La Vallee de| their opinion? Well, now, it happens that there is on 
Josaphat,” Symphonie biblique (G. Salvayre, first time); Requiem principle on which all a composers are agreed, viz, 
(Th. Gouvy, first time); Sym shonie espagnole (Lalo); Third Act ce we . ‘ SiR, 
“Lohengrin,” and March from “ Tannhauser” (Wagner). Concert that the ultimate result of al 1 teaching g ought to be facility : 
Spirtuel of M. Pasdelou; p (Good Friday): Marche Funebre, first time ] in reading the notation or language. in uhigh they write gystem 
on Aaa hag (Mozart); Benedictus, pages sang: Clip ecard their musical thoughts, i.c., the staff notation. Let ype ™° 
i ga PS cua Gancecuetaire: (April 16): fied ipnees then take this one point as our guiding principle; and pitaine 
(Beethoven); Fragment from “Orpheus” (Gluck); Air from “Le ] insist upon the children in elementary schools being exa, methor 
Nozze di Figaro ” (Mozart); Theme with vé ariations Scherzo, ¢ mined in accordance with it. I oifer this suggestio taught 
Linas fom Septet (iesinoven). Last Concert Popsle (ARTA), Because. think that musical education in. clementay fl 
Rossini); Violoncello Concerto’ (Saint-Saéns); Air from ‘Les|schools has long been retarded by difference of opinics than a 
Saisons” (Massc); “er ee ot —— av between only two sections of the musical public as ows 
rf ie hd ys I si erod 1¢€ Wassenet egre agitato ~ : ain) 1 i 
ot wae fo « Mireille * eeueds: Guten, ae to the best MEDS of palate the desired end, and ang 
Wives of Windsor a. also because I believe neither of the systems which ee 
Lyons. -Conce * of the Sainte-Cécile Society (March 26): “ La Fille | have been so prominently before the public to be nenti¢ 
le Grandval): Air from “ Iphige en Tauride® capable of producing the best poses results. Th Jate in 
! ittiioiod °° % stem, which was introduced into our schools pom 
ig—At St. Thomas’s Church (Good : Passion Music, | forty years since, with the prestige implied in the given 
St, Matthew (J. S. Bach). “Misea Solemnis.” D | fact of being ‘‘ Under the sanction of the Committee of ing ‘ 
_,Cologne.— Cone« ee Heroica (Beethove a ak | Council on Education,” stan¢ ds condemned in the eyes of know 
~ Wiesbaden.—Cur-Orchester (M: | musical men by Dr. Hullah’s own reports of the exami- 
giel); ymphony, Pract ioe ~ spert} Pn nt | nations in our training colleges. As to the letter notation, 
al bi from Op. 97 ; Ra : sph . ee Preludes, if its advocates believe it to be the best means of leading 
ymphonic poem (Lisz t). Cacilien-Verein tap ril 4): sl i pawl children to a knowledge of the staff, they will be willing ¢ to 
place it in competition with other systems, on equal con

Society (April 6): “Christus fac tus

tr 
3altimore.—Pea 5 rz): phony, C major 
Schubert); Songs (R. Schumann); Pianoforte s oe iszt); Faust

overture (R. Wag Peabody Concert (March 25): Symphony, E | : : 
major (R. Sc humann); : (R. eee ig ); Pianoforte solos | any prej udice against the staff rise ip, a serious responsi- 
ak Ph ; o— heir —_ i ee agner) - ts’ | bility will rest on the head of Tonic Sol-fa teachers.” | 
yncert of the Peabody Institute (March 1 lartet, p. 74, =) a er te eels ¢ : (ae 
(Haydn); Agr’ from Macie Minte meee C will now ask you to print an account of the doings of 
major; ‘Sympl hony, DB flat major (Haydn). Students’ Concert (March | twenty-eight schoolboys at a meeting in Bristol, December 
18): String Qu a4 , Op. 41 (R. Sc 1); Songs(Brahms); | 20, 1579. “The results here vouched for were produced by the 
forte Trio, E flat major, Op. 100 (Schu > ma:,.| System which rendered the Elizabethan era, so far as this 
York.—Philharmonic Club (March 7) “Con certo, F major | “- . . ' 
Variations on “God save the Emperor’ ’ (Haydn); Ballade, | country is concerned, a musical landmark—I mean the 
G minor (Chopin); Septet, Serenade in F major (F .L. Ritter) : old English, commonly called the ** Lancashire” System of 
e —Pup Pr the Wel lege a wy : 
Wellesley (U.S.)—Pupils’ Concert of the Welles lege (March | Sol-fa. I hope its perusal will convince your readers that 
20): Pianoforte Pieces (Schumann, Roec Jer); ‘ Sonata Pathétique Pmauents f cer t ee I 
Beethoven); Rondo Capriccioso (Mendelssohn) ; Pianoforte , | the question of musical edt ion ine nentary schools has 
(Schu: nann, R hei nberger); Fan nor (Mozart); Etu I | not only two, but at least th Yours fait! fully, 
hoy : (Mendelssohn, Schumann, Mozart, § REENWOOD, 
Dolby, . a 
ete an Concert of Profe Author of No. 19, Novello, Ewer and Co.'s 
ie A ‘* Music Primers,”’ 
One oe 4 Trafalgar Villa, Cotham New Road, 
oO lertoire (Batiste . orchtight’ "Bictina 1 < 
stol, April S, 
March (Meyerbeer); Romance, in I’, re viol lin (Beet! 10ven); pl Bristol, AE oi 
Fugue in E minor (Flagler). NARRATIVE oF Facts.—One of the boys wrote on the blackb 
——————— ee If changes of key from C to C tiat major, and another wrote rite . 
k to the ori tinal key. Th t boys then sang the exe 
CORRESPONDENCE. d when the keynote was s finish it was found th 
| were just a tr ha 1other boy to write the c +4

THE TON SOL-FA AND STAFF NOTATION

We do not hold ourselves responsible for any opinions expressed in 7 
this Summary, as all the notices ave either collated srom the loca 
papers or supplied to us by correspondents. a

Banrr.—The members of the Musical Association gave a Miscel c 
laneous Concert in St. Andrew’s Hall, on the , before a ¢ iu 
and thoroughly appreciative audience. The programme incit Jed a 
Macfarren’s May Day and several choral pieces, which were exce!] t] 
rendered. Amongst the instrumental composition: performed mav be | 
mentioned Beethoven's ‘* Moonlight” Sonata, finely played b: Herr , 
Hoffmann, and a Concertante by “Charles Dar for two violins a : 
pianoforte (Miss Dickson and Herr Hoffmann as violinists and Mis P 
Isa Ramsay as pianist), which was warmly and deservedly applat sded.} 
In Macfarren’s Cantata Miss M. Coutts, as the May Queen, was | ; 
highly successful; and vocal solos were also contributed during the

Miss K, Marti

Port Giascow.—The Parish Church Choir, under the conductor. 
ship of Mr. C. E. Midgeley, gave a Concert of Sacred Music on the 
4th ult., before a large audience. Dr. A. L. Peace presided at the 
organ, and played several solos with much skill and expression. The 
choruses were sung in a manner reflecting the utmost credit upon the 
training of Mr. Midgeley, who came forward for the first time on the 
occasion as a composer, with an aria,“ The Mercy Seat,” which was 
exceedingly well received

Retrorp.—The second Concert of the Choral Society for the 
present season took place on the 2oth ult. Gade's Spring's Message 
and Costa's Dream were the two works selected, and amongst the 
part-songs ‘The Bellman” (Dr. Macfarren) and “ Hunting Song” 
(Mendelssohn) deserve special mention, the latter being redemanded, 
Mrs. Daglish was the solo soprano, and Mr. Gregory the solo tenor, 
the latter giving a good rendering of Beethoven’s “ Adelaide,” with 
the Conductor's accompaniment. Miss Denman was the pianist, and 
played with much success Mendelssohn's Concerto in G minor, 
Mr. H. White performing a condensed 3 of the orchestral accom 
paniments on an American organ. Mr. F. W. Wells and Mr. G, F, 
Ashley acted very efficiently as eM ts on the piano and organ

Rrpon.—Haydn’s Passion Music was sung by the Cathedral Choir 
at the special nave services held during Holy Week. Mendelssohn's 
Hymn of Praise was sung on Easter Wednesday at a special evening 
service, by the Cathedral Choir, assisted by the Ripon Musical 
Society and the Harrogate Vocal Union. The Symphony was played 
on the magnificent organ by the Cathedral Organist, who afterwards 
conducted, Mr. H. Taylor, F.C.O.(a former pupil) taking the organ, 
The service was greatly appreciated by the large congregation

SHERBORNE

SouTHGATE.—The eighth Dedication Festival of St. Michael's 
Bowes Park, was held on Wednesday, the rgth ult., the services con 
sisting of Holy Communion at 11 and full choral Evensong at 7.30 
The latter service was sung by Rev. E, V. Casson, Curate of St, 
Michael's, and was followed by a very appropriate sermon by the Rev. 
Thomas Helmore, of Her M:z s Chapel Royal. The music, 
rendered by a choir of forty vo , included Tours’s Service in F; 
Anthem, ‘I was glad” ( Tuckerman); and Beethoven's “ Hallelujah,” 
from The Mount of Olives, Mr. Henry J. Baker conducted ; Mr. Chas, 
F, South, Organist of St. Augustine and St. Faith’s, W atling Street, 
presiding at the organ

Stratrorp.—A Concert of Sacred Music was given in the Hall of

On the 13th ult., Peter Josern Ries, for 
Broadwood and Sons, and brother to

id and pupil of Beethoven, aged gr. 
ck, County

ohn Ries: 
the fr

Adagio, F major oo. _ Pleyel, 
Sarabande, D minor S. Bach. 
Gavotte, D major ... ‘i S. Bach. 
Prelude : E. W. views 
Book IV.—Andante, “ Julius Cwsar’ ass Handel. 
Prayer, Larghetto Maestoso... “ss a6 Bellini, 
Marcia Religioso ; Gluck

Beethoven. 
tw ilhelm Bach

Andante Cantabile, Trio, C minor

Book V.—Moderato and Allegro, from the seventh Con icerto Corelli 
Andante Grazioso ... a .« Dussek. 
Andante, from the Concerto i in G minor Mendelssohn. 
Andante, from Op. 83... see : tephen Heller. 
“But Thou didst not leave tee is Handel

Book VI.—March, from the second S« t of Sonatas ... Handel. 
Prelude Gebhardi. 
Andante molto Cant: abile, ‘from Sonata a, O; 1 09 Beethoven. 
Larghetto ‘ * eis 3 ; Weber. 
Andante Sostenuto Mendelssohn

Book VII.—Trumpet Voluntary ... ie ee Purcell. 
Duet, from the ‘‘ Vater unser” ... xa ai : Spohr. 
“Ich grolle nicht,” Song... > ‘Schumann. 
Andante, from the twelfth S ymphony ide Mozart. 
Minuet and Trio, from the twelfth Symphony... Mozart

Book X.—March, D major . Hummel. 
Pie Jesu, from the Requiem Mass Cherubini. 
Melody, F major ... Adolphe Nordmann. 
Air, “Ne men con l’ombri” (“Xerxes ") sae - Handel. 
Andante Moderato, C major . ¥, Lachner

Book XI.—Romanza, from the second 1 Concerto . Mozart. 
Quintet, “ Giuri ognuno’ ‘ .. Rossini. 
Adagio Cantabile, Sonata Path: é ‘tique | Beethoven. 
Bourrée, D major ... i -_ o. (at del

Book XII.—Chant de Berceau, from Op. 81 : ahi Heller 
Andante, G minor, from a Sonata ; . Hay oa 
Adagio, 1p) major, from a Sonata oii Pinto. 
Traumerei, Lento, F major — es a Schu imann

Handel

Haydn 
Old German. 
Beethoven. 
Handel

La Malinconia

Fugue, C major

ap S. Bach. 
Beethoven. 
H. Jaeschke

Book XIX.—Arietta con Coro, ‘ ‘Invano alcun desir Gluck, 
Salutaris ... : ‘ Batiste. 
Prelude, C major 3eethoven. 
Andante, F major... . Mozart. 
Book XX.— —Organ Theme, A flat. . A. Hesse. 
heme, A flat son F. Schubert

s. d

BACH'S PASSION (ST. MATTHEW) ... Io 
BEETHOVEN'S MOUNT OF OLIVES... o 6 
BENNETT'S MAY QUEEN - a6 
GRAUN’S PASSION Io 
HANDEL’S MESSIAH o 8 
- JUDAS M ACCAB.EUS o 8

ea SAMSON a oni o 8

