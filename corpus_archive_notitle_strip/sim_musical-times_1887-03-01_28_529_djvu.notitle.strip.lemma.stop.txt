MADAME CLARA WEST ( soprano ) . 
 MISS LOTTIE WEST ( contralto 

 Beethoven Villa , King Edward Road , Hackney . 
 MISS WIKE ( soprano 

 Miscellaneous Concerts , address , 25 , Upper Park 
 Road , Havers stock Hill LN . W 

 lrace woman country 
 irresistible attraction . speak Russian , Italian , 
 French equally , know little German , 
 , lesson Paris , learn 
 Spanish . belong , , type 
 gentleman cosmopolite . unnecessary 
 scratch surface order find true 
 Russian . attraction pleasure dominate 
 sense . Paris waste month grisette 
 low class actress 

 accord authority , Glinka cherish 
 sovereign reverence fashion , 
 Russia . refuse witness Meyerbeer 
 ' " L ’ Etoile du Nord , " manner 
 Peter Great treat , sure way anger 
 advocate change government 
 native land . annoyance 
 express mask comedy . 
 occasion , Moscow professor advocatec 
 revolutionary idea , Glinka kneel image 
 St. Nicholas exclaim , ' ' o great saint , 
 destroy University Moscow , destroy 
 Wilna , 
 man talk like man . " 
 seriously resent radical idea , , 
 later year , constitutional nervousness in- 
 crease , excited visitor 
 caution leave politic hall . 
 umbrella . music , hand 
 Glinka preserve open mind , ready 
 consider novelty prejudice . Bach 
 Handel favourite master , 
 yield Beethoven entire dominion soul . 
 hear ' * Fidelio " weep , occa- 
 | sion return home froma concert look pale 
 agitated wife question : * * 
 matter ? " ' Beethoven , " reply Glinka . ' 
 j}has ? " instead answer , 
 | composer throw couch , bury face 
 hand , remain long unable 
 articulate word . listen , 
 |time , Beethoven Seventh Symphony . Glinka 

 find , , keen sympathy Berlioz , 
 strengthen , doubt , french master 
 personal fascination . ' result indulgence 
 day dream , come , 
 fashion dream general . , , write 
 programme symphony . new world achieve- 
 ment open , fancy 
 enter conqueror . different 
 reality , remainder life 
 barren . M. [ Touque speculate , interesting 
 manner , reason 

 152 MUSICAL TIMES.—Marcu 1 , 1887 

 redoubtable Leeds chorus . | Schonberger display exquisite delicacy touch , 
 Dr. Stanford fine setting ballad ' * revenge , " | " strangely flurried , play 
 work popular good sense word , follow ; false note , indulge exaggeration style 
 increase familiarity music peculiarly objectionable Beethoven music . general 
 concern interpretation attend | feeling true form , 
 happy result . chorus err , on| impression strengthen admirable way 
 excess zeal ; Talleyrand maxim , how- | render pianoforte Schumann trio 
 sound diplomacy , hold good instant|in D minor . 
 domain music . desire , prefer believe defect 

 respect admiration ' Parsifal , " we|in Sonata temporary indisposition , 
 regard Herr Steinbach concert adaptation } Concert Mr. Heermann appearance 
 " Klingsor Magic Garden " ' ' Flower Maidens , " | leader , sound classical executant 
 opera — chief orchestral novelty Concert | Beethoven Quartet , . 5 , Adagio 
 19th ult.—as effective impressive piece when| Spohr . Miss Liza Lehmann sing little air Flotow 
 thu detach scenic vocal accessory . | ' ' Martha , ' usually omit performance , ot 
 " performance ’' — far Crystal Palace | Brahms Lieder , utmost refinement charm , 
 concern — clever Concert Overture } work Beethoven mark " time " rarity 
 write Mr. F. kK. Hattersley recent Leeds | Concerts , reference catalogue 
 Festival , meet decidedly cordial reception , | sonata F , pianoforte horn ( Op . 17 ) , 
 intrinsic merit fine — | Saturday , 5th ult . 
 receive Mr. Manns baton . Madame Falk- -| pleasing example composer 
 Mehlig glad hear , executive | style , tune , free touch sadness save 
 talent unabated power expression far | second subject movement , , 
 develop mature , gifted pianist know to| way , reveal true Beethoven . piano 
 concert - hall year ago maiden | course child play Mr. Hallé , horn enable 
 Anna Mehlig . refined } Mr. Paersch display wonderful command 
 graceful rendering solo Chopin Piano- | difficultinstrument . interesting item Hummel 
 forte Concerto , Madame Falk - Mehlig abundant | septet . popular work neglect 
 brilliancy force Liszt piquant version piano- | nearly dozen year , novelty 
 forte orchestra Weber familiar Grand Polonaise | large proportion audience . Hummel 
 ( Op . 72 ) , win double recall . habitué | lack true fire genius , septet extra- 
 Crystal Palace Concerts bear } ordinarily effective work , hear 
 express opinion Mr. Manns lead fine per-| time wonder 
 formance Symphony — Beethoven . 8 , F—| shelve long . ' performance Messrs. Hallé , 
 occasion . gloomy | Svendsen , Horton , Paersch , Hollander , Bottesini , 
 rumour future Crystal Palace afiecte | Piatti absolutely perfect . Mr. Santley , 
 efficiency player . certain rendering | fine voice , sing Schubert " Orpheus " Handel 
 time slipshod . Mr. Sims Reeves | ' * Nasce al bosco 

 vocalist , applaud effort inj remark need concern Concert 
 Glumenthal ' requital ' recitative air | Monday , 7th ult . work Beethoven per- 
 ' Gideon " ( C. E. Horsley ) . | form , , Quartet } minor ( Op . 95 ) 

 Pianoforte Trio D ( op.70 , . 1 ) . Mr. Schénberger 
 ; ' | pianist , content Mendels- 
 MONDAY SATURDAY popular concert . sohn Prelude f ugue e minor ( Op . 35 , . 1 ) , 

 create profound impression , , | magnificent work , thoroughly representative com- 
 ' | } 
 | poser genius bright , hear , owe 

 rate , prove tedious audience . middle | 
 movement good , op . 5 surprising . | caprice pianist , 
 composer possess real individuality | effective piece write instrument . 
 remain . present | bar , apparent Mr. Schonberger 
 accomplished musician , . | exceptional performance , expec- 
 Madame Néruda appearance previous | tation disappoint . , , 
 continental tour . Mozart favourite | prefer exquisite beauty touch evince 
 Quartet D minor ( . 2 ) , Mr. Hallé , Schu- | second movement fury passion 
 bert spirited effective Fantasia C ( Op . 159 ) , | attack Finale . doubt wonderful 
 piano violin . pianist Beethoven Sonata | display kind , , like Rubinstein , ap- 
 e minor ( op . ) , movement ! parently model , Mr. Schonberger indulge 
 singularly slow pace . greatly prefer Mr. Hallé | good wrong note , time pleasing 
 reading beautiful Allegretto . Mr. Lloyd } ear . young , wise 
 sang " native worth , ' extremely tasteful | ultimately adopt pure style realise 
 pleasing setting Shakespeare - sonnet , | particular merit mere noise . popular audience , 
 Dr. A. C. Mackenzie . , resist way genuine 
 audience follow- | sensation , performer recall . time . 
 e Monday mainly attract announcement | Mozart Quartet C ( . 6 ) , Rheinberger Piano- 
 Mr. Schénberger play Beethoven Sonata | forte Quartet E flat ( Op . 38 ) , concerted work 
 Appassionata . young german pianist create j Concert , Signor Piatti play movement 
 extremely favourable impression recital , , some- | Molique Violoncello Concerto D. Miss Liza 
 strangely , programme occasion | Lehmann delight hearer rendering 
 contain example mighty Bonn master work . | Mozart song ' ' Chloe , " opening bear 
 high expectation raise result sur-| curious resemblance Schubert familiar Lied 
 prising disappointment . slow movement , Mr. | ‘ Wohin 

 MUSICAL TIMES.—Marcu 1 , 1887 . 153 

 _ 

 h , Concert November 15 , move- | Kaiser , Mr. Thorndike , Mr. [ Franklin Clive . 
 play ment Pianoforte Suite Grieg , entitle ' * aus/| order score observ e , lucid remark 
 f style Holberg Zeiten , " introduce Madame Fricken- lon number , include extract Otto Jahn 
 eneral haus , notice Concert | | description opera , enable audience realise 
 nd entire work subsequent occasion . | situation , heighten enjoyment 
 vay suggestion carry 14th ult . , Mr. Max | Mozart delightful music . master Symphony 
 Trio Pauer executant . work lively im-|in e flat — truly melodious sym- 
 > pression , thank skilful manner com- ' phony — perform , form welcome 
 lefects poser imitate early eighteenth century style , contrast Prahms clever decidedly dry Pianoforte 
 sition , introduce , , little fi gures progres- | Concerto D minor ( Op . 15 ) . grand sublime 
 arance sion indicate scandinavian origin ; the| utterance composer 
 cutant admirable playing Mr. Pauer , rapidly advance | Schumann " discover , " terribly unsympathetic 
 gio art , bid fair high rank . , metaphorically , cap pedagogue 
 otow fair Monday subscriber ! write light midnight lamp , appear 
 ot opportunity renew acquaintance Hummel | instance . Mr. Max Pauer master 
 arm . Septet , work repeat list of| difficulty Concerto apparent ease , 
 rity performer . Haydn Quartet D ( Op . 64,| duly applaud , refuse believe 
 vs . 1 ) , pleasing romance Pathétique , Signor | popular work . 
 , Bottesini , play course composer , include | | attendance somewhat Concert , 
 programme . Miss Carlotta Elliot highly } Thursday , 3rd ult . , principal attraction 
 s acceptable vocalist . | undoubtedly Beethoven symphony , . 7 . 
 ave Mozart Clarinet Quintet work | come away unrewarded , fine 
 oy age wither custom render stale . | spirited rendering favourite work . 
 vas , enjoy period | allegretto enjoy audience ask 
 nable 1789 , compose , , performance second time , Mr. Henschel wisely decline 
 er Saturday , 1gth ult . , Mr. Straus leader Mr. ! request ; close receive generally 
 nmel Lazarus solo , absolutely perfect , } know ' ovation . " " Miss Amina Goodwin 
 e audience rare treat . effective contrast | elicit applause able rendering Minuet 
 y toa Mozart mellifluous strain Schumann Piano Sonata Gavotte Raff suite pianoforte orchestra 
 G minor ( Op . 22 ) , render highly able | ( Op . 200 ) ; moderately commend Miss 
 extra- manner Miss Fanny Davies . Beethoven trio G | Hamlin performance Mendelssohn Scena ' Infelice . ' 
 ( op . 1 , . 2 ) , Molique Saltarella violin | Schumann fine overture ' ' Genoveva " ' commence 
 - unnecessary sayanything . Mr.and Mrs. Henschel sing | concert , Dvorak highly characteristic slavic dance 
 Halle , good manner Boieldieu Duet ' ' Le Nouveau C ( op . 46 , . 1 ) , bring effective close . 
 » Seigneur de Village . " Mr. Henschel certainly concession popular 
 va finish record month chronicle | taste draw programme eleventh Con- 
 ndel return Dr. Joachim follow Monday . cert , Wednesday , gth ult . , consist 
 prince violinist welcome , justify | entirely work appeal mainly , present , 
 cert welcome magnificent playing , withoutsaye . artistic minority . Brahms , , 
 e per- choice Dvorak masterly sextet ( Op . 48 ) open | consider , fine Sy mphony C minor . 
 d Concert peculiarly appropriate , | truly great work , impression 
 e initiative Dr. Joachim work introduce , perform country Cambridge , Dr. 
 ndels- attention draw genius Joachim , fresh recollection 
 bohemian composer . work Beethoven , , a| present occasion . rendering Mr. 
 eased , sine qua non occasion , choice fall Henschel orchestra admirable , enthusiastic 
 1 bright tuneful quartet G ( op . 18 , . 2 ) . Schu- | applause imply audience fully appreciate 
 Mit mann Fantasia minor , violin ( Op . 131 ) , work . great sensation evening , , 
 song unfortunate composer ’ slat work , andit reflect mental | produce wonderful capacity violinist 
 effect . dimness fast come . , Mr. Joachim | Miss Nettie Carpet Max Bruch Concerto 
 ain naturally look regard , specially|in G minor . tone , phrasing , execution alike 
 compose . encore movement | remarkably good young performer , future 
 imself Bach Sonatas . Miss Zimmermann | Miss Carpenter watch interest . Dr. 
 nce hear advantage Schubert impromptu C | Stanford clever humorous overture ' ' Canter- 
 bert minor ( op . ) , Valses Nobles ( op . 77 ) , | |bury Pilgrims " recall pleasant recollection , 
 audience pleased play | regret necessity chiefly consider provincial 
 com- entire set . | taste compel Mr. C Carl Rosa remove " Opera 
 e SS repertory season . Beethoven early 
 oe — ree ee Mozcart - like terzetto ' ' tremate , empi tremate , " 
 LONDON SYMPHONY concert . sing Mrs. Henschel , Mr. Feery ' Pasa , Mr. Eliot 
 erger praise bestow Mr. Henschel | Hubbard . 
 xpec- enterprise perseverance evince his| speak adversely Richter 
 vever , arduous undertaking , , , find | concert account sectional character , 
 e reward patronage public . | experience Tuesday , 15th ult . , Mr. Henschel 
 ich twice St. James Hall , attendance | declare find public sectional 
 derful occasion wretchedly meagre . | taste . try Mozart , Mendelssohn , Brahms , 
 ap- word , Mr. Henschel experience fate | master vain , offer programme 
 red befall believ e , cost , | Richter pattern , , wagnerian selection , 
 ase existence widespread desire public | Beethoven Symphony throw , ' succeed fill 
 e orchestral Concerts . thousand flock hear a| St. James Hall enthusiastic audience . 
 sensational performer , work everybody | profess understand ; , 
 lence , talk ; good sound , honest , artistic labour reg arde | narrowness public ’ vision state 
 nuine indifference . melancholy | thing disclose truly lamentable . time 
 fime . oblige write , use blink fact . | | unduly severe blame concert - giver occa- 
 iano- general cultivation music immeasurably | sionally offer bait order secure house . 
 work advance father , high form | exception ' W agner piece 
 ment art care select . ninth|heard ad nauseam St. James Hall , , 
 Liza Symphony Concert , Friday , January 28 , conductor | Vorspiel Liebestod ' Tristan und Isolde , " 
 e venture ordinary track , usual result | Poger address " Die Meistersinger , " Wotan 
 car & bench . selection number Mozart | abschie Feuerzauber , ' ' Die Walkie , " 
 Lied opera buffa " cosi fan tutte " , vocalist | noisy ' " ' Huldigungs " ' Marsch . vocal excerpt 
 Miss Larkcom , Madame De Fonblanque , Mr. Charles | sing , declaim , Mr. Santley , 

 xum 

 excellent voice , orchestra sufficiently 
 subdue attainment Wagner intention . 
 trifling novelty , sketch entitle ' ' Traume , " ' 
 string ( bass ) , clarinet , bassoon , horn . 
 kind reflection love duet ' Tristan 
 und Isolde , " set song composer , 
 violin solo . version 
 publish year , orchestral piece 
 time occasion . evidently 
 like , conductor decline grant encore . 
 symphony " Eroica , ' " ' finely interpret 
 enthusiastically receive 

 Herr Joachim appear thirteenth concert , 
 24th ult . , magic , course , sufficient 
 draw capital house . London amateur 
 disgrace hang 
 occasion . principal solo famous hungarian 
 violinist Brahms Concerto D ( Op . 77 ) , 
 perform Gewandhaus Concerts 
 New Year Day , 1879 . frequently hear 
 country , regard composer 
 inspired utterance . solo laboured un- 
 grateful player , owe probably fact 
 Brahms violinist . , Herr Joachim 
 executant , mean ineffective , vigorous 
 Finale especially prove satisfactory audience 
 present occasion . Beethoven romance F ( Op . 50 ) 
 persistent demand encore , Mr. 
 Henschel persistently decline , great honour 
 . great treat tohear Haydn symphony 
 . charming work , foolish 
 caprice , studiously neglect concert - giver , | 
 enthusiasm awaken - night . 9 , B 

 flat , Salomon set , Mr. Henschel unwise if| 
 frequently allow delightful old | 
 master appear programme . Sterndale Bennett 
 overture " Parisina " open Concert Liszt | 
 hungarian rhapsody D ( . 2 ) conclude . 
 performance evening unusual 

 somewuat large audience usual assemble 

 St. James Hall , Monday afternoon , 21st ult . , 
 Mr. Bache annual Recital . 
 programme announce miscellaneous , 
 people convert truth 
 Liszt , determine . rate , 
 Mr. Bache continue pursue self - impose mission 
 undiminished firmness , music 
 revere master scheme Recital include 
 Prelude Fugue Bach , Beethoven Varia- 
 tion e flat play audience , Mendelssohn 
 Fantasia f sharp minor ( Op . 5 ) play . 
 conspicuous Liszt work - Fantasia 
 quasi Sonate , ' Apres une Lecture de Dante . " 
 extraordinary composition , absolutely 
 impossible form idea hearing . 
 theme quote programme , 
 evidently represent " Inferno , " 
 " Paradiso , " trace 
 definite meaning constant progression discord 
 piece . selection 
 " etudes d’execution transcendante " ' prove far in- 
 telligible , transcription piano 
 picturesque symphonic poen : " Mazeppa ’' come 
 . Mr. Bache assist Mr. Fritz 
 Hartvigson . ifthe audience appreciate music , 
 admirable playing Mr. Bache , recall 
 time course afternoon 

 MR . SCHONBERGER PIANOFORTE RECITAL . 
 young pianist début London 
 audience immediately hail performer 
 ordinary calibre , natural consequence 
 large audience connoisseur second recital 
 Wednesday , 16th ult . , St. James Hall . sooth 
 , , note , strikingly 
 favourable impression outset scarcely 
 confirm . occasion notice 

 Signor Piatti , , addition miscellaneous 
 selection , play Molique Violoncello Concerto 
 usual perfect finish ability . 
 C minor , , follow Concert , Haydn charm 

 e no.7 , C. Beethoven Choral Fantasia ( Op . 80 ) 

 figure programme , rendition scarcely 
 complete satisfactory desire . 
 deficiency instance , , amply atone 
 choir intelligence care 
 sing , course evening , 
 - song Sullivan Pinsuti . Madame Marie Roze 
 vocalist Concert , win high favour 

 piano accord admirably tone , player 

 neatness execution style . great effect 
 create aver , everybody 
 suavity music , smoothness 
 delicacy presentation . Bach work different 
 calibre masculine vigour , display un- 
 rivalled contrapuntal skill , fresh fancy 
 great master . vocalist evening , Miss Agnes 
 Janson , favourable impression 
 scandinavian french song . 17th ult . , Mr. 
 Hallé , time severe illness 
 grievously interfere plan season , re- 
 appear solo pianist , play undiminished 
 skill ease Beethoven ' Waldstein " Sonata 
 Chopin F minor Concerto . Miss Emily Winant 
 sustain steady pure tone , powerful voice 
 acceptable ; incessant vibrato 
 ‘ agreeable . Moszkowski suite F ( Op . 39 ) play 
 time , portion pleasing . 
 concern " Elijah " ( 24th ult . ) , need 
 write 

 Mr. de Jong conclude series Concerts 
 12th ult . , party include Mdlle . Marimon ( long 
 | absent unknown present Concert 
 frequenter ) , Madame Sanderini , Miss Eleanor Rees , 
 | Messrs. Guy Maybrick , Signor Mattei pianist , 
 /and Signor Hegzesi violoncellist . benefit con- 
 | cert , 26th ult . , Mr. de Jong announce large party , 
 | mode select portion programme 
 | try concert - room , 
 | Free Trade Hall . think voting 
 | instrumental item dignified mrode arrange 
 scheme , calculate raise character 
 speculation 

 musician , evidently catch spirit 
 teacher , Madame Schumann ; exceedingly 
 happy selection , deservedly gain 
 hearty encore . hear Madame Valleria 
 voice , effort recall ; Miss Ada Doyle 
 received , Mr. Ellison , sing 
 great taste , Signor Foli popular 

 Miss Lock Chamber Concerts place 
 16th ult . , small Victoria Rooms , 
 scarcely large audience , 
 partly doubt announcement Mr. 
 Walter Macfarren contribute pianoforte 
 solo . Concert open Beethoven trio C 
 minor , pianoforte , violin , violoncello , 
 fairly render Miss Mary Lock ( pupil Mr. 
 Macfarren ) , Messrs. Hudson Pavey . Mr. Macfarren 
 Chopin Nocturne d flat , 
 valse C sharp minor d flat com- 
 poser , later evening contribute Rondino 
 Espressivo Impromptu Gavotte 
 pen , gain hearty encomium . Mendels- 
 sohn Sonata D , piano violoncello , open 
 second programme , Miss 
 Lock Mr. Pavey . vocalist Miss Kate Nicholls , 
 contribute song pleasing manner . Mr. 
 Hudson play romance violin com- 
 position , evince musical talent , Mr. F. 
 Rootham good service accompanist 

 usual , great interest manifest 
 annual " lady ’ Night " Bristol Orpheus Glee 
 Society , 17th ult . Colston Hall thoroughly 
 fill expectant audience , hope 
 evening rare music disappoint ; 
 occasion Orpheonists sustain well- 
 know reputation , hardly room 
 criticism performance long varied 
 programme . ' conductor , Mr. George Riseley , 
 place desk o’clock , amid loud applause , 
 following piece performed:—Part 1 , ' " 
 National Anthem , " arrange G. Riseley ; 
 ' strike lyre , " J. Cooke ; ' ' hymn night , " Beethoven ; 
 " " hush death , " H. Hiles ; ' image rose , " 
 Reichardt ; ' * Jubilee ode , " G. Riseley ; ' * drink 
 thine eye , " arrange G. Riseley ; " ' martyr 
 arena , " L. de Rille ; ' ' die child , " Viotta ; 
 " Jubilee Choral song , " W. Macfarren . 2 , ' * high- 
 land war song , " W. Macfarren ; ' " Ossian , " Beschnitt ; 
 " love wine , ' Mendelssohn ; ' hour beauty , " 
 Hargreaves ; ' sad autumn wind , " Root 

 Riseley ; ' * retreat , ' L. de Rille ; ' absence , " 
 Hatton ; ' Chafers , " Truhn . arrangement 
 ' National Anthem " effective , Mr 

 MUSIC EDINBURGH . 
 ( correspondent 

 Mr. STAVENHAGEN recital place afternoon 
 January 22 , Music Hall . programme con- 
 siste Beethoven sonata e minor ( op . ) 
 C sharp minor ( op . 27 ) , Liszt variation theme 
 Bach , ' " * Weinen und Klagen , " composition Liszt , 
 Chopin , Schumann ( Papillons , Op . 2 ) . gifted 
 artist interpret composition include 
 selection masterly style 

 January 24 sixth Choral Union Orchestral 
 Concert place ; Mr. John Dunn solo violinist , 
 perform Niels Gade Concerto violin orchestra , 
 fantasia ' " Otello , " Ernst . remainder 
 programme consist Schubert ' * Rosamunde , " 
 Godard Canzonetta b flat , Hermann Goetz Sym- 
 phony F , Auber Overture " La Siréne . " Miss 
 Thudichum vocalist , sing charmingly ' Qui 

 aid Railway Guards ’ widow ’ Orphans ’ 
 Fund , concert evening 4th 
 afternoon 5th ult . popular vocal music 
 constitute chief programme . vocalist 
 Miss Annie Grey , Miss Adelaide Mullen , Miss Marian 
 Williams , Messrs. Bernard Lane , Bantock Pierpoint , 
 Thurley Beale . Miss Bertha Brousil contribute 
 violin solo usual finished style , Mr. Wilhelm 
 Ganz accompany 

 Mr. Henry Waller Pianofore Recital Queen 
 Street Hall , 5th ult . composition 
 play Concert - giver Schubert Fantasia C 
 major , Beethoven sonata C minor ( Op . 13 ) , selec- 
 tion Schumann , Chopin , Liszt 

 Orchestral Concert season , 
 auspex Choral Union , place evening 
 7th ult . Wagner overture ' " Tannhauser , " 
 romance C , Mozart Serenade string G 
 major , Liszt Rakoczy March , principal 
 instrumental selection . Miss Amy Sherwin sing 
 great taste finish Recitative Aria Mozart , 
 Mr. John Probert ' Dalla sua pace , " Don 
 Giovanni . Beethoven Choral Symphony ( . g ) com- 
 plete Concert . task assign member 
 chorus Symphony power , 
 music indifferently render . soloist 
 , Miss Sherwin Mr. Probert , Miss Annie 
 Layton Mr. Glencorse . Mr. Manns conduct 

 amateur Orchestral Concert 
 evening 8th ult . principal item pro- 
 gramme Mendelssohn Scotch Symphony , 
 Goltermann Concerto D minor , cello orchestra , 
 cello ably interpret Mr. Carl Hamilton , 
 Conductor Society . Mrs. Ellis song , 
 appreciate 

 Concert , connection annual Reid Festival , 
 Music Hall , afternoon Saturday , 
 12th ult . , afford opportunity hear 
 Mr. Charles Hallé orchestra , Sir Herbert 
 Oakeley engage occasion . Beethoven Grand 
 Overture C ( ' * Weihe des Hauses " ' ) , Schumann Sym- 
 phony C major , selection Rubinstein " Bal 
 Costumé , " Entr’acte ' " ' Lohengrin ’' 
 principal orchestral number . Mr. Charles Hallé solo 
 Chopin Impromptu F sharp , Heller ' La 
 Truite , " solo movement 
 Pianoforte Concerto , Tschaikowski . Mr. Edward Lloyd 
 gain great applause rendering Handel ' * sound 
 alarm , " recitative aria , Halévy " La 
 Juive , " Gipsy Songs , Dvorak 

 Reid Concert proper place evening 
 14th ult . , open , usual , General Reid 
 Pastorale , Minuet , March , play memory com- 
 poser , bequest afford Edinburgh Concert . Spohr 
 overture ' " Jessonda , " Chopin Pianoforte Concerto 
 F minor , Beethoven Seventh Symphony , Men- 
 delssohn ' ' Midsummer Night Dream " music , Liszt 
 hungarian Rhapsody D , Overture " William 
 Tell " remain orchestral number . Mr. Charles 
 Hallé play piano Concerto . vocalist 
 Miss Amy Sherwin Mr. Edward Lloyd . Miss 
 Sherwin sing scena aria Weber " Freischiitz , " ’ 
 ' couplet du Mysoli , " David " Perle du Brésil , " 
 , encore , Sir Herbert Oakeley setting 
 laureate word , ' ' home bring warrior dead . " 
 Mr. Lloyd selection Beethoven " * Adelaide , " 
 " Ad Amore , " Sir Herbert Oakeley , enthusi- 
 astically encored . Concert lengthy , 
 success undoubted , fully maintain reputa- 
 tion earn event musical season 

 MUSIC GLASGOW WEST SCOTLAND , 
 ( correspondent 

 Tue Choral Orchestral Series Concerts run till 
 12th ult . need particular date o1 
 Concerts letter , mention 
 principal musical work selection perform . 
 follows:—Suite Mackenzie opera ' " 
 Troubadour , ' — viz . , Prelude act Jeu de 
 Paume , Masque Music Entr’acte act , 
 execute warmly receive ; Mendelssohn Con- 
 certo Violin Orchestra , Mr. Sons , soloist ; Beet- 
 hoven Eighth Symphony , finely perform ; Mendels- 
 sohn Scotch Symphony ; paraphrase Violin Or- 
 chestra Walther prize song ' ' die Meistersinger , " 
 violin play violin ; hungarian 
 rhapsody " Teleki , " Liszt . completion perform- 
 ance set symphony Beethoven , 
 ninth include Concerts series . 
 interpretation hardly 
 stupendous conception , 
 audience enjoy Symphony undoubtedly 
 set . evident strain voice 
 join , remark critically 
 work , away enjoyment 
 anticipated , feeling relief 
 . Miss Amy Sherwin , 
 Miss Annie Layton , Mr. J. Probert , Mr. A. Black 
 quartet Symphony . overture ' " Tann- 
 hauser " ' play Concert . follow , 
 twelfth Subscription Concert , come performance 
 Mendelssohn Oratorio ' St. Paul . " altogether , 
 rendition great work 
 neighbourhood . chorus , rule , sing 
 nobility refinement , , place , 
 earn conscientious training Mr. Allan Macbeth ; 
 orchestra play accustomed skill , 
 experienced direction Mr. Manns . 
 quartet principal , 
 exception Mr. W. H. Burgon bass 

 plebiscite Saturday Tuesday 
 Concerts regard music desire hear 
 night season . audience pretty 
 conservative , old groove . 
 expect , overture " Tannhauser " 
 " William Tell , " Pastoral Symphony , 
 high voting , , selection 
 favour , duly include programme occa- 
 sion . hall fill utmost capacity , 
 desire present unable gain admission . 
 audience enthusiastic , suppose , 
 end Concert recall Mr. Manns 
 . Mr. Manns , evidently 
 gratified mark confidence esteem 
 , short speech , hope return 
 December 

 e 

 MUSIC AMERICA , 
 ( correspondent . ) 
 New York , February ro . 
 | operatic affair uppermost mind 
 musician music patron New York . german 
 season Metropolitan Opera House finish ere 
 read , influence remain 
 | american metropolis country . 
 month add list , performance 
 period having revival . opera 
 bring forward " Fidelio , " ' * Die Meistersinger , " 
 ' * Rienzi . " " great interest attach 
 performance Beethoven opera , 
 | time year receive public great 
 enthusiasm . occupy place plan Dr. Dam- 
 | rosch , undertake experiment year ago 
 turn successful ; 
 | performance effort rehabilitate abandon 

 applaud stint manner render | case ' Tristan und Isolde , " 

 death announce , Paris , gth ult . , | 
 M. Auguste Désiré Bernard Wolff , thirty year head | 
 firm Pleyel , Wolff et Cie . M. Wolff receive a| 
 musical education Conservatoire , rise a| 
 professor piano institution . mark | 
 play instrument | 
 perfect structure . credit having invent 
 improvement , having discern merit 
 far astoadopt . double escapement , short 
 grand , - stringing , metal bar , transpose pedal , 
 tonal pedal , pedalier device 
 come category . 
 great distinction M. Wolff manage- | 
 ment firm , enrol | 
 man contribute fully present 
 perfection household instrument . M. Wolff 
 reach age - year 

 Westminster Orchestral Seciety second 
 series Concerts , 16th ult . , Town 
 Hall , Westminster , feature especial inte- 
 rest present programme . 
 , place , performance 
 Beethoven second Symphony , satisfactory 
 highly creditable orchestral body consist 
 exclusively amateur . production Sir Arthur 
 Sullivan Violoncello Concerto D major deserve 
 special mention , work having publicly 
 perform ( Signor Piatti play Crystal Palace 
 1866 ) , remain manuscript . 
 early composition , Concerto write 
 instrument , especially second 
 movement ( Andante expressive ) exhibit melodic charm 
 matter regret 
 frequently hear . efficiently interpret 
 Mr. J. Edward Hambleton . proceeding even- 
 ing include composer ' * ' Di Ballo " Over- 
 ture , allegretto grazioso incidental music 
 " Henry VIII . " Master Gerald Walenn cleverly play 
 Scéne de Ballet , De Bériot , Miss Blanche Murray 
 Mr. T. J. Grylls contribute vocal number . Mr. 
 Charles Stewart Macpherson conduct ability , 
 able painstaking force command 
 successful progress Society , second 
 season , assure 

 Hotel Drouot , Paris , sell , 
 5th ult . , musical effect late M. Abel Boujour , 
 know player connoisseur fine instrument . 
 principal object interest notable violoncello 
 Stradivarius , bow celebrated Francis 
 Tourte — unique specimen , perfect 
 day leave maker hand — stick octagon , 
 mount tortoiseshell gold . lot bring 
 high price pay bow public auction , 
 knock english dealer , Hill , Wardour 
 Street , sum £ 44 . Stradivarius violoncello , 
 early period , remarkable 
 specimen , fetch high price . exhibit 
 french contribution deceased owner 
 " Historic Music Loan Collection " gallery 
 Royal Albert Hall , 1885 , attract 
 attention . bear date 1689 realise sum 
 £ 760 , purchase M. Delsart ( Professor 
 Conservatoire ) . , date 1691 , buy 
 agent M. Hollmann ( know violoncellist ) 
 sum £ 480 . Francesco Ruggerius violoncello 
 fetch sum £ 128 . instructive notice 
 increase number Stradivarius instrument purchase 

 position ( scholarship ) , candidate . prelimi- 
 nary examination place local centre 
 2nd inst . , final examination College 
 end month 

 Examination Degree Doctor Music 
 University Oxford , Second Examination 
 Degree Bachelor Music , hold October 
 . - examination , addition 
 usual subject , require critical knowledge 
 Beethoven ' " Fidelio " ( include e major overture 
 ) Mozart symphony e flat . exercise 
 send Professor Music , Sir Frederick A. Gore 
 Ouseley , St. Michael , Tenbury , early possible . 
 receive end June 

 Woodside Park Musical Society excellent 
 performance Mendelssohn ' Elijah , " 17th ult . , 
 Woodside Hall , North Finchley , direction 
 Mr. Alfred J. Dye , A.Mus . soloist Miss Mary 
 Beare , Madame Florence Winn , Mr. Percy Palmer , 
 Mr. Bridson , acquit admirably . 
 chorus consist eighty voice , accompani- 
 ment render professional string quartet , 
 lead Mr. S. Dean Grimson . Mr. J. G. Callcott preside 
 harmonium , Mrs. Williams piano 

 prospectus Brighton Sacred Harmonic 
 Society , conductorship Mr. Robert Taylor , 
 announce Concerts season 1887 . , 
 31st inst . , Beethoven Mass C Rossini 
 " Stabat Mater " perform ; second , June 
 16 , selection Haydn " creation , ’' Handel * * Corona- 
 tion " Anthem , Sullivan * ' Martyr Antioch " ; 
 , November ro , Costa " Eli . " ' leader 
 band Mr. W. Baker , Mr. J. Spearing , jun . , 
 preside organ 

 tue member Holborn Choral Society , 
 Concert second season Holborn Town 
 Hall , roth ult . programme 
 consist Prout Cantata ' " Alfred , " extremely 
 render , solo vocalist Miss Chapuy , Mr. 
 Thurley Beale , Mr. W. Price . second 
 miscellaneous ; vocalist Miss Chapuy , Miss A. 
 Brooks , Mr. Hutchinson , Mr. A. Greenwood . Mr. 
 Tobias Matthay contribute pianoforte solo , Mr. 
 Webster solo organ 

 176 MUSICAL TIMES.—Marcu 1 , 1887 

 Italy , addition wagnerian music drama , opera | experiment entirely successful , Concerts well- 
 Mozart Weber , Beethoven " Fidelio , " | attend highly appreciative audience . 
 produce . promising young singer , Fraulein Hedwig Sicca , pupil 
 Herr Franz Rummel Concerts chamber music | Herr Julius Stockhausen , lately début 
 continue attract numerous appreciative audience } german concert - room , sympathetic soprano 
 Berlin past month . voice excellent training meet universal 
 selection M. Gounod * ' Mors et Vita , " | recognition . 
 Conservatoire Concert 13th ult . , consist accord recently publish report , Stuttgart 
 follow number — viz . , ' ' Lacrymosa , " ' " quid sum miser , " ' | Conservatorium attend 528 pupil , 
 ' Felix culpa , " ' " Judex , " ' Pie Jesu , " Agnus Dei , " | number 89 foreigner — viz . , 46 english , 39 
 special importance attach production | United States , 3 India , Africa . 
 lead institution question , work } new opera entitle " Das Ellishorn , " Herr Rudolf 
 hitherto hear french capital } Raimann , course mount Munich 
 Trocadéro . M. Arthur Pougin refer per-| Hof - Theater . 
 formance Le Ménéstrel following term : — ; excellent performance Byron ' Manfred , " 
 portion select conservatoire M. Gounod Robert Schumann music , recently ( 
 Oratorio produce sensation , success , | time ) Stuttgart Hof - Theater , direction 
 especially " Judex , " far surpass | able capellmeister , Dr. Klengel . 
 witness Trocadéro performance year . } theatre construct model Bayreuth 
 add , , Madame Krauss .. .| Festspielhaus , capable accommodate 3,000 
 render justice music master , } spectator , build Buenos Ayres . 
 excellent artist ably second Madame Marie ; M. Saint - Saéns opera " Henri VIII , " shortly 
 Masson , MM . Auguez Rinaldi . pathetic | perform , time Germany , Frankfurt 
 powerful quartet chorus , constitute | Stadt - Theater . 
 * * quid sum miser , " particularly afford Madame } Dr. Hans von Bilow series Beethoven 
 Krauss opportunity display superb quality , | recital german capital early 
 shine brightestin soprano solo " Felix | present month . 
 culpa . " number represent pro- richard Wagner book , entitle ' * Oper und Drama , " 
 gramme , inclusion ' Judex " particularly | translate Italian Dr. Cesare Pollini . 
 happy choice ; superb instrumental prelude tothis nobly ; correspondent write Frankfurt - - Main : 
 inspired piece , beautiful phrase violin , | ' interest reader hear 
 effect , marvellous outset , in- | opera , Herr Anton Urspruch , accept 
 crease tenfold come support powerfully | performance Stadt - Theater , libretto whereof 
 write choral ensemble : combine produce , tounde Shakespeare drama ' Tempest . ' 

 profound impression audience . " | new work , bear title ' Der Sturm , ' pro- 
 M. Bourgault - Ducoudray , french composer , , duce present season , 
 complete opera act . | principal novelty 

 j'ctais , " 

 Leicest ! 1c Mr. Harvey Lohr Cis 
 Concert , Tuesda . , exce aaly interesting , in- 
 clude sonata F " Beethoven ( ( Op . 17 ) , ye horn piano- 
 torte excellently play e Mr. Probin Mr. Lohr , Larghetto | 
 Rondo Mozart Horn Con icerto E flat , Mr. | 
 probi n refined style pure tone : ntageously display , 
 Handel Suite g minor , finely rende pianoforte 
 Mr. Lohr . Mr. Lohr assist Mr. Whitehouse ( violoncello ) , 
 artist unite talent success Beethoven | 
 Sonata major ( Op . 69 ) . Mr. Whitehouse play solo , | 
 Miss Ambler win hearty recall charr singing 
 choose song . Hector Berlioz La de Faust | 
 . Philharmonic Society roth ult . , Temper- | 
 ance Hall , natke success . principal vocalist Miss 
 Mary Davie . Pyatt , Mr. Santley , , need- 
 , thoroughly satis factory arduous music entrust 
 . ' chorus finely render , 
 conductorship Mr. Ellis ( work hard ensure 
 satisfactory result ) , band , lead Mr. Val Nicholson , 
 desire . durir ¢ interval 
 rehearsal work , beautiful r ince d ivory biiton 
 present Mr , Ellis , honorary Conductor , tenor 
 bass Society , recognition time attention 
 devote tothem preparation Faust . presentation 
 Mr. J. Herbert Marshall , gracefully acknowledge 
 recipient 

 Lonponperry.—The member St. Columb Choral Union 
 Concert season , Uni Hal l , Tuesday even- 
 ing , sth ult . , Barnett b Ship per- 
 form . solo sustain ie Mary Russell 

 Luton.—An Organ Recital Christ Church , oth 
 ult . , Mr. , J. Lambert , Organist Church , programme 
 include March ( Zimmermann ) , Study ( Chipp ) , Minuet trio 
 ( Mozart ) , Andante ( Grison ) , March Naaman ( Costa ) ; Mr. J , 
 Heald sing ' deep deeply ' ? ' ' waft , angel " 
 ( Handel ) , " thou faithful ' ( Mendelssohn ) . Recital 
 great s satisfaction 

 MarpeENHEAD.—The Philharmonic Society second Concert 
 season Town Hall , Tuesday evening , 15th ult , 
 large audience . chief item programme 
 selection St. aul . principal artist Miss Katherine 
 James , Mr. Hagyard , Mr. Shepley . second 
 Concert include movement Beethoven Symphony D — 
 Larghetto Finale — Boccherini Minuet , & c. ' Concert 
 successful . Mr. J. G. Wrigley , Mus . Bac . , aa , conduct 

 MALveRN.—Mr . W. Elzy benefit Concerts Assembly 
 Rooms , rcth ult . , large audience . vocalist 
 Mrs. Mason , Miss Helen Dew , Mr. Sims Reeves ; Signor E. 
 Rubini ( selo pianist ) , Messrs. Hartung Eisen ( violin ) , Daeblitz 
 ( viola ) , Hofmayr ( cello ) , Larkin foot itra - basso ) , Mr. H. Nicholson 
 fl evening Popular Concert , Mr. Bickley Birming- 
 ham Glee Union assist 

 salem " " thou f : sithful unto death 
 ( Rossini ) , ' ' hallelujah ' ( Beetho 

 ng , ' ' o wing hh . eee 
 ; well- ' Gounod ) , Beethoven " Halle ulist Miss 
 ance , Jessie Royd , Mr. Sylvester , Mr. Love 
 shoral preside 1 Mr. f Concests ta 
 art Society sing portion vocalist 
 3ehr Organ recital t 

 oices , 3rd ult . , atte 
 work tl ) 
 render . vocalist Mr 

