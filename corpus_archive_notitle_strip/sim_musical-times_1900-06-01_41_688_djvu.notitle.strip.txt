PRINCIPAL TENOR, Trinity College, Cambridge). 
London and Provincial Oratorio and Ballad Concerts

BEETHOVEN'S CHORAL SYMPHONY AT NEWCASTLE-ON-TYNE.— 
Conductor: Dr. Richter.—" It would be difficult to get together a 
more thoroughly efficient quartet than Miss Agnes Nicholls, Miss 
Muriel Foster, Mr. Joseph Reed, and Mr. Daniel Price, who sang the 
florid passages with exceptional clearness and careful phrasing. Mr. 
Reed’s artistic singing of the tenor solo deserves a special word of 
praise.”"— Yorkshire Post

Terms: 31, Chesterton Road, Cambridge

379

picturesque lute or guitar. A young lady has 
been helping to stimulate after-dinner conver- 
sation by her performance of a composition 
sufficiently light in calibre not to interfere with 
the proceeds of digestion. She is still toying 
with the keys, as the enamoured youth at her 
side expresses his conventional gratitude, 
moved perhaps more by her eyes than her 
fingers. A much more platonic affection is 
suggested by Mr. Carl Schloesser’s ‘A Duet’ 
(929), but I cannot help fancying that the old 
professor sitting at the harpsichord has a 
sincere regard, if of a fatherly kind, for the 
agreeable young lady who is playing the 
mandoline—or is about to play it, for she has 
not yet her plectrum in her hand. Perhaps 
the ‘harpsichord ’ is really an early pianoforte, 
for there is a portrait on the wall that seems to 
be meant for Beethoven, which would bring us 
well into the reign of the pianoforte. As the 
keyboard is out of sight, we are in this matter 
left to conjecture

In looking through the Academy after 
musical subjects I have come across a really 
curious coincidence. Three recently elected 
Academicians have sent to the exhibition their 
diploma works, which have to be deposited at 
Burlington House on their election to full 
membership, and each of the three has chosen 
a musician as his subject. Sir W. B. 
Richmond has taken for his theme the proto- 
type of the modern virtuoso, Orpheus (138), 
whom he represents returning from the shades, 
in illustration of Shelley’s lines

should be no performance that evening. But Lachner 
sent for young Levi, who used to attend rehearsals, 
and asked him to undertake the work. ‘Getrau'’st Du’s 
Dir?’ (Do you dare?) he asked. The young man 
said yes—and the performance went very smoothly. 
By an odd coincidence, the opera was Halévy’s ‘ La 
Juive

Levi's recollections of his visit to London were 
very pleasant ; but he insisted (as he said in the inter- 
view in question) that he could not feel at home with 
Beethoven’s Seventh Symphony in the key of B flat. 
At the time he was here—1895—we still enjoyed the 
luxury of the high pitch

Mr. RuskIN contemplated writing a ‘ Treatise on 
the Principles of Music.’ Judging from his references 
to the divine art scattered throughout his published 
works, the achievement of this intention would have 
been a most interesting contribution to the literature 
of music from so distinguished an author

Sunpay (July 22).—Special service in the Cathedral at 
6.30 p.m. ‘Hymn of Praise,’ Mendelssohn

WepnespAy (July 25).—Morning : ‘ Elijah,’ Mendelssohn. 
Evening: ‘Zion,’ Gade; ‘Symphonie Pathétique,’ 
Tschaikowsky ; Mass in C, Beethoven

Tuurspay.— Morning: ‘Funeral and Triumphal’ Sym- 
phony, Berlioz (first performance in England); 
*Requiem, Joseph C. Bridge (composed expressly for 
this Festival). 
Afternoon: ‘Song of Miriam,’ Schubert (with Herr 
Mottl’s new accompaniments) ; ‘Good Friday’ Music 
from ‘Parsifal,) Wagner; ‘The Deluge,’ Saint- 
Saéns (first performance in England). ; 
Evening : (in the Music Hall), ‘ Faust,’ Berlioz

Organist—Mr. J. T. Hughes. 
Conductor—Dr. Joseph C. Bridge

HEREFORD. 
September 9, 11, 12, 13, and 14. 
Sunpay (September 9g), at 10.30.—Special opening 
service in the Cathedral. Benedictus, Mackenzie ; 
Te Deum and Benedictus, Elgar; ‘It came even to 
pass,’ Ouseley; and ‘ Hallelujah,’ Beethoven

TuEspay (September 11).—Morning: Patriotic per- 
formance. National Anthem; mew work by Sir 
Hubert Parry; Symphony in D (No. 2), Brahms; 
Requiem, Verdi

Evening: ‘Creation’ (Part I.), Haydn; ‘Dixit 
Dominus,’ Leo; and ‘Symphonie Pathétique,’ 
Tschaikowsky

THuRspDAY.—Morning : New work, Horatio W. Parker; 
Good Friday and Grail Music (‘ Parsifal’), Wagner ; 
‘ Choral Symphony,’ Beethoven. 
Evening : ‘God goeth up,’ Bach; new work by 
S. Coleridge-Taylor; ‘Hymn of Praise,’ Mendelssohn. 
Fripay.—Morning : ‘ Messiah,’ Handel. 
Evening : Chamber Concert in the Shire Hall

PRINCIPAL VOCALISTS

BIRMINGHAM. 
October 2, 3, 4, and 5

The programme will include: ‘Elijah,’ Mendelssohn ; 
‘ Messiah,’ Handel; ‘St. Matthew Passion,’ Bach; 
‘ Spectre’s Bride,’ Dvorak; ‘Mass,’ Byrd; ‘ Requiem,’ 
Brahms; ‘Scenes from the Song of Hiawatha,’ S. Coleridge- 
Taylor ; ‘ De Profundis,’ Parry ; and ‘ Dream of Gerontius,’ 
Edward Elgar (composed expressly for the Festival) ; 
Symphonies by Mozart, Beethoven, Schubert, and 
Glazounow, and other Orchestral Works

PRINCIPAL VOCALISTS

FESTIVAL OF THE SONS OF THE CLERGY

THE 246th Festival of the Corporation of the Sons 
of the Clergy was celebrated in St. Paul’s Cathedral, 
on the oth ult., in which, as usual, a full orchestra 
and achoir of 300 voices participated. Sir Arthur 
Sullivan’s ‘In Memoriam’ Overture opened the 
service, which included Myles Foster’s Magnificat 
and Nunc dimittis in A, Bach’s cantata ‘ Sleepers, 
awake’ (‘Wachet Auf’), the Old tooth Psalm, and 
Beethoven’s ‘ Hallelujah’ chorus, the last-named 
superseding Handel’s ‘ Hallelujah,’ which for very 
many years was sung at this anniversary service. 
Sir George Martinconducted. The office of conductor 
at this Festival has not, however, always been held by 
the organist of the Cathedral. Dr. William Hayes 
and Dr. Boyce both discharged this duty. The 
former added instrumental parts to the Old 1ooth for 
the Festival (are they still in use?) and the latter 
composed his anthems ‘ Lord, Thou hast been our 
refuge,’ and ‘ Blessed is he that considereth the poor 
and needy.’ In 1844, the orchestra, which had 
always been a special feature of the Festivals, was 
suppressed at the instigation of the Bishop of London 
(Dr. Blomfield.) It was not reinstated till 1873, its 
re-introduction being one of the ‘good things’ 
resulting from Sir John Stainer’s organistship. For 
the Festival of 1862 Sir John Goss composed his fine 
anthem, ‘ The Wilderness

CHURCH MUSIC IN MONTREAL

Hymn before Action. Arranged from the song by 
Rudyard Kipling and Walford Davies

Creation’s Hymn. Words by the Rev. J. Troutbeck. 
Music composed by L. van Beethoven, Arranged by 
C. Sachs

Music by

Action’ has already been so widely recognised as to render 
description unnecessary and praise superfluous ; but it 
should be said that this setting is admirably suited for large 
choirs. We should like to hear it delivered by the tenors 
and basses of the Royal Choral Society

Mr. Sachs’s arrangement of Beethoven’s magnificent 
‘Creation’s Hymn’ also seems to crave for abnormal 
power of vocal tone, but with average sized choirs it would 
also be very impressive. It should be added that the 
English version of the text is by the late Rev. J. Troutbeck, 
and that Mr. Sachs has accomplished his work with 
judicious skill

PIANOFORTE MUSIC

There is much diversity of opinion as to the effect of the 
combined bands, and that this effect was in every way 
ideal we should be the last to contend. But, on the other 
hand, to laugh at the experiment as nothing but a symptom 
of latter-day megalomania is absurd. It was not merely a 
large band enlarged; it was the fusing into one of two 
orchestras, each with an idiosyncrasy of itsown. The tone 
of the 128 strings was not merely like a doubling of the 
tone of each body of strings. The net result was a 
wonderful union of the breadth and depth, which are the 
chief strength of our English strings, with the lightness and 
sweetness which are the great charm of the French

At each concert there was a symphony: these were the 
‘Pathetic’; Beethoven’s Fifth and Eighth, conducted by 
Mr. Wood; and Beethoven’s ‘ Eroica’ and Seventh, and 
Schubert’s ‘ Unfinished,’ conducted by M. Chévillard. 
Contrary to expectation, the ‘ Pathetic’ fared worst in the 
changed conditions, while the Beethoven Symphonies 
gained in force and grandeur not a little. The only move- 
ment of Beethoven which really suffered was the Finale of 
the Seventh Symphony. The ‘Funeral March’ of the 
‘ Eroica ’—especially the fugal passages—was superb and 
the Finale irresistible. The Trio of the Scherzo of the 
Seventh was magically beautiful, and the Fifth was infinitely 
majestic throughout. Of the Wagner excerpts, the 
‘ Meistersinger’ Overture, the Prelude and Death Scene 
from ‘ Tristan,’ and the ‘ Funeral March’ from the ‘ Gotter- 
dammerung’ were the most impressive. In the last of 
these M. Chévillard showed himself a conductor of origin- 
ality and real merit. He does not merely follow the 
traditions of M. Lamoureux, but is far more dramatic and 
vigorous. And his experience is so wide that he can secure 
what effect he wishes with certainty. He quite shared the 
honours with Mr. Wood, though on the whole, perhaps, 
the greater amount of the best work was done under the 
English conductor

The soloists were Madame Albani, Miss Clara Butt, Miss 
Lillian Blauvelt,and M. Ysaye. The violinist stood out head 
and shoulders above everyone else. His performances of 
Max Bruch’s Concerto in D and Vieuxtemps’s Concerto 
(No. 4) were superb, but paled by the side of his masterly 
reading of Bach’s ‘Chaconne.’ Of the singers, Miss 
Blauvelt, perhaps, distinguished herself most, singing 
delightfully each time. Particularly well did she sing on 
the last afternoon, when she introduced a song by 
Johann Strauss, ‘ Voci di Primavera,’ orchestrated by Mr. 
Clarence Lucas, which may be cordially recommended to 
sopranos who wish for a change from ‘ Sweet Bird

The fifth concert, on the 24th ult., brought forth 
a novelty in the form of an orchestral suite by Mr. S. 
Coleridge-Taylor, entitled, ‘Scenes from an Every-day 
Romance,’ originally announced, by the way, as ‘ Miniatures 
from an Every-day Comedy.’ Since, however, the com- 
poser furnishes no clue to the romantic happenings in his 
four ‘scenes,’ the title is somewhat in the nature of a 
ted-herring drawn across the path of his music, in that 
speculation upon what it is all about is apt to distract the 
listener from the music per se. The four movements of 
the suite consist of (1) an Allegyo, E minor, 6-8 time; 
(2) an Andante, G major, 2-4 time; (3) a Tempo di valse in 
B minor, and (4) a Presto, E minor, in 2-4 time. Mr. Taylor 
has made so great and deserved a reputation with his 
‘Scenes from the Song of Hiawatha,’ that any new work 
from his pen commands the respect due to his genius. 
Opinions will doubtless be strangely at variance as 
to the merits of this, his latest achievement—in fact, 
even in the concert-room, after the performance, it 
was quite possible to learn that one critical hearer bestowed 
upon it the laudation ‘perfectly lovely,’ while another, 
equally competent to judge, dismissed it with ‘ awful stuff.’ 
Time, the great tester, will judge as to these differences of 
opinion. His verdict will probably avoid these extremes. 
In listening to Mr. Taylor's recent strains there arises 
the question, ‘Is the eight-bar phrase to be banished from 
modern symphonic music?’ Such an interrogator will pro- 
bably be called old-fashioned by the young bloods. But 
there seems to be a danger of the domination of the two- 
bar or four-bar phrase, and the snippity wee-bittieness 
which may result therefrom in their so-called ‘ development.’ 
What may be termed ‘the barking of the brass’ may 
also become a snare to young composers, who may. now 
and then be reminded that a composer named Schubert 
had some very good friends in the wood-wind of the 
orchestra

In regard to the remainder of the concert, lovers of pure 
melody, as from a pellucid stream, derived supreme satis- 
faction in the strains of an air, with six dainty variations, 
in F by the master-melodist, Mozart, from a Divertimento 
in B flat, meaninglessly designated ‘No. 15’ in the 
programme. Herr Ernst von Dohndnyi gave an artistic 
interpretation of Beethoven’s ever-welcome Pianoforte 
Concerto in G, and Miss Ada Crossley was successful in the 
‘Inflammatus’ from Dvordk’s ‘ Stabat Mater.’ The concert 
concluded with Berlioz’s fine and finely played symphony, 
‘Harold in Italy,’ in which that excellent artist, Mr. A. 
Hobday, gave every satisfaction in the viola obbligato. 
With the exception of the suite, which was conducted 
by its composer, the concert was under the direction of 
Mr. Cowen. Both concerts took place at Queen’s Hall

YSAYE CONCERTS

THE first of the three ‘Ysaye’ concerts, which took 
place on the 17th ult., at the Queen’s Hall, bore convincing 
witness to the superlative powers of the Belgian violinist. 
To announce three concertos—Bach’s in E, Beethoven’s, 
and Mendelssohn’s—was to show remarkable faith in the 
musical public, to play them was a herculean task, and to 
succeed, as M. Ysaye succeeded, was a veritable tour de 
force. To the student of music the selection possessed 
exceptional interest. The style of each work is so distinct 
and each in its own way so great. In loftiness and nobility 
Beethoven’s immortal composition rightly occupied the 
central place, and it towered above its companions. 
Although to the thoughtful comparison of the works was 
inevitable, M. Ysaye seemed to be in such perfect sympathy 
with each that there was no desire to compare the merit of 
his readings. Mr. Wood secured an ideal accompaniment 
from the orchestra, the sympathetic manner in which M. 
Ysaye was supported in delicate passages resulting in 
entrancing effects. Mozart’s Allegro in D, heard on this 
occasion for the first time in England, is the last movement 
of a symphony, and is a naive example of the master’s 
genius when he was fifteen years of age. It is in sonata- 
form, laid out for strings, oboes, and horns, and possesses 
many touches that will appeal to admirers of Mozart

MADAME ALBANI’S CONCERT

THE BRIDLINGTON MUSICAL FESTIVAL. 
(By our SpEcIAL CORRESPONDENT

THE customary spirit of enterprise characterised the 
annual festival which is ‘run’ by Mr. Bosville at Bridling- 
ton, and which took place for the seventh time on April 26. 
It is astonishing what this most enterprising amateur 
accomplishes with a purely local chorus and a scratch band 
of players gathered from various parts of Yorkshire. A 
bare enumeration of the compositions heard at the afternoon 
and evening concerts will demonstrate this. In the after- 
noon we had Verdi’s ‘Requiem,’ Beethoven’s Eighth 
Symphony, three pieces from Mr. T. T. Noble’s ‘ Wasp’ 
music, and a March by Tschaikowsky. In the evening the 
choral works were Coleridge-Taylor’s ‘Death of Minne- 
haha’ and Stanford’s ‘ Revenge,’ with the ‘ Coriolan,’ 
‘Midsummer Night’s Dream,’ and ‘Carnaval Romain’ 
Overtures, the ‘ Siegfried Idyll,’ and some vocal solos

Enterprise has, however, always characterised the 
programmes, but it is satisfactory to be able to record 
a distinct advance in the efficiency of the performances. 
The ‘Revenge’ was not satisfactory: it had been given 
before at one of these festivals, and perhaps too much 
reliance was placed by the chorus upon their familiarity 
with a work that is, as a matter of fact, extremely catchy. 
The ‘ Requiem,’ too, strained the resources of both band and 
chorus pretty severely, but with these exceptions the 
performances were most creditable, the chorus singing 
with an intelligence that went far to atone for certain 
defects that seem inseparable from North and East Riding 
choirs, and the band being more efficient than it has ever 
been before. No doubt this may be due in great measure 
to the increased power over his forces which experience is 
giving to Mr. Bosville, who, with less apparent exertion, 
exercises a greater control on both players and singers 
than he did aforetime. This was especially notable in the 
Symphony, of which an excellent all-round performance 
was given, the conductor's reading evidencing sympathy 
and an intelligent study of the music. ‘The Death of 
Minnehaha,’ which had not previously been heard in 
Yorkshire, was exceedingly well done, far better than 
might have been expected in the case of a work both 
novel and difficult. It made a really profound impression, 
to which the highly sympathetic singing of the two 
soloists, Miss Agnes Nicholls and Mr. F. Harford, con- 
tributed not a little. It is indeed remarkable that a poem 
which, no doubt because of its monotonous metre, had 
hitherto attracted no composer of note, should have pro- 
vided the material for two works as powerful, as picturesque, 
and, above all, as varied as Mr. Coleridge-Taylor’s 
‘ Wedding-Feast’ and ‘ Death of Minnehaha.’ There is 
a note of tragedy in the latter that is genuinely moving, 
and one feels that heart as well as head has had a share in 
the work. The description of Minnehaha’s hallucinations 
would seem to have appealed less strongly to the composer

401

April 26, the morning and afternoon were devoted to solo 
singing and village orchestra competitions. Twenty-seven 
soloists presented themselves, and there were four village 
orchestras. The test piece for the orchestras was Schubert’s 
Overture to ‘ Rosamunde.’ The playing showed a great 
improvement on that of previous years. It was not 
to be expected that such orchestras could be anything 
like complete, but even with the makeshifts for brass 
and wind the result was extremely creditable. The 
Leasgill band, under Mrs. T. A. Argles, gained the first 
place. At the evening concert, in which choirs from fifteen 
places took part, ‘ Phaudrig Crohoore’ (Villiers Stanford) 
was performed, under Miss Wakefield’s direction, with great 
success. The combined choirs numbered over five hundred 
singers. Mr. Plunket Greene sang eighteen songs in inimit- 
able fashion, including the ‘Maud’ cycle, composed by Mr. 
Arthur Somervell ; the ‘Scotch’ Symphony was admirably 
played under the direction of Mr. Risegari. On the 27th, 
the morning and evening were occupied with competitions 
of village choirs variously constituted. Eleven female voice 
and twenty mixed voice choirs competed. In the chief 
section, the test piece was ‘Sweet love for me’ (Villiers 
Stanford). The Kendal Parish Church Choir gained the 
first place, Yealand coming close behind. The singing 
throughout the day was of a high standard. The evening 
concert was commenced by a fine performance of 
Beethoven’s C minor Symphony, conducted by Mr. 
Risegari. Mr. Plunket Greene sang twelve songs in two 
sets, and was received with the greatest enthusiasm. The 
chief feature of the programme was Coleridge-Taylor’s 
‘Hiawatha’s Wedding-Feast,’ upon the preparation of 
which Miss Wakefield, who conducted, had spared no 
pains. Mr. Henry Beaumont sang the tenor solo. The 
work was well received by the large audience. At each of 
the evening concerts Miss Wakefield's popular setting of 
Colonel Hay’s words, ‘When the boys come home,’ was 
sung by the chorus with much fervour. Saturday, the 28th, 
was the children’s day. Fourteen junior choirs competed. 
Last year it was generally felt that the children did not 
sing as well as in some former years, but this year there was 
an unmistakable improvement. Most of the singing was 
acceptably in tune and afforded evidence of skilful training. 
After the competition, at the prize distribution, a concert 
was given, the chief feature of which was the humorous 
cantata ‘The Frogs and the Ox,’ by Shapcott Wensley 
and Sir Frederick Bridge. Dr. McNaught adjudicated at 
all the competitions. Mr. A. H. Willink proved himself 
to be an able secretary

At the last evening concert Miss Wakefield definitely 
resigned her position as conductor. She felt that, after 
fifteen years’ service, she was entitled to a rest, and she was 
proud to leave the Festival in flourishing condition. All 
who know of Miss Wakefield’s remarkable work in 
creating an enthusiasm for music in a district formerly 
stigmatised as being unmusical will wish her well in her 
well-earned retirement. As we propose shortly to give a 
fuller account of this work we need say no more on this 
occasion

Tue Victoria Madrigal Society brought its season to a 
successful conclusion, on April 26, at St. Martin’s Town 
Hall. The choralists remained true to their mission by 
singing, under the watchful conductorship of Dr. G. 
Stanley Murray, such justly esteemed compositions as 
Weelkes’s ‘As Vesta was,’ Morley’s ‘ Now is the month 
of maying’ and ‘What saith my dainty darling,’ and 
Wilbye’s ‘The Lady Oriana.’ These and other pieces 
were sung with genuine taste combined with spirit. Vocal 
contributions were forthcoming from Miss Gertrude 
Macaulay and Mr. Henry Turnpenney, and harp solos 
from Miss Molteno

Tue North-West London Choral Society, of which Mr. 
W. H. Speer is the conductor, honourably acquitted itself 
at the Hampstead Conservatoire on the 7th ult. The 
choralists, numbering about a hundred, sang with steadiness 
and regard for detail the anthems ‘ Ascribe unto the Lord’ 
(S. S. Wesley), Stanford’s ‘Awake, my heart,’ and 
Mendelssohn’s ‘ Hear my Prayer,’ their efforts in the last- 
named being particularly deserving of praise. Madame 
Isabel George and Mr. W. P. Richards were the vocal 
soloists, Mr. Healey Willan did excellent work at the 
organ, and Herr Georg Liebling played Beethoven’s 
‘Sonata Appassionata’ and other pianoforte compositions

THE South London Choral Association, on the 2nd ult., 
gave a very satisfactory interpretation of Haydn’s First 
Mass in B flat and of Mendelssohn’s ‘ Athalie,’ the 
principal solo vocalists being Misses E. Coward and Ch. 
Edwards, Madame Edith Hands, and Messrs. F. Bamford 
and J. Lacey. The excellently trained choir, under the 
direction of Mr. Leonard C. Venables, entered upon their 
task with great spirit and musical intelligence. Nor 
should a word of praise be omitted as to the share taken in 
the performance by the orchestra. Mr. Richard Temple 
recited the connecting narrative in ‘ Athalie’ with much 
fervour and dramatic effect

THE sixth annual concert of the Marlborough Place 
Amateur Orchestral Society took place at the Hampstead 
Conservatoire on the 22nd ult. The programme included 
Schubert’s ‘Unfinished’ Symphony, Mendelssohn’s 
‘Athalie’ Overture, Mackenzie’s ‘ Benedictus,’ and two 
of Moszkowski’s Spanish Dances. In the interpretation of 
these works, the performers gave abundant evidence of 
their efficiency under the skilful training and alert 
conductorship of Mr. Paul Oppenheimer. Mr. N. H. 
Geehl brilliantly played the first movement of Beethoven’s 
Pianoforte Concerto in C, and Mrs. Helen Trust con- 
tributed some songs with acceptance

AT the fine new Parish Church of St. Mary’s, Hornsey, 
an excellent rendering of Stainer’s ‘ Daughter of Jairus’ 
and Mendelssohn’s ‘Hear my Prayer’ formed the 
musical attraction at a special service, on the 16th ult. 
The soprano solos were admirably sung by Master Jeffries ; 
the tenor and bass work found capable exponents in 
Messrs. Thomson and Mason. The choir, numbering 
about seventy voices, sang with finish and precision, 
showing evidence of very careful training. Mr. Henry J. 
Baker conducted, and Mr. H. J. Baggs (assistant-organist, 
St. Mary’s) presided at the organ, the overture and the 
accompaniments throughout being tastefully and neatly 
executed

To the new Philharmonic Society fell the honour of 
giving the last concert in the room which has been the 
home of so much musical activity, and it may unhesitatingly 
be said that the performance was one of the Society’s 
highest achievements. The work selected for this farewell 
was Schumann’s ‘ Paradise and the Peri,’ and, on the 
whole, it received an excellent interpretation. A high level 
of artistic excellence was reached in the chorus following 
the contralto solo, ‘Sweet is our welcome of the brave ones,’ 
in the chorus of Houris, ‘ Wreathe ye the steps,’ in ‘ Oh, 
blessed tears of true repentance,’ and in ‘ Joy, joy for ever.’ 
The solos were in the capable hands of Miss Alice Esty, 
Miss Alice Larkin, Mr. John Coates, and Mr. Mansell 
Lewis, all of whom acquitted themselves excellently

The second part of the programme was devoted to 
Beethoven’s ‘ Pastoral’ Symphony and Wagner’s 
‘ Meistersingers’ Overture, and the band, under the 
skilful leadership of Mr. Lewis Hann, finely interpreted 
both works. Mr. C. J. Phillips is a born conductor; he 
knows his score thoroughly and never misses a point. 
The orchestra, however, too often gave evidence of the 
common fault of over-assertiveness in accompanying the 
vocal music. Conductors need to exercise great firmness 
in this matter and to insist that the accompaniments be 
subservient to the voices

The Society has made a wise choice in appointing its 
new and courteous secretary, Mr. C. E. Rainger, whose 
arrangements were in every way admirable, and added not 
a little to the success of a memorable concert

in Wilbye’s madrigal, ‘ Flora gave me fairest flowers,’ a too 
solid and rigid performance marred the effect of a delicious 
piece of light and wanton expression. The choir is 
immensely improved. The basses are still somewhat 
weak, but the quality of the ladies’ voices is admirable. 
This was very noticeable in the motet for female 
voices, ‘The heavens declare the glory of God,’ by 
Dr. Culwick, which, as a composition, is distinctly above 
the average of such works. Mrs. A. McC. Stewart (7ée Alex 
Elsner) was enthusiastically welcomed once more at a 
Dublin concert. Mr. Henry Verbrugghen, a clever artist, 
and Mr. Edwin Wolseley were the other soloists

The last concert of the season given by the Dublin 
Orchestral Society took place on the 2nd ult., in the 
Theatre Royal, when Beethoven’s Symphony in F (No. 8) 
was played for the first time in Dublin, when it received an 
excellent rendering. The Schumann Concerto, with Signor 
Esposito as the pianist, was repeated, and the solo artist 
secured a perfect ovation at itsconclusion. Humperdinck’s 
Vorspiel to ‘ Hansel und Gretel’ was played for the first time 
here by the Society, and also the ‘ Tannhauser’ Overture. 
The performance of the latter was quite a revelation to 
those who think a piece of Wagner’s can only be inter- 
preted by an imported band. Signor Esposito is to be 
congratulated on the signal artistic success of the second 
season of the Orchestral Society

MUSIC IN EAST ANGLIA. 
(FROM OUR OWN CORRESPONDENT

The Eyam Choral Society performed Haydn’s ‘ Creation,’ 
on the roth ult., under the direction of Mr. J. W. Froggatt. 
There was a large audience and the fine singing of the 
chorus was a feature of the concert. A small orchestra 
was excellently led by Mrs. Smedly, who also played violin 
solos with striking success. Mr. T. Mellor was the 
organist and Mr. E. A. Ireland accompanied. The 
principals were Mrs. Haynes, Mr. H. Bryars, and Mr. A. E. 
Innocent

408 THE MUSICAL TIMES.—Junz 1, 1go00. 
MUSIC IN WALES. | in its soul than atin spoon. This was a pity, for the (largely Milit 
| amateur) strings did well. The soloists were Miss Estella the c 
(FRoM OUR OWN CORRESPONDENT.) | Linden, who gave a good reading of the soprano part, of pie 
Ar the third concert of the Cardiff Musical Society, | though unable to do herself justice in consequence of a bad Alt 
given on April 25, Cowen’s ‘Ode to the Passions’ and cold ; Mr. Henry Beaumont, whose fine tenor voice rang in th 
Mendelssohn’s ‘Hymn of Praise’ were performed with | Out well in ‘Onaway! awake, beloved!’; and Mr. W. E. for th 
much success, the solo vocalists being Miss Gertrude ; Davies, who should have made much more of the most by si 
Drinkwater, Madame Nellie Griffiths, and Mr. Wetten, of | touching baritone solos in ‘The Death of Minnehaha.’ their 
Bath. Mr. T. E. Aylward conducted, as usual. The works and their composer were most enthusiastically 
On April 28 the ‘Hymn of Praise’ was satisfactorily | received. . : 
performed by the Newtown (Mont.) Harmonic and Orchestral} _ The concert also included Tschaikowsky’s posthumous 
Society, under the conductorship of Mr. J.C. Gittins. The | duet, ‘Romeo and Juliet,’ and the Overture-Fantasia of the Ro 
soloists were Miss Emily Davies, Miss E. J. Taylor, and|Same title. These two works were on this occasion has b 
Mr. Gwilym Richards. performed side by side for the first time in England, and Lond 
For this summer’s holiday course in music, at the| probably anywhere. Their juxtaposition proved highly Hans 
University College of Wales, Aberystwyth, the following | interesting, because the music of the duet elucidates the Schol 
teachers have been appointed: Pianoforte and organ, Mr. | Overture, the love music in the two works being virtually (of Si 
Leah; violin, Mr. Ollerhead ; voice, Mr. Wilfred Jones; | identical. The beautiful duet was sung by Miss Linden and Cumr 
art of class-teaching, Mr. W. T. Samuel; and harmony, | Mr. Beaumont with the requisite amount of passion, and the Stern 
counterpoint, composition, and orchestration, Mr. David difficult overture went well under the spirited direction of Oscat 
Jenkins, lecturer in music at the College. Welsh musical | Mr. Atkins. : Oscar 
students should largely avail themselves of the opportunities | _ Two days afterwards, in the afternoon of the 5th ult., and \ 
offered them through the medium of these annual holiday |the Worcestershire Philharmonic Society, conducted by TH 
courses. Mr. Edward Elgar, gave its fifth concert in the same hall. ‘Con: 
It was asmart enough function in all conscience. Adream the p 
of fair women! Such gowns! such hats! But the music's mont! 
the thing we have to discuss. Well then, to begin with, orche 
MUSIC IN WORCESTER. we had one of the most — ag —sere So 4, “ 
Starting with that gorgeous chorus, ‘ Wach’ auf!’ from : 
(By our SPeciaL CORRESPONDENT.) Waals C Malahesttnaee which, we understand, ushers oon 
WitH the gorgeous tone of the London Festival double |in all the Society’s concerts (and the title of which is, 
orchestra still ringing in our ears, we wended our way, on | moreover, its rousing motto), we heard Wagner’s ‘ Rienzi’ TH 
the 3rd ult., tothe Public Hall in Worcester City. Thither ; Overture, Brahms’s ‘Schicksalslied,’ Beethoven’s Fourth rende 
had foregathered a most goodly company of music-lovers | Pianoforte Concerto (played with rare refinement and Hall, 
to let sweet music creep in their ears, and to wax warmly | beautiful technique by that too rarely-heard Irish pianist, solois 
appreciative over those much sung, much played, and oft- | Miss Elizabeth Reynolds), Mr. Granville Bantock’s orches- much 
maltreated twin cantatas, ‘Hiawatha’s Wedding-Feast’ | tral scene ‘ The Funeral’ (Section I. of his orchestral drama Kings 
and ‘ The Death of Minnehaha,’ and their gifted composer, | ‘ Kehama’), the Cavatina and chorus of Druids from Bellini’s and } 
who was toconduct. It was the Worcester Festival Choral |‘ Norma,’ Act I., Arcadelt’s motet, ‘Ave Maria,’ four of TH 
Society’s second concert of the season, and its enthusiastic | Brahms’s part-songs for ladies’ voices (Op. 44), and the the R 
young conductor, Mr. Ivor Atkins, to whom a thing of such | ballet (‘Dance of the Hours’) from Ponchielli’s ‘La Schol 
fresh beauty as Mr. Coleridge-Taylor's music seems indeed | Gioconda.’ There is eclecticism for you, ye London Mich 
a joy, had taken exceeding pains (so many-tongued rumour | compilers of hackneyed programmes! To say that all these excep 
wagged) to secure performances worthy of the works. | things were perfectly rendered would be beside the mark. becon 
Let us hasten to assure our readers that the chorus, for the | But the ‘Rienzi’ Overture, the orchestral portions of Colles 
preparation of which Mr. Atkins was responsible, easily and | Brahms’s cantata, the accompaniments to the concerto, On 
magnificently beat the soloists and orchestra out of the field. | and the ballet were played in a style little suggestive of M . 
We have heard many performances of the two cantatas, but |the fact that the orchestra of fifty performers contained b D 
can honestly affirm that the small, but most excellent | quite a large number of amateurs, chiefly, of course, amongst ie 
Worcester Society was an easy first as regards those|the strings. The most perfect playing was heard in i 
qualities which constitute good chorus singing—e.g., a good, | the divinely beautiful orchestral introduction and epilogue p “ore 
round, ‘musical’ tone, crisp attack, and exact release, |in the ‘Song of Destiny’: than this nothing could have rs 4 
true intonation, clear enunciation, and an intelligent | been better. Mr. Bantock’s ‘Scene’ left us (and the _ 
appreciation of the emotional side of the music in| public) utterly unimpressed. Much as we desire and At 
all its varying moods. These points are not brought out | strive to appreciate native music, we confess that this Blind 
without much careful and systematic work by a thoroughly | most productive of English composers has not yet suc- Road, 
competent choir-trainer, and to Mr. Atkins are due the | ceeded in convincing our dull brain that his ‘ Kehama,’ select 
thanks of those who were at last enabled to hear two such|in nineteen (or is it twenty-nine?) movements—most perfor 
beautiful works beautifully and expressively sung. monstrous of fearful wild-fowls amongst modern develop- Profe: 
We entered the hall anathematising (molto dolce e¢ piano) |ments of the symphony—contains any beautiful music. AT 
stern Duty that had called us away from the Lucullus| Yet there must be something ‘in it,’ or why should a the g¢ 
feast provided at Queen’s Hall, to chew the oft-turned cud of | brilliant musician and trenchant critic like Mr. Elgar was N 
‘mere provincial performances’! But no sooner had the | perform it? We cannot solve the riddle, but wait and organ 
full-toned chorus started, ‘ You shall hear how Pau-Puk- | hope that our eyes may be opened to perceive beauties Strab: 
Keewis,’ &c., than we forgot Queen’s Hall and its marvellous | to which we are still blind, alas! The choir sang with won t 
musickings, and were enjoying Mr. Taylor’s work as if it | refinement and expression, though as regards tone it 
had been absolutely new to us. For it is one of the | could not compare with that of the Festival Choral Society. TH 
wonders of this simple music that, given an adequate per- | The slow movement in Brahms’s ‘Song of Destiny’ and dedic: 
formance, it appears always fresh and delightful. the same great master’s unaccompanied part-songs (sung, orche: 
As we have said, the small chorus produced a beautiful | and excellently sung, in the original German !), were their Britis 
tone ; its quantity, however, was almost more astonishing | best efforts. War. 
than its quality, and it is evident that Mr. Atkins has in Mr. Henry Brown was the soloist in Bellini’s ‘ Cavatina,’ Mi 
these barely 100 voices one of the best and most workman- | while Mr. Elgar conducted with the thorough savoir faire who | 
like choral bodies in the Kingdom. Would that the | and sangfroid resulting from a most intimate acquaintance Royal 
orchestra had been as efficient. But, truth to tell, it was | with the technique of the orchestra, long experience, and was a 
not, the wood-wind and percussion especially leaving not a | an inborn aptitude, allied to an artist’s temperament and a Hr 
little to desire. Though drums and cymbals were repre- | genuine enthusiasm for the beautiful or worthy wherever to om 
sented, the frequent and often striking effects of cymbals | found in our art. We shall hear more of Mr. Elgar as a B hel 
struck with drumstick were omitted; the tambourine was | conductor in the future, or we are much mistaken. — 
conspicuous by its absence, though, surely, any amateur; Yet another concert, though of a very different kind, was MR 
might have struck it; and the triangle had no more music! given at the same hall, on the 8th ult., when the ‘ Civil- Alexa

XUM

