permission Directors 

 Beethoven 

 ScHERzO Dukas 

 1909 

 BEETHOVEN SKETCHES HITHERTO 
 UNPUBLISHED . 
 written Beethoven 

 sketch - books , peculiar method | 
 composing . offer photo- | 
 graphs thoughts . Owing succinct 
 times mysterious method recording 
 thoughts , large measure unfortunate 
 dismemberment sketch - books 

 works 
 stage . , , — 
 lesson composers — 
 continually trying transcribe faithfully 
 possible musical ideas . 
 moment , , offering striking illustra- 
 tion peculiarity method working 

 letter friend Wegeler — dated June 
 29 , 1800 1801 — Beethoven speaks 
 composing works time , 
 statement confirmed sketch- 
 books . instance , sketches 
 second act ‘ Leonore ’ found relating 
 Pianoforte sonata F minor ( Op . 57 ) , 
 known ‘ Appassionata , ’ song 
 ‘ die Hoffnung . ’ , sketches 
 opera , come Piano- 
 forte sonata F ( Op . 54 ) , triple 
 concerto ( Op . 56 ) . sheets filled 
 sketches , possession Miss A. E. Willmott , 
 offer illustration ; , kind 
 permission , describe . 
 composer engaged fewer 
 works time ; , contain 
 important hitherto unpublished sketches 
 composer greatest works , , 
 Pianoforte concerto E flat ( Op . 73 ) . 
 sketches movement concerto 
 begin 

 . 1 

 showing Allegro con brio section 
 developed , ’ based figure 
 given ; , printed version proves , 
 actually carried 

 bars Adagio sketched , 
 descending scale bar 5 ; 
 indication bars 
 sketch 
 B major section find specimen , 
 solitary , Beethoven suddenly thinking 
 coda composition . 

 . 10 

 marked 
 . 11 

 Comparison printed version enable | 
 readers points resemblance 
 difference . 
 Beethoven goes , carries mind , 
 section follows repeated 
 Adagio bars , 

 Presto 

 sonata , , shown , 
 thoughts time 

 sheets confirm Beethoven 
 statement , sole point interest . 
 Nottebohm ‘ Zweite Beethoveniana , ’ sketches 
 given works 
 reference . fact naturally 
 suggests question , connection 
 Miss Willmott Herr Nottebohm sketches 
 traced ? ‘ Zweite Beethoveniana ’ ( Art . 47 , 
 p- 495 ) , Nottebohm mentions a_sketch - book 
 containing sketches movement 
 E flat Concerto ( Op . 73 ) . regards 
 earliest ; moreoverheconsidersthat Beethoven 
 book middle 1808 
 beginning 1809 . Phantasie ( Op . 80 ) , 
 sketches , produced 
 concert given Beethoven Theater der 
 Wien December 22 , 1808 . ‘ sketches 
 work certainly 1808 , 
 Beethoven testimony music 
 composed shortly date 
 concert , quickly time 
 write score 

 brief description given 
 Concerto sketches Miss Willmott MSS . , 
 evident later date 
 given Nottebohm . positive 
 statement sketches 
 Phantasie , reason 
 believe belonging Miss Willmott 
 later Nottebohm 

 year 

 Zweite Beethoveniana , ’ p. 255ff ) , sets 

 sketches described . 
 set belong , says writer , ‘ 
 movement E flat Concerto ( Op . 73 ) , 

 second 

 Beethoven ist kein Bedienter — sie wollten einen Bedienten 

 den haben sie nun . 
 [ Beethoven servant — wanted servant , 

 

 77 undoubtedly 

 Beethoven - Heft , Year 3 , 1903/1904 , 
 2 March Heft Die Musik , Julius Levin 
 described interesting sketches 
 second movements E flat Concerto 
 sheets possession M. Malherbe . 
 Nottebohm , Levin , Miss Willmott 
 sketches collated , fuller 
 consistent account given Beethoven 
 preparatory work great concerto 
 ready publication 

 J. S. SHEDLOCK 

 October 11 , 18 , Compton Terrace , Islington , 
 Mr. JOSIAH FOUNTAIN MEEN . self - taught much- 
 respected musician , Mr. Meen born Hackney , 
 September 14 , 1846 . held organ appointments 
 successively successfully Clapton Wesleyan Chapel , 
 St. Mary Church , Stoke Newington , 1880 
 discharged similar duties faithfulness distinction 
 Union Chapel , Islington , fine building long associated 
 ministry Rev. Dr. Henry Allon . 
 seven years existence organist Sacred 
 Harmonic Society , 1886 professor 
 organ Guildhall School Music . man genial 
 kindly disposition , Mr. Fountain Meen 
 excellent organist , excelled pianoforte accom- 
 panist . remains cremated Golders Green , 
 ashes subsequently interred Highgate Cemetery 

 October 6 , Berlin , aged - , Dr. ALFRED 
 CHRISTIAN KALISCHER , distinguished Beethoven 
 scholar . best known editorship Beethoven 
 letters , valuable contribution life master , 
 recently available English readers 
 translation Mr. J. S. Shedlock . book , 
 * Beethoven Frauenkreis , ’ pathetic coincidence 
 published day death 

 October 9 , Dresden , Herr HEINRICH GUDEHUS , 
 eminent German tenor singer . début 
 Berlin Royal Opera House 1871 Spohr ‘ Jessonda . ’ 
 Wagner heard 1881 , engaged 
 Bayreuth , sang production ‘ Parsifai ’ 
 1883 , ‘ Tristan ’ years later 

 , 1909 . 737 

 Dr. kichter ‘ Leonora . 3 ’ Beethoven , 
 surpassed kind heard recent years 

 Handel ‘ Judas Maccabzeus ’ formed programme 
 Thursday morning . performance triumph 
 choir , good characterization efforts 
 soloists -Madame Gleeson- White , Miss Agnes Nicholls , Miss 
 Ada Crossley , Mr. John Coates Mr. Kadford — inter- 
 pretation justified inclusion scheme Handel 
 warlike oratorio . second novelty heard Thursday 
 evening . consisted Mr. Granville 
 Bantock minutely descriptive setting Fitzgerald apt 
 translation ‘ Ruba’iyat Omar Khayydm . ’ new 
 section preceded second . Birmingham 
 professor fresh addition monumental composition 
 far best . glitter East cleverly 
 reproduced second parts wholly 
 original methods , warm glow . 
 eye dazzled appreciate . homo- 
 geneity grateful , 
 , shows composer strained manner . 
 infinitely superior manner , previous 
 sections undeniably clever , expression apt 
 little wanting suavity expects Eastern 
 subject ; suavity 
 wonderful humanity added carries inevitable 
 appeal . composer good . 
 utterances Pots , turn phases 
 Beloved , Poet , Philosopher marked 
 new feeling , final chorus 
 background , highest plane musical - dramatic 
 expression reached , turn dies away deeply 
 moving description end — ‘ Turn 
 glass . ” ‘ Omar ’ introduces Mr. 
 Bantock altogether new light , 
 represents real method expressing genius 
 exceptionally endowed composer . performance 
 excellent . composer conducted , solo parts 
 given effect Miss Phyllis Lett , Mr. John 
 Coates , Mr. Frederic Austin . Bach Suite , Strauss 
 ‘ Till Eulenspiegel , ’ solos Madame Donalda , 
 created excellent impression appearance 
 Birmingham audience , comprised remainder 
 concert 

 day morning programme arranged 
 included Cherubini Mass C ( . 4 ) , Brahms 
 ‘ Song Destiny , ’ Beethoven ‘ Eroica ’ Symphony . 
 Brahms Beethoven gems 
 collection ; choir sang increasing warmth tone 
 expressiveness , Dr. Richter surpassed 
 reading Beethoven . concert , Berlioz 
 ‘ Faust ’ set proved exceedingly popular . 
 performance maintained excellence festival . 
 Madame Donalda , sang memory , distinguished 
 charm vocalization ; Mr. Walter Hyde 
 gave tenor music ardour ; Mr. Henschel 
 sardonic years ; Mr. Robert Radford , 
 case festival , sang admirably . 
 choir fully maintained position , orchestra 
 notably good , general effect performance 
 bring memorable festival memorable end . 
 music sung week Cathedral referred 
 toon p. 721 

 PROMENADE CONCERTS 

 pianoforte work , ‘ Tableaux de voyage ’ ; 
 violin pianoforte sonata group songs marked 
 Opus 1 . artists Mlle . Selva ( pianist ) , Madame 
 Jeanne Lacoste ( vocalist ) , M. Firmin Touche ( violinist 

 series concerts given Orchestra 
 Mr. Landon Ronald direction opened October 7 . 
 novelty occasion ‘ Overture drama , ’ 
 Op . 45 , Georg Schumann , work testified 
 composer powers assimilation desire 
 present music kind experience shown to| Miss Eva Digby O'Neill Miss Maude Dixon gave 
 palatable . overture commended enjoyable recital Steinway Hall October 109 , 
 * healthiness . ’ works performed occasion } Miss O'Neill pleasing style gave point 
 Brahms second Symphony , Mr. Ronald Scena | recitations , especially Jean Ingelow ‘ High tide . ’ 
 * Adonais , ’ sung Madame Ella Russell , Sir Alexander | Miss Dixon displayed excellent technique 
 Mackenzie ‘ Scottish ’ Pianoforte concerto , solo portion | ex5ression - chosen selection pieces Beethoven , 
 interpreted Miss Irene Scharrer . The| Brahms , Schumann , Chopin Liszt . Special proof 
 orchestra played manner promised continuance | qualifications sympathetic accompanist displayed 
 season feats brilliance . Stanley Hawley music ‘ Bells , ’ 
 recited Mr. Charles Fry effect 
 warmly encored , reciter giving ‘ Riding 
 broom , ’ Mr. Hawley music 

 INTERESTING PIANOFORTE RECITALS 

 Herr Moriz Rosenthal gave recital season 
 hall October 12 , excited great Old music occupied prominent place programme 
 enthusiasm intellectual technical gifts . | Herr Fritz Kreisler recital Queen Hall October 9 . 
 interpretation Brahms difficult ‘ Variations theme | distinguished violinist introduced , , 
 Paganini ’ astonishing example legitimate important contribution British art Suite violin 
 virtuosity . pianoforte D minor Mr. York Bowen , proved 

 programme Signor Busoni recital Bechstein | worthy honour received . pianoforte 
 Hall October 16 consisted entirely artist | played bv composer . 
 transcriptions arrangements works Bach , Beethoven , 
 Mozart Liszt . , Bach Toccata Fugue 
 D minor ‘ Choralvorspiele ’ } pianist composer Mr. Felix Swinstead 
 perfectly transcribed reason Signor Busoni | musician considerable talents . attainments 
 intimate acquaintance Bach style , combined | capacities exhibited .Kolian Hall October 19 
 wonderful knowledge resources modern ] executant contributed interpretations works 
 pianoforte , qualities render works direction | Beethoven Chopin ; performer composer 
 examples transcriptions organ music | submitted ‘ Preludes ’ ‘ Fantasie . ’ 
 pianoforte . playing afternoon 
 distinguished greatest technical perfection 
 inexhaustible wealth tone - colour . Madame Ada Crossley seldom appears English concert 
 platforms oratorio , consequently 
 interest attached recital Bechstein Hall 
 ny ee October 19 . sang Handel ‘ Dove sei ? ’ number 

 Master Eddy Brown , violinist , | German , French English songs . large audience 
 precocious executants ready derive advantage | showed abundant pleasure . . 
 attraction extreme youth lends 
 performances . majority cases , youth 
 disappears compensating artistic qualities place 
 allurement public . present stage } tenth National Brass Band Festival , took place 
 rash prophesy Master Brown future , Crystal Palace September 255 attracted 140 
 playing Queen Hall October 19 showed lack | bands 50,000 visitors . chief sectional 
 promise interpretative , technically competitions Championship Great Britain 
 able good account Beethoven Concerto Colonies , prize National Crystal 
 Tartini * Trillo del Diavolo ’ sonata . Needless | Palace Challenge Trophy . gained Shaw , 
 audience delighted conducted Mr. W. Rimmer . Lieut . Charles Godfrey 

 Mr. James Ord Hume Mr. James Brier 
 ‘ netics adjudicators . proceedings terminated concert 
 ‘ ; massed bands 

 West Bristol Choral Society commenced rehearsals 
 Handel ‘ Samson . ’ second concert ‘ 
 song Miriam ’ ( Schubert ) , Max Bruch ‘ Fair Ellen , ’ 
 Elgar ‘ Black Knight ’ given 

 successful chamber concert Victoria 
 Rooms October 9 , players Miss Mary Lock 
 ( pianoforte ) , Miss Averil Woodward ( violin ) , Mr. 
 Herbert Walenn ( violoncello ) . Excellent performances 
 given Sonata F ( Op . 6 ) , Richard Strauss ; 
 Sonata G ( Op . 30 ) , Beethoven ; Popper suite 
 vocalist Miss Marion Gaskell , 

 Mr. Hubert Hunt 

 outside cost £ 6,000 

 played Brahms Violin concerto , D major , Op . 77 , 
 | * Fantasia Appassionata , ’ Vieuxtemps . Dr , 
 Cowen direction orchestra played Beethoven second 
 | Symphony , , novelties , Wagner ‘ Rule , Britannia ’ 
 | overture Tchaikovsky ‘ Battle Poltava , ’ 
 act opera ‘ Mazeppa . ’ expressive singing 
 chorus Mendelssohn ‘ hunter farewell ’ 
 grateful item diversified programme . 
 |the interregnum extensive repairs 
 roof hall , reconstructed 
 Especial care 
 taken altering interior ceiling , perfect 
 acoustic conditions beautiful concert - room 
 jeopardized 

 local musical Societies , conducted Mr. J. W. 
 Appleyard , resumed rehearsals ; Handel ‘ Jephtha ’ 
 selected Rock Ferry Choral Society , ‘ Elijah ’ 
 Claughton St. Cecilia , ‘ Messiah ’ 
 Waterloo Choral Society 

 Walton Philharmonic Society , new organization 
 northern suburbs , comprise orchestral 
 choral members , rehearsing Gade ‘ Zion ’ 
 Jensen ‘ Feast Adonis , ’ Mr. Albert Orton 
 direction 

 Symphony Orchestra concerts , conducted 
 Mr. Vasco Akeroyd , transferred locale 
 Sun Hall Philharmonic Hall , 
 new series concerts held October 19 . 
 Mr. Plunket Greene sang Stanford ‘ Sea Songs , ’ assisted 
 Birkenhead Glee Madrigal Society , fine 
 orchestra heard Beethoven eighth Symphony , 
 Chabriér Espana Rhapsody , items programme 
 ‘ popular ’ best sense 

 auspices Liverpool Manx Society 
 Douglas ( Isle Man ) Choral Society gave concert 
 Central Hall October 20 , choir , conducted 
 Mr. T. P. Fargher , heard advantage Brahms 
 * Song Destiny , ’ ‘ Storm ’ ( Kk . Rogers ) , ‘ Song 
 Pedlar ’ ( LU . Lee Williams 

 leading musical societies . 
 Gentlemen Concerts , addition afternoon 

 recitals , announce orchestral concertsand joint choral 
 | orchestral , conducted Mr. Henry J. Wood . 
 |the - orchestral items named prospectus , 
 | heard Manchester : Valse Badinage 
 ( CORRESPONDENT ) | ( Liadoff ; Rhapsodie Espana ( Chabrier ) ; Dream Children 
 Peas |(Elgar ) ; Baba - Yaga ( Liadoff ) ; Serenade Impressions 
 Mr. Donald Francis Tovey lectured University on|d’Italie ( Charpentier ) ; Love scene ‘ Feuersnot ’ 
 October 8 , ‘ Symphonic aspects Music , ’ | ( Strauss ) ; Adagietto strings harp ( Gustav Mahler ) ; 
 interest thoughtful scholarly address greatly | Introduction Act II . ‘ Konigskinder ’ ( Humperdinck ) ; 
 enhanced pianoforte playing illustration | Scherzo ‘ L’Apprenti Sorcier ’ ( Dukas ) . choral 
 Beethoven plan method , construction | concert January 17 , Rossini ‘ Stabat Mater ’ Haydn 
 * Waldstein ’ Sonata , G major Sonata , Op . 31 , and| ‘ Spring ’ performed . 
 eighth Symphony . took especial pains explain ] Hallé season opened October 21 , chief 
 meant term ‘ Classical music . ’ brief , | orchestral works Elgar Symphony , Strauss symphonic 
 lecturer described ‘ work stood test | poem ‘ Till Eulenspiegel , ’ Beethoven ‘ Leonora , ’ 
 time . ’ | . 3 , , appropriately , placed 
 interesting lecture given Dr. A. H. Mann , | programme . Sir Charles Santley soloist . 
 King College , Cambridge , October 9 , local | second concert , Dr. Richter conducted time 
 section I.S.M. subject ‘ Handel method | Manchester Bautock comedy overture ‘ pierrot 
 ‘ f preparing great compositions . ’ Illustrated series|the minute . ’ orchestral items Berlioz 
 - lantern slides , Dr. Mann method | ‘ Franc Juges ’ overture , Liszt . 3 Hungarian Rhapsody , 
 reproduced photographs Handel sketch - books , | Beethoven . 4 Symphony . Miss Agnes Nicholls 
 ‘ Messiah ’ score composer handwriting . | soloist . 
 seventy - season Philharmonic Society } important features coming season 
 brilliantly inaugurated opening concert October 12 , | note following works given 
 Mile . Alice Verlet vocalist M. Ysaye|time Manchester : Strauss ‘ Don Quixote ’ tone - poem 

 MUSIC LIVERPOOL 

 Sibelius Varsang , Max Reger Serenade G , Bossi | 
 Intermezzo Goldoniani , Alexander Ritter Symphonic 

 Poem ‘ Sursum Corda . ’ choral works include Elgar 
 ‘ Gerontius , ’ Berlioz ‘ Faust , ’ ‘ Messiah , ’ ‘ Elijah , ’ Liszt 
 ‘ St , Elisabeth ’ ( heard Manchester sixteen 
 seventeen years ago Sir Charles Hallé ) , 
 concert Cherubini Mass C minor , . 4 , Choral 
 Symphony Beethoven . evenings devoted 
 Wagner programmes 

 series Saturday evening Promenade 
 Smoking Concerts , conducted Mr. Simon Speelman , 
 commenced October 9 , Miss Lillian Risque 
 pianist Mr. Charles Tree vocalist . 
 works Bach , Wagner Mendelssohn heard 

 Ancoats Recreation Committee , conjunction | 
 Royal Manchester Institution , arranged series 

 Oxford University Extension Lectures , Mr. T. W. 
 Surette , Concord , Mass. , U.S.A. , started 
 October 4 course lectures song composers , 
 vocal musical illustrations . series deals Schubert , 
 Schumann , Robert Franz , Tchaikovsky , Grieg Brahms . 
 arranged Mr. Egon Petri 
 series pianoforte recitals , illustrative musical form 
 pianoforte composition time Beethoven 

 Mr. Gerald Cumberland arranged Thursday 
 evenings , Hallé concert , short lecture 
 orchestral works performed evening 
 Dr. Richter . gladly welcomes endeavours 
 assist earnest musical student extract maximum 
 enjoyment pleasure music 
 heard course winter season 

 Leeds Municipal | 
 | Society honorary conductor , Mr. Fred Wintersgill , 

 afford spend music , continued 
 | ‘ self - supporting concern , ’ condition Art 
 | Gallery libraries find difficult exist . 
 prices necessarily raised , serial 
 ticket concerts purchased half - - crown 
 upwards , hardly styled exorbitant . 
 Mr. Fricker direction long list popular classics , 
 including symphonies Haydn , Mozart , Beethoven , 
 Schubert , Schumann Tchaikovsky , promised , 
 centenaries Haydn , Chopin Schumann 
 suitably recognized 

 Bradford Subscription Concerts forward series 
 seven excellent programmes , Dr. Richter 
 Hallé Orchestra appear . Beethoven Choral 
 Symphony , Bach motet , Beethoven E flat Pianoforte 
 concerto ( soloist , Signor Busoni ) , ‘ Tod und Verklarung ’ 
 Strauss , Wagner concert chief things 
 promised . Bradford Permanent Orchestra intend 
 giving concerts , music Beethoven 
 Wagner , Russian ‘ centenary ’ programme 
 special features . Mr. Allen Gill Society conductor , 
 occasion Mr. Landon Ronald appear 
 capacity . Bradford Old Choral Society promises 
 performances Elgar ‘ King Olaf , ’ Bach ‘ Christmas 
 Oratorio , ’ Handel ‘ Samson , ’ Bradford 
 Festival Choral Society Parker ‘ Hora Novissima , ’ 
 Goring Thomas ‘ Swan Skylark , ’ ‘ Messiah . ’ 
 works chosen powerful Huddersfield Choral 
 Society , Dr. Coward conducts , include ‘ Parry 
 * St. Cecilia Day , ’ Walford Davies ‘ Everyman , ’ 
 Coleridge - Taylor ‘ Hiawatha . ’ venerable 
 active Halifax Society gives Mendelssohn ‘ St. Paul , ’ 
 second portions Coleridge - Taylor ‘ Hiawatha , ’ 
 Handel ‘ Acis Galatea . ’ Mr. English 
 conductor 

 Middlesbrough Musical Union , , situated 
 sort ‘ man land , ’ close borders Durham 
 Yorkshire people apt forget belongs 
 county , , usual , prepared thoroughly artistic 
 programme , - eighth season . curious 
 , purest coincidence , particular work 
 enjoy special popularity particular season , 
 surprising find important York- 
 shire Society turning Mr. Coleridge - Taylor ‘ Hiawatha , ’ 
 portion given Middlesbrough , 
 ‘ Winter ’ section Haydn ‘ Seasons . ’ 
 Berlioz ‘ Faust , ’ concert Parisian String 
 Quartet play chamber music César Franck 
 Debussy , complete programmes concerts , 
 enthusiastic musician , Mr. Kilburn , act 
 conductor 

 BERLIN 

 Philharmonic Concert season , Elgar 
 Symphony received initial performance Berlin , Herr 
 Arthur Nikisch securing masterly performance 
 famous work.——The Symphony concert ‘ Kgl . 
 Kapelle , ’ given Royal Opera House baton 
 Richard Strauss , took place October 5 , Haydn D major , 
 Mozart G minor , Beethoven C minor Symphonies 
 constituting programme . — — young English violinist , 
 Miss Harrison , gave successful concert 
 orchestra , performances Brahms Glazounoff 
 Violin concertos calling forth great praise critics 
 Berlin newspapers.——On September 24 5ooth 
 performance Offenbach opera ‘ Hoffmanns Erzahlungen ’ 
 took place Komische Oper.——Smetana opera 
 ‘ Dalibor ’ novelty season repertoire 
 Royal Opera House.——Max Reger new String 
 quartet played time Berlin October 2 , 
 Lange Quartet Frankfurt 

 BIARRITZ 

 LEIPsICc 

 Gewandhaus concerts , Professor Nikisch 
 direction , opened season Wagner ‘ Faust ’ overture 
 Beethoven fourth Symphony . solo vocalist , Miss 
 Edyth Walker , sang interesting songs orchestral 
 accompaniment , ‘ Verfiihrung ’ ‘ Gesang der Apollo 
 priesterin , ’ Richard Strauss 

 MOSCOW 

 New Symphony Orchestra.—The season consist 
 symphony concerts — | afternoon ( 
 commenced October 7 ) , evening , 
 place October 28 . Symphonies Brahms , | 
 . 2 , D ( October 7 ) ; Georg Schumann , . 1 , per- | 
 formance England ( October 28 ) ; Stanford , . 6 , E flat , 
 Tchaikovsky , . 4 , F minor , César Franck D minor , 
 Symphony March 17 — concert —— 
 announced . items special yy new } 
 symphonic poem , ‘ maid Astolat , ’ J. D. Davis ; 
 Orchestral Variations Old King Cole — new version ( | 
 performance ) . Nicholas Gatty , Rhapsody 
 orchestra , ‘ Africa ’ ( performance ) , Coleridge- 
 Taylor . Mr. Landon Ronald conductor 

 Classical Concert Society.—Ten concerts chamber music 
 announced — afternoon evening . 
 began Bechstein Hall October 13 , conclude 
 December 15 . String quartets ty Beethoven , Haydn , 
 ve Schubert ; ( uintets Beethoven Brahms ; Trios | 
 Beethoven Mozart included ; Brahms Horn | 
 trio E flat major , Dvorak Pianoforte quartet E flat | 
 major , Schubert Octet strings wind F major , 
 Schumann Pianoforte trio F major , Beethoven 
 Pianoforte trios C minor D major , Brahms 
 l’ianoforte trios B major C minor 

 
 concerts given ( Queen Hall eaanbes 10 

 

 Bacn , J. 
 Higgs Edition , 
 BEETHOVEN . — Andante a. ariati 10ons , 
 Septuor . W. T. Best Arrangement 
 Harwoop , Basit.—Sonata . 1 , C sharp minor r ( Op . 5 

 . 22 , 

