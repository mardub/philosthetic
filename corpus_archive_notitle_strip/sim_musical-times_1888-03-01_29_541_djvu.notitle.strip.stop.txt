LIST WORKS PERFORMED 

 CATHEDRAL , 
 | WepNEsDAY MorRNING , 11.30.—ELIJAH ( Mendelssohn ) . 
 THURSDAY MorninG , 11.30 . 
 MOTETTS . Written expressly Festival 
 Mr. OLIVER KING . wiles 
 a. waters Babylon } ) , ; ‘ 
 6 . O sing unto Lord y Soli Chorus . 
 SYMPHONY C MINOR ( Beethoven ) . 
 REQUIEM ( Verdi ) . 
 Fripay MorninG , 11.30 . 
 SYMPHONY B MINOR ( Schubert ) . 
 ENGEDI ( Beethoven ) . 
 LOBGESANG ( Mendelssohn ) . 
 Fripay Eventnc.—THE REDEMPTION ( Gounod 

 MUSIC HALL . 
 Wepnespay Eveninc.—THE GOLDEN LEGEND ( Sullivan ) . 
 Tuurspay Eventnc.—MISCELLANEOUS CONCERT 

 MRS . BARTER ( Soprano ) . 
 Oratorios , Ballad Concerts , & c. , address , Westbury Road , Wood 
 Green , N. ; M Messrs. Novello , Ewer Co. , 1 , Berners Street , Ww 

 MISS MARI BATES ( Soprano ) . 
 Concerts , Oratorios , & c. , address , Mr. Brook Sampson , Mus . Bac . , 
 Beethoven House , Northampton 

 MADAME CARRIE BLACKWELL ( Soprano ) 
 ( Pupil late Madame Sainton - Dolby ) . 
 Orchestral , Oratorio , Ballad Concerts , & c. , 4A , Sloane Square , S.W 

 MISS FANNIE SELLERS ( Soprano ) . 
 Oratorios , Classical Ballad Concerts , Crag Cottage , Knaresbro ’ , 
 MADAME CLARA WEST ( Soprano 

 MISS LOTTIE WEST ( Contralto ) , 
 Beethoven Villa , King Edward Road , Hackney 

 MISS LOUISA BOWMONT ( Principal Contralto ) 
 ( St. Peter Church , Manchester 

 MISS CONSTANCE POOCK ( Contralto & Pianist ) 
 ( R.A.M. Honours Singing Certificate 

 Oratorios , Ballads , ‘ ‘ Beethoven ” ’ ( Verbal , Vocal , Pianoforte 
 Lecture ) , Recital Classical Popular Vocal Pianoforte 
 Selections , & c. , 8 , Buckingham Palace Road , S.W 

 Miss Poock sang songs , ‘ come ’ ( Sullivan ) ‘ 
 Castle Gate ’ ( Gade ) , good style ; rapturously applauded 
 encored . ”—Vide Press.—St . Andrew Hall , Norwich , Feb. 11 , 1888 

 WAGNER LISZT LETTERS 
 

 Tue recent publication , Messrs. Breitkopf 
 Hartel , goodly volumes , containing inter- 
 change long series letters * * passed 
 Wagner Liszt 1841 - 61 , 
 welcome unexpected , 
 seeing heralded preliminary 
 blowing trumpets , justifiably 
 undertaking exceptional 
 interest musical world present day . 
 Wagner Liszt ! — , derived pleasure 
 listening musical works 
 great giants fost - Beethovenian 
 period , plodded 
 voluminous literary writings 
 written , burn know 
 

 , reading letters eminent 
 man , longed know 
 reply ! seldom wish 
 gratified ! recalls letters Goethe 
 Schiller notable exception ; , happily , 
 , 
 equal interest . letters Wagner 
 Liszt arranged chronological order , 
 great extent comes reply , 
 , reading , certain sense continuity 
 maintained 

 autobiographical sketch earlier 
 career ( 1842 ) , Wagner drawn 
 mournful picture stay Paris , , 
 hoping hope production 
 ‘ Rienzi , ’ order body soul 
 , driven undertake hack- 
 work writing articles Schlesinger Gazette 
 Musicale making arrangements popular 
 operas day pianoforte instru- 
 ments , including cornet - - pistons . picture 
 drawn earlier exile 
 Ziirich perusal letters pales 
 . Fleeing Dresden , left sick 
 wife , soon found abso- 
 lutely penniless condition , immediate 
 opportunity obtaining hack - work 
 managed subsist Paris . 
 turn help Liszt ? — 
 Liszt , befriended , 
 ungrudgingly supplied neces- 
 saries life far able 
 pocket , enlisted sympathies 
 behalf 

 lack evidence Liszt noble- 
 mindedness generosity sketches life 
 time time forth . 
 looks Beethoven Festival Bonn , 
 1845 , prime mover , 
 - fifth cost monu- 
 ment erected memory great 
 master , responsible 
 expense inaugurative musical festival 
 accompanied unveiling statue , 
 including cost erection building 
 musical performances held . 
 heard repeatedly munificent gifts country- 
 men suffered inundation 
 conflagration ; heard liberal dona- 
 tions poor places given 
 Concerts , unbounded kindness pupils , 
 fee . 
 comparison 
 Richard Wagner . recognise Wagner 

 mn ieee 3 Ok ae ee ee fk ee ee ee , ee mes cn 

 , flowing orchestra 
 exquisitely beautiful effect , brought conclusion 
 impressively chords ‘ pizzicato ’ 
 double bass , ” ‘ ‘ fancy picture rustic 
 dance adjuncts soft scenery , brilliant 
 moon , Italian sky , different measures 
 trip section section orchestra , ” 
 ‘ ‘ jubilant symphonies , ” feel 
 quiet quarters raise musical criticism 
 ranks finer arts . genius 
 declared certain singer successful * 
 vocalistic point view , ” clever creature 
 praised performance Rossini Overture 
 * remedy , ” wait long 
 reward 

 conclude course 
 Coutts - Lindsay Hallé - Carr controversy 
 art painting degraded association 
 room art music ? Messrs. Hallé Carr 
 — Mr. Hallé better judgment 
 expected — appear entertained 
 notion Grosvenor Gallery desecrated 
 concerts , evening parties 
 vicinity refreshment rooms . hand , 
 Sir Coutts - Lindsay different 
 opinion . , , opinion involved . 
 men agree procedure , 
 flagrantly improper , trouble think . 
 , , Sir Coutts let gallery 
 concert - givers ashamed . ‘ revolt 
 Messrs. Hallé Carr caused turn 
 matter mind . fancy 
 cogitating long deeply question 
 execution Beethoven Rasoumowsky 
 quartets insults Mr. Burne Jones pictures 
 angular distressful women . issue 
 matter , proving — Grosvenor 
 Gallery closed concerts — Sir 
 Coutts constrained agree 
 lieutenants pecuniary interests . 
 degradation painting music 
 come ? Musicians disposed think 

 si 

 public . Concert Miss Fanny Davies played 
 selection Schumann charming * * Davidsbiindler , ” ’ 

 somewhat ill - advised entire work , 
 hardly bears mutilation . concerted works Mozart 
 Quintet C Beethoven Trio G ( Op . 1 , . 2 ) , 
 Mrs. Hutchinson sang agreeably airs Handel , 
 Haydn , Bennett 

 masterpieces rank included 
 programme following Monday — , Beethoven ’ 
 Sonata , pianoforte d violoncello ( Op . 69 ) , 
 Mendelssohn Quintet B flat ( Op . 87 ) . shall 
 venture opinion greater work 
 , peculiar association 
 entertainments , opening item 
 Monday Popular Concert , - years ago . 
 performance magniticent , especially Adagio « 
 Lento , Mendelssohn finest slow movement . Miss Fanny 
 Davies piano solos given reverse 
 order . 3ach great Fugue minor , Spitta 
 characterises finest , longest 
 master contrapuntal exercises , gave effect anti- 
 climax Brahms Rhapsody G minor , piece 
 composer best manner . Miss Liza Lehmann 

 

 alteration moment 
 programme Monday Popular 
 Concert , exception occurred 6th ult . 
 work list Brahms Quartet C minor ( Op . | 
 51 , . 2 ) , owing slight injury right hand | 
 Madame Nérud : t unable rehearse , Haydn | 
 far easier Quartet E flat ( Op . 71 , No.3 ) substituted . | 
 possible audience | 
 change unwelcome , 
 reg sretted cause , Brahms work exhibit | 
 genial mood . earnest conscientious | 
 English pianist , Madame Frickenhaus , | 
 appearance season essayed Schumann Sonata | 
 G minor ( Op . 22 ) . — proved , nes | 
 unfortunate , work Single iy executant mn 
 advantage . rendering technically acc wate | 
 lacked true expression poetic music there- 
 fore create effect . Madame Irickenhaus heard 
 greater advantage works romantic character . 
 Mdlle . Gambogi , voice trained , 
 favourably received airs Handel Godard 

 Beethoven Septet , popular works 
 entire repertory , closed Concert 

 Mr. / - Chappell congratulated zeal 
 fidelity « Saturday patrons . ‘ weather 
 11th ult . ungenial description , hall 
 vas nearly usual . remarkable 
 feature Concert executants 
 sterner sex , somewhat rare 
 occurrence . leader Herr Heermann , 
 extremely favourable impression year ago 
 strengthened present occasion . aremarkably 
 able player , tone rich execution 
 true intonation warminexpression . qualities 
 exhibited alike Mozart Quartet G , . 1 , 
 principal solo , Ernst ‘ * Hommage Molique . ” 
 Mr. Max Pauer appearance season 
 showed progress making artist 
 excellent performance Chopin Sonata B minor 
 ( Op . 58 ) . work itselfis particularly interesting , 
 Mr. Pace brought good points mastered 
 difficulties apparent ease . Rubinstein Trio B flat 
 ( Op . 52 ) exhibited new - comers advantage , 
 work brilliant effects regarded 
 Russian composer satisfactory efforts . 
 Mr. Santley vocal selections remark 

 rio D minor ( Op . ¢ 3 

 space describe Beethoven Quintet C ( Op . 29 

 Mendelssohn Trio C minor ( Op . 66 ) per- 
 formed , Herr Joachim played Leclair somewhat 
 hackneyed Sarabande Tambourin Sonata 
 ID ( . 3 ) . true , programme stated , 
 greater old F ° ord composer music long 
 date , excerpts 
 introduced , sake v 
 agreeable note improvement young artis 
 congratulate Miss Mathilde Wurm 
 advance pianist appeared 
 St. James H rendering Schumann 
 Papillons ( Op . 2 ) distinguished charming lightness 
 delicacy , keeping piquant fanciful 
 nature music . Miss Marguerite Hall 
 voc : lis ts weary audience t ! nread - bare 
 songs . occasion sang gre : Ut purity pr 
 Brahe ms clever pleasing “ ‘ Geistliches V 
 founded old Catholic Hymn . viola o 
 played Mr. Hollander 

 hoping greater things 

 future . 
 fault found programme 
 modest . items 
 true important works 
 Beethoven ‘ * Waldstein ” ’ ? Sonata Schumann 
 Etudes Symphoniques . - named te 

 Miss Davies scarcely best . ! Y 1ce 
 good points , times player litt 
 flurried , sublime Adagio taken de 

 Master acquainted score ; | Zeitung , Kolnischer Zeitung , f 
 shall understand accuse imitation | Blatt , Allgemeine Musik - Zeitung . ‘ journals 
 | J 

 regard certain similarity conception | practically unanimous praise work , 
 Barber Beckmesser hand , |noteworthy describe important 
 castigation scene . service art | ( bedeutende ) . need opinions detail , 
 feel duty express thanks theatre | cover ¥ d travelled 
 managers performance work . ” English press observations Symphony , 
 manner , comme ng happy 
 choice national themes , composer sound workman- 
 DR . STANFORD 6c IRISH ” SYMPHONY ship , interest nd ch 1 ays 
 ae ae movement . * Irish , ” , hardly 
 GERMANY , trial fairness German critics , 
 rew months ago occasion , un- | given reason think hold scales 
 questionable facts quotations , unworthy treatment | justice upright . congratulate 
 Sir Arthur Sullivan “ Golden Legend ” received | handsome behaviour Dr. Stanforca , doin : 
 press Berlin . reference matter | nature t 
 purpose - opening old wound , influenced Dr. von Bilov 
 opportunity saying acerbity admiration work . , co 
 German critics intensified considerations | English music important success . 
 merits demerits Sullivan music | think present state art ‘ Father- 
 . sake contrast | land , ” agree extremely desirable obtain 
 recent past , throwing stronger relief |German approval productions composers . 
 courtesy , consideration , frank approval dificrent case lrench music 
 extended representative insular } Germany . ‘ productions Gallic composers 
 art . cordially resented thought an| welcomed relief change bring , 
 undeserved slight ; let cordially recognise applaud | products entirely different school conti sted 
 - bestowed honour . national temperament . Englishman descen- 
 Professor Villiers Stanford took “ Irish ” Sym- | dant , speaking generally , migrated German . 
 phony Germany day , a| family relationship obvious , English music , 
 stranger strange land . Professor persona | especially affected intimate artistic connection 
 grata country great masters . works | Teutonic cousins , 1s German music , speak , 
 sympathises peculiarly German develop- | * removed . ” ‘ necessary consequences 
 ments art ; friendly terms leaders | obvious . English music Germany comes 
 musical faith practice , asa composer rival German music ground , naturally 
 unknown , especially Hamburg , years ago subjected usual scrutiny , succeeded 
 * * Savonarola ” played . proceeding lay engaging attention . Allowing 
 Teutonic connoisseurs ripest fruit talent , | operation prejudice , man entirely 
 , , desolate feeling free , , , test advance national 
 knows greatly dis- | art attitude German amateurs . Judged 
 posed care . Professor Stanford congratulated | standard , certainly { 
 fact men like Hans von Biilow , | encouragement satisfaction . agai 
 sort , guarantors worth countrymen . | British music performed hearing 
 Better favour Courts princes . | Beethoven countrymen , measure 
 Alman de Gotha popular composer , | success , circumstances , regard 
 position depends suffrages republic thoroughly deserved — events , 
 whereof men cultivated art letters free | product prepossession . 
 independent citizens . remarks close acknowledgment 
 lucky representative British music fortunate | cordial sympathy shown Dr. von Bilow , 
 respect . work- | performance * Irish ” Symphony Berlin 
 manship “ Irish ” ? Symphony , |and Hamburg . Doctor , informed , 
 important feature case . unimportant , cer- | actually taken trouble work heart — 
 rate , conducted performance book , 
 * criticism verbatim appeared German paper , | ways showed keen interest success . 
 think pity writer attempts cry Master | Courtesies kind , representative musicians 
 depreciation , especially thing ] ~~ y-,5 _ . eS eae 
 Cornelius Wagner wished , firm friends , | 2 ditterent nations 
 capable admiring genius , sympathy selida 

 lost . ‘ tend mutual 
 ité prevail best 

 creditably given . Herr Sick confirmed favourable 
 impression produced violin playing Guild 
 Concert nights previously , admirable perform- 
 ance Max Bruch somewhat ponderous Romance 
 A. singing Miss Edwards ( contralto ) Mr. 
 F. Cox ( bass ) considerably amateur level 
 style : vocal quality . Mr. J. W. Cooke 
 usually onerous task Conductor amateur 
 band , came ordeal creditably 

 second Madame Agnes Miller ’ s Chamber Concerts , 
 16th ult . , drew large high - class audience , 
 attendance parts hall larger 
 occasion . previous Concerts , 
 executants ladies , includ g , béncficiaire , 
 undertook responsible duties pis inist , Miss Emily 
 Shinner Miss Lucy Riley ( violins ) , Miss Cecilia Gates 
 ( viola ) , Miss Florence Hemming ( violoncello ) . 
 programme consisted Schubert fine String Quartet 
 minor ( Op . 22 ) , Brahms Pianoforte String Quintet 
 F minor ( Op . 34 ) , Beethoven Pianoforte Fantasia 
 G minor ( Op . 77 ) , Boccherini Vicloncello Sonata 
 , Study Improm Chopin , little 
 Duets violins , BL . Gedard , French com- 
 poser engaged score * Ruy Blas . ” 
 Madame Miller play @ remarkable exhibition 
 power technique , alike Brahms Quintet 
 Beethoven Fantasia ; displayed good 
 deal grace fancy Chopin pieces , 
 obviously fitted style acquirements . Miss 
 Florence Hemming gave charming rendering 
 violoncello Boccherini popular Sonata , Miss 
 Shinner , Miss Riley , Miss Gates confirmed favour- 
 able impressions produced previous performance 
 

 Mayor reception , r4th ult . , 1,300 
 local élite entertained Council House , 
 pleasing feature entertainment Concert 
 given Council Chamber , Mr. Troman direc- 
 | tion , principal performers Madame Florence 
 Winn , Miss Ethel Winn , Mr. Sackville West , vocalists ; 
 Miss Donaldson , violinist 

 Mr 

 Norman - Néruda , Madame yrdic 
 Concert opened Spon- 
 followed Beethoven ’ S ore 

 Hallé , Madame 
 Mr. Watkin Mills 
 tini Overture ‘ Vestale 

 evening Saturday Night People 
 Concert took place Syrod Hall . Miss Romola 
 Tynte varied music recitations , 
 received . hall crowded , hundreds 
 unable gain admission 

 Edinbur gh Amateur Orchestral Society , 
 endian Mr. Carl Hamilton , gave second Concert 
 season evening 7th ult . Beethoven 
 * * Leonora , ” Concerto Mozart , Symphony laydn 

 Overtures Reissiger Suppé , ‘ * Spring Melody , ” 
 irieg , ‘ Wedding March » 9y Soderman , songs , 
 Grieg , ‘ ‘ Wedding March , ’ Sod 1 g 

 Quintet E flat played . Neérud 

 Mr. Hallé gave Bach Sonata violin 
 Madame Néruda solos Barcarole d Scherzo 
 Spohr , Mr. Hallé Mendelssohn Caprice Brillant a1 d 
 Prelude Fugue E minor . Beethoven Grand Septuor 
 completed Concert , fitting close musical treat , 
 value , esthetic 

 overrated . tl 
 Edinb 
 ; Prof 

 MUSIC GLASGOW WEST 

 SCOTLAND . 
 ( CORRESPONDENT . ) 
 present season Choral Orchestral Concerts , 
 management Glasgow Choral Union , 
 qth ult . chief instrumental 
 works performed date Concert 
 referred issue Schumann 
 * * Genoveva ’ ” ’ Overture , Serenade Ishmaelites 
 ‘ Childhood Christ , ” Berlioz ; Haydn 
 . 10 Symphony , Beethoven C minor Symphony , 
 Thursday evening , 2nd 

 

 MUSICAL TIMES.—Marcu 1 , 1888 

 parts , augmented orchestra , strength 
 chorus . direction Mr. Joseph Bradley , 
 important work presented satisfactory 
 manner . Mr. Bradley evidently showed _ intimate 
 acquaintance score , complete command 
 chorus orchestra . Concert season 
 orchestral works receiving highest 
 number votes plebiscites taken previous 
 meetings . result voting time hardly 
 improvement exercise ‘ Suffrage universel ” ’ 
 years . princips al favourite selections 
 Ov erture ‘ William Tell , ’’ Beethoven 
 Pastoral Symphony , Overture ‘ Tannhauser , ” 
 heretofore , minor varieties included 
 exercise discretion programme com- 
 mittee . compliment composer , 
 acknowledgment genuine merits music , Mr. 
 Hamish MacCunn Concert - Overture “ Land 
 Mountain Flood ” included programme 

 performance ‘ Faust , ” night 
 4th ult . , members Union held annual Con 

 scriptior 1s , large measure | 
 support ‘ non - subs scribing public 

 Choral Union , fortnight rest , taken 
 Beethoven “ Engedi ” ( Mount Olives ” ) , pending | 
 future arrangements , music | 
 sing opening , course , Glasgow | 
 International Exhibition 

 performance Mr. T. Mee Pattison setting | 
 Coleridge * ‘ Ancient Mariner ” given choir | 
 Ibrox United Presbyterian Church , evening 
 oth ult . annual competition gold medal 
 awarded Directors Saturday Evening City | 
 Hall Concerts best singing took place r1th | 
 ult . singing competitors high order . 
 Dr. Hiles Cantata ‘ Crusaders ” included 
 programme Concert given 16th ult . Choir | 
 Kilmalcolm Established Church , performances 
 direction Mr. J. C. Fyfe , Organist | 
 Church , sail California . Motherwell , | 
 Lanarkshire , performance given Handel 
 ‘ * Messiah ” Choral Union industrious locality . 
 Mr. John Marshall conducted . Wishaw orchestra 
 played accompaniments . Mendelssohn * Elijah ” 
 performed gth ult . Helensburgh Choral Union , 
 creditable manner , Mr. T. Brash , Conductor . 
 accompaniments played Mr. Cole orchestra 

 undiluted , chief item Mr. Hallé rendering of| striking definite consistent path 
 ’ 5 o x 
 | having clearness purpose 

 Beethoven G major Pianoforte Concerto , , 
 direction , Berlioz weird — ghastly — Symphonie 
 Fantastique C 

 Philharmonic Society Concert , 7th ult . , 
 Miss l'anny Davies played , artistic manner , 
 Beethoven Concerto . Symphony Mozart 
 C major , work given greatest prominence 
 C. H. Lloyd Cantata ‘ Hero Leander . ” 
 principals , Mrs. Hutchinson Mr. Bantock 
 Pierpoint , upheld reputation , chorus 
 carefully 

 seventh Concert , 
 ult . , Mr. Hallé gave Concerto Beethov 
 - known ‘ “ ‘ Emperor’—his reading characterised 
 thorough ceeeaiaationnas unerring precision . Mr. 
 Halle pianoforte contribution “ Liszt “ Rhap- 
 sodie Hongroise ” E ( No.1 ) . real novelty 
 performance Dvorak Symphony D minor , 
 work pene composer estab- 
 lished style originalit contains themes 
 high merit , . theoretical construction 
 workmanship artist pre - eminent . 
 orchestral items , included dainty Mozart 
 Romanza strings appreciated previous 
 Concert , band justified reputation . Mr. 
 Santley vocalist , giving examples Wagner 
 Gounod 

 Concer 

 ies Beethoven great 

 s awn version 

 Hallé band . works selected ‘ ‘ Chevy 
 Chase ” Overture , Sir George Macfarren , 
 displayed characteristic learning com- 
 poser , genial attributes , 
 feeling , picturesqueness , delightful flow melody 

 feature evening , , Beethoven 
 Pastoral Symphony , , familiar musicians 

 4s warmth 

 Mr. Charles Ha vorman - N 
 assistance Miss Clara Leighton vocalist , re- 
 sponsible carrying Subscription Concert , 
 tenth es , given Huddersield Town 

 Beethoven Sonatas 

 Hall , rst ult . 
 Pastoral “ Kreutzer , ” 
 
 minor , Chop 
 axtemps , Spohr , 
 hton cc mtrib utions , vigorously appla ed , 
 wlaed songs Coenen Spoh r , selection 
 * * Oberon . ” Mr. J. E. Ibeson accomps 
 M. de Pachmann following Concert , 
 displayed perfect command 
 forte h delighted audience 

 ARCH , 1888 

 sixth Concert , given 17th u ult . , , 
 hand , special interest , hall 
 usualiy filled . Concert opened Beethoven 

 Festival ” Overture ( found 
 work comp nosed opening 
 Josephstadt Theatre Vienna , including music chorus 
 solo voices shortly pr oduced Bach 
 Society ) , followed Mendelssohn unaccom- 
 panied Psalm “ Judge , O God , ” ‘ Reformation ” 
 Symphony , Verdi Requie ar 
 exceedingly effect . performance refined 
 careful , magnificent double chorus Sanctus , 
 “ Libera ” particularly impressive . Miss 
 Annie Marriott , Madame de Fonblanque , Mr. Lloyd , 
 Mr. Gilbert Campbell pr cips . Mendelssohn 
 Psalm rendered equal skill chorus , 
 Festival Choral Society , leader- 
 ship Dr. ae = ge , Conductor . orchestral 
 performances Hallé band characterised 
 usual effi yy . Mr 

 idea getting 
 talian translation work offer r 
 Rio de Fanetvo ( likely shortly pro duce 
 ‘ Tannhauser ’ ) performance Italia , 1 
 italics 

 following list projected operati & 
 concert performances connection wit International 
 Exhibition held Bologna — k * * Alceste , ” ’ 
 Sacchini * Cidipus , ” ” Cimarosa nonio segreto , ” ’ 
 Opera Piccini , Intermezzo Pe rtures 
 Symphonies Lulli , Scarlatti , Bach , Haydn , Mozart , 
 Boccherini , Beethoven , Mendcl humann , 
 Berlioz . probable produ Wagner 
 ‘ Tristan und Isolde ’ 

 sacred works named Mendelss 
 W agner * Das Liebesmahl der Ap « 
 tative * international ” programme 

 moni 

 contril d “ Chit - c Beethoven Sonata ( . 1 ) 
 — -Gilbert Sul 1s ic operetta Trial Fury pre- 
 sented Lane Head Schools , ‘ 11th ult 

 RLISLE.—On Friday , 1o 
 fourth grand Concert 

 Eritu.—A Concert given Public Hall , 13th ult . 
 Mr. W. Sanderson , Organist Parish Church . principal 
 soloists Mr. James Bayne , Mr. Anderson , Miss Nellie 
 Roberts . orchestral music reflected great credit Conductor 
 performers 

 ESHER , SurRREY.—Mr . J. E. Adkins , Organist Parish Church , 
 commenced series Organ Recitals Mc nday , 6th ult . , 
 arranged programmes view showing con- 
 trasting schools composers . Recital consisted 
 examples French , Belgian , German masters . Guilmant , 
 Batiste , Wély , Salomé drawn - named ; 
 Lemmens Belgian ; Bach , Mendelssohn , Rheinberger , 
 Handel Germany . Mr. Wilfrid Jones gave vocal illustra- 
 tions : Gounod ‘ green hill ” Handel ‘ “ saith 
 Lord . ” 13th ult . English German music pre- 
 sented , examples Dr. Roberts , Dr. Stainer , Dr. Bridge , 
 Sterndale Bennett , H. Smart ; Handel , Merkel , Schumann , 
 Mendelssohn , Beethoven , respectively . Mr. Kilby voca- 
 list occasion , gave Sullivan “ ‘ Chorister “ Sound 
 alarm ” ( Handel ) . night large assemblies , H.R.H 
 Duchess Albany number , evidently taking 
 great interest music given organ erected memorial 
 toherlate husband . Recitals continued 11th inst 

 Frome.—A enjoyable Concert given Mechanics ’ 
 Hall , 7th ult . , band chorus numbering upwards 
 eit ghty performe rs , leadership Mr. T. Grant , forthe benefit 
 library fund Mechanics ’ Institution . 
 programme consisted selections Handel Oratorio Samson . 
 soloists Miss Bradbury , Miss B. Grant , Mrs. Rogers , Mr. H. 
 J. Morgan , Mr. J. Lewis , Mr. : . Deggan , Mr. F , C. Tucker , 
 second opened ’ Overture Auber Masanz 
 Mr. Mrs. H. Welham , Mr. Miller , Mr. G. Bourke ( contra - bass ) , 
 Herr Schottler ( violin ) sang played solos . Miss Deggan 
 accompanist 

 GRANTHAM.—On oth ult . Amateur Vocal Union gave 
 Beethoven Engedi Smart Bride Dunkerron , principals 
 Miss Adelaide Mullen , Mr. Henry Beaumont , Mr. J. B 
 Smith , Conductor , Mr. Dickenson 

 GRAVESEND.—The - sixth Concert Gravesend 
 Milton Choral Association given Tuesday evening , January 31 . 
 artists Miss Hope Glenn , Miss Alice Gomes , Mr. 
 Charlies Chilley vocalists , Mr. J. T. Carrodus , solo violin . Mr. 
 Carrcdus played pieces Ernst , Chopin , Paganini , selection 
 exquisitely rendered . response encores Mr. 
 Carrodus played extra pieces . singing Mr. Chilley 
 deserves especial mention . good - songs rendered 
 highly creditable manner members , baton 
 Conductor , Mr. Charles R. Green . Mr. Howard Moss , suc- 
 cessfully contributing pianoforte solo , ably accompanied 
 performers 

 ory , Mus . Bac . , Mr. McCall . Rev. E. M. Weigall 
 o ted , Rev. W. Insull accompanied pianoforte . 
 performance successful 

 SHERBORNE ABBEY.—Mr . G. E. Lyle , Organist , & c. , Abbey , 
 gave Organ Recital Saturday afternoon , rth ult . 
 programme selected wiitings Batiste , Mendelssohn , 
 Rink , Léfébure - Wély , Bach , W. Spark , C. Vincent , E. M. Lott , 
 Handel , Beethoven , Costa.—Mr . Lyle repeated programme 
 Tuesday evening , 14th ult . , benefit business people 

 S11co.—The Musical Society gave performance Gade 
 Crisaders 14th ult . , direction Mr , A. T. Froggatt . 
 Rinaldo taken Mr. Drummond Hamilton , Christ 
 Church Cathedral , Dublin ( unavoidable absence Mr. Bapty ) . 
 Cantata followed miscellaneous selection , including 
 Hatton “ Indian maid " Dr. Mackenzie “ merry dwarfs . ” 
 Mr. Delaney , Organist Roman Catholic Cathedral , played 
 Moscheles ’ “ Sonate Mélancolique ” great delicacy expression 

 192 MUSICAL TIMES.—Marcu 1 , 1888 

 NOVELLO , EWER CO . NOVELLO , EWER CO . 
 PIANOFORTE VOCAL ALBUMS 
 B ° 
 4 
 ALBU MS ue Pg 
 . Gilt . 
 ; BEETHOVEN . es 
 = 4 2 LD TOURS . s. d , s. d , 
 Sone - SONGS ( Vol . I. ) ... 0 . ire = 
 Paper Cloth KAREL NDL . 
 ” BACH . age ~~ GIPSY SONGS . Seriesrand2 ... 26 — 
 oO. Ss . ° » 
 1 . COMPOSITIONS .. « w wto — STERNDALE BENNE TT , 
 2 . COMPOSITIONS ... « . 4 . 1 0   — | SONGS . ( English German ) ... Io 26 
 g. COMPOSITIONS rr |S parages 
 Volume Po TY 4 0 GEO . J. BENNETT . 
 SONGS ( Robert Burns ) m 2 6 — 
 HANDEL . SONGS ( Shelley Rossetti ) 2 6 _ 
 4 . - COMPOSITIONS .. « 1 ro ) — BERLIOZ . 
 s. - COMPOSITIONS ... .. 1 0 = |SUMMER NIGHTS ... . « < < « © i. w @6 < = 
 6 . - COMPOSITIONS .. .. 1.0 = BRAHMS . 
 Volume ... — 4 © } . SEVEN SONGS ... “ ie . 
 COMPOSERS . . DANNRE UTHE 
 7 , MARCHES rad 7 _ . | SONGS ( D. G. Rossetti ) « 2 6 = 
 8 MARCHES ... 0 . wwe tS | SONGS ( W. Morris ) ... .. oe io 
 9 . MARCHES mM “ Al Owe ee ee — A. DV ORAK . 
 Volume oe SS ee 4 © | SIXTEENSSONGS ( Op . 2 , 5 , 17 , 31 ) ee _ 
 1o . SIXTEEN GAVOTTES , ae ee oe - . ae 
 ur . SIXTEEN GAVOTTES , & c. Dry Oe ie Se : pe J. W. ELLIOT r. ; : 
 12 , SIXTEEN GAVOTTES , & c. nee eg ~~ ) | NATIONAL NURSERY RHYMES , 665 Illustrations — 7 6 
 Volume nie _ 4.0 R. FRAN 
 THIRTY SONGS ... ee ee 
 MH . A. WOLLE NI H AU Pr , FOURTEEN SONGS ( Robert Burns ) . oe 
 133 . COMPOSITIONS ... ae oe > 
 m4 . SRENCCOMPOSITIONS ssc es ots EO HERMANN GOE 7 
 aon le ee eee er 
 Volume ove oe -o = 4 0 EDVARD GRIE G. 
 = - SONGS ... ... 26 = 
 O. SCHWEIZER . ; 
 SCOTTISH AIRS ( D LADY ARTHUR HIL L. 
 16 . E » ey om hl RO ley Bee . , ; wee oS 
 FRITZ SPINDLER . ADOLPH JENS SEN , 
 17 COMPOSITIONS .. .. « ro   — | - SONGS ... .. ow 26 = 
 a8 . COMPOSITIONS ... « . « . « 5 F 0 _ LISZT . 
 Ig . COMPOSITIONS ove a8 ‘ ok O — | SONGS ... oot _ 
 Vol eae jo 
 pisscaineesseis c% A. ¢. MACKENZIE . 
 HERMANN GOETZ . EIGHTEEN SONGS . Books .. .. 2 6 ~ 
 20 . LOSE BLATTER ( Op . 7 ) , 1 - 5 ee ae 2 QC | EIGHTEEN SONGS , Vol . Pe ee ees 
 21 . LOSE BLATTER ( Op.7),6 - 9 .. w « . wf O — Mz ARI ANI . 
 22 , GENREBILDER ( Op.13 ) .. 0 « 2 0s wwe « TE O)0d = | - SONGS , ( Italian ) ... 7 ee < = 
 Volume eee wo 4 0 NDELSSOHN . 
 ie RHE INBE RGER . SONGS . ( Portrait ) ... Pe . Folio — 21 0 
 ‘ SONGS . ( German English ) ... ea oo & 6 6 o 
 ” ) > - _ — 
 maranatha ty . ‘ — | SONGS ( Deep Voice ) . ( Ditto ) .. sy “ WGneIOR tor ~saer 
 25 . SEVEN COMPOSITIONS ... ... . « « « . Z © _ MOORE . 
 heiGne Velnie oe gt .. — 4 0|IRISH MELODIES . = Ss Sere Bice 
 IRISH MELODIES . ie li : RON 8 oo 
 BERTHOLD TOURS . R , ANDEGGE R. 
 26 . JUVENILE ALBUM ( Duets ) ... « “ » 2.0 — |SACRED SONGS LITTLE SINGERS . Illux 26 5 0 
 MOSCHELES . RUBINSTEIN . 
 27 . DOMESTIC LIFE ( 12 CharacteristicDuets ) Bx . I. 2 0   — | ! WENTY - SONGS . mee EB 
 28 . Ditto BID ’ ess pale . 210 _ SCHUBE RT . 
 Volume 4 0 ] SONGS ( Mezzo - soprano ) .. « . ww . 1 % 6 — 
 SONGS ( Contralto ) ... ee wa * % _ 
 HALFDAN KJE RULF . SONGS ( Soprano Tenor ) .. ise uw & = _ 
 29 . COMPOSITIONS ...... . 0   — |SCHWANENGESANG ( Swan Songs ) . 
 30 . COMPOSITIONS _ .. w. t 0 — | DIESCHONE MULLERIN(The Fair Maidofthe Mill ) 1 6 — 
 31 . - COMPOSITIONS . ee ] TN 
 Volume vor Sa ee 4.0 aba M — N. 
 SONGS « . Folio — 10 6 
 EDVARD GRIE G. MYRTHEN ( 26 Songa ) ste ae t 6 _ 
 32 . VIER ALBUMBLATTER ( Op . 28 ) , & . « 10 = ic Geece ee ee ee aoe - " 
 . POETISCHE TONBILDER ( O 1 = ri ey aes en Se ‘ 
 33 aout Gnawsad hemes ts . oo Nore 5 ) | WOMAN LOVE LIFE ( 8 Songs ) ... ‘ . = 
 34 . LYRISCHE STUCKCHEN ( Op . 12 ) AUS COMPOSERS . 
 ga ena 19 ) . ae EF © ~= — |OLD IRELAND ( Irish Melodies ) ... _ _ .. cc - 
 Se ! ete — 4 OlTHE SUNLIGHT SONG . 46 lllustrations ... — 5 0 
 List Contents ra isl tie . VOLKSLIEDER ALBUM apap sn English 
 German ... es evs 26 4 6 
 LONDON & NEW YORK : NOVELLO , EWER CO , LONDON & NEW YORK : NOVELLO , EWER , CO 

 Printed NovELto , EWER Co. , 69 & 70 , Dean Street ( W. ) , published 1 , Berners Street ( W. ) , 80 & 81 , Queen Street ( E. C. ) 
 Sold Kent Co. , Paternoster Row ( E.C.}—Thursday , March 1 , 1888 

