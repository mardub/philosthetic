THuRSDAY Eventnc . — Creser ’ Cantata SACRIFICE 
 FREIA ( write Festival ) ; Spohr Symphony CON- 
 SECRATION SOUND ; Violin Solos , & c. principal : Miss 
 MACINTYRE , Mr. LLOYD , Mr. BRERETON , Seftor SARA- 
 SATE 

 Fripay Morninc . — Parry ODE ST . 
 ( write Festival ) ; Mendelssohn VIOLIN CONCERTO 
 E MINOR ; Beethoven CHORAL SYMPHONY . principal : Miss 
 MACINTYRE , Miss FILLUNGER , Miss DAMIAN , Mr. IVER 
 McKAY , Mr. BRERETON , Sefor SARASATE 

 Fripay Eveninc . — Stanford Ballad 

 time music repre 

 sente " Promenades " Covent Garden 
 Theatre . symphony Haydn , Mozart , Schu- 
 mann , Schubert , symphony 
 Beethoven succession , orchestral | 
 portion ninth , perform " " 
 meeting . ' season Saturday Concerts com- | 
 mence October 13 , Christmas 

 addition number favourite piece , 

 like success attend Sir Charles Hallé series 

 Concerts , thread temporarily 
 drop — — Popular Concerts . 
 interesting novelty 
 forward , delight confess en- 
 joyment chamber music write 
 Beethoven , Mozart , Haydn , Schubert . 
 artist ’ Concerts legion . messr 

 parent | 
 body , Sacred Harmonic Society , example 

 remarkable robustness mind character 

 ifnot physique . travel adventure love 
 Nature , great case , prove powerful 
 incentive genius composer . ' 
 combative , contentious , pugnacious . 
 lack virility character 
 Beethoven . Handel sturdy stuff , capable 
 volcanic explosion fury . extraordinary 
 recuperative energy gauge tact 
 write fine work paralytic seizure . 
 , surely , lack physical energy . Men- 
 delssohn wonderfully good - round man — 
 nthil tetigit quod non ornavit . time robust 
 individuality Brahms music outcome 
 thoroughly masculine nature . hothouse life 
 , mind , erroneously consider 
 parcel musician career dwarf 
 stimulate genius . ' manual labour copy- 
 e composition desk ; 
 actual inspiration , notable instance , 
 come composer open air . , 
 believe adverse circumstance 
 composer - - day work 

 

 EDWARD FITZGERALD letter 

 tue allusion music Edward Fitzgerald 
 letter , recently publish Macmillan , 
 frequent suggestive warrant extract 
 benefit reader Musica 
 Times . Fitzgerald literary man 
 literary man condescension art 
 know little . 
 musician , play pianoforte organ fairly , 
 glee tunefully correctly . 
 compose , song 
 print , aversion publicity probably 
 prevent title - page . 
 Mr. Aldis Wright quote preface amusing 
 recollection late Archdeacon Groome 
 Fitzgerald musical taste . old age 
 Fitzgerald compare note subject 
 great singer year ago — Braham Vaughan 
 Miss Stephens , performance ' Acis 
 Galatea " Concerts Ancient Music . ' 1 
 , " Vitzgerald , " dear 
 old creeter gold eye - glass turban , 
 nod head sing ' o pleasure 
 plains’’’—the old creeter soprano 
 sing girl George 
 ke . ' great lover , " add Archdeacon 
 Groome , ' old [: nglish composer , specially 
 Shield . Handel , , scroll marble 
 hand Abbey , write 
 bar ' know Redeemer liveth ' ; Shield 
 hold title - scroll , write 
 bar ' flaxen - headed ploughboy . ' " 
 turn Fitzgerald letter , find 
 assiduous opera concert - goer company 
 Frederic Tennyson , Laureate brother , 
 epistle musical chat . quote 
 follow curious passage page 92 - 4 , 
 contain acute criticism , 
 writer power appreciation limit : " dear 
 Frederic,—Concerning bagwig composer . 
 Handel bagwig . . . .. Haydn 
 mozart’s — influential character , 
 ostentatious : tower 
 high , roll follow ( ? flow ) curl 
 low overlay nature brain . 
 Handel wear Sir Godfrey Kneller wig — 
 great wig , great general 
 day use head fatigue 
 battle , hand valet 
 bullet comb . wig fugue 
 . " follow passage Fitzgerald 
 inability comprehend Beethoven . ' Mozart , 
 agree vou , universal musical genius . 
 Beethoven analytical erudite , 
 inspiration true . . . . think 
 , strickly speak , thinker 
 musician . great genius , ... . 
 try think music — reason 
 music — , , content 

 feeling . speak definitely . 

 Nasce al bosco rozza cuna , un felice pastorello . ' 
 describe ' walketh 
 wing wind ' fall happily ' e con 
 l’aura di fortuna , ' pastorello sail 
 . ' character music ease large- 
 ness : shepherd live , soGod Almighty walk 
 wind . music breathe ease : word 
 tell easy . . . music far 
 universal language , piece particular 
 strain symbolise analogous phenomenon , 
 spiritual material — talk spiritual 
 phenomena . ' Eroica Symphony describe 
 battle passion armed man . " 
 write correspondent 1844 , Fitzgerald 
 : " play evening Handel great 

 chorus , brave music , . 
 |am true John Bull style music . 1 
 | delight Handel Allegro Penseroso . 
 { know fine pompous joyous chorus ' 
 | pleasure , mirth , thou canst , ' & c. ? Handel 
 | certainly music old Bacon desire 
 | essay Masques , ' let song loud 
 |cheerful — pule , ' & c. think 
 | water - music write text . " contrari- 
 |wise , Fitzgerald little sympathy 
 |modern music , english foreign . ' 
 | dreadful vulgar ballad , " write year , 
 |**compose Mr. Balfe , sing 
 { unbounded applause Miss Rainforth , ' dream 
 ithat dwell marble hall , ' sing 
 |organe corner London . think 
 | imagine kind flowing 6 - 8 time 
 | degree imbecility . ' word write 
 Mr. Bunn ! Arcades ambo ! " following year 
 |he write : " new tell music 
 | ... hear ' Le Desert ' ; fancy 
 | English come fair judgment . , 
 | want hear . 
 | play time , new batch people ; 
 | doubt twice . nearly 
 | french thing : clever showy surface : 
 |no Holy Holies far withdraw ; conceive 
 depth mind , receive 
 depth attention . Beethoven , 
 experience , depth 
 reach . admit 
 bizarre , , think , morbid . original , 
 majestic , profound 

 Finale C minor Symphony speak 
 noble , add : ' * , like hear 
 Mozart ; Beethoven gloomy . , in- 
 contestably , Mozart pure musician ; Beethoven 
 poet painter , 
 deep great soul , imagination . " ' 
 humorous passage letter date 4 , 1848 : " 
 hear famous Jenny Lind , 
 world rave . spedding especially 
 mad , 1 understand ; , , 
 good weak vessel way ? 
 night night bald head par- 
 ticular positionin Opera House , stall ; 
 miserable man forget Bacon philosophy , 
 strange woman . doubt 
 lady wondertul singer ; hot 
 crowd till Pasta come ; hear 
 worth crush . 
 perform head Handel chorus 
 Exeter Hall performance . 
 hear Mendelssohn ' Elijah ' spring . 
 find worth trouble . 
 good music , original ; Haydn - 
 . think day oratorio , like 
 day paint Holy Families , & c. 
 tired oratorio , 
 tired Raffaelle . Mendelsschn 

 466 

 pianoforte sell . " dealer : ' 
 matter , Madam ? " customer : ' 
 key crack , brittle . " 
 dealer : ' ah ! account . , 
 man Africa time shoot 
 elephant , ivory key . little 
 ago shoot baby elephant mistake , 
 key infant pachy- 
 derm tusk turn , customer 
 trouble 

 prospect Leeds Musical Festival 
 good , application serial ticket consider- 
 ably advance 1886 . change appear 
 late edition programme ; overture 
 " Le Nozze " " Der Freyschitz " place 
 Beethoven ' ' Leonora " Gounod " Mirella . " 
 selection Sullivan music ' ' Macbeth " 
 include scheme , Dr. Mac- 
 kenzie ' Pibroch ' * — scottish fantasia , violin 
 ( Mr. Sarasate ) orchestra . analytical note 
 programme - book write Mr. 
 Joseph Bennett . haere 

 tue rage minutely describe dress artist 
 singe public evidently 
 increase , paragraph state vocalist " look 
 nice silver grey , " example , read 
 interest criticism 
 singing . sorry ; 
 importance matter ignore 
 artist sufficiently evident 
 remark , presence audience 
 Festival , know singer " receive 
 large sum concert , meanness 
 appear day dress 

 hear London recent year , , believe , 
 itis unknown lancashire public . fact 
 composer favourite excite interest 
 sympathy Manchester amateur 

 ENG isu admirer Beethoven liberty 
 contribute fund raise 
 Germany purchase great master ’ 
 ' " " birth - house , " establishment 
 Beethoven museum . ' subscribe £ 2 10 . 
 shareholder ( dividend ) , £ 25 
 receive diploma patron 

 recent issue Musical Courier " con- 
 trive double debt pay ’' : — ' * ' 
 anybody afford waste time read 
 foolish paper Chicago — — , 
 hand bright - edit journal 
 Chicago — — , recent copy 
 |just receive 

 RICHTER CONCERTS 

 musician point view penultimate Concert 
 season Monday , ist ult . , inter- 
 esting series , hackneyed wagnerian excerpt 
 time aside favour novelty 
 ordinary significance . Dr. Hubert Parry Symphony 
 E minor , work worthy prolific composer , 
 elaborate ambitious - ' * english 
 Symphony , " outcome request 
 Herr Richter , year Birmingham Festival , 
 write Symphony specially Concerts . 
 probably account marked resemblance 
 movement Symphony opening 
 composer Oratorio ' * Judith . " " note 
 alternation rugged syncopated figure suave , 
 flowing melody , distinct similarity 
 character principal theme . , , 
 movement restless ; contain fine 
 passage , equal composer . 
 section key C long draw , 
 deep expression , decidedly religious character . 
 movement , Allegro scherzoso minor , 
 gem work . annotator 
 suggest ' al fresco féte olden time — coquettish 
 dance lord lady , interrupt song . " 
 right regard opening theme , detail 
 essentially modern . consequence , 
 style movement thoroughly 
 charming , close Dr. Parry 
 platform vociferously applaud . Finale 
 triumphant tone , 
 restless movement somewhat diffi- 
 cult follow hearing . 
 , worthily conclude work certain 
 hear . piece curiosity 
 way , movement Pianoforte Concerto , allege 
 Beethoven . 
 find possession Herr Emil Bezecny , Prague 

 submit Dr. Guido Adler pro 

 nounce movement genuine production 
 Beethoven , probably date 1790 . movement 
 ordinary Concerto form time , 
 pen Mozart weak 
 moment , beethovenish touch 
 colour idea 
 juvenile effort mighty Bonn master . intrinsi- 
 cally , , little value , Madame 
 Stepanoff play quiet refinement suit 
 music period , certainly 
 employ interesting music . imposing Finale 
 ' Gétterdammerung , " Miss Fillunger sustain 
 Briinnhilde considerable effect , Beet- 
 hoven symphony F ( . 8) , magnificently play , 
 complete programme 

 follow Monday season bring 
 conclusion performance Berlioz ' Faust , " 
 splendid rendering orchestral portion 
 indifferent singing choir con- 
 spicuous feature . defect chorister 

 MUSIC EDINBURGH . 
 ( correspondent 

 performance chronicle ot 
 Recitals Miss Clara Lichtenstein 
 session connection Charlotte Square 
 Ladies ’ Institution . long course study Buda- 
 Pesth Conservatorium equip comprehen- 
 sive répertoire , , Recitals avowedly 
 educational end view , hamper way 
 popular prejudice . historical sequence 
 observe programme , 
 interfere variety . interpreta- 
 tion Brahms Scherzo successful effort 
 Recital , rendering Beethoven Trio 
 C minor , assistance Mr. Carl Hamilton 
 Mrs. Kedzlie , appreciate audience . 
 singing Miss Georgette Lichtenstein , 
 Conservatorium , add greatly charm 
 interest recital . 
 beautiful rendering Mackenzie " boat * ' ( violon- 
 cello obbligato Mr. Hamilton ) , sing Grieg 
 " Ich liebe dich " great expression 

 MUSIC SOUTH WALES . 
 ( correspondent 

 inauguration enterprise . London Military Band 
 certain great request , particularly open air 
 gathering 

 Viottn Pianoforte Recital Mr 
 Nachtz Mr. Arthur Friedheim Princes ’ Hall , 
 fon 1st ult . unfortunately Shah week , 
 ordinary performance suffer , audience 
 depressingly small . owe 
 cause Mr. Nachéz good , intonation 
 | execution rapid passage Beethoven " * Kreutzer " ’ 
 Sonata , Max Bruch Concerto G minor ( . 1 ) , 
 Bach Sonata G minor incorrect . Mr. 
 | Friedheim play solid accurate manner , far 
 | regard note , succeed Liszt 
 | rhapsodical piece , miscall Sonata b minor , satis 
 factory interesting . altogether performance 
 | leave favourable impression mind 

 Cuorat Festival hold St. Barnabas ’ Church , 
 | South Kennington , Sunday , June 30 , choir 
 church assist member Choral 
 | Society . canticle sing tour F , 
 sermon selection Schubert ' Miriam , ' 
 Barnby ' * Rebekah , ' Farmer ' Christ 
 | soldier . solo Miss L. 
 Jecks , Miss Williamson , Rev. St. Clare Hill . 
 service , include Canticles , Hymns , 

 Tue Music Scholarships Mary Datchelor Colle- 
 giate School , Camberwell , award year 
 Miss Grace Keeble ( pianoforte pupil Miss Fitch ) 
 Miss Marion Kitching ( singing pupil Miss Bessie Cox ) . 
 competition , student ( Misses Emily Bullock 
 Amy McDowall ) receive special prize ; 
 Miss Jessie Foster Miss Rose Campbell earn 
 honourable mention . Miss Fitch , superintendent 
 music teaching Datchelor School , con- 
 gratulate success achieve student , 
 win honour Royal Academy Local 
 Examination , send pass 

 Str CHARLES HALLE bring Concerts close 
 Friday , June 28 , familiar programme , include 
 Brahms quintet F minor ( Op . 34 ) , Beethoven 
 * * Kreutzer ’ ? Sonata , Handel Violin Sonata , 
 surely place time companion 
 hear , Schumann delightful ' * Wald- 
 scenen " ' pianoforte . executant 
 previous occasion , wish 
 change . Sir Charles Hallé season exceptionally 
 interesting , production hitherto unknown 
 Quartets Cherubini sufficient distinc- 
 tion series Chamber Concerts 

 opera season Majesty Theatre close 
 abruptly , scarcely unexpectedly , 
 June 29 . announcement Madame Sembrich 
 engage ground hope Mr. Mapleson 
 retrieve fortune ; prima donna 
 ill appear , public 
 scant interest faded opera Mr. 
 Mapleson rely , choice close 
 house . experience season afford valuable 
 lesson manager hope learn 
 inwardly digest 

 attractive Bazaar Garden Féte hold 
 17th 2oth ult . ground North- 
 East London Institute School Music , Science , Art , 
 aid fund provide new building , present 
 accommodation having insufficient meet 
 demand . opening ceremony conduct 
 17th Mrs. H. L. Lawson , 18th Dr. A. C , 
 Mackenzie . School Music ( principal , Mr. Ebenezer 
 Prout ) number thirty professor , 
 successful work 

 Mr. W. pe Mansy Sercison fifth annual 
 Concert June 26 , Princes ’ Hall , assist number 
 talented artist , vocal instrumental . 
 highly interesting programme provide include 
 Beethoven Sonata pianoforte violin F major 
 ( Mr. Tivadar Nachéz Concert - giver ) , Rubin- 
 stein ' Salon Sticke , " pianoforte violoncello 
 ( Mr. Leo Stern Concert - giver ) , Chopin Scherzo 
 C minor pianoforte ( Miss Jeanne Douste ) , vocal 
 solo Concone , Diaz , Haydn , Clay , 

 Mr. JoHN THomas annual Harp Concert 
 June 29 , St. James Hall . programme , usual , 
 contain piece band harp , charming Trio 
 harp , violin , organ , harp duet 
 Concert - giver . pro- 
 gramme , , considerable extent , 
 familiar ballad , Madame Valleria , Madame Edith 
 Wynne , Miss Liza Lehmann , Miss Eleanor Rees , Mr. 
 Hirwen Jones , Mr. Daniel Price , Misses Clara 
 Marianne Eissler 

 recent performance Bale Bach ' ' St. Matthew 
 Passion music , " ' bass solo sing Professor 
 Julius Stockhausen , eminent german singer , 
 remember amateur country . : 
 Stockhausen , complete - year , 
 remain head german concert - singer 

 read , Frankfort paper , eulogistic 
 account concern pianoforte playing Mr. How- 
 grave , young Englishman mention 
 previously column . recent public 
 | Concert Raff - Conservatorium , Mr. Howgrave ( 
 pupil institution ) play , inter alia , Beet- 
 jhoven variation e flat major ( Op . 35 ) manner 
 | , Frankfurter Zeitung , " entitle 
 | place executant day pupil 
 | performance . seldom hear Beethoven , " add 
 |the journal question , ' ' interpret truly 
 | classical style , mature artist . " 
 | Herr Felix Mottl , eminent german conductor , pro- 
 | pose perform season , Carlsruhe Hof 

 Theater , Berlioz opera ' Les Troyens " ' entirety . 
 | performance wiil event musical world , 
 | work hitherto produce enormously 
 curtail condition , notably Théatre - Lyrique , ot 
 | Paris , year ago , second act 
 | entirely omit 

 Kinac Lynn.—The annual Choir School Festival London | 
 Road Primitive Methodist Church hold Monday , 15th ult . , | 
 St. James ’ Hall , G. F. Root Cantata ' ' Palms , " | 
 band accompaniment , perform . special scenery illus- 
 trate subject Cantata fit Mr. G. M. Bridges , 
 palm , fern , plant platform . music 
 render excellent effect precision . chorus 
 child receive . Mr. J. G. Churchman leader 
 band , Miss Barnard , Miss Rutter , Miss Sporne , Mr. E. Taylor , 
 Mr. G. Rose principal vocalist . Mr. W.O. Jones ' preside 
 pianoforte , Mr. B. Hewetson organ , Mr. Jasper J. 
 Wright conduct 

 Lreeps.—The Borough Organist half- 
 yearly Organ Recitals atthe Town Hall onthe 13thult . following 
 programme : — Grand March , L’Etoile du Nord ( Meyerbeer ) ; 
 romance , " resignation " ( Eugene Wagner ) ; old english organ 
 piece , Voluntary D ( John Keble ) ; Larghetto Grand 
 Symphony D ( Beethoven ) ; modern french organ pieces—{a ) 
 Pastorale G , ( 6 ) Finale D ( Widor ) ; duet , ' ' come , smile 
 liberty , " chorus , " sing unto God , " F#udas Maccabeus ( Handel ) . 
 — reward proticiency attain student past 
 year Leeds Conservatoire Music , Cookridge Street , 
 distribute 13th ult . Albert Hall , Alderman Sir Edwin 
 Gaunt . entertainment pupil . Herr Alfred 
 Giessing Conductor , Herr Christensen accompany . — — 
 fourteenth annual Service Song Choir Salem Chapel 
 hold ryth ult . , Sir A. Sullivan Oratorio pro- 
 digal Son choir voice . solo 
 entrust Mrs. Naylor , Mrs. Curess , Mr. C. Suther , Mr. John 
 Browning . solo chorus render 
 expression taste , appreciate . Mr. Hardingham 
 accompany , play J. S. Bach Toccata fugue 
 d minor Sir W. Sterndale Bennett Barcarolle F. Mr. W. 
 Toothill conductor 

 Morrat , N.B.—The new organ , build Messrs. Bevington 
 Sons , London , United Presbyterian Church , open 
 Dr. Creser , Organist Parish Church , Leeds , assist Mrs. 
 Creser , vocalist , 5th ult 

 popular NUMBERS 

 4 . british Grenadiers . s. a.7.b. G. A. Macfarren 2d . 72 . Hearts oak G. A. Macfarren 1d 
 8 . soldier ’ chorus . 1.1.b.b. .. Gounod 4d . 80 . Christ rise ( e anter Anthem ) . S.A.T.B. Berlioz 34 , 
 g. Kermesse ( scene ' Faust ’' ) 6d . 81 . sun set o’er mountain ( ' ii 
 10 . , quit thy bower . s.a.t.e . = Brinley Richards 4d . Demonio " ' ) a. Rubinstein 34 . 
 14 . Gipsy Chorus ... ar se . Balfe 4d . ’ 82 . hymnof nature ... ; + » Beethoven 34 , 
 21 . anold Church song . s. A.T.B. .. Henry Smart 2d , 83 . Michaelmas Day ( humorous - song , . 1 ) 
 22 . Sabbath Bells . s.a.r.p . .. ae " 2d . Walter Maynard 4a. 
 23 . Serenade . SAPDB , sc 2d . 84 . SportingNotes ( humorous - song , No.2 ) , , 4d . 
 28 . Marchofthe Menof Harlech . s.a.1.8.dr . Rimbault 2d . gt . Star Bethlehem ( Christmas Carol ) 
 29 . God save queen . S.A.T.B. se . d. T. L. Clemens 2d . 
 30 . Rule , Britannia ! s.a.7.b. r 1d . 2 . busy , curious , thirsty fly . T.a . T.B. " 3d . 
 34 . Market Chorus ( ' * Mz saniello " ) , S.A.T.B. Auber 4d . 93 . love wake weep . a.1.B.B. Felix W. Morley 2d . 
 35 . prayer ( ' ' Masaniello " ' ) .   s.a.r . b. 1d . 94 . brother , thou art Arthur Sullivan 6d . 
 51 . Charity ( La Carita ) . fe ea ec Rossini 4d . 95 . come away willing foot " 3 6d . 
 57 . Chough Crow .. Str H.R. Bishop 3d . 96 . Madrigal ( ' ' Mikado ’' ) .. * : 6d . 
 58 . ' " Carnovale " .. ve Bs Rossini 2d . 97 . little maid ( ditto ) .. - od . 
 65 . Dame Durden . ; st " 1d . g8 . climb rocky mountain ( 
 66 . little Farm w ell till * : . Hook 1d . " pirate Penzance " ' ) ‘ 3 6d . 
 67 . simple maiden .. . Mac sfurren 1d . . hunting chorus = ar Alfred Cellier 2d . 
 69 . love maiden fair .. es 5 1d . ror . song Orval ... Tsidore de Lara 64 . 
 71 . oak ash .. ss ie rd . £ 02 , Madrigal ( * ' Ruddigore " ) Arthur Sullivan 64d 

 Couns te list application . 
 new ork singing class 

