OUTLINE performance 

 TugespAy Morninc : ELIJAH . Tuespay Eveninc : Brahms 
 SONG DESTINY ; Mr. Edward German new ORCHES- 
 TRAL WORK ( compose expressly Festival ) ; Beethoven 
 C MINOR SYMPHONY , . 5 ; Wagner MEISTERSINGER 
 OVERTURE ; ; scene 3 , Act iii . , DIE WALKURE ; Schumann 
 MANFRED OVERTURE 

 WEDNESDAY MorninG : Professor Stanford new REQUIEM 
 MASS ( time performance ) ; Bach Cantata O light 
 EVERLASTING ; Brahms SYMPHONY , . 1 . WEDNESDAY 
 EveENING : Purcell ’ KING ARTHUR MUSIC ( specially edit 
 Mr. J. A. Fuller Maitland Festival ) ; Cherubinits MEDEA 
 OVERTURE ; Beethoven LEONORA OVERTURE , . 3 

 TuHurRsDAyY Morninc : MESSIAH . Tuurspay Eveninc : Gluck 
 IPHIGENIA AULIS overture ; ; Arthur Somervell new 
 Cantata , ODE SEA ( compose expressly Festival ) ; 
 Wagner SIEGFRIEDIDYLL ; Mozart’sG MINOR SYMPHONY ; 
 Dvorak carnival overture 

 Sept. 15 . — ' ' stronghold sure " ( Bach ) , Magnificat ( Hubert Parry ) , 
 selection " Parsifal " ( Wagner ) . ' judgment " ( Spohr ) . 
 evening : " Elijah 

 Sept. 16.—mass D ( Beethoven ) , Symphony , b minor ( Tschai- 
 kowsky ) , ' creation ' ( i. ) . evening : ' ' redemption 

 Sept. 17 . — ' Messiah . " evening : chamber concert Shirehall 

 complain autumnal week 
 work . cherish great respect 

 listen 
 wonder . town - bear , mayhap , , 
 cling breast mother . 
 doubtless happy way , 
 , appoint season , instinct 
 opportunity hurry child field 
 hedge - row native haunt . 
 music , sonata 
 Beethoven nocturne Chopin come 
 open window rose- 
 clad cottage , bespeak presence 
 migrant distant city . country 
 music , form 
 Othello willing tolerate 

 music hear . " know 

 MUSICAL TIMES.—Avceust 1 , 1897 

 doubt plume polyphony , 
 bound admire masterfulness ; composer , like 
 Keats owl , ' ' feather - cold . " 
 music impress , sorry 
 long delay final bar reach . second 
 performance cause alter opinion , 
 present consider Finale interesting 
 symphony . composer conduct , , 
 capital performance , warmly applaud 
 audience . Mr. Alexander Siloti play Beethoven E flat 
 Pianoforte Concerto fine technique hardly suf- 
 ficient expression ; Mdlle . Camilla Landi , admirable artist 
 , complete justice air ' ' printemps 
 qui commence , " Saint - Saéns ' ' Samson et Delila , " 
 air Handel ; pompous pointed per- 
 formance " Meistersinger " ’ overture , conduct 
 Sir Alexander Mackenzie , complete programme 

 LEEDS FESTIVAL CHOIR LONDON 

 pianoforte recital Queen ( Small ) 
 Hall Miss Edie Barnett , pupil Miss Fanny Davies , 
 Friday afternoon , 2nd ult . juvenile executant 
 state year age , 
 effort scarcely amenable criticism . , 
 , dawn ability mean order , 
 movement Bach Clavier Concerto 
 D minor Mendelssohn Capriccio Brillant b 
 minor ( op . 22 ) Miss Barnett aid Miss Davies , 
 play second pianoforte arrangement 
 orchestral accompaniment . solo include 
 piece Schumann , Sterndale Bennett , Stephen 
 Heller 

 Miss Gertrude Palmer , pianoforte recital 
 Queen ( Small ) Hall Saturday afternoon , 
 3rd ult . , come Sydney , New South Wales . " advance , 
 Australia , ' motto young 
 aspiring musician hail Antipodes , 
 reason Miss Gertrude Palmer 
 eventually lead place profession , 
 unquestionable ability . programme include 
 number minor piece Schiitt , Chopin , Heller , Liszt , 
 Brahms , Beethoven Sonata D ( Op . , 
 . 3 ) Chopin rondo c pianoforte ( Op . 
 73 ) , assist Mr. Algernon Ashton 

 Miss Edith A. Greenhill responsible pianoforte 
 recital Salle Erard Tuesday evening , 6th ult . | 
 pianoforte recital evening somewhat a/ 
 novelty , Miss Greenhill secure large fashionable 
 audience . commence prelude Bach , Handel , 
 Mendelssohn , pass Brahms fine variation 
 ona theme Schumann ( op . g ) , 
 justice . Beethoven sonata d minor ( op . 31 , . 2 ) 
 play , lovely Adagio b flat especially 
 interpret taste demand . piece Field , 
 Chopin , Liszt , Cramer , Steibelt , Henselt , Rubinstein 
 bring performance conclusion 

 ROYAL COLLEGE MUSIC 

 Co. , festival committee choose 

 thirty tune chant , anthem , 
 " OQ love Lord " ( Sullivan ) ' , soul , 
 land brightness ' ? ( J. H. Roberts ) , 
 chorus , ' ' worthy Lamb " ' * hallelujah 
 Father ' ( Beethoven ) , render meeting . 
 singing afternoon evening gathering 
 reach , , high level excellence , 
 remarkable chorister unaccustomed 
 sing orchestral accompaniment , previous unite 
 rehearsal hold . soprano voice 
 fine , tenor bass good , alto weak , 
 rendering hymn - tune effective . 
 , , lack electrical religious 
 fervour distinguish welsh congregational 
 singing , light shade singing 
 minor light tune . great choir 
 enjoy chant , unsatisfactory 
 printing word . effect ' worthy 
 lamb " , , impressive , precision 
 attack , quality tone , heartiness singing 
 excellent . performance Beethoven ' hallelujah " 
 praiseworthy ; blemish 
 detect train ear comparatively 
 trifling . Mr. J. H. Roberts conduct anthem . 
 meeting Miss Kate Morgan , Dowlais 
 ( winner gold medal Chicago ) , sing . 
 success festival encourage welsh musician 
 persevere effort encourage orchestral 
 music , step organisation 
 united festival — , Carnarvon Castle — ' 
 Messiah " ' perform choir gather 
 North Wales 

 mors et vita " ITALY 

 interesting paper read day 
 training choirboy , Mr. Walter 
 Henry Hall , organist choirmaster St. James 
 Church , New York , illustrate principle tone 
 production Mr. Hall platform dozen 
 choirboy , sing exercise anthem 
 manner toelicit hearty applause large 
 audience gather hear late idea subject 
 tone production . Mr. Hall enthusiastic believer 
 method training develop " thin " ' 
 ' head " distinguish " thick " ' ' chest " 
 register , plea attention 
 important branch choirmaster duty . 
 speaker remark , deprecate , temptation 
 conscientious organist " flesh - pot " 
 ear - tickle , sentimental novelty congregation 
 instead ' ' manna " true standard work , 
 reason emotional nature 
 appeal music Church . 
 blemish sensational mechanical organ playing 
 comment , speaker conclude 
 point great opportunity responsibility 
 choirmaster , exhort adoption 
 high good standard , , 
 example influence , influence generation 
 come 

 ' grand orchestral concert " evening attrac- 
 tion , special mention Mr. Harry Rowe 
 Shelley new Symphony E flat , instru- 
 mental composition magnitude . work 
 original idea , influence master , 
 Dvorak , Beethoven , little Wagner 
 trace 

 Saturday morning Virgil Piano 
 recital , pianoforte recital Mr. William H. Sherwood , 
 song recital Mrs. Gerrit Smith , conference 
 ' * musical Journalism , " chairmanship Mr 

 unusually successful ; union exist | . lady , rest Carlsbad , 
 Owens College Royal Manchester College | sign engagement Opéra Comique 
 Music tend advantage Institutions , | month . ' Louise , " new work M. Charpentier , 
 enable Dr. Hiles , control composition depart- | accept performance M. Carvalho . owe 
 ment , carry large comprehensive | delicate health M. Daubé subject 
 scheme possible , ensure the| time past , management appoint 
 attainment high executive skill acquaintance | assistant - conductor person M. Alexandre Luigini , 
 practical modern art University graduate , | hitherto conductor theatre Lyons , able 
 thorough verse inthe grammar construction music | musician . engagement enter 
 student College Ducie Street . , house Mdlle . de Lafond 

 Saturday , 3rd ult . , Vice - Chancellor confer ; musical world capital having greatly 
 degree Mus . B. student only|put news discontinuance 
 complete year ’ course , | Lamoureux Concerts , erroneous report having 
 satisfy examiner ' " Exercises’’- submit . | spread subject person pretend 
 finish study Owens | - inform , state actual fact , 
 college class , send motet | : place , M. Lamoureux 
 originative talent test ; andj know turn matter . 
 understand examination junior under- | hope find , time great Exhibition 
 graduate second year marked | year 1900 hold , head theatre 
 advance , exciting bright hope gradually increase | ' ' Der Ring des Nibelungen " Wagner 
 attainment . College Music Principal , Mr. | work produce . necessary 
 Brodsky , reason gratulation . evening | great liberty time come , order 
 devote public demonstration success | needful preparation undertaking , 
 work year . annual festival week | pay prolonged visit Germany engagement 
 College excite wide eager attention , | interpreter high order , & c. equally im- 
 year orchestral rendering ( direction | portant famous orchestra , 
 Principal ) Brahms Symphony ( . 2 ) | , , desire series concert 
 " Leonora " Overture ( . 3 ) , performance | periodically London Berlin . doubt way 
 act Mozart ' " Il Flauto magico " operatic class , | find reconcile conflict personal interest , 
 afford convincing evidence general excellence | know matter arrange . 
 talent discipline Institution . succeed | ' Les Erynnies , " Leconte de Lisle , Massenet 
 evening programme devote mis- | music , produce Théatre d'Orange , 
 cellaneous display acquirement small group | follow ' ' Antigone , " music Saint - Saéns . 
 executant . specially notice ensemble play- | - composer engage , M. Louis Gallet 
 e member Mr. Carl Fuchs ’ class selection | librettist , elaboration work retrace history 
 chamber work Brahms , Schumann , Beethoven , | nineteenth century , view performance 
 Dvorak , ; pianoforte solo performance | come World Exhibition 

 advanced pupil Miss Olga Néruda| Grand Prix de Rome award 
 Mr. Dayas ; finish playing | M. d'Olonne , pupil MM . Massenet Lenepveu , 
 young people enjoy advantage | second prize M. Crose Spinelli , 
 direct care Mr. Brodsky . work choir|i deuxitéme second Grand Prix M. 
 light , satisfactory ; stress , | Schmitt 

 contest . eighteenth century music 

 morning , ' ' stronghold sure ' ? ( Bach ) , Magnificat ( Dr. 
 Hubert Parry ) , selection ' ' Parsifal " ' ( Wagner ) , 
 ' ' judgment " ( Spohr ) ; evening devote 
 Mendelssohn " Elijah . " Thursday morning , Mass 
 D ( Beethoven ) , Symphony B minor ( Tschaikowsky ) , 
 ' creation ' ( i. ) ; Gounod ' ' redemption " 
 occupy evening . ' ' Messiah " 
 Friday morning , festival conclude 
 evening chamber concert Shire Hall . 
 principal vocalist engage Madame Albani , Miss 
 Anna Williams , Madame Medora Henson , Miss Hilda 
 Wilson , Miss Marie Brema , Miss Jessie King , Miss Marian 

 Watkin Mills , Mr. 
 Price 

 organ performance Town Hall £ 1,000 , 
 M. Auguste Wiegand , city organist , receive salary 
 £ 500 , annual profit £ 500 

 Fritz Steinbach , propose series concert 
 |shortly , Brahms symphony work 
 | Bach , Beethoven , Schubert , Wagner programme . 
 | professor Joachim Hausmann , Herr Felix Kraus , 
 Mr. Eugene d’Albert 
 | soloist . important new choral work nor- 
 wegian composer , W. Stenhammar , entitle ' " Snofrid , " ' 
 | produce Philharmonic Choir come 
 | season.——An interesting valuable series ' * life 
 | Great Composers " ' issue newly 
 | found Berlin publishing company ' Harmonie . " pro- 
 ' fessor H. Reimann , eminent organist , 
 |editor , contributor series 
 |Dr . Jadassohn , Dr. Bluthaupt , Professors Gernsheim 
 land L. Auer , Herren Otto Lessmann , , Niggli , 
 Wittmann , Capellmeister Volbach , ' " ' La Mara , " 
 numerous distinguished writer 

 bources.—a monument unveil June 20 , 
 erect composer Louis Lacombe , ceremony 
 including performance prelude march 
 decease opera ' ' Winkelried , " bring 
 year Geneva 

 polish enthusiast lately cause memorial 
 erect commemorate fact , bear bronze 
 medallion - portrait composer 

 BRussELS.—Musical performance Hall 
 International Exhibition , acoustic defect 
 extent ameliorate , inaugurate officially , 
 15th ult . , grand concert , direction 
 M. Ysaye , include Schumann Symphony C , 
 overture ' ' Freischiitz ’' " Tannhauser , " much- 
 appreciate rendering , MM . Ysaye 
 Thomson , Bach Concerto D violins.——A 
 highly - appreciate performance 
 unique orchestra clarinettist , thirty - seven number , 
 conductorship M. Poncelet , - know 
 professor Royal Conservatoire . Mozart G minor 
 Symphony , Adagio Beethoven ' ' Sonata Pathé- 
 tique ' ? Weber ' ' Moto continuo , " rhapsody 
 Liszt effectively render curious ensemble 
 instrumentalist , performance attract 
 numerous audience.——On 22nd ult . perform- 
 ance announce place M. Tinel drame 
 musical , ' * St. Godelive , " ' 300 executant , 
 direction composer 

 Caracas.—The capital Venezuela , 
 possess Opera - house Philharmonic Society , 
 chiefly support german resident , establish 
 Conservatoire Music aid govern- | 
 ment grant 

 CARLSRUHE.—The season opera Hof - Theater | 
 come close June 13 performance , 
 enhance price , entire ' " Nibelungen " tetralogy . | 
 novelty come season a| 
 comic opera somewhat awkward title " Das | 
 Unmodglichste von Allem , " Herr Anton Urspruch , | 
 bring Frankfort | 
 Weimar.—A number special performance opera 
 announce place , Herr Mottl direction , 
 September 5 Cctober 3 , work 
 produce Berlioz ' " ' Les Troyens " ' ( ) , 
 Liszt ' St. Elizabeth " ( scenic representation ) , Wagner | 
 " Tannhauser , " ' Lohengrin , " ' Tristan , " ' die 
 Meistersinger , ’' Beethoven ' Fidelio 

 CosurG.—A new - act opera , " * Die Grille " ( ' ' 
 Cricket ’' ) , Herr Johannes Dobber , Capellmeister 
 Court Theatre , libretto found story 
 Georges Sand , shortly bring 
 Leipzig 

 BACH . — " Passion " ( St. Matthew ) .. ee 

 BEETHOVEN . — " Mount Olives " ( en enki . ) 
 ( Tonic Sol- fa 

 BENNETT . — " onal ( Paper boards , ts . 6d . ) 
 ELGAR . — " King Olaf " .. ' ( Tonic Sol - fa ) 
 GLUCK . — " Orpheus " .. . 4 ( Tonic Sol - fa ) 
 GRAUN , C. H. — " Peden ’ al te Tod Jesu " ) 
 HANDEL . — Messiah " " ( Paper boards , ts . " - ) 
 — " Solomon " .. " aa 

