Praise " ( Mendelssohn ) . OcToBerR 29 ; NOVEMBER 12 , 26 ; DECEMBER 10 . 
 | THURSDAY , 11.30 , New Cantata ( Parry ) ; " Eroica ’' Symphony 18 

 Beethoven ) ; ' ' Christmas " Oratorio ( Bach ) . 99 

 4 " FRIDAY , 11.30 , cagencsin ™ " eee 
 ; N SHIRE HALL : — te . ARTHUR W. PAYNE . 
 £ WEDNESDAY evening , 8 , Orchestral Works — Coleridge- |   Gramah’t Accompanist et oeRCY PITT . 
 e io Taylor Wagner ; Choral Ballad ( Rosalind F. Ellicott ) ; ' ' Golden 
 fo ( Sullivan ) . ain sic nacelle tain MR . ROBERT NEWMAN 
 eo mission : reserved seat , 15s . 1 . 6d . ; Serial ticket , £ 4 . 7 
 : 2 programme , ticket , & c. , apply Partridge Robins , SUNDAY afternoon CON cert , 
 3d . Westgate Street , Gloucester . conductor : Mr. HENRY J. WOOD , 
 | 3d , resume autumn . 
 : ROYAL ACADEMY MUSIC , QUEEN HALL ORCHESTRA . 
 4 TENTERDEN STREET , W. eonmcrneniae 
 3d . ; Principal Violin .. Mr. ARTHUR W. PAYNE . 
 6d , institute 1822 . incorporate Royal Charter , 1830 . _ organist accompanist Mr. PERCY PITT . 
 patron : Majesty QUEEN RoyAL FaMILy , LONDON MUSICAL FESTIVAL , 
 president : H.R.H. DuKE SAxE - CoBURG GOTHA . 8 
 — s , principal : Sir A. C. MAcKENz1E , Mus . Doc . 1099 . 
 management Mr. ROBERT NEWMAN . 
 Michaelmas term begin Monday , September 26 . entrance exami- — _ — — 
 nation therefor , Thursday , September 22 , 10 . QUEEN HALL : — 
 prospectus , entry Forms , information obtain | Monpay , 8 ' 4 " ' ae + » 3 8.30 p.m. 
 Secretary . . W. RENAUT , Secretary . TurEspay , Mayg . 3 8.30 p.m. 
 Wepnespay , 3 8.30 p.m. 
 ROYAL COLLEGE ORGANISTS . THURSDAY , : ee 3 8.30 p.m 
 4d . — Fripay , 12 3 8.30 p.m. 
 = Pi pe Loans , | - daily 10 a.m. 5 p.m. , Saruapay , 15 .. oe 
 4 uesday ursday 5 p.m. 7 p.m. Saturdays 
 # College open 10 1 o’clock . College close QUEEN HALL ORCHESTRA 103 
 July 30 August 3 . conduct Mr. HENRY J. WOOD 
 member desire practice College Organ obtain par- 
 3d ticular application . 
 3d . se sundry small room , hire concert , LAMOUREUX PARISIAN ORCHESTRA 100 
 meeting , & c J 
 : E. H. TURPIN , Hon . Secretary . conduct Mons . CHARLES LAMOUREUX 
 3d . Hart Street , Bloomsbury , W.C. 
 > : J ' J LOISTS 
 3d . GUILDHALL SCHOOL MUSIC . — EMINENT VOCAL ya itaiaeaaas 

 Karl Ludwig Klindworth bear 
 Hanover , September 25 , 1830 . father 
 clever engineer optician , lover 
 form art , player instrument , 
 good singer . young Karl self- 
 teach regard pianoforte . ata party 
 mother birthday play 

 overture " ' Caliph Bagdad " ( Boieldieu ) 
 year old . violin 
 instrument boyhood . study 
 careful master , attain con- 
 siderable degree virtuosity . play 
 concerto Rode , Molique , Vieuxtemps , 
 Fantasias Studies Paganini 
 Ernst , quartet great 
 master . passionately fond singe 
 ( sing ballad Léwe ) especially 
 play pianoforte score opera ; 
 pass day , morning till 
 night , engross occupation . Karl 
 acquainted music Wagner 
 atypically boyish way . 
 attract folio volume reason 
 unusual bulkiness ! ' ? " 
 enquire youthful curiosity . prove 
 ' Rienzi , ' , usual , eagerly 
 play study . " boy , " 
 Professor , ' fond score . 
 able library , copy 
 end — ' Der Freyschiitz , ' ' Don Juan , ' 
 ' Huguenots , ' Ninth Symphony , 
 quartet Beethoven , 
 work great advantage 
 life . ' leave violin ? ' 
 , want study Spohr ; 
 father large family , reverse 
 fortune time , compel apply 
 King August , Hanover , money help 
 . request refuse 

 OPERA conductor seventeen 

 life WEIMAR 

 companion Weimar , " pro- 
 fessor Klindworth , ' Peter Cornelius , 
 Ferdinand Laub , Bernhard Cossmann . 
 Liszt visitor include Wieniawskis , 
 Marx , Moscheles ; Ferdinand David 
 Brendel Leipzig ; Brahms , bring 
 sonata composition 
 Liszt , previous submit 
 Schumann ; Bettina von Arnim ( young 
 friend Goethe Beethoven ) 
 charming daughter ; Berlioz , visit 
 occasion Berlioz Festival , per- 
 formance place ' Benvenuto 
 Cellini , " ' Faust , " " Romeo Juliet . " 
 difficulty find sufficient 
 orchestral player , young fellow place 
 Liszt disposal — Hans von Bilow 
 play big drum , Dionys Pruckner 

 triangle , cymbal , obtain 
 high commendation infallibility 
 personage great Hecto 
 

 MR . J. W. DAVISON criticism 

 Professor Klindworth public 
 appearance London John Ella 
 " musical Winter Evenings . " 
 March 30 , 1854 , place Willis Rooms , 
 ' pupil Liszt " play Beethoven 
 sonata C ( op . 2 , . 3 ) Liszt 
 Fantasia Mendelssohn ' Midsummer 
 Night Dream . " ' young man , " 
 Ella previous programme , ' come 
 good testimonial Joachim , _ 
 master — hungarian magnate , Liszt , 
 Klindworth study year 
 Weimar . " Mr. J. W. Davison , great critic 
 day , particularly genial 
 reception accord pianist 
 english , criticise Herr Klindworth 
 performance musical World . ( 
 date paper , 1st April , 1854 , 
 note 

 new pianist , Herr Klindworth , pupil M. Liszt , 
 début occasion . play twice — 
 Beethoven Sonata C ( . 2 Haydn set ) , 
 fantasia M. Liszt , theme Mendelssohn 
 Wedding March Midsummer Night Dream . Hert 
 Klindworth evidently young , present talent 
 look promising . exhibit fault 
 master . beauty . thump 
 instrument right good , means exact 
 execution . mechanism , , defective ; 
 clearly ( clearly , 
 ) fantasia Liszt , 
 difficult , time incoherent 
 unmeaning bravura piece listen . 
 Liszt spite Mendelssohn 
 write . heavy tax 
 fantasia - maker , prevent mangle 
 caricature work great master 

 like Herr Klindworth reading Beethoven 
 Sonata . exaggerated expression incorrect 

 good point scale sixth , 

 effect Klindworth pianoforte score 
 ' Ring ' Bilow version score 
 ' Tristan , ' Tausig ' Meistersinger , 
 Joseph Rubinstein ' Parsifal . ' Klind- 
 worth version hard play 
 , efficient ; 

 Liszt transcription Weber overture , 
 Berlioz Beethoven symphony 
 concerto . Liszt transcription Klind- 
 worth model 

 wo nt express great admiration Biilow 
 ' Tristan , ' describe ' impossible 
 possible ' — , declare 
 effect pianoforte satis- 
 factory — ' Die Basse sind zu jung ' ( ' 
 bass thin ' ) . Klindworth 
 player left hand exceptionally hard work 
 , bass stand forth grandly . 
 Tausig method subtle , pianistic . 
 Tausig confess realise 
 Wagner tempo MS . score , 
 true speed virtuoso con- 
 trivance way . 
 thing , emphatically , 
 Joseph Rubinstein version score 
 ' Parsifal ' ' Siegfried Idyll , ' 

 reflect orchestra score closely 

 hear Klindworth play 7 
 public , 7 
 late Walter Bache performance 7 
 arrangement pianoforte © 
 private | 7 
 hear play , week week , 7 
 year , good deal Liszt , Beethoven , 7 
 Chopin , pianoforte 7 

 sitet 

 Episode de la vie d’un artiste ’' " Retour } Suites , " Schumann pianoforte work , § , , oe 

 la vie " symphony , overture , and|and Beethoven pianoforte sonata . Alg ies 

 Faust " ( twice ) ; Beethoven : Ninth Symphony| ' 24 grosse Etiiden , ' selection ane 

 time ) great mass d;|has Clementi ' " Gradus aq _ 

 receive benefit Professor Klindworth 
 editorial supervision . work 
 press shortly publish messr , F 
 Novello . perusal proof sheet 
 reveal critical insight character . 
 istic acumen naturally look 

 work , page evidence 
 wide experience teacher complete f ended 
 knowledge art pianoforte playing , dining 
 phrasing fingering receive minute | queer 
 attention , preface help demonstrate telegr 
 thoroughness Professor f " yo 
 discharge task . cause surprise f " y | 
 , case work Chopin , — s@ ™ 
 Klindworth edition edition | " th : 
 Mendelssohn " ' song word . " oa 
 LOVE ENGLAND . 
 " « week happy time , — 4 loc 
 enjoyable life . shall & place . 
 think deep gratitude ,   eavel 
 remembrance remain F shone 
 source great pleasure tome . " write f spark 
 Professor present writer immediately § curios 
 conclusion recent visit jp telegr 
 London . " shall come Ft " 
 autumn conduct Symphony concert § dour 
 , concert Frederick — mood 
 Dawson , " remark course conver : § 
 sation . " ' capitalist ' § 
 build cottage Hampstead Heath , ! 9 andt 
 live happily . " > ia 
 recital audience . [ 7 ~y 
 Professor Klindworth play public 9 holde 
 sojourn London leafy month / ) week . 
 June . ' strictly private " | ) , 
 pianoforte recital ( invitation ) day © nothi 
 leave . hour unconventional © frien : 
 8 a.m. : listener writer } ) enou , 
 biographical sketch . piece Liszt , © nong . 
 Chopin , Beethoven play © 
 great artist playthem . vigour , refinement , — deyic 
 accentuation , technical facility , self - effacement , — 
 , , true musicianship combine , co 
 rarely individual , — carm 
 ideal result . plea 
 felicitous appropriateness | « 
 Professor choice Beethoven sonata " Les " Bu 

 Adieux , l’Absence , et le Retour . " 

 MUSICAL TIMES.—Avucust 1 , 1898 

 day Herr van Rooy humorous 
 recital St. James Hall , Herr Mottl 
 magnificent reading 
 Prophet " Elijah ' Alexandra Palace . 
 Dr. Joachim embodiment 
 Siegfried lack youthful exuberance , 
 imitate " Bird Song " effectively 
 violin stage 
 reed . following day M. Jean de Reszke 
 reproduce incident recital 
 Mr. Leonard Borwick Beethoven 
 symphony , specially arrange 
 : M. Korderowski violin flute . 
 Mr. E. F. Jacques analytical programme 
 occasion edifying . M. Paderewski 
 specially engage 
 Loge ' Das Rheingold " account 
 hair , remarkably realistic effect 
 character . early season 
 M. Paderewski appearance title - rdle 
 " Orfeo " regard crowning 
 triumph Covent Garden Syndicate , 
 doubt pianoforte playing 
 secure passing safely Hades 
 - woode pasture Elysian 
 fields , efficiency lyre 
 hitherto open question . 
 descent cleverly manage . 
 stage manager Bayreuth _ 
 engage bring apparatus use 
 scene ' Das Rheingold , " 
 beautiful sight behold Paderewski 
 resonator float space , scale 
 arpeggio stream forth direction . 
 embodiment 
 long remember . Sir Henry Irving sing 
 exquisitely Romeo . Miss Ellen Terry 
 Briinnhilde surprise 
 season , Miss Marie Brema 
 occasion nearly clear stage 
 dauntless horsemanship bare backed 
 steed possess singularly wild eye . 
 generally know aim 
 Royal Opera Syndicate 
 educational . purpose Madame Melba 
 engage instruct audience 
 sing scale ; Madame Eames effect 
 emotion control constant desire 
 graceful ; Madame Calvé teach art 
 fascination ; Fraulein Ternina inculcate 
 appropriate expression ; Miss Adams _ 
 encourage youth ; M. Saléza expose 
 evil exaggeration . successful 
 artist season Covent Garden 
 Mr. Neil Forsyth , thought reading 
 interview display acuteness 
 marvellous . having gain possession 
 Wagner original tarnhelm , sudden dis- 
 appearance wonderful 
 celerity appear want , 
 restore diamond brooch disconsolate 
 lady , find wander stall private 
 box . perform night . fear 
 article clear comprehensive 
 , useless 

 mention reader remember , 
 sure forget 
 wish recollect , 
 logically chronicle season 
 complete . a. ¥ . Z 

 reader 
 holiday reach Dresden week 
 month , September , find follow 
 information ( kindly supply correspondent 
 Germany ) useful 

 date performance Wagner opera Dresden : 
 August 24 , ' Rienzi ' ; 27 , ' Der fliegende Hollander " ; 
 30 , * Tannhauser " ' ; September 2 , ' ' Lohengrin " ; 6 , ' Die 
 Meistersinger ' ; 8 , ' ' Tristan und Isolde " ; 13 , ' Rhein- 
 gold " ; 14 , " Die Walkie " ; 16 , ' Siegfried ' ; 20 , 
 " " G6tterdammerung . " intermediate evening 
 : Gluck " Iphigenie Aulis " " Iphigenie 
 Tauris " ; Beethoven ' ' Fidelio " ; Mozart " * Don Juan " ; 
 Méhul " Joseph Egypten " ; Berlioz ' ' Benvenuto 
 Cellini " ; Verdi ' " ' Amelia " — i.e. , ' Ballo Maschera " ; 
 Rossini ' ' Barber Seville . " price 
 usual — i.e. , 7 mark 1 mark , accord 
 position seat 

 Fresu proof attention bestow 
 worship - music nonconformist afford 
 recent issue " Bible Christian Sunday School 
 Hymnal " ( 26 , Paternoster Row , Novello 
 Company , Limited ) . hymnal , contain up- 
 ward 500 tune , commendable fix - tune 
 system , edit Mr. J. R. Griffiths 
 conspicuous success , editor having discharge 
 responsible duty sound judgment combine 
 knowledge practical requirement . 
 unusual feature , general 
 interest book , find 
 " alphabetical index tune , " contain 
 early know " appearance " tune 

 ’ Almaviva , Basilio , Bartolo respectively 

 probably thank Fraulein Ternina 7 
 performance season , gth ult . , Beethoven © 
 " Fidelio , " ~ 
 present fine embodiment devoted 

 wife Covent Garden stage 

 533 

 e 
 tility , doubtful work receive brilliant Henniker , Mr. Kennerly Rumford , M. Joseph Hollman . 
 1 sympathetic interpretation . Mozart Sonata A|Miss Hewitt style reflect measure 
 delight ( . 17 ) follow Tartini familiar sonata ' Il | teacher , gain great confidence 
 Catherine trillo del diavolo . " fine interpretation | command expression , playing 
 Beethoven sonata c minor ( Op . 30 , . 2 ) , | attractive present . acquire 
 ment climax evening | admirable /egato , advantageously 
 > artist performance Brahms beautiful composition g/| rendering Thomé ' " L’Extase , " fluency 
 2 ( Op . 78 ) , conclude selection interpret | rapid passage display Hubay ' Echo des 
 rare keenness perception spirit music | Alpes " Wieniawski " ' Air Russe 

 consummate executive ability 

 asic command keyboard entitle rank | Wagner work , " ' chip 

 m sing | - musical prodigy - day . | wagnerian block " ; Verdi opera 
 ira , | hear Presto Bach " italian " Concerto , | happy choice Jago Credo ' Otello . " 
 art wert ; Schubert Impromptu e flat ( op . 90 , . 2 ) , Chopin | Beethoven ' ' Adelaide , " modern french song , 
 bert , q Impromptu flat , piece , | pianoforte duet , 

 play marvellous executive fluency taste vivaciously Mdiles . Douste de Fortis , 
 ich tender year . violin piece contribute | include programme 

 MUSIC LIVERPOOL . 
 ( correspondent 

 definite official pronouncement 
 regard approach season , 
 come event cast shadow . 
 Philharmonic Society contemplate production Verdi 
 ' Stabat Mater ' ? Te Deum , Beethoven ' " * ruin 
 Athens , " Handel ' Israel Egypt " 
 Christmas , Cowen ' ' Water Lily " ? Rossini 
 " * Stabat Mater " talk respectively ninth 
 twelfth concert . Mr. F. H. Cowen , course , retain 
 position conductor , Mr. H. A. Branscombe 
 chorus - master 

 new chief find Musical Society 
 person Mr. F. H. Crossley , excellent 
 work year past conductor choral 
 society Warrington , Runcorn , Newton - le- Willows . 
 information hand regard programme 
 coming season , , judge Mr. Crossley 
 achieve , good look 

 voice musical ability , thoroughly com . 
 petent chorister admit , receive 
 definite emolument service , choral 
 concert devote Mendelssohn " Elijah " 
 act Wagner ' Tannhauser 

 Mr. Granville Bantock Capital 
 concert Tower , New Brighton , time 
 writing midway cycle Beethoven 
 Symphonies , commence Sunday , 3rd ult , 
 continue weekly . adjacent 
 place recreation , way , Sunday con . 
 cert North England — viz . , New 
 Brighton Palace — exploit 1883 ; 
 crush existence local authority 
 accord license lineal descendant , 
 progress 
 district question , albeit movement somewhat 
 nature Adagio 

 MUSIC MANCHESTER . 
 ( correspondent 

 Leo Smith ( Mr. Fuchs ) , Vieuxtemps Concerto E 
 young Arthur Catterall , highly gifted pupil Mr. 
 Brodsky ; Saint - Saéns Concerto G minor , play 

 Helen Brown , open movement Beethoven 

 Concerto b flat young Edward Isaacs , pupil 
 Miss Olga Néruda ; performance Mr. Dayas 
 student , Irene Schaefsburg ( d’Albert Concerto E ) , 
 Edith Webster ( Chopin Concerto e minor ) , Muriel 
 Blydt ( Weber - Tausig ' invitation dance " ' ) , 
 Mary Spencer ( Bach fugue g sharp minor ) . 
 singer Ruby Keighley Milly Jones greatly 
 advance Mrs. Hutchinson care 

 MUSIC NOTTINGHAM . 
 ( correspondent 

 Tue Sacred Harmonic Society issue prospectus 
 - season . importance far exceed 
 previous effort afford pleasing contrast 
 dark time , happily long past , fund 
 low , supporter , imminent collapse continuously 
 threaten Society . committee 
 secure service Mr. Henry J. Wood conductor , 
 include scheme orchestral concert , 
 intention encourage formation strong 
 local orchestra . Mr. Wood conductor , 
 orchestra promising beginning . Midland 
 Orchestral Union , pluckily venture 
 season ago Mr. Allen risk , 
 partly reveal artistic possibility 
 undertaking ; certain , moderate 
 financial backing , good band soon firmly establish 
 populous district . concert announce 
 recital Gounod opera " ' Irene , " November g , 
 orchestral concert , December 8 ( 
 glad notice engagement Miss Cantelo 
 solo pianist Beethoven ' ' Emperor " Concerto ) . " 
 Messiah " Boxing Day ; Sullivan ' Martyr 
 Antioch " February 9 ; second orchestral concert 
 1 March 2 ; Mendelssohn " Elijah , " 
 Madame Ella Russell , Miss Hilda Wilson , Messrs. William 
 Green Andrew Black soloist , close season 
 March 23 . spirited policy recent year , 
 bring Society present strong position , 
 greatly assist reserve fund £ 1,000 
 establish alegacy estate late Mr. Alsop 

 MUSIC PARIS . 
 ( correspondent 

 Miss Maup AGNes WInTER concert Queen 
 ( Small ) Hall , 12th ult . , justify hope 
 professor Trinity College , London , private 
 friend . 
 exceptional command technique youthful executant 
 perception varied spirit pervade work 
 interpret . play correctness fervour 

 Beethoven " Sonata Appassionata , " opening Allegro 

 specially render ; Schumann " Papillons 

 549 

 solo Miss Winter warmly compli- 
 mente . Messrs. L. Szczepanowski Hans 
 Brousil ( respectively violin violoncello ) contribute 
 share excellent rendering Beethoven 
 Trio C minor . vocalist Madame Zippora 
 Monteith 

 Tue Alexandra Palace choir display fitness 
 interpretation standard sacred composition 
 23rd ult . , ' " hymn praise ' ? successfully 
 perform , baton Mr. Henry J. Baker . 
 chorus sing spirit impulse , 
 rendering orchestration generally meritorious . 
 acceptable work precede overture 
 " Athalie " Mr. W. Augustus Barratt cantata 
 " Lancelot Elaine . " , previously per- 
 form London , effective point 
 specially striking character . composer receive 
 justice engage . Miss Marie Elba Mr. 
 William Higley respectively soprano baritone 
 solo expressively , whilst execution choral 
 instrumental feature adequate 

 BarcELonA.—The performance Berlioz 
 Requiem place 8th ult . , Palace Fine 
 Arts , able conductorship Sefior Nicolau . 
 grand work — choice , prevail 
 circumstance country , appropriate 
 — create profound impression numerous 
 audience 

 Ber.in.—After successful performance 
 " ' Nibelungen " cycle , Royal Opera close door 
 beginning month , great lead 
 member depart enjoy earn rest , 
 remain support personnel New Royal Opera 
 House ( Kroll ) , performance opera 
 carry summer month crowded 
 audience . Madame Arnoldson continue 
 great attraction favourite , M. Lasalle , 
 famous Paris baritone , add appearance 
 originally contemplate . West - end 
 Theatre Mdlle . Prevosti draw house 
 ' * Carmen , ' " ' Trovatore , " ' Il Barbiere , " 
 Frau Sedlmair , appearance 
 Fidelio Norma create enthusiasm . 
 theatre entirely devote opera future , anda 
 number interesting work , old new , promise 
 coming Autumn season.——A new concert hall 
 shortly open , intend chiefly orchestral 
 chamber concert . ' ' Beethoven Saal " 
 contain thousand seats.——The new 
 catalogue publish Herr Liep- 
 mannsohn , antiquarian bookseller , devote exclusively 
 Wagner , Liszt , Berlioz , contain 483 number , 
 379 appertain wagnerian literature . — — 
 M. Jacques van Lier , lead violoncellist 
 Philharmonic orchestra , appoint professor- 
 ship Klindworth - Scharwenka Conservatorium , 
 conduct chamber music class 

 CARLSBAD.—Under auspex Carlsbad Musik- 
 verein , a2 commemorative tablet place 
 house , Hirschensprung Gasse , Johannes 
 Brahms reside sojourn 1896 . 
 ceremony unveiling place roth ult . , 
 include musical performance Cur Orchestra 
 local choral society , able address 
 Musik - director Alois Janetschek 

 MiLan . — Umberto Giordano , successful composer 
 " André Chénier , ' ? complete score new 
 operatic work , ' ' Fedora , " libretto found well- 
 know french drama , bring 
 Teatro Lirico , Milan , October . Signora Gemma 
 Bellincioni young tenor , Signor Caruso , sustain 
 principal . — — owe activity display 
 influential syndicate amateur , ample 
 subscription forthcoming ensure 
 resumption operatic performance La Scala 
 coming season . circumstance , Municipal 
 authority , , likewise consent renew 
 annual subvention £ 6,000 . Signor Gatti - Casazza , 
 manager Municipal Theatre , Ferrara , 
 appoint director La Scala 

 Municu.—The thirtieth anniversary memorable 
 performance — Wagner exquisite musical comedy 
 ' * Die Meistersinger ’' — celebrate , June 21 , 
 Royal Opera excellent special performance 
 work , Court - Capellmeister Fischer direction , 
 Fraulein Hofman , Herren Bertram , Mikorey , 
 Friedrichs principal . occasion original 
 production master superintend 
 rehearsal , Dr. von Biilow conductor Dr. 
 Richter director chorus , enthusiastic 
 reception accord work great 
 majority present amend 
 unworthy intrigue year previously cause 
 Wagner residence bavarian capital . 
 regard original interpreter , , Herren Bausewein 
 Schlosser , active member Munich opera , 
 Herr Nachbaur , " create " Walther 
 von Stolzing , remain attached institution 
 honorary member . Herr Betz , Berlin , Hans 
 Sachs , present Prince Regent 
 gold medal Arts Sciences honour notable 
 anniversary.——the new - act comic opera ' Zinober , " ' 
 Herr Siegmund von Hausegger , bring 
 June 19 , Royal Theatre , Herr Richard 
 Strauss direction , obtain complete success , 
 excellent interpretation contribute share . 
 libretto , pen composer , found 
 Hoffmann fanciful story , effectively 
 dramatise , music work gifted 
 frequently highly original composer . Herr von Hausegger 
 - fifth year , native Graz , — _ — 
 annually recur performance Beethoven 
 symphony Kaim orchestra announce 
 commence 22nd ult . , Professor Loewe 
 direction 

 PRAGUE.—A new operetta , ' ' Der Opernball , " Herr 
 Richard Heuberger , esteemed viennese musical critic 
 composer , bring German Theatre 
 , June 25 , great success . libretto 
 operatic version - know comedy ' Pink 
 Dominoes 

 native Taunton , year organist 
 > Majesty private Chapel Osborne , composer 
 " Osborne " March , write 1887 Queen 
 Jubilee frequently perform Queen command . 
 © _ thedeath announce , June 24 , Leipzig , 
 : live retirement year past , HEINRICH 
 ~ Wotrr , doyen german violinist , age eighty- 
 arrive Vienna concert 

 surprised find city mourning : 
 | day Beethoven funeral ! Wolff subsequently 

 acquaintance Paganini , fre- 
 quently associate performance chamber music . 
 treasured reminiscence Wolff artistic 
 career friendship form Mendelssohn , 
 try number com- 
 position manuscript , quintet 
 — viola ) , Mendelssohn play second 
 viola 

 arrange 

 Slow Movement ( Pianoforte Quintet ) 8 . d , 
 " chumann G. C. Martin 3 . 24 . ' oe con saat < 4 > " * . Arthur B. Plant 2 5 
 Minuet ( menuet Orchestra ) ets 
 Beethoven 25 . " aie va ea St. Cecilia Macpherson 1 § 
 Andante ( Pianoforte Sonata , B ... ih é " - es Handel 
 chuber ' : arghetto ymp ) ony ) 
 = 0 aha bgt Handel G.C. Martin 1 6 ) 26 , { 8 tit 4 W. Marchant 1 9 
 s pants hart " Calvary pohr Warum ? .. - rao 
 Agnus Dei(MassinG ) .. Schubert al { liebeslied os } , W. Marchant 1 9 
 ( os ture ( " Acis Galatea ' ' ) Handel G.C. Martin 1 0 ad : Ss Oo 6 , 
 verture cis oe » \. tet 

 Albumblatter ( . 1 , op.g9 ) Schumann } 28 . { " n = . eeu ue , ta ya A. W. Marchant 1 9 

 Adagio(Sonata , op.2 , no.1 ) Beethoven 

 4 { theCater Fugue e ' jel G.C. Martin 1 6 x , { introduction Fugue ine fat iam A. W. Marchant 1 0 

 11 , Adagio b minor Mozart A.W. Marchant 1 o jae " Lord , thee heart profier ° 1d. ee 

 12 , Adagio ( Sextet , op.81 ) .. Beethoven .. A.B. Plant 1 0| 4 % Matthew Passion 

 13 . Elysium ( ' Orphée " ) .. Gluck .. . yond t Ol gr , { Andante con moto ( Symphony inpfag ) W. Cruickshank 1 6 

 book 
 Marche Funébre . oe 
 Moderato ( Quintet , Op . 18 ) . 
 Adagio ( Sextet , Op , 111 

 oe Bach . 
 + » Spohr . 
 Beethoven . 
 oo   aminck , 
 Schumann . , 
 luck . 
 Mendelssohn . 
 eo Bach . 
 Beethoven 

 Mendelssohn 

 Beethoven 

 Mendelssohn . 
 Mendelssohn 

 Kalliwoda . 
 Beethoven . 
 Kulak 

 Beethoven . 
 Mendelssohn . 
 Mendelssohn 

 celebrate marche 

 content 

 BEETHOVEN.—March ' " ' Egmont . " 
 BEETHOVEN.—Funeral March . 
 CHOPIN.—Funeral March ( op . 35 ) . 
 HANDEL,—Dead March " Samson . " 
 HANDEL.—Dead March " Saul . " 
 HANDEL.—March " Scipio . " 
 MENDELSSOHN.—Cornelius March ( op . 108 ) . 
 MENDELSSOHN.—Wedding March . 
 MENDELSSOHN.—War March . 
 MENDELSSOHN.—Funeral March ( op . 103 ) . 
 MEYERBEER.—March " Le Prophéte . " 
 MOZART.—March " Idomeneo . " 
 SCHUBERT.—March b minor ( op . 27 ) . 
 SCHUBERT.—Marche Solennele . 
 SCHUBERT.—Grand March 

 Price shilling Sixpence net 

 compose 

 L. VAN BEETHOVEN 

 
 
 t 

 NOVELLO , EWER CO . , NEW YORK 

 book 1 , | book 7 . 
 Good Shepherd . . ee ) , Myles B. Foster | 2 + impromptu .. John E. West 
 W. Warder Harvey ! 2 : Minuet , Pianoforte Sonata ( Op . 10 , * . ) .. Beethoven 
 Andante " se . " . Warder Harvey 
 Andante con moto .. ' 3 o « oo Moe Gaus ; 3 introductory ay se ee S. J. Rowton 
 Christmas Bells ... é oo oo G. J. Bivey| 4 arch .. . . * . Oliver O. Brooksbank 
 Minuet ae " ' Philip Hayes | 5 : Sunday song --Max Oesten 
 Judex , " Mors et Vita " sy tae * ghee ee ounod| 6 . Minuet Trio , n Quartet ( Op . 9 , . ) .. Haydn 
 Soft Voluntary é ite H. A. Harding | 7 Pastorale wa ' " ia clAlfred Ww . Youlen 
 , slow March .. ne ae ue ae Cunningham Woods | 8 Religioso . + . T.L. Southgate 
 ASongof Praise .. .. « 2 oe J. Stainer | book 8 
 , Andante G minor « e ee 3 ] ELH : Fellowes | , 
 | 1 . elegy + » « CC . H. Lloyd 
 | 2 po . 6 ( Piéces de Clavecin ) . Couperin 
 book 2 | > aa . " ee ee ee oe Frederick A. 
 - ae « ee eo ee s Tozer 
 ; ow oe ee - F. Cota | | 5 . Allegretto Pastorale .. W. John Reynolds 
 ee oo ee " w. Warde ; mene | | 6 . ee F , String Quartet D1 iapaenl ™ Sane er 
 aa 7 . meditation . e . Wolstenholme 
 Foaleding Voluntary " s oe ee A. R. Gaul | 8 finale , Pianoforte Trio ( Op . ' 88 ) oe ee - . Schumann 
 ee Voluntary 22 62 eae : 2 teen | @ Camis aa 4c + » « + « + Battison Haynes 
 . Tempo di Minuetto . oe yles B. Foster 
 po sad con moto , Quartet D minor . | BOOK 9 . 
 agio " es 7 William Sterndale Bennett | . Larghetto ‘ ie ry .. F. Cunningh 
 ; naalanery Voluntary oe au « - Daniel McIntyre | ct el March .. pa ee ayy hey 
 | 3 . = man , thing ( " Lobgesang " * ) ae ae eon 
 book 3 . | 4 . Allegro poco maestoso ee ee ee ee . G. Cusins 
 | 5 . communion .. oe oe ee ee ee _ Alfred R. Gaul 
 . Andante Tranquillo .. 4d ae 3 C. H. Lloyd| 6 . Andantecon moto .. « - + . + » John Francis Barnett 
 | Village March . Ferris Tozer | 7 : Andante Religioso .. .. « + « « Alfred W. Tomlyn 
 . romance , Serenade Strings ( . 0 f .. Mozart | 8 . evensong + + « « Cuthbert Harris 
 Gavotte , " Semele " ... 3 Handel | 9- minuet , quartet Gminor 2 ... ) Schubert 
 . evening Prayer . * " alfred W. Tomlyn | 10 . melody ina .. oe ef ef ce eo W.H. Callcott 
 . Heaven earth display ( Athalie Py ee Mendelssohn BOOK 10 . 
 1 . Allegro moderato .. ae ee ee .. E. Bunnett 
 BOOK 4 . 2 . Opening voluntary . oe Ferris Tozer 
 . meditation .. ve oe ia - . Battison Haynes 3 . watch Tam , ( ' Choral 
 . allegro moderato .. W. John Reynolds wee son Xi = oe Gounod 
 . Funeral March , Pianoforte Quintet ( Op . 44 ) _ .. Schumann | 4 ° Slow Air , Suite de Pieces es zp l ed 
 . conclude Voluntary xe es ae . » Cuthbert Harris z Allegretto Dextarate = J. Campbell 
 sag od aaa Sey er ke Osis C. gyi 7 . Allegretto Grazioso , Movement ath 
 » solemn Marc ne oo eo " oe ee oy Pianoforte Sonata Mozart 
 8 . Hallelujah Chorus , " ' Messiah " < .   ° : Handel 
 book 5 
 book 11 . 
 Agnus Dei ... + » « + f , Cunningham Woods | 1 , Pastorale .- _ Battison Haynes 
 Minuet , 9th Pianoforte Sonata .. ..   « e Mozart | 2 . Gavotte , 12th sonata 2 violin Cello Boyce 
 . Jerusalem Ceelestis ( ' Mors et Vita " ) .. ee ee Gounod | 3 , Evensong Kate Boundy 
 Andante Grazioso .. « swe . Kate Boundy | 4 , Minuet , Organ Concerto B fat ( . % 
 . — _ con brio oe ee ee ee " Frederick A. ee Set ) ae pe se : Handel 
 ommunion .. ° u — olme } « , ‘ aia aa " Oli 
 . Allegro , Finale gth Pianoforte Trio ee aydn ; mg ( op . 72 . 2 ): ys iver 
 conclude Voluntar ee ° ' Cuthbert Hans E 2 
 g y + oe 7 . Communion .. + " uw . ' Wetuahelon 
 8 . prelude e minor chorale .. ee ° « - J.S. Bach 
 book 6 . g. Andanteconmoto .. .. « « G. A. Macfarren 
 » OSalutaris Hostia .. - » Myles B. Foster book 12 . 
 » slow March , 4th sonata . ee os oe Boyce| 1 . Berceuse ( op . 77 , no.3 ) .. ee - Alexandre Guilmant 
 » o great depth , " St. Paul " . oo Mendelssohn 2 . Introductory Voluntary .. « ' aa - » Hamilton Clarke 
 . penowel ema oe oe ee ee » . J. Warriner } 3 . ae ad ar ee oe ee ee c Bruce Steane 
 » Largo , ' ' Xerxes " P Handel | 4 . elody ee oe ee ee ee * S. Colerid e - Taylor 
 forsake , Duet " judgment " . " Spohr } 5 . eventide ee ee ee ee eo ee Clowes Bayley 
 , Allegro moderato ee p os os Ww . Warder pr 6 . postlude ee ae " " e Josiah Booth 
 inuet oe ee oe ee ee oe Samuel Ould 7 . Jubilant March . ee ee ve W. John Reynolds 
 continue , 
 Lonpon : NOVELLO COMPANY , Limitep 

 572 MUSICAL TIMES.—Avcust 1 , 1898 

