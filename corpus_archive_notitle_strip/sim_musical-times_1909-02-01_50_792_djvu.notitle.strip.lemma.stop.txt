remain visit _ briefly 
 notice . 1844 conduct 
 concert Philharmonic Society , _ 
 ' St. Paul , ' Sacred Harmonic Society . 
 bring Schubert great Symphony C , 
 Philharmonic band treat glorious 
 work coldness — ' insultingly 

 withdraw performance , 
 Mendelssohn annoyance . introduce Bach 
 suite ) , Beethoven ' Leonora ' overture . 1 , 
 ' ruin Athens ' music , Schubert 
 ' Fierrabras ' overture , music 
 ' Midsummer Night Dream ' ( complete ) , include 
 Wedding March 

 ' Elijah ' year , 1846 , 
 England production great oratorio 
 Birmingham musical festival . story 
 great event life - know 
 need - telling . following spring pay 
 tenth visit England . conduct 
 performance revise version ' Elijah ' 
 Sacred Harmonic Society , addition 
 performance Birmingham Manchester . 
 conduct Philharmonic play , 
 wonderful fire poetic insight , Beethoven 
 G major Pianoforte concerto-—his old cheval de 
 bataille , — Queen Victoria 
 Jenny Lind enthusiastic audience 

 late — — 
 leave Queen Prince Consort , 
 Queen Mendelssohn discuss , 
 nursery Palace , respective child 
 present King , aged , 
 doubtless attract attention 
 distinguished composer . leave London 
 8 , wear mental physical fatigue . 
 month music - love soul 
 Felix Mendelssohn calm death 

 free admission Sunday 
 concert London , world 
 Bremen possible hear - rate 
 symphony concert renowned 
 german conductor , Professor Panzner , 

 Pfennigs — threepence - halfpenny ? rhis _ 
 local Goethe Society offer work 
 man poor class government official 
 civil servant , 30 Pfennigs include free use 
 cloakroom programme explanatory 
 note ! concert annually 
 Philharmonic orchestra good soloist , 
 audience 2,000 people listen rapt attention 
 classic modern masterpiece form 
 excellent programme . Beethoven choral Symphony 

 é . ' 1 perform 

 Belgian Academy Fine Arts elect 
 Richard Strauss member place late 
 N. Rimsky - Korsakoff 

 musical critic newspaper South Africa 
 opinion open movement Beethoven 
 Pianoforte sonata C sharp minor ' certain 
 unpopular length . ' 
 try , find trying 

 MENDELSSOHN ORATORIO ‘ ST . PAUL . 
 SiR GEORGE GROVE , C.B 

 far list ( imperfect good ) piece 
 sacrifice Mendelssohn hear oratorio 
 perform . doubtless autograph score 
 reveal change , certainly 
 addition balance erasure 

 St. Paul ’ deeply interesting , 
 merit landmark Mendelssohn 
 artistic life . earnestness , divination 
 character , deep religious feeling , spirit , nobility , 
 diverse musical treatment ; 
 style composer acquire time 
 write ' Lobgesang . ' respect , 
 parallel pardon , work compare 
 somewhat ' Mount Olives ' Beethoven 
 career 

 word music 

 year concert Mr. L. 
 Malsch — unknown good orchestra 
 - day — ' Music Master Royal 
 Highness Military Band . ' concert , 
 Old Ship inn , programme comprise 

 overture Mozart ' Zauber flute , ' Beethoven 
 Symphony , grand chorus ' creation , ' Violin concerto 
 Spohr , concerted piece bassoon , horn 
 clarinet , glee vocalist Chichester Cathedral , 
 Handel ' hush , ye pretty warbling choir , ' 
 boy , Pianoforte concerto , , petite 
 piece pandean pipe 

 Beethoven symphony Brighton 1808 ! 
 tell 

 concert elegantly attend . act 
 concert commence o’clock , 
 finish midnight . concert , violin 
 strike lively air , merry dance animation 
 pleasure votary , till nearly o’clock 
 morning 

 Society orchestra , number 
 - performer , choral membership 
 stand 200 voice . organist Mr. Percy 
 | Taylor , son conductor ; Mr. W. A. Baker 
 leader band , Mr. W. T. Ashby 
 |enthusiastically discharge duty honorary 
 secretary . memorable achievement Society 

 performance December 7 , 1881 , vocal 
 portion Beethoven Choral Symphony . 
 place Aquarium , form excellent 
 | concert conduct Mr. Frederick Corder 
 time ( 1880 - 82 ) ably true artistic 
 success hold office director music 
 Brighton Aquarium . noteworthy achievement 
 | Mr. Corder performance Beethoven 
 symphony , include . 9 , mention 

 Mr. Robert Taylor , conductor Brighton 
 | Sacred Harmonic Society , native Evesham , 
 | town twofold interest scene 
 | celebrate battle , place Clementi die . 
 parish church St. Lawrence , Mr. Taylor 
 play , age , choral serv ice hold 
 church Reformation . 
 chorister Worcester Cathedral , , 
 articled pupil Dr. , organist , 
 assistant - organist . 1866 settle Brighton , 
 appoint organist St. Patrick , Hove , church 
 fame musical service . commence 
 duty , Mr. Taylor change Gregorian 
 anglican type service , successor 
 carry good work Mr. E. H. Thorne , 
 Dr. F. E. Gladstone , late Dr. F. J. Sawyer . 
 1870 1889 Mr. Taylor organist 
 Brighton College , year , addition 
 church work , hold office 
 choirmaster St. Michael Church . inspiring 
 service hold Dome Easter Sundays 
 connection Volunteer review hold 
 Brighton , charge entire musical 
 arrangement , preside organ ; 
 occasion fine tune St. Patrick , ' o worship 
 King , ' sing thrilling effect ac compani- 
 ment mass Volunteer band organ . 
 conjunction Dr. Alfred King — respected 
 musician come town 1865 — Mr. Taylor 
 start 1883 Brighton School Music . 
 student , number 250 , receive instruction 

 echo , 
 | second 

 generally suppose Beethoven 

 occasion Mr 

 concert large Lecture Theatre December 13 , 

 direction principal , Mr. Granville Bantock . 
 executive Midland Institute Orchestra , augment 
 teacher instrumental department , lead 
 Mr. Max Mossel . fine performance realise 
 Schubert ' unfinished ' Symphony Mr. Bantock 
 orchestral variation Bach cantata ' Wachet auf , ' 
 score string horn , represent 
 choral portion . Mr. Max Mossel fine reading 
 Beethoven Violin concerto , evoke great 
 enthusiasm 

 Birmingham Symphony 

 correspondent 

 sixth Messrs. Paterson Orchestral Concerts , 
 McEwan Hall December 21 , Mr. Fritz 
 Steinbach conduct fine performance Beethoven fifth 
 ( c minor ) symphony , Mendelssohn ' hebride ' overture , 
 Mr. Steinbach arrangement dance Mozart . 
 Mr. John Petrie Dunn , native Edinburgh Bucher 
 Scholar University , win cordial applause 
 playing Schumann Pianoforte concerto group 
 Chopin piece 

 seventh concert , December 28 , conduct 
 Dr. Cowen , programme contain Mozart ' Don 
 Giovanni ' overture , V. d’Indy symphonic legend , ' La 
 forét enchantée , ' Beethoven fourth Symphony , 
 Edward German tarantella , ' Winter , ' suite ' 
 Seasons . ' soloist Mr. Fritz Kreisler , create 
 great enthusiasin splendid performance Lalo 
 * Symphonie Espagnole ' ' introduction Scherzo ' 
 composition , violin 

 conductor eighth concert , January i1 , 

 Mr. Emil Mlynarski . principal item programme 
 Kalinnikoff Symphony , work perform 
 Beethoven ' Egmont ' overture , prelude 
 * Parsifal , ' scherzo Symphony D minor 
 Stojowski , overture Goldmark ' Sakuntala 

 Miss Muriel Kerr - Brown annual pianoforte 
 recital Freemasons ’ Hall January 15 , 
 programme comprise composition Bach , Schumann , 
 Chopin , Paganini - Liszt , Moszkowski , 
 pianist ordinary attainment 

 performance ' Messiah ' McEwan 
 Hall December 26 Mr. Moonie Choir , 
 Music Hall New Year Day Choral 
 ( conductor , Mr. T. H. Collinson 

 ninth concert January 18 , conduct Dr. 
 Cowen , M. Paderewski magnificent rendering 
 Beethoven Pianoforte concerto e flat , play 
 group Chopin piece Liszt Rhapsody , 
 interpret inimitable style . purely 
 orchestral piece MacCunn ' Land mountain 
 flood ' overture , ' Petite suite ' Debussy , 
 symphonic suite , ' Scheherazade , ' Rimsky - Korsakov 

 second Chamber Concert Freemasons ’ Hall , 
 January 16 , Mr. Denhof assist Professor Carl 
 Halir ( violin ) , Professor Julius Klengel ( violoncello ) , 
 Mr. Horatio Connell ( vocalist ) . trio perform 
 Schubert b flat major Mendelssohn d minor . 
 Professor Halir Professor Klengel contribute 
 solo , Mr. Connell sing song Beethoven , Handel , 
 Brahms 

 Miss Agnes Copeland , Edinburgh lady , assist 
 Miss Margurite Bruel pianoforte , violin recital 
 Freemasons ’ Hall January 20 . programme 
 ambitious nature , comprise Beethoven 
 sonata G ( Op . 30 ) , Wieniawski Concerto D minor , 
 piece Mozart , Guiraud , Debussy , Vieuxtemps 
 Paganini . Miss Copeland technical equipment excellent 
 play great verve brilliancy , great 
 experience good headway art 

 Messrs. Challen , old - establish we!l - know 

 associate Scottish Orchestra , Pollokshields 
 Philharmonic Society concert performance Verdi 
 ' Ernani ' January 7 . occasionally overweighte 
 accompaniment , choral portion work 
 sing great vim crispness , solo music 
 [ entrust experienced exponent Miss Lizzie 
 | Burgess , Messrs. John McCormack , Charles Victor 
 | Lewys James . Mr. John Cullen direct performance 
 | conspicuous ability . ninth Classical Concert , 
 | January 5 , programme include remarkably fine 
 | rendering Max Bruch ' scottish fantasia ' violin 
 | orchestra , Mr. Henri Verbrugghen soloist , Mozart 
 | ' Jupiter ' symphony , . 2 , 4 , 5 7 Elgar 
 | suite ' wand Youth , ' - 
 time . January 12 , Mr. Emil Mlynarski 
 replace Dr. Cowen , prove orchestral 
 | conductor great distinction . composition 
 |the conductor fellow - countryman — Kalinnikofi _ 

 symphony ( G minor ) scherzo Stojowski 
 symphony D minor — bring hearing , 
 , Beethoven familiar ' Egmont ' 
 overture , playing Scottish Orchestra reach 
 high level . Choral Union ' popular ' 
 performance ' Messiah ' City Hall January 14 , 
 , Dr. Coward inspiring direction , achieve 
 distinct success 

 outstanding event present concert 
 season appearance Paderewski eleventh 
 Classical Concert , January 19 . sufficient 
 famous pianist good form , strikingly 
 powerful reading Beethoven fifth Concerto . purely 
 orchestral programme include performance 
 Petite suite Debussy , Rimsky - Korsakoff symphonic 
 suite ' Scheherazade , ' , way contrast , Bach 

 familiar Bourrée string , b minor 

 sane Beidler conduct . Lady Hallé play Spohr Concerto 

 . 8 , Beethoven Romanza F. orchestral 

 MUSIC LIVERPOOL piece Bach Overture ( suite ) b minor , flute 

 PRER Ses RRESPONDENT . ) ' Eroica ' Symphony , Funeral March 

 closely follow successful performance Mendels- | movement slow pace . follow 
 sohn ' Elijah ' Welsh Choral Union December 19 , | concert , January 14 , conductor orchestra greatly 
 Philharmonic Hall fill December 22 , | distinguish rendering ' Leonora ' 
 occasion performance work } . 2 Overture , ' Siegfried ' Trauermarsch , 
 Philharmonic Society . Mr. Herbert Brown sing | Tchaikovsky Symphony . 4 , f minor Master 
 Prophet music excellent effect , principal | Ernst Lengyel play Beethoven pianoforte Concerto . 4 , 
 Miss Perceval Allen , Miss Edna Thornton , Mr. Lloyd | G , Schumann ' Etudes symphoniques 

 Chandos Master J. Baines , Pro - Cathedral Choir . sign choral music come 
 Dr. Cowen guidance , choir band render | rightful place musical appreciation 
 familiar music fine effect . Seventh Philharmonic | Gentlemen Concerts January 11 , Haydn 
 concert January 12 , César Franck symphonic poem , | ' creation ' perform , Mr. Beidler conduct . 
 ' Le Chasseur Maudit ' perform time | principal Madame Esta D’Argo , Mr. Webster Millar , 
 . descriptive ballad Burger , skilful and| Mr. Charles Clark . year 
 vivid musical illustration reach high plane | oratorio programme concert . 
 f music description . Miss Evelyn Suart play | choir large share success performance . 
 Tchaikovsky Pianoforte concerto great skill , | situation music admirably express 
 Dr. Cowen conduct fine band expressive | connection trio , ' heaven tell , ' 
 performance Mozart G minor Symphony . |in duo commence ' thee bliss . ' 

 n ona finnish air pianoforte violoncello 

 Beethoven sonata D ( Op . 102 , . 2 ) , 

 Mr. Horatio Connell admirably sing 

 recital native county , Miss Beatrice 
 yn September 18 , prove conscientious 
 young artist great promise , Chopin playing | 
 especially good Mr. A. Tait Knight assist vocally . | 
 | Glee Singers concert Exeter 
 December 11 . Exeter Orchestral Society , conduct 
 Dr. Wood , perform , December 15 , ambitious | 
 progra good result . number 
 ' Pelleas Mélisande ' suite Sibelius egyptian 
 dance Dr. H. A. Harding novelty . Miss 
 Bartlett vocalist 

 Torquay Musical Association , November 26 , 
 programme evenly divided usual 
 instrumental choral section . Schumann 
 ' rhenish ' symphony play time 
 West . Stanford ' revenge ' part- } 
 song choir balance tone - quality | 
 average . Mr. T. H. Webb conduct , Mr. H. Crocker 
 lead band . Haydn String Quartet ( Messrs. H. FE . 
 Crocker , J. Stevens , F. Crocker C. T. Heaviside ) , 
 November 5 , play quartet Brahms ( op . 51 , . 1 ) | 
 Beethoven ( op . 18 , . 2 ) . congratulation 
 player successful effort promote | 
 chamber music Torquay . Barnstaple Musical 
 Festival Society miscellaneous concert November 
 23 , Dr. H. J. Edwards appear conductor 
 pianist . suggestion , approach 
 practicability , revival North Devon Festival 
 abeyance year 

 December 30 , Exmouth Choral Society fine 
 rendering ' King Olaf , ' Miss Mary Wilmot , Mr. 
 Charles Saunders Mr. Leslie Wilmot principal , 
 Mr. Raymond Wilmot conduct 

 January 6 include Haydn ' Bear ' Symphony , 

 concert , January 20 , introduce Beethoven C 

 principal Misses | 
 Messrs. Foster 

 f Royal Orchestra , Richard Strauss , 

 Beethoven rarely play work , 

 rth Symphony , overture ' King Stephen , ' 

 NN 

 Beethoven - House Society decide hold | 
 great chamber music festival , 
 © mark preliminary expense 

 Concert , Prof. 
 Shakespearean Comedy , ' 
 g , produce . prove merry 

 famous 

 cilia concert Corea Ha 
 conduct — — Prof. ' Karl 
 Panzner , Bremen , exclusively devote italian 
 music : Scarlatti , Corelli , Leonardo Leo , Cimarosa , Spontini 
 ( overture ' Cortez ' ) , Martucci , Mancinelli Sgambati 
 ( Symphony ) . composer ’ work overture 
 Cimarosa ' Matrimonio segreto ' ap preci e , 
 repeat . Christmas Day Prof. Panzn 
 conduct Beethoven Choral Symphony au 

 3,000 people , success 

 solo vocalist 

 close c 
 Mr 
 1ety . 
 DoRCHESTER.—The Madrigal Orchestral Society 
 performance Coleridge - Taylor ' Hiawatha 
 fedding - feast ' January 21 , Corn Exchange . 
 sang especially unaccom 
 portion , admittedly acquit 
 Beethoven 
 ' Prometheus ' overture , ' Nell Gwyn ' 
 dance . Mr. Henry Turnpenney Whittle 
 vocalist 

 

