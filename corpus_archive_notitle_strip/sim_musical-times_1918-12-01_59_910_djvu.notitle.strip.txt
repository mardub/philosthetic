In those days young McNaught approached music 
as an amateur, though very seriously. On leaving 
school he had entered the coffee trade in the office of 
Mr. Tate, eldest son of the Sir Henry Tate, Bt., who 
founded the Tate Gallery and contributed generously 
to other schemes which aimed at furthering the 
pictorial art. But the attraction of music was greater 
than that of coffee, and he devoted to it all hts spare 
time and energies. With his friend, Alexander 
B. W. Kennedy, afterwards Dr. Kennedy, F.R.S., the 
great electrician, McNaught took up the study of the 
violin, and succeeded sufficiently well without a 
teacher’s aid to run violin classes for the love of the 
thing in the apparently unpromising district of 
Shadwell. As a member of the Ashcroft-Evans choir, 
then flourishing at Stepney, McNaught acquired a 
good knowledge of the standard oratorios, especially 
those of Handel, and also had opportunities for con- 
ducting a band of instruments connected with the 
choir, When he was about nineteen he organized and

Beethoven’s Sonatas (with impromptu revised texts !) 
which he set me to learn with a fine disregard for my 
technical capacity. My friendship with him dated from 
the time before he embraced Roman Catholicism, and 
while he was organist at Bickley Church and in the 
throes preceding the momentous decision. His early 
death was deeply lamented by all who knew him, and by 
no one more than myself

McNaught recalled also his friendship at that time

His sacred works, especially his Masses, are 
distinguished for their admirable declamation, 
attaining here and there to rel dramatic pathos. 
‘ Each part in his polyphonic compositions 
has its own individual interest, and his modula- 
tions are simple, natural, and, at the same time, 
original

Another musician who exercised some influence 
on the art in general was Anton Josef Reich 
(1770-1836), a native of Prague, who was a good 
all-round musician at eighteen, and played second 
flute in the private band of Maximilian of Austria 
While living in Vienna he frequented an interesting 
circle which included Beethoven and Haydn. In 
1808 he settled in Paris, and had the satisfaction d 
seeing three of his operas staged there: ‘ Cagliostro 
(1810), * Natalie’ (1816), and ‘ Sapho’ (1822). In 1818 
Reicha succeeded Méhul as Professor of Counterpoint 
at the Conservatoire. His theories, much discussed: 
century ago, need not concern ushere. He composed 
a good deal of chamber music for unusual combins 
tions of wind and strings ; the capacities of the former 
were well understood by him, and in encouraging the 
study of wind instruments he followed a nation 
tradition, for Bohemia has been particularly rich @ 
wind virtuosi ; the oboists FlaSka, Fiala, and the two 
Cervenkas ; the flautist Sedlacek ; Venceslas Batka 
Cejka, and later, Kopra$, bassoonists ; Fridlovsky, 
the clarinetist ; Palsa, Mokowetky, and Janatka 
horn players—all these have helped to raise the 
standard of orchestral-playing at home and abroad

The national renaissance in Russia, which followed 
Napoleon’s disastrous retreat in 1812, brought forward 
a group of transitional composers all trying timidly © 
graft folk-melody upon traditional forms. The essays 
of Verstovsky, the Titovs, and Alabiev, were the steps 
by which Glinka mounted to his throne. In Bohemia 
two composers of this type preceded Smetana: 
Pavel Kiikovsky (1820-85), whose knowledge of the 
folk-music went deep, wrote some excellent chord 
music: ‘The Drowned Maiden’ (‘ Utonala’) and 
‘The Love Gift’ (‘Dar za lasku’), &c.; 
Frantisek Skroup (1801-62) (whose portrait appeared 
in the September number of the A/usical Tims 
together with his national hymn, ‘ Kde domov mvj

Of the remaining operas, ‘Libusa’ (1868-72) was 
written for the opening of the National Theatre, and 
was intended by the composer to be reserved asa 
festival work for days of national importance. Dealing 
with a Czech legend of great antiquity—the story o 
the Virgin Queen and prophetess, Libusa, who takes 
as husband and protector the agricultural worker 
Premysl, and founds the dynasty of the Premyslids— 
the opera is frankly constructed on Wagnerian lines 
a work cast in a large mould and based on a series of 
leading themes. ‘Libusa,’ written for the Czech 
Opera House, is not very likely to be transferred 
elsewhere, but the Overture might with advantage 
find a place in our concert programmes. ‘The Two 
Widows’ (‘ Dve Vdovy’) is a slight but finished little 
comedy-opera in two Acts—a charming musica 
setting of a French libretto transferred to Czech 
surroundings. Smetana describes itas ‘an attempt at 
opera in an elevated drawing room style, the music 
of which is purely Czech and cannot be thought o 
anywhere else than in Bohemia

By this time the malady, attended by deafness, 
which ended in madness and death had begun to 
torment Smetana. It prevented him from conducting 
the first performance of ‘ Libusa’ as early as 1874 : but, 
like Beethoven, he was able to compose long after 
deafness had cut him off from other musical activities 
After having spent a good deal of his modest incomt

in trying to find alleviation at the hands of specialists

It cannot be said that the native pieces on the 
Board’s lists for 1919 are representative of the best 
work of each of their respective composers, so that it 
would be relatively easy to make a damaging criticism 
in comparing them with some of the foreign works, 
The Board’s method of choosing the pieces does not 
allow for the best to be included. But this year’s 
syllabus is such an advance upon previous records 
that it seems worth mentioning. The presence of 
those British names means more perhaps to composers 
who have not had their works chosen than to those 
who have. The majority of teachers, traders, and 
music publishers are influenced more by what others 
do than by their own efforts, and they will detect the 
significance of all these British names. I do not 
intend to repeat the many points I have hitherto 
offered in previous criticisms of the Associated Board, 
because it will be wearisome both to me and the reader, 
and I feel now that I can well leave the subject 
altogether. The Board will either progress or fall by 
their future plans ; one thing is certain, and that is that 
the public, to whom they appeal, is better educated in 
matters of national importance than it was ten years 
ago. I think a time will come when parents will not 
need to pay guineas away to know whether their 
children play decently or not, for we are steering 
into sane days and sane thinking. And I predict that 
in the next twenty years the composers of our country 
will vindicate themselves. They will reveal in music 
what has not yet been done; and Nature—an inex- 
haustible fount of beauty—will be their text. We have 
had enough introspection, hysterics, melancholy, and 
mock music from the Schools; we want now a little 
fresh air and sincerity

Composers will cease, I hope, to wish to portray in 
music all the heroic variations of the mind, except 
perhaps in comedy, and there is every indication that 
they are realising the importance of beginning at the 
right end of the ladder. From a small music-seller 
I recently received a huge parcel of children’s music 
by British composers, all of the most recent publication. 
It was astounding. In mere volume it would represent 
about ten years’ production of all the music publishers 
in pre-War days. Therearea few critics about who are 
for ever riding high, pleading that nothing but the best 
is all that need be done. They forget that once they 
were young, and had no skill with the pen, and they 
forget also that the good and the best have to be lured 
and moulded. A parent who would expect a child of 
ten to converse in, say, the language of ‘ Rasselas’ is 
deficient in sense. To such a man the prattling of an 
‘Alice’ in the wonderland of his home would be 
boredom. He would not detect the beauty and 
boldness of it, and the rich possibilities. He would 
wonder when his child would begin to be sensible—and 
then go out to a Bach organ recital or a Beethoven 
Symphony. And on his return he would read Plato

XUM

QUEEN's HALL. 
QUEEN'S HALL ORCHESTRA

The two outstanding features at the Symphony Concert on 
November 10 were the Franck Symphony and the poignant 
‘Shropshire Lad’ Rhapsody of Butterworth. Miss Irene 
Scharrer played delighttully in Beethoven's fourth Concerto, 
and Miss Margaret Balfour's fine voice was well suited in 
Leroux’s ‘Le Nil’ The remaining orchestral items were 
Brahms’s ‘Tragic Overture,’ and ‘ Forest Murmurs’ from 
‘Siegfried.’ Sir tlenry Wood conducted, and the performance 
throughout reached a very high level

THE LONDON SYMPHONY ORCHESTRA

This fine band made a welcome re-appearance at Queen's 
Hall on November 4, when a Beethoven programme was 
submitred. Mr. Hamilton Harty conducted, and obtained a 
remarkable performance of the C minor Symphony. 
Musician Albert Sammons played the Concerto in D, and 
Miss Ethel Fenton sang three songs. The remaining items 
were the Overtures to * Egmont’ and the ‘ Leonura’ (No. 3). 
There was a large and appreciative audience. We may be 
allowed to express a hope that the L S.O. will not forget the 
importance of variety. One-man programmes are a mistake, 
we think, even when the man is Beethoven

JEOLIAN HALL

Concerts are just now plentiful, indeed far in excess of 
those in ordinary times, but the War has brought a new 
concert-going public, the masses being more fully repre- 
sented than ever before. Concerning the Festival Choral 
Society’s performance of Berlioz’s * Faust’ it need only be 
said that Sir Thomas Beecham conducted a remarkably 
impressive reading at the Central Hall on October 16, being 
admirably supported by choir, orchestra, and principals, the 
latter including Miss Gladys Ancrum, Mr. Frank Mullings, 
Mr. Herbert Brown, and Mr. Arthar Cranmer

The second of three violin and pianoforte recitals promoted 
by Miss Gertrude Fuller and Miss Beatrice Hewitt was held 
in the large Lecture Theatre of thie Midland Institute on 
October 19. Scholarly and impressive resdings were given 
of Brahms’s Sonata for violin and pianoforte in G major, 
Op. 78, and Mozart’s Sonata, also for violin and pianoforte, 
in G major. If for nothing else than its lovely Adagio, 
Brahms’s Sonata wiil always appeal to musicians and true 
lovers of chamber music. Deeply contrasted stood Mozart's 
play ful and grace‘ul Sonata, the performers being in perfect 
accord. For her violin solos Miss Fuller chose a triad 
18th-century pieces arranged by Al‘red Moffatt—an Arioso 
and Sarabande (accompa:ied on the organ by Mr. Appleby 
Matthews), a Bourrée, and the * Admiral’ Galliard. In these 
Miss Fuller realised a beautiful and sympathetic tone, 
displaying sound musicianship in her admirable performance. 
Miss Hewitt is to be congratulated on her very excellent 
rendering of Beethoven’s great Pianoforte Sonata in E fiat, 
Op. 81a, each movement bearing a distinctive title—‘ Les 
Adieux,’ ‘ L’Absence,’ and ‘Le Retour.’ It is some yeats

with Mr. Liddle at the pianoforte

ying of the Barcarolle in F sharp, Op. 60, and the

rious Fantasie in F minor, Op. 49. Moiseiwitsch is 
atitled to rank amongst the foremost of living pianists, 
and is as great a player of Brahms and Beethoven as he is 
of Chopin. His execution is never aggressive, and his 
technique is absolutely flawless. He met with a glorious 
reception, and at the close of the recital he was accorded 
an ovation

The greatest drawing power of any vocalist before the 
public is that of Madame Clara Butt, and here in Birmingham 
it does not matter how often she comes—she is literally 
supreme, and always attracts overflowing houses. It was 
her second visit to Birmingham this year, and took place at 
the Central Hall on October 24, being her first appearance 
in that Hall, our Town Hall having been requisitioned for 
National purposes. The singer's wonderful voice was heard 
in all its rich and voluminous timbre in the dramatic aria 
‘0 don fatale,’ from Verdi’s ‘ Don Carlos.’ She never sang 
better, but her admirers were principally interested in a 
group of dainty English songs demanding no strenuous vocal 
efort, but whose message was perhaps more intimate. She 
was excellently accompanied by a new-comer, Miss Grace 
Torrens. The artists associated with the prima-donna were 
Melsa and the veteran Ben Davies

There has been some splendid playing at the Symphony 
Concerts, and Mr. Godfrey is to be congratulated upon the 
high state of efficiency of his orchestra. Not even in pre-War 
days did we enjoy greater technical and interpretative 
excellence on the part of the string and brass sections. This 
has been clearly demonstrated in recent performances of 
some extremely exacting works. The compositions of chief 
interest played during the past month have been Mozart’s 
G minor Symphony and ‘ Figaro’ Overture (in which Mr. 
Godfrey’s variations of tempi were, we would say, rather 
abrupt), Brahms’s C minor Symphony, Ravel's ‘ Pavan for 
a Dead Infanta,’ Granville Bantock’s Overture, ‘ The 
Pierrot of the Minute,’ Glazounov’s second Symphony, and 
Parry’s Symphonic Variations—played In Memoriam the 
deceased composer. Of the novelties, a tone-poem, 
* Niobe,’ by Arnold Trowell, proved to be a very charming 
and most expressive composition, worthy of repeated per- 
formances. We are unable to speak of Cyril B. Rootham’s 
Overture to the opera ‘ The Two Sisters,’ as illness prevented 
our attendance at the concert wherein it figured. Kebikov’s 
pleasing ‘Christmas-Tree’ Suite was given its first per- 
formance here at the fifth concert of the series

The soloists at these concerts have been : 
MacEwan, a clever child-performer, who revealed con- 
siderable promise in Beethoven’s C minor Pianoforte 
Concerto, though we should recommend a less restless 
attitude at the instrument; Mr. Arnold Trowell, whose 
playing of the Saint-Saéns Violoncello Concerto in A minor 
was at this excellent artist’s usual distinguished level ; 
Mile. Juliette Folville, an admirable artist whose performance 
of the C minor Pianoforte Concerto by Rachmaninoff we 
regretted having to miss; and Miss Bessie Rawlins, who 
scored an unqualified success in Hamilton Harty’s interesting 
Violin Concerto. Miss Rawlins’s able performance revealed 
her as one of the most delightful violinists it has been our 
good fortune to hear for some time past. She should soon 
take her place among our leading players

Miss Desirée

CAMBRIDGE

The University Musical Society has arranged an attractive 
programme for the current academical year in anticipation of 
an increase in membership. The choir and orchestra have 
been seriously reduced in numbers owing to the War, but the 
committee has firmly refused to consider any suggestions for 
the suspension of the Society’s activities, feeling convinced 
that it was a prime duty to keep music alive in Cambridge 
and to develop local artistic resources. The first concert this 
term took place in the Guildhall on November 13, when the 
Philharmonic String ()uartet played Quartets by Beethoven 
in F major (Op. 59, No. 1), Dr. Charles Wood in D major 
(first performance), and Ravel in F major. On December 6, 
at the Choral and Orchestral Concert, Sir Charles Stanford 
is to conduct his Pianoforte Concerto, with Mr. Herbert 
Fryer at the pianoforte ; and in memory of Sir Hubert Parry 
the choir will sing the two Motets, ‘My soul, there is a 
country” and ‘There is an old belief.’ The other items 
include a ‘ Fantasia on Christmas Carols’ by Vaughan 
Williams, a set of North- Country Folk-Tunes arranged by 
W. G. Whittaker, ‘Pavane pour une Infante deéfunte’ 
(Ravel), and the Overture to Dr. Rootham’s new opera, 
*The Two Sisters.’ Next term Sefior Pedro Morales is 
arranging a concert of Spanish chamber music, and there is 
to be a memorial concert at which Dr. Rootham’s setting of 
Mr. Laurence Binyon’s words ‘ For the Fallen’ and other 
works will be performed

The Masical Club is holding fortnightly concerts. The 
Club rooms are still closed, and the concerts have up to the 
present taken place in the Corpus Combination Room. But 
with the steady increase in membership, and consequent 
improvement in finance, it is very probable that soon the 
Club’s activities will be resumed

LIVERPOOL

There were two outstanding items in the programme of 
the first concert of the Philharmonic Society which Sir 
Henry Wood conducted on October 29. These were Mr. 
Howard Carr's orchestral pictures of ‘ Three British Heroes,’ 
and J. C. Bach’s eight-voice Motet, ‘I wrestle and pray.’ 
The Australian composer’s pieces are a notable achievement. 
They made a deep impression, and that not only as 
programme-music. In connection with their titles they 
possess forceful inspiration, and an inventive and descrip- 
tive suggestiveness which make them masterpieces in their 
way. For each of his heroes Mr. Carr has provided an 
atmosphere with sure and telling touches of colour, whether 
in the martial Irish lilt of ‘O'Leary, V.C.,’ the dreary 
Arctic desolation of the second sketch, ‘ Captain Oates,’ or 
in the picturesque realism of Zeppelin destruction in 
*Warneford, V.C.’ It is hoped that a second performance 
may soon be vouchsafed. Equally notable was the choral 
singing in the Bach Motet, upon which the new chorus- 
master, Dr. A. W. Pollitt, may well be congratulated and 
encouraged. Under his clear and helpful direction the 
singers were very responsive, and exhibited some rarely 
beautiful fiano singing. Sir Henry Wood secured an 
attentive hearing of Brahms’s first Symphony, which, with 
Gluck’s ‘ Alceste’ Overture and Sullivan’s ‘ Macbeth’ 
Overture, completed the instrumental items. Miss Carrie 
Tubb sang with vocal beauty and art in an air from 
Beethoven’s ‘ Fidelio,’ and in songs by Liza Lehmann, 
Ireland, and Somervell

Mr. Eugéne Goossens, jun., conducted the second 
Philharmonic Concert on November 16, when the programme, 
if not exhilarating, was interesting in including Borodin’s 
second Symphony in B minor, a work of considerable 
although not sustained power, and also Dvordk’s Violoncello 
Concerto in B minor, which gave Madame Suggia scope for 
her great skill and delicate singing tone. The Concerto is 
all the more acceptable as pure music than merely as 4 vehicle 
for executive display. The sparkling performance of 
Bantock’s Comedy-Overture, ‘ The Pierrot of the Minute,’ 
deepened appreciation of its great poetic fancy and exquisite 
workmanship, which wears weil. A special line is due to 
the successful singing of Miss Margaret Balfour, whose 
beautiful voice was especially well-shown in Bizet’s ‘ Agnus 
Dei,’ a fine melody which makes a direct and natural appeal 
without recourse to the tonal scale. Massenet’s ‘ Evocation’ 
and Frank Bridge’s ‘Love went a-riding,’ experily 
accompanied by Mr. Walter Bridson, further increased the 
favourable impression Miss Balfour made. Elgar’s ‘ Pomp 
and Circumstance,’ No 1, in D. with choral refrains, in 
which the audience joined, or tried to join, and Handel's 
* Hallelujah Chorus,” which respectively opened and ended 
the concert, suffered somewhat in dignity in each case owing 
to the forced pace

Mr. Josef Holbrooke and Mr. Arthur Beckwith (who 
wore khaki and spurs) gave a notable performance jp 
Crane Hall on October 21, when they played three British 
Violin and Pianoforte Sonatas, viz., Sonata in A, No. 2, 
Ireland ; Sonata in A flat, No. 2, McEwen ; Sonata in F

p- 69, Holbrooke. Of these representative works, 
John Ireland's Sohata has already passed into the category 
of things accepted, where Holbrooke’s Sonata should join it. 
J. B. McEwen’s music is somewhat less individual in 
note, while very melodious. The wide gulf which separates 
modern musical idiom from the conventional formality as 
well as classic beauty of Beethoven, was shown by the 
performance of the C minor Sonata, Op. 30, No. 2

During the past month there has been a regular epidemic 
of pianoforte recitals, with a manifest quickening in public 
appreciation of the opportunities provided by Messrs, 
Rushworth, the pioneers of Wednesday mid-day recitals, 
and also by Messrs. Crane in Crane Hall. Headed by such 
renowned names as Hambourg, Pachmann, and Dawson, 
the list includes Mr. Edward Isaacs, Miss Una Truman, 
Miss Marguerite Stilwell, Mr. Walter Bridson, Miss Elsie 
Walker, and Miss Marjorie Sotham, who collaborated with 
Mr. Johann C. Hock (’cello) in an example of Dutch music 
of the Handelian period, the Sonata in D minor by 
Willem Defesch, a placid work whose interest is chiefly 
antiquarian. But it was a good vehicle for Mr. Hock’s 
suave tone. Last, but not least, was the recital on 
November 12 by Mr. William Faulkes, an accomplished 
pianist as well as organist and organ-composer of high degree

It is interesting to compare the various programmes which

more or less run the conventional course from Bach, 
Beethoven and Chopin, to Liszt. With _ these 
inevitable names were associated Schumann (‘ Etudes

Symphoniques’) and Mendelssohn (‘ Variations Serieuses’), 
But the conviction remains that of the beloved classics the 
music of Bich alone towers imperishable —in interest ever 
fresh, ever wonderful, especially in such numbers as the 
D major Organ Prelude and Fugue (Mr. Hambourg), the 
‘Italian Concerto’ (Mr. Isaacs), and the ‘ Chromatic 
Fantasia’ (Miss Una Truman). It is equally unmistakable 
that the music of the moderns who have dared to break away 
from academic shackles is growing in popular esteem. Our 
English School was notably represented by Cyril Scott 
(‘Lotus Land’ and ‘Nigger Dance’), John Ireland 
(‘Island Spell’ and * Ragamuffin’), and Frank Bridge 
(‘ Fragrance’ and ‘ Fireflies

Despite the tumult and excitement of the crowded streets 
on Armistice Day, the fateful eleventh of November, a full 
house assembled at the first concert of the Rodewald 
Society in the Yamen Rooms, when the Catterall Quartet 
played Beethoven's G major, Op. 18, No. 2, the Debussy 
Quartet, and Borodin’s Quartet in A. It is cheering to note 
that there are to be five more of these delightful concerts, by 
whose aid the Society has lightened many dark days of war. 
With Sir Charles Stanford as president, the old officials who 
have carried on so well in difficult times happily remain in 
Mr. Ernest Bryson (chairman), Dr. Pollitt (hon.-treasurer), 
and Messrs. Ernest Roberts and W. Rushworth 
(hon.-secretaries

Recent recitalists on the new organ in Hope Street Church 
have included Mr Sydney H. Nicholson, who played Bach's 
D minor Toccata and Fugue and Debussy's Prelude to 
‘The Blessed Damozel,’ and Dr. A. H. Brewer, whose pro- 
gramme on November 14 paid tribute to two predecessors 
at Gloucester in Dr. S. S. Wesley’s Andante in E flat and 
Dr. C. H. Lloyd's Elegy No. 2. To this trae organ music 
a transcription of ‘ Finlandia’ was heard in sharp contrast. 
Dr. Brewer also gave a masterly performance of Bach's 
* Passacaglia

The news of November 11 led to a complete transformation 
of the programme originally announced for the third Hallé 
concert. It included Mackenzie’s ‘ Britannia,’ Elgar’s ‘ The 
4th of August’ (‘The Spirit of England’), ‘Pomp and 
Circumstance’ March (No. 1), and solo with chorus, * Land of 
Hopeand Glory’; Biz: t's ‘ Patrie’ Overture ; Purcell’s ‘Come, 
if you dare’; C. H. H. Parry’s ‘ Blest Pair of Sirens,’ and a 
selection from ‘Hymn of Praise’ embracing the ‘ Watchman’ 
solo and the ‘ Night is departing’ chorus, with the ‘ Now 
thank we all our God’ chorale. These Mendelssohn 
selections followed Elgar’s ‘ The 4th of August.’ Should any 
choral conductor be inclined to repeat the Manchester 
programme, may I suggest that Elgar should follow the 
Mendelssohn? Tchaikovsky’s ‘ Francesca da Rimini’ was 
the only big-scale orchestral work, and Mr. Hamilton 
Harty’s graphic treatment of this now highly-appreciated 
symphonic-poem only strengthened the already firm 
conviction that as an orchestral conductor he seems destined 
to share with Goossens the first place amongst the younger 
conductors. The Hallé chorus-master, Mr. R. H. Wilson, 
conducted ‘The 4th of August,’ and Miss Gwladys Roberts 
sang the brief solo portions

The closing section of the concert took on quite a homely 
character, as the big audience took up the ‘Land of Hope 
and Glory’ refrain, a mighty volume of tone rising from 
orchestra, organ, choir. and people. A former representative 
of this Journal 19 Menchester—the late Arthur Johnstone— 
made two very striking predictions as to Elgar's music. One, 
about 1900, concerned the place ‘Gerontius’ would occupy 
in the world’s choral literature; the o-her was in 1902, 
when to the present writer he declared that E/gar’s ‘ Land 
of Hope and Glory’ tune was destined to become something 
very lke a British national anthem, and in one of his 
criticisms he alluded to it as ‘perhaps the broadest 
open-air tune composed since Beethoven’s ‘ Freude schéner 
Gétterfunken.’ Many pooh-po shed his judgment in those 
days, but the last few years have proved him equally right 
in this as he was about ‘ Gerontius

Another rather unique experience in the Free Trade Hall 
deserves mention. At Mr. Brand Lane’s orchestral concert 
on November 2, conducted by Sir Henry Wood, Pachmann 
p'ayed solos in each half of the programme. Following his 
second group came three orchestral works. The audience 
was so enthusiastic about the pianoforte-playing, that 
Pachmann went on playing and playing until there was no 
time for the three orchestral works, and Pachmann ‘ played 
out time!’ Very different was Sir WHenry’s gracious 
acquiescence in comparison with Richter’s i!l-natured refusal 
once at a Hallé concert to allow a distinguished visitor to 
play even one encore, let alone a whole series of them

The Thursday Three o’Clock Concerts of the Misses Foxon 
continue their successful progress. At the fourth Miss 
Violet Markham afforded variety by some apposite and 
pungent remarks on music of the day and its associations with 
literary ideals. Major Arthur Hall’s clever set of ‘ Nursery 
Rhymes ’ was sung to the delight of a large audience

At a Sonata recital given by a few talented pupils of Mr. 
Claude Crossley some interesting music was heard. César 
Franck’s Sonata in A was played by Miss Winifred 
Rowbotham and Mr. Maurice Taylor. Excellent as was 
the ensemble, the general effect suffered by the transference 
of the string part to the ’cello. Misses Addy and Ridgeway 
played Beethoven’s Sonata, Op. 12, No. 1, and Miss 
Charlesworth and Mr. Taylor were in brilliant association in 
Scharwenka’s Sonata in E minor for pianoforte and ’cello

The five-hundredth People’s Saturday Concert at Victoria 
Hall was made the occasion of a specia' ation and a 
presentation to Mr. R. Wright, the organizer. The concerts 
are a feature of Sheffield public musical life

OTHER TOWNS

The Bradford Subscription Concerts have succeeded in 
attracting crowded audiences to their first two concerts. On 
October 18, it was Madame Clara Butt, on November 1, 
Viadimir de Pachmann, who was the star. The Bradford 
Permanent Orchestra's concerts were resumed on November 
2, when Mr. Arthur Catterall’s quietly masterful performance 
in Beethoven’s Violin Concerto was the chief feature in the 
programme, which also included Mr. Arnold Trowell’s tone- 
poem, ‘Niobe,’ a graphic and powerful composition which 
follows closely the story, yet avoids too great a realism, 
The Huddersfield Choral Society, on October 25, gave a fine 
performance, under the composer’s direction, of Major 
Walford Davies’s ‘ Everyman.’ As it had not been heard 
for some years in the district, it was very welcome, 
and a renewed acquaintance only served to emphasise 
belief in its originality and power. The choir was rather too 
large to give quite the intimate effect that is demanded, 
otherwise it was more than satisfactory, and responded 
admirabiy to the conductor’s beat, while the solo parts 
were excellently filled by Miss Esta d'Argo, Miss Gertrude 
Higgs, Mr. Spencer Thomas, and Mr. Foster Richardson. 
Dr. Coward conducted Bach’s motet, ‘ Sing ye to the Lord,’ 
which suits this massive chorus well enough. The Halifax 
Society, under Mr. C. H. Moody, gave the whole of 
Coleridge-Taylor’s ‘ Hiawatha’ on November 7, with Miss 
Ellinger, Mr. D’Oisly and Mr. Graham Smart as soloists, 
At Hull the Janssen Concerts were resumed on October 19, 
when Dr. Rumschisky as pianist and Mr. Plunket Greene as 
vocalist were responsible for a thoroughly artistic and 
interest ng programme

Musical Wotes from Abroad

