Price 2s. 6d. each; or in scarlet cloth, 4s

NOW READY. 
BEETHOVEN’S FIDELIO 
(With German and English words). 
With the two great Overtures as usually performed; being the only

Piano Score that has been published agreeing with the Original 
Score as to the notes and signs for phrasing. 
AUBER’S FRA DIAVOLO 
(With French and English words

MELODIOUS MELODIES

Twenty Subjects selected from the Works of Beethoven, 
Mendelssohn, Schumann, Heller, &c., arranged for the 
Organ or Harmonium. Price 2s. 6d. nett

UaANbh RUN 
ceoo coo CoC CO 0

individuals about to erect a building more 
Worthy. The ill-situated, inodorous, ugly, and, in

fies the “conscript fathers” of Glasgow, will never 
again witness a Musical Festival, and we can afford, 
therefore, to let it pass on this occasion. Apart from 
the Hall, the opening performance showed every- 
thing connected with the proceedings as more or less 
what it should be. There was a capital chorus of 
400 good sonorous voices, well trained, and in all 
respects up to its work. The conductor, Mr. Lam- 
beth, certainly left somewhat to desire, but not more 
than was expected, in view of the fact that his oppor- 
tunities of presiding over a large orchestra are rare. 
Under such circumstances, partial failure is the re- 
sult of conditions that entail no blame. At the same 
time, Mr. Lambeth’s unavoidable lack of experience 
is a matter which the Committee of the next Festival 
will have to consider, if they would place their 
musical doings in the very first rank. All the minor 
arrangements, with a single exception, worked well 
even at the beginning. The exception has reference 
to a prohibition of applause at the sacred concerts. 
True, the edict was only partially obeyed; but in so 
far as it was obeyed, it had a bad effect, depressing 
audience and performers alike. A similar rule against 
encores met with rigid observance. We cannot join 
our contemporary, the A then@um, in its remarks upon 
this matter. The critic complains of injustice done 
to Costa’s *‘ Eli,” and avers that the audience would 
gladly have re-heard six specified numbers. We were 
present on the occasion, and have had fair experience 
in divining the wishes of an audience, but we de- 
tected no such longing as that of which the Atheneum 
speaks. Assuming that. it existed, there could be 
no better argument in favour of the Committee’s 
rule. Audiences need, sometimes, to be protected 
from themselves; and six encores in “Eli” would 
have extended the performance to an unreasonable 
length, and fatigued everybody concerned. Applause 
is very well, and very desirable when deserved; but 
encores are an unmitigated nuisance, and should be 
stamped out. Over the second concert, which took 
place on Wednesday morning, we shall pass lightly. 
The programme was miscellaneous, and such interest 
as it had belonged to the instrumental selections, 
which included Bach’s organ fugue in G minor, 
played to perfection on an indifferent instrument by 
Mr. W. T. Best, the con sordino Entr’acte from 
Reinecke’s “Konig Manfred,” Sir Sterndale Ben- 
nett’s overture, “‘ The Naiads,” the Larghetto and 
Scherzo from Schumann’s first Symphony, the Over- 
ture to “‘ Der Fliegende Holliinder,” and the Romance 
from Haydn’s Symphony, “La Reine de France.” 
Both the Schumann and Wagner music was very 
indifferently played, conveying an impression that 
neither the conductor nor his followers knew much 
about it. The ‘* Eli” performance, under Sir Michael 
Costa, took place on the evening of the same day, 
and was a complete success. We shall not discuss 
the work. Its merits and demerits were estimated 
long ago, and the judgment is not likely to be altered. 
But it is our duty to state that rarely has an Oratorio 
been more warmly received. Sir Michael conducted 
in his best style, and succeeded in showing that a 
master of the baton could make the Glasgow band 
and chorus work in a fashion beyond reproach. 
Another miscellaneous entertainment was given on 
Thursday evening, when the orchestra played the 
overture to “Oberon,” Sullivan’s ‘‘Ouvertura di Ballo,” 
the ‘ Pastoral” Symphony of Beethoven, the intro- 
duction to ‘‘ Lohengrin,” and the overture to ‘* Ruy 
Blas.” Maurer’s Concertante quartet for four violins 
and orchestra, entrusted to Messrs. Carrodus, Collins, 
Pollitzer and Betjemann, was also a conspicuous

Wain contingencies, dangerous edifice which satis

CRYSTAL PALACE

Tue production of M. Felicien David’s Ode Symphony, 
“Le Desert,” at the fifth of the Saturday concerts, though 
§ carefully prepared, produced but little effect. The work 
has but small merit, even the Oriental colour with which 
the composer has endeavoured to invest it being strained 
and artificial throughout. Dr. Hans von Bilow’s render- 
ing of the concerto in E flat of Beethoven, at the sixth con- 
cert, provoked comparisons which we, who cannot allow 
the display of his memory to form a portion of the exhibi- 
tion, consider to his disadvantage. He is essentially an 
) astonishing player, but interest too often flags with the

listeners when astonishment ceases. Of his executive 
wer there cannot be two opinions ; but that he occasion- 
ally sacrifices his revererice for the author in the desire to 
assert his own individuality, is made too evident to us to 
admit ofa doubt. His reception, however, proved that he 
has gained the unqualified approval of a large portion of 
the musical public; and whether he may be the “ lion” of 
the day or of years, his attraction at present is more likely 
toincrease than to diminish. A feature of this concert 
wasalso M. Gounod’s sacred piece, ‘‘ Abraham’s Request,”’ 
an eloquent composition, excellently sung by Signor 
Gustave Garcia, and directed by the composer. The 
Elegy for pianoforte and orchestra, by M. Silas, which 
was given at the seventh concert, is a work deserving 
many hearings. It is melodious and delicately instru- 
mented throughout, the pianoforte part (finely played by 
the composer) being written with a grace and hes 
which should render it a favourite with all pianists. M. 
Silas was warmly applauded both in this composition and 
two of his, smaller works, ‘‘ Malvina,” and a well written 
_ Gavotte—and, indeed, so cordial was his welcome that we 
may now hope to hear more of him and his works in public 
than we have hitherto done. Sir Julius Benedict’s Sym- 
phony in G minor, completed expressly for performance at 
the Crystal Palace, produced a profound impression at the 
eighth concert, on the 22nd ult. Of the first movement 
and the “‘ Scherzo,” we have spoken when they were given 
for the first time at the last Norwich Festival. The 
“Andante con moto” and final ‘“ Allegro con fuoco,” are 
| well worthy of being associated with the movements 
already written, the “ Andante” being charmingly melo- 
dious, and the ‘ Allegro” (a highly effective example of 
passion without noise) concluding the work most appro- 
priately. The composer was called for at the termination 
of the Symphony and loudly applauded. We have little 
to say of the vocal music at these concerts. Amongst 
Many singers of established fame, we have had some 
pa efforts were not of sufficient merit to warrant special 
mention

321

If such a thing be legitimate art, then, indeed, the scope 
of legitimate art is a wide one. Hardly could it and the 
overture to ‘‘ Leonora” have even the smallest relationship. 
Dr. von Biilow’s rendering of Liszt’s Hungarian Fantasia 
was a brilliant display of his peculiar powers, and excited 
much enthusiasm, but it was hardly a fitting prelude to 
Beethoven’s C minor Symphony, with which the concert 
ended, Feeling this, perhaps, many of the audience went 
away, and left the great master to pour forth his strains 
to comparatively empty benches. Beethoven, however, 
can survive ill treatment. Above and beyond all possible 
influences, he stands serene, with something yet to gain, 
it may be, but nothing to fear

Bacw’s SACRED CANTATAS.—Allusion was made by Mr. 
Barnby, in the Paper on Church Music which he read at 
the Church Congress at Bath in October last, to the numer- 
ous Sacred Cantatas by J. S. Bach, which might be used 
on Church festivals and during special seasons. Messrs. 
Novello, Ewer and Co. have already published two of these 
with English words, viz. : “‘God’s time is the best’ and “* My 
spirit was in heaviness,” and a third is in preparation, 
“Q Light Everlasting’? (O Ewiges Feuer), as well as 
Bach’s setting of the Magnificat., They propose to con- 
tinue the series (the translation and adaptation having 
been undertaken by the Rev. J. Troutbeck), and thus open 
to the English Church a treasure-house of sacred music 
of the very highest character

322 THE MUSICAL TIMES.—Decemser 1, 1873

___ ne 
redoubled energy. These cheers were the welcome on| Tue third of the ‘ Musical Evenings” was given at to lea 
the part of the ‘‘ people,’ and it was, perhaps, not the} George’s Hall, on the roth ult., before a large audience, — com! 
worst either, for probably no other crowd of the same size | Schubert’s posthumous string Quartet movement, in ¢ were, 
could have given expression to its feelings so unanimously | minor, was well played, but coldly received; the String whicl 
and so energetically. Later in the evening the Munici- | Quintet of Beethoven, in E flat (Op. 4), however, exciting even 
pality gave a féte in the Grand Hotel of Pesth, the Hun-| the hearers to an enthusiasm which— considering that jt of th 
garia, where, besides a number of notabilities and native | contains five movements, and was placed at the end of the TH 
guests, the foreign admirers who had been attracted by | programme—affords ample proof of the growing taste for Orato 
the festival were likewise present, the lady admirers being | the highest chamber music. One of the principal features since 
the most prominent among them. A gipsy band was] of the evening was the performance of Sir Sterndale Ben. the 2

there, of course. At the banquet which followed, toast | nett’s pianoforte Sonata, ‘ Maid of Orleans,” by Mr. Walter fy were 
came after toast, enthusiasm rising more and more at each. | Macfarren. Each movement of this charming work was

the National Academy of Music, conferring on Liszt the| forte part of Schumann’s “ Fantaisie-Stiicke—in which } 
right of presentation for his lifetime; and at 10 a.m. the | he was joined by Mr. Henry Holmes (violin) and Signor {& 
ceremony of presenting him with a laurel wreath in gold, | Pezze (violoncello)—was also deserving of the highest 
which had been got up by subscription, was performed in| praise; the “ Romance” and ‘“ Humoreske”’ especially, 
the Great Hall of the Redoute, used for all such occasions, | being given with a perfection which appeared thoroughly The 
as the most spacious locality in the town. In the evening, | appreciated by the listeners, who could scarcely be re. togeth 
Liszt’s Oratorio of ‘ Christ’ was performed before a large} strained from attempting an encore. A smoothly-written and ot 
audience, most of whom were enthusiastic enough to] solo for the Viola, by Mr. Henry Holmes, was excellently ing by 
enjoy the treat, which lasted four and a half hours. The| played by Mr. Burnett, and Miss Abbie Whinery contribu. J. Tro 
third day was taken up by a banquet given by Liszt’s| ted some well-selected vocal pieces, accompanied by Mr. Turle, 
admirers, and by a festive representation of one of| Stephen Kemp. * 
the popular pieces in the National Theatre, at which all] 4 ypsrrmontaL, consisting of a splendidly illuminated proved 
the foreign guests made their appearance, although, as the | address on vellum, and signed by all the Professors of the St, Pa 
play was performed in Hungarian, they can scarcely have | Royal Academy of Music, has been recently presented to We 
derived much enjoyment from it. Mr. G. A. Macfarren, congratulating him upon the successof that i 
A vERY good musical performance was given by the] his Oratorio, ‘St. John the Baptist,’’ at the Bristol Festi- St. Pa 
pupils of the London Society for Teaching the Blind to] yal. The presentation took place at the Academy, Sir result 
Read, at the Institution, Upper Avenue Road, Regent’s| Sterndale Bennett, the Principal, surrounded bya large body church 
Park, on the 31st October. An air, with variations by | of the Professors — many of whom had been Mr. Mac- momer 
Hesse (excellently played on the organ by Mr. W. Allen) ;| farren’s fellow students—prefacing the proceedings bya Londo: 
the solo, “ It is enough,” from “ Elijah” (carefully rendered | speech which, though brief, was happily expressive of the of En; 
by Mr. Edward Long), and the chorus, “‘ There is joy,” | feelings of his brother artists. It is now definitely an- Y) bring < 
from Sullivan’s “‘ Prodigal Son,” were most deservedly | nounced that “ St. John the Baptist” is to be given this ofa Ri 
applauded ; and sufficient evidence of the careful training | season by the Sacred Harmonic Society ; and coupled with Dioces 
to which the students are subjected by Mr. Edwin Barnes, | this statement, we are told that Sir Michael Costa has , the orig 
the professor of music at the Society’s schools, was given | withdrawn his Oratorio “« Naaman,” to make room for the in whic 
throughout the concert to warrant us in awarding the | new work; a graceful act which we are certaim will be fully take it 
utmost praise to the system pursued in the institution. The| appreciated, not only by the composer, but by all who We ma 
chair was occupied by the Rev. Walter Peile. desire that our native artistsshall be ensured a fair hearing. as case 
Tue Harvest Festival was held at St. John’s, Waterloo | The Oratorio will be shortly published by Stanley Lucas, such € 
Road, on the 14th ult., the most noticeable feature of | Weber and Co. authori

which was a new hymn, written and composed by Mr.| oy the 7th ult., the St. George’s Glee Union gave its tained 
C. Castell, the organist. The whole service was admir- monthly concert "at the Punlien Rooms Tie principal 
ably performed by the efficient choir of St. John’s, under vocalists were Miss Janet King, Miss Clara Buley, and Mr. 
the superintendence of the Precentor, the Rev. S. Little, Howells, all of whom were highly successful. Mr. Tamplin's 
seme. brilliant execution of a solo on the harmonium was warmly of 
Mr. F. A. Bripce’s Concert and Operetta Party ap-| received. Amongst the glees and part-songs “Oh! my “The | 
peared at Beaumont Hall, on the 15th ult., the artists| Jove is like a red rose”? and the ‘Carnovale’’ were the bio’ of 
being Miss de Seale Penson, Miss Fanny Emerton, Mr.| most admired. Mr. Garside conducted. 6 
Arthur Thomas, Mr. T. C. Travers, and Mr. F. A. Bridge; 3 » at | 
Pianist, Miss. E. Stirling. The first part consisted of a con- Phe ee ee —— a pl 
ivan’ H “ 1X ’ . ’ 
+ ri gy Se ee Se eer place on the 18th ult. Beethoven’s Sonata in E flat (Op. | which

Ss aate sages ‘ ; , tice’s f Upon wh

AN evening concert was given on the 24th ult., at the | was given at St. James’s Hall, on the roth ult., and attrat- feat Cl 
Bow and Bromley Institute, under the direction of Mr.| ted a large audience. A Fantasia by Hummel (Op. 18), Mity of

Henry Parker, at which the following artists appeared :—{| Bach’s Italian Concerto, Beethoven’s Sonata (Op. 100); ly: 
Miss Ellen Horne, Miss Estelle Emrick, Mr. Stedman, | and Sir Sterndale Bennett’s Sonata the “ Maid of Orleans, ily ser 
Mr. Frank Elmore, and Mr. Thurley Beale; Cornet-a-| were the principal features in the programme; ant We belie, 
piston, Mr. Reynolds. The concert was highly successful, | need scarcely say that all these works were given with fequenc 
and the able exertions of the clever concert-giver and his | artist’s usual fluency and marvellous executive powef, We think

supporters were much appreciated by a crowded audience. | applause however, being so indiscriminately bestowed a8

audience. The programme was carefully selected, including overtures, 
pianoforte and clarionet solos, songs, duets, part-songs and choruses, 
all of which were well rendered

BASINGSTOKE.—T wo concerts were given in the Town Hall on the 
28th Oct. by the choirs of Basingstoke and Hartley Westpall, in aid of 
the organ fund of the latter place. The choir of forty voices, under 
the direction of Mr. H. Blackith, was very efficient. The programme, 
besides part-songs, included selections from Mendelssohn's St. Paul, 
and Handel’s Acis. Mrs. Fowler was encored in “ But the Lord is 
mindful of His own,” as was Master Whitehouse in “ Let me wander 
not unseen.” Mr. O. Christian was highly effective in his solos ‘“‘O God, 
have mercy,” and “O ruddier than the cherry,” and Mr. Blackith was 
loudly applauded for his performance of Beethoven’s ‘ Sonata 
Pathétique

Betrast.—The first concert of the Belfast Musical Society for the 
present season, was given in the Music Hall on the 7th ult., and 
proved, both from an artistic and social view, a most successful opening 
of the winter campaign. The artists were Miss Leonora Braham, 
Herr Elsner, and Mr. W. Penry Williams. Miss Braham showed her- 
self a singer of great promise, and Herr Elsner played a solo on the 
violoncello in a most artistic manner. The chorus and orchestra of 
the Society acquitted themselves admirably in selections from Haydn, 
Mendelssohn, Cherubini, Bach, Weber and Wagner. Mr. James 
Thomson conducted. The hall was well filled by the subscribers and 
their friends

Croypon.—A singing school has been established at the Lj 
and Scientific Institution under the direction of Mr. E. Griffiths, F C6 
the music master of the Whitgift Middle Class Schools in this} 
The classes are arranged for study and practice in every 
vocal music, and are very successful. Examinations are contemplgts 
at the end of each season to grant certificates of competency to ¢j 
who may desire them, the papers etc., being set by eminent Musician {> n, and 
not connected with the town. At the Whitgift Schools, concerts yj Mr. J. B. El 
be given by the pupils and friends as usual in the week before Christ. | MANCHEST 
mas, the works for performance being Haydn's Spring ang Vindconcert of th 
Bree’s St. Cecilia's Day with full accompaniments

EpINBURGH.—A most attractive and well attended concert 
given in the Music Hall, on Saturday afternoon, the 15th ult,, byd 
Choral Union, when Acis and Galatea was performed. There 
an excellent orchestra, conducted by Mr. Adam Hamilton, and the 
singers were Miss Edith Wynne, Mr. Whitehead, Mr. Darling 
Mr. Santley. The second part of the programme was miscellaneous } 
commencing with Beethoven's Symphony in A major

Oakeley’s song ‘‘ Tears, idle tears” (sung at the last Birmj bei 
Festival) was exquisitely rendered by Miss Wynne, and Miss u 
Drechsler Hamilton received an enthusiastic recall for her at + 
performance ofa violin solo. ——THE first festival of the Edinburgh Dip 
cesan Choral Association took place on Wednesday afternoon deg 
ult., at St. Paul’s Episcopal Church. The festival consisted of an e 
service at four. Although admission was by ticket, the church wasn 0 
ed to excess half-an-hour before the service began. The choirnumbersy guamserto in E 
about 192, of whom 32 were from St. Peter's Edinburgh; 22 from fies “ Dans 
John’s, Alloa; 20 from St. Mary’s, Dalkeith; 20 from St. James’, Leith; much app 
18 from All Saints, Edinburgh; 14 from St. Andrew's Home muenovelties p

Jarrow.— On Thursday the 6th ult., Handel's Oratorio af which we 
was performed in the Mechanics’ Hall, by a company of abouttis 
instrumentalists and vocalists. The solo singers were Miss 4 
Penman (encored in ‘‘Let the bright Seraphim”), Miss Emme 
Moore, Mr. D. R. Mitchelson, Mr. T. Moore, Mr. J. Duke and Mr. 
Tibbo, The concert, which was conducted by Mr. J. Hickman, pt 
one of the most successful that has taken place in Jarrow, 
hall being quite full

LiverPoot.—The eighth subscription concert of the Philha 
Society took place on the 28th of October, and was one of r 
excellence. The principal artists were Madlle. Alvsleben, Mrs. Se 
Fennell, Mr. Edward Lloyd and Mr. Santley. The whole of the n 
part was devoted to a very spirited rendering of Beethovens #0 
Symphony (No. 9, in D minor), which was well sung by 50 
choir, in spite of the high pitch of the orchestra. The sec iG 
the concert was miscellaneous; the overtures being those to 7 
summer Night's Dream (Mendelssohn) and Zampa (Herold). 
choral members sang very well a charming part-song of BM 
“ The Sailor’s Serenade.” The concert was highly ap reciated 
crowded audience.—THE ninth subscription concert of the 
monic Society was given on the 11th ult. Principal artists, 2 . 
Sinico, Signor Aramburo and Mr. Perkins. The Sinfonia 
Mozart's in E flat, and was admirably performed. The ove

i, by Mrs, 
Was a lar

333

eS 
‘ere those to L’Alcade de la Vega (Onslow), and Tannhduser 
Wagner), Which last went magnificently, and was loudly redemanded, 
See repeated. The concert concluded with the March from the 
The choral members sang “ Fair as a bride” (Rossini), 
Siitendelssobn’s “ Hunter’s farewell,” with four horns and trombone 
iment, very well, and the solo artists were much applauded. 
Madame Sinico’s rendering of Beethoven's grand scena “ Ah perfido,” 
wasexceedingly fine ——ON Thursday the 13th ult., the annual tea meet- 
‘ggandconcert was held at St. Matthew’s Church, Hill Street,the chair

3 red taken by the Rev. T. W. Moeran, Incumbent of the church. 
Li schoolroom was tastefully decorated with evergreens, etc.,for the 
8, F.C, The principal vocalists were the Misses Macdonald, Oxton 
this town, Fo Truscott, and Messrs. Nicholson, Oxton, Sowden and Sanderson. 
branch of 's quartett, ‘Softly fall the shades of evening,” was most 
templated stively sung by Mrs. Bishop, Miss Truscott, Messrs Sanderson and 
y to those den, and other part-songs were very fairly rendered by the choir. 
musicians

scerta af Mr. J.B. Ellison presided at the pianoforte

STER—The Atheneum Musical Society gave the first 
“ae ar eeieon on Friday the 31st October, at the Memorial 
| Mall, Albert Square. The programme wasone of peculiar interest, and 
erformance of more than average excellence. A Cantata by the 
‘or of the Society, Dr. Hiles, formed the principal portion of the 
sthalf ofthe concert. It is entitled The Crusaders, and is written 
solo soprano, and tenor, and a chorus. The music is admirably 
aptedfor Choral Societies,the most effective numbers being,“ Evening 
ntly flowing,” the March of the Templars, and the Pilgrims’ 
he remainder of the programme calls for no special remark, 
being composed of the usual choral works.——On the 13th ult., Mr. 
Charles Hallé gave a fine performance of Mendelssohn’s St. Paul, with 
A TM es Alvsleben and Enriquez, Messrs Lloyd and Merrick as 
ci Both the ladies sang well, and Mr. Lloyd (who is a great 
orite here) acquitted himself excellently. ‘The choruses were 
on the 19th Feely given, On the 2oth ult., Dr. von Bilow made his first appear- 
neal ee, before. a crowded audience. His pieces were Beethoven's 
bs certo in E flat, Chopin’s Allegro de Concert in G, and Liszt's two 
r numbers fies “Dans les bois,” and “Ronde des Lutins,” in all of which he 
jmuch applauded. Miss Edith Wynne was the vocalist. Amongst 
novelties performed at Mr. Hallé’s concerts were the following: 
movements from Raff's symphony ‘‘Im Walde,” the scherzo from 
Gad ny in C minor, a Concertstiick by Volkmann, anda 
, Liszt, for pianoforte and orchestra, on Hungarian airs. 
Wt Mr. de Jong’s Saturday evening concerts, we had, on the 15th ult., 
Han udas Maccabeus, with Madame Sinico, Miss Mary Thorley, 
Miss Alice Fairman, Messrs Pearson and Wadmore as solo vocalists. 
Bridge (Cathedral organist and chorusmaster at these concerts) 
' Bendered efficient aid at the organ. On the following Saturday Maadlle. 
| Patti, Madame Fanny Huddart, Signor Camero and Mr. 
with Mons. Theodore Ritter as pianist, attracted a large 
d The orchestral pieces which were given in irreproachable 
tyle, included Beethoven's overture to Egmont, Gounod’s ballet music 
b La Reine de Saba, ard the March from Wagner's ‘Tannhduser. 5

Mowrrost.—On Saturday the 15th ult., an audience was assembled 
rt Guild Hall, on the invitation of Messrs Methven, Simpson and 
0, tolisten to a recital on one of Mason and Hamlin’s concert organs, 
y Mr, W. H. Richmond, organist of St. Paul’s Pro-Cathedral, Dundee. 
i d played through a programme of classical and popular 
including Mendelssohn’s “ War March of the Priests,” and 
Pections from the L obgesang ; and also improvised, showing with much 
mill the various effects to be obtained from the instrument. Much 
feditis due to Messrs Methven, Simpson and Co, for the treat they 
the music lovers of the district

niments. } Pigsonstown.— Mr. Arnold, the newly a pointed organist, gave 
| Society "fis first concert in the Printing House Building, on Friday evening 
Class-r00t Be 7t ult, toa crowded audience, when he was kindly assisted 
‘ Willington, Mrs. Brereton, the Misses Brereton, Mrs. 
Richard Biggs, Esq., M.A., Fred. Witny, Esq., the students 
neste: field College, St. Brendon’s Church choir and others. The 
#4 Music was highly appreciated, many encores being .awarded, 
Bike gem of the apm Raga Beethoven's Sonata, for piano and 
it, by Mrs. Biggs and Mr. Arnold, which created quite a furore

pre Was alarge audience

OrGAN APPOINTMENTS.—Mr. Charles Osmond to St. Mary Church, 
South Devon.——Mr. W. Osmond to St. Saviour’s, Liverpool.—— 
Mr. Henry Ditton-Newman (late organist and choirmaster of St, 
Thomas’s Church, Rhyl), organist and director of the choir to St. 
Margaret’s Church, Anfield, Liverpool.——Mr. George Ryle to St. 
Thomas's, Bayswater.——Mr. James Edward Butler, organist and 
choirmaster to St. Thomas’s, Bethnal Green

DURING THE LAST MONTH. 
Published by NOVELLO, EWER & CO. 
BEETHOVEN. — Thirty - eight Sonatas for the

Pianoforte. Edited and fingered by Agnes Zimmermann. Folio 
size, handsomely bound in cloth, gilt edges, 21s

Arranged by

Adam, Adolphe ... Marche Religicuse W. T. Best ... 
Alexander, Alfred March “a 
Beethoven «e Military March, Posthumous W. T. Best . ese 
, Triumphal March (Tarpeja) ‘o ie 
« Grand March in D (Op. 45) @ oon 
« Funeral March (Op. 26) 
«» March, (Egmont

Best, W. T. «+ March fora Church Festiv al

SUITABLE FOR A CHRISTMAS PRESENT

BEETHOVEN'S SONATAS

NEW AND COMPLETE EDITION

