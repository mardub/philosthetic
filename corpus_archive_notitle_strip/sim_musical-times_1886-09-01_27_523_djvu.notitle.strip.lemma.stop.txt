TuursvpAY Morninc.—Bach MASS B MINOR . principal : 
 Miss ANNA WILLIAMS , Miss HILDA WILSON , Miss DAMIAN , 
 ! ; BARTON McGUCKIN , Mr. SANTLEY 

 TaurspDAY Eventnc.—New work , Chorus Orchestra , 
 REVENGE , C.V. Stanford ; symphony C MINOR , 
 ; ( Beethoven ): WALPURGIS NIGHT ( Mendelssohn ) . Prin- 
 s : Miss DAMIAN , Mr. IVER McKAY , Mr. BRERETON 

 ay Morninc.—ST . LUDMILA . oratorio write 
 val Antonin Dvorak . principal : Madame ALBANI , 
 Yadame PATEY , Mr. EDWARD LLOYD , Mr. SANTLEY 

 Tuurspay Eveninc.—THE maid ASTOLAT ( Cantata 
 | compose Festival Dr. C. S. Heap ) ; overture , CHEVY 
 CH : ASE ( Macfarren ) ; RHAPSODY F , . 1 ( Liszt ) , & c 

 Fripay Morninc.—Dvorak STABAT MATER ; Mendelssohn 
 | LAUDA SION ; Beethoven symphony C minor ( . 5 

 Fripay Eventnc.—THE bridal TRIERMAIN ( Cantata 
 compose Festival Mr. Frederick Corder ) ; overture , 
 ERFLOTE ( Mozart ) , TANNHAUSER ( Wagner ) ; 
 FANDANGO Violin ( Molique ) , & c 

 MADAME CLARA WEST ( soprano 

 MISS LOTTIE WEST ( contralto ) . 
 Beethoven Villa , King Edward Road , Hackney 

 MISS MADELEINE WHARTON ( soprano ) . 
 MDLLE . JOSE D’ARCONVILLE , R.A.M. 
 ( contralto , St. James Hail London Concerts ) . 
 Oratorio , Italian Operatic , Ballad Concerts . 27 , Central Road , 
 Withington , Manchester 

 LORELEY 

 second concert , December 13 , 1886 : Beethoven MASS C ; 
 Mendelssohn HYMN PRAISE 

 concert , February 21 , 1887 : Spohr CALVARY 

 adie ’ school student ’ home 

 Beethoven House , Northampton.—Principal , Mrs. 
 Sampson.—Large House , Tennis Court , Playground . piano , 
 Organ , Harmony , & c. , Mr. Brook Sampson , Mus . Bac . , Oxon . , 
 F.C.O. Organ ( 3 manual ) house ; Harp ; 15 piano 

 recent success : Senior Honours , R.A.M. ( Piano organ ) ; | 
 Class Senior Honours , Trinity College , E.M.and K. ; certificate 
 Pianist , T.C.L. , & c. ; 
 College Preceptors . South Kensington 

 pin town father ; , finally , 
 concert Pressburg , decide future 
 musical prodigy . Adam Liszt 
 long doubt son vocation , 
 number hungarian magnate , hear 
 boy occasion , offer furnish 
 mean education—6oo florin yearly 
 year 

 exorbitant fee demand Hummel 
 tuition question , Liszt 
 family Vienna , , careful con- 
 sideration , Charles Czerny Antonio Salieri 
 strict 
 methodic instruction , 
 taste young high - flier , exactly 
 need : lay sound foundation 
 correct technique 
 rear . Salieri teaching , far 
 , good , consist exercise 
 sacred composition , read analyse 
 classical score . result ie study 
 master Franz proof public 
 Vienna concert December 1 , 
 correspondent niusikalische 
 Zeitung report young virtuoso trans- 
 port audience admiration ; 
 execute Hummel minor Concerto 
 incredible vigour , feeling , ex- 
 pression , delicate shading ; free 
 fantasia ( nature capricciv ) 
 theme ( Andante Beethoven 
 major Symphony , Cavatina Rossini , & c. ) 
 concert 

 1822 

 ei ook Pt 

 occasion ( April 13 , 
 1823 ) place - describe truly 
 memorable incident — Beethoven , honour 
 concert presence , ascend 

 ae 

 mean disparager classic , remark 

 pertinently " real nature form 
 consist harmonious , musically logical — 
 i.e. , thematically - construct — organism , Liszt 
 work correct form quartet 
 Beethoven , , sure , 
 fault attribute . " consider- 
 able reduction — time 
 — till reach remainder 
 Liszt piano , orchestral , vocal work 
 live . remainder , firmly believe , 
 greatly surpass value legacy 
 3erlioz leave , musical world 
 thing beauty joy forever . Liszt , speak 
 year ago Walter Bache endeavour 
 popularise work England , 

 pity dear friend pupil undertake 

 Newe Frete Presse come 
 , translate , read follow 
 notice Philharmonic Society Con- 
 certs 

 Sterndale Bennett slack unenergetic 
 conductor , Sir Arthur Sullivan , successor , 
 altogether drowsy fellow . large close - cropped 
 head firm neck , dark face black 
 eye , impression passionate man 
 anger explode suddenly like cannon . 
 instead unequalled phlegm . 
 G minor Symphony Mozart perform . Sullivan 
 conduct lift eye 
 score — time . 
 heavenly piece play badly , feeling 
 elegance . end , public applaud 
 enthusiastically continuously , Sullivan 
 think turn round audience . 
 remain unmoved arm chair , await 
 second piece — Beethoven Violin Concerto , play 
 Ondricek . Sullivan conduct usual 
 phlegm , time look score . 
 remain beloved arm chair , 
 Christine Nilsson appear sing ' ah , perfido ! ' 
 Philharmonic deserve 
 praise - day command twenty- 
 year ago 

 read grow pleasure note 
 follow 

 reader ' patriotic pride English | distinguish fact fiction . , Dr 

 , honour reward native production " ; | Hanslick declare classical music 
 intimate , native foreigner | perfectly hear England Richter , 
 y. equal , english press certainly proclaim|and Richter Beethoven Ninth 
 praise high tone . Soj Symphony Mass D intelligible english , 
 st little good music produce foreigner / pity willing victim mislead infor- 
 opportunity test point | mation , brand second - hand statement 
 afford , fitly refer critic } absolutely untrue . unscrupulous humorist , 
 friend Antonin Dvorak information . , | fall Doctor mood , 

 f , assume favour countryman , | serve Alfred Jingle , Esq . , serve 
 Dr. Hanslick certainly man cast } unsuspecting President ot Pickwick Club 

 formity long outraged law health , 
 Schubert grow strong . regain old 
 cheerfulness , , 

 gay . , alas ! improvement delusive . 
 dieting , exercise , come late — old story — 
 shortly return Vienna relapse 
 place . ' ' evening end October , " write 
 Kreissle , ' ' dine hotel , hardly 
 swallow morsel fish suddenly 
 throw knife fork plate , declare 
 food absolutely odious , taste 
 like poison . " beginning nature 
 final revolt treatment 
 subject , , thenceforward , 
 Schubert , word quaint old 
 epitaph : " pain portion ; physic 
 food . " brave fight life , , , 
 probability speedy death occur 
 . mind remain thought work , 
 especially idea completion opera 
 ' Graf von Gleichen , " live music 
 , contemplate course study 
 counterpoint Sechter . read 
 composition Handel — like Beethoven 
 crisis — come conclusion 
 . word 
 : " learn , 
 work hard Sechter 
 lost time . " ' earnestly bent 
 , " add biographer , " day 
 walk Hernals — i.c . , November 4 — not- 
 withstanding weakness , Vienna , , 
 musician Lanz , Sechter 
 consult matter , actually 
 decide Marpurg text book , 

 XUM 

 521 

 jmber date lesson . " time 
 sis nervous system terrible state . hear- 
 e Beethoven C sharp minor Quartet , break 
 gown completely , way 
 acitement , friend alarm 
 ; iscondition . hear music sad state , 
 away city , 
 ‘ sto quiet , reposeful place , agitation 
 night calm chance 
 nature recuperative force . ' mischief , 
 patient grow weak weak , till , November 
 1 , come word accent 
 jespair . apparently alarm nature 
 sisown foreboding , seek divert thought ; 
 solonger music mean fiction . | 
 te write Schober — use pen time 

 dear Schober,—I ill . eat 
 unk day , amso tired 
 shaky bed chair 
 . Rinna attend . taste any- 
 thing bring directly . distressing | 
 condition kind help reading . 
 Cooper , read " Mohicans , " 
 " Pilot , " ' Pioneers . " any- 
 thing entreat leave Frau 
 von Bognor , coffee - house . brother , whoi 
 conscientiousness , bring 
 conscientious way . else.—your friend , 
 SCHUBERT 

 ne ? 
 

 retort sufferer , ' ’ true ; Beethoven 

 . ' easy sce wander 
 thought moribund composer slip 
 hallucination stranger possession 
 mysterious purpose matter 

 letter narrate death bed conversation 
 refer , proceed 

 t index , speak , heartfelt 
 wish rest Beethoven , 
 deeply reverence ? speak 
 Rieder , ascertain cost remove 
 body — seventy florin — 
 large sum , large sum , little 
 honour Franz resting place . , 
 spare temporarily sum florin , yester- 
 day pay . rest , believe , 
 expect ail expense incidental 
 illness burial meet leave . 
 , dear Father , agree senti- 
 ment , assure mind relieve 
 heavy load . 
 mind , let know bearer letter , 
 ican arrangement arrival 
 hearse . care notice 
 - day clergyman Wiahring . afflicted 
 son — FERDINAND 

 P.S.—Should lady appear mourn- 
 e ? manager funeral think need 
 provide crape , usvally wear funeral 
 unmarried people , pall - bearer 
 red cloak flower 

 Ferdinand suggestion meet father ap- 
 proval , funeral place November 21 — 
 bad weather , like Mozart . Schubert 
 friend run home rain 
 illustrious predecessor . view body , 
 lie coffin dress hermit crown 
 laurel , accompany procession St. 
 Joseph Church , funeral service cele- 
 brate , music Motett Gansbacher , 
 dead composer ' pax vobiscum , " set 
 word specially write Schober . 
 church procession tock way viilage 
 Wihring , road traverse Beethoven 
 funeral train 1827 . reach little ceme- 
 tery remain lay rest place 
 great composer , 
 Schubert desire sleep . spot soon mark 
 monument , erect sorrow friend , 
 consist bust , inscription write 
 Grillparzer 

 et _ 

 , ' ' twere consummation devoutly 
 wish , ' " ' reply , " , course , utopian 

 think , ? " rejoin inventor . 
 * — 
 work , year — machine 
 , ? machine 
 construct combine musical phrase , 
 harmonise , , die , , hope , 
 able build symphonic form , 
 work Beethoven 
 . 

 ! " exclaim ; ' , 
 crazy ! ' inventor laugh gently , 
 face form transfigured , remind 
 change chinese lantern 
 light inside . lose faded helpless air , 
 , rise brisk decision , pluck 
 sleeve , quietly , ' ' come 

 scarcely 

 es 
 Schubert , Schumann , Cornelius , Wagner- — 
 receive appreciation aid need , 
 fame , help hand 
 death ! beginning chain stand 
 Beethoven — end stand Liszt ! 
 away — lock intact recess heart 
 memory — add ; circle complete ; 
 little Liszt receive kiss giant Beethoven — 
 great meet , end chain unite close , 
 , sparkling gem circlet , appear 
 immortal ! C.B 

 BAYREUTH FESTSPIEL 

 weekly dispatch 

 t describe detail previous occasion , 
 shall choral work 
 Beethoven convey mind strong sense sus- 
 taine grandeur , pathos , melodic inventiveness 

 LONDON NEW YORK : NOVELLO , EWER & CO 

 OBITUARY 

 Henry JARRETT.—On 2nd ult . die , Buenos Ayres , 
 ¢man exert small influence course 
 t operatic event England year . Henry 
 Jattett native Bath , connected 
 music horn player theatre . 
 ime company serve theatre Bath Bristol , 
 ‘ form alternate day . Jarrett 
 tuty , , pay small 
 wher mode locomotion , regularly walk Bristol 
 night , carry instrument 
 usarm . progress executive musician presently 
 title look employment Metropolis . 
 ' difficulty obtain , soon rise 
 tminence cornist , Opera , con- 
 tection good orchestral Concerts time . 
 ‘ formance horn Beethoven Choral 
 ‘ ymphony remember veteran amateur . 
 /atrett , shrewd man , know 
 tuman nature , aspire position good 
 Wwalities free play , , course time , 
 ‘ tere managerial career , figure season 

 Roberts lead orchestral band 

 mainly confine composition 

 mention Beethoven Mendelssohn 
 

 Paganini . 
 violin concerto occur work 

 aristocracy adopt nom de plume Rancé . 
 | destroy early work 

 remain great master , Beethoven , Schubert , 
 central cemetery Vienna , moot . " 
 * number Liszt composition , far 
 n , 647 . - appertain 
 orchestra ( thirty - transcription ) 
 transcription ) . 
 tten work ; num 

 thirty - , 

 DunpALK.—Mr . J , W. Dry , Organist St. Nicholas ’ C } uurch , 
 m Organ Recital Church Wednesday evening . r8th ult . 
 good attendance , include large number person 
 member congregation . Recital prece de short 
 evening service , conduct Rev. S. J. Carolin , close 
 singing Charles Wesley hymn , ' Jesus , lover soul . ' 
 programme Recita ! afford Mr. Dry ample opportunity 
 display ability , capacity organ . 
 service conclude singing hymn 

 Herne Bay,.—Mr . E. A. Cruttenden , Organist Choirmaster 
 Parish Church , annual Ballad Concert Town Hall , 
 Monday , July 26 . solo vocalist Miss M. Kelly , Miss 
 Lena Law , Mr. R. J. Thompson ; instrumentalist , Miss M. 
 Hobday ( violin ) Master T. Hobday ( violoncello ) . Miss E. Fleet , 
 Miss Hobday , Master , G. ; rgulden preside pianoforte , 
 ind Mr. Cruttenden accompany . programme , mis- 
 tellaneous , render . | 
 Leeps.—The Leeds Corporation free Concerts 
 fr season Town Hall , Saturday evening , 
 july 31 . artist pupil Conductor Organist , 
 Dr. Spark , know merit attract exceptionally large 
 udien Dr. Spark contribute solo — Lemmens ’ Grande 
 Marche Pontificale , Handel Air , " leave , deceiver , " follow 
 ya new March C major composition . Dr , Spark 
 March attractive work , performance warmly applaud . 
 song creditably render Mr. Walter Nicholson , Miss 
 Braithwaite , Mr. Gilbert Jackson , Miss Hettie Peel . provid- 
 ingan organ solo , Miss L. Greenwood somewhat ambitiously choose 
 Beethoven Larghetto , Grand Symphony inD. ' 
 tefformance receive cordial approval . monthly Organ 
 Concerts , vocal music , prove interesting | 
 attractive . free Concert Town Hall , 
 md ult . Dr. Spark come 

 n Scere 
 orchestra 

 ALARD , D.—Twenty - easy melodious study 
 Violin Pianoforte 

 BEETHOVEN.—Six string aon Op . 18 . ( . ) 
 mark edit E. Rontgen — 
 no.1 se ose net 
 . " fe , net 
 — — Symphony . arrange Pianofortes E. Nau . 
 mann ose o 
 — Triumphal March " ' King ' Stephen . " 
 Pianofortes ( hand ) A. Horn 
 — — Funeral March Sonata , Op . 26 . 
 Pianofortes ( hand ) A. Horn 
 DANCLA , CH . — " Les Perles d’italie de France et d’Alle- 
 magne . " thirty favourite melody , " ia Violin 
 Pianoforte . , Op , 107b? . book .. os ach 
 — " Nouvelle cole de Ja Mélodie , " easy progressive 
 piece book : — 
 books2and6 ... ; sa Sag 
 — " 
 DUPONT , A. — * Pianoforte Sch ool . " ? tee 
 FAURE , J. — " La Voix et le Chant . " Traité Pratique ... net 
 GUILMANT , A.—March Fantasia church hymn , 
 Organ , harp , orchestra . op . 44 . score , net 
 — meditation " Stabat Mater . " Organ 
 Orchestra . op , 63 : — 
 score .. 
 arrange Organ Harmonium 
 HAYNES , BATTISON.—Romance . Violin . Piano- 
 forte . op . ro ... . : ove 
 JENSEN , G. — * Sinfonietta . " " Op . 22 . String Orchestra . 
 parte « « ose 
 KIENZL , W. — " Child ren ond Life . " ry preseitation 
 Book , magnificently bind illustrate , contain 
 sores Pieces net 
 KLIEBER CH . 
 Fiaatowtk Pieces 
 LEONARD , H.—Sonata D minor ( N. Porpora ) 
 Violin Pianoforte 
 LISZT.—March W ' agaes nS Tannhiueer . " 
 Pianofortes ( hand ) F. Hermann 
 MARKULL , F. W. — ' Roland Horn . " op . 136 
 Chorus , Soli , Orchestra . orchestral Parts 

 Ir 
 arrange 

 song 

 L. van BEETHOVEN 

 english version Rev. Dr. TRouTBECK 

 Agnus Dei . ws 

 Schubert . Boor VI.—PRE lu di : S fugue _ ... ' 
 Handel . sixth book include Bach great d 
 Spohr . popular ran Works . ( 1 ) Toccata D minor , 
 Schubert . contrast oe recitative - like pass sage 
 Overture , " Acis Galatea " ae Handel . massive harmony . ) - know prelude 
 Albumblatter , no.1 ... ' Schumann . fugue D major , Spitta describe ' ' 
 Adagio ( Pianoforte Sonata , ' Op . 25 ' . Beethoven . mos lingly beautiful al ] master Organ 
 Cat Fugue oes pee eee 
 Albumblatter , . 5 ( Op . 99 ) 
 Romanze Scherzo tr ourth symphony ) 
 Air ( overture suite ] D 

 Scarlatti . pedal- -player find ex xactly uited 
 London : NovELLO , EWER Co 

 Huntsman 

 Schumann . | 
 Beethoven 

 gether Fugue section , know 
 St. Ann . lay « especial regard 

 ATALIA MACF ARREN- BERTHOLD TOURS . 
 english Translations 
 \TALIA MACFARREN & Rev. J. TRourseck , D.D. , & c 

 Paper Scarlet 
 AUBER cover , cloth . 
 fA DIAVOLO . French English ... a0 se ee 
 YASANIELLO . French English 3 6 ( 
 BEETHOVEN . 
 ( DELIO , German English 

 BELLINI . 
 \ORMA . Italian English 
 14 SONNAMBULA . italian E ngiish 
 ( PURITANI . italian English 

 J. BARNBY 

 REBEKAH . 
 BEETHOVEN 

 CHORAL FANTASIA . 
 ENGEDI . 
 MOUNT OLIVES . 
 MASS , C. 
 * MASS , C. 
 RUINS athen 

