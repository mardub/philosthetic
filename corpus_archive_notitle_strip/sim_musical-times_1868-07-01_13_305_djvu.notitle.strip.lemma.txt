_ — _ 

 incident in the life of BEETHOVEN . 
 by R. M. HAYLEY 

 to all true lover of music the name of Beethoven 
 must ever be a household word . the wonderful 
 creation of his genius possess a charm peculiarly 
 their own , and be know and value wherever 
 the art of music , of which he be so great an orna- 
 ment , be cultivate and understand . ' lhe life of this 
 extraordinary musician present detail of a varied 
 character , — combine at one and the same 
 time , commanding genius , and strange eccentricity . 
 unlike Mozart , he be no infant prodigy , astonish 
 the world by his precocious power . his wonderful 
 ability for music be , however , early discover , 
 and require time for its development ; but , by the 
 force of his genius , he awaken in his countryman a 
 recognition of his merit , and the judgment of his 
 contemporary have be since fully confirm in pro- 
 portion as his work be more know and study 

 Ludvig van Beethoven spring from a musical 
 family . his grandfather have distinguish himself 
 as a writer of musical drama and opera , and his 
 father , at the time of his birth , hold the appointment 
 of tenor singer in the Chapel of the Klector of 
 Cologne . ' the family have originally come from 
 Maestricht , in Holland , where the name be by no 
 mean uncommon in the present day , and be at this 
 time reside at Bonn , where , on the 17th December , 
 1770 , the great musical composer , on whose life we 
 be now enter , first see the light . until recently 
 there have be some uncertainty as to the exact year ; 
 and even Beethoven himself always place the date of | 
 his birth two year later than we have do ; but | 
 the matter have be now set at rest by the research 
 of Fétis pere . the mistake have arise from Beet- 
 hoven having have an eld brother , who die in his 
 infancy , with the same christian name ; but on 

 guishe himself so much in the study and practice of 
 it . at least , it be record by Fétis pére , in his life of 
 Beethoven , on the authority of M. Baden , of Bonn , 
 who be Beethoven ’s companion from infancy , that 
 his father be oblige to use violence to urge he to 
 the cultivation of his art , and that he rarely take his 
 place at the piano , except under compulsion . from 
 other source we learn that a devotion to music yery 
 soon supplant every other idea in his mind ; and 
 that his great delight as a child be to listen to his 
 father ’s performance on his favourite instrument , for 
 which purpose he would leave the society of his 
 playmate . it be not till his fifth year that his 
 father seem to have commence with he a course 
 of instruction , and the two story may be reconcile 
 by the well - know fact that the treatment of he by 
 his father from this time , be such as to give he an 
 actual distaste for that sublime art which have pre- 
 viously so entrance he . under the influence of 
 drink , to which he be addict , his father abandon 
 himself to paroxysm of fury which know no bound ; 
 and , anxious as he be to bring up his son to the 
 practice of music , nearly defeat his own end by 
 his intemperate conduct , and all but deprive the 
 world of the pleasure which the wonderful composi- 
 tion of his gifted son be calculate to inspire 

 after Beethoven have overcome the obstacle so 
 early present to the pursuit of that study for 
 which he be by nature so pre - eminently endowed , 
 he make rapid progress in the prosecution of his 
 study . ' lhe good part of his musical education 
 be receive from the bandmaster of a bavarian 
 regiment of the name of Pfeiffer , and he remember 
 with gratitude in after year the advantage he have 
 derive under his tuition , and relieve his necessity 
 at a time when he stand in need of his assistance . 
 his aptitude for the study of music soon begin to 
 attract attention in his native city , and Von der Eden , 
 one of the most distinguished violinist in Bonn , take 
 a lively interest in his progress , give he gratuitous 
 instruction as far as his own numerous avocation 
 permit ; but when the boy ’s talent be bring 
 under the notice of the Elector , Max Franz , the 
 latter desire Von der Eden to give he an hour ’s 

 consult the baptismal registry of his native town , 
 it appear that Beethoven be bear as above state , 
 and his name - sake brother twenty month early . 
 besides these two son , his father have two young 
 child , both boy . ' the elder of the two ultimately 
 become a violinist , and the other study surgery . 
 both of they follow their brother to Vienna , 
 where he himself spend the great part of his life . 
 it . have be state by a well know authority 
 { Choron and Fagolle ’s Dictionnaire des Musiciens ) 
 and , consequently , often repeat , that Beethoven 
 be the natural son of Frederick William II . , King 
 of Prussia . he appear himself to have be aware 
 of this story , for in write to an old friend towards 
 the close of his life , he say : — ' ' this report be 
 mention to I long since , but my principle have 
 always be neither to write about myself , nor to 
 take any notice of what other write about I , and 
 therefore I willingly leave to you the vindication of the 
 honour of my parent , especially of my mother . " 
 story in fact have not a vestige of foundation , 
 and all the fact respect Beethoven 's birth and 
 parentage militate against the truth of it . iti a 
 strange circumstance that Beethoven at first do 

 instruction every day at his own expense . from 
 this time Beethoven make such rapid stride in the 
 art of music , that his performance in the Chapel of 
 the Elector ’s palace , elicit great applause . he 
 be now place under the tuition of Neefe , a cele- 
 brate composer of the day , who have receive the 
 appointment in the Elector ’s Chapel , render vacant 
 by the death of Von der Eden 

 his new master , fail not to discover the re- 
 markable genius of the pupil whose education he have 
 undertake , and in place of confine he to the 
 study of inferior composition , seek to introduce 
 he , without delay , to the grand conception of 
 Bach and Handel . the work of these great com- 
 poser warm the imagination of the youthful artist 
 and he always retain for they so fervent an 
 admiration , that he never mention their name , 
 but with the high reverence . in his eleventh year 
 he have learn to perform Sebastian Bach ’s ' " ' well- 
 tempered clavichord " with such admirable taste and 
 precision , that his playing rival that of many dis- 
 tinguished professor . it be this early acquaintance 
 with the work of so profound a musician that give 

 not , although possess so much precocious / to Beethoven thatease and rapidity in execution which 
 aptitude , indicate the desire to learn which would| distinguish his performance on the piano in after 

 have be expect in one who afterwards distin 

 the MUSICAL TIMES.—Juty 1 , 1868 

 his ninth year , and after a study of thorough - bass , to 
 which Neefe then direct he , he find that great 
 success attend his effort . amongst these early 
 juvenile attempt be some variation of a march , 
 three sonata for the piano , and a few german song . 
 Most of they abound with such serious infringement 
 of the law of harmony , that Beethoven , at a 
 late period of his life , be so ashamed of these 
 defect , that he disclaim be their author . his 
 wonderful power of improvise be at this time 
 remarkable , and the fertility of his imagination 
 excite the admiration of all who hear he . Gerber 
 ( Neues Lexikon der Tonkunstler ) relate that the 
 composer Junker on hear he extemporize at 
 Cologne on a theme he have give he , express 
 great astonishment at the marvellous power of inven- 
 tion by which his performance be distinguish 

 at the age of fifteen , through the influence of the 
 Count of Waldstein , who be not only a great con- 
 noisseur , but a practical musician , Beethoven be 
 appoint organist in the Elector ’s Chapel at Bonn 

 his nobleman early appreciate the great ability 
 of the young musician , and the interest which 
 he exert in his favour be abundantly justi- 
 fie by the manner in which the duty of his 
 new office be discharge . he exhibit the same 
 mastery over the organ , as he have formerly do 
 over the piano , and continue to astonish everybody 
 by the extent of his intuitive genius and inventive 
 power . whilst still a pupil of Neefe , and assist 
 he in the musical department of the Elector ’s 
 Chapel , he pay a short visit to Vienna , in order to 
 hear Mozart , whose music he greatly admire , to 
 whom he curry some letter of recommendation . 
 this occur about the year 1790 . he play 
 extempore before Mozart , who pay very little atten- 
 tion to the performance , imagine it to be a piece 
 learnt by beart . at last the young musician , pique 
 by this indifference , beg Mozart to give he a 
 subject . Mozart mutter to himself , ' well , stay 
 a little , let I try your metal , " and write down a 
 chromatic fugue subject which , take backwards , 
 contain a counter subject for a double fugue . 
 Beethoven , though know little of the science , be 
 not take in . he work upon the subject , the 
 hidden property of which he immediately discover 
 with such force , originality , and genius , that his 
 hearer , more and more confounded , and almost 
 breathless with attention , at last rise , and walk 
 on tiptoe into the adjoining apartment where some 
 of his friend be sit , say to they , with great 
 emotion , ' ' attend tothat young man ; you will hear 
 of he one day 

 in order to gain a livelihood Beethoven be 
 oblige to have recourse to teaching , and by this 
 mean he be introduce to an amiable family at 
 Bonn , of the name of Brenning . . this acquaintance 
 exercise a beneficial influence on his character , and 
 assist materially towards his intellectual improve- 
 ment . Madame de Brenning be the widow of a 
 Court Councillor , and have three son and a daughter , 
 who be afterwards marry to Dr. Wegeler , and be 
 affectionately mention in Beethoven 's correspon- 
 dence with he , under the name of Leonora . to 
 this daughter he dedicate his first variation for the 
 piano . throughout life he cherish a fond re- 
 membrance of the happy day he have spend in that 
 family , the member of which seem to have always 
 retain asincere affection for he . harshly treat 

 by his father , he be happy in be domesticate 

 amongst such kind friend . Madame de Brenning 's 
 maternal kindness win his heart , and she gain an 
 ascendancy over his wayward spirit which no other 
 person ever possess . in one respect only her 
 influence fail ; she never could conquer his repug- 
 nance to give lesson in music . ' the dradgery of 
 teaching be intolerable to he , and , except in the 
 case of the Brenning ’s , where friendship aud gratitude 
 make he punctual , he use to shuffle out of 
 it as well he could . one day Madame de 
 Brenning having urge he very much to go and 
 give his usual pianoforte lesson at the austrian 
 Ambassador ’s , who live opposite her house , Beet . 
 hoven go away for that purpose ; but when he 
 arrive at the Ambassador ’s door , his natural dislike 
 to teaching get the well of he , and he return to 
 Madame de Brenning , say to she , ' ' for God 's 
 sake , madam , do not insist on my give this lesson 
 to - day . I will give two to - morrow . " this anti . 
 pathy accompany he through life ; and , indeed , 
 he never have any pupil ( at least whom he acknow- 
 ledge as such ) , but the Archduke Rudolph and his 
 friend , Ferdinand Ries . it be during his intimacy 
 with the Brenning family that he first imbibe a 
 taste for literature , his father be so entirely en- 
 grossed with music that he never encourage any 
 other kind of study . the direction which Beethoven 's 
 mind now take be permanent , and he acquire a 
 taste for reading , which continue with he to the 
 end of his life 

 late in the year 1791 Beethoven be appoint 
 one of the Elector ’s private musician , and be 
 thus bring into contact with the most celebrated 
 performer of the day , whereby his own fame be 
 greatly extend . in the month of July , 1792 , 
 Haydn pass through Bonn on return from his 
 first visit to England , and the Elector ’s choir invite 
 he to a breakfast at a place of resort near the town , 
 on this occasion Beethoven show he a Cantata 
 of his own composition , and desire his opinion of it , 
 the illustrious veteran praise it highly , and give 
 he every encouragement to pursue a career he have 
 so well begin . this piece , however , be never pub- 
 lishe , nor ever perform , be find too difficult , 
 especially for the wind instrument . he thus show , 
 at the very outset , that disregard of mechanical 
 facility which have always be an impediment to 
 the satisfactory performance of his music . in the 
 judgment of one of his contemporary , the perfor- 
 mance of Beethoven on the pianoforte , and for which 
 he afterwards become so famous , be , at this time , 
 not without a certain harshness and want of finish . 
 he have little acquaintance with any distinguished 
 player ; but an opportunity now present itself of 
 appreciate those finer- touch which distinguish 
 the consummate artist . having accompany the 
 Elector 's Choir to Aschaffenburg , he be introduce 
 by some friend to the choirmaster , Sterkel . this 
 celebrate musician be the first to make Beethoven 
 aware of the deficiency in his performance as re- 
 specte taste and delicacy . he be himself by no 
 mean a powerful player , but the grace and precision 
 of his style be remarkable . when he sit down to 
 the pianoforte Beethoven stand behind he motion- 
 less , with his eye rivet on the key , admire the 
 delicacy of touch which characterize : the player . 
 conversation having arise on an air with variation 
 which Beethoven have recently publish , Sterkel , 
 in allusion to its extreme difficulty , express a doubt 
 as to whether even the author himself could satis 

 earlies| 
 sayin 
 he have 
 ianofo 
 jenna 
 Lichno\ 
 musicia 
 the rest 
 compan 
 however 
 most ur 
 set , the 
 who kne 
 pay no : 
 own opir 
  conce 
 he , tha 
 Jealous r 
 ever loge 
 music the 
 when H 
 Commi 
 Albrecht : 
 point , but 
 authority 
 Haydn , 
 method oo 
 state of th 

 factorily execute it . 
 repugnance to play in public , feel pique at the 
 remark , he sit down at the instrument without the 

 jece before he , which have be unfortunately 
 mislay . he then begin to play such variation as 
 he could recollect , add other extempore in such a 
 manner that Sterkel and everybody present be 
 astonished ; and what be the more remarkable in 
 this improvisation be that Beethoven , suddenly 
 adopt Sterkel ’s style of execution , play with a 
 neatness and a delicacy which he have never before 
 exhibit . the disinclination of Beethoven to per- 
 form before other , to which we have above allude , 
 be a peculiarity which he carry with he through 
 life . ' ' he use to come to I , " say one of his 
 friend , ' * gloomy and out of temper , complain 
 that he have be force to play , even though the 
 blood tingle in his finger . I endeavour to calm 
 and amuse he , and then the conversation drop . 
 by and by , whilst talk to I , he would sit down 
 upon the stool that stand in front of the piano , and 
 whilst apparently look the other way would care- 
 lessly run over the key , and evoke the most agreeable 
 melody . I dared not make any remark , and he 

 would ultimately take his leave in a very different 
 mood from that in which he first enter my room . " 
 this reluctance to play before other be not always 
 so easily overlook , and oftentimes occasion a 
 degree of coldness between Beethoven and his 

 friend 

 notwithstanding the favourable commencement of 

 Beethoven ’s acquaintance with Haydn , their subse- 
 quent intercourse turn out anything but agreeable . 
 inthe year 1792 he be send by his constant friend 
 and patron the Elector of Cologne to study at Vienna , 
 under the great musician of the age . under this 
 master Beethoven be introduce to the study of the 
 work of the good composer ; and Haydn entertain 
 a great affection for his promising pupil , whose pro- 
 gress he mark with the great interest . it be 
 the wish of Haydo that Beethoven should acknow- 
 ledge himself as his pupil in the title page of his 
 early publication . ' to this Beethoven demur , 
 say that he have never teach he anything . when 
 he have finish his first work — a set of trio for the 
 Fenotorte , violin , and violoncello , and publish at 
 leona in 1795 — he play they at Prince 
 lichnowsky ’s , before a party of the principal 
 musician of Vienna . Haydn be present amongst 
 the rest , and join in the applause bestow by the 
 company on these charming production . he 
 however take the author aside , and advise he 
 most unaccountably not to publish the third of the 
 set , the well know trio in C minor . Beethoven , 
 who know well that this be the good of the three . 
 pay no regard to the advice ; and when he find his 
 own opinion confirm by the judgment of the public , 
 he conceive the notion , which never afterwards leave 
 he , that Haydn have be actuate by a spirit of 
 jealous rivalry . this he never forgive , nor do he 
 ever lose the opportunity of make Haydn and his 
 music the subject of ill - naturedremarks and criticism . 
 when Haydn take his departure again for London , 
 he commit his pupil to the care and instruction of 
 Albrechtsberger , a celebrated professor of counter- 
 point , but Beethoven submit to Albrechtsberger ’s 
 authority with scarcely more patience than to that of 
 Haydn . his new master be a profound theorist , 
 and teach the science in all its scholastic rigour — a 

 notwithstanding Beethoven ’s 

 to bend under the yoke of antiquated rule which he 
 feel to be mere pedantry , and be constantly lead by 
 his ardent imagination to disregard . he be contin- 
 ually therefore commit error , which his teacher 
 assiduously endeavour to correct . hence arise 
 many dispute between master and pupil , although 
 to Beethoven ’s credit , it must be say , that he never 
 lose sight of therespect and esteem due to his venerable 
 instructor . the exercise which he write under the 
 eye of his master be preserve , accompany by 
 observation of his own on the absurdity of 
 some of the theory by which it be seek to 
 restrain his lively imagination these exercise , and 
 the sarcastic remark in which Beethoven indulge 
 upon they , be publish at Vienna after his 
 death , under the title of ' ' Beethoven ’s Studies in 
 thorough Bass ; " and , though a worthless publication , 
 be still curious , as show the supreme contempt 
 Beethoven have for the task impose upon he . the 
 rule , take down of course from Albrechtsberger ’s 
 mouth , be sometimes so obscurely , inaccurately , and 
 even unintelligibly express , that the pupil evidently 
 do not comprehend their scope ; and in the example , 
 instance of bad harmony , false answer to subject 
 of fugue and other error be to be detect in almost 
 every page . " ' * Beethoven , " say Fétis , ' ' be not , 
 as have be suppose , unacquainted with the science 
 of music , but the science be too circumscribed for 
 his view . " it appear to thwart his most congenial 
 view , and he never be able to become familiar with 
 it . in some of his remark he be very amusing . there 
 be a chapter on Canon , for instance , contain 
 example of this kind of composition in all its absurd 
 and puzzling variety . in his enumeration of they 
 he mention , ' ¢ the numerical and enigmatic canon 
 which , like every thing that partake of the nature 
 of a riddle , be easy to invent than to solve , and 
 seldom yield any compensation for the time and 
 trouble bestow upon they . in former time , 
 he add , people take a pride in rack their brain 
 with such contrivance ; but the world be now grow 
 wise . " it be thus that Beethoven ridicule in 
 conversation the strict precept of the school . 
 when any infringement of they in his composition 
 be point out to he by his friend , he would term 
 they a set of pedant , and rejoin , laugh , ' oh , 
 yes , you be quite astonished and confounded , because 
 you can not find this in one of your treatise on 
 harmony ! " during his residence at Vienna , Beet- 
 hoven do not cease to indulge in pleasing recollec- 
 tion of the happy day he have spend with the 
 Brenning family , in his native city . much of the 
 correspondence which he have with they be preserve , 
 especially his letter to Leonora , the friend of his 
 youth , to whom he be able to express his feeling 
 without reserve . on send to she some variation 
 on the aria in Mozart ’s Figaro , se vuol ballare , which 
 he have dedicate to she , he thus writes:—‘‘I only 
 wish that the work be of more importance , and 
 more worthy of you . I have be torment here to 
 publish it , and I have avail myself of the opportu- 
 nity to give you a proof of my regard and friendship , 
 and of my constant remembrance of your family . 
 accept this trifle from one who highly esteem you . 
 if it afford you any pleasure , I shall be amply 
 satisfied . let it be a souvenir of the time when I 
 pass so many , and such happy hour in your house , 
 it will perhaps serve to keep I in remembrance till 

 I see you again , which I fear will not be soon 

 of the series of these Concerts which take place on the 

 for the choir include Pinsuti ’s « ' the sea hath its pearl " 
 ( which be enthusiastically encore ) , Benet ’s " all 
 creature now be merry - minded , " Stevens ’ ' " cloud - capt 
 tower " ( encore ) , Bishop ’s " Chough and Crow " ' ( also 
 re - demand ) , and Pearsall ’s " take heed , ye shepherd 
 swain . " Goss ’s beautiful glee , " there be beauty on the 
 mountain , " be well sing by the Quartet Glee Union ( the 
 member of which we presume also belong to Mr. Leslie ’s 
 choir ) , and Mendelssohn ’s Psalm , ' ' judge I , o God , " 
 be give so excellently as to cause an immediate call for 
 it repetition . Mr. Sims Reevessang in his very good style 
 Beethoven ’s " ' Adelaida " ( accompany by Mr. Charles 
 Hallé ) , " come into the Garden , Maud , " and " Tom 
 Bowling , " the last name song , especially be declaim 
 with such real pathos as to awake whatever dormant 
 tautical feeling might have exist amongst the audience . 
 Miss Edith Wynne be highly successful in her rendering 
 of " softly sigh , " from Der Freischiitz ; Madame Patey- 
 Whytock sing a new and well write song by Mr. Henry 
 Leslie , " my darling , hush ! " with the utmost teeling ; 
 and Signor Gustave Garcia give much pleasure by his 
 singing of the old ballad " black - eyed Susan . " the pianist 
 be Mr , Charles Hallé , who play Beethoven ’s " Sonata 
 pathétique " with his usual success 

 concert ancient and modern 

 yeock , who execute every passage to perfection ; and 
 much do the last movement delight the audience , that 
 tt be unanimously re - demand . the work be , we 
 uderstand , still in M.S. ; and be play on this occasion 
 for the first time . we trust , however , that those who 
 lave the charge of Mendelssohn 's manuscript will no 
 onger allow it to rest in unmerited neglect . the selec- 
 tion from Handel ’s Fire and Water Music fare no well 

 she also give the well - know romance from Fra Diavolo| viously come to grief more than once . the principal 
 encore ) and join Mr. Reeves ina duet from Hrnani , | vocalist in Bach ’s Uratorio be Miss Palmer , Mr. W. H. 
 which be 80 beautifully sing as to be re - demand , in|Cummings , and Mr. Patey . amongst the miscellaneous 
 spite of the unusual number of encore already insisted|vocal solo give , the most effective be Hummel ’s 
 upon . Mr. Sims Reeves give Weber ’s song " I would weep|‘Alma Virgo , " ( with chorus ) , well sing by Miss Banks , 
 with thee , " and " the message , " in excellent style ; andj}and Purcell ’s ' ' come , if you dare , " give with so much 
 in obedience to an enthusiastic recall , sing " my pretty|energy by Mr. W. H Cummings as to receive an un- 
 Jane , " which it be needless to say excite the most un-|deniable encore ; a compliment , however , which he only 
 bound applause . the pianist be Miss Agnes Zimmer-| respond to by bow his acknowledgment , a system 
 mann , who perform Beethoven ’s " fifteen variations,| which we need scarcely say we should like to see more 
 with Fugue , " and two posthumous study by Mendelssohn | generally follow 

 no . 1 , in bk minor , and no , 2,in f major — with her 

 glory of the Creator . the hundredth Psalm can never 
 grow old with so many youthful th : oat to keep alive its 
 unpretending appeal to our sympathy ; and to Mr. Goss 
 should all honour be give for prove that new work can 
 be fit to the occasion which shall . at the same time , add 
 to the store of solid and conscientiously write music for 
 the Church . on the present occasion , his Ze Deum and 
 Jubilate ( which have now completely take their place at 
 these festival ) be give with wonderful effect . the 
 Anthem before the prayer for the Queen — Handel ’s " Zadok 
 the priest”—Mendelssohn ’s chorale , Sleepers , wake , " 
 ( which be now invariably sing before the sermon ) and the 
 " Hallelujah " chorus , from the Messiah , be also wonder- 
 fully impressive , rise in many part indeed to positive 
 sublimity , much of the effect in the Coronation Anthem 
 be materially heighten by the organ accompaniment , 
 which be play as a duet by Messrs. Goss and George 
 Cooper . the prayer be intone by the Rev. J. Spar- 
 row Simpson , and the lesson read by the Rev. J. Lupton . 
 as usual , the response use be by Tallis ; and the 
 psalm of the day be chant by the united choir to a 
 slow chant in C , by Dr. Crotch , the child join in the 
 Gloria Patri at the conclusionof each , the steady con- 
 ducting of Mr. Shoubridge , ( a worthy successor to the late 
 Mr. Buckland ) , be of the utmost service throughout the 
 morning 

 Tue third and last of Mr. Walter Macfarren ’s 
 Pianoforte Recitals for the present season , take place at 
 the Hanover Square Rooms , on the morning of the 6th 
 ult . Mozart ’s Sonata in G , for pianoforte and violin , and 
 Beethoven ’s ' ' Kreutzer " Sonata , for the same instrument , 
 be most artistically perform by the Concert - giver and 
 Mr. Henry Holmes , a violinist who seem to be rapidly 
 make his way as a thoroughly satisfactory exponent of 
 the high classical work . Mendelssohn ’s 8th book of 
 Lieder ohne Worte create the usual effect with the audience , 
 two — the Presto , in C , and the Allegro vivace , in a,—bein 

 encore : the first , however , only be repeat . all these 
 little gem be perform by Mr. Macfarren with a true 
 appreciation of their refined beauty . Schumann ’s An- 
 dante and variation , in b flat , ( Op . 46 ) , for two piano- 
 fortis , afford Mr. Macfarren ’s clever pupil , Miss Emma 
 Buer , an opportunity of join her master in a duet 
 which successfully display her well cultivate power ; 
 and the programme be pleasingly varied , as usual . by 
 some of Mr. Macfarren ’s elegantly write piece , two of 
 which , " La Fete d’Eté . " anda Romance , call " Bianca , " 
 have not be hear before . the first of these be a light , 
 trip Bohemienne , full of character , and not too 
 difficult for those who have train their finger to dance 
 upon the key ; and the second , a graceful " song without 
 word , " the melody of which move almost throughout 
 with the bass , the quaver accompaniment be play 
 with the right hand . Miss Robertine Henderson be 

 t+ month at St. James ’s Hall , with the usual success 

 rs . Macfarren ’s selection of pianoforte music include 
 Beethoven ’s ' ' Sonata Pastorale , " the « Menuetto Capric- 
 cioso " ' from Weber ’s Sonata in A flat , the 8th book of 
 Mendelssohn ’s Lieder ohne Worte , & c. , all of which . be 
 most excellently perform , and receive with the warm 
 applause by the audience . the singer have be Miss 
 Banks , Miss Robertine Henderson , Madame . Patey , Mr. 
 W. H. Cummings , and Mr. Patey 

 Tx account furnish we of the first Trien- 
 nial Festival give by the Handel and Haydn Society at 
 Boston , in the United States , in May last , be full of praise , 
 both of the music and the performance . the Americans 
 appear frantic in their estimation of Wagner ; for in 
 Watson ’s Art Journal we be tell that the Zannhiéuser 
 Overture " contain some of the grand orchestral effect 
 ever imagine ; and that it be a creation of such splendid 
 proportion that it would confer a live name upon its 
 composer , be it the only work that he have write . " 
 the accuracy of the news from London may be question , 
 if we may judge by the paragraph in the same journal , 
 state that Mendelssohn ’s Reformation Symphony be 
 produce at the Crystal Palace Concerts , " under the 
 direction of that talented conductor , Dr. Ganz . " let we 
 hope that , should our Handel Festival be comment upon 
 in America , the name of those engage in it may be 
 more correctly quote 

 Mr. Freperick WEsTLAKE give an Evening 
 Concert at the Beethoven Rooms on the 26th May , when 
 a programme , select with the utmost taste and judgment 
 be provide . Mr. Westlake ’s pianoforte playing be 
 highly successful , both as regard executive power and 
 true artistic feeling ; and his performance be receive 
 with much applause . amongst his most effective piece 
 be Dr. Bennett ’s Trio ( Up . 26 ) for pianoforte , violin , and 
 violoncello ( in which he be ably support by Messrs. 
 Henry Holmes and Aylward ) , Niels Gade ’s pianoforte and 
 violin Sonata ( Op . 21 , no . 2 ) , ( violin , Mr. Henry Holmes ) , 
 Schumann ’s " Andante Espressivo , " with variation , for 
 two pianoforte ( with Mr. Walter Macfarren ) , and his own 
 MS . ' Duo Concertante , " forviolinand violoncello , which be 
 excellently perform by the composer and Mr. Aylward . 
 Mr. Westlake also give two solo ( Henselt ’s « Repose 
 d’Amour , " and Schubert ’s Impromptu , up . 90 ) with good 
 effect . Miss Robertine Henderson be the vocalist ; and 
 in two M.S. song by the Concert - giver be much ap- 
 plauded , Mr. Walter Macfarren conduct 

 Mavame Everne Oswatp give a Matinée at 
 St. George ’s Hall on the 26th May , when a selection of 
 strictly classical music be perform , well choose to 

 display the Concert - giver ’s qualification as a pianist of 

 the legitimate school . we must especially mention 
 Weber ’s Solo Sonata in D minor ( Op . 49 ) , and Beethoven ’s 
 " Kreutzer " Sonata ( in which . she be join by Herr 
 Straus ) , both which exacting work be render by she 
 in a manner which thoroughly prove her right to the 
 position she aspire to . several other composition of the 
 high class be include in the programme . the 
 vocalist be Madame Sauerbrey , Madame Raby Barrett , 
 and Herr Wallenreiter . we must not omit to mention , 
 that , in . addition to Herr Straus , the service of Mr. Paque 
 be secure for the violoncello , Herr Wilhelm . Ganz 
 be the conductor 

 WE understand that the College of Organists 

 AN account furnish we of a Concert give 
 by the pupil of Westbourne Park College , on the 22nd 
 ult . , contain such a powerful list of establish vocalist 
 and instrumentalist who be say to have " assist " they , 
 that it would appear to be a Concert give by well know 
 artist " assist " by the pupil . in confirmation of this , 
 we be tell that the artist be receive with the great 
 enthusiasm , " the pupil also receive much applause . " 
 as this be an annual Concert , we hope next year to 
 hear more of the pupil , and less of the " artist 

 Mr. E. H. Tuornz , give a Matinée Musicale , 
 at the Beethoven Rooms , on May 14th the artist on 
 the occasion be Miss Ida Thorne , Miss Julia Elton , and 
 Mr. W. H. Cummings . Pianoforte , Mr E. H. Thorne ; 
 violin , Herr L. Ries ; violoncello , M. Paque . conductor , 
 Signor Randegger , Mr , Zerbini , and Mr. Knapp . the 
 Whole of the performance be of the high order . and 
 give great satisfaction toa fashionableaudience . Mr Thorne 
 display unusual executive power and irreproachable 

 Messrs , Chatterton , Regondi , and Cheshire for their 

 taste in several classical composition . his rendering of 
 Beethoven ’s " Sonata Appassionata " be as intelligent as 
 his performance be artistic 

 the ROYAL ACADEMY of MUSIC 

 in number , of course occupy a considerable space , fill 
 at half - past 

 the transept , and a portion of the nave . 
 three o’clock , the clergy enter the church in procession 
 by the west door , and walk up the nave , take their 
 place in the chancel . the service be full choral , 
 prayer heing intone by the Rev. E. D. Garven . the 
 first lesson be read by the Rev. Canon Barclay , and the 
 second by the Ven . Archdeacon Pollock . the psalm 
 be the 93rd ( Sir F. A. G. Ouseley ) , and the 94th 
 ( Beethoven ) , both of which be well render . the 
 Deus Misereatur , ( of Kelway ) , be sing with much 
 power , and the response be give by the choir with 
 admirable precision . the anthem be by John Goss , 
 « © taste , and see how gracious the Lord be . " before the 
 sermon , the hymn « 0 love , who form’dst I to wear , " 
 from hymn ancient and modern , be sing with much 
 effect . after the sermon , the choir give Handel ’s grand 
 hallelujah chorus . great praise be due to Mr. Towers , 
 the choirmaster , and to his able assistant Mr. Arnold , for 
 the efficient manner in which they have train the choir 
 compose the association . Mr. Towers be the organist 
 on this occasion 

 Brentwoop.—On the 11th ult . , the double 
 feast of S. Barnabas , the Apostle , and of Corpus Christi , 
 be observe with great honour in Brentwood Church , 
 the day having be choose to celebrate the choral 
 festival of the United Parochial Choirs , comprise in the 
 Brentwood District of the Association for promote the 
 improvement of Church Music . the Venite be give 
 to a chant in f major , by the Rev. W. Felton , and the 
 psalm to one in e flat , by Barrow . the Anthem , 
 ' teach I , o Lord , " ( a quartet from an Anthem , by 
 Thomas Attwood ) , and Tallis ’s prece and response be 
 quite refreshing , as genuine specimen of what sacred 
 music should be . in the evening service an Anthem , by 
 C. Gardner , Esq . , the choirmaster , be most effectively 
 give , especially the fugue , " for there the Lord . " the 
 composition contain some good point , and be smoothly 
 write for the voice throughout . special mention 
 should also be make of Joseph Barnby ’s beautiful setting 
 of the hymn , " @ Paradise , " and Arthur H. Brown ’s 
 simple and unaffected tune to Dr. Neale ’s translation of a 
 hymn , by S. Anatolius , a.p , 454 , " ' the day be past and 
 over . ' great praise be due to Mr. Gardner for the 
 admirable manner in which he have train the choir ; and 
 a word of commendation must also be award to Mr. A. 
 H. Brown , who preside most efficiently at the organ 

