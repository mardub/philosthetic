while listening to this section that the scene might 
better be depicted orchestrally.) And who will 
forget that glorious high B natural of the sopranos 
near the end of Part I.? Mr. John Coates as 
Gerontius repeated the success he had made at 
Worcester; and Mr. Ffrangcon-Davies impressively 
sang the music assigned to the bass voice. Miss 
Muriel Foster worthily replaced Miss Marie Brema 
(absent through illness) by her rendering of the 
Angel music in Part II. This young singer is a true 
artist, and the possessor of a beautiful voice. Her 
rapid rise in public estimation is well deserved, and 
her refined singing always gives pleasure. Chorus 
and orchestra spared no pains to do justice to a work 
that has become epoch-making in the history of 
English music

Following the Prelude to Act III. of ‘ Lohengrin’ 
and Beethoven’s Violin Concerto (soloist, M. Ysaye) 
came Dr. Elgar’s ‘ Coronation Ode,’ a setting of some 
fine verses by Mr. Arthur C. Benson. This work was 
to have been sung by the Sheffield choir at the gala 
opera performance in London as part of the Coro- 
nation celebration, but the King’s illness necessitated 
the abandonment of the function, to the intense dis- 
appointment of the Sheffield singers. No traces of 
this disappointment were, however, noticeable in their 
interpretation of Dr. Elgar’s melodious strains. The 
rendering of the unaccompanied chorus * Daughter 
of ancient Kings’ proved to be the perfection of 
refined chorus-singing. The purity of tone, the ideai 
enunciation of the words—.g., ‘ purest, stateliest "—no 
less than the perfect blending of the voices, reached 
the highest points of choral technique. The final 
chord of this movement contains a low C for the 
basses; this note, sung softly and of good tone 
and absolutely in tune, produced a wonderful effect 
—‘successful soundings of the deep C’s,’ as 
one of the audience nautically observed. The 
principals in the ‘Ode’ were Miss Agnes Nicholls, 
Miss Muriel Foster, Mr. John Coates, and Mr. 
Ffrangcon-Davies; and Dr. Elgar conducted a 
performance which must have afforded him as much 
pleasure as his music gave to the audience

Upon reassembling in the evening we were favoured 
with the ‘Wandrer’s Sturmlied’ of Richard Strauss, 
performed for the first timein England. This setting 
of Goethe’s words, for six-part chorus and orchestra, 
is an early work of the most modern of German 
composers, and hardly represents the true Strauss. 
Contrast, not unwelcome, was afforded in Mozart’s 
delicious G minor Symphony, in which Mr. Wood 
gave evidence of his genius as a conductor of orches- 
tral music. The work was most beautifully played. 
Strauss was again in evidence when two songs from 
his Opus 33, entitled‘ Hymnus’ and ‘ Pilgers Morgen- 
lied,’ were sung by Mr. Bispham. Dr. Cowen then 
conducted his setting of Collins’s ‘Ode to the 
Passions,’ a work which has attained great popularity, 
and to which all concerned did ample justice. A 
selection from ‘Israel in Egypt’ concluded the day’s 
music. Suffice it to say that in old Handel’s stately 
strains Sheffield again triumphed gloriously

DR. JOACHIM 
PROFESSOR KARL

MR. BERTHOLD TOURS 
Dr. G. M. GARRETT 
MR. W. T. BEST 
*REV. SIR F, A. 
BEETHOVEN aie 
MOZART (At the age of 7) 
*DR. WILLIAM POLE

KLINDWORTH

The weakest saint upon his knees, 
shows us rather a mild and innocuous devil, as compared 
with the fiends of ‘The Golden Legend ’ or of ‘ Eden,’ to 
say nothing of the Mephistopheles of Berlioz, Gounod, or 
Boito. Possibly a more experienced actor than 
Mr. Watkin Mills might have made Dr. Parker's Satan 
seem more vividly imagined; nothing in the Act is as 
beautiful as its conclusion, in which the choir takes a 
prominent part. Of the beautiful third part, with its 
splendid sequence of ecclesiastical choruses, and its fine 
realization of Saint Christopher’s feat in bearing the 
Christ-child through the floods, it is not necessary to 
speak again in this place: it is only to be recorded that 
Mr. Andrew Black sang ‘it with much vigour, that Miss 
Agnes Nicholls was successful as the Queen of the first 
Act and the Angel of the last, and that Mr. Saunders was 
good as the King turned Hermit

In the first part of the same concert came Dr. Elgar’s 
new ‘Coronation Ode,’ first heard the week before in 
Sheffield. It was curious that the little chorus ‘ Daughter 
of Ancient Kings,’ which provoked the only encore of the 
festival at Sheffield, fell almost flat at Bristol. Still the 
work as a whole delighted the audience, and the usual 
wagging of heads testified to the popularity of the big tune 
on which the final numbers are based. Between this and 
‘Saint Christopher,’ both of which were conducted by their 
composers, Miss Adela Verne played Liszt’s ‘ Hungarian 
Fantasia,’ with orchestra, in admirably finished style. I 
seemed to be raining eminent pianists, for not only this 
clever young lady appeared, but on the first night of the 
Festival, when Dr. Grieg was to have been present, 
Mr. Borwick played the Norwegian composer's single 
pianoforte concerto to perfection (giving also Saint-Saéns’s 
brilliant ‘Africa’ Jater on), and on the last night of the 
meeting, M. Paderewski played in Beethoven's ‘ Emperor ’ 
concerto, and his own beautiful, poetic, and immensely 
difficult ‘ Polish Fantasia.’ He was heard at his best in 
the latter work, and being required to play an encore 
gave Mendelssohn's ‘ Song without words’ in B fiat, 
with the strong flavour of the chase about it

The splendid male-voice organisation of 350 voices 
which, under Mr. Riseley’s direction, has attained such 
eminence, took part in Mendelssohn's ‘ Antigone ’ music 
and Grieg’s ‘Landerkennung,’ the vocal soloist in each 
being Mr. Plunket Greene, and the reciters in the former 
Mrs. Brown-Potter and Mr. Rudolph de Cordova. 
Mrs. Brown-Potter also declaimed the ballad ‘ Bergliot,’ 
for which Grieg wrote music ; she would come nearer the 
ideal of the elocutionist’s art if, when reciting ‘ through

THE NORWICH MUSICAL FESTIVAL. 
(BY OUR SPECIAL CORRESPONDENT

The twenty-seventh triennial Festival at Norwich, and 
the eighth which has been under the artistic direction of 
Mr. Randegger, took place on the 21st to the 24th ult. 
For the general public the interest of a festival lies 
chiefly in its relation to current art, so I shall not 
apologise for baldly recording performances of such 
well-known works as ‘Elijah,’ ‘The Golden Legend,’ 
Verdi's ‘ Requiem,’ ‘ The Redemption,’ and Beethoven's 
C minor symphony, and concentrating my criticisms on 
the less hackneyed features of the Festival

The first to be heard was Sir Hubert Parry’s ‘ Ode to 
Music, which is, of course, not a novelty in the strict 
sense of the word, having been heard on two occasions 
at Royal College concerts. But its first public appearance 
was at the opening concert of the Festival. Suffice it to 
say that the noble music fits Mr. A. C. Benson's fine 
poem ‘like a glove.’ The solo parts are not prominent, 
but there are five of them, which were taken by Madame 
Albani, Miss Kate Moss, Miss Ada Crossley, Mr. Ben 
Davies, and Mr. Robert Radford

result of Mr. Carnegie’s benefaction

At the Palette Club’s usual monthly concert, on the 
Ist ult., the orchestra of the Club, under Dr. Hardy, 
gave a highly creditable performance of Berlioz’s ‘ King 
Lear’ Overture, Beethoven’s C minor Symphony, and 
German's ‘Henry VIII.’ Dances. Vocal solos lent variety 
to an interesting programme

It is much to be regretted

Although the musical work of our Training Colleges 
here does not come under public notice in the usual way, 
it may be of interest to mention the choral music selected 
for study during this session. At Nétre Dame College, 
they have in hand Wilfred Bendall's ‘The Lady of 
Shalott,’ Mendelssohn’s ‘ Laudate pueri,’ and Schubert's 
‘Serenade’ for alto soloist and chorus; at the Church of 
Scotland College, Arthur Somervell’s ‘ Ode to the Sea,’ 
Part III. of Coleridge-Taylor’s ‘ Hiawatha,’ and Elgar’s 
‘ The Banner of St. George’; at the United Free Church 
College, Haydn’s ‘ Spring ’ and Gounod’s ‘ Gallia

Mr. Philip E. Halstead, a leading local pianist 
associated with the Verbrugghen Quartet (Messrs. 
Verbrugghen, Freeman, Haigh and Foulds) gave the first 
of a series of four chamber concerts on the 2oth ult. 
The programme included Beethoven’s fourteen variations 
for pianoforte, violin, and. violoncello, Schubert’s string 
quartet ‘Death and the maiden,’ and Dvorak’s quintet 
for pianoforte and strings (Op. 81). With regard to both 
ensemble and general intelligence the performance was 
one of great merit

MUSIC IN GLOUCESTER AND DISTRICT 
(FROM OUR OWN CORRESPONDENT

Professor Horatio Parker was present at the first 
rehearsal of the Gloucester Orpheus Society, and 
promised to write a part-song especially for the Society. 
An item of the programme for the Society’s annual 
concert on January I, 1903, is a setting of a Coronation 
Ode, written by Mr. H. Godwin Chance (a member of 
the Society) and set to music by Sir Hubert Parry, its 
President

The programme for the season issued by the Chelten- 
ham Musical Festival Society (under the direction of 
Mr. J. A. Matthews) includes Elgar's ‘ Coronation Ode,’ 
‘The Hymn of Praise,’ ‘The Golden Legend,’ the 
‘Messiah,’ and Beethoven's ‘ Eroica’ Symphony

The Cirencester Choral Society are rehearsing Sir 
Charles Stanford's ‘ Revenge’ and Bennett's ‘ Mav 
Queen

MUSIC IN LIVERPOOL AND DISTRICT. 
(FROM OUR OWN CORRESPONDENT

The first concert of the sixty-fourth season of the 
Philharmonic Society took place on the r4th ult. M. 
Kocian made his first appearance in Liverpool, and the 
talented young violinist achieved a large measure of 
success. Mr. Andrew Black appeared in the place of 
Mr. Van Rooy, and was in particularly good voice, his 
singing of ‘ Wotan’s Abschied’ (‘ Die Walkiire’) being 
excellent. The orchestra, under Dr. Cowen, and with 
Mr. A. W. Payne as principal violin, played Beethoven’s 
C minor Symphony with much care. The chorus were 
let off lightly with one item, to wit, Sullivan's ‘ Song of 
Peace’ from ‘ On‘Shore and Sea

M. Godowsky made a great impression on his first 
appearance in Liverpool, when he played at the Small 
Concert Room, St. George's Hall, on the 16th ult.; anda 
large audience welcomed M. Paderewski after an absence 
of several years, at his recital at the Philharmonic Hall, 
when he achieved all his usual success

MUSIC IN MANCHESTER. 
(FROM OUR OWN CORRESPONDENT

Though the string quartet formed in Manchester by 
Dr. Brodsky has only been in existence a few years, it may 
now be regarded as firmly established among the institu- 
tions of musical Manchester. Not having heard his earlier 
Leipzig quartet—which included Hans Becker, Novacek, 
and, I think, Klengel—I can draw no comparison. But 
that the Manchester combination can hold its own among 
the finest quartets in existence is now generally recognised. 
At the first Brodsky concert of the present season, which 
took place on the 8th ult., Mr. Simon Speelman, 
the unsurpassed viola- player, was absent from 
his usual place, in consequence of recent family 
bereavement, but a remarkably efficient substitute 
had been found in Mr. Catterall — Dr. Brodsky’'s 
young pupil—who is rapidly winning recognition as a 
string-player of first-rate ability (having already become 
a member of both the Hallé and Bayreuth orchestras). 
In other respects the quartet was constituted as usual— 
second violin, Mr. Rawdon Briggs, violoncello, Mr. Carl 
Fuchs. The programme consisted of Volkmann's G minor 
and Haydn's B flat major Quartets, and Beethoven's 
Third Sonata for pianoforte and violin. The last-named 
work, which naturally occupied the middle place in the

programme, afforded an opportunity of hearing a youn 
pianist named Edward Isaacs,—like Mr. Catterall, 4 
pupil of the Royal Manchester College—who in the 
main acquitted himself very satisfactorily, playing 
with neatness and good taste, and falling short 
of masterly style only by reason of a slightly dry 
tone. The single point that in any way detracts from 
the success of these concerts is the want of a satisfactory 
hall

With Dr. Brodsky’s first concert, the Manchester 
musical season may be said to have opened, though one 
or two song recitals and the first performance here of 
German's ‘ Merrie England’ came earlier. For the 
greater public the real opening ceremony was, however, 
the Hallé concert of the following Thursday, when, with 
four masterly works in as many different and sharply 
contrasted styles, Dr. Richter proved that the Hallé 
orchestra maintains in nearly all respects the extremely 
high standard to which he had brought it by the end of last 
season. ButI begin to fear that some occasional blatancy 
in the heavier brass instruments is an ineradicable fault of 
the present combination. Here one traces the influence 
of those brass-band contests so prevalent in the North of 
England. It is true that Dr. Richter has very greatly 
mitigated the fault in question, but it is still liable to crop 
out again early in the season. The programme consisted 
of the ‘ Meistersinger’ Overture, Strauss’s ‘Tod und 
Verklarung,’ Elgar’s Variations, and Beethoven's Seventh 
Symphony. Though there was no soloist the concert was 
extremely well attended, and Dr. Richter’s reception was 
most enthusiastic

At the concert given on Monday, the 2oth ult, 
by that most ancient—and at the present time 
wonderfully flourishing — institution the Gentlemen's 
Concert Society, the same world-famous conductor 
presided over a smaller orchestra, and charmed a more 
exclusively fashionable audience with his interpretations 
of Sterndale Bennett's ‘Naiads’ Overture, Schumann's 
Overture, Scherzo and Finale, the two new dream pieces 
by Elgar, and Handel’s ‘ Partenope’ Overture, the final 
piece taken from the programme of a concert given by the 
same Society, or its lineal predecessor, about the middle 
of the eighteenth century—I mean the eighteenth, not the 
nineteenth! The singer was Miss Alice Nielsen, whose 
youthful and vivacious manner led to happier results in 
the cavatina from ‘ Rigoletto’ than in Gounod’s ‘ Jewel 
Song.’ In the Verdi air the audience had the unwonted 
pleasure of hearing Dr. Richter play a pianoforte accom- 
paniment, which, in spite of fingers cramped by 
overlong holding of the baton, he did not fail to bring 
out with exquisite rhythmical elasticity

MUSIC IN YORKSHIRE. 
(FROM OUR OWN CORRESPONDENT

It must be that Yorkshire musicians, though energetic 
enough when they do get to work, are slow in putting on 
their harness, for at the time of writing I findI have only 
one concert of first-rate importance to record. There are, 
however, the pleasures of hope to indulge in, for the chief 
concert-giving societies have issued programmes of a 
fairly interesting nature. The Leeds Philharmonic and 
Subscription Concerts, for example, will have given 
‘Israel in Egypt’ before this appears in print, and 
promise also Stanford's ‘Eden,’ Richard Strauss’s 
‘Wanderer’s Sturmlied,’ C. Wood's ‘ Dirge for two 
Veterans,’ the ‘ Vatergruft’ of Cornelius, and Elgar's 
pleasant choral suite, ‘From the Bavarian Highlands.’ 
The Leeds Musical Union are giving Elgar's ‘ Caractacus’ 
and ‘ Coronation Ode,’ Dr. Parker's ‘ Star Song,’ Gluck’s 
‘Orpheus,’ and Parry’s ‘ Song of Darkness and Light,’ 
while they are pleasantly varying the invariable Christ- 
mas ‘ Messiah’ concert by giving a selection from that 
work, together with portions of an equally venerable, but 
less hackneyed composition, the ‘ Christmas Oratorio’ of 
Bach. At the Bradford Subscription Concerts it is not 
proposed to give any choral works of peculiar interest, 
but Beethoven's Choral Symphony and the third Act of 
‘Lohengrin’ will take up one programme, and ‘ St. Paul’ 
a second

The Bradford Festival Choral Society, besides supply- 
ing the chorus on these occasions, will give on its own 
account Dr. Cowen’s ‘Coronation Ode,’ and was to 
perform, on the 24th ult., too late for notice, ‘Israel in 
Egypt,’ which, after being shelved for some time past, 
seems to be enjoying a ‘ run

HamBurG.—Massenet’s remarkable operatic legend, 
‘Le Jongleur de Notre-Dame,’ with its poetical and 
dramatically effective libretto, was produced at the 
Stadt Theater for the first time in Germany, on 
September 25, under capellmeister Gille’s direction, 
and with very efficient artists in the solo parts

Leripzic.—A series of subscription concerts, given by 
the excellent Chemnitz Municipal Orchestra, was opened 
on the 6th ult., under Herr Weingartner’s direction. 
The programme was devoted entirely to Liszt's com- 
positions, and included the symphonic poems ‘ Tasso’ 
and ‘ Hungaria,’ and the Pianoforte Concerto in A major, 
Herr A. Reisenauer being the pianist. The first 
Gewandhaus concert of the season, uader Herr Nikisch’s 
conductorship, was dedicated to the memory of the late 
King Albert, and included vocal numbers from Handel's 
oratorio ‘ Deborah ’ and Bach’s cantata ‘ Ich habe genug, 
as well as Wagner’s funeral dirge from ‘ Gétterdam- 
merung,’ and Beethoven's ‘ Eroica’ symphony. It wasa 
deeply impressive performance.—— Herr Stephan Krehl 
has been appointed to the senior professorship of com- 
position at the Conservatorium, in the room of the late 
Professor Jadassohn

Lunp(SWEDEN).—The doyen of Scandinavian organists, 
M. Delan, chapelmaster of the cathedral, recently cele- 
brated his eighty-eighth birthday. He has pursued his 
avocation without interruption for over sixty years

March, by Halversen. Mr. D. Sinclair played a move. 
ment from Weber’s Concerto in E flat asa clarinet solo, 
and Miss E. Barber and Mr. W. A. Bowring were the 
vocalists

Winpsor.—The programme of Messrs. Thomas Dunhill 
and Edward Mason’s third concert of Chamber Music 
at the Royal Albert Institute on the 9th ult. was very 
interesting. It comprised Beethoven's Quartet for 
strings inG major (Op. 18, No. 2), a MS. quintet for 
strings and horn in F minor by Mr. Dunhill, and 
Mendelssohn’s Concert Variations for pianoforte and 
violoncello, Op. 17. The artists included the Grimson 
Quartet and Mr. Hale Hambleton, in addition to the 
concert-givers, with Miss Perceval Allen as vocalist

Correspondence

In non-apostrophising the place where the remains of 
Dr. Crotch lie buried, we followed the form of its name 
as stated in the legal notices affixed to the door of the 
church at the time of our visit ; and as the various issues 
of so accurate a book of reference as Kelly's Directory of 
Somerset and, indeed, ‘ The Clergy List,’ give the same 
form, we certainly err in very good company.—Ep. M.T

JBEETHOVEN’S HOUSE

Dear Sir,—Will you permit me to correct a slight 
error which found expression in last month’s Musicat 
Times? It is there stated that the Schwarzspanierhaus— 
the Sterbehaus of Beethoven—bears no commemorative 
mark of the fact that the great master drew his last 
breath within those walls

Visitors who approach this interesting spot from the 
Beethovengasse, which leads into the spacious courtyard, 
will look in vain for such a sign; but the outside of the 
building, which forms part of the Schwarzspanierstrasse 
—and here are situated the rooms which Beethoven 
occupied —bears a small tablet, which states in_ the 
briefest terms the fact, together with the date of the 
great composer's death

The inscription, time and weather-worn, is none too 
clear, and may easily be overlooked. Still, there it 15, 
and in justice to the music-lovers of Vienna, I shoul 
like to testify from personal observation that they have 
not overlooked the sombre days towards the end of 
March, 1826, whose record makes sad the hearts of 
musicians.— Yours faithfully

A. R. C. O.—The works here enumerated are without

soprano and contralto solos: ‘ Festgesang’ (Mendelssohn), 
‘Rock of Ages’ (Bridge), ‘Zion’ (Gade), ‘Song of 
Thanksgiving’ (Cowen), ‘The Procession of the Ark’ 
from the ‘ Rose of Sharon ’ (Mackenzie). 
_ ScopuLus.—(1) There is much to be said fro and con 
In regard to the sonatinas in F and G ascribed to 
Beethoven, but perhaps the balance of opinion is in 
favour of the con. (2) Try Naumann’s ‘ History of Music’ ; 
the chapters on English music are by Ouseley

Portion.—(1) Mr. J. Brotherhood, of New York, may

