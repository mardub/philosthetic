content . 
 Arrival visitor . . . Hofmann 
 Bright Buxom Lasses , ' * Martha " Flotow 
 Percy E. Fletcher 
 Je D. Davi 

 p 
 Beethoven 

 Instrumental Band , 
 Little March , 

 composition canon 
 disappear 

 music , remark , 
 occur time . grant 
 music vertical horizontal 
 aspect , successive , horizontal 
 aspect far transcend vertical cause 
 latt dwindle comparative insignificance ? 
 succession mean , 
 succession possible ? regard 
 individual unit , beat , single sound , 
 isolated chord , _ 
 far actual musical significance concern . 
 , true , true chord , 
 chord complex sound . Beethoven 
 begin end 4//egretto major 
 Symphony isolated chord . 

 chord mean . 
 contrary , stroke genius . 
 

 reviewer . 
 critic praise 

 th 
 general princi ] 
 othe event swan emerge 
 geese , 
 alternatively cautious line 
 fnal pronouncement composer w ork | 
 ‘ ould awhile ; | 
 throw fail | 
 greatness Milhaud Pratella , 
 s predecessor miss Beethoven 
 agner | slip certain early critic 
 bring forward — generally 
 second - rate composer smart unfavour 
 abl hold warning . ° 
 , parallel 
 Beethoven Wagner dozen present 
 jay experimentalist behalf utmost 
 rabid adherent — 
 
 jone . ' ' 
 e , piece pioneer 
 tk , point generally 
 pioneer necessarily 
 e follow . merely hack 
 swamp , zm / fasse 
 lite perceptible onlooker , case 
 m notice lie 
 ngheadedness . remain high 
 rest un 

 e review- 
 

 rsuch reason hesitate 
 eave fence come unmistakably 
 generally " reactionary . 
 good company evident 
 rus hear , hear ! ' reach 
 mt unexpected quarter . 
 conclusive evidence burst 
 bubble 

 progressive ' 
 ' awful warning , ' long 
 doubt allege lack appreciation 
 great composer contemporary 
 time occasion overhaul 
 graphy Beethoven , example , find 
 wonder long - hold popular legend 
 Beethoven neglect genius singe 
 fart world listen . 
 ay mind like think great 
 ‘ st lonely soul , waste sweetness 
 ndifler ent antagonistic world , 
 obscure unhonoured grave . 
 far great composer concern , 
 4y mind easily possession the| 
 ' act read Mr. Ernest Newman late book , 
 musical Critic Holiday . ' ( Cassell , 12 . 6d 

 f 

 rate , 

 
 cconsciot 
 Newman dissection Lobe 
 ellort 
 good reading , 
 
 write 
 low goad 
 extravagance 
 ' progressive ' day . , 
 example , Schumann rank 
 Beethoven unnaturally reply 
 stress obvious fault Schumann , 
 similarly tempt belittle 
 behalf 
 

 Mr 

 moon . ' word ' reactionary , ' way , js 
 |discusse Mr. Newman . antithesis * 
 prefer ' classic ' ' romantic ' 
 progressive , ' conservative ' ' radical , @ 
 ' age ' ' youth . ' , neyer g 
 time young people . 
 old . lot 

 musician - day pro - beethoven 
 Schonberg ; conservative young 
 hardly start career , ang 
 radical old finish theirs 
 ' reactionary ' ' progressive ' 
 meaning music , appear progress 
 - day prove year ’ time 
 false . real ' progressive ' 
 prefer stand 
 direction 

 reactionary 

 theory inevitable failure 
 ) recognise great composer fallacy , | 
 lern fallacy . imagine 
 f late 19th century , | 
 Wagnerians . fancy | 
 ) music - lover period , , 
 1 1850 , tell , tear eye , 
 sure sign composer genius 
 world ridicule revile , great | 
 composer look posterity recognition , 
 laugh face . age , think , 
 pretty accurate idea composer 
 great credit . man 
 highly praise contemporary , 
 vast majority case , man world 
 recognise - day big figure epoch 

 mention Beethoven 
 supposedly neglect genius . Mr. Newman find | 
 mere page sufficient knock | 
 idea . , Beethoven tell 
 young man publisher | 
 fight privilege | 
 bring work ; drawing ’ power is| 
 fact co - operation con- 
 stantly seek specially important concert | 
 Vienna ; new work obtain | 
 Newman 

 dashing 

 . 
 lable , W 

 quote article appear musical 
 | journal recently ( fancy J / usica/ Times ) , 
 consist extract contemporary english 
 | criticism Beethoven , design prove 
 futility blindness critic . 
 London writer fall foul ' Diabelli ' 
 Variations , add air ' derive 
 additional interest having select 
 ' theme great musical genius 
 century . ' Mr. Newman quote similarly handsome 
 epithet english critic period , 
 | add 

 deluge life - time eulogy 
 sort universally misunderstand , 
 composer - day pray similar 
 misunderstanding 

 music foreign press 

 BEETHOVENIANA 
 \pril Zeitschrift fiir Musik 
 examine Beethoven sketch 
 reveal 
 method composition 

 wissenschaft , 
 Alfred Loren ; 
 ' Eroica , ' 
 Beethoven 
 artistic ideal . 
 Zettschrift fw S practically 
 special Beethoven number . 
 instalment essay Dr. Karl Nefon ' Beethoven 
 Politics , ' excellent article F. X. Pauer 
 LBeethoven final movement , special 
 reference /’vale second Symphony . 
 Dr. Edmund Richter write Johann Vincenz 
 early 

 

 Richter ( 1788 - 1853 ) , 

 champion Beethoven music . Dr. Alfred 
 Heuss devote care task prove 
 supernumerary bar ( 25th ) 

 movement ' Pastoral ' Symphony 

 funny private joke classical piece music 
 s play appear bad - mannered . 
 difficult indicate wave 
 hand eye disapprovingly , 
 music responsible 
 lamentable breach good taste . 
 suppose , 1s concert : 
 concert Bach play , 
 prove undoing . throug 

 ourse self - control stand , 
 instance , Beethoven Symphonies 
 twitch—-some Schersos try 
 severely , know danger - spot , 
 brace rigidly meet . Bach 
 n lively mood fail time . 
 know ; iti . example , time 

 Bach 

 composer learn language 
 somebody , unreasonable expect 
 come maturity day . 
 anybody want strive originality 
 sake , consciously eliminate music 
 suggest influence : 
 composer cut borrow 
 cut interesting . 
 time true composer 
 ultimately arrive feel , 
 dependent day , 

 Sonata Beethoven , debt 

 , stand clear Mozart Haydn 
 early Brahms Sonatas Beethoven late . 
 , , kind immature work 
 , sensitiveness receptivity 
 composer , deep personal feeling 
 ; class Laurence Powell 
 ' Phacelia ' ( Op . 17 ) belong . 
 section piece come Scriabin , 
 middle section suggest Ireland , introduce 
 middle mixture snatch modal 
 tune , come source . result 
 piece unity style , continuous 
 growth , individuality , , attractive 
 bar , little interest . 
 Roy Agnew ' Pangbourne Fields ' 
 homogeneity style , short length 
 modal stuff sew , securely , 
 garment frankly scriabinesque material . 
 consecutive ninth , , rise semitone 
 climax , bear burden heat long 
 day , allow rest . ’ pity , 
 help feel , composer promise 
 purpose publish work , 
 , exercise 
 hope develop talent . Thomas F. Dunhill 
 ' white Peacocks ' firm- 
 ness grip plenty technical control . 
 sort stately dance delicate treatment 

 player , easy 

 Prof. Révész little friend 

 onsiderable composer book fund 
 biographer . warning fond , 
 wer - sanguine parent . little E. N. talent 
 prompt psychologist compile comparative 
 hapter drag Bach , Handel , Mozart , Beethoven , 
 Schubert , Mendelssohn , . 
 prodigy pianist american 

 reputation 

 Dr. H. D. Statham recital dedication 
 War Memorial organ Chigwell School , programme 
 include movement Handel B flat Concerto , 
 Gibbons Voluntary minor , Bach C minor Fugue 
 movement e flat Sonata , Purcell Suite , 
 nocturne f sharp minor . organ 
 build Messrs. Henry Speechley Sons , 
 - manual sixteen stop 

 occasion visit St. Anne , Soho , 
 London Society Organists 30 ) , Mr. Albert 
 Bach recital ( Sonata . 4 , Passacaglia Fugue , 
 Prelude Fugue G , chorale Preludes ) ; , 
 tea interval , pianoforte recital , play Bach 
 Prelude Fugue D , Schubert Impromptu B fiat , 
 Beethoven D minor Sonata ( Op . 31 ) , Debussy ' Reflets 
 dans l’eau , ' Chopin group , « 
 th Parish Church , 14 , special musical 
 service place aid Lincoln Cathedral Restora- 
 tion Fund . Mendelssohn ' hymn praise ' Parry 
 * bl Pair siren ' chief feature . 
 choir orchestra . Mr. O. M. 
 Price , organist church , conduct , Dr , G. J. 
 Bennett organ 

 

 l , performance 

 round high artistic level . attractive 
 ' Sally Alley ' ' Molly Shore , ' 
 play real understanding piece . Mr. 
 Jean Pougnet , leader instance , con- 
 grtulate ; incidentally , promising young violinist 
 success recital Wigmore Hall 
 fine technique , play utmost 
 eness feeling , careful 
 succumb overmuch sweetness . 
 succeed concert , fortnight later , pleasant 
 reading movement Beethoven 
 uartet C minor , Op , 18 , . 4 . 
 ‘ light tendency - emphasise accent noticeable , 
 t generally speak performance admirable . 
 Mr. Ithali Mack good deal taste piano- 
 piece Brahms , Miss Joan Morris sing 
 pleasing simple fashion Wolf ' Verborghenheit . ' 
 alist voice distinctly good quality 
 mise , ( ood french pronunciation rare 
 student - fledged , 
 ¢ feature Mr. Dudley White performance 
 Massenet ' vision fugitive , ' satisfactory state affair 
 probably mentor , Mr. Maurice d’Oisly . 
 interesting performance Saint - Saéns 
 Fantaise ' violin harp , harpist , despite 
 ccasional intonation uppermost string , 
 good diction , invaluable asset 
 ‘ nger , clear sign imagination Miss 
 ‘ nna Hubble song Herbert Hughes . 
 _ Tuesday afternoon , June 16 , Queen Hall 
 ‘ coratively garb , occasion student ’ 
 tchestral concert . Sir Henry Wood prophesy rightly 
 anticipate good playing , 
 Perk rmance second , , movement 
 - Tchaikovsky Symphony common 
 mark great advance previous effort ; 
 satisfactory hear refinement string . young 
 conductor , Mr. Guy Baron , charge 
 movement , congratulate , 
 co 
 ee 7 ~e Jean Pougnet play Glazounov Violin 
 chant wit taste feeling , orchestra 
 ye unusual discretion accompaniment 

 lack 
 

 orchestral concert June , 
 | * ? orchestra , , important . 
 | programme large scale , include Elgar 
 | Violoncello Concerto , Delius Pianoforte Concerto , Rimsky- 
 | Korsakow ' Coq d’Or ' Suite , scene ' Aida , ' 
 | soloist Miss Ida Starkie , boldly tackle 
 | Violoncello Concerto short notice success ; 
 Miss Bisset , reading Delius fine 
 | moment , despite occasional lack elasticity ; 

 Miss Avis Phillips , sail ' Ritorna vincitor ' 
 | confidence old hand . ' Coq d’Or ' 
 | Suite prove immensely popular , under- 
 | stand success opera child 
 | North England , , tell , choose work 
 | British National Opera Company répertoiré . 
 second orchestral concert afford opportunity 
 conductor orchestral work , vocal solo , con- 
 |certo , include performance , Dr. Malcolm 
 Sargent direction , Beethoven eighth Symphony 

 second Patrons ’ Fund rehearsal term intro- 
 duce soloist train privately , Mr. 
 Geoffrey Tankard perform Grieg Pianoforte 
 Concerto , Miss Katharine Stewart Agnus Dei 
 Bach B minor Mass , Miss Lilly Phillips Miss 
 Vyvyan Lewis ( violoncellist ) play d’Albert Concerto 
 } Boéllmann Variations 

 Motet R. O. Morris 
 borrow idiom classical counterpoint , jus 
 borrow favourite title . neat 
 pleasant work hear Mr. Morris drop hint 
 * 16th - century counterpoint work t 
 destiny instrument . let th 
 17th century drop . start , string 
 quartet . quiet liberty set ball 
 roll . " Motet understand 0 
 Mr. Morris proceed similar line 

 tough problem concert Bernard vat 
 Dieren ( Quartet . new counterpoint devise Mr. 
 van Dieren good quality beauty . listener 
 appreciation depend readiness ear t 
 dispense soft way Bach Beethoven t 
 draw pleasure strong line fine clashing . Mr. 
 van Dieren question compellingly mos 
 composer pursue new path musica 
 discovery . asceticism far banish 
 violoncello account lusciousness sentimentality , 
 substitute double - bass . ordinary 
 string quartet violoncello wholly engage 
 voluptuous . spend time chord 
 collection sound come roundly ear , & 
 far double - bass , unobtrusively 
 

 Curlew ' thoroughly perform - 
 John Goss group - know string wind 
 player . simply ask sit receive 

 Quartet | . 
 | time opera present , orchestral 
 | music opportunity hear 
 | long winter spring , little likely crowd 
 | orchestral concert late June , especially 
 | orchestra small , , competent , 
 | certainly way superior orchestra 

 Overture — ' Die Meistersinger ' 
 Suite D ; 
 Overture — ' Magic Flute ' 
 * Enigma ’ variation 
 hungarian rhapsody f 
 symphony c minor Beethoven 
 concert 
 orchestra , strength , play 
 press Orchestra 

 afternoon , 
 magnificently 

 Breastplate strength protection 

 heroic love - tale ; certain 
 orchestra , chorus , soloist 
 feel , 
 il style — exalted tradition ask . 
 Kennedy Scott conduct Mr. Bax work 
 s Alto Khapsody , sing Miss Dorothy Helmrich . 
 t George conduct music , age 
 enty - . Ilow easily assert wish ! 
 ¢ fuss , authority ! come 
 en Hall season , , Beethoven 
 Brahms M 

 tianity ) 
 

 25 M , Kussewitzky , imperfect Mozartean 

 G minor Symphony , servant Beethoven 
 m~ Ninth , day later subject M. Sokolov , 
 » te Cleveland Symphony Orchestra , U.S.A. , Bach 

 Hesco bright orchestral scoring hisown . week 

 _ orchestra play Parry ' english ' Suite 
 Suite r flute string 

 HARROGATE.—The symphony concert season 
 27 . Mr. Basil Cameron 
 orchestra direction Haydn ' Oxford ' 
 Symphony , Holst ' japanese Suite,’and , Mr. Angus 
 Morrison , Beethoven fourth Pianoforte Concerto 

 HARWICH.—Bach ' sing Lord ' principal 
 task Harwich Dovercourt Choral Society 
 concert season . programme largely draw 
 test - piece study competition include 
 ' Meistersinger ' Fantasia Purcell ' Soul world 

 1925 

 Quintet Schubert 
 early Mr. Frank Mullings Hugo Wolf recital . 
 new Tudor Trio — Miss Kathleen Forster ( violin ) , 
 Miss Molly Wright ( violoncello ) , Miss Annie 
 Warburton ( pianoforte)—made débit 25 
 Beethoven B flat Trio , Op . 07 

 OXFORD.—In anticipation th 
 centenary June eights - week concert 2 
 programme draw entirely composer 
 mnecte House Cathedral . list include 
 Crotch , Basil Harwood , Peter Warlock , 
 King Henry VIII . Dr. Ley direct concert . — — 
 Mr. Keith Douglas conduct ' New World ' Symphony 
 symphonic poem , ' Oxford , ' Town Hall , 
 24.——A new cantata , ' Salve Caput Cruentatum , ' 
 Mr. Fritz che Hart , director Melba Con- 
 ervat t Melbourr perform 
 ine 14 Mr. Maurice Besly 

 e direction o 

 Orchestra , fon 
 7 nder Mr. Arthur 
 rapid progress , June 
 factory performance Beethoven fifth symphony 

 Town Hall , S 

 NEW YORK 

 conductor manager yearly try 
 leningrad | rake Europe fine tooth comb artist present 
 end musical season mark | american audience great success 
 phony concert Philharmonia , conduct |this summer . import singer 
 z Unger , Berlin . shortly , late | impression past season , operatic 
 operatic novelty produce stage | artist appear Metropolitan Opera House — 
 Academic Opera Theatres ( late Marinsky Michel ) . | Nanny Larsen Todsen ( soprano ) Karin   Branzell 
 generally speak , season characterise | ( contralto ) . speciality german opera , 
 seater number conductor interpreter come |though sing Italian French . Larsen 
 Leningrad abroad . State Philharmonia , | Todsen appear Briinnhildes — ' Walkiire ' 
 aich grow ' year ago late court|impersonation far good , 
 hony Orchestra , principal centre | ' Gétterdammerung ' successful . Isolde 
 ‘ ctivity . concert late Salle de la / fair achievement , great , Kundry 
 Noblesse , add small hall for|was colourless , Branzell successful 
 tcerts . hall quartet consist Lukashevich | - round singer , successful Brangaene 
 Violin ) , Pechnikov ( second violin ) , Ryvkin ( viola ) , | Fricka . occasion , Todsen fall ill fail 
 " gylevsky ( ’ cello ) , series recital devote | sing ' Walkiire , ' substitute hoarse 
 " ork Beethoven , Tchaikovsky , S. Tanéiév , | fail second Act , omit 
 . ther devote new work include |scene Wotan . Branzell discard Fricka garb , 
 " tormance composition Prokofiev , Stravinsky , | don Briinnhilde robe armour , sing 
 " sella , Honegger , , Act , save opera collapse . Todsen 
 , ure early season symphony concert | Branzell satisfactory appearance italian opera , 
 . tae Philharmonia conduct young | singe Aida Amneris ; sing 
 " apable new - comer , Valery Berdaiev , conduct | French — Todsen Rachel , ' La Juive , ' Branzell 

 * rformance . orchestra hand , Dalila , ' Samson . ' 
 ~. achieve fine , highly - finish effect . fault concert - platform new pianist ( Harold 
 » endency hasten thing . particularly obvious | Samuel specialist ) sensation , viz . , Alexander 
 " tioz " Damnation de Faust , ' play | Brailowsky , prove brilliant player fine 

 MUSICAL TIMES — Juty 1 1925 653 

 Beethoven , Schumann . small choral items/ write , adapt technical 
 include Charles Wood ' Expectans expectavi , ' part- resource . able 
 song arrangement Dyson , Whittaker , Morley , | little kind sketch score . 
 Herbert Hughes , Stanford , Xc . notable feat , } omit ? obviously , bass 
 Dauntsey music - master year . | , simplify ; main 
 number boy little , | theme subsidiary theme possible find place 
 sing day - boy ; sing choral society , | sketch . , , remember 
 nember staff help , choir orchestra . | result , slight incomplete , mzszca/ 
 vo ! power elbow Mr. J. A. Davison , | rhythmical . achieve develop 
 music - master . kind communal activity | habit bad sight - reader constantly halt correct 
 t count . incidentally , remark Wiltshire is| mistake . rhythm 
 fen describe unmusical area . fact , | happen note ev rov‘e 

 se , country unmusical 
 shusiastic teacher leader collect material 
 set hire 

