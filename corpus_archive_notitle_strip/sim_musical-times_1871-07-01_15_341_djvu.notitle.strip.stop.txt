FELIX GANTIER OPERATIC GEMS . 
 Published . new Numbers Popular Series Pieces 
 founded following Operas : — 
 . 25 . Der Freischutz . 28 . Tannhiauser . 
 26 . Il flauto magico . 29 . Crown Diamonds . 
 27 . Zampa . 30 . L’Ombre ( Flotow new opera ) . 
 Solos , 2s.6d . Duets , 3s 

 CATHEDRAL GEMS , LOUIS DUPUIS . 
 1 , NANTES.—Fantasia , subjects Mozart 12th Mass 
 2 . WURMS.—Fantasia , subjects Weber Mass G 
 3 . ROTTERDAM.—Fantasia , subjects Haydn 3rd 
 ( Imperial ) Mass ain pn ose pa el aA 
 MALAGA.—Fantasia , subjects Beethoven Mass 

 eo © com 

 PHILHARMONIC SOCIETY 

 Ar fifth concert , 22nd , Schubert Sym- 
 phony ( No.9 ) excellently given , scarcely succeeded 
 rousing audience like enthusiasm . 
 attractive , , subjects work , 
 skilful greater portion writing , spite 
 certain diffuseness , Symphony hardly 
 fail eventually position orchestral 
 compositions annual place pro- 
 grammes Society . Italian Symphony , 
 Mendelssohn , went admirably , especially vivacious 
 Saltarello , performed decision play- 
 fulness rarely equalled . feature concert 
 Madame Norman- Neruda’sartistic rendering Beethoven 
 Violin Concerto ; orchestral Introduction 
 Fugue Mozart , welcome excellence 
 novelty . Mr. Bentham heard advantage 
 Mozart ‘ Un aura amorosa , ” Madame Sinico sang 
 , ; think Verdi 
 ‘ ‘ Ernani involami ” judicious piece choose 
 classical concert . sixth concert Sth ult . , in- 
 cluded Mr. Cipriani Potter highly dramatic overture 
 “ Cymbeline , ” appears extraordinary find 
 manuscript . audience , , fully 
 recognise worth ; showed appre- 
 ciation merits prolonged applause , insisted 
 composer appearing acknowledge public 
 testimony talent . interesting item pro- 
 gramme performance Paganini difficult 
 showy concertos violin , Signor Sivori , 
 appeared thoroughly enjoy eccentricities 
 composition , mastered passages ease 
 elicited warmest marksof approbation . Mozart 
 Symphony G minor , Beethoven Pastoral Sym- 
 phony , principal orchestral works ; 
 vocalists Madame Sinico Madame Trebelli- 
 Bettini . seventh concert given 19th ult . , 
 Haydn Symphony C ( Letter R ) , Beethoven , 
 B flat , receiving careful highly finished ren- 
 dering , intelligent conductorship Mr. Cusins . 
 Madame Arabella Goddard played , perfect 
 executive power artistic feeling , Sir W. Sterndale 
 Bennett Concerto F minor , overwhelmed 
 applause . Viotti clever , somewhat antiquated Violin 
 Concerto performed Herr Straus ; M. 
 Capoul gave eternal ‘ “ Salve dimora ” excellently 
 receive unanimous encore . vocalists 

 Madlle . Titiens Madlle . Ilma de Murska 

 PRIVATE evening concert , Highbury 
 Hill Choral Society , took place 9th ult . , Wel 
 lington Hall , Islington . admissions cards 
 invitation , crowded fashionable audience 
 assembled occasion . chorus singing 
 effective , received signal recognition . Miss M , A. 
 Pocklington assisted solo music , 
 favourable impression . Mr. N. Neville , organist St 
 Peter , London Docks , presided pianoforte , 
 Mr. J. Milton Clarke , Tottenham , conducted 

 Mr. P. E. Van Noorpen gave “ Concert 
 d’Invitation , ” Beethoven Rooms , Tuesday 
 80th , assisted pupils following artists : 
 Miss Patti Laverne , Madame F. Eldon , Messrs. C. Lane , 
 Bolton Theodore Distin . Madame Eldon sang “ 0 
 rest Lord ” taste expression , Mr. Bolton 
 gave artistic rendering Wallace “ happy 
 moments , ” Mr. Theodore Distin sang Van Noorden 
 new song “ true loving home , ” 
 applauded . violin solos Mr. C. Lane greatly 
 admired . Mr. Van Noorden conducted 
 efficient manner 

 Tue fifth anniversary North London 
 Philharmonic Society celebrated Monday 
 22ud , thirty members _ 
 gentlemen interested working Association 
 took supper . W. Ralph , Esq . , efficiently 
 performed duties chairman ; 
 assembled J. F , Barnett , Esq . , President ; J. D. 
 Hill , Esq . , F.R.C.S. , Dr. Barrett , Vice - Presidents ; E. 
 Silas , Esq . , H. Chipp , Esq . , Mr. Heath Mills , 

 Voi che sapete , ” Neithardt “ Echo Chorus , ” in| Mr. C , E. Stephens 

 took solo , obtained - deserved 
 encore . Miss Sophie Stuart received encore 
 Rossi Cavatina , “ Ah rendimi . ” 
 Suchet Champion contributed pieces , 
 sung ; Mr. Alfred Carder played 
 brilliancy Beethoven Sonata E flat . Mr. Goodwin 
 displayed great skill solos violoncello ; 
 College choir , conducted Mr. W. C. Filby , 
 sang madrigals choruses , 
 highly successful 

 Tue series concerts , aid 
 organ fund St. Andrew Presbyterian Church , New 
 Road , Shepherd Bush , given Lecture Hall , 
 Leysfield Road , Thursday 8th ult . 
 programme entirely composed Scotch music , 
 excellently rendered , pieces 
 enthusiastically encored . Mr. W. Wilkinson , R.A.M. , 
 able accompanist . sum £ 16 14s . 6d . 
 handed Committee 

 choral effects “ Cum Sancto Spiritu , ” 
 portions chorus combined 
 solo voices . final “ Amen ” skilfully worked ; 
 subject religious character ; especially 
 conclusion , choral , ascending scale 
 passages , tonic dominant harmony , trite 
 uninteresting . solos extremely melodious , 
 effective ‘ ‘ Gratias agimus , ” 
 tenor , “ Agnus Dei ” baritone ; , 
 distinctive character possess , 
 sung words second , 
 composer operatic pieces . exquisite singing 
 Madame Adelina Patti gave interest “ Christe 
 eleison ” merit music scarcely 
 called forth ; vocalist infused 
 extraordinary vitality somewhat common soprano 
 solo “ Credo . ” Mention tenor 
 baritone duet “ O Salutaris , ” excellently written 
 voices , striking figure accompaniment 
 running movement minor , 
 harp breaking effect change 
 tonic major . Thesolo parts finely sung Madame 
 Adelina Patti , Madame Cora de Wilhorst(whwis deservedly 
 progressing public favour ) , Madlle . Sanz , Signor Gardoni 
 Mr. Santley . Signor Vizetti presided pianoforte , 
 Mr. Sidney Nayloratthe harmonium , harp obbligato 
 played Mr. A. Lockwood . selection 
 followed Mass , Madame Patti received 
 encore graceful ballad , ‘ * Farewell ; ” Madame Cora de 
 Wilhorst created genuine effect Cavatina 
 opera , “ Pierre de Medecis ; ” cleverly written trio 
 opera , ‘ ‘ Don Desiderio , ” excellently sung 
 ( Signor Ciampi lending valuable aid ) ; Mr. Santley 
 gave “ Yeoman wedding song ” 
 dramatic feeling cause enthusiastic demand 
 repetition . Prince Poniatowski accompanied 
 music second pianoforte , 
 conducted Mass 

 Mr. Briyrzy Ricwarps evening concert 
 Hanover Square Rooms , 2nd ult . , attracted 
 large fashionable audience . programme , 
 usual , contained excellent selection Welsh 
 music , choruses ably sustained 
 Welsh Choral Union . Mr. Richards compositions — 
 “ Boat song , ” Madrigal “ Ye little birds , ” sacred 
 song , chorus , ‘ hour , ” “ Cambrian 
 war song ” ( sung Miss Edith Wynne , 
 second Mr. Lewis Thomas ) , received 
 rapturous applause , songs enthusiastically 
 encored . pianoforte solos concert - giver . 
 course , important feature , Beethoven “ Sonata 
 pathétique , ” works — Welsh 
 Fantasia , graceful little piece ‘ Memoriam ” 
 Tarantella E flat ( encored ) , sufficiently attesting 
 power interpreting music varied styles . Mr. 
 Richards performed Sir Sterndale Bennett Sonata 
 Duo , pianoforte violoncello ( joined 
 M. Paque ) , listened interest 
 warmly applauded . A- successful début 
 Miss Llewellyn Bagnall , young student 
 Royal Academy Music , song Pinsuti 
 proved pure sympathetic 
 voice , natural qualifications carefully 
 judiciously trained . created highly favourable 
 impression audience , unanimously 
 recalled . Miss Watts ( thoroughly successful 
 Mr. Richards sacred song , “ o’er past ” ) , Madlle . 
 Angéle , Miss Annie Edmonds Mr. Vernon Rigby 
 vocalists ; Mr. John Thomas , 
 addition conducting Welsh Choral Union , performed 
 harp solo accustomed effect . vocal solos 
 artistically accompanied Mr. Henry Eyers 

 performed . good writing Mass. 
 “ Kyrie ” mind unquestionably 
 devotional movement , clever 

 FavovuraBLE mention Miss 
 Alice Ryall , gave morning concert 
 Hanover Square Rooms 10th ult . , displayed 
 excellent soprano voice cultivated style songs 
 Spohr , Mozart Macfarren , 
 warmly deservedly applauded . vocalists 
 Miss Rebecca Jewell , Madlle . Drasdil Mr. 
 Edward Lloyd ; solo instrumentalists Mr. Walter 
 Macfarren Mr. J. Hallett Sheppard ( pianoforte ) , Mr. 
 Lazarus ( clarionet ) , Mr. Pettit ( violoncello ) Mr. Oscar 
 Edwards ( harmonium ) . Conductors , Mr. Walter Macfarren 
 Mr. Stephen Kemp 

 Mr. Watrer Macrarren second concert , 
 20th , included Sir W. Sterndale Bennett 
 Sonata Duo ( op . 32 ) pianoforte violoncello , 
 Beethoven Sonata C minor ( op . 30 ) , pianoforte 
 violin , Schumann Quartet E flat ( op . 47 ) 
 pianoforte , violin , viola violoncello , 
 artistic playing pianoforte concert- 
 giver conspicuous feature , Herr Straus ( violin ) , 
 Mr. Burnett ( viola ) Herr Daubert ( violoncello ) , 
 proved worthy coadjutors . 
 compositions Mr. Macfarren selected performance 
 spirited written pianoforte duet “ L ’ Appas- 
 sionata ” ( ably joined Mr. Stephen 
 Kemp ) , Nocturne , “ Music Jake , ” Romance , 
 ‘ “ « Madeline , ” “ Polonaise , ” 
 pieces pleasing recalled plat- 
 form , unanimously expressed wish audience . 
 Miss Marion Severn highly successful Henry 
 Smart dramatic song “ petrel warning ” ; 
 sacred songs Mr. Walter Macfarren — * O Lord , 
 rebuke , ” “ O sing unto Lord new 
 song ” — Miss Rebecca Jewell achieved marked success 
 compelled - appear acknowledge 
 prolonged applause , compliment 
 awarded refined rendering “ Rose softly 
 blooming . ” concert , 3rd ult . , 
 Mr. Walter Macfarren Sonata F major , pianoforte 
 violin , given excellent effect composer 
 M. Sainton ; programme comprised 
 Mozart Quartet G minor , pianoforte , violin , viola 
 violoncello , Mendelssohn Trio D minor , 
 pianoforte , violin violoncello , executants 
 pieces , artists mentioned , Mr. 
 Burnett ( viola ) Signor Pezze ( violoncello ) . Men- 
 delssohn Duet “ Allegro brillante ” ( op . 92 ) 
 played concert - giver talented pupil , Miss 
 Linda Scates , effect materially heightened 
 performance pianofortes . pieces , 
 “ Spinning song , ” “ * Goldenslumbers ” ‘ ‘ Mountain 
 stream , ” Mr. Macfarren elicited warmest applause 

 recalled enthusiastically 
 conclusion compositions Sonata . Miss 

 Nee intoned Rev. W. J. Hall , M.A. , 
 read Rev. W. S. Simpson , M.A 

 Mr. Frepericxk Penna Miss Kate Penna 
 7 ° concert Beethoven Rooms 3lst 
 _ e @ numerous audience . Miss Penna 
 WN asa voealist talent ; occasion 

 composition elements of|she fully supported reputation excellent sing 

 ing “ Di piacer , ” Bishop “ Tell heart . ” 
 took Signor Caravoglia Rossini duet , 
 ‘ * Danque io son , ” pieces concerted music . 
 Mr. Penna sang effect “ ” ( 
 ‘ « Elijah ” ) Verdi “ Infelice , ” 
 applauded . Miss Penna proved 
 graceful composer Serenade , “ Look thy lattice . ” 
 given Mr. Trelawny Cobham . 
 artists mentioned concert - givers 
 assisted Madame Penna Signor Tito Mattei 
 ( pianoforte ) , Herr Pollitzer Mr. N. Mori ( violin ) , Herr 
 Goffrie ( viola ) , Herr Lidel ( violoncello ) , Madame 
 Calderon Mrs , Osborne Williams , vocalists 

 Ar annual concert Mr. Walter Bache 
 programme conventional pieces little expected 
 surprise created selection 
 include single composition Beethoven , 
 Mozart , honoured names 
 advanced art present high position . 
 performance Friday evening 26th , given 
 | Hanover Square Rooms , exception rule . 
 ; Liszt Pianoforte Concerto E flat , com- 
 |poser “ * Les Préludes ” ( Potme symphonique d'aprés 
 | Lamartine ) works orchestra 
 engaged ; scarcely believe Mr. 
 Bache converted audience worship 
 | eccentric German composer , doubt 
 performance highly satisfactory 
 disciples , exceedingly interesting 
 heard preaching musical doctrines 
 time . Mr. Bache playing thoroughly earnest 
 intellectual ; applause 
 received legitimately earned . Miss Clara Doria 
 Mr. Nordblom vocalists ; conductors 
 Mr. Dannreuther Mr. Walter Bache 

 Mrs. Joun MacrarreEn gave concert St. 
 George Hall afternoon 25th , 
 large highly appreciative audience . 
 pianoforte solos given concert - giver 
 Sketches Sir Sterndale Bennett , ‘ « Lake ” “ 
 Fountain , ” Chopin “ Fantaisie - lmpromptu , ” Walter 
 Macfarren “ Tarantella , ” heard 
 utmost advantage Beethoven Trio B flat ( Op . 
 11 ) pianoforte , clarionet violoncello , 
 ably assisted Mr. Lazarus Herr Daubert . 
 vocalists Miss Edith Wynne , Miss Banks , Miss 
 Julia Elton , Miss Marion Severn , Miss Jessie Royd , 
 Signor Gardoni , Herr Reichardt M. Jules Lefort . 
 accompanists Herr W. Ganz , Mr. Walter Mac- 
 farren Signor Randegger 

 Tue fourth concert Welsh Choral 
 Union given Concert Hall , Store Street , 
 29th , large audience . singing 
 Welsh airs , harmonised Mr. John Thomas , 
 excellent scarcely wonder enthusiasm 
 received , 
 doubted choir progress , 
 great future Society . preservation 
 national melodies object deepest interest ; 
 leader zealous talented Mr. John Thomas , 
 Welsh Choral Union grow importance 
 scarcely contemplated pre- 
 sent unpretending series concerts commenced . 
 end , , definite plan adhered : 
 plenty choirs sing Mendelssohn “ O hills , 
 O vales pleasure , ” body vocalists specially 
 trained interpret Welsh music , guidance 
 Welshman , ’ mission apart ; , 
 properly managed , create taste native melo- 
 dies Wales beneficial pro- 
 gress art . improvement choir 
 short time public worthy 
 highest commendation ; tone — especially female 

 Mendelssohn “ Lieder ohne worte ” Harpsichord 
 lesson Scarlatti , received enthu- 
 siastic deserved applause , unani- 
 mously encored . concert - giver assisted Herr 
 Straus ( violin ) Signor Pezze ( violoncello ) ; 
 vocalists Miss Galloway , Miss Alice Fairman , Madlle . 
 Drasdil , Mr. Montem Smith Mr. Robert Hilton . 
 Signor Randegger able accompanist 

 Mr. E. H. Tuorne gave morning concert 
 Hanover Square Rooms 26th , 
 fully fashionably attended . refined rendering 
 Beethoven Sonata E flat ( Op . 7 ) brilliant per- 
 formance Thalberg variations “ God save Queen ” 
 “ Rule Britannia ” amply asserted concert - giver 
 claim highest rank intellectual executive 
 artist , applause greeted 
 spontaneous deserved . solo instru- 
 mentalists Herr Louis Ries ( violin ) M , Paque 
 ( violoncello ) . singing ef Miss Ida Thorne 
 marked feature programme ; little 
 doubt cordial reception audience 
 lead toa desire frequent appearance 
 London concert - rooms . vocalists Miss 
 Sydney , Miss Enriquez ( encored Sullivan 
 “ Looking ” ) , Madlle . Carola M. Jules Lefort . 
 concert respect thoroughly successful 

 Thursday evening , 22nd ult . , 
 West London Amateur Orchestral Choral Society 
 gave concert Hanover Square Rooms . 
 programme comprised Mendelssohn “ Lobgesang , ” 
 Miss Laura Whitton , Miss Muir , Mr. Stanton 
 soloists , sang artistically . Pianoforte 
 Concerto , G minor , composer , 
 played Miss Miles ( amateur ) ; Handel « Let 
 bright Seraphim , ” carefully rendered Miss Maudsley 
 ( Mr , Dearden excellent trumpet obbligato ) , 
 encored , repeated . Selections Bellini 

 152 MUSICAL TIMES.—Joty 1 , 1871 

 performance Beethoven Trio , E flat , piano- 
 forte ( Mr. P. H. Diemer ) , violin ( Mr. Folkes ) , violoncetlo ( Mr. 
 Gough ) , movewent rendered elicit 
 enthusiastic applause . vocal pieces successfully 
 given ; concert brought close March 
 Chorus Ruins Athens 

 Braprorp.—The choirs churches 
 Bradford recently held united festival Parish Church . 
 1400 tickets issued public , 
 church apparently seven o'clock , numbers con- 
 tinued enter half hour longer , seat 
 provided standing room available occupied . 
 choristers entered church grand porch , marched 
 centre aisle singing processional hymn , ‘ Brief life 
 portion . ” procession closed number 
 local clergy . prayers beautifully intoned Rev. W. 
 Dyson , curate Parish Church , responses effect- 
 ively given . Psalms day ( Ps . xliii . , xliv . ) sung 
 chants Harcourt Harris , “ Cantate Domino ” 
 ‘ * Deus Misereatur ” positions iderable merit 
 Mr. Swaine , organist . Mendelssohn anthem , ‘ ‘ Judge , 
 O God , ” given conclusion prayers fine effect . 
 hymn sermon , ‘ ‘ O Lord , joyful ’ tis , ” 
 sung ‘ Intercession , ” joined heartily con- 
 gregation . appropriate sermon preached Right 
 Rev. Bishop Ryan , word “ Hallelujah . ” fol- 
 lowed Old Hundredth Psalm ; end service 
 Clark Whitfeld - known anthem , ‘ Jewry God known , ” 
 given . choir left church singing recessional 
 hymn , “ blessed home . ” arrangements 
 superintendence Mr. A. R. Swaine , organist choirmaster 
 Parish Church , presided organ , assisted Mr. 
 A. Swaine , organist Bingley Parish Church 

 Hontey.—The annual festival connection St. 
 Mary Church took place 28th . Psalms 
 chanted single chants — Cooper E , 
 Barnby E. anthem “ glad said 
 unto ” ( Sir George Elvey ) . music effectively rendered 
 choir , augmented occasion upwards 
 90 voices . Mr. J. C. Beaumont displayed considerable skill ' 
 judgment organist 

 Leeps.—The Organ Recital season took place 
 Town Hall , 13th ult . programme included Handel ' « 
 Organ Concerto , . 2 ( B flat ) ; aselection Beethoven sonata 
 E flat , op . 7 ; Fantasia form offertoire ( C major ) , 
 Berthold Tours ; performance Romanza Gounod 
 C minor . selection beautifully played , 
 appreciative audience . August Dr. Spark 
 introduced interesting Recitals large number clas- 
 sical works ; excellence performance , 
 variety selection , succeeded fully establishing 
 entertainments public favour 

 Liverroot.—The thirty - fifth open rehearsal 
 Societa Armonica , Mr. Armstrong conductor , 
 Mr. Lawson leader , took place Institute , Mount Street , 
 Saturday evening , 10th ult . programme consisted 
 selections Auber , Mozart , Haydn , Donizetti , Bishop , « c. 
 Kyrie , Mozart , creditably given chorus , 
 orchestral accompaniment adding effect . Miss Monk- 
 house , air * Gratias Agimus ” ( Guglielmi ) , clarion : t 
 obbligato , Mr. Haddrell , accompaniment string win : 
 instruments , rapturously encored . singing choruses . 
 ‘ Merry boys , away , ” ‘ ‘ tramp , ” “ gipsy tent 

 153 

 Lyminatron.—On Tuesday evening , 20th ult . , 
 Philharmonic Society gave excellent concert Assembly 
 Rooms , large fashionabie audience . com- 
 mencement performance , Mrs. Warren Peacocke ( Efford 
 House ) stepped forward , behalf Lord George Gordon 
 Lennox presented Mr. Klitz handsome ivory baton , 
 mark sense entertained lordship , ladies 
 gentlemen , Mr. Klitz musical abilities , ac- 
 knowiedgment exertions conductor director 
 society . Mr. Klitz replied words , received 
 hearty round applause , gratifying compli- 
 ments present platform . programme 
 selected , contained instrumental pieces Mozart 
 Sonata major , pianoforte violin ( excellently played 
 Mr. Morant Mr. C , Fletcher ) ; Beethoven Septett , arranged 
 Holst , pianoforte , harp , violin , concertina violoncello , ( 
 executants Mrs. Warren P ’ ke , Mrs. Deschamps de la Tour , 
 Messrs. Morant , Fletcher , Conway Parkinson ) ; solo 
 violin Mr. Charles Fletcher , composed dedicated 
 Mrs. Deschamps de la Tour ; ‘ ‘ Toy Symphony ” Haydn 

 ocal solos given success Mrs. Deschamps 
 de la Tour Rev. Ernest Williams . Mrs. Brown piano- 
 forte performances important feature concert , 
 accompaniments vocal music ad- 
 mired . concert respect decided success . — — _ 
 pleasure stating Mrs. 8 . St. Barbe lately 
 presented Mr. R. A. Klitz , Organist Choirmaster church 
 choir , valuable set silver tea service , recognition 
 important services connection recent assembly 
 choir neighbourhood Beaulieu 

