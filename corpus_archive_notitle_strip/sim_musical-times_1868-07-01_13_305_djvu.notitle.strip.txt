_—_

INCIDENTS IN THE LIFE OF BEETHOVEN. 
By R. M. HAYLEY

To all true lovers of music the name of Beethoven 
must ever be a household word. The wonderful 
creations of his genius possess a charm peculiarly 
their own, and are known and valued wherever 
the art of music, of which he was so great an orna- 
ment, is cultivated and understood. ‘Lhe life of this 
extraordinary musician presents details of a varied 
character, — combining at one and the same 
time, commanding genius, and strange eccentricity. 
Unlike Mozart, he was no infant prodigy, astonishing 
the world by his precocious powers. His wonderful 
ability for music was, however, early discovered, 
and required time for its development; but, by the 
force of his genius, he awakened in his countrymen a 
recognition of his merits, and the judgment of his 
contemporaries has been since fully confirmed in pro- 
portion as his works are more known and studied

Ludvig van Beethoven sprang from a musical 
family. His grandfather had distinguished himself 
as a writer of musical dramas and operas, and his 
father, at the time of his birth, held the appointment 
of tenor singer in the Chapel of the Klector of 
Cologne. ‘The family had originally come from 
Maestricht, in Holland, where the name is by no 
means uncommon in the present day, and was at this 
time residing at Bonn, where, on the 17th December, 
1770, the great musical composer, on whose life we 
are now entering, first saw the light. Until recently 
there has been some uncertainty as to the exact year ; 
and even Beethoven himself always placed the date of | 
his birth two years later than we have done; but | 
the matter has been now set at rest by the researches 
of Fétis pere. The mistake has arisen from Beet- 
hoven having had an elder brother, who died in his 
infancy, with the same Christian name; but on

guished himself so much in the study and practice of 
it. At least, it is recorded by Fétis pére, in his life of 
Beethoven, on the authority of M. Baden, of Bonn, 
who was Beethoven’s companion from infancy, that 
his father was obliged to use violence to urge him to 
the cultivation of his art, and that he rarely took his 
place at the piano, except under compulsion. From 
other sources we learn that a devotion to music yery 
soon supplanted every other idea in his mind; and 
that his great delight as a child was to listen to his 
father’s performance on his favourite instrument, for 
which purpose he would leave the society of his 
playmates. It was not till his fifth year that his 
father seems to have commenced with him a course 
of instruction, and the two stories may be reconciled 
by the well-known fact that the treatment of him by 
his father from this time, was such as to give him an 
actual distaste for that sublime art which had pre- 
viously so entranced him. Under the influence of 
drink, to which he was addicted, his father abandoned 
himself to paroxysms of fury which knew no bounds ; 
and, anxious as he was to bring up his son to the 
practice of music, nearly defeated his own ends by 
his intemperate conduct, and all but deprived the 
world of the pleasures which the wonderful composi- 
tions of his gifted son are calculated to inspire

After Beethoven had overcome the obstacle so 
early presented to the pursuit of that study for 
which he was by nature so pre-eminently endowed, 
he made rapid progress in the prosecution of his 
studies. ‘Lhe best part of his musical education 
was received from the bandmaster of a Bavarian 
regiment of the name of Pfeiffer, and he remembered 
with gratitude in after years the advantages he had 
derived under his tuition, and relieved his necessities 
at a time when he stood in need of his assistance. 
His aptitude for the study of music soon began to 
attract attention in his native city, and Von der Eden, 
one of the most distinguished violinists in Bonn, took 
a lively interest in his progress, giving him gratuitous 
instruction as far as his own numerous avocations 
permitted ; but when the boy’s talents were brought 
under the notice of the Elector, Max Franz, the 
latter desired Von der Eden to give him an hour’s

consulting the baptismal registry of his native town, 
it appears that Beethoven was born as above stated, 
and his name-sake brother twenty months earlier. 
Besides these two sons, his father had two younger 
children, both boys. ‘The elder of the two ultimately 
became a violinist, and the other studied surgery. 
Both of them followed their brother to Vienna, 
where he himself spent the greatest part of his life. 
It. has been stated by a well known authority 
{Choron and Fagolle’s Dictionnaire des Musiciens) 
and, consequently, often repeated, that Beethoven 
was the natural son of Frederick William II., King 
of Prussia. He appears himself to have been aware 
of this story, for in writing to an old friend towards 
the close of his life, he says:—‘‘ This report was 
mentioned to me long since, but my principle has 
always been neither to write about myself, nor to 
take any notice of what others write about me, and 
therefore I willingly leave to you the vindication of the 
honour of my parents, especially of my mother.” 
story in fact has not a vestige of foundation, 
and all the facts respecting Beethoven's birth and 
parentage militate against the truth of it. Itis a 
strange circumstance that Beethoven at first did

instruction every day at his own expense. From 
this time Beethoven made such rapid strides in the 
art of music, that his performances in the Chapel of 
the Elector’s palace, elicited great applause. He 
was now placed under the tuition of Neefe, a cele- 
brated composer of the day, who had received the 
appointment in the Elector’s Chapel, rendered vacant 
by the death of Von der Eden

His new master, failed not to discover the re- 
markable genius of the pupil whose education he had 
undertaken, and in place of confining him to the 
study of inferior compositions, sought to introduce 
him, without delay, to the grand conceptions of 
Bach and Handel. The works of these great com- 
posers warmed the imagination of the youthful artist 
and he always retained for them so fervent an 
admiration, that he never mentioned their names, 
but with the highest reverence. In his eleventh year 
he had learnt to perform Sebastian Bach’s ‘“‘ Well- 
tempered clavichord ” with such admirable taste and 
precision, that his playing rivalled that of many dis- 
tinguished professors. It was this early acquaintance 
with the works of so profound a musician that gave

not, although possessing so much precocious/to Beethoven thatease and rapidity in execution which 
aptitude, indicate the desire to learn which would| distinguished his performance on the piano in after

have been expected in one who afterwards distin

THE MUSICAL TIMES.—Juty 1, 1868

his ninth year, and after a study of thorough-bass, to 
which Neefe then directed him, he found that greater 
success attended his efforts. Amongst these earlier 
juvenile attempts are some variations of a march, 
three Sonatas for the piano, and a few German songs. 
Most of them abound with such serious infringements 
of the laws of harmony, that Beethoven, at a 
later period of his life, was so ashamed of these 
defects, that he disclaimed being their author. His 
wonderful power of improvising was at this time 
remarkable, and the fertility of his imagination 
excited the admiration of all who heard him. Gerber 
(Neues Lexikon der Tonkunstler) relates that the 
composer Junker on hearing him extemporize at 
Cologne on a theme he had given him, expressed 
great astonishment at the marvellous powers of inven- 
tion by which his performance was distinguished

At the age of fifteen, through the influence of the 
Count of Waldstein, who was not only a great con- 
noisseur, but a practical musician, Beethoven was 
appointed organist in the Elector’s Chapel at Bonn

his nobleman early appreciated the great abilities 
of the young musician, and the interest which 
he exerted in his favour was abundantly justi- 
fied by the manner in which the duties of his 
new office were discharged. He exhibited the same 
mastery over the organ, as he had formerly done 
over the piano, and continued to astonish everybody 
by the extent of his intuitive genius and inventive 
power. Whilst still a pupil of Neefe, and assisting 
him in the musical department of the Elector’s 
Chapel, he paid a short visit to Vienna, in order to 
hear Mozart, whose music he greatly admired, to 
whom he curried some letters of recommendation. 
This occurred about the year 1790. He played 
extempore before Mozart, who paid very little atten- 
tion to the performance, imagining it to be a piece 
learnt by beart. At last the young musician, piqued 
by this indifference, begged Mozart to give him a 
subject. Mozart muttered to himself, ‘ Well, stay 
a little, let me try your metal,” and wrote down a 
chromatic fugue subject which, taken backwards, 
contained a counter subject for a double fugue. 
Beethoven, though knowing little of the science, was 
not taken in. He worked upon the subject, the 
hidden properties of which he immediately discovered 
with such force, originality, and genius, that his 
hearer, more and more confounded, and almost 
breathless with attention, at last rose, and walking 
on tiptoe into the adjoining apartment where some 
of his friends were sitting, said to them, with great 
emotion, ‘‘ Attend tothat young man; you will hear 
of him one day

In order to gain a livelihood Beethoven was 
obliged to have recourse to teaching, and by this 
means he was introduced to an amiable family at 
Bonn, of the name of Brenning. . This acquaintance 
exercised a beneficial influence on his character, and 
assisted materially towards his intellectual improve- 
ment. Madame de Brenning was the widow of a 
Court Councillor, and had three sons and a daughter, 
who was afterwards married to Dr. Wegeler, and is 
affectionately mentioned in Beethoven's correspon- 
dence with him, under the name of Leonora. To 
this daughter he dedicated his first variations for the 
piano. Throughout life he cherished a fond re- 
membrance of the happy days he had spent in that 
family, the members of which seem to have always 
retained asincere affection for him. Harshly treated

by his father, he was happy in being domesticated

amongst such kind friends. Madame de Brenning's 
maternal kindness won his heart, and she gained an 
ascendancy over his wayward spirit which no other 
person ever possessed. In one respect only her 
influence failed ; she never could conquer his repug- 
nance to give lessons in music. ‘The dradgery of 
teaching was intolerable to him, and, except in the 
case of the Brenning’s, where friendship aud gratitude 
made him punctual, he used to shuffle out of 
it as best he could. One day Madame de 
Brenning having urged him very much to go and 
give his usual pianoforte lesson at the Austrian 
Ambassador’s, who lived opposite her house, Beet. 
hoven went away for that purpose; but when he 
arrived at the Ambassador’s door, his natural dislike 
to teaching got the better of him, and he returned to 
Madame de Brenning, saying to her, ‘‘ For God's 
sake, madam, don’t insist on my giving this lesson 
to-day. I will give two to-morrow.” This anti. 
pathy accompanied him through life; and, indeed, 
he never had any pupils (at least whom he acknow- 
ledged as such), but the Archduke Rudolph and his 
friend, Ferdinand Ries. It was during his intimacy 
with the Brenning family that he first imbibed a 
taste for literature, his father being so entirely en- 
grossed with music that he never encouraged any 
other kind of study. The direction which Beethoven's 
mind now took was permanent, and he acquired a 
taste for reading, which continued with him to the 
end of his life

Late in the year 1791 Beethoven was appointed 
one of the Elector’s private musicians, and was 
thus brought into contact with the most celebrated 
performers of the day, whereby his own fame was 
greatly extended. In the month of July, 1792, 
Haydn passed through Bonn on returning from his 
first visit to England, and the Elector’s choir invited 
him to a breakfast at a place of resort near the town, 
On this occasion Beethoven showed him a Cantata 
of his own composition, and desired his opinion of it, 
The illustrious veteran praised it highly, and gave 
him every encouragement to pursue a career he had 
so well begun. This piece, however, was never pub- 
lished, nor ever performed, being found too difficult, 
especially for the wind instruments. He thus showed, 
at the very outset, that disregard of mechanical 
facilities which has always been an impediment to 
the satisfactory performance of his music. In the 
judgment of one of his contemporaries, the perfor- 
mances of Beethoven on the pianoforte, and for which 
he afterwards became so famous, were, at this time, 
not without a certain harshness and want of finish. 
He had little acquaintance with any distinguished 
player; but an opportunity now presented itself of 
appreciating those finer- touches which distinguish 
the consummate artist. Having accompanied the 
Elector's Choir to Aschaffenburg, he was introduced 
by some friends to the choirmaster, Sterkel. This 
celebrated musician was the first to make Beethoven 
aware of the deficiency in his performances as re- 
spected taste and delicacy. He was himself by no 
means a powerful player, but the grace and precision 
of his style were remarkable. When he sat down to 
the pianoforte Beethoven stood behind him motion- 
less, with his eyes riveted on the keys, admiring the 
delicacy of touch which characterized: the player. 
Conversation having arisen on an air with variations 
which Beethoven had recently published, Sterkel, 
in allusion to its extreme difficulty, expressed a doubt 
as to whether even the author himself could satis

earlies| 
Sayin 
he had 
ianofo 
jenna 
Lichno\ 
musicia 
the rest 
compan 
however 
most ur 
set, the 
who kne 
paid no: 
own opir 
 conce 
him, tha 
Jealous r 
ever loge 
music the 
When H 
Commi 
Albrecht: 
point, but 
authority 
Haydn, 
method oO 
state of th

factorily execute it. 
repugnance to play in public, feeling piqued at the 
remark, he sat down at the instrument without the

jece before him, which had been unfortunately 
mislaid. He then began to play such variations as 
he could recollect, adding others extempore in such a 
manner that Sterkel and everybody present were 
astonished ; and what was the more remarkable in 
this improvisation was that Beethoven, suddenly 
adopting Sterkel’s style of execution, played with a 
neatness and a delicacy which he had never before 
exhibited. The disinclination of Beethoven to per- 
form before others, to which we have above alluded, 
was a peculiarity which he carried with him through 
life. ‘‘He used to come to me,” says one of his 
friends, ‘* gloomy and out of temper, complaining 
that he had been forced to play, even though the 
blood tingled in his fingers. I endeavoured to calm 
and amuse him, and then the conversation dropped. 
By and by, whilst talking to me, he would sit down 
upon the stool that stood in front of the piano, and 
whilst apparently looking the other way would care- 
lessly run over the keys, and evoke the most agreeable 
melodies. I dared not make any remark, and he

would ultimately take his leave in a very different 
mood from that in which he first entered my room.” 
This reluctance to play before others was not always 
so easily overlooked, and oftentimes occasioned a 
degree of coldness between Beethoven and his

friends

Notwithstanding the favourable commencement of

Beethoven’s acquaintance with Haydn, their subse- 
quent intercourse turned out anything but agreeable. 
Inthe year 1792 he was sent by his constant friend 
and patron the Elector of Cologne to study at Vienna, 
under the greatest musician of the age. Under this 
master Beethoven was introduced to the study of the 
works of the best composers ; and Haydn entertained 
a great affection for his promising pupil, whose pro- 
gress he marked with the greatest interest. It was 
the wish of Haydo that Beethoven should acknow- 
ledge himself as his pupil in the title page of his 
earliest publications. 'To this Beethoven demurred, 
saying that he had never taught him anything. When 
he had finished his first work—a set of trios for the 
Fenotorte, violin, and violoncello, and published at 
leona in 1795—he played them at Prince 
lichnowsky’s, before a party of the principal 
musicians of Vienna. Haydn was present amongst 
the rest, and joined in the applause bestowed by the 
company on these charming productions. He 
however took the author aside, and advised him 
most unaccountably not to publish the third of the 
set, the well known trio in C minor. Beethoven, 
who knew well that this was the best of the three. 
paid no regard to the advice; and when he found his 
own opinion confirmed by the judgment of the public, 
he conceived the notion, which never afterwards left 
him, that Haydn had been actuated by a spirit of 
jealous rivalry. This he never forgave, nor did he 
ever lose the opportunity of making Haydn and his 
music the subject of ill-naturedremarks and criticisms. 
When Haydn took his departure again for London, 
he committed his pupil to the care and instruction of 
Albrechtsberger, a celebrated professor of counter- 
point, but Beethoven submitted to Albrechtsberger’s 
authority with scarcely more patience than to that of 
Haydn. His new master was a profound theorist, 
and taught the science in all its scholastic rigour—a

Notwithstanding Beethoven’s

to bend under the yoke of antiquated rules which he 
felt to be mere pedantry, and was constantly led by 
his ardent imagination to disregard. He was contin- 
ually therefore committing errors, which his teacher 
assiduously endeavoured to correct. Hence arose 
many disputes between master and pupil, although 
to Beethoven’s credit, it must be said, that he never 
lost sight of therespect and esteem due to his venerable 
instructor. The exercises which he wrote under the 
eye of his master were preserved, accompanied by 
observations of his own on the absurdity of 
some of the theories by which it was sought to 
restrain his lively imagination These exercises, and 
the sarcastic remarks in which Beethoven indulged 
upon them, were published at Vienna after his 
death, under the title of ‘‘Beethoven’s Studies in 
Thorough Bass;” and, though a worthless publication, 
is still curious, as showing the supreme contempt 
Beethoven had for the tasks imposed upon him. The 
rules, taken down of course from Albrechtsberger’s 
mouth, are sometimes so obscurely, inaccurately, and 
even unintelligibly expressed, that the pupil evidently 
did not comprehend their scope ; and in the examples, 
instances of bad harmony, false answers to subjects 
of fugue and other errors are to be detected in almost 
every page.” ‘* Beethoven,” says Fétis, ‘‘ was not, 
as has been supposed, unacquainted with the science 
of music, but the science was too circumscribed for 
his views.” It appeared to thwart his most congenial 
views, and he never was able to become familiar with 
it. In some of his remarks he is very amusing. There 
is a chapter on Canon, for instance, containing 
examples of this kind of composition in all its absurd 
and puzzling varieties. In his enumeration of them 
he mentions, ‘¢ the numerical and enigmatic canons 
which, like every thing that partakes of the nature 
of a riddle, are easier to invent than to solve, and 
seldom yield any compensation for the time and 
trouble bestowed upon them. In former times, 
he adds, people took a pride in racking their brains 
with such contrivances; but the world is now grown 
wiser.” It was thus that Beethoven ridiculed in 
conversation the strict precepts of the schools. 
When any infringement of them in his compositions 
was pointed out to him by his friends, he would term 
them a set of pedants, and rejoin, laughing, ‘ Oh, 
yes, you are quite astonished and confounded, because 
you cannot find this in one of your treatises on 
harmony!” During his residence at Vienna, Beet- 
hoven did not cease to indulge in pleasing recollec- 
tions of the happy days he had spent with the 
Brenning family, in his native city. Much of the 
correspondence which he had with them is preserved, 
especially his letters to Leonora, the friend of his 
youth, to whom he was able to express his feelings 
without reserve. On sending to her some variations 
on the aria in Mozart’s Figaro, Se vuol ballare, which 
he had dedicated to her, he thus writes:—‘‘I only 
wish that the work were of more importance, and 
more worthy of you. I have been tormented here to 
publish it, and I have availed myself of the opportu- 
nity to give you a proof of my regard and friendship, 
and of my constant remembrance of your family. 
Accept this trifle from one who highly esteems you. 
If it afford you any pleasure, I shall be amply 
satisfied. Let it be a souvenir of the time when I 
passed so many, and such happy hours in your house, 
It will perhaps serve to keep me in remembrance till

I see you again, which I fear will not be soon

of the series of these Concerts which took place on the

for the choir included Pinsuti’s «‘ The sea hath its pearls” 
(which was enthusiastically encored), Benet’s “ All 
creatures Now are merry-minded,” Stevens’ ‘“ Cloud-capt 
towers” (encored), Bishop’s “ Chough and Crow”’ (also 
re-demanded), and Pearsall’s “Take heed, ye shepherd 
swains.” Goss’s beautiful glee, “ There is beauty on the 
mountain,” was well sung by the Quartet Glee Union (the 
members of which we presume also belong to Mr. Leslie’s 
choir), and Mendelssohn’s Psalm, ‘‘Judge me, O God,” 
was given so excellently as to cause an immediate call for 
its repetition. Mr. Sims Reevessang in his very best style 
Beethoven’s “‘ Adelaida” (accompanied by Mr. Charles 
Hallé), “Come into the Garden, Maud,” and “Tom 
Bowling,” the last named song, especially being declaimed 
with such real pathos as to awake whatever dormant 
tautical feeling might have existed amongst the audience. 
Miss Edith Wynne was highly successful in her rendering 
of “Softly sighs,” from Der Freischiitz; Madame Patey- 
Whytock sang a new and well written song by Mr. Henry 
Leslie, “My darling, hush!” with the utmost teeling; 
and Signor Gustave Garcia gave much pleasure by his 
singing of the old ballad “ Black-eyed Susan.” The pianist 
was Mr, Charles Hallé, who played Beethoven’s “ Sonata 
pathétique” with his usual success

CONCERTS ANCIENT AND MODERN

yeock, who executed every passage to perfection; and 
much did the last movement delight the audience, that 
tt was unanimously re-demanded. The work is, we 
uderstand, still in M.S. ; and was played on this occasion 
for the first time. We trust, however, that those who 
lave the charge of Mendelssohn's manuscripts will no 
onger allow it to rest in unmerited neglect. The selec- 
tions from Handel’s Fire and Water Music fared no better

She also gave the well-known Romance from Fra Diavolo| viously coming to grief more than once. The principal 
encored) and joined Mr. Reeves ina duet from Hrnani, | vocalists in Bach’s Uratorio were Miss Palmer, Mr. W. H. 
which was 80 beautifully sung as to be re-demanded, in|Cummings, and Mr. Patey. Amongst the miscellaneous 
spite of the unusual number of encores already insisted|vocal solos given, the most effective were Hummel’s 
upon. Mr. Sims Reeves gave Weber’s song “I'd weep|‘Alma Virgo,” (with chorus), well sung by Miss Banks, 
with thee,” and “ The Message,” in excellent style; andj}and Purcell’s ‘‘ Come, if you dare,” given with so much 
in obedience to an enthusiastic recall, sang “ My pretty|energy by Mr. W. H Cummings as to receive an un- 
Jane,” which it is needless to say excited the most un-|deniable encore; a compliment, however, which he only 
bounded applause. The pianist was Miss Agnes Zimmer-| responded to by bowing his acknowledgments, a system 
mann, who performed Beethoven’s “Fifteen variations,| which we need scarcely say we should like to see more 
with Fugue,” and two posthumous studies by Mendelssohn | generally followed

No. 1, in Bk minor, and No, 2,in F major—with her

glory of the Creator. The Hundredth Psalm can never 
grow old with so many youthful th:oats to keep alive its 
unpretending appeal to our sympathies; and to Mr. Goss 
should all honour be given for proving that new works can 
be fitted to the occasion which shall. at the same time, add 
to the store of solid and conscientiously written music for 
the Church. On the present occasion, his Ze Deum and 
Jubilate (which have now completely taken their place at 
these festivals) were given with wonderful effect. The 
Anthem before the prayer for the Queen—Handel’s “ Zadok 
the priest”—Mendelssohn’s chorale, Sleepers, wake,” 
(which is now invariably sung before the sermon) and the 
“ Hallelujah ” chorus, from the Messiah, were also wonder- 
fully impressive, rising in many parts indeed to positive 
sublimity, much of the effect in the Coronation Anthem 
being materially heightened by the organ accompaniment, 
which was played as a duet by Messrs. Goss and George 
Cooper. The prayers were intoned by the Rev. J. Spar- 
row Simpson, and the lessons read by the Rev. J. Lupton. 
As usual, the responses used were by Tallis; and the 
psalms of the day were chanted by the united choirs to a 
slow chant in C, by Dr. Crotch, the children joining in the 
Gloria Patri at the conclusionof each, The steady con- 
ducting of Mr. Shoubridge, (a worthy successor to the late 
Mr. Buckland), was of the utmost service throughout the 
morning

Tue third and last of Mr. Walter Macfarren’s 
Pianoforte Recitals for the present season, took place at 
the Hanover Square Rooms, on the morning of the 6th 
ult. Mozart’s Sonata in G, for pianoforte and violin, and 
Beethoven’s ‘‘ Kreutzer” Sonata, for the same instruments, 
were most artistically performed by the Concert-giver and 
Mr. Henry Holmes, a violinist who seems to be rapidly 
making his way as a thoroughly satisfactory exponent of 
the highest classical works. Mendelssohn’s 8th book of 
Lieder ohne Worte created the usual effect with the audience, 
two—the Presto, in C, and the Allegro vivace, in A,—bein

encored: the first, however, only was repeated. All these 
little gems were performed by Mr. Macfarren with a true 
appreciation of their refined beauty. Schumann’s An- 
dante and variations, in B flat, (Op. 46), for two piano- 
fortes, afforded Mr. Macfarren’s clever pupil, Miss Emma 
Buer, an opportunity of joining her master in a duet 
which successfully displayed her well cultivated powers ; 
and the programme was pleasingly varied, as usual. by 
some of Mr. Macfarren’s elegantly written pieces, two of 
which, “ La Fete d’Eté.” anda Romance, called “Bianca,” 
had not been heard before. The first of these is a light, 
tripping Bohemienne, full of character, and not too 
difficult for those who have trained their fingers to dance 
upon the keys; and the second, a graceful “ song without 
words,” the melody of which moves almost throughout 
with the bass, the quaver accompaniment being played 
with the right hand. Miss Robertine Henderson was

t+ month at St. James’s Hall, with the usual success

rs. Macfarren’s selection of pianoforte music included 
Beethoven’s ‘‘ Sonata Pastorale,” the «Menuetto Capric- 
cioso”’ from Weber’s Sonata in A flat, the 8th book of 
Mendelssohn’s Lieder ohne Worte, &c., all of which. were 
most excellently performed, and received with the warmest 
applause by the audience. The singers have been Miss 
Banks, Miss Robertine Henderson, Madame. Patey, Mr. 
W. H. Cummings, and Mr. Patey

Tx accounts furnished us of the first Trien- 
nial Festival given by the Handel and Haydn Society at 
Boston, in the United States, in May last, are full of praise, 
both of the music and the performance. The Americans 
appear frantic in their estimation of Wagner; for in 
Watson’s Art Journal we are told that the Zannhiéuser 
Overture “ contains some of the grandest orchestral effects 
ever imagined; and that it is a creation of such splendid 
proportions that it would confer a living name upon its 
composer, were it the only work that he had written.” 
The accuracy of the news from London may be questioned, 
if we may judge by the paragraph in the same journal, 
stating that Mendelssohn’s Reformation Symphony was 
produced at the Crystal Palace Concerts, “ under the 
direction of that talented conductor, Dr. Ganz.” Let us 
hope that, should our Handel Festival be commented upon 
in America, the names of those engaged in it may be 
more correctly quoted

Mr. Freperick WEsTLAKE gave an Evening 
Concert at the Beethoven Rooms on the 26th May, when 
a programme, selected with the utmost taste and judgment 
was provided. Mr. Westlake’s pianoforte playing was 
highly successful, both as regards executive power and 
true artistic feeling; and his performance was received 
with much applause. Amongst his most effective pieces 
were Dr. Bennett’s Trio (Up. 26) for pianoforte, violin, and 
violoncello (in which he was ably supported by Messrs. 
Henry Holmes and Aylward), Niels Gade’s pianoforte and 
violin Sonata (Op. 21, No. 2), (violin, Mr. Henry Holmes), 
Schumann’s “ Andante Espressivo,” with variations, for 
two pianofortes (with Mr. Walter Macfarren), and his own 
MS. ‘Duo Concertante,” forviolinand violoncello,which was 
excellently performed by the composer and Mr. Aylward. 
Mr. Westlake also gave two solos (Henselt’s « Repose 
d’Amour,” and Schubert’s Impromptu, Up. 90) with good 
effect. Miss Robertine Henderson was the vocalist; and 
in two M.S. songs by the Concert-giver was much ap- 
plauded, Mr. Walter Macfarren conducted

Mavame Everne Oswatp gave a Matinée at 
St. George’s Hall on the 26th May, when a selection of 
strictly classical music was performed, well chosen to

display the Concert-giver’s qualifications as a pianist of

the legitimate school. We must especially mention 
Weber’s Solo Sonata in D minor (Op. 49), and Beethoven’s 
“ Kreutzer” Sonata (in which. she was joined by Herr 
Straus), both which exacting works were rendered by her 
in a manner which thoroughly proved her right to the 
position she aspires to. Several other compositions of the 
highest class were included in the programme. The 
vocalists were Madame Sauerbrey, Madame Raby Barrett, 
and Herr Wallenreiter. We must not omit to mention, 
that, in. addition to Herr Straus, the services of Mr. Paque 
were secured for the violoncello, Herr Wilhelm. Ganz 
was the conductor

WE understand that the College of Organists

AN account furnished us of a Concert given 
by the pupils of Westbourne Park College, on the 22nd 
ult., contains such a powerful list of established vocalists 
and instrumentalists who are said to have “assisted” them, 
that it would appear to be a Concert given by well known 
artists “assisted ” by the pupils. In confirmation of this, 
we are told that the artists were received with the greatest 
enthusiasm, “the pupils also receiving much applause.” 
As this is an annual Concert, we hope next year to 
hear more of the pupils, and less of the “artists

Mr. E. H. Tuornz, gave a Matinée Musicale, 
at the Beethoven Rooms, on May 14th The artists on 
the occasion were Miss Ida Thorne, Miss Julia Elton, and 
Mr. W. H. Cummings. Pianoforte, Mr E. H. Thorne ; 
violin, Herr L. Ries; violoncello, M. Paque. Conductors, 
Signor Randegger, Mr, Zerbini, and Mr. Knapp. The 
Whole of the performances were of the highest order. and 
gave great satisfaction toa fashionableaudience. Mr Thorne 
displayed unusual executive power and irreproachable

Messrs, Chatterton, Regondi, and Cheshire for their

taste in several classical compositions. His rendering of 
Beethoven’s “ Sonata Appassionata” was as intelligent as 
his performance was artistic

THE ROYAL ACADEMY OF MUSIC

in number, of course occupied a considerable space, filling 
At half-past

the transepts, and a portion of the nave. 
three o’clock, the clergy entered the church in procession 
by the west door, and walking up the nave, took their 
places in the chancel. The service was full choral, 
prayers heing intoned by the Rev. E. D. Garven. The 
first lesson was read by the Rev. Canon Barclay, and the 
second by the Ven. Archdeacon Pollock. The psalms 
were the 93rd (Sir F. A. G. Ouseley), and the 94th 
(Beethoven), both of which were well rendered. The 
Deus Misereatur, (of Kelway), was sung with much 
power, and the responses were given by the choir with 
admirable precision. The anthem was by John Goss, 
«© taste, and see how gracious the Lord is.” Before the 
sermon, the hymn «0 love, who form’dst me to wear,” 
from Hymns Ancient and Modern, was sung with much 
effect. After the sermon, the choir gave Handel’s grand 
Hallelujah chorus. Great praise is due to Mr. Towers, 
the choirmaster, and to his able assistant Mr. Arnold, for 
the efficient manner in which they have trained the choirs 
composing the association. Mr. Towers was the organist 
on this occasion

Brentwoop.—On the 11th ult., the double 
feast of S. Barnabas, the Apostle, and of Corpus Christi, 
was observed with great honour in Brentwood Church, 
the day having been chosen to celebrate the choral 
festival of the United Parochial Choirs, comprised in the 
Brentwood District of the Association for promoting the 
improvement of Church Music. The Venite was given 
to a chant in F major, by the Rev. W. Felton, and the 
Psalms to one in E flat, by Barrow. The Anthem, 
‘Teach me, O Lord,” (a quartet from an Anthem, by 
Thomas Attwood), and Tallis’s Preces and Responses were 
quite refreshing, as genuine specimens of what sacred 
music should be. In the evening service an Anthem, by 
C. Gardner, Esq., the choirmaster, was most effectively 
given, especially the fugue, “For there the Lord.” The 
composition contains some good points, and is smoothly 
written for the voices throughout. Special mention 
should also be made of Joseph Barnby’s beautiful setting 
of the hymn, “@ Paradise,” and Arthur H. Brown’s 
simple and unaffected tune to Dr. Neale’s translation of a 
hymn, by S. Anatolius, a.p, 454, “'The day is past and 
over.’ Great praise is due to Mr. Gardner for the 
admirable manner in which he has trained the choir; and 
a word of commendation must also be awarded to Mr. A. 
H. Brown, who presided most efficiently at the organ

