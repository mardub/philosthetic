Sir Henry Wood finding endorsed 
 frequently mixed 
 company musicians . invariably 
 found narrowest outlook , educated 
 taste , greatest pretensions 
 singers . sight - reading powers long 
 byword 

 musicians presumably start fair , 
 assume deficiencies result 
 vocalists ’ training . Sir Henry Wood , 
 article quoted , went 
 demanded singing pupils way 
 attendance chamber orchestral concerts . 
 ‘ kind training necessary students , 
 need singers . 
 early career , budding pianists , 
 violinists , organists touch great 
 deal fine music ordinary course study . 
 soon busy Bach Beethoven , | 
 recreative work 
 wealth excellent music Grieg , Chopin , 
 minor tone - poets generally . early 
 stage singer pupillage ( especially 
 ) thoughts lightly turn , vocal 
 equivalent lyrical pieces Grieg , 
 jolly little dances Bach , royalty 
 ballads , stay , pupil 
 aspires coloratura singing , case 

 floridity , human musical interest , 
 stays . Armed , Press 
 agent skilful shameless use big 
 | drum , pervades larger centres population , 
 ia rolling stone disproves proverb 
 gathering great deal moss 

 organ repertory long libera| 
 supply . smoothly - running fugues likely 
 esteemed long time , excellent technical 
 work , , clear way , enjoyable hear gp 
 long demand fugue mere 
 fugality , speak . want invention 
 expression — passion — Bach 
 Rheinberger , Karg - Elert , Reger ( cases ) , 
 handful 

 mere bulk , Rheinberger Sonatas 
 impressive output . composer save Beethoven 
 written , - round excellence , 
 instrument 

 glance complete list Rheinberger works 
 — run Op . 197 — explains smallness 
 proportion unsuccessful movements organ 
 music . experienced - round composer 
 began write instrument , 
 gave little attention years , 
 , organ work — C minor Sonata — 
 numbered Op . 27 ; second — Trios — Op . 49 ; 
 , fourth , fifth — Sonatas flat , G , 
 minor — Opp . 65 , 88 , 98 . Op . 11 
 onwards organ works appear frequently , 
 hard heels ( ¢.g . , Opp . 
 154 , 156 , 161 , 162 , 165 , 167 , 168 ) , 
 ripest period wrote sixteen Sonatas , 
 - Fughettas , Characteristic Pieces 

 mannerism met final 
 cadences , favourite form consisting plain 
 13th — 7th — followed dominant 7th 
 resolving , tonic chord , - 
 — broad ecclesiastical effect . gives q 
 great variety treatments formula 

 slow movements notable 
 undoubtedly extended pieces romantic 
 expressive kind organ sonatas . Mendelssohn 
 fairly long ones song form . 
 Rheinberger uses form delightful effect 
 Cantilene . 11 Cansona . 13 . 
 Sonatas , slow movement 
 important middle section , highly - organized 
 affair . Occasiorally , /dy// . 14 , 
 Adagio . 15 , touch 
 dramatic . variation form Nos . 10 
 19 — best slow movements , second 
 Beethovenian breadth . truism 
 movements test composer . Rheinberger 
 comes ordeal . Inall Sonatas 
 moven - ents insignificant — 
 Adagio . 1 /ntermezzo . 3 , 
 ( possibly ) /zfermezzo . 4 

 said Sonatas lack 
 appreciation country , disposition 
 overplay earlier ones expense 
 rest . excuse . divide 
 composer work periods , think 
 found . 5 , began 
 decline slightly . 15 . , Nos . 
 16 , 18 , 19 , 20 , contain long stretches , some- 
 times complete movements , fine 
 rest , successful wholes . Nos . 
 16 , 18 , 19 , 20 occasional turgidity , 
 old vigour driving power missing 

 
 , ( ) dead classic failure , 
 shelf , ( 4 ) living Composer 
 far better work ought hearing 
 instead 

 Like everybody , read Mr. Newman ; 
 work years , recall concert notice 
 like serioy : 
 protest injustice . ‘ loading g 
 dice moderns ’ habit ¢f 
 half - educated musicians , pedantic professors 
 ought thing critic 
 tolerate . 
 arms , utmost things right 
 case point . Speaking Beethoven ; 
 Rondino wind instruments , Mr. Newman says js 
 dull work , adds 

 young British composers produce 
 work ( ) ueen Hall , critics 
 accord things 
 ears tingle year . , 
 morning concert , think Kondino dull , 
 drop hint eflect , half . 
 apologetic way , knew duing wrong 
 thing supposing great man Beethoven 
 - rate 

 ’ : Mr. Newman doubt sat 
 Rondino . 
 outspoken ‘ ’ ? ofien bent 
 making young British composer ears tingle ? 
 interesting collect utterances 
 music Rondino , compare 
 scathing contemptuous terms 
 waved aside certain works Vaughan 
 Williams example . : matter Mr. 
 Newman speaks . plenty , 
 pretensions brilliance , manage 
 avoid ‘ loading dice 
 moderns ’ ; , ashamed plead 
 guilty working things 
 living Composer gets extra pip , encourage 
 , set - pedants 
 homage open ears dead , 
 cold shoulder hand sole 
 foot living 

 BORN OCTOBER 16 , 1849 : 
 DIED OCTOBER 16 , 1919 

 regret announce death Dr. 
 Charles Harford Lloyd , took place Slough , 
 seventieth birthday . native 
 Thornbury , Gloucestershire . bent 
 organ shown early years 
 age officiating neighbouring church 
 Rangeworthy , boy played 
 Falfield . regular study music began 
 age thirteen , taught piano- 
 forte harmony Mr. Jobn Barrett , Bristol . 
 turned organ Rossall 
 ( 1865 - 68 ) , taking lessons Charles Handel ! 
 Tovey , school organist music - master . 
 English organ students period , 
 staple musical diet inferior 
 young pianist . Lloyd working 
 pianoforte Barrett , daily bread Bach 
 Beethoven : organ music given Rossall 
 chiefly Batiste Léfébure - Wély . Fortunately 
 youth soon obtained open scholarship 
 Magdalen Hall ( Hertford College ) , Oxford , 
 came influence Stainer , 
 gave term lessons harmony , , 
 important , freedom organ loft . ‘ gained 
 experience untold value watching 
 played organ , ’ said Lloyd years 

 time leaning ordination , 
 attended Divinity lectures Liddon 
 Payne Smith . took Mus . Bac . 1871 , 
 B.A. degree 1872 , second - class Classical 
 Mods . theology . studies 
 pointing clerical career , music 
 getting hold . good deal 
 - round practical musical work University , 
 activities playing harmonium 
 conducting Glee Club Pembroke College . 
 important factor , , friendship 
 young musicians marked future eminence — 
 Parry , undergraduate Exeter College , 
 Walter Parratt , succeeded Stainer Magdalen 
 period 

 RECITALS 

 impossible attempt bare record 
 recitals month . pianists 
 played important M. Moiseiwitsch , 
 Mr. Lamond , Signor Busoni . - named 
 heard 1914 . rentrée 
 Wigmore Hall ( October 15 ) cloud- 
 compelling mood-——less vehement old , 
 stronger , tenderness . 
 ‘ arrangement ’ ‘ Goldberg ’ Variations 
 Bach opens question justifica- 
 tion treatment classics . 
 , doubt playing 
 amazing . virtuosity - playing 
 incredible , spite modern technique 
 Bach spirit violated . playing 
 Beethoven ‘ Hammerklavier ’ Sonata 
 bewildering . seldom heard tenderness 
 beauty ( obscurity ) slow movement . 
 artist succeeded convincing 
 long 

 recital Miss Myra Hess , playing 
 Mozart flawlessly , introduced effective charac- 
 teristic ‘ Phantasy ’ Bax , composed time ago 
 Mr. York Bowen gave recital war , 
 played admirable lucidity strong 
 technique . Studies interesting , 
 suited concert room . ‘ Sericus Dances ’ 
 charming — especially second — find 
 place répertoire pianists want 

 present musical season began Appleby 
 Matthews Sunday evening orchestral concert Futurist 
 Cinema House September 7 . Town Hall 
 vocal concert given September 29 Wolseley 
 Male - Voice Choir ably trained conducted 
 Mr. W. E. Robinson , known results 
 achieved rendering - known songs . 
 selections familiar kird , special praise 
 singing Sullivan ‘ Beleaguered ’ 
 ‘ O Peaceful Night . ’ Mr. John Coates received 
 enthusiastic reception , creating perfect furore 
 captivating singing ‘ Eleanore , ’ given voice infinite 
 lyrical charm . Miss Helen Anderton , great 
 favourite , gifted sympathetic contralto voice 
 exquisite timbre uses perfect art . Mr. 
 Arthur Hytch contributed violin solos , Mr. C. W. Perkins 
 organ solos , Dr. Rowland Winn accompanied 
 musicianly manner 

 series Saturday afternoon concerts 
 given Repertory Theatre , September 27 , 
 Miss Beatrice Hewitt , accomplished local pianist , 
 coadjutors Mr. Catterall ( violin ) , Mr. Paul 
 Beard ( viola ) , Mr. J. C. Hock ( violoncello ) Brahms 
 Pianoforte Quartet G minor , Op . 25 , given intelligence 
 fine tone balance . programme contained 
 Beethoven Trio pianoforte , violin , ’ cello , E flat , 
 Op . 70 , Grieg Sonata pianoforte violin , 
 Op . 45 , excellently interpreted Miss Hewitt 
 Mr. Catterall 

 New Philharmonic Society orchestral concert 
 season given Town Hall September 25 , 
 aid fund new organ Birmingham Royal 
 Institution Blind . conducted Mr. Matthew 
 Stevenson , gave pleasing , perfect , reading 
 Dvordk ‘ New World ’ Symphony Wagner ‘ Vorspiel 
 und Liebestod ’ ‘ Tristan Isolda . ’ Miss Desirée 
 Ellinger , Beecham Opera Company , star 
 concert , achieving triumphant success fine 
 Singing flute - like soprano voice . Organ solos 

 esentation Mr. Riseley mark unique occasion . 
 ‘ Tam sure , ’ says Mr. Roeckel , ‘ cisizens , 
 like , wish testi‘y appreciation great 
 work Mr. Riseley , , cause 
 music Bristol " — message find response 
 Bristol music - lovers 

 month weekly organ recitals St. Mary 
 Redcliff , direction Mr. Ralph Morgan , 
 efficient organist fine old church , continued 
 draw large congregations . Mr. H. G. Ley , organist 
 Christ Church , Oxford , visiting players , 
 varied programme compiled Franck , 
 Debussy , Bach , Beethoven , Elgar . recital 
 Mr. Morgan , entire collection memorial 
 members choir fallen war 

 Bristol Musical Club commenced session 
 excellent examples chamber music , Heath 
 ‘ Macedonian Sketches ’ Elgar pieces — 
 Romance . 1 new String Quintet — 
 programme . Mr. Maurice Alexa : der , best- 
 known local vivlinist — player plays heart 
 brain fingers — given recital 
 Literary Society Cotham Grove Baptist Church . Muss 
 Imogene Hawkins , prominent Bristol pianist , gave 
 evening recital Victoria Rooms benefit St. 
 Dunstan Hostel , played taste nice skill 
 phrasing . Songs given Miss Langrish - Stephens 

 Mr. H. Goss Custard ( ‘ Modern Organ : Tonal 
 Equipment ’ ) , Mr. Albert Orton ( ‘ Interpretation 

 Crane Hall Musical Wednesday afternoons continue 
 provide attractive programmes . Outstanding occasions 
 recitals given Mr. Anderson Tyrer 
 Mr. Joseph Holbrooke . Mr. Holbrooke usual 
 attractive exponent individual 
 music , including Nocturne , ‘ Night Sea , ’ 
 Study D. recital M. Mischa Leon , 
 accompanied Mr. Harold Craxton , gave fine vocal 
 interpretations , notably Holbrooke setting Tennyson 
 ‘ Come dead . ’ October 15 , Miss Una 
 Truman played Beethoven Sonata , Op . 27 , . 2 , Liszt 
 Rhapsodie . 10 , pieces Carpenter , Quilter , 
 Ireland , abounding executive skill interpretative 
 vision . recita!s vocalists included Miss Amy 
 Horrocks , Mr. Charles Wade , Miss Raymonde Amy , Miss 
 Rita Landi , Mr. W. H. Atkinson . October 8 
 programme sustained Manchester Trio , Mr. J. S. 
 Bridge ( violin ) , Mr. Walter Hatton ( violoncello ) , Miss 
 Ethel Midgley ( pianoforte ) . Excellent performances 
 given Arensky D minor Trio , exquisite slow 
 movement , Dvorak Trio 

 pianoforte recital Rushworth Hall October 10 
 Mr. Edward Isaacs gave masterly performance 
 attractive scheme , features Bach Chromatic 
 Fantasia Fugue , Beethoven C major Sonata Op . 2 , 
 3 , Debussy Toccata , Liszt ‘ Venezia e Napoli , ’ 
 exploited player virtuosity . 
 interesting musical point view Mr. Isaacs 
 pieces , ‘ Hunting Song , ’ ‘ Nocturne , ’ 
 Staccato - Caprice 

 course lectures ‘ Appreciation ’ 
 given October 16 Arts Theatre 
 University Liverpool Dr. A. W. Pollitt , newly 
 appointed Lecturer Music University . 
 lectures given Thursdays 5.30 , open 
 public fee . Dr. Pollitt lecture discoursed 
 ‘ listen , ’ subjects subsequently dealt 
 include ‘ Construction Melodies , ’ ‘ Growth 
 Harmony , ’ ‘ Suite Dances , ’ ‘ Sonata Form , ’ ‘ 
 Sonata , ’ ‘ Symphony , ’ ‘ Fugue , ’ ‘ Opera , ’ 
 ‘ Choral Music 

 Misses Foxon resumed Friday afternoon 
 concerts Victoria Lecture Hall , feature 
 programmes introduction fresh unfamiliar 
 songs . pianoforte music . certain proportion 
 chamber music heard concerts 
 announced . concert Mr. Stanley Kaye played 
 MacDwell ‘ Tragic ’ Sonata . strong young player , 
 energizes /or / e passages boyish enthusiasm , 
 happy passages calling great tenderness 
 emotion , assimilated formula expression 
 needs absorb - equipped , 
 picturesque player . Mr. Ernest Platts sang spirited 
 baritone songs , best Schubert ‘ Speed 
 , Kronos , ’ Miss Parker Machon sang songs Parry , 
 Mackenzie , Rimsky - Korsakov , Miss Ethel Cook 
 accompanied 

 Promenade Concert large audience welcomed 
 revival excellent orchestral music - makings . 
 Symphony Beethoven . 2 . orchestra 
 revealed qualities neatness refined tone , slow move- 
 ment particularly . Stirring performances 
 Sullivan ‘ Memoriam ’ Overture selection 
 numbers p quant ballet suite , ‘ Good - humoured 
 Ladies ’ ( Scarlatti - Tommasini ) , given . Miss 
 Lena Kontorovitch soloist Mendelssohn 
 Violin Concerto . new work , Nocturne Rondo , 
 Frank H. Shera . proved music great imagination 
 harmonic interest . Tinged , unduly , modern 
 feeling , pieces significant additions new 
 British orchestral music . Conductors search fresh , 
 interesting , playab!e numbers acquaint- 
 ance striking works . Mr. J. A. Rodgers , 
 conducted , secured sympathetic performance 

 YORKSHIRE 

 Concerts b : thicker ground 
 coming season time war began . 
 chief choral societies dropped work , 
 cases renewing energy . Particularly 
 case Leeds Philharmonic Society , 
 celebrating fiftieth anniversary , marking 
 occasion exceptionally good programme . 
 head Beethoven great Mass D , work 
 monumental character suitable great event , Berlioz 
 * Faust , ’ having curious ‘ run ’ North 
 England , selections ‘ Meistersinger ’ 
 ‘ Parsifal , ’ Dear ‘ Songs Open Air , ’ 
 customary ‘ Messiah ’ concert Christmas , choral 
 works given , addition promised 
 concerts Hallé Orchestra , conducted respectively 
 Sir Henry Wood Mr. Albert Coates . Dr. Bairstow 
 conduct choral concerts , relied 
 maintain technical efficiency choir . Leeds 
 Choral Union attempting given 
 previous seasons , special interest imparted 
 prospectus promised appearance Sir Edward Elgar , 
 conduct consecutive dates March , 
 ‘ Dream Gerontius ’ ‘ Apostles , ’ Hallé 
 Orchestra - rate cast soloists . works 
 preparation ‘ Messiah ’ ‘ Samson ani Delilah ’ — 
 composer received 
 canonization , twice styled prospectus 
 * St Saéas . ” Dr. Coward remains post conductor 
 Society 

 Bradford Festival Choral Society , Dr. 
 Bairstow , season Berlioz ‘ Faust , ’ 
 * Messiah , ’ miscellaneous choral concert , 
 Bach Motet ‘ Sing ye Lord ’ presented . 
 Bradford O'd Choral Society , Mr. Wilfrid Knight , 
 giving , addition ‘ Messiah , ’ ‘ Elijah ’ ‘ Merrie 
 England ’ — composer direction . 
 3erlioz ‘ Faust ’ chosen Ialifax Society , 
 year following familiar lines , 
 works announced ‘ Elijah ’ ‘ Messiah . ’ Mr. C. H. 
 Moody conductor . Huddersfield Society 
 promises fresh , Bach B minor Mass gives 
 distinction programme , includes Sullivan 

 Leeds Saturday Orchestral Concerts year 
 conductorship Mr. Hamilton Harty 
 Mr. Eugéne Goossens , junr . programmes 
 familiar lines proved attractive past , 
 resist wish committee shown 
 confidence supporters case 
 Bradford Permanent Orchestra , appeals 
 ‘ popular ’ Saturday night audience , 
 ventured insinuate pieces popularity 
 assured considerable number familiar works , 
 including fairly representative series contemporary native 
 composers , Arnold Trowell , Percy Pitt , Coleridge- 
 Taylor , Sullivan , Stanford , Elgar , Vaughan Wiiliams , 
 Holbrooke , Reynolds , Hamilton Harty , Grainger . 
 conductors , case Leeds concerts , 
 new - comers , Mr. Godfrey Brown Mr Geoffrey Toye 

 season time writing 
 begun , considerable number concert , tu 
 recorded . Harrogate season , course , thing 
 past , excellent series weekly symphony 
 concerts having taken place October 15 , Miss 
 Fanny Davies gave fine reading Beethoven fifth 
 Pianoforte Concerto . season long series 
 Symphonies Concerios heard , worthily 

 rformed small , complete efficient orchestra 
 Mr. Julian Clifford . noteworthy 
 features half season Fauré suite , 
 ‘ Pelléas et Mélisande , ’ Pierné Harp Concerto ( Miss Hilda 
 Atkinson ) Rachmaninov second Pianoforte Concerto ( Mr. 
 Anderson Tyrer ) , late Ernest B. Farrar Variations 
 pianoforte orchestra old sea - song , ‘ Hark , 
 Boatswain , ’ pleasing work , accentuated 
 sense premature death action year ago . 
 concert Mr. Clifford introduced orchestral wrk , 
 written memory friend coileague , entitled 
 ‘ Lights , ’ graceful sincere tribute , showing 
 distinct creative power previous compositions . 
 recent concert Miss Bertha Vanner introduced 
 Stanford second Pianoforte Concerto C minor , 
 new North England , 
 favourable impression , virile , effective , sincere 
 music , obvious vein feeling usual 

 Leeds Tetrazzini Melba concerts 
 require description criticism . October 18 
 Saturday Orchestral Concerts took place , 
 Mr. Hamilton Harty conducting typical programme , 
 chief features ‘ Unfinished ’ Symphony , 
 Rachmaninov second Pianoforte Concerto ( Mr. Anderson 
 Tyrer ) , ‘ Britannia ’ ‘ Freischiitz ’ Overtures , 
 Bizet ‘ L'Arlesienne ’ Suite , tit - bits Jarnefelt 
 * Preludium , ’ Berlioz ‘ Hungarian March , ’ orchestral 
 arrangement Bach Toccata F. Ona October 10 Miss 
 Lily Simms , co - operation Mr. Lloyd Hartley 
 . sonata recital Vivlin Sonatas 

 rahms ( ) , Elgar , Debussy artistically 
 interpreted , admirable vocalist appeared Miss 
 Muriel Robinson . Ona October : Mr. Frederick D : iwson 
 gave pianoforte recital Leeds 
 artistic success veritable ‘ de force , 
 left pianoforte whilst playing unfligging vigour 
 long succession pieces ranging area 
 pianofrte music , Bach Beethoven ( ‘ Funeral 
 March ’ Sonata ) Debussy , Ravel , Cyril Scott , 
 Holbrooke . held great audience end 
 masterly performance . Ou October 18 pilgrims 
 gine round country known claims 
 ritsh Music S ociety visited Leeds , Lord H > ward de 
 Walden , Dr. Eaglefield Hu!l ( founder ) , De . E. H. 
 Fellowes , Mr. Francis Toye spoke stand- 
 points aims work Society , , 
 backed , influential helping forward cause 
 native art 

 October 3 Bradford Subscription Concert season 
 began concert Madame Tetrazzini party , 
 gifted violinist , Madame Renée Chemet , 
 interesting member . October 9 , Mr. 
 Charles Stott began series organ recitals 

 piece equal musicianship greater musical charm 
 majority erudite composer works . Mr. Stott 
 played Elygar Sonata G 

 Huddersfield second season Music Club 
 began October 8 , Mr. Gervase Elwes , Mr. 
 Kiddle pianoforte , gave delightful recital British 
 songs . Future concerts admirable series wi!l intro- 
 duce Catterall Quartet , Ru - sian vocalist M. Vladimir 
 Rosing , Mr. Mrs. York Bowen , Mr. William 
 Murdoch . interesting Huddersfield enterprise 
 orchestra formed Mr. A. W. Kaye , 
 local violinist , years trained string 
 orchestra pupils , completed 
 addition professional wind , demonstrated effiziency 
 series concerrs , October 11 , 
 remarkably good finished performances given 
 familiar works like ‘ Unfinished ’ Symphony , 
 ‘ Witliam Tell ’ ‘ Tannhauser ’ Overtures , 
 brief novelry Balfour Gardiner charming little 
 piece , ‘ Home - coming . ’ Mr. Laurence Turner proved 
 capable executant [ chaikov - ky Violin 
 Concerto , outstanding feature concert 
 wonderfully good ensemble achieved large body 
 strings , young players , 
 thoroughly drilled little 
 raggedness associated amateur work . Octoher 14 
 Huddersfield Glee Madrigal Society gave programme 
 chief feature E'gar fascinating ‘ Bavarian 
 Highlands ’ Suite , went smoothly Mr. 
 C. H. Moody direction . Mr. Clifford brought Yorkshire 
 Permanent Orchestra , heard Beethoven 
 fifth Symphony , Elgar ‘ South , ’ Mr. Clifford 
 ewn work , ‘ Lights , ’ attention 
 directed 

 Miscellaneous , 
 VIOLS ENGLAND 

 ORCHESTRAL MUSIC 

 BEETHOVEN — String Op . 18 , 
 Miniature scores 

 ROGER - DUCASSE , . — Romance 

