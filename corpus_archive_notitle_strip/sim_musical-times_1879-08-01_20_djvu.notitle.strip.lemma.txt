the success of the ' Orchestral Festival Concerts , " 
 although perhaps scarcely satisfy the promoter 
 in a pecuniary point of view , prove beyond doubt 
 that there be a select public in this country ready to 
 appreciate whatever be really good either in executive 
 or creative art . but english people be slow to re- 
 ceive impression ; and it be not therefore until the 
 later concert that the attendance be at all satis- 
 factory . certainly for purely orchestral concert the 
 name of Wagner appear too often in the pro- 
 gramme , but some of the grand composition of 

 Beethoven and other be also include ; and the 
 fine performance of these work , under the excellent 
 conductorship of Herr Richter , amply compensate 
 for some few defect observable in the selection , 
 we be glad to find that these concert will be re . 
 sum at St. James ’s Hall next season , when Beet- 
 hoven ’s eight Symphonies be to be perform in 
 regular order , and that an extra concert will be give 
 at the Albert Hall for the performance of the same 
 composer ’s Choral Symphony 

 the selection of modern work at Madame Viard- 
 Louis ’s Concerts have prove to we that " variety " be 
 not always ' ' charming " ' ; for certainly amongst much 
 music worthy of a hearing , we have have some com- 
 position which we could willingly have spare . the 
 performance of many of the standard orchestral 
 work by the fine band , under Mr. Weist Hill , be 
 always a powerful attraction at these concert ; and 
 although we can not conscientiously admit the béné . 
 ficiaire ’s claim to the rank of a great pianist , she be 
 much to be commend for her earnestness of pur- 
 pose and artistic feeling . an announcement be make 
 that it be the intention of Madame Viard - Louis to 
 resume her concert in October ; and as the rendering 
 of the composition of the great master be so impor- 
 tant a feature at these performance , it will be 
 advisable when comparatively unknown work be 
 introduce into the programme that they should 
 have a high claim upon our attention than the 
 mere fact of their novelty 

 lation 

 waistcoat be wet through . " at last Mendelssohn 
 reach Interlachen , only to be turn away from 
 the inn , and oblige to return to Untersee . this 
 really put he out . ' ' neither the dreadful storm , 
 nor the various discomfort I have endure , annoy 
 I half so much as not be able to remain at 
 Interlachen ; consequently , for the first time since ] 
 leave Vevay , I be out of humour for half an hour , and 
 oblige to sing Beethoven ’s Adagio in a flat major 
 three or four time over before I could recover my 
 equanimity 

 these swiss experience might be follow at 
 almost any length , but enough have be observe for 
 the purpose in view . Mendelssohn present himself 
 to we throughout as we have just see he — full of 
 passionate admiration for natural beauty , overflow 
 with spirit , and animate by a disposition the most 
 frank and generous conceivable . ' the littleness we 
 have notice elsewhere be not now find . the 
 mountain crush all foible out of he by reveal , 
 as he himself remark , his own insignificance , and of 
 they he could say : " I do believe that as such be 
 the thought of the Almighty . those who do not 
 yet know he may here see he , and the nature he 
 create visibly display 

 specific information upon the experience of some of 
 our reader in cathedral town . in give we the 
 information , they may suggest much that will be 
 valuable to the official inquiry now inaugurate . the 
 Commission will approach the subject irrespective 
 of the special interest of the lay or clerical element 
 in cathedral corporation , and will consider only the 
 desire of the community or that large portion of it 
 to whom the preservation of the Church as a state 
 institution be an object , the different member of the 
 Commission will have their own opinion as to how 
 far the grandeur and solemnity of the cathedral 
 musical service , the last and most innocent relic of 
 the ancient pomp of the Church , will conduce to its 
 defence against an attack that it would be idle to 
 deny be imminent . such a view of the case be per- 
 haps the good guarantee to organist or chorister that 
 his position and social status will be tenderly care 
 for by the newly appoint Royal Commission 

 Mr. J. McCuttocn , of San Francisco , have com- 
 pose music to an Opera on the subject of the ' ' Lady 
 of the Lake , " and , @ propos to this achievement , tell 
 the world , in an elaborate preface , what he think on 
 musical matter generally . Mr. McCulloch be a bold 
 man , and have the courage of his opinion , which , by 
 the way , be as odd as his language . as far as we 
 can make out , he be possess by a deadly hatred of 
 german music , which he call " artificially com- 
 pounded subtiltie , " and cherish a no less uncom- 
 promise love for the music of Italy . but his hatred 
 be funny than his love , and we will parade a few 
 example of its expression . here , however , we be 
 embarrass by very wealth of material , with such 
 zest and perseverance do Mr. McCulloch hunt 
 down his game . for a beginning , take a rather choice 
 specimen : ' " ' assemble together every vibratory con- 
 cordance of Haydn , Mozart , Beethoven , Weber , 
 Meyerbeer , and every other composer that ever live , 
 and there can not be make up a score of combination ; 
 yet , incredible as it may seem , in this poverty of 
 variety not a single one of the aforesaid worthy have 
 give practical evidence of be possess of a 
 thorough knowledge of the elementary combination , 
 few as they be . " this be pretty well , but our exile 
 Scot go much far , and tell we that ' ' the whole 
 old fraternity of Bachs and Handels " be mere 
 child compare to modern composer , while 
 Beethoven " in the association of sound and senti- 
 ment " be quite a failure . thus " Fidelio " miss 
 its mark ; and , say Mr. McCulloch , in a wonderful 
 sentence , ' to single out and amplify a meaningless 
 harmonic fantasy , a kind of purgatorial classic , and 
 try to pass it off as high - class opera be indicative 
 of an overweening meekness too torturously modest . " 
 Beethoven , in point of fact , be a poor creature , and 
 " none of his operatic composition ( how many be 
 there ? ) be fit to stand comparison with Weber 's , " 
 while " the power to sustain rhythymic ( sic ) and 
 poetical sentiment and consistency be wofully 
 want . " he be not even aware , miserable man ! 
 of the " unscientific notation which disfigure many 
 of his work , " and , like Weber and Mendelssohn , 
 could not tell the difference between " an auxiliary or 
 leader and a diminished seventh . " to add to his 
 crime , he " allow much to come forth that be 
 trumpery and silly , as , for instance , the Eroica Sym- 
 phony , " about which we be tell : " ' overlook the 
 many glaring inconsistency , bad harmony , and much 
 clap - trap make - up , the whole Eroica be disfigure 
 by a mawkish and discreditable attempt to make a 
 show of much mental analytical profundity when 
 it do not legitimately exist . " other composer , 
 usually call great , be treat in the same way , 
 and Mr. McCulloch smash our whole pantheon of 

 hero . happily he leave we something to worship , 
 namely , Bellini and — himself , for have not Mr , 
 McCulloch write his " ' Lady of the Lake " in Scotch 
 style , and have not Scotland a " ' peculiarly character- 
 istic national music , but little know at home and 
 scarcely at all abroad , with rhythmic beauty and 
 ingenious constructive technological fancy far in 
 advance of any other nation ? " Bellini we know , 
 but not our McCulloch ; and how much we long to 
 make the acquaintance of such a man in this some- 
 what uniform and monotonous world there can be no 
 need to say . will not Mr. Carl Rosa act in the 
 matter 

 thoroughly sound , teacher the most eminent in their 
 several department be engage , and every facility 
 be afford to the pupil of attend the concert 
 of the orchestra , the rehearsal , and the Organ Con- 
 certs . an important portion of the instruction , too , 
 be the ' ' College Choir , " where singer be carefully 
 train for chorus - singing ; the course of study , how- 
 ever , also include general musical education . " we 
 invite to this choir , " say the musical director in his 
 address , ' " ' only those who be in earnest , and who 
 will meet our effort to teach with correspond effort 
 to learn , who will attend the appoint hour for 
 study and the concert . a private student in the 
 college who lose a lesson be the chief sufferer ; but 
 the neglect of a member of the choir injure that 
 symmetry which be make by the perfect balance of 
 the part . " these word can not be too much im- 
 press upon all who desire to attain eminence in the 
 art they have choose 

 amoncst the number of song forward to we for 
 review we scarcely remember one in which any indi- 
 cation be give what voice it be originally write 
 for , or indeed whether it be intend for a female 
 ora male vocalist . it might reasonably be imagine 
 that the nature of the word would , in many case , 
 sufficiently show whether they be to be sing by a 
 lady or a gentleman ; but consider that we now 
 hear singer of the gentle sex warble the most 
 impassioned serenade , it be evident that we be not 
 to be in the slight degree guide by the character 
 of the composition . it be true that catalogue of 
 vocal work often inform we of the compass of each 
 song name ; but this be a matter entirely of pitch , 
 and leave we as much as ever in the dark as to the 
 ey of voice intend . if it be urge as a reason 
 or this important omission that a song will sell more 
 extensively when the voice it be compose for be not 
 mention , why should we not have piece publish 
 merely call ' ' Instrumental Solos , " on the supposi- 
 tion that they be equally available for a violin , flute , 
 clarionet , or indeed any instrument which can pro- 
 duce the note , and upon which the passage can be 
 execute ? surely what be know as timbre must 
 have something to do with the effect of an instru- 
 mental composition ; and be this to be entirely ignore 
 in one write for the voice ? would Beethoven ’s 
 " Adelaide " or Mozart ’s " ' Qui sdegno " satisfy their 
 composer if the former be transpose for a bass 
 or the latter for a tenor ; and , if not , must we not 
 look upon it as a sign of decadence in vocal music 
 that the same song be publish in all sort of key to 
 suit any amateur vocalist who may please to sing it ? 
 surely a composer must know well what he mean ; 
 and , even if his intention should be disregard , at 
 least every musician would desire that they should 
 be place upon record 

 it be usually consider that England be slow in re- 
 cognise the musical work of foreign artist ; but 
 two instance have lately occur which seem to 
 prove that , even admit the partial truth of this 
 reproach , we be by no means singular in our tardi- 
 ness . in Paris , it appear , attempt have recently 
 be make to introduce some of the operatic work 
 of Wagner , which have not by any mean be re- 
 ceive with warm token of encouragement ; but a 
 selection of his music perform on the composer 's 

 favour by bright weather , the Festival begin auspi- 
 ciously on the afternoon of the 23rd ult . the Cathedral 
 soon fill , and the scene be decidedly imposing as , to the 
 strain of the organ , the long array of surpliced singer 
 file in to their appoint place . the band , by the way , 
 be not surplice , and the appearance of its member in the 
 foreground of the platform seem a little incongruous . 
 Attwood ’s Coronation Anthem , ' ' I be glad , " open the 
 service , and be sing with great spirit and effect . un- 
 fortunately its commencement suffer from a misunder- 
 standing on the part of the organist , between whose 
 instrument and the orchestra moreover some slight differ- 
 ence of pitch make itself unpleasantly feel . but this be 
 forget in the interest call forth by Mr. Bridge ’s new 
 setting for voice , organ , and orchestra of the Magnificat 
 and Nunc dimittis . Mr. Bridge seem here to have in- 
 tend a pleasing and popular " service " rather than one 
 illustrative of his high power ; and it be clear far 
 that the accompaniment be not originally mean to be 
 orchestral . popular the music will certainly become , as 
 earn of which it be hear on this occasion with the 
 satisfaction which readily understand work never fail to 
 give when they have any merit at all . Spohr ’s ' last 
 judgment " form the piéce de résistance , and no well 
 could have be choose , since it always borrow solemnity 
 from its surrounding when hear in a cathedral , and never 
 produce half such an effect elsewhere . I can not praise 
 the performance without reserve , because the orchestra 
 often leave much to wish for , and be now and then posi- 
 tively offensive . the vocal music , on the other hand , 
 receive a fair amount of justice , the choir singe in- 
 variably with spirit and correctness , sometimes , as in 
 " bl be the depart , ' with touching expression . 
 above all do the boy distinguish themselves — the fine 
 quality of their voice , their prompt attack , just in- 
 tonation , and oneness of delivery be point that 
 connoisseur could not weary in admire 

 Miss Jessie Jones seem a little out of voice , but Miss 
 Sherrington sing well than I ever remember to have 
 hear she before , and bring out admirably the feeling of 
 the music . of Madame Patey , who have little to do , it be 
 needless to say more than that she be in full possession 
 of her great power ; while both Mr. Maas and Signor 
 Foli discharge their respective task in a manner becom- 
 e their repute . Mr. Maas especially win golden opinion ; 
 his pure voice and refined style give to his music all 
 needful effect . the service end with the " hallelujah " 
 from Beethoven ’s ' ' Mount of Olives , " in which the voice 
 be again conspicuous for dash and vigour 

 on the following day another imposing congregation 
 assemble , every seat , as far as I could observe , be 
 occupy , and splendid weather lend to the whole fes- 
 tivity a brightness and animation most enjoyable . the 
 order of proceeding be exactly that of the previous day , 
 the music alone be change . this time the introductory 
 work be the Overture to Handel ’s ' " ' Samson , " of which 
 not more than the minuet receive anything like justice , 
 and even that lack finish . an anthem would , under the 
 circumstance , have be far preferable and well suit 
 to the occasion . amend’was make , however , before the 
 first lesson , when Madame Patey sing Gounod ’s " there 
 be a green hill far away , " accompany by organ and or- 
 chestra . the effect of this could not be mistake , and 
 every one present must have feel it ; with such earnestness 
 do the singer deliver her beautiful and expressive theme 

 ye only once at a concert of the " British Orchestral 

 iety , " under the direction of Mr. Mount . the first 
 and last movement of this work be excellent example 
 of scholarly writing , the treatment of the subject 
 evidencing throughout the hand of a master ; but the 
 Gavotte and Musette — which stand in place of the usual 
 Minuet and Trio — fairly take the audience by surprise , 
 not only by the quaint and melodious character of the 
 theme , but by the exquisite orchestral colouring , which , 
 although thoroughly suggestive of the time in which these 
 dance flourish , be positively refreshing , even to those 
 who come prepared to listen to ' modern effect . " at 
 the end of the Symphony the composer be call into 
 the orchestra , and receive an ovation which must have 
 convince he how legitimately and decisively he have win 
 hissuccess . the other orchestral work be Beethoven ’s 
 " Pastoral Symphony , " ' and Weber ’s ' Jubilee ’' Overture , 
 both of which be finely play . M. Saint - Saéns ' ’s per- 
 formance of his own pianoforte Concerto in G minor be 

 temarkable for digital dexterity and perfect command of 

 ye only once at a concert of the " British Orchestral 

 jety , " " under the direction of Mr. Mount . the first 
 and last movement of this work be excellent example 
 of scholarly writing , the treatment of the subject 
 evidencing throughout the hand of a master ; but the 
 Gavotte and Musette — which stand in place of the usual 
 Minuet and Trio — fairly take the audience by surprise , 
 not only by the quaint and melodious character of the 
 theme , but by the exquisite orchestral colouring , which , 
 although thoroughly suggestive of the time in which these 
 dance flourish , be positively refreshing , even to those 
 who come prepare to listen to " modern effect . " at 
 the end of the Symphony the composer be call into 
 the orchestra , and receive an ovation which must have 
 convince he how legitimately and decisively he have win 
 hissuccess . the other orchestral work be Beethoven ’s 
 " Pastoral Symphony , " and Weber ’s " Jubilee " Overture , 
 both of which be finely play . M. Saint - Saéns ’s per- 
 formance of his own pianoforte Concerto in G minor be 

 _ temarkable for digital dexterity and perfect command of 

 Baden - Baden.—Orchestral Matinée ( July 4 ): overture 
 ' * Medea ' " ' ( Cherubini ) ; Serenade ( Haydn ) ; love - song 
 from ' ' the Tempest , " for stringed orchestra ( Taubert ) ; 
 " * Waldweben , " ' from " Siegfried " ( Wagner ) ; March from 
 ' " ' Christus " ' ( Liszt 

 Mannheim.—Ninth Music Festival of the Middle - Rhine 
 ( July 27 and 28 ): ' ' the creation ' : ( Haydn ) ; overture , 
 ' ' Egmont ’' ( Beethoven ) ; Violin Concerto , execute by 
 Jean Becker ( Beethoven ) ; duet from " ' Jessonda " ( Spohr ) ; 
 symphony , no . 2 ( Brahms ) ; ' * Walpurgis night " ( Men- 
 delssohn ) , vocal soli 

 Bad - Nauheim.—Historical Evening of Dr. Ludwig 
 Nohl : subject , ' ' the Development of Instrumental 
 Music . " performance : prelude and Fugue ( Bach ) ; Mili- 
 tary Symphony , Adagio and Allegro ( Haydn ) ; " Jupiter " 
 symphony , Andante and Minuet ( Mozart ) ; Symphony 
 no . 5 , Scherzo and Finale ( Beethoven ) ; Années 
 Pélerinage ( Liszt ) ; overture , ' ' Rienzi " ( Wagner 

 the MUSICAL TIMES.—Avceusrt 1 , 1879 

 0 often mar the beauty of singing in our day . Schumann ’s 
 overture to ' ' Manfred , " on this first hearing , fail to 
 excite my interest as I have expect , until towards its 
 close ; when a strain of mournful and even tragic solemnity 
 seem music to which the die Hamlet might have 
 breathe out his last word — " the rest be silence . " the 
 rst movement of Schubert ’s unfinished Symphony , con- 
 tain some charming subject for the violoncello and inter- 
 woven motivi for the other instrument ; but it — like most 
 of this composer ’s production — strike I as be some 

 L. van Beethoven 

 what overwrought . Schubert , when he seize a graceful 
 idea , be apt to repeat and repeat it , to turn and twist it 
 again and again , to reframe and intertwine it , until the 
 ear become wearied with a sense of monotony even while 
 take pleasure in the musical phrase and acknowledgin 
 the contrapuntal skill with which they be work oa 
 rework . Beethoven ’s " Seventh Symphony , " with its 
 sublimely poetical slow movement and exquisitely playful 
 scherzo — execute to absolute perfection — close the first 
 evening ’s musical feast . the day ’s enjoyment harmonise 
 well with the evening ’s entertainment ; for a town of choicer 
 loveliness in situation and scenery be rarely to be see . 
 place on the bank of a rapid stream — the river Salzach — 
 surround by green height and distant mountain , well- 
 wooded slope on which picturesque castle and lordly 
 mansion be perch , shore along which brightly and 
 variously colour house range in the neatness and grace 
 of adornment that characterise most german dwelling — 
 this spot form an endless succession of picture and charm- 
 e landscape , besides afford scope for enchanting 
 drive amid lane and woodland 

 as a final touch — which would have rejoice the heart of 
 Walter Scott himself , who know , none well , that good fare 
 crown befittingly the enjoyment of nature ’s romantic 
 scenery and refined art - pleasure — the eating in Salzburg 
 be of the good ; trout that would have have Isaac Walton ’s 
 cordial commendation , chicken delicate and ' ' tender as 
 morning dew " ' ( to use a french gastronome ’s phrase ) , with 
 alpine butter and fresh cream , make each day ’s repast a 
 feast worthy of the ' ' Musikfest " at night 

 18 , Juli , abend 7 uhr 

 Ouverture ( nr . 3 ) zu " Leonore " L. van Beethoven 

 concert fiir zwei Claviere _ ... oie one « . W.A. Mozart . 
 ( Briider Louis & Willi Thern aus Pest 

 arie aus der Oper " Die Hochzeit des Figaro " .... W.A. Mozart 

 Frau Schuch - Proska . ) 
 nite sea sie ape - .. L. van Beethoven . 
 ( Herr Concertmeister Griin aus Wien . ) 
 Sachsens Monolog aus dem 2 . Akte der " Meister- 
 singer , " ' ' be duftet doch der Flieder " lin 
 ( Herr Opernsanger Dr. Krauss . ) 
 PANONIO TSG : ccs . Ses | sce see , see , one ’ will he BROERET . 
 the performance of the third Overture that Beethoven 
 write for his opera of ' " Fidelio " ' under the name of 
 ' * Leonore , " be absolutely faultless . the precision and 
 perfect ensemble of this viennese orchestra be wonderful ; 
 and the effect produce , when they play composition of 
 such extreme elaboration and difficulty as this one , be 
 consummately gratifying to the musical sense . the 
 brother Thern play with great delicacy of execution 
 and perfectly in the style of the master ’s composition they 
 give . Herr Richter conduct Wagner ’s ' ' Vorspiel " with 
 true spirit of course ; and the executant he marshal 
 be implicit minister to his will . Frau Schuch - Proska 
 so completely delight her audience by the mode in which 
 she sing Mozart ’s enchanting recitative and air , ' ' Deh 
 vieni " that she be encore ; but I own to having hear it 
 sing with far more sentiment and charm by other singer I 
 could name . Herr Griin ’s execution of Beethoven ’s Violin 
 Concerto be a masterly piece of manual dexterity and 
 finished performance , and elicit lively applause . the 
 imaginative and characteristically treat ' " ' Monolog " by 
 Wagner have a very efficient interpreter in Dr. Krauss ; 
 whose magnificent voice and fine declamation give full 
 effect to this piece ; while the matchless way in which it 
 be accompany hold the hearer ’s breath suspend with 
 interest and admiration . it be a piece of music that make 
 one feel one ought to hear it many time ere properly 
 understand and appreciate its entire poetic intention . 
 Mozart ’s exquisite E flat Symphony make a fitting climax 
 to the entertainment on this occasion 

 owe to the indisposition of two of the lady who be 
 to have take part in the third day ’s programme , it be 
 modify as follow 

 5 . ( a ) Minuten - Walzer win ann eat 
 ( b ) Tiirkischer Marsch _ ... be ine sab we 
 ( Fur zwei Claviere ( Briider Thern . ) 
 6 . Zwei Lieder ( a ) ' ' Mondnacht " ... eve ee 
 ( b ) " * Friihlingslied " por wie 
 ( Frau Schuch - Proska . ) 
 7 . Clavier - Quintett , op . 44 ... eat nae « Rob . Schumann , 
 ( Die Herren Louis Thern , Griin , Hofmann , Zdllner , Giller . ) 
 with thorough refinement Mozart ’s Quartett in C major 
 be execute by the four ' ' Philharmoniker " gentleman ; 
 and then come a group of three entrancing " Lieder , " one 
 more exquisite than the other , sing to perfection by Dr , 
 Krauss , whose fine manly tone and rich voice be equal 
 by his taste and feeling in expression . these Lieder be 
 accompany by Herr Hans Richter , whose delicacy and 
 skill in accompany vie with his consummate excellence 
 asaconductor . he not only play the elaborately diff . 
 cult accompaniment to these three Lieder with the polish 
 and mastery of a concerto - player , but he give enchanting 
 effect to the dainty brilliancy of those to Mozart ’s duet 
 from ' ' Don Juan , " and to Schumann ’s ' Mondnacht , " 
 and Mendelssohn ’s animated ' ' Frihlingslied , " ' in each of 
 which last Frau Schuch - Proska distinguish herself by 
 beautifully characteristic vocalisation . she be in even 
 well voice than on the two precede occasion , and sing 
 really charmingly . Schumann ’s two glorious composition , 
 the ' ' Andante und Variationen , " and Quintett , be give 
 in pre - eminent style , and complete the intense satisfaction 
 with which this truly delightful " ' Salzburger Musikfest " 
 be enjoy by 

 Chopin . 
 . van Beethoven 

 ob . Schumann , 
 Mendelssohn 

 the MUSICAL TIMES.—Avceusr 1 , 1879 

 artistically render , and Spohr ’s " as pant the hart , " the solo be 
 well sing by Miss Williams . Mr. Detmar and Mr. Hyslop be also 
 very successful . ' " Ti Prego " ( Curschmann ) be effectively render 
 by Miss Williams , Miss Ball , and Mr. Detmar . the second part com- 
 mence with Beethoven 's Overture , Egmont , arrange as a piano- 
 forte duet , well play by Mr. Sharp and Mr. Ward . the trio , " O 
 memory " ( Leslie ) , by Miss Place , Mr. Ball , and Mr. Hyslop , be well 
 receive and encore , as be many of the song . the choir sing 
 several somo with good expression , reflect great credit on the 
 conductor , Mr. Sharp , the organist of the church . Mr. Ward be an 
 able accompanist 

 BERWICK - ON - TWEED.—The nineteenth Annual Festival of Parochial 
 Choirs in connection with the Durham Diocesan Church Choral Asso- 
 ciation take place in the Parish Church on Tuesday , the 15th ult . , on 
 which occasion the choir consist of about 400 voice . the move- 
 ment be originate in 1860 by Mr. T. Rees Evans , and annual 
 meeting have since be hold in the diocese , the centre of attraction 
 every fourth year be the Cathedral Festival . the great good 
 derive from F on meeting be fully show by the steady chanting of 
 the psalm and canticle , the precision with which the five - part 
 anthem be execute , and the careful attention to light and shade in 
 the rendering of the metrical tune . the prayer be intone by 
 the Rev. H. Clementi Smith , M.A. , late precentor in Manchester 
 Cathedral ; the lesson be read by the Rev. T. Procter , M.A. , Vicar 
 of Tweedmouth , and the Ven . Archdeacon of Lindisfarne ; and the 
 sermon preach by the right Rev. the Bishop of Edinburgh . Mr. 
 Alfred Heap , organist to the right Hon . the Earl of Home , preside 
 at the organ ; and Mr. Rees a Becket Evans , Organist of the Episcopal 
 Church , Melrose , officiate as Conductor 

 a 
 Paynestown , Rathmolyon and Agher , in addition to several individual 
 member of other choir . before the service , the hymn " praise , 
 soul , the king of Heaven , " be sing to Dr. Gauntlett ’s tune . the 
 Venite be sing to a single chant by Sir F. Ouseley , the ro7th Psalm 
 to a cycle of four chant by Mr. Young , of Lincoln , and the rast Psalm 
 to a chant in E by Mr. Barnby . the Te Deum be tour in F , and 
 the Anthem , Sir G. Elvey ’s " o give thank . " the Kyrie be take 
 from Dr. Steggall ’s Service in G , and the Introit from Sir R. Stewart 's 
 service in the same key . the Jubilate be sing to a fine double chant 
 by Sir . J.Goss . the singing of the united choir be very sati 
 throughout , the various lead in the Anthem and Te deum bein ; 
 up with precision . the sermon be preach by the Rev. G. N 
 Chaplain to the Bishop of Meath . Mr. W. H. Gater , Mus , 
 choirmaster to the Association , preside at the organ 

 New MILL , near HuppersFIeLp.—On Sunday , the 6th ult . , special 
 service be hold in the Parish Church , and collection make in 
 behalf of the Day and Sunday school . the sermon in the morning 
 be preach by the Rev. E. Brownrigg , Vicar of Shelley , and in the 
 evening by the Rev. C. A. Hulbert , Vicar of Almondbury and Hon , 
 Canon of Ripon Cathedral . the musical portion of the service wag 
 render very efficiently under the direction of Mr. C. E. Holmes , 
 Organist and choirmaster , who preside at the organ . the anthem 
 in the morning be " it come even to pass , " Sir F. A. G. Ouseley ; 
 and the Jubilate be sing to Dr. John Smith 's ( Dublin ) setting in b 
 flat . in the evening , the anthem be Handel ’s air , " praise the Lord 
 with cheerful noise " ( Esther ) , and the chorus " fix in his ever . 
 last seat " ( Samson ) . the solo vocalist be Miss Annie jenkinsca , 
 the chorus be sing with great precision and effect . the Magnificat 
 and Nunc dimittis be take to Sir R. P. Stewart ’s setting in G. at 
 the close of the evening service Beethoven 's " hallelujah Chorus , " 
 from the Mount of olive , be sing . the hymn at both service be 
 special , and the tune include two new one by the Organist , which 
 be especially well render 

 Newport , Essex.—On Thursday , the 17th ult . , the Annual Festival 
 of the North - West Essex Choral Association be hold in the Parish 
 Church . Thg choir number about two hundred voice . Mr. 
 Worsley Ste ~ aforth , the choirmaster to the Association , preside at 
 the organ . ' xe response be sing to Tallis ’s music ; the psalmg 
 in the morning @. ing take to gregorian tone , and those in the after- 
 noon service to anglican chant . Mr. Warwick Jordan ’s arrangement 
 of the fifth Tone be use for the Te Deum , and the anthem be Sir 
 John Goss ’s " o give thank . " an excellent sermon be preach by 
 the Rev. Edgar Smith , of Highgate . the festival be in every respect 
 a great success 

 Nortu Berwick , N.B.—Mr . Frank Bates , Organist of St. Baldred ’s 
 Church , give an organ recital on Monday , the 14th ult . the pro- 
 gramme include Bach ’s Fugue in G , and selection from Handel , 
 Mendelssohn , Smart , Guilmant , & c 

 SHERBORNE.—The forty - ninth Commemoration Concert be give 
 on Thursday evening , the 3rd ult . , by the School Musical Society in the 
 new school - room . the programme be an ambitious one , but its 
 success be quite equal to its aim . part i. open with two move- 
 ment from Beethoven ’s Symphony in C , perform by the school 
 orchestra , assist by eminent artist from London . this be fol- 
 low by Mendelssohn ’s Cantata Praise Fehovah , which by its massive 
 breadth and the prominent part take in it by the chorus be the good 
 possible work that could have be select for the occasion . in this 
 the school chorus highly distinguish itself under the able conducting 
 of Mr. Louis N. Parker . the solo be all sing by member of the 

 take 
 ent , 
 ac 

 two new number 

 BOOK xi . 
 Romanza from the Second Concerto Mozart . 
 Quintett ( Giuri ognuno ) 2 Rossini . 
 adagio cantabile ( Sonate > pathétiaue ) Beethoven . 
 Bourrée — d major Handel 

 BOOK xii . 
 Chant de Berceau ; Stephen Heller . 
 Andante — g minor ( from a Sonata ) .. Haydn . 
 Adagio — d major ( from a Sonata ) ... Pinto . 
 Traumerei , Lento — f major Schumann 

 for the Pedal Piano ) .. 4 3 0 
 allegretto alla polacca , ' Seow iattinirents see Op . 8 , for 

 Violin , Viola , and Violoncello ... 3 0 
 Scherzo from Beethoven ’s trio , for Violin , Viola , ond Violon 

 Cello , op.9,no.r = hae 
 menuetto from ditto , ditto , op . . no . : 2 a4 nie 
 Menuetto from Schubert 's Quartett , Op . 29 .. wi ke 

