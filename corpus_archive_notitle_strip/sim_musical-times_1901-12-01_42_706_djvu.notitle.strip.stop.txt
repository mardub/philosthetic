TIMES 

 vocal recitals given Steinway Hall Thurs- 
 day night Mr. Edward Iles , lately great progress 
 art . voice , sympathetic baritone considerable power , 
 gained qualities effect ; singer 
 isa finished musician , performance songs different styles 
 pleasant listen . Beethoven “ Kuss , ’ Schumann 
 “ Gestandniss , ” Schubert beautiful “ ‘ Nacht und Traume , ” “ Der 
 Einsame ” ’ instances Mr , Iles wide knowledge classics , 
 inclusion songs Max Bruch , Eugen d’Albert , Richar 
 Strauss , Widor , proves artist broad sympathies . 
 best Parry splendidly vigorous Anacreontic “ Fill , 
 boy , deep draught " Stanford arrangement “ ‘ Eva Toole ” ; 
 musicianly songs composition given 
 success . charmingly simple melodious 
 ballad , ‘ walked forth , ” , “ Little soul disdain , ” 
 intelligent singer hardly fail effect . Miss White 
 “ “ Montrose Love Song ” ended recital , sung , 
 faster usual 

 DAILY TELEGRAPH 

 Dr. Edward Elgar Orchestral Variations 
 recently produced Berlin Royal Orchestra , 
 ynder Herr Weingartner . Dr. Otto Lessmann , 
 foremost German musical critics , greets 
 splendid work genuine trumpet - blast 
 enthusiastic appreciation , , hope , 
 heard musical circles Fatherland , 
 find echo musicians music 
 lovers wish appreciate great 
 peautiful art country origin , 
 strange - sounding unknown com- 
 oser . referring Dr. Elgar ‘ Dream 
 Gerontius , ’ Dr. Lessmann heard Bir- 
 mingham year , writes follows 
 Allgemeine Musik - Zeitung 15th ult 

 Elgar , English artist arisen 
 courage himself,—one horizon wider 
 composing compatriots , 
 strives imitate , draw 
 imagination , find _ particular tone- 
 language express . Variations , 
 overture ‘ Cockaigne , ' 
 pleasure reading Score , confirmed 
 opinion England hope progress 
 musical art creative work Edward Elgar ; 
 , works convince , public , 
 rate cultured unprejudiced musicians , Elgar 
 exceptional talent , high aim extraordinary 
 technical equipment . work reveals 
 respect inventive power , brilliant ( geistvoll ) workman- 
 ship , sovereign command means expression , 
 equals best produced — 
 way orchestral variations — Beethoven . 
 hesitate , taking , place level 
 Brahms famous Haydn - Variations , 
 regards exploitation handling 
 orchestra . indescribably brilliant performance 
 proved Herr Weingartner orchestra 
 studied work love enthusiasm . 
 case great talent , work 
 importance , Herr Weingartner repeated 
 Variations concerts . feel con- 
 vinced render service 
 music lovers audience , 
 work 

 Elgar ‘ Variations , ’ way , bid fair 
 round continental cities . Professor 
 Julius Buths , Diisseldorf , belongs honour 
 introducing German audience . 
 heard Mayence ( 
 Professor Fritz Volbach ) , Wiesbaden ( Professor 
 Franz Mannstaedt ) , Berlin ( Herr Wein- 
 gartner ) . Early December 
 produced Brussels , M. Ysaye , 
 Herr Weingartner announced 
 Munich . United States , , 
 matters musical , cue Germany , 
 expected follow suit . per- 
 formance abroad Dr. Elgar ‘ Dream Gerontius ’ 
 place Diisseldorf , roth inst . , 
 direction Professor Buths . enthusiastic 
 conductor provided poetical singable 
 translation poem : difficult task 
 ably accomplished . auspicious event , 
 place rgth inst . , anon 

 illustrate character remarkable Negro patriot 
 furnishes title composition . 
 entirety overture shows advance Mr. Coleridge- 
 Taylor previous writings orchestra . second 
 concert , gth ult . , Brahms Fourth Symphony 
 E minor furnished principal feature , soloist 
 M. Ysaye 

 orchestral concert exceptional importance , 
 series , given 16th ult . , Mr. 
 Henry J. Wood conducting . orchestra consisted 
 200 executants , comprised 130 string players , 31 
 wood - wind , remainder players brass 
 percussion instruments harpists . finest effects 
 secured excerpts Wagner operas , 
 echoes building interfered inter- 
 pretative clearness Beethoven seventh Symphony . 
 solo vocalists Mrs. Henry J. Wood 
 Mr. Philip Brozel 

 POPULAR CONCERTS 

 QUARTET CHAMBER CONCERTS 

 Kruse Quartet commenced series Chamber 
 Concerts , St. James Hall , October 30.——Some 
 excellent quartet playing heard Bechstein Hall , 
 October 30 , Hans Wessely party.——The 
 Wietrowetz Quartet Party gave commendable concert 
 4th ult . , Fraulein Wietrowetz violin recital 
 7th ult . , Bechstein Hall —— Chaplin Trio 
 heard advantage concert given party 
 5th ult . , Steinway Hall . remarkably 
 fine Chamber Concerts given 14th 21st 
 ult . , M. Ysaye , Herr Becker , Signor Busoni , rare 
 combination talent.——M. Ysaye Signor Busoni , 
 7th ult . , gave recital Queen Hall Beethoven 
 Sonatas G ( Op . 96 ) , C minor ( Op . 30 , . 2 ) , 
 Kreutzer , interpretation 
 highest artistic interest . Mr. Tovey gave Chamber 
 Concerts 7th , 14th , atst ult . , St. James 
 Hall , pianoforte solos including Bach Goldberg 
 variations . — -—-Mr . Charles Phillips Miss Ethel Barns 
 began fresh series Concerts r4th ult . , 
 Bechstein Hall , performance London 
 given Christian Sinding Sonata C violin 
 pianoforte , proved melodious pleasing work , 
 produced anew Song Cycle Mr. William 
 Wallace , entitled ‘ Lords Sea 

 

 Mr. Hirwen Jones concert , 8th ult . famous conductor gave great ‘ Leonora ’ b 

 Miss B. Spencer vocal recital , 13th ult . overture Beethoven ; symphony minor , tt 

 Mr. H. W. Charles pianoforte recital , r4th ult . | Saint - Saéns — new tous ; fantasia , ‘ Impressions d’Italie , ’ W 

 821 

 Concerto ( Op . 58 ) , grand performance given . 
 — — tThe second concert took place 12th ult . , 
 Town Hall filled . attraction 
 visit M. Busoni , gave poetic reading 
 Beethoven Pianoforte Concerto E flat , played 
 brilliant style pieces Chopin Liszt . orchestral 
 works : Beethoven Pastoral Symphony , beautifully 
 played ; Volbach symphonic poem , ‘ Es waren zwei 
 Konigskinder , ’ new , given cordially received ; 
 Wagner ‘ Huldigungsmarsch . ’ Mr. Halford con- 
 ducted customary judgment skill 

 drawing - room concerts held Grosvenor 
 Rooms Grand Hotel , direction 
 Mr. Max Mossel , resumed October 31 . 
 function vocal pianoforte recital Miss Marie 
 Brema Professor Carl Friedberg . lady introduced 
 sacred songs 14th century , interesting , 
 scarcely harmony surroundings . 
 sth ult . , Mr. Howard Hadley gave excellent 
 appreciated pianoforte recital Masonic Hall . 
 programme main chosen , unhackneyed , 
 included Chopin Sonata B minor ( Op . 58 ) , 
 Etude young Englishman , Clement Harris , 
 died fighting cause Greeks , 1897 . Madame 
 Kirkby Lunn vocalist.——On the13th ult . , Madame 
 Albani party gave concert Town Hall , 
 interesting item Mozart aria 
 ‘ Non temer , ’ violin obbligato , performers 
 gifted vocalist Lady Hallé.——Messrs . W. H. 
 Priestley Sons gave invitation concert 
 19th ult . , New Temperance Hall , Miss Fanny 
 Davies , Miss Alice Davies , Miss Edith Robinson 
 performers . new hall great acquisition , 
 soon request concert givers 

 MUSIC DUBLIN . 
 ( CORRESPONDENT 

 Dublin Musical Society opened season 
 miscellaneous concerts , Madame Albani , Mr. 
 Santley , Miss Muriel Foster , Mr. Melfort D’Alton 
 sang , Lady Hallé played , Society Choir sang 
 - songs . Dublin Chamber Music Union 
 gave concert , 14th ult . , Mr. Charles 
 Schilsky appearance violin place 
 Mr. Wilhelmj . programme included Schumann 
 minor String Quartet , Beethoven Trio B flat , 
 Fauré violin pianoforte Sonata A. 
 works , Mr. Schilsky showed artist 
 great merit , excellent performances old 
 friends , Signor Esposito , Herr Bast , Mr. Grisard , Mr. 
 Delany , concert distinctly best 
 heard Dublin years 

 pleasure able admirable 
 programme given Mrs. Coslett - Heller 
 song recital . lady possesses cultivated mezzo- 
 soprano voice considerable power sweetness , 
 uses expression taste . Madame Fromm 
 played Beethoven seldom heard Sonata Op . 7 , 
 lighter pieces considerable technical ability taste 

 MUSIC EAST ANGLIA . 
 ( CORRESPONDENT 

 18th ult . , Dr. A. L. Peace inaugurated series 
 organ recitals Wellpark United Free Church , 
 success 

 Glasgow Amateur Orchestral Society presented 
 somewhat ambitious programme concert 
 season , 21st ult . included Beethoven Violin 
 Concerto , Bizet dramatic overture ‘ Patrie , ’ Dr. 
 Elgar prelude ‘ Dream Gerontius , ’ last- 
 named performed time Glasgow . 
 Mr. Louis Pecskai played solo Concerto , 
 contributed solos ; Miss Vida Giolli vocalist 

 MUSIC GLOUCESTER DISTRICT . 
 ( CORRESPONDENT 

 Gloucester Orpheus Society ( con- 
 ducted Mr. A. Herbert Brewer ) busy preparing 
 concert given New Year Day . Dr. C , 
 Harford Lloyd written - song occasion , 
 entitled , ‘ Men Gotham , ’ 
 dedicated Society . items 
 performed Schumann hunting songs , 
 horns obbligati 

 series Chamber Concerts Gloucester 
 Cheltenham , arranged Miss Ellicott 
 Miss Hirschfeld , successfully given Guildhall , 
 Gloucester , October 24 . instrumental works 
 Trio D minor ( Mendelssohn ) , Trio 
 B flat ( Beethoven ) , artists Herr Wessely 
 ( violin ) , Mr. J. E. R. Teague ( violoncello ) , Miss Hirschfeld 
 piano ) . Herr Wessely lady played 
 Kreutzer Sonata . second concert took place 
 Rotunda , Cheltenham , 2oth ult . , instrumentalists 
 Miss Ethel Barns ( violin ) , Herr Paul Ludwig 
 ( violoncello ) , Miss Isabel Hirschfeld ( pianoforte ) . 
 Mr. Charles Phillips contributed number songs , 
 ably accompanied Miss Ellicott 

 Cheltenham Philharmonic Society , 
 direction Mr. C. J. Phillips , great success 
 concert , October 23 . chief works given 
 Mendelssohn ‘ Walpurgis Night , ’ Beethoven 
 C minor Symphony , composer ‘ Calm Sea 
 Prosperous Voyage 

 Cheltenham Musical Festival Society ( Mr. J. A. 
 Matthews , conductor ) announce course 
 season produce new work Dr. Iliffe , entitled , 
 ‘ Power Song 

 MUSIC LIVERPOOL DISTRICT . 
 ( CORRESPONDENT 

 Concerts Philharmonic Society occurred 
 October 22 , 5th ult . , roth ult . 
 , Madame Melba vocalist , orchestra , 
 Dr. Cowen , relied Tschaikowsky Suite . 2 , 
 Smetana ‘ Vysehrad ’ symphonic poem , Beethoven 
 ‘ Congratulation ’ minuet , Elgar ‘ Imperial ’ march . 
 second concert , Gluck ‘ Orpheus ’ performed , 
 vocalists Miss Helen Jaxon , Miss Teresa Blamy , 
 Miss Marie Brema , orchestra played Dr. 
 Cowen ‘ Butterfly Ball ’ overture Glazounow 
 ‘ Scéne de Ballet . ’ chief interest concert 
 tgth ult . centred M. Ysaye performance 
 Beethoven Concerto D. Sullivan overture 
 ‘ Macbeth , ’ Haydn ‘ Surprise ’ symphony , 
 chief orchestral items 

 initial concert Orchestral Society Season 
 occurred roth October , Philharmonic Hall , 
 able directorship Mr. A. E. Rodewald . 
 Special interest centred performance Dr. 
 Elgar new Military Marches , ‘ Pomp ’ ‘ Cir- 
 cumstance , ’ played time . fully 
 charged vivid life , robust colouring , individuality , 
 particular brightness orchestration . Herr Frdlich 
 vocalist , Mr. Leonard Borwick played Beethoven 

 Mr. W. T. Hoeck conducted 

 M. Paderewski pupil — Mr. Harold Bauer — played Brahms 
 concerto capitally , ‘ Air de Ballet ’ 

 Saint - Saéns , displayed exquisitely pearly touch 
 delicacy style . Naturally , Beethoven Second Symphony 
 presented band difficulty . 
 21st ult . , Tschaikowsky Symphony Pathétique 
 Wagner selection performed 

 Mr. Lane bold undertake Handel ‘ Israel ’ 
 force 600 performers ; result 
 , long great 
 oratorio attempted , far worse recital 
 thankfully accepted . , how- 
 , mistake break chain massive choruses 
 songs selected works author , 
 individually good interpolated items 
 

 indebted Mr. Carl Fuchs bringing 
 Dolmetsch family . Schiller Hall isa suitable 
 room enjoyment charming selection old works 
 played , , old instruments . Mrs. 
 Dolmetsch added sixteenth century ditties 
 great discretion refinement 

 Mr. Brodsky chamber concerts resumed 
 13th ult . , delightful interpretation Beethoven 
 String Quartet ( Op . 127 ) , pleasant able 
 record connection Royal College Music 
 Owens College strengthened 
 appointment staff — Mr. Brodsky , Dr. Watson , 
 Dr. Pyne — occasional lecturers University 
 musical subjects . Dr. Hiles schemes 
 view fuller recognition music educational 
 force equal value classics literary subjects 
 generally 

 Dr. Pyne - commenced organ recitals 
 16th ult . , Town Hall filled enthusiastic 
 audience 

 MUSIC NORTHUMBERLAND DURHAM . 
 ( CORRESPONDENT 

 concert season given Newcastle 
 Gateshead Choral Union , Town Hall , Newcastle , 
 October 25th , great success . programme 
 consisted entirely orchestral works played Hallé 
 Orchestra , conductorship Dr. Richter , 
 Beethoven Pastoral Symphony , Dr. Edward Elgar 
 new Overture ‘ Cockaigne , ’ occupying prominent 
 positions . movements Berlioz ‘ Romeo 
 Juliet ’ Symphony singled audience 
 special marks approval 

 Chamber Music Concerts appear gaining popu- 
 larity Newcastle . 2nd ult . , Messrs. Alfred 
 Sigmund Oppenheim , conjunction Miss Effie Smith , 
 Mr. Robert Smith , Miss Gertrude Smith , performed 
 Dvordk Dramatic Quintet , works , 
 Assembly Rooms ; 4th ult . , place , 
 Newcastle Musical Society gave seventh concert 
 Chamber Music , Beethoven Quartet B flat 
 ( Op . 18 ) , Mendelssohn C minor trio , played 
 Mr. Alfred Wall , Miss E. Thew , Mr. Perry , Miss Helen 
 North Mr. Oscar Cohen 

 concert - second season , 
 Assembly Rooms , Newcastle , r8th ult . , New- 
 castle Chamber Music Society afforded patrons treat 
 unique . programme consisted 
 chiefly sonatas piano violin Beethoven , 
 — C minor ( Op . 30 , . 2 ) , 
 Kreutzer — played Mons . Ysaye Signor 
 Busoni 

 MUSIC NOTTINGHAM DISTRICT . 
 ( CORRESPONDENT . ) 
 Sacred Harmonic Society performed Elgar ‘ King 

 MUSIC SHEFFIELD DISTRICT . 
 ( CORRESPONDENT 

 programme Sheffield Musical Festival , 
 held October 1 , 2 , 3 , 1902 , issued . Choral 
 works predominate , chorus given nearly 
 cent . work festival . New works 
 Mr. Coleridge - Taylor Dr. Coward , entitled respec- 
 tively ‘ Meg Blane ’ ‘ Gareth , ’ announced , 
 ‘ Wandrer Sturm Lied ’ ( - chorus orchestra ) , 
 Richard Strauss , time England , 
 nature novelty . remaining works 
 ‘ Elijah , ’ ‘ Hymn Praise , ’ _ half - programme 
 selection ‘ Israel Egypt , ’ Dvorak ‘ Stabat 
 Mater , ’ Mozart Requiem , Bach motet , ‘ Jesu , Priceless 
 Treasure , ’ Brahms ‘ Triumphlied ’ ( baritone solo , 
 - chorus orchestra ) , Goldmark sacred opera , 
 ‘ Queen Sheba , ’ Cowen ‘ Ode Passions , ’ 
 Max Bruch ‘ Frithjof , ’ Parry ‘ Blest Pair Sirens . ’ 
 orchestral items Dr. Elgar Variations , 
 Tschaikowsky Symphonie Pathétique , Symphonic 
 Poem orchestra organ Fritz Volbach , 
 Beethoven overture , ‘ Leonora , ’ . 3 . Wagner repre- 
 sented Spinning Chorus ‘ Flying Dutch- 
 man . ’ Mr. Henry J. Wood conduct Festival , 
 chorus - master Dr. Coward organist 
 Mr. J. W. Phillips 

 Sheffield Children Musical Festival took place 
 Albert Hall , 7th , 8th , oth ult . musical 
 results - organised affair far - reaching , 
 fail favourably affect future musical life 
 city . large choirs children , strong 
 contingent men voices , sung - selected programmes 
 good - class music , direction Mr. T. Miles 
 Morgan . Mr. T. Westlake Morgan organist . 
 Hall crowded occasion 

 important concerts highly 

 interesting performance ‘ Elijah , ’ Leeds Philhar- 
 monic Society , 6th ult . soloists Miss 
 Agnes Nicholls , Miss Muriel Foster , Mr. William Green , 
 Mr. Plunket Greene , sang 
 Elijah time country . 
 low pitch happily prevails music 
 places effective parts voice 
 — transposition effected ‘ 
 word’—his dramatic instinct reading 
 highly interesting , power pathetic expres- 
 sion strikingly manifested ‘ . ’ Dr , 
 Stanford , conducted , boldness observe 
 indications tempi score closely 
 customary , effect went far prove 
 Mendelssohn interpretation music 
 unreasonable . chorus , trained Mr. 
 Fricker , sung excellent spirit , brightness , 
 intelligence , fully maintained improvement 
 shown late years . preceding evening 
 Mr. Edgar Haddock orchestra formed chief feature 
 concert given charitable purposes . material 
 band leaves room improvement , Mr. 
 Haddock allowed , 
 managed procure spirited performance 
 Meistersinger overture . Mr. Georg Jacobi conducted 
 bright rhythmical music , including suite 
 pieces ballet , ‘ La Tzigane , ’ Mr. Tivadar 
 Nachez soloist Max Bruch G minor Violin 
 Concerto . 
 enjoyable chamber concert given 
 October 30 ‘ Bohemian ’ Society , able quartet 
 local artists , Messrs. Elliott , Wright , Haigh , Giessing , 
 playing quartets Haydn , Mozart , Beethoven 
 sympathetically . modest concerts , smoking 
 forms accompaniment — happily ad libitum 
 obbligato — growing public esteem , 
 deserve . Finally , Leeds Musical Evenings , 
 October 29 , Madame Clara Butt appeared , 
 rgth ult . , briefly chronicled 

 BRADFORD 

 sion evening service , Mr. C. H. Moody , Organist 
 Choirmaster , played Mozart Clock Fantasia 
 F minor , Bach Toccata Fugue D ( Doric 

 Dover.—Mr . Wilfred Barclay gave orchestral 
 concert season Town Hall , 6th ult . 
 orchestra composed Dover Amateur Orchestral 
 Society leading amateurs district , 
 large contingent Band Royal Engineers . 
 programme consisted Mozart ‘ Jupiter ’ Symphony , 
 Mendelssohn overtures ‘ Ruy Blas ’ ‘ Hebrides , ’ 
 ‘ Notturno ’ ‘ Midsummer Night Dream ’ 
 music , Edward German ‘ Gipsy Suite . ’ Mr. Cecil 
 Gann , Canterbury , leader band , played 
 Beethoven Romance G violin orchestra . 
 Madame Zippora Monteith Mr. Wilfred Barclay 
 vocalists , conducted . Mr. H. J. 
 Taylor accompanist 

 Durpan ( S. Arrica).—The Durban Orchestral Society 
 gave fourth Concert season Town Hall , 
 October 5 , programme included movements 
 Tschaikowsky ‘ Casse Noisette ’ suite , Grieg ‘ Peer 
 Gynt ’ suite , ‘ Spinning Chorus ’ Flying 
 Dutchman , ‘ Yellow Jasmine ’ Cowen suite 
 Language Flowers , Overture ‘ Masaniello . ’ 
 vocalists Miss Mary Neithardt , Mr. E. Phillips , 
 Mr. J. Fellows , Berea Glee Union . Mr. Charles 
 Hoby conductor 

 WELLINGTON , N.Z.—On September 30 , St. Paul Pro 
 Cathedral , Mendelssohn ‘ Hear Prayer ’ 
 42nd Psalm sung . solos works 
 taken Madame Evelyn Carlton , choruses 
 Church choir , augmented occasion Glee 
 Madrigal Society , supported small efficient 
 orchestra fine organ . addition 
 works , Mackenzie Benedictus strings 
 organ tastefully played . Mr. Robert Parker presided 
 organ , Dr. W. Kington Fyffe , organist 
 St. Peter Church , conducted 

 WoopHousE EAaves.—Two interesting performances 
 took place October 31 , direction Mr. F. 
 Storer . afternoon programme , given 
 Parish Rooms , comprised Beethoven Overture ‘ Prome- 
 theus , ’ Elgar ‘ Chanson de Matin ’ ‘ Chanson de 
 Nuit , ’ Mozart Symphony G minor , Schubert 
 * Rosamunde ’ Overture ; Beethoven Romance G , 
 violin , Herr Kienle . evening special service 
 held church , Mr. Herbert Brewer 
 Gloucester cantata , ‘ Emmaus , ’ Mendelssohn ‘ Hymn 
 Praise , ’ performed . Choir , mainly 
 Leicester , sang efficiency , orchestra 
 heard excellent effect . solo vocalists 
 Miss E. Locke , Miss D. Pollock Mr. Harry Stubbs , 
 Mr. F. Storer conducted ability 

 ANSWERS CORRESPONDENTS 

 AvFrepD Hotuins _ 14d 

 1236 , Creation Hymn .BEETHOVEN Id. 
 » 1237 . Evening Star & C. MackeNziE 14d . 
 » 1238 . Hope heart oHN Warp 14d , 
 + ) 1239 . Praised Diana Cc . ) . STANFORD d 

 1240 . flow'ry meadows . PALESTRINA 7 

 SERIES ARRANGEMENTS 
 ( PEDAL OBBLIGATO ) 
 FREDERIC ARCHER . 
 VoLume , CLOTH , SHILLINGS ; 
 , 14 Numbers , , 2s 

 1 . Largo ( Piano Sonata E flat , Op . Beethoven . 
 Prelude Fugue , minor . »   M. Brosig . 
 Andantino , minor ; r E. Batiste . 
 Allegretto Affettuoso oe os R. Schumann 

 2 . Andante ( Piano , Sonata E minor ) . ‘ ‘ .. Weber . 
 Introduction Fugue ( Fantasia ) sie . Kalkbrenner . 
 Andantino , G M. Brosig 

 3 . Andante ( Sonata , Violin , Pines ) ; M. Hauptmann . 
 Fantasia , G .. E. Bunnett . 
 Lied ohne Worte ( . SE “ Book 6 ) . Mendelssohn . 
 Adagio Maestoso .. F. Archer . 
 Fugue , D minor J. Keeble 

 4 . Andante , F ( Op . 35 ) Beethoven . 
 March ( Oratorio ) C. Steggall 

 5 . Andante quasi Allegretto x oe C. E. Stephens . 
 Introduction Fugue , C minor .. .. E , F. Gaebler . 
 Andante Sostenuto , F. oe ..   E. Batiste 

 MUSICAL TIMES.—Decemper 1 , 1901 . 845 

 Associated Board Examinati 
 dies € ASSOCIATE Oar XaMiMNatilons , 1902 . 
 R. T 
 NOVELLO EDITIONS . 
 PIANOFORTE . | VIOLIN — ( continued ) . 
 Cloth , 3/- ) . LOCAL CENTRE — JUNIORS . bag LOCAL CENTRE — SENIORS . ~~ 
 MEDivw BacH.—Fugue E minor , . 10 Book I. ( 48 Preludes FLORILLO.—36 Etuden , Nos . 3 , 12 , 28 .. fe “ oS 
 NS Fugues , Edited W. T. Best ) ae 6 o| BeeTHoven.—Sonata major , Op . 12 , No.2 .. - 44 
 CrameR.—In C , . 43 ( F. Taylor Studies , Book XXV . ) .. 1 0 SCHOOL EXAMINATIONS — ELEMENTARY . 
 ALTO , BEETHOVEN.—Sonata E , Op . 14 , . 1 ( Edited A.Zimmer- WILHELMJ.—Modern Violin School . BooklIa .. - 8 
 mann , No.9 ) .. 2 o| WILHELMJ.—Modern Violin School . Book 20 
 BacH.—Invention ee Parts , " . 6 , E ( Edited Folk Dances Denmark . Nos . 4 7 ( Wilhelm ) ! s 
 J. Higgs ) 1 6 School ) . ; 26 
 MENDELSSOHN . ae ohne Worte , ‘ flat , Op . 38 , " . 6 SCHOOL EXAMINATIONS — LOWER DIVISION . 
 ( Lied . 18 ) . o g| WirHeLmMj.—Modern Violin School . Book ia 20 
 ScHUBERT — . Pe inc ‘ sharp minor pag “ Monients Musicals , ” Parry , C. H. H.—Twelve Short Pieces . ol ee co 2 G 
 Op . 94 ( Pianoforte Albums , . 49 ) 0 SCHOOL aa eee DIVISION . 
 CraAMER.—In D minor , . 24 ( F. Taylor " Studies , Book vil . ) 1 o| WiLtHELMJ.—Modern Violin School . Book Ills . 3 0 
 : CLemenTI.—In , . 9 Gradus ( F. Taylor Studies , Warner , H. WaLpo.—Elegie .. — . oes EG 
 ARTS , Book XV . ) Io 
 S. 27 40 BEETHOVEN . — Allegretto F minor , Sonata , Op . 10 , VIOLONCELLO .. 
 j . 2 ( Edited A. Zimmermann , . 6 ) 20 LOCAL CENTRE — SENIORS . 
 Hanpev.—Courante E minor , Suite IV . ( Pianoforte MENDELSSOHN.—Last Movement , Sonata B flat “ Saw ee 
 VE Albums , . 4 ) oe e Io SI N GI N G 
 LOCAL CENTRE — SENIORS . 7 7 
 LOCAL CENTRE — SOPRANO . 
 Ss Bacu.—Fugue F , . 11 Book I. ( 48 Preludes Fugues , O worse death ( 12 ncaa Songs 
 ‘ Edited W. T. Best ) ... 6 © ] Hanpev . | Angels bright| Handel Seman 20 
 MoscHELES.—In E flat minor , Op . 70 , . 8 ( F. Taylor fair . ee ( Edited A. Randegger ) ) 
 3 . 30 42 , Studies , Book XXXVI . ) © } Purcett . — Nymphs Shepherds ( Edited W. H. Cum- 
 BEETHOVEN.—Prestissimo minor ( Finale ) , ‘ Sonata , mings ) . Novello Purcell Album ; 6 
 Op . 10 , . 1 ( Edited A. Zimmermann , . 5 ) 2 0| Mozart.—Voi che sapete . Novello edition Songs 
 ICES . RHEINBERGER.—Fugue G minor , Op . 5 , . 3 ( Pianoforte Le Nozze di Figaro ( Edited A. Randegger ) « s ce 
 Albums , . 23 ) : Io MEZZO - SOPRANO . 
 Parts . HanpeL.—Gigue , Suite . ( Pianoforte Albums , arc eae , og A. gag = ea SESE 
 . . 4 ) « . Io o Heaven Almighty ( 12 oprano 
 « 31 50 . Handel ’ ) 
 Czerny.—In , Op . " 740 , . 26 ( F. Taylor Studies , Book XVI ) O| HANDEL . =| Songs , Handel aie 
 Mozart.—Adagio E flat , Sonata C minor vem ¥ o Literty , ti thou choicest Fig | 
 7 ae « oJ 
 A. Zimmermann , . 14A ) . 2 6 ScHUMANN.—Widmung flat ( Dedication ) ae ‘ ac enc ae 
 MENDELSSOHN.—Lied ohne Worte , inB flat minor , + Op . 30 . 2 CONTRALTO . 
 ( Lied . 8) .. s ° 91 Panseron . — 42 Exercises Contralto , , ( Edited A. 
 SCHOOL EXAMINATIONS — ELEMENTARY . Randegger ) es y zs os ae ° 
 Lemo1nE.—In C , Nos . 1 2 , Op . 37 @. Taylor Studies , ( ‘ shall eyes ) 12 Contralto om | 
 Book III . ) s 6 shall feed } Handel Oratorios 20 
 Cesany.—In ¢ , Op . 599 , . 18 . Taylor : Studies , Book I. ) 1 © ) BrauMs . Sapphic Ode , D ( Roses gathered Th , Book tls Low 
 SCHOOL EXAMINATIONS — LOWER DIVISION . Voice .. ; 
 LAS Czerny.—In D , Op . 636 , . 11 ( F. Taylor Studies , Book X. ) 1 0 TE . 
 BEETHOVEN.—Rondo ( Allegro ) G , Sonatina , Op . 49 , ( O God , ) 12 Tenor Songs 
 . 1 ( Edited A. Zimmermann , . 19 . ) .. 1 6| HanpEL.—+Sing Songs Handel Oratorios(Edited+ 2 0 
 Czerny.—In C , ” 299 . 8 ( F. — Studies , Book { Praise A. Randegger ) .. ) 
 . - XVIL ) Io BARITONE . 
 NG Mozarv . hase eater : anaes 2 , heen Sait E flat ( Edited PANSERON . — 42 Exercises Baritone Bass , I. ( Edited 
 A. Zimmermann , . 4 ) .. 6 A. Randegger ) . : o 
 rom 12 Bass Songs 
 SCHOOL EXAMINATIONS — “ HIGHER DIVISION . ( Behold , tell fien Handel Ore ) 
 HANDEL . — ; 20 
 JI 7 oy — . 13 , minor ( Edited sis ‘ ~ | trumpet shall sound ES ( Edited ‘ f rf 
 5 ry iggs ) .. 1 6 Randegger ) 
 Czerny.—In C , Op . 299 , . 5 ‘ ( F. Taylor ‘ Studies , Book IV . ) Io BAS 
 Mozarr . — Allegretto gor gg Sonata D demons 7 Concons.—4o Lessons Bass ( Edited pro sn 2 6 
 A. Zimmermann , . 19 ) .. 2s Hanpe . — { comforted | Handel Ora-| , 
 — ORGAN . ane | Lord worketh wonders ~ — ( Edited ! li 
 . Randegger ) 
 LOCAL CENTRE — JUNIORS . ScuuBERT.—The Wanderer , Schubert Songs , Vol . II ... 1 6 
 Best.—Thirty Progressive Studies , Op . 33 , Nos . 18 , 20 , 30 2 6 SCHOOL EXAMINATIONS — ELEMENTARY . 
 SMART.—Twelve short easy pieces , . rin E flat .. 2 6 SOPRANO . 
 oe MENDELSSOHN.—Allegretto F , Sonata IV . 2 6| Concone.—5o Lessons ( Edited A. Randegger ) .. « © @ 
 LOCAL CENTRE-—SENIORS . MEZZO- St a. * : 
 | ag ( Fg Pedal , Nos . 89 , 96 , 102 . ( Art Organ- | bomen Wiegenlied , Ba allay Goodnight , ‘ Book sales 
 aying , II . ) . 7 6 ] Lc - 3 
 canes Srneind B flat ( Pieces Organ , | Iit . , Low Voice : CONTRALTO . 
 o.1 ) .. 20 P 
 Bacu.—Fugue “ major ( Bridge Higgs ! Edition , PANSERON.—42 Exercises , I. ( Edited A. Randegger ) .. 2 0 
 Book III . ) 3 0 SCHOOL EXAMINATIONS — SOPRANO , ‘ 
 = n ConconE.—50 Lessons ( Edited A. Randegger ) “ « 2@ 
 ello ) SCHOOL EXAMINATIONS — LOWE R DIVISION . Saint - Sa#ns.—Thou , O Lord , Helper ( 19th Psalm ) . ee 
 TRA , STAINER.—Organ Primer , Nos . 35 , 56 , 59 ne ello Haypn.—My mother bids bind hair . ae a. oe 
 Primers , . 3 ) 2 0 MEZZO - SOPRANO . 
 MENDELSSOHN . — Adagio flat , Sonata 2 6| Concone.—50 Lessons ( Edited A. Randegger ) ae oe 3 6 
 Bach.—Acht kleine Priludien und Fugen , No.1 C major BENNETT , W. STERNDALE.—Dawn , gentle flower < e- . sa oune 
 ( Bridge Higgs ’ Edition , Book I. ) . : 2 6 CONTRALTO . 
 SCHOOL EXAMINATIONS — HIGHER DIVISION . 2 ain Contralto , I. ( Edited 
 .—The O N 8 . ae . A. Randegger : ee ee ee oo 2 0 
 rence . — o ta ple HanpDeEv . — Return , O God Hosts . 12 Contralto Songs 
 Rink.—Organ School , . 155 . 2 6 } f Handel Ovat Rdited Rand ) 2 
 Bacu.—Short Prelude Fugue ji E | minor ( Bridge : neh Taper > Cneneros CARES ph alae Sa 
 Higgs ’ Edition , Book II . ) . 3 BARITONE BASS . 
 2 “ | Concone.—4o Lessonsfor Bass Baritone(Ed.by A.Randegger ) 1 t 6 
 VIOLIN . | MENDELSSOHN.—For mouuvtains shall depart . ‘ 
 . | Bass Solo Music Elijah ( tdited A. Randegger ) .. 2 6 
 S : LOCAL CENTRE — JUNIORS . | MENDELSSOHN.—Consume ( St. Paul ) ° 
 Exvear , E.—Chanson de Nuit .. . 6 } Mozart.—Se vuol ballare . Novello edition Songs 
 HanveEv.—Sonata , . 5 , Second ‘ Mov ements ( Piano Mozart Le Nozze di Figaro ( Edited A. Randegger ) 20 
 Violin Albums , . 17 ) 3 6 ScHuMANN.—Row gently ( Edited Macfarren , . 26 ) .. 0 9 
 Lonpon : NOVELLO COMPANY , LimitTeEp 

 846 MUSICAL TIMES.—DeEcemser , Igot 

