CHOPIN.—Nocturne in e flat . op . 9 , no . 2 ... FAULKES 
 BACH.—Air and Sicilienne ie a 5 BEstT 
 ELGAR.—Dream Children . op . 43 , no . 1 ATKINS 

 WAGNER.—Albumblatt , in C WESTBROOK 
 KLEIN.—Offertoire , in e flat ... sa a 
 GUILMANT.—Third Nuptial March . op . 90 
 BELLERBY.—Cradle Song and Angel Choir 
 SCHUTT.—Romance . op . 38 , no . 2 ... 
 CHOPIN.—Prelude in d flat ... one 
 LUCAS.—Meditation . op . 27 , no . 1 
 BEETHOVEN . — L : z — from Violin 
 Concerto ... 
 CROOKES.—Berceuse _ ... 
 HAYDN.—Andante from Surprise ' Symph 1ony KING H. ALL 
 RICKMAN.—Revérie du Soir 

 POLLITT 
 BEstT 

 with obsolete convention , but substitute in their place little 
 that be endure 

 in the meanwhile , culture have prosper among the Elite 
 of the french music - lover . Habeneck have introduce 
 Beethoven ’s symphony , interest grow in classical work . 
 but despite the advent of earnestly - minded and gifted 
 like Gounod ( 1818 - 93 ) , Lalo ( 1823 - 92 ) , 
 Saint - Saéns ( b. 1835 ) , something would have miss to 
 far the progress of the musical and of the gener 
 public , but for the revelation of Wagner ’s work and of his 
 theory 

 indeed , the influence of Wagner , his part in the move- 
 ment that end in a modern Renaissance of music in 
 France , a Renaissance by virtue of which France have give 
 birth to one of the leading school of the world , be capital 
 he have greatly contribute to teach the French to take 
 musical art in earnest , to interest they in the high ideal 
 and in the philosophy of music . his influence , however , 
 become excessive , and would have do much harm but for 
 the violent reaction that ensue 

 by GERALD CUMBERLAND 

 just as we can see sign of the rise sun long 
 before it have appear above the horizon , so can 
 we clearly trace the coming of a new philosopher 
 or poet in the work of the man of a previous 
 generation . a great man , it would seem , begin 
 to influence the world before he be bear ; tiding 
 of he reach we mysteriously , and our need of 
 he seem to have the power of actually bring 
 he into being . great man be not bear 
 fortuitously ; they always arrive at the precise 
 moment when they be require . moreover , 
 their coming be prepare for ; in one place or 
 another some man and woman be always make 
 ready for the reception of their idea . Charles 
 Darwin do not blaze suddenly upon the world of 
 science : the comet , swim into existence from 
 outer space , have be dimly see many year 
 before its arrival . in the music of Haydn be the 
 ghostly feature of the come Beethoven , and upon 
 the opera of Gluck we can detect the faint impress 
 of Wagner ’s hand . even Nietzsche , the most 
 original of all recent philosopher , be not new ; 
 he have , so to speak , appear in the world many 
 time before , write in faltering tone with the 
 pen of Stendhal , aim blow at the world with 
 the puissant arm of Napoleon , and occasionally 
 flout Europe with the music of Berlioz . the 
 personality of Nietzsche be but a gathering 
 together , a focussing , of many scatter force 
 which , by some unknown principle of Nature , 
 unite themselves together in order to strike more 
 heavily upon the closed door of intellectual 
 prejudice 

 the truth be that all great man present in their 
 genius certain common human quality in a 
 concentrate form . the essence of many 
 thousand of rose - leave be distil into a pint 
 of attar , and into a man of genius have be 
 press the unconscious thought and aspiration 
 of multitude of his fellow - man . he merely 
 voice what they feel ; when they hear he 
 speak aloud their own thought , they hail he 
 as a prophet and seer . but he be not different 
 from they except in his great power and courage . 
 when Nietzsche begin to be extensively read , a 
 large section of Europe become conscious of 
 something in themselves which they have never 
 see before ; that be to say , the philosopher do not 
 reveal anything new , but simply express what 
 be and always have be embed in human 
 nature . in a word , the nietzschean spirit be not 
 new — it be old ; but for many year it have either 
 be lie dormant , or it never fully reveal 
 itself until Nietzsche himself begin to write 

 flute 

 in the most audacious chord combination . his idea 
 be , evidently , that Beethoven and Wagner have have 
 their day , and that the song of humanity must contain 
 the noise of tramway , taxi , and crowd . come 
 after the threat which be make to invade music with 
 fraction of tone and a complete alteration of the 
 value of the degree of the scale , this perhaps do 
 not affect we very much . but it show that the spirit 
 of unrest be abroad 

 a survey , however cursory , of contemporary activity 
 can not afford to omit a reference to Schonberg . 
 about he opinion be widely divide . he be well 
 know by the ' Drei Klavierstiicke , ' ' Pierrot Lunaire ' 
 Gurrelieder . ' Schénberg seem to have a liking 
 for peculiar group of instrument . * Pierrot Luni sire ? 
 be write for pianoforte , violin ( and viola ) , violoncello , 
 and piccolo ) , clarinet and bass - clarinet ) , and 
 speak - voice . in the String quartet ( Op . 10 ) we 
 have the addition of a soprano voice in the last two 
 movement , and in the Chamber Symphony some 
 fifteen solo instrument . whatever the composition , 
 Schénberg be certainly create a stir . ina concert 
 give in Vi jenna , and devote , for the most part , to the 
 work of Schénberg ’s pupil , the item be greet 
 with laughter , hiss , and applause . controversy over 
 the ' Gurrelieder ' be so great that the second 
 performance be indefinitely " postpone , owe , it be 
 say , to a hint from the authority . in the press , as 
 in the concert- room , the advanced music be provide 
 excitement . critic cross sword over this ultramodern 

 THE MUSICAL TIMES.—SepTemMBer 1 , 1913 

 lo say that this music be misunderstand be not to | entirely homogeneous , the inflexion be merely a variation 
 say that it be bad . criticism be base on knowledge , of pitch , and only approximately represent by bar 
 and if we do not know a man ’s process and speech we | ™ usic . _ : ls : . 
 can not pass judgment upon his work . some think that | this be a return to first principle , and involve what be at 
 the late tendency in music show a _ delicate the present time an unusual treatment of the anglican chant , 
 relationship to those in painting , and that the most age ieee know , ¥ not a ! rye adopt . t 
 revolutionary writer be the musical equivalent of — on SNES GES Ne CRRNENES SD CURE Gt Oem : 
 Matisse and Picasso and of the Cubist painter equal Cutten , cape P herpes even the anaes oe ame Tae . 
 " a ane ' I ; necessarily equal in value ; all that be leave and all that be require 
 generally . whether this apply to work like |i ; ( 1 ) the variation of pitch to avoid monotony , and ( 2 ) the 
 Scriabine ’s ' Extase ' and ' Prometheus , ' or to his| accent which give the rhythmical feeling . | moreover , the 
 pianoforte piece like ' Enigme ' and ' Désir ' I can not | accent need not be equal in strength , and the intensity of 
 say . other hold that present inclination , if persist | the accented note will depend on the word or syllable wit 
 in , will lead we to harmonic nihilism . we in England | which it be associate , while in some case it will scarcely be 
 shall require to become much more familiar with this | apparent . ; 
 music before we can say anything very definite about| | the basis of all pointing should be good and therefore 
 t. but it can not be too strongly state that the deliberate reading , consequently each verse have be s 
 right kind of modernism be base on the past . any mark that the inflexion of the chant , when rendere 
 modern movement which cut we off from Bach , freely , will not interfere with the phrasing of the sentence 
 Beethoven , and Wagner , which regard these man as | the result of treat the whole — — ( inclade the 

 aie a pts , inflexion ) in this manner be that very few pointing 
 of no further value for the musical world , be doom . indication be necessary : such mark as be use & 
 there may be a future for this cult or for that , but for ! ¢ommon to all printing , bear their ordinary significance 
 a futurism which break absolutely with the great / and in no way obscure the text . the reduction in th 
 past I can see no abide prosperity . all healthy | number of the mark be a great gain , because it lead toa 
 spirit must rejoice at the continual manifestation of| well grasp of the meaning of the verse ; for it must be 
 new idea . experience would almost seem to teach| remember that this lsalter be mark not with the ide 
 that in the good musician there be something of the } of compel the singer to chant correctly , which would | 
 natural revolutionary . do not Chopin celebrate the | 2 " impossible feat , but to enable he to sing the inflexio 
 fall of Warsaw in an etude ? do not Wagner find with that easy rendering which approximate most closely 
 his name prominent in the Dresden revolt ? do not good reading . 
 Schumann sing the cause of liberty in male - voice| the author claim for their psalter that : 
 chorus ? and it may be that the young voice it be base upon the principle of make the sens 
 wish to pipe another strain , to take the ' new / of the word the first consideration , and this 
 path ' of which Schumann write . doubtless some } vital principle be not only enunciate in the preface 
 of they feel the need of what Nietzsche describe as but carry out in the book 

 609 

 rformance , all the great novelty of the year , be due to 
 the illuminate enthusiasm of an intelligent , well - inform 
 private individual ; that while he be introduce new 
 singer who fulfil with singular ability the many demand 
 ofmodern music , and new opera illustrate ali that be vital | 
 in modern music , Covent Garden be content with a 
 star — Caruso — and with a réchauffé of old opera season 
 by two extraordinary novelty . 
 " of the old opera some we would like to hear every year . 
 Wagner ’s ' Ring ' be one of those supreme achievement for 
 which the musician and the public at large profess equal | 
 affection . like Beethoven ’s ninth Symphony , it ought to 
 be give once at least in the year in every city with a claim | 
 to musical taste . but it be not enough to produce it . it | 
 must be do well . if the work be to retain its hold on the | 
 public the performance ought to improve in quality every 
 year . this year , instead , apart from the admirable 
 conducting of Herr Nikisch , the stage arrangement be | 
 wholly unsatisfactory . of the singer engage a few w ere 

 very good , most of they be tolerable , some be bad . 
 the only novelty in the production , which have be 
 trumpet abroad as if it have be a great innovation , be 
 the apparatus for the Rhine daughter — a new toy for good | 
 child . the rest of the stage machinery be , as it have 
 always be , deliciously naive . Wotan ’s daughter be | 
 still see ride their hobby - horse , and a picture of local 
 worthy in congress , with or without calumet , be still 
 meant to represent the god await their final doom . 
 stage machinery , however , be only a secondary consideration . 
 the all - important matter be the music , and in this respect the 
 performance of the ' Ring ' show a falling off from the 
 standard of previous year . to take one instance , the singer 
 who take Siegmund and Siegfried be , neither as a singer nor 
 asan actor , of the class one have a right to expect at Covent | 
 Garden . the part of Siegfried , we be aware , be one of the | 
 most difficult in existence , and few tenor can stand the test . 
 but when the charge for a stall be what it be at Covent Garden 
 we can surely expect that the leading part be take by 
 those who can give a truly representative performance . this 
 may seem unfair to an artist who possess unquestionably 
 good quality ; but the point be that these quality be not 
 of high enough rank for London ’s good and only official 
 ra 

 in our last issue we give an analysis ( which have 
 be widely reproduce ) of the chief feature of the 
 announce for the season . so far all the 

 programme 
 promise of the prospectus have be fulfil . the opening 
 concert bring forward Strauss ’s ' Till Eulenspiegel , ' a 
 Wagner concert on the 18th excite the usual interest , on 
 the 19th Humperdinck ’s ' Hansel und Gretel ' Overture , 
 Tchaikovsky ’s Francesa da Rimini ’ Fantasie , and Liszt ’s 
 Preludes be the attraction . a novelty be Percy 
 Grainger ’s characteristic treatment of the ' County Derry ' 
 Air and ' Shepherd ’s Hey . ' on the 2oth , Bach ’s second 
 * Branderburg ’ Concerto , Debussy ’s ' L’Aprés - midi , ' Berlioz ’s 
 ' Queen Mab ' Scherzo , and Brahms ’s C minor Symphony be 
 the chiefitems . Elgar 's * Cockaigne ' Overture , and Strauss ’s 
 * Don Quixote ' ( the violoncello solo part to which be 
 well play by Mr. C. Warwick - Evans ) be attraction 
 on the 21st , but there be even more general interest 
 show in the appearance of the famous Dr. Ethel Smyth to 
 conduct a performance of her fine dramatic overture to ' the 
 Wreckers . ' the rare spectacle of a lady , and a particularly 
 energetic specimen of her sex , act in this capacity evidently 
 absorb attention , and the ability display in the music draw 
 enthusiastic approbation . the 22nd be largely a Beethoven 
 night , Mr. Arthur Catterall play the Violin concerto with 
 great skill . the vocal soloist that so far have appear 
 include Miss Carrie Tubb , Miss Aimée Kemball , Miss Mary 

 Bates , Mr. Haigh Jackson , Mr. Lorne Wallet , Mr. George 
 Parker , and Mr. Ivor Foster . 
 and Mr. Sydney rosenbloom(pianoforte ) have also perform 

 it be gratifying to be able to state that chamber music w 
 once more be under the control of the Birmingham Chamber 
 Concerts Society , the executive be the Catterall Str 
 ( Quartet , hitherto . the most prominent feature @ 
 } connection with the four Harrison concert will be tk 
 reappearance of Madame Tetrazzini at the first concert , ani 
 Herr Arthur Nikisch , with the London Symphony Orchestra , 
 at the last concert . the four Max Mossel drawing - roo 
 | concert provide an excellent array of eminent artist , amo 
 |whom be Miss Irene Scharrer , Miss Adela Vem 
 | Miss Daisy Kennedy , Madame Julia Culp , Madam 
 | Lula Meisz - Gmeiner , Miss Dorothy Silk , Miss Jen 

 Waterston , Herr Egon Petrie , Herr Louis Schnitzle . 
 and the Vienna Rose String Quartet . 
 | choral music will be an important factor , all the loa 
 | Choral Associations again be well represent . os 
 | premier choral body , the Birmingham Festival Choo 
 | Society , conduct by Dr. Sinclair , have issue the follow 
 |attractive scheme in addition to the customary Yuleti 
 | performance of the ' Messiah . ' the work to be give # 
 | * Gerontius , ' also Elgar ’s ' we be the Music Maker , 
 Sullivan ’s ' golden Legend , ' Beethoven 's great ' MassinD 
 | Goring Thomas 's ' the swan and the skylark , ' and Bact 
 * St. Matthew ’ Passion 

 the Birmingham Choral and Orchestral 
 ( conductor , Mr. Joseph H. Adams ) propose to give Berlia 
 dramatic legend ' the damnation of Faust , Colend 
 Taylor ’s beautiful cantata ' a tale of Old Japan , & 
 ' Elijah , ' and a concert recital of Wallace ’s opera * Lily 
 Killarney 

 the University Musical Society ’s programme for next 

 an orchestral concert will be 
 give at the end of the Michaelmas term . this have not 
 be part of the Society ’s programme in past year . at this 
 concert the Society ’s orchestra will play Bach ’s fourth 
 Brandenburg Concerto for solo violin , two flute , and 
 string ; Beethoven ’s third Symphony , and Brahms ’s Violin 
 concerto . on February 12 the choir and orchestra will 
 perform Bach ’s Mass in B minor , and on June 12 Berlioz ’s 
 ' Faust . ' with such a programme before we , the untiring 
 energy and enthusiasm of our conductor must be meet by a 
 like spirit among the member of the choir and orchestra , 
 and , moreover , by a like capacity for work . the chamber 
 concert need more support . on October 29 , a concert will 
 be give by the Rosé Quartet . on November 12 , Egon Petri 
 will give a pianoforte recital , and on December 2 Elena 
 Gerhardt will give a vocal recital . in the Lent term we 
 shall hear the London String Quartet on January 21 . on 
 February 4 , a concert of chamber - music will be give . 
 music for flute , clarinet , string , and harp ( which will be 

 year be an ambitious one 

 on August 11 a miscellaneous concert at Chudleigh be 
 give by Mr. C. W. McAlister ( pianist ) , support by an 
 excellent party 

 at the Torquay Pavilion the Municipal Orchestra have be 
 augment to forty player , and the enlarge resource have 
 produce such excellent artistic result that it be hope 
 the enlargement will be permanent . on August 13 Miss 
 Gertrude Lonsdale bring an exceptionally efficient party of 
 artist , who give an interesting ballad concert . the special 
 feature be the introduction of several violin piece and song 
 compose by Mr. Haydn Wood , who himself play . his 
 music be essentially sincere , and the subject - matter be 
 original . as an interpretative artist as well as a vocalist 
 Miss Lonsdale reach a high standard . the other 
 member of the party be Miss Jennie Taggart , Mr. 
 Geoffrey Seabrook , and Miss Marjorie Wigley ( pianist ) . 
 on the follow day the Orchestra perform a Grieg 
 programme , and on August 16 they celebrate their first 
 anniversary with a Symphony ( Beethoven , C minor ) concert 
 in the afternoon , and a specially - select popular programme 
 in the evening . the speciality - composer programme be 
 continue during the following week , the earnestness of 
 purpose of Mr. Basil Hindenberg and his man show no 
 abatement 

 Axe Vale Musical Society give a stage performance on 
 August 14 and 15 of ' the Yeomen of the Guard , ' Mr. W. C. 
 Walton conduct . the chorus and orchestra number 
 over sixty performer , and work very creditably 

 among the new work to be perform by the Deutsches 
 Opernhaus during the come season be Otto Fiebacs 
 ' Die Herzogin von Marlborough ' and ' Die Blinde va 
 Pompeji ' by Marziano Perosi.——Bruckner ’s fifth Symphony 
 in B flat ' major , Draeseke ’s ' Symphonia Tragica , ¢ 
 ' Sinfonietta ' by Erich Wolfgang Korngold , Heinnet 
 Zoellner ’s second Symphony , variation on the choral 

 Dvorak ’s ' Serenade , ' the ' Sinfonia Domestica ' and the nev 
 ' Festliches Praeludium ' by Richard Strauss , and Elgar ’ 
 overture ' Cockaigne ' be among the work to be perform 
 at the come series of Philharmonic concert ( conductor , 
 Arthur Nikisch).——Along with interesting manuscrip 
 lately sell by Herr Leo Lipmannsohn at an auction hold 
 in Berlin be the seven Menuets by Mozart , composé 
 when he be thirteen year of age ( £ 158 ) , Weber 's first 
 pianoforte sonata in C major with the famous ' Perpetuum 
 mobile ' ( £ 152 ) , the second bassoon part of Beethoven ’ 
 overture , Op . 124 , and a number of letter by Gluck , 
 Haydn , Schumann , and Richard Wagner 

 BRESLAU 

 se 
 Faure s 

 Salome . ' 
 BU DA - PEST . 
 Beethoven ’s ballet ' Prometheus ' be recently give for 
 the first time at the Royal Opera . the original plot , which 
 be lose , be on this occasion replace with one write by 
 the poet M. Alexander Brédy . on the same occasion 
 Gluck ’s ' Maienkénigin ' be perform for the first time . 
 a little later Alexander Szegho ’s one - act opera ' Bathory 
 Ersébet , ' Debussy ’s ' l’Enfant prodigue , ' and Mozart ’s 
 Ballet ' Les petits riens > be stage for the first time 

 CREFELD . 
 under the direction of Prof. Miiller - Reuter performance 
 be give of Bruckner ’s seventh Symphony , Mabler ’s fourth 
 Symphony , and the * Passionsoratorium ' by Felix Woyrsch . 
 — the Liedertafel ( conductor , Herr Willy Geyr ) perform 
 Heinrich Hofmann ’s ' Waldfraiilein ' for solo voice , chorus , 
 and orchestra . 
 CRACOW , 
 Leoncavallo ’s opera ' Zaza ' and Massenet ’s ' Le jongleur 
 de Notre Dame ' be recently hear here for the first time 

 MUNICH 

 under the direction of Herr Bruno Walter , Hans Pfitzner ’s 
 interesting opera ' Der arme Heinrich ' be recently give 
 at the Royal Opera.——The annual festival concert of the 
 Konzertverein , give under the direction of Herr Ferdinand 
 Loewe , be at present proceed ( from August 15 to 
 September 15 ) . the programme include Beethoven ’s nine 
 Symphonies , symphony in C major and e flat major by 
 Mozart , Schumann ’s fourth symphony in D _ minor , 
 symphony by Brahms ( c minor and d major ) and his 
 Haydn Variations , Tchaikovsky ’s symphonie pathétique , 
 Liszt ’s symphonic - poem ' Tasso , ' symphony ( no . 5 , 7 , 
 and 8) by Bruckner , Max Reger ’s ' romantische ' suite , and 
 Richard Strauss ’s ' Don Juan , ' ' Tod und Verklarung , ' ' Till 
 Eulenspiegel , ' and his ' Symphonia domestica 

 ostend 

 os we os .. A.C. MACKENZIE : 
 { ' ' LoOHENGRIN " ) ° . WAGNER 1 ° 
 no . 4 , in F 

 BEETHOVEN : 
 C. MACKENZIE 

 MENDELSSOHN 

