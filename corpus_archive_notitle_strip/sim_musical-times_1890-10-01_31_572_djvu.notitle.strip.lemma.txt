vocalist , Madame Valleria . V ioloncello , Herr Julius Klengel . the | WEDNESDAY , THURSDAY , FRIDAY , and SATURDAY , 
 full Crystal Palace Orchestra . conductor , Mr. August Manns . Stal ! October 22 , 23 , 24 , 25 , 1890 . 
 ticket ( transferable ) for the twenty Concerts , two Guineas each 

 pei he 7 Wepnespay Morninc — REDEMPTION . Gounod . 
 CHELTENHAM MUSICAL FESTIVAL , | Wepsespay Evexisc — GRAND MISCE LLANEOUS CONCERT . 
 1890 Tuurspay Morninc — ELIJAH ae Mendelssohn . 
 2 | Fripay Morninc — JUDITH C. Hubert H. Parry . 
 president : Sir Hersert Oaketey , Mus . Doc | Zespay Evewno — Tle sy DEN LEGEND . + » Sullivan . 
 musical Director : Mr. J. A. MATTHEWS . | SaturDay Morninc — MESSIAH ase +2 Handel . 
 . owe - . principal performer . 
 TUESDAY EVENING , OcTOBER 28 . | 
 | Mac 3 
 the creation ( part 1 ) , HAYDN . | se naeicetes . 
 CONCERTO in D ( for Viottn and OrcuEstRA ) , BEETHOVEN , Miss HILDA WILSON . Madame HOPE - GLENN . 
 STABAT MATER , DVORAK . | Mr. EDWARD LLOYD . 
 - | Mr. IVER — , Mr. ANDREW BLACK 

 VeEpDNESDAY evening , OCTOBER 29 . . WATKIN MILLS 
 the RE PENTANCE of NINEVEH , Professor J. F. BRIDGE Mr. B. PIERPOINT : Mr. MONTAGUE WORLOCK . 
 ( conduct by the composer 

 exist of note it , must eventually become utterly 

 lost . we need not go so far as the time of old 
 Egypt to illustrate this vanishment . how little do 
 we now know of the effect produce by the instru- 
 mental music use ( say ) in the elizabethan Masques ; 
 or how can we judge what " the Messiah " 
 sound like with the multitude of oboe and 
 bassoon Handel employ , as compare with the 
 meagre provision of string ; or to come to later 
 time , Ries and Czerny describe Beethoven ’s extem- 
 poraneous music as fine and more wonderful than 
 anything he commit to paper — alas ! it be lose 
 for ever . what remembrance have we of Handel ’s , 
 and of Mendelssohn ’s marvellous improvisation ? 
 unless music be note — prick , as our forefather 
 term it — it soon become forgotten and lost 

 586 the MUSICAL TIMES.—Ocroser 1 , 18go 

 ES.—OcroBer 1 , 18go . 591 

 the BEETHOVEN - HOUSE society in 
 BONN 

 apart from the interest attach to it as the 
 birthplace of Beethoven , Bonn be memorable for the 
 three Beethoven Festivals which have be hold 
 there in honour of the great master . the first , con- 
 ducte by Liszt and Spohr , take place in 1845 , on 
 the occasion of the unveiling of the monument 
 erect there to Beethoven ’s memory . ' the second , 
 conduct by Ferdinand von Hiller , tollowe in 1871 , 
 in commemoration of the centenary of Beethoven 's 
 birth . the third , the primary object of which be 
 to augment the fund which have be raise for the 
 purchase of the house in which Beethoven be bear , 
 and for the establishment of a Beethoven Museum , 
 be hold in May last . of these three Festivals the 
 first two be orchestral and choral ; the last be 
 devote exclusively to the master ’s chamber music , 
 both instrumental and vocal . of the first two it be 
 unnecessary at this date to speak in detail , except to 
 point out that by a strange chance a retrospective 

 account of that of 1845 appear in the MusIcat 

 with so well devise and representative a pro 

 gramme , and with such an array of executive talent 
 for its presentation , the success of the Festival , 
 from an artistic point of view , be assure . it be 
 satisfactory to add that financially it be equally 
 successful , the sum of 16,000 mark ( f{800 ) having 
 be net for the fund . unique in its scope , as 
 illustrate by no means the least important side of 
 3eethoven ’s artistic achievement , and as one which 
 seldom figure on Festival occasion , it must have 
 be a cause of unusual satisfaction to many in 
 Germany , where Beethoven ’s chamber music be less 
 generally familiar than , thank to Mr. S. Arthur 
 Chappell ’s ' popular " Concerts , it be in England . 
 from these preliminary remark we may now turn 
 to the main subject of this notice — the Beethoven- 
 House Society , or to give it its german appellation , 
 the " Verein Beethoven - Haus . " it be not till the 

 news of Beethoven ’s death , which take place at 
 Vienna on March 26 , 1827 , reach Bonn that the 

 worthy burgher of that city fully realise the loss 
 |they have sustain , and the honour which accrue 
 |to their city as the birthplace of so great a master . 
 | for the first time they now feel thoroughly proud of 
 [ their former townsman , who , as a young man of 
 | twenty - two , and the descendant of an obscure and im- 
 | poverished family , have some five - and - thirty year 
 | before migrate to Vienna , and have since make himself 
 | famous throughout the musical world as the great 
 jlive composer . foresee the lustre which the 
 | memory of so great aman would shed upon their city , 
 | they soon begin to investigate his antecedent and 

 search the record for the house in which he be 
 |born . as in the day of ancient Greece , where seven 
 | city contest the honour of be the birthplace of 
 |Homer , so now the inhabitant of four separate 
 | street in Bonn , in which at one time or another the 
 | Beethoven family have reside , assert their claim 
 |to Beethoven having be bear in their midst . for 

 a long time a house in the Rheingasse , in which his 
 | mother die , be believe to have be his birthplace , 
 |andatablet to this effect be affixedtoit . subsequently 
 a claim be put in in favour of a house in the Bonn- 
 | gasse , which the Beethoven family have previously in- 
 |habite . in 1838 , or thereabout , the question be 
 | fiercely debate as to which of these two house be 
 |the master ’s veritable birthplace . at length Mr. 
 A. W. Thayer , Beethoven ’s enthusiastic and most 
 |indefatigable biographer , come to the rescue , and 
 lafter diligently search the municipal and eccle 

 siastical record , conclusively prove in favour of the 
 | house in the Bonngasse . a tablet record the fact 
 | be accordingly affix to it on the occasion of the 
 | Centenary Festival of 1571 

 it be not , however , until the spring of 1859 that , 
 | under the honorary presidency of Dr. Joseph Joachim , 
 a society be establish in Bonn for the purchase of 
 |the house , and for its preservation , as a last 

 memorial of the great master who first see the light 
 init . during the Festival of May last , when an ex- 
 { tensive loan collection of Beethoven relic be 
 bring together from all part , it be throw open 
 to view , but only to a limited number of visitor at 
 ithe same time . since that date immense progress 
 | have be make in the work of restore it , as far as 
 | possible , to the exact condition it bear at the period 
 | of Beethoven 's childhood . 
 the progress thus make be say to be chiefly due to 
 |the laudable exertion and enthusiasm of Herr 
 | Wilhelm Kuppe , a musician long resident in Bonn , 
 | who , by the energy and perseverance which he have 
 | display in the exercise of his art and profession , 
 |has acquire a competence , which enable he to 
 | devote the great part of his time to superintend 
 ithe work of restoration now in progress , and have 
 admit of his undertaking the honorary curator- 
 | ship of the house and its content . 
 | < a recent visit to it , or rather to that portion of it 
 | which the Beethoven family occupy , and which be 
 open to the public in August last , be one of 
 extreme interest . it might , perhaps , well be describe 
 as a double house , or as a tolerably commodious 
 | house , face the street , with a small one back to 
 back with it in the rear and look into a small 
 garden . at the time of the Beethoven family ’s 
 residence there , the front ground floor and the first 
 floor be occupy by one Clasen , a dealer in lace , 
 and the owner of the house . Salomon , famous for 
 his connection with Haydn , whose symphony he 
 be the first to bring to England , with his family , in- 
 habit the second floor . the Beethoven family 
 dwell in the small tenement at the back 

 the four small room which it contain be fully 
 large enough to hold the small collection of Beethoven 
 relic which the Society have as yet acquire , and 

 92 the MUSICAL TIMES.—Ocvoser 1 , 1&go 

 which at present can only be regard as form 

 the nucleus of the large collection which , as 
 opportunity arise and as fund permit , it hope to 
 acquire , and for the accommodation of which the 
 large room in the front part of the house 
 will amply serve . but as Beethoven relic be 
 both costly and difficult to meet with , a long 
 time must probably elapse before the promoter 
 of the scheme can hope to see themselves in 
 the possession of so complete a Beethoven Museum 
 as they desire . the collection , though at present a 
 small one , contain much that , in the way of 
 original manuscript , bust , portrait , & c. , can not 
 fail to be of interest to the student of Beethoven ; 
 and much that be calculate to rouse the visitor ’s 
 sympathy and commiseration for the mean and 
 sordid surrounding of the great composer 's early 
 youth . many will surely shed tear on behold the 
 wretched garret — a small lean - to attic in the roof — in 
 which Beethoven be say tohave be bear . scarcely 
 less affect be the sight of the ear - trumpet , manu- 
 facture by Maelzel in 1812 - 14 , which on account of 
 his deafness Beethoven use up to the time of his 
 death . they be four in number , and of various size 
 and shape . what add to the horror , at least of one 
 of they , be the metal band with which it be furnish , 
 for the purpose of attach it to his head while 
 play the pianoforte or conduct . as a special 
 point of interest allusion may be make to the grand 
 pianoforte specially manufacture for the master by 
 Graf of Vienna . its peculiarity consist in the fact that 
 almost throughout it have four string to each note . 
 in addition to the ordinary so- call loud and soft 
 pedal , it have a pianissimo , or echo , pedal , act as a | 
 damper upon the string , and be say to have be for- 
 merly furnish with resonator , but which be long 
 agoremoved . noless interesting isthe quartet of string 
 instrument upon which the ce lebrate Schuppanzigh 
 quartet party be in the habit of play . it con- 
 sist of a violin by Nicolaus Amati , 1690 ; a vioiin by 
 Jos . Guarnerius , fil . Andreas , Cremona , 1718 ; a viola 
 by Vicenzo Ruger detto il Per , Cremona , 1690 ; and a 
 violoncello by Andreas Guarnerius , Cremona , 1675 , 
 together with four bow , & c. ' the two violin bear 
 Beethoven ’s seal on the back just below the neck ; 
 and a big b have be scratch on the back , presum- 
 ably with the point of a pen - knife . it would be easy 
 to extend the list of curiosity , but we think we have 

 say enough to show that the house and its content 

 pronounce its opinion . 
 no other profession so full of impostor , charlatan 

 this be not quite correct , at 
 at the Richter Concerts 
 Beethoven be often graciously admit to the com 

 and dilettante , and no other class of charlatan that | panionship of Wagner 

 edify to the ungodly of Yeovil ! jarrange , and that the singer now take a large 
 — — | proportion of the property 

 Le Ménestrel announce the immediate issue by | 
 Breitkopf and Hartel of two unpublished work by | an example of respect for a composer 's inten- 
 Beethoven — an arrangement for pianoforte only of | tion : — at Vienna , lately , ' * La Juive " be perform 
 the Pianoforte Concerto in e flat , and , in full score , | with the serenade , all the duet , and several other 
 the first movement of a Pianoforte Concerto in d,/number omit , while the ballet be repre- 
 presumably that which Beethoven be know to have | sente by a pas de deux dance to music not by 
 begun subsequent to the completion of the ! Halévy ! 
 " Emperor 

 a BoLp journalist of Buenos Ayres , write of an 
 Wuen Scharwenka and " a party of gentleman " | opera company there , declare the feminine artist 
 enter the Concert - room at Brighton Beach , " inci- | t9 be an assemblage of Junos and Venuses , but that , 
 dentally make considerable noise , " Anton Seidl , | unhappily , the Lucrezias be rare than the Mes- 
 the conductor , look annoyed , but , recognise the | salina . it be to be hope that the writer " clear 
 pianist , forthwith play his arrangement of Chopin ’s | out * with speed when he see the werd in print . 
 " Funeral March ! ' ? an american contemporary | 
 7 Ic 6 " 2gaee incide " 
 ail a aeiiialals M. Maxime Lecomte have draw up a bill for lay- 
 ae . |e a tax of ten franc upon each organ , pianoforte , 
 horror upon horror ’ head accumulate ! acertain } 44 harmonium . he claim that revenue should be 
 man name Kuhmeyer , of Presburg , have invent a | obtain , as far as possible , from article of luxury . 
 machine play like a pianoforte , and produce | 4 cynical commentator add " and especially troim 
 sound from six violin , two viola , and two violon- | instrument of torture . " i 
 cello , conceal in the body of the instrument . this | 
 — ORTH TE : SECON by SE RTE 6 g ce ci ) xs have give the nucleus of a valu- 
 — able museum to Dieppe , in the shape of rich Louis 
 Ir be difficult to mistake the " true inwardness " of Quinze furniture , jewellery , paisngt , é . large library , 
 the follow advertisement , which recently appear | 2 collection of autograph , the MS . of ' the Soldiers 
 ae 2 : : y apr Chorus in " Faust , " and a MS . march by Mozart . 
 in France : ' " * a grand opera in manuscript , un- Th Pre Rite gad i ie pe ? 
 publish and complete , with full orchestration , the COMICON 5 VEER BE £ 4,000 . 
 work of a composer recently dead , be for sale secretly . 
 strictly confidential . address . A. M. , Poste Restante , 
 Marseilles . " here be a chance 

 598 the MUSICAL TIMES.—Ocroser 1 , 1890 

 torious execution of Beethoven ’s ' " ah , pertido ! " ' and Mr. 
 Edward Elgar for the personal introduction of a romantic 
 Concert - Overture , entitle ' Froissart . " Mr. Elgar , 
 formerly of Worcester , be now , we believe , a resident in 
 London , where , it may be hope , and , give opportunity , 
 even expect that he will make his mark . the overture 
 be of course chivalric in style , and , perhaps , more commend- 
 able for what it try to say than for the manner of its 
 expression . there be upon it , what surprise no one — the 
 mark of youth and inexperience ; but it show that , with 
 further thought and study , Mr. Elgar will do good work . 
 he must acquire great coherence of idea , and concise- 
 ness of utterance — those inevitable sign of a master , only 
 to be attain by extended and arduous effort . for such 
 effort , no doubt , Mr. Elgar may be trust , ' ' Froissart " ' 
 be much applaud — the Prophet have honour even in his 
 own country . in the second part of the programme be 
 the Introduction to the third act of " * Lohengrin " ; Grieg ’s 
 Suite ' " Peer Gynt , ' and a new eight - part chorus , ' lo 
 morning , " compose by Mr. C. Harford Lloyd . this be a 
 very interesting and effective example of pure vocal writing , 
 in a style which combine to some extent the science of the 
 ancient madrigal with the grace and charm of the modern 
 part - song . Mr. Lloyd 's counterpoint be facile , each 
 " voice " move freely even amid the difficulty of eight 
 part , while the general etfect of the music be in the high 
 degree pleasing . pende the composer ’s next important 
 production , amateur be glad to have such an example of his 
 taste and skill as ' to morning . " the piece be capitally 
 sing by the Leeds people 

 Thursday morning bring the novelty of the 
 Festival — Professor Bridge ’s Oratorio ' ' the Repentance 
 of Nineveh ’' — to hear which come many musician from 
 stress of work or from enjoyment ofrest . the curiosity be 
 natural . the composer have do nothing on the same 
 scale before , but have accomplish enough to excite 
 an expectation of success . how far that expectation be 
 met be a question to which different people might give 
 answer not exactly the same , but there can be no doubt 
 that ' " * Nineveh " have advance its composer ’s position by 

 of large standard composition the programme 
 Mozart ’s * requiem 

 upon it . 
 contain the ' * golden legend , " 
 and two part of the " Creation . " these , with the new 
 Cantata , occupy the two morning . ' the evening Concerts 
 be both of a miscellaneous character , have among their 
 more important feature Mendelssohn ’s Violin Concerto , 
 Beethoven ’s Symphony in A , Parry ’s " bl Pair of 
 siren , " Stanford ’s " Revenge , ' Mendelssohn 's ' Judge 
 me , o God , " and Sullivan ’s overture , * \ [ n memoriam . " 
 ( hese be certainly good enough for any Festival . 
 Mesdames Nordica , Damian , and Macintyre : 
 Lloyd , Mills , and Foli be engage as solcist ; there will 

 the 

 Tue local musical season bid fair to be one of singular 
 activity . it look , indeed , as if all previous record be to 
 be break , and , as be too often the case , concert - giver will 
 be find in serious collision with each other in the matter 
 of date . for example , Sarasate , Sir Charles Hallé ’s 
 orchestra ( two performance ) , and the Carl Rosa Opera 
 Company appeal in one and the same week — just a large 
 enough order all at once for most Glasgow amateur 

 a forecast of the Choral and Orchestral Concert scheme 
 be give in last month ’s Musica . time , and to the list 
 of choral work Verdi ’s ' ' requiem " have now to be add . 
 it will be give the same evening as Dr. Hubert Parry ’s 
 ' Ode to St. Cecilia ’s Day . " " the orchestral programme , 
 carefully draw up under the guidance of Mr. August 
 Manns , will be find to include Dvordk ’s new Symphony , 
 no . 4 , in G ; Spohr ’s Violin Concerto , no.g ; Mr. Fred . 
 Cliffe ’s tone - picture , ' " ' cloud and sunshine ' " ' ; Mr. Staven- 
 hagen ’s new scena for soprano and orchestra , ' " Suleika " ' ; 
 Henselt ’s first Pianoforte Concerto ; selection from Raff ’s 
 ' " Ttalian ’' Suite , and from Saint - Saéns ’s Ballet music 
 ' ¢ Ascanio , " and Beethoven ’s Symphonies , no . 7 and 8 . 
 the list of solo violinist have be far strengthen by 
 the engagement of Mr. Emil Sauret and Herr Hans Wessely . 
 the service of Mr. Santley have also be secure , and it 
 may be of interest to note that the great english baritone , 
 who have not be hear at these Concerts for many year , 
 will make his first appearance on return from his 
 australian tour . up to date the Guarantee Fund amount 
 to £ 3,335 , almost £ 1,000 more than at this time last year 

 the thirty - seventh season of the City Hall Saturday 
 Evening Concerts commence on the 2oth ult . , when an 
 excellent company , which include Mr. Ffrangcon Davies , 
 the new baritone , appear . Mr. Airlie have book , we 
 understand , several important engagement for these 
 popular and well - conduct Concerts , and in quite 
 another line of business the small choral society 
 have also be show renew enterprise . thus 
 Mr. A. R. Gaul , whose good fortune be so well merit , 
 be again to the fore with one of his Cantatas , no few 
 than four church choir having take up his late sacred 
 work , ' * the ten Virgins . " these be St. george’s- 
 in - the - Fields , Shamrock Street , John Street , and Kelvin- 
 grove Street United Presbyterian Church Choirs . Mr. 
 Gaul ’s perennial Cantata , " the Holy City , " have also 
 find favour with the Strathbungo Parish Church Choir ; 
 Weber ’s Mass in G will be rehearse by the Pollock 
 Street U.P. Church Musical Association ; Mr. Louis N. 
 Parker ’s * Silvia " have be fix upon by the Eglinton 
 Street Congregational Church Choir , as also by the Cath- 
 cart Musical Association ; and the Bridgeton Choral 
 Union will produce during the season Handel ’s ' ' Joshua 

 but our thought be now begin to turn toward 
 the winter Concert room , and to be occupy with 
 anticipation of keen musical delight . rumour of what 
 may be expect during the cold month be begin to 
 { ly about , and hope be excite that some novelty may 
 reach our long ear , as well as excellent feasting upon 
 well know and love classical dainty . at the large St. 
 James ’s Hall , which have be elaborately decorate , Mr. 
 T. A. Barrett have resume the entertainment which 
 last season prove so attractive , announce a great 
 number of engagement which ought to satisfy his friend 
 and to ensure no diminution of the vigour which hitherto 
 have characterise his undertaking . the Carl Rosa 
 Company be occupy the Prince ’s Theatre , and will , 
 probably , ere it leave we , depart a little more boldly from 
 the old list of familiar work . especially would we 
 welcome a revival of Goring Thomas ’s ' Nadeshda , " ' 
 which , after the encomium bestow upon it , have so 
 strangely be withhold from we of late 

 Sir Charles Hallé announce that , as usual , on the last 
 Thursday in this month he will open his season , when he 
 will , no doubt , meet the congratulation of his friend upon 
 his return from triumph in Australia and immediately 
 after will conduct what we all hope will be a successful 
 Festival at Bristol . that the twenty Concerts will be 
 enrich by many orchestral novelty and exhibit the 
 splendid orchestra in undiminished force and finish of 
 performance no one doubt ; and the promise of Dr. Parry ’s 
 " Judith ' ? and Brahms ’s ' " ' requiem " after Christmas be 
 most welcome ; while the revival here of Beethoven ’s great 
 Mass in D must be take as evidence that the able Choir 
 Director , Mr. R. H. Wilson , be grasp the rein firmly 
 and hopefully . at the first Choral Concert , November 6 , 
 " Judas " will be give ; to be follow , of course , by 
 " Elijah , " and the customary two performance of ' the 
 Messiah " on December 18 and 19 

 to open his twentieth campaign Mr. de Jong have 
 secure the service of Mr. George Grossmith , who always 
 crowd the Free Trade Hall to its utmost capacity ; and 
 during the winter we be to have five Orchestral and five 
 Ballad Concerts , for one of which the new precocity , Max 
 Hambourg , be engage 

 two week after your own Worcester Festival , the 
 thirty - third annual Musical Festival of Worcester , 
 Massachusetts , will take place , under the Conductorship of 
 Mr. Carl Zerrahn . the choral work to be perform be 
 ' Israel in Egypt " ( selection ) , ' ' Elijah , " ' * the golden 
 Legend , " " the Erl - King ’s Daughter , ' J. C. D. Parker 's 
 " * 1\¢demption Hymn , " and chorus from " Lohengrin ' 
 and * * Tannhauser . " ' the principal instrumental composi 

 tion select be Schumann ’s Symphony in E flat ( no . 3 ) , 
 | Beethoven ’s Seventh Symphony , a Suite for string 
 orchestra , by Victor Herbert , the Assistant Conductor of 
 the Festival ; Rubinstein ’s ' ' Bal Costumé , " ' and ' an 
 Island Fantasy , " by J. K. Paine 

 _ _ in our city the dead season have not come to an end yet . 
 | with the exception of a few comic opera at some of the 
 small theatre , the music lover who be oblige to spend 
 the dog daysin hot New York have nothing but the Strauss 
 | Concerts at the immense Madison Square Garden Hall to 
 | appease his musical appetite with . these Strauss Concerts 
 | be not an artistic success . the band be too small for this 
 | large hall , and do not play well together . they perform 
 | nothing satisfactorily but Strauss waltz , but even these 
 we have heard give more effectively at the Thomas 
 Concerts at Lenox Lyceum last season . ' the Strauss 
 Concerts be soon to end , and in their place the popular 
 Conductor , Herr Anton Seidl , be to commence , on the 
 2oth inst . , a series of Orchestral Concerts , with a band 
 number 100 performer . besides these " ' Seidl ’' Concerts 
 | we shall soon have the regular Sunday night ' " ' Thomas " 
 Concerts at Lenox Lyceum , while Mr. Franko promise a 
 lseries of Orchestral Concerts of light character . 
 | between these and the Concerts give annually by the 
 | Boston Symphony Orchestra and our own Philharmonic 
 and Symphony Societies , our amateur will soon have 
 plenty of opportunity to revel in the realm of their 
 favourite art 

 the New York Music Hall , the laying of whose founda- 
 tion stone we report in our last letter , be to be ready for 
 occupancy by January 1 , 1891 . it be a very handsome 
 building in the most fashionable part of our city , and hold 
 within its wall three separate hall to accommodate 
 respectively 3,000 , 1,200 , and 500 person , each Hall have 
 | separate lobby , ticket office , and entrance . it will be 
 | the home of our Oratorio , Symphony , and Philharmonic 
 | Societies 

 Tue Hornsey Rise Amateur Orchestral Society ( Con- 
 |ductor , Mr. Arthur Moody ) propose to include in the 
 | work to be perform during the season , which com- 
 | mence on the rst inst . , the following work : symphony , 
 |Haydn ’s no . 2 , in D ; Beethoven ’s no . 1 , in C ; 
 Haydn ’s ' " Military , " Haydn ’s ' * the Farewell ' and ' ' Toy 
 | Symphony , " & c. ; overture , ' William Tell , " ' * Masa- 
 | niello , " ' " * Poet and Peasant , ' and ' * Semiramide , " & c. ; 
 lselection from ' nautical Songs , "   ' Carmen 

 bohemian Girl , " " Pinafore , ' & c. ; ' * the War March ot 

 and end on March 21 . the Monday Concerts will 
 commence at eight instead of half - past eight , and the 
 Saturday Concerts will begin at three 

 the fifth series of Messrs. Hann ’s Chamber Concerts 
 will be give at the Brixton Hall , Brixton , on the 28th 
 inst . , and November 18 and December 9 . the following 
 work , among other , will be perform : quartet by 
 Schubert , Mozart , Haydn , Mendelssohn ; together with the 
 trio in b flat , Beethoven ( Op . 97 ) ; the Quintet ( pianoforte 
 and string ) , Dvorak , & c. some vocal selection will be 
 give at each Concert 

 S1cNnor LaGo announce the opening of his opera season 
 at Covent Garden on the 18th inst . the opera likely 
 to be produce be Ponchielli ’s ' ' Gioconda , " Wagner 's 
 " ' Tannhauser , " and Verdi ’s ' Othello , " with , probably , 
 Gluck ’s ' ' Orfeo . " " , among the artist engage be Madame 
 Albani , Miss Damian , Miss Fanny Moody ; Messrs. 
 Manners , Galassi , Giannini , and Maurel 

 some few year ago the prussian Government purchase | 
 fom Herr Paul de Witt , of Leipzig , a most interesting | 
 collection of antique musical instrument , which have be | 
 the delight ever since of connoisseur visit the Berlin | 
 Museum . in the comparatively short time which have | 
 elapse , the indefatigable collector have succeed in bring- 
 e together a second and almost equally valuable collection 
 which , in the exercise of a most praiseworthy public spirit , | 
 have just be likewise acquire by the authority of the | 
 museum in question 

 a highly interesting sale of autograph by eminent musi- 
 cians be announce by the firm of Liepmannssohn , of 
 Berlin , to take place on the 13thinst . it include important | 
 number by Beethoven , Berlioz , Cherubini , Chopin , Men- | 
 delssohn , Meyerbeer , Schumann , Verdi , Wagner , C. M. von 

 Weber , and many other 

 the recent discovery of egyptian flute and thei 

 the Great Composers — Wagner ee ee ee ae £ 
 | Advance , Chicago ! .. = ee e ee as ee wa 59 
 the Beethoven - House Society in Bonn 591 
 | occasional note .. aa aa oe oe ae es 502 
 | fact , rumour , and rem : " s £ g2 
 Worcester Musical Festival ae sof 
 Coming Festivals da ‘ ea - . 599 
 report of the Associated boa die of the Royal and Royal 
 College for Local i : nation in Music me at + . 599 
 | Dr. Mackenzie 's music to " Ravenswood " .. oa re ant 
 | Globe Theatre ax x “ 601 
 | the Tonic Sol - fa Jubilee in 1 601 

 strument in the world , and be | 
 construct by Messrs. Hill and Son , of London , the builder of the 

 18 , and December 9 ( Tuesday ) , 1890 , at 8 p.m. the following work 

 will be perform : quartet , Schubert , Mend : ' Issohn , Mozart , Haydn ; 
 trio in b flat , Beethoven ( op . 97 ) ; Quintet ( piz inoforte and st rings ) , 
 a. Dvorak , & c 

 M USIC SCHOOL.—CHURCH of ENGLAND 

 analysis of form 
 a display in 

 BEETHOVEN ’s 

 thirty - two PIANOFORTE 

 VINCENT 

 study and piece require for the above examination : — 
 study . 
 BACH.—Invention in two part , no . 9 , in f minor 
 CRAMER.—In A , no . 13 ; a 
 CHOPIN.—Prelude in F ‘ sharp , Op . 28 , no . 13 a 
 CLEMENTI — in B , Gradus ad Parnassum , no . 28 
 CZERNY.- in F , op . 299 , no . 4 
 BERTINI . ine , op . 29 , no . 2 
 CR . \ME R.—In c minor , no . 4 . 
 MOSCHELES,.—In E , op . 70 , no . me 
 PIE CES . 
 BEETHOVEN.—Rondo in C , op . 51 , no . 1 sis oe " 308 
 MENDELSSOHN,—Lied ohne W orte , in b minor os oa . a 
 PARADIES.—Sonata in F , no . 5 - xe his 
 BACH.—Pre 7 and f ugue in x minor ' : 
 BEETHOVEN.- Allegro from Sonata in fe flat , op . 31 
 SCHUMANN.—Novellette in E , op . 21 , no . 7 

 BEETHOVEN.—Quant e piu bello , with variation 
 MOZART 

 Allegro maestoso from Sonata in a minor .. 
 SCHUBERT.—Impromptu in b flat , op . 142 , no . 3 
 EXAMINATIONS of the NATIONAL SOCIETY 
 of PROFESSIONAL MUSICIANS : — 
 SCARLATTI.—Harpsichord lesson in d , no . 16 .. ' 3 
 Harpsichord lesson in e minor , no . 18 2 
 Harpsichord Lesson in D , no . 21 . ee " « 2 
 Dr. C. — ty Vivace .. aie 4 4 
 Etude Melodieuse 4 
 " INCE NT , G. F.—Con Energia ae as ne : 
 staccato e legato .. s ' ey iar 

 new foreign PUBLICATIONS.| the 

 BECKER , ALBERT.—Auf Kaiser Friederich ’s Tod ; funeral 
 March . for Orchestra and chorus . op . 60 : — 
 full score ae vn ne ‘ be a a a5 - o 
 BEETHOVEN , L. v AN — overture from the Ballet ' the 
 Creatures of Prometheus . " op . 43 . arrange for two 
 Pianofortes ( eight hand ) .. ed up " a a be 
 DANCLA , C.—Three short piece . for Violin and Pianoforte 

 op.177 . no.1 . L'Eglantine ; no.2 . La Violette ; no . 3 

