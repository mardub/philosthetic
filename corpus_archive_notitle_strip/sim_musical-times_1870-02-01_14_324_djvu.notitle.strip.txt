HE ENGLISH GLEE UNION—Messrs. Ashton 
Pearson, Clagett and Rudkin. For terms, &c., address Mr, z 
Rudkin, 122, Culford-road, N

HE ST. CECILIA CHORAL SOCIETY.— 
Conductor, Mr. C. J. Hareitr.—For the Performance of 
Cantatas and other large works little known in this country, 
Cantatas by Beethoven, Mendelssohn, Gade, Schneider, Gounod 
&c., have been studied during the past season. Rehearsals every 
Monday Evening, at the Grafton Hall, Grafton-street, Fitzroy- 
square. Periodical Rehearsals of complete works will be held, 
assisted by Eminent Professional Artistes. Subscription:— 
Ordinary Members, 5s. ; Honorary Members, £1 1s. New Members 
must apply to Taos. Cutt, Hon. Sec

XFORD ST. CECILIA GLEE PARTY 
Messrs. Rowley, Guy, Robson, and Sinkins. For terms, & 
address Mr. A. Rowley, New College, Oxford

Tue North London Sacred Harmonic i 
gave a concert at the United Methodist Free Ch 
Lecture Hall, Charlotte Street, Islington, on ‘Tuesday 
evening, the 18th ult. The programme contained selections 
from Mozart’s Twelfth Mass, and Haydn's Creation. The 
soloists were Misses Pedder and Bent, Mrs. Broad, Mr. 
Marriott, and Mr. H. G. Froome, all of whom acquitted 
themselves very creditably. The choruses also were W' 
rendered. Mr. Bent was leader of the orchestra, and Mr. 
James Boyce conducted, There was a numerous audience

At the fourth of the “Monthly Popular Con- 
certs,” held at the Angell Town Institution, Brixton, 
several pieces of much interest were performed, amongst 
which may be mentioned Beethoven’s Quartett in D, for 
two violins, viola, and violoncello, a Duet for two piano- 
fortes, by Mr. Cipriani Potter—excellently rendered by 
Mr. Walter Macfarren and Mr. Ridley Prentice—ani

recognition of its sterling merits throughout the country

Beethoven’s Sonata in G (Op. 30, No. 3) well played by

THE MUSICAL TIMES.—Fesrrvary 1, 1870

By Louis Dupuis

Turse six pieces, like those by Felix Gantier, reviewed 
some time ago in the “ Musical Times,” are especially 
intended for young players, and will, we think, be found 
equally acceptable to teachers and pupils. No. 1 is 
founded on Mozart’s twelfth Mass; No. 2, on Weber’s 
Mass in G; No. 3, on Haydn’s third or “ Imperial” Mass ; 
No. 4, on Beethoven’s Mass in C; No. 5, on Gounod’s 
Messe Solennelle; and No. 6, on Rossini’s ‘‘ Stabat Mater.” 
They are written in the form of little Fantasias, each com- 
mencing with a short introduction. No doubt those on 
subjects from the ‘‘ Stabat Mater,” and Weber’s Mass will 
be the most acceptable to juvenile pianists; for the themes 
are extremely pleasing, and it is music that “ parents and 
guardians” can nod their heads to throughout. For

ourselves, we should have preferred that such headings as

THE MUSICAL TIMES.—Fesrvary 1, 1870. 375

pianofortes produces certain effects we cannot get on one 
fnstrument. As I said before, the size of the pianoforte 
is a drawback to its employment for concerted music. 
Still, in large houses, where the expense of frequent 
tuning is no great object, two pianofortes kept at the same 
itch will be found an immense economy of time in teach- 
ing music, as well as a means of rendering musical study 
far more interesting than it can be made on one instru- 
ment. Where there are three or four girls in one family, 
and no violin to be had, the learning overtures and 
symphonies arranged as pianoforte quartetts, is an excellent 
study. 1 know two ladies living in an out of the way part 
of the country, who seldom hear any music besides a few 
concerts in London, during the six weeks they spend 
there every season; yet they play like artists, and contrive 
+o make acquaintance with the Trios of Mozart and 
Beethoven, with the aid of two pianofortes, the violin and 
violoncello parts being played on the second instrument. 
They had a brother so fond of music that he would sit 
listening to’ them all the evening. How much better it 
would have been if one lady had learnt the violin and 
the brother the violoncello, and so have had the pleasure 
of rendering the music in its original language. For 
thongh a good translation is better than nothing, still it 
is but a translation after all. At the same time, such a 
composition as Mendelssohn’s grand Quatuor for piano- 
forte, violin, viola, and bass, arranged as a duett for two 
janofortes, is most interesting music to play, and when 
me by good pianists, very enjoyable to hear. 1 must 
really bring this rambling letter to an end; and entreat- 
ing all wno cultivate music to remember that “ Union is

atren gth.” 
Iam, Your obedient ne

Notice is sent to all Subscribers whose payment (in advance) is ex- 
hausted. The paper will be discontinued where the Subscription 
is not renewed. We again remind those who are disappointed in 
obtaining back numbers that, although the music pages are 
always stereotyped, only a sufficient quantity of the rest of the 
paper is printed to supply the current sale

BEETHOVEN.—Apply to Mr. J. Gill, Secretary of the Royal Academy 
of Music, Tenterden Street, Hanover Square, who will forward 
a prospectus containing all the desired information

A. R. Swarne.—The consecutive fifths mentioned are certainly there, 
and undoubtedly Handel wrote them ; but we can scarcely under- 
stand what our correspondent means by asking us how they are 
to be “ accounted for

Mawstone.—The annual concert given by Mr. H. F

enniker, R.A.M., took place on the 4th ult. The principal vocal- 
sts were Madile. Mathilde Enequist and Mr. R. West; violinist, 
Mr. J. Carrodus; pianist, Mr. Henniker. There was also a most 
efficient orchestra, selected from the Royal Engineer and Marine 
bands, and a chorus, numbering sixty voices. The Sonata by 
Beethoven (dedicated to Kreutzer) was excellently played by Messrs. 
Carrodus and Henniker; and the former was encored in a Fantasia 
on Scotch airs. The orchestra effectively rendered a Symphony in C 
major, by Mozart, and the overture to the opera of The Admiral's 
Daughter, by H. F. Henniker. The choruses were carefully sung, 
and amply proved that great pains had been bestowed on them by 
the conductor; “Ye Mariners of England,” by Pierson; “ Mark 
now the Heavens.” by Verdi; “ List over head,” by Henniker; and 
the “ Kermesse ” scene, from Faust, were especially well rendered. 
The concert on the whole gave great satisfaction to a crowded 
audience ; the classical selections being better appreciated than is 
generally expected in a country town

Mancuesrer.—At Mr. Charles Hallé’s Concert, on the 
6th ult, Beethoven's Pastoral Symphony, the overtures to La Cle- 
menza di Tito (Mozart), and Manfred (Schumann), and Mendelssohn's 
Grand March (pusthumous), were the principal orchestral works, 
Madame Norman-Néruda created a genuine effect in the Larghetto 
and Finale of Vieuxtemps’s Concerto, and in Beethoven's Romanza 
in G; and Mr. Charles Hallé performed in his usual excellent style 
an Arabesque, by Schumann, and a waltz by Schubert and Liszt. 
The vocalist was Madame Sinico, who was warmly applauded in 
two operatic airs, and in Gounod’s Serenade, to which a violoncello 
obbligato was well played by M. Vieuxtemps

Parrick Brompron, Yorksuire.—Mr, Lax, organist 
and choirmaster of Patrick Brompton, gave his annual concert in 
the school-room, on the 18th ult., which was attended by all the 
principal families in the neighbourhood. Several glees and part- 
songs were given by the choir in a highly creditable manner. The 
solos, which were sung by amateur ladies and gentlemen, gave 
great satisfaction

MENDELSSOHN’S Lieder ohne ,Worte, the § Books

complete ooo ove ee ove owe £4 0 
*BEETHOVEN’S Thirty-Bight Sonatas eve ow 5 DO 
*BHETHOVEN’S Thirty-Four Miscellaneous Pieces 20 
*SCHUBERT'S Ten Sonatas eco ove we 4 @ 
SCHUBERT'S Dances, complete tit s on h ¢ 
*SCHUBERT'S Pieces... ove ove ~ 20 
*MOZART’S Eighteen Sonatas oce ooo ae 3S 6 
*WEBER'S Complete Pianoforte Works eco oo & O 
*SCHUMANN’S Album, containing 43 Pieces ... ~~ £4 
SCHUMANN'’S Forest Scenes. Nine Easy Pieces «=. 3 0 
The Volumes marked * may be had handsomely bonnd in cloth

gilt edges, at 2s. each extra. 
London: Novello, Ewer and Co

33 The Juvenile Vocal Album

32 Beethoven's Sonatas. Edited by Charles Hallé (No. 6). 
31 Beethoven's Sonatas. Edited by Charles Hallé (No. 5.) 
30 Beethoven's Sonatas. Edited by Charles Hallé (No. 4.) 
29 Ten Contralto Songs, by Mrs. R. Arkwright, &c

28 Beethoven's Sonatas. Edited by Charles Hallé (No. 3.) 
27 Five Sets of Quadrilles as Duets, by Charles d’Albert, &c. 
26 Thirty Galops, Mazurkas, &c., by d’Albert, &c

25 Sims Reeves’ most Popular Songs

22 Twenty-one Christy Minstrel Songs. (First Selection.) 
21 Nine Pianoforte Pieces, by Ascher and Goria

20 Beethoven's Sonatas, Edited by Charles Hallé (No. 2.) 
19 Favourite Airs from the Messiah, for the Pianoforte

18 Twelve Songs by Verdi and Flotow, with English words. 
17 Nine Pianoforte Pieces, by Osborne and Lindahl

16 Twelve Sacred Duets, for Soprano and Contralto Voices. 
15 Eighteen of Moore’s Irish Melodies

14 Ten Songs, by Schubert. English and German Words. 
13 Twelve Popular Duets for Soprano and Contralto Voices. 
12 Beethoven's Sonatas. Edited by Charles Hallé, No. 1

11 Six Pianoforte Pieces, by Wallace

