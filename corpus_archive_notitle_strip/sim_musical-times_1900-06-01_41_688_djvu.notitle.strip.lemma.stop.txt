PRINCIPAL TENOR , Trinity College , Cambridge ) . 
 London Provincial Oratorio Ballad Concerts 

 BEETHOVEN CHORAL SYMPHONY NEWCASTLE - - TYNE . — 
 conductor : Dr. Richter . — " difficult 
 thoroughly efficient quartet Miss Agnes Nicholls , Miss 
 Muriel Foster , Mr. Joseph Reed , Mr. Daniel Price , sing 
 florid passage exceptional clearness careful phrasing . Mr. 
 Reed artistic singing tenor solo deserve special word 
 praise . " " — Yorkshire Post 

 term : 31 , Chesterton Road , Cambridge 

 379 

 picturesque lute guitar . young lady 
 help stimulate - dinner conver- 
 sation performance composition 
 sufficiently light calibre interfere 
 proceed digestion . toy 
 key , enamoured youth 
 express conventional gratitude , 
 eye 
 finger . platonic affection 
 suggest Mr. Carl Schloesser ' duet ' 
 ( 929 ) , help fancy old 
 professor sit harpsichord 
 sincere regard , fatherly kind , 
 agreeable young lady play 
 mandoline — play , 
 plectrum hand . 
 ' harpsichord ' early pianoforte , 
 portrait wall 
 mean Beethoven , bring 
 reign pianoforte . 
 keyboard sight , matter 
 leave conjecture 

 look Academy 
 musical subject come 
 curious coincidence . recently elect 
 Academicians send exhibition 
 diploma work , deposit 
 Burlington House election 
 membership , choose 
 musician subject . Sir W. B. 
 Richmond theme proto- 
 type modern virtuoso , Orpheus ( 138 ) , 
 represent return shade , 
 illustration Shelley line 

 performance evening . Lachner 
 send young Levi , use attend rehearsal , 
 ask undertake work . ' getrau'’st Du 
 dir ? ' ( dare ? ) ask . young man 
 yes — performance smoothly . 
 odd coincidence , opera Halévy ' La 
 Juive 

 Levi recollection visit London 
 pleasant ; insist ( inter- 
 view question ) feel home 
 Beethoven Seventh Symphony key b flat . 
 time here—1895 — enjoy 
 luxury high pitch 

 Mr. RuskIN contemplate write ' Treatise 
 principle Music . ' judge reference 
 divine art scatter publish 
 work , achievement intention 
 interesting contribution literature 
 music distinguished author 

 Sunpay ( July 22).—special service Cathedral 
 6.30 p.m. ' hymn Praise , ' Mendelssohn 

 WepnespAy ( July 25).—morning : ' Elijah , ' Mendelssohn . 
 evening : ' Zion , ' Gade ; ' Symphonie Pathétique , ' 
 Tschaikowsky ; Mass C , Beethoven 

 Tuurspay . — morning : ' funeral triumphal ' Sym- 
 phony , Berlioz ( performance England ) ; 
 * requiem , Joseph C. Bridge ( compose expressly 
 Festival ) . 
 afternoon : ' song Miriam , ' Schubert ( Herr 
 Mottl new accompaniment ) ; ' good Friday ' Music 
 ' Parsifal , ) Wagner ; ' deluge , ' Saint- 
 Saéns ( performance England ) . ; 
 evening : ( Music Hall ) , ' Faust , ' Berlioz 

 organist — Mr. J. T. Hughes . 
 conductor — Dr. Joseph C. Bridge 

 HEREFORD . 
 September 9 , 11 , 12 , 13 , 14 . 
 Sunpay ( September 9 g ) , 10.30.—special opening 
 service Cathedral . Benedictus , Mackenzie ; 
 Te Deum Benedictus , Elgar ; ' come 
 pass , ' Ouseley ; ' hallelujah , ' Beethoven 

 TuEspay ( September 11).—morning : patriotic per- 
 formance . national anthem ; mew work Sir 
 Hubert Parry ; symphony D ( . 2 ) , Brahms ; 
 requiem , Verdi 

 evening : ' creation ' ( i. ) , Haydn ; ' Dixit 
 Dominus , ' Leo ; ' Symphonie Pathétique , ' 
 Tschaikowsky 

 THuRspDAY.—Morning : new work , Horatio W. Parker ; 
 Good Friday Grail Music ( ' Parsifal ' ) , Wagner ; 
 ' Choral Symphony , ' Beethoven . 
 evening : ' God goeth , ' Bach ; new work 
 S. Coleridge - Taylor ; ' hymn praise , ' Mendelssohn . 
 Fripay.—Morning : ' Messiah , ' Handel . 
 evening : chamber Concert Shire Hall 

 PRINCIPAL VOCALISTS 

 BIRMINGHAM . 
 October 2 , 3 , 4 , 5 

 programme include : ' Elijah , ' Mendelssohn ; 
 ' Messiah , ' Handel ; ' St. Matthew Passion , ' Bach ; 
 ' spectre Bride , ' Dvorak ; ' mass , ' Byrd ; ' requiem , ' 
 Brahms ; ' scene Song Hiawatha , ' S. Coleridge- 
 Taylor ; ' De Profundis , ' Parry ; ' dream Gerontius , ' 
 Edward Elgar ( compose expressly Festival ) ; 
 symphony Mozart , Beethoven , Schubert , 
 Glazounow , orchestral work 

 PRINCIPAL VOCALISTS 

 FESTIVAL SONS CLERGY 

 246th Festival Corporation Sons 
 Clergy celebrate St. Paul Cathedral , 
 oth ult . , , usual , orchestra 
 achoir 300 voice participate . Sir Arthur 
 Sullivan ' memoriam ' Overture open 
 service , include Myles Foster Magnificat 
 Nunc dimittis , Bach cantata ' sleeper , 
 awake ' ( ' Wachet Auf ' ) , Old tooth Psalm , 
 Beethoven ' hallelujah ' chorus , - 
 supersede Handel ' hallelujah , ' 
 year sing anniversary service . 
 Sir George Martinconducted . office conductor 
 Festival , , hold 
 organist Cathedral . Dr. William Hayes 
 Dr. Boyce discharge duty . 
 add instrumental Old 1ooth 
 Festival ( use ? ) 
 compose anthem ' Lord , thou hast 
 refuge , ' ' blessed considereth poor 
 needy . ' 1844 , orchestra , 
 special feature Festivals , 
 suppress instigation Bishop London 
 ( Dr. Blomfield . ) reinstate till 1873 , 
 - introduction ' good thing ' 
 result Sir John Stainer organistship . 
 Festival 1862 Sir John Goss compose fine 
 anthem , ' wilderness 

 CHURCH MUSIC MONTREAL 

 hymn Action . arrange song 
 Rudyard Kipling Walford Davies 

 Creation hymn . word Rev. J. Troutbeck . 
 music compose L. van Beethoven , arrange 
 C. Sachs 

 music 

 Action ' widely recognise render 
 description unnecessary praise superfluous ; 
 setting admirably suit large 
 choir . like hear deliver tenor 
 bass Royal Choral Society 

 Mr. Sachs arrangement Beethoven magnificent 
 ' Creation hymn ' crave abnormal 
 power vocal tone , average sized choir 
 impressive . add 
 english version text late Rev. J. Troutbeck , 
 Mr. Sachs accomplish work 
 judicious skill 

 PIANOFORTE MUSIC 

 diversity opinion effect 
 combine band , effect way 
 ideal contend . , 
 hand , laugh experiment symptom 
 - day megalomania absurd . merely 
 large band enlarge ; fusing 
 orchestra , idiosyncrasy itsown . tone 
 128 string merely like doubling 
 tone body string . net result 
 wonderful union breadth depth , 
 chief strength english string , lightness 
 sweetness great charm French 

 concert symphony : 
 ' Pathetic ' ; Beethoven fifth eighth , conduct 
 Mr. Wood ; Beethoven ' Eroica ' Seventh , 
 Schubert ' unfinished , ' conduct M. Chévillard . 
 contrary expectation , ' Pathetic ' fare worst 
 change condition , Beethoven Symphonies 
 gain force grandeur little . move- 
 ment Beethoven suffer Finale 
 Seventh Symphony . ' Funeral March ' 
 ' eroica ' — especially fugal passage — superb 
 Finale irresistible . trio scherzo 
 Seventh magically beautiful , fifth infinitely 
 majestic . Wagner excerpt , 
 ' Meistersinger ' Overture , prelude death scene 
 ' Tristan , ' ' Funeral march ' ' Gotter- 
 dammerung ' impressive . 
 M. Chévillard conductor origin- 
 ality real merit . merely follow 
 tradition M. Lamoureux , far dramatic 
 vigorous . experience wide secure 
 effect wish certainty . share 
 honour Mr. Wood , , , 
 great good work 
 english conductor 

 soloist Madame Albani , Miss Clara Butt , Miss 
 Lillian Blauvelt , M. Ysaye . violinist stand head 
 shoulder . performance 
 Max Bruch Concerto D Vieuxtemps Concerto 
 ( . 4 ) superb , pale masterly 
 reading Bach ' Chaconne . ' singer , Miss 
 Blauvelt , , distinguish , singe 
 delightfully time . particularly sing 
 afternoon , introduce song 
 Johann Strauss , ' Voci di Primavera , ' orchestrate Mr. 
 Clarence Lucas , cordially recommend 
 soprano wish change ' Sweet Bird 

 fifth concert , 24th ult . , bring forth 
 novelty form orchestral suite Mr. S. 
 Coleridge - Taylor , entitle , ' Scenes - day 
 Romance , ' originally announce , way , ' Miniatures 
 - day Comedy . ' , , com- 
 poser furnish clue romantic happening 
 ' scene , ' title somewhat nature 
 te - herring draw path music , 
 speculation apt distract 
 listener music se . movement 
 suite consist ( 1 ) Allegyo , E minor , 6 - 8 time ; 
 ( 2 ) Andante , G major , 2 - 4 time ; ( 3 ) Tempo di valse 
 b minor , ( 4 ) Presto , e minor , 2 - 4 time . Mr. Taylor 
 great deserve reputation 
 ' Scenes Song Hiawatha , ' new work 
 pen command respect genius . 
 opinion doubtless strangely variance 
 merit , late achievement — fact , 
 concert - room , performance , 
 possible learn critical hearer bestow 
 laudation ' perfectly lovely , ' , 
 equally competent judge , dismiss ' awful stuff . ' 
 time , great tester , judge difference 
 opinion . verdict probably avoid extreme . 
 listen Mr. Taylor recent strain arise 
 question , ' - bar phrase banish 
 modern symphonic music ? ' interrogator pro- 
 bably old - fashioned young blood . 
 danger domination two- 
 bar - bar phrase , snippity wee - bittieness 
 result therefrom - ' development . ' 
 term ' barking brass ' 
 snare young composer , . 
 remind composer Schubert 
 good friend wood - wind 
 orchestra 

 regard remainder concert , lover pure 
 melody , pellucid stream , derive supreme satis- 
 faction strain air , dainty variation , 
 F master - melodist , Mozart , Divertimento 
 B flat , meaninglessly designate ' . 15 ' 
 programme . Herr Ernst von Dohndnyi artistic 
 interpretation Beethoven - welcome Pianoforte 
 Concerto G , Miss Ada Crossley successful 
 ' Inflammatus ' Dvordk ' Stabat Mater . ' concert 
 conclude Berlioz fine finely play symphony , 
 ' Harold Italy , ' excellent artist , Mr. A. 
 Hobday , satisfaction viola obbligato . 
 exception suite , conduct 
 composer , concert direction 
 Mr. Cowen . concert place Queen Hall 

 YSAYE CONCERTS 

 ' Ysaye ' concert , 
 place 17th ult . , Queen Hall , bear convincing 
 witness superlative power belgian violinist . 
 announce concerto — Bach E , Beethoven , 
 Mendelssohn’s — remarkable faith 
 musical public , play herculean task , 
 succeed , M. Ysaye succeed , veritable tour de 
 force . student music selection possess 
 exceptional interest . style work distinct 
 way great . loftiness nobility 
 Beethoven immortal composition rightly occupy 
 central place , tower companion . 
 thoughtful comparison work 
 inevitable , M. Ysaye perfect sympathy 
 desire compare merit 
 reading . Mr. Wood secure ideal accompaniment 
 orchestra , sympathetic manner M. 
 Ysaye support delicate passage result 
 entrancing effect . Mozart allegro D , hear 
 occasion time England , movement 
 symphony , naive example master 
 genius year age . sonata- 
 form , lay string , oboe , horn , possess 
 touch appeal admirer Mozart 

 MADAME ALBANI CONCERT 

 BRIDLINGTON MUSICAL FESTIVAL . 
 ( SpEcIAL correspondent 

 customary spirit enterprise characterise 
 annual festival ' run ' Mr. Bosville Bridling- 
 ton , place seventh time April 26 . 
 astonishing enterprising amateur 
 accomplish purely local chorus scratch band 
 player gather Yorkshire . 
 bare enumeration composition hear afternoon 
 evening concert demonstrate . after- 
 noon Verdi ' Requiem , ' Beethoven eighth 
 Symphony , piece Mr. T. T. Noble ' Wasp ' 
 music , March Tschaikowsky . evening 
 choral work Coleridge - Taylor ' Death Minne- 
 haha ' Stanford ' Revenge , ' ' Coriolan , ' 
 ' Midsummer Night Dream , ' ' Carnaval Romain ' 
 Overtures , ' Siegfried Idyll , ' vocal solo 

 Enterprise , , characterise 
 programme , satisfactory able record 
 distinct advance efficiency performance . 
 ' Revenge ' satisfactory : 
 festival , 
 reliance place chorus familiarity 
 work , matter fact , extremely catchy . 
 ' requiem , ' , strain resource band 
 chorus pretty severely , exception 
 performance creditable , chorus singe 
 intelligence far atone certain 
 defect inseparable North East Riding 
 choir , band efficient 
 . doubt great measure 
 increase power force experience 
 giving Mr. Bosville , , apparent exertion , 
 exercise great control player singer 
 aforetime . especially notable 
 symphony , excellent - round performance 
 , conductor reading evidence sympathy 
 intelligent study music . ' death 
 Minnehaha , ' previously hear 
 Yorkshire , exceedingly , far 
 expect case work 
 novel difficult . profound impression , 
 highly sympathetic singing 
 soloist , Miss Agnes Nicholls Mr. F. Harford , con- 
 tribute little . remarkable poem 
 , doubt monotonous metre , 
 hitherto attract composer note , pro- 
 vide material work powerful , picturesque , 
 , , varied Mr. Coleridge - Taylor 
 ' Wedding - Feast ' ' death Minnehaha . ' 
 note tragedy genuinely moving , 
 feel heart head share 
 work . description Minnehaha hallucination 
 appeal strongly composer 

 401 

 April 26 , morning afternoon devote solo 
 singing village orchestra competition . - seven 
 soloist present , village 
 orchestra . test piece orchestra Schubert 
 overture ' Rosamunde . ' playing great 
 improvement previous year . 
 expect orchestra 
 like complete , makeshift brass 
 wind result extremely creditable . 
 Leasgill band , Mrs. T. A. Argles , gain 
 place . evening concert , choir 
 place , ' Phaudrig Crohoore ' ( Villiers Stanford ) 
 perform , Miss Wakefield direction , great 
 success . combine choir number 
 singer . Mr. Plunket Greene sing eighteen song inimit- 
 able fashion , include ' Maud ' cycle , compose Mr. 
 Arthur Somervell ; ' Scotch ' Symphony admirably 
 play direction Mr. Risegari . 27th , 
 morning evening occupy competition 
 village choir variously constitute . female voice 
 mixed voice choir compete . chief 
 section , test piece ' sweet love ' ( Villiers 
 Stanford ) . Kendal Parish Church Choir gain 
 place , Yealand come close . singing 
 day high standard . evening 
 concert commence fine performance 
 Beethoven C minor Symphony , conduct Mr. 
 Risegari . Mr. Plunket Greene sing song 
 set , receive great enthusiasm . 
 chief feature programme Coleridge - Taylor 
 ' Hiawatha Wedding - Feast , ' preparation 
 Miss Wakefield , conduct , spare 
 pain . Mr. Henry Beaumont sing tenor solo . 
 work receive large audience . 
 evening concert Miss Wakefield popular setting 
 Colonel Hay word , ' boy come home , ' 
 sing chorus fervour . Saturday , 28th , 
 child day . fourteen junior choir compete . 
 year generally feel child 
 sing year , year 
 unmistakable improvement . singing 
 acceptably tune afford evidence skilful training . 
 competition , prize distribution , concert 
 , chief feature humorous 
 cantata ' frog Ox , ' Shapcott Wensley 
 Sir Frederick Bridge . Dr. McNaught adjudicate 
 competition . Mr. A. H. Willink prove 
 able secretary 

 evening concert Miss Wakefield definitely 
 resign position conductor . feel , 
 year ’ service , entitle rest , 
 proud leave Festival flourishing condition . 
 know Miss Wakefield remarkable work 
 create enthusiasm music district 
 stigmatise unmusical wish 
 - earn retirement . propose shortly 
 account work need 
 occasion 

 Tue Victoria Madrigal Society bring season 
 successful conclusion , April 26 , St. Martin Town 
 Hall . choralist remain true mission 
 singe , watchful conductorship Dr. G. 
 Stanley Murray , justly esteemed composition 
 Weelkes ' vesta , ' Morley ' month 
 maying ' ' saith dainty darling , ' 
 Wilbye ' Lady Oriana . ' piece 
 sing genuine taste combine spirit . vocal 
 contribution forthcoming Miss Gertrude 
 Macaulay Mr. Henry Turnpenney , harp solo 
 Miss Molteno 

 Tue North - West London Choral Society , Mr. 
 W. H. Speer conductor , honourably acquit 
 Hampstead Conservatoire 7th ult . 
 choralist , number , sing steadiness 
 regard detail anthem ' ascribe unto Lord ' 
 ( S. S. Wesley ) , Stanford ' Awake , heart , ' 
 Mendelssohn ' hear prayer , ' effort last- 
 particularly deserving praise . Madame 
 Isabel George Mr. W. P. Richards vocal 
 soloist , Mr. Healey Willan excellent work 
 organ , Herr Georg Liebling play Beethoven 
 ' Sonata Appassionata ' pianoforte composition 

 South London Choral Association , 2nd ult . , 
 satisfactory interpretation Haydn 
 Mass b flat Mendelssohn ' Athalie , ' 
 principal solo vocalist Misses E. Coward Ch . 
 Edwards , Madame Edith Hands , Messrs. F. Bamford 
 J. Lacey . excellently train choir , 
 direction Mr. Leonard C. Venables , enter 
 task great spirit musical intelligence . 
 word praise omit share 
 performance orchestra . Mr. Richard Temple 
 recite connect narrative ' Athalie ' 
 fervour dramatic effect 

 sixth annual concert Marlborough Place 
 Amateur Orchestral Society place Hampstead 
 Conservatoire 22nd ult . programme include 
 Schubert ' unfinished ' Symphony , Mendelssohn 
 ' Athalie ' Overture , Mackenzie ' Benedictus , ' 
 Moszkowski spanish Dances . interpretation 
 work , performer abundant evidence 
 efficiency skilful training alert 
 conductorship Mr. Paul Oppenheimer . Mr. N. H. 
 Geehl brilliantly play movement Beethoven 
 Pianoforte Concerto C , Mrs. Helen Trust con- 
 tribute song acceptance 

 fine new Parish Church St. Mary , Hornsey , 
 excellent rendering Stainer ' Daughter Jairus ' 
 Mendelssohn ' hear prayer ' form 
 musical attraction special service , 16th ult . 
 soprano solo admirably sing Master Jeffries ; 
 tenor bass work find capable exponent 
 Messrs. Thomson Mason . choir , number 
 seventy voice , sing finish precision , 
 evidence careful training . Mr. Henry J. 
 Baker conduct , Mr. H. J. Baggs ( assistant - organist , 
 St. Mary ) preside organ , overture 
 accompaniment tastefully neatly 
 execute 

 new Philharmonic Society fall honour 
 concert room 
 home musical activity , unhesitatingly 
 performance Society 
 high achievement . work select farewell 
 Schumann ' Paradise Peri , ' , 
 , receive excellent interpretation . high level 
 artistic excellence reach chorus follow 
 contralto solo , ' sweet welcome brave , ' 
 chorus Houris , ' wreathe ye step , ' ' oh , 
 bless tear true repentance , ' ' joy , joy . ' 
 solo capable hand Miss Alice Esty , 
 Miss Alice Larkin , Mr. John Coates , Mr. Mansell 
 Lewis , acquit excellently 

 second programme devote 
 Beethoven ' Pastoral ' Symphony Wagner 
 ' Meistersingers ' Overture , band , 
 skilful leadership Mr. Lewis Hann , finely interpret 
 work . Mr. C. J. Phillips bear conductor ; 
 know score thoroughly miss point . 
 orchestra , , evidence 
 common fault - assertiveness accompany 
 vocal music . conductor need exercise great firmness 
 matter insist accompaniment 
 subservient voice 

 Society wise choice appoint 
 new courteous secretary , Mr. C. E. Rainger , 
 arrangement way admirable , add 
 little success memorable concert 

 Wilbye madrigal , ' Flora fair flower , ' 
 solid rigid performance mar effect delicious 
 piece light wanton expression . choir 
 immensely improve . bass somewhat 
 weak , quality lady ’ voice admirable . 
 noticeable motet female 
 voice , ' heaven declare glory God , ' 
 Dr. Culwick , , composition , distinctly 
 average work . Mrs. A. McC. Stewart ( 7ée Alex 
 Elsner ) enthusiastically welcome 
 Dublin concert . Mr. Henry Verbrugghen , clever artist , 
 Mr. Edwin Wolseley soloist 

 concert season Dublin 
 Orchestral Society place 2nd ult . , 
 Theatre Royal , Beethoven Symphony F ( . 8) 
 play time Dublin , receive 
 excellent rendering . Schumann Concerto , Signor 
 Esposito pianist , repeat , solo artist 
 secure perfect ovation itsconclusion . Humperdinck 
 Vorspiel ' Hansel und Gretel ' play time 
 Society , ' Tannhauser ' Overture . 
 performance revelation 
 think piece Wagner inter- 
 prete import band . Signor Esposito 
 congratulate signal artistic success second 
 season Orchestral Society 

 MUSIC EAST ANGLIA . 
 ( correspondent 

 Eyam Choral Society perform Haydn ' Creation , ' 
 roth ult . , direction Mr. J. W. Froggatt . 
 large audience fine singing 
 chorus feature concert . small orchestra 
 excellently lead Mrs. Smedly , play violin 
 solo striking success . Mr. T. Mellor 
 organist Mr. E. A. Ireland accompany . 
 principal Mrs. Haynes , Mr. H. Bryars , Mr. A. E. 
 Innocent 

 408 MUSICAL TIMES.—Junz 1 , 1go00 . 
 MUSIC WALES . | soul atin spoon . pity , ( largely Milit 
 | amateur ) string . soloist Miss Estella c 
 ( CORRESPONDENT . ) | Linden , good reading soprano , pie 
 ar concert Cardiff Musical Society , | unable justice consequence bad Alt 
 April 25 , Cowen ' ode passion ' cold ; Mr. Henry Beaumont , fine tenor voice ring th 
 Mendelssohn ' hymn praise ' perform | ' onaway ! awake , beloved ! ' ; Mr. W. E. th 
 success , solo vocalist Miss Gertrude ; Davies , si 
 Drinkwater , Madame Nellie Griffiths , Mr. Wetten , | touching baritone solo ' death Minnehaha . ' 
 Bath . Mr. T. E. Aylward conduct , usual . work composer enthusiastically 
 April 28 ' hymn praise ' satisfactorily | receive . . : 
 perform Newtown ( Mont. ) Harmonic Orchestral } _ concert include Tschaikowsky posthumous 
 Society , conductorship Mr. J.C. Gittins . | duet , ' Romeo Juliet , ' overture - Fantasia Ro 
 soloist Miss Emily Davies , Miss E. J. Taylor , and|same title . work occasion b 
 Mr. Gwilym Richards . perform time England , Lond 
 summer holiday course music , the| probably . juxtaposition prove highly Hans 
 University College Wales , Aberystwyth , follow | interesting , music duet elucidate Schol 
 teacher appoint : pianoforte organ , Mr. | overture , love music work virtually ( Si 
 Leah ; violin , Mr. Ollerhead ; voice , Mr. Wilfred Jones ; | identical . beautiful duet sing Miss Linden Cumr 
 art class - teaching , Mr. W. T. Samuel ; harmony , | Mr. Beaumont requisite passion , Stern 
 counterpoint , composition , orchestration , Mr. David difficult overture spirited direction Oscat 
 Jenkins , lecturer music College . welsh musical | Mr. Atkins . : Oscar 
 student largely avail opportunity | _ day , afternoon 5th ult . , \ 
 offer medium annual holiday |the Worcestershire Philharmonic Society , conduct TH 
 course . Mr. Edward Elgar , fifth concert hall . ' con : 
 asmart function conscience . adream p 
 fair woman ! gown ! hat ! music mont ! 
 thing discuss . , begin , orche 
 MUSIC WORCESTER . — ag — sere 4 , " 
 start gorgeous chorus , ' Wach ' auf ! ' : 
 ( SPeciaL CORRESPONDENT . ) Waals C Malahesttnaee , understand , usher oon 
 gorgeous tone London Festival double |in Society concert ( title , 
 orchestra ring ear , wend way , | , rousing motto ) , hear Wagner ' Rienzi ' th 
 3rd ult . , tothe Public Hall Worcester City . thither ; overture , Brahms ' Schicksalslied , ' Beethoven fourth rende 
 foregather goodly company music - lover | Pianoforte Concerto ( play rare refinement Hall , 
 let sweet music creep ear , wax warmly | beautiful technique rarely - hear irish pianist , solois 
 appreciative sing , play , oft- | Miss Elizabeth Reynolds ) , Mr. Granville Bantock orches- 
 maltreated twin cantata , ' Hiawatha Wedding - Feast ' | tral scene ' Funeral ' ( section i. orchestral drama Kings 
 ' death Minnehaha , ' gifted composer , | ' Kehama ' ) , Cavatina chorus Druids Bellini } 
 toconduct . Worcester Festival Choral | ' Norma , ' Act I. , Arcadelt motet , ' Ave Maria , ' TH 
 Society second concert season , enthusiastic | Brahms - song lady ’ voice ( Op . 44 ) , R 
 young conductor , Mr. Ivor Atkins , thing | ballet ( ' dance hour ' ) Ponchielli ' La Schol 
 fresh beauty Mr. Coleridge - Taylor music | Gioconda . ' eclecticism , ye London Mich 
 joy , exceeding pain ( - tongued rumour | compiler hackneyed programme ! excep 
 wag ) secure performance worthy work . | thing perfectly render mark . becon 
 let hasten assure reader chorus , | ' Rienzi ' Overture , orchestral portion Colles 
 preparation Mr. Atkins responsible , easily | Brahms cantata , accompaniment concerto , 
 magnificently beat soloist orchestra field . | ballet play style little suggestive M . 
 hear performance cantata , |the fact orchestra performer contain b D 
 honestly affirm small , excellent | large number amateur , chiefly , course , ie 
 Worcester Society easy regard those|the string . perfect playing hear 
 quality constitute good chorus singing — e.g. , good , | divinely beautiful orchestral introduction epilogue p " ore 
 round , ' musical ' tone , crisp attack , exact release , |in ' Song destiny ' : r 4 
 true intonation , clear enunciation , intelligent | . Mr. Bantock ' scene ' leave ( _ 
 appreciation emotional music in| public ) utterly unimpressed . desire 
 vary mood . point bring | strive appreciate native music , confess blind 
 careful systematic work thoroughly | productive english composer suc- road , 
 competent choir - trainer , Mr. Atkins | ceede convince dull brain ' Kehama , ' select 
 thank enable hear such|in nineteen ( - ? ) movement — perfor 
 beautiful work beautifully expressively sing . monstrous fearful wild - fowl modern develop- profe : 
 enter hall anathematise ( molto dolce e¢ piano ) |ment symphony — contain beautiful music . 
 stern Duty away Lucullus| ' , ' g¢ 
 feast provide Queen Hall , chew oft - turn cud | brilliant musician trenchant critic like Mr. Elgar N 
 ' mere provincial performance ' ! soon | perform ? solve riddle , wait organ 
 - toned chorus start , ' shall hear Pau - Puk- | hope eye open perceive beauty Strab : 
 Keewis , ' & c. , forget Queen Hall marvellous | blind , alas ! choir sing win t 
 musicking , enjoy Mr. Taylor work | refinement expression , regard tone 
 absolutely new . | compare Festival Choral Society . th 
 wonder simple music , adequate per- | slow movement Brahms ' song destiny ' dedic : 
 formance , appear fresh delightful . great master unaccompanied - song ( sing , orche : 
 , small chorus produce beautiful | excellently sing , original German ! ) , Britis 
 tone ; quantity , , astonishing | good effort . war . 
 quality , evident Mr. Atkins Mr. Henry Brown soloist Bellini ' Cavatina , ' Mi 
 barely 100 voice good workman- | Mr. Elgar conduct thorough savoir faire | 
 like choral body Kingdom . | sangfroid result intimate acquaintance Royal 
 orchestra efficient . , truth tell , | technique orchestra , long experience , 
 , wood - wind percussion especially leave | inborn aptitude , ally artist temperament hr 
 little desire . drum cymbal repre- | genuine enthusiasm beautiful worthy om 
 sente , frequent striking effect cymbal | find art . shall hear Mr. Elgar b hel 
 strike drumstick omit ; tambourine | conductor future , mistaken . — 
 conspicuous absence , , surely , amateur ; concert , different kind , MR 
 strike ; triangle music ! hall , 8th ult . , ' Civil- Alexa 

 XUM 

