down the house, it is almost invariably with a work 
which is at least long and large, if not great; a 
short solo played on the heels of it is very highly 
tried, and there are not many that come off with 
honours

396 THE MUSICAL TIMES—Jvune 1 1923 
By the way, why is there almost invariably a| The other quotation, however, showed that these ~ 
conspiracy of silence as to these little encore| old Norwichians had, like Mrs. Beeton, ‘another | 
pieces? If the player cannot trust himself to} way,’ and a far less desirable one. At this same] m 
announce the title, or if he regards such an under-| festival concert an alien Queen of Song, on f 
° ° ° . . . ~ . . " 8, ¢ 0 
taking as beneath his dignity (like opening the! Caradori, sang Beethoven’s Quaz/ Song, and, we } to

pianoforte or adjusting the seat), there ought to be| read, of 
somebody at hand with sufficient heart and voice| .. . . sang it very well ; and it was instantly signified | bu 
to address the audience and say the necessary by the President’s wand that an encore was desired, 
two words. ‘The repertory of great players is were of course took place. . or 
distressingly small (with honourable exceptions in| This is worse than the spattering of handclaps! pi 
Samuel, Mitchell, and a few other natives), but it) I should have stood up with my neighbours after Ms 
may easily contain a few trifles unknown to the| the Gibbons chorus, but the sight of a Member of fa 
audience. Sometimes, it is true, the ignorance of| the Nobility and Gentry of the Neighbourhood | 
the latter is a disgrace. I remember Cortdt| evoking an encore by a wave of his wand would | 4 
playing as an encore a Minuet of Purcell’s that| have made my angry passions rise. The calm of | at 
ought to have been familiar to nine-tenths of his Norwich would have been broken by a voice, | vir 
hearers. It was the little one in G that has more| We want to hear . . .’ anything but the Quail | be 
than a hint of ‘God Save the King’ in its melody | Sovg. we 
and rhythm, and it happens to be one of the ” lies 
handful of Purcell’s harpsichord pieces that has Finally, a word for the only encore enthusiast | sus 
been available in cheap English editions for at| Who always gets just what - wants, and who gets od 
least thirty years. Yet its performance was followed | it without making himself a nuisance: the | ° 
by whispered questions, * /I’Aa¢ is that charming gramophonist. He can at once repeat a choice is 
little piece?’ and even music critics (Shame!) bit, without hearing the whole of the work, and he on 
were among the askers. Had it been a Minuet by | ©#" have it as many times as he likes. Here] por 
Couperin or Rameau probably most people would shamelessly take my place among the insatiables qua 
have spotted it at once. A more than usually beautiful phrase of Gerhardt’s, | do 
the little bit of bass flute work in Bliss’s| qui 
The depths to which famous singers have sunk In the Ballroom, a pet passage in the Flonzaleys’} mal 
P : : . e . . " . ] , - rect 
in this matter of encores is shown by the fact that playing of the slow movement of the Nigger inst 
ggaes 1 . ‘ Quartet—back goes the needle, and more than 
last year, when the Northcliffe Press boosted Thin eulft secs of & meme ie 
Melba’s Albert Hall concert, we were told before- — . i “| sub: 
‘gp delight is possible only on the gramophone: the hil 
hand what the encores were to be. ~ For encores , . . pat 
; : 7 : encore in the concert-hall is a slow and speculative | {>,, 
I shall sing so andso. . . .’ That a singer should sabeg . and it is } dds th 
be prepared is right, and considerate to the business ——_ — it is long odds tat) blur 
“ae % all the critics who say hard things about the encore | rela 
accompanist; but this calm announcement days ; ; one’ eal h b 
beforehand is the last word in assurance. Only | DUS@Nce succumb weakly over a Se he 
aa ; . * | Given a little of what we fancy (in Marie Lloyd's | bad 
divas seem to be so blessed with a forehead of ig ‘ ‘tle | Our 
brass phrase), it is only human of us to ask for a little : 
; pin aeetaahag more. Scratch us, critics and mob alike, and you .y 
Discussing this question in the J/usica/ Zimes of | will find that we are all encorists under our skin. sare 
February, 1919, I gavea couple of short quotations into 
from the Musical World of September 30, 1836, TASTE obvi 
showing two diflerent ways of obtaining encores— By FRANK S. Howes obvi 
one democratic, the other autocratic. They will, Slee : ‘ .. | and 
I think, bear repeating. In an account of the De gustibus non disputandum, it has been sait,} . 
esad h Sinica anes ° Neer i Noa and yet we all constantly make judgments of taste ultin 
orwich Festival occurs the following : __. |and are quite emphatic about them. There is 0) 1.0, 
an ee —— me fo the Son of gainsaying that the whole subject of taste bristles the 
avid, one of Orlando Gibbons’s best specimens . . . | with difficulties so soon as it is examined beyond | I 
was performed in a most masterly manner ; the whole h : f om nein hell £ lity whic! 
audience rose at its conclusion, which was an intimation the point of making & simpie ju gment of quay: The | 
for its repetition. It was accordingly encored. The first to arise 1s the wide divergence of vee lests. 
, ; : a saat ,| amounting sometimes to flat contradiction—which § ment 
That was an encore if ever there was one ‘| found in pronouncements of equally eminent critics | 1: 
Orpheus himself might have envied Orlando this or of equally sensitive laymen on the same work of to its 
achievement of moving an entire audience to its|art. So great is the difficulty of finding anything > ae 
feet. No conductor or music critic could com-| like an objective standard, that most people quickly 
> . : : : —: “| badn 
plain of an encore so impressively and unanimously | reach, as the conclusion of an argument involving Th 
demanded. Compare this quiet uprising of a huge | @sthetic judgments, the very unsatisfactory proposit™ 
audience with the spattering of handclaps that most Paes Bs epee a matter of — The a sine 
singers deem sufficient warrant for giving another SORNNNES 65 CUED & cnnebaten Res Het » on “put | follov 
ballad. They knew a good thing at Norwich a Be tee shes it hen tole as pracy eee 
ht d eee zere.d A. B . that it implies that no standard is possible, whereas} ©™ 
iundred years ago. Not many audiences to-day are | all artists and sensitive persons know positively th] (¢ 
given to encoring choral works of any kind. Choirs, | there is a standard of some kind, which quitt 
like orchestras, are mere bodies of musicians, and | definitely marks off good from bad. But what's 
bouquets and encores are rightly reserved forsoloists. | the nature of the standard? The best way at

Occasional Wotes

The fourth Aberystwyth Festival will be held by 
the University College of Wales at the University 
Hall, Aberystwyth, on June 22 to 25. The con- 
ductors will be Sir Edward Elgar, Sir Walford 
Davies, and Dr. Adrian C. Boult. The choir and 
orchestra will be formed of members of the College 
Choral and Orchestral Unions, with various other 
contingents, and the British Symphony Orchestra. 
The following is the programme: June 22, Mozart's 
Symphony in E flat and works by Elgar; June 23, 
Beethoven’s seventh Symphony and Choral Fantasia; 
June 24, public rehearsal of the S¢. Matthew Passion; 
June 25, the S¢. /atthew Passion (afternoon) and 
miscellaneous programme (evening

The arrangements for the Handel Festival 
(June 16, 19, 21, and 23) are now well under way, and 
report speaks highly of the quality of the choir. But 
that is to be expected, since Sir Frederic Cowen has 
long since shown us that a choir of thousands need 
be no mere noise producer. There is nothing more 
impressive at a Handel Festival than the Aianissimo 
singing, and nothing more astonishing than the 
nuances. These subtleties were unknown in the 
earlier Festivals, when the choir, having the power 
of a giant, used it like one all the time. Apropos of 
this choir, Jeremiahs who say there is no choralism 
in London may be reminded that the great bulk of 
the three thousand five hundred singers are drawn 
from the London district. Even the leaven of a 
Yorkshire contingent couldn’t make the choir so 
good if these Londoners were a poor lot

closely imitated from the ‘O ciel, quel est donc ce 
mystére?’ in Za Dame Blanche. 
But he also points out things that herald the style of 
Wagner's maturity

In the Neue Musikzettung (April 5) Dr. R. 
Scherwatsky examines the music of Zhe Fairies. 
He notes that it owes much to the influence of 
Mendelssohn and Beethoven, and considers it ‘ the 
bridge between the works of Wagner’s youth and 
those of the first period of his maturity

POLYTONALITY

selections from (/efia, played by the band of 
the 1st Life Guards (12-in. d.-s.), and from .)/adame 
Butterfly, played by the Regent Orchestra (12-in. d.-s.), 
and Percy E. Fletcher’s /olie Bergere and /leurette 
@ Amour (1o-in, d.-s.), the two latter played by the 
Regent Orchestra

The chamber music records are first-rate. I have 
so far heard nothing better than that of the Flonzaley 
players in the /res/o from the Beethoven Quartet 
in D, Op. 18, No. 3 (H.M.V. 12-in.). We have had to 
complain so often about the nebulous and apologetic 
reproduction of chamber music that it is a pleasure 
to come across one so vividly clear as this. Now 
we know that there is really no need for the ’cello 
part to become a murmur from time to time, or for 
the bottom suddenly to fall out of the texture; we 
must keep the recorders up to it

Excellent, too, though a little less definite (partly 
owing to the character of the music) is the record of 
the London String Quartet’s playing of Nos. 2 and 3 
from Frank Bridge’s TZhree J/dylls (A..-Voc. 
10-in. d.-s

In * Professor’ Ella’s book, A/usical Sketches at Home 
and Abroad (3rd Ed., 1878, p. 50), reference is made to 
ithe first Musical Festival held in York Minster, in 1523, 
under the direction of Greatorex, conductor of the Concerts 
|of Antient Music. Ella complains that ‘much confusion 
arose from the neglect of details in the organization of the 
285 vocalists and 180 instrumentalists engaged’; and theo 
| proceeds

Shortly before the commencement of an evening 
concert it was discovered that a parcel expected 
from London with duplicate parts of Beethoven's 
C minor Symphony, for the stringed instruments, had 
not arrived. In this dilemma, it was agreed that the 
Symphony should be omitted. No sooner, however, did 
Miss D. Travis begin with the Scotch ballad, ‘ O, Charlie 
is my darling,’ than a general murmur arose among the 
audience, and one of the stewards, with a stentorian 
voice, lustily called, ‘‘ Symphony ; none of your darlings, 
we can hear them any day in Yorkshire. I insist upon 
the Symphony being played.” Appeal, explanation, 
or excuse was in vain, and at last the Symphony was 
scrambled through, with six or eight to a part. Mor! 
led, and every musician, stimulated by the good taste of 
the steward, played his part with zeal and unflagging 
spirit

The gentleman who then rose to the occasion was Mr. F

MISS ANNA HEGNER

Historical recitals have their uses, but it is a debatable 
point whether the drawbacks are not greater than the 
advantages. Miss Anna Hegner’s scheme, for instance, 
aims at a representative presentation of violin concerti of all 
schools and epochs. She is hence bound to give us the 
hackneyed with the unhackneyed, to place in juxtaposition 
the masterpiece and the ‘ parlour’ piece, for indeed the 
concerti of de Beriot are, except in form, nothing more than 
morceaux de salon. Hier first recital was all that could be 
wished. Viotti, Bach, Mozart, Beethoven—these are 
excellent company, as is proved by the fact that hardly a 
recital is given which does not pay due homage to one or 
more of them. The second programme was open to 
objection, for indeed of three concerti played only one 
lives now outside the school-room—the Wieniawsky 
Concerto in D. De Beriot and Vieuxtemps have both 
forfeited the freedom of the concert-room. Vieuxtemps now 
and again is smuggled through under the gis of a great 
virtuoso, But Miss Hegner does not belong to their class. 
She is much more than a mere virtuoso, as the pellucid 
clarity of her playing of Beethoven’s Concerto proved. 
Iler technique has been quite equal to all the tests 
to which it has been put, but her chief assets are a tone 
that is warm and powerful yet is never urged beyond 
the correct limit, and temperament which apparently 
delights equally in the great conceptions of Bach and in the 
technical tricks of Wieniawsky. F. B

MICHAEL ZACHAREWITSCH 
It is apparently with players as it was during the late war 
with belligerents. No sooner has one side evolved a new 
trick than all others feel bound to follow suit. Once well- 
known resident performers were content with one recital. 
Now the fashion seems to have changed, and recitals are 
given in series. Mr. Michael Zacharewitsch’s series of

WoopsripGE.—This little Suffolk town held its first 
Competitive Festival on April 25. The instrumental side 
Was especially promising, with several chamber music parties 
and a capable full orchestra. A packed audience attended 
the winding-up concert, and the keen public interest and 
evident enjoyment of all concerned is the best of auguries 
for the future. Mr. Harvey Grace judged

WORCESTERSHIRE.—This Festival was held at Malvern, 
April 24-26. An outstanding feature was the plain-song 
singing, which Father Anselm judged. <A psalm, a hymn, 
the Puer natus est Introit for Christmas Day, and the 
Sequences were set as tests. Nearly all the choirs competing 
gave a beautiful treatment of free rhythm. The winners, 
Mr. A. Shaw’s Newland Boys, gave some highly perfected 
unison singing ; and, except for an outcrop of hooty tone at 
times, it is to be questioned whether more finished examples 
of this genre are ever likely to be met with. Father Anselm 
specially commended them for the observance of a duly 
adequate pause between the lines of the text. The solo 
competitions for female voices proved a disappointment, and 
the competitions for men’s voices still more so, for only one 
singer turned up. Poor production marred most efforts, 
suggesting the need for capable tuition in the country 
districts. The principal adjudicator, Mr. Julius Harrison, 
on the other hand, found much to commend in perform- 
ances of Mendelssohn’s D minor Trio, and the first (Quartet 
of Beethoven’s Op. 18 in the chamber music classes, A 
class for soprano solo and string quartet, in which Boughton’s 
Mother Mary was prescribed, proved less happy through the 
lack of unified expression between singers and players. 
The Women’s Institute Choirs rather lacked virility; they 
had rounds to sing among their tests, but failed in Z?mothy 
Tippen’s Horse to give full play to the sense of fun. The 
singing, however, had earnestness of a kind, and was often 
beautifully blended. In a male-voice choir competition, 
Mr. L. Gauntlett’s Malvern Wells choir furnished good 
technical and interpretative work. A Children’s Day had, 
among its incidents, folk-dances, action-songs, and choral 
competitions, and culminated in a performance by the united 
children’s choirs of Bernard Johnson’s Dream IVehs, Mr. 
Harrison conducting. Miss Chorley won the conductors’ 
competition on the closing day ; the competition for Village 
Choral Societies showed a marked advance on last year’s 
standards, Combining in the evening for a performance of 
Handel’s Z’ Allegro, under Sir Ivor Atkins, these village 
choirs gained enormously by association, and a really enjoy- 
able interpretation resulted. G. W

OTHER COMPETITIONS

BepFORD.—The Musical Society gave a very successful 
performance of Zhe A/fostles in the Corn Exchange on 
May 17, under the conductorship of Dr. H. A. Harding. 
This was the second performance of the work within the 
last twelve months, and the venture was amply rewarded. 
There was a crowded audience. The soloists were Miss 
Elsie Suddaby, Miss Dilys Jones, Mr. John Adams, Mr. BRISTOL. — The Co-operative Society’s choir and 
Frederick Woodhouse, Mr. George Parker, and Mr. Harold | orchestra, numbering two hundred performers, were 
Williams. Band and choir numbered two hundred and sixty | on April 14 conducted by Mr. A. F. Lawrence, when 
performers. | they gave Schubert’s Song of Miriam. Unaccompanied

xIRMINGHAM AND Dustrricr.—The City Orchestra | Patt-songs included Beale’s Harmony and Walmisley’s 
completed its winter activities by giving a series of Sunday Music all-powerful. The orchestra played a Haydn 
evening concerts in the Town Hall during April. At the | Symphony. Chew Magna United Choral Society, formed 
first of these, Sibelius’s Symphony No. I was given, and | last winter, now numbers sixty voices under the direction a 
though its idiom is unusual it proved greatly to the liking of | Mr. W. J. Hutchings. At its first concert, on April 18, 
the audience. Mr. Appleby Matthews left the conductor's | the programme included 7%e Ancient Mariner, a Handel 
desk to play the solo part in Bach’s D minor Clavier | Chorus, and glees.——At the April meeting of the Mendip 
Concerto. His reading was clean and rhythmical, though | Musical Club at Shipham, the works performed included two 
there was a tendency to over-finesse with the pianoforte tone, | Trio-Sonatas by Corelli, in B flat and E, Beethoven's String 
——On the following Sunday ‘the’ birthday was celebrated | ‘2uartet, Op. 18, No. I, and a Sonata in D minor by Gade. 
by a Shakespeare programme. Tchaikovsky’s Romeo and| BuUDLEIGH SALTERTON.—The Musical Society, con- 
Juliet, and the exquisite love scene from Berlioz’s dramatic | ducted by Mr. H. Fowler, performed Phaudrig Crohoore on 
Symphony on the same subject were given. The latter} April 19. The choir sang part-songs by Elgar, Edwards, 
had rather a downright and insensitive performance, but the | Farmer, Parry, Lloyd, and Gibbons, and the orchestra 
Tchaikovsky work was exceedingly well played. ——At the | played Elgar’s Wand of Youth, a Suite by Purcell, and the 
final concert M. Zacharewitsch was the soloist in| 4//egro Vivace from the Jupiter Symphony

Beethoven’s Violin Concerto, and Miss Edna Iles in| 
Rachmaninov’s D minor Pianoforte Concerto. A novelty | 
was the Prelude to Orsmond Anderton’s music-drama | 
Baldur, a richly-scored piece of writing with a certain = , as : 
bigness of idea. nner vor He the City Orchestra has had a | Mr. T. J. O’Lemy. vines eg ag an ena Wien 
successful season, though it has added considerably to its | °° May 4, ae part-songs by Byrd, “ae Sa = 
debit balance. Its losses, however, have been chiefly on the | Julius Harrison, and Hubert Parry. _Mr. Albert 3 reser 
less ambitious ventures. These are to be curtailed next and Mr. William Murdoch played Sonatas for violin and 
season, while the number of Symphony concerts by the full | pianoforte. With the object of aS a= 
cochesten is to be inceensed. Of these four ase to be orchestral work, Mr. Herbert Ware’s Orchestra of bits 
conducted by Mr. Eugéne Goossens, Mr. Appleby | Performers —_ at a lecture-concert in Cory re or 
Matthews—the orchestra’s musical director and general| ~)*Y > Mr. W. H. Reed _ =e tecture, oi arn 
conductor—taking the remainder in addition to the concerts | played _ included eethoven s Prometheus and - 
in its other series. Two concerts in the latter wecks of | Symphony and a Suite by Elgar. : At Ton Pentre, o 
April brought to an end the present series of Mid-day pro- oo 7-9, Parry’s oratorio Joseph was staged by the ripe 
grammes. At one Mr. Johan Hock gave a recital of ’cello| * ondda Operatic Society, the title = being playe ra 
music to a large audience. At the closing concert Miss | 5°" by Mr. David Harry, and that of Pharaoh by Mr. Jobt 
Sotham, to whose enterprise the scheme owes its | Broad, a singer sixty-one years of age

existence, played very beautifully the solo part in Bach’s CHUDLEIGH.—The Choral Society sang F. Cunningham 
D minor Pianoforte Concerto; a capable orchestra, | Woods’s historical cantata Aing Harold, on April 19, cot 
conducted by Mr. Hock, supplied the accompaniment. At} ducted by Mr. G. M. Coulson

and viola, Mozart’s Trio for pianoforte, clarinet, and viola, 
and pianoforte music by Scriabin (Prelude, Op. 11, and 
Etrangeté, Op. ©3) and Palmgren (Might in May).—— 
On April 18, Grieg’s Pianoforte Concerto was played in 
St. Michael’s Church by the Rev. W. G. Lees, with Mr. H. 
Treneer at the organ. This followed a precedent set in 
the Cathedral in March, when the Rev. W. G. Lees played 
the Schumann Concerto, with Dr. Ernest Bullock at 
the organ

HARROGATE.—Mr. Stanley Kaye (Sheffield) was the 
soloist in MacDowell’s Pianoforte Concerto, Op. 23, at the 
Symphony Concert in the Royal Hall on April 19, when 
Mr. Howard Carr also conducted Beethoven’s first 
Symphony, Tchaikovsky’s Mozarteana, some Coleridge- 
Taylor, and the Dox Grovanni Overture. ——The opening 
Chamber Concert of the season took place in the Royal 
Hallon April 20, the programme including Beethoven’s 
String and Wind Septet.——Bach’s Arandenburg Concerto 
No. 2, in F (Mr. A. Tomlinson playing from Mottl’s 
arrangement of the high trumpet part), and Schubert’s 
fourth Symphony in C minor (7%e 7ragic), were in the 
truly generous programme on April 26,.——On May 3, Mr. 
Carr gave gratifying readings of WHaydn’s Surprise 
Symphony and Zhe Spinners from Gabriel Fauré’s music 
for Peliéas and Mélisande. Norman O’Neill’s Valse M/ignonne 
for a quintet of violin, oboe, horn, ‘cello and harp 
received its first concert performance.——May I1 brought 
Moszkowski’s Pianoforte Concerto in E major, with Miss 
Helen Guest, of Sheffield, as soloist

HUDDERSFIELD.— Béla Barték gave a pianoforte recital 
at Highfield Hall on May 9, when, besides some Scarlatti 
and Debussy, he played his own second Elegy, Theme with 
Variations, Bear Dance, a Dirge, three Burlesques, and the 
Sonatina

Leeps.— Mr. Julius Harrison conducted Holst’s Beni 
Mora Suite (first performance at Leeds) at the concluding 
Saturday Orchestral Concert on March 19. Mr. William 
Murdoch played Delius’s Pianoforte Concerto, and the 
Symphony was Dvorak’s New World.——Pudsey Choral 
Union sang Parry’s St. Cecilia’s Day and Bath’s Wedding 
of Shon Maclean on March 19.——Leeds Parish Church 
Choir gave a recital of Tudor polyphonic music at the 
University on March 19.— -Armley Choral Society gave 
Brahms’s Reguiem on April 10,——For its concert on 
April 11 the Leeds New Choral Society selected Auéla 
Khan and A Tale of Old Japan. Mr. Turton, returning 
after a serious illness, conducted both works, as well as 
Mendelssohn’s Aérides Overture and the Unfinished 
Symphony. — —A fine programme ranging from Sumer ts 
scumen in to Bach’s Sanctus in D and Parry’s Blest Pair of 
Sirens, via madrigals by Byrd, Weelkes, Morley, and 
Gibbons, was given by the Leeds Philharmonic Society, 
conducted by Dr. E. C. Bairstow, on April 14.——Miss 
Phoebe Moore’s vocal recital at Pudsey on April 19 covered 
the work of modern British composers, including Granville

Bantock, Cyril Scott, Frank Bridge, Peter Warlock, and 
Malcolm Davidson. Mr. Norman Stafford conducted 
the Calverley Choral Society on April 23 in Somervell’s 
/ntimations of Immortality.——At Leeds University, on 
April 24, the Huddersfield Ladies’ String Quartet played 
Beethoven’s Op. 18, No. 4, and Mozart’s (Quartet No. 17, 
in C.——Before the Yorkshire section of the Incorporated 
Society of Musicians, Mr. H. Percy Richardson played 
pianoforte works of twenty-five different composers. —— 
The Edward Maude String Quartet performed Beethoven’s 
‘Harp’ Quartet and Glazounov’s /n/er/udium in modo antico 
on Ascension Day, in Leeds Parish Church, when Dr. A. C. 
Tysoe played Harold Darke’s Choral Fantasy on Darwell’s 
148/i and some Guilmant organ music. The five-part 
Tudor Motet, Zhe Lord ascendeth (Peter Philips), was sung 
by the choir

LIVERPOOL.—Before the British Music Society, on April 
13, Mr. Lionel Tertis, giving a viola recital, played Zhe 
Dance of Satan’s Daughter (Rebikov-Tertis), the Romance 
by B. J. Dale, and Sonatas by McEwen and Rachmaninov. 
——On April 20, Miss Muriel Herbert gave a recital of 
her own compositions at the Sandon Studios. The principal 
item was a Violin Sonata, the last movement of which was 
a Rondo in canon. A Légende for violin, and several 
songs, were performed.———-On April 21, a memorial to 
the late Harry Evans, first conductor of the Welsh Choral 
Union, was unveiled by Sir J. Herbert Lewis in Smithdown 
Road Cemetery. Members of the Union sang appropriate 
part-songs at the ceremony, conducted by Mr. T. Hopkin 
Evans. Mr. Arnold Dolmetsch’s chamber concert on 
May I included an Oboe Sonata by Handel, a Fantasy for 
five viols by Jenkins, and a Suite for gamba by Marais.—— 
The London String Quartet were the performers at a free 
concert at the Bon Marché’on May 3, and played Mozart’s 
Quartet in D minor, the Debussy Quartet, and two 
movements from a Quartet by Tchaikovsky

1923 433

Musical Wotes trom HMbroad 
AMSTERDAM 
At the time of writing a series of Beethoven concerts is

in full swing at the Concertgebouw. The

Concertos and other orchestral works. 
rarely-heard works of Beethoven were played on May II

viz., the Overture and the Ballet music from Prometheus

NEW YORK

Last year, at the close of the season, Mengelberg gave 
two performances of Beethoven’s ninth Symphony to} 
commemorate the eightieth anniversary of the founding of 
the Philharmonic Society, and this year he chose the same 
for his last concert. Last year the Oratorio Society 
sang in the Finale. This year the singers were Kurt 
Schindler’s Schola Cantorum, a vastly superior choral 
organization, of better material and better training. It was 
an exceptional performance, a Hymn to Joy sung in an| 
exalted mood. Little praise, however, can be given to the 
soloists. Why are opera singers chosen for such works ? 
The Symphony was preceded by Bach’s second Orchestral 
Suite, in B minor, for strings and flute. The seven 
movements were superbly played by the strings, and fairly | 
well by the choir of eight flutes

Mr. Stokowski did not end his season with such classics | 
as Bach and Beethoven, but announced for his last concert | 
but one Schinberg’s Aammersinfonie. Does Mr. Stokowski | 
think Carnegie Hall too crowded for the performances of 
his Philadelphia men, or does he want to decrease the

work

ROME

An exceptionally busy month opened with a concert given 
at the Augusteum by the two Swiss choral societies 
Caecilienverein and Liedertafel, which, under the direction 
of Fritz Brun, recently performed Bach’s Jesse solennelle. 
The concert was enriched by the performance of clavi- 
cembalo music by Wanda Landowska, and the programme 
included Mozart’s /ztaniae de Venerabili Altaris Sacramento, 
for choir, solo, orchestra, and organ ; an O/d English Dana 
by Purcell; Beethoven’s Zlegiac Song; and _ Bach's 
Maenificat

number of subscribers? Perhaps the Chamber Symphony is 
not the worst of Schonberg’s compositions, but it is so bad | 
that there is not a good word to be said for it. New York 
audiences are not prone to hiss, but while there was some | 
applause for this freak composition, there was distinct | 
hissing also. Perhaps it is cruel to suggest that maybe | 
the applause came from a claque, and that the hissing better 
expressed the real feelings of the audience. At Mr. | 
Stokowski’s last concert he partly redeemed himself by | 
giving a fine performance of Liszt’s Faust Symphony, a| 
work admittedly having vilifiers as well as admirers, but | Nor indeed was the public much more tender with Coates’s 
which shone like a gem in comparison with Schinberg’s| own symphonic poem, 7%e Hagle. At his second and last 
inanities. concert Coates presented two novelties for Rome, . viz

The Beethoven Association has just closed its fourth | Vaughan Williams’s Zondon Symphony and a_ Suite, 
season with the appearance of the most remarkable group | Acguare/li, of Santoliquido

The Swiss visitors were succeeded by Mr. Albert Coates, 
who is now beginning to be looked upon as necessary to an 
Augusteum season, and who is ever received with remarkable 
|enthusiasm. Not that this prevents the Roman public from 
passing severe judgment on some of the works presented, 
for at his first concert a divided reception was given 
to the Aallata for two pianofortes and orchestra by the 
young American composer, Leo Sowerby, who lives in the 
American Academy, and whose new work Coates ‘ baptised

of professionals ever heard on our concert platforms, | Arthur Bonucci, who is amongst the first violoncellists of 
Beethoven’s Quartet in E minor was played by Jascha| Italy, gave two concerts the following week, and then came 
Heifetz, Hugo Kortschek, Albert Stoessel, and Felix | the eagerly awaited visit of Richard Strauss, whose presence

at Rome signified something more than merely the visit of 
a celebrity. Rome, in fact, has been the battle-ground—or 
one of the battle-grounds—of the music of Richard Strauss, 
and a battle-ground where, after much failure and defeat, 
that music has gradually come to be understood, accepted, 
and appreciated. " Thus it was less the music than the 
personality of the composer which drew enormous crowds to 
the Augusteum for the three last concerts of the season. 
The five tone-poems that he conducted had all of them 
received enthusiastic approval under other batons before 
they were acclaimed under the composer himself

At Santa Cecilia a visit from Maurice Ravel also raised 
great interest among the Roman public, who gave him4 
hearty reception, which, however, was aptly described as 
‘a duty,’ more perhaps than as an expression of sincere 
conviction. As a fact the music of Ravel, and particularly 
his later work, has not yet succeeded in convincing Rome 
—we doubt whether it ever will

Amongst other concerts at Rome this month particular 
mention is due to a ‘ Beethoven Day,’ organized by the 
Giornale della Donna for the working-class public, and 
given in the Roman College on May 6. Alfredo Tazzoli

Salmond. Brahms’s A major Violin Sonata was played by 
Heifetz and Dohnanyi, the pianist making his part of equal 
value to that of the violinist. In Brahms’s Concerto for 
three pianofortes with string accompaniment, the soloists 
were Myra Hess, Harold Bauer, and Dohnanyi, while 
Walter Damrosch directed the small orchestra with Heifetz as 
leader

The Beethoven Association, formed by Harold Bauer, 
attracts the best artists in the world, who appear there for 
the love of their art. The selections offered are from the 
best compositions in the realm of music. Every seat in the 
house is sold at a high figure, and all the money taken is 
devoted to some worthy purpose. It may be remembered 
that it was funds from this Society that made possible the | 
publication of Krehbiel’s translation of Thayer’s Zz/e| 
of Beethoven

The Friends of Music announce ten concerts for next 
season, instead of only six—this year’s figure. They are prone 
to expound the classics, and also to explore them for 
comparatively unknown works. Of course this Society 
gives some modern things, most of them quite worth while, 
but unfortunately Bodanzky, like Mengelberg, persists 
in thinking that Mahler was a great composer, and|a noted Piedmontese pianist who was in London three 
that the public, however unwilling it may be, must listen to | years ago, collaborated with the violinist Corrado Archibugi 
him, The subscribers to the Friends of Music have heard | in a programme including three Sonatas

conceptions. The Philharmonic Orchestra played, and 
Wiengartner conducted. 
The second rarity (heard at a concert of the

Schubertbund ’) was Beethoven’s incidental music to a 
tragedy entitled Leonore Prochaska, written in 1814 by a high 
Tussian government official named Leopold Drucker. The 
play deals, in strongly patriotic vein, with a historical 
incident from the Napoleonic war. Beethoven’s music 
Consists of a chorus, a romance, melodrama and a closing

The music

j It is an unpublished work, the manuscript of which has 
| recently been discovered in the archives of the Austrian 
| National Library at Vienna. Rudolf Kolisch gave the first 
production of the Concerto under the baton of Anton von 
Webern

A hitherto unknown Symphony in F minor by Anton 
Bruckner has been performed at the beautiful old monastery 
of Klosterneuburg on the Danube, near Vienna, It was 
composed in 1863, before Bruckner fell under the spell of 
Wagner, and is strongly influenced by Beethoven and 
Schumann

NEW WORKS

concerts, as it were, of three different conductors seemed to 
corroborate this opinion, Of the three conductors, only

Hans Knappertsbusch, at present general musical 
director of the Munich Opera, was a newcomer to 
Vienna. His success with Beethoven’s Zroica Symphony

was vociferous, owing, it appears, chiefly to his original 
of conducting without seeming so much as to 
move arms. Such restraint, ostensibly calculated to 
centre the attention of the audience on the music, instead of 
on the leader, indeed tends to achieve the opposite effect, 
and, while it impresses the layman, it makes the judicious 
grieve. Withal, Knappertsbusch, who is quite a young 
man, seems to be a conductor of more than ordinary energy 
and vitality. His competitors are Clemens Krauss, for two 
years a favourite leader at the Staatsoper, and Ernst Kunwald, 
who, after many years spent in America and Germany, 
returned to his native city to conduct a Beethoven 
programme and, as a novelty, the thirty years’ old Symphony

method 
his

