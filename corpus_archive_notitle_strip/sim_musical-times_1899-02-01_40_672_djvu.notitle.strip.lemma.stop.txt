sympuony , . 4 , F minor < ee ee Tschaikowsky . 
 PIANOFORTE CONCERTO , d minor ae ee -- Rubinstein 
 Herr ZwWINTSCHER 

 OveERTURE , " Egmont " Beethoven 

 vocalist — Miss LUCILE HILL 

 MISS MARIAN JAY ( Solo Violinist ) 
 medalist Certificate , R.A.M. 
 ( Pupil Emile Sauret ) . 
 concert , home , & c 

 Wieniawski single Concerto Miss Jay display good deal 
 virtuosity , Beethoven sonata D ( op . 12 ) genuine artistic 
 feeling manifest . tone rich style considerable 
 breadth , day efficient violinist continually 
 come forward success matter certainty . ”—Times 
 ( London ) , November 25 , 1898 

 Address , 5 , Hollywood Road , South Kensington ; , W. Adlington , 
 224 , Regent Street , W 

 technique , especially pianoforte , 
 possess interesting memento early 
 year hard work key 
 pianoforte , ivory case 
 wear right wood . organ 
 hall , limit resource 
 manual , need skilful manipu- 
 lation produce effect operatic overture 
 similar composition — composition 
 absolutely necessary lighten 
 programme suit popular audience . Riseley 
 doggedly determine beat . 
 play , spend nearly 
 money receive recital pay 
 blower ! success attend effort , 
 proof thereof follow notice 
 early recital appear Western 
 Daily Press ( Bristol ) October 18 , 1869 

 programme include selection Handel , 
 Beethoven , Hesse , Haydn , Bach , Rossini , Sir 
 Michael Costa , arrange judiciously 
 view produce antithetical effect 
 overlook prepare programme . performance , 
 judge applause , rise encore 
 pitch , appear great satisfaction afford 
 real enjcyment audience , numerous . 
 probably person present surprised learn 
 capacity organ . majority 
 public know organ , rule , 
 hear place worship attend ; 
 instance hear accompaniment play 
 inferior performer , little idea vast 
 capacity , infinite variety , tremendous power , 
 extreme delicacy . hear play 
 time astonish massiveness 
 volume sound vocal power interpret 
 high class music . introduce instru- 
 ment city public performance supply new 
 pure source enjoyment 
 appreciator . recital deserve obtain 
 support 

 COLSTON HALL RECITALS 

 MUSICAL TIMES.—FeEsruary 1 , 1899 . 85 

 society lady play 
 orchestra far Bristol concern . " 
 1877 start carry 
 risk Monday Popular Concerts . concert 
 orchestral , educational value 
 Mr. Riseley organ recital apparent 
 fact listener , having gain 
 familiarity movement 
 hear play Colston Hall 
 organ , experience additional enjoyment 
 follow music intelligent interest . 
 " audience ready symphony . 
 play Beethoven ' , 
 sure prove great educator . 
 carefully nurse english music 
 concert , composer come 
 fom London specially conduct 
 work . receive penny 
 local authority expense — 
 fact , 1 considerably pocket . think 
 duty municipality 
 subsidise orchestral music . 
 work away 

 BRISTOL CHORAL SOCIETY 

 quality impress 
 bring personal contact Mr. 
 Riseley thoroughness enthusiasm . 
 illustrate , 
 conscientiousness , mention 
 occasion visit , 
 play concerto day , 
 perfectly familiar note , 
 know sit till small hour 
 morning practise difficult passage 
 pedal piano house . 
 minute attention detail 
 notice , 
 , conduct orchestral rehearsal . 
 study score thoroughly 
 know exactly want , 

 understand . 
 remarkable enthusiasm 
 good , matter style . 
 large library orchestral music , jg 
 equally home symphony Beethoven , 
 Brahms , Dvorak , suite Massenet 
 Délibes , Wagner selection . unlike 
 fine orchestral conductor , Mr. Riseley js 
 successful chorus - master , 
 attend Bristol Festival 
 hear magnificent male - voice choir 
 testify . fine organ playing 
 remarkable remember 
 begin work instrument 
 late period life attain 
 distinction 

 personal character 
 unbecoming . know 
 highly appreciate 
 honesty purpose warmth 
 heart 

 Wednesday morning : Te Deum ( Dvorak ) , ' ' Coronation " 
 Mass ( Liszt ) , new Symphony Mr. Elgar , 
 " Requiem " ( Brahms ) . Wednesday evening : ( concert 
 Public Hall ) miscellaneous , include instrumental 
 work Mr. Elgar Wagner selection 

 Thursday morning : ' Hora Novissima " ( Parker ) , 
 ' * Stabat Mater " ' ( Palestrina ) , Symphony ( Beethoven ) , 
 " judgment " ( Spohr ) . Thursday evening : 
 " God time good " ( Bach ) , ' * bl pair Sirens " 
 ( Parry ) , " hymn praise " ( Mendelssohn 

 Friday morning : ' ' Messiah 

 Majesty QUEEN command per- 
 formance Mendelssohn ' ' hymn praise " 
 Windsor Castle , 2oth inst . 
 chorus consist Windsor Eton Madrigal 
 Society orchestra 
 Majesty private band . performance 
 place St. George Hall , order 
 organ , essential feature Men- 
 delssohn score , use occasion . Sir 
 Walter Parratt , Master Queen Music , 
 conduct 

 Philharmonic Society complete 
 arrangement approach season , 
 follow chief feature 
 attractive scheme : Beethoven " Choral " Symphony 
 perform 18 , chorus 
 consist 150 member Leeds Festival Chorus , 
 - voiced choir sing Sir Hubert 
 Parry ' Blest Pair Sirens . " Professor Stanford 
 conduct performance " Concert 
 variations english theme " pianoforte 
 ( Mr. Leonard Borwick ) orchestra ; M. Alexander 
 Glazounow discharge similar duty new 
 Symphony ( . 6 ) C minor ; likewise , Herr 
 Richard Strauss Tondichtung ' Tod und Ver- 
 klarung " ' Mr. F. H. Cowen performance 
 Concertstiick b flat , M. Paderewski 
 solo . M.S. Rachmaninoff ( appear- 
 ance England ) play new Pianoforte 
 Concerto sclo , Mr. S. Coleridge - Taylor 
 Orchestral Ballade minor scheme , 
 far include Mr. German Overture 
 " * Ado , " Mr. B. Luard Selby 
 Idyll small orchestra , — , — 
 Sir Alexander Mackenzie Orchestral Ballad ( com- 
 pose Philharmonic Society ) , ' ' La Belle Dame 
 sans Merci . " orchestral work 
 acknowledge excellence duly present , 
 soloist sufficient guarantee 
 reputation Philharmonic Society 
 respect . seven pianist violinist 
 announce perform , 
 votary keyboard instrument 
 favourite . Sir Alexander Mackenzie occupy 
 accustomed place conductor 

 Feis Ceoil , Irish Musical Festival — 
 annual event kind — announce 
 hold Dublin 15 20 inclusive . 
 musical competition , substantial prize , 
 hold — e.g. vocal ( choral , church choir , 
 solo voice ) instrumental , include 
 orchestra , wind band , string quartet , variety 
 solo performer . special competition national 
 music instrument intimate . 
 sixteen prize — range £ 30 
 £ 1 — offer composer , advantage 
 . new feature propose 
 exhibition musical MSS . , rare edition irish 
 music , portrait , musical instrument , & c. , 

 Mr. E. Sivas author little pamphlet 
 entitle ' ' Accidents stave matter " 
 ( obtain 20 , Stanwick Road , West 
 Kensington ) . isin chapter , 
 entitle ' ' Chapter Accidents . " 
 specimen Mr. Silas racy style critical 
 acumen 

 prejudiced people find trace upset ink- 
 bottle Beethoven score , undoubtedly 
 affirm mean . autograph score 
 Handel " Messiah " ( Buckingham Palace ) , 
 page large circle , evidently cause pot 
 saucepan . , respect greatly score 
 " Messiah , " respect saucepan , 
 think obvious circle 
 intend illustrious composer . author 
 immaculate , mortal liable mistake 
 carelessness , hear year Paris 
 Pastoral Symphony ( Beethoven ) play important 
 bar miss viola ; Fifth Symphony 
 bar " Scherzo . ’' half - century , 
 , composer quartet 
 continually perform horrible cacophony , owe 
 second violin shift bar 
 proper place seventeen bar ! quartet , 
 posthumous work , leave 
 hand careless ignorant corrector . curious 
 circumstance good violinist play 
 state , hideous noise ascribe Beethoven 
 eccentricity ! - mistake 
 rectify 

 article find column Dr. 
 Arne " Caractacus . " note connection 
 subject Sir Henry Rowley Bishop 
 write ' Grand Ballet Caractacus , " 
 produce Drury Lane April 22 , 1808 , , 
 accord title - page vocal score , 
 " * unbounde applause . " wind , way ; 
 ' Grand Processional March . " ' 
 account Times following day . 

 SATURDAY popular concert . 
 RaBi QuaRTET E FLaT ( op . 1 

 Tue Saturday afternoon Popular Concerts St. James 
 Hall resume 7th ult . , concerted 
 work Beethoven Quartet F ( Op . 18 , . 1 ) 
 Brahms Quintet F minor ( Op . 34 ) . executant 
 Lady Hallé , Messrs. H. Inwards , Gibson , P. 
 Ludwig , Mr. Leonard Borwick pianist Mr. 
 Kennerley Rumford vocalist 

 distinction concert follow 
 week addition repertory Walter Rabl 
 Quartet e flat ( Op . 1 ) pianoforte , violin , clarinet 
 ( viola ) , violoncello . work hear 
 London Mr. G. A. Clinton chamber concert April 1 
 , Queen ( Small ) Hall . great merit , 
 source inspire theme 
 difficult trace . , , Herr Rabl 
 - - , model great 
 master , indebtedness promising sign 
 . work gain prize competition 
 arrange Gesellschaft der Musikfreunde Vienna , 
 Brahms judge , eminently 
 pleasing nature ; pleasing , , audience 
 14th ult . think movement , 
 repetition demand grant . little 
 charm work owe clearness design 
 spontaneous flow music , , , 
 pretentious . admirably interpret Mr. Leonard 
 Borwick , Lady Hallé , Herr Mihlfeld , Mr. Paul Ludwig . 
 famous clarinettist subsequently hear 
 Brahms quintet b minor ( Op . 115 ) Weber 
 Duo Concertante e flat ( op . 48 ) , 
 associate Mr. Borwick . - 
 fine performance Mozart rarely - hear sonata D 
 ( Op . 21 ) . Miss Lucia Fydell vocalist 

 Andante variation write 
 Mozart 1786 toy clock musical box 

 second recital , 23rd ult . , programme 
 contain arrangement Fantasia 
 F minor , compose Mozart 1791 musical box , 
 Beethoven Sonata e flat ( Op . 81 ) , Mendels- 
 sohn ' ' Lieder ohne Worte . " scarcely necessary 
 add Mr. Borwick playing occasion 
 distinguish exquisite finish 

 Miss Madeline Payne , progress Guildhall 
 School Music watch interest 
 time past , pianoforte recital roth ult . , 
 Salle Erard . Miss Payne , , 
 choose large room , seventeen , 
 touch style powerful brilliant 
 appreciation reading Chopin ballade flat 
 ( Op . 47 ) Brahms " variation fugue theme 
 Handel " impossible , resolve 
 aural assault aggressive nature . Miss Payne , 
 , sympathy Grieg sonata 
 G minor ( Op . 13 ) , assist M. Johannes 
 Wolff , render Allemande Gavotte D’Albert 
 great delicacy 

 ar meeting Musical Association 17th 
 ult . , hold Royal College Organists , Mr. Henry 
 Davey read paper " Giovanni Pierluigi , da 
 Palestrina 

 refer english flemish composer 
 music precede contemporary Palestrina , 
 polyphonic school fifteenth 
 sixteenth century reach culminate point 
 work great italian church composer , Mr. Davey 
 review length chief event Palestrina 
 life , contradict statement 
 Agazzari Baini , particularly regard famous 
 " Missa Pape Marcelli . " . Mr. Davey favour Haberl 
 opinion Giovanni Pierluigi bear 1526 , possibly 
 1525 , Palestrina , 
 commonly know , live - 
 year . lecturer contradict statement Grove 
 Dictionary Palestrina poor man , dismiss 
 mythical Baini story composer having specially 
 write masse preservation music 
 roman Catholic Church . Palestrina 
 marry twice , second time , seven month 
 death wife , widow large dealer 
 fur . spouse , , possess property 
 appreciable value , emolument receive 
 office form comfort- 
 able income . masse , include ' " ' Missa Pape 
 Marcelli , " submit Palestrina 
 committee appoint Council Trent suppress 
 abuse creep music Church , 
 declare write year . Mr. 
 Davey insist intention 
 dismiss music entirely Church service , 
 authorise style find work furnish 
 model serve guide composer . conclusion , 
 Palestrina describe great 
 composer time , worthy rank 
 Handel , Bach , Mozart , Beethoven 

 subsequent discussion Mr. W. H. Cummings , 
 occupy chair , great glory Palestrina 
 having eliminate secular association 
 Church music . Sistine Chapel Pales- 
 trina music sing unaccompanied , think 
 place probable voice 
 support instrument ; regard 
 opinion express Mr. Davey accidental 
 introduce acceptable modern ear , 
 Mr. Cummings strongly object . support 
 Mr. Otto Goldschmidt , refer perform- 
 ance London ' * Missa Pape Marcelli " 
 Bach Choir 1881 

 Miss Ethel Holmes ( Elsie ) , Miss Alice Lakin } | . ; : 
 ‘ Ursula ) , Mr. Charles Manners ( Prince Henry ) , Mr. Dan Vignoles . [ Simpkin , Marshall Co. , Limited 

 Gn , member Society ( Forester ) , Mr.| Tue late Sir Robert Stewart musician comparatively 
 Watkin Mills ( Lucifer ) . work the| little known border native Erin , 
 orchestra , consist player Birmingham | biographer record ' ' remarkable 
 Potteries , play Beethoven ' ' Egmont " Overture . Dr. | - round musician Ireland hitherto produce , 
 Swinnerton Heap , connect } Balfe high place dramatic composer . " 
 society commencement , conduct | Stewart life particularly eventful . bear 
 know ability distinction . 1825 , chorister Christ Church Cathedral 

 Dublin , , age nineteen , organist , appoint- 
 ment retain till death 1894 . self - teach , 
 possessor remarkable gift extemporizing 
 extraordinary memory music , Stewart live life 

 Tue motto young Society ' * wach ' auf , " | 
 chorus " Die Meistersinger " bear title | 
 open second concert present season | 
 Public Hall , Worcester , 7th ult . Goetz | 
 cantata ' * Noenia ’' follow , second 

 programme include Beethoven " ruin Athens , " | chiefly know organ work 

 - song lady ’ voice Brahms , daintily sing , 
 carol , ' holly ivy , " orchestrate Mr. | 
 Edward Elgar — effective performance , choir 

 104 MUSICAL TIMES.—Fesrvary 1 , 1899 . 
 heart . ' Haydn visit England ( 1791 | song little demand vocal ability , 

 1795 ) eighteen month ’ duration , fact 
 hardly agree ' year spend Haydn 
 London . " _ Beethoven famous septet " 
 string ’' , Moscheles Secretary 
 Philharmonic Society . Mendelssohn marry 
 year 1837 , 1836 , " ' Elijah ' perform 
 London time , , spring 
 1847 . novel welcome feature book 
 comprise fac - simile musical notation 
 ' ' maker music , " original 
 British Museum , library author 
 gift - book officer 

 Mr. Hathaway produce somewhat severely 
 analytical book Mendelssohn organ work . ven- 
 ture think , , interest 
 lessen history 
 oft - play composition King Instruments . 
 regard chorale use Mendelssohn sonata , 
 author ( p. 79 ): ' ' know 
 considerable antiquity , probably write 
 Mendelssohn . " matter fact , 
 chorale trace ; fifth sonata 
 original , uncertain . chorale 
 Sonata . 1 ' * mein Gott . " 
 Mendelssohn supply title manuscript , 
 find way print copy . Mr. Hathaway 
 curious statement regard passage 
 sonata 1 , movement , bar 22 23 . : 
 " scarcely advisable introduce pro- 
 gression examination paper , fault 
 fourrd position chord . " " ? 
 respectfully ask . ride pedagogic 
 hobby - horse little far dreary overmuch- 
 traverse examination - road ? fac - simile 
 book — d minor prelude 
 Mendelssohn autograph British Museum , , 
 , differ slightly publish version . 
 way , prelude fugue ( Op . 37 ) 
 dedicate Mendelssohn Thomas Attwood , " 
 reverence gratitude , " original german edition 
 record . unfortunately tribute respect 
 young german musician old english organist 
 hitherto print title - page english 
 edition ; add issue 
 Messrs. Novello , original publisher 
 work , year ago 

 symphony meaning . Philip H. Goepp . 
 [ J. B. Lippincott Company 

 american kinsfolk accuse neglect 
 literary music , judge 
 book issue time time ' ' Jonathan 
 continent , ' use Bilow designation . author 
 thoughtful book claim ' reverse 
 traditional . little tell life master . 
 . . « » concrete event , , noplace . 
 , believe , instead loss , 
 omission great gain personal interest , insight 
 essence master individual quality , poetic 
 character . " , reader page 
 miss characteristic charm invest writing 
 Sir George Grove , example ' " * Beethoven 
 symphony , " interesting 
 non - technical character . signification 
 word ' ' meaning " title , Mr. Goepp " 
 negative intent , strong positive . book 
 meant restrain wrong interpretation , urge 
 right . " excellent , author effort 
 listener symphonic music right road 
 goal intellectual enjoyment deserve 
 encouragement appreciation . interesting 
 list symphony analyse : 
 Haydn , d E flat ; Mozart , G minor ' ' Jupiter " ; 
 Beethoven , ' ' Eroica , " " . 5 7 ; Schubert , b minor 
 c major ; Schumann , c major " rhenish " ; Mendels- 
 sohn , ' ' italian ’' ; Brahms , . 2 

 Order Burial Dead , set music 
 Merbecke , harmonise Sir John Stainer , 
 selection suitable hymn 

 meeting member Walsall Philhar . 
 monic Union , hold 16th ult . , Dr. C. Swinnerton 
 Heap , conductor Society , present 
 handsome baton token appreciation 
 past service 

 Mr. George Halford submit attractive interest . 
 e programme fifth orchestral concert , 
 Town Hall 17th ult . , Beethoven fourth 
 Symphony receive masterly interpretation . novelty 
 consist Tschaikowsky overture , ' ' Der Woiwode , " 
 Moszkowski ballet music , ' ' Laurin , " Glazounow 
 picturesque overture , ' ' Carnaval . " Miss Louise Phillips 
 vocalist 

 recital sonata pianoforte violin 
 Masonic Hall , roth ult . , Dr. Rowland 
 Winn Mr. William Henley , following 
 perform : Bach E ; Mozart b flat ; Beethoven , 
 " Kreutzer ’' ; Brahms d minor ; Saint - Saéns , 
 d minor 

 MUSIC BRISTOL . 
 ( correspondent 

 ' Messiah . " chorister 
 singularly good account , band ( 
 Scottish Orchestra Company ) unusually fine , 
 soloist , Miss Mabel Berrey , Miss Ada Crossley , Mr. 
 Braxton Smith , Mr. Andrew Black , case 
 fully equal duty . Mr. Joseph Bradley conduct 
 know care ability 

 seventh classical concert , 5th ult . , Mr. 
 Coleridge - Taylor new Orchestral Ballade minor 
 ( Op . 33 ) attract special attention . hear 
 concern merit , Glasgow verdict 
 endorse enthusiasm September Gloucester 
 audience , opinion Mr. Taylor 
 grasp musical art attractive phase . 
 Dvorak ' ' New World " Symphony form 
 programme . ballad symphony 
 splendidly render band , baton Mr. 
 Bruch , popularity conductor steadily 
 rise . Mr. Hugo Heinz , german baritone , 
 appearance , achieve marked success , 
 Miss Mabel Berrey sing acceptance 
 popular concert 7th ult . , eighth 
 classical concert Lady Hallé return 
 scene memorable triumph . need- 
 , Mendelssohn Violin Concerto play 
 manner fully reveal surpass beauty . 
 Brahms Symphony welcome feature 
 programme . orchestra good , Mr. Bruch 
 artistic sympathy text , 
 equivalent work , favourite Glasgow , 
 superb performance . Miss Jenny Taggart , 
 singe season increase knowledge art , 
 appear " Pop " r4th ult . , 
 programme include Beethoven Second Symphony 
 acouple Brahms hungarian Dances . 
 ninth concert classical series novelty find 
 place programme : Mr. Bruch Prelude 
 opera " Hirlanda , " simple melodious bit 
 writing , Mr. Walter Petzet symphonic poem 
 " appeal happiness , " fail 
 impression . Mr. Busoni , début 
 Glasgow , essay Liszt Pianoforte Concerto , . 2 , 
 major , attain respect genuine success , 
 new - comer cleverness 
 invest work degree interest . feature 
 evening , undoubtedly , fine performance 
 accord Schubert great Symphony , . 1o , C major , 
 veritable triumph , , orchestra 
 conductor 

 series student ’ concert connection 
 Glasgow Atheneum School Music place 
 14th ult . , Cowen cantata " fairy ’ 
 spring " repeat , owe marked success 
 recent local concert . Mr. Allan MacBeth , esteemed 
 principal school , conduct , programme 
 devise occasion subject favourable 
 comment 

 MUSIC LIVERPOOL DISTRICT . 
 ( correspondent 

 tue second half - session Philharmonic Society 
 open roth ult . Symphony Beethoven 
 . 2 , D , overture Mendelssohn ' ' hebride " 
 Auber ' ' Zanetta , " familiar work , 
 needless , toy hand orchestra . 
 strong contrast stand rendering Brahms 
 colossal Concerto D minor , pianoforte 
 magnificently play Mr. Busoni . chorus 
 justify appearance singe couple part- 
 song , case 24th ult . , 
 programme mainly instrumental , 
 Liszt ' ' Tasso " ' Suite place honour . selection 
 Massenet ballet music ' ' Le Cid , " Tschaikowsky 
 ' Fest ' ? March , Mozart ' ' magic flute " Overture , 
 Mendelssohn Violin Concerto E minor , 
 play , Lady Hallé allot solo 
 - composition . Mr. Cowen conduct 

 21st ult . welcome Schiever Quartet 
 second concert present season , 

 MUSIC MANCHESTER . 
 ( correspondent 

 Hallé concert resume 12th ult . 
 break fortnight . apparently Christmas interval 
 establish Mr. Cowen quicken musical 
 appetite public , , combine announce- 
 ment Lady Hallé , consequence 
 approach tour America , unable appear 
 season , suffice attract immense audience , spite 
 extreme violence weather . programme 
 include Beethoven Second Symphony ( faultlessly 
 render ) , Brahms Violin Concerto ( D ) , Dvorak 
 ' * Otello " Overture — originally design Finale 
 symphony — Mendelssohn ' Hebrides , " _ 
 charming second theme , vocal music Madame 
 Ruth Lamb Mr. MacInnes . Lady Hallé play 
 exquisite refinement unfailing accuracy 
 characterise performance , receive 
 enthusiasm greet 
 city ; help feel Tartini " Trillo 
 del Diavolo " old - fashioned , formal , hackneyed 
 worthy great executant . 1gth ult . 
 " Eroica " ' superbly play . 
 expect equally unimpeachable rendering ? 
 Mr. Ben Davies , great taste discretion , 
 ' Salve ! dimora , " Gounod ' Faust , " 
 Brahms Lieder ; remainder programme 
 , fancy , music 
 early day . Miss Margaret Pierrepont select Mozart 
 fifth Pianoforte Concerto , stale Prelude 
 Rachmaninoff , Scarlatti study ; concert 
 commence Bach Suite b minor flute 
 string ; Beethoven great Symphony stand 
 surrounding 

 performance reception Mr. Cowen 
 ' ' ruth , " ' 26th ult . , notice necessarily 
 defer ; doubt success 
 scene composer especially 
 happy — Reapers Gleaners ( ' ' 
 pasture wilderness " ) song dance 
 harvest feast , year ago 
 Atheneum concert city . expect , 
 strength work lie purely pastoral music 
 contrapuntal writing 

 gratifying note regularly increase 
 attendance chamber recital Brodsky , Rawdon 
 Briggs , Speelman , Carl Fuchs Quartet Party . 
 18th ult . Association Hall fill audience 
 enthusiastic qualified judge merit 
 programme performance . ' opening String 
 Quartet Professor Stanford D minor fairly justify 
 selection cordially receive , form 
 movement ( especially ) 
 somewhat difficult follow hearing . 
 work principal College Music 
 delight chamber composition Brahms , 
 great variety wonderful delicacy 
 tone particularly suit ; Sonata 
 violin pianoforte ( Op . 78 ) ably 
 Mr. Dayas assemble student rest 
 satisfied repetition , mean 
 good , movement . rendering great Quartet 
 ( Op . 130 ) Beethoven veritable triumph 

 concerned . work great demand 
 executant , animated 
 absolute unity purpose . Presto loudly rede . 
 mande , attention concentrate 
 intense pathos sob tone broken 
 utterance Mr. Brodsky wail recitante 
 measure immediately precede Da capo 
 Cavatina . singer 
 audience 

 lover organ music opportunity hear 
 masterly rendering exhaustive programme 
 Dr. Corbett , Mechanics ’ Institution , 7th ult . ; 
 Mr. Stanley Hawley recital , St. George 
 Church , 13th ult . , assist Madame Jennie 
 Bentley Mr. . Downing 

 Miss Cantelo Subscription Concert Albert 
 Hall , roth ult . , interesting programme 
 present , include Brahms trio C minor ( op . 101 ) , 
 Beethoven Pianoforte Sonata F minor ( Op . 57 ) ; 
 Tartini Violin Sonata G minor , Beethoven Trio 
 B flat ( op . 9 ) . Miss Cantelo play 
 remarkable sympathy refinement , _ ably 
 second Mr. Johann Kruse Mr. W. E. White- 
 house 

 Derby " Messiah " perform Boxing 
 Day Derby Choral Union . solo 
 entrust Miss Percival Allen , Madame Marie Hooton , 
 Mr. Edward Branscombe , Mr. Montague Borwell . 
 Mr. S. Neville Cox preside organ , band 
 chorus 250 performer conduct Mr. 
 Charles Hancock 

 " Messiah " performance | Brahms Symphony play ( time 
 distinguish feature music Christmastide — especially | Yorkshire ! ) Hallé band , Mr. Cowen , 

 Yorkshire — present novelty , | 
 local interest typical | Beethoven Fifth Concerto 

 Mr. Busoni appearance county 
 deep 

 enjoyable Wednesday evening Princes ’ 
 gallery Curtius Concert Club resume 
 11th ult . , M. de Pachmann pianoforte recital 
 chief feature exquisite delicacy 
 beauty tone rendering Chopin selection . 
 concert follow Wednesday open 
 performance England String Quartet E flat 
 ( Op . 10 ) young Bohemian Ottokar Novacek , 
 pupil Dvorak . work , movement 
 good , promising , quartet 
 englishman wait hear decidedly . 
 executant Messrs. Gompertz , Haydn Inwards , 
 Emil Kreuz , Charles Ould , subsequently perform 
 Schumann quartet ( Op . 41 , . 3 ) . Madame 
 Sobrino , attractive vocalist , commend 
 singe song English German 
 Italian 

 prospectus Spring series Crystal Palace 
 Saturday concert issue . concert 
 , commence 25th inst . , novelty 
 include programme Miss Liza Lehmann 
 choral orchestral ballad ' Young Lochinvar , " Mr. 
 W. Wallace symphonic poem " Sister Helen , " Mr. W. 
 H. Bell symphonic poem " Pardoner Tale " ( 
 Chaucer ) , Scharwenka Pianoforte Concerto C sharp 
 minor , M. Jean Renard Violoncello Concertino , J. 
 Klengel Violoncello Concerto minor , Reginald 
 Steggall Suite E , Otto Manns Scherzo Capriccioso . 
 Mr. Manns benefit place 6 , 
 Beethoven ' " Choral ' ? Symphony perform 
 - seventh time direction 

 difficulty threaten opera season 
 present year successfully overcome 
 Grand Opera Syndicate , sum £ 110,000 , 
 purchase remainder lease Covent Garden 
 Theatre ( grant originally late Frederick Gye ) 
 exclusive performing right large number indis 

 11 g 

 Messrs. Joser Paut Lupwic chamber 
 concert , place 2oth ult . Salle Erard , 
 include admirable performance Brahms Pianoforte 
 Quartet ( Op . 26 ) , Beethoven trio D ( op . 8) , 
 solo respective instrument concert - giver 
 Madame Haas . Miss Louise Phillips , vocalist , 
 commend bring notice attractive 
 daintily write song , severally ' * Desdemona 
 song , " ' ' hark , lark , " ' * primrose , " ' , " 
 Joseph Moorat 

 Tue Willesden Green Philharmonic Society 
 concert High School Hall , Churchill Road , 
 18th ult . , direction Mr. Louis Robbins . 
 programme include Beethoven Symphony ( . 1 , C ) , 
 elegiac melody Grieg , Gounod overture 
 " Mirella , " Moszkowsky spanish dance , Mendels- 
 gohn " Cornelius " ? March . Mr. Louis Robbins , 
 conductor , play violin solo , vocalist 
 Madame Carrie Dulcken Mr. Maurice Aubrey 

 Tue New Year convention annual meeting 
 Tonic Sol - fa Association hold Young Men 
 Christian Association Hall , Aldersgate Street , 
 i4th ult . , chief feature lecture 
 deliver Mr. L. C. Venables ' present day ten- 
 dencie concert lesson . " discussion 
 hold ' ' music Evening Continuation Schools , " 
 Mr. C. G. Lewis introduce subject able paper 

 Tue Highbury Philharmonic Society second 
 concert present season 13th ult . , 
 programme comprise Beethoven Second Symphony , 
 Tschaikowsky ' ' Casse - Noisette ’' Suite , Spohr dramatic 
 Concerto ( solo admirably play Mr. G. 
 H. Betjemann ) , Cowen successful Leeds cantata , 
 " ode passion . " Miss Clara Butt solo 
 vocalist 

 Miss EmMMiE TaTHAM , pupil Madame Blanche 
 Marchesi , début concert 
 Hof - Pianist , Herr Lutter , Hanover , December . 
 Hanover Daily News , notice concert , 
 : ' ' Miss Tatham singer gift 
 sympathetic voice , especially rich low tone , 
 artistic performance contribute greatly 
 enjoyment evening 

 Tue Clapham Choral Society concert 
 present season 24th ult . , direction Mr. 
 Walter Mackway , chief feature programme 
 late Henry Leslie cantata ' " Holyrood . " ' 
 second concert , April 18 , programme 
 comprise Verdi ' " ' Stabat Mater , ’' Mackenzie cantata 
 " bride , " ' Spring ' ? section Haydn 
 " season 

 " Beethoven Haus " ? Society , Bonn , offer 
 prize , £ 100 £ 50 respectively , chamber 
 composition wind instrument , combination 
 pianoforte stringed instrument , competitor 
 request send score ( ) later 
 June 30 , present year , honorary 
 president Society , Professor Joachim , Berlin 

 Mr. Grorce MAcHUuTCHIN concert Steinway 
 Hall 7th ult . , programme include 
 song , duet , violin piece compose concert- 
 giver . artist assist Madame Mabel 
 Ward , Madame Minnie Shatel , Miss Kathleen Purcell , 
 Miss Alice Elieson , Messrs , Penderel Price , Montague 
 Borwell , Percy Woodgate , Mrs. MacHutchin 

 weekly lecture Wednesday afternoon 
 Royal Academy Music , February March , 
 consist discourse ( chiefly biographical ) Brahms 
 Tschaikowsky , Sir Alexander C. Mackenzie , 
 deliver 8th 15th inst . , follow course 
 lecture ' ' Pianoforte Composers subsequent 
 Beethoven , " Mr. Walter Macfarren 

 Mr. Penna lecture ' ' Tempest , " Holy 
 Trinity Rooms , Kilburn , roth ult . , Miss Elsa 
 Brettingham ( pupil Mr. Penna ) sing Ariel 
 song satisfaction large audience 
 second enthusiastically - demand 

 BayREUTH.—Some excerpt Siegfried Wagner 
 long - expect comic opera ' " Der Barenhauter " 
 produce villa ' ' Wahnfried " ' Carl Orchestra , 
 Nuremburg , Christmas holiday , connection 
 birthday celebration Frau Cosima . 
 impress audience greatly favour . 
 work , pianoforte score 
 publish Leipzig , bring 22nd ult . 
 Munich Hof - Theater 

 BERLIN.—The new Beethoven Saal , fine concert 
 hall , excellent acoustic property accommodate 
 1,100 person , inaugurate performance 
 2nd 3rd ult . occasion 
 Philharmonic Orchestra , Herr Rebizek direction , 
 play Beethoven Overture " ' Zur Weihe des Hauses " ' 
 " Leonora " Overture ( . 3 ) . second 
 concert Joachim Quartet , Mdlle . Clotilde Kleeberg , 
 artist , Philharmonic Choir , 
 Herr Siegfried Ochs , . Philharmonic 
 concert oth ult . , direction Herr Nikisch , 
 new symphonic poem , ' ' Fata Morgana , " ' Carl Gleitz , 
 produce time consider musician 
 present interesting remarkable work . Mr. 
 Eugene d’Albert pianist occasion . — — 
 Fraulein Anna Hegner , aged seventeen , sister Otto 
 Hegner , violin recital Bechstein Hall , 
 month , play , piece , 
 Beethoven Violin Concerto marked success . 
 Emmanuel Chabrier posthumous opera , " Briséis " 
 bring time stage 12th ult . , 
 Royal Opera , coldly receive . accord 
 statistical report publish , number different 
 work produce 1898 , Royal Opera , fifty- 
 . great number performance 
 accord Lortzing ' ' Czar und Zimmermann ’' — 
 viz . , - , ' ' Tannhaiiser " come 
 nineteen , total performance work 
 composer , Wagner head list seventy- 
 performance 

 120 MUSICAL TIMES.—Fesruary 1 , 1899 

 Municu.—Siegfried Wagner comic opera ' ' Der Baren- 
 hauter ’' produce time Royal 
 Theatre , 22nd ult . , receive high favour 
 audience crowd house 

 Paris — long - expect performance M. 
 Paul Vidal new - act opera , ' ' La Burgonde , " ' 
 place , December 23 , Opéra , Mesdames Bréval 
 Héglon , MM . Delmas Alvarez principal 
 , meet favourable reception . 
 libretto , MM . Emile Bergerat Camille de Sainte- 
 Croix , adaptation episode german 
 " Waltari - Lied , " action place camp 
 Attila , end death 
 hand bride , Ja Burgonde . M. Vidal score , 
 picturesque effective , somewhat wanting 
 originality .. —_—The revival , December 30 , Opéra 
 Comique , ' Fidelio , ' merit record , account 
 chiefly Beethoven opera having 
 number year . M. Gevaert recitative 
 use , work mount _ finely 
 render ofthestral portion , solo 
 interpreter scarcely equal , admit , 
 altogether easy task . Conservatoire concert , 
 8th ult . , Bach cantata ' Ich hatte viel Bekim- 
 merniss ’' time , produce 
 profound impression . programme include 
 Beethoven Symphony Berlioz ' Roi Lear " 
 Overture . interesting novelty Lamoureux concert 
 , December 25 , symphonic poem , ' La Naissance 
 de Venus , " M. Alexandre Georges , melodious 
 skilfully instrument work , , 8th ult . , series 
 song entitle ' ' Chaine d’Amour , ’' solo voice 
 orchestra , M. Bonval , work extremely 
 receive . Colonne concert 8th ult . new 
 symphonic poem , entitle ' ' Procession Nocturne , " 
 young composer M. Henri Rabaud , produce 
 time , generally consider work 
 originality power . MM . Ysaye Raoul Pugno 
 solo instrumentalist 

 Pesaro . — Mascagni engage new 
 operatic score , Signor Illica supply 
 libretto . title new work " Le Maschere . " 
 audience recently Queen Marguerite , 
 composer promise 
 produce Rome 

 study Menter Franz Lachner , Munich . 
 1874 1893 occupy post conductor 
 Frankfort Stadt - Theater . composition , 
 effectively write instrument , play 
 Crystal Palace London 

 distinguished spanish pianist , Juan B. Pujor , die 
 Barcelona , December 28 , age - . 
 tour France Germany , early 
 career , establish high reputation 
 performer instrument , especially inter- 
 preter Beethoven Chopin , meet 
 appreciation visit country . 
 look native country head 
 school modern spanish pianist , nearly 
 noted pupil 

 GeorGE F. Bristow , composer repute 
 United States , die New York December 21 , aged 
 - 

 a.d. ( Auckland).—The score mention Professor 
 Prout * instrumentation " Primer ( p. 11 ) 
 obtain moderate price Peters ’ edition 
 Messrs. Novello 

 R , Y.—It impossible musical 
 prodigy Mozart Beethoven . time 
 tell realisation possibility 

 H. J. Sr . B. C.—For volume carillon music , " Les 
 Carillons de Van den Ghyn , " H. van Elewyk , price 
 . net 

