THE TIMES

The first of two vocal recitals was given in Steinway Hall on Thurs- 
day night by Mr. Edward Iles, who has lately made great progress in 
his art. The voice, a sy mpathetic baritone of considerable power, has 
gained many of the qualities which make for effect ; and as the singer 
is a finished musician, his performance of songs of different styles is 
always pleasant to listen to. Beethoven's “ Kuss,’’ Schumann's 
‘« Gestandniss,’’ Schubert's beautiful ‘‘ Nacht und Traume, "and ‘‘ Der 
Einsame”’ are instances of Mr. Iles’s wide knowledge of the classics, 
and his inclusion of songs by Max Bruch, Eugen d’Albert, Richard 
Strauss, and Widor, proves him an artist of broad sympathies. He 
was at his best in Parry’ s splendidly vigorous Anacreontic ‘ Fill me, 
boy, as deep a draught’ and Stanford’s arrangement of ‘‘ Eva Toole”’ 
and three musicianly songs of his own composition were also given 
with much success. The first is a charmingly simple and melodious 
ballad, ‘‘ As I walked forth,” and in the third, ‘‘ Little soul of disdain” 
any intelligent singer can hardly fail to make an effect. Miss White's 
‘‘Montrose’s Love Song’’ ended the recital, and was well sung, 
although rather faster than usual

DAILY TELEGRAPH

Mornin Post: ‘An executant of extraordinary skill. ‘Kreutzer’ 
Sonata played with much distinction. Juggles at will with harmonics, 
double-stopping, &c

Datty News: ‘Remarkably fine performance of Beethoven's 
‘Kreutzer Sonata.’ Execution marvellously accurate

Sunpay Times: “ Fairly took his audience by storm

HIS WORK IN THE SCHOOLS

Before dwelling upon his professional music 
career—his ‘third period,’ if the Beethoven 
reference may be permitted, and there is a good 
deal of the ‘ unbuttoned’ in Dr. Coward’s nature 
and methods—allusion must be made to his 
experiences as an amateur devotee of the art. 
For fifteen years he was teaching classes and 
conducting choirs five nights per week without 
fee or reward. With a few exceptions, he 
did not receive a penny for all this laborious 
and valuable musical work, nor did he accept 
any remuneration till he had obtained his 
degree of Bachelor of Music at Oxford. 
Always fond of children, one of his first 
important successes as a conductor was 
achieved when the school teachers of Sheffield 
asked him to conduct a big choral concert for 
them, at which the bairns of the various schools 
were to be the performers. For the last twenty- 
five years he has conducted an annual demon- 
stration by a huge choir of Sunday School 
children—an event moving and thrilling, such 
as we do not know in London. The words 
of a friend, Mr. W. W. Chisholm, may be 
quoted in this connection

Sheffield prides itself upon its great gathering of 
Sunday School children on Whit Monday. On that 
occasion, in the natural amphitheatre furnished by 
Norfolk Park, some forty to fifty thousand children and 
teachers sing special hymns. Dr. Coward has been the 
conductor of this feature of Yorkshire religious life for 
twenty-five years. In addition to simple hymns—with 
their overwhelmingly thrilling strains—the Hallelujah 
Chorus from ‘The Messiah,’ and the Gloria from 
Mozart's Twelfth Mass have been sung by these thousands 
of young Yorkshire choristers, whose parents and friends 
assemble in their tens of thousands

Symphony Pathétique abe Tschaikowsky

Overture, Leonora, No. 3 ... as Fr . Beethoven

Mr. Henry J. Wood is the appointed conductor, 
and Dr. Henry Coward is again chorus-master. We 
are glad to learn that the pitch of the organ in the 
Albert Hall (where the concerts will be given) is to be 
lowered. The cost of this important and necessary 
alteration will be about £1,000, of which sum no less 
than £800 will, it is understood, be furnished by one 
gentleman, a true lover of the art in Sheffield. 
Bravo

Opus 84

Beethoven (1770

A Midsummer Night's

4. WaGnerR—Marcia Funebre di Sigfrido nel Crepuscolo degli Dei. 
5. DonizETTI—Linda di Chamounix Sinfonia

2. BEETHOVEN

Dr. Elgar’s ‘Enigma’ Variations were recently 
performed at Wiesbaden at the second symphony 
concert of the Royal Theatre Orchestra, under 
Professor Franz Mannstaedt, and enthusiastically 
received. The Wiesbadener Tageblatt speaks in terms 
of warm praise of the originality of the work, of its 
most brilliant (geistreich) workmanship, and _ its 
surpassing technical masterfulness. The critic, Herr 
Otto Dorn, continues

g00 persons. The efforts of the Society to popularise 
high-class music are very much hindered by the lack 
ofa large hall

Perhaps the Society’s greatest achievement was the 
frst performance in Newcastle of Beethoven’s ‘ Choral 
Symphony,’ under Dr. Richter, on November 24, 1899. 
The success of that concert was so great that, both last 
season and this, the Hallé Orchestra has been engaged 
for all the concerts. The Society has now a reserve fund 
of £250, a financial position which is probably unique

Among the many works which the Society has 
performed for the first time in Newcastle may be 
mentioned Beethoven's ‘ Choral Symphony,’ Mackenzie's 
‘Jason,’ Bach’s ‘God's time is the best,’ Sullivan's 
‘Festival Te Deum’ and ‘Golden Legend,’ Parry’s

MR. JAMES B. CLARK

The biography of Jaroslav Kocian is one that 
may be told in a very few words. He was born at 
Wildenschwert, in Bohemia, February 22, 1884, therefore 
he is not yet eighteen. He began to play the violin 
when only three-and-a-half years of age—a mere baby, 
in fact. His father, a schoolmaster by profession, was 
his first teacher. At the age of twelve the boy entered 
the Prague Conservatorium of Music, and_ studied 
the violin in that institution under Sevtik—the master 
of Ondrigek and Kubelik. He had an equally good 
teacher for composition in Antonin Dvorak. He left the 
Conservatorium only in July last, and since then he has 
been giving concerts in Brinn, Prague, and Vienna with 
the greatest success

M. Kocidn has given two recitals during the past 
month at St. James’s Hall—on the oth and 2oth ult. 
Beethoven, of all composers, is the strong test of an

artist’s sensibility to the hidden depths of profound 
feeling. In the slow movement of that great master’s 
pianoforte and violin Sonata in C minor, young

Kocian showed that he was the fortunate possessor 
of a soul. Tenderness, poetry and faultless intonation 
combined to make this a true poem that came from the 
heart and went to the heart. His interpretation of the 
exacting Chaconne of Bach was a magnificent achieve- 
ment—worthy of one of the greatest masters of the 
instrument. In listening to young Kocian one could 
hardly realise that he was a stripling of eighteen. What 
may he not become? We shall see. In the meantime 
his future career will be watched with the keenest interest

A word of praise is the just due of Miss Marguerite 
Elzy, an excellent English pianist, for the admirable 
manner in which she shared with M. Kocian the 
success of the Beethoven Sonata above referred to, 
and for her delicate playing of some Chopin pieces. 
Mr. Wilfred Bendall was an able accompanist

ELGAR’S DREAM OF GERONTIUS 
IN GERMANY

In conclusion, it may be mentioned that the writer 
heard the work rehearsed in the morning and performed 
in the afternoon of the same day—a very severe test— 
and was far more deeply moved by the second playing 
than by the first

_Miss Adéle Haas (pupil of Mr. Willem Coenen) gave a 
pianoforte recital with much success at the Club Hall, 
Sevenoaks, on the 5th ult. Miss Haas played Beethoven's 
Sonata, Op. 27, No. 2, pieces by Karl Nawratil, Schiitt, 
Godard, Liszt, and Schumann, with much delicacy and 
tefinement, and was specially excellent in the Prelude 
and Scherzo in B flat minor by the last-named composer. 
Mr. Coenen was associated with her in Scharwenka's 
Scherzo for two pianofortes, given with much effect. 
Miss Helen Pattison contributed among other songs 
Coenen’s ‘ Thou wilt remember us,’ very successfully

The Suite in F et small orchestra) by Mr. F. 
Cunningham Woods, found a place in the concerts of the 
Handel Society and the Metropolitan College of Music, 
on the 13th and 16th ult. respectively, when on both 
occasions it was conducted by the composer

ROYAL COLLEGE OF MUSIC

An unusually interesting concert was given by the 
students on the 13th ult., under Professor Stanford. It is 
not often an English orchestral work is heard in the beau- 
tiful concert hall attached to the College, wherefore we 
hailed with delight the prospect of a performance by a 
students’ orchestra of Dr. Elgar’s splendid ‘ Enigma’ 
Variations. The work is no child’s play to even the 
most capable orchestra in the world; how then would 
Sir Hubert Parry’s clever young people fare over its 
difficulties ? We are glad to report that they came out 
of the ordeal with flying colours. There was no 
mistaking the enthusiasm of those young players; and 
their admirable interpretation was greeted with quite 
exceptional warmth. Hermann Goetz’s very beautiful 
setting for chorus and orchestra of Schiller’s ‘ Neenia’ 
received a much less satisfactory interpretation. The 
singers were none too sure of their notes, and the 
orchestra played in a somewhat rough-and-ready style. 
The lovely music (why are Goetz’s works so rarely heard ?) 
could scarcely make its proper effect under such 
circumstances. Schumann's ‘ Genoveva’ overture and 
the fascinating ‘Schatz Walzer’ of Johann Strauss— 
an excellent feature even in a classical programme— 
completed the orchestral selection. Harold Samuel 
played Beethoven's B flat Pianoforte Concerto, and Seth 
Hughes and Florence Macnaughten sang

The performance, under the composer’s direction, 
of Professor Stanford's opera, ‘ Much ado about nothing,’ 
at the Lyceum Theatre, on November 29, furnished proof 
of the capabilities of the students, of whom Kate 
Anderson, as Beatrice, made her mark

The popular concerts at St. 
well attended, but no new works having been brought 
forward, it is only necessary to say that the Quartet 
Party, on November 23 and on the 7th ult., was led by 
M. Sauret, on November 30, by Herr Carl Halir, and on 
the 14th by Lady Hallé

The Stock Exchange Orchestral and Choral Society 
opened its nineteenth season on the roth ult., at Queen's 
Hall, under the conductorship of Mr. Arthur W. 
Payne. The programme included a revival of Litolf's 
‘Maximillian Robespierre’ Overture, and Beethoven's 
Fourth Symphony

Mr. Harold Bauer’s Pianoforte Recital at St. James's 
Hall on the 6th ult., was distinguished by probably the 
first performance in London of a remarkably fine work, 
consisting of a Prelude, Aria, and Finale, written in

42 THE MUSICAL TIMES.—January 1, 1902

favourable notice. Paraphrases rather than variations, | professional and amateur players. The singer was Miss h 
these little pieces are charmingly effective, and received | Beatrice McCready, who is possessed of a very fine and a 
a con amove interpretation under Hellmesberger’s direc-| well-trained contralto voice. There is not much risk in ps 
tion. Richard Heuberger, by-the-way, is not only avery | prophesying that this young lady will ‘go far.’ The ( 
able composer, but also an author of considerable merit. | approval of those present at the concert was so marked Fi 
He has published two interesting volumes of essays on| and general that in all probability the next concert of the Mi 
modern music and musicians, and a Schubert biography | series will attract a much larger audience. No 
from his pen, tastefully illustrated, has just issued from] On the 6th ult., Dr. Laurence Walker gave his second ves 
the press. Chamber Concert of the season, and had, as usual, a Mies 
The Gesellschaft der Musikfreunde gave a performance | large attendance. He himself undertook the larger part her 
of the ever-welcome ‘Creation’ of Haydn. Familiar as|of the work, and played, with his accustomed skill, Ma 
we all are with every detail of the work, its charm|selections from the works of Schumann (second and per 
remains undiminished, despite the hundred years of its}third movements of Fantasia, Op. 17), and Chopin Mis 
existence. Amongst the solo interpreters was Frau] (Etude in A flat, Op. 25, No. 1). Mr. Dettmar Drepel om 
Herzog, of Berlin, an excellent vocalist and true artist,| was the violinist, and performed Bach’s ‘Chaconne’ ee 
who made her début in Vienna on this occasion, and also} (with Dr. Walker) and Beethoven’s Kreutzer Sonata. Sivi 
proved herself a charming interpreter of songs, both old| Miss W. Kisack contributed a number of well-selected Blac 
and modern, at a recital given by her. songs. ae onl 
At the Opera the prevailing scarcity of remarkable] The majority of the inhabitants of this city are Me 
new works has led to a revival of one of Offenbach’s. His| Presbyterians, and as such are closely allied to the notic 
fantastic opera ‘ Hoffmann’s Erzahlungen’( Les Contes| Scottish Churches. Like the dwellers North of the Glas 
d’Hoffmann), sumptuously mounted and prepared, | Tweed, they have long held conscientious objections to re 
musically, with his usual admirable care for every detail, | the introduction of organs into their churches, and the F nq; 
by Director Mahler, is being greatly appreciated by | use of instrumental music in public worship. But they rot 
lovers of light, easily-to-be-understood music. Offen-| have suddenly begun to follow the lead of their Scottish P Mi 
bach’s strains are the manifestation, not so much of an| brethren in the matter of the use of the kist o’ whistles. -_" 
original talent, but of a ripe and subtle theatrical] As a case in point, an organ by Messrs. J. W. Walker poten 
experience, and presented as it is in the present case, | and Sons, of London, was opened at Fisherwick Church, powers 
with such absolute perfection, its powers of attraction|on the 9th ult., by an admirable recital, given by 
upon the public here are considerable. Mr. Alfred Hollins. The selection of music performed 
Amongst those who have appeared in Vienna lately | included a number of the most effective modern works 
must be mentioned, in the first place, the violinist | for the instrument, as well as a ‘Theme with variations 
Hubermann, whose precocious talent we had occasion] and fugue’ by the organist himself, performed for the

first time, although it was written for the late Mr

to admire some years ago, and who now presented 
himself once more as a ripening young artist of the first | Frederic Archer's recitals in the Carnegie Music Hall, Fay 
order. His playing is superb, technical difficulties | Pittsburg. Mr. Hollins showed to the utmost the fine B g),io, 
do not exist for him, and his soulful interpretations | quality of the organ. | Golde 
prove him to be a born musician. Another violinist, wo » given 
more advanced in years and experience, Richard Sahla, | Clara 
court-capellmeister of Biickeburg, delighted us, inter alia, MUSIC IN BIRMINGHAM. ; acquit 
with a masterly interpretation of Beethoven’s Violin / Mr. A 
(FROM OUR OWN CORRESPONDENT.) Cathe

Concerto. Joachim, too, the king of violinists as he is 
most justly called, paid us a visit, with the artists forming} The third concert of the Halford Society took place F the pr 
his quartet, and aroused the enthusiasm of his audience|in the Town Hall, on November 26. The programme 600 pet 
to even a greater degree—if that be possible—than in] included Borodine’s Overture to ‘Le Prince Igor,’ A p 
preceding years. given for the first time in England. M. Ysaye was § ond uit 
MANDYCZEWSKI. recalled four times after his splendid rendering of B direct: 
Beethoven's Violin Concerto, but no encores are allowed F There | 
at these concerts. At the fourth concert the novelties F from ¢p

MUSIC IN ABERDEEN. were Granville Bantock’s ‘Helena’ Variations, finely players 
(FROM OUR OWN CORRESPONDENT.) — veg — sors tag Bo poe and ae rendere 
: : received, and Volkmann’s Violoncello Concerto, wi : 
The musical pulse of Aberdeen continues to beat some-| 7 Willy Lehmannas soloist. Mr. Halford’s conducting oe

giving, on November 27, a popular orchestral concert.| The Chamber Concert Society began its season in the under the 
By ‘popular’ was meant: firstly, cheap, for admission| New Temperance Hall on November 27. Borodine'’s 
was one shilling or sixpence; secondly, there were a| String Quartet in D was the chief feature of the pro F

number of light and attractive pieces combined with a| gramme. Chamber music seems at last to be receivit{ i Mr. Ww. 
Symphony, the No. 1 of Beethoven; and thirdly, there| proper support in Birmingham, to judge by th four lect; 
was a vocalist to avert monotony. The orchestral | audienc assembled at this concert. turday 
items were very creditably performed under the baton of} On the 16th ult., the Birmingham Amateur Orchestra ist and’ 
Dr. Francis Koeller by a band of fifty-eight local | Society gave an excellent concert before the members 0 the Histo

Golden Legend’ and a miscellaneous selection were

MUSIC IN DUBLIN. 
(FROM OUR OWN CORRESPONDENT

The first performance given by the Dublin Orchestral 
Society was in every respect worthy of the traditions 
of the Society. A most thoroughly satisfactory per- 
formance of Beethoven’s Symphony No. 8 was given. 
Some lighter music by Saint-Saéns and Dvorak, and 
Wagner’s early work, ‘ Eine Faust Overture,’ made up 
a fine programme. The second concert was chiefly 
remarkable for the Tschaikowsky Pianoforte Concerto in 
B flat minor, the solo part of which was magnificently 
played by Miss Annie Lord. Haydn’s B flat Symphony, 
and Beethoven’s ‘Coriolan’ Overture, were well 
rendered by the band, and Signor Esposito conducted 
with his usual ability

At the first concert given by the Dublin Musical 
Society, Sullivan’s ‘Golden Legend’ and some mis- 
cellaneous items were included. The chorus was strong 
and bright, but the performance as a whole was not 
altogether satisfactory, owing to what appeared to be 
insufficient rehearsal

MUSIC IN EDINBURGH. 
(FROM OUR OWN CORRESPONDENT

At the first of Messrs. Paterson’s excellent Orchestral 
Concerts, the Symphony was Schumann's in D minor, 
which Dr. Cowen conducted from memory; and 
Mr. Borwick gave a brilliant and artistic reading of 
the solo part in Beethoven's Pianoforte Concerto in 
C minor. At the second concert, Beethoven’s C minor 
Symphony, and the Symphonic Poem of Saint-Saéns, 
‘Le Rouet d’Omphale,’ were the main attractions, 
and Miss Lilian Blauvelt sang. At the third concert, 
on the 16th ult., the Choral Union gave an eminently 
complete and satisfactory rendering of ‘Elijah.’ 
Chorus, principals, and band alike did well, but the 
feature of the concert, and one not to be hurriedly 
forgotten, was Mr. Ffrangcon - Davies’s startlingly 
fine representation of the part of the prophet. The 
other soloists were Madame Emily Squire, Mr. Lloyd 
Chandos, and Miss Lilian Hovey

Mr. Denhof’s Chamber Concerts have reached their 
sixth season, and seem to increase year by year in artistic 
value. The performance of Schumann’s D minor Trio 
was an ideal one. Mr. Denhof was assisted by MM. 
Hans Wessely and Bramsen, with Miss Luisa Sobrina as 
vocalist, and Mr. Scott Jupp as accompanist

MUSIC IN GLASGOW. 
(FROM OUR OWN CORRESPONDENT

The season of the Choral and Orchestral Union opened 
auspiciously on November 26. In some quarters it was 
feared that the surfeit of music at the International 
Exhibition during the summer and autumn might 
adversely affect the attendance at the concerts. It is 
gratifying to report that so far from that being the 
case, the number of subscribers to the scheme is larger 
than formerly, while the support of casual concert-goers is 
very generous. A large audience assembled at the opening 
concert, when the Scottish Orchestra, under Dr. Cowen, 
gave a highly-finished performance. The programme 
included Beethoven’s Coriolan Overture, the prelude 
and finale from Tristan and Isolde, Liszt’s Pianoforte 
Concerto in E flat (Signor Busoni playing the solo part), 
Tschaikowsky’s Air and Variations from his Third 
Suite, and Schumann’s Symphony in D minor. On the 
3rd _ult., the Choral Union, associated with the Scottish 
Orchestra, gave a capital performance of Dr. Elgar's 
‘Caractacus,’ which was given here for the first time 
last year. The soloists were Madame Medora Henson, 
and Messrs. William Green, Robert Burnett, and Andrew 
Black, all of whom performed their parts excellently. 
Mr. Bradley ably conducted. The appearance of Madame

Lillian Blauvelt, and an excellent orchestral programme

which included Beethoven’s Eroica Symphony, 
attracted a large audience to the concert on roth ult. 
The choir of St. John’s United Free Church gave

a very carefully prepared performance of Handel's f

46 _ THE MUSICAL TIMES.—January 1, 1902

selected for performance at the first concert of the season. 
The soloists were Miss Agnes Nicholls and Mr. Ivor 
Foster, both of whom sang remarkably well. The choir 
and orchestra also acquitted themselves very creditably, 
and the performance as a whole was distinctly successful. 
Mr. Kilburn couducted with his usual resourcefulness. 
At the same concert, Mozart's Clarinet Concerto in A, 
and Beethoven's ‘ A Calm Sea and a Prosperous Voyage,’ 
were performed

Sir Frederick Bridge’s ‘Callirhoé’ was the work 
selected by the National Telephone Vocal Society for its 
first concert of the season, given in the Town Hall, 
Newcastle, on the 4th ult. This organization is distinctly 
progressive in its aims, and the success which has 
hitherto attended its efforts should encourage its 
promoters to still greater achievements in the future. 
The solos on the present occasion were admirably 
sung by Miss Perceval Allen, Miss M. Bowmaker, and 
Mr. Fred. Fallas. Mr. Yeaman Dodds presided at the 
organ, Mr. H. M. Renwick at the pianoforte, and 
Mr. George Dodds conducted

We have had a good deal of music this term, some of 
it of the truly miscellaneous kind, and therefore it will 
only be possible to chronicle some of the more serious and 
artistic performances

The first concert of note took place in the Town Hall, 
on October 24, under the auspices of the Musical Club, 
when the Kruse String Quartet gave a capital chamber 
concert. The principal works constituting the programme 
were Beethoven’s Quartet in E flat major (Op. 74), and 
Schubert’s Quartet in G major (Op. 161). The next 
day, in the same building, Mr. Leonard Borwick and 
Mr. Plunket Greene gave a concert, Mr. Borwick playing 
very delightfully, as the first item, Schumann’s Sonata 
in F sharp minor (Op. 11) dedicated to Clara Wieck— 
and in the second part of the programme, several pieces 
from ancient as well as modern composers. Mr. Greene 
contributed a number of songs, no less than thirteen 
being placed in the programme. ae

On November 13, the professor of music, Sir Hubert 
Parry, discoursed in the Sheldonian upon ‘ The 
Differentiation of Style in Music,’ giving us a thoroughly 
admirable lecture

The Ludwig String Quartet, under the auspices of the 
Musical Union, gave an excellent all-round concert 
in the large room of the Examination Schools, on 
November 18, the chief items being Beethoven’s F minor 
Quartet and Haydn’s Quartet in C (Op. 33). On the 
28th, in the Town Hall, Mr. Borwick again appeared with 
Herren Carl Halir, Hugo Becker, and Mr. Hobday as

ome of 
it will 
yus and

An epidemic of ‘Messiah’ performances has broken 
out in the district, something like a score having taken 
place during the past month. Among others are to be 
noted those by the Chesterfield Harmonic Society (Mr. 
G. A. Seed), the Penistone Choral Society (Mr. J. 
Cooper), the Hoyland Common Choral Society 
(Mr. G. M. Coates), and the Norton Lees Choral 
Society (Mr. H. Reynolds

The Brincliffe Musical Society, still developing under 
Mr. J. H. Parkes, gave a concert, at which Beethoven’s 
Symphony No. 4 was capitally played

The Eckington Choral Society (Mr. Geo. Harrop) and 
the Walkley Musical Society (Mr. Henry Brown) have 
also done progressive and appreciated work during the 
past month. The Sheffield Orchestra gave a concert on 
the 19th ult., under Dr. Coward, playing Mozart’s 
Symphony in G minor, Wagner’s ‘ Walkurenritt,’ 
Mackenzie's ‘ Manfred’ Suite, Liszt’s symphonic poem, 
‘Mazeppa,’ and other works. Miss Maggie Jaques was 
the vocalist. A recital by M. Paderewski, under Miss 
Foxon’s auspices, fittingly rounded off the month’s feast 
of good things

OTHER YORKSHIRE TOWNS

At Halifax, the chamber concerts were resumed on 
November 26, when Mr. Rawdon Briggs’ Manchester 
Quartet and Mr. H. F. Webster played Dvorak’s Piano- 
forte Quartet in E flat and a Beethoven String Quartet, 
Miss Edith Wehner being the vocalist. Two days later 
the Halifax Madrigal Society, under Mr, Shepley, gave

Hiawatha's Departure,’ which is certainly heard to 
greater advantage when it does not come after the two 
preceding scenes of Mr. Coleridge-Taylor’s ‘ Hiawatha ' 
trilogy. The principals were Madame Goodall, Mr, 
Horsfield, and Mr. Tree, and the performance, though 
lacking something in power, was generally creditable. At 
Huddersfield, on the roth ult., the Glee and Madrigal 
Society, under Mr. Ibeson, gave one of its concerts, several 
of Bishop’s works—not often performed nowadays— 
being the feature of the occasion

The Dewsbury Choral Society, of which Mr. Fricker, 
organist of the Leeds Town Hall, has been recently 
appointed conductor, gave a very enjoyable performance 
of ‘Acis and Galatea,’ and Gade’s ‘ Crusaders,’ on the 
1oth ult. The chorus-singing was admirable in precision 
and tone, showing that the new conductor has secured 
the full confidence of his choir. The band was of more 
than average efficiency, and Miss Agnes Nicholls, Mr. 
W. Green, and Mr. C. Knowles were excellent in the solo 
parts. On the following evening, the Keighley Orchestral 
Society, of which Mr. Summerscales is the conductor, 
gave, among other things, a creditable performance of 
Beethoven’s Fourth Symphony, which has not been 
heard in the West Riding for many years past At one 
of the Wakefield chamber concerts, on November 21, 
the Leighton House String Quartet party, of which 
Mr. Bent is leader, played a quartet by Dvorak in highly 
artistic style, and were joined by Madame Adine O'Neill 
as pianist in Brahms’s F minor Quintet. The Morley 
Choral Society, on the same date, gave a miscellaneous 
concert, Mr. Alfred Benton conducting some orchestral 
pieces, capably played by a small band

The York Symphony Orchestra, which is so energeti- 
cally trained and conducted by Mr. Tertius Noble, 
showed the good results of his teaching, in a concert given 
on November 20. Mozart's so-called ‘ Jupiter’ Symphony, 
and Suites by Bizet, Sullivan (the ‘ Kenilworth’ Masque) 
and Landon Ronald, made up a highly interesting 
programme, and were very ably played. Mrs. Burrell 
was the vocalist. On the 4th ult., Miss Edith Wehner 
gave a most enjoyable vocal recital in York, assisted by 
Mr. F. Norton, a baritone (who sang some good songs of 
his own composition) and Mr. Charles Henrich, a highly 
accomplished pianist. On the 17th ult., the York 
Musical Society, under Mr. Noble, gave a good account 
of ‘The Death of Minnehaha’ and ‘ Hiawatha's 
Departure,’ the chorus showing a distinct improvement. 
Miss McLaughlin, Mr. Hyde, and Mr. Iles were the 
principals

ANTWERP.—The first performance, on November 30, 
of a new opera by Jan Blockx, the successful composer 
of ‘La Princesse d’Auberge,’ was an event which 
attracted an audience from all parts of Belgium, 
interested in the progress of the Neo-Flemish move- 
ment inaugurated by the late Peter Benoit. The new 
work, ‘De Bruid der Zee’ (‘ The Bride of the Sea’), 
is very dramatic in parts, its musical themes being 
either directly representative or happily imitative of 
the traditional Flemish folk-song, and, aided by a good 
performance, a complete success was scored

BayrEUTH.—Beethoven’s ‘ Missa Solemnis’ was heard, 
for the first time in Bayreuth, last month, when a most 
impressive performance of the great work was given, 
under Herr Julius Kniese’s direction, and with highly 
efficient interpreters of the solo parts

Ba 
sava) 
trilos 
been 
Bala, 
musi 
Its | 
eagel

arty

a new work by Th. Miiller-Reuter, for 
chorus and orchestra, entitled ‘Hackelberend’s Begrab- 
niss,’ the libretto founded upon Julius Wolff's novel, 
‘The Wild Huntsman,’ achieved a great success on its 
first performance at the third Giirzenich concert of the 
season. A very favourable reception was also accorded 
toa new ‘orchestral legend,’ by Jean Sibelius. At the 
Stadt Theater, a new three-act opera, ‘Lorenza,’ by 
Edvardo Mascheroni, has been brought out, for the first 
time in Germany, with marked success. During an 
interval the composer was publicly presented by the 
president of the ‘ Beethoven Haus’ with the diploma of 
honorary membership of that institution

DaRMSTADT.—At a recent concert of the Kirchengesang 
Verein, the programme included the first performance of 
anew motet, ‘Ich steh in meines Herren Hand,’ by 
Arnold Mendelssohn, the conductor of this excellent 
society

HELSINGFoRS.—A performance was given recently, at 
the Finnish Theatre, of Hauptmann’s fairy drama, 
‘Hannele,’ with some highly characteristic incidental 
music by Erkki Melartin, a native composer. Frau 
Agathe Backer-Groendahl has given two concerts here, 
in which she introduced herself most favourably, both 
a a pianist and composer for that instrument, as 
well as of some charming songs

Leipzic.—A new Pianoforte Trio by Ernst Heuser, the 
talented young Cologne composer, was produced and 
very favourably received, at the Chamber Concert of 
the Gewandhaus, on November 30. By the Riedel 
Verein, under Dr. Géhler’s zealous direction, excellent 
performances have been given recently of Beethoven’s 
‘Missa Solemnis,’ and of Mozart’s C minor Mass, as 
completed from Mozartian themes, by Herr Schmitt, of

WliM

Sonata in D minor by Dr. Ferris Tozer and Mr. Allan 
Allen. —— The Oratorio Society gave an _ excellent 
performance of Haydn’s ‘Creation’ and Stanford's 
‘Last Post,’ at the Victoria Hall, on the 13th ult. 
The choir and orchestra were worthy of high praise, 
both in Dr. Stanford’s cantata, which opened the pro- 
gramme, and in the oratorio. The solo vocalists in the 
latter work were Madame Emily Squire, Mr. Gwilym 
Richards, and Mr. Watkin Mills. Dr. H. J. Edwards, 
who conducted, had his forces under excellent control 
throughout

HEREFORD.—The Choral Society gave a performance 
of Handel's ‘ Acis and Galatea,’ in the Shire Hall, on 
November 26. Both choir and orchestra acquitted them- 
selves admirably under Dr. Sinclair’s spirited direction, 
the choir being specially excellent in the choruses ‘ Happy 
we,’ ‘Wretched lovers,’ and ‘Mourn all ye muses.’ 
The solo vocalists, Madame Sobrino, Mr. Vivian 
Bennetts, and Mr. Ineson were a highly capable trio. 
The miscellaneous selection which followed included a 
madrigal for six voices, ‘ Spring, the sweet Spring,’ by 
the Rev. W. D. V. Duncombe, and Beethoven's Romance 
in F, for violin and orchestra, the solo part being re- 
markably well played by Miss Evangeline Anthony, a 
youthful violinist whose efforts were enthusiastically 
rewarded. The concert was altogether very successful

Hopart.—The first subscription concert of the newly- 
formed Philharmonic Society took place in the Town 
Hall on October 29, when a somewhat ambitious 
programme was undertaken, including Beethoven’s 
Symphony in C major and Felicien David's ‘ The 
Desert.’ In the former work the orchestra (led by Mr. 
W.G. Tucker) came through the ordeal successfully, and 
both choir and orchestra were fully efficient in David’s 
descriptive cantata. The solo vocalists in this work 
were Mrs. Holden, Miss Gill, and Mr. James Dean, and 
the verses illustrating the story were recited by Mr. A. E. 
Shakespeare. Mr. Bradshaw Major, who conducted, 
may be congratulated on his efforts to establish good 
music here

HorsHAM.—The Musical Society gave their first con- 
cert of the season on November 27, when ‘ Hiawatha’s 
Wedding-Feast’ (Coleridge-Taylor), and ‘The Bride’ 
(A. C. Mackenzie) were performed. The tenor solo, 
‘Onaway, awake,’ was finely sung by Mr. Gwilym 
Richards, who was joined in Mackenzie's cantata by Miss 
Minnie Cooper. The chorus sung with a great deal of 
spirit. Mr. A. P. Whitaker conducted

LEAMINGTON.—The Leamington Orchestral Society 
gave their first concert of the season on the gth ult., 
when the following works were performed :—Mozart’s 
Symphony in D major, No. 38; Grieg’s Elegiac Melodies 
for Strings (Op. 34); Schubert’s overture, ‘ Rosamunde’ ; 
Bizet’s suite, ‘L’Arlésienne’; and German's ‘ Nell 
Gwynne’ Dances. The orchestra was heard to special 
advantage in the ‘Rosamunde’ overture. Mr. William 
Henley was the leader, and contributed two solos to the 
programme with much success, and Madame Siviter was 
the vocalist. Mr. Walter Warren conducted with his 
usual skill

LLANDUDNO.—On the 13th ult., Miss Margaret Thomas 
gave her Annual Chamber Concertat the Prince’s Theatre, 
assisted by the following artists:—Miss Ira Aldridge 
(vocalist), Miss Florence Walton (harpist), M. H. 
Verbrugghen (violinist), and Mr. J. Walton (violoncellist). 
The programme included Beethoven's Trio in B flat 
(Op. 1, No. 1), and Grieg’s Sonata in F, for pianoforte

and violin

London: NoveLto AND Company, Limited. 
Settings of the Vesper Hymn. 
‘* LORD, KEEP US SAFE THIS NIGHT

Asuton, A. T. Lee.—In B flat ca we ee sae “i |6=AGe 
BEETHOVEN.—Twe Settings in G rp ass pa re sca ds 
STEANE, B.—In F ... (Sol-fa, 14d.) 1d

Suttivan, A.—In F (with Sevenfold Amen) ... (Sol-fa, 1d.) 1d. 
VincoE, A. L.—In F ; sie we 3G

REV. SiR F. A, Gore OUSELEY, Bart

BEETHOVEN

April, 1897. 
May, 1897. 
June, 1897. 
November, 1900. 
January, Igor

