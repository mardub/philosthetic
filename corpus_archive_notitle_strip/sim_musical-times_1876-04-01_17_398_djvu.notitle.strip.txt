Arranged for the Organ or Harmonium. 
By JOHN OWEN. 
Each Part One Shilling net

Part 1. VOLUNTARIES; Subjects by Heller, Mendelssohn, Schu- 
mann, Beethoven. tA 
» 2 VOLUNTARIES; Subjects by Handel, Beethoven, Rossini

Mozart, &c

I HAD completed my father’s biography. During 
ten years, in the silent hours of night, I had lived 
over again another life by the side of my own—that 
of my father. It was a wondrous time—a time so 
strange as to almost make me believe in the possi- 
bility of citing spirits from the other world. When 
late in the evening I had laid aside my onerous 
official work, and had settled down before the desk in 
my little study wherein the material for this precious 
task lay piled up almost to the ceiling—lo! how they 
would swarm out, those kindly spirits, from countless 
faded letters, from enormous heaps of time-worn 
journals, from ancient, mysteriously-sounding piano- 
fortes and guitars—so real, so full of life, that, in the 
delusion of my senses, the living and the dead would 
appear mixed up together. There they were, these 
old gentlemen with dandy-swords and bob-wigs, in 
abbés’ robes and violet stockings, in court uniforms 
and hair-bags; these grave or smiling faces with 
violins, violoncellos, conductors’ batons, and zithers ; 
and these ladies, too, with rosy cheeks, their hair 
fashioned a la “ Victime ” or “ Titus,” wearing tight 
dresses with ‘“Gigot” sleeves, and with rolls of 
music in their hands, women with sunny eyes and 
gaily ringing voices—and all these assembled round 
the small pale man seated in their midst by the 
piano

Time had fulfilled its purgatorial office. The 
heated sympathies and antipathies of the days when 
politics were still confined to the care of diplomatists, 
and Art alone was free to rouse the heart to stormy 
emotions, had cooled down. I comprehended, ab- 
stractedly, why many of the chief actors of that 
bygone period had misunderstood and persecuted one 
another, but I could no longer share with them their 
hatred or their love; so that some even among those 
whom my father had loathed and despised during life 
had become great and attractive in myeyes. The 
canonization of remarkable men is just now the 
fashion. Every laurel-wreath is converted into a 
saintly nimbus, which is made to fit the brow of even 
a Tiberius or a Nero. But, for all that, these great 
artists have hated and despised and abused one 
another heartily, and their mutual feelings of aversion 
were the more intense, the more decidedly original 
their artistic organization had been. ‘The critic 
must look to all sides,”” Weber had often exclaimed; 
“‘the creative artist, however, must say, ‘ Mine alone 
is the true art,’ or he will never be worth much.” Thus 
Beethoven could call the composer of * Freischiitz”’ a 
“tender mannikin,” and the latter write a parody on 
the “Eroica.” The poet Tieck could designate 
“ Freischiitz” as the most unmusical noise which 
was ever let loose on the stage; Andrzas Romberg 
could crack his jokes upon Beethoven’s quartets, 
calling them “ motley stuff,” and Spohr consider the 
‘“Ninth Symphony” in questionable taste; Vogler, 
after hearing Poissl’s “ Athalia,” could find “Don 
Giovanni” no longer suitable to his palate; and 
Zelter declare “‘ Euryanthe” not worth the amount

of perspiration it had cost its author. All this was 
natural and a matter of course. But most natural 
of all was that, in the eyes of Weber, Rossini should 
appear as the Lucifer in Music—he, the most beauti- 
ful originally, but also the most fallen of angels. 
‘*He can do everything, and the good amongst it,” 
he had exclaimed, after hearing the second act of 
“Mosé;” “only this Satan doesn’t care to do it

424 THE MUSICAL TIMES.—Aprit 1, 1876

here it was Rossini who filled the throats of these 
rare artists with his fascinating melodies. And had 
not Weber himself, in spite of his wrath, experienced 
the charm to such a degree that, during a perform- 
ance of “Cenerentola,” after listening to a duet 
between Ambrogi and Lablache, he abruptly quitted 
the house, with the furious exclamation—‘‘ Now I 
shall run away; this stuff begins to please me too!” 
His heart drew together in his bosom, and ever and 
again he was fain to recall the confiding passage in 
his new work, ‘I trust in God and in my Euryanth’” 
when casting his eyes over the battle array with which 
he was to face this “ army of archangels,” as he him- 
self called it. To represent the parts of Eglantine, 
Adolar, and Lysiart, he had the efficient but some- 
what worn singer Griinbaum, the excellent but weak 
tenor Haizinger, the capital but rather vulgar bass, 
Forti. But who was there for the heroine herself, 
Euryanthe? Mdme. Ungher-Sabatier, who may have 
been in Weber’s mind when writing the part, was not 
to be procured. There was no choice, then, but to 
run a risk by placing it in the hands of Henriette 
Sontag, then barely nineteen years of age. And 
only two years before Weber had pronounced this 
gifted girl to be “ still a bit of a goose;” while, but a 
tew months back, Mdme. Fodor-Mainville, when hear- 
ing her sing the part of Amazili in Spontini’s opera, 
had exclaimed—* This little German can do nothing 
yet!” But there was no alternative; and the result 
proved a success. All the performers manifested 
their enthusiasm and affection for Weber; the true 
aristocracy of minds, the independent Press, headed 
by the genial Kanne, were on his side. The 
better part of the Vienna public, moreover, 
longed for the more powerful emotions aroused 
by a truly great German work, heartily tired as 
they were of the many insipid imitations of “ Der 
Freischiitz,” such as Weig]l’s “ Eiserne Pforte,” and 
Kreutzer’s ‘‘ Libussa.” Had not the “grey lion of 
Baden ” himself (Beethoven) at one of his fitful visits 
to Stainer’s music-shop remarked, “ There is much 
need for a good German opera; I wish Weber 
success.” And yet, already ‘after the first few re- 
hearsals the latter could not deceive himself as to 
the contrast of colours his work would exhibit, as 
compared with the popular Italian productions of 
the day. In creating his new work, the chaste con- 
ception of his thoroughly German genius, produced 
under great anxiety of mind by German earnestness 
and German perseverance, he had spent his very 
life’s blood—he had given the best he had to give. 
For this very reason, however, the impression pro- 
duced on seeing this work now at last on the stage 
would, in comparison with the glowing, glittering, 
merrymaking, effervescing, and enticing creations of 
the “Barber of Seville,” of “Othello,” ‘*Gazza 
Ladra,” ‘Matrimonio segreto,” and “Donna del 
Lago,” be like the effect of the dreamy poetry of a 
moonlight night as contrasted with the sunny day. 
Men of specifically German organization, men of 
concentrated and intense feeling, paid homage to the 
German master; the Viennese public at large, whose 
tastes—directed now, as then, chiefly to scenic and 
rhetorical effects in art—caused them to be attracted, 
like the moth, to the glaring light, forsook the serious 
German work already at its third performance, while 
even before the arrival of Barbaja’s troupe every 
seat in his theatre had been engaged in advance for 
as many as twenty performances. It is but small 
comfort to think that the light by which the public 
had been thus attracted was really a beautiful and 
lustrous one

Weber possessed a keen sense of the advantages 
waiting upon personal beauty, even in man. On one 
occasion, while travelling with the clarionet-virtuoso 
Barmann, a man handsome and well-formed, to whom 
a distinguished reception was everywhere accorded, 
both at hotels and in society, Weber remarked to him: 
‘“‘ This is not surprising ; the creator in his goodness 
has fixed a letter of recommendation on your 
exterior, whilst I carry it only in my pocket.” A 
glance at his own small, fragile figure, therefore, 
would often provoke in him a melancholy humour. 
Thus, when for the first time he donned his court 
uniform as capellmeister at Dresden, he piteously ex- 
claimed, ‘‘ Dear me, my calves would compare favour- 
ably with those of the finest butcher-dog!” And only 
a few days previous to his death he wrote to his wife, 
‘“‘T shrink together like a dried plum. To shave my 
face smoothly will henceforward be a puzzle to me.” 
Although his sterling heart was a stranger to envious 
feelings, it would yet be painfully touched if, careful 
in his toilet as he was, he would glance at his figure 
in the glass, previous to a rehearsal, and find therein 
reflected the pale, spiritualised face of the German 
thinker, the weary eyes behind the strong spectacles, 
the narrow shoulders and almost transparent hands ; 
and would then meet at the opera-house his brilliant 
rival, upon whose cradle Apollo and the Graces had 
with equal liberality showered their precious gifts. 
Broad-shouldered, with elastic step, his splendid head 
erect, orders upon his proud breast, this man con- 
fronted him, the same who but a few years previously 
had, along with his sweet melodies, won the hearts 
of the most beautiful women of Italy, certain of vic- 
tory wherever he went. And yet, strange to say, 
there was a similarity in the features of the two 
great masters, in so far as sickness and exuberant 
health, ingenious work and spontaneous creating, 
Germany and Italy, in short, can be at all likened 
to one another. This outward resemblance in the 
physiognomy of the two men may, however, be 
psychologically established also. Was not Weber 
exactly to Germany what Rossini was toItaly? The 
congeniality of their minds to their respective native 
countries forms an exact parallel, which would seem 
to account for the similarity in their features. The 
fact that the existing antipathy between Weber and 
Rossini, founded as it was alike on homogeneous 
and heterogeneous elements, had taken a less violent 
form with the latter may be explained by a feeling of 
generosity on the part of the more fortunate. With 
Weber, on the other hand, the aversion had reached 
its culminating point after the encounter in the arena 
at Vienna, gradually losing its intensity in proportion 
as his bodily weakness ‘increased, so that in time he 
could even bring himself to admit the melodiousness 
of “Il Barbiere.” Passing through Paris, on his way 
to England, he resolved to pay a visit to the com- 
poser. A previous visit to Cherubini, for whom he 
always entertained the highest admiration, gave him 
strength for this effort, and, perhaps, he was actu- 
ated also by a desire'to make his peace with everyone 
in this world while there was yet time. Rossini, the 
man of the world, who at an interview with Beethoven 
at Baden, near Vienna, had ostentatiously bent his 
knee before the great master, received Weber with 
signs of homage, not omitting at parting to accom- 
pany the “ maéstro del franco arciero ” to the foot of 
the stairs, whereby the latter, whom want of breath 
compelled to rest at every step, was fain to lean upon 
the gladly-profferred arm of his former enemy

Forty years had gone by, time had reduced the flames

THE MUSICAL TIMES.—Apriz 1, 1876. 425

become historical. But the picture of the grand im- 
mediate past of German musical art had, aided by the 
researches upon which I had been so recently engaged, 
assumed in my mind a degree of life and distinctness 
which surrounded the few remaining representatives 
of that period with a halo of glory. Above all, Ros- 
simi. And even though he had contended with my 
father—they were champions worthy of each other’s 
steel, and both had been in the right. Rossini, in 
my eyes, had become identified with the graceful 
and the sweet sensual charms in music. When, 
therefore, in the spring of 1865 I had occasion 
to make a prolonged stay at Paris, with the impres- 
sion still on mind of the double life I had lived and 
the spirits I had cited, I felt almost as if the express 
train were to convey me, not some hundreds of miles 
onward, but forty years back into the golden time of 
music; into the very centre of its whole delightful 
tone-atmosphere. I fancied as if, round its still 
living heroes, the great departed would move visibly 
in life and sunshine who had hitherto yielded only to 
my entreating call to enfold their presence to me 
in the silence of night. Those hands with which 
Rossini, Auber, Caraffa pressed mine, had assisted in 
creating that remarkable epoch, had on their part 
laid in those of Beethoven, Schubert, and my father’s ; 
thus establishing complete ‘“‘ rapport” between my- 
self and them. On the third day after my arrival, 
the Prefét, M. Haussmann, apologised during dinner 
to General Morin for having been unable to keep an 
appointment he had made with the latter. ‘I had 
some very distinguished guests,” said he; ‘‘ guess 
who they were.” The General mentioned some very 
aristocratic names. ‘Go up higher,” replied Hauss- 
mann. ‘ Well, then—Prince Napoleon.” ‘ Higher 
still,’ cried Haussmann. “Indeed,” said the Gene- 
ral, laughing, “their Imperial Majesties could not 
have been with you, or else the papers would know 
it.” ‘Not the majesty of rank, but that of genius

replied Haussmann with animation ; “ it was Rossini, 
Auber, and Morse” (inventor of the telegraphic 
writing-apparatus). ‘A la bonheur,” respectfully 
saluted the most distinguished engineer of France. 
Much impressed with the homage thus rendered to 
genius, so characteristic of the French character, I 
at once wrote to Rossini to be permitted to visit him, | ( 
and directly after received a note from his wife, in- 
viting, in the most amiable terms, “the son of the 
great master.” I have never known embarrassment 
in presence of the great of this world; but, mounting 
the gloomy staircase at No. 2, Rue de la Chaussée 
d’Antin, on the 21st of March, 1865, I felt my heart 
beating. I was at once admitted, and, passing 
through a spacious but somewhat sombre ante- 
chamber, I was ushered into Rossini’s study. He 
rose from behind a huge mass of papers piled upon a 
table, and quickly walked towards me: very different 
in appearance to the picture I had conceived of him, 
the hero of art and of the fair sex—small of stature, 
corpulent, yet swift in his movements. I was deeply 
impressed. This was Rossini; and yet—lI verily 
believe my father would have looked like this if, in 
the full possession of health, he had reached his 
seventieth year. There were his forehead, his eye- 
brows, and, above all, the nose; quite different, how- 
ever, the massive lower part of the face, and the full 
and decidedly plain, sensual mouth. With a smile 
which still reminded one of the “ love-inspiring en- 
chanter ot Pesaro,” he extended to me his hand. 
That was the hand which had written “ I] Barbiere” 
and “ Tell;’? and never had I held a nobler one in 
mine. Involuntarily I inclined my head towards it

whereupon he clasped me in his arms, exclaiming— 
“Indeed, you are taller and stronger, but you are 
like your father, who was very ill when I last saw 
him.” “If this be the case, I am fortunate enough 
to resemble you somewhat too,” was my reply; ‘ for 
the upper part of your face—the forehead especially 
—is like his.” ‘‘ Well, then,” he retorted laughingly, 
“it would seem there is something musical in my 
features after all! Why do we appreciate great men 
only when we grow old and wise! And the worst of 
it is, they are then generally already gone. By this 
time I know that a work like ‘ Oberon’ can never be 
written again. And if he were still alive, he would 
give me some credit too. We should, in fact, be old 
men both, and nothing more.” And then he went 
on to relate, with an astonishing resource of 
memory, the particulars of Weber’s visit at his 
house, upon the latter’s death-journey to Eng- 
land. “He breathed heavily as I descended the 
stairs with him. I was obliged to assist him 
into his carriage —it was impossible he could have 
breathed in this manner much longer. But he did 
well to die early,” he added with a truly French turn, 
‘*what am I good for? I amuse myself, but no longer 
anyone else.” ‘Surely this is the result merely of 
your own free choice,” I ventured to suggest, ‘‘ other- 
wise the composer of ‘ Tell’”——— “ Hush,” he replied, 
with a melancholy wave of the hand, “ let us talk no 
more about that! Iam composing continually. Look 
at these shelves closely packed with manuscript ; 
they have all been filled since “Tell.” I publish 
nothing now, and write only because I cannot help it. 
If we were living among the ancients, I should have 
the consolation to know that all this would some day 
be burnt with me, as it is, it will have to find its way 
into the fire without.” I expressed my satisfaction 
at seeing heaped upon his table the works of exclu- 
sively German masters: Beethoven, Gluck, Mozart. 
‘The old composers,” he replied eagerly, * only the 
old ones! They call M. Wagner a great genius. 
Maybe he is. I am quite sure, however, that J shall 
never comprehend him, should I reach the years of 
Noah. Nor would I entertain any doubt as to his 
genius, if only he would have the goodness to venti- 
late it in any other direction but that of music.” 
(Just about that time the malicious observation had 
obtained currency, which Rossini had actually made 
after hearing the overture to ‘“‘ Tannhauser:” “ Si 
c’etait de la musique, ce serait horrible.”) At parting, 
the veteran master requested my company at dinner on 
the following Saturday. ‘‘ I shall cause something very 
confidential to be whispered into your ear,” he said, 
‘‘my new Spanish Romances, but you must not tell any 
one that you heard them!” On the occasion in ques- 
tion I found the master—who appeared to-day quite 
‘‘en belle fourchette””—seated by the chimney with 
the aged Caraffa, in the most pleasant of humours, 
and gravely discussing the manner in which certain 
Sicilian mushrooms, of which we were to partake, 
should be prepared. I will not decide whether 
Rossini’s or Caraffa’s mouth was the more sensual of 
the two, but I could not refrain from smiling when I 
called to mind that the latter’s thick lips had once 
sung the love-message ‘to Alexis,” and had “ kissed 
the rose.” The eyes of both old gentlemen sparkled 
during their “‘ piquante” conversation. A small and 
select company soon assembled. Among them were 
the vivacious and sympathetic Gustave Doré, looking 
with his moustache and chin-beard, his round and 
rosy face, like a German student; the Professor of 
physical science, Davy Marié, and Madame Ludre, 
whose clever lectures on an universal language

426 THE MUSICAL TIMES.—Aprit 1, 1876

Tue death of Signor Puzzi, which occurred, at an 
advanced age, during the past month, again draws 
our attention to the fact that of the well-known artists 
connected with the old King’s Theatre, in the days 
when music was a mere aristocratic luxury, there are 
now but very few left. Signor Puzzi, in addition to 
holding a high position as solo horn-player at the 
fashionable lyrical establishment in the Haymarket, 
was an active agent in making engagements with 
foreign vocalists, one of whom—Mdile. Toso—he not 
only brought over from Italy, but subsequently mar- 
ried. For many years Madame Puzzi’s name has 
been more before the world than that of her husband; 
for her annual concerts have always been held in

News comes to us of a “ Pianoforte Bee” recently 
given at the Soldiers’ Institute, Portsmouth ; and we 
are also informed that a ‘novel idea” of the same 
kind has been announced by Mr. Stroud L. Cocks in 
the metropolis. We have no doubt that the custom 
of holding these public tests of progress in the various 
branches of the art will rapidly grow; but there is 
nothing “ novel” in the idea, for assuredly the meet- 
ings organized by Mr. Willert Beale at the Crystal 
Palace were nothing but ‘‘ Musical Bees” on an 
extensive scale. It would give us much pleasure to 
insert the long account forwarded to us of the Ports- 
mouth gathering, did our space permit; but we may 
mention that the plan there adopted of giving such 
appropriate prizes as Schumann’s pianoforte works, 
Beethoven’s Sonatas, edited by Agnes Zimmermann, 
a fac-simile of Handel’s manuscript of the “‘ Messiah,” 
Stainer and Barrett’s “ Dictionary of Musical Terms,” 
and two volumes of Mozart’s works, is one well worthy 
of imitation at similar meetings

A society to protect us from our friends is scarcely 
one we should imagine to be a positive necessity ; but 
the amicable relationship which has so long existed 
amongst composers, publishers, and executive artists 
has latteriy been so disturbed by the doings of Mr. 
Harry Wall—an account of which was given in our 
last number—that meetings have taken place, and 
an Association formed called the ‘‘ Vocal and 
Dramatic Artists’ Protection Society,” the object of 
which is to acquaint its subscribers with all the 
circumstances connected with the copyright of pub- 
lished works, so that no penalty can be unknowingly 
incurred by the performance of a composition in 
public. Such a Society must have the best wishes 
of those who believe that it is the duty of all who 
claim legal rights to take care that a knowledge of 
the existence of such rights is not systematically held 
back until the moment of enforcing them

On February 26th, the special feature of the afternoon was 
the performance of Mendelssohn’s g5th Psalm. This fine 
work had been previously given at Sydenham, but particular 
interest attached to it on the present occasion from the fact 
that the unpublished chorus which there is every reason to 
believe Mendelssohn intended as the finale was given at 
the end of the work, this being its first public hearing. 
It will be remembered that the printed score of the Psalm 
begins in the key of E flat and ends in G minor, a close, 
which however defensible on esthetic grounds, is certainly 
unsatisfactory from a musical point of view. The new 
finale, which has been published by Novello & Co., in vocal 
score and separate chorus parts in the key of E flat, 
consists of a broad introduction and a spirited fugue ; most 
of the material is taken from the earlier parts of the work. 
The words of this new chorus are taken from the fifth, 
sixth, and seventh verses of the Psalm; and it may perhaps 
have been a feeling that their re-introduction in this place 
was hardly felicitous, which induced the composer to 
suppress this chorus when preparing the work for publication

On the following Saturday (March 4), Herr Joachim 
played Beethoven’s violin concerto in his own unapproach- 
able style. At the same concert his orchestral arrange- 
ment of Schubert’s great Duo in C, Op. 140, was brought

much esteem and liberally patronized

Rubinstein’s ballet music, from his opera, Feramors, 
requires no detailed notice. It consists of three dances 
and a wedding march. The first and second numbers are 
very pretty, especially the latter; the third and fourth are 
much weaker. The style of the whole is rather that of the 
light French opera than of the new German school to 
which most of Rubinstein’s compositions belong

Last Saturday, the 25th, besides a very pleasing overture 
to ‘‘ Euterpe” by the late Charles Edward Horsley, and 
two songs from the same pen, Beethoven’s choral sym- 
phony was given. The performance was, as regards the 
instrumental part, one of the finest ever heard, even under 
Mr. Manns. The choral portion, was, of course, less 
perfect ; but the difficulties of the music are so great that 
this is not to be wondered at: and the vocal performances 
may at least be fairly called satisfactory. The solo music 
was well sung by Mdlle. Johanna Levier, Miss Annie 
Butterworth, Mr. Edward Lloyd, and Signor Foli

PHILHARMONIC SOCIETY

THE first Concert of this Society for the present season 
was given at St. James’s Hall on the 23rd ult., before a 
large audience. The programme, although presenting no 
novelty, was in the highest degree interesting, Schumann’s 
Symphony in C (No. 2) being so finely rendered throughout 
as to elicit the warmest marks of admiration; the ‘‘Scherzo,” 
with its two characteristic and melodious Trios, fairly ex- 
citing the hearers to almost a frenzy of delight. Madame 
Schumann’s performance of Beethoven’s pianoforteConcerto 
in G was, in every respect, so absolutely perfect as to dis- 
arm criticism. Such an intellectual grasp of the composer’s 
meaning, combined with a mastery over executive difficulties 
rarely exhibited, even by this great artist, on former occasions, 
gave to the audience that effect of extempore playing which, 
save in Mendelssohn,—whose rendering of the same Con- 
certo at the same Society’s Concert is distinctly in our 
recollection—we have never before remarked. The double 
recall of the pianist after her retirement from the platform, 
will, we hope, convince her how thoroughly such a mani- 
festation of her exceptional gifts is appreciated in musical 
England. The vocalists were Mdlle. Ida Corani and 
Signor Pollione Ronzi, the latter a tenor from La Scala, 
Milan, with a somewhat thin and not very pleasing voice. 
Mr. Cusins, who was enthusiastically received on his en- 
trance into the orchestra, conducted with much skill and 
judgment

ROYAL ACADEMY OF MUSIC

AN orchestral concert was given by the students of this 
Institution, at St. James’s Hall, on the 18th ult., before a 
large and most appreciative audience. The excellence of 
the pianoforte instruction was strikingly displayed by Mr. 
Morton, in the first movement of Rubinstein’s Concerto in 
D minor; Miss Thurgood, in the first movement of 
Beethoven’s Concerto in E flat; Miss Borton, in the first 
movement of Sir Sterndale Bennett’s Concerto in F minor ; 
and Mr. Matthay, in Schumann’s Concert Allegro in D 
minor (Op. 134), all these clever and highly promising 
young pupils being called forward at the conclusion of their

performance and warmly greeted. Miss Gabrielle Vaillant’s 
rendering of Beethoven’s Romance in G, for violin, was 
remarkable, not only for purity of tone, but for an intelli- 
gent perception of the composer's intention scarcely to beex- 
pected from a student. Only one composition by a pupil was 
given—an Overture in C minor, by Miss Oliveria Prescott— 
the clearness of writing and delicacy of instrumentation in 
which, however, proved that good judgment had been 
exercised in selecting it as a representative work. It was 
perhaps somewhat ambitious to attempt the second part ot 
Handel’s “ Belshazzar ;” but the result must have been 
highly gratifying to the many professors present, under 
whose direction the several parts had been studied. The 
choir was steady and well in tune throughout, the opening 
chorus, ‘See, from his post Euphrates flies,” being 
especially worthy of praise. The solo parts were entrusted 
to Miss Kate Brand, Miss Barkley, Mr. Seligmann and Mr. 
Gordon Gooch, all of whom acquitted themselves of their 
difficult task with much credit, Miss Kate Brand eliciting 
well deserved applause for her refined interpretation of the 
air, “ Regard, O son, my flowing tears,” and Mr. Gordon 
Gooch displaying a good voice and method in the trying 
recitative, in which Daniel expounds the oracle to Bel- 
shazzar. Miss Annie Butterworth sang Stradella’s song, 
“Pieta, signore,”” with good expression. Handel’s aria, 
‘“‘Nasce al Bosco,” gave an opportunity for Mr. Eugene 
Boutenopp, to evidence the possession of an excellent 
voice ; and Miss Jessie Jones, who although still a student, 
is already winning honours out of the Academy, gave 
‘* Hear ye, Israel,” (from ‘ Elijah”) so admirably as to keep 
the majority of the audience in their seats (notwithstanding 
its being the last item in the programme), the following 
chorus, “Be not afraid,” being, in consequence, heard 
without the usual interruptions at the conclusion of a 
concert. The performance was ably conducted by Mr. 
Walter Macfarren

MR. HENRY LESLIE’S CHOIR

THE principal feature in the programme of the concert 
on the oth ult., was the performance of Mendelssohn’s 
music to ‘ Antigone,” which was rendered additionally 
attractive by Mrs. Stirling’s intelligent reading of the text, 
or rather that portion of it which was necessary to connect 
the several pieces of music. The choruses were admirably 
sung, so well indeed as to excite more than the usual 
amount of interest with the audience, the result being that, 
much to the detriment of the work, the ‘‘Hymn to 
Bacchus” and the quartet, ‘‘O Eros,’’ were re-demanded. 
In the second part of the concert Mendelssohn’s “‘ Vintage 
Song ”’ (finely sung by the choir) was encored, and Herr 
Joachim elicited the most enthusiastic marks of approbation 
by one of the finest interpretations of Beethoven’s Violin 
Concerto we ever heard. Mr. Leslie conducted with his 
accustomed care and judgment

MR. WALTER BACHE’S CONCERT

442 THE MUSICAL TIMES.—Aprit 1, 1876

cold,” “Gadeamus igitur,” two new songs by the conductor, “ Regna 
il terror” (Tancredi), and the Barcarolefrom Masaniello. The prin- 
cipal instrumental piece was Mozart’s E flat Symphony, which was 
given with character and feeling. The overtures were Stradella, 
Freischiitz, and Masaniello. A student, Mr. Galletly, gave, in con- 
junction with Mr. Carl Hamilton, a good reading of the adagio and 
finale of Beethoven’s Duet Sonata, for pianoforte and violoncello, in A, 
Op. 69, and further contributed two pianoforte solos, a ‘“‘ Lied” in D flat 
by Esain, and Heller’s Tarantella in A flat. Professor Oakeley 
accompanied the songs on the piano

ENNISKILLEN, Co. FERMANAGH, IRELAND.—The members of Mr. 
Arnold’s Choral Class gave their first Concert on Friday evening, 
February 25th, in the Protestant Hall, when selections from Handel's 
Messiah, Mendelssohn's Elijah, St. Paul, &c., were very creditably

erformed. The solos, “I know that my Redeemer,”,“ Then shall the

ighteous,” “O rest in the Lord,” and “ Lord God of Abraham,” were 
well sung by Miss Bagot, Miss Emily Graham, Miss Graham, and Mr. 
W. R. Cooney. The part-songs included ‘“‘ Blow, ye balmy breezes,” 
Young; “ Ahcould I with fancy stray,” Hatton, (encored) ; and “‘ Sweet 
and low,” Barnby. Songs were also given by Messrs. Arnold, Black, 
Leddall, Trimble, and McKeague. Beethoven’s Sonata in F, Op. 24, 
for piano and violin, was played by Miss Gray and Mr. Arnold, and a 
clarionet Fantasia (Reissiger) by Herr Werner. Mr. Arnold conducted

FREEMANTLE.—A lecture upon Music and Musical Compositions 
was delivered in connection with the Freemantle Mutual Improvement 
Association, at the Church Schoolrooms, on Thursday evening, the 
and ult., by Mr. Thos. Miell, jun. Mr. J. Cockburn, one of the vice- 
presidents, occupied the chair, and introduced the lecturer, stating 
that the proceeds were to be devoted to the augmentation of the library 
fund. Mr. Miell commenced his lecture with a brief notice of the 
history of music from the earliest period, referring to the system of 
notation, the laws of harmony, and in illustration of some remarks on 
operatic music, performed a selection from the opera of Masanjello

MANCHESTER.—The recent Concert of the St. Cecilia Choral 
Society, under the able conductorship of Mr. Hecht, at the Free Trade 
Hall, drew a large and appreciative audience. The members of the 
choir are all, or nearly all, amateurs, but the intelligent manner in 
which they sing suggested greater labours than amateurs are 
generally supposed to give, and it is seldom the good fortune of the

conductor of a non-professional choir to command such a splendid 
array of vocal talent. On no previous occasion has this excellence 
been so equally distributed. The sopranos are as bright and fresh as 
ever, but the other departments of the choir have perhaps never been 
so satisfactory. Bach's Cantata, ‘‘ My Spirit," with which the Concert 
opened, is so pathetic and so characteristic of the genius of the com- 
poser, that it is surprising it is not better known; and it is to be 
regretted that only a single performance of such a work could be heard. 
Schumann’s Faust music was quite a surprise, and if the scenes 
given on this occasion may be accepted as a fair specimen of the whole, 
we have no hesitation in saying that it will attract the English public 
far more than the same composer’s Paradise and the Peri. Schu- 
mann has admirably taken advantage of the mystic character of the 
poem, and his fancy has seldom had a more appropriate subject. 
Sullivan’s part-song, ‘‘ Say Watchman,” is one of his happiest inspira- 
tions, and the “Shepherds’ Chorus,” from Schubert’s Rosamunde 
was welcomed, if only as a contrast to the prevailing sadness of most 
of the first part. An admirable performance of Beethoven’s Concerto 
in C major, by an amateur, must also be mentioned as a feature in the 
programme. The band, carefully led by Mr. Straus, added con- 
siderably to the success of the evening; and the good understanding 
between the choir and the orchestra was very creditable to the 
energetic and accomplished conductor, Mr. Hecht, who could not do 
more for the St. Cecilia Society if it were hisown private undertaking. 
— Under the title of the ‘“ Manchester Cathedral Glee and Choral 
Union” a Society has lately been formed, with every prospect of ulti- 
mate success. It is proposed to give four concerts during the season, 
under the able direction of Mr. J. Kendrick Pyne, organist of the 
Cathedral, who is appointed permanent conductor of the Association

MaIpsTonE.—A sacred Concert was given in the West Borough 
Congregational Church on Friday, Feb. 25th, in aid of the Organ 
Fund. The first part of the programme consisted of selections from 
Handel’s oratorio, fudas Maccabeus, the second part being miscel- 
laneous. The solo vocalists were Mrs. Day, Miss Somerton, Messrs. 
Crowe, Airs, and Jefferies,and Master P. R. Day. Thechoruses were 
given with great precision and care. Mr. T. G. Day conducted

art-songs. Miss Williams had to repeat Carola’s “ So far away.” 
Madame Patey received an ovation in Giordani’s ‘‘Caro mio ben,” 
and Mrs. Buxton, with Miss Gadsden, elicited an encore for Campana’s 
“Tel Rammenti.” The part-songs by the choir, under the careful 
direction of Mr. Latter, were capitally sung

Yarmoutu.—A pianoforte recital and lecture on the great musical 
composers was given on Tuesday evening, Feb. 2gth, in the Town Hall, 
by Herr Louis Loffler; E. P. Youell, Esq.,in the chair. The method 
adopted by Herr Loffler was to give a short lecture on the composers in 
the order of the names on the programme, and to follow this up by a 
performance on the pianoforte of selections from the works of these 
masters. In this way the lecturer dealt with Handel, Mozart, Hummel, 
Beethoven, Weber, Mendelssohn, Liszt, and Thalberg, giving a short 
account of the life of each of these great composers, and pointing out 
its chief incidents. At the conclusion of the recital, the Chairman paid 
the highest compliments to Herr Loffler, and a vote of thanks was 
passed to the Mayor for the use of the hall

Yeovit.—A Concert was given on Thursday, the 16th ult., under the 
conductorship of Mr. Loaring, whena very interesting programme was 
performed, including Handel’s “ How vain is man,” Hullah’s “ Storm,” 
“When the swallows homeward fly,” Abt, ‘‘The Market chorus,” 
Auber, and the overtures to Fra Diavolo and Guy Mannering

