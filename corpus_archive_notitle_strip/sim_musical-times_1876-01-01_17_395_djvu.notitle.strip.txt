formers. Variations in F major Schumann, R.—‘ Al Ballo,” Novelette in D major, 
Weber, C. M. von.—Arioso in D major, for two per- from Op. 61

formers, Op. 6c, No. 1 Beethoven, L. van.—Sonata in D ees for two 
Weber, C. M. von.—Alla Militaire, Allegro in C performers .. be

major, for two performers, Op. 60, 'N 3 Beethoven, L. van. Variations: } in c ‘major, ¢ ona 
Volkmann, R.—Musical Picture Book, Op. 11, Book Theme by Count von Waldstein, for two performers

2, for two performers Schumann, R.—Children’s Ball, for two performers, 
Weber, C. M. von.—Andantino, Grazioso, and Op. 130, Book 1, Polonaise

Arranged for the Organ or Harmonium. 
By JOHN OWEN. 
Each Part One Shilling net

Part 1. VOLUNTARIES; Subjects by Heller, Mendelssohn, Schu- 
mann, Beethoven. 
a ° Veeorae Subjects by Handel, Beethoven, Rossini, 
ozart 
» 3. OPERATIC MELODIES, by Mozart, Donizetti, Auber, 
Verdi, Meyerbeer, &c. 
1. de VOLUNTARIES; Subjects by Spohr, Handel, Mendelssohn 
and Croft

TENTH THOUSAND. 
SONGS, 
HARMONIZED FOR SCHOOL PURPOSES. 
By MICHAEL WATSON

Cuirton.—On the 2nd ult,. Mrs, Viner-Pomeroy gave her annual 
Morning Concert at the Victoria Rooms. The following instru- 
mentalists assisted :—Mr. A. W. Waite (violin), Mr. E. Woodward 
(viola), Mr. J. Pomeroy (violoncello), Mr. {; O. Brooke (clarionet

rt. J. Pimm (contrabasso), Mr. Edwards (French horn), and Mrs. 
Viner-Pomeroy (piano). The programme included selections from 
Schubert, Weber, Beethoven, Servais, and M{cscheles. On Thursday 
evening, the oth ult., Signor Paggi gave a Concert at the Victoria 
Rooms, assisted by his daughters, Mdlle. Paggi (piano), Mdlle. Anita 
Paggi (flute), and Mdlle. Josephine Paggi (violin), and his son Master 
Paggi (violoncello). Miss Brettelle and Mr. J. H. Kearton were the 
vocalists. Signor Paggi's children displayed great musical talent.—— 
On Monday, the 13th ult., Mr. J. C. Daniel introduced to a Clifton 
audience, at the Victoria Rooms, the eminent Russian pianist, 
Madame Annetta Essipoff, who gave « grand recital from the works of 
Mendelssohn, Handel, Chopin, Gluck, Schubert, Bach, Scarlatti, 
Rubinstein, Heller, Liszt, &c. The rendering of the various pieces 
was most highly appreciated

Co.cHESTER.—Mr. Groom's Concert took place on Nov. 29. The 
vocalists were Miss Jessie Royd, Miss Dones, Mr. Stedman, and Mr. 
Thurley Beale, by whom an excellent selection of vocal music, com- 
prising quartetts, duets and solos, was given, each artist being honoured 
with encores. Mr. Groom officiated as accompanist, and was also 
successful in some performances on the harmonium

KEenpaL.—The Choral Society gave a Concert on Monday, the 13th 
ult., in the National School-room, the band and chorus numbering one 
hundred. Handel's Oratorio, the Messiah, was performed in a very 
satisfactory manner. The principal vocalists were Miss Clelland, 
Miss Edith Clelland, Mr. T. J. Harrison, Mr. Grant and Mr. Godfrey. 
Mr. W. B. Armstrong and Mr. Wm. Smallwood were the conductors. 
The proceeds of the concert will be given to the Kendal Industrial 
Exhibition, which takes place next year

LEAMINGTON.—Mr. C. Sydney Vinning gave two classical Chamber 
Concerts at the Royal Music Hall, on November 26. The first part of 
the morning’s performance was selected from the works of Sir 
Sterndale Bennett; the chamber trio, ‘‘Rondo Piacevole,” for 
pianoforte, and a romance for violoncello and pianoforte, being princi- 
pal features. Amongst other works given, were Mozart’s trio in E, 
Op. 15; Sonata, violin and pianoforte (Beethoven), Op. 24; Sonata 
(Beethoven), Op. 14, for pianoforte alone, and other compositions by 
Schumann, O’Leary, Bache and Mayseder. The artists were Mr. 
Henry Hayward (violin), Mr. Joseph Owen (violoncello), Mr. 
Sydney Vinning (pianoforte), and Madame Whitaker (vocalist

LrEeps.—The members of the Arion Quartett Society gave a Concert 
in the Church Institute on Thursday evening, the 16th ult., assisted by 
Miss Tomlinson, Mr. J. H. Walker, Mr. W. S. Hall (flautist), and 
Miss Fanny Arthur (pianist). The concert was an exceedingly good 
one, the part-singing of the quartett, Mr. H. James Coldwell (alto), Mr. 
B, Johnson (first tenor), Mr. G. White (second tenor), and Mr. Tom 
Law (bass), being a great feature

New Octavo Edition

BEETHOVEN'S SONATAS. Edited and Fingered

by AGNES ZIMMERMANN. Paper Covers, 5s. ; Cloth, gilt, 7s. 6d. 
‘Of all the small editions yet issued we must certainly give the 
palm to that before us, which is a very careful reprint of the full-size 
publication edited by Miss Agnes Zimmermann, of which we spoke in 
the highest terms when it first appeared a year ortwosince. This 
volume is not only the best engraved, printed, and bound that we have 
seen—it is far and away the most carefully edited; so that, look where 
we will, and with whatsoever keenness we may, it is impossible to 
discover a mistake. The accomplished editress—one of the most 
distinguished of the later pupils of the Royal Academy of Music—has 
evinced sound judgment in her treatment of questionable matters, and 
has earned the gratitude of future art-students by the patient care she 
has bestowed upon the fingering of this mass of music—no light task 
wher its extent and (in some instances) complicated character are 
borne in mind; and, by her interpolated marks of phrasing, she has 
done much towards rendering the least intelligible of Beethoven's 
intricacies clear and appreciable. All honour, then, to Miss Zimmer- 
mann, who has accomplished her difficult and responsible duty to 
Beethoven's memory and to the public in a manner above all praise, 
and has presented to the world one of the greatest products of the 
human mind with a completeness and thoroughness worthy its 
immortal author.”—The Queen, Oct. 15. 
London: Novello, Ewer and Co., 1, Berners-street, W

USIC BUSINESS for SALE.—To be disposed 
of, with immediate possession, the STOCK and GOODWILL 
of the old-established Business carried on at Cardiff, under the name 
of “Righton and Co.,” and now in liquidation. To suit the conve- 
nience of the purchaser, the connection and goodwill of the business 
will be sold with or without the Pianos and Harmoniums on the pre- 
mises, Also the Lease (99 years) of the commanding and extensive 
Premises, recently improved and enlarged at a considerable expense. 
Altogether, this is a most favourable opportunity for investment in the 
Pianoforte Trade, for any one who has a disposition that way. For 
further particulars apply to the trustee, Mr. Frederick Lucas, Public 
Accountant, 20, Great Marlborough-street, London, W

LONDON: NOVELLO, EWER AND CO

Just Published. / ;Just Published. 
A NEW EDITION (OCTAVO) OF , A NEW EDITION, (OCTAVO) OF 
b 
BEETHOVEN’S HAYDN’S

RUINS OF ATHENS.| yR pEUM LAUDAMUS

