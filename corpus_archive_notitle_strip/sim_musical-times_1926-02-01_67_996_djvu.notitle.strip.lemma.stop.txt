approve 

 GRESHAM COLLEGE , Basinghall Street , E.C.2 . 
 lecture " mind Beethoven , ’' Sir Walford 

 davy , 6 p. m. February 9 , " melody " ; February 10 , 
 " chord " ; February 11 , " rhythm " ; February 12 , 
 " Likeness Shakespeare 

 context . fo suggest context 
 = , 

 composer approach chord 
 form quit second , 
 ordinary pun music ( article ' word - play 
 Music ' Musical Times December , 
 1913 ) . result , broadly - speak , 
 note mean thought note , chord 
 language music mean group 
 thought thought — easy 
 customary mental feat 
 laboriously conscious course beautiful 
 music composite fact , like type- 
 writing organ - playing , short - handing . ( 2 . ) 
 equally clear harmonic 
 thought present ear way 
 different physical effect case 
 muddy unacceptable , 
 acceptable clear . Beethoven 
 fine contempt comfort 
 ear , hurl concord harshly 

 ear , : let chord 

 attention usefully draw 
 book ' Selected Studies Pianoforte ' 
 ( Paterson Publications ) , select , edit , 
 finger , respectively , Désirée MacEwan , Nancy 
 Gilford , Cuthbert Whitemore , Felix Swinstead . 
 grade primary , elementary , Higher 
 elementary , high grade . Mr. Whitemore , 
 book 3 , Miss Gilford book 2 , preface 
 set interesting helpful foreword ; 
 book suggestion 
 head study . particularly helpful teacher 
 beginner comment Miss MacEwan 
 book 1 . G. G 

 new edition BEETHOVEN sonata 

 sight piquant 
 association Casella Beethoven . 
 remember Casella early distinguish 
 asa pianist , distinguished teacher 

 136 MUSICAL TIMES — Fesruary 1 1926 

 edition Beethoven Sonatas ( volume , | _ finished interpretation — remove 
 separate number ) publish Ricordi , under| small scruple question work 
 Casella editorship , praetenap d like Sonatas Beethoven 

 thorough , enterprising , incidentally reveal suggestion modification 

 - day young folk decide s : ean rN ofel 
 carefu 

 te recommend edition warmly . 
 Beethoven number . edition | ~ : : ; 2 o . : 
 | study preface foot - note sufficient 

 open lengthy preface Italian , French,| > . ; : ° 
 ery : _ ' _ « ? | increase revive old enthusiasm 
 English , text , phrasing , dynamic , | = . - 
 ! hye > 47 4 ’ | work . editor thank 
 touch , use pedal , & c. , fully discuss . | andl Galiouad ' 
 copious foot - note occur | " " H. G 

 ably illuminate . Casella pedalling mark 
 interest . player mistake 

 - pedal early Sonatas observe 
 care reserve Casella indicate 
 use fatal accessory . 
 hand dare suggest continuous 
 use fourteen bar movement 
 ' Waldstein , ' ground Beethoven 
 probably want muffled , confused , far - sound — 
 noise music-—from right hand 
 gradually work keyboard , 
 herald return subject . Casella 
 opine effect origin ' title 
 ' Aurora ' sonata . 
 interest suggestion Casella fingering . 
 frequently add note effect 
 advanced player , preface explain 
 Beethoven present special difficulty way 
 keyboard writing bad 

 Chopin , mechanism keyboard 
 guide inspiration accordance hand ; 
 Beethoven music disdainfully dictate super- 
 human humble finger . meet 
 extraordinary demand abnormal mean . 
 necessity finger Beethoven 
 * barbaric ' manner 

 good practical point frequent division 
 awkward passage hand . ' 
 extent probably precedent . 
 , Casella 

 New Year Honour List unusually lavish 
 far music concern . congratulate 
 heartily recipient : Dr. A. Herbert Brewer 
 receive knighthood ; Sir Hugh Allen 
 c.v.o. ; Victorian Order new 
 member Mr. W. Barclay Squire Mr. Sydney H. 
 Nicholson . hope list recognise 
 claim composer long 
 overlook 

 excellent work matter 
 child concert confine London 
 large provincial city . 
 receive programme concert 
 Exeter Chamber Music Club recently , 
 thousand elementary school child thoroughly 
 enjoy Beethoven String Quartet G , Op . 18 
 ( movement ) , Scherzo Brahms F minor 
 Pianoforte Quintet , Schumann ' Papillons , ' song 
 duet Stanford , Vaughan Williams , Armstrong 
 Gibbs , Martin Shaw , Roger Quilter . 
 interval youngster group 
 folk - song . practically largish town 
 group chamber music enthusiast ; 
 capital field energy . present 
 readily assume valuable work 

 recent publication ordinary letterpress 
 authoritative work Braille music notation 
 great importance teacher , friend , 
 relative blind musician . book 
 question , ' Key Braille Music Notation , ' 
 issue National ! Institute Blind 
 Messrs. Novello . _ letterpress version 
 Key publish Braille Institute 
 1922 . Key embody result year ’ 
 work representative committee expert , 
 regard definitive . Braille system 
 notation comprehensive 
 translate score , simple 
 | elaborate , staff , tonic sol - fa , plain- 
 song notation . letterpress Key enab‘e 
 sighted reader quickly conversant 
 system , thank numerous illustration , 
 excellent paragraphing , copious synopsis 
 index . Mr. H. C. Warri'ow , Institute 
 Organist Musical Director , contribute 
 interesting preface , Sir Landon Ronald 
 come sympathetic foreword . reader wish 
 know history development 
 Braille sign apply music find 
 matter fully deal September ( 1925 

 Andante Mistico ' fine ' 48 ' 
 Preludes . effective version , hardly | 
 expressive good pianist . ) 
 Finale wonderful example Bach power 
 little . slight thematic 
 germ spin splendidly jubilant movement 
 flag . introduction expansion | 
 Bach movement Violin Sonata | 
 E , interest note cantata to| 
 belong compose ' Rathswal , ' | 
 election councillor — inspiring occasion , 
 think , lead Bach fine 
 cantata ( l1684 - 5 

 Beethoven early Concerto C minor , 
 William Murdoch soloist Sir Hamilton Harty | 
 conducting , suffer dead level power 
 come m / f practically . music | 
 Beethoven good , ill afford | 
 loss interest result . playing round | 
 capital , pianoforte tone clear ( l1686 - 9 

 far successful matter variety | 
 Brahms Quartet minor , play Léner | 
 party . 
 way , music , usual Brahms , | 
 gain acquaintance . 
 gramophone score . wait 
 opportunity hear Quartet concert | 
 room , live long } 
 repeat hearing music need . you| 
 sit turn till know inside | 
 ( l1691 - 4 

 behave like maiden likely suitor Bi OP eat ee ee RS 
 introduce . thaw Se pga oe ce 
 MUSIC 

 delightful . free hand regard cut : ee . ae 7 p : 
 ' Pelléas . ' . . . sayshe incapable understanding November Zertschrift fiir Musik , Jon Leifs 
 music ; confront Beethoven Symphony | sober method usual 
 blind man confront display painting . | interpretation classical masterpiece 

 date 

 case point , movement * Eroica 
 . . : select . Beethoven prescribe ¢emfo : 
 day , feel pain 
 concern String ( Quariet . feel 
 work fond 
 certain thing , hope 
 forget thing . , shall write , 
 , , shall try 
 achieve great nobility form . wish | 
 influence good wigging , 
 tell wrong regard . 
 exercise great pressure idea , 
 poor thing long dare 
 , fear lest raiment strike 
 unsuitable . relax yield , 
 allow free hand mysterious prompting — | ; 
 lead find true expression feeling Players Piano Wotes 
 industrious study heart searching 
 sure merely weaken . feel certain 
 endowed needful power expression 
 grieve waste vitality useless | 
 struggle . realise compare art , duo - Art.—1 wait long 
 absolutely ; instrument complete joy Schumann ' Etudes Symphoniques . ' 
 destiny , behove allow destiny " - " ' rar nee : ps Ty , , 
 fulfil . , second roll , contain . 8 - 13 , issue 
 |month ( 6868 ) , , think , fine . 
 include triumphant 
 Finale , sforsando effect , rarely 
 |satisfactory reproduce piano , 
 Warsaw .Wusyéa ( December ) , Andréi| bring . , dynamic variation 
 Rimsky - Korsakov write : different section contrasted , 
 tone occasionally err heavy 
 Borodin Symphony ( publish pianoforte duet notably , , Variation . 10 . 
 form ) revised version ( | roll interest type music - lover , 
 version publish score form ) work — | cordially recommend far wide . 
 Borodin ; Mr. Calvocoressi supposition | cost 24 . , solid joy _ 
 Rimsky - Korsakov Glazounov responsible | treasure . 
 altogether unfounded . time write second 
 [ Norre.—Andréi Rimsky - Korsakov prepare | movement Beethoven sonata e flat , 
 publication article point , | Op . 81 ( 6909 ) . complete work play 
 Guiomar Novaes , , judge , provide 

 dilegro con brie , 7a , = 60 , ' imply _ brisk , 
 lively character . - day hear move- 
 | ment far slow , change ¢empo , 
 | pace prescribe Beethoven 
 | reach . liberty 
 mention exaggeration nwance indicate 
 | composer , introduction nuance 
 | prescribe , overlooking 
 | nuance prescribe 

 M.-D. CALVOCOREsSI 

 kindly communicate text . shall 

 refer soon appears.—M.-D. C. ] jan admirable performance thereof . title 
 |of ' L’adieu , Vabsence , et le retour , ' , suppose , 
 OLD HUNGARIAN MUSIC know later Beethoven work 

 December Zeitschrift fiir Musikwissenschaft | important addition Duo - Art library . | 
 appear second instalment Benedict Szabolesi | Friedman play Schubert - Liszt * Erl - King 
 useful study old hungarian music . ( 6407 ) great power verve . bergen 
 article devote Hymns Psalms 17th | dramatic intensity evoke music , 
 18th century . : lyrical section play way impress 
 anew beauty . < fine roll . 
 RICHARD STRAUSS CADENCES Salon music represent Moszkowski 
 ' En Automne , ' Op . 36 , . 4 ( 6174 ) , brilliantly play 
 Hans Ebell ; reveal composer 
 characteristic mood . 
 |tribute wind ( ' whisper , ' time ) 
 SCHUBERT NUMBER | ~ Mazurka Caprice , ' Wollenhaupt ( 6914 ) . 
 | ' — j ac 

 Backhaus play Carl Reinecke Concerto F sharp 
 ( § 5580/2 ) brilliantly , afraid work 
 academic inspire . inspiration 
 come pretty plainly Mendelssohn — 
 pleasantly flowing , melodious , con- 
 structe . case love 
 concerto player piano — frequently 
 hear music note 

 roll find interesting — 
 variation G , Beethoven ( 52700 ) , 

 theory practice superlatively good , | Tchaikovsky Nocturne , Op . 10 , . 1 ( 57551 ) , 
 performance time supremely refined , | obviously immature work 

 1926 151 

 Kit Cat Club Dance Bands come _ . 
 Sir Walford talk occupy minute , 
 music 
 grumbler comprehension Mozart Allegro , 
 Handel Minuet , Seherszo Beethoven 
 Sonata . 3 , - know hymn - tune ! 
 occasion talk ( January 19 ) 
 variety item dramatic monologue 8.5 
 g , 9 g 10 music Plunket Greene 
 , Ciro Club Dance Band follow 
 exhausting ordeal listen Sir Walford 
 minute . letter example 
 kind selfishness discuss 
 column . musician grudge 
 non - musical friend Radio Radiance , 
 Roosters , Dance Bands , John Henry , relaying 
 musical comedy revue , dozen similar 
 light item . want , leave 
 . , writer letter 
 forget probably hard 
 day work , rest necessarily 
 mean torpor , mental physical . 
 far shape new interest change 
 occupation . hope writer observe 
 letter Zhe Times appear 
 Continental programme . fatigued 
 glance , probably feature 
 kind object regular 
 evening arrangement abroad . hope 
 B.B.C. influence grumbler , , 
 incapable mental 
 effort 8 p.m. , readily assume 
 

 Church Organ Music 

 understand prophet - day | 
 inclined poke fun certain old stock type | 
 musical expression . , ' Tzigane , ' Ravel | 
 parody chief feature - familiar | 
 hungarian rhapsody fantasia . Milhaud | 
 ' Rag Caprice ' parody sentimental romance 
 19th century . Poulenc , impudent | 
 amusing waltz , caricature commonplace | 
 bass average waltz 

 Beethoven music find essential , | 
 Sir Walford Davies lecture Carlisle | 
 ' music Human nature’—rhythm turn | 
 disciplined action , fine form , air 
 akin mystery . Beethoven commence new era 
 music time thing pompous | 
 formal . Beethoven break , and| 
 Vaughan Williams Holst | 
 - day 

 certain thing quicken pulse , | 
 . fashion Prof. T. H. | 
 Pear , Manchester University , illuminate | 
 remark Liverpool ' psychology Music | 
 appreciation . ' person experience 
 bodily thrill miserable creature . | 
 People talk swoon excitement , | 
 feel year ’ young hear certain 
 music , regard certain 
 contempt high critic ; , 
 people music originally 
 invent . long ago , Prof. Pear , train | 
 conversation Norfolk farmer , tell 
 possession wireless set month | 
 introduce Beethoven , Bach , Debussy . | 
 farmer speak composer bate 
 breath , know practically 
 music technically . painting , | 
 sculpture , literature extremely | 
 direct appeal young child rhythm music 

 use music ? ' intriguing | 
 title delightful address Wigan | 
 Mr. Arthur Hirst , experienced lecturer . brief 
 answer : music pure pleasure , utilitarian | 
 teaching music , financial gain 
 command musical genius , , important , 
 music actually healing agent . touching | 
 example lecturer experience . | 
 superintendent mental hospital 
 therapeutic value music measure . 
 restless mental patient , Mr. Hirst find 
 attentive . man notice sit 
 apart , gloomily dark frown , end 
 lecture furrow brow . 
 look like man great weight | 
 lift mind , step forward | 
 shake hand , _ 
 enjoyable evening life . 
 expression face entire change , 
 completely new spirit enter . 
 stimulus music Mr. Hirst illustrate 
 little girl , listless study . teacher 
 try play , latent talent 
 discover , result girl improve 

 L.S.O. begin new year great style . 
 Sir Thomas Beecham conductor 
 concert January 18 , Arthur Catterall 
 soloist . performance 
 remember . Sir Thomas begin little- 
 know Overture Mozart write comic- 
 opera ' L’Impresario’—not Mozart 
 great , unquestionably Mozart . 
 conductor reading colour vitality , 
 wonderfully clear . 
 passage violin lead rest 
 string half semiquaver . 
 exception . unanimity , generally 
 speak , impeccable Delius finely- 
 work symphonic poem ' Summer Garden . ' 
 come Mr. Arthur Catterall , choose 
 occasion trying Violin 
 Concertos — Brahms . provide 
 final test violin playing interpretation , 
 Catterall come flying colour . 
 exquisite charm phrasing Adagio 
 tender episode movement tell 
 extraordinary effect , audience rouse 
 enthusiasm seldom , 
 case favourite long standing . , , 
 obvious feature performance . 
 important , , revelation 
 exceedingly fine , sensitive temperament ally 
 technical skill high order . 
 delight watch Catterall hurdle 
 — pretty stiff — 
 confidence consummate mastery _ perfect 
 understanding . delight enhance 
 modesty demeanour praise 
 highly . till absence like 
 pose mistake want personal 
 style ; wonderful performance kill 
 legend conclusively . incidentally note 
 ensemble understanding 
 soloist orchestra conduct Sir Thomas 
 flawless — far case 
 little ago Brahms Concerto 
 accompany orchestra Bruno Walter 

 interval come second Symphony 
 Beethoven Tchaikovsky ' Francesca da Rimini . ' 
 Sir Thomas Beecham easily strong 
 individual style enliven bring 
 excellence seldom - play Symphony . , 
 performance describe 
 - discovery , 
 pack hall enjoy time . 
 bring conductor orchestra 
 hearty acknowledgment , fine frenzy 
 Tchaikovsky ' Francesca . ' altogether 
 concert expectation arouse 
 - arrange programme surpass 
 wholly admirable performance . B. V 

 new SUNDAY CONCERTS 

 

 Belfast correspondent write defend Vatican 
 Choir stricture ' Feste ' : ' far ahead 
 choir Bach , Beethoven , Wagner 
 english composer live . . . . 
 average English Cathedral choir innocuous , hooting , 
 expressionless . " know ! regard 
 | Vatican Choir , * Feste ' point certain feature — 
 | shrill , forced tone treble , shouting 
 | tenor , fault result lack blend . 
 |have allude ad caftandum trick 
 | prolonged decrescendo cadence effect 
 | text music , exaggerated 
 | dynamic general . everybody agree point , 
 | critic excuse ground nationality 
 } temperament . common writer , 
 * feste ' hold hold virtuosity 
 | Choir fine choice music alter fact 
 method detail bad , 
 | dislike accordingly . Belfast friend think 
 good like , ’ 

 

 opening week current term inaugurate 
 address Prof. Granville Bantock , choose 
 subject * ' Reminiscences Sibelius ' ( page 153 ) . 
 usual function , crowded attendance 
 student 

 occasion choose present diploma BEDFORD School , November 26 , Dr. Harding , 
 student successful recent } performance ' Hervé Riel ' ( Walford Davies ) , 
 examination , medal prize award | Symphony B flat ( movement ) , ( Schubert ) , Pianoforte 
 proficiency study , & c. ; introduce to| Concerto C minor ( movement ) , ( Beethoven ) , 
 member College Board successful candidate | pianoforte violin solo , song , - song . 
 tl : iti scholarship Decembe : ' 
 open competition scholarship hold December ) BERKHAMSTED , November 7 , concert 

 sit : 8 " = Pl dsgpttonstn eg school Berkhamsted District Festival 

 7 " 3 : . movement ) , Chopin Polonaise C sharp minor , 
 David Nasmith silver medal Roberta Genevieve Ss . ma   o . 
 : . Saale Scharwenka Polish Dance e flat minor . boy 
 Spencer , gold medal 1925 Elga V. Collins , K 
 ae 5 . play movement Mendelssohn Concerto 
 Chappell gold medal Vera Snare , F.T.C.L. 2 : ie 
 . " g minor . orchestral item Mendelssohn ' Ruy 

 Open Scholarships ( year ): Violet Annear , George ' .¢ ' - ? 
 wats - gg , Blas ’ Overture , Beethoven Symphony . 1 ( move- 
 Edwin Ansell , Harry Blech , Albert Bregman , Phyllis Mary . . ¢o ' 

 ee ae , " oe . ' | ment ) , Gounod ' Funeral March Marionette . 
 Burgess , Eric Cecil Coleridge , Constance Edinborough , 
 Dorothy Nellie Fox , Leslie Noel Fumasoli , Reginald 
 Horace Gibbs , Dora Gilson , Joshua Goldstein , Max 
 Kossovsky , Leim , Gastone Marinari , Margaret 
 Mercia Morgan , Clement P. Peters , Vera Alice Blanche 
 Rees , Roberts , Winifred Alice Saunders , Mary 
 Thomas , Israel Treger , Ted Warburton , Yetta Waxman.| Curist Hosprrat , Hertford , December 10 , 

 DAUNTSEY COLLEGF , Wilts , small school 
 boy , enthusiastic direction 
 music - master , Mr. J. A. Davison , fast build 
 tradition good music . Musical Society , com- 
 prise - quarter School , assist 
 master , end - - term concert capital per- 
 formance * Elijah , ' Psalm Holst , 
 madrigal Byrd , Vaughan Williams , Holst 

 Eton COLLEGE Musical Society concert , 
 December 19 , consist selection * L’Allegro ed 
 ASSOCIATED BOARD : AWARD ! Pensieroso ' ( Handel ) , chorus ' Gipsy Life ' 
 ( Schumann ) , * Silver Swan ' ( Gibbons ) , * lovely 
 MEDALS Phyllis ’ ( Pearsall ) , movement Bach Concerto 
 C , pianoforte string , movement 
 Beethoven second Symphony , song , pianoforte 
 violin solo . December 20 , Chapel , 
 service carol Christmas hymn 

 follow candidate gain gold silver 
 medal offer Board high second 
 high . honour mark , respectively , final , advanced , 
 intermediate grade Local Centre Examinations 
 November - December , competition open Leys ScHoot Musical Society 
 candidate british Isies : final grade gold } annual Bach recital November 29 , programme 
 medal , Martha B. Erwin , Belfast centre ( pianoforte ) ; | contain Church Cantatas . 56 , ' burden 
 final grade silver medal , Cyril J. Smith , Middlesbrough | gladly carry , ' , 6 , ' bide , ' Sinfonia 
 centre ( pianoforte ) ; Advanced Grade gold medal , Freda | introductory Church Cantata . 42 , Organ Prelude 
 Firth , Great Malvern centre ( pianoforte ) ; Advanced Grade | hymn , * glory God high , ' excerpt 
 silver medal , Margaret Harris , Bristol centre ( pianoforte ) , | Church Cantata . 113 . carol service hold 
 Mary Chandler , Torquay centre ( pianoforte ) ( | Chapel December 13 , include carol , * 
 candidate gain equal number mark ) ; | Babe bear Bethlehem ' ( ' Pie Cantiones ' ) , ' shepherd 
 Intermediate Grade gold medal , Margaret S. D. Lyell , | field abiding ' ( French ) , ' Dulci Jubilo , ' * 
 Perth centre ( pianoforte ) , Joan R. I. Manning , Yeovil | rose virtue ' ( Arnold Bax ) , * descend 
 centre ( pianoforte ) ( candidate gain equal | heav’n , ye angel , come ' ( 1588 ) . Orpheus Club meet 
 number mark ) ; Intermediate Grade silver medal , | weekly Sunday evening , pianoforte song 
 William D. Rees , Newport ( Mon . ) centre ( pianoforte ) . recital paper read 

 SHEBBEAR COLLEGE Choir ( North Devon ) 
 concert Christmas music December 14 15 , 
 direction Mr. G. A. Armstrong 

 WELLINGTON COLLEGE music , direction | ... en ee ee ee . 
 Mr. Stanton , activity . | rhe gacapeeas es cone — al _ — cinsees , 
 concert , November 8 ( choral ) November 22 | Quick Study class , singing 
 ( orchestral ) . programme consist | pianoforte accompaniment , 
 unison , two- , three- , - song song ; regard sight - singing Newcastle grasp 
 treble , range Arne Walford Davies ; | nettle firmly year ago . subject com- 
 second , * Magic Flute ' Overture , movement pulsory school class , maximum 
 symphony C minor ( Brahms ) , movement | mark add total gain song . 
 symphony C minor ( Beethoven ) , serenade string | plan intimidate , judge 
 ( Elgar ) , introduction Act 3 * Lohengrin , ' Symphony f wager unre tes te chenete aft aoe 
 G minor ( Mozart ) , ' Valse Triste ' ( Sibelius ) , trom large entry class affectec reve 
 ' Rakoczy March ' ( Berlioz ) , glee singing | ' * lead remarkably high standard sight- 
 competition October 31 ; Miss Myra Hess a/ reading fetish subject 

 clas 
 fed 
 

 BaTH.—At Pump Room following work 
 appear programme : overture * Phédre , ' Bach 

 Concerto violin , Beethoven sixth Symphony 

 December 30 ) ; Bach B minor Suite flute string 

 ToRQUAY.—At Symphony concert year , 
 January 7 , Mr. Ernest W. Goss conduct suite C , 
 arrange W. Y. Hurlstone Purcell harpsichord 
 music . soloist occasion Mr. Lionel Tertis , 
 — — -Elgar ' daughter ancient king ' Cornelius 
 " surrender soul ' sing Dr. Harold 
 Rhodes concert Philharmonic Society 

 WorcesTER . — Choral Society , Sir Ivor Atkins , 
 " Dream Jubal ' concert betore 
 Christmas . — — orchestral piece Mr. J. W. Austin 
 | play Symphony Orchestra , 
 | composer conductor , December 13 . Symphony 
 Beethoven 

 YARMOUTH.—The Musical Society open sixtieth 
 season December 16 ' Caractacus , ' 

 Music Scotland 

 EvDINBURGH.—The Paterson Orchestral concert , 
 Scottish Orchestra Mlynarski _ Talich , 
 follow general line Glasgow concert 
 ( page 167 ) , special feature ( ) appearance 
 guest - conductor Dr. Malcolm Sargent , 
 favourable impression programme include 
 Tchaikovsky fourth Symphony short work 
 Stravinsky , Delius , Mendelssohn , Rossini ; ( 4 ) 
 Christmas concert child , M. Mlynarski 
 present series national dance european nation . 
 — — Prof. Tovey Sunday evening concert Synod 
 Hall include chamber concert , programme 
 contain Schubert * Trout ’ Quintet , Mozart Pianoforte 
 Trio G major , rarely - hear Saint - Saéns Septet 
 pianoforte , trumpet , string ; orchestral concert 
 Reid Orchestra , Beethoven programme 
 comprise fifth Symphony , ' Coriolanus ' Overture , 
 Violin Concerto ( soloist , M. Angel Grande , 
 spanish violinist ) ; recital sonata violin 
 pianoforte , M. Lidus van Giltay Prof. Tovey 
 play example Mozart , Brahms , César Franck . — — 
 concert season Edinburgh Amateur 
 Orchestral Society , - fourth year , play 
 movement Brahms Symphony , ' Leonora ' 
 . 3 Overture , ' Pastoral ' Symphony Bach 
 Christmas Oratorio , Tchaikovsky ' Sleeping Beauty ' Suite , 
 Peter Warlock Serenade string . Mr. Ralph 
 T. Langley débft Society conductor , 
 good work , programme prove over- 
 ambitious resource available.———At , 
 good , chamber concert Falconer 
 String Quartet , combination play Ravel Quartet , 
 join Prof. Tovey Brahms Pianoforte 
 ( Quintet Pianoforte Quartet Prof. Tovey . 
 Edinburgh Royal Choral Union ( Mr. Greenhouse Allt ) 
 sing ' Messiah ' New Year Day , scottish 
 Choir scottish concert - song solo 
 evening . Mr. Moonie Choir ( Mr. W. B. Moonie ) 
 performance ' creation , ' accompaniment 
 play Edinburgh Amateur Orchestral Society , 
 later month sing ' Messiah . ' Edinburgh 
 Male - Voice Choir ( Mr. A. Russell ) , annual concert 
 sing varied selection - song , collaborate 
 Mr. Robert Burnett Stanford ' Songs Sea . ' 
 North British Railway Musical Association ( Mr. A. Russell ) 
 choral concert interesting item 
 Stanford ' blue Bird ' Bantock wonderful 
 setting hebridean ' Sea Sorrow . ' choral concert 
 recently - form Edinburgh William 
 Morris Choir ( Mr. G. W. Crawford 

 Faust ' impromptu ending 

 1926 167 

 GLascow.—Emil Mlynarski , follow Hermann 
 Abendroth Felix Weingartner visit conductor 
 Scottish Orchestra , direct concert , 
 devote Wagner musically point 
 audience successful . Tchaikovsky fifth 
 Symphony , highly - coloured work , M. Mlynarski 
 old tendency - emphasise emotional 
 music expense form , balance , line 
 pronounced , stirring 
 performance Strauss ' Zarathustra . ' Mr. Philip 
 Halstead , doyen Glasgow pianist , welcome 
 long - overdue appearance pianoforte soloist , play 
 D minor Concerto Mozart charming delicacy 
 lucidity , join wood - wind leader 
 performance movement Beethoven Wind 
 Quainet . young polish violinist , Mlle . Irene Dobiska , 
 introduce M. Mlynarski new country , 
 hear Beethoven Violin Concerto solo , 
 despite creditable playing , fall definitely 
 requirement Concerto general standard 
 Tuesday concert . Glasgow Choral Union 
 Scottish Orchestra join force programme 
 include Brahms ' Requiem , ' Debussy ' blessed 
 Damozel , ' woman voice , Coronation Scene 
 ' Boris , ' vocal solo Mozart Schubert — 
 mix banquet . regrettable absence , owe 
 illness , Mr. Wilfrid Senior , Mr. Hutton Malcolm 
 conduct , good solid choral singing , 
 genera ! effect level point dullness , 
 orchestra effective control . choir 
 good work ' Boris ' excerpt . work 
 choir orchestra New Year Day performance 
 ' Messiah , ' Mr. Hutton Malcolm conduct , 
 standard maintain repeat performance 
 fortnight later . Talich succeed Mlynarski conductor 
 Scottish Orchestra New Year evening , 
 - establish good - round conductor 
 Orchestra Kussewitzky . second 
 concert , inspiring reading César Franck 
 Symphony sweep audience foot , later 
 concert ' New World ' Symphony Dvorak 
 Vaughan Williams Suite , ' wasp , ' notable 
 performance . Schubert ' unfinished ' Symphony 
 unduly slow ¢emfo superfluous point - making 
 rob music lyrical charm , 
 d major Suite ( . 3 ) Bach entirely free 
 similar interruption . Saturday evening Talich 
 adopt ' national ' basis , turn French , 
 russian , czecho - slovakian programme . scheme 
 describe great success , probably 
 intrinsically unsound . certainly introduce lot 
 music play ' time Scotland , ' 
 time Britain , fail 
 justify ' national ' ground , 
 agree heartily distinguished british composer 
 , listen impatiently 
 ' national ' programme , ejaculate , ' decent 
 british programme , simply obliterate . ' 
 honourable exception Tanéiév interesting 
 ' Orestes ' Overture . M. Lidus van Giltay , dutch 
 violinist , appear concert , play 
 major Violin Concerto Mozart impeccable 
 technique , great purity style , complete 
 absence vibrato.——the Glasgow Bach Choir , 
 Mr. J. Michael Diack conduct , sing Christmas 
 Oratorio ( abridge ) , Glasgow Cathedral , 
 strikingly fine choral pastiche comprise chorus select 
 Bach lesser - know Church cantata , 
 skilfully appropriately fit word Psalm 21 
 Ivor Atkins J. Michael Diack . item 
 worth attention choir look 
 attractive difficult short choral work.——The 
 Glasgow Amateur Orchestral Society , old - establish 
 musica ! body vicissitude 
 history , excellent work direction 
 Mr. J. Peebles Conn , capital appearance 
 programme include fiéce de résistance Schumann 
 second Symphony.——The Glasgow Orpheus Junior Choir 
 ( Mrs. Catherine Armstrong ) , annual concert pre- 
 sente ambitious programme unison 

 two- , three- , - song , sing 
 fine musical intelligence insight 
 naturally associate . 
 tone soundly base , intona- 
 tion frequently far secure . Glasgow 
 Orpheus * Sangspiel ' young child , direct Miss 
 Ella Voysey , fine work folk - dance , 
 classical dance , action - song . outstanding thing 
 classical dance , ' Timbrel ' ' 
 Gleaners , ' ' mime dance ' presentation 
 scene — * Morning ' ' Gnome King Hall ' — 
 * Peer Gynt . ’—-—Other concert include perform- 
 ance ' Messiah ' large Y.M.C.A. Choir 
 ( Mr. Hugh Hunter ) , choral concert Glasgow 
 Tramways Choir ( Mr. Wilfrid Phillips 

 BRIDGEND , Boxing Day ninth annual 
 eisteddfod hold Town Hall , Messrs. 
 Haydn Morris , Carmarthen , T. J. Harries , Rhymney , 
 musical adjudicator 

 CARpDIFF.—On December 27 , Mr. Herbert Ware 
 orchestra Beethoven eighth Symphony , con- 
 tinuation series commence 
 year ago . vocalist Mr. John 
 believe forthcoming performance ninth 
 Symphony hear Wales 

 Cwmparc , Ruonppa,—At Bethel English Baptist 
 Church , Christmas Day , choir 
 Dr. Challinor * Green Light ' 
 audience . conductor Mr. Tom Jones 

 LONDON STRING QUARTET 

 recent visit London String ( Quartet 
 Berlin fail stir curiosity music - lover , 
 place Christmas time — , course , 
 favourable period new - comer . 
 unfavourable circumstance programme . L.S.Q. 
 prove respectful chronological order 
 Beethoven Quartets . lose habit hold 
 adivine Beethoven service . Beethoven festival 
 announce London player ought 
 present work character 
 evening . , instance , doubtful 
 item belong . 18 claim 
 attention . , detriment 
 general impression , overlook . spite 
 militate success tour , chamber 
 music association win hearty applause musical 
 public . time England — , 
 german view , hitherto lag european 
 nation field executive art — send missionary 
 chamber music Germany . , course , matter 
 wonder Germany think possess 
 true Beethoven style . audience 
 appreciative perfect sonority ensemble , 
 express certain aristocratic austerity 
 imply reserve feeling . player advance 
 Beethoven work , warm 
 interpretation ; prove highly 
 representative english chamber music , 
 good chamber music organization world 

 , fortnight , Léner Quartet 
 second appearance Berlin , racial difference 
 group evident . Quartet , come 
 South , preserve southern 
 temperamental quality , exhibit refined 
 style spell - rate 
 chamber music 

 England , norse composer , Hjalmar Borgstrom , 
 scarcely , production 
 tone - poem , ' Hamlet , ' pianoforte orchestra , 
 Utrecht Town Orchestra , introduce new element 
 inte programme . young conductor , 
 J. L. Mowinkel , jun . , work , believe , compara- 
 tively early example , national element 
 ' Borkman ' Symphony , Ibsen , 
 psychological ' mind Man ' poem . , 
 impression composer power 
 imagination . conductor real ability , 
 somewhat immature method idea 

 new combination designate ' Dutch Trio , ' 
 consist pianist , Madame F. Tabakspinder- Roeper , 
 violinist , Madame B. van Breemen - Schrik , ' cellist , 
 Henk van Wezel , promising débit 
 Amsterdam programme consist work 
 Mozart , Beethoven , Fauré . player 
 achieve perfect ensemble idea execution , 
 seriousness apply 
 work , real ability individual artist , 
 suggest wil ! come 

 hopeful sigh prejudice remove occur 
 criticism musical editor ' Amsterdam 
 Handelsblad , Herman Rutters , singing German 
 Handel oratorio dutch chorus 

 objection french italian , English 
 insuperable , constantly 
 impression laxity lack 
 goodwill . case , original language , 
 german translation dutch ? 
 ' ' Deutschtum " Handel 
 english oratorio . ' HERBERT ANTCLIFFE . 
 NEW YORK 

 symphony Beethoven , Brahms , Tchaikovsky 
 imperishably great , repetition 
 season weary jaded concert - goer , 
 welcome relief delight Nicolai 
 Sokoloff present Sibelius Symphony principal 
 feature concert recently bring 
 Cleveland Orchestra New York . hear 
 Sibelius , principally Monteux Stransky , 
 occasionally Stokowski , Philadelphia Orchestra 
 play . 5 New York concert winter . 
 Sokoloff choose , 
 immaturity compare later example , 
 marvellous composition abound musical idea 
 energetic expression . performance talent 
 leader interpreter drill - master 
 constantly evidence . Sokoloff appear 
 guest - conductor Stadium summer , work 
 Philharmonic Orchestra receive high favour , 
 greatly enhance hear man 
 winter respond clearly idea , 
 technical perfection . report reach straitened 
 condition Sibelius life Helsingfors , fund 
 start New York 
 prominent musician relief , chief conductor 
 head list — testimony , , admire 
 work finnish composer . success 

 Cleveland Orchestra lead wish 
 guest - conductor appear Stadium ( Mr. Ganz , 
 St. Louis , Mr. Reiner , Cincinnati ) 
 bring orchestra play — change 
 opinion capability , 
 confirm value exponent 
 conductor art . Mr. Reiner direct small orchestra 
 concert International Composers ’ Guild , 
 programme trivial ugly , exception 
 Casella ' Pupuzzetti . ' music puppet real 
 music , simple poetic , vitality modern , 
 item worth Mr. Reiner attention , 
 Casella worth mention . 
 aim Society good , modern 
 composer deserve hearing , line draw 
 work indicate talent possible 
 future , futile thing ' Guild serve . 
 American League Composers offer recently 
 performance America ' El Retablo de Maese Pedro , ' 
 Manuel de Falla , think great 
 live spanish composer . marionette opera 
 perform puppet large small , 
 sing . Gabrilowitsch 
 bring Detroit Orchestra city , frequently 
 appear pianoforte recital soloist 
 orchestra . recent performance Beethoven 
 E flat Philharmonic Orchestra produce 
 beautiful reading ' Emperor ' hear , 
 little disturbing use 
 right hand employ key . 
 apparent accustom play 
 Concerto conduct orchestra 
 time . hope Mengelberg mind duty 
 interfere 

 certainly world great pianist 
 winter , hard dispute supremacy 
 Messrs. Hofmann , Rachmaninov , Paderewski , add 
 Gabrilowitsch . Russians 
 Poles defy rivalry . prefer , prefer 
 , fair 
 Hofmann pianist . owe 
 political fame , add great musicianship , 
 Paderewski occupy unique position . finger 
 nimble year ago — 
 passage blur — surpass 
 recent performance Chopin Mazurka ; big 
 heart time political ally . 
 year european soldier receive penny 
 instrument , season american 
 Legion , estimate 
 receipt concert twenty- 
 thousand dollar , record 
 recital yield - thousand , 
 series 

 OLD READER.—(Coustant old , trust ! ) 
 | paper history Church music 
 | consult , addition work , historical 
 | Edition Hlymns A. & M. ( Clowes ) , Curwen ' Studies 
 Worship Music ' ( print , copy 
 borrow pick second - hand ) ; Bumpus ' History 
 English Cathedral Music ' ( Werner Laurie ) ; Myles B. 
 Foster ' Anthems Anthem Composers ' ( Novello ) ; 
 John E. West ' Cathedral Organists past present ' 
 | ( Novello ) ; Ernest Walker ' History Music England ' 
 | ( Oxford University Press ) ; Grattan Flood ' early Tudor 
 Composers ’ ( Oxford University Press ) ; Lightwood 
 * Hymn - Tunes story ' ( Epworth Press ) ; Bridge 
 | * Westminster Pilgrim ’ ( Novello ) ; biographical 
 article ' Grove , ' forget ' Plainsong . ' 
 include list certain book 
 | value anecdotal historical , 
 | order lecture light relief 

 M. D. d.—(1 . ) book form contain good deal 
 | reference sonata form use Mozart 
 | Beethoven . consult Hadow ' sonata form ' ( Novello ) , 
 Harding ' Analysis Beethoven sonata ' ( Novello ) . 
 book deal specially Mozart ' Sonata : 
 Form meaning exemplify Pianoforte Sonatas 
 Mozart , ' Helena Marks ( William Reeves ) . ( 2 . ) 
 hear orchestral transcription Mozart 
 Sonatas , think likely . 
 slight , essentially keyboard music . 
 ( 3 . ) question low price certain musical 
 journal compare deal literature 
 discuss . main point regard 
 Musical Times wonderful sixpennyworth . , 
 . ( 4 . ) agree 
 high standard good english musical critic , 
 comparison 

 F. FE . H. — Vaughan Williams ' Sine Nomine ' 
 publish leaflet form , good 
 tune * English Hymnal . ' write publisher 
 2 jist . sympathise difficulty ; 
 tune send touch rock - vulgarity futility 

 thankful.—we like opinion 
 teacher method , especially hold 
 good diploma pianist obtain . certainly 
 odd scale arfegg ? study 
 hear play . expect 
 tell pauliten hand . 
 tackle point ? know little 
 standard diploma aim . progress 
 likely hour - - - quarter day 
 keyboard depend method practice 
 opinion . time great , 
 wonderful thing thoughtful , 
 intensive practice . grade piece . 
 difficult player easy . , 
 , advise cut list * Maiden 
 Prayer ' Brinley Richards piece . think 
 dead long ago 

 G. K.—(1 . ) London College Music , Great Marl- 
 borough Street , W.1 . ( 2 . ) term ' classical ' 
 use antithesis ' romantic , ' apply 
 strictly , type overlap . Beethoven , example , 
 classical composer ; regard 
 great romantic . use label 
 distinguish great mass music pass 
 probation , consensus critical opinion , hold 
 permanent value . word 
 apply contemporary recent work : 
 excellent , time 
 decide - round value 

 ORGANUM.—The passage page 22 , line 4 , 
 later , Rheinberger G sharp minor Sonata 
 dificult rhythmically regard alternation 
 6 - 8 2 - 4 time , resolutely count bar . 
 synchronisation exactly note . 
 regard brahmsian passage 
 effective little rhythmical adventure 

