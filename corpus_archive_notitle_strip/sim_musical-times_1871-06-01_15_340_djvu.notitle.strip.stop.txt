FELIX GANTIER OPERATIC GEMS . 
 Published . new Numbers Popular Series Pieces 
 founded following Operas : — 
 . 25 . Der Freischutz . 28 . Tannhiauser . 
 26 . Il flauto magico . 29 . Crown Diamonds . 
 27 . Zampa . 30 . L’'Ombre ( Flotow new opera ) . 
 Solos , 2s 6d . Duets , 3s 

 CATHEDRAL GEMS , LOUIS DUPUIS . 
 1 . NANTES.—Fantasia , subjects Mozart 12th Mass 
 2 . WURMS.—Fantasia , subjects Weber Mass G 
 3 . ROTTERDAM subjects Haydn 3rd 
 ( Imperial ) Mass 
 4 . — — Fantasia , ‘ subjects Beethoven Mass 

 nc 
 5 . ROUEN . — Fantasia , “ subjects Gounod Messe 
 Solennelle 
 6 . CAEN . — Fantasia , subjects Rossini Stabat Mater 
 7 . oye E.—Fantasia , subjects Haydn 1st 
 Mass , B flat 
 8 . oy E — Fantasia , ¢ ‘ subjects ‘ Mozart Ist 
 eco 
 9 . PARIS . Fantasia , subjects Mendelssohn ; Hymn 
 Praise 
 10 . LONDON . — — Fantasia , ' subjects Handel Messiah 
 11 . NORWICH.—Fantasia , subjects Haydn Creation 
 12 . CANTERBURY . Mpegs — Spohr - 
 pants Hart .. eee ove 

 Tue chief event establishment past 
 month unqualified success Madlle . Marie 
 Marimon , début 3rd ult . , 
 character Amina , “ * La Sonnambula . ” 
 questioned policy entirely unknown artist 
 appearing time country 
 associated successes innumerable predecessors , 
 Madlle . Marimon evidently conscious 
 strength , achieved triumph honourable 
 difficulty self . imposed task . 
 soprano voice exquisite purity , delicacy vocalisa- 
 tion , accuracy intonation facile 
 execution , scarcely wondered , 
 proverbially cold audience , gradually asserted 

 wer . Opera singing instinct 
 intelligence true musical feeling ; 
 passionless critic detecta flaw 
 daring cadenzas . acting , , thoroughly 
 French , carefully subdued feeling , 
 little doubt characters 
 freer scope histrionic qualifications , leave 
 desired . brilliant execution 
 finale , “ Ah non giunge , ” applause enthusiastic 
 unanimous , Madlle . Marimon compelled 
 appear times curtain receive 
 renewed congratulations admirers . sorry 
 débutante better Elvino . Signor 
 Fancelli sang parts , love scenes 
 hard unreal , voices 
 sympathetic ring soul Italian music . 
 Signor Agnesi scarcely ease 
 Count , Signor Casaboni onlv respectable 4 / lessio . 
 orchestra , excellent direction Sir Michael 
 Costa , remarkably efficient , accompaniments 
 played utmost delicacy refine 
 ment ; chorus coarse tune , 
 portions Opera actually marring general effect 
 ofthe scene . revival Gounod “ Faust ” enabled 
 Madlle . Léon Duval favourable impression 
 - actress singer , scarcely 
 pronounced equal execution music 
 exacting Margherita . Signor Nicolini achieved 
 marked success Faust . appeared 
 greater effect Raoul “ Huguenots . ” Signor 
 Sparapani mentioned reliable baritone 
 Valentino « Faust ” having points worthy 
 commendation . unnecessary enlarge 
 eontinued success Madlle . Titiens Leonora 
 Beethoven “ Fidelio ” ; 
 assumption year finer . 
 general excellence cast , Opera stands 
 works given season 

 ORATORIO CONCERTS 

 Tue series excellent concerts 
 present season given St. James Hall 
 5th ult . , Beethoven Mass D 
 composer Choral Symphony performed . 
 presentation Beethoven colossal Mass Oratorio 
 Concerts year , given , time 
 country alteration curtailment , 
 thoroughly inspired musical public confidence 
 power efficiency Mr. Joseph Barnby Choir 
 repetition work looked forward 
 utmost interest ; 
 respect second performance showed marked 
 improvement , certain 
 recording opinion present 
 competent judge . sublime grandeur choral 
 porti ns Mass thoroughly felt 
 singer engaged interpretation extreme 
 intricacy difficulty obtruded 
 detriment general effect ; end 
 movements applause enthusiastic 
 “ Gloria ” given wondrous 

 decision ; solemnity “ Agnus Dei ” 
 felt hearer ; “ Benedictus ” sung 
 heavenly calmness truly sympathy feeling 
 ‘ movement , abstruse 
 especially portion “ Et vi.am 
 venturi ” — precision points attacked 
 remarkable . solo vocalists Madame 
 Cora de Wilhorst , Madame Patey , Mr. Lloyd Herr 
 Carl Stepan , acquitted 
 arduous task utmost credit , Madame de 
 Wilhorst , imagine little aceustomed 
 interpretation sacred musie , sang 
 earnestness good expression ; , especially 
 ‘ “ ‘ Benedictus , ” upper notes heard 
 advantage . Madame Patey , usual , thoroughly 
 satisfactory contralto ; Mr. Lloyd sympathetic 
 voice refined style told concerted 
 music good effect , Herr Stepan sang 
 enthusiasm true lover art . ' Theorchestra 
 desired ; violin obbligato 
 “ Benedictus ” exquisitely played Mr. 
 Carrodus elicit marked applause . Choral 
 Symphony went excellently . 
 instrumental movements performed extra- 
 ordinary precision truth expression , choral 
 portion — spite exertions choir 
 Mass — sung remarkable vigour . principal 
 parts given vocalists mentioned , 
 opening recitative declaimed Herr Stepan , 
 tenor solo sung effect Mr. 
 Lloyd . Mr. Joseph Barnby conducted care 
 judgment displayed commencement 
 career public , exercise 
 , rehearsals performances , 
 produced results highly estimated 
 desire progress real art country . 
 organ , usual , efficiently played Mr. 
 F. A. W. Docker 

 PHILHARMONIC SOCIETY 

 Tue concert season introduced Madlle . 
 Brandes , young pianist , performed Men- 
 delssohn Concerto G minor perfection 
 mechanism augurs future career , 
 powers evidently possesses shall 
 ripened age experience . Symphonies 
 Haydn D , . 7 , Spohr D minor , 
 , able direction Mr. Cusins , 
 played . Madame Monbelli Signor Delle Sedie 
 vocalists , lady giving “ Batti , batti , ” 
 Jewel Song fiom Gounod “ Faust , ” excellent 
 effect . fourth concert 8th ult . , inter- 
 esting feature programme “ Concerto grosso , ” 
 G minor , Handel , excellently played , 
 pleased , spite antiquated character 
 inseparable class music , 
 wonder compositions ( 
 choice specimens works past age ) 
 rarely presented modern audience . Schumann 
 Pianoforte Concerto minor finely performed 
 Madame Szarvady , greeted conclusion 
 warmest applause ; Signor Bottesini produced 
 marked effect Concerto composition 
 Contra Basso . Beethoven Symphony F , 
 Mendelssohn Overture “ ‘ Ruy Blas , ” orchestral 
 pieces included Auber capital “ Exhibition ” March , 
 composition deserving frequently heard , 
 understand called 
 “ March . ” vocalists Madlle . Regan Herr 
 Jules Stockhausen 

 ROYAL SOCIETY MUSICIANS , 
 Tne 133rd anniversary Festival excellent Charity 
 held Freemasons ’ Hallon 28th April , 
 presidency Sir Sterndale Bennett . 

 Tue series concerts given 
 Welsh Choral Union took place Concert Hall , 
 Store Street , 1st ult . , direction Mr. John 
 Thomas . choruses , usual , sung remark- 
 able decision , elicited unqualified marks approbation . 
 principal vocalists Miss Annie Edmonds 
 Sisters Doria ; instrumentalists Miss Stone 
 ( pianoforte ) Signor Scuderi ( violin ) Mrs. Henry Davies 
 Mr. John Thomas ( harp ) highly 
 effective , Mr. Thomas Duet harps , Welsh 
 melodies , received deserved applause . 
 concerts respect excellently conducted , 
 appear rapidly increasing attraction 

 Mr. WALTER Macrarren gave 
 Matinées Hanover Square Rooms Saturday 
 morning 6th ult . programme exceedingly 
 interesting ; , appealing especially 
 lovers classical works , contained sufficient variety 
 thorough satisfaction mixed audience . 
 interesting novelty MS . Sonata major , 
 pianoforte violin Mr. G. A. Macfarren ( excellently 
 played concert - giver Mr. Henry Holmes ) , 
 received applause , slow move- 
 ment especially pleasing , attractiveness 
 theme excellent musicianlike manner 
 treated . pianoforte performance 
 Mr. Walter Macfarren instinct grace 
 elegance facile execution fails 
 felt audience ; versatility 
 style sufficiently tested works 
 selected appear — Beethoven Sonata G minor , 
 pianoforte violoncello , Mendelssohn Quartet 
 B minor ( addition Sonata mentioned ) 
 lighter pieces composition , 
 including effective duet , ‘ « Andante Bolero ” 
 ( joined Mr , Stephen Kemp ) 
 Nocturne called “ Twilight , ” charming sketch 
 entitled ‘ “ ‘ Morning Song . ” ‘ Iwo sacred songs Mr. 
 Walter Macfarren sung expression 
 rising young vocalist Miss Goode , Miss Dalmaine 
 equally effective vocal contributions . 
 instrumentalists lent assistance occasion 
 Mr. Henry Holmes ( violin ) , played 
 excellent effect quaint “ Andante e allegretto , ” 
 Handel , Mr. Burnett ( viola ) Herr Daubert ( violoncello ) . 
 vocal music ably accompanied pianoforte 
 Mr. Stephen Kemp 

 SigisMOND THALBERG , decease re- 
 cently taken place Naples , pianist 
 remarkable powers , compositions instru- 
 ment powerful effect modern school 
 fantasia writers . manner 
 fingers sing melody , whilst surrounding 
 brilliant ornaments distributed entire key- 
 board , created astonishment delight 
 musical non - musical listeners ; scarcely 
 influence works beneficial 

 LL 

 Overture , Die Pflegekinder , Lindpaintner ; Symphony D , 
 . 2 , Op . 10 , Fesca ; Fantasie burlesque , viol 
 ( orchestral accompaniment ) , ‘ * Carnavale de Venise ” 
 Servais , solo played Mr. Weston ; Beethoven man , 
 Prometheus . thoroughly successful . Bop . 
 berg Lay Bell rendered , choral pans 
 especially effectively given , solos efficiently suny 
 Miss Monkhouse , Miss Borst , Mr. T. J. Hughes , Mr. T. F 
 Mr. W. Forrester . Mr. Armstrong officiated conductor , ay/ 
 Mr. Henry Lawson leader band.—TuHE consecration gf 
 St. John , West Derby , Liverpool , took place Saturday , } ty 

 2 

 Lreps . — members Leeds Madrigal 
 snd Motett Society held Soirée St. George School- 
 room 15th ult . tea , chair taken 
 Mr. C. E. Wurtzburg , number solos , madrigals , 
 - songs sung . chairman congratulated 
 members excellent manner pieces 
 given , great strength Society , 
 numerically musically . annual report read Mr. 
 W. 8 . B. Cheveley , Hon . Secretaries , ap- 
 peared past session total receipts 
 £ 83 17s . 11d . , expenditure left balance £ 30 hand . 
 registered members numbered 136 . report recognised 
 debt owed founder Society , Dr. Spark , stated 
 consented retain position conductor . spoke 
 Mr. T. W. Dodds valuable adjunct ranks 
 members . Society declared amore flourishing 
 condition time formation 
 years ago . motion adoption report 
 agreed , thanks voted Dr. Spark , Mr. T. W. Dodds , 
 Mr. Cheveley , ladies chairman 

 LeIcester.—An amateur concert given 
 Freemasons ’ Hall , Halford Street , Friday evening , 28th 
 April , Mr. E. J. Crow Choral Class . concert commenced 
 Mr. Barnby Cantata , Rebekah , solo parts 
 Rebekah , Isaac , Eliezer ably sustained Miss Black . 
 Mr. 8 . Tebbutt Mr. George Read , choruses given 
 good effect class . Mr. Crow conducted , 
 assisted accompaniments Mr. T. A. Wykes 
 harmonium . second consisted secular music , including 
 - songs Mendelssohn Leslie , Balfe duet , ‘ ‘ O’er 
 shepherd pipe ” ; Cooke tenor bass duet , ‘ ‘ Army 
 Navy , ” sung Messrs. Tebbutt Read , encored ; 
 songs , ‘ ‘ Happiest Land , ” Mr. G. Read ( encored ) : ‘ ‘ Un- 
 changeable , ” ‘ ‘ heart thine , ” Bishop popular 
 glee , ‘ ‘ Chough Crow . ” course second 
 Mr. Crow played Beethoven Sonata E Minor , Op . 90 , excel 
 lent style , elicited applause . concert evidently 
 gave gratification numerous audience 

 Liverrpoo : — members “ Societa Armonica ” 
 gave thirty - fourth open rehearsal Saturday evening , 
 22nd April , Liverpool Institute , Mount Street , pre 

 Mawen Newton , Dorsersuire.—On 16th ult , 
 members Maiden Newton Choral Society gave fin 
 concert National School - room . excellent singing 
 Mr. H. Tayler ( Tenor ) , Mr. Horscroft ( Bass ) , Lay Vicars 
 Salisbury Cathedral , playing Mr. Boyton Smith , Dor . 
 chester , admired . Mr. J. Brown conducted 

 Mar.poroveu.—A concert given Tom 
 Hall , 10th ult . , aid Savernake Cottage Hospital 
 , regret , numerously attended , 
 performance respect thoroughly sati- 
 factory . programme highly attractive . Andank 
 Allegro Beethoven Sonata violin pianofore 
 ( Op . 12 ) , excellently played Messrs. Salisbury Bainbridge ; 
 Meyer Trio violin , violoncello , pianoforte ( Op . 2 ) , 
 Mr. Bambridge took violoncello , Mr. 
 pianoforte — instrumental concerted pieces . Mr. Bam- 
 bridge pianoforte playing interesting feature pr- 
 gramme , solos ly - d ded . Th 
 - songs rendered excellent effect , vocl 
 solos given mueh success Messrs. F. Glass , ? . 
 G. Foster , W. H. Hillier , Lavington . magnificent 
 “ Concert Grand ” pianoforte kindly lent occasion 
 Messrs. Broadwood Sons , free charge 

 MontreaL , Canapa.—A highly successful concert 
 sacred music given St. James Street Wesleyan Church . 
 8lst March chorus consisted 
 trained voices , conducted Mr. Smith , Organist Chur . 
 programme composed selections standan 
 Oratorios , Mozart Twelfth Mass , Romberg Lay tk 
 Bell . soloists Mrs. Saunderson , Miss Hoerner , Mm . 
 Walde , Mr. James Hilton , Mr. Bentley . solo , ‘ Angels 
 bright fair , ” sung Mrs. Saunderson 
 taste feeling ; “ know Redeemer liveth ” 
 rendered good expression . Mr. Bentley s0lo , 
 “ Straight opening fertile womb , ” displayed voice grest 
 advantage ; duet , ‘ ‘ O Lovely peace , ” Misses Hoernet 
 Pringle , effectively given . trio , ‘ thee 
 living , ” sung Mrs. Walsh , Messrs. Hilton Bentley , 
 best pieces evening . choruses wert 
 carefully rendered , parts evenly balanced . 
 ably accompanied noble organ Master C. Hilton , 
 amateur , eighteen years age . proceeds concett 
 presented ‘ * Methodist House 

 _ _ _ 

 trained tLe members Society ; thanks 
 chairman conclusion Concert , wer , 
 thoroughly deserved . 
 Worrsive.—On Thursday , 27th April , Mr , Lg 
 Palmer gave evening concert , Miss Jenny Pratt 
 principal vocalist , Mr. Kuhe solo pianist . Mr. H Ba . 
 grove , advertised violinist , unable tend oq 
 account severe illness , Mdme . Sydney Pratten ab } 
 gap playing solos guitar , wi 
 encored , Miss Jenny Pratt sang feeling song 
 Dinorah , Sullivan ‘ ‘ Looking ; ” Miss Burden wy 
 successful song E. Thurnam , “ Let depart . ” yp 
 Kuhe played , effect , Beethoven Sonata , 3 , Rubia . 
 stein Melody F , compositions , ty 
 Glee Madrigal Union gave best old 
 new conductor . Miss Palmer , Mr. Cooke , ani 
 Mr. Jaeckel accompanied , Mr. L. S. Palmer conducted 

 Oraan Appointments.—Mr . Herbert Leach th 
 Parish Church Westbury , Wilts.——Mr . Thomas Bensted , 
 ganist Choirma : ter , Allhallow , Lombard Street.——Mr , j - 
 Buither Grange - - Sands , Lancashire.——Mr . R. T. 
 Organist Cheirmaster Saints ’ Church , Benhilton , 
 Surrey.——Mr . David Strong , Organist Choirmaster 
 Mury , Aldermanbury.——Mr . E. C D. Gilmore , St , 
 Church , Maidstone.——Mr Ing'i « Bervon , Organist Choi . 
 mas er Parish Church , We'shpool 

 Natatia MacraRREN . 
 Price 2s . 6d . ; scarlet cloth , 4s 

 N OW READY , 
 BEETHOVEN ’ FIDELIO 

 great overtures usually performed ; Pianoforte Score 
 published agreeing original score notes signs phrasing 

