much regret will be feel in Manchester that Mr. 
 Eyon Petri have decide , after five year ’ residence in 
 that city , to take up permanent residence in Berlin 

 he have long be famous for his 
 Beethoven ’s pianoforte sonata at 

 Hall in Ancoats — perhaps the dingy , 
 industrial Manchester , have for its 
 lory the frown front of mill and 
 one ’s imagination be fire by the contem- 
 plation of this glorious vision of the mighty Beethoven , 
 play by the pupil of Busoni , right in the heart of this 
 teeming industrial population . how often anywhere 

 from next 
 ot 

 orkshop 

 can opportunity be find for such an experience as 
 al/ these sonata ina few week ? in the winter of 
 1908 - 9 , Mr. Petri play they at the University , and 
 then in Ancoats , only omit the very early ; the 
 followin winter they be all perform in Ancoats 
 n diversified order , and this winter they have be 
 yiven again in chronological order . and after 
 Christmas follow a series of recital draw from 
 Bach and Beethoven ’s miscellaneous work 

 Mr. Petri have be pleasantly outspoken in give , 
 through the medium of a letter to the Das / y News , 
 his reason for leave England 

 t t for stance , no creator have be 
 t haracteristic of their own nation than Shakspeare 

 ba Beethoven , or Wagner . 
 I tudy our own folk - song may help to show we 
 how we may develop on the line most congenial to our own 
 t t Wi we wish , create a * national ’ 
 le artificially rhapsody on national theme 
 exerci for the composer , and may show 
 to the hearer in a new and _ pleasing 
 t t t tak 1 handful of I nglish folk - song 
 te they to taste with some patent Strauss mixture 
 t srahm - and - water will not make a national style . 
 [ $ personal , and not a national style that 
 su Smit i d Brown have get to write 
 smithi and brownian music , just as Beethoven and 
 S rt write b venian d schubertian music . 
 llowever , t ndividual be the member of some race and 
 res m s iracteristic ww his fellow - country 
 1 ' | the most individual art be usually also the most 
 char rist f the 1 i whol rherefore | would 
 their own 

 in their musical 

 us 1 up a blank 
 i d i they their own limitation 
 1 quality | tendency as a musical race . 
 phis study he place of study of the great 
 reign mus f necessity build on the great 
 ractit whic ) with the whole of Europe , 
 é erite Beethoven , Schubert , and 
 wa r , and f pure technique it would be 
 shu teaching of well - equip 
 | t | lesson to our own lecessitie 
 | p s. not its slave 
 i s m i ik - song vou have hear to - day 
 e be collect as part of a movement which be go on 
 r England to recover our traditional melody and put 
 rail the possession of the nation . the 
 ! ral development of the folk - song have , I freely admit , be 
 ( 1 I think myself that these check have be purely 
 rtificial , and therefore the process of set the development 
 g ag must t rtain extent be artificial also . the 
 y| which have en dig up a d leave to decay must be 
 ! te sol f the plant will doubtless wither and die , 
 hose which have real vitalitv will live in their new 
 surrounding . the folk - song as we present it to you now , 
 n of course never take the place of what have exist in the 
 tl be g as entirely as the old life to which it 
 ge . the folk - song be enter on a new stage in it 
 nd will I lieve again live adapt to new ne¢ 
 \ sta c 
 LONDON SYMPHONY ORCHESTRA 
 i rst portant event « IQII concert season 
 rre on January 13 , when t Leeds Philharmonic Choir 
 1a visit to ( Queen 's Hall , and sing under the direction of 
 MI 

 M. Safonofi . the chief 

 distinguished 
 lity , attain an exceptionally high level at their 
 Eolian Hall on January 9 . the concerted 

 form be Beethoven 's Trio in C minor ( op . 1 , no . 3 ) , 
 Tchaikovsky 's Trio in A. Madame Amina Goodwin 
 e pianoforte work by Handel and Kiriakoff , and song 

 vere contribute by Miss Margaret Balfour and Mr. Owen 
 r 

 Society , the Monday chamber 
 after the holiday on January 9 , 
 a pianoforte Esposito , who play 

 Beethoven ( op . iii ) , and 
 publish prelude 

 on January 16 , Miss Nora Thomson ’s String Quartet 
 make their first appearance at these concert , play 
 Haydn ( op . 33 ) , Mendelssohn ( op . 44 ) , no . 1 , and 
 Beethoven ( op . 59 ) . the member of the Quartet be 
 Miss pen Thomson , Miss Madeleine Moore , Mons . Octave 

 and Mr. Clyde Twelvetrees 

 Sunday Orchestral Concerts 
 » 15 . Dr. Esposito conduct a good performance of 
 Beethoven ’s seventh Symphony , and three ' irish dance ' 
 himself for orchestra from old irish air . Miss 
 vocalist , and Mr. Clyde 

 Grisard 

 EDINBURGH . 
 appearance at 
 Messrs. Paterson ’s orchestral McEwan 
 Hall on December 19 ( the sixth of the series ) , and give 
 1 splendid performance of the solo part in Liszt ’s * Rhapsodie 
 Espagnole , ' arrange for pianoforte and orchestra by Busoni . 
 he be also eminently successful in his playing of a group of 

 pin piece . the most important orchestral work 
 perform be Beethoven ’s Symphony No . 7 , in A 

 Mr. Fritz Kreisler be the at the seventh concert 

 I , IQII 

 trio by Beethoven ( c minor , op . 1 ) , Schubert ( op . 99 ) and 

 Schumann be play by Mr. Midgley ( pianoforte ) , 
 Mr. Dunford ( violin ) and Mr. Schott ( violoncello ) , and 

 WELLINGTON ( N.Z.)—On November 11 , Elgar 's * the 
 Dream of Gerontius ' be perform for the first time in 
 New Zealand . the executant be the Wellington Choral 
 Society , conduct by Mr. J. Maughan Barnett . the 
 dimension of the choir , which consist of 160 singer , be 
 small , but not toosmall for adequate interpretation of the 
 choral music , and the quality of expressiveness and good 
 tone , which be independent of number , be present in 
 the singing . thus Elgar ’s fully - develop style be 
 its first introduction to a New Zealand audience in a manner 
 to secure full appreciation . the soloist be Miss Nellie 
 Castle , Mr. E. J. Hill and Mr. John Prouse . the 
 municipal orchestra play the instrumental part efficiently . 
 the performance be highly creditable to all concern 

 a combined orchestral concert and organ recital be give 
 at the Town Hall on November 23 by the Municipal 
 Orchestra , and Mr. Barnett . the chief of the orchestral 
 number conduct by Mr. Barnett the Adagio from 
 Beethoven ’s second Symphony ; his principal organ solo be 
 Bach ’s Prelude and Fugue 

 give 

 violin may be worth £ 1,000 

 Sypit EAtox.—The motto in the = score u 's 
 Concerto mean ' here lie bury the soul of 
 Beethoven 's symphony arrange for pianoforte be publish 

 in Peters ’ edition in tw 
 Brown.—We be u le 
 f the Antiphon you send . we can not print t 

