12 . Tue Comrapes ’ Sonc or Hore Adolphe Adse 
 13 . THe Dear LittLe SHAMROCK .. " ( arr wy ) H. Elliot Butts 
 14 . THe FrRoG ( humorous ) Ernest Newt 
 15 . THe Lonc Day CLossgs .. " .. Arthur Sullive 
 16 . THe THree CHAFERS ( humorous ) = H. Tri 
 17 . THe TuHree HuntTsmMEN ( humorous ) . C. Kreatat 
 18 the wintry wind be blow J. Mille 
 19 . THURINGIAN VoOLKSLIED ot ‘ x Franz Ab 
 20 . what care I how fair she be Jacques Blumenths 

 BACH . BEETHOVEN . 
 HAYDN . MENDELSSOHN . 
 MOZART . SCHUBERT . 
 SCHUMANN . 
 may be obtain in the Novello Editions at continental price . 
 London : Novetto anp Comvaxy , Limrrep 

 London : Novetto anp Company , Limited 

 in the much low sense of ' pattern ' or ' proportion 

 they approach music at the wrong end . they 
 make the cardinal mistake of suppose that a 
 Beethoven symphony , for example , be great — this 
 among other reason , of course — because it exhibit 
 a certain symmetry of pattern . the truth be that 
 the pattern be only interesting because the music be 
 the symphony do not 
 small degree from the pattern 

 might have be modify by 
 Beethoven at a hundred point have the nature of 
 his think it — and the pattern 
 therefore no help at all to yew , for instance , in 

 ihe greatness of 
 n the 
 really 

 try to compose something equally great . by | 
 copy the pattern without equal the idea 
 you merely fire a blank cartridge , or make a 
 wax figure that have the proportion of a man 

 without the life of he , or , as Wagner would have 
 say , you make sword without blade . or perhaps 
 l may be allow to the process 
 in word that I have use elsewhere : ' the text 
 book and the professor only teach form because 
 the spirit be unteachable . no great musician evei 
 be , or will in 
 call 
 re product the 
 by plan his section , his repetition and so on , 
 in the same order and on the ale . he can 
 copy the form of a Beethoven allegro to a hair 's 
 breadth by use the same number of theme and 
 Beethoven , cut they to the same 
 size and pattern , go up when Beethoven go 
 ip and down when 

 modulate wl 

 figure 

 Beethoven go down 

 nen 
 1 

 t it at 

 Beethoven modulate , enter 
 out , his recapitulation , and all 
 precisely as many bar ’ distance 

 pon his wt 

 from the commencement as Beethoven do , and 
 idde a cvda of the same length and build as 
 Beethoven 's . but his work would be no more lik « 
 Beethoven 's than an isosceles triangle be like the 
 West pediment of the Parthenon 

 I be well aware that some of the good composer 

 have on verge to another line . he change his point of | happy in life and happy in Geath , but that it also 
 ve pictur sew , his line of approach , a hundred time in the|{seems to cling like a second self to every line of 
 e way use of the conversation ; he never hold to a|Goethe ’s poem , every fold of its deem . what and 

 there int a single instant after he have drive it home . | whence be this magic ? I do not know : and no 
 and sucha attime he will modulate so delicately into a new | analysis of the ' form ' of the song , nothing that | 
 aterial of ; ggument that you hardly know how he have carry | could put in a text - book , would come within a 
 t a math : you over from the one to the other ; at other time | hundred mile of accounting for the magic . ' that 
 i ve Italie ¢ will make you sit up sharply in your chair , |i Wolf 's secret ; he do this kind of thing simply ' 
 intereste — f gl eye and ear , by a change of manner,!because he be Hugo Wolf , not because he be 
 portion , mood , or thought , the amazing abruptness | anybody ’s pupil,—indeed , he be no one ’s pupil ; 
 er bute which be supportable only by the still more}and he be Hugo Wolf because he can do this kind 
 site sik ff amazing feeling you have that the more this } of thing so often and so infallibly ! yow will 
 ‘ Oregrounf atraordinary man seem to diverge from his point}once go and do likewise : but I warn you in 
 arge bac te close he be really keep to it . in a word , | advance that the treatise on form will hardly help 
 enever fail to do the right thing in the right|you in the least to do so , any more than they 
 ne pictu - ff jace -- and that be genius . I therefore can not } would have help you , have you be Beethoven , 
 t oblom [ ficiently impress upon you , if you would be a}to bring the horn steal in with the original 
 , € ach off geat composer , the necessity of always do the| tonic theme of the ' kroica ' upon that apparently 

 f balance nght thing in the right place . see to that , and| irrelevant dominant seventh harmony in the violin . 
 proportio werything else be easy . all you have to do , you see , be to bea genius , and 
 of plaxf ineed hardly say that you can not be too careful } the rest shall be add unto you 

 good f 

 about-§ composer and the little composer be simply | repetition of thetheme of the Romanza inthe ‘ Kleine 
 between this , that in the course of a hundred year | Nac ht - Musik ' : what demon tell Bach how to pick 
 iccess Sf ( t so the great one be discover and the little | up again and again the thread of the organ toccata 
 right nes be find out . you will therefore see that | in F just as he seem to be on the point of lay 
 re , maki you begin with theme as vital and as durable as|them down , they having honourably do their 
 befor ose of Wagner , Bach , and Beethoven . secondly , | work and nothing more to be expect of they ; 
 careful ) make sure that your germinal theme have really | what minister angel stand by Wagner 's sid 
 ter — o ge power of germination . one of the most ] when he be write the second Act of ' Tristan , 
 n for 8 % pwele thing about the theme of the big man is| and whisper to he not only what he be to say 
 ng thet teway they seem to contain in themselves the | and how to say it , but — which be just as important 
 » you ® b whole of the come composition . it be this when to say it ? during the writing of ' Tristan ' 
 you whe quality , among other , that make Hugo wolf’s| Wagner discover , as he tell Frau Wesendonck 

 094 

 in themselves , logically relate to what 

 before and what come after , and that they 
 come pat at the proper moment , and all will } 
 be well with you . can I recommend a _ book } 
 that will teach you this , I fancy I hear you 
 ask . I be afraid I can not . then will a study of 
 the transitional method of Bach and Beethoven 

 and Wagner help you ? not in the least , 1 be 
 sorry to say , except in the general way that look 
 at harmonious statuary will make a sculptor ’s eye 
 fastidious , and so less like ly to tolerate inharmonious | 
 work of his own . these thing help the student 
 negatively rather than positively ; they may make 
 he resolve to turn the devil out of door , but they 
 will not of themselves bring he angel to entertain 

 you can not crib Bach or Beethoven ’s game . | 
 you will have play the bowling off your own 
 bat , as they do . but though the book and the 

 teacher can not help you , do not despair . the } 
 thing be really simplicity itself ; all you have to do| 
 be to think of the right thing in the right place 

 as your great forerunner have the common - sense 
 to do . 
 and so with the end of your work — in some 

 respect the most difficult part of it . 
 sO impressive as a good exit from the stage ; and 
 the manner of it must necessarily vary with the 
 play , the character , and the situation . if you learn 
 from a book on form how Beethoven manage his 
 and then try to apply the information to a 
 work of your own , you will be like an actor who 
 die 
 Macbeth . you may see quite clearly how an effect 
 be make , yet that of itself will not enable you to 
 produce the living equivalent of it . 
 the end of the aria of Bach that Max Reger have 
 use as the subject of his variation and fugue . the 
 serene dignity of the conclusion be plainly due to 
 the depart actor — for so we may cail the theme 

 turn half round when he have all but leave the stage , 
 give the audience an eloquent look and make 
 an impressive gesture , and ' Aen turn slowly on 

 QUEEN ’S HALL ORCHESTRA 

 the Saturday afternoon Symphony Concerts of this 
 Orchestra open on November 7 with an _ estimable 
 programme that be dominate by Beethoven 's ' Eroica ' 
 Symphony . Miss Isolde Menges interpret Tchaikovsky ’s 
 Violin concerto with her usual spirit and eloquence , and 
 Dr. Walford Davies play the pianoforte part in his own 
 ' Conversations . ' these , which be produce at a recent 
 Promenade Concert , deepen the impression then make of 
 their ingenuity and musical interest 

 lhe PROMENADE CONCERTS 

 toa high pitch . the programme also contain a Symphony | Madame Clara Butt give a highly successful concert x 
 by Haydn in e flat , Bach 's Brandenburg Concerto in F for | Queen ’s Hall on November 1 , assist by Mr. Kennerle 
 violin , flute , oboe , trumpet , and string orchestra , and | Rumford , Mr. Gervase Elwes , and the Queen ’s Hall Orchesta 

 Beethoven ’s second ' Leonora ' Overture . | under Sir Henry Wood . it be announce that the receipt 
 Brahms ’s fourth Symphony be the most considerable work | of the concert amount to 4350 , and that the profit be to 
 play at the concert on November 9 , but Tchaikovsky ’s | be devote to fund in aid of artist and musician who wer 
 four - movement Serenade for string ( Op . 48 ) be the chief | suffer by the war . 
 uccess of theevening . under M. Savonov ’s direction it be 
 exquisitely perform , and doubtless surprise many with its | 
 ittractiveness among the great number of neglect | 
 pianoforte concerto , that of Rubinstein in G , no . 3 , which | 
 Miss Vera Brock play on this occasion , be not one of the | 
 most worthy , in spite of excitingly brilliant passage . 
 Berlioz ’s ' Carneval Romain ' Overture complete the 
 programme 

 Chausson ’s Double Concerto for pianoforte , violin , anj 
 string quartet be the chief number in the programme of the 
 Royal Academy of Music Students ’ Concert at Queen ’s Hal 
 on November 2 . a Pianoforte sonata by Alec Rowley 
 | be give for the first time 

 with orchestral accompaniment , be give for the first time 

 with Mr. Samuel Mann as vocalist . the programme include 
 Beethoven ’s fourth Symphony 

 Students ’ Chamber Concerts give by the College o 

 rae men " ag y pia ) Miss Dolmetsch play both the viol da gamba and the 
 Schubert 's String quartet in G major , Op . 161 , and , with 4 ' . : — oy 
 | Tor : a. : ~ © violoncello — the former in Bach ’s g minor Sonata , will 
 Ir . Ernest Tomlinson as second viola , Mozart ’s String ay eae og Secs : lg samiet 
 quintet in e flat . Mr. Plunket Greene sing Somervell ’s | SS VOFOY " osenege as pianist , 
 cycle ' Maud , ' accompany by Mr. S. Liddle . : ? = — a 

 on November 18 the programme include Bach ’s A major A series of concert in London and the province bi 
 Violin sonata , no . 2 , Brahms ’s e minor Violoncello sonata , be organize by three belgian musician M. Desite 
 and Beethoven ’s e flat major Pianoforte trio , with Miss Defauw ( violin ) , M. Joseph Jongen ( pianoforte ) and Madame 
 Fanny Davies as pianist , Miss Jelly d’Ardnyi as violinist , | Weber - Delacre ( vocalist ) for the benefit of the Queen of = 
 and Madame Guilhermina Suggia as violoncellist . | Belgians Relief Fund . the first , which take ae 

 Eolian Hall on November 16 , attract a large audience 
 |and prove highly interesting . Chausson ’s Concerto lo 
 violin , pianoforte , and double string quartet , with which tt 
 open , be a work of great beauty and originality , and int 

 out of work ' ; on November 14 Mr. Max Darewski be hear 
 with the orchestra in Saint - Saéns ’s G minor Pianoforte 
 concerto . a Sunday evening concert in aid of the local 
 War Fund be an innovation that justify itself : Mr. Pedro 
 de Zulueta ’s singing be somewhat unequal in effect 

 at the Symphony Concerts there have be much orchestral 
 playing of a high order , in one or twoitems Mr. Dan Godfrey 
 and his instrumentalist excel themselves . work of 
 special interest to be record be Rachmaninov ’s e minor 
 Symphony , Schubert ’s Symphony in C , Beethoven ’s fourth 
 Symphony , Brahms ’s E minor Symphony , Cesar Franck ’s 
 Symphony , Elgar ’s ' Enigma ' Variations , Stravinsky 's 
 ' firework ' Fantasia , and Borodine ’s ' Prince Igor ' 
 Overture . these last two be play for the first time at 
 these concert , and another first performance here be that 
 of R. H. Walthew ’s ' Friend Fritz ' overture , which the 
 composer himself conduct . the soloist have comprise 
 Mr. Percy Frostick , who be rather overweighte in the 
 Glazounov Violin concerto ; Miss Adela Hamaton , whe 
 play Balakirev ’s expressive Pianoforte concerto ( the first 
 performance at Bournemouth ) ; Miss May Mukle , whose 
 interpretation of Davidov ’s melodious A minor Violoncello 
 concerto ( another ' first performance at these concert ' ) 
 charm all ear ; Miss Madeline excellent in 
 MacDowell ’s D minor pianoforte concerto ; and M. Albert 
 f Paris ) , whose peculiarly sweet tone and finish 

 Royle , 
 Geloso { or 
 phrasing find a charming medium in Mozcart ’s e flat Violin 
 concerto 

 since the visit of the London Symphony Orchestra to | 
 Woodbrook Concert Hall in August last , when Mr. 
 Hamilton Harty and Dr. Esposito share the duty of 
 enductor for the week , there have be nothing worthy | 
 fnote until now . | 
 the Chamber Music Recitals at the Royal Dublin Society | 
 ommence on November 2 , with a pianoforte recital by | 
 \. de Greef , who be very warmly receive 

 on November 9 , the London String Quartet play 
 hartet by Beethoven ( op . 59 , no . 1 , in F ) , Haydn ( op . 20 , 
 2d ) , and Dvorak ( op . 51 , in e flat 

 on November 16 , Dr. Esposito give a pianoforte recital , | 
 aading in his programme Beethoven ’s Sonata , Op . 109 , | 
 Schumann ’s ' Die Davidsbiindler , ' and two new piece of his | 
 own , ' remembrance ' and ' a Village Fete . ' 
 the Sunday Orchestral Concerts begin on November 1 | 
 with a concert at which the member of the band , conductor , | 
 and soloist give their service free , and the proceed , which 
 mount to £ 28 Is . 7d . , be send to the Prince of Wales ’s | 
 fund . the programme include Grieg ’s ' Peer Gynt ' Suite , | 
 and some piece by Jarnefelt , Auber , and Saint - Saéns . the | 
 wloist be Madame Borel ( soprano ) and Mr. J. C. Doyle | 
 aritone 

 nducte by 
 ; Work fer 
 f Rossinis 
 rogramme 
 valist be 
 t , Messrs 
 appealing 
 e and fr 
 h Orphes 
 lure th 

 he circa 
 ~onducte 
 tic Fund 
 ruses ane 

 on November 8 the programme include Mocart ’s | 
 ' Hafner ' Symphony in D , and Beethoven ’s ' Leonore | 
 no . 3 Miss Edith Mortier be the solo vocalist , and 

 Mr. Clyde Twelvetrees the solo violoncellist . 
 on November 15 the programme include Mendelssohn ’s | 
 Violin concerto ( admirably play by Signor Simonetti and | 
 the band ) , the ' Good Friday Music ' from ' Parsifal , ' and | 
 Buet ’s Suite ' Children at play . " Mr. J. C. Doyle be the 
 solo vocalist 

 during the past few month there have be innumerable 
 concert give for various war fund . the principal one 
 fom an artistic standpoint be the vocal recital give by 
 Miss Jean Nolan , assist by Miss Madalene Mooney | 
 ( violin ) , Mr. Clyde Twelvetrees , and the Rev. Arthur Oulton 
 accompanist 

 EDINBURGH , | 
 the organ recital refer to last month have be | 
 continue with much success . the recitalist be Messrs. | 
 Goss Custard on October 24 , H. Walton on October 31 , | 
 Bernard Johnson on November 7 , and F. Cunningham on | 
 November 14 . it be gratifying to report that Paterson ’s | 
 Orchestral Concerts have receive sufficient support to 
 sure that the series will continue for the usual period . 
 the first concert take place on November 9 , with Mr. E. | 
 Miynarski as conductor . Miss Katherine Goodson be solo | 
 plauist , and score a distinct success in Saint - Saéns ’s second ! 
 Voncerto and Liszt ’s third . on November 16 Beethoven ’s | 
 ' Etoica ' Symphony be the main item , and Elgar ’s * Sospiri , ' 
 up . 79 , receive a first hearing at Edinburgh . Mr. Fellowes , | 
 who have succeed Henri Verbrugghen as leading violinist , | 
 be soloist . a novelty be introduce at this concert in 
 he shape of duet sing by Miss Ellen Beck and Miss 
 sama Neovi ( from Finland ) . a number of other concert | 
 % a miscellaneous type have be give , all organize on 
 " ore or less popular line on behalf of various War Funds 

 a concert - lecture illustrate ' hygienic deep - breathing | 
 ' xercise ' be give by Mr. C. J. Bishenden or November 2 
 25 , Guilford Street , Russell Square 

 LIVERPOOL 

 the second and third concert of the Philharmonic Society , 
 hold on October 20 and November 3 respectively , be 
 conduct by M. Savonov , whose orchestral command be 
 fully exhibit in the fine performance he direct of 
 Beethoven ’s Symphony No . 2 , Brahms ’s variation on the 
 ' St. Antony ' Chorale , and Schumann ’s ' Manfred ' Overture 
 on October 20 , and of Mendelssohn ’s welcome ' italian ' 
 Symphony and Tchaikovsky ’s ' Francesca da Rimini ' on 
 November 3 . on the former occasion Mr. Herbert Brown 
 sang , and at the later concert Miss Isolde Menges make a 
 deep impression by the mastery of her play in Brahms ’s 
 Violin concerto . the choir be usefully employ in the 
 National Anthems of England and Russia , and also in 
 Pierson ’s inspirit ' Ye mariners of England 

 at the second concert of Mr. Akeroyd ’s Symphony 
 Orchestra on November 10 , a capital programme include 
 Elgar ’s suitably - choose ' Sursum Corda , '   Reissiger ’s 
 ' Felsenmiihle ' Overture , Tchaikovsky ’s ' Capriccio 
 Italien , ' and Weber ’s ' Jubel ' Overture . Max Bruch ’s 
 G minor Violin concerto be brilliantly play by Melsa , 
 and Mr. Alfred Benton do all possible with the 
 Philharmonic organ in the solo part of Handel ’s fourth 
 Organ concerto . there be a large audience 

 Mr 

 high attraction , raise consideration as to whether what my 
 | be call the handelian manner of singe Haydn js Qui 
 | permissible . Miss Caroline Hatchard , Messrs. Webs : 
 Millar and Robert Radford form an ideal trio of soloist , 
 | Verbrugghen ’s programme take on quite an epic characte 
 Beethoven ’s ' Egmont ' and ' Eroica,’and Liszt ’s ' Les Préludg 
 in adapt the modern orchestra to the task of Beethoys 
 interpretation Weingartner counterbalance the prepondery , 
 | string tone by double his wood- and brass - wind sectiog 
 |and the result , to modern ear at any rate , be exceed ! 
 satisfying . Verbrugghen , too , do not accept the mode , 
 orchestra just as it stand as a substitute for that , 
 Beethoven 's day . where he find its large number of string 
 effective he use they , but in many passage he reduce they 
 in order to secure what he believe to be the essential balang 
 of the instrument . there be this much to be say , thy 
 whilst Haydn and Mozart do not easily bear augmentatic , 
 the big the band in Beethoven the noble the result , and » 
 | balance of advantage the Weingartner method see 
 preferable . the conductor commence his concert wit 
 a stirring appeal for belgian orphan , and £ 74 be collect 
 as the audience disperse . 
 on November 7 the Free Trade Hall be well filled fy 
 Mr. Brand Lane ’s concert , at which Madame Clara By 
 and Mr. Kennerley Rumford sing , and Sir Henry Wog 

 conduct 

 be the vocalist , and her presence contribute materially to 
 the success of the concert 

 since Mr. M. E. Sadler become Vice - Chancellor of Leeds 
 University he have do much for local music , and have 
 organize recital and lecture which , give in the great hall 
 of the University , have be thoroughly appreciate by the 
 student and other . two such recital have take place 
 during the past month : on October 28 Mr. Archy Rosenthal 
 give a pianoforte recital , and on November 10 the Leeds 
 String Quartet play Glazounov ’s ' Novelletten ' and a 
 Tchaikovsky movement . on the following evening the 
 first of the Leeds Bohemian Chamber Concerts take place , 
 with a programme consist of three fine and representative 
 pianoforte trio , Beethoven in b flat ( op . 97 ) , Schumann in 
 | D minor ( op . 63 ) , and Franck in f sharp minor ( op . 1 ) , which 
 be play brilliantly and also sympathetically by Messrs. 
 a. Cohen , Hemingway , and Herbert Johnson ( pianoforte ) . 
 yet another chamber concert be give at the Leeds Arts 
 Club on November 16 , when Messrs. Cohen , Fulford , Lowe 
 and Geary play some more russian ( quartet , include 
 Kopylov ’s Op . 15 , which be new to Leeds , and the curious 
 set of variation by ten different russian composer . a 
 recital be give at Harrogate on November 14 , and again 
 at Leeds on November 16 , by Mlle . Herckelbout , a clever 
 young artist of Liege , who have have to seek english 
 hospitality . on November 19 , too late for criticism in this 
 place , the Leeds Choral Union give Coleridge - Taylor ’s 
 * Hiawatha ' Trilogy , with Miss Caroline Hatchard ard 
 Messrs. Ivor Walters and Ivor Foster as principal , and 
 under the conductorship of Dr. Coward 

 the Bradford Subscription Concert on October 30 
 consist of chamber music . Messrs. Achille Rivarde and 
 Pablo Casals , and Miss Fanny Davies play , with remarkable 
 delicacy and sympathy , Beethoven ’s great Trio in b flat 
 ( Op . 97 ) and Saint - Saens ’s Trio in F ( op . 18 ) , and each 

 member of the party contribute solo . on October 24 
 the Bradford Permanent Orchestra begin its season , 
 Mr. Hamilton Harty conduct the so - call ' Jupiter ' 
 Symphony , his own delightful ' Comedy ' Overture , the 
 ' Parsifal ' Prelude , and other thing . Miss Ada Forrest and 
 Mr. Woodcock be the soloist 

 publish in two form . a. voice part in staff and 
 = a Sol fa notation , with Pianoforte Accompaniment 

 b. Voice part only , in Tonic Sol - fa Notation . 
 6 — 17 . ' wake , bird , awake . " Unison A. 
 song . J. CLIFFE FORRESTER 1d . 
 foe . 1218 . * * holy night . " two - part song . 
 . Ac dapte from Beethoven by 
 EDWARD TAYLOR — 1d 

 ONIC SOL - FA publication 

 edition NOVELLO 

 BEETHOVEN — Sonatas and Sonatinas , edit and 
 fingered ( english fingering ) by dane Zimmermann 

 cloth , gilt edge i 16 0 
 ditto , i in 2 vol . , ini each 5 o 
 six Sonatinas ... one 1 6 
 Sonatas and Sonatinas , Svo .. 5 0 
 ditto , ditto , cloth , gilt edge 7 6 
 ditto , ditto , roan . 9 0 
 ditto , in 2 vol . , paper each 2 6 
 six Sonatinas , 8vo . 1 oo 
 | CLEMENTI — Gradus ad Parnassum . 24 select t Studies . 
 ( Franklin Taylor ) ' ' a 4 0 
 or , in 4 book : sie we « . @€acth £ o 
 CR amer—56 Select study . ( Franklin Taylor ) 4 0 
 or , in 5 book ' : — « ce £ oo 
 CZERNY—30 Select study from op . 2 299 . School of 
 Vv elocity . ( Franklin Taylor ) ik oS 
 or , in 3 book each I oo 
 19 Select study from op . 630 . anunnand 
 School of Velocity . ( Franklin Taylor ) ¥ 1 6 
 ROBERT SCHUMANN — complete Works for the Piano- 
 forte , edit and finger by Agnes Zimmermann . 
 ( continental fingering ) : — 
 Orus . 
 1 variation on the name ' " ' Abegs y — ne 
 2 Papillons _ ... ' I oo 
 3 study on P aganini ’s sc aprice , with preface . 1 6 
 4 intermezzo ... 1 6 
 5 impromptu on a Theme by Clara W ieck , with 
 appendix to the second edition of the same 1 6 
 6 Die Davidsbiindler , with preface 1 6 
 7 toccata in c d io 
 8 allegro in b minor : © 
 9 Carnival . Scenes Mignonnes sur " Quatre- note 1 6 
 10 six concert study on caprice by Ps aganini 1 6 
 11 Grand sonata in f sharp minor ... 1 6 
 12 phantasiestiicke 1 6 
 13 study in the form of Varis ation ( Symphonic 
 Etudes ) ... 2 0 
 14 third Grand Sonata ( Concerto Ww ithout Orchestra ) 
 in f minor 2 0 
 15 scene of c hildhood Io 
 16 kreisleriana win 1 6 
 17 phantasie in c major 1 6 
 18 arabesque in c major i oo 
 19 flower piece in d flat major i oo 
 20 humoresque in b flat major 1 6 
 21 novelette 2 6 
 22 second sonata in 6 minor i 6 
 23 night piece i oo 
 26 carnival freak from Vienna i 6 
 28 Three Romances ; i oo 
 32 Scherzo , Gigue , Romarce , ' and Fughetta Io 
 56 six study for the Pedal Pianoforte : 6 
 58 four sketch for the Pedal pianoforte ... I oo 
 68 forty - three Pianoforte Pieces for the young 
 Part I. , no . 1 to 18 Io 
 Part II . , no . 19 to 43 ... 1 6 
 72 four fugue 2 © 
 76 Four Marches I 6 
 82 forest scene be 1 6 
 99 leave of many colour . fourteen piece 2 : © 
 111 three Phantasiestiicke 1 oo 
 118 three Pianoforte sonata for the Y oung Io 
 124 album leave ( 20 piece ) ... 1 6 
 126 seven piece in fughetta form Io 
 133 Songs of the dawn : 6 
 complete work in three volume , cloth 
 vol . 1 , op . 1 to op . 12 : 16 ° o 
 ss 2 , op . 13 to op . 23 16 0 
 s & oe 26 to op . 133 . 16 0 
 in three Volumes , Svo , paper cover , " each volume 5 o 
 - in i cloth _ 99 7 6 
 Novello ’s select List of C ! cal and Romantic Music for Pianoforte 
 Solo will be send gratis and post - free on application 

 the principal winner in the solo class be as follow : 
 soprano ' Nymphs and Shepherds ’ Miss B. Catterall 
 Purcell 

 Contralto * Secrecy ' ( Hugo Wolf ) ... Miss D. Bradley 
 Tenor * Adelaide ' ( Beethoven ) Mr. J. Holland 
 Bass * the two Grenadiers ’ ( Schumann ) Mr. D. German 

 we draw special attention to the very high character of the 
 song set as it be in this department that the popular 
 taste of the day be apt to be too much consider with a view 
 Mr. Walter S. Nesbitt be adjudicator 

 KENDAL ( WESTMORLAND).—April 21 to 24 . 
 the ' MARY WAKEFIELD ' FESTIVAL 

 this event still continue to prosper . the appeal be 
 entirely local . nowhere else be the ideal end of the 
 competition Festival well exemplify , for practically 
 gl the resource of the singer be concentrate on the 
 combine production of important choral work , which on 
 this occasior include ' sleeper , wake ' ( Bach ) , ' song of 
 destiny ' ( Brahms ) , ' the mystic trumpeter ' ( Hamilton 
 Harty ) , ' the Spectre ’s Bride ' ( Dvorak ) , and ' now shall 
 the grace ' ( Bach ) . the policy of this Festival be to have the 
 sssistance of the good orchestra obtainable . last year the 
 Queen ’s Hall Orchestra , under Sir Henry Wood , appear , 
 and on the present occasion the fiallé Orchestra provide 
 accompaniment and play orchestral work , amongst which 
 be Dr. Somervell ’s Symphony ' Thalassa , ' Beethoven ’s 
 no . 8 Symphony , and the ' Oberon ' and ' Meistersinger ' 
 overture . Mr. Michael Balling conduct . the artist 
 be Miss Agnes Nicholls and Mr. Herbert Heyner 

 the competition be hold on each day . the Carnforth 

 this Festival be establish mainly for the performance 
 of important work , competition be , however , regard 
 as indispensable educative mean , and they serve to show 
 that there be excellent capacity and good teaching in the 
 district . on the child ’s day Petersfield gain the 
 challenge banner offer for the high aggregate of mark . 
 in another division Harting secure a challenge cup for the 
 high aggregate , and they be very successful in other 
 class . in the adult choral class Petersfield , Horndean , 
 and Sheet be respectively first in various section , 
 Horndean win the challenge shield . Dr. Somervell 
 adjudicate 

 the principal work perform be Mendelssohn ’s 
 ' Walpurgis Night , ' Schubert ’s ' Song of Miriam , ' Dr. 
 Vaughan Williams ’s Fantasia on Christmas Carols , a Concert- 
 stick for violin and orchestra by Dr. Somervell , and 
 Beethoven ’s Symphony No . 8 . Dr. H. P. Allen conduct 

 Keswick.—Mr . Harry Evans , who adjudicate , pay a 
 glow tribute to the worth of competition in the musical 
 world . one hear the most perfect interpretation by 
 amateur choir and conductor 

 CHORAL competition ( open ) . 
 ( a ) ' night watch ' ( Brahms ) . 
 ( 4 ) * the lee shore ' ( Coleridge - Taylor ) . 
 repton Mallet C.S. 
 choral competition ( village ) . 
 * o happy eye ' ( Elgar 

 a ) * my bonnie lass ' ( German ) . 
 ( 6 ) * hallelujah chorus ' ( Beethoven ) . 
 * gather ye rosebud ' ( Lawes 

 MADRIGAL ( open ) . 
 ' sweet honey - suck bee ' ( Wilbye 

 value of competitive festival 

 this be , iain from write you a line or two about the real value o 
 of Galloway , fie event when it happen in the town where I live . I| Instrumental Trio ( no . 3 , Beethoven).—1st , Miss 
 clude junio oe pee to even that smattering of the subject which | Margaret Robinson , Messrs. Amesley Voysey and Edward 
 s contribute fusmade I rejoice in the success of our local experiment Robinson . 
 1y , dougly fasome other art and craft ; but I feel strongly that there 

 twenty - sir fae several broad reason why this Festival should not be eanlieten Anak mae a 7 Se - _ 
 on Stewart , fglecte even by the non - musical or those , if any , who be | 5 " ) 4 Edward y 4 y » a 

 Tests : ' dream , baby ' ( Percy Fletcher ) . 
 * the death of Trenar ' ( Brahms 

 ist . Blackpool Glee and Madrigal ( Mr. H. Whittaker ) . 
 2nd . Blackpool Orpheus ( Mr. Clifford Higgin ) . 
 MALE - Voice chorir ( nine entry ) . 
 SMALL CuorrRs . 
 Springtime ' ( Beethoven ) . 
 the boy ' ( A. H. Brewer ) . 
 1st . | Goodshaw Glee Union ( Mr. Burnet Peel ) . 
 and . Hebden Bridge ( Mr. H. Greenwood 

 test 

 a 

 but amidst all these activity his hope be centre 
 | the music of his native Wales . candid at all time , | 
 s , | point to the absorption of the country ’s interest in chor 
 look 
 too narrow to know anything of Beethoven or Wagner 
 he go far still . not only 7 he bemoan the cymp 
 | lack of interest in orchestral music , but he criticise also i 
 lack of creative gift , inasmuch as in neither the vocal ny 
 the instrumental sphere have it produce a first - class natjy 

 composer . one remedy for this he have propose 

 nature never repeat herself ; art likewise abhor imitation . 
 for certain purpose reproduction may be good , but such a 

 yugh 
 whichever way be prefer . the composer himself view his 
 wr work differently from time to time . it be well - know 
 that Beethoven twice ' metronome ' the eighth and ninth 
 Symphonies , and that there be considerable variation in the 
 marking ; and this be to speak of elementary expression 
 enly . the truth be that the sense of an idea vary with each 
 repetition of it , and that the composer himself be subject to 
 the same law . with Heraclitus , we never descend into the 
 same stream ( of thought ) twice , for each experience we have 
 modify the experience which follow . there be , in short , 
 no exact expression of anidea . the formula be constant , but 
 at isall . the complete expression of an idea could only 
 be if all humanity read its meaning into it . ' not one man 
 of all man be God , ' say Swinburne , ' but God be the fruit of | 
 the whole . ' the God - like or perfect idea would be none | 
 other than that in which the experience of humanity have 
 but the composer certainly have no notion of 

 he 

