IMES.—May 1 , 1914 . 297 

 music - master private school Windsor . 
 | year , felt impel 
 | seriously prepare musical career . 
 | seek advice Sir Frederick Bridge , 
 | counsel endeavour takea musical degree . 
 Durham come field cater 
 non - residential candidate , way 
 | University offer advantage Sir Frederick 
 think meet Bairstow circumstance . 
 examination place 
 | week time , Bairstow know scarcely 
 janything composer counterpoint 
 | austerity strictly academic 
 | counterpoint require University . never- 
 | theless set work , able pass 
 examination , remain portion 
 | Mus . Bac . examination complete 1894 . 
 | examine , result 
 |he realise know Beethoven 
 | Sonatas Bach ' - ' pianoforte 
 music , practically ignorant classic organ 

 music indispensable 
 |associate service Church 
 England . wisely decide 

 ' polish handle 
 big door . way angular 
 corner rounded rough place 
 smooth , begin find level 

 remember Sir Frederick , * 
 doubt think little Beethoven , 
 plenty fellow clever . ' 
 wholesome remark . occasion zeal 
 bring perilously near career reckless 
 crime . music want quickly Novello , 
 Bairstow scorch bicycle ; 
 Parliament Street arrest 
 summon furious riding . 
 police court offer alternative 
 14 . fine day ’ hospitality . pay 
 fine 

 influence Sir Frederick , Bairstow , 
 1894 , appoint organist choirmaster 
 Saints ’ , Norfolk Square , Rev. 
 William Boyd , composer ' fight good 

 MUSICAL TIMES.—May 1 , rgrq . 301 

 perceptible rhythm impassioned acting . } disappearance score Clementi 
 kind thing available ancient greek|symphonie account 
 tragic , Wagner meet } supposition composer destroy 
 exceptional case , Niemann Sucher | mss . write 1800 , 
 instance . present case language | utilise musical material pianoforte 
 intervene , oblige conceive|works . admit hypothesis 
 act somewhat differently . try to| regard symphony overture 
 create anew art proceed equal |his principal occupation 1812 , year 
 music gesture associate rhythm . |his return London Russia , 1832 , 
 ‘ course , case , music must| year death . entirely 
 endow direct suggestive | composing pianoforte 1800 , 
 power current lyric - drama ; |and family pupil let know 
 yooriate closely acting , so/|that dissatisfied pianoforte music 
 reveal soul of| wish devote composition 
 character . Strauss deal a|symphonie . 1812 1824 
 problem great possible | programme London Philharmonic Society 
 luxuriant conception art . scene in|include year ' New 
 vhich Joseph , awaken , rise , lets|Sinfonias , MS . , compose Society ' ; 
 cloak fall , appear unclothed eye of|the critic day agree record 
 Potiphar wife — scene serve assert|enthusiasm work receive , 
 impossibility union these|and praise melodic invention 
 wo — Strauss spend year in|richness orchestral treatment , 
 prepare compass _ stand , it}clementi seventy 
 s consider climax the|years age . visit Paris , Munich , 
 work modern music . music appear here| Leipsic time , place 
 asa kind religion , etymological sense , |symphonie meet success , 
 connect ( ve / igare ) spectator drama in|rate favourable criticism 
 straight line , word resort , | Press . 
 spectator allow to| symphony 
 remember exist thing verbal | constitute composer final artistic period 
 expression . ' single trace remain , exception 
 score available . to| movement D major , MS . score 
 beremarkably brilliant effective , possess | preserve British Museum . mastery 
 agood deal suggestive power — suggestive |of instrumental technique comparable 
 power descriptive narrative order|the seventh eighth symphony Beethoven , 
 ' Salome . ' score / Clementi symphony 
 instrument , include harp | level , need surprise esteem 
 elesta . duration work hour . | hold . 
 - Clementi die March 10 , 1832 , 
 posthumous manuscript |country house , Elm Lodge , near Evesham , 
 MUZIO CLEMENTI . | Worcestershire . , date January 2 
 21 ; |same year , leave sum /'5 friend , 
 T. pe Wyzewa G. DE Satnt - Forx . | composer William Horsley , buy 
 Clementi associate the|ring token composer gratitude 
 minds musician - day , classify order manuscript , 
 sonatina practise in| thenceforward property 
 youth . acquaint | widow . hardly doubt 
 ' Gradus ad Parnassum , ' aware | manuscript score 
 year spend in|symphonie , , 
 England , Clementi known composer principal occupation thirty year . 

 symphony . work | difficult believe death 
 tforme London , admire | destroy score attach 
 y learned critic day / importance , especially 
 general public . dozen symphony | use ' Gradus ad 
 compose perform London | Parnassum ' pianoforte work 
 1785 1795 , publish 1787/|he write 1812 1832 . 
 y Clementi , number op . 18 . ! , search lose work 
 , tell Clementi pupil , |hitherto completely fruitless . british 
 ludwig Berger , fine sonata G minor,|museum possess 
 Op . 34 , arrangement Symphony | fragment mention , probably 
 belong group ; possible|only sketch , know Clementi 
 sonata b minor D major ( Op . 40)|compose Symphony C major , 
 vee transcription rearrangement work | movement principal theme 
 omginally conceive carry full|subjects MS . present key 
 orchestra . ' D. inquiry descendant 

 posit little doubt 

 specimen 
 London Schénberg north south pole . 
 musician shade thought find 
 ' Verklirte Nacht ' Sextet excessively beautiful , 
 sort ' Tristan und Isolde ' string ; 
 contrapuntal mastery novel 
 handling string - combination noticeable 
 musical beauty . ' report hostile 
 original reception puzzle londoner . 
 3662 Universal Edition , 
 run read . equally strong 
 opinion orchestral piece . 
 musical attribute 
 having throw overboard late 
 style , state , Schonberg 
 abandon resource orchestra . 
 use resource elephant 
 pick pin ; orchestra Beethoven , 
 Wagner , Strauss 
 small trick . Scriabin ( Zeitschrift 
 xiv . , 167 , March , 1913 ) rate fulness . 
 odd extreme 
 orchestra represent . Mustel 
 pretty little ' Celesta ' good effect 
 performance , hold 
 matter definite , small , /essctura . 
 general view treat Schonberg 
 power fine 
 man genius ; express 

 respect , 
 musician 

 fascinating warmth expression , combine 
 exquisite ease delicacy execution , 
 strike feature individual style . 
 exceptional beauty tone touch invariably 
 remind vividly Rubinstein — 
 master ' Ercles vein’—than 
 eminent pianist hear ; total 
 absence like effort — affectation 

 personal charm playing complete . 
 Buonamici perform Liszt Chopin perfection , 
 reverential thoughtful interpreter 
 Beethoven certainly excel 

 return Germany accept 
 principal professorship ' instrument 
 R. Istituto Musicale , undertake 

 listen applause , master ! ' ' , ; ¢ 
 , ' reply . heed 
 , Wagner 
 lean , assist spectator 
 , pull Buonamici , coram fopitlo , box , 
 young friend remain rest 
 performance 

 _ deceased artist busy editor , 
 issue valuable edition pianoforte music 
 include Beethoven Sonatas , Studies special 
 difficulty Beethoven , ' Biblioteca del Pianista : 
 & c. late publication _ interesting 
 Suite transcription quaint piece old 
 forgotten italian composer , Azzolino Bernardino 

 Ciaja di Siena , publish Bratti ( Florence 
 effective old - world piece repay 
 attention advanced student 

 remain - welcome 
 pianist visit London time , viz . , 1887 , 1890 , 
 1892 , 1893 , occasion public 
 appearance successful gratifying 
 kind 

 occasion , play Beethoven 
 Choral Fantasia concert short - live 
 London Musical Society , June 24 , St. James Hall , 
 direction , masterly performance 
 Beethoven ' Emperor ' Concerto enthusiastically 
 applaud ata Philharmonic Concert June 5 , 1890 

 rank musician good 
 fortune bring personal influence 
 Liszt Bilow rapidly thin , fes 
 word tribute memory ' Beppe ' ar 
 pen sad knowledge irreparable loss 
 true artist lovable friend , share 
 happy time life 

 TORQUAY MUSICAL FESTIVAL 

 second musical Festival Torquay Municipal 
 Orchestra hold Pavilion April 15 16 , 
 consist concert devote exclusively 
 work modern composer . item Bach 
 perform , Mozart , Beethoven , Schubert , Brahms 
 place Elgar , Delius , Strauss , Stravinsky . 
 british composer represent , 
 work perform native origin . policy 

 include modern music sound 
 educational business point view , 
 possible large number Devonians enable 
 acquaintance - - date composition , 
 novelty scheme draw large audience 

 programme Liszt ' Eine Faust Symphony 

 representative composition perform ip | 
 | ¢ > wear ‘ ve ; > " 
 ROYAL PHILHARMONIC SOCIETY . London year . work sing | 
 5 |move greaily , point intere 
 familiar music choose last|/that fully worthy _ revival . M. Aue Th 
 concert season March 31 , evening } \faaskov isa capable violinist , reveal power débur 
 sensation . Miss Muriel excellent performance Tchaikovsky Concerto , Fairni 
 Foster singing Max Bruch Scena , ' Aus der Tiefe item ' Lohengrin ' Vorspiel vocal 
 des Grames , ' ' Achilleus . enthralling } * Aufforderung zum Tanz ' Weber - Weingartner . solit 
 ; dramatic power . Herr Mengelberg eloquent } orchestral playing unquestionably good , display Forre : 
 individual reading Beethoven * Eroica ' Symphony | time curious vigour suggest intention 
 { , interesting detai ! breadth , } lady regard weakly femina | 
 traditional Mr. Frederic Lamond played| \ir . George H. Shapiro conduct capably . March 

 Tchaikovsky B flat minor Pianoforte concerto great | pe 
 virility , power somewhat waste | yn 
 music . Liszt * Les Preludes ’ bring concert MK . ELLIS CONCERTs . ' Fron 
 ee ae second Mr. F. B. Ellis concert modern music desert . 
 : place .Lolian Hall March 23 , devote ti ad G 
 QUEENS HALL ORCHESTRA , |chamber work . London String Quartet , assist | 
 concert March 28 programme anil Mr. James Lockyer second viola , performance 
 largely choral , Shettield Choral Union , train Dr. } Phantasy Quintet string , Dr. R. Vaughan Mr. 
 | Coward , having engage . unfortunate | Williams , work fully represent composer Arts C 
 owe railway delay concert begin nearly imaginative fanciful invention power W 
 half - - hour late . circumstance upset | sustain vary interest . M. Ricardo Vines pi 
 equanimity imperturbable set singer ; play Ravel ' Gaspard de la nuit ' Dukas ' \ ariations , viola , 
 | sing rare spirit high finish . interlude , finale theme Rameau , Mr , Ex 
 hi ' song Destiny ' ( Brahms ) superbly peculiar sensitiveness way mood modem ' Sunri 
 sing ,   Beethoven Choral Symphony | F rench style display . worksin Walthe 
 display virtuosity . performance | programme Hugo Wolf , Percy Grainger , Holbre 
 | Bach Cantata , * ' Weinen , Klagen , ' interesting | Balakirev . , asiste 
 feature programme . new| orchestral concert ( Jueen Hall March 27 ws " olin ) 
 edition recently prepare Sir Henry Wood . | extreme interest . introduce new work nolone 
 true Bach , , appeal lover importance — Dr. Vaughan Williams * London Symphony , 
 composer . solo Choral Symphony | specially consider article p. 310 ths 
 sing Miss Esta D’Argo , Miss Phyllis Lett , Mr. Herbert | ' s¥e confirm success Mr. Geofirey Toye , Du ! 
 Heyner , Mr. Gwynne Davies . Sir Henry Wood | " sing young english conductor . direct confidence Service 
 conduct usual , Orchestra splendid . ability Symphony , Balakirev * Thamar Grieg 
 Delius ' summer garden , ' revise 6 mino 
 |composer performance . Mr. Ellis th 
 ; BEETHOVEN FESTIVAL . |conductor Ravel ' Valses nobles et sentimentales , hs Heathe 
 able report concert important | age version ( ¢ — cg . 
 serie , glad record success . London | = new song Arnold Bax , sing ¢ 
 Symphony Orchestra , school Richter tradition , | ilys Jones . " oncert 
 executant , experienced M. Henri Verbrugghen Hubert 
 ee ® ) mphony poe reach high level . good FRIDAY concert . 
 n April 20 symphony , 2 ' . : owa 
 April = ' Eroica . ' seneteete aie ee ( Jueen Hall Good Friday afternoon — Mis 
 represent ' Emperor , ' Mr. Frederic Lamond 0 moor Wagner concert y 
 soloist , second , play Herr von Vood . ith exception song sing Jo Boos 
 Dohnanyi . case interpretation wen superb . Kirkby Lunn , music choose npr April 
 singer Miss Elena Gerhardt Miss Till , evening concert sacred music place Musiciay 
 Koenen . large audience attend , especially opening -~ ree 4 - know artist Pa » Crystal Palace ' Vocalist 
 concert . concert place ( Jueen Hall , _ ry stal Palace afternoon Crys r String 
 Choral Society London Symphony Orchestra unde ! Miss Ed 
 Mr. W. W. Hedgcock performance Rossitis 
 ' Stabat Mater , ' Miss Agnes Nicholls , Madame Comm 

 LONDON SYN NY O STR : . > d 
 ew ee Ada Crossley , Mr. Ben Davies , Mr. Robert Radford Stoc 

 Bechstein Hall March 28 , Mr. Frederic Lamond 

 play Sonata Glazounov b flat minor , Op . 74 , 
 
 Howard - Jones Bach - Beethoven - Brahms programme 
 
 | 
 Nathan Mr. E. Parlovitz Steinway Hall March 31 . 
 programme , devote russian music , 
 contain number interesting song interpret Miss 
 Alys Bateman . 
 | Mr. 
 attraction 
 Lambert recital composition ( Queen 
 Hall April 16 

 group piece Vladimir Metze . Mr 

 programme 
 fourth 

 Sorcier , ' Beethoven 

 Woof . 
 ' l’apprenti 

 ica " Symphony Beethoven 

 interpretation Schumann Pianoforte concerto 

 Walpurgis Night ' creditably sing Chagford 
 Choral Society April 15 . Mr. R. Percy Collings 
 unfortunately small choir usual 
 baton , hope interesting 
 Society revive number . number child sing 
 ' festival flower ' Totnes Baptist Church 
 March 26 ; ' Olivet Calvary ' choir 
 St. Paul Church , Honiton , April 8(Mr . H. E. Carnell ) ; 
 ' crucifixion ' sing evening 
 Saints ’ Church , Torre ( M1 . Winship ) . choir orchestra 
 - perform Jamouneau Cantata ' Saviour 
 man ' Heavitree April 10 , Mr. Charles Stait 
 conduct 

 Torquay favour chamber music 
 event month . April 2 persevere 
 Haydn String ( Quartet , eighth season , play 
 Glazounov Op . 26 Beethoven Op . 18 ( c minor ) 
 Mr. J. P. Curran sing cycle french song set 
 old english Airs . April 18 , day follow 
 Festival , London String ( quartet concert play 
 Dohnanyi Op . 15 ( d _ flat ) , Beethoven Op . 18 , 
 Tchaikovsky Op . 11 ( D ) , Schénberg String 
 sextet d minor , Op . 4 ( assist Messrs. James 
 Lockyer Cedric Sharpe 

 CORNWALL 

 programme close concert flourish 
 orchestral Societies mainly comprise amateur player — 
 Societa Armonica , Oxton Claughton , 
 Liscard,—exhibited uncertain way advance 
 amateur taste technique recent time . 
 - hear March 21 Cherubini ' Anacreon ' 
 Overture , Tchaikovsky fourth Symphony , movement 
 MacDowell strongly individual ' indian ' Suite , 
 Schumann minor pianoforte concerto , Mr. Frederick 
 Brandon soloist . Mr. Ivor Foster sing . 
 Oxton Claughton Society , conduct Mr. James FE . 
 Matthews , choose Tchaikovsky F minor Symphony 
 Schumann Pianoforte concerto , soloist Mrs. 
 E. W. Morrice , fluent graceful player . vocalist 
 Mr. Roland Jackson 

 evening , April 4 , Liscard Society , 
 conduct Mr. Philip Smart , close seventeentl 
 season programme contain example Weber 
 Beethoven , Svendsen , Coleridge - Taylor . Miss Elsie 
 Chadwick play Mendelssohn ' Capriccio Brillant ' 
 orchestra , Mr. Edward Stansfield example 
 skill double - bass , Miss Dorothy Freeman 
 contribute vocal item 

 new combination , Liverpool Trio — Mr. J. P. 
 Sheridan ( violin ) , Mr. Walter Hatton ( violoncello ) , 
 Mr. Douglas Miller ( pianoforte)—were hear advantage 
 Beethoven Trio b flat , Op . 97 , music , 
 Yamen Rooms , April 6 . concert 
 saloon Philharmonic Hall , April 2 , Mr. Vivian 
 Burrows ( violin ) Mr. Frederic Brandon ( pianoforte ) 
 exhibit artistic qualification especially 
 Mr. Brandon new Sonata violin _ pianoforte , 
 clever work modern influence 

 Mr. Vasco V. Akeroyd , - know violinist 
 conductor Akeroyd Symphony Orchestra , Societa 
 Armonica , Blundellsands Orchestral Society , 
 chief guest complimentary dinner hold honour 
 Midland Adelphi Hotel April 7 

 declaim baritone solo impressively . Mr. 
 Gwynne Davies soloist ' Hymn praise 

 flourish suburban choral Society Hillsborough 
 perform Goring Thomas ' swan skylark ' 
 point general care accuracy expression . 
 Mr. F. A. Smith conduct . Shiregreen Choral 
 Society , Mr. Gregory zealously direct , perform 
 ' St. Paul ' bravely , promise develop- 
 ment . local performance 
 F. C. Woods ' King Harold , ' Woodseats ( Mr. W. 
 Ludgate ) , Sterndale Bennett ' Queen ' 
 ( Mr. J. C. Simon ) , St. Oswald Choral Society . 
 Mr. Alfred Barker progress conducting 
 neat - study paying Sheffield Amateur 
 Instrumental Society Beethoven C minor Symphony 
 work . Miss Bettina Freeman sing ballad 

 YORKSHIRE 

 Church performance Leeds mention . 
 April 1 Brahms ' german Requiem ' St. Chad , 
 Headingley , conductorship organist , Mr. 
 Percy Richardson , April 6 annual performance 
 Bach ' St. Matthew ' Passion place Leeds Parish 
 Church , forthe time direction Mr. Willoughby 
 II . Williams , new organist . reverent 
 sympathetic performance , feature 
 special mention highly artistic singing 
 Jesus Mr. William Hayle , principal bass 
 Parish Church . March 23 Mr. Fred Dawson 
 pianoforte recital Leeds , delight large audience 
 brilliant playing . Carl Rosa Opera Company 
 pay annual visit Leeds March 30 , 
 unfamiliar feature programme Verdi ' Aida , ' 
 highly creditable production , excellent - round 
 cast 

 Bradford chamber concert March 27 
 recently - form Edgar Drake String Quartet , 
 sympathetic performance Beethoven early Quartet 
 , co - operation Mr. George Smith 
 pianist , introduce Dvordak delightful Pianoforte quintet . 
 Miss Carrie Birkbeck vocalist . fourth series 
 Bradford Free Chamber Concerts come end 
 March 30 , string quartet Mozart Schubert , 
 Brahms Pianoforte quartet G minor , 
 song Miss Pattie Clayton . typical 
 programme , worth record , 
 like , concession ' popularity , ' 
 secure closeattention large audience . Mr. S. Midgley , 
 establish concert , mean provide 
 generous amateur , certainly good work 
 create appreciative audience 
 exacting type music 

 York Musical Society , March 31 , 
 excellent performance ' St. Matthew ' Passion , 
 - round efficiency reflect great credit 
 Dr. Bairstow , conduct . Mr. Herbert Brown reading 
 Saviour good possible taste ; 
 dramatic , time dignified reverent . 
 Mr. Mullings , suit 
 Narrator , sing invariable intelligence , Miss 
 Agnes Nicholls Miss Dilys Jones thoroughly 
 artistic respective . choir sing 
 great spirit precision usual , allot 
 chorus Disciples Cathedral Choir 
 utterance distinguish Crowd . 
 Ripon Cathedral , english adaptation Dvordk 
 ' Stabat Mater ' April 8 , Mr. C. H. Moody 

 _ . high level effectiveness , excellent work 
 Orchestral Choral Society , direction | neon . = 
 : , © " | soloist — Madame Laura 1 aylor , Mr. Frank Mullings , 

 Mr. T. E. Wright . efficiency expressiveness Mr. lao 
 choral singing reach high standard . solo t. Herbert Parker . 
 ge g } ' ' 
 iaken Miss Bessie Kerr , Miss Belle Runcieman , | Hoy._aAkk&.—The Hoylake Male Voice Choir packe 
 Mr. Knight , Mr. Littlewood . programme include | house concert Town Hall March 7 . 
 ' Jena ' Symphony . | Choir regain old artistic efficiency . 
 BLACKBURN.—The programme concert soloist Miss Norah Dall ( vocalist ) Mr. boy 
 Ladies ’ Choir March 23 include Frederic H. Wood | Burrows ( violin ) . conductor Mr. Charles Hughes 
 ' ballad Semmerwater , ' Walker ' music , ' Percy Hytur.—The Choral Society concert 0 ! 
 Buck ' dawn , ' new group choral song Mr. W. | eleventh season March 25 , * Mount 
 Wolstenholme entitle ' choir invisible . ' Mr. F. | Olives ’ ( Beethoven ) , ' Festival Te Deum ' ( Sullivan ) , - 
 Duckworth conduct excellent singing choir , | ' Prospice ' ( H. W. Davies ) perform soloist 
 solo Miss Lilian Whiteside and| Miss Marjorie Walker , Mr. Herbert Thompson , = 
 Mr. Norman Allin ( vocalist ) Mr. W. Wolstenholme | Mr. Edward Halland . Mr. Alfred T. Dixon pnncip= 
 ( pianoforte ) . violin , Dr. A. T. Froggatt conduct 

 AyToNn.—For tenth season Choral Association 
 successful performance Mendelssohn ' 13th Psalm ' 
 Elgar ' Banner St. George , ' baton Mr. | 
 George Allan . Miss Katherine Vincent soloist 

 es 

 music JOHANNESBURG.—A concert March 18 high reputation , singe fine tone , precision , 
 grea Choral Orchestral Societies , Schubert s | brightness . orchestra play good effect Bantock 
 modem ' Rosamunde ' overture movement Beethoven fifth | ' Far West , ' Purcell Suite C major , Grainger 

 ll , Wallace Symphony chief contribution orchestra , | ' Mock Morris , ' Bach concerto minor , 
 ' Y ; Farjeon choir hear Lahee ' bell ' Haydn | Mr. John Lawson solo violinist . vocalist 

 praise ? successfully perform amateur | ell - write work adequately perform , achieve 
 ; Masical Society March 30 . choir eighty voice | * notable success . soloist concert 
 ven direction Mr. John Cope , Mr. W. Sherratt Miss Florence Holderness , Miss Sara Silvers , Mr. John 
 4 , organist , Mr. R. T. " Ford pianist , solo Collett , Mr. John Prout . orchestra assist 

 " Jena mrt Madame Lily Moffitt , Miss Lily 
 oe Yoorhouse , Mr. Richard Ripley , Mr. Francis Billing . | — — — 2 ' ereome - ay 
 ATISS iV 
 d Mr. ct LEVEN.—The amateur Orchestral Society MUSIC PARIS 
 gtformer successful concert March 26 | MUSIC PARIS . 
 ; creditably er anand e > , eal ae pm Opéra , rehearsal Alfred Bachelet 
 ie Wesleyan performance |0 , Haydn Prog ba Pons selection | lyric - drama ' Scémo ' proceed . _ 
 k Edmonds , rei Wagner Flying wer wees — haynes | new work interest genuine originality . 
 ote mee Tom Beethoven s€ ianoforte ] Tiss Carswell production place . 
 , Miss Nora e soloist . gg Pew — _ — Mrs. Cooper , — Opéra - Comique , dissension arise 
 , Mr AE ishacello solo Mr. Messeas . brother Isola M. P B. Gheusi , matter 
 ne include LLANELLY.—A highly creditable performance Elgar manager ’ respective attribution . issue conflict 
 dream Gerontius " Zion Choral | ' ! S foresee . ; 
 ombud Society April 7 , direction Mr. D. J. de proussan , actual manager Mae 
 : ond ak Loyd . ability expressive power | ¥ ~ — ae ghee oy 7 = current e wi — 
 e opinion o chor service music way reg es co — vale cor ge : — ree 
 rch 25 great satisfaction . solo Miss Maud f yyects WI oy ee publish produce work , engage artist , 
 undelete ’ Wnght , Mr. Hughes - Macklin , Mr. David Brazell . orm troupe , red : ene sa 
 Mt. W. F. Hulley orchestra assist . Théatre des Champs Elysées begin 

 ag oe er April 23 ' anglo - american ' opera season , 

 soloist Frau Nordewier - RKeddingius , Frau Hoffman 

 new Romantic Symphony italian composer , Antoni , 
 Scontrino , perform Koyal Opera Richarj 
 Strauss , meet cold reception . — Beethoven 
 Symphonies , Choral Fantasia , Violin concerto , 
 Triple Concerto , Pianoforte concerto jp 
 c minor , G , e flat , perform 
 popular concert Philharmonic Orchestra , _ 

 BREMEN 

 ninetieth Lower Rhenish Music Festival 
 place Whitsun , Panzner direction . 

 Onegin , M Urlus , M. Loéoltgen , M. _ Plaschke 
 M. Brongeest ( singer ) , Frau Elly Ney ( pianoforte ) , 
 Herr Hubermann ( violin ) . programme _ include 
 Verdi ' Requiem , ' Tchaikovsky ' Symphonie pathetique , 
 Handel ' coronation Anthem , ' Brahms _ Pianoforte 
 concerto b _ flat , Beethoven ' Eroica ' Symphony 
 Bruckner 150th Psalm , Beethoven Violin concert 
 Haydn Symphony . 13 , Reger ' Ballet ' Suite , 
 Strauss ' Burlesque ' ( pianoforte orchestra 

 ESSEN 

 Ss 

 INTERMEZZO ( " Tue Rose ) 
 C. MACKENZIE 
 arrange Joun E. West 
 WHIMS ( ' " GriLven , " FANTASIESTECKE ) 
 SCHUMANN 
 arrange Joun E , West tis . mS 
 ANDANTE ( Vpouin Concerro ) .. MENDELSSOHN 
 arrange W. A. C. CrurcksHaNnk ' . - 
 SYMPHONY 1s B minor ( ' " UNFinisHepD , " 
 MoveMENT ) ; SCHUBERT 
 arrange W. A. C. CruicksHANK ' - 
 BERCEUSE axp CANZONETTA ( op . 20 , . 8 g ) 
 CESAR CUI 
 arrange Percy E. Fi ercuer ; ' 
 SCHERZO RUSTIQUE ( op . 20 , . 12 ) CESAR CUI 
 arrange Percy FE , FLercHer : = 
 ( /NACHTSTUCK ( op . 23 , . 4 ) SCHUMANN ) 
 arrange A. B , PLant ' - 
 MOMENT MUSICAL tn F minor SCHUBERT ¢ 
 ( op . 94 , . 3 ) 
 arrange A. ANT , ' 
 FANTASIA axnp FUGUE 1x C minor C. P. E. BACH 
 arrange Joun E. West 
 PRELUDE ro ( " IL . Apvosties 
 EDW ARD ELGAR 
 arrange G. R. SincLair 
 . FINALE rrom SYMPHONY . 5 BEETHOVEN 
 arrange A. B. Pan - ae 
 ADORAMUS TE HUGH BLAIR 
 arrange HuGu Biair 
 . INTERMEZZO ( ' Tue Birvs " ArisrorHANEs ) 
 C. H. H. PARRY 
 arrange W. G. AtcocKk : 
 - BRIDAL MARCH FINALE ( Tue Birps ” 
 ARISTOPHANES ) Cc . H. H. PARRY 
 arrange W. G. ALcocKk ae 
 ANDANTE ( Piaxororre Soxatra tn C , op . 1 ) 
 arrange Joux KE . West J. BRAHMS 
 ANDANTE ( Piaxororte Sonata F minor , op . 5 ) 
 arrange Joun E. West J. BRAHMS 
 MODERATO anv CANZONA ( sonata 
 Parts , . vi . ) H. PURCELL 
 arrange Joun Puce - 
 3 . HUMORESKE ( op . 10 , . 2 ) TSCHAIKOVSKY 
 arrange HEALevy WiLLan 

 ELEGY 

 forte Solo r 
 Sinfonia traversi , du e Violinen , Vix 
 transcribe Pianoforte Solo 

 BEETHOVEN.—i2 Redouten Menuette ( aus d 

 arrange Pianoforte Solo H. Riemann 

