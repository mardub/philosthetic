to be publish by subscription , price 15 

 rrangement from the full score of 
 Handel , Beethoven , Mozart , Haydn , Graun , Schneider , 
 & c. , for the 
 ORGAN . by Wiu1am T. Best 

 it be intend to include in this work several composition 
 which have not hitherto be arrange for the Organ ; amongst 
 many | other may be mention , Beethoven ’s ' ¢ Andante con 
 moto , " from the Symphony inc minor , the " ' Andante " from the 
 symphony in D ; Handel ’s chorus , " * wretched lover , " * let 
 their celestial concert , " " t will sing unto the Lord , " ( the 
 horse and his rider , ) ' * he trust in God , " & c. several movement 
 from the Stringed Quartetts of Beethoven and Mozart have 
 be expressly arrange ; it be therefore hope that nothing 
 will be want to render the volume acceptable to those who 
 study the Instrument with the view of employ its vast 
 resource in a characteristic and legitimate manner 

 subscriber ’ name receive by W. T. Best , 23 , West- 
 Derby - street , Liverpool , and at the London Sacred Music 
 Warehouses , 69 , Dean - street , Soho , & 24 , Poultry 

 FOR PIANOFORTE.—(First series . ) 
 useful practice , edit by R. Barnett 

 no . ae ! 
 1 Steibelt ’s Sonata , in a. Op . 50 ee eee a 
 2 Beethoven ’s Sonata , in G. Op . 49 2 6 
 3 Haydn ’s Rondo , inC. .. 2 6 
 4 J. N. Hummel ’s Romance and Rondo , i in g. 2 6 
 5 Steibelt ’ s Sonata , in G. es ns a6 
 6 Clementi ’s Sonata , in F. Op . 32 ° oe 2 20 
 7 Dussek ’s Rondo ( rule Britannia ) , in g. oo 2a 
 8 Kalkbrenner ’s Rondo , inC. .. é aer ae 
 9 g Dussek ’s Rondo , in e flat aa ee os ee 

 10 Clementi ’s Rondo , in C. op . 21 ee o 2 6 

 13 Sterkel ’s Andante , in e flat .. ee oe 

 14 Haydn ’s Rondo , from op . 17,ing.   .. 
 15 Beethoven ’s Rondo , in C. ; ar ais 
 16 Mozart ’s three Waltzes an le 

 a Thematique Catalogue of the ienion may be he 
 ( to be continue 

 vast crowd that listen attentively on Friday 
 night can amply testify 

 to criticise the Lobgesang in detail would 
 exceed the limit we can afford ; nor be it neces- 
 sary , since it be above criticism ; but , be one 
 of the least intimately know , as well as one of 
 the great of the work of Mendelssohn , a few 
 remark may not be unacceptable . the mere 
 plan , independent of the exclusive tendency of 
 this composition , be not entirely new . the uni- 
 versal Beethoven , in his musical illustration of 
 Schiller ’s Ode to Joy , originate the notion of 
 substitute a succession of choral movement 
 and vocal solo for the usual symphonic finale . 
 the ninth , and last , symphony of the author of 
 Fidelio must , therefore , be acknowledge as the 
 point from which Mendelssohn start . but there 
 be one remarkable difference to be note in the 
 construction of the two work . in Beethoven ’s 
 the three instrumental movement , which stand 
 among the lofty achievement of his genius , 
 constitute by far the most important portion of 
 the symphony ; the vocal jizxale be simple , and 
 even light , in comparison ; in the Lobgesang , 
 on the contrary , although the orchestral move- 
 ment be perhaps more elaborate than those of 
 Mendelssohn ’s great predecessor , the finale , 
 which comprise the vocal part , be long , grand , 
 and more deeply coloured than all that pre- 
 cede it . thus , while Beethoven begin , Men- 
 delssohn end with his full strength ; the one 
 relax as he go on , the other gradually 
 heighten the interest until he reach the most 
 elevated point at the climax . it be for this reason , 
 and for its consequently great unity of purpose 
 and equality of style , that , with deference , we 
 be compel to own our preference for the 
 Lobgesang over the remarkable work to which 
 its peculiar form be undeniably to be trace . 
 but Mendelssohn have in his favor the high 
 sublimity of his theme ; the restless passion of 
 joy which Beethoven have illustrate with such 
 gigantic power be still ' of earth , earthy , ' while 
 the aspiration of the Hymn of Praise be of 
 that purely spiritual nature which raise man 
 above himself , and make his thought sublime ; 
 both , it be true , be masterpiece , but the one be 
 the expression of a human , the other of a divine 
 feeling 

 the three instrumental movement which 

 Tue ScarsorovcH Cuorat and PuiLuarmonic 
 Socrery hold their annual general meeting on the 5th 
 of February . its prospect appear to be flourish ; 
 it number about 70 member ; and it have now be 
 establish about two year 

 Sacrep Harmonic Society , Exeter Hati.—On 
 the 9th of February be perform Beethoven ’s Mass 
 in C , and Mendelssohn ’s magnificent sinfonia - cantata 
 the Lobgesang , or Hymn of Praise ; both work be 
 render in the most satisfactory manner . the extra 
 care now bestow on rehearsal , under the judicious 
 training of Sig . Costa , bid fair to place the perform- 
 ance of this society in advance of anything yet 
 achieve in this or any other country . on the 23rd , 
 Handel ’s Israel in Egypt be we with addi- 
 tional wind part add by the conductor ; and this 
 work will be repeat on the ist of March 

 Canrersury Cuorat Socrery.—It be with great 
 pleasure we record the increase and deserved popu- 
 sarity of this excellent society . Judas Maccabeus be 
 publicly perform on the 13th of February , by local 
 soloist , and a chorus and band number about sixty , 
 in a very effective manner , result from the careful 
 and repeat rehearsal which have take place under 
 their conductor , Mr. W. H. Longhurst . the spacious 
 Guildhall Rooms be so much crowded , as to warrant 
 the repetition of this magnificent work , to take place 
 on the 27th of March 

 new ' Music Book for the Young , " in richly - illu- 
 minate cover , 1 . ; and ' fifty melody for Children , " in 
 similarly elegant cover , 1s . , be prepare expressly for the pro- 
 motion of Musical Education amongst the youth of both sex , 
 all the melody be easy , and suited to young voice , the 
 | book be adapt for either school or private family ; and on 
 |that account , as well as from their exceedingly beautiful ex- 
 | terior , be find unusually acceptable as present , 
 | to be have ( with all the Musical Works of Dr. Mainzer ) at 
 |J. A. Novello ’s , 69 , Dean - street , Soho , and at Simpkin , 
 arshall , & Co. ’s , Stationers’-hall - court , London 

 p : 
 io Henri Panofka ’s Classes for 
 * Vocal music , read at sight , part and Solo Singing . 
 I.—Classes for Children , from eight to twelve year of age . 
 |in this class the child will be make acquaint with the 
 | first element — the Alphabet of Music , and will learn to read 
 |and sing at sight : it be therefore particularly adapt to those 
 | who have not previously receive any musical instruction . 
 IL — class for Young Persons . the principal object of 
 these class be the Formation of the Voice , and the reading at 
 Sight . the pupil will be as well prepare for Solo Singing 
 |as for singe in part — each individual be occupy accord- 
 e to his or her own faculty and special musical disposition , 
 this class be peculiarly adapt to those who have previously 
 have some musical instruction , either in singe , or play the 
 piano - forte . they learn therein to apply to the voice the 
 knowledge acquire at the instrument . their progress be , con- 
 ; sequently , very rapid ; and , though previously unacquainted 
 |with singing , they reach , in a short time , the most difficult 
 | division in part - singing , viz . , Counterpoint , Imitation , Fugue , 
 and canon . after having master musical reading of this 
 description , they will find little or no difficulty in read any 
 kind of composition , either of Handel , Haydn , Mozart , or 
 Beethoven . 
 iii.—classe for the high branch of Vocal Music , Solo 
 and Choral Compositions of the great master 

 the term upon which any of the above Classes for Singing 
 may be form , as well in school as by a union of private 
 family , be the follow 

 Importers of Foreign Music and Instruments , Roman 
 Strings , & c. , 89 , St. james’s - street , have just receive the 
 follow Classical Works from Germany 

 AIBLINGER , I. F. , Offertoire , Jubilate Deo , for So- 
 prano , Alto , Tenor , and Bass . Pianoforte score 
 and separate voice part - - - . - 
 ditto , ditto , ditto , deus noster Deus , for 
 2 Sopranos , 2 Altos , 2 Tenors , and 2 Basses . 
 piano score and separate voice part - - ~ 
 BEETHOVEN , L. Van . , Messe solenelle , Op . 123 , 
 a quatre party , Solo et Cheer . een de 
 de Piano , par Rink ~ - - - 12 6 
 BIBLIOTHEQUE DE Mus1Que D’Ecuise , contain the follow- , 
 ing Pieces : — 
 ist Book.—HAYDN , Micu2 1 , Missa Solemnis in C , 
 sub titulo , Jubilai , quatuor vocibus can- 
 tanda comitante Clavicembalo . Piano 
 score and separate vocal part - - 
 2nd Book.—VALOTTI , F. A. , Responsoria in para- 
 sceve , ditto , ditto , - - 
 3rd Book.—Ditto , Responsoria in Sabbato 
 sancto , ditto , ditto , - - 
 4th Book.—Ditto , Responsoria in Cena 
 Domini , ditto , ditto , - 5 
 ( adjecte sunt due Antiphonz , autore Orlando di Lenn ) 
 sth ditto.—no . 1 ; NEUNER , C. , " ecce quam bonum 
 et quam jucundum . ”—no.2 3 ETT , gas- 
 Paro , " hac die , quam fecit dominus . " 
 no . 3 ; HAYDN , Micuati , Offer- 
 torium , * ‘ de omni tempore . " no . 43 
 GRAZ , Jos . , Motetto , ‘ qui timetis 
 ronal no . 53 EBERLIN , E. , 
 ui confidunt in Domino . " - - 
 CASCIOLINI , Cravupez , _ a + ated win 
 cum Organo - 
 DUVAL , E. , Stabat mater , a quatser wate , for 2 
 Tenors , 2 Basses and Organ - - - - 
 EYKEN ’S , J. Messe , a trois voix avec mempngenmens 
 @ Orgue , no . 1 & 2 , each - 
 JANSSEN , N. A. , Missa , a tribus vocibus ( 2 te- 
 nors and Bass ) , cum Organo = 
 KALLIWODA , J. M. , Messe in a per fiir 4 Solo 
 Stimmen und Chor , mit Clavier - auszug , Opus 137 4 
 LACHNER , Franz , Messe a deux voix , égales avec 
 accompagnement , d’orgue , Opus 92 - - 
 NEUKOMM , Sicismont , Messe a deux voix , ditto 6 
 dittto , ditto , ditto , a trois ditto 7 
 ditto , ditto , ditto , pax animz a 4 party 
 de voix d’*hommes , Solo et cheur avec accom- 
 pagnement d’orgue - 
 NICOLAI , Orro , Pater noster , fee 8 ' scene in 
 2 Charen , op . 33 - 
 RINK , C. H. , Missa , pro seniie Alto , ‘ Tences et 
 Basso , cum obligato organorum comitatu , Opusg5 5 
 ditto , ditto , ditto , solemnis quatuor vocibus 
 virilibus decandanta , organo obligato - 
 TOMASCHEK , W. F. , hymni in sacro pro dalenetia 
 cantari ‘ soliti , quatuor vocum contentu en , 
 baritonis adjuto , Opus 72 - - 
 ditto , ditto , Te deum Hymnun , divi po tar 
 pleno contentu musico , Opus 79 - - . 
 VOGLERI Assatis , Requiem seu missa pro de- 
 functis , accomodata clavicembalo a C. H. Rink 10 

 2 

 HAYDN ’S MASS , each number singly , Quarto size . 
 i. 45 . 6.—ii . 4s.—ili . 3s . 6d.—iv . 4s.—v . 5s.—vi . 4s . 6d . — 
 vii . 38.—vili , 28.—ix . 4s.—x . 3s.—xi . 1 . 6d.—xii . 3 . 6d . — 
 xiii . 38.—xiv . 3s.—xv . 4s.—xvi . 5s 

 BEETHOVEN in C. Folio , 8s . 6d . ; the same Quarto , § s 

 BEETHOVEN in D , Folio , 14s 

 HUMMEL in e flat , 8s . HUMMEL in b flat , 7s 

