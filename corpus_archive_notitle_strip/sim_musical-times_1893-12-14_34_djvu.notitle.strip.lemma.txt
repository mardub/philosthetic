then real and not a mere simulacrum , as now , 
 could never subdue he , as we have see . 
 ' he have the inflexibility of all great mind . 
 he be a true hero — a moral hero . vexation 
 at defeat , ruin , bankruptcy , and all the sorrow 
 which they bring upon a man so proud as he 
 be , could not weigh he down ; he recom- 
 mence again and again , and by dint of 
 activity , energy , dignity , and courage , he 
 finish by conquer fortune 

 against he on the score of illicit relationship , 
 and there be no reason to believe other than 
 that , like Beethoven after the flush of youth 
 have subside , he remain indifferent to 
 passion which , in all age , have cause wide- 
 spread disgrace and ruin . but because 
 Handel never seek domestic joy and hold 
 aloof from woman , it by no means follow 
 that he be , therefore , want in the soft 
 feeling of humanity . like many man whom 

 10 the MUSICAL TIMES 

 HANDEL , paint by HUDSON . 
 ( from the Buckingham Palace Collection . by permission of her Majesty the Queen 

 sense of mystery unpenetrated , of secret un- 
 reveal . so it be , no matter what the form 
 in which genius be make evident . Shakespeare 
 be such a riddle that man , in very desperation , 
 have try to lessen the difficulty by assign 
 his work to one of the great philosopher 
 and keen intellect of all time . who can 
 understand Sebastian Bach as he pour his 
 wealth of mind and feeling into the petty 
 channel of a provincial town ? or Beethoven 

 where no man hear , but he sing on . so do 
 another , equally unregarded — he who write 

 HANDEL . 31 

 several — here and there one to become after- 
 ward noteworthy , as Emmanuel Bach ’s 
 Silbermann clavichord and Beethoven ’s Broad- 
 wood grand pianoforte . old Bach leave three 
 harpsichord in his will , but Handel only 
 one , the " large harpsichord " it be now my 
 object to identify . but in his long career 
 he must have have more , and it be not sur- 
 prising tradition should connect his name with 
 harpsichord and a spinet and clavichord 
 which still exist . I possess a spinet of Queen 
 Anne ’s time which the family tradition of 
 the late owner affirm be once Handel ’s . 
 it come from Downham Market , Norfolk . 
 Andrew George Lemon , who die at Lynn 
 in 1756 , have , so say his descendant , come 
 with Handel to England , in 1710 . the spinet 
 be afterwards give by Handel to Lemon , 
 who be a violin player , and have be a 
 paymaster in the King of Prussia ’s horse . 
 Lemon be an anglicized form of a name which 
 may have be Lehmann . I be not aware 
 that any biographer of Handel mention this 
 early friend , but thing move slowly in Norfolk , 
 and I can see no reason for the invention 
 of a fictitious pedigree for an instrument 
 which must have seem to be of little value 
 or use 

 John Hitchcock , who make it ( it be num- 
 bere , not date , 1676 ) , be oné - of two of that 
 name — Thomas and John — who have make 
 spinet in London . they make many , and 
 their instrument be model for their suc- 
 cessor , include Mahoon , immortalize by 
 Hogarth by the inscription on the harpsichord 
 in ' * ' the Rake ’s Progress . ’' I have not meet 
 with a contemporary reference to either of the 
 Hitchcocks , while their rival Haward be men- 
 tione more than once by Pepys and in other 
 seventeenth century gossip 

 qi ' . in preparation — 
 a new and striking portrait of L. ALMA TADEMA ’s portrait of 

 BEETHOVEN PADEREWSKI 

 the Master compose at his piano ) . 
 and many other 

