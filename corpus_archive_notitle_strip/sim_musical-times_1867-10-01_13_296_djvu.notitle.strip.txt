ly have been surprised that Madlle. ‘Tietjens was

overture to Leonora, performed with wonderful spirit 
and precision by the orchestra, and Professor 
Bennett’s beautiful Pianoforte Concerto in F minor, 
executed by Madame Arabella Goddard in her usual 
finished manner, were the special features in this 
miscellaneous act ; the rest of the programme being 
composed of well worn materials, the redeeming 
point being Mr. Sims Reeves’ interpretation of 
Beethoven's Adelaide, with Madame Arabella God- 
dard’s pianoforte accompaniment

The performance of the Messiah on the third 
morning of the Festival, drew an enormous audience 
to the Hall; there being, in fact, not even standing 
room in any part of the building. Mr. Sims Reeves 
was in excellent voice, and sang ‘‘ Comfort ye” and 
“ Every valley” with his usual truthful expression 
and feeling ; but in the trying song ‘Thou shalt 
break them,” there were evident traces of an 
indisposition which he could not shake off. The 
other principal vocalists were Madame Lemmens- 
Sherrington (who sang all the soprano solo music in 
the first and second parts), Madlle. Tietjens (who 
replaced her in the third part, and was encored in 
“T know that my Redeemer liveth”), Mesdames 
Sainton-Dolby Patey-Whytock, and Mr. Santley. 
The choruses were given to perfection ; and, although 
we cannot endorse Mr. Costa’s opinion that the 
opening of ‘For unto us a child is born” should be 
sung piano, in order that a theatrical burst of sound 
shall come in upon the word ‘ Wonderful,” the 
general effect of this, as well as the ‘ Hallelujah,” 
was such as we never heard produced save by the 
Birmingham choir

76

to the congregation. The service was intoned clearly and solemnly 
by the Rev. Mr. Williams, Rector of Beaumaris. The Rev. J. 
Price read the first, and Archdeacon Jones the second lesson. 
Tallis’s Preces and Responses were used throughout. The chants 
were all Anglican, the Psalms being taken to a double and single 
chant of Dr. Monk's; while the Magnificat and Nunc Dimi:tis were 
sung to a double and single chant from Mercer's ‘* Church Psalter.” 
The anthem was Richardson's, ‘‘O how amiable are thy dwellings,” 
which was well rendered. The service concluded with a Congre- 
gational Psalm Tune, to the words ‘‘Duw mawr y rhyfeddodau 
maith.” The number of choristers amounted to nearly 300. The 
‘whole of the musical portion of the service had been organized by 
Mr, E. W. Thomas, the Organist of St. Ann's, who conducted the 
festival of the day in the most effective manner. The following 
were the choirs present on this, the first festival :—Llanllechid, 
Llanfairfechan, Aber, Llandudno, Glan Ogwen, St. Ann's, Lian- 
degai, Gelli, and Penmachno. A Pranororte Recital of 
Classical Music was given at the Penrhyn Hall, by Mr. A. Lan- 
dergan, Organist of St. James’ Church, Upper Bangor, on Tuesday 
evening, the 17th ult. Vocalist, Miss Edith Wynne. ‘The pro- 
gramme was selected from the works of Beethoven, Mozart, Men- 
delssohn, Hummel, Dussek, Clementi, Gounod, Benedict, c. 
Mr. Landergan also contributed an Air and Variations on an 
original theme for the Pianoforte. The hall was well filled by a 
fashionable and attentive audience, who thoroughly enjoyed the 
entire programme. The local papers state that the Concert was 
a great success. and speak in the highest terms of the vocal abilities 
of Miss Edith Wynne, and of Mr. Landergan’s pianoforte playing

Boston, U.S.—Mr. James Pearce, Mus. Bac., 
Oxon., of Philadelphia, gave in July and August a series of Organ 
Concerts, in the Music Hall, to larger audiences than on any of his 
previous visits. The programmes consisted of the works of Bach 
and Mendelssohn, and transcriptions from Handel and Beethoven

CARMARTHEN. — The Choral Association of the 
Archdeaconry of Carmarthen held its Second Festival in St. David's 
Church, on Wednesday, the 28th August. Thirty-three choirs 
were present, numbering upwards of 800 voices. There were two 
full choral services on the occasion, that in the morning being in 
Welsh, under the direction of Mr. Owen Davies, Welsh Choir- 
master, and that in the evening, in English, entirely in the hands 
of Mr. Scotson Clark, who first organized the choirs of this Asso- 
ciation, and of whose great ability and untiring energy in training 
them we cannot speak too highly. The prayers were intoned 
in Welsh by the Rev. Mr. Howell, of Lianedy, the first and 
second lessons read respectively by the Rev. Mr. James, of 
Abergwili, and the Rev. Mr. Evans, of Llandebie, and the sermon 
preached by the Rev. John Griffith, of Cwmavon. The musical 
part of the service was conducted throughout by Mr. Owen Davies, 
and accompanied on the organ, in a very creditable manner, by 
Mr David Williams, organist of the church. At 3.15 the English 
service commenced. Prayers were intoned by the Rey. Mr. Phil- 
lips, of Llwynmadoc; lessons read by the Rev. R. Lewis, of 
Lampeter Velfrey, and the Rev. Mr. Harrison, of Laugharne, and 
the sermon preached by the Rev. Montague Erle Welby, M.A., 
Incumbent of All Saints, Oystermouth. The choral service, as a 
whole, went remarkably well. Mr. Scotson Clark, as on a former 
occasion, entirely dispensed with the services of a conductor, pre- 
siding himself at the organ with his well-known ability. We may 
particularly mention the careful rendering of the versicles and 
canticles, and the life and spirit which was thrown into the 
hymns; also the distinctness of the words in chanting the psalms, 
though on this latter point there is still room for improvement

Miiller 
Miiller

Beethoven

J. Atterbury

313 Thanks be to God Elijah 
317 Then shall your light -. ditto 
Why, my soul 42nd Psalm 
Ww hy, my soul (last Chorus) ditto 
Ye nations, offer Lobgesang

BEETHOVEN’S “‘ENCEDI. 
(MOUNT OF OLIVES

195 O praise him, all ye nations 3

196 Hallelujah ... ese wo 2

BEETHOVEN’S MASS INC. 
190 Kyrie—When I call upon thee 4 
Gloria—Praise the Lord en 
Qui tollis—Give ear to my

22 OO ee om me Co

11. Six Pianoforte Pieces, by Wallace

12. Beethoven’s Sonatas. Edited by Charles Halle. 
(No. 1.) Containing Sonatas Nos. 1 and 2 of 
Op. 2 complete

13. Twelve Popular Duets for Soprano and Contralto 
Voices

19. Favorite Airs from the Messiah, Arranged for 
the Pianoforte

20. Beethoven’s Sonatas. Edited by Charles Halle. 
(No. 2.) Containing Sonata No. 3 of Op. 2, 
and Sonata Op. 7, complete

21. Nine Pianoforte Pieces, by Ascher and Goria

No

28. Beethoven's Sonatas, Edited by Charles Halle, 
(No. 3.) Containing the Sonatas Nos. 1 and 
2 of Op. 10

29. Ten Contralto Songs, by Mrs. Arkwright, Hon, 
Mrs. Norton, &c

80. Beethoven’s Sonatas. Edited by Charles Halle, 
(No. 4.) Containing the Sonata No. 3 of Op, 
10, and the Sonata Pathetique

31. Beethoven's Sonatas. Edited by Charles Halle, 
(No. 5.) Containing Sonatas Nos. 1 and 2 
of Op. 14

32. Beethoven’s Sonatas. .Edited by Charles Halle

No.6.) Containing Sonata Op. 22, and Sonata 
p. 29, with the celebrated Funeral March

