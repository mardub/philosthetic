THURSDAY MORNING , 11.30 

 particulars forwarded applicatior Psat M CXXXVII . Soli , Chort nd 
 E.R. TU Sec , SymMpPHONIC CANTATA ) OLIVER KING . 
 05 , Great usse il Street , Bloomsbury , W.C. Composed ¢€ xpresslv Festival . 
 GU ILD ORGANISTS . | SYMPHONY C MINOR ( ‘ Beethover 
 55 , WELLINGTON STREET , STRAND , W.C. | } REQUIEM ( Verdi ) . 
 FRIDAY MORNING , 11.30 . 
 PaTRON — TueE Ricut Riv . Lorp BrsHop Lonpon . — _ ot 
 nee ey ee ee SYMPHONY B MINOR ( Schubert ) . 
 FELLOWSHIP EXAMINATION , JU nd 12 . ENGEDI ( Beethoven ) . 
 Syllabus requi irement inform r btained LOBGESANG ( Mendelssohn 

 1 iC 
 Hon . Sec . , Dr. J. H. Lewis , Staines , Middl . ‘ ta sted . : 
 ag Reahiee : = Fripay Evenrnc.—THE REDEMPTION ( Gounod 

 Scenes Childhood ” * * Album Young , ” 
 immense educational value . 
 soulless music — good deal — 
 music little soul — 
 — given pupils , kind 
 stuff useful technical purposes ; 
 ought told 
 good . fact , conversations 
 music practised , music 
 practised , profitable 
 respects , artistic education 
 pupil practice . 
 neglected matter enormous importance — , 
 choice music . course , bad , vulgar , 

 lewd music avoided like poison ; 
 music head pupil . 
 Beethoven works , instance , 
 milk babes . ‘ practising compositions 
 intellectually difficult harm 
 practising compositions technically difficult . 
 aspect art teacher 
 reveal pupil — esthetical . , per- 
 haps , aspect stands need 
 revelation . , « sthetical — beauty 
 line form — , manifestation soul 

 conclusion , venture remind fellow- 
 teachers proverbs , ask remem- 
 ber wise saws pursuit profession . 
 comes Romans : Iestina 
 lente . second comes Italians : 
 Chi va piano va sano 

 , man t 
 pen paper ( fix maximum 
 order include Symphonies cf Leet- 
 hoven ) ; Xeally t } 
 think shall publi set 

 cB 288 : popular — 1 
 profits founding scheme . ; 
 use . Music consigned tomb 
 resuscitated , case greatest 
 names . discovered - morrow score 
 Beethoven contemplated tenth Symphony , 
 Schubert phantom Gastein Symphony , co 
 believe cither permanent 

 place répertoire concert - rcom ? 

 followed 

 sifts single artist . 
 |to Beethoven $ 3 
 concert , Bach Ma 

 Beethoven Mass D ; “ * Recitals ” 
 rapidly growing publ wvour ) ~~ 
 worship artist , art , narrow 

 n 
 selec 

 suggestive programme illustrates . 
 1 expected Mr. Barnett known 
 fications _ result — qualifications 

 , rarely received 
 fication present case . Suite heard 
 attention , close , composer , 
 directed performance , received ample assurance 
 labours revision emendation vain . 
 note Mr. Alfred Hollins , 
 blind pianist , gave remarkable performance 
 Beethoven Fifth Concerto . Having regard 
 physical disadvantage labours , Mr. 
 Hollins played astonishing correctness freedom . 
 { t clear , , artist having qualities 
 higher mechanical skill . read music 
 intelligence feeling . ‘ William Tell ” Overture 
 ended Concert , course Mrs. Hutchinson 
 sang vocal music 

 final programme season contained 
 claim novelty . excellence , 
 crowded room testified ample approval 
 directors ’ choice . Mr. Svendsen conducted capital 
 performance Beethoven “ Pastoral ’ ? Symphony ; Miss 
 Soldat played Brahms Violin Concerto manner 

 complete exempli 

 certain success achieved work played earlier 
 year London Symphony Concerts . 
 pianist Henschel pianist Richter , 
 Mr. Fritz Hartvigson achieving great executive 
 triumph case . amateurs , 
 doubt , find hard understand artist 
 devote time immense labour mastery 
 composition like ‘ Todtentanz , ” , 
 hand , admire industry _ skill 
 possible rendering given Mr. Hartvigson . 
 rhere undoubtedly power work , 
 understand subject appeal highly 
 sensational picturesque nature like Liszt . 
 Wagnerian selections Concert “ Die 
 Meistersinger ” ‘ ‘ Tannhauser , ” vocalist Mr. 
 Henschel ; concluding Symphony , politely bowed 
 charmed circle Wagner reigns 
 supreme , Brahms . 2 . fine per- 
 formance given ; , , Concert 
 ranked brilliant executive success 

 fourth Concert ( 4th ult . ) introduced new violinist 
 person Mr. ( Master ) Henri Marteau , boy 
 fourteen , comes , believe , Rheims , 
 pupil Léonard . young gentleman , bright 
 intelligent looking lad , took solo Max Bruch 
 vell - known Concerto , played marked satisfac- 
 tion audience . tone small , pure ; style 
 illustrates special , , unrivalled merits 
 French school , supplements executive capacity 
 grace charm come musical nature . 
 equipped , wonderful received promptly 
 favour public . Haydn Symphony ( C ) , 
 Beethoven ( . 4 ) B flat , andan Orchestral Transcrip- 
 tion , Felix Mottl , Liszt pianoforte solo , ‘ St. 
 Francis preaching — * pro- 
 gramme , Dr. Mackenzie new Overture “ Twelfth 
 Night , ” specially written ak , performed time . 
 interesting work Srottish composer 
 attempt produce musical epitome play 
 , limiting , , incidents 
 characters , principally connected Malvolio 

 
 found great length necessary adequate expo- 
 sition ideas , result Overture 
 stand Allegro Symphony violation 
 proprietics . ‘ use come , 
 form , , ‘ ‘ Twelfth Night ’ ? Symphony — work 
 Dr. Mackenzie shown qualified 
 produce . Overture , inferred remarks 
 , regular form , contains charming 
 appropriate melodic ideas , worked skill 
 practised hand . , , find special 
 peculiar merit new piece quaint , old - fashioned 
 humour , sentiment , colour . said 
 Mendelssohn incidental music dramas Sophocles 
 know Greek music , 
 know Mendelssohnian , com- 
 poser satisfied demand illustrations 

 amme seventh Concert ( t sth ult . ) | 
 thine ve familiar selections , whic 

 icient nan Beethoven * N 

 ne exc m Waener * Meisters 
 ifal ’ ; Saint - Saén R 

 412 THI 

 sonatas Beethoven 

 , favourite 

 works omitted gladly heard 
 play . justify 
 describing performance herculean feat . 

 occasion early works given , 
 concluding D ( Op . 25 ) , commonly known 
 ‘ Pastorale , ” ’ sets Variations . 
 Recital Op . 27 ( Nos . 1 2 ) , Op . 31 
 2 3 ) , Variations Fugue theme trom 
 ‘ * ‘ Eroica ” ’ Symphony , thirty - Variations C 
 minor . programme , , child play com- 
 pared Recital , pianist gave 
 Sonatas F minor ( Op . 57 ) , ‘ ‘ Appassionata ” ( Op . 78 ) , 
 “ “ Les Adieux ” ( Op . 81 ) , . Sonatas \Op . 
 109 , 110 , 111 ) . test physical endurance 
 noteworthy , final work finely , 
 finely , played . qually 
 exhausting fourth programme , 
 Sonata ( Op . ror } , Grand Sonata B flat 
 ( Op . 106 ) , rarely heard thirty - Variations 
 Waltz Diabelli ( Op . 20 ) , Beethoven composition 
 pianoforte 

 necessary consider 
 unquestionable greatness Hans von Biilow interpretation 
 mighty works , certainly mere executive 
 skill possesses , remarkable . 
 hears runs , trills , arfeggi played 
 crisp , pearly touch . hand , occasionally 
 stumbles comparatively easy passage , 
 fair add occurred twice 
 recent performances ; fourth 
 tals technique quality 

 correct emphasis inflection clear enunciation ; 
 spirit master animate 
 interpreter highest sense word . 
 kind sensation experienced 

 listening M. de Pachmann playing Chopin , 
 2 far degree , Polish composer inferior 
 Beethoven . repeat hear greatest works 
 written pianoforte rendered rare 
 insight subtle beauty strength artistic 
 treat highest order , , 
 Hans von Bilow arded 
 Ve kable musicians time . trust 

 sufficient d reception pay 
 visit distant d 

 widely 
 

 vating x melody . ex- 
 . marked ‘ tres lent , ’ ? 
 material employed , 
 hearing 
 played Beethoven 

 

 Volkmann 

 Op . 5 ) , commenced Concert , 
 | exception slow movement , somewhat dry . 
 Mr. Hallé warmly recalled fine rendering 
 Beethoven Sonata E ( Op . 109 ) , honours 
 afternoon carried otf Madame Néruda , 
 | Schumann Sonata D minor ( Op . 121 ) 

 magnificent example violin playing . 
 WER : Cl Gish SE 

 owing iday novelty 
 , Pianoforte Trio minor , 
 Ex alo ( Op . 26 ) . composer hitherto 
 known country ‘ deemhonte Espagnole , ” 
 Senor Sarasate plays sofrequently ; France 
 esteemed , Trio fair ex ample 

 ion Mad 
 works 
 mann * * Mahrchen Erzahlungen , ” 
 violoncello ; Beethoven Sonatain 
 Mr. Hallc ; 
 Brahms 

 fol 

 Damrosch 

 led avery 
 Mr. Walter Damrosch 
 
 German Opera New 
 commenced Beethoven 
 Svmphony ( . 7 ) , fair test Conductor 
 ability , Mr. Damrosch reading generally com- 
 mendable , excepti mn taken slow 
 teinpo adopted san tric » movement . ‘ 
 jlo M. Musin series Concertstiicke form 
 serenade aie Damrosch . ‘ work 
 movements , chiefly noteworthy light , fanciful , o 
 , * tricky ” efiects ; second move- 
 ment , ‘ Midsummer Dream , ” ’ , 
 ‘ Love Song , ” Belgian violinist 
 scarcely having , believe , 
 changed violin moment , recovered 

 OCC 
 States . 
 ver director Symphony 
 Oratorio Concerts 

 yu 

 Beethoven Mend 

 Sefior Sarasate plays mz 
 declared season , need 
 . understand | 
 result recent visit thi 
 y¢ 
 large circle 

 ter ; matter 
 , Tau 

 study certainly useful . items | developmen ft 
 programme Madame de Pachn clever Theme delay t second tal , 
 Variations , Brahms Balla 2 ) , utier audience , 
 Beethoven Sonata F sharp ( O ; » tempora ositi 
 vhich , happily , Menter sufiiciently recov 
 carry programme . , best 
 MADAME DE PACHMANN AL . opening item , Beeth yven Sonata Caracterist sue 
 ‘ E flat ( Op . 51 ) . movements rendered 

 Tuis event , took place oth ult . Pri t ty ] ] en bei 

 came 
 dot shee ss amateurs remembered 
 ig ail . birth German , 
 studied Heinrich Litoltf , 1861 took 

 ence States , turn head 
 ithe Chicago Conservatoire ’ Beethoven Conser- 
 |vatory St. Louis , subsequently settling New 
 York . Mr. Goldbeck pianist charac- 
 ic attributes school studied — 
 sound technique , rare light : delicacy touch , 
 remarkable Sritency style , allied great charm 
 jintelligence « expression . playing , whilst giving 
 | evi ice exceptional n ranipuli itive force , free 
 | slightest trace exaggeration , powers invariably 

 ce Mr. Goldbecl 

 1 . Weber — Ouverture dell ’ Opera “ ireischiitz 

 2 . Beethoven — Andante della 5 . * Sinfonia 

 3 , Rossini — Ouverture dell ’ Opera * * l’Assedio di Corinto . ’ 
 Rubinstein — Andante ( istrumenti ad arco ) . 
 T'scaikowsky- Walzer ( istrumenti ad arco 

 8 . Wagner — Calvalcata delle Walkire 

 Concert agreeably surprised German 
 British visitors , scarcely prepared heat 
 finished orchestral playing south Alps . 
 performance chiefly remarkable grace 
 refinement associated Italians , respects 
 “ wind ” immaculate . purity intonation 
 delicacy horns commencement 
 Overture rivetted attention , family 
 ‘ “ * wood - wind ” ’ appeared equal excellence , in- 
 stance , Paris Conservatoire - years ago . 
 unison violas violoncellos divine melody 
 flat second movement Beethoven 
 C minor Symphony begins , exquisite . 
 Overture Rossini , perfectly played , interesting 
 selection concert city ‘ “ Swan 
 Pesaro ” passed time ; 
 items modern composers , names music 
 longer strange uncouth sensitive Italian 
 ears , rapturously received . fact , close 
 attention audience , contained 
 large number University students appeared absorbed 
 music , unanimous burst applause 
 selection , deep - rooted 
 love devotion divine art possessed 
 Italians . E nce Bologna cultured taste music 
 indicated calibre Martucci periodical 
 Exhibition Concerts . evening 
 reluctantly obliged return northwards , Beethoven 
 Seventh Symphony good things pro- 
 gramme . testimony musical student 
 * * Liceo Rossini , ’ principal teaching conservatoire 

 lauree d ’ 
 rinnasio 

 Bologna , taken accurate , sign recent 

 advance music present admired 
 teachers students school Bach , 
 Mozart , Beethoven , Schumann , & c. , 

 assiduous study counterpoint considered sine quad 
 non de rigueur Bolognese musicians . , ssebala , 
 true motto , ‘ * Bononia docet 

 Specially interesting features m orning function 
 r2th ult . congratulatory es , chiefly 
 Italian Latin , University delegate 
 nation . Inthe case address England , 
 read Latin accomplished Professor Jebb 
 { contributed masterly Greek Italian 
 Ode Centenary ) , unconsciously severe satire 
 British pronunciation Latin observed 
 Bologna journal following day , caused considerable 
 amusement . mentioning language 
 congratulatory address delivered , Italian 
 reporter , opinion generally shared 
 countrymen , stated English representative 
 spoke English , * parlo il reppresentante del l’Inghilterra 
 inglese 

 words added Musical Exhibi- 
 tion , , understood , correspondent 
 little time examine . interesting portion 
 coliection placed , hurried visit , 
 altogether missed — , rooms upstairs , 
 app sroached “ scala , ” unusually mean 
 narrow Italy . catalogue published , 
 indicated viva voce aid native foreign 
 exploring ‘ parties . ‘ upper rooms referred contain 
 priceless exhibition MSS . autographs , 
 lent continental libraries private 
 collectors . probably unequalled collection old 
 ecclesiastical MSS . , 
 contributed chiefly Bologna * * Liceo Filarmonica , ” ’ 
 Italian musical societies libraries , follow- 
 ing autograms greatest ma 
 scores Haydn Mozart , famous * Exercise ’ 
 Counterpoint written fourteen 
 years age , diploma received 
 Bologna Philharmonic Society ‘ Liceo Filarmonica ” ’ ; 
 scores Beethoven opera ‘ Fidelio , ’ Ninth 
 Symphony , Septuor , & c. ; scores Bach , Cherubini ; 
 pianoforte music Clementi , lud 

 ddre 
 adaress 

 G , Op . 34 ( Ries ) , received 

 U 1e Trios violin , violoncello , 
 pianoforte , Rubinstein G minor ( Op . 14 , . 2 ) 
 Beethoven C minor ( Op . 1 , . 3 ) , Miss Hutchinson , 
 Mr. P. Kiefert , Mr. Ennis attractive items 
 excellent programme . Miss Julia Allen 
 efficient accompanist 

 Meeting music publishers , relative appoint- 
 ment Mr. Moul Agent - General Brit empire 
 |of Société des Auteurs , Compositeurs s , et Editeurs de 
 | Musique , pec Internationale de la Propriété 
 | Artistique et Litteraire , Monday , 3 , fol- 
 lowing resolutions passed . Mr. Ashdown voted 
 chair , introduced Mr. Moul meeting 
 fas chosen representative foreign musical interests , 
 felt sure trade glad hear 
 | selected . resolutions closed meeting 
 } moved respectively Mr. Thomas Chappell 
 | Mr. Augener : — * desirable come speedily 
 |as possible amicable decisions foreign proprie- 
 j tors musical copyright matters Berne 
 | Convention domestic legislation furnished 
 joew forms protection . ” ‘ Mr , Alfred Moul 

 n choir 

 able direction 
 Engli ‘ } s 
 voices , _ blind Spanish guitar player , Senor Mz 
 delighted club extraordinary tr : iscription 
 instrument Beethoven * * Moon 
 pieces expressively executed ; violin solo Tivadar 
 Nachez ; violoncello sclo Mr. Leo Stern ; 
 Mr. Herbert Thorndike , Mr. Percy Palmer , Mr. 
 Lloyd James ( new tenor irmi é 
 Franklin Clive . intendec 
 perform - music , sucl 

 Antigone ” Mendelssohn , 

 ice Princes ’ Hall , 

 eal eltbemaokimmlino inal : RA , 13th ult . , programme fi * Popu 
 € xcCiusls rly L making Manusci 7S 8) u S . 39 ax 
 ee ee Pairk ! ; ar Concert ’ ? model presen.ied numerous audience . 
 copyright works vested owner copyright : + 1 9 P 
 ; aa 4 ] ee ee nat fs : . ‘ tho : ‘ 2 ? | instrumental works Erahms second Sextet 
 persons making cop withou , : : cae ‘ seid 
 Pee * . d peeanent ae ie = ( Op . 36 ) , Beethoven Rasoumowski Quartet C ( Op . 
 said owner th Sige eee pea ee : : Sod . 
 , . 3 ) , Haydn Trio E , Boccherini familiar 

 damage 
 yn copyri 

 FOREIGN NOTES 

 Tue mortal remains Ludwig van Beethoven re- 
 moved suburban churchyard Wahringen 
 hoped prove final resting - place , 
 Central Cemetery Vienna , 22nd ult . 
 laid Schubert , close 
 grave Mozart . immense con- 
 course spectators addition officially engaged 
 ceremonies — musical societies parts 
 Austria Germany , representatives town 
 Bonn , composer birth - place , — 
 hearse , , followed 
 carriages . Dr. Angerer , popular Assistant - Bishop 
 Vienna , supported large number priests , performed 
 Funeral Service , Herr Lewinsky , 
 leading actors Burg Theater , delivered brief 
 impressive oration . musical performances forming 
 proceedings entirely selected 
 works great master memory 
 intended honour . gathering 
 professors Conservatoire conditions curi- 
 ously associated memory Beethoven . 
 great professor counterpoint , sum money 

 

 433 

 left recently Conservatoire order 
 professors sup Beethoven anni- 
 versary day devoted tohismemory . 
 testator Herr Marxsen , Altona , master art 
 great living musicians , Brahms . 
 died long ago age eighty - , life 
 curiously connects modern composers great 
 founders German school . pupil Von 
 Seyfried , constant friend Beethoven , 
 known Haydn Albrechtsberger intimately 

 Wagner characteristic music - drama , ‘ Tristan 
 und Isolde , ” performed , time Italian 
 soil , 2nd ult . , Bologna , received through- 
 enthusiasm , fact noteworthy 
 considering uncompromising character , musicaily 
 dramatically , work . translation 
 book Signor Arrigo Boito , Signor Martucci 
 conducted carefully prepared performance 

 compositions selected character calcu 
 display abilities taught . 
 hoven Sonata C sharp minor , , howev er , Se 
 obstacles facile ae , Schumann F : 1 es 
 Scherzando Mendelssohn , Impromptu Fantasia Chopi , 4 

 Adagio Allegro Beethoven Sonata ( Op . ror ) 
 played memory , crisp freedom ‘ prevent | 
 accuracy . ! Stratton selections Hiller “ Alia 

 Marcia , ” W. Sr cioso , 

 KILLaLor , LIMERICK ' oe Recital given St. Flannan 
 Cathedral 15th ult . , Mr. Charles Haydn Arnold , Organist 
 Choirmaster . es ume included Mendelssohn 

 Organ Sonata ( Op . 65 ) , Ancante G ( Léfébure Wcly ) , Prelude 
 Fugue E minor ( Bach ) , Romanza D minor ( W. T. Best ) , 
 “ Hallelujah , ” Mount Olives ( Beethoven ) , ‘ Worthy t Lamb ” 
 ( Handel ) . vocalists Misses B. A. Twiss , Rev 

 J. Kempston , Cathedral Choir 

 Leeps.—Encouraged reception accorded selections 
 compositions popular holid Dr. Spark 
 repeated items Saturday night , rth ult . , b etore 
 audience filling Victoria Hall . examples performe : 
 included Marche Iriumphale D ( ‘ Sardanapalus ” ’ ) , 
 originally written orchestra ; ‘ ‘ Lake , ” wl Toric 
 years ago purpose illustrating peculiarities beautiful 
 little echo organ added Victoria Hall organ 
 years ; Concertsiiick minor major , written 
 Leeds Musical Festival 1874 ; old Vesper Hymn , varia- 
 tions fugue . tribute memory deceased German 
 Emperor productions Borough Organist 
 replaced Beethoven Funeral March , composed death 
 hero , equally appropriate selection Chopin 

 devoted 

 BACH , J. S.—Symphony unknown Cantata . od 

 Violin Pianoforte L. Abel 6 
 BEETHOVEN.-—Funeral March Sonata . " Op . 26 , ss 
 ranged Harmonium Organ M. Oesten + 
 — Adelaide . Arranged Harmonium Pianoforte t 
 — Romance G. Arranged Harmonium Pianoforte 4 
 — — Seventh Symphony . Arranged Pianoiortes , 
 hands 20 
 BRAHMS , J.- Concerto ‘ Violin Violon cello , Or- 
 chestra . Op . 102 : — 
 Score , net 40s , Orchestral parts ie 548 

 Arranged Pianoforte Accompaniment 
 CHOPIN.—Funeral March 

