Whatever Saint-Saéns may think, the present

generation has become ‘so truly French’ as regards 
music only after having understood both Wagnerian 
powerfulness and weakness. We readily admit that 
it is as well Wagner should not be played in France just 
now, but for the same reason we should agree to lay 
aside the symphonies of Beethoven and Schumann, 
were it only to let them have a rest and thus allow us 
to hear a certain number of French works, old or 
modern. At this hour there is no less reason to play 
Wagner than Beethoven, but there is not any more 
either ; both are equally classical, and Saint-Saéns’s 
denunciation can in no wise alter this fact

The truth is that it would be highly more useful 
to help to speed the knowledge and influence of French 
symphonic works, than to decry the German geniuses 
of the past. That is the standpoint of the best known

THE MUSICAL TIMES.—January 1, 1917

played by Mr. Albert Sammons, whose great interpreta- 
tion of this work his become happily tamiliar——The 
concert given on December 4 introduced no novelties. 
The programme included the ‘Tannhauser’ Overture, 
Elgar’s ‘Enigma’ Variations, and Beethoven’s No 7, all 
finely played under Safonov. Miss Edith Evans, who was in 
excellent voice, gave a highly dramatic performance of 
* Ocean, thou mighty monster

THE BACH CHOIR, QUEEN’S HALL, DECEMBER 12

LONDON STRING QUARTET

On November 24, Beethoven’s Quartet in E flat, Op. 74 
Schumann’s in A, and Frank Bridge’s Quintet formed a 
attractive programme.——On December 8 the concert 
narrowly escaped disaster owing to the illness of Mp 
Warwick Evans, the ’cellist, and Mr. Wynne Reeves wa 
interned at the Beecham Opera where he leads the 
orchestra. But a good concert was contrived by the aid 
of Mr. Petrie (violin) and Mr. C. A. Crabbe (’cello

GLEE AND MADRIGAL SOCIETY: VISIT

Musical Festival in 1900. The Birmingham Festival 
Choral Society and the Hallé band joined forces, Dr. Sinclair 
conducting, and under such favourable conditions the 
performance of ‘Gerontius’ was quite on grand Festival 
lines, the choral singing being ideal, and the playing of the 
Orchestra glorious indeed. Capt. John Coates, who was 
engaged to sing in the title-réle, could not at the last moment 
fulfil his engagement, owing to military duties, but the 
committee was most fortunately able to secure Mr. Gervase 
Elwes to take Capt. Coates’s part. He put all his 
fervour and soul into his singing, and nothing could have 
surpassed his touching and emotional delivery. Miss Dilys 
Jones in her part of the Angel sang very finely. Mr. Charles 
Mott sang the part of the Priest and that of the Angel of the 
Agony, achieving his greatest success in the latter. 
His voice, which is of sonorous quality, completely filled 
the hall. Mr. C. W. Perkins gave splendid help at the 
organ, and Dr. Sinclair is to be complimented on the 
impressive performance

The Catterall second chamber concert of a series of five 
was given on November 29, under the auspices of the 
Birmingham Chamber Concert Society, at the Royal Society 
of Artists Exhibition Rooms. The programme included 
Haydn’s String Quartet, Op. 76, No. 5, in D minor, 
Beethoven’s Op. 131, in C sharp minor, and a Quartet, each 
movement being written by a different Russian composer, 
namely, Rimsky-Korsakov, Liadov, Borodin, and Glazounov. 
This work proved quite a novelty, the theme being based on 
the letters B-la-F, referring to the name of the celebrated 
Russian publisher Belaieff. It is full of colour, and was 
excellently performed

The Birmingham Choral and Orchestral Association, at the 
Town Hall, on November 18, gave a vivid and spirited 
concert-performance of Edward German’s opera ‘ Tom Jones,’ 
ably prepared and conducted by Mr. Joseph H. Adams. Its 
melodic structure lends itself well for concert purposes, 
especially the important work for principals, very grateful 
to sing being the soprano soli assigned to Sophia, the heroine, 
who was represented by Miss May Whitfield, the possessor of 
a fine and pure voice and who sings artistically. Miss Doris 
Russell, Mr. Wrigglesworth, and Mr. Alfred Askey com- 
pleted an excellent cast of principals. The choir and 
orchestra did it excellently

DUBLIN

At the Royal Dublin Society the following chamber music 
recitals have been given: on November 27, string orchestra 
conducted by Dr. Esposito, the programme including 
Concertos by Corelli and Bach (for violin and strings), 
Mozart’s ‘Serenade,’ and smaller pieces by W. H. Reed, 
Albanesi and Grieg; on December 4, Mr. Ellingford, of 
Liverpool, gave an organ recital, including Bach’s Toccata, 
Adagio, and Fugue in C, Mendelssohn’s first Sonata, and an 
effective arrangement of the Scherzo from Stanford’s ‘ Irish’ 
Symphony ; on December 11, Dr. Esposito, Signor Simonetti, 
and Mr. Clyde Twelvetrees played Schubert’s Trio, Op. 99, 
Beethoven’s Trio, Op. 97, and Signor Simonetti and 
Dr. Esposito played Bach’s Sonata in A ; on December 18, 
Mr. Max Mossel and Mr. Benno Moiseiwitsch played 
3rahms’s Op. 78 and Grieg’s Op. 13, besides solos. This 
was Mr. Moisiewitsch’s first appearance in Dublin

The Rathmines Operatic Society gave a week’s per- 
formances of ‘The Gondoliers’ and ‘Iolanthe’ at the

EDINBURGH

Madame Stralia, Mr. O. G. O’Brien, and Mr. Vallier, 
vocalists, and Miss Adela Verne, pianist, gave a concert on 
November 18. The programme was of a popular order, but 
of high artistic merit. On November 25, the second 
Harrison Concert re-introduced to Edinburgh Madame 
G. Suggia, the Spanish ‘cellist. Her playing was superb, 
and was undoubtedly the feature of the concert. Mesdames 
Flora Woodman and Ada Crossley, and Messrs. Maurice 
D’Oisly and Frederic Austin were the vocalists. Mr. Charlton 
Keith made an ideal accompanist. Prof. Tovey gave his 
second and third Historical Concerts on November 29 
and December 13 respectively. Carrying out the scheme 
outlined last month, Prof. Tovey gave a recital of 
Beethoven’s pianoforte works in the following order: Op. 2

No. 20, p. 27; No. 20, p. 31; No. 3; ‘Vieni Amore’ 
Variations ; Op. 101 ; Op. 10, No. 3; Variations, Op. 35 ; 
Op. 78; Op. 108. This list will give to the interested

solos. The new Australian soprano, Miss Kate Campi constr 
was solo vocalist, and made a favourable impression at ‘Vita. 
her first appearance at these concerts. The programme@ which 
the fourth concert, on December 2, was notable chiefly fori gear

variety—chamber music, organ music, and vocal solos. Banto 
first was sustained by a newly-formed combination, tht examp 
Fellowes String Quartet (Mr. Horace Fellowes, Miss Bess§ Violin 
Spense, Miss Emily Buchanan, and Mr. Andrew Templeton Samm 
who played Mozart’s Quartet in B flat major, No. 34, ang Violin 
Beethoven’s Quartet in A major, No. 5 (Op. 18), with goo pianof 
ensemble and fine insight. To many of the audieng dance 
the excellent playing of Mr. Herbert Walton, the gifted minut 
organist of Glasgow Cathedral, of pieces by Mozart, List Rar 
Bach, and R. S. Stoughton, made the strongest appedf pees’ 
Madame Marthe Terrisse, who made her first appearans| song ‘ 
here as solo vocalist, gave refined performances of songl «pet

covering a wide range of style. The outstanding featured 
the fifth concert, on December 9, was the particularly fir 
playing of compositions by Bach and Scarlatti by M. Arthey 
de Greef, the Belgian pianist. He was also associated wit! 
Miss Margaret Holloway in the performance of Schumann 
Sonata in D minor, No. 2 (Op. 121), the second and thir 
movements of which proved the most acceptable to th 
audience. As solo violinist, Miss Holloway played 
excellently a Sonata in A major by Handel, and numbey 
by Francis Francceur and J. S. Henderson. Madam 
Jeanne Jouve, the vocal soloist, sang with much acceptang 
songs by Meyerbeer, Fauré, Marcello, and Vaccaj. Th 
sixth concert, on December 16, ranks as one of th 
finest given this season. Mr. Albert Sammons as violinis) 
and Mr. Wassili Safonov as pianist, gave magnifices 
interpretations of three Sonatas for violin and _pianofort] 
by Beethoven, viz., C minor (Op. 30), No. 2, G majaj 
(Op. 30), No. 3, and A major (Op. 47), the ‘ Kreutzer 
Miss Lillie Wormald sang an irreproachable selection d 
songs with splendid effect. Both vocal and instrument 
soloists this season owe very much to Mr. Wilfrid E. Seniay 
for his artistic aid as pianoforte accompanist at ail th 
concerts. 
The enormous popularity of the Glasgow Orpheus Choi 
(Mr. Hugh S. Roberton, conductor) was evidenced by th 
fact that every available inch of space in St. Andrew’s Hal 
was booked a week before the annual Scottish Concert 
December 7. This popularity is thoroughly deserved, fo 
no finer choral interpretations than those of Mr. Roberton 
cheristers can be heard north of the Tweed. The distinctio 
of their performances is due not so much to the personnel 
the Choir as to the personality of the conductor, who i 
gifted with an imagination which he infuses into the presenta 
tion of what even may be deemed commonplace choral music 
The most notable number on the programme was Mr. Granvill 
Bantock’s ‘Sea Sorrow’ (from ‘Songs of the Hebrides’) 
of which the Choir gave a ‘first performance.’ This charming 
form of choral arrangement is one in which Mr. Bantod 
shows real genius, and his treatment of the old Hebrides 
song is quite as masterly as that of his ‘ The Death Croon 
which was also sung. Miss Boyd Steven's sympatheté 
singing of the ‘ Lament’ in ‘ Sea Sorrow’ was most impressive 
A departure from usual practice at the Choir’s concerts wai

the employment of members of the Choir as solo vocalists, ami

LIVERPOOL

Sir Henry Wood is a thrice-welcome visitor here, and his 
direction of the fourth Philharmonic Concert on November 
21 resulted in an alert and animated performance, notably 
of Dohnanyi’s Suite in F sharp minor, a most delightful 
example of absolute music. Its sustained melodic interest, 
constructive ingenuity, and colour are amazing. Smetana’s 
‘Vitava,’ and Rimsky-Korsakov’s ‘Chanson Russe’ (in 
which latter the choir sang the revolutionary folk-song ‘ The 
dear old oak-tree’) were well received, as was Granville 
Bantock’s Prelude to ‘Sappho,’ which is an eloquent 
example of the composer's quieter manner. Beethoven’s 
Violin Concerto was finely played by the soloist, Mr. Albert 
Sammons, who also introduced a characteristic Légende for 
Violin by Delius, which was ably accompanied on the 
pianoforte by Miss Helena McCullagh. Grainger’s ‘ Clog- 
dance,’ ‘Handel in the Strand,’ provided four inspiriting 
minutes for those who remained to the end

Ranging from Wilbye’s madrigal ‘ Sweet honey-sucking 
bees’ to Delius’s Pianoforte Concerto, and from Purcell’s 
song ‘ Ye twice ten-hundred deities’ to Granville Bantock’s 
‘Hebridean’ Symphony, the programme of the fifth 
Philharmonic Concert, conducted by Mr. Bantock on 
December 5, could not be considered as lacking in range or 
variety. To these interesting items was added Elgar’s 
choral work ‘ For the Fallen,’ in which the soprano solo was 
sung with unaffected feeling and fervour by Miss Edith 
McCullagh. This, alas! too timely elegy made a deep 
impression. Mr. Bantock’s ‘ Hebridean Symphony ’ chiefly 
sustains his reputation as a master of orchestral device. The 
music, it need hardly be said, is not cast in the usual 
symphonic form, and viewed as an orchestral poem, the 
treatment of the composer’s Hebridean material is largely 
objective—indeed oppressively so in the ‘battle-music,’ 
which is sound and fury without parallel. In the 
Pianoforte Concerto of Delius, a continuous work, original 
in thought and individual in expression, Mr. William 
Murdoch played magnificently in the tremendously difficult 
solo-part. The young Australian pianist, who appeared in 
khaki, has a bent towards novel and peculiar music, as shown 
later in items by Debussy and de Severac. Havergal 
Brian’s sprightly and energetic ‘ Festal Dance’ closed a fine 
concert. The vocalist was Mr. Robert Radford, who 
has been heard to greater advantage than in Stanford's 
‘Sea Songs

The second concert of the Akeroyd Symphony Orchestra, 
on December 2, was notable for the impressive performance 
under Mr. Akeroyd’s direction of the ‘ Pathetic’ Symphony, 
which still evidently retains a firm hold on the general 
public. Schumann’s fine Overture to his unsuccessful 
opera ‘ Genoveva,’ of which Spohr thought so highly, was 
aiso interesting hearing, although it sounded a gloomy note 
which Tchaikovsky’s music deepened. The ‘ Tannhauser’ 
Overture, taken rather sedately, brightened matters some- 
what, as did the delicate charm of the Ballet Suite, ‘ Cephale 
et Procris’ (Grétry-Mottl), music which suited the lesser 
weight of tone of the violins held by the lady-players. The 
vocalist, Madame Elsa Stralia, found favour by her brilliant 
singing in the ‘Jewel Song’ from ‘ Faust,’ and in the 
contrasted mood of Mascagni’s ‘ Voi, lo Sapete,’ but Arditi’s 
‘Il bacio’ was less well chosen. Rimsky-Korsakov’s 
‘Chanson Russe’ made a cheerful Ana/e

At the second meeting of the Rodewald Concert Society, 
on December 11, the Arthur Catterall Quartet played the 
delightful composite quartet ‘B la F’ (Belaieff), written 
in the celebrated Russian publisher's honour by Rimsky- 
Korsakov, Liadov, Borodin, and Glazounov, and also Ernest 
Walker’s Phantasie Quartet, a charming work. A lucid 
and balanced performance of Beethoven's C sharp minor 
Quartet, Op. 131, completed the programme

At the meeting of the local Association of Organists and 
Choirmasters, on December 4, a lecture was given on 
‘Carillons’ by Mr. W. A. Roberts, with interesting lantern 
views of bell-towers and carillons at home and abroad. 
Musical illustrations were played, showing the influence of 
bells upon composers, ranging from Purcell to Dukas, and 
incidentally the provision of a representative English 
Carillon for the new Liverpool Cathedral was suggested

Sir Thomas Beecham’s programme of November 30 was 
finely balanced, and for sheer orchestral delight hard to 
match much less surpass. Grandeur and breadth were in 
the ‘ Mastersingers’ Overture and the Brahms Symphony 
No. 2. Rarely has the imposing pageantry of the 
‘ Mastersingers’ been so adequately pourtrayed

Sir Thomas has now played three big Symphonies here: 
César Franck’s, Beethoven’s No. 3, and Brahms’s No. 2. 
The first and last produced on the hearer that sense of abso- 
lute finality, the conviction growing with every bar that his 
reading was as close to the ideal as one is ever likely to get; 
but this confidence in the conductor’s surety in symphonic 
work was disturbed when he tackled Beethoven’s ‘ Eroica.’ 
He invites a re-adjustment of attitude towards Beethoven: 
if the truth were known, probably Wagner had not a little 
to do with Richter’s Beethoven readings, and Manchester folk 
had almost come to regard Richter’s Beethoven much as the 
Meiningen folk thought that there were no Brahms wter- 
pretations worth a rap alongside Steinbach’s. It is certainly 
odd that Sir Thomas Beecham should achieve such notable 
grandeur in Wagner, and prefer in the big Beethoven to insist 
rather on delicate fancy and grace than on the more imposing 
treatment to which Richter accustomed us

NEWCASTLE AND DISTRICT

YORKSHIRE. 
LEEDS

On looking through the records of last month's concerts 
I find Leeds seems to have been exceptionally busy, as if the 
war had not proved much of a check to musical activity. It 
is even possible to note the establishment of a new series of 
concerts, Miss Edith Robinson, the Manchester violinist, 
having begun a series of weekly mid-day concerts, at low 
price and with the privilege of smoking, which should make 
them particularly welcome to business men who live outside 
Leeds. So far four have been given: on November 28 the 
concert-giver brought her quartet party, and, with Mr. 
Herbert Johnson, gave Schumann’s (Juintet ; on December 
5 Mr. Alexander Cohen and Miss Kathleen Frise Smith gave 
a highly sympathetic performance of César Franck’s Sonata 
for Violin and Pianoforte ; on December 12 Madame Terrisse, 
an accomplished French vocalist, gave a recital of French, 
Italian, Russian, and British songs, and achieved a remark- 
ably even level of excellence; on December 19 Miss 
Robinson and Mr. Frank Merrick played the ‘ Kreutzer’ 
Sonata. Like all new enterprises, it will take time 
before it will become known, but if Miss Robinson be 
only encouraged to persevere, there should be room 
for concerts like these. On November 22 the Leeds 
Choral Union, under Dr. Coward, gave a performance of 
Verdi’s ‘ Requiem’ which was chorally of great merit, and 
the soloists—Miss Stiles-Allen, Miss Phyllis Lett, Mr. 
Brearley, and Mr. Herbert Parker—were well-chosen for 
their task. At the Parish Church the annual service of 
which Brahms’s ‘German Requiem’ is the feature, took 
place on December 5, when Mr. Willoughby Williams, the 
organist, directed a sympathetic performance. Mr. W. 
Hayle sang the bass solos very artistically, the soprano solo 
being given to five choir-boys. Mr. John Groves was at the 
organ, to which were added drums and pianoforte to 
secure some variety of colour. On December 6 the 
Leeds New Choral Society, conducted by Mr. M. 
Turton, gave Mendelssohn’s ‘St. Paul.’ The choir 
was alert and well-taught, and the  soloists—Miss 
Annie Scott, Miss Vipond, Mr. George Wood, and Mr. 
John Browning—were fully efficient. Circumstances 
did not permit of an orchestra, but Mr. Fricker did much 
to atone for its absence, though the Town Hall organ is 
singularly unfitted for a task requiring some refinement and 
subtlety of tone-quality. On December 9 the Leeds 
Saturday Orchestral Concert was an excellent one. Leeds 
has some reason for congratulating itself that it could supply 
orchestra, conductor, and soloist, all home-made and all 
displaying so much efficiency. In Beethoven’s Violin 
Concerto, the solo part was played by Mr. Alexander Cohen, 
the leader of the orchestra, who entered thoroughly into the 
spirit of the music, giving a thoughtful reading, and intro- 
ducing some clever cadenzas of his own composition. A 
novelty was afforded to Leeds in the shape of Ravel’s brilliant 
and very fanciful ‘Mother Goose’ Suite, and there was 
at least a topical appropriateness in Enesco’s first ‘ Roumanian 
Rhapsody,’ a piece built on Liszt's pattern, based on 
characteristic themes, but calling for some compression, since 
its repetitions are rather tiresome. Mr. Fricker conducted 
with sympathy and judgment. At the Leeds Bohemian 
Concert, on December 13, Mr. Alexander Cohen’s Quartet, 
which now includes two ladies, gave most praiseworthy 
performances of Beethoven’s A minor Quartet (Op. 132), 
McEwen’s very interesting ‘ Bay of Biscay,’ and Dvorak’s 
(Juartet in F. Another chamber concert, given by 
Mr. Fulford’s Quartet at the Leeds Arts Club on November 27

when String Quartets by Godard, Waldo Warner, Frank 
Bridge, and Glazounov, were played, can only be briefly 
mentioned, as must Mr. Percy Richardson’s pianoforte recital 
at the University on November 21, and his organ recital (on 
behalf of ‘ Music in War-time’) at St. Chad’s, Headingley, 
on November 28. Both events were of more than passing 
interest, as also was a lecture-recital of Serbian folk-songs 
given by Miss Vivien Edwards, at the Leeds Arts Gallery on 
December 5. 
OTHER TOWNS

1. S, Bad MINIATURE FULL SCORES. 
ed Hollins ORCHESTRAL. OPERAS—CoMPLETE

G. Ould 
BEETHOVEN. BELLINI, V. s. d

G. Ould SYMPHONIES (with modern notation by Umberto Giordano). Norma * 5 ° 
. G. Ould eC. Oe - ; DONIZETTI, G. 
Je ° J . rc, P. . ; 
iam Sewell . = ae — 36 i : oe: 1.’Elisir d'Amore , _ ; : 5 0 
yt 3. InE flat. Op. 55. roica : 3 0 — 
iam Sewell} {In BR flat. Op. ¢ P PONCHIELLI, A. 
iam Sewell s In C minor. Op. 67 La Gioconda oe

Night sinks on the wave Henry Smart

Martin Roeder 
Bateson 
by H. Leslie) 
T. Weelkes ad. 
Nights, The G. Roberti 3d. 
Noble be iby lite (Canon for 6 v.) 
Beethoven 3d 
H. Hofmann 2d 
Brahms 3d. 
* Now May again (4 v.) Mendelssohn 2d 
* Now sleeps the crimson (4 v.) 
G. von Holst 2d 
. J. Brahms 2d. 
B. Luard- Selby 2d. 
Nymphs in the Rhine, The Marschner 4d 
* O beautiful violet (2 v.) C. Reinecke ad 
O clap your hands E, H, Thorne 
O grateful evening .. C, Reinecke

Night Song (2 v.) 
* Nightingale, The a 
The (Arr

