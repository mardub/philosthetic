price 2 . 6d . ; scarlet cloth , 4 

 ready . 
 BEETHOVEN ’ FIDELIO 

 german english word 

 publish . new number Popular Series piece 
 found follow Gperas : — 
 . 25 . Der Freischutz . 28 . Tannhiuser . 
 26 . il flauto magico . 29 . Crown diamond . 
 27 . Zampa . 30 . L'Ombre ( Flotow new opera ) . 
 Solos , 2s.6d . duet , 3s 

 CATHEDRAL GEMS , LOUIS DUPUIS . 
 1 , NANTES.—Fantasia , subject Mozart 12th Mass 
 2 . WURMS.—Fantasia , subject Weber Mass G 
 8 . ROTTERDAM — Fantasia , subject Haydn 8rd 
 ( imperial ) Mass pas pai eco eco ose o < e 
 4 . MALAGA.—Fantasia , subject Beethoven Mass 

 n - ose oo fie oo 
 5 . ROUEN . — Fantasia , subject Gounod Messe 
 Solennelle ee oo ao pe da ave 
 6 . CAEN.—Fantasia , subject Rossini Stabat Mater 
 7 . MAYENCE.—Fantasia , subject Haydn Ist 
 ee RT RG rs bre 
 8 . COLOGNE.—Fantasia , subject Mozart lst 

 especially precarious state MadJle . Marimon 
 health allow rely 
 individual attraction . sympathy 
 Mr. Mapleson attempt revive Ballet . 
 old‘‘King Theatre,”’this entertainment flourish 
 glory ; opening 
 Covent Garden Opera , lyrical drama 
 assert undivided power , 
 dancing occupy far public 
 attention . endeavour resuscitate Ballet , 
 special feature , step 
 ward ; desire aid cultivate 
 taste appreciation high work 
 operatic art scarcely regret result 
 year experiment . dancing Madlle . Katti 
 Lanner Ballets incidental Operas , 
 , materially aid general effect . need 
 scarcely engagement Sir Michael 
 Costa conductor orchestra 
 department establishment unusually attractive 
 season . seldom hear 
 accompaniment Operas perform ; 
 chorus teach sing tune , 
 think thatli ttle cause complain 
 material compose 

 Autumn year centenary 
 birth Beethoven celebrate Crystal 
 Palace excellent performance 
 Symphonies regular order , 
 concerto vocal piece . work 
 Philharmonic , 
 concert event similarly recognise . 
 Crystal Palace recent series term 
 ' ' Opera Concerts " acquire additional interest 
 substitution recital - know Operas 
 heterogeneous mass material 
 programme usually compose . provide 
 work continue judiciously select , 
 think little doubt exist innovation 
 decided improvement . concert 
 Mr. Henry Leslie , usual , 
 legitimate attraction season , excellence 
 choir assert delicate part- 
 music eminently fit . concert 
 , Mr. Leslie , 
 eminent operatic vocalist 
 engage , attend . 
 little performance 
 Sacred Harmonic Society . representation 
 usual Oratorios season 
 remarkable merit demerit 
 frequently point . 
 latt concert Albert Hall , 
 change think scarcely approve 
 solo vocalist . ' ' Oratorio Concerts " 
 earn thank real artist , 
 excellence manner music 
 perform , perseverance 
 promoter undertaking continue present 
 public work , formation 
 concert , comparatively unknown 
 England . Beethoven sublime Mass D — strangely 
 pass performance expressly 
 glorify great composer — 
 year ' ' Oratorio Concerts " 
 great effect season ; Bach 
 ' passion music " ( excessive beauty 
 public gradually familiarise ) 
 equally fine rendering . choir ( able 
 direction Mr. Joseph Barnby ) 
 place institution metropolis ; 

 zeal talent conductor repay 
 continuance prosperity asso- 
 ciation worthily gain earnest 
 wish truly desire healthy progress 
 art country 

 record principal event season 
 complete omit mention 
 manner true position musical artist 
 acknowledge conferring 
 honour knighthood worthily 
 earn right distinction . Sir Sterndale 
 Bennett , Sir Julius Benedict Sir George Elvey 
 look representative man ; 
 engage active pursuit 
 profession , official recognition art 
 unfortunately hitherto 
 neglect high quarter , constantly 
 publie notice , fail advance 
 music musician estimation , 
 especially accustom wait 
 mark royal favour admit possibility 
 existence social artistic worth 

 BEETHOVEN FESTIVAL BONN 

 special correspondent 

 Tue celebration centenary birth 
 Ludwig van Beethoven native town 
 arrange place , remember , 
 time year , postpone in- 
 definitely account war break 
 France Germany 

 imagine stirring 
 sanguinary event follow immediately 
 declaration war , Musical Festival 
 place period profound peace 
 month date 
 originally fix 

 Concert little remark , 
 wholly successful 
 performance , second eminently 
 — Choral Symphony render man- 
 ner long remember hear 

 excursion Rhine , pic - nic , illumination , 
 procession Beethoven statue , etc . , form 
 interesting subject dilate space permit . 
 suffice state , look 
 — view , satis- 
 actory earn minded musician 
 manner Festival carry . 
 glorify memory great musical 
 genius world , eminent 

 musician place service disposal 
 Committee , , thought reward , 
 week valuable time , look 
 privilege . certainly Germany 
 know honour great man 

 Epinsureu.—During visit British association : 
 ber assemble music class - room 
 University hear organ Recital Dr. Oakeley . 
 programme iste ition organ , 
 second adaptation arrangement instrument . 
 needless performance afford high 
 gratification large audience , applause spon- 
 taneous enthusiastic 

 Keswick.—An interesting organ recital 
 Parish Church Crosthwaite 28th July , Mr. P. T. 
 Freeman , audience comprise principal family 
 neighbourhood . programme select work 
 classical writer , include Mendelssohn 
 organ Sonatas , Prelude Fugue composer , Sir G. 
 Elvey Festal March ( write marriage H.R.H 
 Princess Louise ) , grand Fugue , D , Bach , Hallelujah 
 Chorus , Beethoven Mount Olives . performance 
 listen attention 

 New Bricuton.—The series popular 
 classical concert Wednesday evening , 23rd ult . , 
 interesting programme provide . open 
 Beethoven Trio C minor , op . 1 , . 3 , play Mrs. 
 Beesley , Mr. H. Lawson . Sig . Bodani . Mrs. Beesley Mr. 
 Lawson admirable rendering Beethoven Sonata F 
 pianoforte violin . Mrs. Beesley performance Weber 
 " moto continuo , " pianoforte , wae admired , 
 solo violin violoncello . Madame 
 Billinie Porter vocalist , display charmingly 
 fresh voice , cultivated musician - like style 

 Newport , Iste Wicur.—A festival choir 
 Isle Wight hold St. Thomas Church , 
 Thursday , 10th ult . , successful character . 
 voice number 400 , week previous 
 advantage training great musical knowledge 
 Rev. W. H. Nutter , M.A. , spare pain perfect 
 choir important 
 perform occasion . morning order service 
 — Hymn ( processional ) , ' ' church foundation " ( S. S. 
 Wesley ) ; Venite ( Thorley ) ; psalm ( Hodges , Goldwin , Stainer ) ; 
 Te Deum ( Mornington ) ; Benedictus ( J.T. Mew ) ; anthem , ' o praise 
 Lord " ( Goss ) ; hymn sermon , ' ye boundless 
 realm joy " ( Darwell ) ; hymn ( recessional ) , ' ' o happy band 
 pilgrim " ( harmonize Kocker ) . singing generally 
 mark great precision , rendering anthem 
 mention having particularly meritorious . Rev. W 

 20 number , 2 . ; volume , cloth , price 36 

 content . 
 61 . overture , " ' Athalie " ar oe Mendelssohn 
 62 . Motett , ' hear prayer " ove om Mendelssohn 
 Gavotta e Rondo , sixth Sonata 
 Violin eee eos ere eo » Bach 
 63 . overture ( Jessonda ) ... ase oe ew Spohr 
 64 . Grand March D ( op . 45 ) si os Beethoven 
 Andante sonata C ( 4 hand ) Mozart 
 65 . Andante ( clock Movement ) , 4th symphony D Haydn 
 Air Chorus , " Almighty ruler sky , " 
 ( Joshua ) ... oon oon ven Handel 
 66 . Andante Cantabile , 4th symphony C nls Mozart 
 Air Chorus , ' ' Shali 1 Mamre fertile 
 plain , " ( Joshua ) ... ace ese Handel 
 67 . sonata D major , compose Pianoforte , 
 4 hand , ( op . 3 ) eco ose ie Mozart 
 68 . allegretto 4th Symphony ... Mendelssohn 
 Andante con Variazioni Serenade 
 Flute , Violin Viola ( Op . 25 ) Beethoven 
 69 . War March o7 Priests ( Athalie } ove + . Mendelssohn 
 Andantino , second Entr’acte Drama 
 ' * Rosamunde " ono a0 « . F. Schubert 
 70 . duet Chorus " Sion head shall raise " 
 ' « tune harp song praise " ( Judas 
 Maccabeeus ) ve = Handel 

 Andante Pianoforte Duets ( op . 85 , . 3 ) R. Schumann 
 Andantino Pianotorte Duets ( op . 85 , no.6 ) R. Schumann 
 71 . Andantino Pianoforte Sonata minor 

 Op . 164 ) . xs ec ...   F. Schubert 
 chorus , ' ' God find guilty " ( Occasionai 
 Oratorio ) ... eco ae Handel 
 Marziale Chetestertette Pieces 
 Pianoforte ( op . 68 , . 29 ) wee « . R , Schumann 
 Andantino Characteristic Pieces 
 Pianoforte ( op 68 , . 15 ) . R , Schumann 
 72 , Adagio trio ( @p . 3 ) < > Beethoven 
 March , C miner eee F. Spindler 
 73 . chorus , ' ' swell chorus " ( Solomon ) ove Handel 
 Prelude , C minor nce ban f , Chopin 

 Marche Religieuse . 7 Adolphe Adam 
 74 . harmonious Blacksmith . ' Aly , Varia- 
 tion " ' 5th Suite de Piéces pour le 

 Clavecin " Handel 
 Andantino , Piindes Musicales ' Pianoforte 
 ( op . 94 , . 2 ) don oop « . F. Schubert 
 Largo Pianoforte Sonata , major , 
 ( . 2 , op . 2 ) nee ons Beethoven 
 75 . allegretto 4th sonata ( op . 70 ) Weber 
 Tema con Variazione Serenade ( op . 8) ... Beethoven 
 short prelude fugue C asp .. dl , Krebs 
 76 . fugue G minor , Pianoforte Works ... J. 8 . Bach 
 Fantasia F , compose mechanical Organ Mozart 
 77 . Sarabande Gavotte E ( Suites Francaises ) ... Bach 
 Gavotte G ditto ais Bach 
 Gavotte Musette G minor ( Suites Anglaises ) Bach 
 Sarabande F ditto sve Bach 
 Sarabande Bourée minor ditto pee Bach 
 Gavotte Musette d minor ditto Bach 
 78 . chorus , " music spread thy voice " ( Solomon ) Handel 
 Chanson Orientale ( op . 66 , . 4 ) ... R. Schumann 
 Fague b mivor nag oe Albrechtsberger 
 Fugue F ( Suite de Piéces ) aa le Handel 
 79 . chorus ; " hallelujah " ( Occasional Oratorio ) ... Handel 
 Minuetto ( Notturno , wind instru- 
 ment , op . 34 ) ae sa ote Spohr 
 Polacca jditto ditto ae Spohr 
 Weber 

 80 , overture , " Jubilee " ose 

 thisis Jehovah Temple ... ditto u 
 far thy path ditto } 
 night depart Lobgesang 3 
 313 thank God Elijah 4 
 317 shall light ... ditto 4 
 187 , soul 42nd Psalm 1 
 188 , mysoul ( Chorus ) ditto 3 
 189 ye natjon , offer Lobgesang 3 
 338 doth Lord eve Elijah 4 

 BEETHOVEN " ENCEDI . " 
 ( mount olive 

 185 { 
 186 

 195 o praise , ye nation 3 
 196 hallelujah ... oa ea 
 BEETHOVEN MASS INC 
 190 Kyrie — thee 13 
 Gloria — praise Lord eee 
 ui tollis — ear sup- 
 191 ' plication eee ' 
 Quoniam — thou art holy 
 Credo — glory great worship 
 192 et incarnatus — o Lord , ear 4 
 et resurrexit — thou " exalted ... 5 
 et vitam — u lage ye Lord 
 Sanctus — holy , holy ... 
 198 { Benedictue — peng 5 ooo - _ s 
 Agnus Dei — hear cry vos 
 1 % { Dena nobis — bless bet ioe Sieed } s 
 WEBER MASS CG . 
 Kyrie Eleison eee eee ) 
 304 { TUTN uponthelord ... J 44 
 305 | Gloria excelsis eee ~ ] 3 
 \ praise Lord . ate 
 credo oe ewol oa 
 306 | nation shall service } " 
 Sanctus Benedictus s ) 
 307 { hoty holy , holy ... eee 2 
 bless 
 Agnus Dei dona nobis ove 
 308 { stew thy favor unto thy people ... > 1 } 
 look favor thy people 

 _ MUSICAL TIMES.—Serremsper 1 , 1871 

