March ee 3 0 
Mlustrated with Portrait in eGebsilik: 
LO UIs DUPUIS. 
CATHEDRAL GEMS :— 
No. 16. Lincotn.—On weniete from Haydn’s an 
Mass aad 3 0 
17. Ery.—On eibbacts from “Mozart? s ‘ica 
Mass a3 ate % es ‘Sea 
GRAND OPERATIC DUETS. 
Arranged by 
No. 1.—Lohengrin (Wagner) J. Rummel 5 0o 
2.—Tannhiuser (do.) are a7 5 0 
3.—The Crown Diamonds Wz: pa 5 0 
4-—Galathée pA 4 0 
5-—La Marquise ibelenaes) a 4 0 
6.—Ernani (Verdi) ss ae J. Lesage 5 oO 
7.—Il Don Giovanni (Mozart)... ib 5 0 
8.—La Gazza Ladra (Rossini)... nd 5 0 
Wiha PAPE. 
Il Trovatore (Verdi) ... 4 
Martha (Flotow) 4 0 
Saladin Grand March 4 0

JOHN OWEN. 
MELODIOUS MELODIES :— 
Twenty Subjects selected from the Works of Beethoven, 
Mendelssohn, Schumann, Heller, &c., arranged for the 
Organ or Harmonium. Price 2s. 6d. nett

Now ready. 
SONGS FOR CHILDREN. 
Harmonized for School purposes by MIcHAEL WATSON. 
Price Sixpence. 
This Selection consists of popular Copyright subjects

process of attaining it are very different things. 
Assuredly the latter as well as the former can be 
made interesting, and the degree in which it is made 
so will depend upon the method and tact of the 
teacher. But that it can be carried on without 
trouble, as a kind of play; that the acquirement of 
anything worthy of the name of musical knowledge 
or musical skill can be ‘made easy,’ is an ignorant 
misapprehension or a wilful misrepresentation.” 
These observations should be taken to heart by all 
engaged in popular musical instruction. Although it 
is evident that Mr. Hullah is opposed to the Tonic 
Sol-fa notation—and indeed to the ‘“‘ Moveable Do” 
method altogether—his examinations on both systems 
have been conducted with the utmost fairness; and 
the Committee of Council on Education may be con- 
gratulated upon having secured the services of a 
musician who brings with him not only a large amount 
of experience, but a zeal and devotion to the cause 
which cannot fail to be of the highest value

THE Daily Orchestral Performances, which are still 
given in the Royal Albert Hall in connection with the 
International Exhibition, have, during the past month, 
fully earned the good wishes of all who are interested 
in the advance of musical art. Not only have the great 
masters been amply represented by the performance of 
such works as Beethoven’s Eroica and Mendelssohn’s 
Italian Symphony, and many other equally well-known 
compositions; but what is still more important several 
works by young English composers hitherto unknown 
have been produced. Most of these, although perhaps 
lacking originality of design, have been remarkable 
for clever instrumentation, a symphony by Mr. Hamil- 
ton Clarke being perhaps one of the best. A sym- 
phony by Mr. Henry Gadsby, the last movement 
of which is particularly striking, also calls for especial 
remark. Several young singers have appeared, among 
whom Madlle. Arnim, Mr. Dalton, and Mr. Melbourne 
have been most successful; while pianoforte pieces have 
been very ably performed by Madlle. Le Brun, Miss 
Marian Rock, Miss Jessie Morison (a promising young 
pupil of Mr. W. H. Holmes), and especially. Mr. Franz 
Rummel, whose clever interpretation of Schumann’s Con- 
certo in A minor calls for special mention. Mr. Deich- 
mann, in the absence of Mr. Barnby, has conducted 
uniformly with great care and intelligence

THE death is announced of the Rev. W. Mercer, Vicar 
of St. George’s, Sheffield, and editor of the well-known 
Church Psalter and Hymn Book

s the composer’s contrapuntal power to great 
advantage. The subjects are well studied, and treated 
with much skill throughout, the movement forming an 
( animated and effective close to a duet, which reflects great

credit upon its composer. The Overtures are Auber’s 
“Zanetta,”’ Rossini’s ‘‘ Guillaume Tell,’’ Weber’s ‘* Der 
Freischiitz,”” Beethoven’s ‘‘ Egmont,” and Rossini’s 
“Siege of Corinth.’’ All these are most ably arranged, 
and will be found exceedingly effective in performance, 
the passages being most judiciously adapted for both 
instruments

Smmpkin, MARSHALL AND Co

DupLey.—Very commendable performances of the masterpieces of 
Handel have been given on different occasions by the Dudley Choral 
Society, in aid of charitable institutions, and having thus fully 
established its claim to public favour, it was no surprise that a 
numerous and fashionable audience should have assembled in the 
Public Hall on Tuesday, July 29th, to listen to the fine Oratorio, 
Samson. Considering the short time the Society had been engaged 
on the work, its execution was in every respect most satisfactory, and 
reflected much credit upon Mr. Eyland, the conductor, who had so 
earnestly exercised his acknowledged capacity for teaching. The 
overture elicited the heartiest applause, and the accompaniments 
throughout were thoroughly efficient. The principal soprano was 
Mrs. Sutton, whose singing of the airs, ‘‘ Ye men of Gaza” and “ Let 
the bright Seraphim,” was warmly appreciated. Miss Blower gave 
the contralto music with much taste, and Mr. G. Mainwaring was 
highly successful in the bass part. The most marked applause was 
gained by Mr. Bywater, who was in excellent voice, and gave a refined 
and artistic interpretation of the whole of the music allotted to the 
character of Samson. The choruses were rendered with decision and 
intelligence

LIveRPOOoL.—The concert given on the 12th ult. by the Representa- 
tive Prize Choir at the Philharmonic Hall, was one of the finest 
choral performances ever heard in Liverpool. Compositions by 
Mendelssohn, Schubert, Schumann, Beethoven, J. L. Hatton, Gibbons, 
Sir John Goss, &c., were faultlessly sung by the choir, and the solo 
parts were excellently rendered by Messrs. T. Foulkes, T i Hughes, 
and Robinson. Mr. J. Sanders conducted with much skill, and the 
warmest praise must be awarded to Mr. W. H. Jude for the efficient 
manner in which he accompanied the music on the pianoforte

MoRETONHAMPSTEAD.—An organ has been built for and erected in 
the Cross Chapel, by Banfield, of Birmingham, which is considered 
by many competent judges, to be the most complete and beautiful 
instrument of its size to be met with anywhere in England. The 
metal pipes are made of three-quarters pure tin, to one-quarter lead, 
a very brilliant and durable composition, but rarely adopted on 
account of its expense. The organ is perfect in every respect, and 
worthy of imitation, both in design and workmanship

ORGAN

Arranged by s. d. 
. Adam, Adolphe ... Marche Religieuse oo W.T. Best... 2 0 
Alexander, Alfred March He swe ove 2. 2 0 
Beethoven +. Military,March, Posthumous W. t. Sask « 0 
je +. Triumphal March (Tarpeja) os um 2 2 
Po ee Grand March in D (Op. 45) ee mm 220 
m= + Funeral March (Op. 26) ” 2 0 
me +» March, (Egmont) .. ” 2 0 
Best, W. T. os er for a 4 Festival, 
major... at Ps «w 3 6 
‘ eco Wedding March ... eee ” o- 3 0 
te «. Funeral March ... ond - aa 9 @ 
Cherubini +» Marche Religieuse, composed 
for the Coronation of 
Charles X. ae . HJ. Lincoln 1 3 
Chopin... oe Funeral March from mceph 
forte Sonata eee . W. T. Best... 2 0 
Calkin, J.B.  ... Marche Religieuse ... Se ae ae 
* +. Festal March ss psd oi 2 0 
Carter, William .... Processional March “ Placida" nee sow 20 
® George .. Grand Festival March. wal ow 8 € 
Collin, Charles .... Three Marches Iat3s.,2at2 6

Dussek... «. Marcia Funébre, Sonata, 
Op. 74, four hands W.J. Westbrook

Price 2s. each

No. 
81. March from the ‘‘ Ode on St. Cecilia’s Day” ... andel, 
Prelude and Fugue (E minor) from Scaiins Works Ntendelssonn 
82. Chacone (F major) andel. 
Andante con variazioni from Pianoforte Duets, Op. 3. Weber. 
Romanza ditto ditto Weber. 
See the Conquering Hero comes .. cco Handel. 
83. Funeral March “On the death of a hero,” from Piano- 
forte Sonata, Op. 26 poy eco pee - Beethoven, 
March, B minor, Op. 27. No.1 ... ey Schubert. 
Hunting Song from the ‘“‘ Waldscenen,” Op. 82 R. Schumann, 
Adagio from Pianoforte Duets, Op. 10 ... ove ° Weber: 
84. Fugue, in G minor .. ove ove ace ° Mozart. 
March Triomphale ... Moscheles. 
Fantasia from 6th Quartet, i in E flat major wee Haydm 
85. Allegretto, with variations and finale yo: pe 6th 
Quartet, E flat major .. ooo Haydr. 
Allegretto (G minor), Op. 14 eee C. Mayer. 
Chorus, “ goa é il cielo,” “Alcina Handel. 
86. Allegretto, Haydn

rom 2nd Quarter, in D minor 
flat major, from Piano Duets, ‘Op. 3 Weber. 
uartett,in D major... eee

Andante cantabile in 
Largo cantabile, 5th

87, Overture (D major), Pp. 15 Sp 
88. Prelude and Air, with variations from the ‘Suites de Pieces Handel. 
Andante (G major) Rosamunde ... . -. Schubert. 
Serenade eco . Speahee Heller: 
89. Andante, from T2th Sy mphony eco Mozart. 
Prelude and Fugue ie sea ore one pe oe Bach. 
Finale notturno, Op. 3 Spohr. 
go. Chorus, “ Thou shalt faabe me hear of joy and ‘gladness” 
(r51st Psalm) ose Handel. 
Evening music, Allegretto scherzoso (Op. 99) R. Schumann. 
gt. Chaconne, “ La Favorite” ... eco Couperin. 
Triumphal March “* Vom Fels Zum Meer”... Liszt. 
Chorus, ‘God is my King ans Bach. 
92. Gavotte, from the 6th V ehineele Sonata. ove Bach. 
Bourrée, from the 3rd ditto eco « Bach. 
Gavotte. (Orphée) ... eee ° ° Gluck, 
93. Adagio, from the third Quintett .. as Mozart. 
Chorus, “ Behold the wicked — their bow (Anthem 
“In the Lord”). _ ..< om an pa ‘a Handel. 
Adagio, Symphony inG ... aid ae poor - Haydn. 
94. Scherzo, 2nd Symphony on ose . Beethoven. 
Overture, Messiah Handel

Andante, Sonata, A major, Pianoforte and Violin, Op

a

Beethoven

Chorus, Go your way into his gates, Jubilate ead a Handel. 
Chorus, “ Tellit out omens the pecans! etiam O

343 "beatwicirsinne Mendelssohn 6d

Hearts feel that love Thee } Atti 6 
MassinD. .. pee pet Beethoven 3d 
Athalia .. ee es «. Handel 3d. 
Belshazzar ea te ae do. 3d 
Leonardo Leo 2d. 
Samuel Wesley 44

344. Kyrie eleison. 
345- The mighty power. 
346. By slow degrees

347- Dixit Dominus .. xn oe ve 
348.. InexitulIsraei ... Pa =< oe

349. Whereishe. Engedi.. ee +s o« Beethoven 3d. 
350. Jehovah, Lord God of Hosts.. ian ee «. Spohr 4d. 
351. O God, who in Thy heavenly hand. Joseph. .. Handel 3d. 
352. Come with torches. Walpurgis ms +» Mendelssohn 4d. 
353- I wrestle and pray a « J.S.Bach 4d. 
354- Gentle night, O descend. Cibo s we oe -» Spohr 2d. 
355- Though all thy friends prove faithless. Calvary do. 2d

356. His earthly race is run.. Ke v. do. do. ad. 
357- Domine salvam fac. Festival Te Deum .. A. S. Sullivan 4d

MACFARREN AND BERTHOLD TOURS

AUBER’S FRA DIAVOLO. DONIZETTI’S LUCREZIA BORGIA. 
1. Comrades, fill yourglasses  . ; P - ‘ 4 4d. | 32. Not a word. ‘ f ‘ ; P - 
En bons militaires. Non far motto. 
2, Hail, festal morning . ‘ ‘ » ‘ P ‘ . 2d. | 33. From his window . ‘ ‘ ‘ z 
Crest grand féte. « Eisehiaspia é la finestra. 
34. Would you know how to while away sorrow . 
AUBER’'S MASANIELLO. Il segreto per esser felice. 
. Allhail the bright auspicious day . F ‘ ad- * nee 
’ Du Prince objet de notre amour. 2 MOZART s DON GIOVANNI. 
4 All hail the bright auspiciousday . ; é 2 3 1d. | 35- Let's enjoy while the season invites us . "i . ‘ . 
Du Prince objet de notre amour. Giovinette, che fate all amore. 
5 BPD Dieu puissant! ae 1d. MOZART’S LE NOZZE DI FIGARO. 
6. Companions, come. (Fishermen's chorus) ‘ 2d. | 36. Come deck with flowers . 
Amis, amis, Giovani liete. 
y Behold the morninsplendour. . 2d. | 37- Noble Lady, fairest roses . 
Amis la matinée est belle. Ricevete, 0 padroncina. 
§. Come hither all who wish to buy. (Market chorus) 3d. | 38. Each voice now rejoices . 
Au marché qui vient de s’ouvrir Amanti, costanti. 
We come, we willavenge thee . a a e d. 
é Courons & la ~egnene. . ROSSINI'S IL BARBIERE. 
+10 Power benign. 39. Sir, we humbly thank your honour . ° é 
Saint bien heureux. Mille grazie, mio signore. 
fo, All hail, the noble victor. (Marchand chorus) . 6d. 
Honneur! honneur et gloire. VERDI'S IL TROVATORE. 
40. See how the darkness of night dissolves. (Gipsy chorus) 
BEETHOVEN'S FIDELIO. Bf Vedi! le fosche notturne. 3 
10, Oh what delight. (Prisoners’ chorus) 3d. | 42: Now the dice invite our leisure! . e 
O welche Lust. _ Or co’dadi ma fra poco. 
u. Farewell, thou warmandsunnybeam . . . . .  4d.| 4% Miserere Scene. OF A ® m2 oot 
Leb’ wohl, du warmes Sonnenlicht. VERDI'S RIGOLETTO. 
BELLINI'S I PURITANI. 43- Hush, in silence fulfil we our errand 
Zitti, zitti, moviamo a vendetta 
wz, When yonder bugle callsus) . . 1d. | 44. Unto a lonely abode directed. 
Pa ne la tromba squilla. ol Scorrendo uniti remota via 
j0% ! boc "ie ; 
A festa. WAGNER'S LOHENGRIN. 
y. Noble Arthur, welcome . . ee Poe . ° e 1d. 45. The call hath summoned us betimes 
Ad Arturo onore, In Frith’n versammelt uns der Ruf 
1S ey Pree thee . 2d. 46. We follow where he leads! ° 
16. Fatal Po o cara. “i Zum Streite voor y nicht ! 
BP : . . . : : $ ’ _ P . May every joy attend thee 
Ahi! dolor . : Gesegnet soll sie schreiten 
48. Faithful and true,we lead ye forth 
BELLINI’S NORMA Treulich geftihrt ziehet dain 
17. Hasten, ye Druids, the heights ascend . ‘ ‘i : . 2d. WAGNER'S TANNHZUSER. 
a tect echt O Druidi. 4 49. Hail, bright ee (the March). he ie ‘ 
ne . . Id. Freudig begriissen 
Norma viene. vith joy. (Pilgrim’s Ch 
19, Not yet gone? no, yet they linger . a a 
Non parti? finora é al campo 7 
2, Vengeance, vengeance . ; 1d. WEBER'S OBERON. 
Guerra, guerra! 51. Light as fairy foot can fall 
Lieve il pie cola volgiam 
BELLINI'S LA SONNAMBULA. 52. Bons and joy “oe - 
MeL Amina. o  «, .o.,,8 0 1d. ae Lae 
Viva! viva, Amina! 53. Glorytothe Caliph . . + 
22. Fairest flower of the mountains fic 6, «ORME ae besccake ps baron ee 
wnt lees non aha rosa So: ON Seatac eee 
23. When dusky twilight 1d. : W eel 
Ah fosco cielo. 55. For thee hath beauty (Women’s voices) . 
24. Here a moment we'll shelter and rest us 4 2d. ~~ te “ Mixed voices) 
Qui la selva é pit foita ed ombrosa. 56. 0. o. (Mixed voices 
WEBER'S DER FREYSCHUETZ 
DONIZETTI'S LA FIGLIA, 57. Victoria, victoria 
25. What pleasure, what gladness . ONG <6 end cee aiee Victoria, Victoria : 
Cantiamo, cantiamo. 58. The Bridal wreath for thee we bind 
26, Hark, how the drums arerolling . ‘ a F 1d. Wir winden dir den Fungfernkranz 
Sprona il tamburo e tncora. 59. The joy of the Hunter. (Huntsman’s chorus} 
27. prepien. en e “ . . 1d. Was gleicht wohl auf Erden 
ataplan, rataplan. 
game ROSSINI'S GUILLAUME TELL. 
’ 61. Brightly the rosy morn , 
DONIZETTI’S LUCIA. <Oucl jour sevein 
28. Let us roam through these ruins deserted 1d. | 62. Come, with flowers crown the bowers . 
Percorriamo le spiagge vicine. } Hymenée, ta journée ; 
29. Hail, to the happy bridal day . P . ° ° C % 1d. | 63. Hark, how the horns gaily sounding 
Per te d’immenso giubilo. | Quelle sauvage harmonie 
$0. What from vengeance yet restrains me . 2d. | 64. Hail to the mighty ruler . 
Chi raffrena il mio furore. | Gloire au pouvoir supréme 
3. With warlike minstrelsy e e P . Seer " 1d. | 65. Swift as the bird in summer sky (Tyrolean

D'immenso giubilo

