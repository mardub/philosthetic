A. Don’t console yourself that way. The public 
would look on and laugh anyhow. Remember Schu- 
mann and Berlioz—not bad critics, eh 2? Now here is 
Gounod. Does he know what he writes about ? The 
other day I saw Sullivan’s name in the list of con- 
tributors to a periodical; Stanford and Parry both 
dip their pens in critical ink; Corder is actually an 
editor. Look out, old fellow; the Philistines—I beg 
their pardon—are upon you and your order, and 
i there will be ructions, as the saying is. 
| C. What I suggest is that if composers take to 
| criticism, each man should confine himself to his own 
works, about which he surely ought to know more 
than anybody else

A. In that case, of course, matters would run very 
|smoothly, if not to edification. 
| C. And why not to edification? How you would 
rejoice if somebody were to discover—this is an age 
of “finds ’’—an elaborate criticism of Beethoven's 
Symphonies written by Beethoven himself

A. That would be a special case; but Jones or 
Robinson, writing about his own works, would throw 
up the lights unduly

made a suggestion which, no doubt, the conductors 
jof oratorio concerts will receive with becoming 
gravity. It is to the effect that at performances of 
“The Messiah” all present should take part in the 
| Hallelujah Chorus. ‘As usual,” writes this critic 
|anent a recent occasion, “the audience rose during 
|the Hallelujah Chorus, and why this should not be 
taken part in by all the voices of the audience it is 
hard to understand. There may be havoc played 
with some of the parts; to the majority it must be 
fairly well known.” ‘The audacity of this proposal 
almost takes away one’s breath. Its author is the 
Danton of musical journalists

THE musical world has been interested of late by a 
succession of * finds,” the regular and frequent occur- 
rence of which may possibly excite distrust in a 
certain orderof minds. Among the latest discoveries 
are the portraits of Beethoven's parents—these will 
require to be fully authenticated—and, in Manchester, 
some copies of early works by Mozart, as well as 
fragments, said to be in the master’s handwriting, of 
his youthful opera ‘“* Mithridates.” Being only copies 
of works easily accessible, these MSS. are not very 
valuable, but it is interesting to have them found in 
England. They are supposed to have been brought 
over by a Miss Harford, who, early in the present 
century, studied at Florence

Tue London School Board has assented to the 
introduction of pianofortes in the elementary schools. 
This will probably cost the unhappy ratepayers a sum 
amounting to nearly six thousand pounds for the experi- 
ment, and lead to results whose ends cannot be seen. 
As a contemporary asks: ‘*Can anybody suppose that 
if instruments are provided in one school the pianoforte 
movement will not spread? No doubt, as was urged by 
some members, ‘music hath charms’ to soothe the 
schoolboy’s breast, and it ‘improves their natures’; 
but any improvement of this kind will be entirely 
counteracted by the deterioration in the natures of 
the parents of the same boys owing to an increased 
‘Education Precept

Tue intended musical festival at Bonn, next May, 
in aid of the Beethoven House, is an occasion of per- 
fect propriety. Germany, which claims to be the

musical country par excellence, should not need foreign 
| help when the question is one of honouring her great 
composers. Anattempt to raise money in England 
ifor the Bonn house was not, we understand, very 
successful. The reason is clear. English amateurs 
are among the most ardent worshippers of Beethoven, 
but they see that his countrymen are the persons to 
create a local memorial of his genius

that English eagerness to speed the parting may

Why do so many vocalists prefer superfluous lag- 
ging on the stage to decent and dignified retirement 
when means begin to fail? Here is Miss Clara 
Louise Kellogg still ‘‘concertising” in the United 
States, and the papers are saying that her voice is a 
“ painful wreck.” The best years of an artist’s life 
are devoted to the erection of a worthy memorial in 
public esteem; the closing period is often spent in 
demolishing it

THERE is a Boston writer on music who holds 
that Beethoven’s ‘“ Pastoral” Symphony has become 
“somewhat threadbare and hackneyed.’ So, we 
suppose, have the beauty of a rural landscape; the 
murmur of the brook; the grandeur of a summer 
storm, and the feelings with which the toil-worn man 
finds comfort on the bosom of his great Mother. All 
threadbare, all hackneyed, save the creation which 
will, to-morrow, be thrown aside in turn

THE same shining light of musical criticism tells us 
that the slow movement of the ‘“ Pastoral” js 
‘‘soporific,” and one part of it ‘“nauseatingly 
reiterated,” while the storm is a “‘tempest in a tea. 
pot.” Ifthe idea were not too absurd even for them, 
we might suppose that the Wagner fanatics have 
made up their minds to befoul and render loathsome 
all music save that of one man

is he (the editor) an enthusiast for the freedom of the 
press ? When he protests that the Growler man is 
silly, and tries to put him down, is he benefiting 
his own cause ? We ask for information

THE absurdity of insisting upon the repetition of 
a piece has never been more obviously displayed than 
by Miss Janotha, who, at one Concert, for an encore 
to Beethoven’s “Sonata Appassionata,” played the 
same composer's Variations on “ Rule, Britannia,” 
and at another, for the ‘* Moonlight Sonata” substi- 
tuted Chopin’s ‘* Marche Funébre

On the authority of the British Medical Fournal, 
enemies of the performers on bagpipes are amply 
tevenged upon their persecutors, for we are told that 
by the friction of the mouthpiece of the pipe the 
teeth of the players are—not ‘set on edge,” like 
those of the listeners but—actually severely injured

Tue Overture and Entr’actes for Mr. Irving’s pre

sentation of ‘‘The Master of Ravenswood” at the 
Lyceum in the autumn have been completed by Dr. 
A. C. Mackenzie, who, as a Scottish composer, must 
be considered peculiarly qualified for the task. 
| Mr. Barnsy took the chair, on the 24th ult., at the 
jlast of the course of Lectures on Sunday School 
|Singing, which have been delivered by Mr. W. 
| McNaught before the members of the Sunday School 
| Union. 
THERE is a Beethoven Place in Chicago, and a 
| Mozart Square in New Orleans. A careful search of 
| the map of London is necessary to find out a Beet- 
| hoven Street somewhere up Kilburn way

One of the best portraits of Madame Albani we 
have ever seen appears in the American Musician of 
| March 2g. It isa full length figure in the dress of 
| Desdemona

281

low baritone. The audience much appreciated the quality 
of his voice in his solos, which were ‘“ Qui sdegno,”’ from 
J] Flauto Magico,” and Schumann’s * I'wo Grenadiers.” 
The orchestra, under the ever-careful direction of Mr. 
Manns, surpassed itself in the performance of the Over- 
ture to Weber’s “Oberon,” the Pastoral Symphony by 
Beethoven——in which the representation of a Summer

Storm was brought out with remarkable effectiveness— | 
and the same composer’s third Overture to the Opera | 
“ Leonora

if she errs at all it is always on the right side. The pro- 
gramme of the present Recital was made up of pieces by 
no fewer than fourteen composers, the most important 
work being Schumann’s rarely-heard Sonata in F sharp 
minor (Op. 11). Pieces by Ratt, Zarzycki, Nicode, 
Gernsheim, and Dupont proved more or less interesting, 
and special mention should be made of a fanciful yet refined 
Nocturne in E flat, by E. Cutler (Op. 43), an English 
composer who has so far met with more success in Germany 
than in his native country

The same hall was again fairly well filled on the 21st 
ult., when Mr. Lamond gave a Recital. His programme 
contained fewer pieces, but alarger proportion of important 
works. The young Scottish pianist created an excellent 
impression in Beethoven’s Sonata in A flat (Op. 110), a 
work for which he seems to entertain a special affection ; 
Brahms’s Variations on a theme by Paganini (Op. 35); and 
Schumann’s Etudes Symphoniques. All these were ren- 
dered with praiseworthy breadth of style and good 
technique, Mr. Lamond’s reading suggesting the influence 
of Hans von Bilow. The touch at times seemed rather 
hard, but this may have been due to the instrument. 
Smaller pieces by Chopin, Liszt, and Raff completed the 
scheme

Mr. John H. O. Dykes did not trust entirely to his own 
powers at his Recital, on the 23rd ult., which also took 
place at the Princes’ Hall. He was aided in Beethoven's 
“Kreutzer ’’ Sonata by that admirable violinist, Mr. Willy

Hess, who also played some solos, and by Mrs. Helen

THE POPULAR CONCERTS. | 
Very little space is required to complete the record of 
the thirty-second season of these Concerts. At the last | 
Saturday performance, which took place on March 29, a} 
Beethoven programme was provided so far as regards the 
instrumental pieces. These were the Quintet in C (Op. | 
29), the Serenade Trio in D (Op. 8), the Violin Romance | 
in F (Op. 50), played by Dr. Joachim, and the ‘Moon- | 
light ’ Sonata, of which Miss Janotha was the executant. | 
As usual the Polish pianist took the opening Adagio at a| 
singularly rapid pace, thereby to our thinking depriving it | 
to a large extent of its impressiveness. The vocalist, Mr. | 
Norman Salmond, decidedly improved his position by his 
declamatory but perfectly legitimate rendering of “O 
ruddier than the cherry.”’ Healso introduced an extremely | 
charming if somewhat sad little song, ‘Good Night,” by | 
Battison Haynes, in which the composer has caught the 
spirit of the German verses by Betty Paoli. | 
The final Concert, on the following Monday, may be dis- | 
missed with almost equal brevity. Critical remarks are | 
certainly not required concerning Mozart’s Quintet in G| 
minor, Rubinstein’s Sonata in D, for pianoforte and violon- | 
cello (Op, 18), and Schumann’s Quintet in E flat (Op. 44). 
To these acknowledged masterpieces was added Spohr’s | 
Concerto in B minor, for two violins (Op. 88), the accom- 
paniment to which was played on the pianoforte by Miss | 
Agnes Zimmermann. That the work suffered in con- | 
sequence of this arrangement cannot be said, as its interest | 
lies mainly in the solo parts, which are splendidly written | 
for the violin, and, of course, were rendered to perfection by 
such executants as Madame Néruda and Dr. Joachim. It | 
only remains to be added that Miss Fanny Davies took the

Trust, who sang some songs composed by the Concert- 
giver. Mr. Dykes’s playing was, perhaps, more note- 
worthy for energy than refinement. He was at his best 
in Schumann’s trying Toccata in C (Op. 7). Mendels- 
sohn’s Prelude and Fugue in E minor and pieces by 
Chopin and Rubinstein were included in the programme

THE MUSICAL TIMES.—May 1, 18go

Pianoforte Quintet in A (Op. 81), which was given here for 
the first time. This most characteristic work of the gifted 
Bohemian was much enjoyed. Miss Florence Hemmings 
gave a very neat rendering of Marcello’s Violoncello 
Sonata in F, and Mr. Kruse played with passionate expres- 
sion Ernst’s “ Elégie,”’ and with astonishing bravura, a 
Polonaise in A, by Wieniawski. The programme also 
included Beethoven’s String Quartet in F (Op, 18, No. 1), 
which went with splendid aplomb

The Organ Recitals at the Town Hall have been given 
every Saturday afternoon since the opening ceremony 
already mentioned in your columns. On the 5th ult. Mr. 
C. W. Perkins played, and, a week later, Mr. J. Kendrick 
Pyne delighted a large audience by his vivacious and 
animated style of performance. On the roth Dr. A. L. 
Peace was the organist, and attracted many lovers of the 
instrument. This appearance of distinguished organ per- 
formers from all parts of the country is quite a new feature 
in our musical life, and one, perhaps, impracticable under 
the old arrangement

The other divisional choral societies, formed last October, 
|have just completed their work for the present season, 
| with good results in all cases. The prizes awarded by 
Mr. G. Riseley and Mr. E. T. Morgan to the most efficient 
members of the Bristol East Musical Society were dis- 
tributed on March 19

The Redland Orchestral Society, which has been work- 
ing in private for about five years, made its initial public 
essay on March 27, when an Overture of Hermann, and 
smaller works of Beethoven, Grieg, Sullivan, Moszkowski, 
and Prout, were performed with commendable skill, under 
the direction of Mr. E. Purcell Cockram. Songs and 
concerted vocal pieces were contributed by Miss Kate 
Nicholls, Mrs. Gridley, Mr. S. Evans, and Mr. Percy 
Baldwin

The Bristol Society of Instrumentalists, the largest body 
of amateur players in the kingdom, gave its second 
annual Concert, on the 17th ult., at Colston Hall. The 
chief works performed were Beethoven’s Symphony 
in D (No. 2), Weber's “ Peter Schmoll’? Overture, 
Mendelssohn’s ‘Son and Stranger’ Overture, and 
Mackenzie’s beautiful ‘‘ Benedictus,” all of which were 
played with surprising skill. The tone of the strings was 
remarkably pure, rich, and full, and the marking of light 
and shade was all that could be desired. Indeed, the per- 
formance was more like that of a band of professionals than 
of amateurs. Mrs. Nixon and Mr. A. Wetten were the

So

and, consequently, an opportunity for standing up was 
eagerly seized; but the bare new wood of the temporary 
platform, the interval to enable the choir to go into some 
vestry, presumably for refreshments, and the speech of a 
minister of the Church of Scotland, who made mild j¢ 
and invited a ‘‘ hearty vote of thanks,” were all in a piece 
with the opera glasses, general conversation in the audience, 
and the ‘‘ evening dress optional” of the programmes. A 
great opportunity was offered and lost. As to the perform- 
ance itself, it was so good that it should have been much 
better. The orchestra was sadly deficient in strings. 
no doubt a great pleasure to be able to hand a substa 
sum to the deserving charity on whose behalf the Cor acert 
was given, but Mr. Kirkhope and his Committee had even 
a higher duty—to Brahms and to art—and it cannot be 
said that four or five first violins to about thirty sopranos 
tended to fulfil that duty. The ar yea form of the 
choir was shown in the unaccompanied twelve-part Motet, 
‘** Agnus Dei,” by Richter, which received a most beautiful 
and s;mpathetic rendering. Gounod’s ‘* Ave verum ” was 
not softly enough sung for due effect. Twosolos, *“ If with 
all your hearts * and * Angels ever bright and fair,” varied 
the programme, and the orchestra was quite strong enough 
for these. The Requiem alone, in a dimmer, more religious 
light, with the organ and without the speech, would have 
made a far more ideal programme

At the second Concert of the Edinburgh Quartet, the 
most important number was the Quintet (Op. 81) by Dvorak 
classed among famous Hungarian composers by one of the 
educators of musical opinion in the local Mr. 
Francis Gibson played the exacting pianoforte part most 
admirably, the Scherzo being quite brilliantly rendered. 
Messrs. Daly, Dambmann, Laubach, and Carl Hamilton did 
every justice to the work, but failed to maintain their 
wonted style in Beethoven’s Op. 18, No. 2. Extracts 
from Quartets by Spohr, Raff, and Rubinstein completed 
Mr. Stronach’s

and

Weber’s ‘ Jubilee”? Cantata, the Choir of St. Vincent

Street U.P. Church produced Gounod’s ‘* Messe Solen- 
nelle,” 
by its performance of Bennett’s ‘‘ May Queen.” 
favourite Cantata Mr. E. Branscombe made 
appearance in Glasgow, and was accorded a flattering 
welcome. Other miscellaneous Concerts comprised a 
good performance of Stainer’s ‘‘Crucifixion,’’ by the 
Greenhead U.P. Church Choir, and highly successful 
appearances by the Bridgeton Choral Society in Mac- 
Cunn’s ‘*Cameronian’s Dream” and in Mendelssohn’s 
‘‘Hear my Prayer.’”’ The Society, which is ably con- 
ducted by Mr. George Taggart, is young in years, but it 
has often done promising work, and its vitality seems to be 
well assured. The life of another excellent organisation is 
also full of vigour. This is ‘*The Glasgow Glee and 
Catch Club.” It is small—designedly so—in point of 
numbers, but the voices are, it is no exaggeration to say, 
superb in quality, while the musical culture of the mem- 
bers is of the foremost order. This was amply shown on the 
evening of the roth ult., when the Club gave a Smoking 
Concert in presence of a large and exceedingly representa- 
tive audience. A ‘palpable hit’ was made in Mr. Joseph 
Bradley’s elegant and altogether clever Serenade, ‘‘ Good 
night, my love.’’ It was composed for the Society, sung 
with remarkable appreciation of its beauties, and received 
with uncommon marks of favour. Beethoven’s ‘* Hymn to

Others, again

and the St. Cecilia Choral Society gained credit | 
In this 
his first

pianoforte, with orchestra and chorus. This work was more 
within the means of the Society; the choral writing is often 
melodic and graceful, and every justice was done to it by the 
choir. On this occasion the composer’s adaptation of the 
orchestral part for a second pianoforte was used. It was 
well played by a lady friend of the Society, and in the real 
pianoforte part itself Mr. Philip Halstead once more 
achieved a brilliant success. The last Concert for the 
season of the Hillhead Musical Association took place on 
the 2nd ult., and was supported by Dr. Joachim and Miss 
Fanny Davies. The enterprise of the management has, it 
| is understood, been entirely successful, and if in days gone 
| by reflections were freely made on the apathy of Glasgow 
amateurs towards chamber music, such a charge cannot 
| now be maintained. The work already accomplished by 
| the Association is invaluable, and on the occasion under 
| brief notice a programme of sterling worth was forth- 
|coming. It included Schumann’s Fantasie for violin (Op, 
| 31), Brahms’s Third Sonata for violin and pianoforte, in D 
|minor, and Beethoven’s G major Sonata for these two 
jinstruments. Both Dr. Joachim and his accomplished 
| coadjutor were at their best the whole evening, and no 
'more need, therefore, be said concerning the interpretation 
'of the programme

On the 19th ult. Mr. Fred. Niecks, the accomplished 
| author of ‘The Life of Chopin,” gave his Lecture on “ The 
| Sharp, the Flat, and the Natural,’’ before the Glasgow 
| Society of Musicians. There was a good attendance, and 
| much interest was felt in Mr. Niecks’s admirable paper

During the past month there have been amateur operatic 
performances at Rock Ferry, directed by Mr. D. Dean; 
and at St. Helen’s, under the management of Miss H. Swift 
The Runcorn Musical Society gave the third and last 
Concert of the season with a miscellaneous programme on 
the 15th ult., under Mr. E. W. Humphreys

At Chester, Parry’s ‘‘ St. Cecilia Day”? and Beethoven’s 
Choral Fantasia, with Mr. Steudner Welsing at the piano- 
forte, formed the programme of a Concert given on the 21st 
ult. by Dr. J. C. Bridge’s Musical Society. At Waterloo, 
Bennett’s ‘“*May Queen” was given, under Mr. E. J. 
Morrison, on the 15th ult.; and farther afield, at Portmadoc, 
on the same date, Mr. J. Roberts conducted Mendelssohn's 
“Elijah.” The Stowell Brown Guild, of which the veteran, 
Mr. J. Sanders, is chorusmaster, gave Barnett’s ‘‘ Ancient 
Mariner,” on the 17th ult. On the 28th ult. Barnby’s 
“Rebekah”? was announced at Bootle, under Mr. A. 
E. Workman; and there have been numerous other 
performances of more or less interest. A notable innova- 
tion of the past month has been the employment of 
an orchestra at a dissenting place of worship—the congrega- 
tional church in Burlington Street—nothing of the sort 
having taking place previously in Liverpool, though such 
has been the case in Birkenhead

By permission of Mr. D’Oyley Carte, the Rock Ferry 
Amateur Opera Society, on the 22nd ult., invaded the 
eastern shores of the Mersey, and gave a remarkably good 
performance of the ‘‘ Mikado” at St. George’s Hall, in aid 
of one of the most deserving local charities. The whole 
opera was excellently performed, the principals and chorus 
being alike competent, and, in fact, the conventional amateur 
element was conspicuous only by its absence. Had there 
been other accompaniments than those of a pianoforte and 
Mustel organ-—both very well played, by the way-—the per- 
formance would have been among the best of any light 
opera that has taken place in this city

The Nottingham Philharmonic Choir has again received | 
an invitation from the Master of Balliol College, Oxford, to | 
give a Concert in the hall of the College during the | 
*“* Eights week

MUSIC IN LEIPZIG. | 
(FRoM A CorRESPONDENT.) 
THE tenth and last examination at the Conservatoire | 
took place on March 25. In summing up the resuit of | 
these Easter trials it is safe to affirm that Greater Britain | 
has been foremost in two, if not three branches in this | 
competitive exhibition of young musical talent. As piano- | 
forte players, Messrs. George Moon, of Plymouth, and | 
Ernest Hutcheson, of Melbourne (Australia), head the list. | 
The latter is a genuine child of the Conservatoire, as his | 
first teacher at home was Dr. Torrance, of Dublin, who, | 
about 1856, studied here with Moscheles and others of the 
original staff. As a child of five the young Hutcheson 
played minor pieces at various Concerts and at private 
annual Recitals. He entered at Easter, 1886, and had 
Zwintscher for his chief instructor. He has also received 
lessons from Reinecke. His execution of Beethoven’s Fifth 
Concerto at the eighth trial was admirable for a youth 
not yet nineteen. His touch is clear, vigorous, and free 
from mannerisms, and his conception accurate without 
coldness. It was the best pianoforte-playing we have 
heard this year at the Conservatoire and promises well for 
the future. Mr. Moon played the D minor Mendelssohn 
Concerto at the first of the official trials, and exhibited a | 
high degree of technical skill, no prominent subordinate 
phrase being slighted, slurred, or otherwise marred; his 
conception of the work showed a thorough appreciation 
of its many beauties. At the ninth trial he produced 
one of his own compositions, ‘‘ Variations for Pianoforte | 
on an original Theme,” a well-defined, graceful piece of 
work of the Mendelssohn style, very effectively performed | 
by him. As a violin player Miss May Brammer, of 
Grimsby, is, on the whole, the ripest fruit of this season’s 
growth. She is the daughter of the late Edwin Brammer, 
a leading musician in Grimsby, and she played repeatedly 
at Concerts given in her native town. In 1882 she was 
received into the Leipzig Conservatoire, when not quite ten 
years old, on the strength of her musical ability, and the 
next year played Mayseder’s Variations in E with full 
orchestra, winning great applause. Her ‘technical 
precision, good bowing, and really full singing tone” were | 
flatteringly noticed in the public prints. She has a con- 
siderable répertoire at her command, all of which she plays 
with great ease of manner and maturity of conception. A 
criticism of April 2, 1889, says: ‘* The star of the Concert 
was undoubtedly Miss Brammer, a violinist of great talent | 
and grandly developed technique. She played Spohr’s 
Eighth Concerto and the very difficult ‘ Faust’ phantasie 
with complete mastery of technical detail and absolute preci- | 
sion in the most difficult passages, . . . showing throughout | 
a musicianly earnestness doubly remarkable in so youthful 
a performer.” This is high praise, but it is well merited. | 
Miss Brammer will, at no far distant time, take high rank | 
among the young artists of whom Old England may well 
be proud. In the last trial Mr. Edward Levy, of Man- 
chester, conducted an original Overture by himself, which 
exhibited no mean talent. To close the list, mention should 
be made of the organist, Mr. Edwin Clemence, of Plymouth, 
who played the Toccata and Fugue in C major, by Bach. 
In swiftness and ease of manual and pedal technique he 
has no rival among the pupils this year; on his taste for 
registration, practical or theoretical, no judgment can be 
passed, the pupils not being allowed to draw the stops 
themselves. Mr. Clemence has great capacity, which he 
will doubtless develop further during the ensuing course of | 
study at the Conservatoire

formers are a chorus

c., has had a chance of appeasing their appetite. But the 
fact must be chronicled that, with exception of the evenings 
when the incomparable Patti created unbounded enthu- 
siasm before audiences which filled the spacious house to 
overflowing, the attendance of the public left much to be 
desired. Even Tamagno, the great tenor, sang several 
times before a (comparatively) small audience. One of the 
greatest artistic treats of this Italian opera season is the 
wonderful singing of Madame Albani as Desdemona inVerdi's 
“Otello,” and in other parts. It is much to be regretted 
that a great effort which was made to arrange for a 
performance of Gounod’s ** Redemption ” in this city, with 
Albani, Lloyd, and Ludwig in the solo parts, has failed for 
the reason that previous engagements prevented these three 
great artists meeting together here at a suitable time

Of instrumental performances we have had the usual 
number, prominent of which was a Concert by the 
Symphony Society. 
two Pianoforte Concertos—Beethoven in E flat and Liszt 
in E flat—both played by Hans von Biilow in a somewhat 
dry and colourless manner; and an Orchestral Ballad, 
“Des Singers Fluch,” from the pen of the same artist 
The three Pianoforte Recitals given by Bilow at the 
Broadway Theatre did not create quite the same sensa- 
tion as his last year’s Recitals did, but it was reserved 
for the great Chopin player, Wladimir de Pachmann, to 
create the sensation of the hour. His has been a most 
wonderful success, and Chickering Hall was crowded to 
suffocation at each of his Recitals, with an audience listening 
with rapt attention to the marvellous execution and 
wonderfully retined rendering of Chopin’s works. The 
three Recitals originally announced by Pachmann have 
taken place, but so great has been his success that 
additional Recitals have been arranged. To-night a grand 
Orchestral Concert will take place, when he will play 
Chopin’s Concerto in F minor, and Madame de Pachmann 
will perform Liszt’s E flat Concerto, and both will play 
together Saint-Saéns’s Scherzo for two pianofortes

BOSTON HANDEL AND HAYDN FESTIVAL. 
3oston, April 14

THE annual Concert of the Violin Classes connected with 
the Birkbeck Institute took place on the 18th ult. In the 
selections played by the members of the classes directed 
by Mr. Gatehouse and Mr. Thornton ample evidence was 
given of the careful training they had received at the hands 
of their teachers. The vocalists who assisted were Miss 
Margaret Hoare, Miss Mary Doughty, Mr. Arthur Thomp. 
son, and Mr. R. E. Miles. Mr. Alfred Izard played Greig’s 
Wedding March and accompanied with his accustomed 
ability, and Mr. Gatehouse gave a clever rendering of 
“Le Tremolo,” by De Bériot, for which, being encored, 
he substituted Popper’s ‘Gavotte.’ Special mention 
should be made of Miss Edith Doughty, who joined her 
teacher, Mr. Gatehouse, in a Symphonie Concertante by 
Dancla, and played with a tone and breadth of style 
exceptional in a young girl, her intonation being also 
remarkably pure. The recitations given by Mr. Charles 
Fry, who was obviously suffering from a severe cold, 
were very favourably received. The attendance was by no 
means so large as could have been desired

Tue arrangements for the North Staffordshire Musical 
Festival are now fairly complete. The locus in quo is 
Stoke-on-Trent, and the dates are October 1 and 2. On 
| the first morning Mozart's “‘ Requiem”? and the ‘ Golden 
| Legend” will be given; on the first evening, a new 
| Cantata, written expressly for the Festival by Dr. Heap (the 
| libretto by the late D. L. Ryan), called ‘* Fair Rosamond,” 
with a short miscellaneous Concert. The second morning 
will be devoted to orchestral works, and one of Beethoven's 
Symphonies will be given; in the evening the first and 
second parts of the ‘ Creation,’ Stanford’s ‘* Revenge,” 
jand Parry’s ‘“ Blest Pair of Syrens”’ will form the pro- 
gramme. The artists engaged are Madame Nordica, Miss 
Macintyre, Miss Damian, Mr. Edward Lloyd, Mr. Iver 
McKay, Mr. Foli, and Mr. Watkin Mills. The band will 
consist of seventy players, selected from Sir C. Hallé’s band 
}and from Birmingham, with Mr. Willy Hess as leader and 
solo violin; Mr. F. Mountford will be master of the 
|chorus (about 280 voices), and Dr. C. Swinnerton Heap 
will be the Conductor

On the 17th ult. the last Concert of the Finsbury Choral 
| Association for this season was given at Holloway Hall, 
;under the conductorship of Mr. C. J. Dale. The band 
| and chorus numbered 300, with Mr. Carrodus as leader. 
|The vocal soloists were Miss Kate Norman, Miss Lizzie

acquainted with them for the first time. 
selected are “‘ The Knotting Song,” ‘‘ Nymphs and Shep- 
herds,” from ‘* The Libertine.”” The well-known airs 
“J attempt from Love’s sickness,” ‘Full fathom five,” 
and ** Come unto these yellow sands ”’ are included in the set, 
as are also the recitative and air from *‘ Dido and /Eneas,” 
the harmony of the latter being constructed upon a ground 
bass ; “* I'll sail upon the dog-star,”’ ‘‘ They tell us that yon 
mighty powers above,” ‘‘On the brow of Richmond Hill,” 
« Fairest isle,” from ‘“‘ King Arthur’; ‘*‘ What shall I do,” 
from ‘** Dioclesian,”’ with a supplemental second verse ; and 
the Cantata from ‘ Don Quixote,” entitled ‘ From rosy 
bow'rs,’’ which, as the copy in the ** Orpheus Britannicus ” 
states, ‘* was the last song that Mr. Purcell sett, it being in 
his sickness.” The words of these songs are by Shakes- 
peare, Sedley, Howard, Dryden, Nahum Tate, Shadwell 
(Poet Laureate), and Tom D’Urfey—whose name Mr. 
Cummings gives without the apostrophe usually employed 
by the owner of the name. One or two of the songs are 
transposed from the original keys to fit them to modern 
needs, and the whole of the accompaniments are for the 
most part constructed upon Purcell’s figured basses, a few 
chords only here and there being found which are not 
accounted for by the figures. Where introductory 
symphonies are required they have been written in full 
conformity with the character of the song to which they 
are attached. The lovers of old English music in general, 
and of that of Purcell in particular, will give a glad welcome 
tothis ably arranged collection of the music of him who 
has ** gone to that blessed place where only his harmony 
can be exceeded

Songs. Composed by L. van Beethoven. 
version by the Rev. Dr. Troutbeck. Vol. III. 
[Novello, Ewer & Co

English

In this third collection of the Songs of Beethoven, which 
completes the series, there are several of the lesser known 
songs of the great master with a few of the better known. These 
all bring their own interest with them, and the admirers of 
3eethoven will note with special interest the four settings of 
the same poem, by Goethe, ** Sehnsucht”’ (** Longing ’’), and 
the two melodies to “ An die Geliebte,”’ each different and each 
agem. The well-known contralto aria, ‘‘ In questa tomba 
oscura,’’ the songs from ‘*Egmont,” and the noble scena for 
soprano voice, ‘* Ah, perfido!” arealso contained in the third 
series. The original words in German, with Dr. Trout- 
veck’s English version, offer a double means of acceptance

FOREIGN NOTES

BASINGSTOKE.—The second Concert of this season of the Basing- 
stoke Choral Society was given in the Drill Hall, on Tuesday, the 22nd 
ult., when Haydn’s Oratorio the Creation was performed. The chorus 
had been specially augmented and numbered over seventy members. 
The accompaniments were played by a professional string orchestra of 
fifteen, led by Mr. Charles Griffiths, the wind parts being given ona 
large harmonium by Mr. Frank Idle, whilst Mr. H. Shepherd 
accompanied the recitatives on a pianoforte alone. The solo vocalists 
were Miss Kate Norman, Mr. Reginald Groome, and Mr. B.H. Grove, 
the Conductor being Mr. H. E. Powell

Battey.—On Monday evening, the 14th ult., at St. Thomas's 
Church, an Organ Recital was given by Mr. W. Marsden, the Organist 
of the Church. Miss Lottie Atkinson and Mr. Harry W. Kemp also 
gave vocal selections. Mr. Marsden’s selection was ‘‘ Concertstiick,” 
W. Spark; Allegretto (B minor), Guilmant; (a) Romance (for violin), 
Beethoven; (b) Fugue in G major, Krebs; March Romaine, Gounod; 
Sonata (No. 5), Mendelssohn; (a) Andante in F, Morandi; (6) Cap- 
riccio, Lemaigre ; Allegretto Moderato, Gambini; Grand March in D 
major, Boyton Smith

BELVEDERE, Kent.—On the roth ult. the Belvedere and Abbey 
Wood Choral Society brought a successful winter season to a close, 
witha grand Concert given in the Public Hall, Belvedere. Part one 
of the programme was devoted to Van Bree’s Cantata St. Cecilia's 
Day, while the second part consisted of a miscellaneous selection. 
The performers engaged on this occasion were Miss Edith Clay, Miss 
Bertha Colnaghi, Mr. G. F. Nichols, and Mr. Charles Bonham; Mr. 
Sidney Horton (violin); Miss Thomas (accompanist). Mr. Arthur W. 
Castell, the able Organist of All Saints’, officiated as Conductor

COMPOSED BY

L. VAN BEETHOVEN

The English Version by The Rev. Dr

