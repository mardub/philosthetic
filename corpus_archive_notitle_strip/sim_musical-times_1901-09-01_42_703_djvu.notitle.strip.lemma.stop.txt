WEDNESDAY , 11.30 , symphony C minor ( Brahms ) ; Mass 
 D ( Cherubini ) ; Organ Concerto ( Handel ) ; motet double Choir 
 ( Lloyd ) , & c 

 THURSDAY , 11.30 , " Eroica ’’ Symphony ( Beethoven ) ; ' Job " 
 ( Sir Hubert Parry ) ; requiem ( Verdi 

 THURSDAY evening , 8 , " Emmaus " ( Brewer ) ; ' Sleepers , 
 wake " ( Bach ) ; " ' Hymn Praise 

 sketch Mr. Mutlow draw 
 Madame Malibran , reproduce 
 facsimile , endorsement 
 genuineness handwriting Henry 
 Phillips , Gloucester Festival 
 seventy - year ago . depict old 
 cathedral organist possessor con- 
 siderable avoirdupois — 
 word , man weight . res 

 note word 
 BEETHOVEN choral symphony 

 remarkable author word set 
 music seat comparison 
 composer . curiously exem- 
 plifie Wagner - know analysis 
 article Choral Symphony , 
 mention Schiller . 
 following remark Schiller poem 
 offer small contribution history 
 Beethoven symphonic masterpiece 

 Lied die Freude , portion 
 form choral section Beethoven ' . g , ' 
 write Friedrich Schiller year 
 1785 . - , live 
 house Gohlis , pretty village 
 Rosenthal , near Leipzig . photo- 
 graph house , tablet 
 properly fix 

 Schiller share modest dwelling - place 
 friend , Herr Goschen , grandfather 
 Viscount Goschen . verify letter 
 Viscount , date 8th ult . , 
 

 beautiful lyrical production . ' ' 
 bespeak mind impetuous glad . 
 ness , overflow warm earnest 
 emotion 

 Beethoven conceive idea set 
 music Ode Joy ' verse verse ' 
 early 1792 , enter 
 - year life . resident 
 Bonn , write Schiller sister Charlotte , 
 January 26 , 1793 , 

 enclose setting Feuerfarbe 
 like opinion . young man place 
 talent widely esteem , Elector 
 send Haydn Vienna . intend compose 
 Schiller Freude , verse verse . expect some- 
 thing perfect ; , far know , 
 grand sublime 

 trace music Ode 
 date remain , till thirty year 
 later Beethoven carry , 
 different manner , intention form 
 youth . story gradual evolution 
 complete achievement masterpiece 
 music admirably tell late 
 Sir George Grove ' Beethoven 
 Symphonies , ' need 
 repeat . specially interesting 
 extract , important bearing 
 subject choral portion sym- 
 phony , , , quote 

 Beethoven use half Schiller word , 
 employ order stand 
 poem ; arrangement selection appear 
 trouble . note - book cite abound 
 reference ' disjointed fragment ' ( abgerissene 
 Sdtze ) try arrange connect — 
 necessarily employ Schiller long Ode — 
 ' Abgerissene Sdtze wie * Fiirsten sind Bettler u.s.w . nicht 
 das Ganze . ' selection Beethoven 
 omit , chance intention , 
 passage strike english mind risqueés 
 Schiller Ode : 

 Dieses Glas dem guten Geist 

 ’ glass good Spirit 

 star high ! 
 omission furnish example taste 
 colossal power , exception , guide . 
 point puzzle greatly 
 connect vocal movement instrumental . 
 biographer , Schindler , interesting description 
 walk room endeavour 
 discover , length cry , ' 
 , . ' hold sketch - book , Schindler 
 perceive word , ' lasst uns das Lied des unsterblichen 
 Schiller singen'—let sing song immortal 
 Schiller — recitative bass , word 
 Ode follow immediately soprano solo . 
 alter soon write 
 — word recitative change 
 ' o friend , tone ; let sing 
 pleasanter , joy ! ' word Ode 
 solo voice — method ot 
 connection remain . strongly 
 hesitation corroborate Beethoven word 

 strange word refer line , ' Bettler werden Firsten- 
 Briider ' ' ( ' beggar shall royal brother ' ) , stand 
 Schiller poem . Schiller original title Ode 

 February 
 ' Procee 

 MUSICAL TIMES.—SEPTEMBER , 1901 . 59 ! 
 n 1822 — ! , time past ' Sir George Grove attention charac- 
 Je write easily . sit think , think , | teristic alteration Beethoven Schiller’stext . 
 and| jt occur word ' die Mode streng 

 o edition , p. 83 , ef 

 1 settle ; come paper , 
 k trouble immensely outset ; ) esetheilt ' ( Novello octav 
 ' Grove , ' excited 
 indulge usual love plain 

 * ' | ° 
 ’ right . | seq.).t ' Beethoven 

 efore Musical Association | subject , 
 change Schiller word streng 

 p 

 ee ee 
 SCHILLER write ' 
 Hoffmann , Leipzig . ) 
 ' fashion strictly , ' | word alteration 
 ' fashion rudely . ' surely , ' | . Beethoven , ' eloquently argue 
 continue , ' characteristic personal enthusiastic Beethoven - lover , ' Beet- 
 | hoven second - rate composer , 

 touch great composer 
 suffer remain ! ! the|have challenge change 4 word 
 octavo subsequent edition word streng | text ; Beethoven second - rate . 
 h . . 
 restore , @ + authority Politz , - know Ge 
 - _ _ _ _ _ _ — | old version line — 
 | ' der Mode Schwert getheilt 

 near LEIPZIG , WwW ode joy 

 curious concert Leipzig 
 long series absurd under- 
 taking straitened mean lead 
 . town public 
 appear en masse , record 
 artistic success ; till 
 Russia pecuniary result worth 
 mention . ' ( Grove . Dictionary Music 
 Musicians , vol . iv . , p. 362 

 Mr. Dannreuther state Wagner con- 
 ducte concert Vienna , Prague , St. Peters- 
 burg , Moscow , Pesth , Karlsruhe , Lowenberg , 
 Breslau , programme con- 
 siste Beethoven symphony , fragment 
 Nibelungen Ring , & c 

 , , Wagner concert 
 operation St. Petersburg , accom- 
 paniment facsimile , wish 
 specially attention . Wagner find 
 friend russian capital person 
 distinguished critic - composer , Alexander 
 Nikolaevitch Seroff ( Syeroff ) , 
 kind composer - conductor , , 
 accord Sir George Grove , subsequently 
 ' extreme enthusiastic partisan 
 Wagner . ' Glasenapp tell Wagner 
 find musical press St. Petersburg 
 Moscow untainted Jewcraft ( ¥ udenschaft ) , 
 newspaper public 

 receive ( Wagner ) favour . 
 orchestra St. Petersburg consist 13 
 player . concert programme 
 comprise Beethoven Eroica Symphony , 
 sailor ’ chorus , senta ballad , overture 
 ' fly Dutchman ' ; Introduction 
 ' Lohengrin , ' march , Wolfram Romance , 
 overture ' Tannhauser . ' second 
 concert Beethoven C minor Symphony 
 Wagner selection perform . pro . 
 gramme concert ( March 6 , 1863 ) 
 facsimile . actual document 
 inch long , lower half 
 duplication russian language 
 upper ( french ) portion . second 
 concert — ' Fragments des opéras : la Wal . 
 kyrie , et le jeune Siegfried ( ! ) appartenant ay 
 cycle des Nibelungen ' — specially interesting 

 appear concert Wagner | 
 shower laurel wreath , hand | 
 orchestral player | 
 deserve recipient . attract 7 
 attention Grand Duchess Helen , | 
 read poem ' Nibelungen Ring , ' § 
 ' Die Meistersinger , ' ' Tristan . ' news [ 
 success concert land f 
 Czar soon find way England . § 
 musical World June 27 , 1863 , © 
 journal Mr. J. W. Davison , contain | 
 information : — 4 

 Atheneum July 27 , notice 
 performance Mozart ' Don Giovanni ' 
 Royal Opera , Covent Garden , 

 greatness music ' Don Juan ' lie 
 simplicity , directness , marked power character- 
 isation . Beethoven admire Handel simple 
 mean achieve grand result , similar 
 reason ought admire Mozart . note 
 hand master feel ; little figure , chord , 
 choice bit colouring work wonder , 
 help contrast spontaneity freshness 
 music write 

 perfectly true 

 4 , Great Coram Street , 
 Russell Square , W.C. 
 March 13 , 1893 

 dear Sir,—You correct Mendelssohn 
 Violin Concerto ; dare date , | 
 hand . unfortunately Diary , 
 ought — believe , , 
 Concerto previously play 
 public audience London ; , think , England . 
 violinist near relative ( forget degreee ) 
 Rodolphe Kreutzer , eminent violinist Paris , 
 associate Beethoven Sonata , 
 Op . 47 . course orchestra , | 
 play accompaniment ( arrange score ) ona 

 Broadwood Grand Pianoforte . long ago , | 
 remember ( forget ) enthusiastic member 
 audience , sit row , mark 
 satisfaction thump floor heavy stick , 
 umbrella , rhythm , way 
 confound nervous player , , ( fortu- 
 nately ) 

 distinguished critic eminent amateur , 
 like Berlioz , enter long 
 sleep ; regard writer note 
 perfectly correct , 
 lifetime , greet appearance 
 ' late Mr 

 Mr. Arthur Chappell retire director- 
 ship Popular Concerts , 
 founder , having hold post distinction 
 upwards year . issue September , 
 1898 ( p. 595 ) , historical particular 
 concern inauguration famous concert , 
 trace origin Cattle 1858 , 
 endeavour connection 
 bullock Beethoven , pig Pops . refer 
 Mr. Chappell retirement , evening contem- 
 porary recently spake 

 MR . CHAPPELL ' turnover 

 Eliza Bingham , wife gentleman connect 
 Customs , widow Bellini , eminent composer . 
 Mrs. Bellini english lady , 
 unascertainable , husband 
 biographer mention composer 
 marriage 

 english biography Bellini ignore 
 fact visit country , apparently 
 time , 1833 . consider reputation 
 opera composer expect 
 lionize sojourn London , 
 case , newspaper 
 day profuse reference 
 . Morning Chronicle ( 
 Chronigle Beethoven ) April 29 , 1833 , 
 interesting paragraph appear . 
 

 Madame Pasta , Madame Malibran , Rubini , Paganini , 
 De Beriot fiddler , husband Malibran , Bellini , 
 composer , Hummel , Vaccaj , Mendelssohn , Henry 
 Herz , unrivalled pianist , Madame Schroeder Devrient , 
 Haitzinger , Dobler , Uetz , german actor form 
 portion audience King Theatre 
 Saturday . Pasta Malibran excite particular 
 attention 

 Cash aaa aad 

 send copy interesting extract , 
 Mr. Algernon Black ( Broadwood ) : — ' 
 entry Day Book Corri , 
 clerk Messrs. Broadwood relative 
 marriage composer Dussek ; Ledger 
 entry handwriting Joseph Ries , clerk 
 office , brother Ferdinand Ries , 
 friend pupil Beethoven . ' 
 Bellini London 
 month , long . exact location 
 lodging known time 

 GLOUCESTER festival novelty 

 study cultivation music educate physically 
 develop capacity organ hearing 
 song speech . practice draw 
 paint eye gain accuracy vision , hand 
 precision delicacy touch , practice 
 music ear gain accuracy hearing , larynx , 
 mouth , tongue , hand , & c. , precision delicacy 
 action . usefulness train eye perceive 
 hand render size , distance , form , tint colour , 
 insufficiently understand , 
 widely recognise train ear perceive 
 vocal organ render height interval 
 pitch , rate proportion time , quality tone . 
 case instrumental music , come 
 play finger , hand , arm , foot 

 intellectual advantage study cultivation 
 music obvious physical , 
 real . reflection quicky disclose 
 physical development intellectual 
 concomitant , hand hand development 
 organ hearing organ 
 mental faculty . short , shall 
 proper study cultivation music develop 
 power mental perception , power analysis 
 synthesis , tonal memory , form sense , 
 imagination . enjoyment piece music 
 imply mind ear . able enjoy 
 Beethoven symphony , learn 
 sing , play , compose , 
 attain high degree intellectual acuteness vigour , 
 attain power analysis synthesis 
 necessary comprehension melodic , rhythmic , 
 harmonic , contrapuntal , structural , colouristic combina- 
 tion . enjoyment complex composition 
 likewise imply analytical synthetical power , 
 proportionately degree . mark , 
 speak active , intelligent enjoyment music , 

 mere passive , sensuous enjoyment . 
 enjoyment . little intellectual profit 
 obtain 

 sense ought carefully nurture . uncon- 
 scious growth achieve good possible result . 
 pupil learn feel beauty dimly 
 clearly . example explanation 
 handin hand . pointing beauty 
 tone , appreciation production funda- 
 mental esthetic development , 
 continue beauty melody , rhythm , form , 
 harmony . order remove impediment smooth 
 steady progress necessary set 
 simple gradual advance 
 complex . teacher draw attention 
 pupil beauty constituent music , 
 especially form , easily perceive 
 understand tone , melody , rhythm , 
 harmony . unsystematic , thoughtless choice music 
 study incalculable harm learner ; retard 
 progress , worse , stunt growth . 
 thoughtfully graduate choice music secure 
 attainment good object esthetic culture — 
 refinement manner character , transmutation 
 artistic orderly harmonious moral 
 orderly harmonious 

 ethic education , , pupil leave 
 guidance teacher . ought 
 point ethic characteristic composition 
 study , pupil hand music 
 suitable age temperament . 
 convince offer sound advice 
 teacher . toa child music demand 
 grow - person intellect emotional experience ; 
 instance , Beethoven Chopin . avoid 
 vulgar , weak , unwholesome , _ vicious . 
 erotic composition , Liszt ' Mephisto ' Waltz , 
 Wagner opera — ' Venusberg ' 
 music love scene ' Tristan Isolde ' 
 baneful influence . music teacher long wide 
 experience , good observer , tell find 
 Chopin Jensen music quicken amativeness young 
 people . effeminate , languid music certainly relaxing 
 effect . Spohr ultra - sentimental 

 music wholly absent vaguely indicate | noble music , Chopin large extent 
 depicting external sound , movement , colour , | morbid refined music , deleterious 

 foreign note 

 BADEN ( near ViENNA).—A handsome monument , 
 work sculptor Herr Bock , unveil 
 Carl Milloecker , popular composer operetta . 
 Milloecker bequeath inconsiderable fortune , 
 manuscript work , town , 
 associate Beethoven 

 Bertin.—Richard Strauss , newly organise 
 orchestra , series concert , chiefly 
 devote modern music , come season . 
 series inaugurate performance , 
 chronological order , symphonic work Liszt ; 
 interesting undertaking , 
 precedent 

 VIENNA.—At annual concour Conservatorium , 
 young student , Herr Bruno Eisner , pupil Professor 
 Fischhof , obtain prize , include 
 grand pianoforte offer Herr Boesendorfer . 
 regard authority gifted young artist 
 form Institution quarter 
 century 

 Warsaw.—The facade handsome new building 
 Philharmonic Society ornament 
 statue Mozart Beethoven , artistically execute 
 sculptor Ladislas Mazur 

 OBITUARY 

 A. A. P.—We fear market comeposeis 
 opera ' number melody , unharmonised , 
 ' undoubtedly original con- 
 siderable value . ' composer opera way 
 prefer invent theiy melody , 
 endeavour , , 
 case , altogether successful attainment 
 originality . ave advance 
 study harmony , glad draw 
 store theme — 
 Opus 1 , opera 

 SLAYANDO.—Beethoven pianoforte sonata ' Les Adieux , 
 L ’ Absence , Le Retour ' \Op . 81a ) metronomise 
 Hans von Biilow ( excellent guide ) follow 

 1st movement , Adagio , quaver = 60 , Allegro , 
 minim = 120 ; 2nd movement , Andante espressivo , 
 quavey = = 72 ; 3rd movement , Vivacissimamente 

 content . rwee| SCHOOL MUSIC REVIEW 

 Malibran Mutlow ( special Portrait Illustrations ) .. 585 ] publish ist monrth . price 14d . annual 
 note word Beethoven Choral Symphony ( j//ustrate ) 590 SUBSCRIPTION , include postage , 2s , 
 Schubert Setting - Psalm ( Facsimile ) ... 592 school music review ror SEPTEMBER 
 Wagner concert - giver ( Facsimile ) ... ve o > 594 ONT SS ' 
 Handel borrowing . ByJ.S.Shedlock .. oe aie + » 596 
 : revise instruction singe day school — EnGtayp 
 Occasional Notes .. oe oe ee oe oe ee ee 600 D 
 aE Dees WALES . 
 Bellini England . ai oy - + + Tue Roya NATIONAL EIstepDFOD , MERTHYR . 
 Gloucester Festival Nov eltie .. s+ 605 school sight - singing reader.—staff notation . 
 New Festival Composer — Mr. W. H. ' B ell ( " Port rai ait ) oe 1000 : sr ackee Minnuciaop PonrrING ( continue ) . 
 Church Organ music . 4 ° + e oe ee 607 theory question . 
 Professor Niecks Ethical Aapecte Music ar oe + » 699 } review . 
 review ee oe os ce ate - . 612 ) answer theory question . 
 twentieth- Comey Vecsmasan Comeait wn 5 .. 614 | action song OPEN - air . 
 National Eisteddfod .. rey oe es ‘ oo ot% correspondenc e. 
 - fifth year Bayreuth .. — oe oe + . 619 
 Foreign Notes sn " b pe ee es 6 school " music . review . 
 oaeay ane ees bee eee rere ov , Tue SEPTEMBER number contain follow music ! = 
 correspondence pe oe oe « » 621 m es 
 brief summary Pannen au Colonial ve ve 622 . | o happy HEART child . " - chorus . S , p , 
 answersto Correspondents .. « ss « + oe 622 WappincTon 

 Junior ScHoot Music Course . 
 , extra supplement , contain ' So.prer - Boys , " 
 action Song . Myves B. Foster . ( . 532 . Novello School 

 edition , revise 

 BEETHOVEN 

 

 London : GeorGcE Newnes , Limited , Southampton Street , Strand . 
 publish 

 new edition finale 
 BEETHOVEN 
 CHORAL symphony 

 Tue english translation revise , partly - write 

