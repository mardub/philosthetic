encouragement 
 english music publisher 

 BOSWORTH EDITION . 
 Times : 
 ( Weekly edition ) 
 * * iti great satisfaction note successful 
 effort british firm meet beat Germans 
 ground hitherto largely . 
 Bosworth Portrait Classics series Piano 
 composition easily superior cheapness excel- 
 lence fo production foreign music publisher . 
 printing clear bold , editing 
 volume scholarly fashion . 
 binding stout art cover handsome 
 substantial , — ' honest english . ' 
 volume special bindings,—a feature 
 commend search 
 present musical friend . short , publica- 
 tion world good music wonderful value 
 money . offer wide range choice , 
 include Bach , Beethoven , Chopin , Grieg , Mozart , 
 Mendelssohn , Schubert , Liszt , Schumann , 
 Tschaikowsky , Rachmaninoff , Liadow , Arensky , " & c. 
 british Edition classic moderate price 
 great achievement . british Edition 
 classic quality price outstrip 
 edition great achievement . Zhe 
 Times ’ warm appreciation letter 
 private source surely confirm 

 BOSWORTH & CO , 
 8 , HEDDON STREET , REGENT STREET , LONDON , W.1 

 16 MUSICAL TIMES.—January 1 , 1919 

 apart concert , great deal educative 
 influence exercise sprinkle 
 musician rank . pianoforte rest- 
 hut use solely ragtime sentimental 
 ballad : Bach Beethoven , Schumann Chopin , 
 speak , deliver message 
 new ear . [ write , December 
 Music Student come hand , contain , 
 point , short article Mr. Frederick Dawson , 
 recital hospital Y.M.C.A. 
 hut war begin . Mr. Dawson experience 
 Army appreciation good music 
 regard typical 

 musician Army , 
 receive — gain . 
 hear admit view 
 art considerably widen Army 
 experience . come enjoy great deal 
 hitherto despise . example , 
 swing rhythm good music - hall tune 
 lose . appreciation vital 
 quality important section popular music . 
 likely influence young composer , 
 influence good . good 
 music find way vid Queen Hall 
 Coliseum great place popular entertain- 
 ment , return . piquancy 
 idea St. Martin Lane Piccadiliy Circus 
 Langham Place thing , 
 probability 

 need general study Clavier 

 Toccata consider idiom keyboard music . 
 matter general knowledge . 
 musician observe series piece 
 ' Fantazia foure Parts ' Orlando Gibbons 
 1583 - 1625 ) , ' golden Sonata ' Henry Purcell 
 1658 - 95 ) , french Suites Chromatic Fantasia 
 fugue Bach , ' sonata ' Domenico 
 Scarlatti ( 1683 - 1757 ) , diverse piece 
 Beethoven Romantic Schools , _ sufficiently 
 aware history general principle key- 
 board music note technical 
 situation Bach Clavier Toccata . 
 remove matter scope remark 

 need spend time ( 
 pass ) matter obscure confused 
 origin Toccata . stand lose 
 gain Sonatas , Ricercatas , 
 Fantasias , Capriccios , Canzonas , Concertos , Fugues , 
 , Toccatas fill 
 nonage instrumental music ; dry 
 insidious dust settle remain , dust 
 ( judge taste antiquary ) 
 power blunt artistic percipience student . 
 far understand appreciate Clavier 
 Toccatas Bach concern , let dust 
 lie practically undisturbed , find Bach 
 need way specimen antique 

 stay away ' Falstaff ' programme ; | word ' pageantry ' lead people 
 having draw usual homage , | anticipate revival pageantry type 

 easily induce 
 fairly decent music write Beethoven 

 league intention 

 GeorGE DIxon 
 ( Lieut.-Colonel 

 excellent item include miscellaneous 
 recital West End Wesleyan Church , Middlesbrough , 
 November 24 . Miss Frances Richardson play Beethoven 
 Violin Sonata , R. G. Thompson ' long , ' 
 Stanford Cavatina Scherzino , old english 
 piece — John Collet Largo Cantabile , Robert 
 Valentine Allegro Vivace ; Miss Edith Groat sing ' hear 
 ye , Israel , ' song Noel Johnson , Liddle , Ashford , 
 Sanderson , rest programme consist 
 anthem choir , organ solo Mr. Horace 
 Walker , play Parry Preludes ' martyrdom ' 
 ' Rockingham , ' Largo ' New World ' 
 symphony , MacDowell ' Maestoso . ' Sunday , 
 February 23 , choir festival place , ' Samson ' 
 sing 

 meeting Edinburgh Society Organists 
 St George U. F. Church November 23 , Mr. John 
 Pullein , St. Mary Cathedral , Glasgow , recital , 
 play follow programme 

 turn consideration training 
 eye ear , change . 
 Continent , , find National Opera 
 subsidise , country . 
 common saying eye train 
 ear ; thing , possible 
 ? ear leave care . ' know 
 like ' apparently consider sufficiently 
 safe guide cultivation public taste . 
 increasingly fashion terminate series 
 concert plebiscite programme ; 
 result usually disconcerting , 
 occur ' excellent way 

 concert programme apt suffer 
 diametrically opposite cause . firstly , frequent 
 repetition classic ; , secondly , 
 ' performance . ' curious notice , 
 musical season London , constant repetition 
 favourite ( suppose favourite ) work . instance , 
 eminent pianist play certain Concerto , course 
 week half - - dozen eminent pianist 
 . half - - dozen orchestral concert 
 different conductor course 
 week , likely Symphony 
 hear . year ago Symphony 
 certain Tchaikovsky sixth ; present 
 moment inevitable choice Beethoven fifth . 
 particular ' Prelude ' repeat ad nauseam , 
 exclusion Preludes composer . 
 thirty year ago violinist fail include 
 certain Cavatina programme 
 regard oddity . 

 prevalence - know composition course 
 consequence popularity , intend attract 
 audience . object ' performance , ' 
 present time enjoy considerable number , 
 different . encouragement talent — possible genius , 
 composer friend — natural desire 
 composer draw public attention 
 work , usually reason performance , 
 , alas ! follow second . un- 
 doubtedly , new composition arrive 
 hearing guarantee work genius , 
 scarcely ' performance , ' 
 , unhappily , case , conclusion 
 inevitable new piece , equally old 
 favourite , detract satisfactory effect 
 programme artistic 

 , object ninety - concert - giver 
 financial , experienced concert- 
 giver tell unknown composition seldom 
 attract ; little - know work cf past 
 attract ; safe course pursue frequent 
 repetition old favourite . melancholy 
 reflection favourite work good . 
 ( - ) ' moonlight ' fine Beethoven 
 Sonatas ; ' Elijah ' fine Mendelssohn 
 oratorio ; Rafts Cavatina beautiful solo 
 write violin ; ' lost chord ' 
 good Sullivan song ; ' Ilome , sweet home ' 
 good Bishop . follow , , 
 art music exercise valuable 
 educational influence , different 
 plan selection need adopt 

 xum 

 Miss Murray Lambert provide interesting programme 
 violin recital December 7 . include 
 little - know work — Concerto Jules Conus Dvordk 
 Sonata F minor . recitalist good form , 
 ably accompany Mr. Harold Samuel 

 Mr. Victor Benham fine form Beethoven 
 recital December 11 . set heavy task , 
 play Opp . 57 , 106 , 53 , 27 , . 2 . rouse 
 large audience enthusiasm , especially 
 * Hammerklavier 

 ROLIAN HALL 

 Philharmonic Society open - fifth season 
 miscellaneous concert October 25 . unusually large 
 audience evidence increase subscription list , 
 general approval public work 
 Society depressing influence great War 
 happily end . artist engage high 
 eminence , performance worthy 
 reputation . Mr. Leonard Borwick , Miss Phyllis 
 Archbald , M. Lucien Caveye . - 
 - know need mention . M. 
 Caveye excellent violoncellist French School . 
 choir , conduct Mr. Godfrey Brown , sing 
 power artistic finish Bach Motet ' afraid , ' 
 - song Charles Wood , Sir Edward Elgar , 
 R. Somerville 

 Society second concert , November 24 , 
 large ambitious scale . principal work 
 lament Sir Hubert Parry Ode ' song darkness 
 light , ' solo sing Miss Dorothy Silk . 
 Miss Tessie Thomas violinist , performance 
 Mendelssohn Concerto Saint - Saéns ' Havanaise ' 
 prove deserve high reputation . 
 work orchestra include Beethoven Symphony . 2 , 
 D ( Op . 36 ) , selection Glazounov Suite , 
 * Ruses d’Amour ' ( Op . 61 ) . provincial orchestra 
 attempt good performance work 
 creditable cnvelel training admirable 
 conducting Mr. Godfrey Brown , Society conductor 

 Winifred Burnett Quartet ( lady 
 accomplished leader associate Mr. 
 Fred Clarke , Miss K. McEndoo , Miss Carrodus Taylor ) 
 chamber concert November 14 . programme 
 include Beethoven Serenade Trio , Tchaikovsky Trio 
 ( Op . 80 ) , Mrs. Warnock pianoforte , Grieg 
 String Quartet ( Op . 27 ) . vocalist Mrs. Jasper 
 Grant , remarkably fine contralto hear 
 pleasure choice selection song 

 BOURNEMOUTH 

 orchestral playing recent Symphony Concerts 
 uniformly excellent , evident Mr. Dan 
 Godfrey great pain preparation 
 composition wend vy forward . 
 distinctly elaborate nature , speak volume 
 industry enthusiasm conductor instru- 
 mentalist complexity intricate 
 musical work slight hindrance truly 
 satisfying performance . 
 technicality exacting work over- 
 come ease 

 attractive composition appear 
 recent programme specially mention follow : 
 Scriabin second Symphony , Rhapsody , ' Shropshire 
 Lad , ' George Butterworth , Beethoven Symphonies 
 Nos . 2 4 , introduction Act 3 , ' Lohengrin , ' 
 ' mastersinger ' Overture , Tchaikovsky fifth Symphony 
 ' Romeo Juliet ' Overture , Brahms Symphony 
 D. particularly graphic treatment accord 
 ' Mastersingers ' Overture Tchaikovsky 
 Scriabin Symphonies , combine grandeur charm 
 - work realise telling 
 fashion . purely orchestral novelty 
 elegie brass instrument drum H. A. Keyser , 
 concert - overture , ' spring , ' Charles O'Brien , 
 poetical little Réverie ( Op . 24 ) Scriabin , Chabrier 
 highly - pictorial overture opera ' Gwendoline , ' 
 ' memoriam 1914 - 18 , ' Throsby Hutchison . 
 soloist , Mr. Leopold Davis , elect 

 appear work previously unheard concert , 
 i.é . , famous Pianoforte Concerto D minor Bach , 
 unaccountably long time wait ere 
 comin ; 
 play 

 Symphony Concert audience . Mr. Davis 
 neatness resource , 

 hardly sufficiently convey emotional 
 content music . delightful performance Miss 
 Marjorie Hayward popular B minor Violin Concerto 
 Saint - Saéns notable feature seventh concert 
 series . rarely hear particular 
 composition - effectively play , solo 
 orchestral standpoint . November 28 youthful 
 pianist , August Ardenois , son Municipal 
 Orchestra trumpeter , essay - know Concerto 
 E flat Liszt , display considerable promise , 
 performance somewhat unequal . 
 Mr. Victor Benham performance Beethoven - 
 ' Emperor ' Concerto , ninth concert , 
 interesting , pianist receive hearty reception . 
 finally , December 12 , record exceedingly 
 enjoyable performance Miss Adelina Leon Boéllmann 
 Symphonic Variations violoncello orchestra . 
 hear Miss Leon occasion , 
 advantage 

 BRISTOL 

 noticeable Brahms Quintet . solo player 
 quality success 

 Catterall ( Juartet concert November 23 
 Beethoven work player 
 fear comparison famous continental 
 american quartet visit . 
 visit London Spring ought 
 follow visit Paris Brussels . fortunate 
 possess instrument unusual quality 

 Brodsky Quartet concert , November 21 
 December 12 , notable new Pianoforte 
 Quintet , MS . , Sydney H. Nicholson . 
 man spend bulk time musical 
 atmosphere ecclesiastical establishment succeed 
 completely banish trace influence 
 composition . work healthy , vigorous . melodic 
 strength , movement exploit gavotte 
 rhythm piquant manner . Miss Lucy Pierce 
 ideal associate trio quartet work , 
 colleague play quintet evident enjoy- 
 ment . day later repeat Tuesday 
 noon - time concert 

 December 12 came Busoni Sonata . 2 , e major , 
 play 1902 Dr. Brodsky late 
 W. H. Dayas . recently perform 
 composer Kreisler , occasion Dr. Brodsky 
 Mr. R. J. Forbes executant . 
 thing enthuse - day . obscurity 
 1902 , inform spirit harmonize ideal 
 fashion mood present moment , 
 interpretation notice quality , easier recognize 
 describe , wo nt term spirituality . 
 rare community thought mood , alike 
 composer player , achieve interpretative 
 power . fine Schubert Quintet follow , | 
 want music night 

 Bowden Chamber Concerts enter 
 eleventh season . December 10 Messrs. Catterall , 
 Forbes , Hock play Tchaikovsky Trio(Op . 50 ) , 
 Beethoven b flat ( Op . 97 ) . singing Miss Phyllis 
 Archibald hardly high plane excellence 
 playing Trio 

 contribution devote 
 influence Royal Manchester College Music , 
 line devote Armistice Celebration 
 concert December 10 . Miss Brema arrange ' Prologue 
 Peace ' opening Rameau ' Castor 
 Pollux . ' follow Elgar ' Carillon , ' belgian 
 song Paul Kocks , interesting thing , altogether 
 programme beaten track , satisfy artistic 
 emotional sense high degree 
 Hallé Brand Lane concert . repeat 
 Free Trade Hall , theatre , Rameau 
 orchestration 

