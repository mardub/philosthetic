concert, Mendelssohn’s Overture to “Ruy Blas” i

Rossini’s to “ Semiramide”’ were the orchestral pi 
and Dr. von Biilow’s performance of Beethoven's P 
forte Concerto in G was an attractive feature in the 
tion. Well-worn vocal nrusic from the operas were 
tributed by Madlle. Titiens, Madame ‘Trebelli-Bet 
Signor Mongini, and Signor Agnesi, which elicited 
usual amount of enthusiastic applause from those 
adhere to the opinion that classical works are not

for warm weather. Mr. Manns, as usual, conducted

PHILHARMONIC SOCIETY

Tue principal feature at the third concert, on the 28th 
April, was the performance of Beethoven’s Concertoin E flat, 
by Dr. Hans von Biilow, an artist who comes to us witha 
German reputation which raised public expectation to the 
highest pitch of excitement. To say that in the rendering 
of this great work he fully realised our ideal is more than 
we Gould conscientiously do; but that he evinced the 
possession of true artistic feeling and of consummate powers 
of execution, especially in the Adagio and Rondo, is un- 
questionable. His reception was most enthusiastic; and, 
after his performance of Bach’s ‘Chromatic Fantasia and 
Fugue,” in the second part of the concert, the applause was 
so prolonged, that he returned to the platform and played 
the ‘ Passepied,” from the same composer’s ‘“ Suites 
Anglaises.” The orchestral pieces were Haydn’s charm- 
ingly melodious Symphony in G (Oxford), Sir Sterndale 
Bennett’s Overture, ‘‘The Naiades,” Mendelssohn’s “ Re- 
formation Symphony,” and Wagner’s Overture, ‘‘ Der Flie- 
gende Hollander,” all of which, under the steady conductor- 
ship of Mr. W. G. Cusins, were finely played. Madlle. Gelmina 
Valdi, has a fine contralto voice, but somewhat overtaxed 
her powers by selecting the trying Scena, “‘O Prétres de 
Baal” (from ‘Le Prophéte’’), the applause, however, 
proving that our opinion was not shared by many! The 
other vocalist was Madame Alvsleben, of whom it is only 
necessary to say that she fully sustained her reputation. 
At the concert on the 12th ult., the programme included a 
new Violin Concerto, by Mr. G. A. Macfarren, played by 
Herr Straus, and the Andante and Rondo from Molique’s 
Concerto for the flute, performed by Mr. Oliif Svendsen, an 
artist who has so fairly won his reputation in this country, 
that we cannot but wonder why we miss him this season 
from his accustomed place in the orchestra of Her Majesty’s 
Opera. Mr. Macfarren will, we are certain, thank us for 
doing no more than record our conviction of the excessive 
merit of his new Concerto, for a work which demands both 
time and thought from a composer is not to be dismissed 
with a few hasty words, either of praise or censure, by a 
listener. Our impression is that it shows a consummate 
knowledge of the capabilities of the instrument, and is 
scored throughout with a masterly hand. The first move- 
ment is the most elaborated, but we prefer the ‘‘ Larghetto,” 
which has an exquisitely melodious theme, the accompani- 
ments being so delicately written as materially to enhance 
its effect, and the final Rondo—a vivacious movement, the 
brilliant passages in which appearto grow up spontaneously, 
instead of being, as is too often the case, patched on for 
display. Herr Straus proved himself thoroughly master 
of the work, and when he led on the composer at the con- 
clusion of the performance, the applause was most en- 
thusiastic. Mr. Svendsen’s rendering of the two movements 
from Molique’s Concerto was faultless, both in executive 
power and expression. His tone is pure and full, and his 
playing so sympathetic as to command the earnest attention 
of his hearers, a fact amply demonstrated by the genuine 
expressions of approval with which he was greeted on his 
retiring from the platform; indeed, the general regret 
appeared to be that the entire work had not been given. 
The orchestral pieces were Mozart’s “Grand” Symphony 
in C major, Beethoven’s in C minor, and the Overtures to 
“ Anacreon” (Cherubini), and “Le Nozze di Figaro” 
(Mozart). Madlle. Justine Macvitz, and Madlle. Alwina 
Valeria were the vocalists, but the music chosen was not 
particularly interesting

108

WAGNER SOCIETY

Tue third and last concert of the present series was given 
at St. James’s Hall, on the 9th ult., with a success so de- 
cisive as to set at rest all doubt as to the fate of the com- 
poser’s music in this country. The Overture to “Der Flie- 
gende Hollander” was received with perfect enthusiasm, 
and the extracts from “Tannhiuser” and “ Lohengrin,” 
which were performed at the former concerts, were again 
applauded with a vigour which must have astonished those 
who believed that by the next generation only could 
Wagner’s works be accepted. Mr. Hidward Dannreuther, 
who conducted all these compositions, resigned his bdton 
to Dr. Hans von Biilow, to direct the Introduction to, and 
Finale to the third Act of, ‘‘ Tristan und Isolde,” which he 
did entirely from memory, and with a vigour which pro- 
duced a inagical effect upon the band. Dr. Biilow also per- 
formed the tifteen variations and fugue on the theme of the 
Finale to Beethoven’s ‘‘ Eroica” Symphony, with a power 
and executive skill which held the audience spell-bound ; 
and although we could not quite endorse the burst of ap- 
plause with which he was greeted, there can be no question 
that, disagree as we may with what we must call eccen- 
tricities in his playing, his mastery over the key-board, and 
his command over every gradation of tone, stamp him at 
once as an artist of the highest rank. The only vocalist at 
this concert was Madame Otto-Alvsleben, who sang Hlsa’s 
song from ‘“‘ Lohengrin” and Elizabeth’s Prayer from 
“ Tannhauser’”’ with the most earnest and truthful expres- 
sion. The performance was terminated by the stirring 
“ Huldigungsmarsch,”’ which was finely played. We are 
glad to find that the Society intends to give a series of 
concerts, commencing in November next, at which not only 
the works of Richard Wagner will be performed, but also 
those of the great classical masters, from Sebastian Bach to 
the present time

Miss AcNEs ZIMMERMANN assembled a highly-appreciative 
audience at the Hanover Square Rooms on the evening of 
the 29th April, the programme, as usual, appealing only to 
those of cultivated musical taste. With praiseworthy 
modesty, Miss Zimmermann introduced one piece only of 
her own composition, the “‘Suite’”’ for pianoforte, violin, 
and violoncello, of which we spoke in such favourable terms 
at her concert last year. This characteristic chain of little 
gems was beautifully played by the composer, Madame 
Norman-Neruda and Herr Daubert, and warmly applauded, 
especially the clever “ Canon a la 7itme ” and the final 
“Gigue.” An effective pianoforte arrangement, by Miss 
Zimmermann, of Handel’s Overture to ‘‘ Ariodante,”” Men- 
delssohn’s variations for pianoforte and violoncello, and 
Brahms’s Quartett in G minor, for pianoforte, Violin, viola, 
and violoncello, were the other pieces in which the concert- 
giver displayed her exceptional powers as a pianist, and we 
ueed scarcely say that in all these compositions she was 
equally successful. The vocalists were Miss Abbie Whinery 
aud Madame Anna Regan-Schimon, the last-named lady 
kindly supplying the place of Mr. Santley,whowas absent from 
indisposition. We were glad to find that Miss Zimmermann 
not only announced upon her programme that the doors of 
the concert-room would be closed during the performance 
of the last piece, but had the firmness to act upon this 
notice by refusing egress to the many persons who, as 
usual, remaining until the composition had commenced, 
were desirous of treating it as a voluntary to play them 
from the room. Our readers know how anxious we have 
always been to institute this reform, and we warmly con- 
gratulate Miss Zimmermann on the courage she has shown 
in carrying it out

Tue Southwark Choral Society gave an excellent concer, 
at the Emerson Street School-rooms, on Tuesday, the 
ult. The chief features of the evening were a small 
tion of the least-known of Handel’s Oratorios, “‘ Joseph,” 
areading, by Mr: W. F. Taunton. Solos were given 
Master Porter, Miss Dear, Miss L. Giblett, Miss Jenki 
Mrs. Underwood, and Mr. A. Bunker; and Mr. W. H 
on the harmonium, and Mr. Ralph Horner on the piano, 
their valuable aid, both in solo and accompaniment. M, 
J. Courtnay conducted with his usual ability

A very excellent concert was given at St. James’s 
on the 10th ult. by Mr. J. G. Calicott, an artist whose nam 
has been so long connected with Mr. Henry Leslie’s concer 
as to ensure not only a large audience, but the cordial w 
operation of some of the most eminent vocalists and instr 
mentalists. An admirable programme was provided, a 
principal singers being Miss HKdith Wynne, Miss Hi 
D’Alton, Madile. Justine Macvitz, Mr. Edward Lloyd, Mf 
Maybrick, and Signor Borella, the part-music being suy 
to perfection by Mr. Henry Leslie’s Choir. The selectiog 
included two melodious and well-written part-songs by tht 
concert-giver—‘‘ Love wakes and weeps,” and “‘ Hark, 
pretty lark’’—(the latter given for the first time), 
several favourite choral compositions by Sullivan, Benedict 
and Henry Leslie. The ‘‘ Andante” and ‘ Allegro” from 
Beethoven’s Quintett in E flat was finely rendered by Mess 
Lazarus, C. Harper, Hutchins, Horton, and J. G. Calleoti 
and much applauded ; and a feature in the programme Wil 
the violin performance of Mr. Henry Holmes. ‘The accom 
panists were Sir Julius Benedict, Mr. John C. Ward, Signa 
Pinsuti, and, Mr. J. G. Callcott

A BAND of real negro vocalists has appeared in the m 
tropolis during the past month, and given concerts, whid 
have been highly successful. The troop consists of {og 
males and seven females, most of whom are emanci 
slaves. Their singing enlists the sympathy of the audi 
by its extreme purity; and their simple eloquence maki 
even the somewhat uncouth sacred verses, to which thel 
music is allied, acceptable to hearers who, under othé 
circumstances, might pronounce such words profane. Theg 
“ Jubilee Singers,” as they term themselves, are — 
of Fisk University, and their object in visiting this count 
is to raise funds to build a“ Jubilee Hall” 1n conneetidl 
with the University. There can be little doubt that the 
will succeed in this laudable undertaking, for their concer 
have all been fully attended, and their popularity is 
creasing

THE MUSICAL TIMES.—Jvnz 1, 1873. 109

the music of Beethoven than in that of any othercomposer: | transcription, severely taxes the powers of any pianist. 
his rendering of the compositions of Bach, Chopin, and Liszt | Something more than conventional words of commendation 
vad = ee, can Re aaegie. » Parone apart — oe be eg <0. = — mr paw ay 
its y; and at his last concert we may als agrove, for in his hands it appeals to us with a po 
cages 5 ame interpretation of a “ — by —- | — we hardly — it to be Sa Te pas pli 
a e inuetto,” in which was a marvellous display | membere a is is an instrument for which ev 
of manual dexterity, combined with true poetical feeling. | Molique has written, and at this concert we had a work cast 
When we say that since his arrival in England he has never | in the truly classical form by Mr. Silas—a Quintett for iano- 
a note v0 nerva before him at ~ — a forte, — Ripe. —— erage ir hg yt 
we desire only to place upon record such a wonderful exhi- | composed with a part for the concertina under the full con- 
bition of memory, for we are not of those who believe that | viction that it has qualities which deserve a more ex- 
this is a matter with which intelligent listeners should have | tensive recognition. Perhaps the expressive powers of the 
any concern. His reception at his Recitals has been most | instrument were most successfully revealed in a MS. “ Duo 
icing qoced cae Ges io te Einglacd om setiense he ah| Gounedy *Mkieciic® tp Oohasy este ieureae alana 
who can appeal to his hearers with the exceptional eloquence | by the two concert-givers), which was warmly and most de- 
h ppeal to his h h th ptional el y g 
of Hans von Biilow. | servedly applauded. Messrs. Dando, J. Zerbini, and 
Tue fifteenth annual concert of the Great Northern | Daubert lent their valuable assistance in the instrumental 
Musical Society took place on the 2nd ult. in the large Meet- | department ; and the vocalists were Miss Matilda Scott

i ing’ i | Miss Abbie Whinery, Miss Atkins, and Miss Arabella West- 
Rosy ag. ecbiagige te ane palbordpe ae Cgewre EP peri brook ; Herr Oberthtir contributed a solo on the harp, and

Miss Jeannie Brown was encored in Benedict’s ‘Rock me| Tux 219th Festival of the Sons of the Clergy was held in 
to sleep,” and a similar compliment was awarded for| St. Paul’s Cathedral on the 14th ult., before a large congre- 
Gounod’s “ Serenade,” to Miss Kate Worth, who has only| gation. The choir consisted of about 250 voices, under the 
been recently heard in metropolitan circles. Mr. W. steady conductorship of Dr. Stainer, Mr. George Cooper 
Bowyer contributed De Beriot’s Seventh Air Varied, with| presiding at the organ. The service was rendered most 
good tone and style. impressively throughout, the “ Magnificat” and ‘‘ Nunc

Mrs. Joun Macrarren gave an evening concert at the |) Dimittis,” the composition of Dr. Stainer, especially pro- 
Hanover Square Rooms on the 9th ult., before a large | ducing a marked effect upon the listeners, a result attribu- 
audience. Her performance of Beethoven’s ‘ Moonlight” | table alike to the truly devotional and masterly setting of the 
Sonata, a Rondo by Dussek, Mozart’s Sonatain A for piano- | words and the admirable manner in which they were sung. 
forte and violin (with Mr. Carrodus), and Chopin’s Polonaise, | A complete orchestra (led by Mr. Weist Hill) played the 
for pianoforte and violoncello (with Herr Daubert), exhibited | two first movements of Mendelssohn’s “‘ Hymn of Praise,” 
her powers as a pianist of the highest order with much effect, | at the commencement of the service ; and the choral por- 
and she was most warmly applauded. The vocalists were | tion of the same work was given as an anthem, the tenor 
Madlle. Nita Gaetano, Miss Banks, Mr. Vernon Rigby, Signor | solos being well sung by Mr. Kerr Gedge. The Psalms 
Caravoglia, and Mr. Santley, the latter of whom sang, with | were sung to Sir John Goss’s chantin E. After the sermon 
decided success, a “ Hunting Song,” composed for him by | the “ Hallelujah” chorus was given by the choir, and the 
Signor Piatti. Mr. Walter Macfarren was a most efficient | concluding voluntaries, finely played by Mr. George Cooper, 
accompanist. included a beautiful ‘‘ Andante,” by Mr. Henry Smart

Mr. and Mrs. Ricuarp BLaGRove’s annual morning concert} Mr. E. H. Tuorne’s first evening concert took place on 
at the Beethoven Rooms on the 21st ult., drew a large and | the 30th April, at the Hanover Square Rooms, before a large 
fashionable audience. The principal features in the pro-| audience. The programme was excellently selected, not 
gramme were the pianoforte performance of Mrs. Blagrove| only for the display of the concert-giver’s powers as a 
and the concertina playing of Mr. Blagrove, which, it is! pianist of the highest class, but also for the exhibition of 
needless to say, were of the highest order of merit. Mrs.| his claims as a composer. His first Trio in G major, for 
Blagrove (who earned her reputation before the public as| pianoforte, violin, and violoncello (in which he was ably 
Miss Freeth) has a firm touch, facile execution, and a com- | assisted by Herr Louis Ries and Mr. Howell) was deservedly 
mand over the gradations of tone, which must always ensure | received with the warmest marks of favour, and three vocal 
the sympathy of her hearers; and in the compositions | pieces—the ‘“ Lake and Waterfall” (a part-song for the 
selected on this occasion these qualities were displayed to| choir), a Psalm, for ladies’ voices, and a song, “On a 
the utmost advantage. We were especially pleased with | faded Violet” (charmingly sung by Miss Enriquez and en- 
her rendering of Stephen Heller’s arrangement of “ Laj|cored)—are works of so melodious and refined a character 
Truite,” a little piece, which, although of course a mere|as to justify us in anticipating a successful career for a

110

THE MUSICAL TIMES.—Junz 1, 1873

writer who has but lately made his appeal to a London 
public. The Trio is remarkably well written for all the 
instruments, the first movement (preceded by a short 
Adagio) having much vivacity in the opening phrases, and 
containing a charmingly melodious second subject. The 
Adagio, which follows, is based upon a theme which speedily 
won its way to the sympathies of the audience; and the 
“Finale a la Polonaise,” is brisk and animated, the passages 
for the pianoforte especially, being extremely brilliant and 
effective. The Part-song has been composed with a thorough 
appreciation of the verses, which are taken from ‘All the 
Year Round.” The melody is appropriately simple, and the 
voice parts are written with much care throughout. A point 
which told admirably in the performance, was the unison 
passage in the tonic minor, which was boldly sung by the choir, 
and afforded an excellent contrast with the more subdued por- 
tions of the composition. There can be no doubt, that the 
** Lake and Waterfall,” will become a favourite with Choral 
Societies. Mr. Thorne’s performance of Beethoven’s ‘Sonata 
Appassionata,” and of the pianoforte part in Bach’s Sonata 
in A major (Herr Ries joining him with the violin) proved 
alike his thorough mastery over executive difficulties, and 
his true feeling for classical music. A choir of about 
fifty voices sang the choral music, with much effect. Mr. 
Thorne’s second evening concert is announced for the 6th 
instant, when he will perform his new Trio in C minor (with 
Messrs. Henry Holmes and Paque), and several pianoforte 
pieces of his own composition

Mr, Riwiey PRENTICE’s evening concert, on the 14th ult., 
at the Hanover Square Rooms, was well attended, and in 
every respect thoroughly successful. The concert-giver’s

erformance of Beethoven’s “Sonata Appassionata” was

ighly appreciated, and the applause which it elicited was 
as genuine as it was well deserved. The programme 
also included an “Allegro Assai” of Schubert, for two 
violins, viola, and violoncello, Mendelssohn’s Variations in 
D major, for pianoforte and violoncello (in which Mr. 
Prentice was ably seconded by Mr. Walter Pettit), Weber’s 
* Duo Concertante,” in E flat, for pianoforte and clarionet 
(by Messrs. Prentice and Lazarus), and a Quintett in 
G minor, by Mr. Prout, for pianoforte, two violins, viola, 
and violoncello, excellently played by Messrs. Prentice, 
Holmes, Folkes, Barnett, and Reed. Miss Katharine Poyntz, 
Madame Patey, and Mr. Cummings were the vocalists, all 
of whom were highly effective; a sacred song, by Mr. 
Prentice, “‘ Hear my prayer ’”’ (exquisitely sung by Madame 
Patey), receiving an enthusiastic encore. Mr. Minson ac- 
companied with much ability

A concise and practical explanation of the rules of simply 
Harmony and Thorough Bass, with chapters on Time, K 
Intervals, and general elementary musical knowledge. By 
Thomas Smith

TxoseE who are constant readers of cur reviewing col 
must by this time be convinced that if amateur pianists do 
not study the theory as well as the — of music, itis 
not for want of a sufficient supply of books on the subjec, 
In spite of this fact, however, here is one more, by th 
“organising choir-master to the Church Music Society fy 
the Archdeaconry of Sudbury,” the great fault of whichiy 
that, in the attempt to teach too much, it teaches nothing 
thoroughly. For instance, ‘The scale,” our author say, 
“is composed of thirteen semitones, comprised in @ 
octave ;” “all (major) scales are composed in precisely thy 
same manner—i.e., with a semitone between the third anj 
fourth, and seventh and eighth notes of the scale, all th 
rest being tones.” Nothing is said about the constructiq 
of aminor scale; but afterwards, in speaking of keys,» 
are told that every “major key has its relative minor;" 
and then, two minor scales being given, the pupil is lefty 
discover for himself how itis formed. The false notiong 
believing a composition to be in a major key until iti 
proved to be in a minor is then enforced in these words; 
“To find out whether a piece is in a major or minor 
look through one or two lines of the music, and if the fifth 
of the key is repeatedly sharpened from its position accort 
ing to the signature, it is not the major key represented y 
the signature, but its relative minor, the affected note bei 
the seventh of the minor scale.” Why, this is teaching thy 
student to do the very thing that he should be cautione 
against doing. Will Mr. Smith turn, for example, to th 
opening of Beethoven’s “ Sonata pathétique,” and say whe 
ther he believes that any pupil, even of average intellett, 
could ‘‘look through two or three lines of the music,” ant 
imagine the key-note to be anything but C; and, if so, isit 
not utterly absurd to talk about the “ fifth of the key’ 
being ‘‘ repeatedly sharpened?” Whilst young players am 
allowed to grow up thus instructed by those in whom the 
have a right to believe, we can expect but a limited amount 
of musical knowledge amongst our rising generation. I 
the chapter on “Time,” there is no explanation of the di 
ference between duple and triple rhythm; but “all the 
other times” are said to be “taken from common time” 
The only information as to compound time must be gathered 
from the following observation: “Simple times are thos 
with only one principal accent in a bar,” by which we may 
infer that compound times only have more than one: W 
this means we are at a loss to understand. 'The chapter om 
Intervals contains a complete table of those which ar 
chromatic, but diatonic intervals are left to be found out by 
the student, the only guide being that they are “ those com 
posed of notes incidental to the signature of the key.” 
Speaking of the first inversion of a common chord, 0 
author says that the figure 6 indicates the harmony to 
played, “which is the common chord of the sixth n 
above,” although he has previously explained that the chord 
is formed by taking the third for a bass instead of the fu» 
damental note. The chord of the diminished seventh, bh 
says, “is formed by raising the bass of the dominant seveutl 
a semitone ; the other notes incident to that chord remaining 
unaltered ;” but as no mention is made of the fact thal 
both root and key are completely changed by this process 
we question whether ‘a little knowledge” of this kind is 
not “a dangerous thing” for young pupils. We agree with 
Mr. Smith that the “ popular idea as to the use of Time 
music is quite an erroneous one,” because, as he truly - y 
“the figures which denote time at the commencement of& 
piece do not in any way indicate the speed at which it is 
be played.” Some other observations on the usual methods 
of teaching have also much weight ; but, as we have show), 
the matter contained in the book wants more careful cob

sideration than the author has bestowed upon it; and wef

the Church Modes,” both published by Novello, Ewer and Co

J. STEwARtT.—Thompson's Collection of Scottish Airs edited and arranged 
by Haydn and Beethoven (published in several volume), and Napier's 
Collection, in three parts, for Violin, Voice. and Bass, edited by Haydn 
are out of print, and can only be occasionally met with at ssecond-hand 
music shops

An AMATEUR.—The Organ is used in the performance of Oratorio, because 
in nine cases out of ten the composer has intended it. The organ 
part played on the occasion alluded to was written by the composer 
himself. The loudness of the organ may be explained by the position 
* An Amateur ” occupied in the hail

scarcely add that Mr. Best not only fully maintained the high 
reputation he has gained, but amply realised the most sanguine expecta- 
tions which had been formed, and displayed the power and varied 
capabilities of the organ to the best advantage. The evening pro- 
gramme was—Andante, H. Smart; Allegretto, A. Guilmant; March 
(with vox humana), W. T. Best; variations on the hymn, “ O sanctis- 
sima,” F, Lux ; Andante, Handel; Marche funebre, et chant seraphique, 
A. Guilmant. On each occasion the pieces were interspersed with 
verses of hymns by thechoir. Atthe conclusion collections were made 
= behalf of the organ fund, the total amount realised being about 
48

LINCOLN.—The re-opening of the Wesleyan Chapel organ, after 
extensive additions and improvements by Mr. T. H. Nicholson, organ 
builder, of Lincoln, took place on the 23rd ult., when two recitals were 
given by Mr. W. T. Best, the celebrated organist of Liverpool. The 
instrument is now one of the best, if not the best, in the county of Lin- 
coln. It p three plete claviers, and a separate pedal organ. 
There are thirtv-six stops (thirty-two of which are sounding stops) 
containing 1,635 pipes. The wind is supplied by two large horizontal 
bellows, placed in the basement and remote from the organ itself, 
which are blown by an hydraulic engine. The first recital took place 
in the afternoon, the company numbering about 700. The pieces se- 
lected thoroughly tested tne instrument, and showed that Mr. Nichol- 
son fully deserves the high reputation he has attained as an organ 
builder. Mr. Best’s manipulation was really wonderful, and so rich a 
musical treat has not been provided in this city for many years. The 
following was the programme of the afternoon recital: Overture to 
the oratorio, Samson (Handel); Andante with variations (J. Lem- 
mens); Prelude and Fugue (Bach); Allegretto, from the Symphony to 
the Hymn of Praise (Mendelssohn); March, from an Orchestral Suite 
(F. Lachner); Air with variations (W. T. Best); Chorus ‘ Hallelujah,” 
(Mount of Olives), Beethoven. The second recital took place at 
7 p.m. The gallery of the chapel was crowded to excess, and the 
lower part was fairly filled, the company numbering about 1,700. The 
programme was as follows: Organ Concerto (Handel); Variations on 
the Hymn, “O Sanctissima’’ (F. Lux); Organ Sonata (Mendelssohn) ; 
Chorus, by the Choir, “ Hallelujah” (Messiah), Handel; Toccata and 
Fugue (Bach); Air with variations (Haydn); air, ‘He layeth the 
beams of His chambers in the waters ” (Handel); chorus, by the choir, 
from the Creation ‘The Heavens are telling” (Haydn) The 
chapel choir was assisted by members of the Cathedral and other choirs 
in the city, and the choruses were rendered in a style rarely heard in 
Lincoln. The “ Hallelujah” was wonderfully effective, and was de- 
servedly applauded enthusiastically. Mr. C. Roberts conducted with 
his usual ability

LiveRPooL.—Miss Lillian Barrett gave her first annual concert on 
the Ist ult., assisted by Mrs. Skeaf, Mrs. G. Keef, Mr. Geo. Barton, 
Mr. G. Hardie, and Mr. W. Jarrett Roberts. Mr. Skeaf presided at 
the piano. There was a numerous audience, and the concert gave 
much satisfaction

ScarBorouGH.—On Tuesday evening the 29th April, an invitation 
concert was given by the members of the Amateur Vocal and Instru- 
mental Society, at tue Prince of Wales Hotel. ‘the Society was only 
established rather more than two years ago, and the office of conductor 
is undertaken by Dr. Sloman as a labour of love, from a wish to pro- 
mote a taste for good music amongst the inhabitants of Scarborough. 
Two entire works, besides many part-songs, have now been publicly 
performed by the members of this Society. The concert on the 29th 
April consisted of Mozart's 12th Mass, foilowed by a miscellaneous selec- 
tion consisting of part-songs, vocal solos, and an instrumental trio. 
The choir and instrumentalists showed by their performance of the 
Mass, and especially of the difficult fugue ‘Cum Sancto Spiritu” (which 
is frequently omitted by amateur societies), that they nad been most 
carefully trained

SeLkirk.—The Choral Union, which was formed in November last, 
and has been meeting for weekly practice during the winter, under the 
conductorship of Mr. F, K. Stroh, organist of the Episcopal Church, 
Selkirk, gave a concert in the Volunteer Hall on the evening of 
Friday, the 25th April. There were about 70 members of the Society 
on the platform, and the services of Miss H. C. Lindley and Mr. G. M. 
Dayidson, Edinburgh, had been secured for the soprano and tenor 
solos, Mr. C. Guild (Dalkeith), acting as accompanist. The concert 
consisted of two parts, sacred and secular. In the former, choruses 
and anthems were given from the works of Handel, Haydn, Beethoven, 
Mozart, &c.; and the second part comprised various glees and part- 
songs. Under their able conductor, the members of the Union rendered 
the pieces with a precision and just expression which evidenced skilful 
training on his part, and much careful practice on theirs; and the sacred 
solos artd songs of Miss Lindley and Mr. Davidson contributed much to 
the success of the entertainment

Sovutuport.—On Tuesday the 29th of April the Southport Choral 
Union gave its second concert in the Tewn Hall, with a band and 
chorus of upwards of 100 performers. The band was led by Mr. 
Lawson, anu Mr. H. Hudson presided at the harmonium. The prin- 
cipals engaged were Miss Clelland, Mr. Edmondson and Mr. A. Wroe. 
The first part of the programme isted of d's Messe Solennelle, 
selections from Mendelssohn constituting the second part. The man- 
ner in which both band and chorus acquitted themselves gave evidence 
of appreciative study and careful training, reflecting the utmost credit 
on their conductor Mr. Dobson, under whom this Society has made 
steady progress

ATURE’S LULLABY. Arranged for four voices

or solo. Subject from Beethoven. S8vo., 6d

ELCOME NEW MAY DAY. Arranged for three

A PROFESSOR OF MUSIC, residing in a large

provincial town, close to the sea, requires a PARTNER. The 
gentleman wishing to join him must be an excelient Pianist, a sound 
musician, and competent to teach singing, harmony, and composition. 
The practice is of twenty years’ standing, and is increasing year by 
year. Address ‘‘ Beethoven,” care of Messrs. Metzler and Co., 37, Great 
Marlborough-street, London, W

A LADY, good Vocalist and Musician, having some 
time disengaged during the day, wishes for further ENGAGE

126

EDITED BY 
ATALIA MACFARREN AND BERTHOLD TOURS agrly 
. petore, 13d. 
in the curren 
AUBER’S FRA DIAVOLO. J 
1. Comrades, fillyourglasses . . . ie DONIZETTI’S LUCREZIA BORGIA. 
En bons militaires. : , : , 49] Sh. Blot 8 ped. , ri . a ‘ 
2. Hail,festal morning. . a a 
Crest avant file , 4 ‘ : ; ; 2d.| 33. From his window . 
- bs panes éla finestra. 
Perens : 34. Would you know how to while away so: an 
; ‘ AU BER S MASANIELLO. II segreto per esser felice. instal “ "Mig A Cani 
3. All hail the bright auspicious day . ‘ é : , ; ad. BS Che 
. All hae — objet de notre amour. MOZART’S DON GIOVANNI. 17 Alittle 
. ail the bright auspicious day : ’s enjoy whi invi 
Du Prince objet = notre amour ad. 35 Tet “Giovtn ante ip - yiocttn ‘ ¢ * a of A Sele 
Pt a" tt z ‘ tovinette, che fate all’ amore. 9 mids 
+f ta EY a igh MOZART’S LE NOZZ jeu, 
E DI FIGARO. 101 Adieu, 
6. ee (Fishermen's chorus) ‘ ‘ ; 2d. | 36. —- with flowers . . #3 aa 
+. Bebe Bens iovani liete. ai 
: , 7 ic oie : ; ‘ : ‘ . , sant nl — ate aninent re . 7 ‘ ‘ ‘i ‘ 78 Awake 
8. age ons - who wish to buy. (Market chorus) ‘ ‘ 3d. | 38 PE ssn os «rong 382 Awake 
u marché qui vient d (an j ; i edt: Pa, Vibe o> 
9. We come, we will av bes vuamal 3d Amant, costes. a eae 
Courons & la vengeance. : ; . ; ; ; v1’ R SRE { 
o poenreash la ve¥e ROSSINI’S IL BARBIERE. Sate 
Saint bien heureux. 39. Sir, we humbly thank yourhonour. . . + + 6 Britons 
60. All hail, the noble victor. (Marchand chorus) . j ‘ 6d re Oh, we 
Honneur! honneur et gloire. ; VERDI'S IL TROVATORE 160 By Cel 
: ; P 2 ae 130 Calm i: 
BEETHOVEN'S FIDELIO. Ot ete Oe hoy 
10. Oh whatdelight. (Prisoners’chorus) . . . «. . 3d.|4% Now the dice invite our leisure! "ede beat 
O welche Lust. i Or co’dadi ase : + NS Chloe 
11. Farewell, thou warm and sunny beam. 1, | 42+ Miserere pay — 2 Chloe s 
Leb’ wohl, du warmes Sonnenlicht. j , , ; i ; : } : i ; me 
VERDI'S RIGOLETTO 164 C 
77? T c y . °. : om 
BELLINI’S I PURITANI. 43. Hush, in silence fulfil we our errand 4 33 Come, 
12. When yonder bugle callsus . : ; va Zitti, zitti, moviamo a vendetta Come, f 
Quando la tromba — ; "| 44 vanes lonely abode direoted. 1g #0. Come 1 
13. Rejoice we! ! ; ; ; ; - | Scorrendo uniti remota via 
esta. . ; ; 
14. Noble Arthur, welcome ,. ° r F . d WAGNER'S LOHENGRIN, * cote 
‘aid Avtereianors. * + + TG.) 45. The call hath summoned usbetimes . . «+ - Come | 
15. Once Isought thee . ; , . . : In Friih’n versammelt uns der Ru, : Ful 
; ; od f 1 fa 
A te, o cara. ° * | 46. We follow where he leads! . > ‘ _ ‘ Chor 
16.Fatalday . . . . 1 1 ee ve Zum Streite séumet nicht! * “P24 Come u 
pice pe _ % | 47. May every joy attend thee det ama Gently 
Gesegnet soll sie schreiten (A.7.1 
48. Faithful and true we lead ye forth 3. Down i 
, BELLINI’S NORMA. Treulich gefiihrt ziehet dain 3* Down i 
17. Hasten, ye Druids, the height cend ’ ‘f ; “ mvondy 
te sui colle, O Diuidt” sascend . . . «sd. WAGNER'S TANNHAUSER. Eventic 
zhi Naoaiaicometh f 3 : : d 49. Hail, bright abode (the March). ‘ P - . ‘ Fair Fi 
Norma viene. . . . nae Frew lig begriissen . Fair an 
19. Not yet gone? no, yettheylinger. . ra. | 3° Once more with j joy. (Pilgrim’s Chorus) 
ee Non parti? finoraé al campo . . . . Begliickt darf nun dich ed Ki 
. engeance, vengeance . . . e . sy i 
Guerra, guerra! 1d. WEBER'S OBERON. Five tir 
51. wie fairy foot can fall . . ; ; ‘ e oem 8 
B Nr ~ ieve il pié cola volgiam 8 
ae ELLINI’S LA SONNAMBULA. 52. Honour eater ee ie an Cee ye ose For the 
é Vit cy a, “Amina!” ! : , : : , : i + 53. Gl neg Caliph Gente 
va! viv ; 3. Glory to the Calip " i ‘ ‘ . é ‘ . =a pe 
= as Fare Gower ol memouniis © + +L wrnogoul ee es or ai 
vezia no : 54. Who would stay in her coral cave 4 gi ia — orlou 
23. bist — ieee ak aed Oe. : ee ~ potria fra l'onde restar God sar 
24. Here a moment we'll shelter and rest us ‘ od ora: oS noma’ CREO ET i j a ing hrr 
Qui la selva é pitt folta ed ombrosa. > 6. Do. do. (Mixed voices) Good m 
re 2 Py! ee Good ni 
DONIZETTY’S LA FIGLIA. WEBER'S DER FREYSCHUETZ. Good ni 
25. What pleasure, what gladness of i “piece, se 4 a eee wags 
Cantiamo, cantiamo i . , : , : a % Victoria, hey tay 
at niu taoas tha Aaa Fe la 58. The Bridal w reath for thee we bind Hail! a 
‘Sprona il tamburo ¢ oes lll : : : : 1d. i Wir winden dir den Jungfernkranz Hi of ¥ 
, 27, Rataplan, Rataplan 59. The joy of the Hunter. (Huntsman’s chorus) é F ‘ ail, bl 
Retaplan, Fetaplan. rune 6 . F . ; ; rd. Was gleicht wohl auf Erden a, 
i 
ROSSINI'S GUILLAUME TELL. Happy | 
DONIZETTI’S LUCIA. or. ee rosymom , . . So areal Hark! t 
28. Let us roam through th i yest yy enoheg 
peared oe omer equate i ° F a 1d. | 62. Come, with flowers crown the bowers . . . . off 
29. Hail, to the happy bridal day Hyménée, ta journée Hark! 
wh Per te d’immenso giubilo. ’ 5 : ; i cach hea mK: nol on raphy bonocd _ 
30. on login weddemine vet See uelle sauvage harmonie 
Chi rafirena it cng me . ah lea her ee. SE | 64. Hail to the mighty Pdler V4.4 BIN oatgh 
31. With warlike minstrelsy : | Gloire au pouvoir supréme Hear, hi 
D'immenso ginbslo. ; , : . : F 1d. | 65. Swift as the bird in summer sky (Tyrolean) ; ee Here in 
| Toi que Voiseau ne suivrait pas How ge 
(To be continued.) How sle 
LON

LONDON: NOVELLO, EWER & CO., 1, BERNERS STREET (W.), AND 35, POULTRY (E.C.) 
NEW YORK: J. L. PETERS, 599, BROADWAY

Sleep, while the soft evening breezes 
Sir H. R. Bishop

278 Snow-flakes - Arthur Cottam 
16 Soldiers, brave and —, be 
(8.S.A.T7.B.) Gastoldi 
341 Songtospring - Francesco Berger 
3 Soonaslcarelessstrayed - esta 
45 Spring’s delights (s.4.1.B.) Miiller 
145 Spring's delights (T.T.B.B.) Miiller 
216 Spring-time (t.1.B.B.) - Beethoven 
353 Summer and Winter - B. Tours 
95 Summer is a coming in (for 4 Trebles

7 Sweet enslaver (Round, for 3 voices) 
J. Atterbury

