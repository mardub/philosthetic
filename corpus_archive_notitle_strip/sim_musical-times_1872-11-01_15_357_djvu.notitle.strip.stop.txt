LOUIS DUPUIS 

 CATHEDRAL GEMS : 
 1 . NaNTES.—Fantasia , subjects Mozart 12th Mass 
 2 . Wurms.—Fantasia , subjects Weber Mass G 
 8 . RorrERDAM . — Fantasia , subjects Haydn 8rd 
 ( Imperial ) Mass ... 
 4 , Mataca.—Fantasia , subjects . ‘ Beethoven Mass 
 C 

 5 Rouns . — Fantasia , subjects Gounod Messe 
 Solennelle 

 ay 
 td gjed Wiss J 
 Se 
 — aa 

 , having Tunes possible similar Glees , 
 easy step farther , real glees . 
 find collection , adaptations 
 simple transcriptions ‘ “ * Amidst myrtles , ” « gave 
 heart t’other day , ” “ generous friendship , ” “ Ye 
 spotted snakes , ” named adapted words , 
 “ Warrior ! thy duty stand ; Faithful thy Saviour 
 . ” Having hegun use adaptations , melodies 
 sorts quickly found way collections — 
 operatic , national , instrumental solos , ete . 
 having composer names , Mozart , Beethoven . 
 etc . , found examination consist small 
 quantity great master , combined large 
 proportion editor . mania transcriptions 
 adaptations reached utmost height 
 good chants converted bad hymn - tunes . 
 following tune , called ‘ Mornington , ” tells tale 

 ae sat WSS 
 ( ete SS ere 
 yd ao oer ee 

 Preludes Short Pieces Organ . Composed 

 Francis Edward Gladstone . 
 Tue successor Mr. E. H. Thorne office 
 ist Chichester Cathedral , consider- 
 loss remarkably clever 
 musician ; regard work token 
 skill aplayerand acomposer . capacity 
 , justice music , smooth- 
 ness distinctness insure valuable executive 
 ities , fair believe , , 
 intimacy peculiarities stops 
 enables effectively combine contrast . 
 character producer , praised 
 carefulness invention . general verdict 
 ieces . 10 , Andante Lamentivole B 
 minor , . 11 , Andante ffettwoso F sharp minor ( 
 admire cessation harmony bars preceding 
 resumption opening phrase , giving extra charm 
 theme accompaniment har- 
 mony begins , device 
 uently employed series good effect ) , 
 . 13 , Andante G , rhythm phrasing 
 obvious fellows . piece 
 . 6 bars wrongly divided ; movements 
 begin instead crotchet , 
 secure closing chief phrases 
 bar , . greater , 
 inferior composers committed error , 
 rare instance inaccuracy master , 
 frequent appearance writer small esteem , can- 
 furnish authority evident impropriety ; 
 particular subject demands attention , liability 
 good musicians fall incorrectness 
 lament . present author 
 blamed companions 
 betters matter barring , enjoin look 
 atall Gavottes Bach ( distinguished 
 Bourrées master , beginning 
 ofthe bar ) , ‘ ‘ bee sucks , ” Arne , “ Batti , 
 batti , ” * duet Pamina Papageno 
 Zauberflite , Mozart , melody words “ Dite 
 voi ” great Scena , ‘ “ ‘ Ah perfido , ” princi- 
 = theme Choral Fantasia Beethoven , 
 dante Pianoforte Trio D minor Mendelssohn , 
 example quotation 
 space , find distinct illustration 
 ourmeaning . . 12 gives singular prominence 
 beautiful chord minor second key , F flat 
 key E fiat , moves hearer pleasure 
 occurs , seldom , 
 case , penultimate harmony piece ; 
 - song , ‘ “ ‘ Shortest longest , ” Walter Macfar- 
 , recently reviewed columns , excep- 
 tional use chord occurs , better effect , , 
 inversion , proceeds tonic 
 harmony progression augmented second , 
 takes beauty present instance . 
 work comprised books , contain 
 eighteen movements , exceed , 
 , page length . , , slow 
 tempo , modification Andante ; , 
 generally speaking , , organist extempo- 
 rise purport , glad play 

 opening Service 

 AsuMorE , Dorset.—An excellent concert took place parish 
 Friday , 27th September . programme consisted - songs 
 choir , vocal instrumental solos kind friends lent 
 services occasion . choir train deserve 
 praise , precision finished vocalisation 
 rarely heard village choir . choruses conducted Miss 
 Rossiter . Mr. Dodd Gounod song * * Nazareth , ” highly success- 
 ful ; Miss Fletcher displayed good , uncultivated , 
 contralto voice . Miss Rossiter performed brilliant morceau 
 concertina effect , greatly applauded . Rev. T. 
 Mrs. Davidson congratulated success enter- 
 tainment kindly provided parishioners 

 BEpForD.—The Amateur Musical Society gave concert 
 present season Assembly Rooms Tuesday evening , 
 15th ult . consisted excellent performance J. F. 
 Barnett Cantata , Zhe Ancient Mariner , applauded , 
 especially arias ‘ ‘ fair breeze blew ” ( sung Miss Sophie 
 Ferrari ) , ‘ 0 ! sleep , lovely thing ” ( given expres- 
 sion Miss Crofts ) . second , miscellaneous , 
 opened Beethoven Men Promethus Overture , 
 finely performed . Miss Ferrari enthusiastically encored ‘ * Pur 
 Dicesti ” ( Lotti ) , Mr. Folkes delighted audience fine 
 rendering Beethoven Romance G , violin . word 
 praise given members choir excellent 
 singing H. Smart song , “ Good night , thou glorious sun . ” Mr. Diemer 
 conducted - known ability 

 BERKELEY.—Special Thanksgiving Services late abundant 
 harvest , held Parish Church 26th September . 
 music early celebration Holy Communion setting 
 C , Rev. Sir F. A. G. Ouseley , Hymn 207 “ Hymns 
 Ancient Modern . ” Matins , responses Tallis ; 
 proper Psalms 65 81 sung Felton Savage , Venite 
 Hayes , Je Deum setting Mr. Moffatt , organist , 
 Benedictus . 49 Ouseley Monk Psalter . anthem 
 “ Lord Shepherd ” ( Macfarren ) , given 
 creditable manner . evening Psalms 103 147 given 
 Ouseley Battishill . Processional Hymn morning 
 * -Come , ye thankful ; ” evening ‘ Onward , Christian soldiers . ” 
 Mr. Moffatt , presided , played voluntaries Postlude , F ( Guil- 
 mant ) , March , B minor ( Schubert ) , Offertoire , G ( Kiihmatedt ) , 
 Allegro con fuoco , C minor ( Philip Tietz ) , . 6 Wely Grand 
 Offertoires 

 BromsGrove.—On Thursday , 3rd ult . , concert given 
 Corn Exchange , Mr. Rowland Taylor , aid funds b ; 
 new church Bromsgrove , sum realised £ 30 , 
 artists engaged Miss M. A. Woolley , ( soprano ) , Miss 
 ( mezzo soprano ) , Mr. Rae ( tenor ) , Mr. Randell ( bass ) . fo 
 lady gentlemen amateurs kindly gave services : — Miss 
 Taylor , Mr. G. W. Gibson , Mr. J. Mathews , Mr. R. Taylor , 
 J. B. Tirbutt officiating accompanist usual ability , 
 concert opened Sir S. Bennett quartett ‘ God Spirit , ” fp 
 Miss Wéolley voice told effect . solos 
 mentioned ‘ ‘ O fair dove ” ( Sullivan ) , Miss Bossward , 
 composer ‘ , ” Mr. Rae , Wallace “ Bey 
 Ringer , ” Mr , Randell . Bishop glee ‘ ‘ chough crow , ” wa 
 sung unanimously encored , Mr. Mathews finty 
 solos highly successful . Corn Exchange tastef : 
 Gecorated occasion , great praise ladies 
 superintendence work skilfully carried 

 CampBornE.—A miscellaneous concert given Assembly 
 Rooms 15th ult . , members Choral Society , 
 conduct Mr. J. H. Nunn , A.R.A. programme comprise 
 “ Spring , ’ Haydn Seasons . satisfactory perform . 
 ance given . Benedict * ‘ Hunting Song , ” feature ip 
 performance , owing spirited manner rendered 
 choir . Miss Mitchell “ dreamt heaven ” ( 
 Naaman ) ‘ ‘ Angry words ” ( song Mr. Nunn , viola obbligato ) , 
 highly successful , Miss A. Mitchell soprano 
 music , tenor bass solo parts effectively taken Messrs , 
 Treleaven Smith . programme varied following 
 instrumental performances : — Beethoven Serenade Trio , D ( Op . 8) , 
 Haydn ‘ * * Emperor ” Quartett ( Op . 77 ) , exeal- 
 lently interpreted , executants Messrs. J. H. Nunn ( lst violia ) , 
 Wright ( 2nd violin ) , H. A. Smith ( tenor ) , Marrack ( cello 

 Care Goop Horre.—The concert Capetown Ronde 
 bosch United Choral Societies took place 8th August , 
 Mutual Hall , Capetown , Governor , Lieutenant - Governor , 
 principal residents neighbourhood . 
 programme consisted Lahee Cantata , Building Ship , 
 admirably rendered choir 120 voices , received 
 utmost enthusiasm . second mainly composed 
 - songs , Leslie “ Pilgrims " liked 
 audience . able direction Mr. Bucke 

 Miss Glanville encored songs 

 Hastmxcs.—Master Harry Walker , highly promising pupil 
 Academy Music , gave morning evening concert ( per- 
 mission Sir Sterndale Bennett ) , spacious Pavilion new 
 Pier , 23rd September . orchestra 25 performers , 
 led Mr. Weist Hill , Royal Academy Music , following 
 Institution , lent valuable assistance : Miss 
 Ferrari , Miss Jessie Jones , Mr. Wadmore , vocalists , Mr. 
 Eaton Faning , conductor . Master Walker performance , 
 evening concerts , characterised firmness touch , 
 fiuéncy execution , command varied shades expres- 
 sion rarely attained youthful pianist , applause 
 greeted alike precocious talent 
 excellent teaching instructor Academy , Mr. F. B. 
 Jewson . solo pieces Sir Sterndale Bennett ‘ ‘ Caprice ” ( 
 orchestra ) , ‘ ‘ Valse de Concert , ” called ‘ * Alexandra , ” composition 
 master , Mr. Jewson , Mendelssohn ‘ ‘ Serenade , ” Weber 
 variations ‘ “ ‘ Vien qua Dorina Bella ; ” took 
 Mr , Weist Hill De Beriot Osborne duet Guillaume Tell , 
 Beethoven “ Kreutzer " ’ Sonata , amply justified 
 right medal bestowed examina- 
 tion Royal Academy Music . Miss Sophie Ferrari Miss 
 Jessie Jones warmly received vocal solos , Mr. 
 Wadmore elicited deserved applause rendering Mendelssohn 
 “ Tm roamer . ” Mr. Weist Hill highly successful violin 
 Fantasia , Adagio Spohr Ninth Concerto ; 
 orchestral works , favourable mention new 
 Overture Mr. F. B. Jewson , movement Mr. Eaton 
 Faning Symphony C minor . attendance excellent 
 concerts 

 Hengvorp.—The Michaelmas concert Hereford Choral Society 
 given Tuesday evening , Sth ult . , Shirehall , 
 fashionable appreciative audience . principal vocalists 
 Miss Dalmaine , Miss Lydia Broad , Revs . A. Robinson T. H. 
 Belcher , Mr. J. H. Lambert ; Mr. G. Townshend Smith officiating 
 conductor . Miss D’Almaine song , Haydna Canzonet , 
 “ Fidelity , ” entirely enlisted sympathies audience , 
 Scena “ Softly sighs , ” successful . Sir Henry Bishop 
 song , “ upbraid , ” obtained - deserved encore , 

 Mar.iBoroucH.—On Thursday , 3rd ult . , St. Peter Church , 
 choirs district , union Salisbury Diocesan Choral 
 Association , held Festival , result way 
 successful , service opened processional hymn 322 , 
 surpliced choirs clergy , carrying representative banners . 
 choirs having taken places chancel , prayers 
 intoned Rev. G. Stallard , Precentor Secretary 
 District , Rev. H. de St. Croix , Association Secretary , taking 
 second service . Festival blended Harvest 
 Thanksgiving St. Peter parish , , feast St. 
 Michael Angels , apparent Psalms Lessons 
 selected , Psalms ciii , Tonus Regius ; cxlvii , tone viii , 4 ; 
 cexlviii . tone v , 2 , taken “ Ferial Psalter . ” Magnificat 
 Nunc Dimittis sung plain service Boyton Smith , Preces 
 ( impressive portion Choral Service ) , Tallis 
 harmony . Anthem , ‘ ‘ O Lord manifold ” ( Barnby ) , went 
 great spirit effect . 252nd hymn ¢‘‘A. M. ” ) sung 
 sermon , delivered Rev. J. Parr ( Vicar St. Mary ) 
 text ‘ ‘ Speaking psalms hymns spiritual 
 songs , making merry hearts Lord . ” sermon 
 Te Deum sung Dr. Stainer music , collection 
 alms , Hymn 305 carefully rendered , recessions ] 
 hymn 318 . Lessons read Rector St. Peter 
 Rural Dean . service Mr. Walton , choirmaster , 
 ably presided organ , played * March ” Athalie 
 voluntary . church beautifully decorated occasion 

 MELBOURNE.—The fine Organ , built Messrs. Hill Sons , 
 London , city , opened Town Hall , 8th 
 August . Mendelssohn Organ Sonata , . 3 , , Bach Fugue , G 
 minor , pieces , afforded Mr. Lee excellent oppor- 
 tunity displaying admirable qualities instrument , 
 respect fully realised high expectations 
 formed . concert included selection vocal solos 
 Mrs. Cutter , Miss Christian , Signor Susini , 
 efficiently rendered , Mrs. Cutter receiving encore Beethoven 
 “ Invocation ” song , Miss Christian pure musical voice 
 telling good effect songs . hall tolerably 
 attended 

 Port Exizaneta , Care Goop Hopr.—On 22nd August , Mr. 
 Fred . Griffiths gave Pianoforte Recital Mechanics ’ Institute , 
 attracted large audience . programme comprised 
 pianoforte pieces ( including duet ) , songs Miss Esther 
 Innes Mr. Wroe , encored . Mr. Griffiths exhibited 
 skill taste pianoforte performances . selections 
 Mendelssohn Lieder ohne Worte warmly applauded ; 
 solo , ‘ * Com ’ ? gentil , ” left hand , duet “ Golden 
 Bells ” ( Mr. Griffith assisted Miss Innes ) , 
 appreciated enthusiastically - demanded 

 9th ult . Albert Hall filled , seating arrangements , 
 supervision Messrs. Moon , , usual , excellent . 
 orchestra occupied 160 vocalists large efficient 
 band , performers amateurs , including pro- 
 fessionals Mr. Henry Reed , resumed old place violin , 
 Mr. M. Rice , Torquay ( viola ) , Mr. Reed son ( violoncello ) , Mr. 
 Fly ( trumpet ) , Mr. Kearns ( oboe ) , Mr. Radcliffe ( flute ) . Mr. 
 Vernon Rigby voice grown mellow , 
 accustomed grace skill . .His chief song , ‘ ‘ thou faithful unto 
 death , ” charmingly sung , duet Mr. Brandon , ‘ ‘ 
 ‘ ambassadors , ” encored undeniably 
 repeated . Mr. Brandon hardly effective air , ‘ * Consume 
 , ” subsequent songs thoroughly successful . 
 Madame Demeric - Lablache little scope display fine 
 contralto voice , recitatives quartetts heard 
 advantage . conductor , Mr. Lihr , concert commenced , 
 apologised possible shortcomings Madame Cora de Willhorst , 
 mentioning suffering cold , Madame Will- 
 horst refrained attempting songs placed 
 programme . circumstances , performance 
 respect highly creditable ( choruses especially 
 rendered ) , Vocal Association congratulated 
 auspicious commencement season 

 Quarry Bank ParisH ( Dio . LicuFretp ) , — Annual Harvest 
 Thanksgiving Services , solemnised parish , commenced 
 8rd , concluded 13th ult . music 3rd Goss 
 Evening Service ; Anthem , ‘ ‘ thanks , ” ( Barnby ) 
 Beethoven ‘ ‘ Hallelujah Chords ; ” hymns selected ‘ “ Hymns 
 AandM. ” prayers efficiently intoned Rev. W. H. 
 Hopkins , B.A. , Oxon , curate . following Sunday , Morning 
 Service King , F ; Anthem , ‘ ‘ Lord loving ” ( Garrett ) . 
 evening Canticles sung Arnold , ; Anthems 
 “ Fear , O Land ” ( Goss ) “ Hallelujah Chorus ” ( Handel ) . 
 Sunday , 13th , King Morning Service repeated ; Anthem 
 “ magnify thee ’’ ( Goss ) . Festival concluded 
 evening , Goss beautiful Service E sung , Anthem 
 “ Glory Lord , ” composer . chorus , ‘ ‘ 
 Heavens telling , ” sung sermon . musical portion 
 services admirably rendered choir , 
 amateur gratuitous , reckoning members 
 best voices Midland Counties . voluntaries ac- 
 companiments skilfully performed Mr. Flivélle , choirmaster 
 organist , indefatigable zeal training treble voices 
 choir , highly commended 

 RamscaTe.—Sunday , 29th Sept. , Festival St. Michael 
 Angels , services St. Mary Church 
 usually impressive character , Hymn 325 “ Hark ! hark ! soul , ” 
 sung processional Service , anthems 
 “ Angels ” ( Novello ) inthe morning , ‘ ‘ Thee 
 Angels ” ‘ ‘ Thee Cherubim " ( Handel ) evening . con- 
 gregations unusually large . persons desirous 
 having beautiful Services Church rendered 
 deeply indebted Mr. Thorne , talented organist St. 
 Mary , giving opportunity hearing offices 
 Church performed devotional time attractive 
 manner , church Ramsgate choral 
 services given . following Sunday observed day 
 thanksgiving harvest . church tastefully decorated , 
 altar , altar rails , pulpit , lectern font , adorned 
 flowers , fruit , ferns , hops , & c. font especially deserves 
 mention , foot crosses prettily 
 arranged . gas standards decorated . Services , 
 unusually attractive , beautifully rendered . 
 processional morning evening Services 223rd 
 hymn , ‘ ‘ Come , ye thankful people , come ; ” 7e Deum ( Sullivan 
 D ) , Anthem , ‘ ‘ Praise Lord , O Jerusalem ” ( Hayes ) ; Kyrie ( Men- 
 delssohn ) ; Creed , Gloria excelsis , & c. , ( Marbeck ) . Evensong 
 Magnificat Nunc Dimittis sung Stainer E flat . 
 Anthem “ Fear , O Land ” ( Sir J. Goss ) . conclusion 
 service , previous benediction pronounced . “ Hallelujah 
 Chorus ” sung . offertories aid Ramsgate St. 
 Lawrence Disy ry , ted large sum £ 27 14s . 104d 

 g Mr. Thos . Cooper , highly effective . 
 Chesterfield Quartett Glee Society rendered glees 
 - songs artistic manner . Mr. Staton presided 
 pianoforte 

 Yaxter , Surrorx.—A new Organ , built Bevington Sons , 
 London , opened Mr. George Legge , organist choirmaster 
 § , Thomas ( Archbishop Tenison Chapel ) , Regent Street , London , 
 26th Sept. , presided services Harvest 
 Festival . organ built Mr. Legge direction , great 
 pains taken drilling village choir . voluntaries 
 Hallelujah Choruses Messiah Beethoven Mount 
 Olives , Adagio Spohr , ‘ ‘ Kyrie ” Haydn Second Mass 

 Organ ArpornTMENTS.—Mr . F. A. Hall , St. John Baptist , 
 Saints ’ Church , Belvedere , 8.E.——Mr . Arthur 8 . Roofe , 
 St. John Baptist , Hoxton.——Mr . J. Elliott Waugh , Organist 
 Choirmaster St. Aidan Church , Kirkdale , Liverpool , N.——Mr . 
 fdwin M. Lott ( St. Helier Parish Church , Jersey ) , Organist 
 Director Choir , St. Peter , Kensington Park , W.——Mr . T. 
 mn ( late Urganist Choirmaster Parish Church , New- 
 agncll ) , Organist Director Choir Christ Church , 
 galley R. Stokoe ( late St. Barnabas , Holloway ) , Organist 
 Choirmaster Christ Church , Fair 

 Price 2s . 6d . ; handsomely bound scarlet cloth , git 
 edges , 4s 

 READY . 
 BEETHOVEN FIDELIO 

 German English words 

