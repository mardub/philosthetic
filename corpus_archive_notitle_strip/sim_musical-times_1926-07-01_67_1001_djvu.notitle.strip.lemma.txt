it be owe to this illogical application of 
 half - truth that Scriabin ’s muddled reasoning run 
 to its own destruction . half - truth be always a 
 danger to ill - balance mind just because they be 
 half - truth . the well - balance mind see exactly 
 where the divide line between truth and falsity 
 lie . it be not difficult to distinguish the true and 
 the false in the statement that a man be bronze 
 by the sun , copper - colour by the moon , and 
 freckle by the star . nor be it difficult to 
 disentangle the true and false reasoning of an old 
 lady ( probably an old gentleman ) , who , see an 
 write advertisement in the sky , 
 exclaim : " look , there ’ one of they wireless 
 message catch fire . ' this muddle - headed 
 reasoning be very good fun in farce , but it be rather 
 dangerous when offer seriously . and it be 
 because Scriabin offer muddle - headed reasoning 
 seriously that the value of his later work be be 
 suspected 

 metaphorically , we speak of colour in music . 
 we say , perhaps , that D flat major be dark - brown , 
 but our reason for say so be that such music as 
 the march from * Rheingold ' and the slow move- 
 ment from Beethoven ’s sonata in F minor ( Op . 57 ) 
 have form in our mind a connection between 
 D flat major and darkness . if the same music 
 be play in C major it would sound just as 
 dark . similarly we think of G major as be 
 light green because various fresh and spontaneous 
 tune we know be in that key . they would 
 seem to be exactly the same colour if transpose 
 into A flat major or G flat major . it be primarily 
 a question of association . a few people aver that 
 they see colour when certain common chord be 
 play . ' thus they see red when B flat be play , 
 but if ' God save the king ' be play in 
 |b major would not a few people still see red 

 now Scriabin , accept a half - truth as a whole 
 ' truth , build his ' Prometheus ' on the assumption 
 |that music have definite colour , as though a 
 novelist , to prove he have a sense of architecture , 
 | should write his chapter in the form of Norman 
 arch , just as the mouse in ' Alice - in - Wonderland ' 
 /told its history in the pictorial appearance of a tail . 
 that Scriabin ’s reasoning be illogical be , in 
 | the question of colour , but of little moment , 
 | because it affect only one work , the * Prometheus , ' 
 |but it be of grave importance when he apply 
 himself to the question of harmony and its possible 
 expansion , because it reduce all his later work 

 recital . a few month before , Frau Schumann 
 give a musical party at her house , with the 

 follow programme : 
 Beethoven ’s Sonata , Op . iii Ilona Eibenschiitz 
 Song Julius Stockhausen 

 Schumann ’s Carnival Clara Schumann 

 musician of Vienna , and — Brahms 

 I have already play the Etudes Symphoniques 
 by Schumann , and a few small piece by Brahms 
 and Scarlatti , when Prof. Anton Door , the vice- 
 president of the Tonkiinstlerverein , come with a 
 letter to tell the audience that Herr Griitzmacher 
 have send word at the last minute that he be 
 unable to play . Brahms come up to I , and say : 
 " na , da spielen sie doch mal die op . 111 von 
 Beethoven . ' ( now , you get up and play the 
 Op . 111 of Beethoven 

 I do so , and I believe that henceforward Brahms 
 be my friend . at supper , after the concert , I 
 have to sit next to he . he wish to know all 
 my plan , and what Frau Schumann want I to 
 do . I tell he she have arrange with Mr. Arthur 
 Chappell to engage I for two Monday popular 
 Concerts in London , and that I be just go to 
 London for the first time 

 be he so happy . I have be at Ischl a few 
 day , when one afternoon Brahms pay I a visit . 
 it be a great moment for I , and I feel very 
 happy . I have to tell he all about London ; 
 everything interest he 

 on March 2 , 1891 , his G major string Quintet 
 be perform for the : first time by Joachim , 
 Piatti , Ries , Strauss , and Gibson , at a Monday 
 ' pop . ' at the same concert I play the b flat 
 Trio , Op . 97 , by Beethoven , with Joachim and 
 Piatti , and I therefore hear the two last rehearsal 
 of the Quintet . when I tell Brahms how care- 
 fully Joachim rehearse the Quintet , and what a 
 splendid reception it meet with , he be very pleased 

 a few day later Brahms come to dinner with 
 us ( Mittagessen , one o’clock ) , and he soon make 
 it his habit to come to we once nearly every week 
 to dinner . he like be with we because we 
 make no fuss whatever with he , and never ask 
 anybody to ' meet Brahms . ' even the Mittagessen 
 be very simple . in the beginning he aiways 
 come alone , but later he ask I if he could 
 bring Prof. Gustav Wendt , an old friend of his 
 from Karlsruhe , who come every summer to Ischl 
 for some time to be with Brahms . ' there come 
 also Prof. Késsler , from Budapest , whom he like 
 very much . he often bring other friend in 
 the afternoon , amongst they Fritz Steinbach with 
 his wife , Adolf Menzel , Stockhausen , & c 

 state of 

 material off their own bat , so to speak . 
 there be less adoption of folk - tune as 
 thematic basis than there be a few year ago , but 
 the ' original ' theme that have take their place 
 be often little more than tag and ¢/ché from the 
 folk - tune themselves , so that we be bad olf 
 rather than well . be this building up a national 
 school of composition ? if so , it be surely the first 
 that have be so build . it be true that Haydn and 
 Beethoven ( especially the former ) make use of 
 folk - tune occasionally in their instrumental music , 
 but can it be say that in their work as a whole 
 there be more than an occasional hint of the idiom 
 of folk - song ? where be the folk - idiom in Bach or 
 Wagner , save here and there for purpose of local 
 this be not to deny the searching beauty 
 " Banks of Green 
 rhapsody 

 writing as the 

 see 

 my father ’s relation to Beethoven be somewhat 
 complex . the composer — apart from his last period 
 produce a very strong impression on he . in 
 ' boyhood ' he describe the effect produce by the 
 ' pathetic ' Sonata on his hero ; in ' Family Happiness 
 he refer lovingly to the ' Moonlight ' Sonata ; 
 ' the Kreutzer Sonata ' borrow title 
 Beethoven 's work ; the ' Appassionata ' always excite 
 he ; and finally , he himself often play Beethoven ’s 
 sonata and orchestral four - handed ) . but 
 demand , in this instance as in other case , a 
 critical attitude towards accept idol , he always 
 protest against the exceptional cult of Beethoven , 
 consider his predecessor Haydn and Mozart to be 
 equal if not superior he . he consider 
 Beethoven not as the culminator of the period of 
 music ’s great development , but as the forefather 
 of a decline which ull I remembe : 
 his in that an artist of genius 
 give we in his work not merely a new content , but a 
 form ; Haydn create the form of the sonata 
 and the symphony , Mozart of the opera , but that 
 exist form 
 of Mozart 
 he ought to 

 seem I that 
 this judgment of 
 distinct 
 may be 

 say 
 new 

 create in 
 Haydn and 
 therefore , he ask himself whether 
 consider Beethoven a yvenius . it 
 my father mistaken in 
 Beethoven . his symphonic form be 
 from of his predecessor that they 
 consider as new form . Beethoven ’s construction 
 be broad and free ; it present something quite new 
 his scherzo ) , so that if it be true that genius 

 new form , this be fully applicable to 

 far , my father consider that 
 be comprehensible comparatively 
 small number of people , and that the listener have 
 somewhat to pamper himself in music and spoil his 
 taste in to understand Beethoven , as there be 
 much that be artificial in he 

 for Wagner , Liszt ( with the exception of some of 
 his arrangement ) , Berlioz , Brahms , Richard Strauss , 
 and other of the more modern composer , my 
 father have no great liking . * he accept only 
 a few of Grieg ’s piece . it be true that he be not 
 well - acquaint with the new composer , but there 

 Beethoven 
 those of 

 previously 
 to some extent 
 to 
 be 
 so 

 those 

 y * 
 create i m 
 Beethoven 

 Beethoven to a 

 ordet 

 certain musical be 
 understanding of Chopin . 
 ' probably because my taste be already spoilt 

 in addition to what have be say above about 
 Beethoven , I remember my father say of the 
 Sonata in G major ( Op . 14 , no . 2 ) , that in the first 

 j part one hear , as it be , the cenversation of 
 husband and wife , and in general that it be a toy 
 sonata . in the sonata in e flat major ( op . he 
 consider the 77ze from the Scherzo ( in e flat minor 

 Hadow 

 Seeley , Service & Co. , 5 . each vol . | 
 * Beethoven 's Op . 15 quartet by Sir W H 
 Hadow . 
 { Oxford University Press , | ! 
 \ ( omparison of Poetry and Musi the Henry 
 Sidgwick Lecture , 1925 by Sir W. H. Hadow 
 [ Cambridge University Press , 25 
 the two volume of study , be no n then 
 eleventh edition , be long past the review stage . one 

 can only say that they retain then piace a example 

 Henry would care to be pin down to every one of 

 his early opinion . again , the condition affect 
 the contact of the general publi ith music have 
 undergo such profound change , espe lally during 
 the past twenty year . that such a pa e as this 
 read curiously to - day 
 \ generation ago Englishmen who play the piano 
 forte be almost non - existent , and Englishwomen 
 nde their education with the * Batt of Prague . ' 
 even now the amateur level in thi ntry be not very 
 high , ind we have as vet little « e ot lam liartse 
 ourselves with Beethoven and Schubert , of bring 
 they to our fireside , and admit they to our friend 
 ship . this t , of course , the prin ipal re nm why good 
 Art be ever neglect . to appreciate the good musi 
 we must hear it often : to hear it often we must liv 
 with it : to live with it we must be in the mmpany of 

 those by whem it can be play and ng 

 Frederick Chopin Dvorak ; + Johannes 

 Brahms . 
 the little book on Beethoven ’s Op 18 Quartets be 

 be 

 who take a miniature score of 

 rule a good deal more about 
 Beethoven and his method ( not only in Op . 16 , but 
 elsewhere ) than can be obtain from many a fat 

 volume 

 be re - write and enlarge . the ' study ' be of 

 Bach , Cherubini , Beethoven , Meyerbeer , ¢ hopin , 
 Mendelssohn , Parry , and Reger ; the ' caprice ' play 
 round ' creed and custom , ' ' on go behind the 

 scene , ' ' the negative in Music , ' ' Slender Reputa 

 out . the 

 volume put 
 amateur ; for Mr. 
 sense of some of his musical 
 many will feel at liberty to fall 
 present writer , for example , though no 
 blind Beethoven - worshipper , can not agree with the 
 Beethoven , as Mr. Brent 
 Smith say , ' have his undeniably dull moment ' ; but 

 estimate of that compose 

 the fault be usually due to cause other than those set 
 forth \re his magnificent 
 no : surely one of the characteristic of Beethoven be 

 initial inspiration 

 can see the glint of a hasty safety - pin 

 this be true enough of the immature Beethoven 

 but surely not of Beethoven at his good . if we do 
 not find mastery of construction and texture in his 
 to look for it ? yet one 
 \mong his failing be set down ' trite 
 coda . " well , ' many 

 always think that the 
 Beethoven ’s g it be only fair to add that these 
 brief quotation be more or less set off by glow 

 fine music , where be we 
 more point 
 or interminable 

 ourse , as the great mus 

 of course ' be good ' on the same page 
 irgue that Brahms ’s chamber - music be an improve 
 ment on Beethoven 's , and on the last page we bid 

 he farewell , ' clothe in the mantle of Johann 

 1 collapse of cis 
 viacial aye 

 the heart of the book be a 
 beethoven ’s be the music of the future . 
 all other musi we be sull only on the 
 in the pessimistic world of 
 ruin of theological and 

 declaration that 
 Beethoven 's 
 contain 
 fringe of appreciate it 

 to - day , litter with the 

 sociological hope s , the star of Beethoven 's spirit 

 by W. J 

 shine on . all of a sudden our author grow 
 sympathetic . he have really feel all this , and he 
 express it all far well than I have do . this 
 | praise of Beethoven spring from a fine poetic idea , 
 and for its sake one 1 ine line to excuse a good many 
 of the author ’s hateful denigration . he end the 
 | chapter ( p. 88 

 and if anyone should say that the question , ' why 

 do I live ? ' have not be answer , I reply that 

 Beethoven have render it ridiculous 

 fifty year of Army Lieut.-Col 

 good of the Russians seem mere dabbler 

 Kreisler usually give gramophonist trifle , so it be 
 he in a Beethoven Gavotte and a 
 Bach Minuet . ' true , both be transcription , and so 
 in the they be splendidly 
 play , despite some rather heavy handling of the 
 little Bach piece ( DA777 

 Sharp be record tn Palmgren ’s ' Rococo 
 and the folk - tune ' the Gentle Maiden , ' both arrange 
 by himself 

 the MUSICAL TIMES — JuLy 1 : 1926 6351 

 Tito Schipa , who sing an arrangement of Liszt ’s Beethoven ’s eight variation on the theme , 
 yackneyed * Liebestraiime ' and a poor ' Ave Maria’|*Tandeln und Scherzen , ' be not often hear , and 
 ot own perpetration ( db873 there be no apparent reason why they should be — in 
 their complete form , at all event . Ethel Leginska ’s 
 voucalion playing 1 , like the work itself , unequal . her repeat 
 : chord accompaniment be far to heavy . on the other 
 \ ery appropriately , in view of the Weber centenary , : . " e oth 
 on * } | hand , her crisp playing of the delightful fughetta be 
 ome a capital record of the overture to ' Euryanthe , . : ; 
 : é first - rate . this variation be a reminder of the fact 
 play by the Lolian Orchestra , conduct by Stanley ; 
 4 ote ; , 4 ' | that although Beethoven write very few vood fugue , 
 Chapple . ' the recording be not of the ' new ' type , so : ' PF , 
 he be a capital hand when a neat and lively fughetta 

 here be no great power ; but the reproduction of tone 

 relapse into sobrie 

 that the Johannesburghers ' be move now to secure 
 an even more exciting time next veat witha Beethoven 
 Festival here be an example of organized energy 

 that many a city at home might copy 

 sonata no . 6 , J / endels Larghetto ( symphony 

 in D ) , Beethoven ; fugue in G minor , Bach ; Imperial 
 March , £ /gar 

 Mr. H. E. Knott , St. 
 Psalm - Prelude no . 2 , 
 * lovely , ' Vaughan William 
 Epilogue , Healey Willan 

 ws deny the ordinary source of musical education 

 through circumstance with which one should sympathise . 
 his study of what 
 we be pleased to call the classic , or master , be great , 
 ind his admiration of Beethoven and Bach truly profound 

 Mr. White conclude from Mr. Boughton ’s article that 
 Baines * be a young man of considerable natural talent , ' 
 ind opine that there be in England * several thousand 
 of natural talent equal that of 
 Boughton , in his article , do not use 

 she also seem to have the possibility of a good voice , 
 but her technique and style be full of fault . py , 7 , k , 
 some pianist of the month , 
 Moritz Rosenthal at ( ueen ’s Hall give a _ strictly 

 regulation programme . it be almost unnecessary to refer to 
 1is technical power as a pianist , but perhaps one of his 
 most striking characteristic be his capacity to preserve the 
 note in the most rapid passage . he 
 play Beethoven ’s Sonata in C Ill , and 
 Chopin ’s Sonata , Op 58 , with the complete understanding 
 his programme conclude brilliantly 

 clearness of every 

 Elgar ’s be 

 HARROGATI the summer symphony concert be now 
 in progress under the able direction of Mr. Basil Cameron . 
 Beethoven ’s first and Dvorak ’s ' new World ' be give 

 on May 20 and June 2 

 b minor to be repeat as 
 infl the 
 promise for last season will definitely be 

 from quarter . Beethoven 

 January 27 , and on March 10 Bantock ’s new ' song of 
 song ' will receive its first Lancashire hearing , with Miss 
 Caroline Hatchard and Messrs. Mullings and Allin as 
 solist the Messiah , ' on December 23 and 24 , com 
 plete what will probably prove the most memorable 

 c , odowsky be 

 Suggia , Cassado , Catterall , and William Primrose be 
 include in the string soloist . Formichi will sing at 
 the final concert in March , and one may hope that 
 programme publicity announcement will observe this 
 season a due proportion between the singer and the 
 symphony the cast for the Berlioz , Bach , Handel , 
 and Beethoven choral concert seem _ perfect model of 
 efficiency one Wagner concert only will be give : two 
 soloist appear here for the first time in Messrs. Roy 
 Henderson and Gaspar Cassado , and again two lead 
 member of the Orchestra take part as _ soloist — 
 Messrs. Alfred Barker and Clyde Twelvetrees . 
 Oxrorp.—Mrs . E. S. Coolidge , the famous american 
 patron of music , give a chamber concert - at the sheldonian 

 end of term , the 
 ' Canticle to the Sun 

 Island , " by Percy E. FLETCHER 

 forte , by BEETHOVEN . 2d 

 chool SONGS.—Published intwo form . a 

