Karg - Elert broke away tradition , best 
 examples usually shortest , hardly 
 said creation 
 extended development choral theme hinted 
 occasionally Bach 

 left César Franck . 
 ‘ Chorals ’ - known England 
 need little mention . 
 remarks , . , clear 
 construction ( especially . 1 ) shows 
 Franck disciple Beethoven Bach . 
 suggestive choral prelude 
 fantasia . source , look 
 large variation form later Beethoven . Second , 
 pity works announced 
 English recital programmes ‘ Choral Preludes . ’ 
 French title hardly adequate , 
 misnomer 

 Franck followers taken idea 
 developed ( distinct figured ) choral , 
 produced works broad , hymn - like theme 
 basis , relieved - contrasted matter free 
 agitated character,—a design - suited 
 organ , far ecclesiastical effect 
 average ‘ bright ’ postlude 

 auspices Edinburgh Society Organists 
 aconference Society Ministers Edinburgh 
 district held February 14 . meeting largely 
 attended representatives denominations , 
 frank exchange views took place undoubtedly 
 better understanding relationship 
 ought exist clergy organist . 
 Society fortunate having discussion opened 
 leading clergy city . Rev. Dr. A. Wallace 
 Williamson , St. Giles Cathedral , gave inspiring 
 lead short address ‘ Christian Year ’ ; Rev. 
 Dr. Drummond spoke ‘ Ministry Choir ’ ; 
 Dr. W. B. Ross criticised ‘ Presbyterian Service 
 frequently ’ ; Mr. Arthur Curle gave 
 ‘ Historical Sketch Presbyterian Church Music ’ ; Mr. 
 T. H. Collinson spoke ‘ Influence Clergy 
 Choir . ’ far learn occasion 
 meeting taken place city , 
 prospect similar meetings near 
 future 

 new organ St. Margaret , Durham , erected 
 amemorial parishioners died War , 
 dedicated January 31 Venerable Archdeacon 
 Durham . manuals - stops , 
 built Messrs. Harrison & Harrison . oak carving 
 case work gift Mr. A. V. Yockney , 
 headmaster St. Margaret Day School . opening 
 recital given Mr. William Ellis , sub - organist 
 Cathedral , played Bach E flat Fugue , Parry Prelude 
 ‘ Martyrdom , ’ Finale Reubke Sonata , 
 Beethoven Romance G , Chopin Funeral March , 
 Guilmant Grand Chceur D. chief items 
 subsequent series recitals - known players 
 found organ recital column 

 solemn celebration Holy Communion took place 
 St. Martin - - - Fields February 16 , memory 
 members S.P.G. fallen War . 
 musical , occasion interest showing 
 great possibilities simple music . service 
 congregational character , setting Merbecke . 
 body eighty mixed voices organized . 
 sat nave , large congregation , led , 
 sang impressive effect . service Tallis Funeral 
 Music played . Mr. Martin Shaw ( responsible 
 musical arrangements ) organ 

 Bach ‘ Funeral Ode ’ received undoubtedly 
 performance Australia December 12 , St. John 
 Church , Toorak . soloists Miss Anne Williams , 
 Miss Mary Thirlwall , Mr. Percy Blundell , Mr. Wilton 
 leech . Mr. Arthur E. H. Nickson conducted . 
 choir congratulated enterprise 

 Mr. Bernard Page good work Wellington , 
 N.S.W. , gives Saturday evening organ recitals 
 weekly February December . list pieces 
 played past year note Bach best 
 works , Franck , representative selections 
 Saint - Siens , Karg - Elert , Handel , Lemare ( 16 ) , Wolsten- 
 holme ( 11 ) . Orchestral performances rare , arrange- 
 ments rightly important feature , 
 Wagner ( 15 ) , MacDowell ( 9 ) , Debussy ( 8) , ard Beethoven ( 6 ) 
 head list . glad Mr. Page finds room 
 pieces Byrd , Felton , Greene , Keeble , Russell , Wesley , 
 old worthies 

 ORGAN RECITALS 

 Accompany Pianoforte . Edwin Evans , sen . 
 172 music examples . ( William Reeves , Charing 
 Cross Road . ) handy volume , pp . 231 . price 
 named 

 Beethoven Sonatas . Vol . I. ( Nos . II ) . Revised 
 M. Moszkowski . ( Société Francaise d’édition des Grands 
 Classiques Musicaux , Paris , Enoch & Sons , London . ) 
 Price 4s . 6d . - printed folio volume . fingered , 
 notes metronome rates distinguished 
 Editor 

 Correspondence 

 s town scoured early morning 
 breakfast - time deed 

 written ( //ereford Times , February 17 ) 
 friendship meant possessed , 
 reasons probably prevented developing 
 great gifts composition . 
 enjoyed musical career conducting . loved 
 having large forces , command , strong , clear 
 beat inspiration sang . 
 fine organizer , year work preparation 
 Choirs Festival , note 
 London rehearsals note Festival 
 work conductor sheer joy . great 
 teacher fine interpreter greatest works . 
 finest achievements conductor 
 readings B minor Mass Beethoven Mass D 

 Truly loss English Church music , 
 Choirs Festivals , colleagues , heavy 

 keen interest music , little difficulty 
 getting concert promoters push British music , 
 ultimate resort nearly large concert schemes governed 
 power purse . Baldly stated , concert - gives 
 perform works public payto hear . present , 
 small section cares British music pay 
 , thousands thousands people subscribeto 

 hear Bach , Beethoven , Brahms , Handel 

 moment suggest great composes 
 nation excluded programmes ; 
 horrible calamity lose . @ 
 contend earnestness heart capable , 
 English music given equal share foreigt 
 works land home . j 

 , based - - spirit ¢ Lo 

 Patriotism ’ succeeded banning Wagner 
 Beethoven ancient walls imperial tomb . 
 occasion second concert directed illy 

 trious French conductor , Réné - Baton , Sunday , January2,§ gopra 

 Symphony , ‘ New World ’ Dvorik 
 adequately performed . Miss Scharrer 
 good form , Symphony played ease 
 freedom come familiarity technical 
 demands 

 Berlioz 
 Wagne ) 
 Beethoven 

 QUEEN HALL ORCHESTRA 

 January 27 , Beethoven Rondino E flat wind 
 instruments played singular beauty tone 
 perfect ensemble . Suite , ‘ Legend Tsar Saltar , ’ 
 Rimsky - Korsakov , performed time 
 country . consists Entr’actes opera 
 . movements charming , 
 effective . Suite easily 
 intelligible , scoring fine craftsmanship 
 instinctive gifted composer . audience 
 quick realise beauty music , feel 
 sure welcome early repetition . ‘ New World ’ 
 Symphony , performed great attention 
 finish , successful item . interest 
 felt - appearance Miss Kathleen Parlow 
 Brahms Violin Concerto . played fine 
 feeling , generally certainty technique 

 concert given February 10 , novelty 
 Overture drama ‘ Tsar Boris , ’ Kalinnikov 
 ( 1866 - 1900 ) . turned interesting 
 orthodox composition , periods arrested 
 special attention , effect strikingly 
 impressive . Symphony G composer 
 better exemplification undoubted talent . 
 items programme prelude ‘ Le Déluge , ’ 
 Saint - Saéns , Brahms second Symphony — mag- 
 nificently played — Schumann minor Pianoforte Concerto , 
 solo adequately performed Miss 
 Irene Scharrer , Prelude ‘ L’Aprés - midi d’un Faune , ’ 
 Hamish MacCunn Overture ‘ Land Mountain 
 Flood . ’ Sir Henry Wood conducted occasions 

 Madame Alys Bateman organized excellent concert 
 January 27 , given 500 convalescent soldiers 
 sailors . programme - designed 
 purpose , enthusiastically received 

 London String Quartet , February 2 , played 
 new Debussy Sonata F , written harp , flute , viola . 
 elicited opinions . Quartets played 
 Brahms minor Dvordk F. February 16 
 Ravel Quartet , ‘ Rasoumovsky ’ ( Quartet 
 Beethoven , programme , Vaughan Williams 
 beautiful song - cycle , vocal sung Mr. 
 Gervase Elwes best style 

 Miss Edith Abraham Miss Ethel Hobday gave 
 attractive recital February 3 . ‘ Little Sonata , ’ 
 J. B. McEwen novelty , cheerful 

 Miss Gertrude Peppercorn gave concert February 10 , 
 programme specially arranged interest 
 children . Schumann ‘ Kinderscenen , ’ Op . 15 , 
 selections , Suite ‘ Peter Pan Sketches , ’ Harry 
 Farjeon , novelty 

 concert given London Trio February 14 , 
 programme included Beethoven Grand Trio , Op . 97 , 
 Leclair D major . Miss Goodwin ( pianoforte ) 
 played interesting selections usual skill , 
 Miss Margaret Champneys sang agreeably 

 Miss Daisy Kennedy ( violin ) gave recital February 15 . 
 greatest achievement remarkable performance 
 Paganini Concerto D 

 Central London Choral Orchestral Society drew 
 large audience commodious Central Hall , 
 Westminster , occasion concert given 
 January 20 . Elgar chorus ‘ comes misty ages , ’ 
 ‘ Banner St. George , ’ Percy Fletcher 
 ‘ Song Grey Seas ’ appropriate items . Mr. D. J. 
 Thomas conducted 

 Royal College Music concert given February 1 , 
 students gave remarkably good performances Brahms 
 Quartet , Op . 51 , . 2 , César Franck ( Juintet F 
 minor . interesting item playing Doris 
 Fell Kathleen Connah Saint - Saéns Variations 
 pianofortes theme Beethoven 

 Leighton House Chamber Concerts brought Mr. 
 Arthur Catterall quartet party London play 
 February 8 . proved fine body 
 artists . Modern music represented Taneiev Quartet 
 minor ( Op . 11 ) . Miss Jean Waterston sang 
 Cyril Scott songs accompaniment composer 

 needs ‘ - Breakfast Concerts ’ complete 
 present musical output , future shall 
 Birmingham concerts times day . 
 ‘ - Tea Concerts ’ , concerts held 
 o'clock , , irrespective evening 
 concerts , crown Mr. J. C. Hock , local violoncellist , 
 giving series * Dinner - Hour Concerts ’ 
 Midland Institute 

 new venture organized Mr. Stevenson , 
 jun . , conductor New Philharmonic Society , 
 , ‘ Town Hall Promenade Concerts , ’ 
 given January 18 , attendance 
 means encouraging . match enterprise 
 far early ’ fifties , time Crimean 
 War , famous Julien inaugurated popular Town 
 Hall Promenade Concerts , famed vocalists 
 instrumentalists time appeared , Ernst , 
 Wieniawski , Madame Pleyel , Reichardt , Koenig , Madame 
 Gassier , Liebhardt , & c. years later late Mr. 
 Ffrench Davies essayed Promenade Concerts 
 Town Hall , similar ventures failed 
 draw large audiences . Mr. Stevenson 
 beat fairly large orchestra amateurs pro- 
 fessionals , gave spirited enjoyable performances 
 strictly popular orchestral selections , comprising 
 ‘ Peer Gynt ’ Suite . 1 , Sibelius famous ‘ Valse 
 Triste , ’ Moussorgsky Russian Dance , ‘ Gopak , ’ 
 Barcarolle ‘ Tales Hoffmann , ’ Prelude 
 ‘ Carmen , ’ Overture ‘ 1812 . ’ - named 
 needed larger rank file , especially 
 sonorous array strings . Mr. Norris Stanley , 
 young talented local violinist , played Beethoven 
 Concerto finish technical skill 
 Town Hall year . vocalist Miss Vera 
 Horton , sympathetic contralto . second Promenace 
 Concert given February 8 , attendance 
 come expectations , concerts 
 continued better patronage bestowed . 
 programme advance given 
 previous concert , contained Overture ‘ Merry Wives 
 Windsor ’ ‘ Meistersinger , ’ ‘ Finlandia , ’ Polovtsienne 
 Dances Borodin ‘ Prince Igor , ’ Hungarian 
 March Berlioz ‘ Faust . ’ excellent interlude 
 proved Miss Bessie Clarke scholarly interpretation Grieg 
 Pianoforte Concerto minor , Op . 16 , accompanied 
 orchestra . vocalist Mr. Herbert Simmonds , 
 artistic cultured baritone gifted sympathetic 
 voice . ‘ concert distinct advance 
 predecessor 

 connection Catholic - union Birmingham , 
 January 30 , excellent concert arranged Mr. Gervase 
 Elwes given Grosvenor Room , Grand Hotel , 
 attended high dignitaries Roman Catholic Church . 
 addition Mr. Gervase Elwes appeared Miss Louise 
 Dale , Miss Dilys Jones , Sefior Gomez , Spanish violinist , 
 Miss Dorothy Howell ( pianoforte ) , Mr. B. Kiddle 
 accompanist 

 Considerably half 1916 - 17 series 
 Symphony Concerts given , 
 likelihood interest successful proceedings 
 steadily maintained finish season . 
 Certainly point numbers recent audiences 
 satisfactory , safe assume Thursday 
 functions constitute soundest propositions 
 policy Winter Gardens . , Mr. Dan Godfrey 
 knows public provide . | Tchaikovsky , 
 instance , sure draw average audience , 
 , January 18 , programme entirely 
 works composer presented highly appreciative 
 assembly . excellent playing heard following 
 week diverse compositions Berlioz ‘ Carnaval 
 Romain ’ Overture , Mozart (; minor Symphony , 
 portions Suite Symphonique constructed 
 Charpentier opera ‘ Louise ’ ( time ) , item 
 particular receiving flexible , emotional , 
 polished performance . occasions genuine 
 pleasure derived revival notable works 
 Borodin B minor Symphony , Dvorik ‘ Carnival 

 Overture , Glazounov sixth Symphony , , , 
 Beethoven example major . Compositions previously 
 unheard concerts comprised Overture 
 Introduction Act 3 Goldmark opera , ‘ 
 Cricket Hearth , ’ ‘ Ancient Scots Tunes ’ set 
 strings Sir A. C. Mackenzie , H. Orsmond Anderton 
 * Spring Idyll 

 solo performers Symphony Concerts 
 Miss Daisy Kennedy , reading Tchaikovsky 
 Violin Concerto individuality extremely 
 artistic ; Miss Ruby Holland , , playing 
 revealed good qualities , exhibit sutflicient 
 strength work Scriabin Pianoforte Concerto 
 ( performance ) ; Miss Mary Law , 
 conspicuous success Wieniawski Violin Concerto 
 D minor , reason charming expressive 
 interpretation music ; Mr. Ioan Lloyd - Powell , 
 surpassed performances Bournemouth 
 scholarly thoughtful playing Beethoven ‘ Emperor ’ 
 Pianoforte Concerto ; Miss Joan Willis , inter- 
 pretation Saint - Saéns minor ’ Cello Concerto 
 refined , little lacking authority 

 Owing writer inability attend ‘ Monday 
 Special ’ Concerts impossible furnish details 
 series , according accounts programmes 
 exactly suited inclined little 
 overawed bigger musical forms represented 
 symphonies concertos 

 CAMBRIDGE 

 London String Quartet paid visit Cambridge 
 February 8 , gave concert term 
 connection University Musical Society . 
 players attract large number people , size 
 audience concert showed popularity 
 fully maintained . programme consisted Beethoven 
 Quartet F minor ( Op . 95 ) , McEwen ‘ Biscay ’ Quartet , 
 familiar G minor Debussy . Owing 
 ill - health Dr. Rootham residence term , 
 consequence Society programme recast . 
 hoped performance sacred music St. John 
 College Chapel , announced March 15 , given 
 Easter term 

 February 13 chamber concert given 
 Newnham College Hall aid Cambridge Serbian 
 Relief Fund . players Misses Margery 
 Thelma Bentwich ( violin violoncello ) , Mr. Raymond 
 Jeremy ( viola ) , Dr. Rumschyski , contributed 

 Brahms Pianoforte Trio B major ( Op . 8) , Beethoven 
 Trio C minor violin , viola , violoncello ( Op . 9 , 
 . 3 ) , Dvordk Pianoforte ( ) uartet 

 Mrs. Haydn Inwards gave pianoforte recital 
 February 14 Guildhall works J. S. Bach , 
 Chopin , Liszt , Brahms , César Franck , Debussy 

 chamber - music matinée Torquay Pavilion 
 February unusual interest . string quartet 
 consisted Mrs. Lennox Clayton , Miss Jessie Bowater , 
 Miss Ethel Pettit , Mr. Lennox Clayton , Mr. Edgar 
 Heap collaborated pianoforte . Schumann Op . 44 
 ( Quintet Haydn Op . 77 , . 1 , Quartet received 
 excellent interpretations . success classical pro- 
 gramme assured series similar matinées 
 arranged . occasion cited , Mr. John Buckley 
 sang songs Handel Henschel . February 3 , 
 Municipal Orchestra played afternoon , 
 evening band Reserve Battalion London 
 Regiment , Bandmaster Crane , gavea concert instru- 
 mental vocal music 

 Dr. Rumschisky , Rassian pianist , played 
 Beethoven Sonatas ( Opp . 81 31 ) , pieces 
 Chopin , Torquay , February 9 , Municipal 
 Orchestra performed Schumann Symphony E flat 
 Haydn D. Miss Bessie Bingham sang songs 
 Henschel , Coleridge - Taylor , Tchaikovsky 

 Barnstaple Parish Church performances given 
 February 11 12 sacred Cantata soli , chorus , 
 organ , subject Epiphany , music 
 composed Dr. H. J. Edwards words written 
 selected Rev. Thomas Russell . work , exalted 
 fervently devotional spirit , sung members 
 Festival Society church choir ; Dr. Edwards 
 provided organ music invariably inspired skill , 
 Mr. S. Harper , sen . , conducted . Church 
 filled listeners , singers occupying chancel . 
 scheme Cantata comprised parts — ‘ Prophecy , ’ 
 ‘ Journey , ’ ‘ Adoration ’ --and music , parallel 
 text , develops power spiritual enthusiasm climax 
 . episode Magi centre 
 interest , motif passage star , frequently 
 recurring work , assists particular 
 significance . imaginative suggestiveness gives sense 
 rapture Prelude ( organ ) ‘ Bethlehem ’ 
 scene , forms basis . Reverting 
 second , notice beautiful chorale , ‘ O Thou 

 DUBLIN 

 Royal Dublin Society , chamber music recitals 
 given Brodsky Quartet ( January 29 ) , playing 
 Schumann , Op . 41 , . 1 , Tchaikovsky , Op . 11 , 
 Beethoven , Op . 59 , . 3 ; Dr. Esposito ( February 5 ) , 
 played Bach Chromatic Fantasia Fugue , 
 Beethoven Op . 110 , Grieg ‘ Ballade , ’ Chopin 
 Nocturne B major , Op . 62 , Barcarolle Etude , Op . 25 , 
 . 12 , smaller pieces Arne Scarlatti . 
 February 12 string orchestra , conducted Dr. Esposito , 
 played Handel Concerto Grosso B flat , Bach 
 ‘ Brandenburg ’ Concerto G , Grieg ‘ Holberg ’ Suite , 
 pieces Scarlatti , Esposito , Gernsheim . 
 recital present series came conclusion 

 Feis Ceoil announced week commencing 
 7 , held Mansion House , kind per- 
 mission Lord Mayor Dublin . adjudicators 
 Mr. Bantock Pierpoint , Mr. H. Wessely , Signor Carlo 
 Albanesi , Mr. Sydney H. Nicholson . year com- 
 petitions held Marlborough Street School buildings 
 July , circumstances exceptional , 
 late date unsuitable , schools close 
 end June , people absent Dublin 
 July August 

 Burns anniversary celebrated usual . 
 concerts given January 26 27 , 
 direction Mr. James C. Lumsden , father 
 conducted events . Mr. Lumsden enhanced 
 popularity securing best artists inter- 
 pretation Scottish song , amateur professional 
 alike flock feast Scotch music 

 Prof. Donald Tovey gave fifth Historical Concert 
 January 31 . programme consisted Sonata 
 F , Op . 10 , . Sonata E minor , Op . 90 ; 
 Thirty - Variations Diabella Waltz , Op . 120 ; 
 Sonata flat , Op . 110 . mentioned 
 Professor beginning played works 
 memory , feeling intimacy simply 
 delightful . sixth rehearsal series 
 given February 14 , preceded lecture 
 February 13 , discussed Beethoven later styles . 
 programme Sonata Op . 13 , Variations ‘ La Stessa , ’ 
 Sonata Op . 28 , Fantasia Op . 77 , Sonatas Opp . 81 11 

 concert nominally promoted young people , 
 equally interesting older folk , given February 10 . 
 Mr. Fuller Maitland gave harpsichord selections , Miss 
 Patupa Kennedy Fraser sang Celtic songs Celtic harp 
 accompaniment 

 Sir Henry Wood conducted admirable programme 
 eighth Philharmonic Concert January 23 , 
 included ‘ Fidelio ’ . 4 Overture , Coleridge - Taylor 
 strenuous Ballade minor , Elgar noble ‘ Enigma ’ 
 Variations , interest grows successive 
 hearing . draught crystal spring Sir 
 Henry Orchestral Suite . 6 , excerpts Bach 
 clavier - music , including ‘ Caprice departure 
 beloved brother , ’ Preludes ‘ - , ’ 
 Scherzo Partita , Gavotte Musette 
 sixth English Suite , Preludio 
 Partita solo violin . musicians 
 familiar pieces appealed fresh delight new 
 medium , question favourably 
 received general audience . 
 purists complain transference , 
 forgetful Bach led way direction 
 transcribing accommodating Vivaldi Violin Concertos 
 organ harpsichord . 
 complain irreverence Bach clavier organ- 
 music played modern grand pianoforte 
 concert - organ . conclusion Sir Henry Wood 
 notable service orchestral transcription 
 Bach ancient master Moussorgsky modern . 
 ‘ Rhapsodie Espagnole ’ Ravel strong meat , 
 impossible describe line . Suffice 
 created definite desire hear . fine singer , 
 Madame Kirkby Lunn , sang ‘ Che faro ’ songs 
 MacDowell Quilter great acceptance , 
 Berlioz Chorus ‘ Ophelia ’ female voices orchestra 
 interesting , effectively performed 

 Sir Frederic Cowen received flattering reception 
 appearing ‘ guest - conductor ’ ninth Philharmonic 
 Concert February 6 . long association 
 Society , 1896 - 1913 , assured welcome 
 rostrum , performance conducted C minor 
 Symphony keeping best traditions 
 Society , reputation interpreter 
 Beethoven great music . programme 
 conventional order , containing Tchaikovsky ‘ Marche Slave , ’ 
 heroic , theatrical , trivial turns , Gluck- 
 Mottl ‘ Suite de Ballet , ’ Grainger ‘ Shepherd Hey . ’ 
 violoncello solos Madame Guilhermina Suggia showed 
 great executive skill accurate , delicate , tone 
 Boéllmann ‘ Variations , ’ Schumann uninterest- 
 ing Violoncello Concerto ; vocalist , Madame Licette , 
 sang brilliantly - worn ‘ Ah ! fors ’ é lui , ’ 
 appropriate feeling Charpentier ‘ Depuis la jour 

 Mr. Joseph Holbrooke a_highly - favourable 
 impression series chamber concerts , 
 commenced beautiful Crane Hall January 22 , 
 powers composer pianist , especially 
 interpreting music , exhibited 
 chamber works bear unmistakable stamp 
 original force distinct individuality . Examples 
 qualities , include noticeable avoidance 
 commonplace conventional , carried occasionally 
 verge eccentricity , offered ‘ Sonata 
 Romantic ’ violin pianoforte , Pianoforte Quintet 
 G minor , whimsical ‘ Valse Diabolique ’ 
 vigorous Finale , Pianoforte * Etudes , ’ 
 ‘ Valse Romaneske , ’ ‘ Scéne Bacchanale 

 concert Rodewald Concert Society , 
 January 29 , Mr. R. J. Forbes pianist joined 
 Catterall String Quartet splendid performance César 
 Franck Pianoforte ( Juintet , satisfactory 
 strongly rhythmic Pianoforte Quartet Fauré , 
 finest modern works medium . Mr. Arthur Catterall 
 Mr. Forbes played Delius Sonata violin 
 pianoforte , music ‘ atmospheric ’ order , 
 hearing conveys message 

 attractive programmes drew large audiences 
 Symphony Orchestra Concerts Philharmonic Hall . 
 January 20 , Pachmann played exquisitely 
 orchestra Chopin Andante Spianato 
 Polonaise Brillante , detached solos demon- 
 strated incomparable delicacy touchand toneand revealed 
 usual irresistible drollery . Schubert B minor Symphony , 
 Glazounov acceptable ‘ Paraphrase Hymns 
 Allies , ’ Tchaikovsky ‘ Casse Noisette ’ Suite gave Mr. 
 Akeroyd fine orchestra plenty opportunity . 
 February 3 , Beethoven seldom - heard Symphony . 4 , 
 attractive ‘ Festivo ’ ‘ Scénes 
 Historiques ’ ( Sibelius ) , chief orchestral items , 
 Mr. Albert Sammons gave masterly performance 
 solo Saint - Saéns Violin Concerto . 3 , 
 B minor . vocalist Miss Ida Kidder , accomplished 
 singer revived singular charm Sullivan song 
 ‘ Birds night . ’ Excellent pianoforte accompaniments 
 played Mr. T. Nevison 

 Shakespeare Theatre Harrison - Frewin Opera 
 Company gave January 25 performance 
 provinces French composer Edmond Missa opéra- 
 comique , ‘ Muguette , ’ based Ouida famous ‘ 
 Little Wooden Shoes . ’ principal parts sustained 
 Miss Nora D’Angel , Miss Beatrice Waycott , Mr. Walter 
 Hyde , Mr. Llewys James . music , generally 
 melodious , possesses indications dramatic power . 
 revival Puccini ‘ La Boheme , ’ 
 February 16 . Owing success attended 
 Company visit , season extended hy 
 possibly weeks 

 domain orchestral music causes | Latin rule convey impression Fe . 
 occasioned departures advertised programme | grasp significance text . performance fies 
 events , fresh zest given musical appetite | exception general rule , stronger 
 open morning papers latest operations | poetic technical aspects . “ lo 
 jocularly term ‘ substitution board , ’ ] Goossen handling Debussy ‘ Blessed Damozel ' ( - 
 Mr. Aikman Forsyth rises superior difficulties } February 15 ) sympathetic , small choir aa 
 occasioned illness , distance , submarine menace , naval | - ladies fitted better scheme triki 
 military duties , learning , , | previous occasion November , 1915 ; — 
 practically unlimited extent native riches . matter ] soloists instance attain ideal presentation ve 
 Paderewski , Mlynarski , Rénée Chemet find impossible | unforgettable performance , oe ol 
 visit , — experience | possibly Manchester experience thos Bthe 
 January 25 , instead Paderewski Glinka | exquisite sensations compounded poet , musician , ones 
 ‘ Life Czar ’ ( cuts ) sung English , | personality . nicht 
 heard July , 1888 , Edward Garcia 
 Moscow Operatic Company performed . Russian . , : _ ‘ 
 bass choralists oon finest efiects opening NOTTINGHAM DISTRICT . . 
 chorus , opera April } annual musical Festival Congregational Church , Sheff 
 heavier - toned choir . performance | Hucknall , took place January 28 . music selected C 
 people enjoyed enthusiastic . Miss Mignon | included Te Deum B flat ( Stanford ) , ‘ King glorious Fant 
 Nevada exceptional gifts Antonida . | ( Barnby ) , ‘ Lord hath great things ’ ( John E. West ) , ‘ Path 
 Manchester Operatic Chorus , trained Mr. W. A. | ‘ Babylon Wave ’ ( Gounod ) . orchestra . 
 Lomas , alert tolerably efficient , numerous ] gave Schubert ‘ Unfinished Symphony , ’ Walford “ 
 compete heavy brass - wind ; period | Davies ‘ Solemn Melody . ’ organist Mr. C. E. hi 
 preparation curtailed detention | Blyton Dobson , Mr. J. Munks conducted . sscite 
 scores London performance prior Christmas . | Bingham Choral Society gave twentieth annual Sen 
 Probably operatic performance instant | concert February 7 , Gaul ‘ Joan Arc ’ received Topi 
 appeal general public , , ‘ Boris Godounov , ’ | successful performance . soloists , heard ~ 
 doubt instructed musical public pass ] miscellaneous selection , Miss Lilian Clayton , Mr. Mi 
 favourable verdict . Franklin Pearson , Mr. A. Farnsworth . Mr. Pilling Dale 

 far enforced policy substitution having | conducted , Mr. C. Doncaster responsible § 
 proved disservice Manchester public , produced | accompaniments . Albert Hall organ recital speci 
 positive gain forcing attention| February 4 , Mr. Bernard Johnson played Bach Fugue 
 uncommonly rare combination talents Eugéne| B minor , co - operation Miss Emily Roseblade ; 
 Goossens ( jun . ) . recent article commented } pianoforte , heard pieces Debussy 
 young conductor willingness step breach | Arensky , Guilmant Scherzo Capriccioso 
 moment , carry programme exceptional | pianoforte organ . Mr. Johnson Pianoforte Suite , ‘ 
 difficulty . February 1 15 filled gaps caused | deserted waterway ’ item admirably interpreted L 
 Mlynarski absence Sir Thomas Beecham illness , | Miss Roseblade . Chamber Concert week 
 carrying advertised schemes , | Albert Hall held February 5 February 9 . ite 
 date including new works Spaniard , Turina , a| effort popularising chamber music , Mr. Bernard 1 
 march Chabrier , mention Bantock ‘ Hebridean ’ | Johnson solely responsible , ‘ week ’ included 
 Symphony , hitherto conducted Mlynarski . Goossens | afternoon evening performances . programmes Led 
 obviously master situation , | announced subjoined : aa : shoul 
 performance unusual fluency point . years ago , Monday , February 5 : Pianoforte Quintet F minor , th 
 Gloucester Festival , late Arthur Johnstone César Franck ; String Quartet C minor , Op . 18 , . se 
 city said writer , ‘ Bantock Berlioz - like | ° 4 , Beethoven ( Mr. F. Mountney , Mr. W. Whitehead , whe 
 vastness conceptions , wonder } Mrs. Marshall , Mr. E. Thorpe , Miss Cantelo ‘ ome 
 ideas compassed,’—and memory this| pianoforte ) . Tuesday , February 6 : Variation — y 
 utterance came listened Goossens reading . | Fugue Russian Folk - Song , Knorr ; Concerto ia th 
 suggestive power opening gripped sea-| flat pianofortes orchestra ( organ ) , Mozart 
 music — elemental , vast , tireless } ( Misses Irene Una Truman , Mr. B. Rims 
 heaving mighty waters , revealed overwhelming } Johnson ) . Wednesday , February 7 : Clarinet Quintet » e 
 imaginative force . little later impressive| B minor , Brahms ; Fantasie F minor , Frank Bridge 

 XUM 

 IQI7 . 137 

 String Quartet F , Dvorak ( performers 
 Monday , Mr. Boak , clarinet ) . Thursday , 
 February 8 : Pianoforte solos — ‘ Waldstein Sonata , ’ 
 Beethoven , ‘ Variations Serieuses , ’ Mendelssohn ; organ 
 solo , Prelude Fugue B minor , Bach ; Pianoforte 
 Trio D minor , Schumann ( Miss Cantelo , Messrs. 
 Mountney , Thorpe , andJohnson ) . Friday , February 9 : 
 Pianoforte ( Quartet C minor , Brahms ; Suite Violin 
 Pianoforte , York Bowen ; Trio D minor , 
 Mendelssohn ( Miss Alice Hogg , Mrs. Marshall , Messrs. 
 Mountney Thorpe 

 performance ‘ Messiah ’ aid St. Dunstan 
 Hostel blind soldiers sailors Wesleyan Chapel , 
 Beeston , given February Io 

 Mr. Herbert Ellingford appearance 
 Sheffield solo organist , giving - attended recital 
 Cathedral Church . varied programme included Liszt 
 Fantasia BAC H , Finale Tchaikovsky 
 ‘ Pathetic ’ Symphony , Wagner ‘ Ride Valkyries . ’ 
 Mr. Wolstenholme month heard 
 4 recitalist organ Albert Hall , playing 
 graceful compositions . attractive organ 
 recital given St. Mary Church Mr. C. E. J. 
 Hornsby . programme Sonata D minor 
 Topfer , Bach Fantasia Fugue G minor , 
 movements Borowski Sonata 

 Miss Fanny Davies Mr. Albert Sammons , Miss Louise 
 Dale Mr. Boris Bornoff , chief performers 
 fourth Sheffield Subscription Concert . Mr. Sammons 
 specialised antique music , illuminating 
 exponent , Miss Davies Beethoven 

 YORKSHIRE . 
 LEEDS 

 Entertainments , ’ important thing 
 programme ; - played , received 
 tepid enthusiasm 
 characteristic Leeds 

 Leeds Bohemian Chamber Concert , January 24 , 
 Mr. Cohen String Quartet heard works 
 Schubert ( D minor ) , Tchaikovsky ( D , Op . 11 ) , 
 Walford Davies,—the charming ‘ Peter Pan ’ ( ) uartet . 
 concerts , hand , warm 
 enthusiasm , audience consists élite 
 society élite musical people , , 
 fashionable enjoy sensation 
 provided brilliant virtuosi , solely entirely 
 want enjoy music . similar function 
 supplied Sonata Recitals , instituted Mr. Cohen , 
 January 26 , Prof. Rogers , 
 exceptionally efficient amateur pianist , played Violin 
 Sonatas Bach Schumann , shorter pieces 
 Catoire Medtner . second recital Mr. Percy 
 Richardson pianist , programme consisted 
 Sonatas Beethoven ( /of ‘ Kreutzer , ’ Sonata 
 C , Op . 96 ) , Medtner ( B minor ) , Fauré ( 

 Miss Edith Robinson Mid - day Conccrt , January 23 , 
 quartet ladies , Mr. H. Mortimer clarineitist , 
 played Mozart Clarinet Quintet refinement finish 
 series , February 6 , Mr. Mrs. 
 Rawdon Briggs played Bach Concerto D minor 
 violins , Spohr effective duets . concerts 
 met support 
 entitled , possibly necessarily shorten 
 time allowed lunch ; intended set 
 firmer financial basis , hopes endeavour 
 prove successful 

 Saturday Orchestral Concert January 27 , 
 series proving popular best sense 
 word , Gilson Symphony — Symphonic - Poem — ‘ La Mer , ’ 
 introduced Leeds , Vaughan Williams 
 ‘ Norfolk Rhapsody . ’ performance 
 hardly adequate details , showed necessity 
 fuller rehearsal ; work , regarded series 
 orchestral tone - pictures , effective , highest 
 order , handling orchestra certainly 
 able . ‘ Norfolk Rhapsody ’ enjoyable , 
 César Franck ‘ Les Djinns , ’ Miss Kathleen Frise - Smith 
 solo pianist , Mr. Frank Mullings interesting vocal 
 solos , features programme , , 
 rich . Mr. Fricker conducted . Leeds 
 University recital given February 6 Mr. 
 Herbert Johnson , played interesting series 
 pianoforte pieces , including Schumann G minor Sonata 
 transcription Bach Organ Toccata D minor , 
 brilliant style . Mr. Hoggett , teaches music 
 University , giving series lecture - recitals 
 Beethoven Sonatas , began January 30 
 exposition Sonatina G ( Op . 79 ) , clear lines 
 served purpose analysis . event 
 common importance visit Mr. Alfred 
 Mallinson native town , , co - operation 
 wife vocalist , gave , February 7 9 , recitals 
 songs , manifested extraordinary gift 
 possesses imparting vivid expression verses 
 varied sentiment type . pianoforte parts — ‘ accom- 
 paniments ’ hardly word — wonderfully suggestive , 
 fails required atmosphere , 
 especially plays exceptional 
 subtlety power 

 BRADFORD 

 J. Harrison 3d 

 true love hath heart ( 4 v. ) 
 W. A. C. Cruickshank 3d . 
 ° R. Schumann ad . 
 Night Hamish MacCunn 3d . 
 Night Music H. W. Wareing 3d . 
 . * Night sinkson wave Henry Smart 2d 
 Night Song ( 2 v. ) Martin Roeder 3d . 
 * Nightingale , 
 Nightingale , ( Arr . H. Leslie ) 
 % . Weelkes 2d . 
 Nights , G , Roberti 3d . 
 Noble thy life ( Canon 6 v. ) 
 Beethoven 3d . 
 Northern love song , H. Hofmann 2d . 
 roses ( 4 v. ) Brahms 3d . 
 ( 4 v. ) Mendelssohn 2d . 
 sleeps crimson ( 4 v. ) 
 G. von Holst 
 * Nan , ( 4 v. ) ... « » J. Brahms 
 Nurse Song ( 2 v. ) B. Luard - Selby 2d . 
 Nymphs Rhine , Marschner 4d . 
 O beautiful violet ( 2 v. ) C. Reinecke ad . 
 4 clap hands E. H. Thorne 6d . 
 rateful evening . C. Reinecke 2d . 
 Siew fair ( Arr . 
 Henry Leslie ) Shield 3d 
 O Lord , Thou hast searched ( Surrexit 
 Pastor Bonus ) , 4 v. Mendelssohn 
 O Memory - H. Leslie 
 O love . Hamish MacCunn 
 57 . " O praise Lord ( Laudate pueri ) 
 Mendelssohn 
 * s0 sing God ( Noél ) ... Ch . Gounod 
 Oh , Skylark , thy wing H. Smart 
 ans . Oh , Spring Marie Wurm 
 364 . * O swallow , swallow G. von Holst 
 430 . " Oh , merry P. E. Fletcher 3d . 
 228 . Othou breeze Spring King Hall 3d . 
 258.§ O thou divine ( 2 v. ) A. C. Mackenzie 
 105 . O , thou art Hauptmann 
 289 . O worship Lord ( 4v . ) 
 S. S. Wesley 
 oak thy mournful bier ' prepared 
 C. Reinecke 
 456 © Ona faded violet Hamish MacCunn 
 52 . * Ondeparture ... . Franz Abt 
 197 . land afar extending G. Bartel 
 142 . day Franz Abt 
 » day came Lochlin 

 Nanie 

