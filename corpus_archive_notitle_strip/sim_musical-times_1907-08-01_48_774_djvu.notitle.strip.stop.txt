‘ Charley Smith ’ Wesley letter 
 - known bass vocalist composer , 
 recently married Miss Booth , Norwich : 
 Samuel Wesley evidently colleagues 
 professional visit Yarmouth 

 August , 1820 , ‘ Grand Musical Festival , ’ | 
 lasting days , held St. Nicholas ’ Church | 
 Town Hall , Yarmouth , programmes 
 largely consisting Handel , selections 
 Mozart ‘ Requiem ’ Beethoven ‘ Mount 
 Olives . ’ Mr. Eager ‘ conducted instrumental 
 band Mr. Buck presided organ , ’ 
 occasion Parish Church ‘ presented 
 brilliant appearance number lamps 
 candles orchestra ( expressly 
 built occasion ) parts 
 edifice decorated . ’ addition exercising 
 musical gifts Yarmouth , Mr. Eager pursued 
 avocation dancing - master — Wesley desig- 
 nates Waitre de Ballet — doughty 
 champion Logier system teaching music . 
 settled Edinburgh died 
 June 1 , 1853 . ‘ Dictionary Musicians ’ 
 ( 1824 ) places creative credit Pianoforte 
 concerto ‘ Collection Songs , ’ 
 publications found way 
 British Museum Library 

 December , 1843 , 
 blind organist , appointed 

 Lawrance , pupil remained years . 
 Mr. Lawrance , pupil Moscheles 
 Leipvig , kind boy took great 
 interest progress . carefully graded 
 ical studies , building foundations 

 tecl 
 Bertini , Cramer Moscheles , forming ! ‘ 7.30 , order morning class ( 
 taste Bach , Beethoven Mendelssohn , | miles away ) 8 o’clock . School began g : 

 dinner - hour devoted study ; evening classes 
 lasted till 8.30 ; finished 
 day wild rush Dowlais accompany 
 choir hour . know 
 managed ; 
 indulgent head - master , let fulfil 
 engagements accompanist Eisteddfodau 
 concerts . Saturday /ree day , far 
 schoo ! duties concerned , occupied 
 |in teaching pianoforte organ early 
 | morning till late night 

 “ end pupil - teacher 

 giving lighter - named 
 yser ‘ Songs words . ’ ‘ play , ’ 
 says , ‘ Beethoven sonatas , passed 
 “ honours ” Local Examination held 
 Royal Academy Music ( Associated 
 Board ) . teaching . 
 time lessons Mr. 
 Lawrance piano practise , | 
 times week house 
 kind lady friend , member church . ’ 
 solicitous welfare young | 
 organist , congregation thought desirable } 
 instrument , so| entered Queen Scholarship examination 
 arranged concert , proceeds of|for admission training college , , 
 bought asmall pianoforte . ‘ he|his attention divided music 

 com 

 , 1905 , won chief choral prize 
 National Eisteddfod . 1898 formed 
 1 ladies ’ choir Merthyr ‘ Tydfil , continued 
 years great success . 1900 
 ladies , social position , combined 
 male - voice choir 
 ‘ Hiawatha Wedding - feast , ) occasion 
 proved levelling influence music , 
 fair vocalists sang 
 platform working men fathers 
 employed . ‘ working - men , ’ Mr. Evans 
 proudly observes , ‘ gentlemen , 
 sizg — , result ways 
 best singing secured . ‘ Dowlais 
 male - voice choir formed 1899 
 competing Liverpool National 
 Eisteddfod worked hard 
 months , fateful day arrived sang 
 tenth order choirs , 
 Manchester Orpheus , 
 achieved fame ; came easy winners : 
 saved honour Welsh nation , 
 choral prizes week 
 won English choirs . Dowlais choir gave 
 concerts parts South Wales , 
 Easter , 1901 , sang concerts 
 London success 

 t Royal National Eisteddfod 1901 , held 
 Merthyr Tydfil , Mr. Evans conducted 
 memorable performance ‘ Israel Egypt , ’ 
 chorus 500 voices , concerts 
 ‘ ' Tannhauser ’ overture _ Beethoven 
 C minor Symphony played baton , 
 orchestral concert novelty 
 important national meeting . Lianelly 
 Eisteddfod , held following year , conducted 
 choir won £ 200 prize ; 
 appearance capacity , owing 
 increasing engagements adjudicator 
 thought desirable competitor . 
 , , kept choir 
 time , giving performances ‘ Elijah , ’ ‘ St. Paul , ’ 
 ‘ Revenge , ’ ‘ Hiawatha , ’ ‘ King Olaf , ’ 
 ‘ Victory St. Garmon , ’ 
 occasions took choir London , 
 sang Welsh concerts given ( Jueen Hall 
 

 great change Mr. Evans life - work 
 foreshadowed years ago , story 
 told words . ‘ autumn 1902 
 invited conductor Liverpool 
 Welsh Choral Union , aware 
 100,000 people Welsh descent 
 Liverpool , city fine 
 Eisteddfod choir 1900 , nature 
 experiment , slightest idea 
 Society develop 
 important organization , think 
 means alluring away native 
 heath . work gave Handel 
 ‘ Samson , ” performance 
 months ’ experience choir rehearsal , 
 found great possibilities 
 right kind people work Art sake , 
 external considerations 

 , Care , thou art cruel ’ 
 Come , Musick , sick man jewel , 
 Hrs force nigh slain 

 llar 
 Chancellor : Paesiello ‘ ] Barbiere di Siviglia , produced 
 Serene Chancellor , | year 1780 St. Petersburg , successful 
 permitted rejoice fortune having| Rossini considered bold write opera 
 called great majority votes preside } subject . Beethoven ‘ Leonore , ’ 
 Muses , look round opportunities confer benefits| wished ‘ Fidelio ’ called , threw 
 people countries . | shade Paer opera . 
 stands illustrious representative race to| isa second ‘ Cavalleria Rusticana , ’ composer 
 bound ties warm friendship , ] named Domenico Maleone . work 
 accomplis ! ed music , Camille Saint - Saéns , in| heen given , , said , success , Amsterdam , 
 language suggest popular ear inet somes halen ie niece | Italic 
 flow ‘ melody . perceive possible ut attempt 1s produce Itahan 
 illeviation sickness sea Senate | Stages . Objections naturally raised 
 people Dieppe ( native town ) decreed , Mascagni publisher Sonzogno , present 
 striking enhancement honour , augment title | Accounts differ regard result lawsuit . 
 Rue St. Saéns Rue Saint - Saéns Doctor Music 
 University Oxford ? highly esteem _ 

 particular Degree evident ; illustrious man — _ . ; 7 ; 
 given honours bestowed | Promenade Concerts Queen Hall , 

 Messiah manage belonged Mr. 
 Goldschmidt sold £ 100 

 ‘ nh catalogue 
 following manuscripts penned great 
 masters music — Beethoven , Mozart , Wagner 
 Weber , described follows , additions , 
 including prices realized sale 

 BEETHOVEN . Autograph letter ( unpublished ) , written 
 | M. de Bigot , wife , fine pianist interpreter 
 works , Beethoven presented autograph 
 F minor ( Appassionata ) Sonata . Letter , signed , 
 date , probably written end 
 year 1808 . Beethoven requests pianoforte 
 parts , probably lent Mme . Bigot , returned , 
 concert arranging . letter highly 
 | che aracteristic composer , fine specimen 
 | handwriting . [ £ 415 10s . ] 
 BEETHOVEN . Autograph orchestral sketch Coda 
 Scherzo Ninth Symphony , presented 
 | Ignatz Moscheles friend Henry Phillips , singer , 
 June 14 , 1846 . [ £ 26 . ] 
 Mozart . autograph sketches . , 
 fragment fugue E flat , fine specimen 
 composer handwriting . date 27 bars 
 written exactly known , supposed 
 | 1772 . second edition ( 1905 ) 
 Koechel catalogue , edited late Count Waldersee , copy 
 fragment completion Sechter mentioned , 
 autograph said unknown ! second 
 sketches , 11 bars , consist passages 
 |canonic imitation ; page 
 autograph sketch mentioned , 
 noticed Koechel . [ £ 31 . ] 
 WAGNER ( R. ) . autograph letters written Madame 
 Henriette Moritz , years 1851 - 3 . lady , 
 highly - talented actress vocalist , sister August 
 Roeckel , 1850 condemned death 
 took rising Dresden 1849 , sentence 
 | commuted imprisonment ; letters 
 inquiries respecting Roeckel poor wife . 
 | influence Madame Moritz Wagner 
 * Tannhauser ’ ‘ Lohengrin ’ produced Schwerin 
 German towns , letters touch largely , 
 exclusively , matters . , dated 
 20 , 1853 ; Wagner , thanking interest 
 taking works , warns ‘ lest troubling 
 dangerous state , find 
 black book ’ ( Schwarses Register ) . Mr. W. Ashton 
 Ellis ‘ Life Wagner , ’ Vol . III . , references 
 Schwerin productions * Tannhauser ’ ; 
 | statements evident letters 
 known 

 53 

 Choral competitions Curwen challenge shield 
 followed juvenile concert , winning choir , 
 interesting contest , proving Mile - End Select Choir , 
 conducted Mr. G. Day Winter , Borough Greenwich 
 Choral Society taking prize sight - singing 

 evening concert , choir 2,000 London 
 provincial singers , supported orchestra 
 250 players , opened powerful rendering 
 Beethoven ‘ Hallelujah ’ chorus ; remainder 
 programme included number familiar choruses 
 - songs , Pinsuti ‘ sea hath pearls ’ 
 successful effort . interesting feature 
 concert presentation Miss Lushington medals 
 number veterans taken Tonic 
 lady 
 handed shield prizes winners choral 
 band contributed 
 pieces excellent style providing accompani 

 ment choruses , Mr. H. W. Weston 

 orchestral concert given Miss Tilly Koenen 
 Queen Hall June 25 deserves special mention , 
 vocalist achievements , 
 | presented London Symphony Orchestra able 
 conductorship Herr Max Fiedler . Miss Koenen sang 

 Ah ! Perfido ’ ( Beethoven ) fine power breadth . 
 deeply enjoy interpretation having feel 
 sympathy obvious effort . ripe fullness voice , 
 command resources musical intellectual , 
 enabled Miss Koenen memorable performance 
 great aria , songs ‘ Hymnus ’ ( Strauss ) , 
 ‘ Die Musikantin ’ ( Max Fiedler ) , ‘ Er ist ’ ( Hugo Wolf ) , 
 ‘ Schmied Schmerz ’ ‘ m Kahne ’ ( H. van Eyken ) , 
 | * Die Allmacht ’ ( Schubert ) . orchestra performed ‘ Der 

 Freischiitz ’ overture , ‘ Tod und Verklirung ’ ( Strauss ) 

 C minor symphony ( Beethoven 

 Madame Clara Butt Mr. Kennerley Rumford concert 
 ‘ good - bye ’ previous departure 
 Australasian tour attracted enormous audience 
 Royal Albert Hall June 29 . famous contralto 
 excellent voice , rendering ‘ O mio Fernando ’ 
 Donizetti ‘ Favorita ’ advantageously showed 
 superb quality vocal organ . course 
 twice songs announced programme . Mr. 
 Kennerley Rumford sang finely , interpretation 
 ‘ Largo al factotum ’ Rossini ‘ Barbiére ’ 
 remarkable example vocal skill . artists engaged 
 Misses Pauline Ethel Hook , Esta d’Argo 
 Ada Forrest , Mr. Ben Davies , Miss Gertrude Meller , pianist , 
 Mr. Tivadar Nachéz Mr. W. A. Squire . Mr. Arthur 
 | E. Godfrey organ , Mr. S. Liddle played 
 accompaniments 

 PATRON FUND CONCERT 

 eighth concert Mr. S. Ernest Palmer 
 munificent endowment given Queen Hall 
 July 11 . overture Mr. Walter E. Lawrence , 
 containing clear conscientious writing , prove 
 exciting , Mr. Thomas F. Dunhill scene contralto 
 ( Miss Phyllis Lett ) orchestra , setting Shelley ‘ 
 Night ’ scarcely intensified words poem ; 
 strong effort strong inspiration . 
 ‘ new paths ’ opened ‘ Symphonic Scherzo ’ 
 Mr. Montague F. Phillips , writing clear 
 crisp , scoring effective . suite , descriptive 
 race ‘ Palio ’ standard takes place Siena 
 mn Feast Assumption , suggest rampant 
 realism ; aim composer , Mr. George Dyson , 
 higher plane , music interesting . 
 number programme work 
 student , great master Beethoven , Pianoforte 
 concerto G , solo rendered 
 Mr. James Friskin . conducted Sir Charles 
 Stanford , works played London 
 Symphony Orchestra direction respective 

 com posetTs 

 following novelties announced forthcoming 
 season Hofoper : v. Zemlinsky ‘ Der Traumgorg , ’ 
 Bittner ‘ Die rote Gred , ’ Goldmark ‘ Das Wintermarchen , ’ 
 Roller ‘ Riibezahl , ’ Debussy ‘ l’elléas et Mélisande 

 Sunday afternoon recitals sacred music instituted 
 Dover College Mr. J. Edis Tidnam , newly - appointed 
 | director music , attracted large audiences . 
 | music performed comprised Schubert Unfinished 
 | Beethoven fifth Symphonies rendered 
 | College orchestra , Stainer ‘ Crucifixion , ’ 
 - songs College choir . addition 
 | given Rheinberger Suite violin , violoncello , 
 | organ , trios Schumann Mendelssohn . 
 Sunday concerts place College hall , 
 |contains large - manual organ , presented late 
 Dr. Astley . present vacation chapel organ 
 entirely rebuilt enlarged cost 
 | £ 1,200 , munificent gift Dr. Astley College 

 appeal contributions memorial 
 erected grave celebrated clarinettist , Richard 
 Muhlfeld , died June , Brahms 
 wrote finest chamber compositions , 
 issued Germany conductor Meiningen 
 Orchestra , addressed friends admirers 
 late great artist . Contributions sent 
 Professor Wilhelm Berger , Meiningen , Germany 

 
 libretto somewhat 

 wrote air key F , headed Larghetto 
 Lena.—The following suggested 
 ianoforte pieces name.—Bach ‘ Italian concerto ’ : | 
 41 gro , crotchet 96 ; Andante , quaver 104 ; Presto , 
 minim 112 . Beethoven Sonata E flat ( Op . 7 ): 
 Allegro molto , dotted crotchet 126 ; Largo , quaver 92 ; 
 Allegro , dotted minim 76 section 66 ): 
 crotchet ( Beethoven Sonata E ( Op . 14 , aay ): 
 Allegro , crotchet 138 ; Allegretto , dotted minim 60 ; 
 inim 76 . Studies major 
 Book 3 ( Peters edition ) . headed A//eg > 
 played crotchet 144 ; 
 ) , 2 - 4 lume , quaver 160 
 lecture ‘ Mendelssohn ’ | 
 better consult Sir George Grove biography | 
 ‘ Dictionary Music Musicians . ’ ask for| 
 e dates articles comps ser appeared | 
 Mt AL TiMEs , use | 
 e gladly : ‘ Keminiscences Mendelssohn , ’ 
 August , * Mendelssohn drawings , ’ November , 1897 ; 
 ‘ humorous sketch Mendelssohn , November , 1900 . | 
 article Mendelssohn music associated ‘ Hark ! 
 herald angels sing ’ appeared issue December , 
 1897 . 
 F. B. H.—The suggestion order | 
 realize ‘ ambition travel musical director | 
 light opera ° apply managers opera 
 companies , stating qualifications experience . 
 udvertisement Zra useful method 
 making vour ambition known 

 speeds 

 5s . SYMPHONY B MINOR ( Tue “ UnrinisHep 
 MovEMENT ) - SCHUBERT 
 Arranged W. A. C. CruicKSHANK . 20 
 6 . BERCEUSE ann CANZONETTA ( Op . 2c , Nos . 8 bands ) 
 CESAR CUI 
 Arranged Percy E. FLercuer ° ee es - 68 
 7 . SCHERZO RUSTIQUE ( Op . 20 , . 12 2 ) CESAR CUI 
 Arranged PERCY E. FLETCHER ‘ Io 
 NACHTSTUCK ( Op . 23 , . 4 ) SC HU M. ANN 
 Arranged A. B. PLant 

 MOMENT MUSICAL   n F minor ‘ SCHUBERT e 
 ( Op . 94 , . 3 ) ii 
 Arranged A. B. PLant - - J 
 FANTASIA ano FUGUE c minor C. P. E. BACH 
 Arranged Joun E. Wes 1 6 
 ro . PRELUDE tro II . ( Tie Aro ISTLES : 
 EDW ARD ELGAR 
 Arranged G. R. Sinclair ne . , r 6 
 11 , FINALE Fr — MP HONY . 5 BEETHOVEN 
 | Arranged | . PLant - 2 2 6 
 | } 12 . ADORAMI Ss + z ; * HUGH BI AIR 
 | Arranged Hucu Bair = - ‘ ow SE 
 13 . INTERMEZZO ( “ Tue Bixps ” ArtstorHay s ) 
 C. H. i. PARRY 
 Arranged W. G. Atcock = - 1 
 14 . BRIDAL MARCH FINALE ( “ Tue Birps 
 ISTOVHANES ) . — . & PARR\ 
 Ar ranged G. ALcock . ° t 
 ts . ANDANTE ANOFORTE SONATA C , Op . 
 Arrar ao hp egg - I. BRAHMS 
 16 . ANDANTE ( Pianororte Sonata | vor ( Op . 5 ) 
 Arranged Joun E. Wes1 . . J. BR AHMS 1 6 

 18 . HUMORESKE ( Op . ro , N 

