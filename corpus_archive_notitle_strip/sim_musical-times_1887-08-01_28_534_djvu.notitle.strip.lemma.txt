professional notice . 
 MISS MARTAN BATES ( soprano 

 for Concerts , Oratorios , & c. , address , Mr. Brook Sampson , Mus , Bac . 
 Beethoven House , Northampton 

 MADAME CARRIE BLACKWELL ( soprano 

 MADAME CL ARA WEST ( soprano ) . 
 MISS LOTTIE WREST ( contralto 

 Beethoven Villa , King Edward Road , Hackney 

 MISS MAY LAMBOURNE ( Mezzo- Soprano ' 
 St. Geer re 's Villa , St. James ’s Road , U pper Tooting Ss Ww . 
 MISS MA RY WIL LTS ( Mezzo - Soprano or Contralte ) 
 ( pupil of the late Madame Sairton - Doalby 
 her Academy ; also Professor @ the Hy Park Acad ° mv of Music ) . 
 for Oratorios , Concerts , & c. , addre 9 , Rochester Terrace , 
 Camden Road , N.W 

 these opera be design to minister to the 
 demand for something superior to the ordinary opera 
 bouge , which have loosen its hold upon public estima- 
 tion , even as the burlesque have be drown by the 
 flood of contempt which its own extravagance have 
 let loose 

 from opera to oratorio be but a step , and the 
 record of the past season would be incomplete 
 without a special mention of the success achieve by 
 the performance at ' * Novello ’s Oratorio Concerts , " 
 under Dr. A. C. Mackenzie . ' the story of Sayid , " 
 by the Conductor , for the first time in London ; 
 Stanford 's ' * revenge , " Dvordk 's " St. Ludmila , " 
 Sullivan ’s ' * golden Legend , " Spohr ’s " Calvary , " 
 Cowen ’s " sleep Eeauty , " Beethoven 's ' Choral 
 Symphony , " Gounod ’s ' " Mors et Vita ’' and _ his 
 third ' * Messe Solennelle , " testify to a considerable 
 degree of activity and enthusiasm alike on the part 
 of the director as on that of his splendid band and of 
 his chorus . the performance be in every respect 
 admirable , that of the Choral Symphony especially 
 be magnificent , and a crown glory to the labour 
 of the choir . other society , whose chief labour be 
 direct to the department of Oratorio , such as the 
 Royal Albert Hall Choral Society , with Mr. Barnby as 
 Conductor , andthe Sacred Harmonic Society , with Mr. 
 W.H. Cummingsas chief in command , have do good 
 service . Gounod ’s sacred trilogy ' ' the Redemption " 
 be give by the former body at the Christmas Con- 
 cert , and the work awaken an interest at that time 
 hitherto only excite by ' the Messiah . " whether 
 this be likely to prove permanent , and Gounod ’s work 
 be destine to rival Handel 's as an exposition of 
 Christmas feeling , may be matter worthy of future 

 wy sy 

 concert by Henry Leslie ’s choir have ~ _ give . 
 it have be en observe with regret that the part - singing 
 have not be so satisfactory , and that the constitution 
 of the choral body sadly need to be reinvigorate , so 
 that the purpose for which the Choir be originally 
 establish may be more perfectly fulfil 

 he London ical Society , now conduct ! 
 Dr. Mac seer a true to the object which call it into 
 activ earche out interesting and little know 
 work and bi ring they to a public hearing , pro- 
 duced at its Concert in May a cantata by Beethoven 
 which have probably never be perform in public , 
 and certainly never in England 

 a series of interesting programme be 
 by Mr. Henschel for his * * Symphony Concerts , " 
 although the support of the public be 
 supply , the artistic excellenc 
 be admit on all side . it be just possible that 
 the next season may make up for the want of encour- 
 agement of the first . Mr. Henschel have a guarantee 
 fund for two season , by which time it be hope tha 
 the public will have grow to a knowledge of the 
 value of the scheme 

 of the season proper be 

 wonderful boy pianist and composer , Josef Hofmann , 
 play Beethoven 's Concerto in C major in a manner 
 that excite the great astonishment . he hadshown 
 the versatility of his executive power by the execu- 
 tion of piece by many composer at a Recital in the 
 Princes ’ Hall , and although some ga 3 ais or take 
 ya fe d at the 
 Philharmonic Concerts , his performance be quite 
 equal to many cold pianist concerning whose en- 
 | gagement no objection have be urge . Sir Arthur 
 Sullivan , in consequence of a shock to the nervous 
 system , sustain during the carthquake at Monte 
 Carlo during the time isit to that place , be 
 unable to be present at t two Concerts of the 
 these be conduct by Mr. G. Mount and 

 serie 
 Mr. F. H. Cowen . 
 young Hofmann , to whom allusion be make above 

 we may doubt whether Spohr ’s pre - eminence 
 would have endure even if a young and noble 
 rival have not appear on the scene . his music be 
 hardly adapt for constant wear , and its delicate , 
 finely elaborate structure scarcely accord with the 
 indisputable preference of english taste for some- 
 thing bold , firm , simple , strong . the nation 
 that have make a god of Handel , be not likely to 
 depose he in favour of Spohr . but , however this 
 may be , it be right to congratulate ourselves upon the 
 fact that the Cassel master be himself depose . his 
 music , so luscious and beautiful , have its place among 
 the sweet of the art — to be partake of sparingly 
 and at interval , rather than to be a staple article of 
 food . enjoy often , it would bring on indigestion . 
 happily the star of Mendelssohn arise , and as it 
 soar to the zenith , that of the old master sink , 
 leave behind nothing that can be point out as a 
 permanent influence . Spohr have now no following 
 amongst composer , and nobody think of write in 
 his style , as many do in the style of Mendelssohn , 
 Schumann , Brahms , Wagner , and other who be 
 exemplar with a following 

 if the Spohr fever rage severely for a time , what 
 shall be say of that communicate by the genius of 
 Mendelssohn ? who , have nature expressly make of 
 he a musician to captivate England , would have 
 be no other than he be . save , perhaps , the 
 advent of Handel among we , no more important event 
 than the rise of Mendelssohn ever occur in the 
 history of english music . it certainly be the chief 
 incident in the record of music victorian , which have 
 largely derive its character from mendelssohnian 
 impulse and precedent , just as public taste have 
 be affect in an immense degree by the same in- 
 fluence . 
 of the fascinating Felix have be a curse rather than 
 a blessing , because it have bar the acceptance in 
 this country of new and more progressive com- 
 poser . accord to those who so contend , England 
 would now , but for Mendelssohn . be far ahead of the 
 point she have reach . ' their argument amount to 
 a partial begging of the question . it imply that 
 have England not take so ardently to the composer 
 of the Scotch Symphony and the * * Hebrides " over- 
 ture , she would have surrender herself to the 
 " new and more progressive " " man . that may , or 
 may not , have be the case . there be no proof 
 either way , and , in the nature of thing , there can not 
 be . moreover , it be by no mean allow on all hand 
 that the musician in whose path Mendelssohn have 
 stand be well adapt than he to repay popular 
 appreciation by promote the cause of true art . at 
 the same time , we be prepare cheerfully to grant 
 that the exclusive cult of one composer , or one school 
 of composer , amount to a national misfortune . it 
 resemble breed ' in and in . " the result be de- 
 terioration . with regard to England and Mendels- 
 sohn , however , there never be cause to fear such 
 a disaster , for the simple reason that , while the 
 favourite undoubtedly leave scant room for new comer , 
 he do not expel the old master . Handel , Haydn , 
 Mozart , and Beethoven be not less worship 
 because the people have find a fresh idol , and only 
 the contemporary Spohr , who have reign but a little 
 while , can be say to have suffer . on the whole , 
 there be cause for rejoice rather than mourning in 
 that our country , at the very moment when she 
 awake to musical life , give herself up heart and soul 
 to strain so pure and beautiful , at the same time so 
 masterful and invigorating , as those of Mendelssohn 

 it be sometimes urge that english worship | 
 | Mendelssohn have write nothing else he would still 

 the MUSICAL TIMES.—Aveusrt 1 , 1887 

 as anew creation . it be really a development from 
 it — a development artificially strain to extrava- 
 gance , in many case , no doubt , but still have its 
 root where we have place they . ' the first great 
 romanticist be Beethoven , beyond whom , within the 
 bound of legitimate art , none of his successor have 
 go . Schubert , also , be a romanticist ; so be 
 Weber ; so , in some respect , be Spohr and Men- 
 delssohn ; so , beyond all question , be Schumann . 
 the difference between these master and the roman- 
 ticist par excellence be that they subordinate fancy 
 and expression to the establish rule of art , while 
 their successor give both unrestricted play . but 
 have the root of the matter in common , the 
 second school could follow the first without , to use 
 an intelligible figure , change of carriage . at 
 most , the transfer involve only a _ case of 
 switching from one line of rail to another — 
 an operation which sleepy passenger be not 
 likely even to notice . whatever the fact on this 

 point , there can be no doubt at all that the last fifteen 

 one of those itinerant philanthropist , know to 
 the vulgar as " quack doctor , " but whom the local 
 press more grandiloquently designate " peripatetic 
 son of Aisculapius , " be wo nt to employ the service 
 of a brass band to herald his approach into the 
 large town on his circuit . in the less village 
 certain difficulty stand in the way of his obtain 
 this form of help . as he find that music be 
 always most attractive in call attention to his 
 speech , advocate the unheard of virtue of his 
 pill , salve , elixir , and plaister , he decide to have 
 a hand - organ especially make for his purpose . he 
 give the order to an eminent firm in Clerkenwell , 
 stipulate only that the tone should be loud and the 
 tune popular . the workman who " prick " the 
 tune on the barrel be an Italian , and his knowledge 
 of english orthography somewhat limit , so that 
 his description of the air be rather peculiar . 
 when the organ reach its destination the great 
 satisfaction be express , because the " professor " ’ 
 find that the first melody on the barrel be " see , 
 the corncuring hero come 

 a contemporary journal , in notice the perform- 
 ance of young Josef Hofmann on the 4th ult . , print 
 two account from different correspondent . one 
 state that the boy play " Beethoven ’s C minor 
 Concerto ( without book ) , the orchestral part be 
 perform upon another piano by his father . " the 

 other say , " * the programme be head by Beet- 
 hoven ’s Concerto inc major ( not C minor , as state ) , 
 repeat after the great success at the Philharmonic 
 Concert ; the orchestral part to it be play on the 
 second piano by Mr. Hofmann fére . " do the boy 
 play both ? if not , " who shall decide when doctor 
 disagree ? " especially when they write their prescrip- 
 tion in the same paper 

 RICHTER CONCERTS 

 the season end with a performance of Bach ’s Mag- 
 nificat and Beethoven ’s Choral Symphony , those work 
 be precede , unaccountably enough , by a piece so 
 different as the overture to " ' Tannhauser . " " there be 

 we presume , a real or fancied necessity to have Wagner in 

 the programme ; hence a conjunction startling , incongruous , 
 and every way to be deprecate 

 creation of Bach and Beethoven be give with care | 
 and success , under Mr. Richter ’s intelligent direction , the 

 solo be take by Miss Marriott , Miss Lena Little , Mr. 
 Bernard Lane , and Mr. Watkin Mills . in the Magnificat 
 the exertion of these artist meet with small response from an 
 audience quite out of touch with Bach ’s air , but the splendid 
 chorus make their wonted effect , and be hear with un- 
 feigned pleasure . with regard to the work of the orchestra , 
 we can not praise in excess of desert . here Mr. Richter ’s 
 influence be manifest at its full , and the result could 
 hardly have be well . in the instrumental movement 
 of Beethoven ’s Symphony , the good result of recent 
 change in the personnel of the orchestra be very 
 apparent . it may be question whefher Mr. Richter ever 
 conduct a fine performance in England , the string 
 be hear with all requisite fulness and splendour of 
 tone . 
 under Mr. N. Vert ’s management . regard its pecuniary 
 success we know nothing , and be not dispose to indulge 
 in guess . our hope be that Mr. Vert have make both end 
 meet , and that , by make the programme as comprehen- 
 sive as possible , he will go on to reap a full reward for all 
 his risk and labour 

 PIANOFORTE recital 

 and the number of empty bench suggest a lack of 
 wisdom in the effort to induce the public to alter an estab- 
 lishe custom . it may be say frankly , however , that 
 Miss Remmert be an executant of remarkable ability . in 
 respect of physical mean she remind we somewhat of 
 Miss Sophie Menter , her execution be very powerful 
 and , at the same time , commendably accurate . her 
 sympathy would appear to be with the ultra - modern 
 school , as the two most prominent item in her pro- 
 gramme be Weber 's Concertstiick , arrange for two 
 piano by Hans von Bilow , and Liszt ’s Concerto in E , 
 also arrange for two piano 

 another german pianist who seek for , but only ob- 
 | taine in small measure , the support of London amateur , 
 be Madame Hermann , who give a recital at the Princes ’ 
 Hall on the ist ult . in her instance the executive facility 
 | display be really the only noteworthy point in the per- 
 |formance . Madame Hermann go through Beethoven 's 
 | * * moonlight " Sonata , Mendelssohn ’s Fantasia in F sharp 
 | minor ( Op . 28 ) , Chopin ’s Polonaise in A flat , and various 
 | small piece , in a dry , soulless manner — correct as to the 
 j note , but totally lack in the high quality of piano- 
 forte playing . it be of no use to mince matter ; medio- 
 | critie be far too numerous , and should not be encourage . 
 | the appearance of a distinct luminary in the field of art 
 | be generally follow by a host of imitator , and the case 
 | of little Josef Hofmann be no exception to the rule . Miss 
 | Pauline Ellice , who give a Concert at St. James ’s Hall on 
 the 7th ult . , be officially state to be eleven year old , and 
 | she have therefore an advantage of a year — not two year as 
 state elsewhere — over her juvenile rival . we believe she 
 be chiefly indebted for her present command over the key- 
 | board to Mr. Franklin Taylor , and she be certainly a pupil 
 { of whom any master would feel proud . such work as 
 | Beethoven ’s Concerto in C minor , Mendelssohn 's Capriccio 
 | in b minor , and Liszt ’s arrangement of Weber 's Polaccain EF , 
 | constitute together no mean test of an adult pianist ’s ability , 
 and Miss Ellice play they with almost unfailing accuracy , 
 |More than this it be impossible to say , and we see no 
 special reason for the young lady ’s very early entrance on 
 a public career . the orchestral accompaniment be 
 render in a somewhat rough and perfunctory mannner , 
 Mr. George Mount conduct 

 another young aspirant , whose name , however , be by no 
 mean unfamiliar to the public , be Miss Jeanne Douste , 
 { who give a recital at the Princes ’ Hali on the oth ult . 
 if we remember rightly , this young lady , together with her 
 sister , Miss Louise Douste , first appear about twelve 
 year ago , so that she can be no long a child , though her 
 appearance and manner be distinctly youthful . she com- 
 mence badly , a breakdown occur in the first movement 
 of Beethoven ’s Sonata in b flat ( Op . 22 ) , but afterwards 
 she fairly warm to her work , and play with a good 
 deal of spirit and energy 

 Signor Giuseppe Buonamici , of Florence , give a 
 Pianoforte Recital at the Princes ’ Hall , on the 11th ult . , 
 in aid of the Liszt Scholarship at the Royal Academy of 
 Music . Signor Euonamici , by a thoroughly artistic 
 rendering of the F major variation of Beethoven ( Op . 34 ) 
 as the opening piece , exhibit a delicacy of touch and 
 intelligent manner of phrasing which delight the audience . 
 | although tothe mind of many present Signor Buonamici be 
 happy in his performance of the quiet and more poetical 
 passage — his reading of Chopin be as near perfection as 
 possible — yet the boldness and passion of his execution in 
 the " ' Benediction de Dieu , " and some of the less piece 
 by Liszt , especially the Etude in E , d’apris Paganini , 
 rouse the audience to actual enthusiasm . the two 
 symphonic poem , ' * Hungaria " ? and ' " Mazeppa , " be 
 play on two pianoforte by Mr. Walter Bache and 
 Fritz Hartvigson , the second pianoforte be use for 
 the orchestral accompaniment . the effort of both the 
 | player be greatly applaud 

 MR . CHARLES HALLE ’s concert . 
 two more of these pleasant summer entertainment 
 remain to be notice . at the seventh Concert , on the 

 ES.—Avausr 1 , 1887 . 483 

 Néruda as the violoncellist . the work be likely to be in 
 frequent request , as it be unusually clear and concise as 
 this composer . Spohr ’s trio in F ( op . 123 ) , perform for | 
 the first time , be in every respect worthy of the Cassel | 
 master , though it possess no special characteristic . | 
 the good movement be the Larghetto , which be very lovely . | 
 Mr. Hallé play Beethoven ’s thirty - two Variations in i 
 minor , and join with Madame Néruda in Schubert 's | 
 Fantasia in C , for piano and violin ( Op . 159 ) , the mann rer | 
 in which these familiar work be render by the artist 
 name be well known to lover of chamber - music . Miss 
 Marguerite Hall , as the vocalist , make a decidedly favou 
 able impression , her good effort be Schumann 's | 
 " Widmung . " 
 Mr. Hallé , like his exemplar , Mr. Arthur Chappell , | 
 always take care to bring his season to an effective termi- 
 the special attraction on the 5th ult . be Beet- | 
 ’ Sonata for piano and violin 

 nation . 
 hoven ’s ever fresh " Kreutzer ' 
 which of course receive the full justice at the hand 
 the Concert - giver and Madame Neruda . another item 

 footlight can only come with experience . Mr. Lionel 
 Kilby display a promising light tenor voice as Mucty , Miss 
 Anna Russell sing the music of Agies with quiet taste , 
 Mr. David Price do the same with that of Caspar , and 
 Mr. Otto Fischer be fully competent in the dual part of 
 Prince Ottocary and Kilian . the great amount of stage 
 aptitude be exhibit by Miss Annie Roberts as Aennchen , 
 her singing and her manner be equally bright and 
 pleasant . in one respect the performance compare 
 favourably with that on the professional stage . the air 
 and affectation which be the bane of opera be con- 
 spicuous by their absence , no one leave his or her part to 
 acknowledge applause , or to tout for anencore . the chorus 
 be admirable , and the orchestra , compose mainly of 
 student , do great credit to the conductor , Dr. Villiers 
 Stanford 

 the interest of the three conclude Concerts of the 
 term be largely instrumental , owe to the co - operation of 
 the orchestra at the performance of June 30 and the r4th ult . | 
 Schumann ’s Symphony No . 1 , in b flat , be the chief feature 
 of the programme give on the former date , and the two final 
 movement , in particular , be excellently render under 
 Professor Holmes ’s baton . Cherubini ’s * " * Abencerrages ’ 
 Overture and Beethoven ’s Violin Romance in F , in which 
 the solo part be well play by Mr. Sutcliffe , and Max 
 Bruch ’s " Kol Nidrei , " with Mr. Squire as solo cello , be 
 the other purely instrumental number , the orchestra 

 supply the accompaniment to Mr. Kilby in the ] 
 romance " through the Forest " ( ' * Der Freischiitz " ) | 
 and Miss Davies in Gounod ’s air " far great in| 
 his lowly state , " in both of which the effort of 
 the vocalist seem to meet the approval of the majority | 
 of the audience . Mr. Kilby be again to the fore in the 
 Concert of the 7th ult . , in which he sing two song 
 { ms . ) by Charles Hoby , setting of word by Oliver 
 Wendell Holmes , not want in a certain grace , but 
 curiously incomplete in structure . the choice of vocal 
 music and , it must be add , its execution at these Concerts | 
 be stillon a very much low level than that observe in | 
 regard to the instrumental selection . a welcome innova- | 
 tion , however , remain to be note in the introduction of 
 some part - singing under the direction of Mr. John Foster , 
 who have succeed Mr. Eaton Faning in this department . 
 the material be good , and the tendency to run away from 
 the beat , particularly noticeable in Bennet ’s Madrigal * all 
 creature now , " will no doubt be correct by further 
 practice under so energetic a conductor . the other 
 item in the programme which call for notice be 
 Beethoven ’s Sonata for pianoforte and violin , in G ( Op . 96 ) , 
 play by Miss Annie Grimson and Miss Stone , the same 
 master ’s Pianoforte Sonata in d minor ( Op . 31 , no . 2 ) , well 
 give by Miss Kellett , and , lastly , Schumann ’s immortal 
 Quintet , in which Miss Osborn , Mr. Bent , Miss Elsner , Mr. 
 Hobday , and Mr. Werge be associate with very satisfac- 
 tory result . the last Concert of the term be in some 
 respect one of the good ever give at the College . Goetz ’s 
 lovely Symphony in F , recently perform for the first time 
 at the Philharmonic Concerts , receive such a rendering , 
 thank to Dr. Stanford ’s excellent preparation , as reflect 
 the great credit on all concern . the Intermezzo be 
 play with the utmost delicacy , and the cantilena for the 
 first violin in the Adagio be give with great purity of tone . 
 Mr. Barton show excellent taste and sympathy in his 
 performance of the solo part of Mozart 's Piano Concerto 
 in D minor , introduce the graceful but diffuse cadence 
 of Mr. John Francis Barnett . Sterndale Bennett 's beauti- 
 ful ' Parisina " ' and Mendelssohn ’s " Ruy Blas " overture 
 be also include in the programme , the vocal number 
 be Joachim ’s scena " Marfa , " " O have I Jubal ’s lyre , " 
 and Jensen ’s " Miirmelndes Liiftchen , " sing respectively 
 by Miss Squire , Miss Nunn , and Mr. Atkinson . I 

 MUSIC in SOUTH WALES . 
 ( from our own correspondent . ) 
 generally speak , there have be a lull in the local 
 musical world of late . there have be a few matter 
 deserving of notice , but public attention be now strongly 
 direct towards the forthcoming Listedd the Albert 
 Hall , London 

 M. Piltan ’s experiment also prove that the | 
 tension , more or less great , of the vocal chord be not the | 
 cause of the depth or acuteness of sound , that the voice be | 
 the result of a shock or struggle between the inspirative 
 and expirative muscle ; and , finally , that the point where 
 the collision take place determine the altitude of the 
 sound . the larynx and the glottis , therefore , instead of 
 form sound , only modify they . M. Saint - Saéns do 
 not , of course , enthusiastically accept all M. Piltan ’s con- 
 clusion , but he recommend they to the special attention 
 ofthe member of the Academy and of professor of singing . " 
 the foregoing paragraph , from ' " Paris Day by Day , " in 
 the Daily Telegraph , relate to a most interesting dis- 
 covery . if it be prove possible to apply the invention to 
 voice production , the chief difficulty with which teacher 
 have to contend will be remove , and the art of vocalisa- 
 tion will be impart by machinery , and perhaps any 
 number of voice of like quality may be possibly reproduce 
 ‘ as per pattern 

 the recent establishment of the Highbury New Park 
 School of Music , an Institution for general musical training 
 in this neighbourhood , naturally create some interest 
 in the Concert give by some of the staff of Professors at 
 the Highbury Athenzum , on June 29 . the audience 
 exhibit hearty appreciation of the various item of a 
 specially interesting programme . the Concert open 
 with Prout ’s Concertante Duet in a major , first movement , 
 ably play by Miss C.J. Birch and Mr. Fountain Meen . 
 Miss Hannah Jones be hear to advantage in the recita- 
 tive and air * be thou patient , " from Smart ’s ' Jacob , " 
 and ' " Aufenthalt ' ( Schubert ) , as be also Miss Jenny 
 Eddison in Purcell ’s ' * Nymphs and Shepherds , " and in 
 ' " Nobil signor ' ( encore ) . Mr. John Probert have a 
 hearty re - call for his rendering of ' * Adelaide , " and Mr. | 
 Winn ’s fine voice make a marked impression in Sullivan ’s | 
 " o mistress mine . " Mr. J. H. Leipold play Rheinberger ’s | 
 ' * Menuet " for the left hand , and ' " Toccata , " with great | 
 taste ; and Mr. G. H. Betjemann , who be very popular | 
 here as conductor of the Highbury Philharmonic Society , | 
 play Alard ’s Fantasia for violin on Masaniello , receive 
 a hearty encore , when he substitute Sarasate ’s ' ' spanish | 
 dance . " the special feature of the programme be , | 
 however , Schumann ’s quintet in e flat ( op . 44 ) , admirably | 
 play by Messrs. G. R. and G. H. Betjemann , O’Brien , | 
 H. Channell , and E. Woolhouse , and Beethoven 's septet | 
 in e flat ( Op . 20 ) , render with remarkable executive skill | 
 by Messrs. G. H. Betjemann , H. Channell , Lazarus , 
 Steinebruggen , Anderson , Woolhouse , and John Reynolds . 
 at the commencement of the second part Mr. Charles Fry 
 recite Browning ’s ' good news from Ghent " with dra- 
 matic vigour , and give a humorous rendering of a ' Bab | 
 Ballad . " Mr. A. D. Duvivier conduct throughout . Mr. 
 Oscar Kronke , the principal of the Institution , be to be 
 congratulate on the success of his first concert , give with 
 the laudable object of found a scholarship in the school 

 on Tuesday , the rgth ult . , a numerous gathering of the 
 member of the Royal Society of Musicians of Great Britain , | 
 include those who have retire from the active exercise of 

 on Sunday , June 26 , there be special Thanksgiving 
 Services for her Majesty ’s Jubilee at St. Agnes ’ , Ken- 
 nington Park , S.E. 
 morning be a Service in C compose by Mr. W. W. 
 Hedgcock , the Organist and choirmaster of the Church 

 at the offertory , Beethoven ’s ' * hallelujah ' Chorus from 

 the " * Mount of Olives ' ? be sing , and Costa ’s arrange- 
 ment of the National Anthem conclude the service . Best ’s 
 " March for a Church Festival ' ? be the voluntary 

 a new organ at St. John ’s Church , Bedford Hill , Balham , 
 be open on the 21st ult . , by Mr. H. W. Weston , F.C.O. , 
 Organist and Choirmaster of the Parish Church . the 
 instrument have be build by Messrs. Jones and Sons , under 
 the supervision of Mr. H. W. Weston , and contain two 
 complete manual and preparation for the third . the two 
 recital be largely attend , the organist admirably 
 show off the power and variety of the instrument in a 
 carefully select programme of standard organ work 

 Mr. WILLIAM PoEL give a matinée at the Vaudeville 
 Theatre , on the 5th ult . , when he produce a piece call 
 " * Adelaide , " deal with an incident in the life of Beet- 
 hoven . Miss Mary Rorke represent Adelaide , and Mr. 
 Poel , Beethoven . he also play in a comedy call * " Mrs. 
 Deakly ’s difficulty . " a domestic sketch , ' * drift apart , " 
 be cleverly act by Miss Cowen and Mr. Eric Lewis , 
 and a musical performance by the Neapolitan Quartet com- 
 plete the programme 

 Aw Organ Recital be give at St. Nicholas , Cole Abbey , 
 E.C. , by Mr. H. W. Weston , F.C.O. , of Balham , on 
 Coronation Day , June 28 , to a large congregation , con- 
 siste mostly of business man . the programme contain 
 Mendelssohn ’s Overture ' * Ruy Blas , " two movement for 
 the Organ by Niels Gade , a Concerto by Handel , and work 
 | by Guilmant and Freyer . the recital commence with 
 the National Anthem and conclude with Meyerbeer 's 
 " Coronation March 

 ar St. John ’s , Waterloo Road , S.E. , a Jubilee ser- 
 vice be hold on Sunday evening , June 26 , when the 
 music comprise Tours ’s Evening Service in F , Handel 's 
 " the king shall rejoice , ' and Smart ’s Te Deum in F ; 
 Mendelssohn ’s * Athalie ' ? March be select for the 
 conclude voluntary . Mr. Edmund West preside at the 
 organ , and Mr. Henry J. B. Dart conduct the band and 
 choir , number sixty performer 

 Mr. F. SLApE OLver , Professor of Music at St. Paul 's 
 Hall , Salem , New York , give an Organ Recital at St. 
 Peter ’s , De Beauvoir Town , on Wednesday evening , the 
 6th ult . the programme consist of work by Benedict , 
 Batiste , Olver , Beethoven , and Rink ’s variation on ' * God 
 save the king . " Mr. Taylor sing Mendelssohn 's * o rest 
 in the Lord . " Mr. A. Kelly , the Organist of St. Peter ’s , 
 ably accompany the shortened evening service 

 ar the Commencements , hold at Trinity College , Dublin , 
 on June 30 , the degree of Doctor of Music be confer 
 upon Mr. Charles E. Allum , Mus . Bac . , conductor of the 
 Stirling Choral Society . selection from Dr. Allum ’s 
 Cantata , ' Jehoshaphat , " be perform in the College 
 Chapel , Dublin , on Wednesday , June 29 , the professor of 

 Arrangements for the Organ . by George C. Martin . 
 Nos . 6 and 7 . [ Novello , Ewer and Co 

 so much original organ music of merit be appear at 
 present that transcription be not so much need as in 
 former day , but they be still welcome when wisely choose 
 and skilfully execute , as those by Dr. Martin certainly 
 be . in the first of the present book we have the opening 
 Allegro from Beethoven ’s Quartet in G ( op . 18 , no . 2 ) , 2 
 charming movement , and the simple Minuet and Trio from 
 Schubert ’s sonata in e flat ( Op . 122 ) . the other contain 
 the beautiful ' ' Eia Mater " from Dvorak ’s Stabat Mater , 
 anda lovely romance attribute to Mozart . ' the musicianly 
 taste which mark each of these arrangement merit warm 
 acknowledgment 

 hymn - tune for well - know hymn . compose by 
 W. H.C. Malton , B.A. [ Oxford : Mowbray and Co 

 the reception he receive . his first song , ' my pretty ' Jane , " be 
 | give with rare sweetness and expression . his singing of " Auld lang 
 | syne " be a great treat , and the audience would fain have have he 
 repeat it . throughout ihe evening , however , he wisely set his face 
 against encore . Mr. Reeves also take part with Miss Siedle — the Diana 
 Vernon of the evening — in the two unacc > mpanted duet . Miss Siedle 
 have a light sweet voice of excellent quality , which be hear to much 
 advantage in " Robin Adair " and ' ' Cam ye by Athol . " Mr. Howard 
 take the title ré / e and Mrs. Howard the part of Helen Macgregor ; 
 Mr. Wyndham ( Captain Thornton ) , Mr. Mollison ( Bailie ) , Mr. Rain- 
 forth ( Dougal ) , Miss Kate Sherry ( Mattie ) , and Mr. Tom Walker 
 ( Major Galoraith ) , complete the cast 

 Fatkrrk.—A highly successful Students ’ Concert be give by Mr 
 J. Watson Lee in the Town Hall , which be quite fill , on Friday 
 evening , the sth ult . the priocipal item be Grand Duo for two 
 piano : , ' * Hommage A Handel " 1 Mosc s ) , splendidly play ; the 
 overture " son and Stranger " ( Mendelssohn ) and " Jubel " ’ ( Weber ) , 
 arrange for two piano ( 3 hand ) ; Beethoven 's 16th Sonata , C hopin ’s 
 Etude in G flat , and Bach ’s ' Praambulum " ’ piano solo , ali of which 
 be exceedir gly well play and thoroughly enjoy . Miss Clelland 
 and Mr. James Rule each contribute two song . the Rev. James 
 Aitchison presid i d give out the certificate to the successful 
 candidate who pass the recent examination in connection 
 with alliance College , London . 
 annual Festival of the North Craven Choir Union 
 at Gargrave on the 2nd ult . the Processional 

 faithful people , " set to an original tune by 
 nthem , Dr , Stainer ’s " let every soul be sub- 
 esley " chant service in F ; hymn before sermon , 
 " for all thy countless bounty , " word by the Dean of Wells , set to 
 | music by Mr. R. Watson ; and " holy offering " be sing during the 
 offertory . the sermon be preach by the Rev. Canon Champneys , 
 Vicar of Haslingden . the service be intone by the Rev. John 
 Trursfield , Vicar of S. Edmund 's , Leeds . Mr. F. Holt , Mus . Bac . , 
 Darwen , presice at the organ , and Mr. R. Watson , of Burnley , the 
 organise choirmaster , conduct 

 the MUSICAL TIMES.—Avavst 1 , 1837 . 497 

 Albert Lowe ) ; — xx . , to a chant by Beethoven ; Anthem , by Dr. Bekipe : Prince Coasort ’s te deur 

 let every soul be subject , " by Dr. Stainer ; andah which the ] " exaudiat te dom * ; Anthem , ' blessed be 

