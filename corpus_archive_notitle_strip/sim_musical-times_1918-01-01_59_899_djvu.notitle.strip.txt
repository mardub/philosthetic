That section of the nation which interests itself in 
good music—it is, of course, not a large one—seems 
to me to have pursued the even tenor of its way. 
| While the young men and women have been mostly 
| swallowed up by the all-devouring monster of war, 
| there has been a singular and notable increase in the 
|number of children learning the pianoforte. Actual 
|ear-training and musical understanding remain at 
their usual low ebb, being only cultivated to any 
extent in the musical institutions. These latter, 
| working under very difficult conditions, have naturally 
| suffered, but not to the extent that might have been 
|expected. The attendance at high-class concerts has 
| also been so good as to engender the suspicion that it 
| is more a matter of mere habit than of actual enjoy- 
ment. There has been a slight attempt on the part of 
| concert-givers to exclude German music, but no one 
| will consent to part witn Wagner, and Strauss had

been abandoned before the war, because he was too 
|expensive. The proportion of native instrumental 
|works in programmes has been precisely what it 
always was—almost negligible. Still are English 
| works like muffins—only to appear once and only 
when quite fresh, preferably half-baked. Will some 
one please contradict if | am mis-stating the facts? 
Excepting Sir Ed. ard Elgar’s glorious ‘Carillon,’ 
what new instrumen 11 work has had a second per- 
|formance in London during these three and-a-half 
| years? How many not-new works have been per- 
formed at all? And will anybody produce a shred of 
| evidence that the inclusion of an English instrumental 
| work ina programme is now any less of a damper to 
| the audience than it was formerly? Look down the 
| advertisement columns of concerts in the daily papers 
and tell me what you think. Furthermore has the 
| British public that buys good pianoforte music made 
| any attempt to interest itself in native productions? 
|The answer to this question is emphatically in the 
|negative. It buys its Beethoven and Chopin as 
heretofore and has shown admirable perseverance in 
| obtaining foreign modern works at great difficulty— 
such as transcriptions by d’Albert and Busoni (only 
attainable by roundabout means through neutral 
countries), but all the efforts of the Society of British

8 THE MUSICAL TIMES.—Janvary 1, 1918

III.-WHAT HAVE THE PUBLISHERS DONE

Here again I write with hesitating pen. Probably 
no professional musician has scantier relations 
with publishers than myself ; so that I view the matter 
quite dispassionately. One cannot get away from the 
fact that during the whole 19th century the London 
publishers—with the conspicuous exception of Messrs. 
Novello, who almost confined themselves to the pro- 
duction of choral music—treated good instrumental 
music as a negligible factor. Examination of the 
catalogues—the huge catalogues—of our other firms, 
great and small, will show little else but an amazing 
mass of triviality. And a large proportion of even 
this was from foreign pens; the native productions 
were mostly hack-work of the very lowest. For 
many years not a pianoforte or violin sonata or piece 
of chamber music was published at all, save at 
the composer’s expense. Personally I could name 
at least a dozen really talented young men from 
among my pupils who succeeded in getting the 
always unwilling ear of a publisher, only to be forced 
to write down and down, ever at a lower level, with 
the eternal ‘Oh, that is too good for our people!’ 
dinned into their ears. Is there a brighter side to 
this picture? If so I shall be very happy to have it 
presented to me. What concerns us at the present 
moment is, Has this matter improved since the War ? 
Have our English composers been encouraged to try 
and supplant—not Beethoven and Chopin, of course— 
Grieg, Sinding, Sibelius, Tchaikovsky, Poldini, 
Moszkowsky, and Debussy, to name only a few? 
Publishers have been said—I know not with what 
truth—to spend large sums in pushing some 
worthless song or dance-piece into popularity. I 
never heard of one (with the exception above-named 
that would accept a piece of decent music except 
under strong protest and when unable to refuse an 
influential client. It was this fact that forced a group 
of serious musicians in 1903 to found the Society of 
British Composers, the very existence of which should 
make our nation blush

To be just, all publishers have lately been obliged 
to suspend their operations almost entirely. But 
before this stringency occurred I failed to detect any 
patriotic attempt anywhere to cope with the future 
situation. Even in those musical quarters where 
English music was most desirable and desired 
namely, the public examinations—there was found 
enormous difficulty in obtaining it. And as to 
retail shops, the majority of them have never heard

String Qu

intet in G minor (No. 3) $§ 
First Movement from Sonata in G minor—Pianoforte Schumann 
~ { * Romance 
Songs 4 . Yebhussy 
a vibg t * Mandoline Debussy 
Caprice (MS.)—Pianoforte .. ‘a - ‘* Eva Pain 
First Movement and Scherzo from Pianoforte Quartet : 
in E flat ee : of .. Mackenzie 
. . . * Water-lily ) Arthur Laurence Sandford 
oO ynes (MSS. 7 : P t vEnUr L-AUurEe! a 
I gs (M 2% Dream Maid '; (Sir Michael Costa Scholar) 
Barcarolle— Pianoforte ‘ Chopin 
‘Queen Mab ) 
fords by Thomas d . . 
Musical Re (Wo a I iomas H ) Mackenzie 
The Confession 
\(Words by Thomas Ingoldsby.) / 
Scena and Due ‘Tu la sorte dell’ armi’ (4 ida) Verdi 
( ourante - . . 
from Suite in G—Violoncello Bach 
Minuettes 1. & 11, ¢ ‘TOM Suite once 
Scherz Two Pianofortes Saint-Saéns 
November 21, 1917. 
Tem: 1 Variazioni, from Trio for Pianoforte, Violin, and 
loncello (Op. 50) - Tchathousk 
First Movement from Sonata in C minor (Op. 111) 
Pianoforte - on és ne Beethoven 
Songs from ‘ The Life of a Rose Lisa Lehmann 
* Unfolding. * The Storm.’ 
* The Bee. ‘The Farewell 
‘Rosa Resurget. 
Rondino in E major—Violir Vieuxrtemps 
Sonata in A—Pianoforte - Mosar? 
Le Jardin Mouillé— Harp acques de la Présii 
Songs Te souviens-tt Gedari 
La Paquerette : 
Andante Espressivo {}(MSS.) from Pianoforte Sonata in 
Allegro con Fuoco 4 E flat minor . Hazel Perman 
(Student

Quartet in 
Violoncello Mosar?¢ 
Rachmanine: 
Grechanino 
HW eber-Corder

December 11, 1917

llegr from Concerto in E flat (Op. ) (First 
Movement)— Pianoforte Beethoven 
Duet * Fu la sorte dell’ armi’ (4 fda) lerdi 
Concerto (Op. 35) (First Movement)— Violin 7 chatkousky 
Air * Let the bright Seraphim * (Samsen Handel 
Concert V at (Op. 71)}-—Pianoforte ° Stanford 
« (MS.) aa I Koo rthur L. Sandford 
(Sir Michael Costa Scholar) 
( erto in E (Op. 71) (First Movement)— Violin I ieuxrtemps

Doubting

trinciple of construction. To give this line to a 
uman voice, especially to a voice unimpeded by 
ords, is to intensify it to its utmost emotional

new, the singers will always manage to follow them 
eventually. Mozart and Wagner were both con- 
sidered unsingable in their day. Beethoven is still 
considered unsingable ; but people sing him in spite 
of that. We might even get as far as an opera 
without words. Plays without words are not unknown, 
and musical plays without words are an established 
convention. An opera without words would solve 
many difficulties, and might eventually lead us to see 
that the principles of opera are not identical with 
those of drama. An opera should deal with just 
those emotions which words cannot express. A 
wordless opera, instead of borrowing a plot with a 
maximum of incident, like ‘ Tosca’ or ‘ Fedora,’ would 
have to be based on some story that everybody knows, 
such as the legend of Orpheus, so that there should be 
no need of tiresome explanations on the stage. And 
we should then get rid of the m7//-punsch o wisky ? 
business in opera (¢ un facile vangelo, as Consul 
Sharpless says himself), the unnecessary realistic 
touch which merely serves to make all opera 
appear the more unnatural and absurd

Occasional Wotes

Choir Festival services were held at the Halifax Place 
Chapel, Nottingham, on November 18. The anthems were 
*The Lord hath done great things for us’ (West) and 
*Whoso dwelleth in the secret place of the Most High’ 
(Martin). In the afternoon the choir gave Barnby’s ‘ The 
Lord is King.’ The soloists were Miss Emmie Warner, 
Madame Ethel Parkin, Mr. J. Franklin Pearson, and 
Mr. Joseph Asher. Mr. E. M. Barber was the conductor, 
and Driver C. E. B. Dobson (R.F.A.) the organist

At the Memorial Service for the late General Sir Stanley 
Maude, held in St. Paul’s Cathedral on December 4, the 
band of the Coldstream Guards, under Major Mackenzie 
Rogan, played Beethoven’s Funeral March, ‘O rest in the 
Lord’ (cornet solo), Handel’s ‘ Largo,’ Guilmant’s ‘ Marche 
Funébre and ‘Chant Seraphique,’ Chopin’s Funeral March, 
and Handel's ‘Dead March.’ No British music at this 
solemn national tribute

ITALS

THE ATTITUDE OF BRAHMS TO ENGLAND

Lieut. S. L. Hordle, writing from France upon Sir George 
Henschel’s letter on the attitude of Brahms to this country, 
says that to musicians it is only the character of a composer 
as a musician that matters and not his political or 
nationalistic bias. The works of Beethoven, Mendelssohn, and 
others are the inheritance of the world, not merely of a 
nation. Agreed

THE ALLEGED STUPIDITY OF SINGERS. 
TO THE EDITOR OF ‘THE MUSICAL TIMES

rHE LONDON STRING QUARTET. 
NEW SCHEME OF CONCERTS AT QUEEN’S HALI

This famous Quartet, greatly encouraged by the success 
that has attended its concerts at ./olian Hall, has boldly 
decided to give a series of chamber concerts in the much 
larger arena of Queen’s Hall, and has issued an announce- 
ment in which it is stated that its members are convinced 
there are many people who have no idea there is such a 
wonderful Song-Cycle as ‘On Wenlock Edge’ (Vaughan 
Williams), or such light chamber music as ‘ Molly on the 
Shore ’ (Grainger), by British composers. Also there are vast 
numbers who have never heard the Pianoforte Quartets and 
Quintets of Schumann, Brahms, Xc., or the String Quartets 
of Beethoven, Mozart, Haydn, &c. Therefore the L.S.Q. is 
giving four special concerts at Queen’s Hall on Saturday, 
December 22, at three, and Wednesdays, January 16, 23, 
and 30, at three, thereby hoping to gain a great many 
converts to chamber music, so that this class of concert can 
be considered a permanent institution of musical London 
life. The L.S.Q. will adhere to its principle of including 
a British work at each concert

Miss Gwynne Kimpton gave the preliminary performance 
of the second series of her Orchestral War Concerts at 
Caxton Hall. The programme included Saint-Saéns’s 
Pianoforte Concerto played by Miss Myra Hess

characterised by religious fervour and deeply felt earnestness. 
The magnificent singing of the choir in Bach’s Cantata, 
‘O Light Everlasting,’ for chorus and soprano, contralto, 
and bass soloists (Miss Ada Forrest, Miss Helen Anderton, 
and Mr. Norman Allin), could hardly have been excelled in 
wealth of tone-colour and perfect ensemble. Balfour 
Gardiner’s nautical ballad, ‘ News from Whydah,’ for chorus 
and orchestra, a short but breezy and thoroughly effective 
little work, was given with animation. Mr. C. W. Perkins 
was the organist. The orchestra played Grieg’s charming 
‘Lyric Suite

Madame Minadieu’s second Matinée Musicale, held at the 
Grand Hotel on December 1, was a pleasurable experience. 
It may be hoped that the success of these gatherings will go 
far towards the permanent establishment of such functions. 
The Brodsky Quartet, with Dr. Brodsky as leader, played, 
and Miss Patuffa Kennedy-Fraser sang ‘Songs of the 
Hebrides’ most sympathetically, to the accompaniment of 
the Celtic harp. The Quartets were Beethoven’s in B fiat, 
Op. 18, No. 6, and Ottokar Novacek’s in C major, Op. 13. 
The composer of the latter work was a Czech, born in 1867, 
who died in New York in 1898. Dr. Brodsky played Bach’s 
Chaconne with great skill and nobility of expression

At the concert of the Birmingham Choral Union, on 
December 1, Elgar’s ‘ Banner of St. George’ was revived, 
and was excellently presented under Mr. Richard Wassell. 
The soprano solos were sung by Miss Lilian Green. Miss 
Marjorie Sotham, the pianist, played Grieg’s Concerto. 
This was the third performance of Grieg’s Concerto this 
ason, it having been already performed by Mr. William 
Murdoch and by M. de Greef. The orchestra was heard to 
advantage in Jarnefeldt’s ‘ Preeludium’ and the March from 
‘Tannhauser

The Birmingham Symphony Orchestra, which is still in 
being, decided to give four popular Saturday night concerts 
as usual, the rank and file having been augmented to over 
fifty of our best-known local instrumentalists, the only lady 
performer being the harpist. Its president, Mr. Laurits 
Blakstad, an enthusiastic musical amateur, does everything 
in his power to keep up the artistic standard of these 
concerts, in spite of the many counter attractions in this 
direction. The first concert took place in the Town Hall on 
December 15, and certainly proved a success. Mr. Wymark 
Stratton is the new conductor. He has soon proved himself 
to be the right man inthe right place. A pleasing item of the 
programme was Sterndale Bennett's Overture ‘ The Naiades,’ 
and Mr. Julian Clifford, the solo pianist, gave Liszt’s 
Pianoforte Concerto No. 1, in E flat, in fine style. Mr. 
Robert Parker, the baritone, made his débiit here on the 
concert-platform, and achieved conspicuous success

The fourth and fifth Symphony Concerts by the New 
Birmingham Orchestra were given at the Town Hall on 
November 28 and December 12 respectively, conducted by 
Sir Thomas Beecham and Sir Henry Wood, the vocalists 
being Mr. Robert Radford and Madame Jeanne Brola. 
The programmes included a good deal of Russian music, but 
Mozart and Beethoven had also their proper places assigned, 
as well as Bach and Handel. The Orchestra is making 
steady progress under the various conductors, but a good 
deal has yet to be accomplished

BELFAST

1, 1918

n) and Mr. William Murdoch (pianoforte), who 
uN Beethoven’s ‘ Kreutzer’ Sonata and works by 
ubert and Chopin, as well as two graceful compositions 
by Mr. Sammons. On November 8, the Winifred 
Burnett Quartet, comprising four accomplished local lady- 
musicians, gave a chamber concert in aid of War charities. 
The programme contained Dvor:ik’s String Quartet in E flat 
and the same composer’s Pianoforte Quartet also in E fiat, 
with Dr. Lawrence Walker taking the pianoforte part. 
Miss Florence Nixon contributed choice selection of

a 
A ert at at popular prices was given on November 24 by 
the - Belfas ast Symp hony Orchestra, trained and conducted by 
Mr. E, ¢ sodfrey. Brown. Miss Winifred Burnett played the 
Introduction and Rondo Capriccioso of Saint-Sacns’s Violin 
ntributed a clarinet solo

Concerto, and Mr. Edward Harris c 
by Edward German. Miss Lily Jackson was the vocalist. 
The orchestral music performed was Beethoven’s Symphony 
No. 8, Stanford’s Overture to ‘Shamus O’Brien,’ Borodin’s 
* Prince al march, Dvorak’s Slavonic Dance in G minor, 
and ian lel’s Largo in G, arranged by Hellmesberger for 
p, organ, and strings. A large audience greatly

is choice selection of music. 
second Philharmonic concert, on December 7, 
2% more ambitious programme, the excellent 
r of which reflected much credit on the 
Society’s at ctor. It comprised Coleridge-Taylor’s 
autiful Rh apsody, ‘Kubla Khan,’ for the solo part of 
which Miss Dilys Jones was excellently fitted. The 
Khapsody was admirably a vanied by the Orchestra, 
which also played four movements Borodin’s Symphony 
1 B minor, and the same composer’s March from ‘ Prince 
Igor.’ especial feature of the concert was the 
first a 1 Ireland of the great  violoncellist, 
Madame ria, who was heard in Saint-Saéns’s Concerto 
\ minor, accompanied by the Orchestra, and Bach's 
te in G minor, unaccompanied. Criticism of such playing 
would be an impertinence, l ly no greater artist 
ever delighted the lo in Belfast. Her 
nique, tone, phrasit tation are, indeed

basses

The Clifton Chamber Concerts are meeting with increased 
ipport in this their sixteenth season, and the quintet— 
Madam Marie Faulkner Adolphi (1st violin), Miss Hilda 
Barr (2nd violin), Mr. Alfred Best (viola), Mr. Percy Lewis 
(cello), and Mr. Herbert Parsons (pianoforte)—has given 
some remarkably fine interpretations of classical and modern 
compositions. Among the former may be mentioned 
Beethoven’s String (Quartet in C, and Brahms’s ()uintet in F 
minor, while the modern works included Glazounov’s 
(Quartet in D minor, and (Quartets by Frank Bridge

Dr. Basil Harwood, presiding at the annual meeting of 
the Bristol Musical Club, stated that during the past twelve 
months they had had opportunities of hearing no fewer than 
seventeen string quartets, including well-known works of the 
great classical masters and less familiar ones by modern 
composers. English works had not been neglected, indeed 
me could not fail to be struck by the large increase in the 
number of native composers represented. Speaking of one 
of these, Coleridge-Taylor’s ‘ Fantasiestiick,’ Dr. Harwood 
referred to the curious habit of some English composers in 
the past of giving foreign titles to their pieces, a halit 
which lingered on even in the 20th century. By foreign 
titles he did not mean such ordinary names as Scherzo, 
Capriccio, Pastorale, and so on, which by common usage had 
become part of the musician’s everyday language, but those 
more descriptive titles often suggestive of some definite 
mood or object. He gave instances of German and French 
titles of pieces when, he said, plain English would have done 
as well. Such affectation survived as a relic of the wretched 
tradition that the foreign musician’s work must necessarily 
¢ better than that of the Englishman, a tradition which had 
successively placed much of native music under the dominating 
influence of Handel, Mendelssohn, Brahms, and Wagner, 
and had coloured many a composer’s work and prevented 
his individuality from developing freely. Let us in every 
way seek to free our music from all dependence on things 
foreign, and so learn to be self- supporting, even as we were 
now painfully learning to be self-supporting in many common 
necessaries of life. During the business part of the pro- 
ceedings, by a new rule the Club honoured three of the 
original performing members—Mr. Hubert W. Hunt, Mr

The third University lecture, on November 21, dealt with 
Bach’s ‘Goldberg Variations.’ The Historical Concert on 
November 21 included these Variations, and Prof. Tovey 
certainly found in them a medium perfectly adapted to his

or are likely to be heard again. Beethoven’s Sonata, 
Op. 110, and Mozart’s A minor Sonata, completed the 
programme

The fourth lecture, on December 5, dealt with some laws

of orchestral esthetics independent of contemporary discovery, 
The fourth Concert (pianoforte recital) consisted of Handel’s 
third Suite, in D minor, Beethoven’s Sonata, Op. 106, and 
two sets of Variations, Op. 35, by Brahms. 
The Reid Orchestra gave concerts on December 1 and 15. 
The attendance was good, and the programmes were satisfac- 
torily performed in view of the nature of the band. Elgar's 
* Enigma Variations,’ Brahms’s ‘ Symphony No. 4, the aria, 
* Air des adieux,’ from Tchaikovsky’s ‘Jeanne d’Arc’ (sung 
by Miss Olga Haley), Haydn’s E flat Symphony, Wagner's 
‘Faust’ Overture, and Beethoven’s C minor Symphony, 
were amongst the items. Mr. Maurice D’Oisley also sang

GLASGOW

The last of the chamber concerts at the Royal Institute of 
the Fine Arts took place on November 29, when the 
programme included Rubinstein’s Violin Sonata in G major 
and Sinding’s Quintet in E minor, the latter being finely 
interpreted by the Fellowes Quartet and Mr. Philip 
Halstead as pianist. On the same evening Mrs. Kennedy- 
Fraser and her daughter, Miss Patufia Kennedy-Fraser, 
gave one of their charming recitals of Hebridean songs. On 
December 1 the Glasgow Abstainers’ Union gave a ballad 
concert at which Miss Jean Gibson, Mr. Herbert Brown, and 
Mr. Henry Brearley were the principal vocalists. An 
interesting recital of Church music was given on December § 
at Westbourne Church by the augmented choirs of 
Westbourne Church and the University. Mr. A. M. 
Henderson conducted, and played two groups of organ solos. 
The first of the three Fellowes Quartet concerts on 
December 6 was notable for an excellent performance of 
Beethoven’s Septet, a work which has not been heard here 
for many years. The players were Mr. Fellowes (violin), 
Miss Buchanan (viola), Mr. Templeton (’cello), Mr. Gaitley 
(double-bass), Mr. Beaumont (clarinet), Mr. Woods 
(bassoon), and Mr. Hunt (horn

A circumstance which is probably unique in the history of 
concert-giving in Glasgow was the Scottish Concert by the 
Glasgow Orpheus Choir (Mr. Hugh S. Roberton, conductor), 
on December 11, which, owing to the enormous public 
demand for tickets, had to be repeated on December 
12 and 13, on all three occasions St. Andrew’s Hall being 
filled to overflowing—truly a remarkable tribute to unaccom- 
panied choral-singing of the highest type! The Choir sang 
(entirely from memory) no fewer than sixteen original 
settings or arrangements of Scottish songs, of which the

lace of honour must be accorded to Granville Bantock’s 
‘Death of Morar,’ a composition that makes the highest 
demands on the technical and interpretative powers of both 
conductor and choir. Members of the Choir contributed 
solo numbers, chiefly from Mrs. Kennedy-Fraser’s collection 
of Hebridean songs

The second concert of the Choral and Orchestral Union’s 
series on December 15 took the form of a Chamber Concert 
by the London String Quartet, with Mr. Wilfrid Senior 
as pianist and Miss Carrie Tubb as vocalist. The concerted 
numbers—Beethoven’s Quartet for strings in D (Op. 18), 
No. 3, Mr. Waldo Warner’s Folk-song Phantasy in G minor 
(the Cobbett Competition first-prize composition), and 
Schumann’s Quintet for pianoforte and string quartet—were 
strongly contrasted, and of the three, the performance of the 
Quintet, in which Mr. Wilfred Senior excelled in the piano- 
forte part, calls for special mention. Miss Carrie Tubb’s 
singing of three groups of songs was particularly acceptable. 
Miss Ailie Cullen ably discharged the duties of pianoforte 
accompanist. Before the commencement of the concert 
Mr. Herbert Walton played on the organ the Dead March 
in ‘Saul’ as a tribute to the memory of the late Mr. John 
Wallace, manager of the Choral and Orchestral Union, 
whose death is referred to in the Obituary column, e 20

Amongst other events of the month were the orchestral 
concerts by the Picture House Orchestra, under 
Mr. Cinganelli, three weeks’ performances of their repertor] 
by the D’Oyly Carte Opera Company, and seve

The closing month of the year brought some memorable 
experiences from the Brodsky and Catterall (Quartet parties

It can always be Brodsky’s proud boast that he and| 
his colleagues definitely established chamber-work as} 
a regular feature of Manchester’s musical life, and to-day | 
no more authoritative interpretations of the later Quartets} 
of Beethoven are to be heard anywhere. In_ this 
sense it may truly be said that Joachim’s mantle has 
descended upon him, and to hear the five strophes of the 
Molto adagio of the A minor Quartet given as it was on 
December 4 to an audience of business men and women 
during a market-day luncheon—truly the highest compliment 
that could be paid to such a gathering—was one of those 
treasured experiences which are never effaced from the 
memory. The Catterall Quartet habitually treads more 
unfamiliar paths than its Brodsky associates. These players 
have now attained the sort of exquisitely poised ensemble 
first revealed to some of us by the great Paris and Brussels} 
Quartets. When the Catterall group plays such works as| 
Dohnanyi’s D flat, or the F major of Ravel, the sensation j 
is like that experienced by noting the effect of light playing| 
upon the surface of a Bernard Moore vase or a Pilkington} 
lustre-surface bowl ; and the nearer you sit to the players the} 
more complete is the delicacy of the experience, because of} 
| the exceptional fastidiousness of their performance. j

OXFORD

Quite the most impressive concert that has taken place at 
Leeds during December was the second of the two free 
concerts given in the Town Hall on Sunday afternoons by 
the two principal choral Societies of the town, the Phil- 
harmonic and Choral Union, for the delectation of soldiers

In spite of great difficulties of transport, the ground floor 
| was on December 2 practically filled by soldiers, mostly 
| wounded, who had a better concert for nothing than one is 
jin the habit of hearing at considerable cost. It was the 
}turn of Dr. Bairstow, the conductor of the Philharmonic 
Society, and he secured from the great choir some fine 
performances. Part 1 of Hlaydn’s ‘Creation’ was of course 
|} an easy task, and the mettle, as well as the size, of the choir 
was even better realised from the very inspiring performance 
of Parry’s ‘ Blest Pair of Sirens,’ which formed a fitting 
culmination to a series of brilliant choral effects. With the 
| co-operation of the Leeds Music in War-time Committee, 
| excellent principals in Miss Elsie Suddaby, Mr. H. Brearley, 
|and Mr. H. Parker, together with an efficient orchestra, 
had been engaged, so that there were no weak points in the 
}ensemble. On December 1 the Saturday Orchestral Concert 
| drew a large crowd to the Town Hall, when Tchaikovsky’s 
| ‘ Pathetic’ Symphony—of which Mr. Julian Clifford gave 
a brilliant and effective performance—and the singing of 
Miss Agnes Nicholls were the obvious attractions. The rest 
of the programme was made up of familiar orchestral pieces. 
The Leeds New Choral Society, which has a most enthu- 
siastic director in Mr. H. M. Turton, gave ‘Judas 
Maccabeeus’ on December 5, with somewhat maimed 
rites, since the place of the orchestra had to be taken by the 
Town Hall organ, which is far too clumsy a machine for work 
of such delicacy. Mr. W. Hartley as organist, and Mr. 
H. E. Boot at the pianoforte in the recitatives, did all that 
was possible under the circumstances. The principals were 
Miss Violet Allen and Miss Nancy Howe, Messrs. Tom Child 
and W. Hayle. Two recitals have been given in aid of the 
Music in Wa#-time Fund, which can certainly lay claim to 
having spent all the money entrusted to it to the greatest 
advantage, giving enjoyment to a multitude of soldiers and 
employment to many professional musicians. At St. Chad’s 
Church, Far Headingley, Mr. Percy Richardson gave a very 
interesting organ recital on November 258, his programme 
including two of Parry’s fine Chorale Preludes, pieces by 
Vierne and Mailly, and transcriptions of Wagner and Elgar. 
On December 10 Miss Kathleen Frise Smith gave evidence 
of refined powers in a pianoforte recital that presented several 
unfamiliar things, especially in pieces by Palmgren and 
Scriabin. A concert-party organized by Mr. H. Brearley 
gave on November 28 a concert which deserves mention, since 
it aflorded a most favourable impression of native talent. The 
vocalists were Miss Elsie Suddaby, Miss Jean McGregor, 
Messrs. W. Hudson and H. Parker, with songs at the piano- 
forte by Mrs. Norman Strafford, and some brilliant violin 
solos by Mr. Bensley Ghent. The University Recital on 
December 4 was given by Mr. P. Richardson, whose pro- 
gramme was confined to Biilow’s Trinity, Bach, Beethoven, 
and Brahms, whose music he played with marked clearness 
of style. The Bohemian Concerts, which always attract the 
most appreciative audiences in Leeds, if not in Yorkshire, 
were continued on December 12, when String quintets by 
3eethoven (in C, Op. 29) and Brahms (in F, Op. 88) were 
played under Mr. Alex. Cohen’s leadership with a spirit of 
understanding that made them very enjoyable, while three of 
Speaight’s little Shakespearean sketches for string quartet 
provided a pleasing foil

OTHER TOWNS

own. Works by Rachmaninov and Franck were also 
included in the programme. 
The Hull Harmonic Society, conducted by Mr. Walter

Porter, introduced to the town Elgar's ‘ lourth of August,’ 
and very wisely repeated it at the close of the programme 
for the benefit of those who wanted to realise its beauties 
thoroughly. Beethoven’s second Symphony was nicely 
played, and Miss Muriel Weatherhead was the vocalist. On 
December S$ one of Mr. Janssen’s Subscription Concerts 
introduced a native work more than common importance 
in Frank Bridge’s Violoncello Sonata in D minor, of which 
Messrs. Arnold Trowell and Willia Murdoch gave a 
powerful and satisfying performance. The music has great 
qualities—it grips one, but one inclines to doubt whether it 
would not be much more effective were greater use made of 
contrasts of mood. Miss Kate Campion’s reading of Verdi’s 
‘Willow Song,’ from ‘Othello,’ showed her to be an 
accomplished dramatic artist

HALL. 
ORCHESTRA

A noteworthy feature of Mr. Max Mossel’s violin 
recital on November 21 was the first performance 
of a Sonata in B minor by Mr. J. D. Davis, which was 
excellently interpreted by the concert-giver and M. Arthur 
de Greef. The work is in four movements, the third being 
practically an introduction to the fourth, and is of quite a 
modern calibre, and at the same time interesting and lyrical 
as regards both matter and form. f

The London String (Quartet, 
Scontrino’s Qvartet in A minor. Vaughan Williams’! 
‘On Wenlock Edge,’ with Mr. Gervase Elwes as the 
singer, was another feature, and the third and last item 
was Beethoven's lucid (Juartet in G, Op. 18, No. 2. On 
November 30 Beethoven’s E minor ()uartet, Op. 59, No. 2, was} 
played, and a beautiful performance was given of Mozart's} 
Clarinet Quintet in A, with Mr. Charles Draper as clarinettist | 
The British item was E ugene Goossens’s Rhapsody for ’cell 
and pianoforte, Op. 13, which was admirably played by| 
Mr. Warwick Evans and Miss Ethel Hobday. At the last! 
concert of the series Brahms’s Quartet in A minor, Op. 51, 
No. 2, was the first item. A ‘first-time’ piece was 4 
(Quartet for strings, entitled ‘ Fancies,’ by Joseph Speaight 
It is a bright and flowing composition, and created a veryj 
good impression. We daresay it will be heard again, for it] 
certainly deserves to be. Chausson’s Concerto for violial 
and pianoforte, with string quartet, was finely played by 
Daisy Kennedy and her husband, Benno Moiseiwitsch. j

The bold scheme of the L.S.Q. to transfer their concert} 
to Queen’s Hall is referred to on page 27

With reference to this subject, which was fully discussed in 
our November and December issues, we have to say that we 
have received permission from the War Office to print their 
scheme. We defer publication until our next number

CAMBRIDGE.—On December 8 the University Musical 
Society gave its second concert of the term. Miss Ruby 
Holland played the Scriabin Concerto in F sharp minor and 
the pianoforte part of the Choral Fantasia (Op. 40) of 
Beethoven. The orchestra gave the Overture and Inci 
dental Music to ‘ Rosamunde,’ by Schubert, and some 
dances by Mozart. The choral work for Double Chorus 
with pianoforte accompaniment composed by Mr. R. T. 
Woodman, and published last year by Messrs. Novello, 
was a notable item. The words are by W. E. Henley

JOHANNESBURG.—The town organist, Mr. John Connell, 
keeps musical matters going. He gives lunch-hour recitals 
on the organ as well as othersat normaltimes. He conducts 
the Philharmonic Society, which recently gave a performance 
of Gounod’s ‘ Faust.’ There was an orchestra, and one of 
the successful items was Balfour Gardiner's ‘ Shepherd 
Fennel’s Dance

