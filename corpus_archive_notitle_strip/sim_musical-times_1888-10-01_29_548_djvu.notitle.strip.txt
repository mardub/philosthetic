ELIJAH (Men

Tvuespay Morninc. lelssohn). 
Tuespay Eventnc.—IPHIGENIA IN basse Bett Se and 
Chorus (Gluck); Festival Overture in C, Op. 124 (Beethoven); Con

ann); Poéme 
y Nos. 9 and 
‘. (itendaleeaies, and

HURSDAY EVENING GRAND k VE NING CONCERT —in

ag: Pastoral Symphony (Beethoven); Triume (Study for Or

Tristan 
Orchestral

MISS MARION STEAD (Soprano). 
For Concerts, Oratorios, &c., address, care of W. S. Child, Esq., 
Hon. Sec., Musical Society, York. Press Notices

MADAME CLARA WEST (Soprano), 
MISS LOTTIE WEST (Contralto), 
eee) Beethoven Villa, King Edward Road, Hackney. 
MRS. CHRISTIAN WILLIAMS (Soprano Vocalist) 
(Pupil of Signor Randegger

For Oratorio, English and Scottish Ballad Concerts, address, 
27, Falkner Street, Liverpool

and nearly all the more representative works of 
modern composers. Moreover, the director's policy 
appears to satisfy his public. It should not be for- 
gotten that Mr. Chappell has, during more than one 
season, plied his patrons with compositions of the 
modern school. They do not seem to have thanked 
him for it, and we must look upon his present trust 
in the classical masters as indicating a prevailing 
taste among amateurs of chamber music. With 
regard to artists, the director is fortunate in the con- 
tinued life and activity of his principal executants. 
Madame Schumann, Madame Norman-Néruda, Miss 
Zimmermann, Miss Fanny Davies, Messrs. Joachim, 
Piatti, Straus, Ries, Hollander, Gibson, Howell, and 
the rest to whose presence we are accustomed, are all 
available. Doubtless we shall hear them all again, 
and have further reason to be satisfied with Mr. 
Chappell’s management in this respect

The Bach Choir intend giving two Concerts in the 
course of its next season. Practice resumes on 
November 6, and on March 5 a very interesting 
Concert will take place, the programme being made 
up exclusively of works by the Leipzig Cantor. This is 
as it should be. A Bach Choir which does not devote 
the major part of its labours to Bach’s music has no 
special right to the name it bears. At the Concert 
just referred to will be performed the Motett ‘‘Singet 
dem Herrn,” together with the Cantatas “ Halt im 
Gedichtniss’’ and ** Wachet auf.’ These, we be- 
lieve, have not been heard in London, but the second 
was once given by the Cambridge University Musical 
Society, under the direction of Professor Stanford. 
At the same Concert Mr. Joachim wiil play Bach's 
Violin Concerto in A minor and one of the master’s 
Violin Sonatas. The second performance is fixed to 
take place on May 4, when Beethoven's Mass in D is 
to constitute the programme. On this occasion the 
French diapason normal will be used. It is intended 
to give both Concerts in St. James’s Hall

Although at the moment of writing the matter is 
not quite settled, there is good reason to believe that 
the second Sacred Harmonic Society has practically 
gone the way of the first, after a much shorter and 
less glorious existence. It is no part of our present 
purpose to investigate the reasons why the public 
have not supported this Society in a measure sufii- 
cient to ensure its continued life. We may say, 
however, that few amateurs will fecl surprise when 
the managers make a formal announcement of their 
dissolution, and the disappearance of a name which 
has become historic

went considerable scrutiny at the time, and was objected

to on the score of its foreign element—the cause, as some 
held, of a certain weakness of tone in the violins. Possibly 
some of the men might have been exchanged for others 
with advantage in this respect, but let us, nevertheless, be 
just. My own opinion is that an orchestra able to 
play Beethoven’s Fifth Symphony in the well-nigh 
absolutely perfect manner of its performance on August

3 is good enough for anything. That splendid 
effort put all cavillers to silence on the score of 
efficiency. The solo vocalists’ names speak for them- 
selves: Mesdames Albani, Ambler, Anna Williams

be used with judgment and the skill acquired by practice. 
The performance, conducted by the composer in person, 
was very successful. Both the band and chorus found 
their sympathies excited by the music, and as much may 
be said, with even greater emphasis, of the soloists, Madame 
Albani, Madame Trebelli, and Mr. Lloyd. The first 
and last of these could not have been improved upon, 
justice being done by them alike to the music and to the 
character represented. I should add that the Processional 
March proved to be by no means the least acceptable number 
in the work. Such a thing is difficult to write without 
falling into conventionality, but Dr. Bridge achieved it. 
At the close of the performance audience and executants 
joined in lusty applause of the composer-conductor, who 
right well deserved the tribute. I may pass lightly over the 
remainder of Thursday evening’s programme. Miss 
Fanny Davies gave her usual brilliantly effective reading ot 
Schumann’s Pianoforte Concerto, and obtained as much 
honour in her “own country” as heart could desire. 
Madame Albani sang ‘Softly sighs,’ and Mr. Lloyd the 
‘“ Preislied,”” of which he seems never to tire. The rest 
was purely orchestral, and consisted of Grieg’s ‘ Suite 
in Old Style”? (conducted by the composer), the Pre- 
lude to Act III. of ‘‘ Die Meistersinger,” and Brahms’s 
‘“« Academische ” Overture. For these Dr. Richter and his 
men were well able to answer

The last morning Concert had a somewhat remarkable 
programme; Beethoven’s Symphony in C minor being 
sandwiched between a typical piece by Bach and one not 
less representative by Berlioz. As it turned out, the work 
set to keep these very pronounced opposites apart admirably 
fulfilled its purpose, because performed in such a manner 
that the audience found their impressions of Bach obliterated 
by a new and lively feeling. I have already characterised 
the playing of the ‘‘C minor” as quite exceptional. In 
truth it was extraordinarily perfect. Conductor and orches- 
tra were on their mettle in face of a splendid opportunity ; 
they knew their theme and were conscious of its power, 
they had confidence in each other, and in the candour of 
the audience. All things, therefore, conspired to a good 
result, and the performance of the Symphony will long be 
remembered as an effort wholly out of the common order. 
It was heard with profound attention and applauded 
vehemently. Bach’s Magnificat opened the Concert— 
perhaps I should say that this was done by the Magnificat of 
Sebastian Bach and Mr. Franz, since the last-named gentle- 
man has been good enough to fill up the orchestral score. The 
work is too familiar for discussion here, and those who 
know it are prepared to be told that, while the choruses 
made a deep impression, the solos (sung by Miss Anna 
Williams, Madame Trebelli, Mr. Banks, and Mr. Foli) 
failed to interest the audience, if, indeed, the effect of them 
was not weariness. I am bound to add that the artists 
rather helped to this result than otherwise. They felt no 
sympathy with their task—what wonder !—and allowed 
the fact to appear. It was different with the choral 
numbers, which, as everybody knows, are among the 
noblest of their kind. Though not performed in a manner 
absolutely beyond reproach, these woke the audience up, 
and excited a lively feeling of admiration. The remark 
applies more especially to the magnificent ‘‘ Omnes gener- 
ationes”’ and the ‘Gloria Patri.”” Opening in the severe 
school of Bach, the Concert closed in the licentious com- 
panionship of Berlioz, whose ‘“ Mass for the Dead” was the 
“sensation” of the week. The public had anticipated this 
with some interest. They had heard of the extra brass 
bands, the formidable array of drums, with all the rest of 
it, and they were naturally curious about the result. Some, 
on experience, found the result too much for them, As the 
brass bands blew their loudest and the drums thundered 
like the artillery of heaven, a physical disturbance became 
inevitable among sensitive organisations. A few persons, 
I am told, had to seek safety in flight, and I know that 
on every hand signs of great nervous commotion 
were visible. Is this to be set down as a triumph of music, 
or only as of noise? The reader will answer the question 
as he pleases. It is but justice to remember that Berlioz 
composed his ‘‘ Requiem ” for a purely ceremonial occasion, 
having little connection with real feeling, and inviting mere 
display. He would have written very differently, we may 
assume, had the work been designed for use under the con

XUM

ofa large audience, most of the artists so often named 
above taking part in it; the only important exception being 
Mr. Lloyd. Regarding the performance I cannot speak 
from personal observation; but the ‘ sacred oratorio may 
be left to take care of itself with some confidence

There remains to notice only a miscellaneous Concert | 
given in the Shire Hall on Thursday evening, and the final 
chamber Concert in the same place on the following day. 
These may be dismissed very briefly, because, though | 
welcome features in the general scheme, they had no 
Festival importance whatever. The first programme con- 
tained the Overtures to ‘ Euryanthe,” * A Midsummer 
Night's Dream,” and ‘The Merry Wives of Windsor,” as 
well as a pretty Minuet and Gavotte for strings, by Mr. } 
C, L. Williams, and a number of songs, relieved by mad- 
tigals, &c., contributed by the Leeds contingent of the 
choir. All went smoothly and greatly pleased a numerous 
audience. At the chamber Concert were performed, by 
Messrs. Carrodus, B. M. Carrodus, Blagrove, and C. Ould, 
Beethoven’s Quartet in F (Op. 18) and the No. 1 of Men- 
delssohn's (Op. 44). A crowded audience heard these works 
with an appreciation delightful to note. So with Bach’s 
Chaconne in D minor, for violin alone, wonderfully played 
by Mr. Carrodus, and vociferously applauded. Songs by 
Misses Ambler and Wilson, and part-songs by the Leeds 
choir made up the balance of the programme, and rounded 
off a most interesting Concert

It is to be feared that the Stewards of the Festival will 
have a deficit to make up, but that is their raison d’étre, and 
they will not complain. The collections for the charity 
amounted to a little over £800, which will doubtless be in- 
Creased to the average amount of £1,090

and reassuring construction of the building with a view to 
rapid exit. If broad passages between the rows, un- 
obstructed by extemporised seats ; large double doors on the 
swing; and simplicity of staircase arrangement cannot 
ensure safety in this respect, which has yet to be shown, at 
all events a useful moral effect is produced, and the 
tendency to panic lessened

Amateurs of chamber music will be glad to learn that the 
meetings of the Ton-Kinstler-Verein commence with a 
series of Beethoven Quartets on November 2

Herr Trenkler’s admirable military orchestra, with an 
admixture of strings, takes possession of the Belvedere on 
the Brihl Terrace, so dear to English passers-by, early in 
October ; and about the same time Herr Stahl commences 
a series of highly interesting programmes; the latter 
entertainment includes the performance by a first-class 
orchestra of classical Symphonies, relieved by modern 
works (including occasional renderings of the Scandinavian 
Symphony of our gifted compatriot, F. Cowen), and 
relieved also by a substantial bill of fare in the literal sense. 
Amateurs whose musical experience is bounded by St. 
James’s Hall on the north and the Crystal Palace on the 
south must come to the Gewerbe-Haus to understand how 
Beethoven and beer, Mozart and mutton cutlets, can be 
simultaneously swallowed without an indigestion

MUSIC IN DUBLIN. 
FROM OUR OWN CORRESPONDENT

YORKSHIRE musicians are beginning to buckle on the 
harness for a fresh spell of hard work, and there are indica. 
tions of an interesting season in the announcements which 
have already been made. In four of the principal towns— 
Leeds, Bradford, Huddersfield, and Halifax—the provision 
to be made will lack neither variety, abundance, nor matter 
of novelty. Mr. Alfred Broughton, Mr. Edgar Haddock, 
the Subscription Concerts Committee, and others will 
provide for Leeds; and in Bradford, besides the Sub. 
scription Concerts, amateurs will have the advantage 
of listening to chamber music provided by Mr. Midgley and 
Mr. Misdale, who will have the assistance of many of the 
best instrumentalists. At Huddersfield the Subscription 
Concerts will be resumed and the Choral Society will con. 
tribute largely to the gratification of the enthusiastic 
musicians of that town; while in Halifax Mr. Sykes has 
in hand an extended series of Concerts of the best class

The excellent Concerts which for several years Mr. 
Rawlinson Ford has conducted at Leeds, under the name 
of the Leeds Popular Concerts, will henceforth be known 
as the Leeds Subscription Concerts. The burden, though 
taken up with enthusiasm, was too heavy for one man’s 
shoulders, and the work which Mr. Ford formerly carried 
on unaided has now been transferred to a committee, of 
which the former promoter is chairman. A glance at the 
prospectus issued for the ensuing season is sufficient to 
show that the reputation of the Concerts will not be 
allowed to suffer in the hands of the new management. 
Six Concerts are to be given, two of which will be miscel- 
laneous, two chamber, and two orchestral. Sir Charles 
Hallé will be responsible for the Orchestral Concerts. 
Among the works promised for the season are Beethoven’s 
Septet for strings and wind, Schumann’s Symphony 
(No. 1) in B flat, Liszt’s ‘“ Poéme Symphonique,” 
Schubert’s Quintet in C, for strings, and his great 
Symphony in C, Mendelssohn’s G minor Concerto, and 
several more or less well-known Overtures and Sonatas ot 
importance. Dr. Joachim is to appear at the final Concert 
in March in company with Miss Soldat, Mr. Gibson, Mr. 
Hausmann, Miss Fanny Davies, and Miss Sicca; and 
among other artists who will be eagerly looked forward to 
are Madame Minnie Hauk, Madame Belle Cole, Mr. 
Carrodus, and Mr. Santley. The guarantee fund amounts 
to nearly £1,000

The Philharmonic Society have not quite completed their 
arrangements for the winter, but among other works in- 
intended to be produced in addition to the regular “ annuals” 
—the “Elijah” and ‘The Messiah”—are Dvorak’s 
“‘Stabat Mater,” Mr. Hamish MacCunn’s ‘Lord Ullin’s 
Daughter,” and Dr. Parry’s “ Blest pair of Sirens.” The 
Dewsbury Choral Society, with a chorus of 200 voices, 
which also has Mr. Broughton for Conductor, will give the 
same programme. The Rotherham Choral Union, another 
battalion of Mr. Broughton’slargearmy, is to give “‘Athalie,” 
and a miscellaneous selection. All the performances will 
have a full orchestral accompaniment. The Ilkley Vocal 
Society contemplates the production of Mackenzie's 
‘“‘ Bride,” MacCunn’s “ Lord Ullin’s Daughter,” Lloyd's 
** Song of Balder,” and Sullivan’s * Prodigal Son

619

in A, Dvorak in F, and a Sonata specially written by 
F. Kilvington Hattersley. In addition several new violin } 
works by Mackenzie, Wieniawski, Moszkowski, and others ! 
are promised, and at the final Concert Beethoven’s Septet | 
will be given

The Bradford Subscription Concerts Committee are 
reducing the number of their Concerts from seven to six. | 
Two of these are set apart for ‘* The Messiah” and the | 
“Elijah,” the choruses of which will be rendered by the 
Bradford Festival Choral Society. Sir Charles Halle will 
again take the general direction, and will himself 
give a pianoforte solo. Mdlle. Marie Soldat will appear 
at one of these Concerts. Madame Norman-Néruda 
and Signor Piatti will take part in the Chamber Concert. 
Of works which have not been heard before at Bradford the 
most remarkable of those which may be expected is 
Berlioz’s ‘* Symphonie Fantastique.” Brahms’s famous 
double Concerto for violin and violoncello is down for the 
second orchestral Concert. Mr. and Mrs. Henschel will 
give a ballad Concert, assisted by Miss Lena Little and 
Mr. Orlando Harley, and Mdile. Janotha as_ pianist. 
Madame Valleria, Miss Mcintyre, and Miss Alice Whitacre 
are to be the vocalists at the Chamber Concerts

The Bradford Festival Choral Society is rehearsing 
“The Light of the World,” which it will revive under the 
conductorship of Dr. J. C. Bridge in November

Mr. S. Midgley promises two attractive Concerts, at the | 
first of which, on the 16th inst., the Shinner Quartet will | 
be heard in works by Beethoven and Schubert. With the | 
assistance of Mr. Midgley, Stanford’s Quintet will be heard | 
for the first time in Bradford. Among others, Mr. F. | 
Lamond, the young Scotch pianist, is also to appear if his 
Continental engagements will permit. Mr. Midgley will 
also be responsible for an attractive series of Concerts at 
likley

At Halifax Mr. James H. Sykes is making a departure 
in the scope of his Concerts with the object of adapting 
them more closely to the popular level. Two out of the | 
four meetings will be Ballad Concerts

Ix a few days Messrs. Novello, Ewer and Co. will issue | 
a revised and enlarged edition of their Words of Anthems. 
It is both comprehensive and extensive, and as it contains 
the words of the whole of the anthems in general use in | 
the majority of the Cathedrals, Collegiate Choirs, and | 
Churches where choral service is usually celebrated, | 
amounting in number to about 2,000, will be most valuable | 
for use and reference alike in the house of worship as in the 
home circle. It is printed in bold and readable type, and | 
is SO arranged that additions can be made from time to | 
time without disturbing the character of the plan, which is 
to show, as far as possible, a chronological sequence. The | 
words include many Latin anthems and so forth, which are 
in constant use in the College Chapels in the Universities

We have received the catalogue of a small but very 
choice collection of autographs, which lie in the hands of 
Messrs. W. E. Hill and Sons, 38, New Bond Street, for 
sale. The autographs are all those of composers repre- 
senting the Italian, the French, the German, and the 
English schools, and consist chiefly, but not entirely, of 
musical MSS. Conspicuous among these are a complete 
Tiio for organ, by J. Sebastian Bach ; unpublished songs by 
Schubert, and by our own Henry Purcell ; a Scena (unpub- 
lished) by Boccherini, and sheets of Studies by Beethoven 
for three of his most capital works, the Pianoforte Con- | 
certo, Op. 73; the Pianoforte Fantasia, Op. 77; and the | 
Choral Fantasia, Op. 80, all written in 1810. There are | 
also two complete works from the pen of Mozart, a com- | 
plete composition from that of Haydn, for voices and | 
orchestra; a page of Gluck’s handwriting andan Air de Danse | 
from his ‘* Admetus,” a chapter of Grétry’s autobiography, | 
ashort Marche Funébre, improvised by Liszt on the death | 
of his father at Boulogne, August 30, 1827, and interesting | 
MSS. of Auber, Bellini, Donizetti, Rossini, Gounod, | 
Dalayrae, Piccini, Romberg, Spohr, Wagner, Verdi, Weber, 
and many more well known to fame. The whole collection 
Should not be parted, but should be kept together and 
lodged permanently in one of our public libraries, where 
it may be of use to musical students

duets ‘* When the

Tue mortal remains of Franz Schubert were removed on , : i 
from four to three acts, the composer himself conducts the

en he the 23rd ult. from the suburban Friedhof at Wahring to ; Ab ‘ ia “y 
of the the central cemetery of Vienna, where, it is to be hoped, rehearsals, and the public performances will most likely 
gf the they will now be allowed to rest in peace by the side of the | Commence early in the present month. 
duced ashes of Beethoven. The ceremonies in connection with The Marquis d'lvry, the composer of an opera “ Les 
s now the re-interment of the great musician amounted to an|Amants de Vcrone,” produced in the Freee capital some 
im by imposing demonstration, in which it may be said that half | Yeats since and repeated - Covent Gasden, has poe spe 
ation, Vienna participated, the entire route, some six miles in pleted a new operatic work in four acts, entitled ** Perse- 
in it length, along which the funeral procession passed, having | Verance d'amour.” ee ‘ ais 
ber of been crowded with thousands of spectators, whose attitude | 4 New comic opera, * I Nipoti del Borgomastro, by the 
rican bore eloquent testimony to the love and veneration with | Maestro Achille Gratligna, was well received on its recent 
usical which the Viennese regard their great countryman. “ The | fitst performance at the Teatro Balbo of Turin. eens 
lerive glass-covered funeral car, drawn by eight horses,” says a other operatic novelties recently produced on Italian soil 
correspondent, ‘‘ was preceded by mounted standard-bearers | ™4Y be instanced “Ivanhoe,” by the. Maestro Ciardi ot 
tories and mutes, clad in old German costumes. A number of Prato ; a comic opera, ial) Gatta bigia, _by Agostino 
tions, carriages, laden with magnificent wreaths followed, and Sauvage 5 and a Ballet, * La Recluta, by M. Herbin, the 
those another company of mutes, carrying a golden crown and a | latter having — successfully produced at the Circo 
narks golden lyre on crimson velvet cushions, brought up the rear. | niversale of Palermo. eceree ; ; : 
lf of In the procession were nearly a hundred Musical Societies he Fenice Theatre of Venice is perme revival 
, and and Choirs from Vienna, the Austrian provinces, Hungary, | 0 1s boards of an interesting, though igs neglected, 
‘ions, and Germany. At the Schiller Platz, in the Ring Strasse, | OP¢™@-buffa, the “La Scuffiara,” by Paisicllo. : 
and, a halt was made, when a choir of some two thousand| 4 Project is being entertained in Italian sical circles 
» few voices performed, in front of the hearse and coffin, |COmcerming the removal of the remains of the celebrated 
it to Schubert’s beautiful chorale ‘ Die Nacht,’ with new words, | COMPoser, Niccolo Piccini, from Passy, near Paris, to his 
o be the effect being most impressive. Other Schubert com- | Pative town of Bari. It appears, howe from a state- 
sides positions had been sung on the procession leaving the ment made by Le Ménestrel, that it will be no easy mattel 
tions Wahring Cemetery, and were likewise performed on the | ? determine the exact al where the former rival of 
tO is destination, the Central Cemetery, being reached. ‘There | Gluck was interred iS We year 1500, Une cemetery on 
| the also Herr Gabillon, of the Burg ‘Theater, recited a | question having — long since am cIsuse and partially 
was touching poem, and several speeches were made. The | built over, although the marble slab placed over the grave 
1uk’s religious service was performed by Schubert's brother, the | by _ pupil of the master still remains, with others, in the 
says Rev. Hermann Schubert, other members of the family being small remaining portion of the original cemetery. ; 
It of also present.” ° M. Gevaert, the director of the Brussels Conservatoire, 
first On the second day of last month thirty years had | has written some recitatives, embodying the spoken dialogue 
uent dapsed since the first performance, at Vienna, of Wagner's | 1? Beethoven's * Fidelio,” in which form the immortal work 
rerly “Tannhiuser.” There was no lack of adverse criticism at | !$ to be henceforth produced at the Theatre de la Monnaie. 
ut a the time in the Viennese press, the opera, according to one Joseph Michel, of the Academy of Music, Ostend, died 
re to authority, being ‘‘ quite unsuitable to the taste of a Viennese | 07 the Sth ult., at Ostend, at the early age of ger The 
eh public.” Wagner's early chef-d’wuvre has, however, been deceased musician stood in the foremost rank of Belgian 
the produced two hundred and ten times since the above date, | COMPOSETS, both as a writer of high-class pianoforte 
elli, giving an average of seven representations during each | MUSIC and as the successful i es of two me Seat 
5 Of season, from which it may be inferred that the taste of the entitled respectively “Aux enya _and ** Le Cheva- 
the frequenters of the Vienna Hof-Theater has undergone lier de Tolede.” He leaves behind him, in a finished state, 
: a third operatic work, entitled ‘* Chicot,”’ which willdoubtless

some change in the meantime. 
Messrs. Breitkopf and Hartel, of Leipzig, have just com- 
| menced the issue, in parts, of a popular edition of the

be produced before long by the enterprising and patriotic 
managers of the Brussels Opera. 
An early opera by M. Ernest Reyer, the composer ot

complete works by Beethoven, which will be a reproduction be pee : Segrr ai

ing incheap form of the splendid standard edition of that master’s | “5's4rd,” 1s pe be shortly revived Ngai Théatre de la 
als. works hitherto published by the eminent firm in question. Monnaie, ot Brussels. The work, which en entitled 
vhs, It is stated in German papers that Johannes Brahms has | “ Etostratus,” was produced some years ago at Paris, and 
n. made good use of his summer vacation, spent as usual at withdraws R — the répe reece me the — So 
= Thun, and that the result will shortly become the property — a al : persenceeed — tk heer segs pv

AUDENSHAW.—On Sunday, the 23rd ult., the Harvest Festival 
Services were held at Red Hall Chapel, and included some special 
music at the evening service, when the choir were assisted by Miss 
Marjorie Eaten, of Ashten. Mr. C. H. Waterhouse presided at the 
organ. The service concluded with the “Gloria in Excelsis” from 
Mozart's Twelfth Mass

Batu.—Mr. Minton Pyne, Organist of St. Mark’s Cathedral, Phila- 
delphia. U.S., and son of the Organist, gave a Recital on the organ, on 
the 12th ult., at Bath Abbey. The programme consisted of six pieces— 
Mendelssohn’s Sonata (No. 6) in D minor, Handel’s Andante in B 
flat, Bach's Prelude and Fuge in D major, Beethoven’s Adagio in A 
flat, Wesley’s “ Holsworthy Church Bellis” with Variations, finishing 
with Mencelssohn’s Cornelius March. The compositions of the great 
musicians were played with artistic feeling and just appreciation of 
their intentions

CarcutTta.—An Organ Recital was given by the Organist, Mr. Ernest 
Slater, F.C.O., R.A.M., at St. Paul’s Cathedral, on Thursdav, August

Children pray this love to cherish’; a Solemn March by Smart; an 
Elevation and a Grand O#ertoire by Batiste; and Fugue, “ St. 
Anne’s”’ (after the service), by Bach

Dexver, Cororapo.—Mr. John H. Gower, Mus. Doc., Oxon., 
Organist and Precentor of Denver Cathedral, gave one of his fort- 
nightly Recitals of Music on August 10, of which the following is the 
programme :—Sonata in F minor (Mendelssohn), Allegro serioso, 
Adagio, Andante Recitative, Allegro assai, Concerto in G, arranged 
for organ and pianoforte (Handel); Fugit. in G minor (J. S. Bach), 
Song, “ My heart ever faithful,” with accompaniment for violoncello 
and organ (Bach); Overture (Rossini), Adagio from Sorata for 
violoncello and organ (J. H. Gower), Offertoire in D major (Batiste), 
Song, “The better lard” (Cowen); Largo in G, for violoncello and 
organ (Handel); Turkish March, Ruins of Athens (Beethoven). Mr. 
Gower was assisted by Mr. Houseley (pianoforte), Mr. Smith Winkler 
(violoncello), and Mrs. Leverirg (vocalist). The notice of the Con- 
cert in the Jocal paper was of that form of criticism which has formed 
the theme for many an occasional note in these columns. It is a gem 
of which ro facet should be hidden

LraTHERHEAD.—An Organ Recital was given by Mr. Ferdinand 
Lawson, A.C O., in the Parish Church, on the oth ult. The pro- 
gramme included Weé!ly's Offertoire in G, Andante in D (Archer), Pre- 
lude and Fugue in C minor (Bach), Serenata (Braga), and Lemmens’s 
Storm Fantasia

ALBERT RENAUD Is

591. I saw lovely Phillis ... 
592. Inthe lonely vale .-Dr.Caricotr 1d. 
593. The rainy day.. 1. ARTHUR SULLIVAN 1d. 
594. How bright in the May- time R.L. pe PearsaLy 1d. 
595. Shoot, false love, I carenot R.L. pe PearsaLy 1d. 
596. Sing we and chaunt it «. R.L. pe PEARSALL Id. 
597- Nymphs are sporting | R.L. ve Pearsaty 1d. 
598. The Vesper Hymn ... «+ BEETHOVEN Id. 
599. O-come, let us sing ... BerTHOLD Tours 14d

MACFARREN, G. A.—* May Day.” 9d. 
PROUT, EBENEZER. —‘“ The Red Cross 
Knight.” 2s. 
R. CHARLTON T. SPEER (Associate of and 
Professor of the Pianoforte at the Royal Academy of Music

RECENTLY PUBLISHED

N 
a bes! ees ee oo 
T. ANDERTON. 22 22 FRANZ LISZT. £8 &§ & 
YULE TIDE.. » ee ee 3/6 2/0 3/0) THE THIRTEENTH PSALM... .. 20 — — 
IE NORMAN BARON . o. «=0C we «HO 16 — 
” C. H. LLOYD. 
BEETHOVEN. ANDROMEDA ih ‘ os 3/0 3/6 5 
? PRAISE OF MUSIC dn ~. 16 — —|HERO AND LEANDE R.. a - 16 — — 
ee eae OF Ao ’ SONG OF BALDER _.. = aN 
J. F. BRIDGE. THE LONGBEAR D'S SAGA (Male voices) 16 — — 
ROCK OF AGES (Latin and Englishwords) 110 — =| HAMISH MacCUNN. 
- (Tonic Sol-fa, 4d.) LORD ULLIN’S DAUGHTER = —_ 
CALLIRHOE ger lege ee ee aR a . anes e oe YO 
(Tonic Sol-fa, in the Press.) A. C. MACKENZIE. 
DUDLEY BUCK. ROSE OF SHAR gf 3, Glo 76 
THE LIGHT OF ASIA .. + +» 3/0 3/6 5° THE STORY OF SAYID oe -- 3/0 316 4/6 
YN F. CORDER es ie ae ae 
i Feiler ceca nat | C. HUBERT H. PARRY. 
THE BRIDAL OF TRIERMAIN a AO me ee “4 
BLEST PAIR OF SIRENS... .. 10 — — 
F. H. COWEN. JUDITH; or, THe REGENERATION oF 
" S 5 « om: "6 
SLEEPING BEAUTY .. .. «. 216 3/0 glo}  ‘MANASSEH =... =. 0 ue os 5/0 Glo 716 
RUTH. +» 4/0 46 60 CIRO PINSUTI. 
SGIVING ee io — - 
A SONG OF ‘THANK GIVING = PHANTOMS (Italian and English Words) 1/0 — — 
ANTONIN DVORAK. E. PROUT. 
ES SINT LUDMILA... ++ ee 50 6/0 76| THE RED CROSS KNIGHT .. .. 4/0 4/6 6/0 
THE SPECTRE’S B RIDE. re e- 310 3/6 5/o if 
APATRIOTIC HYMN .. .. .. 1/6 — — C. SAINT-SAENS. 
ite ed: os ee 
HENRY FARMER SCHUMANN. 
MASS IN B FLAT (Latin and English) 2/0 216 3/6 THE MINSTREL’S CURSE a 
— HE KING’S SON ; eee 
ROBERT FRANZ. scien - 
PRAISE YE THE LORD (Psarmiiz).. oo — — ALICE MARY SMITH. 
S (Mrs. Meadows White.) 
NIELS W. GADE. SONG OF THE LITTLE BALTUNG 
PSYCHE ‘a - 2/6 3/0 4!o (Male Voices) .. « Hoe — 
(Tonic Sol-fa, 1,6) THE RED KING (Male Voices) . Sx, RS emt 
F. GERNSHEIM. C. VILLIERS STANFORD. 
SALAMIS (Male Voices) .. «2 «. 1/6 — — THE REVENGE ..  ..  .. «2 16 — — 
(Tonic Sol-fa, gd.) 
ae wo J. STAINER. 
WATER LILY (Male Voices) .. 1/6 — THE DAUGHTER OF JAIRUS a ae 
TNT (Tonic Sol-fa, 94.) 
CH. GOUNOD. ST. MARY MAGDALEN. : «» 2/0 2/6 4/o 
THE REDEMPTION ue -» 5/0 6/o 7/6 (Tonic Sol-fa, 1 -) 
(Tonic Sol- fa, 2 2| t-) THE CRUCIFIXION - - 16 — — 
Ditto (French words) .. - 84 — — (Tonic Sol-fa, ‘od.) 
. Dirro (German words).. 1olo — -— — : saat me 
ES J RoisiiMe MESSE SOLENNELLE.. 26 — — ARTHUR SULLIVAN. 
MORS ET VITA .. 6}0 66 76 THE GOLDEN LEGEND... 3/6 4/0 5/0 
Tonic Sol-fa, Latin and English Ww ords, 2 2 :) | (Tonic Sol-fa, 2’-) . 
ADOLF JENSEN. | C. M. VON WEBER. 
THE FEAST OF ADONIS)... ~—«. 10 «1/6. —! TN CONSTANT ORDER (Hymn) .. 16 — — 
LONDON AND NEW YORK: NOVELLO, EWER AND CO

XUM

