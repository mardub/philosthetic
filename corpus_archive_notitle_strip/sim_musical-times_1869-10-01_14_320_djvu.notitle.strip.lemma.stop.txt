MAGAZINE SACRED MUSIC 

 VOL . iv . ready , contain work following Com- 
 posers.—V. Gabriel , Smart , Elizabeth Philp , Macfarren , ba : ker , 
 Redhead , Allen , B. Tours , Liebich . Beethoven , Mozart , & c. , & e. 
 elegantly bind cloth , price 5 . post - free , 5 . 6d 

 Metzler Co. , 37 , Great Marlborough - street , W 

 

 love music great extent fashion 
 strikingly exemplify comparatiye 
 neglect composition Spohr . 
 time Beethoven , 
 , dare , Handel , stil ] 
 passes opinion Spohr , great 
 work rarely hear allow composer 
 speak . necessary point 
 instance desire educate public 
 taste , pander ignorance , hag 
 end financially successful , 
 mention Handel Israel Egypt , 
 composer Jephtha , brand 
 ' ' heavy , " commit crime lin . 
 gere captivity , release 
 strenuous exertion believe 
 condemn , allow 
 privilege fair trial . surely 
 think , Spohr scarcely realise 
 sublimity subject choose 
 Oratorios , acknowledge abstract 
 beauty music render accept- 
 able audience ambitious composition 
 aspirant fame hide weakness 
 merely - produce form 
 great writer throw imperishable thought , 
 general execution Oratorio good ; 
 early chorus 
 unsteadiness characterise . 
 formance difficult music hurriedly . 
 choral effect thoroughly satisfac- 
 tory portion chorus Jews , " 
 lion rouse " ( fine contrast ad- 
 mirably sustain ) , chorus , ' come 
 dust humble , " ( especially 
 conclude fugue , ' ' shall reign , " ) 
 final chorus " thank unto God . " Madlle . 
 Tietjens admirable voice , 
 exquisite solo , ' ' dear child bondage , " " 
 long shall Judea child wander , " utmost 
 pathos truth expression . Madame Talbot- 
 Cherer , careful delivery music 
 fall share , partially compensate 
 boldness attempt Galatea 
 Monday evening ; Madame ‘ Trebelli - Bettini lend 
 powerful aid - know trio , ' ' loud pro- 
 claim great Salvation ; " Madame Patey 
 fully prove true 
 conscientious artist . Mr. W. H. Cummings 
 Mr. Santley efficient solo music 
 entrust — gentleman produce 
 marked effect expressive air , " o 
 man " — declamation Signor Foli 
 highly dramatic recitative Belshazzar , ¢s- 
 pecially deserving commendation . Mr. A. Byron 
 create favourable impression intelligent 
 singing duet , Madlle . Tietjens , ' ' Judah 
 choose nation , " good specimen 
 Spohr elegantly finish vocal piece 

 Thursday morning performance commence 
 new sacred Cantata , " Song praise , " 
 Mr. Horace Hill , Mus . Bac . Cantab . com- 
 poser , thank goodness , come unheralde 
 flourish trumpet ; music , regret 
 , consist merely number broken frag- 
 ment , property glorious predecessor , 
 cement , sufficient skill 
 deceive person — rest , 
 fear , . little 

 miscellaneous concert , 
 Thursday morning , include composition 
 Conductor , Mr. Benedict — overture 
 Kleist Drama , Der Prinz von Homburg , 
 song chorus , ' ' Forging 
 Anchor " — work ' utterly unequal merit 
 believe scarcely possible 
 proceed mind . course 
 moment imagine song descriptive 
 forging anchor , place @ level 
 grand dramatic overture , suppose 
 good kind ; simply 
 dislike song good 
 specimen bad school . let , , 
 man forge anchor , vocal 
 organ work , scarcely fairly match 
 Mr. Santley ; , victory 
 incline , , trust 
 pride man . 
 regret composition encore . 
 difference music noise , 
 Mr. Benedict produce new overture , 
 hesitation pronounce 
 good orchestral work bas write . 
 high dramatic interest , instrument 
 hand master , perfectly sym- 
 metrical design long hear 
 pressure space 
 compel pass brief recognition 
 important merit . vocal piece 
 produce effect , mention 
 Madlle . Ilma di Murska " Gli angui d’inferno 

 Jl Flauto Magico , " o luce di quest ’ 
 anima , " Linda , Madame Patey " Cradle 
 Song " ( Randegger ) , accompaniment piano . 
 forte , viola violoncello , Madlle . Tietjens ’ ' ] 
 raggio , " Mr. Santley " wish tune quiver 
 lyre " ( Arthur Sullivan ) , Madlle . Tietjens 
 Madame Trebelli - Bettini duet , ' ' Serbami ognor , " 
 Mr. W.H. Cummings ’ song , ' Angel Home " 
 ( H. Smart ) , Mr. Vernon Rigby old ballad , 
 ' * * thorn , " receive enthusiastic 
 encore . Mr. Pierson chorus , ' sound , immortal 
 harp , " sing , remark . 
 instrumental piece , Mr. Benedict overture , 
 mention , Beethoven Leonora . 
 ture Mendelssohn ' ' Cornelius March . " 
 conclude notice Festival 
 ( regret hear scarcely pecuniary 
 success ) bear ample testimony excel . 
 lence orchestra , general efficiency 
 chorus , skilful conductorship Mr. Bene . 
 dict , utmost artistic zeal 
 enthusiasm trying week . 
 Festival Worcester commence 
 ' Tuesday morning following week , allow . 
 e day ’ rest artist employ , 
 Monday occupy rehearsal , 
 ‘ lijah Oratorio select . 
 formance ; person interested 
 time - honour Choir Festivals 
 glad Cathedral 
 , hear nearly ticket 
 week dispose . 
 soprano solo assign 
 Madame Lemmens - Sherrington , second 
 Madlle . Tietjens , tenor music 
 divide , jthe manner Mr. Vernon 
 Rigby Mr. Sims Reeves ; Madame Patey 
 Madame Trebelli - Bettini share contralto solo , 
 Prophet ( satisfaction 
 think , , personality cha 
 racter preserve ) sing 
 Mr. Santley . needless dwell execu- 
 tion know work equally 
 know vocalist ; Madame 
 Trebelli - Bettini , 
 Oratorio music , sing , fully 
 realise true intent Mendelssohn ; Mr. 
 Vernon Rigby beautiful air , ' 
 heart , " chasteness feeling truth 
 expression highly com- 
 mend . course endure usual 
 absurdity hear Madame Lemmens - Sherrington , 
 having sing music widow , assume 
 ' boy ; " 
 point , anomaly 
 distribution character essentially 
 dramatic Oratorio , presume 
 consider hopeless expect reform 
 matter . chorus , espe- 
 cially , ' ' blessed man , " ' watch 
 Israel , " " ' thank God , " , how- 
 , suffer , usual Choir Festivals , 
 unseemly " lunch . " 
 Wednesday morning performance commence 
 Mr. Arthur Sullivan sacred Cantata , 
 Prodigal Son , compose expressly Festival . 
 record success work , 
 means imply high 
 estimate merit . successful 

 tyrant ' lime eventually pro 

 performance Messiah , Friday 
 morning , word suffice . principal 
 sing Madlle . Tietjens , Madame Lem- 
 mens - Sherrington , Madame Trebelli - Bettini , Madame 
 Patey , Mr. Sims Reeves , Mr. Santley Mr. Lewis 
 Thomas . artist know need 
 passing eulogy effort ; 
 help especial mention 
 singing Mr. Sims Reeves , 
 reserve force power great 
 declamatory air , ' ' thou shalt break , " 
 deliver extraordinary energy . 
 Cathedral fill ; effect , 
 look orchestra extreme east end 
 ( throw open ) truly 
 imposing 

 miseellaneous concert commence Tuesday 
 evening , College Hall . Mr. J. F. Barnett 
 Cantata , ' ' Ancient Mariner , " occupy 
 programme , principal vocalist 
 Madlle . Tietjens , Madame Patey , Mr. Vernon Rigby 
 Mr. Lewis Thomas . execution work 
 excellent , chorus sing 
 remarkable steadiness precision . encore 
 award Madame Patey admirable 
 rendering song ' o sleep , itis gentle thing , " 
 Madile . Tietjens Madame Patey 
 singing characteristic duet , ' ' voice 
 air . " Cantata steadily conduct 
 composer , receive ovation con- 
 clusion performance . principal instru- 
 mental feature second concert , 
 fine performance movement 
 Beethoven violin concerto , Mr. Carrodus , , 
 consider length programme , lis- 
 tene tolerable patience , 
 cadenza , ( somewhat elaborated , 
 Molique ) hazardous experiment 

 effective vocal solo 
 Molique song , ' ' moon brightly shin . 
 e , " exquisitely sing Mr. Sims Reeves , Ran . 
 degger Serenade , ' " dors enfant dors , " 
 expression Madame Trebelli - Bettini , 
 Blumenthal song , ' ' love pilgrim , " 
 Madlle . ' Tietjens render justice 
 composition deserve . Nicolai overture 
 ' Merry Wives Windsor , " conclude 
 concert 

 tanti " ( Madame Trebelli - Bettini ) , ' ' fra poco " ( Mr. 
 Sims Reeves ) , ' ' Qui la voce " ( Madlle . Tietjens ) , 
 " come , dare " ( Mr. Vernon Rigby ) . 
 , , redeem point 

 performance Beethoven Symphony F ( . 8) , 
 usher second . concert ter- 
 minate National Anthem 

 W114 Pote , F.R.S. , Mus . Doc . , Oxon . 
 ( conclude p. 204 

 AsHpowN Parry 

 Andante Beethoven 1st . Symphony . 
 Pianoforte . Frederic N. Lohr 

 speak favourably 
 author arrangement Tema variation 
 Beethoven Septett ; award equal praise 
 . excessive beauty . 
 ment , skilfully arrange , carefully finger 
 necessary , recommend Beethoven 
 lover wish revive impression receive 
 hear original instrumentation . 
 excellent condensation score ; trust 
 success piece induce publisher 
 issue extract standard work 
 arranger 

 arrange 

 43 Scotch melody , arrange Pianoforte . 
 42 irish melody , arrange Pianoforte 

 41 operatic air , arrange Pianoforte . 
 40 thirty - valse d’Albert , eminent composer , 
 39 Christy Minstrel Song Book . ( selection ) , 
 38 fashionable Dance Book , Pianoforte . 
 87 country dance , reel , jig , & c. , Pianoforte . 
 36 Christy Buckley Minstrel Airs Pianoforte . 
 35 Christy Minstrel song . ( second selection ) . 
 34 Christmas Album dance Music , 
 33 Juvenile Vocal Album , 
 32 Beethoven sonata . edit Charles Hallé ( . 6 ) . 
 31 Beethoven sonata . edit Charles Hallé ( . 5 . ) 
 30 Beethoven sonata . edit Charles Hallé ( . 4 . ) 
 29 Contralto song , Mrs. R. Arkwright , & c. 
 28 Beethoven sonata . edit Charles Hallé ( . 3 . ) 
 27 set quadrille duet , Charles d’Albert , & , 
 26 thirty galop , Mazurkas , & c. , d’Albert , & c. 
 25 Sims Reeves ’ popular song . 
 24 thirteen popular song , Barker , Linley , & c. 
 23 - Juvenile piece Pianoforte . 
 22 - Christy Minstrel song . ( selection . ) 
 21 Pianoforte Pieces , Ascher Goria . 
 20 Beethoven sonata . edit Charles Hallé ( . 2 . ) 
 19 favourite air Messiah , Pianoforte . 
 18 song Verdi Flotow , english word . 
 17 Pianoforte piece , Osborne Lindahl . 
 16 sacred duet , Soprano Contralto Voices , 
 15 eighteen Moore irish melody . 
 14 song , Schubert . english german word . 
 13 popular duet Soprano Contralto Voices . 
 12 Beethoven Sonatas . edit Charles Hallé , . 1 . 
 11 Pianoforte piece , Wallace . 
 10 Pianoforte Pieces . Brinley Richards , 
 9 Valses , C. d'’Albert , Strauss , & c. 
 8 polka , Charles d’Albert , Jullien , Keenig , & c. 
 7 set Quadrilles , 7 Charles d ' Albert , complete . 
 6 song , Handel . 
 & sacred song , Popnlar Composers . 
 4 song , Mozart , italian english word 
 3 song , Wallace . 
 2 song , Hon . Mrs. Norton . 
 1 thirteen song , M. W. Balfe 

 song pianoforte accompaniment 

