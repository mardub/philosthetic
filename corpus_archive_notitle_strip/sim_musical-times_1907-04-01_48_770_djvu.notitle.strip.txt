Of Handel in his purely instrumental music 
it is said that ‘it would be difficult to discover 
one less open to suspicion of being a writer of 
programme music. 
to vocal music he demonstrated power to paint 
tone-colour vividly. Haydn in ‘ The Creation,’ 
‘The and other works is claimed 
as a programmist, and it is stated that even 
in composing his sonatas and symphonies he 
was dominated by unrevealed romances and 
programmes. A curious case of what may be 
termed programitis is that of Dittersdorf (1739

1799), who in twelve symphonies endeavoured to 
illustrate subjects from Ovid’s ‘ Metamorphoses.’ 
Professor Niecks says that these symphonies ‘are 
full of beauties and points of interest,’ and that 
‘they well deserve to be read and played.’ After 
dealing rather fully with the powerful efforts of | 
Beethoven to express and moods, the 
Professor concludes ‘that the time cannot be far | 
off when he will be regarded as the chief founder 
and the greatest cultivator of programme music.’ | 
Nineteen pages are devoted to Mendelssohn, whose 
remark that ‘since Beethoven had taken the step 
he took in the Pastoral symphony, it was impossible | 
for composers to keep clear of programme music

is quoted

Without being so mean as to pull all the plums 
out of Mr. Michotte’s very tasty pie, I may venture 
to give some samples of its toothsome contents

On receiving Wagner, Rossini at once assured 
him that he had not been so impolite to a brother 
musician as to say the unkind things gratuitously 
attributed to him. ‘As for despising your music,’ 
he went on, ‘I ought in the first instance to know 
it, and to know it I ought to hear it at the theatre, 
for it is only in the theatre, and not simply by 
reading the score, that it is possible to render a 
just judgment of music intended for the stage. 
The only thing of Wagner’s he had heard was, he 
said, the ‘‘Tannhauser’ March, which he had 
After thus 
clearing the ground, intercourse became easy and 
pleasant, and many interesting topics were broached 
and discussed during this short visit. On Wagner’s 
expressing a fear—only too well justified in the 
event—lest the success of ‘’Tannhauser’ should be 
spoilt by a cabal, Rossini expressed his lively 
sympathy, and told how he had suffered in similar 
fashion, notably on the production of his master- 
work, the ‘ Barber,’ and even in later years in Paris 
and Vienna after his world-wide reputation had 
been made. In the latter case it was Weber who 
fulminated against Rossinian opera. Wagner 
admitted Weber’s intolerance, but his admiration 
for the composer who, more than any other, may be 
said to have prepared the way for him, led him to 
excuse him on the ground of his patriotism, though 
this, by the way, could not have been cited as an 
apology for his attacks on Beethoven, for Weber’s 
article ridiculing the fourth Symphony is a 
remarkable instance of how one composer may fail 
to appreciate another whose ideals are different 
from his own

The mention of Weber led Rossini to describe 
how that composer paid him a complimentary 
visit when (in 1826) he was passing through Paris 
to produce ‘Oberon’ in London. The circum 
stances were in curious contrast to those of 
Wagner’s visit, for while Wagner hesitated because 
he believed Rossini had ridiculed him, Weber 
hesitated because he knew he had severely 
criticized Rossini. However, Rossini’s édonhomt 
soon put Weber at his ease, though his condition 
was at this time pitiable, the disease which was so 
soon to carry him off, showing its ravages in his 
livid complexion and extreme emaciation, as well 
as in a consumptive cough

Then the conversation turned to another of 
Wagner’s idols, Beethoven, of whom Rossini had 
some vivid recollections, having paid him a visit in 
1822. What seems to have struck Rossini most 
was the exceeding sadness expressed in Beethoven's 
face. He was received kindly but with characteristic 
brusqueness, beethoven opening the conversation 
by complimenting him on the ‘ Barber,’ and telling 
him with delightful candour that he should stick 
to opera buffa, and not attempt anything serious

E. Michotte. 
Détails inédits et Commentaires. 
1 fr

natural instinct in composition—a modest and 
very just claim—and avowed that his slight

musicianship was chiefly derived from studying 
the scores of German masters. He named 
particularly ‘ Creation,’ ‘ Figaro,’ and ‘The magic 
flute,’ to which he might have added ‘The 
Seasons’ (‘The impatient husbandman’), and 
Beethoven’s eighth Symphony (4//egretto), to 
both of which he owed some definite suggestions. 
On his expressing his regret that he had not 
enjoyed a more thorough training on German 
lines, Wagner showed his appreciation of what 
Rossini had accomplished by citing the ‘ Scene of 
the darkness’ in ‘Moses in Egypt,’ that of the 
conspiracy in ‘Guillaume Tell,’ and, in another 
order, the ‘(Quando Corpus,’ as examples which he 
could hardly have bettered, and these the veteran 
admitted were among the ‘happy moments’ of 
his career

For Bach, Rossini expressed a lively admiration

_

he had so clearly expressed. For his own part he | 
was (00 old—‘ being at the age when one is not so 
much inclined to compose as liable to decompose’ 
—to turn his eyes to new horizons, but he was very 
willing to ac knowledge that Wagner’s ideas were of 
a nature worthy of the serious consideration of 
young composers. ‘Of all the arts,’ he concluded, 
‘music is that which is, by reason of its ideal 
character, most subject to transformations, and to 
these there can be no bounds. Who, after Mozart, 
could have foreseen Beethoven? Or, after Gluck

Weber? And, after these, why should there be an 
end to progress ?’ 
As they came away Wagner acknowledged to

An interesting sale of autographs took place at the 
— Drouot, Paris, during the last week of February

short note by Chopin fetched 45; a receipt by| 
Beethoven for 1,200 florins from the. Archduke Rudolf | 
ree ilized £4 2s. 6d.; two bars in score from| 
‘Lohengrin,’ written in } spo, £5 4s.; a sheet of 
sketches by Beethoven, 47 4s. ; a sonata by Gounod

12 8s. (purchased by St Charles Malherbe) ; the | 
score of Massenet’s —— £20 4s.; a Chopin 
Mazurka (Op. ph No. 1), £28 45.; and a Schubert | 
song (I page), 452

Chopin’s pupil, Madame Dubois (7¢e O’Meara), who | 
had been a sufferer for many years, recently died at | 
Paris. She studied with the composer for five years, 
and Prof. Niecks, in his ‘ Life of Chopin,’ furnishes an | 
interesting list, given to him by Madame Dubois 
herself, of the studies and pieces which she studied

under Chopin’s direction. After naming Bach, | 
Clementi, Beethoven, Weber, Mendelssohn and| 
Liszt, &c., she concluded: ‘and of Schumann

nothing

by Six Hubert Parry. 
| ORCHESTRAL : Symphony in C minor and Leonora

overture (No. 3), Beethoven. Romeo and Juliet 
symphony, Jeriioz. Symphony in E _ minor, 
| Tchaikovsky. Don Juan, symphonic poem, S/rauss

Overture, The butterfly’s ball, Cowen. Overture, 
‘Summer, Hervey. *Two Norfolk Rhapsodies, 
No. 2 in D minor, No. 3, A//a marcia in G minor 
and G major, Vaughan Williams

thirty-seven bars) with which the overture opens, 
the drummer came to the nineteen bars’ rest of the 
mdantino movement. ‘I will miss those,’ said 
the drummer. ‘ No, no, count them, please,’ said the 
maestro

Holz tells me that it is your intention to publish a_| 
larger size of the engraving representing Handel’s | 
monument in St. Peter’s Church, London [ Westminster | 
Abbey]. This affords me extreme pleasure, indepen- | 
dent of the fact that I was the person who suggested | 
it. Accept my thanks in anticipation.—BEETHOVEN

From a letter to Gottfried Weber, April 3, 1826

The history of music shows a steady and continuous 
progress towards more passionate emotional expression. 
In the work of the older composers there are subtle 
emotional effects which < difficult to label. The effect 
produced on the hearer is akin to the emotions which a 
grand landscape excites and suggests. We are conscious

and noble kind, but we cannot easily 
give them verbal expression. The emotional appeal became 
in course of time more and more pronounced, until in some 
of the works of Beethoven we find joined to extreme beauty

form, a passionate emotion. It is by no 
means necessary for the hearer to be placed in possession 
of the stimulating causes, if any, of this pa peal. A Composer 
of a strong emotional nature will produce works that reflect 
external circumstances ; the

S four movements in which brightness comes only as

leams of sunshine on an April day, but the music is 
y understood, | ¢} oroughly Mozartian in idiom. Mr. Wood is greatly to be 
the work recalled the} praised for reviving this interesting work, which it is to be 
good. For the present hoped will be heard uin during the Promenade 
we cannot yoke M. Enesco} Concert Season. Beethoven’s Rondino in E flat for wind

from when he was about twenty years

g haikovsky’s ‘ Mazeppa,’ the love scene from Strauss’s

Stanford’s melodious ‘Irish Rhapsody No. 1,’ and ended opera ‘ Feuersnot’ and Beethoven’s E flat Concerto, the

with Cherubini’s ‘ Les Abencérages’ overture. Miss Marie | pianoforte part of the last-named being beautifully played by 
Brema, the vocalist of the evening, was more successful] Mr. Richard Bublis ; - 
in the songs by Weingartner than in those by Purcell 3 — 
The concert given on March 13 had special interest, if | LONDON SYMPHON ONCERTS. 
nly because it incidentally demonstrated that new British} At the ninth concert given at (Jueen’s Hall on March 11

isic could be favourably pitted against new foreign music. | interest centred chiefly in the performance of Strauss’s 
The first item was the prelude to the as yet unperformed | ‘ Ein Heldenleben.’ Probably no finer interpretation has 
ypera ‘Ione’ by Mr. Arthur Hervey. This was conducted | been given of this remarkable work. No attempt was made 
by the composer and was well received. The opera has a|to mitigate the realism of the Battle section. If we must 
tragic plot in which love, jealousy and revenge are motives, | feed on Strauss, there is no doubt it is well to have him served 
and the drama. The themes have; hot. Mr. Arthur Payne played the violin solo with telling 
character and they are often effectively orchestrated. The} beauty of tone and execution. Mr. Harold Bauer gave

rown’ and brooding sections seemed proportionately too | highly effective performances of the B flat minor Pianoforte 
long to maintain interest, but the poetry of the treatment | concerto by Tchaikovsky and the ‘ Todtentanz’ by Liszt. 
was unmistakable. Another novelty in the programme was| The concert opened with Mozart's beautiful symphony in 
the Violin concerto in A (Op. 45) composed and conducted | E flat, and closed with one of the most brilliant performances 
y Christian Sinding, who appeared for the first time in| of Beethoven’s ‘ Leonora’ No. 3 overture that we have had 
England. The concerto did not make a very favourable| the good fortune to hear. Dr. Richter conducted and

pression. It lacked charm and often seemed laboured. | seemed throughout at his best

engenders a feeling of monotony

THE MUSICAL TIMES.—Aprit 1, 1907. 255 
M Theodora Macalaster, assisted by Mr. Stoney | chale (Mr. Wm. Kendall). Church choirs’ (men and boys 
in Beauchamp (tenor) and Mr. Robert Buchan (violin), gave a] prize, St. Paul's, Wimbledon Park (Mr. G i. Dean). 
i very successful concert at Steinway Ilall on March 6. | Church choirs’ (mixed) prize, Clapham High Street 
oa Th dy has a soprano voice of musical quality, and her Wesleyan Church (Mr. Wesley Hammet Men s choirs 
sis interpretation of an admirable selection of songs by British | prize, Wren male choir, Camberwell Mr. F. C. French). 
id ind tinental composers attested to considerable dramatic | ‘The Gentlewoman Competition for ladies’ choirs, 
. intelligence and sensitive feeling. [ler singing was much | first prize, Mrs. Mary Lavyton’s choir ; second prize, Aristotle 
o appr ciated by a large audience. Road E.C.S choir (Mr. A. G. Gibbs). Elementary 
‘ : : | school choirs, first prize, Aristotle Road school choir, 
ry The choir and orchestra of the City of London ¢ ollege | Clapham Mr. A. G. Gibbs this choir also gained the 
athe performed, on March 7, Schubert’s ‘ The song of Miriam,’ first prize for sight-singing. Second prize, Bolingbroke Road 
ing and Gade’s ‘ Spring’s message.’ The chorus sang throughout | L.C.C. school, Old Battersea (Mr. T. Maskell Hardy). 
a with excellent attack and much attention to details of | Two concerts were given by prize-winners. Sir Walter 
expression. The solo vocalists were Miss Ethel Williams, | Palmer distributed the prizes to the juniors. 
Miss Annie McBride, Mr. W. H. Walter and Mr. George} The adult competition attracted a large audience, 
ot s. The orchestra was ably led by Miss Maude]and, without depreciating any of the performances, all of 
il Swepstone, and the pianoforte accompaniments were in the | which were excellent, it is only fair to make special mention 
on hands of Miss Gertrude Smith and Mr. Russell Bonner. | of Mr. Arthur Holford (bass), the Hampstead Prize 
lly Mr. W. G. Rothery conducted. Silver Band, and Mr. Percy W. Lawton (tenor), the 
t’s ° , , i gold medallist of the festival. The Hon. Mrs. Talbot, 
“x The South London Choral Ass ciation performed Hamish supported by the Bishop of Southwark, presented the prizes, 
li MacCunn’s too rarely-heard work ‘The lay of the last} ,4q Lord Alverstone proposed the vote of ee. 
as minstrel’ on March 20, and gave an adequate rendering Of! Mrs. Talbot. The massed choirs of ladies sang charmingly 
ed the dramatic and declamatory a with which | the work under the conductorship of Dr. Madeley Richardson. The 
abounds. : A capable orchestra did justice to the picturesque adjudicators were Dr. F. N. Abernethy, Mr Oscar Beringer, 
accompaniments, and the vocal solos were ably sustained by Mr. | lr. Field, Dr. C. J. I rost, Mr. Alfred Gibson, 
Miss Mabel Manson, M adame ( ecile Vicars, Messrs Dr rt! | Huntley, Dr W. G. MeN wught, Mr. Dan Price, 
Bernard Turner and Herbert Tracey. Cowen’s choral ballad, | py, 4, Madeley Richardson, and Mr. Fred E. Weatherly 
8, ‘John Gilpin,’ and Faning’s ‘ Vagabonds’ were included in The secretary of the festival te Mr. T. Lester Tooes 
"e the second part of the concert, which was ably conducted by : palsentsat ed 
nl Mr. Leonard C. Venables. OAKHAM (RUTLAND). 
. An enjoyable concert was given by the Hampstead | , rhis was the second = = this typically countryside 
: : estival. The Hon. Mrs. Charles Fitzwilliam is the chief 
ve Conservatoire Orchestra on March 21, under the able ; ‘ anh ba Gale meee cae oes ee 
ly lirection of Mr. René Ortmans. Excellent performances | Promore! Tr a prog ba rat - oe ay = ‘ Ele > 
were given of Beethoven’s eighth symphony, the overture Gistrict. | Tin pomcesg a lid “ — i e oe 
to ‘ Die Meistersinger,’ the first movement of Tchaikovsky's sch ols had entered, but two did not atte a he rcapaneaneat 
<r > a . ; villages were represented by adult choirs. There was a 
Pianoforte concerto in B ilat minor—the solo part in which gs es tea ™ nein a 
was played by Miss Ada Petherick—and Arthut Hervey’s | PX si Sc oe ee ee rem cae 
, - S ‘ . 7 . *n flow’rv meadows,’ Palestrina), an anthem class 
clever and effective concert overture ‘ Youth.’ The vocalists | >, meta. “hae, ee gga -1 pane: 
were Madame Windsor Loche and Miss Gertrude Walton, | (“, What are these?” Stainer), a men s-voice class (" Lovely 
, night,” Chwatal}, a female-voice class The shepherd, 
k II. Walford Davies), a mixed quartet class (*‘ When 
5 , ae . hands meet,’ Pinsuti), and classes for sight-singing and voice 
Musical Competition Festivals. | production.’ virst places were secured by Wing, Exton, 
Reker Ridlington, Burley (two), Oakham (three), and Uppingham. 
STRATFORD Musical. FESTIVAL. The choirs combined to sing two glees at the evening 
This old-established festival has this year celebrated its| concert which was successfully given on the second 
’ silver jubilee Special and very successful efforts had | evening of the festival, March 7 
been made to give the event due importance. Fourteen 
adult choirs and eighteen junior choirs appeared. There were KENSINGTON COMPETITIONS 
: about 400 solo instrumentalists and 173 solo vocalists. Formerly this organizition appealed only to ladies’ 
Altogether it was estimated that upwards of 2,000 performers | choirs. It is now open to orchestras and mixed-voice 
took part in the festival. The results in the chief choral | choirs in addition. e test-piece for the orchestras was 
classes were as follows: First-prize winners—Ladies’ choirs, | Tchaikovsky's ‘ Elegie’ and a short sight-test. The Church 
Madame Hands’s choir; men’s choirs (two classes), | Orchestral Society (Dr. Huntley), the London Diocesan 
St. Columba’s (first in both); girls’ choirs, Stratford | Orchestra (Margaret Ilaweis), andthe Linnet String Orchestra 
Co-operative Junior (Mr. Alfred Sears); boys’ choirs, | Miss Ida Hyett) competed, and the challenge banner, 
St. Saviour’s, Walthamstow, choir-boys ; elementary school | offered as a prize, was won by the Diocesan Orchestra. In 
s choirs (boys), Godwin Road, Forest Gate, Council School ;]a sight-test competition (adjudicator, Mr. Joseph Ivimey 
n (girls), Farmer Road, Leyton, Council School; choral | the Church Orchestral Society gained full marks 
n Societies, Mr. G. Day Winter's choir. The adjudicators} In the choral sections nine choirs competed in seven 
) were Dr. Percy Buck, Mr. George Oakey, Mr. James] sections. Miss Lisa Gibson’s choir, which is a highly 
1 Bates, Mr. Oscar Beringer, Mr. G. WH.  Betjemann, | trained one, gained first-prize in three sections. Mrs. Mary 
l Mr. Ernest Fowles, Mr. Dan Price and Mr. A. L. Cowley. | Layton was successful in two sections. The Essendine 
e The competitions took place on March 9, 11, 13, 14, 15| Choir (Mr. W. Kendall) was first in the female-voice choir 
, and 21. Mr. J. Graham is the indefatigable secretary. | sight-test, and was only two marks below the winning 
S The concert given by the prize-winners in Stratford Town | choir in the mixed-choir section. The singing generally 
Hall on the afternoon of March 21 was honoured by the | was of a high standard. Dr. McNaught adjudicated in all 
presence of the Duchess of Albany. In addition to] the choral sections. The competition was held on March 14. 
distributing the prizes and certificates gained by the successful nee 
. candidates, Iler Royal Highness presented, on behalf of the| The Morley (Yorks) Vocal Union held a competition in 
subscribers, a silver salver and a photographic album to| the Town Hall on February 23. Seventeen choirs competed 
é Mr. J. Spencer Curwen, the founder and president of the | Mr. C. H. Moody, of Ripon Cathedral, adjudicated. The 
: festival. Baptist Tabernacle Choir, Crosland Row Wesleyan, and the 
Nelson Arion male-voice choir were prize-winners in various 
| Soutu LONDON MUSICAL FESTIVAL. | sections. 
The second annual festival, heldin the Town Hall, Battersea, | _—_—— 
on March 2, 4, 5, 6, 7 and 14 was a great success. There On March 15, at a meeting of the Concert Goers’ Cluls 
were nearly 400 entries, and 1,350 performers. The standard | held in Langham I[lotel, Miss Wakefield gave an address 
was generally satisfactory. The ‘ Lady Palmer’ Competition | on the Competition Festival Movement. Mr. W. II. Leslie 
for choral Societies was won by the Essendine (Paddington) | presided, Dr, McNaught and Mr, A. Kalisch spoke

KUM

iford’s fine

three 
Beethoven’s Choral f 
vequitted t

about 160 voices: all the = 
by the

MO N CORRESPONDENT. ) 
The Dublin Orchestral Society gave its first concert of the 
m Febrt The programme was as

Beethoven’s 
s’s ‘Le Rouet d’Omphale’ ; 
‘Tristan and Isolde

There was a good attendance, and the hand

The series of orchestral concerts was brilliantly rounded 
off by that which took place on February 20. Dr. Cowen 
and the Messrs. Paterson deserve unstinted praise and 
congratulations on the success of the season. The programme 
was a plebiscite one, and included the ‘ Pathetic’ 
symphony, the ‘Midsummer Night’s Dream’ Overture 
and the ‘ Ride of the Valkyries ’—all beautifully played 
Miss Marie Hall made quite a sensation by her violin 
playing

charming concert, which was worthy of much better 
support than it received, was at given by the Verbrugglien 
(Juartet on March 7, and the third concert (February 26) of 
the Edinburgh String (Juartet was equally interesting, 
including as it did works by Beethoven, Dvorak and Haydr 
The sixth classical concert (March 16) brought the Brussels 
()uartet, assisted in one number (the Pianoforte quintet of 
Cesar Franck) by Mr. Denhof. Quartets by Glazounow 
and Beethoven were rendered with perfect finish and

homas’s

Opera has been much in evidence during the month. 
T Royal Carl Rosa and the Moody-Manners companies

each gave a fortnight’s performances. <A feature of t] 
Carl Kosa season was the fine presentation of Beethoven’s 
> Amateur performances have been given by the 
eus Club (* Merrie England ’) and the Glas; - 
Music Opera Class (Offenbach’s ‘Grand Duchess’). In

latter work the distinguished appearance of Miss 
Jenny Young, a very promising Glasgow vocalist, merits

special mention

Successful pianoforte recitals have been given by 
Madame Carreho, Mr. Frederic Lamond (a _ Beethoven | 
programme) and Mr. August Ilyllested. \mong other 
performances have been Spohr’s * Last Judgment’ by tt 
Sunday School Union Choir (Mr. Alec Steven, conductor 
Costa’s * Eli’ by the Clydebank Choral Union; and the

the Coatbridge Choral Union, the last two 
Mr. W. J. Clapperton

Mr. Coleridge-Taylor conducted the Symphony Orchestra's 
performance, on February 25, of his symphonic variations on 
an African theme and the £v/racte from ‘ Lierod.’? The

rchestra, under Mr. Akeroyd, also gave fine performances 
4 Tchaikovsky's I and Beethoven’s

Pathetic’ 
* Leonora’ overture

r the of the Orchestral Society, included

Beethoven’s Eiglith symphony

concert, given

evening, sat 
suis Titania’ and Weber's

penultimate 
March 11, Beethoven’s 
ballet suite ‘ La Belle at 
*Le Rouet d’Omphale’ 
he vocalist of the 
* Infelice, Ambroise Th 
* Softly sighs.’ 
The Welsh ¢ 
Bach’s * Passion’ 
March 16. The 
devotional and showed

were

FROM OUR OWN CORRESPONDENT.) 
concerts 3 
e performance of the Mass in 
This third etiort of the choir in this 
I le. The principals were 
Lakin, Mr. William Green 
At the concert of February 21

Beethoven 
Bach 
minor on March 14 
work was the best it 
Miss Agnes Nicholls, 
and Mr. Hjalmar Arlberg. 
there was one more repe the ‘ Pathetic’ 
and Dr. Brodsky played Beethoven’s Violin concerto. 
Bruckner’s Symphony No. 3, in D minor, little 
impression at the following concert, but Madame Carrejio 
scored a great success in a fine performance of Beethoven’s 
E flat symphony was gloriously 
played at Miss Nervil, the brilliant 
\ustralian the vocalist, and Mr. Albert 
Spalding, the played the Violin

h not a little

Mr. Dalton Baker was the

The programme of the Brodsky of 
February 27 included Mozcart’s G, 
Beethoven’s Quartet in and

E minor

L TIMES.—ApriL 1, 1907

Messr Franz S I H D her, Paul Miry and | ‘ Lohengrin.’ Mr. Charles Knowles was accorde 
}. Gaillard, the cultured lful Brussels String (Quartet, | enthusiastic reception for his solos—Henschel’s ‘ \ g 
pr rogr t he Schiller-Anstalt | Dietrich’ and Sullivan’s ‘Thou art passing hence.’ ¢ 
I I r I hree S ann | conce both artistically and financially, is 
(Juar { \ I S I t |} the v ciety has given, 
O ( Mr. ¢ I ; s id Th , Sacred Harmonic Society conclu 
) Juar ( r(Op. 1 y-firs S itl performance of G 
Mr. | fF Pr , rts, | * Reden 22 The artists were Miss 
i S Thr Perceval ladys Roberts, Mr. William ¢ 
’ ‘ r February \ 1 Mr : 
Mr. C1 \ I rts ¢ ] al ] T : 
M St. Patr led ore att officiated at the or 
I gr s I : f On M 
M t S ur 4 dichamber « 
lg i | ll , a 2, di lude 1 T< 
due rt r Dr. Henry Watsor by Brodsky y 
\ ! rps rd, were | performance n sturne inC minor and Balla I 
: c uC pa nts, al G minor, and also accompanied Mr. Brodsky in Bach’s \ 
Dr. W é rps rd Toc concerto in A m This the only opportunit 
Henry Pur I I rami was mos dience had throughout the 1 of hearing Mr. Br y 
" soloist, and his magnificent breadth of tone in B 5 
. polyp! is thoroughly appreciated. The c rt 
conc] 1 fine rendering of Brahms’s Pianoforte 
MUSIC IN NEWCASTLE AND DISTRICT juintet . 
At concer f the Leicester Symphony Or: stra 
O , , Sn : I February 28 the rogramn included M rt’s 
( ( ts February 2 I rte ncer in C (No. 21), Beethoven’s Symphony 
en M } \ r. Carl W , rave a|in D d new Festival March by Mr. | Mar ll 
y = natas by | Ward f N um, conducted by tl composer 
Mozar : I ; we ule | Jessie Adc ] nd Mr. J. A 
f Mr. Borw ' Pia ae Ade 
M S wel erfor by The Leicester lharmonic Societys performan 
S N M S v's cer Elgar’s The | m n Mar 21 , despite some 
. I Mr. Alf W shor n . pon W he Society and its con I 
, Br rt ' ure W ly « t uted Phe ir were hear 
( y 1 grea ad e I Fag © ye priests, nm 
Gr wfos ‘Tr nan | 5 id s lly in the Lord’s Prayer 
lerr | D ( r Fr at a S s were M in 5 r, Miss Gwladys kK 
| . é Mr. Webster M r and Mr. Dalton Baker, each of w 
l S S Or S did splendidl Special menticn ma e mad 
. ! n rogra rat *The sun goeth down,’ the ‘ Arrest’ s t 
} ' r M | ~ y il 1 | I olo ‘ Unto yo that r 
' S rst S ot acl Mr. Ellis, ib] ( leserves cre for 
| ( rtu LAr : é' nterpris¢ 9 r ‘ I ur’s latest orator 
Mr LA I ( I S y 1 Leices’er audien 
, r , Gre \ new ral s y has be formed at Loughbor 
rforn ' | ( Mar : ing fron works selected for 1ts first concer 
t t Mr. M. Fair a) . : ng | fres! " 1 pr $ d celle: work e 
. Choral S Armst: ( rformance on March 5 contained 
New ano Mozart d Walford Davies’ g Thomas’s ‘ Sun-worshippers’ and 
I t men, r Mr. W. G. Whittaker praise.’ The chorus was supported 
[ I ! It 1 Granvil I played the first

I r d | pl ind a piece I 
I Spr , were Miss | 
I N c ( r S Am r } thborough), and 
| uN ' ry re nder Birmingham). Mr. Frank Storer 
‘ r Mr. J. } lefiries It } companied. 
rmer r y \ , separate The Long Eaton Choral Society performed ‘ Elijah’ i 
r 14 t . concert on March 1 The s t Miss Ethel Lister, 
Mr. ( D ; Sulliva ‘ Martv1 atin Miss Ma ters, Mr. Gwily 1 Mr. Dan Pr 
Vas ] M LD S$ 1s retir y from Th chor 1d r str the directi 
e mucl ood and| Mr. J. S. Derbyshi A rendering of 
' wor imiliar worl 
O AY | 2 I or ( r Saris On Mare ( Chor Society sang G 
I ) ~ ' Br Parr | Lloly Citv’ w ul pr S10! The solos were al

1907 261

On |anuary 31 in the same building, and under the auspices | 
ff the Musical Club, a chamber concert was given, its chief | 
features being Brahms’s Serenade for smal! orchestra in A 
(Op. »), Beethoven’s Pianoforte concerto in G—Mr. | 
Dona Tovey playing the pianoforte part in the latter 
work—-and Bach’s Concerto for flute, oboe, violin, trumpet 
nd strings, in F, under the conductorship of Dr. Allen. 
rt was very enjoyable. 
Professor of Music, Sir Hubert 
ebruary 20 his terminal lecture in the Sheldonian

Parry, gave on 
Theatre

auspices of the Musical Club, Miss Fanny Davies, Herr 
eisler and Mr. Whitehouse gave a delightful concert, the 
Brahms’s Sonata for pianoforte and

programme including 
violin in G (Op. 78) and Beethoven’s Pianoforte trio in D 
Op. 70, No. 1), in addition to very 
harmingly played by Miss Fanny Davies

On March 4 in the Examination Schools 
tet, under the auspices of the Musical Union, gave an 
sting concert, the principal pieces being Beethoven’s

in F minor (Op. 95), Mozart's Divertimento for 
, viola and violoncello in E flat and Dvorak’s beautiful 
yartet (‘Aus der neuen Welt’) in F (Op. Songs 
and Legrenzi were contributed by Mr. T. C

Last, thougl 
‘horal Soc ety and Bach Choir were, on

rmonic and ( 
March 14, concentrated upon 
Town Hall, of Beethoven’s Mass in

enthusiastic baton of Dr. Allen, for tl

Mendelssohn’s 13th Psalm, Bach’s church cantata ‘ Bide 
with us’ and Spohr’s ‘ Last Judgment’ constituted the 
programme

At the last concert of the 
Society, Stanford’s quartet \Op. 64) in D minor, Beethoven’s 
quartet (Op. 18, No. 3) and Max Reger’s Pianoforte trio 
in B minor (Op. 2) were performed by Messrs. James and 
J. W. Sharpe’s quartet party

Among other interesting events announced to be given in 
the closing days of were concerts by the 
Sheffield Vhilharmonic Orchestra (conductor, Mr. J. H. 
Parkes) and the Shefiield Amateur Instrumental Society 
conductor, Mr. J. Duffell), also performances of ‘ Acis and 
Galatea’ by the Hillsbro’ Choral Society ; of Mendelssohn’s 
42nd Psalm,’ at St. Mary’s Church ; Tozer’s ‘ Way of the 
Cross,’ at Zion Congregational Church, and Maunder’s 
* Olivet to Calvary’ at Peter’s (Abbeydale) Church

stvie

whe mn ted, ured to secure a very effective per 
formance of Beethoven's seventh Symphony. On Mar 5 
the Philharmonic Society, of which Mr. J. W. Hudson 1s 
conductor, gave a concert the principal feature of which was

Tchaikovsky's ‘ Pathetic’ symphony, and on

Sonata for violin and pianoforte in F were played by Messrs. 
G. E. Cathie, who led the orchestra, and Mr. F. Lewis 
Thomas, and the vocalists Miss Edith Miller and 
Mr. Julien Henry

BURTON-ON-TRENT. The Musical Sociely gave a concert 
in the Town Hall on March 11, when the 
consisted of ‘* Hiawatha’s Wedding-feast,’ Cowen’s * John 
Gilpin,’ and Beethoven's C minor Symphony. The choir 
and orchestra, numbering 170, under the direction of 
Mr. T. E. Lowe, gave an artistic rendering of the first two 
and the orchestra was heard to advantage 
in the symphony. Mr. John Harrison was the solo vocalist 
and Mr. Charles Collier played the solo part in Gabriel 
Pierne’s Concertstiick for harp and orchestra

CaTrorp.—The 
conductorship of Mr. C. W. 
‘Redemption,’ at St. Dunstan’s College, on 
The oratorio was eflectively rendered by a choir and orchestra 
numbering nearly 160 performers. The singing of the choir 
reflected great credit upon the members themselves and upon 
the conductor, the music of the ‘Celestial choir’ being 
rendered by a selected body of boys and men belonging

Chapel, Islington, on March,19

Beethoven

MUSICAL TIMES.—Apri- 1

1540. The Toast

1541. Spring Time. Part-song for T.'r. B. B. 
L. VAN BEETHOVEN § 1d. 
1543. Glory, honour, praise and power. 
Motet, No. 3 - Mozart 2d 
1545. Ye that do live in pleasures plenty

Madrigal for s s.A.1T B. 
1550. Come, let us join our 
Anthem for Easter E Vine HALL 14d. 
Saviour, Thy children keep. Anthem 
for Evening Service A. SULLIVAN Id. 
ii URS, BERTHOLD—* Dreaming.” Arranged for

