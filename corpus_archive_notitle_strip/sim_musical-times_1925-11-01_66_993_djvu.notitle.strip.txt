Because an audience in 1820 recognises, let us say

the greatness of Beethoven, and because one in 1920

did not recegnise the greatness of Bartok, and to

deduce therefrom that Beethoven is a genius and

Bartok is not, may satisfy Mr. Newman, but does not

satisfy me

By what clairvoyance, he demands, have I been 
able to discover ‘that the audience of 1920 is in 
any respect of musical intelligence or artistic sensi- 
bility the same as that of 1820?’ I might ask 
Mr. Sorahji how he knows that the two audiences 
are in any way different. It is surely a reasonable 
presumption that our grandfathers were about as 
intelligent as ourselves, and that they reacted to 
Their 
actual musical experiences, of course, were not 
ours; times change. But I should not imagine 
that the relation between their experiences and 
their intelligence was different from ours. If Mr. 
Sorabji thinks it was, he might give us his reasons. 
But in any case Mr. Sorabji must make his leap 
into the future without the distress of my abhorred 
company—for I am compelled to point out to 
him that his comparison is illegitimate. Beethoven 
has been dead a hundred years; Bartok is stil 
alive. Beethoven’s work is complete, Bartok’s is 
not. Beethoven’s work can have no further 
surprises for us ; Bartok’s may have. Beethoven's 
greatness is now universally accepted ; Bartok’s 1s 
not. The prudent thing is surely to leave a com- 
parison between the generation of 1820 and the 
generation of 1920 to the men of 2020. They 
alone will be able to see the two periods in proper 
perspective ; they alone will have all the documents 
in their hands ; they alone will know which of the 
personalities of to-day has emerged from the crowd 
in the way that Beethoven has emerged from the 
crowd of his own time. They alone will be able to 
say whether the formula I have tried to deduce

_—_

as well as the composer; but to Borwick himself 
such self-expression seemed a side issue, not in any 
It was characteristic 
of him that he was so often to be heard in ensemble 
work of all kinds. In a trio, or quintet, or song 
accompaniment, he seemed to approach the music 
in exactly the same spirit as in a concerto or a pure 
solo: he was there simply to do his share in 
transmitting the composer’s thought, doing his 
duty, neither obtrusively nor subordinately, as the 
musical circumstances might direct

But his art by no means ran in grooves. He 
could be as volcanically stirring as any professed 
emotionalist: I recollect, in particular, a perform- 
ance of Chopin’s F minor Fantasie that was a sheer 
blaze. And he had no unnecessary respect for 
any pianistic orthodoxies as such: his thumbs had 
ways of their own, and he took many a technical 
risk that I do not suppose he ever taught to his 
Brought up as he was in a circle of 
classicism, at the feet of a great artist to whom 
much great contemporary music was alien, his 
sympathies never stood still. Rachmaninov, 
Scriabin, Palmgren, and many more, owed him 
heavy debts ; and he was one of the first Englishmen 
to see Debussy and Ravel with the eyes of their 
own race. And no listener could really say whether 
the greater sympathy went out to Bach or Mozart 
or Beethoven or Schumann or Brahms, or to any 
of the men who might be considerably his own 
juniors

Odd little things might occasionally happen ; 
his normally first-class memory might fail and 
reduce him to circling round and round a page of 
Bach without ever discovering its proper end, or

he might leave out the last two notes of the Zargo

of Beethoven’s Op. 10, No. 3, and be extremely 
surprised to be told he had done so. And, of 
course, like all other performers, he had occasional

982 THE MUSICAL

1 Kocha an . P ractive ices 
Paul Kochanski has a couple of attractive choices Just as my notes are closed, comes the lates

he plays with perhaps overmuch vibrato (10-in.). ‘ : ‘ 
5° the ment as of vocal records one rarely meets} aie pee sey pee a. a a re Pantin, 
with anything so yvood as that of John Coates in the Sinfes fe = 2B che = re ae epee ‘Cuarte: 
Ou lter’s . Blow blow, thou winter wind.’ Morley’s — oo eee ~ $< —_—_. : - 150; Brahms 
* ’ ’ a * °| Sextet in B; and ‘By the Tarn,’ Eugéne Goosse,; 
It was a lover and his lass, and Linley’s ‘ Lawn, as 
white as driven snow,’ the Morley song being the 
vick. It is a model of light, easy singing, breath ' 
control, and rhythm (10-in.) Player-Piano Wotes 
A record of S milar excellence Is the 10 in. of Olga By WILLIAM DELASAIRI 
Haley in Grieg’s ‘I love thee’ and ‘Les Trois 
Princesses,’ a delicious song, apparently of folk- 
origin. Miss Haley sings it with a refreshing beauty 
of tone and rhythm. Chopin provides three important ‘* Duo- 
Good operatic records are of Luella Paikin in|rolls this month, all played by eminent pianis 
the Mad Scene from ‘Lucia’ (12-in.); and Frank} The first is the ever-popular ‘Polonaise’ 
litterton in ‘Your tiny hand is frozen,’ from *La/ A flat, Op. 53 (0231), recorded by Eugéne d’Alber 
Bohéme,’ and ‘Strange harmony of contrasts,’ from| It is certainly a thrilling piece of work, thoug 
‘Tosca’ (12-in. I have rarely, if ever, enjoyed | personally I should prefer rather less of the ‘thun¢e 
a Malcolm McEachern record more than his latest} of horses’ hoofs.’ It is played with astonishin 
Richards’s ‘Captain blaze’ and Sanderson’s ‘The/| verve, and despite the high price is a roll whic 
Old Shepherd.’ The words are remarkably clear,| should create a wide demand. Arthur Rubinstes 
and the first has abundant humour and point (1o-in.). | gives us a group of the Preludes, Nos. 1, 4, 10, 
and 24 (6811). The style of playing does not pleax 
me equally in each, but this is purely a matter ¢ 
POLYDOR taste. I must, however, point out one rather serio 
mechanical defect ; in the last Prelude many esseniia 
A parcel of Polydor records has reached me, some, | melody notes are deprived of accent owing to th 
I believe, not the most recent issues and therefore! accent perforations being cut in the opposite section 
not calling for detailed discussion. I have been/to which they occur. An intelligent owner + 
much struck by the excellence of certain records of | readily correct this, no doubt, but | SUppoxt 
works that, apparently, have not yet been| rolls should be edited having regard to the leas 
recorded in this country. Thus, there is a fine one| measure of common-sense amongst owners, | rea 
of a couple of Wolf’s songs, ‘ Der Kattenfiinger’ and | that critics have belittled the Nocturne in B, Op 
*Verschwiegene,’ sung by Heinrich Schlusnus. In| No, 1 (6443). Certainly it has a rather exoti 
the first the orchestral part is extraordinarily good— | effeminate atmosphere, but in Friedman’s hands 
one can almost see the scurrying rats; the second is| has an unquestionable, if cloying beauty. 1 
notable for the beautiful messa-voce of the singer. | passage, for instance, in which the theme breaks in 
Violinists will enjoy the records of Vitali’s trills is an exquisite example of decorative writing 
‘Chaconne,’ the player being Vasa Prihoda. Here! and I fancy, too, that the work has an addition 
a feature is the accompaniment, in which quiet organ | charm by reason of its being less hackneyed tha 
tone is reproduced more successfully than hitherto, many of Chopin’s works. 
so far as I know. The odd side in four 12-in. is A roll of outstanding importance is Scriabit 
made up with an elaborate version of Schubert’s |‘ Poéme Satanique, Op. 36, magnificently played 
\ve Maria.’ Another outstanding set of records} Leo Sirota (0222). 1 am afraid that its harmon 
are those of Beethoven’s Sonata in F, Op. 24,| style will render it caviare to the yeneral—it s 
for violin and pianoforte, played by Robert Zeilet emphatic ally not a best-seller. Although, of cours 
and Bruno Seidler-Winkler. The pianoforte tone | not in his advanced idiom, it presents many of tit 
inusually good, and the balance could hardly be composer’s characteristics—extraordinarily distinct 
mproved. |‘bitter-sweet’ harmony, ‘wormy’ counterpoint, a 
Of the orchestral records the pick are those of | restless emotionalism—in a remarkable manner. 
Beethoven's B flat Symphony, played by the Berlin} have had the roll too recently to analyse it in deta 
State Opera Orchestra, conducted by Hans Pfitzner. | but it certainly merits the most attentive study 
These succeed where most records of the classical! I cannot help thinking that Liszt's ‘ Mazeppa 
symphonies fail—that is, in clarity and brilliance.| Etude, in D minor (No. 4 of ‘ Etudes d’éxécutien 
I have often felt that some of the conventions of | transcendante (0232), approaches the vulgar. The 
classical scoring, such as the lower-octave-doublings | middle section is a fine example of a Lisztian tunt 
of wood-wind passages, were responsible for the drab | floridly ornamented pianistically very thrilling 
effect of so much of Beethoven on the gramophone, | but the re-statement is really not good. With 
but there is no trace of the fault here. Incidentally, | Emil Sauer the execution is certainly transcendent 
these records have converted me to a Symphony | in fact, it much transcends the inspiration, | thins 
that hitherto has made little appeal to me. | and is a wonderful exhibition of virtuosity

7 = Ps So 4) ‘ Care ? " | ’ . . 
n Raff’s ‘Cavatina’ and Pierné’s ‘ Serenade, which | patch from the National Gramaphonic Society

se a su 
wether

best hand-played rolls are the Schubert- 
hark! the lark,’ played by 
»3), and Grieg’s ‘ Scherzo-Impromptu,’ 
3, I 2 (A825). The latter bears the 
amistakable Grieg idiom, and is a spontaneously 
piece, played with distinction by 
Litolff's Scherzo (A819) and Genevieve 
tot’s playing of ‘ Marcheta’ (A817) were commented 
pon recently in their ‘ Duo-Art’ form. 
‘Beethoven lovers will welcome the issue in straight- 
it form of his Sonata, Op. 14, No. 1 
vigorous music, and the last move 
provides splendid practice in 
entual pedalling. Indeed, the whole work is made 
nteresting or dull according to the manner of 
serformance. Holbrooke’s ‘Queen Mab’ (24648) 50

ne two

The modern market has a prodigious extension. 
moderate talents a world-wide

and Wagner craved for in vain, It is true that they 
enjoyed something which the present-day world 
refuses to its musicians—a stiller air, a chance for 
peace, retirement, and concentration. How to-day 
as 
a Beethoven could do and a Bach had no need of 
doing—to the clatter of publicity, critical, analytical, 
or just gossiping

We may like the position or not, but there it is, 
and the interesting point is the formative effect on 
music. Clearly it has brought about an enormous 
vulgarisation on the one hand, and in opposition to 
that a music of far-fetched subtlety, scrupulously 
hedged in for the few. Prof. Weissmann’s chapter 
is Short, but it starts a train of reflection we can all 
carry on for ourselves. He sums up

ditorship of Dr. Arthur Somervell, recently reviewed 
s these columns, have now been issued in one | 
me by the Oxford University Press (1os. 6¢.). | 
and hardly to be beaten for gift

Volfgang Amadeus Mozart.’ By F. Gehring. 
seph Haydn.’ By Pauline D. Townsend. | 
Felix Mendelssohn Bartholdy.’ By W.S. Rockstro. | 
idwig van Beethoven.’ By H. A. Rudall. 
Sampson Low. 25. 6d. each.] 
[hese volumes are from a series published some 
ago—a good many, if we may judge from the| 
tthat among the contributors was Julius Benedict | 
now re-issued under the editorship of 
sco Berger. A standard biography will of | 
rse bear reprinting over a long period, but as a| 
e the text will need addition or modification, in 
ring the work into line with the latest 
scoveries of biographical and other data. 
arently no such process has been applied in the 
se of the series under notice; and there is a 
sleading absence of any explicit indication that 
e volumes are other than new works. This is 
nfortunate, inasmuch as the reader can never feel | 
fident that the text is reliable in matters of fact. 
¢ publisher's announcement is flamboyant

s

substance; why he made so fine a thing of the 
xonne in {}rahms’s fourth Symphony; why he 
ge the agner of ‘Siegfried’ and ‘Die 
aistersinger tower and glow like a range of Alpine 
ts; why the Tchaikovsky Symphony was child’s 
tohim, and ‘ Prometheus’ a great game ; whence 
e his hing mastery of Holst’s ‘Choral’ 
phony 
L’ SYMPHONY : BY GUSTAV HOLS1

The word ‘tirst’ suggests that Mr. Holst intends to 
perhaps nine, like Beethoven ; or, since 
shas started so late, four, like Brahms. In the lobby 
ter the performance a critic was asked: ‘ Well

is the greater, Holst’s or Beethoven’s?’ The 
estioner was poor in his ignorance, but his question

e others

ecomes instructive when you have to account fo 
answer Beethoven was within the prohibitions 
time. At any point in the Ninth Symphony, 
east In nstrumental movements, there is a 
tto what he may do next. Holst may do what

kes. Not that he abuses liberty or breaks the con

lity of his own style—in so far as he is responsible

the is freer than Beethoven was to create his own 
eto throw open its boundaries. Beethoven 
rusts Sisyphus-like to his peak. Holst mounts in

kit. Modern music is like that—it tempers our 
mest liking with the knowledge that the composer’s

are glad to be able to give in full

achievement It seems appropriate that a word or two should be 
So it is not in a fault-finding spirit that some of} said about Sir Hubert Parry on this night, at this concert, 
its other performances may be described as good and in this Hall; not only because it has most properly 
choral society work and some as being inadequate. been decided to commemorate with a tablet his generous 
Outside the ‘Choral’ Symphony the best achievement] __ gift to the City of Gloucester of the gallery which adorns 
was the Mass in B minor. This, in fact, was festival] this Hall, but because it was just fifteen years ago to-night 
singing. The Qui Tollis, Incarnatus, and Crucifixus| that the gallery was first used for the purpo 
were given with simple purity. The choral muscle} Festival, and, on that occasion, the Mayor of Gloucester 
stood firm throughout those tremendous middle presented an address to Parry, thanking him for his great 
reaches of the Mass that include the Credo and the! service to music generally and to Gloucester. Parry, j 
Sanctus. It was an uplifting and, with Sir Hug reply, said that he hoped the gallery, by increasing the 
Allen to direct it, a considered performance. In the accommodation for concerts, might enable many m 
‘Sea’ Symphony of Dr. Vaughan Williams the choir poor people to enjoy the advantage of fine music, an 
gave what was asked of it by the composer and by so bring more happiness into the drab surroundings 
the conductor, again Sir Hugh Allen. It kept in many lives. More than ever is it necessary 
tune in its wordless contributions to the ‘ Song of do this. He would indeed have been astonished, : 
the High Hills,’ ‘The Eagle,’ and ‘Prometheus.’ It I believe, pleased to know that to-night for the first t 
gave a new est to the Wagner selections. in the history of the Three Choirs Festival (or perhaps 
‘lr. Norman Allin was heard to observe that the of any Festival) a concert in the Shire Hall is be 
chorus of Gibichungs was so tremendous that it listened to by many thousands of people far outside the 
unnerved him. ‘(;00d choral society work’ was limits of the county or of the three countic 
attained n Dvorak’s ‘Stabat Mater,’ \Wood’s Great Britain. The hope he expressed ir 
‘Dirge for Two Veterans,’ Stanford’s ‘ Songs realised, though perhaps not quite in the 
of the Fleet,’ Mozart’s ‘Requiem,’ Brahms’s| anticipated. Parry was a great Three Choirs 
* Niinie’ a mutilated version), Mendelssohn’s} <A splendid series of his works is entwined round the 
‘\Vhen Israel out of Egypt came,’ and Beethoven’s very heart of these Festivals, and one of the greatest 
‘Choral’ Symphony. But in what remains inthe list]! them was sung this morning in its original home. 
the choir did no more than function. Its singing of of the faces most looked for at these jolly gatherings 
Bax’s ‘This worldes joie’ and ‘ Mater ora filium,’ the was his—one of the greetings most cherished was Dis 
\lto Rhapsody ’ of Brahms, Parry’s ‘The glories of | on¢ of the losses hardest to be borne is his. This Fes 
our slood and state.’ Stanford’s ‘Heraclitus.’ tivalin particular had the most intimate connection wit! 
Purcell’s *‘ Soul of the World,’ Palestrina’s ‘ Assumpta | rs ane WOE Oe SN gets smell | pre tos 
, 10) : it is likely that choir and orchestra would all of them 
est Maria’ and Handel’s ‘May no rash intruder’| pave been having tea with him at Highnam, to their 
would not have brought the choir out on top ata great content and his delight. Was there ever a man 
competition festival. ‘The choir had too much to do| who made people feel so happy, or could get the las 
to attend to everything. Underthe heading of ‘tone’| ounce out of life and out cf others as he could? But 
we will imagine an adjudicator’s jottings : Sopranos he put as much into life as he took out, and that is the

fairly bright, no thrill, but pleasant; loss of quality reason that he always had something to spare to hej 
high up. Contraltos—lovely. Tenors—rather loose

ay be purchased from Messrs

p.m.—Women’s Hour. Tlouse-cleaning. (Beethoven. ) 
lin programme in Mai Lith . 
thought Stracciari’s Serenade from the ‘ Damnation of 
ist’ apoor effort. I never heard a less Satanic sneer: it

inded rather like an old gentleman clearing his throat in

lust realise its worth and import. Adverting to the

it had many points of interest, too many, indeed, 
tdetailed reference, but particular commendation may be 
n formance of two movements of Beethoven’s

more apposite

WILLESDEN GREEN AND CRICKLEWOOD Ciioral So 
(Mr. F. W. Belchamber).—* The Messiah’ : ‘A Ts 
of Old Japan’; ‘ Hiawatha’s Wedding-! east’; ‘ Fass 
(Berlioz

Wii ON Cuorat Society (Mr. Kenneth A. Brow: 
—‘ Choral’ Symphony and Mass in C (Beethoven

WimeikEpON CuurcH CHORAL Societ Mr. F, Wilme 
Bates). —‘ The Pied Piper of Hameli Parry); ‘ The 
Banner of St. George

Psalm (Elgar); 
Mass in C min

Mass in C (Beethoven): 20th 
Battle of the Baltic’; ‘ Requiem 
Cherubini) ; ‘ Lauda Sion

BIRMINGHAM FEstTIvAL CHORAL Soc!e1 Mr. Adrian C 
Boult).—* Elijah’; ‘Job’; Mass in D (Ethel Smyth 
Sea’ Symphony; ‘ Alto Rhapsody’ (Brahms); Ma 
in C minor (Mozart); ‘The Messiah

chietly orchestral 
* Re-awakening ’ 
elix Weingartner ; 
‘ Adrian 
ORA!. Union (Mr. Hopkin Evans). 
Berlioz); ‘ The Messiah’; ‘Choral’ Sym

phony (Beethoven); * Requiem’ (Brahms). 
ANCHESTER, THE HALLE Concerts (Sir Hamilton 
Harty). —The Hallé Chorus sings ‘ Messe des Morts

Berlioz); ‘ The Messiah’; Mass in B minor; ‘ The 
Ap

Society gave

The winter series of Sunday 
ty shestra began on October II. Beethoven’s 
ny was the principal orchestral item, and 
also Eveline 
Massenet’s ‘ Le Cid,’ and was 
pecially delightful in the Flower’ of Schumann. 
——Mr. (sustav Holst was the visiting conductor at 
Symphony concert of the City Orchestra on October 13. 
ec Hfaydn’s E flat Symphony, No. 09, and his 
wn ‘ Beni-Mora’ Suite The * Fancy to Holst’s 
4 * symphony was conducted by Mr. Adrian C

Variations were given.—— Miss

Music in Scotland 
GLASGoW.—The British National Opera Company fare 
much better this year with its two-weeks’ season at 
Glasgow. Full houses were the rule, and at the close

Mr. Frederick Austin made the gratifying announcement 
that it would not be necessary to make any call on the local 
guarantors. The performances maintained a good standard 
throughout, although the programmes could hardly be 
described as enterprising, the only novelty being Bach’s 
‘Coffee Cantata,’ staged as ‘Coffee and Cupid.’ This item 
attracted a large audience, doubtless in part as a result of 
the staging last spring by the Glasgow Bach Society of 
another version of the same delightful little work. The 
most interesting feature of the Glasgow B.N.O.C. season 
was to us the special matinée performance of ‘The Magic 
Flute’ for senior school-children, which quite filled the 
theatre with an eager and enthusiastic audience. This is a 
development that is full of promise.——At the annua! 
Chamber Concert Week of the Glasgow Orpheus Choir, 
which to music-lovers has now established itself as one of 
the great events of the musical season, the Léner String 
Quartet played fourteen complete quartets and twenty 
quartet movements by Beethoven, Borodin, Brahms, 
Debussy, Dvorak, César Franck, Goossens, laydn, 
Mendelssohn, Mozart, Ravel, Richter, Respighi, Schubert, 
Schumann, Smetana, Tchaikovsky, and Hugo Wolf. Of 
the novelties, the ‘()uartetto Dorico’ of Respighi, written 
last year for, and dedicated to, the Léner ()uartet, failed t 
make any great impression, much as we ourselves liked it

The

Sonata f

nit, 
| Corner,’ Debussy; Trio in G, No. 2, slow movenn 
and Fin ule, Beethoven. Mr. E. T. Davies, the Director 
Music, is giving a series of ten lectures on the ‘ Foundation 
of Music and Musical Appreciation during the present ters 
T'wo have already been delivered, the illustrations includig 
| Handel’s Violin Sonata in E, several folk-tune arranger, 
for strings and pianoforte, and movements from 
Beethoven, Brahms, and Dvorak. ——An nteresting inp 
vation in the activities of the Music Department this sess; 
is the establishment of six school lecture-concerts for +h, 
secondary schools of the district. Four hundred child 
have become subscribers to the series. The concerts, whi- 
are given on Friday afternoons, are approved by the loc: 
education authorities, and are allowed to take the place ¢ 
the last school lesson on that day. The first concert t 
place on October 16; the programme comprised : [nt 
ductory remarks on Melody and Rhythm, with illustration: 
Trio in G major, Mozart; slow movement from Ty 
Op. 11, Beethoven; Finale from the ‘Horn’ T; 
Brahms: movement from String (Quartet The Bird 
Haydn ; and two Welsh Miniatures for pi te, violir 
and ‘cello, by Mr. E. T. Davies, who is directing 
mecerts and delivering helpful explanatory notes ont 
pieces played and upon the ‘art of listening.’——T 
Choral Society is preparing Bach’s ‘ Christ 
and Vaughan Williams’s Fantasy on Christmas carols 
performance in December. The orchestra is engaged 
the ‘New World’ Symphony and Moz art’s Symphony 
G minor, along with some lighter items

Bryn TANAT.—The fifth annual Festival of the Nor 
Montgomeryshire Musica! Association (of which Mr. W 
Leslie is a leading spirit) took place on October 18, Part 
of the ‘ St. Matthew ’ Passion was given, under the direct 
of Mr. Arthur Fagge, a feature of the performance being 
the singing by school-children of the Choral in the fr 
chorus. The principal soloists were Madame Laura Evan 
Williams, Dr. Ben Davies, and Mr. W.'R. Allen. Parr 
*Blest Pair of Sirens’ was also sung under the direction 
Mr. Adrian C. Boult; a number of orchestral selectior 
incluied Beethoven’s ‘Leonora’ Overture (No. 3). Th 
choral side of the Festival showed great improvem 
Mr. Leslie and his supporters are to be congratulated 
this definite advance in art and technique

SARDIFE.—The first of a series of * international celebr 
concerts was given on October 12. Madame Fre 
H{[empel was the vocalist, assisted by Franz Mittler (pian 
forte) and John Amadeo (flute).——It is proposed 
arrange for a testimonial to Mr. T. E. Aylward, the veter 
conductor of the Cardiff Musical Society, i nition

work of any importance, and partly because of the strongly 
individual character of such works, It was that of 
Willem Pijper, who is not unknown outside Holland, but 
who will in all probability loom large in the coming 
years. His second Symphony, played at the opening 
concert, is powerful alike in its ideas and in their treatment. 
It is not a long work, but is very elaborate and demands 
an unusually large orchestra. It is written for strings, 
four flutes, oboes and cor Anglais, four clarinets, 
three bassoons and double-bassoon, eight horns, four 
trumpets, four trombones, tenor horn, contra-bass, tuba, 
organ, four harps, three pianofortes, celesta, drums of 
arious kinds, tam-tam, bell, clapper, castanets, xylophone, 
and tambourine. The composer’s rhythmic sense, his 
complete control of his forces, and his ability to build out of 
a brief motive melodies of a broad, striking quality save the 
ecoming too involved or overwhelmingly noisy. 
Seriousness prevails in the general sentiment, though there 
is some piquant satire, and occasional outbursts of sheer joy, 
and at the end of the fourteen minutes it takes to perform 
there is present the feeling that not a single note is too much 
or fails of its significance. The Sonata for flute and piano- 
forte, played by Joh. Feltkamp and the composer, and the 
part-song, “Heer Halewijn,’ sung by Sem _ Dresden’s 
Madrigal Society, had more straining after effect. 
latter work, too, required more rehearsal, and possibly also 
a slightly larger choir, to give a right impressiom of its 
qualities. Yet it was Dresden among the moderns—or the 
ultra-moderns, for Diepenbrock is not yet in the historical 
sense a classic—who gave us the most striking work after 
the Symphony of Pijper. This was a String (Quartet based 
on two old Dutch songs, ‘Er was een sneeuwwit vogeltje 
and ‘Het daghet uijh den Oosten.’ Here again we had 
perfect lucidity, beautiful writing for the strings, rhythm 
that never flagged though it was drawn out to the utmost 
tenuousness, and an emotional driving force that made 
everything in the work inevitable. So far from hindering 
his work as a composer, at least in quality, whatever it 
may have done in quantity, Mijn. Dresden’s appointment as 
Director of the Amsterdam Conservatoire 
other hand to have aided it

Even these works, however, with all their f and 
beauty, were put into the background by the performance 
of Alphons Diepenbrock’s ‘The Birds’ of 
Aristophanes. It was a happy idea to give a complete 
stage performance of this work, for it was only thus that the 
full effect of the music could be judged. And what music 
it is! That lovely, rich-toned Overture, with its suggestive 
touches which are the artistic culmination of Beethoven’s 
experiments in the ‘ Pastoral” Symphony ; melody, harmony, 
orchestration, all are beautifully combined to express a 
summary of the beauty and fun and satire of the play And 
then the melodrama in which the orchestra carries along 
the voice of the speaker (in this case by no meansa powerful 
speaker) as a vessel along the surface of the water. Not a 
feature that is in the slightest degree harsh or too loud, not 
a disturbing element of any kind. No wonder that 
Mijn. Evert Cornelis and his Utrechtsche Stedelyk Orkest 
And to close, the fine choral 
orchestra! One need not 
except that it was 
whole, and 
Dutcl

thre

HOLLAND

The summer season at the Scheveningen Kursaal camet 
an end on September 15. For several weeks there b 
been little that was exciting, though not to the exclusion 
of interesting events, one of these being the appearance a 
conductor-composer of Hubert Cuypers. Cuypers is know 
as a facile and always devotional Churcl 
these predilections were evident in his Overture to Vondel’s 
‘Adam in Ballingschap’ and in the music to Heine’s ‘Dit 
? of which the words were declaime 
by Rodie Medenbach. Both in thematic construction ar 
in instrumentation the two works revealed great skill, a 
exceptional gift of melody, and some feeling for moder 
harmony. A greater effort might have resulted in more 
originality. At the same concert Schneevoigt conducte 
Haydn’s Symphony in G (‘ Parisian’) and Beethoven’ 
* Pastoral.’ Other local artists have been Annie Tydeman 
(a violinist who played with some success in London |. 
year, and who on this occasion showed excellent taste 4 
well as ability by playing one of the more neglected Bruc 
and Mia Peltenburg, the last named singing 
Senta’s Ballad and songs by Ropartz, Wolf, and Hendrik 
Andriessen in an extremely charming manner. The Musici 
Amateur Orchestra, conducted by F, E. A. Koeberg, accom 
panied Madame Peltenburg, and the players gave a goo 
account of themselves in Cherubini’s ‘ Anacreon ’ Overtur, 
the ‘Reformation’ Symphony, and the *Casse- Noisette 
Suite. On September 8, Erica Morini, the young Vienne 
violinist, made so great a sensation by the purity of be 
tone and the grace of her rhythm in Mocart’s A majo 
Concerto, that she was promptly engaged for the concert 
on the following Friday, when she had equal succes 
in the Tchaikovsky D major. At the ‘Soloists’ concert o 
September 13 the items were contributed by members 0 
the Residentie Orchestra. Several of the artists had already 
| appeared earlier in the season. In the afternoon a concert 
performance of ‘Der Freischiitz’ was given by the Roman

composer, an

takes

Beethoven’s

f

A SHORT FORM OF SERVICE FOR ARMS S) E DAY.” 
ive and cx mprehensive Memorial Service comprising suitable Hymn Prayers, at espo! 
en, and suggestions for ** The Last Post and Réveille Benediction, &c

ervice Price. 1s ut Post and R Irraneed for reap 
j t ha lh ords only Conervea . p 
va 7 TC . 
ORGAN MUSI(¢ 
RCH IN “SAUI .. Handel ¢ 6d MARCIA FUNEBRI Beethoven 63019 d 
LEGIE HEROIQUE. Tone-Poen MARCHE FUNEBRI Chopin 63018 6d 
larry Farjeon 57356 1s. 6d LAMENT ( 6d 
WERS OF THE FOREST PRAYER AND FUNERAL MARCH 
tuart Arche Is

W

Flotow 
ercy E.. Fletche 
. SJ. D. Davi

Beethoven

ML A. G

