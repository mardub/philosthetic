THE C ae PSALTER

for Chanting, Words y, together with the 
MINIATURE MUSIC EDITION OF HYMNS ANCIENT AND 
MODERN 
(1889 Eprrion). 
Imperia (s y 4s es), India Paper NET 
R M s. od. 
Turkey M ros. ocd 
k LEATHE oe . t2s. 6d 
THE CATHEDRAL PRAYER BOOK 
With Cantick Psalter Pointed for Chanti together with the 
MINIATURE MUSIC EDITION OF HYMNS ANCIENT AND 
MODERN 
(1889 Eprrion) 
Imperi mo ( y i ) India Pape ET 
Rk AND M € ros. ¢ 
I KEY Mos 12s. 6d 
t A LEATHE 1 " 
I Novi A ( ANY, Limited, 
NOVELLO'’S EDITIONS. 
rHE PIANOFORTE WORKS OF 
BACH, BEETHOVEN. 
HAYDN. MENDELSSOHN. 
MOZART. SCHUBERT. 
SCHUMANN, 
M ! N Edi t ¢ tt ul pri

TIMES

obvious duty to tell me. If he won't, I assume

ter to nest be can’t. Now when a teacher says to me, 
ry fond gi is is a rule that must never be broken except 
to belier jet certain circumstances that justify the 
u lack tigjpeeaking of it,’ I naturally expect him to tell 
and woe how to recognise these circumstances when 
> a ligupearise. It is no use his telling me under what 
1 are no ymstances Bach or Beethoven or Wagner broke 
mpositigggt ule. If I am only to break it under the 
as to tyme circumstances, I am merely copying the 
r that yogpaat composer without understanding why. What 
pond wig rant to know Is when and how I may break the 
se myself. If my teacher does not tell me that, 
isnot training me properly. But of course, as 
T quickhfpe wil admit, he cannot tell me that. He is simply 
too meagea vicious circle: the rule is the right way of 
s. Thesimg things, but the opposite way—the wrong 
omplishegay—is the right way if it sounds right. I submit 
compose this is not playing the game: in business it

WO rate

ENGLISH >. 
THE

FINGERING. 
MUSICAL TIMES,’ 
Sir,—It is easy to imagine the jubilance with whig 
| Mr. H. C. Tonking’s suggestion to re-adopt English fingering 
in pianoforte music will be received by patriots all over th. 
country. For nearly thirty years our treacherous teacher 
and editors of music have been doing their wicked utmost t 
establish a uniformity in fingering, quite regardless of the 
fact that the sequence of figures I 2 3 4 5 represents , 
hideously Teutonic aspect of affairs ! 
| Mr. Tonking tells us that publishers would be glad to prin 
English fingering once more, but that is nothing to th 
| radiant gladness that would illuminate the countenances 
small music-dealers all over the country at the prospect oj 
selling off their old stocks of * Popular Classics’ and early 
Victorian copies of Beethoven, which have lain on their dusty 
shelves so long ! ; 
May I venture to call attention to another terrible evil ip 
|our midst? It is horrible to think that some dastard) 
| teachers of Harmony are still allowing their pupils to writ 
| the chord of the German sixth, and may I suggest that som

TO EDITOR OF

Orchestral Society and Crystal Palace Chots

W. Hedgcock).—Merrie England ; The | 
Olaf Trygvason (Grieg) ; Beethoven’s C minor

and a number of British orchestral works. 
Phith fy (Mr. Martin Klickmann).— 
Faust (Gounod); Paradise and the Peri (Schumann) ; | 
The banner of St. George ; Judas Maccabveus ; The 
Martyr of Antioch ; Schumann’s ‘ Rhenish ’ Symphony. 
Weste Railway Musical Si (Mr. H. A. | 
Hughes).—War and Peace (Parry). 
Si (Mr. Claud Powell).—Madrigals | 
and part-songs. | 
y (Mr. Claud Powell

London Concerts. 
QUEEN’S HALL SYMPHONY CONCERTS

A ‘safe’ programme was chosen for the first of the 
season’s Symphony Concerts, given under Sir Henry Wood, 
on October 17. The Symphony was Beethoven’s Fifth, and 
the Concerto that of Tchaikovsky in B flat minor, played, 
to the great satisfaction of a large audience, by the youthful 
Solomon. Brahms’s ‘Tragic’ Overture and Sir Henry 
Wood’s arrangement of a Suite in G for strings from the 
organ Sonatas of Bach completed the programme

THE CLASSICAL CONCERT SOCIETY

THE MUSICAL TIMES.—Novemper 1, 1914. 667

ee MT 
RTS is on November 23. The artists who appeared were 
oo an yjss Florence Macbeth, the American co/orafusa soprano, 
nt chambe, dame Ada Crossley, and Mr. Robert Radford (vocalists), 
mes are, xB yis Isolde Menges, the talented young violinist, a pupil of 
‘St rank anf ponold Auer, of Petrograd, and Monsieur de Greef, 
} take plac je accomplished Belgian pianist, Mr. R. J. Forbes acting 
mall charg 3s accompanist. The programme submitted naturally 
> Orchesin, iluded patriotic songs and popular items which were 
for Practice B eatly appreciated by the audience. 
ace Sunda * fy place of the Quinlan opera season the Theatre Royal 
mmagement arranged for a fortnight’s popular patriotic 
Promenade Concerts, from October 12 to October 24, at 
er 35 wher pices to suit the masses. The orchestra was that of the 
> at Queer’ B Theatre Royal, augmented to forty performers, ably 
eit Lowe onducted by Mr. Harry Rushworth. The programmes 
vere principally composed of light and popular as well as 
uiriotic items and operatic selections from the works of 
ng Concer f Sullivan and Edward German, the classical school as well as 
October ; ff the romantic being entirely excluded. A vocalist or an 
e Grenadie f instrumentalist appeared each evening, all the artists hailing 
f interest: fF fom the Midlands. The experiment proved quite successful, 
and at least provided some employment for local orchestral 
payers, who have been so hard hit this season. The 
Abert Hi Bimingham Festival Choral Society have not yet made 
1y Orchesn ff i: known whether they will give their projected series 
jon of Mr of Choral Concerts, nor have the various local amateur 
1d Madincff oral bodies notified the public of their resumption of the 
‘ernoon, ts cstomary season’s concerts. 
at ()ueen 
lirection — ' 
iven to 4 BOURNEMOUTH. 
lief Fuiff A highly attractive prospectus of the autumn_ season 
een’s Hi} amangements has been issued by the Winter Gardens 
Committee, and little difficulty seems to have been 
encountered in planning a comprehensive and very attractive 
Red Cus griesof events. First and foremost, the Symphony Concerts 
7 by Prt and Monday ‘ Pops’ are bound to rejoice the hearts of those 
‘stra a5 G9 who support the best type of music. No stupid boycott of 
me Kiki the music written by the great German masters of former 
and othesi® dayshas been instituted, although it is understood that no 
vere prexem living Teuton or Austrian will find representation. 
The inaugural Symphony Concert was given on October 8, 
the programme including Mackenzie’s lively ‘ Britannia’ 
it succes Overture, Parry’s ‘ English’ Symphony, a work of solid 
- Kennett? worth, and Delius’s charming Two Pieces for small 
apart I orchestra (first performance at these concerts). Mr. 
nselves, HR York Bowen’s performance of Beethoven’s Pianoforte 
Cowen, § concerto in E flat was a model of soundness and steadiness, 
S Stan and in the orchestral items Mr. Godfrey and_ his 
een's lif instrumentalists repeated their successes of former years. 
est a bi The first Monday ‘ Pop,’ on October 12, was devoted to 
ed. Ith British music by living composers, the following being the 
& pall decidedly interesting selection made: Two military marches, 
) HH. MT ‘Pomp and Circumstance’ (Elgar); Overture to a comedy 
Was SR (Balfour Gardiner) ; ‘ Mock Morris’ and ‘ Shepherd's Hey’ 
ninary © (Percy Grainger) ; ‘In fairyland’ suite (Cowen) ; Overture, 
vefit of SH ‘The land of the mountain and the flood ’ (MacCunn) ; Irish 
Rhapsody (No. 1) by Stanford, 
Pavlova filled the bill—and also, be it said, the Winter 
Gardens—on October 5 and 6, her wonderful powers of 
dancing being exemplified in an extremely varied series of 
§, dances. A recital by Miss Marie Hall, at which the 
concert-giver was heard to the usual advantage, has been 
theonly other event of importance up to date. 
Mr. Philip Cathie, examiner and professor of the violin at 
the Royal Academy of Music, and Mr. S. H. Braithwaite, 
professor of theory at that institution, have recently been 
‘pointed to the staff of the Bournemouth School of Music

was 104 Seer 
) accoul 
the pe: BRISTOL. 
, ol is . . . 
a d In consequence of the war, several musical Societies of the 
ci

Of music in Yorkshire there is still little to chronicle. The 
chief event has been the first of the Bradford Subscription 
Concerts, on October 9, when Mr. Thomas Beecham 
conducted the Hallé Orchestra, and made a_ distinct 
impression by his fiery and brilliant readings of such things 
as the ‘Thamar’ of Balakirev, and Berlioz’s ‘Carneval 
Romain’ Overture. He also gave a fine performance of | 
César Franck’s Symphony, which at last seems to be 
entering upon a period of comparative popularity. Very 
enjoyable, too, was a Handel Concerto in E minor, a 
remarkably fresh and vigorous work. Mr. John Coates was 
the vocalist, and we had two examples of the art of another 
native of Bradford in Mr. Delius’s recent pieces for small 
orchestra, ‘ The first cuckoo’ and ‘Summer night on the 
river

On October 3 the Leeds Choral Union gave a concert on 
behalf of the War Relief Fund which, if not of great artistic 
significance, served its purpose as a patriotic effort. Extracts 
from Handel's martial oratorios, ‘Judas Maccabeus’ and 
‘Israel in Egypt,’ with various National Anthems, were 
sung with great verve under Dr. Coward’s direction, and 
Mr. H. Brearley and Mr. William Hayle, both of the Leeds 
Parish Church Choir, were excellent vocalists. The Leeds 
Saturday Orchestral Concerts opened a season of six concerts 
on October 17, when Mr. Fricker and the Leeds Symphony 
Orchestra offered an attractive programme of well-tried 
orchestral music, including Beethoven’s Violin concerto, of 
the solo part in which Mr. Rawdon Briggs gave an artistic | 
interpretation. There was a very large audience, whose

here i oo. ag = 
enthusiasm indicated that there is a plac for such concerts, | £ Deskuen. On Card. 24

