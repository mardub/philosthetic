une business , Musie 
 Business , dispose . apply W.S. , Messrs. 
 Novello , Ewer Co. , 69 , Dean - street , Soho , London . 
 wax TED purchase good second - hand 
 copy GOSS ORGANIST COMPANION ( com- 
 plete work . ) send price H. Curtis Card , Lewes 

 EW ORGAN ARRANGEMENT 
 BEETHOVEN " HALLELUJAH CHORUS , " W. J. 
 Westbrook . Davies ’ Catalogue , 417 , Oxford - street 

 CHORAL festival . ss 
 prece response , 
 LITANY , compose Tats . arrange Voices 
 JoserH BarnBy . price 4d . 
 FERIAL response , LITANY , use 
 Church 8 . Andrew , Wells - street . edit J. Barnsy . price 44 . 
 simple FORM INTONED SERVICE.—THE 
 PRECES RESPONSES , LITANY , set Monotone , 
 Organ Accompaniment , specially adapt Parish Choirs , 
 JosePH BarnBy . price 4d . London : Novello , Ewer Co 

 MUSIOAL TIMES.—Jvnz 1 , 1867 

 gracefully write , doubtless favourite ; 
 Mr. Lahee ' * ye wood " eminently successful . 
 madrigal , W. Beale ' * come , let join roundelay , " 
 Ward " die , fond man , " Marenzio " lily white , crimson 
 rose , " Gastoldi ' * Maidens fair Mantua city , " 
 attention variation tone 
 soul composition . vocalist 
 Madame Maria Vilda Mr. Sims Reeves , 
 excellent voice . Madame Vilda singing ' * Qui la voce , " " Casta 
 Diva , " * " Son vergin vezzosa , " create furore 
 audience ; encore second song powerful 
 resist . listen Mr. Reeves singing 
 Handel Recitative Air , * * deep deep , " " " Waft 
 angel , " hear perfect interpreta- 
 tion occasion . equally successful 
 Beethoven * " Adelaida " Kiicken " twilight darken , " 
 encore . - Miss Agnes Zimmermann , 
 fulfil prediction , place 
 accomplished pianist day , Mendelssohn 
 " Capriccio , " Op . 33 , excellent taste finished execution , 
 play Gavotte Bach , b minor , 
 Chopin Valse flat . recall platform , 
 gaveMendelssohn " Volkslied " minor , retire amidst 
 enthusiastic applause . Mr. Benedict accompany principal 
 singer skill judgment consummate artist . 
 concert extremely attend ; Mr. Barnby , 
 steadiness intelligence conduct increase confidence 
 - discipline force command , warmly 
 applaud conclusion concert . successful 
 attempt hearer hopeful future 
 choir ; look forward utmost interest series 
 concert advertise season 

 MR . HENRY LESLIE CHOIR 

 concert Mr. Joseph Heming Choir 
 gth ult . , St. James Hall , programme 
 entirely devote glee , quintett , choral composi- 
 tion Sir Henry Bishop . delicacy 
 refinement choir subject toa 
 rigid training ; composition encore 
 enthusiastically . song evening ; 
 Mr. Walter Bache solo pianoforte , Messrs. J. B. 
 Chatterton Cheshire perform duet harp . 
 concert attend 

 Concert Mr. Charles J. Hargitt , 
 St. George Hall , 10th ult . , deserve record 
 attempt conscientious professor place 
 know work Beethoven english public . 
 doubt reason composition familiar 
 majority concert - goer simply account inferiority 
 nore popular work ; contain great beauty 
 unquestionable ; Mr. Hargitt deserve credit 
 public opportunity judge . " Praise 
 Music , " " Calm sea prosperous voyage " 
 rarely hear ; itis highly probable ex- 
 cellent performance work occasion , concert- 
 giver think worth revive . concert 
 respect highly interesting ; pleasure 
 attention entertainm utterly unlike 
 benefit concert constantly chronicle . 
 principal vocalist Madame Lemmens - Sherrington , Miss Rose 
 Hersee , Miss E. Cole , Madame Laura Baxter , Mr. Cummings , 
 Mr. Weiss . Mademoiselle Mehlig solo pianist , 
 orchestra complete excellent department 

 Mr. Marcellus Higgs concert 8ta 
 ult . , St. James Hall , produce operetta 
 composition , * * noble Moringer . " libretto , present 
 Mr. Higgs , gentleman sign ' * Amicus , " 
 means interesting ; scarcely fair judge 
 composer , , employ power subject 
 genial suggestive musical idea . Operetta ex- 
 ceedingly receive audience ; graceful 
 ballad encore . principal sustain Miss 
 Louisa Pyne , Madame Patey - Whytock , Mr. Patey Mr. Cummings 

 Aptommas , singing Herr Stepan , Mannheim , 
 romance Lortzing , Meyerbeer scena Roberto , 
 display bass voice admirable quality power 

 Mr. Ridley Prentice Concert 13th 
 ult . , Hanover Square Rooms , perform true 
 artistic taste good execution pianoforte work , 
 successful Beethoven Sonata appassionata , 
 Mendelssohn Sonata D , pianoforte violoncello , 
 join Signor Piatti . play Minuet 
 Trio , compose , exceedingly receive . 
 vocalist Miss Louisa Pyne , Madame Patey - Whytock , 
 Mr. Patey . Mr. Walter Macfarren conductor 

 tue annual performance Messiah 
 benefit Royal Society Musicians , place 3rd ult . , 
 St. James Hall . principal vocalist Miss Robertine 
 Henderson , Madame Sainton - Dolby , Madame Talbot Cherer , Mr. W. 
 H. Cummings , Mr. Patey , Mr. Weiss . Trumpet obbdligato Mr. 
 T. Harper . Professor Sterndale Bennett conduct 

 CuatHaM.—The Lecture Hall occupy 
 past month concert party , 
 generally successful . follow artist , 
 service ; retain respective programme , 
 viz . , Miss Grace Armytage Madame Somerville , ' " Mackney , " 
 Mr. H. C. Sanders , vocal department , Madille . Bertha 
 Brousil , violin , Madlle . Cecile Brousil , second violin piano , 
 Madame Antonette Grant Brousil , piano , Monsieur Alois Brousil , 
 violin tenor , Mr. Henry Nicholson , flute . , 
 violin solo Madlle . Bertha Brousil successful 

 CuicHEsTeR.—On Tuesday , 7th ult . , Morning 
 concert Chamber Music Assembly Rooms , 
 Mr. Thorne , organist Cathedral ; assist 
 Messrs. Ries , Paque , Goodban , Miss Julia Elton . chief 
 point interest programme grand trio D 
 minor , Mendelssohn , Beethoven sonata G@ , 
 Pianoforte Violin ( op , 30 ) perform MM . 
 Thorne Ries . M. Paque play Boccherini Sonata 
 violoncello , song effect . 
 room half fill , audience evidently appreci- 
 ate music perform 

 CigENCEsTER.—On Thursday , 16th ult . , 
 Cirencester Choral Society performance Haydn ( ' reation 
 Corn Hall . principal vocalist Miss C. Westbrook , 
 soprano , Mr. Green , tenor , Messrs. Brandon Ruperti , bass . 
 band select Bristol , Gloucester , Cheltenham , 
 Stroud ; Messrs. Chew Woodward leader . Mr. E. Brind 
 conduct . amateur belong Society lead 
 satisfactory way . chorus 

 Andantino ; . 
 . 5 , Adagio ; . 6 

 ' edition . paper cover , 1 . , cloth , 
 MURLE WESTMINSTER ABBEY CHAN'I 
 BOOK , contain 189 single double chant , 
 follow author : — Alcock , Aldrich , Attwood , Ayrton , Bach , Bar- 
 row , Battishill , Beethoven , Bellamy , Bentinck , Blow , Boyce , 
 Brownsmith , Chard , Dr. Cooke , R. Cooke , Corfe , Coward , Croft , 
 Crotch , Davy , Dupuis , Dyce , Ebdon , Elvey , Farrant , Felton , Fitz- 
 herbert , Flintoft , Foster , Gibbons , Goodson , Goss , Greene , Gregory , 
 P. Hayes , W. Hayes , Henley , Hindle , Hine , Hopkins , Kelway , Kent , 
 King , Lamb , Langdon , Lawes , Lee , Lemon , Macfarren , Miller , 
 E. G. Monk , Morley , Mornington , Nares , Norris , Ouseley , H. Purcell , 
 KE . Pureell , P. Purcell , Pye , Rimbault , Robinson , Russell , Smith , 
 Soaper , Spohr , Tallis , Tucker , Turle , Turner , Turton , Walmisley , 
 Weldon , Whitfeld , Woodward . 
 London : Novello , Ewer Co 

 service 
 compose 

 publish Monthly Numbers , 2 . . number 1 60 , Volumes , handsomely bind 
 cloth , 36 . 

 , content volume . content Vol . ii . ( continue . ) 
 1 , Adagio Grand Fugue c minor * ee Mozart| 9 Tarchetto fr irst Sv , 0 
 double chorus , * * like unto thee ? " .. * Handel " — — Som Se — — ea , er oe 
 r > . 1orus , " lovely messenger Mendelssohn 
 introduction creation « - Haydn Allegretto fi th artett ( op 59 ) Beethover 
 chorus , ' * trust " Dr. Croft Sener Sees San ee ( op 5 = 
 . ' Andante Sixth Quartett mi ve es Mozart ™ . oo " es } mp ne 7 amg 
 eee eee wee 2 " j " ii 
 rr " eng otett , esu dulcis memoria " ... eee ~ iihler 
 Ge crane Spell . Alans ej(ltemeewet ) " = Sei 
 ‘ nig = - . 34 , ' horus , Fat ner mercy ' oe eve ove ande 
 goal tage rom state te _ Motet tt ,   Splendente te , deus " « Mozart 
 - cee . ne cis Air Chorus , " Dal tuo stellato soglio ee Rossini 
 3 , aa ba Sy eer C minor ... .. Beethoven | 35 , overture , " Fall Babylon " ai aan Spohr 
 ser Cloth , choral Fugue Motett eee ese Spohr oe ore PS 
 P , gilt , 4 , Andante Quartett D minor eee ee Mozart | 9 g Gavotta , pomp pe ee maneat 
 ) ' chorus , * * Kyrie Eleison " ... Schneider | " * os 
 , Concertante ( c major ) ... ow eos « - Handel 
 ] aot Chorus , * * round Starry Throne " - » Handel Motett , " o salutaris Hostia " ; Auber 
 80F Chorus , " ' wretched Lovers " ve « + Handel | 37 , ' air Chorus , " holy ! holy ! " kae Spohr 
 ) 5 { 6 , Larghetto Symphony D oe « + » Beethoven : Chorus , " giv h bas ‘ od 3 
 } va - 1orus , * thank unto God exe oes Spohr 
 ; Pe , ( op . ray Celestial Concerts " o ee Andante P. F. Bagatelles ' Beethoven 
 7 80 ] 1 ) " se te sulla Chorale , ' sleeper wake ! " " Mendelssohn 
 | Canzonetta Quartett . Mendelssohn Arioso , * * Lord mindful ' Mendelssohn 
 } double chorus , * * sing unto Lord " « e Handel } gg , fugue P. F. Works ‘ ii bs Bach 
 i. Aria s Sonata nie tie . — 39 . Romanza , g major ( Op . 40 ) po ooo Beethoven 
 te aydn March ( posthumous Work ) ace « Mozart 
 8 , double chorus , * Fix'd everlasting seat ie Handel Chorus , " rend sky ' , ... Handel 
 — fugue — — _ — cantata « . Reissiger|49 , March ( Scipio ) . » Handel 
 horus Motet eR ' és ae os Bach| ' " 1 - ' 
 9 , overture , ' judgment " ... pre ae Spohr . _ bow thank oi sie fp 
 March Pianoforte Duets ... ie eae Weber . . bi 
 19 , Andante QuartettinF ... os ons Haydn ntent volume iii 
 b minor chorus , * * Heavens tell " ne sa Haydn : ool ps wanes sie " _ ; _ y 
 ' 11 , Wedding March .. 7 = Mendelssohn 41 . Adagio , Symphony C major , . 7 aes Haydn 
 Motett , * * deus tibi " " ne Mozart Choral Fugue , Dona nobis pacem , " Mass Bminor Bach 
 12 . Andante Pianoforte Duet ... ' ..   Dussek| , , Choral Fugue , " Kyrie cy mess tn @ majer . = 
 Adagio Finale Quartett Cc ! Spohr 42 , overture Oratorio " S Paul eee Mendelssonn 
 13 , air , " ask yon Damask Rose sweet " Handel Adagio , Pianoforte Duets e Weber 
 Choral Fugue , " Pignus Future " .. , " ne Mozart Yhoral Fugue , * * sicut locutus est , . Mag- 
 Air Chorus , " non sdegnare " oe Gluck | nificatind ... : ove Bach 
 14 , chorus , ' Queen Summer " wd : Handel | 42 + Adagio , Sy mphony e flat . . 4 . - » Haydn 
 Schiller — March os ce " Mey estier Allegro , Sonata Pianoforte 
 Sinfonia ( Semele ) pe esi ae .- Handel Violin oe " 9 Bach 
 15 , chorus , * hallelujah " ... ... Beethoven Finale second Sonata Pi anoforte Violin Baeh 
 Larghetto Ninth Quartett sie Mozart | 44 : chorus , * God thou art great , " sacred Cantata Spohr 
 16 , Andantino Symphony , " power Larghetto 12th Grand Concerto . - Handel 
 sound " 2 Spohr Motett , * * Misericordias Domini " ... eee « . Mozart 
 Romanza Symphony " La Reine de } ejFranee ' " Haydn | £ 5 Andante , Symphony e flat a. A. Romberg 
 , military Overture ( c major ) ; Menislasshis Chorus , * soon tow'ring hope ( Joshua ) ... Handel 
 18 chorus , " die far soon " . Handel ] , , Andante , Symphony e flat . no.2 . .. Haydn 
 overture , * * Athalia ™ Bh Handel | 46 2 = Recordare Jesu pie , Requiem Mass ... Mozart 
 Motett , " Jehovah , Lord God Hos — yi Spohr Choral Fugue , " * et vitam venturi , " mass Dmajor Righini 
 19 . chorus , " king Cesar " ... . Handel ] , , L4r8o , Fourth Quartett e flat ... Haydn 
 Offertorium , * * Alma Virgo " aa = ... Hummel | 47 + " { deep . " 130th Psalm se oe Spohr 
 March , ( eg t PG ape Funeral March Pianoforte Sonata , op . 3 35 ... Chopin 
 arch , ( Egmont ) ove ove ove » Beethoven s y 
 0 , March , ( Hercules ) ac . Handel Adagio Pianoforte Duet § onat b flat .... Mozart 
 Larghetto Clarionet Quinte tt ity . Mozart | 42 Minuet , overture ' Berenice eve .. Handel 
 overture , Samson " ie : iv Handel ra — r — _ = 
 Minvet , Instrumental Concerto ... ‘ aa ach 
 content volume . 49 . Grand March ( op.40 ) ... pet F , Schubert 
 ° 1 , Andante sonata ( op . 45 ) eee Mendelssohn Chi rus , " * ail pane Joshua " ose Handel 
 Funeral March ( Samson ) eve ae « Handel | 50 . ' Pastoral Symphony Christmas Oratorio « .. Bach 
 _ dead — : lel - ats aie .. Handel Chorus , ' joyful , ye redeem , " sacred 
 horus , cria excelsis " ove eee Bach Cantata Festival St. John Baptist Bach 
 2 , % ovecture , oe pant op el ; eee « . Handel Adagio , sonata Violin Pianoforte 
 ndante , variation ( septuor « ns Beethoven G minor ooo sas eee po oon Bach 
 nn % , March C major ( Idomeneo ) ... acs , Mozart ] 51 . ' Pater Noster . " Motett Chorusand Orchestra Cherubint 
 erman March Solennelle ( Op . 40 ) oe « . Schubert Sarabanda , second Violin Sonata ane Bach 
 = . oo vt ( Op . hall , eee ove Spohr Chorus ( ground Bass ) , " Envy , eld bear 
 ouble Chorus , * * people shall hear " « » Handel Hell , " Oratorio Saul . Handel 
 Air , * * thou shalt bring ' aes Handel | 52 . allegro marziale , Pianoforte Duets ( op . 60 ) Weber 
 Fugue B major ( 48 prelude F ugue , ' . 47 ) Sach Bour ée Second Sonata Violinalone ... Bach 
 ' ' M : arch ( Joshua ) ove . « + Handel | 63 . Triumphal March ( Tarpeja ) oe Beethoven 
 t 28 . 25 . Coronation March ( Le Prophite ) — _ Meyerbeer Military March ( post humous Work ) Beethoven 
 Adagio P. F. Sonata ( Op . 2 oe Beethoven | 54 . prelude aud Fugue F minor " Suites pour 
 dy Air , " Arm , arm ye ! brave _ le Clavecin ™ eco .. Handel 
 ia cc horus , " come ' oun rceabsous ) J oe fandel Air , " honour arm , " ( Samson ) : « . Handel 
 - a. Chaconne ( d minor ) ove Bach Minuet Pianoforte Works ve 3eethe ven 
 41 , chorus , " thee let ev'ry soul subject " + » Handel ] 55 . Adagio Religioso ( Lobgesang ) ' Mendelssohn 
 Air Cc ' horus , * marv'lous work " eee Haydn Choral Fugue , ' * * let sing hallow praise 
 Andante trom P. F. Duets ( op . 2 ) ove Weber ( op . 96 ) ... ove Mendelssohn 
 Air , " * Cangio d’aspetto " . ee « . Handel Chorale , * o god'thou holy Lord " Bach 
 Andante P. F. Duets ( op . ‘ 60 ) ooo eco Weber | 56 . Adagio Fourth Symy J ‘ aa Seethoven 
 28 , chorus , " traitor desery " ene Handel Allegretto Scherzando ith Symphony Beethoven 
 - Finale , e major ( op . 52 vse seg Schumann | 57 . overture * * Esther ™ ane vieted Handel 
 pr'vices , Aagio Notturno ( op . 3 pan ve spohr Andante fifth Qui ntett . Mozart 
 % . overture , ' * Saul " eve oe eve Handel | 58 Adagio grand Fugue c m ajor " ( 5th sonata 
 30 . Air Chorus , " Placido ? 2 il Mar " wd Mozart Violin ) ont Bach 
 Andante Symphony ove Mende Issohn | 59 overture Ope era * Porus ” ans Handel 
 March Notturno ( Op . 34 ) oe Spohr egretto Seventh Symphony : ethoven 
 31 , Chorus , " thou let man " ane pam Handel |60 Allegro Maestoso Serenade e ‘ flat , 
 Musette Sixth Grand Concerto , .. Handel wi ind Instruments . eo eee ane Mozart 
 Chorus , * * come , gentle Spring " ove ove Haydn Chorus , * let peace > joy " 
 Air , * sin , 0 king ° eg Handel Church Cantata Palm Sunday eve ' Bact 
 Mareh , f major ( Idomeneo ) ee ee Mozart ; 
 London : Novet.o , Ewer anp Co. , 87 , Regent Street , 69 , Dean Street , Soho , 35 , Poultry 

 XUM 

