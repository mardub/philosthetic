40, all kinds. Mr. Cassini, Finsbury House, Wood-green, twelve 
minutes’ ride from King’s-cross Station

THE MUSICAL TIMES.—Decemsrr 1, 1868. 599 
of, THE M U S I C A L, T I M EK S written ; and, indeed, so numerous are these criti- 
rate, ?| cisms, that the mental process necessary to compre- 
\ers~ And Singing Class Circular. hend the meaning of a composer appears to spring up 
= ” spontaneously in corners where we should least 
1 to DECEMBER 1, 1868. expect it to exist. ‘lake, for instance, the following 
- be notice upon one of Schumann's Etudes Symphoniques - 
, for sil mies : ‘A gradual ascent from a level country, through 
— DESCRIPTIVE MUSIC. various elevated regions, to a culminating summit, 
: By Henry C. Lexy. composed of stupendous rocks of terrific aspect and 
io Owe of themany good pieces of satire in Sheridan's difficulty, amid which the presiding genius is one of 
1 the Critic, is where Lord Burleigh gravely comes forward |°W° and fear, and where the pole-star of the beautiful 
ber tothe front of the stage, shakes his head, and then begins to pale in the dark obscure, while grace and 
a walks off, without uttering a word. “Now, pray,” loveliness have been left far below. This image may 
, (says Snecr), “ what did he mean by that?” “ Why, symbolize that tremendous study. And if & few 
bs: by that shake of the head,” replies Puff, “he gave! Recs Wh games Ppa a P —_ ys berg 
two you to understand that even tho’ they had more jus-| me of a a yond b nek jc Pr he 
+ &e, tice in their cause and wisdom in their measures, yet, |C@@ OF 1¢ came an uncoud aie brie 1 ame, Of f . h 
ana if there was not a greater spirit shown on the part of | b hole yy md esrs eet if “ty atened by a 
ling the people, the country would at last fall a sacrifice Ak newer from that Dns e-star which one thought had 
enth, to the hostile ambition of the Spanish monarchy.” | 2°C? lost to view.” a 
on “The devil!” says Sneer, ‘‘ did he mean all that by| fe , Aang, indeed, — oo musical —— 
= shaking his head?” ‘Every word of it,” replies! did he mean all that ge nano ygne a | ‘Every 
eel Puff, “if he shook his head as I taught him.” Now,|™ ord of It, replies the — Puff, if the performer 
artly this is doubtless very amusing; and we Jaugh at it! had played * ” I taught him. hens 
sh accordingly ; but does it ever occur to those who are| We can readily believe that were essays like these 
’ inthe habit of talking, thinking, and writing about) Upon the philosophical meaning of every composition 
mer music, how thoroughly this absurdity is constantly | to become a recognised necessity, the effect would be 
Fas acted upon with regard to the works of our modern! not only that those works which are incapable ot 
Apply composers? Do we not continually find that a/ such analysis must inevitably be laid aside as wi 
aig deeply philosophical intention is attributed to a writer| Worthy of the advanced intellect of the age, but that 
ALE. by those'who fancy that they alone possess the power| musical pieces written to illustrate a metaphysical 
36, Old af diving into his real meaning; and that, con-| chain of thought, would be seized upon in preference 
———— sequently, an wopretentious work is thus often ab-| to those in which mere melodious beauty, symmetry 
Be. ally elevated beyond its legitimate place as a| of proportion, and variety of character form the 
Apply musical composition? If it were doubted that it is| chief attractions. Before we proceed, however, to 
the fashion of the day to enlarge upon the inner! ¢xpress any opinion upon the possibility of such a 
roned meaning of a modern German work, for instance, we | result, Jet us endeavour to discover how far this mode 
1 solid have only to turn to the laudatory synopsis placed in| Of criticising musical compositions has already spread, 
—— thehands of the audience whenever it is performed,| The great iostrumental works of Beethoven and 
na and every word of which the hearer is supposed to| Mendelssohn are so constantly before us in the pre- 
fn endorse, or to confess, with a penitential look, that) sent day, that we may safely accept them as a test of 
t front he is utterly unable to appreciate it. ‘Ihe influcnce | the manner in which compositions of such pretension 
. a thus exercised upon criticism is, of course, sufficiently | are received and commented upon. Were it porsible 
% apparent ; for as those who have studied deeply a for us to reproduce one half of the inflated nousense 
—- work, unhesitatingly tell the public what the com-| that has been written and said about Beethoven's 
Ly power thought and meant when he wrote it, no mere| Sonatas, we might fill a moderately-sized volume. 
ps. J. tyro in the art could afford to speak of it in a less|‘* How exquisite,” we are told, ‘is the dreamy beauty 
“Lyne. elevated strain ; and thus we are perpetually called | of the ‘ Moonlight Sonata.’ Do we not almost seein 
armo- won to believe that a mental problem is clearly |to see the silvery moonbeams over the grass ; and, by 
— Worked out in a Sonata, and a profound system of| the pale light, appear to wander over the well-known 
Appli- philosophy developed ina Symphony. ‘That it is the| meadows, pausing at intervals, as some thought of 
Parade, mission of music to awaken a train of ideas in the!|the past reealls us from the contemplation of the 
ae mind of an auditor is universally admitted; but that) lovely scene to the dull realities of the world 2?” Very 
NT) is these ideas should be the same with all is a manifest] pretty, no doubt, is all this maudlin poetry ; but, un- 
Ser dity. A composer may have a real intention | fortunately for the writer of it, Beethoven never 
uiness. widérlying every passage in his work ; but he would| called it the “ Moonlight Sonata” at all; and pro- 
ersholt- tarely be satisfied with the interpretation of these} bably had no more idea of moonlight when he com- 
> oo even by the most intellectual thinkers, | posed it, than he had in any other of the works which 
DES, it would be utterly impossible for them to do|he has bequeathed to the world. Again, in the 
uniques, more than hazard a series of guesses on the subject—| Sonata in D (usually called the “ Sonata Pastorale”), 
wet pd the hearers being compelled to translate music into] there can be little doubt of the pastoral character of 
rs only, Words, whilst the composer translates words into|the composition; but the title, ‘Sonata Pastorale,” 
eo music, “Many persons,” writes Mendelssohn, in| was never given to it by Beethoven. In some of the 
> tone, ‘me of his letters, “consider Melusina to be my best| greatest Symphonies of the same composer, are we 
se ‘from Werture ; at all events, it is the most deeply felt;|not told that the aim of Beethoven has not been so 
m-court- 48 to the fabulous nonsense of the musical papers} much to write a great musical work, which shall live 
tred coral and green sea monsters, and magic|and be remembered for its artistic beauty and ex- 
ob Lot Palaces and deep seas, this is stupid stuff, and fills| cellence, as to depict, by the aid of sound, a definite 
: mewith amazement.” i ; ‘philosophical idea, in which the auditors shall be 
’ stupid stuff,” however, continues to be carried forward, by the genius of the wyitcr, into the

XUM

We shall, of course, be reminded

these devices to heighten the effect of their music; 
and that even Beethoven, in his ‘ Pastoral Sym- 
phony,” has introduced the sounds of the cuckoo and 
the nightingale; but we cannot allow these few 
instances to shake our conviction of the truth of the 
theory we have advanced. Most assuredly neither 
Handel nor Haydn have done more than vulgarise 
their music by venturing beyond the legitimate pro. 
vince of an art which they have done so much to 
elevate; and, exquisitely as Beethoven has written 
the passage to which we have alluded, we should 
have infinitely preferred the ‘‘ Pastoral Symphony” 
without it, because the suggestive character of the 
work (which all must feel to be its great beauty) 
would then be in perfect keeping throughout. Were 
we to seek for an illustration of this principle, we 
could not mention one more to our purpose than the 
performance of this very composition at the Pro- 
menade Concerts of the late M. Jullien. In the 
storm movement, the house was darkened, and 
theatrical thunder and lightning were introduced, to 
aid the intention of the composer. This was received 
with thunders of applause; whilst, at the same con- 
certs, Mendelssohn’s purely suggestive overture to 
the ‘‘ Midsummer Night’s Dream,” was most uncere- 
moniously hissed. ‘That the effect of these two 
works upon a mixed audience, played in precisely the 
same manner, would now probably be exactly re 
versed, is a proof that those who can derive enjoy- 
ment from such compositions at all, have educated 
themselves to the belief that descriptive instrumental 
music, whether bad or good, can never be improved by 
effects borrowed from an art with which it must, 
necessity, be antagonistic

Believing, therefore, as we do, that the te 
mission of music is to suggest, we regret to see the 
growing tendency to analyse a composition beyond 
what we conceive to be the intention of the composé 
in his original conception of the work. As we have 
already said, such criticism may lead to the utter 
destruction of melody, beauty, and form; 
eventually we may be taught to believe that unles 
musical works can be translated into words, they 
cannot be received as the highest productions of the

SACRED HARMONIC SOCIETY

Tue thirty-seventh season of this Society commencea 
on the 20th ult., with a performance of Costa’s Naaman, 
Madame Lemmens-Sherrington singing the soprano part 
with much effect; the other principal vocalists being 
Madame Sainton-Dolby, Mr. G. Perren, and Mr. Santley. 
Mr. Costa, as usual, conducted. The Crown Prince and 
Princess of Prussia were present. Weare glad to find 
that Beethoven’s Mass in D will form one of the attractions 
during the season

GENOA

The local papers speak in the highest terms of 
the singing of Miss Alice Phillips (a daughter of the once 
famous bass singer, Mr. Henry Phillips), who, at the 
Saturday Popular Concerts, at Birmingham, created quite 
asensation. She is said to possess a contralto voice of 
good quality and power. We hope shortly to weleome 
her at our concerts in the metropolis

Tux Amateur Musical Society of Brixton 
opened its twelfth season of private subscription concerts 
during the last month, at the Angel Town Institution, 
Gresham Road. ‘There was an excellent and varied 
programme; and ample justice was done to most of the 
pieces performed. ‘The principal vocalists were Miss 
Fanny Holland and Mr. Alfred Hemming, both of whom 
were received with the utmost favour throughout the 
evening. ‘There were also several well-chosen instra- 
mental selections, which were most effectively given, 
especially Beethoven’s Symphony in C minor, by the 
orchestra—and Mr. H. Weist Hill, who has succeeded the 
late Mr. C. Boosé, made an excellent conductor. Mr. 
Harrison was the accompanist

Tue following paragraph, so thoroughly in 
accordance with our views on the “ status ” of musicians, 
is from a sensible article called ‘¢ Mr. Semibreve’s Grumble,” 
in the new periodical, Vanity Fair. “I see many letters 
and complaints in musical journals about the position and 
social status of musicians, and I hope my brother artists 
won't be angry with me, or think me wanting in modesty, 
if I ask them whether it is not their own fault if their 
status is not as good as they would like it to be, whether 
a man’s position in any calling, provided it be a respectable 
one, does not depend pretty nearly on himself alone. 
Music is a divine art, and a musician, in the highest sense 
of the word, has a noble calling, and one for which he 
need never blush ; therefore, if he respect his calling, he 
will respect himself, and others will respect him. Don't 
let us parade the shop, nor, on the other hand, be ashamed 
of it, and above all, don’t let us make our art so cheap and 
common that people do not care to buy it, but run after 
other wares. Lady May Phayre once pressed me so rudely 
to play that I felt bound to give her ladyship a rebuff,and 
said: ‘My dear Lady May, here is my friend Millais, 
also an artist. Why don’t you ask him to sketch 
portrait of yourself and some of your family, for the 
amusement of the company; he would be gratified, J am 
sure, by having such a compliment paid him?’ And sinee 
then her ladyship has never asked me to any of her musical 
parties, and there wasa coolness between us for some time; 
but this last season she graciously sent me a card for her 
ball, and I went and enjoyed myself immensely

No. 5, “ Dulcissimum convivium,” is an air for soprano. 
It is graceful, admirably vocal, and contains some charm- 
ing effects from a long sustained note in the voice part 
against the motion of the accompaniment; but it is one 
of the least interesting portions of the Litany

A modulation in the closing symphony links this last 
piece to No. 6, “ Viaticum in Domino morientium,” which 
is else totally distinct from it. This consists of a melody, 
which, if it be not an ancient Church tune, is a close 
imitation of the style and form of those curious relicts of 
an early stage of our art’s history. It is assigned to the 
choral soprano voices only, which will give to it marked 
distinctness and prominence through the harmony of the 
florid instrumental figures. There is a break between 
each two of its strains after the manner in the Lutheran 
Choral, which break is filled up by the continuation of the 
accompaniment that forms an interlude between strain 
and strain. We have here, then, a treatment that has 
been commonly practised by the ecclesiastical writers of 
North Germany, and is very generally supposed to be 
peculiar to them as to the requirements of their Service; 
and it will be interesting, if evidence present itself, to 
trace whether the instance before us be a single appro- 
priation by Mozart of the Lutheran use to the Roman 
ritual, or a recurrence by him to a discontinued custom of 
his own Church that has been preserved in the hymnody 
of the Reformed religion. The question bears upon the 
construction and the manner of performance of the hymna 
tunes of our own Church, and may well repay, in an 
antiquarian and ritualistic sense, the pains of investigation. 
There are many analogous examples of the accompall- 
ment of Chorals in the Church music of Bach ; but 1 may 
point to one, more familiar than any of these, in the 
treatment of the second verse of the hymn, “ Nun danket 
alle Gott,” in Mendelssohn’s Lobgesang. A well-known 
specimen of Mozart’s similar treatment of a Canto fermo, 
which, however, is far more elaborate, is the piece for the 
two armed men in his Zauberflite, which, as it illustrates 
a ceremony of the Egyptian priesthood, perhaps implies 
the composer’s belief in the remote antiquity of this 
manner of hymn singing. The movement which 
Beethoven defines asa“ Song of gratitude, offered by & 
convalescent to the Divinity,” in his Quartet in A minor, 
is another instance of the employment of this same form 
by a professed member of the Roman Communion; and 
these several examples show that it was as well known t0 
the musicians of the south, as to those of the north, if 
less cormmonly practised by them

No. 7, is the noble chorus in E flat, “ Pignus future 
gloriw.” The bold subject set to the opening words is

last a prayer for mercy, offered to the Divinity under| Service, but shuts it not out from our Church. There is 
many successive titles. This character is heightened by |no reason therefore, why this most interesting work should 
the exquisite device of a recurrence to the theme of the | not be performed atany of the musical festivals, especially 
“Kyrie” that is the threshold of the composition, which |at those held in Cathedrals, where the surroundings of the 
steals back upon us with so gentle a sweetness, and derives place invest the music with such associations as better 
such new effect from the soft hues thrown upon it by|befit the solemn nature of the words and the thoughts it 
recollection, that it is impossible to rise from a perform-| expresses, than do the promptings of a secular Concert

ance of the work without a happy feeling of consolation. 
(ur composer has herein anticipated the artifice by which 
Beethoven imparts a special beauty to the conclusion of 
his Mass in C, where a like reversion to the idea with 
which the composition opens, seems to clasp the whole 
with a peculiar unity that shows the entire work to be the 
ample development of one ever pervading thought

There is incorporated in this edition a selection of 
Scriptural texts, which are ingeniously adapted to the 
accents of the music; but as they represent not the sense 
of the Latin words, so they fail to interpret the musical 
expression. I feel it to be the peculiar, nay, the inviolable 
duty of an adapter to translate the true spirit, if not the 
very syllables, of the text to which music has been written, 
since this is the only means of doing justice to the com- 
poser’s conceptions, and of showing his poetical as well as 
his technically musical qualities. I protest, therefore

Few men have suffered so much injustice at the hands 
of some members of the English profession as M. Gounod. 
Time was, and not long ago either, when his opera Faust 
was assailed on all sides, and nothing was considered too 
strong in condemnation of the presumption of a French- 
man in fancying he could write anything larger than the 
flimsiest of comic operas. “ Superficial French polish,” 
“thin veneering,” “ military bombast,” and “ windy 
pomposity,” are a few of the choice epithets which were 
liberally bestowed on this great composition ; whilst the 
nervous trepidation exhibited by operatic managers and 
music publishers, before the opera was submitted to the 
ordeal of public criticism, has become a matter of history. 
However, the opera was brought out, and the public 
applauded, and its first success was only the precursor to 
a furore almost beyond precedent. ‘Then it was thought 
advisable by its opponents to ‘‘ trim” a little, and “ want 
of melody ” took the place of the older and less elegant 
phrases. But when excerpts began to be whistled by boys 
in the streets, and squeezed out of asthmatic accordions, 
even this last bulwark was evacuated, and on all sides it 
was agreed that Faust was a great work

About this time rumours began to circulate to the 
effect that an adaptation of a grand Mass in G to the 
requirements of a London church was taking a firm hold 
on the affections of the worshippers and others. It soon 
became known that this Messe Solennelle was the rejected 
of the custodians of musical art of fifteen or twenty years 
ago, when it was first heard under the direction of Mr. 
Hullah; and a new set of phrases were collected together 
to stigmatise the grandest contribution to the service of 
the church since the two Masses of Beethoven astonished 
and delighted the world. ‘‘ Theatrical tinsel,” ‘* operatic 
tawdry,” and unfair comparisons between the sacred com- 
positions of Mendelssohn and Gounod, were the means 
employed to injure the popularity of the great French 
musician, and, as before, they were utterly without avail. 
For, M. Gounod is not the man to put everything on the 
cast of one die; he cares not whether his Messe Solennelle 
becomes popular or not, but continues writing, and each 
succeeding composition increases, or at least consolidates, 
his hardly-won reputation

The subject of the present review is one of the latest of 
these compositions, and perhaps one of the purest pieces 
of inspiration ever conceived by this composer

A pleasing melody, well written for a mezzo-soprano voice. 
The accompaniment flows throughout with the air, chiefly 
in thirds and sixths; and the harmony is unobtrusive, and 
in character with the extreme simplicity of the com- 
position

Lamporn Cock, ADDISON AND Co. 
Three Marches. Composed by Handel, Mozart, and 
Beethoven. First Set. 
Three Marches. Composed by Handel, Gliick, and 
Beethoven. Second Set. 
Arranged for the Pianoforte, from the Full Score, by 
C. Graham Gardiner

In the first set of these arrangements, we have Handel’s 
March from Scipio, Mozart’s trom Die Zauberfléte, and 
Beethoven’s from Fidelio; and in the second set, Handel's 
March in the “Occasional Overture,” Gliick’s from 
Alceste, and Beethoven’s from the Ruins of Athens. All 
these are well arranged, and will form effective pianoforte 
pieces. We especially admire the Marches from the 
‘Occasional Overture,” Fidelio, and Die Zauberflite, the 
harmonies in which are full, and judiciously distributed 
between the hands. Arrangements like these should be 
eagerly sought for by all teachers who desire to cultivate 
a taste for good music amongst their pupils

ASHDOWN AND Parry

we are again compelled to postpone the continuation of

the “ Incidents in the Life of Beethoven

Brief Summary of Country Nels

certed pieces were admirably rendered; and many were 
loudly encored. The Concert concluded with the National 
Anthem

Crirron.— <A highly successful Pianoforte 
Recital (forming one of Mr. J. C. Daniel’s series of Clifton 
Winter Entertainments), was given by Madame Arabella 
Goddard, on the 11th ult. Mozart’s Sonata in B flat 
(No. 5), Beethoven’s Grand Sonata in D, Weber’s “ Invi- 
tation pour la Valse,” with other pieces, by Thalberg, 
Heller, and Chopin, formed a most attractive programme. 
All these compositions displayed Madame Goddard’s ex- 
ceptionally fine powers to the utmost advantage. The 
vocalist was Miss Edmonds, who was most favourably 
received in all her songs

DeptrorD, 8.E.—A competition for the post 
of Organist, took place at High Street Congregational 
Chapel, on the 21st ult. Miss E. Stirling (organist of St. 
Andrew’s Undershaft, E.C.), was the umpire. The suc- 
cessful candidate was Miss Orr, of Christ Church, Cubitt 
Town. After the competition, Miss Stirling, (by special 
desire), played a short selection of organ music

Liverpoot.—The Ninth Subscription Concert 
of the Philharmonic Society, given on the 3rd ult., was 
miscellaneous one. The principal vocal artists were 
Madlle. Sinico, and Signori Mongini and Foli, whose per. 
formances were chiefly from the well-known répertoire of 
operatic songs, duets, and trios. The only novelty in this 
department was a charming song, sung by Madlle. Sinico, 
and encored, “ Chi godere vuol’ ore di vita,” composed by 
Burgmiiller (originally for Madame Clara Novello). The

Les Abencerrages,’’ Weber’s “ Ruler of the Spirits,” and 
Auber’s overture to ‘“ Le Serment.” The gem of the 
evening, however, was Spohr’s Sinfonia in C minor, No, }, 
the first time of performance in Liverpool. The Concert 
was a highly successful one; the music going with great 
spirit, and the applause being most enthusiastic— Tr 
Tenth Concert of the Philharmonic Society was also a mis 
cellaneous one. The solo performers were Madlle. Ilm 
de Murska, Signor Bulterini, and Herr Ernst Pauer. ‘The 
great feature of this Concert was Beethoven’s Sinfonia in 
B flat, No. 4, which was excellently played. The over. 
tures were that to Zuryanthe, and Lindpainter’s Prelude 
to Faust. Herr Pauer was admirable in his pianoforte 
solos, the ‘ Andante Spianato” and “ Polonaise Brillante,” 
Op. 22, of Chopin, with the slight orchestralaccompaniment, 
which has only recently been performed for the first timein 
England; and, lastly, a brilliant transcription of Adolar’s 
Romance, and the Hunting Chorusin Zuryanthe. The most 
remarkable of the vocal selection were a curious and highly 
ornamented Hungarian air, sung, with great execution, by 
Madlle. Ilma de Murska, and a light and attractive Swabian 
chorus, given by the practical members of the Philhar 
monic Society. Both pieces were encored. The Concert 
concluded with a spirited repetition (by desire) of Me 
delssohn’s Cornelius March

Mancnester.—The performance of Mr. Costa’ 
Oratorio, Zli, at Mr. Charles Hallé’s Concerts does not 
appear, if we may trust the opinion of the local press, to 
have met with much success; a large number of the 
audience having left the room before the second part wa 
far advanced. The principal vocalists were Madame 
Lemmens-Sherrington, Madlle. Drasdil, Mr. Cummings 
Mr. Montem Smith, Mr. Merrick, and Signor Foli. Most 
of the solo music was well given ; but the weariness of the 
audience is stated to have been apparent throughout the 
entire work. The greatest success was the unaccompanied 
Quartet, “ We bless you,” (sung by the four principl 
vocalists), which was encored

SHEFFIELD.—Faweett’s Oratorio of Paradise 
was given in Portmahon Chapel on the 2nd ult., Miss 
Harrison, Miss Hides, Mrs. Greenwood, Mr. Birtles, and 
Mr. Cawton being the principal vocalists. Mr. W. Stubbs 
accompanied on the organ.——Madame Arabella Goddard 
gave a pianoforte recital on the 4th ult., in the Music Hall, 
The programme was almost exclusively a classical one; 
and several of the pieces were re-demanded.——On Friday 
Evening, the 23rd October, the Sheffield Choral Union held 
its Annual Meeting, when a satisfactory balance sheet was

uced, and votes of thanks were passed to the chairman, 
Mr. J. Shirley, and the other officers of the Union. A few 
glees were sung by the members before the meeting sepa- 
tated.——On Wednesday, the 28th ult , the Sheffield Har- 
monic Society held its first Concert for the season in the 
Alexandra Music Hall. The principal vocalists were Miss 
Banks, Madame Patey-Whytock, Mr. Patey, and Mr. Sims 
Reeves. The choral glees, by the members of the Society, 
numbering seventy voices, under the direction of Mr. S. 
Suckley, were well sung,and received much applause. Herr 
Willem Coenen accompanied all the vocal pieces; and the 
solos given by him were enthusiastically received, espe- 
cially “ Now, or never’ (a*composition of his own), in 
answer to an encore, for which he gave a very clever ar- 
tangement of ‘* The Last Rose of Summer” for the left 
hand only, Altogether the concert was a great success; 
and reflected much credit on the Sacred Harmonic Society, 
and its indefatigable secretary, Mr. W. Shirley——Miss 
Clara Linley, of Low Ash, gave her Annual Pianoforte 
Recital, at the Music Hall, on the 29th October. The pro- 
gramme included Beethoven’s Sonata in A flat (with the 
Funeral March), Woelfl’s Sonata, “ Ne plus ultra,” Han- 
del’s « Harmonious Blacksmith,” and several other pieces, 
which successfully tested Miss Liniey’s power of interpret- 
ing the most varied styles of composition. In all these 
the was listened to with much pleasure, and rewarded 
with hearty applause. The vocalist was Miss E. Spiller, 
who made a most successful debit on the occasion

STANWELL, By Staines, MrppiEsex. — A 
Reading and Concert took place on Wednesday, the 27th 
October, in the Boys’ Free School, to the entire satisfaction 
of alargeaudience. The Colnbrook Church Choir united 
With the Stanwell Church Choir, under the able direction 
of Mr. R. Ratcliff, organist of Stanwell, and choirmaster

of Colnbrook, and a very excellent programme was given 
with much effect. The readings, by Sir John Gibbons 
and Mr. Hutchins, were highly effective. At the con- 
clusion of the performance a vote of thanks to Sir John 
Gibbons (who presided) was proposed by Mr. Churchwarden 
Ward ; and after the singing of the National Anthem, 
the party separated

WakerreLtp.—The Festival of All Saints’ was 
commemorated at the Parish Church, on Sunday, the Ist 
ult., with somewhat less, perhaps, of the grandeur of past 
years, but with no less excellence in the musical portions 
of the service. The versicles on this, as on all festal 
occasions, were those by Tailis, with the organ accompa- 
niment. The services, morning and evening, were by 
Henry Smart, in F, sung for the first time. The anthem 
in the morning (necessarily a short one), was by G. A. 
Macfarren, “ Now then we are no more strangers and 
foreigners ;” that in the evening was from Spohr’s Last 
Judgment, ‘* And lo! a mighty host, &c. ;” and after the 
sermon Beethoven’s “ Hallelujah to the Father.” It is 
needless to remark that the organ accompaniments, under 
the hands of Mr. Emmerson, were perfect; though 
through the breaking down of the hydraulic apparatus for 
supplying the wind, during the singing of the Nunc dimittis 
he had to labour under almost insurmountable difficulties

Warrineron.—The Warrington Musical 
Society, under the able direction of Dr. Hiles, gave a very 
excellent Concert on the 12th ult. The vocalists were 
Miss Katherine Poyntz and Mr. Wallace Wells. Miss 
Poyntz made a most favourable impression upon the 
audience, and she was much applauded in all her songs. 
Mr. Wells was also effective in the music allotted to him, 
especially in his two duets with Miss Poyntz. Amongst 
the part-songs, the most successful were Mendelssohn’s 
‘“¢ Victors’ Return,” Sullivan’s “O hush thee,” Barnby’s 
“Luna,” and Dr. Hiles’ *“* Evening,” the last of which 
was warmly re-demanded. Mr. Thomas Pattison, organist 
of Prescot Church, presided at the organ with tmuch 
ability

35 Christy Minstrel Songs. (Second Selection). 
34 A Christmas Album of Dance Music. 
33 The Juvenile Vocal Album

32 Beethoven's Sonatas. Edited by Charles Hallé (No. 6). 
31 Beethoven's Sonatas. Edited by Charles Hallé (No. 5) 
30 Beethoven's Sonatas. Edited by Charles Hallé (No. 4

29 Ten Contralto Songs, by Mrs. R. Arkwright, &c

28 Beethoven's Sonatas. Edited by Charles Hallé (No. 3

27 Five Sets of Quadrilles as Duets, by Charles d'Albert, &c. 
26 Thirty Galops, Mazurkas, &c., by d’Albert, &c

23 Twenty-five Juvenile Pieces for the Pianoforte

22 Twenty-one Christy Minstrel Songs. (First Selection.) 
Nine Pianoforte Pieces, by Ascher and Goria. 
Beethoven's Sonatas. Edited by Charles Hallé (No. 2.) 
19 Favourite Airs from the Messiah, for the Pianoforte

18 Twelve Songs by Verdi and Flotow

16 Twelve Sacred Duets. for Soprano and Contralto Voices. 
15 Eighteen of Moore's Irish Melodies

14 Ten Songs, by Schubert. English and German Words. 
13 Twelve Popular Duets for Soprano and Contralto Voices. 
12 Beethoven's Sonatas. Edited by Charles Hallé, No. 1. 
11 Six Pianoforte Pieces, by Wallace

10 Nine Pianoforte Pieces. by Brinley Richards

