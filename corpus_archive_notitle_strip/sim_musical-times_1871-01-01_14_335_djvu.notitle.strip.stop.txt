, price Opera , sewed paper cover , 2s . 6d . ; , bound scarlet 
 cloth , 4s 

 BEETHOVEN “ FIDELIO , ” German English words , AUBER 
 “ FRA DIAVOLO , ” French English words , ready , followed , 
 February 1 , MOZART “ DON GIOVANNI , ” Italian English words 

 following Operas course preparation 

 vanni abound directions phrasing colour- 
 ing . splendid German editions | 
 accessible musician , find } 
 test disparity popular 
 cen , French English pianoforte editions . | 
 Notes altered , omitted , lengthened shortened ; 
 ianos fortes changed left , added , etc . : | 
 Msttions phrasing , Mozart especially | 
 exquisitely minute , shifted dis - re- 
 garded slovenly popular - prints . quote 
 innumerable instances , let compare 
 passage Second Violin , beginning 
 eighth bar Allegro ‘ ‘ Fuggi crudele ” ( Don 
 Gwvanni ) asit stands score , ordinary 
 pianoforte arrangements . Mozart binds notes 
 player fine instinct phrase 
 , making passage agitated comment 
 text . ordinarily known ? 
 aslur bar half bar , longer intelli- 
 gible reasoning phrase , mere scale passage 

 considered Beethoven 
 blotted Leonora divine invocation Hope 
 frivolous gruppetto ? heard 
 seen intrusive gruppetto , Beethoven 
 text existence ; gave singer 
 alternative continue chromatic scale 
 quaver bar , stay the| 
 , double notation mistaken 
 unmeaning ornament , crept current 
 editions performances . unscrupulous shift- 
 ing , heightening altering appliances colour-| 
 ing endlese ; truly rushed field | 
 editorship , angels scarce dare tread , 
 rude footsteps . days Stevenses 
 Malones literature given way 

 versions Shakspere repudiated , 
 “ restorers ” longer called operate 
 Rembrandts Giorgiones , tolerate 
 garbled “ ‘ improved ” versions musical master- 
 pieces ? singers ’ alterations , conductors’“‘effects ” 
 editors ’ opinions incorporated text 
 handed ? distinctly hold 
 , want editions 
 counterpart composer text , practical 
 purposes deviation necessary , 
 deviation plainly specified 

 _ Weare told musical culture stood high | 
 England ; popular dark age inter 

 session , certainly Cherubini classical music 
 produce decided effect audience . Signor 
 Fancelli Jason , Signor Antonucci 
 Creon , merely respectable ; Madame Sinico 
 Neris wished . Madlle . Sessi 
 appeared principal parts , wel- 
 comed clever versatile artist . 
 “ Robert le Diable ” Madlle . Corani ( sang 
 time Crystal Palace Concerts ) appeared Alice , 
 gradually warmed apathetic audience enthu- 
 siasm singing acting , ofa 
 high order . season closed 10th . ult , 
 house - opened 17th . ( hundredth 
 anniversary ‘ Beethoven birth ) special perform- 
 ance “ Fidelio 

 CRYSTAL PALACE 

 Tue scheme announced establishment 
 celebration centenary Beethoven birth 
 satisfactorily carried . Inadditon Sympho- 
 nies Concertos , music “ Ruins 
 Athens , ” little popularly known 
 Turkish March Chorus Dervishes . 
 birth - day great composer “ Choral Fantasia ” 
 “ Choral Symphony ” included programme . 
 pianist named work Madame Arabella 
 Goddard , performed , usual success , 
 thirty variations original theme C minor . 
 praise excellent manner 
 Herr Manns hasglorified Beethoven 
 , faithful chroniclers , record fact Septet , 
 E flat , having executed stringed instru- 
 ments orchestra , wind instruments doubled 
 forte passages . solemn protest 
 art - lovers entered exhibitions , 
 shortly report performance Beethoven 
 Sonatas pianofortes , supplementary 
 instruments additional strength required 

 SACRED HARMONIC SOCIETY 

 performance ‘ ‘ Judas Maccabzeus ” inaugurated 
 thirtyninth year Society 25th . November . 
 exception Madame Vanzini , scarcely 
 equal Handel trying music , solos excellently 
 given , Miss Vinta , Madame Patey , Mr. Vernon Rigby , Mr. 
 Montem Smith Signor Foli vocalists 
 rest music principals entrusted . 
 choruses sung ; 
 orchestra , somewhat overpowering thesingers 
 Sir M. Costa ‘ additional accompaniments , ” played 
 set praiseworthy accuracy . 
 eve centenary anniversary Beethoven birth 

 Vened , public ’ Mass C “ Mount Olives ” performed , 
 entirely emerged . lead far comment | solos Mass given Madame Sinico , Madlle . 
 curious phases process Drasdil , Mr. Vernon Rigby Mr. Lewis Thomas , 

 Anthem , ‘ “ trust Thee . ” Fe- 
 dinand Ifiller . Second Anthem , “ Lo , Children 
 Hebrews . ” Charles Gounod 

 octave Schumann “ Adventlied , ” selection 
 Mendelssonn ‘ Lobgesang , ” Beethoven Hallelujah , 
 “ Mount Olives , ’ Handel Hallelujah 
 ‘ * Messiah , ” heard , respecting 
 Choir says “ finer catholic selection music 
 heard octave , 
 difficult wish , M. Gounod , 
 present , struck unique 
 rendering works . ” luncheon 
 morning afternoon services rendered 
 usually interesting presence 
 eminent architects , painters , sculptors , musicians , 
 , M. Gounod , especially noticed 
 having health proposed Mr. Beresford - Hope , MP , 
 spoke French musician “ illustrious 
 composer advance christial 
 art . ” M. Gounod reply happily characterized 
 Guardian modest , refined , hearty . 
 evening service parochial meeting held 8 . 
 George Hall , hundreds poor people sat 
 tea ; , speeches Mr. Tom Hughes , 
 M.P. , , followed songs , etc . , ( charmingly 
 sung Miss Robertine Henderson , ) Professor Peppers 
 entertainment , entitled « Blue Beard , ” ended day plea- 
 santly . speaking great yearly festival 
 unjust mention periods 
 year music equally high character : 
 solemn season Lent Advent , 
 brilliant festivals Easter Christmas , highest 
 efforts musical genius taken reference t 
 school time , offered daily sacrifice ot 
 praise prayer 

 Ma ester ot sa te Oo Fe Pore 

 2 eee - ? | “ Sound alarm , ” eminently successful , 
 Popular Concerts , ” Angell Town Institution , Brix- ‘ encored , ' band led Mr , Gene 

 ton , devoted entirely works Beethoven , | : 
 contained “ Waldstein Sonata ” ( excellently played | Royat Acapemy Music.—The competi- 
 |tion Westmorland Scholarship Potter Ex 

 Mr. Prentice ) , Trio C minor , Romance 
 G violin , Serenade ‘ l'rio ( . 2 D , Op . 8 ) , | hibition took place Monday , 19th ult . , 
 fully appreciated unusually large jinstitution Tenterden - street , Hanover - square , 
 audience , Mr. Prentice assisted Messrs. H.|examiners Principal ( Professor Sterndale Ben- 
 Blagrove ( violin ) , Richard Blagrove ( viola ) , Aylward | nett ) , Mr. F. R. Cox , Mr. W. Dorrell , Mr. John Hullah , 
 ( violoncello ) ; vocalists Miss Blanche Cole,|Mr . H. C. Lunn , Mr , G. A. Macfarren , Mr. Walter Mac- 
 Mr. Harley Vinning , Mr. W. H. Hillier . Mr. G.8 . farren , Mr. Brinley Richards . results 
 Minson conducted . bo gest page nag ra er ag Mary Craw . 
 : , |ford ( elected ) , Miss Pocklington , Miss Rebecca Jewell , 
 MapaME EUGENE OswaLp gave concert Miss Frith , Miss Goode ( highly commended ) . Potter 
 17th ult . , St. George Hall , programme | Exhibition — Miss Agnes A. Channell ( elected ) , Miss 
 excellent music , pianoforte works | Wield , Miss Taylor , Miss Gardner , Miss Waite 
 Madame Oswald took Rubinstein Trio G ( highly commended ) : 
 minor ( Op . 15 ) , Beethoven Duet F ’ , pianoforte | sind ys ‘ . 
 violoncello ( Op . 5 ) , Schubert Rondo tor piano-| _ _ Second Concert South Norwood 
 forte violin ; mention Beethoven | Musical Society present season took place 
 Sonata flat ( Op . 26 ) , solos , Monday , 19th ult . , Henry Purcell « « Dido 
 imagined styles compositions sufficiently | 4neas , ” occupied post honour . Dido 
 varied test powers utmost . Inthe interpre-|was filled Miss Annie Sinclair , 

 tation pieces concert - giver displayed 
 high qualities asa pianist 
 occasion speak , received 
 applause performance merited . ) 
 assisted Herr Wiener ( violin ) , Herr Daubert ( violon- | 
 cello ) , Messrs , John Thomas T. H. Wright ! 
 ( harp ) . vocalists Misses Blanche Reeves , Klien | 
 Glanville , Maria Langley , Mr. Frank Elmore 

 music . solo Spohr « pants hart , ” | 
 sung effect Miss S. Edwards , 

 Art annual monthly term meeting 
 College Musicians , held , Thursday evening , lst 
 ult . , Shaftesbury Hall , Aldersgate Street , William 
 Harvey , Esq . , read paper “ Structure 
 Vocal Organs , ” exhibiting laryngoscope 
 instruments ; J. J. Haite , Esq . , read paper 
 “ Beethoven . ” Mr. W. C. Filby played Beethoven 
 ‘ Moonlight Sonata ” Sonata flat , op . 26 ; 
 Misses Jeyes Carter Mr. Collier contributed vocal 
 music ; College choir sang selection Bishop 
 glees . Mrs. Mullen Miss Haite presided piano- 
 forte . — — — series chamber concerts 
 given , Saturdays , 26th November 3rd 
 10th ult . , direction Mr. Alfred Carder , Mr. J. 
 J. Haite , Mr. Alfred Mullen respectively . Madame 
 Gilbert , Misses Carter , Elder , M. St. Aubyn , Cole , 
 & c. , Messrs. Albert James , Edward Carder , G. Carter , 
 & e. , vocalists , Messrs. Goodwin ( violoncello ) 
 ana Merriton ( violin ) , Mr. Alfred Carder , Dr. Bennett 
 Gilbert , Mr. Mullen , Mr. Filby , Mrs , Mullen , Miss 
 Moore ( pianoforte ) solo instrumentalists 

 Tue annual concert Immanuel Church 

 Musical Celebrities . Photographed Collection 
 Engravings possession John ‘ lowers , Alderley 
 Edge , Manchester 

 Mr. Towers deserves credit presenting 
 heads men shed lustre 
 musical art ; commended 
 manner arranged . Certainly 
 small portrait Beethoven overwhelmed 
 Bach Handel , appear 
 ot ; placed centre card , pre- 
 sume disparity size pictures com . 
 pensated occupying post honour . 
 especially admire portraits Schubert Mozart ; 
 Dr. Arne looks like carica- 
 ture , reason believe perfectly 
 authentic . likenesses Haydn , Palestrina , Rameau , 
 Cherubini Paganini , scarcely called flattering , 
 sufficient character 
 interesting artists . course photographs 
 living persons satisfactory 
 portraits , guaranteed ; 
 , welcome collection 
 acceptable contribution studio musicians , 
 fault found pictures , 
 taken separately , group , possess value 

 CORRESPONDENTS 

 BALLYRONAN , IRELAND.—On Friday evening , 9th given , Dr. Marks snalemanelaa organ , built Mr. Hill , ot 
 ult . , Miss Wright , organist Woods Church , assisted Mrs. | London , noticeable feature . regret space 
 Stewart , entertained choir iu Female School Room , | print description fine instrument , mention 
 tastefully decorated national flags , evergreens , admitted competent judges best toned organ 
 flowers . evening , glees , solos , & c. , given witn|in Ireland —— TuHE members Christ Church Choral Union 
 success . vocalists especially distinguished them- gave concert season , Sth ult . , Pro- 
 selves Miss Gaussen ( aged years ) , Miss Wright ( who| testant Hall , filled . ‘ programme contained , 
 presided harmonium ) , Mrs. Stewart , Miss Elliott , Miss usual , combination sacred popular music . 
 Bessie Elliot ( aged respectively years ) , Miss Minnie | included choruses Mendelssohn , ‘ * Sleepers wake ” 
 Elliott ( aged years ) , anu Miss Scott . congregation | ‘ Blessed men fear , ” solo ‘ * Lord 
 Woods Church indebted Miss Wright unwearied| God Abraham , ” composer , familiar 
 exertions training eflicient choir . excerpts Creation Messiah , closing Benedict 

 19 . Kerhnech eave « | Hymn Faith . ‘ chorus , powerful 
 Beurast . 20th ult . , Dr. Leo Kerbusch gav ea desired , sufficiently effective o disclose beauties 
 Concert choral chamber music Music Hall . Tne pro-| compositions , develop fuli magnificence . reci- 
 gramme consisted selections works Beethoven , tative , * Comfort ‘ ” ig SR ep Ss 
 9 j ; : 1 - | . ye peuple , ” succeeding , * * 
 Mozart , Macfarren , Pinsuti , Kerbusch , & . choral music | valley , ” dmirably delve . d Miss E ¢ 
 z. Sonar AP aay | y , ” admirably delivered Mr. Baker , Miss Evan 
 sung members Dr. Kerbusch Choir Concordia ; ” | son singing “ mighty Pens ” careful piece vocali- 
 solos given Misses Kichardson , Mr. Thackeray , Mus . B.,| sation . second Mr. Smyth flute solos received 
 Oxon , og — _ Mr. W. = ood ( Bass ) , Armagh Cathedral | applause . guod selection - songs choruses 
 poy t accu “ Tm ng cnn ys Apcerg Elsner ( violoncetlo ) | successfully given ; Messrs. Baker , Smyth , 
 poe Batata : De : Kerb ze aha evening Ori-| Sikes contributed vocal solos . Mr. J. M’Carthy acted 
 ginal Cantata , Dr. Kerbusch — * beginning word . ” | conductor accompanist , discharged duties 

 work reflects great credit author : freshness ! y.yal eflicienc 

 variety thruughout entire composition , received | : 5 P : oui 
 unqualified approbation audience . Fesca grand Trio| COVEN1RY.—An entertainment , entitled “ Minstrelsy 
 B flat , violin , violoncello , pianoforte , perfurmed | Ancient Modern , ” given , St. Mary Hall , Monday 
 ability , contributed nut iittle enjoyment | eveuing , 24th November , Mr. Elis Roberts , assisted Miss 
 evening . ‘ Lhe concert brought close Beethoven chorus , | Elien Glanville . Miss Glanville successful * * 
 * Halielujab , ” * Mount Olives , ” sung honour | Minstrel Boy , ” * * Bid discourse , ” ‘ * Tue Maid Llangollen , ” 
 centenary composer , born December , 177 . Dr. Kerbusch| encored . Mr. Roberts performance 
 acted conductor , presided piano , harp highly appreciated —— Mk . STRINGER annual concert 
 " cae ae ci om | given St. Mary Hall , 13th ult . , large audi- 
 Birmincuam.—A Concert given 1 ' T emperance | ence . ‘ Tne principal instrumental pieces Beethoven Trio 
 Hall , Monday Sth ult . , Temperance Hall choir and| & fiat , pianoforte , violin , violoncello , Mozart ‘ Trio 
 friends . Miss 8 . Wilde Miss M. Young received encores for|the key , fur pianoforte , clarionet , viola . 
 songs ; Miss N. Corbett , Messrs. Cox , Browning , aud ) works , strangely , performed fragments — , 
 T. Bolton highly successful . duet violin piano | jocal paper says , “ judiciously divided " — Mr. Stringer 
 played Messrs. J. E. Burman W. Marle . choir , | merit having introduced novelty pro- 
 conducted Mr. H. Marle , sang - songs gices , | gramme . pianofo : te partin composition sustained 
 Mr. W. Marie rendered efficient service accompanist . con-|py Miss Strmger . instrumentalists Mr. Brooke 
 cert entire success , reflected great credit ) ( violin clarionet ) , Mr. Stringer ( viola ) , Mr. Mander ( vivlon- 
 concerned . | cello ) , Mr. Brooke achieving decided success _ — 

 > Festiv ‘ haral Sapjety | Clarionet . vocal music entrusted Mrs. A. J. Sutton 

 Braprorp.—The attempt Festival Choral Society |Mr . Grayson , received appiause 

 tocelebrate centenary birth Beethoven special | Jin pj f Beethoven * ‘ Adelaide 

 singing Beethoven ‘ ‘ Adelaide 

 concert , devoted performance works , proved successtul 

 number students . word welcome old new 
 friends present , students , Mr. Oakeley proceeded , 
 conformity practice adopted , met 
 general approbation year , remarks , historical 

 Leeps.—Beethoven birth - day , hundredth 
 anniversary , honoured Town [ ! a!l Popular Concert , 
 17th ult . , performance best compositions 

 descriptive , music selected performance . pro- 
 gramme strictly classical , selected excellent 
 taste.——Proressor OAKELZY gave organ performance , } 
 15th ult . , large audience Music Class - room , honour | 
 Beethoven , birthday 17th December , 1770 . | 
 programme — taken Beethoven works — consisted entirely 
 arrangements ( great master wrote organ music ) , 
 excellently selected . ‘ ‘ Adelaide , ” admirably sung dis- 
 tinguished amateur tenor , unexpected acceptable 
 variety . Mr. Oakeley prefatory remarks related 
 Beethoven celebrations going year Germany 
 Britain drew acontrast ‘ ‘ Missa Solennis ” 
 D Mass C , written years earlier , speaking 
 ( vocal portion Choral Sym- 
 phony ) elaborate difficult choral music con- 
 ceived . ended characterising Beethoven greatest 
 composer pure music , independent materialism words , 
 greatest composer world seen 

 Freemant.e , Souraampron.—A special entertainment } 
 given Tuesday , 29th November , Girls ’ School-| 
 room , Mr. J. F. Sharp , entitled ‘ ‘ Lyrics Past . ” ) 
 entirely musical character , exception introduc- 
 tory remarks , great praise Mr. Sharp pains 
 bestowed selecting compositions Handel , 
 Haydn , Mozart , Schubert , Mendelssohn . short account 
 composers , outline critique 
 best pieces . gave interest entertainment . 
 absence Mr. Racine , advertised accompanist , 
 Mr. Winterbon presided piano 

 cessful Bishop ‘ * Tell heart , ” second . Mr. 
 Young . conductor , sang ‘ ‘ Comfort ye sand ‘ * valley ” 
 highly creditable style , Mr. Fisher rendering ‘ ‘ 
 nations ” extremely intelligent . Mr. Chapman received 
 applause solo , glees ‘ ‘ chariot hand ” 
 ( encored ) , ‘ Oberon fairyland , ” ‘ * Men Harlech , ” 
 given choir spirit . Mr. Wilkinson violin 
 solo excellent performance ; , altogether , concert 
 decided success 

 Liverroot.—The Philharmonic Society tenth concert , 
 took place 22nd November , entirely devoted 
 works Beethoven . solo artists , pianoforte , 
 Madame Arabella Goddard ; vocalists , Mrs. Weldon Mr. 
 Vernon Rigby . programme stood : overture “ Eg- 
 mont ” ; recit . aria ( * Engedi ” ) , “ Jehovah ! hear , O hear , ” 
 Mr. Vernon Rigby ; aria . ‘ ‘ questa tomba , ” Mrs. Weldon ; Choral 
 Fantasia ; scena , ‘ * Adelaida , ” Mr. Vernon Rigby ; song , ‘ ‘ O beau- 
 teous daughter starry race , ” Mrs. Weldon ; overture , 
 “ Leonora , ” . 3 . second commenced Sinfonia 
 C minor ( finely played ) ; followed scena , ‘ Ah 
 Perfido , ” Mrs. Weldon ; solo pianoforte , “ Moonlight ” sonata ; 
 ‘ Song Quail , ” Mr. Vernon Rigby ; march choras 
 ( “ Ruins Athens ” ) , “ Crown ye altars ” ; overture , 
 E , “ Fidelio . ” pianoforte performance cha- 
 racterised exquisite delicacy musicianlike feeling , Mr. 
 Benedict accompaniments “ Adelaida ” ‘ * Quail Song 

 Rev. W. D. Duncombe . pieces ably accompanied | entitled highest commendation . Choral Fan- 
 pianoforte Mr. G. Townshend Smith , conducted , as|tasia went particularly . concert 

 _ — _ 

 excellence interpretation styles — praise awarded Miss Ross , Mrs. Jackson , Messrs. Allen , 
 half arch , half tender expression ‘ ‘ Amici ! ogni evento , ” from| Green , Pidcock , acquitted extremely 
 « Italiana Algieri , ” wonderful execution Aria de| , songs encored . Mr. J. Yarwood conducted 
 Bravura , ‘ ‘ Mi paventa , ” Graun ( ‘ * Britannicus ” ) , the}much ability — AnoTueEr gentlemen concerts took place 
 charm Spanish songs — proved power unim-/in Concert Hall , 28th November . principals 
 aired . vocalists Mdlle . Leon Duval Signor | Mdlle . Scalchi , Signor Risegari , Mr. E. Hecht . ‘ programme 
 Delle Sedie , sang expression . choral | selected compositions Cherubini , Mozart , Chopin , 
 music successful , instrumental selection was| Rossini , Beethoven , Weber , Schubert , Heller , Verdi . Mr. 
 extremely happy , consisting overtures * Masaniello ” and| Hecht played ‘ Capriccio alla marcia , ” composed , 
 “ Midsummer Night Dream , ” Mendelssohn Symphony A| deservedly applauded . — — Tue Manchester Vocal 
 minor ; Entr’acte Schubert ‘ * Rosamunde , ” overture | Society gave second concert season , Concert 
 tothesame , finished successful c ncert . — TueE | Hall , 29th November . character pieces 
 series Classical Concerts , plan London ) somewhat lighter usual ; mest severe chorus 
 “ Monday Popular Concerts , ” took place 14th ult . exe-| parts , * * Dixit Dominus , ” Leo , MS . 
 cutants Herren Strauss L. Ries , Mr. Zerbini Signor handwriting , excellently sung . Mr. HALLE 
 Piatti ; pianoforte , Herr Ernst Paver , vocalist Herr ! gave sixth concert season , Free Trade Hall , 
 Jules Stockhausen , rich baritone voice pure style | lst ult . , principals Mdile . Duval , Signor Bottesini , 
 highly appreciated . Quartet E ( . 1 ) , strings ( Cheru-|and Mr. Hallé . Mr. Hallé played , time , Men- 
 pini ) ; Fantasia F sharp minor , pianoforte ( Mendelssohn ) ; |delssohn Caprice minor , . 33 , novelty 
 Sonata F major , pianoforte violoncello ( Beethoven ) ; a| Sullivan ‘ ‘ Ouvertura di Ballo . ” Mendelssohn Elijah given 
 Quartet strings , G , . 1 , Op . 10 ( Mozart ) , principal | seventh concert , 8th ult . , Madame Rudersdorff , 
 instrumental pieces ; Herr Stockhausen sang Schubert * Wan-| Miss Galloway , Miss Palmer , Herr Nordblom , Herr Stock- 
 derer ” Scotch songs , pianoforte , violin . violon-| hausen principals . cLassicaL chamber concert given , 

 cello accompaniments Beethoven . room filled . jthe Concert Hall , 13th ult . , programme 
 FottowinG example local Philharmonic | selected . Mr. HAL k eighth concert , 15th ult . , 

 musical bodies , Societa Armonica , 17th ult . , commem- ) given commemoration 100th anniversary Beethoven 
 orated centenary birth Beethoven devoting ) birth , programme entirely selected works . 
 public concert programme selections his| large attendance . Mr. Sims Reeves , announced , 
 works , hall Liverpool Institute , concert | appear , copies telegrams received trom 
 given , nearly filled . chief instrumental composition| freely circulated room , addition Mr. Hallé 
 programme Symphony , . 2 , weil played | addressed audience subject . Notwithstanding dis- 
 . overture ‘ ‘ Prometheus , ” incidental , appointment concert great success.——On 17th ult . 
 accompaniments , band displayed careful study } choir George Street Wesleyan Chapel , Hulme , gave musical 
 thorough appreciation music . choir rendered ! literary entertainment aid Library fund . pro- 
 “ Kyrie ard Gloria ” . 1 Mass , ‘ ‘ Susceptible Hearts ” | gramme contained good selection sacred music . singing 
 “ Hail , mighty Master , ” * ‘ Ruins Athens , ” | reflected great credit Mr. James Lord , jun . ( organist teacher 

 Manchester Tonic Sol - fa 
 precision , successful effort * ‘ Prisoners ’ Chorus ” | Choral Union gave miscellaneous concert Industrial Hall , 
 Fidelio . long trying air , * Ah ! perfido , ” sung| Hulme , Monday evening . 19th ult . , highly attractive 
 Miss Monkhouse artistic finished style . Mr.| programme . principal parts sung members 
 Tt . J. Hughes showed vocal power musical intelligence in| Union creditable manner . Mr. William Cole 

 delivery air , ‘ * money sorrow ™ ( Fidelio ) . | accompanist , Mr. Lloyd conducted . 
 ‘ famous trio , * ‘ Tremate empi tremate , ” rendered Miss Monk- | 3 aciaindl 
 house , Mr. Hughes , Mr. James M’Ardle good style . } Martporoven.—On evening Friday , 2nd 
 interesting item excellent programme . Mr. Lawson’s|ult . , Mr. W. S. Bambridge , R.A.M. , Professor Music 
 playing Romance F brilliant effective , and| College , gave annual concert Town Hall . per- 
 Mr. Armstrong ably fulfilled duties conductor.—Tug | formers Miss Edith Wynne , Miss Julia Elton , Mr. W. H. 
 twelfth concert Philharmonic Society , given 20th ult . ,| Cummings , Mr Maybrick , M. Sainton solo violinist , 
 extremely interesting members , per-| Mr. Bambridge solo pianist , M. Stanis'aus solo accompa- 
 formance St. Peter , new Oratorio accomplished con-| nist . Beethoven * Kreutzer ” Sonata played 
 ductor , Mr. Jules Benedict . music went admirably , ) masterly style M. Sainton Mr. Bambridge . 
 difficult principals cr chorus } instrumental pieces programme solos operatic 
 earnest determination justice , reception | airs , played M. Sainton M. Stanislaus excellent accompany- 
 work cordial , portions - demanded , | ing , pianoforte solos , performed Mr. Bambridge . 
 repeated . solo artists Mdlle . Titiens,| , Schubert , received hands executant 
 Madame Patey , Mr. Raynham , Herr Jules Stockhausen . Mr./all delicacy touch refinement expression 
 Raynham took , short notice , Mr. Sims Reeves , who| demanded , , Meyer , elicited encore . 
 prevented illness fulfilling bis engagement , jthe vocal pieces programme - song , ‘ ‘ Field Flowers , ” 
 new tenor favourable impression trying | M. Stanislaus , deserves especial commendation , melody 
 position , singing pure , expressive style , voice good ) graceful , harmony simple effective . Dr. 
 powerful quality , , , firm intonation . | Garrett ( Cambridge ) new ballad , “ Parted . ” excel- 
 work , course , manifests / lently rendered Miss Julia Elton ; Madame Sainton - Dolby 
 thorough musician , instrumented good effect . The| song . ‘ ‘ Marjorie Alman : ck , ” gained Miss Edith Wynne well- 
 recitatives deserve praise clearness simplicity . Herr jmerited encore . Mr. Cummings singing * soft southern 
 Stockhausen sang exquisite feeling ‘ ‘ O head ) breeze , ” Barnby Rebekah , successful 
 waters , ” beautiful contralto song , ‘ * O thou afflicted,”| effort . Mr. Maybrick * Rover Life ” spirited 
 admirably given Madame Patey , enthusiastically encored . |energetic , received enthusiastic encore ‘ * Non piu 

 Lymneron.—The inauguration concert Lyming- | # " 4rai- é 
 ton Philharmonic Society took place ‘ heAssemb'y Rooms , the| Mippieron , Lancasuire.—On Monday evening , 
 8th ult . , crowded audience . performance commenced ! 12th ult . , Handel Oratorio , Messiah , performed 
 music ‘ * Macbeth , ” ( attributed Locke ) principal National School Room , band chorus numbering upwards 
 parts sung Mrs. Des Champs De la Tour , Mrs. 120 . principal vocalists Miss Lydia Vernon , Miss Harlow , 
 John Lane Shrubb , Mr. Cross ( Winchester Cathedra ) ) | Mr. Dumville , aud Mr. Lovatt . Miss Vernon acquitted 
 highly effective . choruses throughout| creditably allotted , best effort 
 rendered precision dramatic feeling , } air , ‘ know Redeemer liveth . ” Miss Harlow voice 
 greatly applauded . second instrumental pieces ! told contralto songs , given artistic 
 given , addition vocal solos contributed feeling , devotienal expression . Mr. Dumville sang tenor 
 Mrs. John Lane Shrubb , Mrs. De la Tour , Mr. Hay . music good effect , Mr. Lovatt , suffering 
 utmost praise conductor , Mr. R. Augustus Klitz , for'cold , best circumstances render difficult 
 excellence performance . | bass songs effectively possible . band — . 

 Ricumonp.—Mr . B. Mansell Ramsey gave successful 
 concert , Greyhound Assembly Rooms , 29th November , 
 vocalists Mdile . Emilie Blanche , Miss Rosaline Stuart , 
 Messrs. F. Childerston W. Reeves , 
 highly successful , harp solos Mr. Ellis Roberts received 
 anencore . pianoforte performance Miss Amy Weddle 
 interesting feature programme . Kontski * * Souvenir de 
 Faust ” W. Pape “ Irish Diamonds " pieces selected , 
 enthusiastically - demanded 

 Sarispsury.—The Sarum Choral Society gave mis . 
 cellaneous concert , Tuesday , € th ult . , conductor . 
 ship Mr. C. J. Read . vocalist Madame Talbot - Cherer , 
 songs gave satisfaction audience . Mr. H. Bla- 
 grove played movement Spohr violin concertos , 
 Mr. Alexander Rowland solo double bass . Mr. C.J. 
 Read Mr. J. Whitehead Smith highly effective 
 pianoforte performances . Mendelssohn Overture 
 * * Son Stranger , ” unfinished Opera , Loreley , Beethoven 
 “ Choral Fantasia , ” excellently given , Overture 
 Mr. Read received applause . novelty 
 entertainment Mr. R. Blagrove admirable accompaniment 
 concertina Madame Talbot - Cherer songs , 
 choir sang evening 

 SuHerrretp.—On 7th ult . violin pianoforte 
 Recital given Music Hall , Surrey Street , artists 
 Madame Norman - Neruda Mr. Charles Hallé . concert 
 commenced ballad polonaise Vieuxtemps , 
 violinist displayed artistic feeling marvellous 
 execution . Adagio , Nardini , equally 
 happy , rendering Bazzini brilliant Scherzo Fan- 
 tastique , ‘ Ronde des lutins , ” exhibited powers execu- 
 tant forcibly . duet Mr. Charles Hallé 
 successful . Mr. Hallé played usual masterly 
 |manner . Adagio Rondo B flat ( Dussek ) Beet- 
 |hoven grand Sonata E flat rendered faultless style , 
 Mr. Hallé performed Bach ‘ * Chromatic Fantasia ” Fugue 
 { D minor , ballad flat Chopin . audience 
 gratified , applause enthusiastic 

 Mr. J. 8 . Bates conducted , Mr. F. C. Kitson accompanied 
 piano 

 Torquay.—A fine organ , built Messrs , 
 Speechly Ingram , London , Belgrave Road Church , 
 opened , Thursday , 24th November , Mr. Lohr , Ply- 
 yuouth , played selection works Handel , Mozart , 
 Rossini , & c. , ability.——Mr . WILLIAM VINNING gave 
 pianoforte Recital , 10th ult . , large audience , 
 success . programme opened pianoforte duet 
 Mendelssohn , Mr. Vinning assisted clever young 
 pupil , ‘ * * ‘ Harmonious Blacksmith , ” 
 performed , evinced musical promise . Mr. 
 Vinning solos Beethoven Sonata G , . 1 , op . 31 , 
 * Pastorale ” Bennett fourth concerto , Mendelssohn 
 “ Eighth book Lieder , ” “ * Gayottes , ” Bach , “ Wayside 
 Sketch ” ( elegant composition ) , Arthur O'Leary , Weber 
 “ Concert - Stiick , ” given taste 
 good executive power 

 Torney AsBEY , PETERBORO’.—A concert lately 
 given Mr. Thacker , organist Abbey . Miss Blanche 
 jeeeves Messrs. Ling Swift , Peterborough Cathedral , 
 vocalists , Mr. Thacker Mr. J. Baker ( Whittlesey ) 
 presiding pianoforte . excellent trios , duets , solos 
 sung , Miss Reeves gaining great applause fine rendering 
 ‘ Shadow Song , ” Mr. Ling Mr. Swift ‘ ‘ Alice ” 
 “ ruler sea 

 Richard Bentley , New - Burlington - street 

 BEETHOVEN CENTENARY . 
 published , beautifully printed , handsomely bound cloth , 
 gilt , 3s . 6d 

 EETHOVEN . < Memoir . ELiior GRAEME . 
 Essay ( Quasi Fantasia ) ‘ ‘ Hundredth Anniver 

 Remarks events past year 

 Article Beethoven Life Works ( commemoration 
 Centenary Birthday ) , exact Chronological List 
 works , Mr. John Towers 

 Musical Almanack , blank spaces Memoranda 

 e deve . Tue Directors Oratorio Concerts pleasure announcing Concert 
 tea ensuing Season place Wednesday Evening , February 15 , Bach Oratorio 
 8 
 PASSION , 
 iness F . 
 syoten , created intense interest season , performed . Second Concert , Men- 
 . delgsohn Oratorio 
 tak 
 aly ELIJAH , 
 0 
 rto con given . arrangements Oratorio Concerts preclude possibility giving 

 formance celebration Beethoven , date birth ; , seeing 
 courant greatest work , 

 MASS D 

