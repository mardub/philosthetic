viesgeiieiain 10 , 3 

 4 Brahms . 
 .- Beethoven . 
 Georg Liebling 

 Tragic Overture .. ee 
 Symphony , . 5 , C minor | . 
 New Pianoforte Concerto 
 Herr Grorc LigsLinc . 
 Tannhauser Pilgrimage 

 FEBRUARY 24 , 3 

 Overture — Macbeth ” . Sullivan . 
 Symphony G minor ( . 40 , B. & H. Edition ) .. Mozart . 
 Violin Concerto D . Beethoven . 
 ” Mons . Ysave 

 Overture — ‘ “ Le conte Romain ” “ es Berlioz 

 Violin Solo .. ; és “ ee 
 ” Mons . Ysave . 
 Adagio Canzona Ninth Sonate une 
 Harpsichord ae ea . Purcell . 
 Eine Faust - Ouvertiire .. Wagner . 
 Vocalist , Mr. ‘ Cuartes KNow zs 

 Marcu 1 10 , 3 . 
 Overture — ‘ Ribezahl ” ( Ruler “ arti ‘ Weber . 
 Symphony , . 8,inF .. . Beethoven . 
 Overture — “ Idomeneo ” Mozart . 
 Pianoforte Concerto , . 5 , E flat ( “ Emperor ’ . Beethoven 

 err ZWINTSCHER . 
 Overture — Tannhauser ” os « 3 ae 7 - » Wagner , 
 Siegfried Idyll ee ea ° Wagner . 
 Toccata F ( Orchestrated Esser ) xs . » Bach 

 August 6 — month later — overture 
 finished . Pastor Schubring , Mendelssohn attached 
 friend , tells pretty story concerning composition . 
 says : “ weather beautiful , 
 engaged animated conversation lay 
 shade grass [ Schonhauser Garten ] , 
 sudden , Mendelssohn seized firmly 
 arm , whispered ‘ Hush ! ’ 
 informed large fly gone 
 buzzing , wanted hear sound 
 produced gradually die away . overture 
 completed , pointed semiquaver scale 
 passage violoncellos makes modula- 
 tion B minor F sharp minor , said : 
 ‘ , fly buzzed past 
 Schonhauser 

 MENDELSSOHN brought score overture 
 England , occasion 
 earliest visit London , 1829 . work 
 played England , appropriately , Mid- 
 summer night , 1829 , concert given Drouet , 
 flautist , conducted Mendelssohn . 
 concert played Beethoven E flat Piano- 
 forte Concerto , absolute novelty 
 country . concert , score overture 
 left hackney coach Attwood — lost ! 
 ‘ “ * mind , ” said Mendelssohn , told 
 , ‘ . ” , comparing 
 new score band parts , discrepancies 
 discovered 

 SEVENTEEN years elapsed composition 
 overture remainder ‘ Mid- 
 summer Night Dream ” music ; 
 slightest trace dissimilarity style work- 
 manship — fact , perfectness _ perfectly 
 complete musical equipment play 
 strongest features . performance 
 England entire music Philharmonic 
 concert 27 , 1844 , conducted Mendelssohn . 
 enabled hitherto unpublished 
 extracts Mendelssohn letters English 
 publisher , Mr. Buxton ( Ewer & Co. ) , relating 
 English publication work . December 13 , 
 1843 , sends * ‘ Scherzo , Notturno , March ” 
 ( i.e. , famous * * Wedding March ” ) , pianoforte duet , 
 mentions guineas price . “ , ” 
 says , ‘ wish copyright 
 England ( pianoforte arrangement score ) , ” 
 price complete work ( consisting 
 pieces ) ‘ “ ‘ 
 ’ --i.e . , thirty guineas entire music ! 
 adds work given seventeen times 
 weeks Royal Theatre , Berlin . 
 little later writes English words 
 music alterations , 
 proposes arrangement 
 Cramer Co. , published overture 
 1829 , entire work issued 
 complete form . Buxton evidently suggested 
 pianoforte arrangement performer 
 , Mendelssohn says afraid ‘ 
 little difficult . ” surprising find 
 reference publication contains 
 word “ alterations . ” Mendelssohn 
 alter mind regard visit England . 
 “ yesterday , ” writes Buxton March 5 
 1844 , ‘ certain shall England , 
 need tell great pleasure anticipate 
 stay country . intend arrivt 
 end April bring new 
 things , remain months , t0 
 happy old England 

 Photograph taken 1863 

 librarian Paris Opera House , Herr Felix 
 Weingartner — probably taking special over- 
 sight literary research aspect task , 
 eminent conductor responsible 
 musical erudition . scores 
 issued folio volumes subscription price 
 shillings ; operas , , 
 present published , overtures 
 appear instrumental music . pianoforte 
 edition vocal works issued . 
 Berlioz publication — nearly instru- 
 mental music ready — rank 
 monumental editions Beethoven , Mozart , 
 Mendelssohn , great composers issued 
 great Leipzig firm Breitkopf Hartel . 

 Piazza Michel Angelo Buonarotti , Milan . Having 
 appointed administrative council , includes 
 Arrigo Boito , composer “ Mefistofele , ” Signor 
 Negri , author senator , Signor Giulio 
 Ricordi , eminent music publishing firm , 
 veteran composer , speech lasting hours , 
 explained principles desired 
 Institution carried . Finally , Maéstro , 
 having handed Deed Gift Council , 
 intimated relinquished 
 proceeds copyright works , January 
 , 1900 , favour endowment 
 Home . Truly right royal gift noble 
 monument perpetuate memory Italy 
 Grand Old Man 

 increase significance text . perfect fitting 
 music words , , special feature 
 anthem . endeavours attain end Dr. Davies 
 occasionally expanded usual - writing 
 seven parts , passages . 
 end occurs quintet sopranos , tenors , 
 bass , greatly adds effectiveness 
 composition . reference opening theme 
 augmentation bars choral writing con- 
 siderable originality bring work impressive close 

 Beethoven . Frederick J. Crowest . 
 Wagner . Charles A. Lidgey . 
 ( Master Musicians Series . Edited Frederick J. 
 Crowest . ) [ J. M. Dent Company 

 volumes specimens mere book - making . 
 occupy space point short . 
 comings literary defects , especially 
 Beethoven . glad directed 
 source biographical information relating 
 ‘ “ Anhang , ” ( ! ) given Mr. Crowest 
 “ author ” Appendix A. Beethoven 
 said biographer perpetrate 
 travesty : ‘ ‘ journey , unhappily , proved 
 undertook ere essaying bourne 
 traveller turned face . ” Shades 
 Shakespeare 

 National Music America sources . 
 Louis C. Elson 

 113 

 Dr. stronger ones . 
 oint- weak ones strong , strong ones care 
 vood , . 
 — “ WOMAN MUSICIAN . ” 
 t te Dr. H. A. Harding ( Bedford ) contributed paper 
 pipes afternoon sitting ‘ ‘ Woman Musician ” ” — subject , 
 “ tone way , ably treated Mr. Stephen S. 
 Stratton essay , entitled ‘ ‘ Woman relation 
 lower musical art , ” ” read Musical Associa- 
 tion 7 , 1883 . Dr. Harding began survey 
 opted centuries ago , explaining despite 
 learned men Japanese Greek music , 
 Parish music independent art fourteenth 
 anting century . initial reference fair sex ‘ ‘ woman 
 . singer , ” course interpolated 
 Mr. remark sure woman impregnable 
 1 Cups world song , added ‘ ‘ understood ” ’ 
 vices . ” prime donne ‘ ‘ little trying times . ” fact 
 - ( ladies ’ ) knowledge equal 
 Mr. great natural gifts , humble members 
 ess orchestra accompanied better 
 mateur musicians prime donne . Dealing 
 leputy- songs women , instanced word 
 ly “ Jove ” occurred times bars ! child , 
 hat contended , taught sing , faulty voice 
 production promptly checked . - - days 
 resting large classes girls told “ sing , ” 
 ruined voices completely destroyed 
 charming high notes children possessed . 
 quoting Lord Chesterfield remark son , 
 “ pipe mouth 
 NS . fiddle chin , ” Dr. Harding adverted 
 enormous increase female musical executants , 
 especially violinists , , added , induce 
 rporated [ “ Omposers return symphonic form Beethoven 
 sth ult , time . ‘ “ Weare told women novelists , women 
 eting attists , , ” said lecturer ; ‘ 
 women composers ? ” ’ ‘ , emphatically , ave 
 iad wel . Women composers ; actually exist . ” definitely 
 nd ult ) , stated number 489 ; woman , added , 
 ety , read taken high position composer , 
 n [ ‘ tey literature sister arts . 
 F500 fm splendid executants ; genius 
 con- jg ‘ compose ? authority declared woman strength 
 ciety body hardly endure necessary strains brain 
 00 jm " tve power compose . took strong exception 
 mination . jg ectionable use words “ feminine ” “ femininity , ” 
 ‘ rominent ( @ * applied composers music ; alluding 
 wasnext alleged femininity Schubert , expressed 
 ' ascheme fg devout hope Schubert feminine , women 
 d view . @ “ ould imitate . Women musicians pre- 
 1 Council , § " tted coming maturity want training 
 development . hitherto 
 Bridge Seat composer found ranks women , days , 
 Roya fj “ woman advancing rapidly , 
 Summings fj son high place ranks 
 ion composers , 
 Secondary § discussion followed reading 
 paper Sir Frederick Bridge arrived . winding 
 t obstacles ff usiness meeting , expressed opinion 
 Society Oxford Cambridge allow women 
 atisfactory ig " @duates music . 
 ‘ © . improve Mr. F. H. Cowen , president second day , 
 yutside # Pened proceedings address “ 
 Training Conductors Accompanists , ’ ” ’ 
 tint authorised report column . 
 2 paper ‘ “ PITCH : PAST , PRESENT , FUTURE . ” 
 syster ic Mt. W. H. Cummings , chairman Thursday ( 
 pianoforte , » 
 command Ba day Conference ) , contributed essay 
 ir Pitch : past , present , future . ” Mr. Cummings started 
 ost ' y correcting mistaken impression pitch 
 ve finge ! miginally low gradually grown higher 

 er . reviewed actual state affairs existent 
 fhe time Purcell subsequently . Purcell time 
 € pitches , church 
 ber . Continent , , pitches ; 
 ind instanced organ tuned tone 
 amber pitch , stop added lower 

 13th ult . Balfe ‘ Satanella " ’ revived . perform- 
 ance successful attracted large audience . 
 Miss Chrystal Duncan took title - Mr. Turner 
 appeared Count Rupert 

 Oratory , Edgbaston , lecture- 
 concerts given 3rd ult . programme 
 devoted chamber music , Herr Theodor Werner 
 violinist , Mr. W. Sewell ( organist Oratory ) 
 pianist . pieces given Septet 
 ianoforte , trumpet , strings , Saint - Saéns , 
 8th ult . , lecture , Herr Werner gave historical 
 violin recital . function chamber concert , 
 given Edgbaston Assembly Rooms , 2oth . 
 artists Madame Amina Goodwin , Herr Werner , 
 Mr. W. E. Whitehouse , Mr. W. Sewell 
 accompanist . programme included Beethoven Trio 
 ( Op . 97 ) Rubinstein Op . 52 

 16th ult . Mr. Halford resumed orchestral 
 concerts Town Hall . programme comprised 
 Schumann Symphony , . 2 , C ( Op . 61 ) , Preludes 
 Acts II . III . “ * Manfred , ” Sir A. C. Mackenzie 
 ( time ) , Wagner Prelude Act III . , 
 “ Lohengrin . ” finely rendered , 
 Mackenzie compositions interesting . Signor 
 Ronchini appeared time , gave 
 admirable reading solo Hans Sitts Violon- 
 cello Concerto ( Op . 34 ) , excellent composition ; 
 played short solo pieces 

 late December noticed 
 monthly chronicle , Choral Union gave performance 
 Dvorak ‘ * Stabat Mater , ” McEwan Hall , 
 Mr. Collinson baton . Interest beautiful work 
 attracted large audience , awarded unstinted 
 applause choir capable conductor . 
 intonation tenors reproach , 
 , , chorus deserved praise freely 
 given . second concert devoted 
 Mr. Hamish MacCunn ‘ * Cameronian Dream 

 Messrs. Paterson orchestral concerts better attended 
 season . Scottish Orchestra settled 
 steadier better work , accompaniments 
 ‘ ‘ Stabat Mater ’ ? Brahms Pianoforte Concerto 
 ( . 2 ) carefully successfully played 
 Symphonies Goetz ( F ) , Beethoven ( . 1 ) , 
 Dvorak ( ‘ ‘ New World ” ) . soloists concerts 
 mentioned Mr. Frederick Dawson ( 
 received excellent performances ) , 8th ult . , 
 Madame Marchesi ( songs enthusiastically 
 applauded ) , 13th ult 

 crowded audience assembled McEwan Hall , 
 22nd ult . , hear Rheinberger ‘ ‘ Christoforus ’’ 
 Ninth Symphony , given Messrs. Patterson concert 
 Mr. Kirkhope Choir Scottish Orchestra . 
 splendid choir best form rendering 
 ‘ Ode Joy . ’’ long high notes , terror choirs 
 conductors , easily successfully vanquished . 
 instrumental movements suffered ineffective 
 disposition band pointless reading 
 noble composition . ‘ Christoforus ” ’ singers seized 
 numerous opportunities rich beautiful 
 score offers qualities won 
 enviable reputation . Mr. Kirkhope conducted 
 ‘ “ ‘ Christoforus ” ’ Mr. Bruch Symphony 

 New Year Day Choral Union gave annual 
 performance ‘ ‘ Messiah . ” chorus 
 shown better advantage ; massive choruses 
 splendidly given , florid passages taken 
 precision ease left desired 

 MUSIC GLASGOW . fav 
 ( CORRESPONDENT . ) suc 
 December 23 lady pupils Glasgow Atheneum bee 
 sang Cowen “ Daughter Sea ” pieces rs 
 annual concert . Co 
 Classical Orchestral concerts fairly - 
 attended . Mr. Bruch given satisfying ¢ 
 renderings Tschaikowsky ‘ ‘ Pathetic ” Symphony , je 
 Beethoven . 4 . 8 , Mozart “ Jupiter , ” whi 
 Goetz Symphony F ( Op . 9 ) , particularly interesting brea 
 revival , received ; Mackenzie Prelude serv ! 
 Act II . “ Manfred . ” soloists note 
 appeared Mr. Frederick Dawson ( welcome arr 
 ) , played Brahms Second Concerto ; M. Siloti , = 
 played Schubert - Liszt “ ‘ Wanderer ” Fantasie , = 
 Madame Blanche Marchesi . inter 
 second concert Helensburgh Subscription ment 
 series , Brodsky String Quartet played Schumann ie 
 Quartet , Op . 41 , . 3 , Beethoven Op . 18 , . 6 , oh 
 Miss Nedda Morrison sang . th 
 annual Guards ’ concert , Madame Albani hg 
 Mr. Edward Lloyd sang . 
 death announced notable ot wond 
 Scottish musicians , Mr. J. Roy Fraser , teacher greate 
 organist , Paisley , , means great open - air pn 
 concerts Braes Gleniffer , raised funds sufficient Fiben : 
 erect statues Tannahill Burns town . = ie 
 | 
 special 
 MUSIC LIVERPOOL DISTRICT . pee 
 ( CORRESPONDENT . ) aie 
 Philharmonic Society concert , oth ult , , 
 interesting feature Schumann Symphony § gift 
 D minor ( . 4 ) . Beethoven Pianoforte Concerto J Néruda 
 C minor solo played admirable 9 % th , 
 sympathy expression Mr. Leonard Borwick . § tenderir 
 linterest added concert fine renderings evening : 
 Smart - song , ‘ ‘ Dream , baby , dream , ” Cooke J Brahms 
 “ Hark ! lark . ” 17th ult . Societa 9 vio 
 Armonica 119th concert , direction Mr. Vasco & & forte anc 
 Akeroyd , great success . orchestra proved @ clarinet , 
 reasonably equal Beethoven major Symphony , @§ ult , Mr. 
 exhibited skilful efficiency Dvorak Suite § admirab ] 
 D ( Op . 39 ) . Mr. Akeroyd , worked hard [ & Brah 
 excellent results bring orchestra old Society § sam 
 reliable standard efficiency , played solo & clarinet . 
 Max Bruch Violin Concerto G minor . ff aquisite 
 December 28 Eisteddfod choral competition & modulate 
 Dwyrwd Male - Voice Choir , Talsarnau , Mf « 
 Manchester Prize Glee Society resulted § § asociatio 
 winning award £ 20 gold medal . 
 Gounod ‘ ‘ Redemption ” admirably performed ff silfully ¢ 
 Sunday Society , St. George Hall , r4th ult , Ball con 
 skilful direction Mr. W. I. Argent . Mi successful 
 principal vocalists Miss Ada Standen , Mr. Ben Bigcin dist 
 Roberts , Mr. Arthur Weber , Mr. Hargreaves Hudson . Chopin mo 
 Dr. Peace rendered excellent service organ . Mr. La : 
 Vatied t 
 — 

 MUSIC MANCHESTER . ga “ 
 ( CORRESPONDENT . ) tonotonou 
 fortnight silence , pleasant MW 
 listen Hallé Orchestra , albeit performance 9 % " 
 11th ult . , known work Mozart G 
 minor Symphony , means finished 
 accustomed years . 
 confessed interest excited 
 “ Hiawatha ” scenes Mr. Coleridge - Taylor ( 
 increased Orchestral Ballad minor . 
 novelty programme — Dr. Stanford concert : 
 variations jovial old air ‘ dead § usual , 
 men ” ’ — passages charming reluctant “ clusive att 
 work rendered somewhat dis JP*ks 
 appointing attempt isolated mnsider fi 
 sections text , instead endeavouring grasp Pier . 
 general idea theme irresistible jollityand gteate 
 genuine English character . pianoforte — FFtds Choral 
 admirably played Mr. Leonard Borwick , especial FP0del set 

 meeting 18th ult . , Sir Hubert 
 Parry Orchestral Variations , originally given 
 1898 , works fresh ears . 
 Glazounow Symphony B flat ( Op . 55 ) decidedly 
 interesting , bold , fresh ; move- 
 ment degenerating blatant uproar Russian 
 writers prone . “ Sea Pictures ’ ? Edward 
 Elgar presented orchestra guidance 
 composer , Miss Clara Butt Narrator . 
 listening bright fancies 
 artist attracting great attention , help 
 wondering young author feel 
 greater freedom purely orchestral work , 
 imagination wider range ideas 
 merely sketched , fully worked . Miss Ilona 
 Eibenschutz played Schumann Pianoforte Concerto 
 Brahms Waltzes 

 festival chamber music , rendered 
 specially interesting visit Herr Muhlfeld , 
 Mr. Brodsky , Mr. Cohn , Mr. Carl Fuchs taken 
 advantage producing , respective concerts , 
 works rarely hear . Mr. Brodsky 
 recital , 17th ult . , Beethoven Op . 131 , 
 gifted clarinettist , assistance Miss Olga 
 Néruda , gave Brahms Sonata E flat ( Op . 120 ) , 
 joined quartet party finished 
 rendering Mozart delightful Quintet major . 
 evenings later , Mr. Isidor Cohn programme included 
 Brahms Trio minor ( Op . 114 ) pianoforte , clarinet , 
 violoncello , Schumann Fantasiestiicke piano- 
 forte clarinet , Mozart Trio E flat pianoforte , 
 darinet , viola ; , meeting , 22nd 
 ut , Mr. Carl Fuchs , modestly keeping 
 admirable violoncello playing background , provided | 
 Brahms Quintet ( Op . 115 ) clarinet strings , 
 composer Duet F minor clavier 
 tatinet . result enjoyment 
 exquisite playing Herr Mihlfeld delicately 
 modulated tones produces , sensible 
 clarinet heard advantage 
 association orchestral instruments varied character 
 ad mere pianoforte accompaniment , | 
 skilfully support managed . Schiller | 
 Hall concert Miss Irene Schaefsberg , played 
 successfully weeks Free Trade Hall , 
 distinguished interpretation 
 Chopin morceaux 

 Mr. Lane Ballad concerts attractively 
 vatied interspersed choral work , Leslie ‘ * ‘ 
 sweet moonlight ” Battye ‘ ‘ short , sweet 
 lower , ” served solidity programme 
 nth ult . , somewhat 
 nonotonously fragmentary . Chief soloists 
 Madame Marchesi , rendering Schubert “ Erl- 
 King ? simply superb 

 Bradford , Miss Wehner gave concert 3rd 
 ult . , Chaigneau Trio introduced 
 Yorkshire . clever young ladies gave highly 
 finished performance Saint - Saéns Pianoforte Trio F , 
 played solos artistically . Miss Wehner 
 sang great artistic intelligence interesting 
 series songs varied character . Subscription 
 concert , 1oth ult . , miscellaneous order , 
 artists engaged Miss Louise Dale , Miss Butt , 
 Messrs. Grover Plunket Greene , Mr. Moszkowski 
 pianist Mr. Gérardy violoncellist 

 Huddersfield Glee Madrigal Society , Mr. 
 Ibeson , gave varied programme concerted vocal music 
 December 19 . 16th ult . 
 Subscription concerts , Madame Amy Sherwin , 
 Mr. H. Verbrugghen violinist , Mr. Willibald 
 Richter pianist , Meister Glee Singers , 
 ensemble perfect , appearance . 
 artistic Brodsky Quartet Party responsible 
 Halifax Subscription concert roth ult . , 
 played quartets Beethoven Schumann 
 sympathetically 

 MUSIC WALES . 
 ( CORRESPONDENT 

 fourth International concert given Cercle 
 des Etrangers Monte Carlo devoted English 
 composers . programme included Mr. Frederick 
 Cliffe Symphony C minor , Mr. F. H. Cowen 
 ‘ Dances Olden Style , ” Mr. Edward Elgar 
 ‘ Imperial ’ ? March . concert , usual , 
 able direction M. Léon Jehin 

 Tue Musical Festival Lower Rhine , Whitsun 
 week , held year Aix - la - Chapelle , 
 conductorship Herren Richard Strauss F. 
 Schwickerath . works performed art 
 Liszt oratorio “ Christus , ” Beethoven Ninth Symphony , 
 Haydn ‘ Seasons , ” probably new work Richatd 
 Strauss 

 minor ( M 
 tt solo ingt 
 “ cert th 
 Buttykay , 
 CARLSRUH 

 Hutui.—Dr . G. H. Smith gave interesting lecture 
 Henry Purcell Literary Philosophical 
 Society , 16th ult . referring generally 
 neglect Purcell music conditions musical 
 art composer time , Dr. Smith gave historical 
 outline Purcell opera ‘ ‘ Dido ZEneas , ” work 
 subsequently performed choir thirty voices , 
 solos sung Miss Kathleen Mayes , Miss Evans , 
 Mr. Charles Ratcliffe , Mr. Turnbull . Mr. Kirby 
 accompanied pianoforte Dr. Smith conducted 

 MELBOURNE ( AUSTRALIA).—A special musical service 
 held December 5 , St. Paul Cathedral , 
 Beethoven C minor Symphony Brahms 
 “ German ” Requiem performed conductorship 
 Mr. Ernest Wood , Cathedral organist . large 
 capable orchestra engaged , service 
 commenced praiseworthy rendering Symphony . 
 Requiem , followed , Cathedral choir 
 largely augmented , , difficult 
 choruses work abounds , acquitted them- 
 selves creditably , giving abundant evidence careful 
 rehearsal . baritone solos sung Mr. Henry 
 Rofe , Cathedral choir , soprano solo . 5 
 excellently sung Cathedral choristers . 
 service concluded Bach minor Prelude 
 Fugue , performed assistant Cathedral organist , Mr. 
 R. J. Shanks . Mr. Ernest Wood , devoted time 
 pains secure adequate performance 
 masterpieces , heartily congratulated 
 result efforts . second occasion 
 , direction , Brahms Requiem 
 given Cathedral 

 PENZANCE.—A successful concert given St. 
 John Hall , 5th ult . , Penzance Choral 
 Society . work chosen occasion Sir 
 Arthur Sullivan ‘ Light World . ” oratorio 
 admirably performed choir orchestra , 
 solos efficiently sung Miss Mason , Miss E. 
 Rowe , Mrs. Cornish , Mrs. C. L. Taylor , Mr. W. Sampson , 
 Mr. J. Trebilcock . Mr. R. White presided 
 organ Mr. J. H. Nunn conducted ability . 
 interval performance Mr. Mrs. Nunn 
 presented Mr. T. B. Bolitho , M.P. , cheque 
 £ 500 , silver salver , silver tea coffee service , 
 album containing names subscribers . 
 presentation recognition long 
 valuable services rendered Mr. Mrs. Nunn 
 art music Cornwall 

 SALISBURY.—The members Cathedral choir , 
 assisted Close Ladies ’ Orchestra , gave concert 
 evening New Year Day Choristers ’ School . 
 programme included , Symphony Bach 
 “ Christmas ” Oratorio ( 2 ) , Minuet Trio ( Boc- 
 cherini ) , Elgar Serenade , ‘ “ ‘ Salut d’amour , ” 
 orchestra . following interesting selection part- 
 music sung : ‘ Queen valley ” ( Callcott ) , 
 “ pleasant month ” ( Beale ) , ‘ ‘ cloud - cap’t 
 towers " ( Stevens ) , ‘ ‘ shall win lady fair ? ” 
 ( Pearsall ) , ‘ ‘ silver swan ’ ( Gibbons ) . Violin 
 violoncello solos contributed Miss Carpenter 
 Mr. J. L. Davis , accompanied Precentor , Canon 

 large audience . aims Society 
 p perform work British composer concert 
 o complete orchestral accompaniments . aim , far , 
 ining met extraordinary support 
 good public , attendance concert 
 d larger town neighbour- 
 John hood years past . solo ‘ ‘ Hiawatha ” 
 charmingly sung Mr. Edward Branscombe , 
 nicon , contributed songs second programme . 
 St. soloists Miss F. Lane ( harpist ) Mr. James 
 lerful , Edgar ( violinist ) . ‘ miscellaneous selection included 
 t sixteenth century madrigals Eaton Faning 
 - song ‘ * Moonlight , ” unaccompanied ; 
 1 popular numbers March Chorus 
 duced “ Tannhauser ” ’ Soldiers ’ Chorus ‘ “ Faust , ” 
 nds jg accompanied orchestra . Dr. W. G. Eveleigh , 
 jg Society owes existence , conductor , 
 jg Mr. G. Ely accompanist . 
 kettle . BETHESDA.—Mr . E. D. Lloyd gave annual concert 
 Bethesda Chapel , 8th ult . , assisted 
 y. ff Mrs. Lloyd Mr. Emlyn Davies ( vocalists ) , Miss 
 worked — Louie James Mr. W. R. Watson ( violin ) , Mr. A. 
 — § Corrison ( flute piccolo ) . Beethoven ‘ ‘ Egmont ’ ” ’ 
 distinct JJ Schubert ‘ ‘ Rosamunde ” ’ Overtures included 
 JJ programme played instrumentalists 
 ind J named , supplemented Miss Eames Mr. Morris 
 ; tothe # ( pianoforte ) Mr. Lloyd ( organ ) . 
 modern BristoL.—Very successful performances “ Little 
 Bo - Peep , ” ’ fairy operetta children C. Egerton Lowe , 
 175 given St. Michael School - rooms , Mile Hill , 
 acreon # @ December 28 rst ult . Great pains 
 ylinders taken preparing work , received 
 etheus favour , encores numerous . 
 o , ” Cowes.—An excellent performance Sullivan ‘ * Golden 
 ars Mt Bi leend ” given H.R.H. Princess Henry 
 17 , bas Hi Battenberg , Victoria Hall , rith ult . , 
 te Northwood Band Chorus , numbering 
 . yo . orchestra ( led Miss Rutland ) performed 
 rces ( tht Hi xduous task efficiency , choir 
 rich Bi ieemed particularly happy work , especially 
 ischiitz beautiful unaccompanied choruses . soloists , Miss 
 roduced , Amy Sargent , Miss Lucie Johnstone , Mr. Henry 
 | @ ® Bi Tunpenny , Mr. Charles Tree , admirable . 
 d _ Mendelssohn ‘ ‘ Hear Prayer ” preceded cantata 
 ace , 80 " Bi vas sung appearance Royal 
 party , , learning motet included 
 , REAY . Blin programme , Royal Highness arrived 
 vginning concert , order hear . con- 
 dusion cantata , Princess Henry , Patroness 
 Society , selected cantata performance 
 nthis occasion , expressed gratification conductor , 
 5 . ” Mr. Frederick Rutland . 
 o insert GLOUCESTER . — successful performance 
 es fora -Sat cantata “ King Olaf ” given Choral 
 wr @eiety , 23rd ult . , Shire Hall . 
 ‘ additional tmmendable design Society perform 
 wld help modern composition season , rendering 
 = important work distinctly excellent , 
 e enous hoit singing precision confidence 
 Prarnishedggectestra ( led Mr. E. G. Woodward ) thoroughly 
 1 " ticient . solo vocalists Miss Rosina 
 — lammacott , Mr. Henry Beaumont , Mr. H. Sunman , 
 kindly ill thoroughly satisfactory . cantata 
 aa preceded Mackenzie breezy ‘ Britannia ’ ” ’ 
 nformationg , ture , Mr. A. Herbert Brewer conducted , 
 - anygeetved hearty commendation successful issue 
 aa ‘ te concert . 
 ee Hampton Wicx.—Mr . William Ratcliffe , 
 “ Triiey . ently resigned post organist Parish Church 

 ms , 18th ult . , recipient handsome } 
 hantel - clock , presented Vicar 

 WELLINGTON ( N.Z.).—Mr . Robert Parker , organist St. 
 Paul Pro - Cathedral , gave - annual concert 
 Choral Hall , December 14 , assisted Glee 
 Madrigal Society Wellington Liedertafel , 
 conductor . sang admirable style 
 madrigals , “ Fire , fire , heart , ” ‘ ‘ Matona , lovely 
 maiden , ” ‘ * love beauty ” ( Sullivan ) ; 
 fine selection modern - songs , included 
 Parry “ Phyllis ’ Stanford ‘ ‘ Corydon , arise ! ” ” 
 Liedertafel sang Myles B. Foster fine ' ‘ Ode Music ” 
 tenor solo chorus male voices . talented 
 pupils concert - giver , Miss Barber Miss Janet 
 Ross , played respectively Romanza Finale 
 Mozart D minor Concerto , Rondo Weber 
 Sonata C major . Dr. Kington Fyffe sang 
 refinement Purcell ‘ ‘ Knotting Song ” “ 
 laid earth , ” Mr. John Prouse gave splendid 
 rendering Tschaikowsky ‘ ‘ Don Juan Serenade . ” 
 concert unqualified success 

 WEYBRIDGE.—A successful concert given 
 Village Hall , 11th ult . , Miss Catherine Low . 
 concert - giver played Chopin Ballade flat , 
 Moszkowski ‘ Caprice Espagnol , ’’ Liszt ‘ ‘ Rhapsodie 
 Hongroise , ” . 12 , pieces , assisted 
 excellent rendering Beethoven “ Kreutzer ” Sonata 
 Mr. Rohan Clensy . vocalists Miss Hester Otway 
 Lieut.-Colonel Craig 

 Woop GreEen.—The Alexandra Musical Union Select 
 Choir Orchestra , comprising altogether 
 performers , gave creditable performance ‘ “ St. 
 Cecilia Day ” ( Van Bree ) , Bradley Hall , 5th 
 ult . , direction Mr. E. J. Deason . solos 
 sung Madame Minnie Jones . choruses , 
 ‘ * Brooks shall murmur ” ‘ “ ‘ way pleasure , ” 
 choral , ‘ ‘ Fragrant odours , ” especially 
 sung , second encored 

 MUSICAL TIMES.—FeEsruvary 1 , 1900 

 Published . 
 NEW EDITION FINALE 
 BEETHOVEN 
 CHORAL SYMPHONY 

 Tue ENGLISH TRANSLATION REVISED , PARTLY - WRITTEN , 
 
 NATALIA MACFARREN 

