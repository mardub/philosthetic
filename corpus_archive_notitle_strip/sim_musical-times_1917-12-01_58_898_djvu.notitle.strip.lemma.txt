a26 an 

 532 the MUSICAL TIMES.—Decemeper 1 , 1917 . 
 _ ond 
 D * ® LEWIS , Mus . Doc . , F E.1.S. , Warden , Ini or- | wa \NTED , Oboe , Cor Anglais , Conservatoin — 
 porate G f Church Musi s ; lesson , Pere na ! s arp tch . Loree , or Cabar . Bess 63 , Faulkner St 
 or by | c , in HARA MONY and COUNTERPOINT . 18 , be 4 m . 
 Street , Oxt Street , W | — — one C 
 DR . LEWIS ’ text - book : ' \ hire hy d.- -f = score pore 2P ianoforte Arrange 
 RMONY . ev s Lis sie on Beethoven 's " Rui Athe nm 
 ae . ( } - | or Pianoforte and pot the ras 3 » of the : , - 
 Dovt ( . ca et . ic n or so Full Seore of the iszt lso F WILL 
 Fuc ' _ me | Deliu i forte Concerto . full partic ular to Smith & Whink 
 ELEeME ? re M : . 6d . net . yy e Street , Leeds . 
 Dever ENT AN CAN Cut iM 2 . net . ~~ , 
 Pr UNCING VOCABULARY of MusicaL term , 6d , net | GECOND - HAND ORGAN MUSIC Ww ANTED . St 
 Dictionary oF Musicat Ter 6d . net . | t = t _ R Novello & Co. , Ltd - 
 material of MELOD\ 1 . net . Wardour Street , W.-1 . — . 
 I m visation . be net . - 2 : — _ _ _ _ _ — 0 ( 
 the te , will be f , for £ 1 . scan aa 
 tene . por : sale . Grove ’s Dictionary of Music . 
 RTHUR MANGE LSDORF F , L.R.A.M. , | I Vols . , in goo 
 4 A.R C.M. , pianist ( Speciali coach for Pi Forte Diplom : y Prou " ( Aug ner 's Edit 1 ): Harn Suitable 
 Paper Work by Correspondence . 497 , Coventry Ro ad , Birmir gham | o u . bur ter point and Canon ; Fv 
 | R.A.M.—ARTHUR Legg ay LSDORFF | ; — \ 
 de 1 September , 1917 , su t to state that he be a 
 oor oe Reena merge fa dpe 1 advert — i m 2 ) ASS singer should buy " SONGS of P the 
 iaeiaibaett NIGHT " ( OAKLEY ) two set ? no . x to 4 and 5 - Pi 
 Mss . . HELENA MARKS prepare for | 3 - 64 . each set . Weekes & Co. , 14 , Hanover Street , W.- 
 -a.m. ther i ninations . P forte , 7 mony , | 
 * & e. Le ( r by ponde Pupils oe RCH MU SIC by B. Luard- Selby , publish 
 receive the pi t m re " " t | + by Winthrop gers , mited , 18 , Berners Street 
 L.R A.M i e ( 10 , M : Rd . , West Kensingt | W.-t Magnificat , no . 1 1 , " * the Lord hear th d. | 
 sacchari — — | — " islepndciemal S 
 I R. H L. MIDDLE rON , Mus . Doc . , | x od oe tb DEUM in F. MAG NIFICAT and NUNG ac 
 R UM RC i ve perience in Privz DIMITTIS tn F. 3d . each . by A. Toase , Novello & Co. , Ltd dem t 
 g f i ear vi tha » of - Mi ) 
 Pup i i Degre t the | niversitie , R.A.M RC . M. , 
 k ve Bernera Street , W.-1 ' | Pianoforte Teachers examination for th 
 I [ vr WORKERS Fox THe CHURCH ARMY.—| A.R.C.M. Diploma . — 
 and layman urgently need ( minimem four | suide for candidate . by A. H. Fillingham , A.R.C.M. ( 
 . . . I to cl rl " Car wi € kind Vusical J ald . 
 " ee | ' os Carlile . I D a coe ae * fe " m wi gly hel ; J. H. Thomas , Esq 
 Br Ss M \ l Wa 
 - - - a z = — _ — | pr oo S ] 
 HEREFORD CATHEDRAL . , = eee Li 
 » } " aon : iN¢ F A > 4 ANY , zimite , 
 A Di VICA CHORAI ( 1 I , | — 
 it M let t rv e DAT - yITee at 1 — 
 a rhe R D CENTRAL DEPOT for RUSSIAN MUSIC 

 rganist WANTED.—Cottenham Park 

 i 

 some of its essential quality unless precede } y 
 its opposite . the slow movement and Scherzo of a 
 symphony be so place as to provide relief to the 
 listener . there be no fix order . we find in on 
 case the first movement be follow by an Adagio , and 
 in another by a Scherzo . if we look at the sequence 
 of movement in the ' Eroica ' and the Chora 
 Symphonies , we shall see at a glance why Beethoven 
 adoote a different order in eac h case 

 very far from this ideal be a programme before I 
 in which three fugue be play consecutively . asif 
 | that be not enough , the recitalist have actually choose 
 | three in . flat , one of they be the ' St. Ann ' ! 
 | the Bach example alone would provide enough of 
 | both form ond ke 

 Mr. W. W. Cobbett write to we suggest : 
 ' an orthoepic glossary 

 he say : your reader be , I be sure , grateful for the article 
 by Messrs. Montagu - Nathan and S. W. Pring on Russian- 
 English transliteration , and , regard aid to pronunciation , 
 it be only too true that those which take a phonetic form 
 ' distort the write word ' ; but , Sir , how can this be 
 help ? every page in an english dictionary contain aid 
 to pronunciation which grate upon the sensitive nerve of a 
 scholar , but the man in the street be also to be consider , 
 and for he phonetic be useful , even necessary . if a 
 musical man I think he would be glad if you , or one of your 
 reliable correspondent , would provide an Orthoepic Glossary 
 of all composer of foreign birth . perhaps he have few oppor- 
 tunitie of attend lecture , and seldom enjoy opportunity 
 for conversation on musical subject . in that case he would 
 like to know from an authoritative source how to pronounce 
 foreign composer ’ name , some of which it be the custom to 
 anglicise whilst other retain their original pronunciation . I 
 confess that be , through frequent residence abroad , some- 
 what of a cosmopolitan , I should be glad to be tell how to 
 pronounce Haydn — whether ha - den or Hi - den ; Mozart — 
 whether Mo - zart or Mo - tsart . I care not which , but I 
 should like to know once and for all . I think that the 
 agreed pronunciation of Beethoven be Bate - hoven , but 
 occasionally one still hear the name call Beet - hoven . 
 there be little excuse for pronounce the name of Schubert 
 schu - bare — it be not french , but it be very often do . let 
 us then have an Orthvepic Glossary for the sake of folk who 
 have no pretence to belong to the inner circle of musical 
 literate , and for the sake of cosmopolitan who desire to 
 know ' what the public want ' in the way of pronunciation 

 the compiler would find some difficulty in his way , and 
 would acquire considerable kudo if he do his work well . 
 the sound of some french name can not be convey , by 
 phonetic mean , to a stubbornly british tongue — such a 
 name as Saint - Saéns for example . to write that down as 
 Sang - Song would be simply unendurable , but I think I can 
 suggest a way out of the difficulty . it could be render as 
 follow — Sin - Son ( Fr . ) . this would serve to indicate to the 
 inquirer that a french turn of the tongue be require , and he 
 might be enjoin , in the preface , in such a case to apply to 
 a french friend , ask he to pronounce it , and then do his 
 good to imitate the sound he hear . fortunately most 
 musician have a fair work acquaintance with the 
 french language , and it be an exception to hear Chopin 
 a strange to say , such knowledge would be 
 useful in the case of Dvordk . I believe that Dvor - Jacques 
 be quite the most accurate way of pronounce a name that have 
 give some trouble in England to talker on musical subject 

 Mr. Mallinson encounter some risk when on November 9 
 he give a long recital entirely of his own song . his output 
 — we believe he have compose three hundred song — be 
 always of a high quality , in which the mood of the poem 
 be intimately reflect not only in the vocal part but also 
 in the pianoforte part . the new song introduce on 
 this occasion show that the fount of his inspiration have not 
 run dry . such music as Mr. Mallinson ’s must be perform 
 perfectly if it be to go home , otherwise it have well be 
 leave alone . in this connection it be fortunate for the 
 composer and the audience that the service of Madame 
 Kirkby Lunn have be enlist . she sing her large share 
 of the programme with rare distinction . the remainder of 
 the song be sing sympathetically , but not always in 
 perfect intonation , by Mrs. Mallinson , and the composer be 
 at the pianoforte throughout 

 the London String Quartet on October 26 produce Mr. 
 Herbert Howell ’s prize Phantasy - Quartet . it prove to be a 
 most attractive work . one could admire the originality of 
 the composer ’s idea without trouble a great deal as to 
 their folk - song basis . other item be Mozart ’s Quartet in 
 G major , no , 12 , and the César Franck Quintet , in which 
 Miss Myra Hess co - operate . on November 2 the party 
 play the Quartet by Beethoven , in d , op . 18 , no . 3 , and 
 Haydn in b flat , op . 64 , no . 3 . Mlle . Fernande Pironnay 
 sang with much grace Chausson ’s ' Chanson Perpétuelle . ' a 
 * Poem ' for Violin , Harp , and Organ , by York Bowen , wasan 
 interesting number . on November 9 Ravel ’s quartet in f 
 please and at time puzzle most hearer , as it usually do 

 a threnody in e flat for string quartet by J. B. McEwen , 
 the text of which be ' the flower of the forest be a ' wede 
 away , ' be a welcome item . it have many beautiful moment , 
 and provide no doubtful problem for the auditor . the 
 use of the viola be particularly striking . Faure ’s Quartet 
 for pianoforte and string in C minor be the third and last 
 number on the programme . Mathilde Verne be the pianist . 
 on November 16 Schubert ’s beautiful Opus 29 , no . 1 , begin 
 the programme , and Schumann ’s Pianoforte ( Quintet in e flat , 
 Op . 44 , with Fanny Davies at the pianoforte , close it . 
 between these two masterpiece a new Quartet in A minor , 
 Op . 6 , by Armstrong Gibbs , be perform for the first time . 
 it be in three movement . this composer be feel his way 
 to an idiom if he have not definitely reach ease and fluency . 
 one speculate as to whether some of the harmonic effect 
 present in the playing realise exactly the conception of 
 the composer . the second movement , an Andante , begin 
 attractively but deviate later into some strange path . the 
 last movement also have some attraction . perhaps the whole 
 Quartet would give a well impression on a second hearing . 
 Mr. James Levey , the new leader , who take the place of 
 Mr. Albert Sammons ( who be on service ) , improve at every 
 hearing . he be an undoubted acquisition 

 a viola recital be a rare event . perhaps it will become 
 less a rarity after the success Mr. Lionel Tertis secure on 
 October 24 , when he play in a Mozart Concertante for violin 
 and viola with Mr. Sammons . a solo piece compose by 
 J. B. McEwen , ' Chaleur d’Ete , ' be very effective 

 the recital of the complete Beethoven Sonatas for 
 violin and pianoforte by Mr. Albert Sammons and Mr 

 a befitting tribute to the memory of the late Stan } 
 Hawley be pay on October 25 . he have specialize , 
 recitation to music , and it be therefore meet that s 
 specimen be include in the programme . in these Mig 
 Lena Ashwell ( accompany by Sir Henry Wood ) take pax } 
 other composition , piancforte solo , and song by the hy » 
 musician be perform 

 a concert - recital of Gounod ’s ' Faust ' be give by the 
 Birmingham Choral and Orchestral Association at the Town 
 Hall on November 10 , under Mr. Joseph H. Adams ’s 
 conductorship . there be a packed audience , and every 
 number be receive with enthusiasm , especially the 
 Soldiers ’ Chorus . choir and orchestra do well , and the 
 principal — Madame Parkes - Darby , Miss Eva Rowlands , 
 Miss Mabel Palfreeman , Mr , Walter Ottey , Mr. A. 
 Wrigglesworth , and Mr. James Howell — be elficient 

 the second concert of the season promote by the 
 Birmingham Chamber Concerts Society be give at the 
 Royal Society of Artists ’ Gallery on November 13 , the 
 executive again be the Catterall String Quartet . Haydn 's 
 Quartet , Op . 76 , no . 4 , in e flat , Dohnanyi ’s Quartet in 
 d flat major , Op . 15 , and Beethoven ’s colossal quartet in 
 e flat , Op . 127 , be all superbly perform 

 Mr. Landon Ronald , always a great favourite in 
 Birmingham , conduct the third Symphony Concert on 
 November 14 . his influence be at once feel , the rank 
 and file of the New Birmingham Orchestra loyally respond 
 to his beat . Rachmaninov ’s Symphony No . 2 , in e minor , 
 Op . 27 , which he first introduce here at the Promenade 
 Concerts in 1914 , be a strong feature . the novelty of the 
 evening be Stanford ’s fifth ' irish Rhapsody ' ( still in 
 manuscript ) . found upon irish folk - song , like its 
 predecessor , the opening and final section be in the 
 style of a march , and the intervene slow section of a 
 dirge - like character . the performance be in every way 
 admirable . M. Arthur de Greef , another great favourite 
 with local audience , secure a triumph with his gorgeous 
 interpretation of Grieg ’s Pianoforte Concerto 

 the compiler of this monthly record of Bournemouth ’s 
 musical doing be at length compel , owe to a continuous 
 increase in his professional duty , to limit the scope of his 
 observation in future ( with possibly rare exception ) to the 
 series of Symphony Concerts , these , to his great regret , be 
 the only function at which his regular attendance be in any 
 way certain . these long - establish concert , however , form 
 for most people the premier attraction in the Winter Gardens 
 scheme of concert , besides be without doubt the most 
 reliable indication of the town ’s position as a centre of 
 enlightened and progressive musical intelligence . if the 
 attendance during the past month can be regard in the 
 light of a substantiation of the claim that Bournemouth count 
 for something in the musical life of the country , then indeed 
 be there reason for satisfaction . the number of person 
 attend the first six concert of this season have be double 
 that of last — a proof surely that music - lover from all part be 
 find out that residence at Bournemouth have agreeable 
 attraction to offer 

 the excellence of the initial concert of the series have be 
 fully sustain by succeed programme . Mr. Dan Godfrey 
 have undauntedly overcome many harassing difficulty for 
 which the war have be mainly responsible , and by sheer 
 energy he and his instrumentalist have weld themselves 
 into a combination that though rather small in point of 
 number than could be wish , can hold its own with any 
 other similar organization . many of the work at recent 
 concert have be admirably execute , notably the fourth 
 Symphoay in e flat , of Glazounov , this exacting composition 
 receive the good interpretation of its numerous intricacy 
 ever hear at Bournemouth . then again , on November 15 , 
 the well - know Mozart Symphony in the same key be most 
 tastefully perform . and who would not say that this be 
 actually the great test of the two?—for Mozart and 
 Beethoven lie bare the weakness of an orchestra far more 
 quickly than the general run of modern composer . other 
 Symphonies of the so - call ' classical ' type appear in 
 last month ’s programme be the a major ( no . 7 ) by 
 Beethoven and the no . 2 in d- by Brahms . a first per- 
 formance in England of Ippolitov - Ivanov ’s Symphony No . 1 
 will probably also be its last here , not a single passage 
 relieve its deadly monotony and uninspired staleness of 

 phraseology . another initial english production be 
 that of Goedicke ’s ' dramatic ' overture , about which 
 also there be nothing particularly striking . perhaps 

 however , Mr. Godfrey have scme score of really prominent 
 Russians up his sleeve for future performance . an 
 orchestral ballad , ' the night watch at Sea , ' by Percy 
 Godfrey ( conduct by the composer ) , receive its actual first 
 performance here , and a Miniature fantasy for string 
 Orchestra , by Eugéne Goossens , jun.—an exceedingly charm- 
 e composition which Mr. Godfrey present most effectively 
 — be perform for the first time at Bournemouth . yet 
 another pleasing novelty be Svendsen ’s Legend , ' Zorahayda . ' 
 among old friend we be delighted to renew acquaintance 
 with the ' prometheus ' Overture ( Beethoven ) and the 

 serenade for string ( Elgar 

 the postpone opening of the Winter session of the 
 Corporation Concerts at Plymouth take place on November 3 , 
 when the band of the R.G.A. be conduct by Mr. R. G. 
 Evans in a programme which contain only one novelty — a 
 * lament ' for Organ and Orchestra by Foulds , in which the 
 Borough Organist collaborate with the Band 

 at the Sunday Concerts in the Theatre Royal at Plymouth , 
 on October 28 , Miss Carrie Tubb sing and the band of the 
 R.G.A. play music by Tchaikovsky and Gounod , and 
 accompany Musician East in a ' romance ' for Violin by 
 Max Bruch . on November 4 , Mr. Frank Tapp play the 
 ' Waldstein ' Sonata ( Beethoven ) , and piece by Chopin , 
 Liszt , Balakirev , Brahms , Schumann , and some of his own 
 composition . violin and harp music , play by Sergt . 
 F , A. Wellington and his daughter , Miss Isabel Wellington , 
 be a feature of the concert . violin music be to the fore 
 on November 11,when Miss Evelyn Tyser play with much 
 artistry piece by Henley ( ' au mois d’Avril , ' a charming 
 piece ) and Lederer ; and on November 18 Miss Perceval 
 Allen be the vocalist , and the band of the R.M.L.L , 
 conduct by Mr. P.S. G. O'Donnell , play an excellent 
 programme , include the ' Benedictus ' from Granville 
 Bantock 's ' suite of english scene , ' a ' Caprice Espagnole ’ 
 by Rimsky - Korsakov , and the ' Kol Nidrei ' of Max 
 Bruch , with Band - Sergt . Pike as solo cellist 

 the Education Department of the Plymouth Co - operative 
 Society secure immense audience for its weekly Saturday 
 Concerts . music of good standard be present , chiefly by 
 local artist . the Sunday Concerts on the Pier be invariably 
 throng by large audience , who enjoy the music provide 
 by the military combination of the band of the R.G.A. and 
 R.M.L.I. alternately . a miscellaneous concert give in the 
 same pavilion on October 31 for the Homceopathic Hospital 
 be financially and artistically successful 

 Exeter Army Pay Corps band confer much pleasure 
 in play at a concert on November 7 , at which Miss Freda 
 Rowe 's party of artist , vocal and instrumental , perform . 
 pianoforte solo be give by Miss Adeline Curtis , the Red 
 Cross Trio contribute selection , and Mr. Samuel Bishop , 
 salo bass at the Cathedral , sing 

 in Torquay Pavilion , on November 1 , the Pavilion Select 
 Orchestra make its first appearance in chamber music . the 
 member be Mr. T. S. Bennett ( violin ) , Mr. J. Hardaker 
 ( viola ) , Miss Amy Porter ( ’ cello ) , and Mr. Edgar Heap 
 ( pianoforte ) . in this combination they play Beethoven ’s 
 Op . 16 and Op . 24 ( Violin Sonata ) , and other number . 
 the military band of Plymouth R.M.L.I. pay a visit to the 
 Pavilion on November 1 - 5 . specially effective number 
 be Hubert Bath 's ' wedding of Shon Maclean , ' a keltic 
 Suite by Foulds , a dervish ' scene in the Soudan ' by Sebek , 
 and ' Czardas ' ( ' the Ghost of the wood ' ) by Grossman . a 
 party of artist give a concert in the Pavilion on November 5 , 
 on behalf of Belgian Relief Funds , when M. Emile de 
 Vlieger play ’ cello solo , include Elgar ’s ' Chanson de 
 Nuit ' and a Serenata by Glazounov . the vocalist be 
 Mr. Laurence Leonard and Mr. Kenneth Ellis , the pianist 
 be a belgian artist , Madame Marie Joliet , and Madame 
 Karina , a danish ballerina , give speciality dance , include 
 ' the good angel , ' a dance - poem create by herself 

 Lee Mill , a tiny woodland village , have the unusual 
 pleasure of a concert on October 19 , the performer be 
 mainly from Plymouth , and include Misses E. and M. 
 Allen and R. Reed , who play pianoforte trio 

 Madame Clara Batt , assist by Madame Stralia , 
 Lady Tree , and Miss Adela Verne , give a concert on 
 November 10 

 we be glad to announce that the Edinburgh Choral 
 Union will give two concert , on March 6and 7 . the Hallé 
 Orchestra have be engage , and Sir Henry Wood will 
 conduct . the choral work to be perform be Elgar 's 
 ' Spirit of England ' and Beethoven ’s Choral Symphony . 
 the detail of the orchestral part of the programme have not 
 yet be fix . this commendable enterprise deserve the 
 liberal support of Edinburgh musical folk 

 GLASGOW 

 the MUSICAL TIMES.—Decemser 1 , 1917 

 a selection of piece which include part of Bach ’s ' Jesu , 
 priceless Treasure , ' Bantock ’s ' vanity of vanity , ' and 
 Beethoven ’s Choral Fantasia , the solo - pianoforte part in 
 the last - name be play by Mr. Halstead , and the 
 orchestral part on a second pianoforte by Miss Ailie 
 Cullen . associate with Mr. Halstead at the pianoforte , 
 Mr. Arthur Catterall give a first - rate performance of the 
 first movement of Grieg 's Violin Sonatain G. Miss Ailie 
 Cullen play the pianoforte accompaniment with great 
 acceptance 

 LIVERPOOL 

 contrast Debussy ’s sea - music with any of the recognise 
 notable instance of ocean - inspire orchestral writing , and you 
 must note the absence of any suggestion of ground - swell , — 
 ' ocean ’s diapason . ' Ejigar , Delius , Bantock , MacDowell , 
 among latter - day worker always impress I as having 
 capture something of the vastness of the deep , the tireless 
 heaving of mighty water : Debussy ’s Suite be ' water- 
 music rather than ' sea - music , ' concern with the play 
 of light on dance , sunlit wave , suggest much 
 the same atmosphere as that conjure up by Ravel 's 
 ' Jets d’'Eaux ' — intensely interesting to any mind attuned 
 to the varied mood of the sea , but leave the feeling 
 that the hearer have be in touch with the miniature 
 rather than with the infinite . hear Stravinsky ’s 
 ' firework ' again after a course of the later ' Bird of Fire ' 
 and ' Petrouchka , ' the early work seem to fali into its 
 proper perspective as a sort of advanced study in the 
 Stravinsky medium . the band toss it off as though its 
 execution be the mere child’s - play . Mr. Arthur Catterall ’s 
 solo - playing advance from strength to strength , and it be 
 good to hear he play the Tchaikovsky which we have 
 have so often from his master , Dr. Brodsky . the war - period 
 have at any rate enable two british solo - violinist to emerge 
 quite definitely as player of the first rank : Mr. Arthur 
 Catterall be one , and Mr. Albert Sammons be the other 

 the Catterall quartet season hold promise of several 
 interesting work new to Manchester . Speaight ’s 
 ' shakespearean Sketches , ' Dohnanyi ’s D flat Quartet , 
 Chausson ’s double Concerto for pianoforte , violin , and 
 quartet , and Halvorsen ’s Passacaglia for violin and viola . 
 the initial Brodsky quartet concert , on November 1 , 
 consist of a Haydn and a late Beethoven Quartet , and 
 Busoni ’s no . 2 Sonata in E , play by the leader and 
 Mr. Forbes . probably most member of the audience fail 
 in their attempt to get at close grip with this Sonata 

 the Bowdon Chamber Concerts Society open its tenth 
 season on October 31 , the Catterall Quartet play 
 Dittersdorf in e flat , the Arensky Quartet on the ' Légende ' 
 theme of Tchaikovsky , and the Brahms C minor . Miss Olga 
 Haley give two example of the 477a Antica , two favourite 
 Debussy song , and ' soft - footed snow , ' by Sigurd Lié , 
 which have to be repeat notwithstanding the ban on 
 encore at these concert 

 I , 1917 

 the Choral Union begin on October 31 with an Elgar 
 concert , to which the presence of the composer give 
 additional éclat . he conduct a performance of ' the 
 dream of Gerontius ' which , though otherwise a fine one , 
 be mar by the hoarseness of the tenor , Mr. Gervase 
 Elwes , who be suffer from an attack of laryngitis , but 
 as it be too late to secure a substitute , very pluckily 
 struggle through his part . there be a rumour that if it 
 come to the bad , Miss Agnes Nicholls have volunteer to 
 fill the breach , and one must admit that to hear a ' Gerontia , ' 
 when sing by so gifted an artist , would have be an 
 exceedingly interesting experience . the other part be 
 take by Miss Phyllis Lett and Mr. Herbert Brown , who be 
 too well - know to need comment . Miss Agnes Nicholls ’s 
 task be to sing the solo in Elgar ’s ' Spirit of England , ' the 
 first part of which be new to Leeds , and indeed to 
 Yorkshire . the sincerity and absence of pose or affectation 
 in the work — which should form so perfect an antidote to the 
 german ' hymn of hate ' that one would like , for once , to 
 hear they in the same programme — make a deep impression . 
 on November 7 the Leeds Philharmonic Society open its 
 season with an orchestral concert , in which the Hallé 
 Orchestra , conduct by Mr. Eugéne Goossens , jun . , take 
 the principal share . a welcome feature of the programme 
 be that for once in a way british music be in the ascen- 
 dant in an orchestral concert . Mr. Holbrooke conduct 
 the first performance in this country of his ' lyrical ' Violin 
 Concerto , a work which have some angularity but many 
 individual and interesting feature . it be finely play by 
 Mr. John Dunn , to whom it be dedicate . young George 
 Butterworth ’s ' Shropshire Lad ' rhapsody , which be first 
 hear at the Leeds Festival in 1913 , remind we of the 
 tragedy by which this more than promising composer have 
 be cut off at the moment when he have give earnest of a 
 brilliant career . then the Philharmonic Society ’s new choir- 
 master and conductor , Dr. E. C. Bairstow , who succeed Mr. 
 Fricker in that office , conduct a very finished and expressive 
 performance of his fine anthem , ' Lord , thou hast be 
 our refuge , ' write for the last Festival of the Sons of the 
 Clergy at St. Paul 's . this work sustain the high 
 tradition of Anglican Church music — dignified , sober , yet 
 without dryness or mere erudition . Stanford ’s late ' irish 
 Rhapsody ° be the fourth work by a british musician in the 
 programme , and it prove a brilliant and admirably 
 construct fantasy on some characteristic melody . on 
 November 11 the Choral Union and Philharmonic Society 
 join force in a free concert give to soldier in the 
 Town Hall , and with the co - operation of the Leeds Music in 
 War - time Committee , and under the conductorship of Dr. 
 Coward , give portion of ' the hymn of praise , ' together 
 with Parry 's ' Blest Pair of Sirens . ' the soloist be Miss 
 Elsie Suddaby and Mr. Henry Brearley , and the Leeds 
 Symphony Orchestra co - operate . a similar concert be to 
 be give on December 2 , when Dr. Bairstow be to conduct , 
 among other thing , a selection from ' the Creation . ' on 
 November 10 the Leeds Saturday Orchestral Concerts be 
 resume . Mr. Julian Clifford have here take the place of 
 Mr. Fricker , the founder and honorary conductor of these 
 concert . a large audience be present to hear Mr. 
 Clifford ’s reading of Beethoven ’s C minor Symphony and 
 Tchaikovsky ’s ( or must I write Chaikovsky ’s ? ) b flat minor 
 Pianoforte Concerto , the solo part in which be play by 
 Mr. Anderson - Tyrer with splendid force and warmth . on 
 November 14 the Leeds Bohemian Concerts introduce 
 String Quartets by Arnold Trowell ( in g ) and Glazounov 
 ( in a ) , tne former be new to Leeds . one of Mozart 's 
 maturest quartet be also bronght forward . Mr. Trowell ’s 
 work show a real poetic instinct join with a thorough 
 musicianship , and the appreciation of the capacity of a 
 string quartet that one would expect from an expert 
 violoncellist . the quartet party furnish a striking 
 illustration of the gradual inroad of woman musician , and 
 now only the leader , Mr. Alex . Cohen , be of the masculine 
 persuasion ; but his colleague — Miss Elsa Stamford , Miss 
 Lily Simms , and Miss Kathleen Moorhouse — prove 
 thoroughly equal to their responsibility , and sustain their 
 part in a remarkably good ensemble . other Leeds event 
 must be refer to very briefly . at the Leeds University 
 Mr. W. H. Williams , of the Parish Church , give on 
 October 23 a recital - lecture on ' Parsifal . ' illustrate by his 

 and Miss Lily Simms play Sonatas for pianoforte and viok 
 by Brahms ( the Clarinet Sonata in e flat ) and Winkler ; ang 
 on November 20 Dr. Frere , assist by Miss Elsie Suddaby 
 as vocalist and the Leeds Bohemian Quartet , give the first 
 of a series of lecture - recital on russian music , deal with 
 Glazounov . at St. Aidan ’s Church Mr. H. M. Turton 
 give a very interesting recital of modern organ music , ang 
 play in brilliant style work by Vierne , Bonnet , Pietro 
 Yon , Jongen , and Stanford . oa November 14 Madame 
 Clara Butt ’s concert - party visit Leeds . in connection with 
 the ' music in War - time ' scheme , which have now give 
 nearly six hundred concert to soldier in the camp and 
 hospital of the district , Mr. Brearley give on November ; 
 a vocal recital , assist by Miss Morkill , an accomplished 
 amateur ' cellist , and on November 20 Mr. Frederick 
 Dawson give two delightful impromptu pianoforte recital to 
 the soldier in the hospital at Beckett ’s Park 

 other town 

 at Huddersfield the Philharmonic Society , of which 
 Mr. W. R. Ward be conductor , give on October 20 a per- 
 formance of Beethoven 's first Symphony , which reflect 
 credit on an orchestra largely amateur in its composition . 
 the powerful Huddersfield Choral Society give on 
 November 2 , under the composer ’s direction , a fine per- 
 formance of ' the dream of Gerontius , ' with Miss Olga 
 Haley as an excellent representative of the Angel , Mr. Elwes 
 — partially recover from his indisposition at Leeds — and 
 Mr. Charlesworth . in ' to woman ' and ' for the fall ' 
 the solo be , at very short notice , sing with nice feeling 
 by Miss Doris Hall . the Halifax Chamber Concert on 
 November 9 introduce Pianoforte Quartets by Chausson 
 and Dvorak , play by Mr. and Mrs. Rawdon Briggs , Mr. 
 Seth Lancaster , and Miss Lucy Pierce ( pianoforte ) . on 
 November 15 the Halifax Choral Society — in which Mr. 
 Fricker have be succeed by Mr. C. Hl . Moody , of Ripon 
 — give Bach ’s ' a Stronghold sure ' and Sullivan ’s ' golden 
 legend . ' the choir sing with intelligence and finish , and 
 the soloist be Miss Manson , Madame Beaumont , Mr. 
 Mullings , and Mr. Ranalow 

 Mr. Frederick Dawson , together with a very clever pupil 
 of his , Miss Guendolen Roe , give a very enjoyable pianoforte 
 recital at Hull on November 17 , when they play Schumann ’s 
 beautiful Andante and Variations , and another less familiar 
 piece for two pianoforte in Henselt ’s own arrangement of 
 his ' si oiseau j'etais , ' with a second pianoforte part impose 
 upon the original . likeso many of Mr. Dawson ’s recital , 
 this one be for the benefit of * music in war - time , ' which 
 have greatly benefit by his splendid service 

 we regret that our Bristol letter be unavoidably hold over 

 the Derbyshire Advertiser record the performance of an 
 ' Adante Bon Mots ( Beethoven ) . More bad writing by the 
 critic 

 admirable pianoforte - playing ; on November 6 Prof. Rogers 

