entry tenor bar 1 page 9 , 
 right - hand chord shortened quaver 
 toa semiquaver . tricky managed 
 neatly , reduction deferred till treble 
 entry subject bars later . point 
 plenty power reserved final page . 
 concluding voluntary big service big 
 building better truly great 
 movement . decidedly difficult manual 
 work ; feet , usual Rheinberger , 
 comfortable time 

 Jntermezszo opens _ simple 
 Beethovenish theme — fact , recalls slow 
 movement Fourth ( E flat ) Pianoforte Sonata 

 dropping leading - note _ favourite 
 Rheinberger trick ( Ex . 1 instance ) . 
 theme subjected little way 
 development , bulk movement 
 concerned pleasant secondary motives 

 department song ranks 
 - raters . Parry instrumental music 
 destined early oblivion , 
 hardly denied composer 
 songs choral works tbe big 
 men . Chopin approached best save 

 writing pianoforte , , , smaller 
 forms . Sowith Rheinberger . Muchof great output 
 excellent ( particularly charming 
 things small pianoforte pieces ) , 
 organ music reach stature . 
 position Bach . claim 
 extravagant musician knows little 
 organ repertory . , apart 
 Beethoven , composer written Sonatas 
 pianoforte violin 
 excellence Rheinberger wrote 
 organ ? , farther , boldly ask 
 Beethoven thirty - 

 high level 

 material student , Rheinberger organ 
 works invaluable , chiefly difficulties , 
 like Bach , ‘ faked ’ shirked . 
 texture continuity admit 
 halts stop - hunting . organ music depends 
 registration , , tried 
 articles , — , — 
 composer indicated . 
 fact remains considerable organ 
 composers played satisfaction 
 pedal - pianoforte organ manual 
 stops — Bach Rheinberger . Josef 
 shares likenesses John Sebastian — 
 excels fugue writer ; organ music 
 true architectural spacious quality . 
 enjoy _ pedal - pianoforte 
 pianoforte - duet arrangements composer 
 . Similarly lot pleasure 
 like arrangements Haydn Symphony 
 Mozart Quartet . cases style 
 forget original form . 
 music bears closest scrutiny gua music , 
 delivers message real 
 medium . test double - barrelled , 
 estimable works brought winged 
 onone orthe . Rheinberger comes 
 honours . Rank asa 
 composer pianoforte , chamber , orchestral , 
 operatic music ; department rose far 
 level works showed 
 stuff makes big 
 men . Opinions differ nearness 
 approach Bach organ composer , believe 

 pianoforte superior organ works . 
 , Rheinberger include such| 
 immature feeble works Sonatinas G| 
 major G minor , Quasi una Fantasia in| 
 E flat , Sonata A//a Tedesca Beethoven . 
 add Sonatas eighty - 
 short pieces , Rheinberger 
 best , mass fine organ music 
 parallel found outside Bach . | 
 difficult understand , years | 
 ago , critics placed 

 Rheinberger second } 
 merely competent workman Merkel , 
 spoke important organ composer 
 Mendelssohn . , long run , 
 position likely menaced 
 German rivals — Reger Karg - Elert . , | 
 power Germany , long 
 tried found wanting . 
 works remain high level ; 
 magnificent page followed plunge | 
 depths dry   platitude . Karg - Elert began | 
 splendidly , Sérty - Choral Improvisations | 
 dozen works merely 

 join common chorus civilised humanity , 
 sing tune way 

 nationalists distinctly national 
 type style , slogan ‘ folk- 
 song . ’ attitude easy appreciate . 
 greatest national treasures country 
 store folk - song , Wales store rich 
 country world . year 
 new folk - melodies discovered diligent 
 workers Welsh Folk - Song Society , 
 prove fruitful source inspiration 
 Wales ‘ national ’ composer comes . 
 day , school . Sing 
 lovely old folk - songs , time 
 forget learn Bach , 
 Mozart , Beethoven , rest glorious 
 company music masters 

 schools germ matter 
 , better Wales 
 forego wrangling work 
 common good country . , 
 undoubtedly , Wales day world music 
 dawn sooner 

 
 Ww 

 
 rry reasoning clear , appear sound . con- DOM POTHIER 
 Perr , clusion : . , _ Les Tablettes de la Schola ( January ) , Amédée 
 hat | ory old Greece , chief musical | Gastoué outlines biography Dom Pothier 
 c } instruments notation instrumental ( 1835 - 1923 ) , describes important played 
 epoch . Sj ; music originate pentatonic principle , music restoration Gregorian chant . 
 dral . remained essentially pentatonic collapse s ‘ . view 
 e old world . Dom Pothier work began early ’ sixties , 
 fines ; 1868 preliminary labour ended , principle 
 dens ‘ MALBROUCK ’ SONG restoration discovered . book , famous 
 cao e ; ‘ Mémoire des Mélodies Grégoriennes , appeared 1880 
 e Abbey issue , Max Friedlander retraces ] . 1888 Paléographie Musicale founded 
 forfl thoroughly evolution , wanderings , family } Dom Mocquereau co - operation . 1892 
 , hearing . ties famous tune . composers 1914 , Dom Pothier published edited Revue du 
 S gained , f tune—‘consciously uncon- Chant Grégorien , Pius X. acts 
 il kind sciously’—the following named : Beethoven,| # ppoint President Special Gregorian 
 happy § Weber , Albrechtsberger , Schumann , Berlioz , Délibes , Pontifical Commission ; 1904 Pope entrusted 
 ethan works , § Bizet , Leoncavallo . distribution ranges hin cohen Sy " aman = 
 bees f Je - F : : = 8 work modern reform old 
 ave 1 | France Russia , Germany Palestine , } ( Church music started ; principles 
 little f Britain Canada , Spain Chilli . followed . 
 ; retirement 
 old SATIE NEW ITALIAN PERIODICAL 
 ae March issue Revue Musicale } ( February ) number Za Prora , 
 devoted Satie . article Charles Keechlin| monthly organ newly - founded Corforazione 
 calls special notice , constitutes | de//e Nuove Musiche ( headed d’Annunzio , 
 vbley | convincing piece praise written late on| Casella , Pizzetti ) , begins following 
 rformances § - discussed composer . Keechlin firm | declaration principles : 
 ement § believer musical quality Satie humour periodical probably described 
 direct apart verbal humour . Socrate ‘ vanguard ’ organ . epithet applicable 
 nmissioners § writes : experimental , sincerely hope 
 y ( Empire ; , ae apply . , certain extremists 
 concerts , think , Satie greatest work , consider conservative , 
 Exhibition wonder people like , intend grope blindfold future . 
 Dominions altogether new original . Satie object create Italy new musical consciousness 
 wae Socrates talk Socrates wished , founded strong widespread musical culture , 
 ; attempt rhetoric hardly co - operate formation new Italian 
 artists , expression . sensitiveness , musical style reflect idiosyncrasies 
 y 31 . unobtrusive , hidden , accordingly race . ‘ 
 intense . exist knowledge works 
 genuinely Greek . Clear - cut , simple , issue contains thoughtful article Casella 
 thoughtful , serene , music constitutes synthesis | polytonality atonality . writer admits 
 hellenism Socrates . legitimate esthetically acceptable , 
 Recete Geom o tettees te Coctems seeviie believes future holds ‘ store 
 rl left es . . oo > Bg . polytonality atonality — 
 things , information Satie’s| 4 - 3 ctension musical tradition 
 mother English , composer ne . 
 — — = ff Erik Leslie Satie . Cocteau tells NAPOLEON OPERA 
 following story : 
 ress Debussy told Satie said : ‘ February Revue Musicale , Z. lachimeki 
 orchestra attitudinise character describes opus © Napoleon plays S , 
 ; appears stage . trees | extolled allegorical 
 N attitudinise ? needed musical back- fashion . 
 : ( March ) , ground , musical climate . . . couplets leit- , Act , entitled 277s et Valcour , ou 
 motives , atmosphere @ /a Puzis de Chavannes . ’ | Bonaparte au Caire , words music 
 vee Debussy asked Satie planning , Satie Prince Michel Cléophas Oginski . subject 
 . deci- tll - thinking setting music Maeterlinck | y sual love intrigue , Bonaparte appearing play 
 # ewntsrs . ee es ae oer eee aa ton Deus ex machina , work ending 
 vhenever author permission . ’ days later Debussy , : P 4 : . > 
 al music having procured permission Maeterlinck , started apotheosis . written 1798 - 99 , 
 writing Pel / éas et Mélisande . performed published . 
 ts modes second , Androméde , words Osinski , music 
 rumental GASPARD LE ROUX Joseph Elsen , written celebrate Napoleon 
 , , victory Poland enemies . produced 
 possible issue , André Tessier considers } \arsaw 1807 . 
 tion . music forgotten composer , ‘ rediscovered , ’ 
 -iple , ff 8 ° t © speak , J. Ecorcheville : INTERESTING MISCELLANY 
 aken far Little known Le Roux life , issue contains wealth information 
 acquainted Lorenzani ( left jon topics . Philippe Stern describes 
 dis Paris 1694 ) , mentioned certain | discusses dances Java , Cochin - China , India . 
 verting 4 documents bearing dates 1690 , 1692 , 1695 . |The article usefully illustrated sketches 
 book clavichord pieces appeared 1705 . | Maurice Garnier . G. Radiciotti contributes particulars 
 5 ( de , — és closely related Chambonniéres Rossini visit London ( 1823 - 24 ) . obituary 
 strings , Bégue , contains features belong £ T Breton . Paul Guinend , gives useful 
 > beware clavichord music 18th century oe lomes t hi » d ” , : th ‘ 
 = 4 17th . melody lyrical character , particulars career , appreciates par 
 ses free pattern , sustained . played renaissance Spanish music 

 MUSICAL TIMES — 1 1924 

 palatial cottages , decorous oceans , 
 paraphernalia operatic scenery . look 
 forward thing , compensate 
 loss actor presence , 
 distinct trend direction 

 question music cinema 
 vital cinema art 
 takes upward bound , subjects 
 presented medium momentous 
 import case , Cinema audiences , 
 airy disregard art , require 
 momentarily impressed ; fact , unlike 
 arts , accepted classical master- 
 pieces picture world thought worth 
 revival ; appear , 
 creep silently rest . use composing 
 good music share oblivion picture 
 called forth ; musical conscience 
 invulnerable , case stones 
 glass - houses , long heard 
 works César Franck Séatitudes , Mahler 
 Symphony , Beethoven Avug John Overture , 
 countless host neglected works ? , 
 millennium cinema world — afraid 
 near far millenniums ( purists , 
 pardon)—we shall content 
 present state things , cinema orchestras 
 provide particular film excerpts 
 bars half - century works , ranging 
 oratorios fox - trots , intimate 
 spiritual kindred 7yistan Isolde , Katinka , 
 Yes , , & c. , snippets 
 Parsifal ’ Pathétiqgue , proceeding 
 exquisitely appropriate sequence 

 Finally , like uttera word admiration 
 skilful use organ cinemas . 
 main function serve stop - gap till 
 orchestra arrives , economy noteworthy 
 economic musical 
 sense , Cinema organists unaware 
 organ actually organ , appearing regard 
 kind sedentary flute , evoke 
 protracted note lachrymose sentimentality . 
 refer , course , incessant use voir 
 céleste . day , , cinema organist 
 ... - optimistic 

 university lecture - room years ago| Bach technique , describing ( qualifica 

 , subject consecutive fifths , | tion ) generally organ technique . 
 appeared like flying face Providence : Scarlatti . clearly concisely treated 
 bound distrust appeal ear | consideration “ receives far 
 protested . — proportionate significance . 
 denied succession perfect fifths counter- | 2 , Classic Period , 
 point sounds objectionable musicians . - . ; 
 recollected moment | , comment . chapter = Clementi 
 musician began study composition taught | group es , notably - written . Particularly 
 hold consecutive fifths abhorrence ... . Ifthere | instructive paragraph discriminates 
 physical physiological cause | relative significance Clementi 
 antipathy ought capable shown . . . « | technique style , ‘ 
 know experience fifths sound | grchestra//y - minded Haydn Mozart . ’ Mr , 
 offensive happen ignorant | Westerby gives little needful attention 
 rule . |to Beethoven pianoforte music formal 
 Dr. Hartridge concluding pages supplement generally musical aspect . 
 Pole chapters acoustics , , , } come section Mr. 
 place securely based Helmholtz . C. , | Westerby gives rank — Romanticism . 
 - find Schumann assumes greatest prominence ; 
 History Pianoforte Music . Herbert ) , spite generous allowance musical 
 Westerby . [ Kegan Paul , 12s . 6¢. ] | quotations book , example 
 volume , glance , to| given Schumann works . — ‘ 
 said pianoforte music . | Variation form played exceedingly important 
 second glance , , shows author| evolution pianoforte music , especially 
 means achieved impossible . Mr. | technical aspect . ’ denied 
 Westerby tried combine methods of| single musical quotation . great deal 
 dealing subject , , in| room taken matter 
 completeness , require volume large as| belongs general musical history , ¢.g , , 
 . tried roll dictionary | chapter dealing formal 
 evolutionary history , touch « esthetic | technical evolution concerto . 
 discussion thrown . , clearly ! readable chapter Magyar music valuable . 
 intention , seeing successful compromise | draws attention influence ‘ s¢mdalon 
 nature welcome , book | ( kind dulcimer ) , ’ says 

 accepted light . . « . effectiveness new fechnigque , 
 shortcomings mainly faults inspired Liszt , suggested 
 organization proportion . Mr. Westerby| tempts reproduce music Magyar 

 principles method , criticising | G ” 23412312 
 suggest , shall combine the| ” ee ee 
 best . B 99 422313234 

 good old ‘ German ’ days one| old method case followed 
 right fingering scale , Frederick to| fingering principle , point divergence 
 learn lot touch Beethoven | use ‘ second ’ finger note 
 Sonata , F sharp mirfor ( harmonic | octave E9 , ? ( B ? 
 melodic ) come little later | octave ) , B?—a small point 
 feeble - minded . shall return aspect | worth discussing . , regards right hand , 
 discipline , present point notice ' old school vindicate claim provide 
 aim “ best fingering case . } best case . 
 raises question , best ? similar lines determine fingering 

 principles answer ) left hand . issue considered 
 surely simple dispute . Common-| passing thumb , opposed 
 sense dictates fingering octave,| complementary process , passing 
 disuse ‘ fifth ’ finger ( final | finger , descending scale . , 
 note right hand ascending left hand de-|C major far indeterminate . G F maior 
 scending scale ) , change position respectively offer 23123412 34123123 
 hand occur necessary twice|}and 31234123 34123123 . 
 octave . principles combined mean } mainder 

 Duparc Chanson Triste ( French ) Schubert 
 li hither ( 10 - . d.-s . ) . occasionally 
 aware sung English 

 COLUMBIA 
 Beethoven eighth Symphony lesser 

 fire ; 
 unusual asso 

 Quartet , recorded parts . hope 
 marks end old policy recording snippets 
 chamber music . set delight . 
 hesitates suggest fault performance 
 players , especially heard 
 gramophone , violin 
 shade - prominent . Mozart music 
 cure hump , remedy 4 
 surgical operation . way , listening 
 enigmatic harmonies opening , recalls 
 interest caused pained head - shakings 
 ‘ correct ’ musicians period 

 Pianoforte records William Murdoch 
 plentiful afford wasting 
 Beethoven Sonata G major — little , 
 early work , called Sonatina . record 
 useful teaching purposes , work 
 1s 
 

 feeble music , despite august origin 

 Cherniavsky Trio improves month record 

 arrangement Braga familiar Avgel ’’ 
 Scherzo Beethoven Trio 

 B flat ( . 7 ) . players 

 H.M.V 

 bulls - eye month Petrouchka , recorded 
 complete 12 - . d.-s . , played Albert 
 Hall Orchestra , conducted Eugéne Goossens . 
 Having heard music times conjunction 
 Ballet , position judge 
 pure music . attractive 
 way doubt appeal 
 uninitiated , appeal 
 complete immediate . Stravinsky score depends 
 instrumental colour like 
 failure recording disastrous . 
 Fortunately reproduction best 
 achievements H.M.V. success opens 
 question touched . 
 experience best orchestral 
 recording extremely modern music , 
 instruments allowed preserve — 
 assert — individuality . case 
 complex examples . scoring 
 classical symphonies contains good deal 
 doubling parts , believe accounts 
 fact Beethoven Symphonies generally come 
 modern orchestral works . 
 Petrouchka records triumph concerned 

 Suggia recorded 12 - . d.-s . , playing 
 Allemande Senaillé Spanish Dance 
 Popper — delightful playing , mighty poor music . 
 Renée Chemet better furnished far 
 material goes — Kreisler arrangement Dvorak 
 Songs Mother taught Achernley 
 Dream Song ( 10 - , d.-s . ) . 1 repeat monthly 
 question : swell string players going 
 play music worthy skill 

 Miss Balfour contralto , hall 
 . different . 
 section Philharmonic audience , 
 Daly Theatre Philharmonic concerts , 
 bring shame hitherto respectable institution coming 
 applause wrong place Scherzo ? 
 L.S.O. audience rose far superior refrain 
 applause end work — sensible thing , 
 course , , happened 
 London performance Ninth Symphony . 
 differences 

 ninety - years Philharmonic Society 
 played work . shines good deed naughty 
 world ! Philharmonic relations doomed 
 Beethoven perpetually honour 
 associated Society . knows tale . 
 obscure Beethoven behaviour dedicating 
 Society Symphony ( King Prussia ) 
 . Philharmonic 1824 mind , 
 impertinent magnanimous 

 doubt Weingartner performance 
 finest given London 
 years . said Weingartner — 
 Wagner dry , fondly fancies face 
 evidence world wants hear 
 compositions — conducts Beethoven 
 mistake distinguished musician . flow 
 music natural easily thought 
 having little . Symphony 
 effect night mighty scroll solemnly unwound , 
 grand effortless pageant nature , sunrise 
 . slightly modified scoring . 
 . extraordinarily reassuring 
 satisfying obvious rightness /emfi 
 . instruments proper time 
 according natures . course 
 vocal music sounded strained awkward , 
 time prepared Ninth 
 Symphony , drawback discounted 

 performance , sobriety measure , given 
 foil nights Kussevitsky . told , 
 realised , merits 
 Weingartner . time , 
 Kussevitsky extravagance inequality , performance 
 moments open sublime vistas ; generally 
 attaining , suggested wondrous new possibilities 
 thrilling imagination , Weingartner simply 
 accomplished preciseness 
 intended . Kussevitsky qualities - appreciated 
 London — way sacrifices 
 cause moment music ; ardour 
 strength ; way communicating powerful 
 energy players singers . 
 perfect sincerity doubt , Beethoven 
 feverish frantic convinced feeling 
 Symphony . believe consciously 
 ‘ stunting , ’ unconventionalities . 

 MUSICAL TIMES — 1 1924 

 454 

 enormous , fairly kept gué vive . Kussevitsky 
 want transfer Beethoven allegiance 
 Hapsburgs years ago , savage 
 oriental rule . drama music normally 
 taken tame . darker shadows , 
 light flames incendiaries . 
 temper -///egro madea wonderful beginning , midnight 
 foreboding . Beethoven support 
 Kussevitsky pitch , music clear 
 violent strain anticipated . instance , 

 Mr. Adrian C. Boult , March 29 , drew choir 
 best tone , painstaking 
 concern effect , Sea Symphony . whipped 
 singers uncommon spirit , , 
 got tang salt spray : dilute solution , 
 recognisable flavour . Slight jars groundings 
 snags shoals intonation uncomfortable 
 moments , ship sustained bravely 

 sort conducting indubitably scored 
 choral movements . time came Finale 
 choir , inspired Kussevitsky self - immolation , 
 likewise prepared die . ended wondrous , 
 barbaric clamour . mild author Ode /oy 
 uttered recommendation fraternisation . expression 
 war - dance Attila legions 

 ( spite sopranos ’ ) mighty Fina / e ! 
 surely clinching declaration Beethoven genius . 
 stupendous symphonic movements , 
 composer mind paused ? 
 orthodox /yxale properly complete edifice ? 
 Beethoven courage . 
 trusted feeling end slow movement 
 orthodox symphonic music board . 
 man distinguished cultivation , 
 spoken proper , - chosen terms , 
 faced suddenly turning - points life felt 
 words meet case 
 — burst vigorously low 
 , simply primitive heroic 
 vulgarism . Beethoven opened doors Fina / e | 
 comers , regardless manners , ended tale | 
 symphonies extraordinary bang clatt « | 
 circus - music 

 ROYAL CHORAL SOCIETY 

 Barclay Bank Musical Society gave concert Queen 
 Hall April 9 . male - voice choir , Mr. 
 Herbert W. Pierce , sang Stanford Songs Fleet 
 Bantock Lucifer Starlight , orchestra played 
 German lWelsh Rhapsody works Mr. 
 Herbert J. Rouse . went excellently 

 concert given Philharmonic Choir Queen 
 Hall March repeated Central Hall April 5 
 audience children secondary schools . 
 stiff programme young-—Alvest Pair Sirens , 
 Delius C minor Pianoforte Concerto , Holst Wynn Jesus , 
 Franck /syche , Beethoven Pianoforte Choir 
 Fantasia 

 annual concert Wandsworth Male - Voice Choir 
 Battersea Town Hall , March 27 , dress 
 rehearsal Choir success London Festival 
 days later . excellent programme Elgar / 
 jind thee Bantock .1 / y /ove zs like red , red rose 
 pieces prepared competition . Mr. J. C. Clarke 
 conducted 

 F particular lustrous . Miss Stevens diction 
 laboured . knack glib utterance 
 save trouble pleasanter hear 

 compliment Mr. Howard Fry , sang 
 March 31 , tell moments reminded 
 delightful American baritone J. C. Thomas . Accom- 
 panied Mr. Reginald Paul , sang sacred songs 
 |of Beethoven Op . 48 , Arias Berlioz Verdi , 
 songs Vaughan Williams Stanford . Mr. Fry 
 true lyric baritone quality . voice warm 
 diction good ( lighten short words , ‘ , ’ ‘ " 
 soon ) . I[t interesting develop 
 power , flashing lingual brilliance , pealing 
 high notes eminent singers type 

 Shepherd ! thy demeanour vary’—so runs line 
 old song Thomas Browne , figured 
 Miss Rosa Alba programme . advice 
 taken heart singer . Miss Alba began 
 prepossessing . time lost 
 hold stereotyped style . sang , 
 bright soprano voice considerable range , 
 manage turn subtle use , reflect 
 marked personality . reproached 
 | Slurring , told assistance reaching high 
 notes given craning ofsthe neck 

 BRIDGWATER.—The Choral Society assisted 
 Mr. John Coates spring concert April 10 , Mr. 
 Arthur Trowbridge conducted , choir sang Callcott 
 Love wakes weeps , Rutland Boughton /%fer Song , 
 Parry Vy delight thy delight , Geotirey Shaw Worship , 
 Holst Song Blacksmith , Fletcher Follow 
 Carlow , madrigals Byrd , Gibbons , 
 Dowland 

 BrRisToL.—On March 15 Avawatha performed 
 Bristol South Choral Society , choir 
 orchestra , Mr. R. T. Young . 
 recital works Arnold Bax given Victoria 
 Rooms March Mr. John Coates , Miss Olive 
 Franks , Miss Bessie Rawlins ( violin ) , Miss Harriet 
 Cohen ( pianoforte ) . Mr. Bax present , played 
 accompaniments . programme included second 
 Violin Sonata.——At Symphony concert March 19 
 Madame Suggia played Dvorak Concerto B minor 
 ‘ cello orchestra . orchestra played Ravel .J / 
 Goose Suite Brahms fourth Symphony , Mr. Eugéne 
 Goossens . — — Stapleton Hill Choral Society performed 
 Elijah March 31 , conducted Mr. S. A. Harris . — — 
 Horfield Choral Society gave Judas Maccabeus April 2 . 
 — — Mr. Herbert Hunt Mr. Ralph T. Morgan 
 given programme Children Concert Society . 
 played Air Variations Rheinberger , 
 Bach Aria ( violin organ ) , Bach Organ Prelude 
 Fugue C minor , Elgar Sonata G , added 
 explanatory remarks.——At concluding concert 
 Philharmonic Society , April 5 , Elgar Zhe / usic - Makers 
 Sea Pictures , Vaughan Williams Mass G minor , 
 Parry Jerusalem , Beethoven Violin Concerto 
 performed . Miss Margaret Fairless violinist , Miss 

 Phyllis Lett vocalist , Mr. Arnold Barter conducted 

 CAMBORNE.—Beethoven fourth Symphony 
 Bach Concerto D minor violins 
 programme Cornwall Symphony Orchestra gave 
 Camborne March 30 31 , Truro . 
 Dr. C. Rivers conducted 

 CAMBRIDGE.—The chief events unusually active 
 term performances B minor Mass 
 C.U.M.S.—in Guildhall , Mr. J. F. 
 Shepherdson , Ely Cathedral following day , 
 Dr. Cyril Rootham.——The C.U.M.S. gave 
 new series popular orchestral concerts , chamber 
 concerts ( Spencer Dyke String Quartet , 
 second including Schubert Octet Mozart Clarinet 
 Quintet ) given xgis 
 Society.——College musical societies cases hold 
 weekly meetings Sunday evenings ; 
 informal concerts mentioned given 
 Newnham College Musical Society King College Hall 
 March 9,.——Dr . Wood Passion according St. Mark 
 sung King College Chapel _ Friday , 
 March 7 , Brahms Song Destiny , vocal 
 instrumental music , performed Christ College 
 Chapel successive Sunday evenings . — — Informal 
 Musical Society continues flourish , given 
 good chamber concerts term.——Dr . Gray , 
 Trinity , Mr. Philip Dove , Queens ’ , given 
 fortnightly organ recitals —— March 6 , Dr. Rootham 
 conducted performance Achilles Scyros 
 Guildhall 

 CaRDIFF.—In series historical performances 
 Beethoven Symphonies , organized Mr. Mrs. 
 Herbert Ware , Zro%ca played March 26 , 
 orchestra . Mr. W. H. Reed gave lecture 
 Symphony , Mrs. Ware played Elgar ‘ Cello 
 Concerto 

 CoLcHEsTER.—Mr . W. F. Kingdon conducted 
 Musical Society - chosen programme April 3 , 
 works sung choir Avest Pair Sirens , Brahms 
 Song Destiny , Cowen John Gilpin 

 DEAL WALMER.—A series monthly symphony 
 concerts inaugurated year Lieut . Walton 
 O’Donnell orchestra Royal Marines . 
 January programme included Tchaikovsky fifth Symphony 
 Smetana V / tava ; February , New World 
 Symphony Zes Pré / udes . March 29 largest 
 audience season heard /ufiter Symphony 
 Tchaikovsky Romeo Juliet 

 EpINBURGH.—On March 15 , Paterson orchestral 
 concert children , exposition musical forms 
 given Mr. Herbert Wiseman , conducted 
 orchestra Bach Air ( counterpoint ) , Jarnefeldt Prelude 
 ( canon ) , Zhe Magic Flute Overture ( fugal writing ) , 
 Mozart Pianoforte Concerto D , Mr. Philip 
 Halstead soloist.——The North British Railway Musical 
 Association , conducted Mr. Archibald Russell , numbered 
 voices concert March 15 . 
 programme included Bach O Sacred Head Wilbye 
 Flora gave fairest flowers.——In pianoforte recital 
 March 15 , Mr. Harold Samuel included music Bach , 
 Handel , Mozart , Beethoven , Schumann , Debussy . — — 
 Reid orchestral concert season closed March 15 , 
 Brahms Pianoforte Concertos played . 
 Mr. Johannes Roentgen soloist D minor 
 Prof. Tovey B flat Concerto , 
 Dohnanyi Variations Theme Childhood . Holst 
 Fugal Overture played.——On March 10 , 
 Prof. Tovey soloist Dr. Arthur Somervell 
 Highland Concerto . composer conducted 
 Normandy Variations . interval thirteen 
 years , Zhe Dream Gerontius performed 

 458 MUSICAL TIMES — 1 1924 

 Royal Choral Union March 17 , Usher Hall . LEEDS . — outstanding event season 
 Mr. Greenhouse Allt conducted , soloists | performance Mass G minor Dr. Vaughan 
 Miss Muriel Brunskill , Mr. Arthur Jordan , Mr. Philip | Williams Philharmonic Choir direction 
 Malcolm . Fellowes Orchestra collaborated , | Mr. Norman Strafford . singing fine quality , 
 Mr. Ralph T. Langdon organ.——On March 17,|and Mass deep impression . Choir 
 fifth — — Prof. Tovey University Historical | gave Stanford 7he Blue Bird Dr. Whittaker 
 Concerts included Brahms Pianoforte ( Quartet C minor , | northern folk - song arrangements.——Recitals 
 Beethoven String ‘ ) uartet E flat , Mozart Pianoforte given Mr. Frederick Dawson Miss Jean Sterling 
 Quartet G minor , given Prof. Tovey Edinburgh | Mackinlay 

 String Quartet.——In Music Hall , March I8 , } vs 
 Scottish Girls ’ Friendly Society Choir Girl Guides ’ LiveRPOOL.—Sir Edward Elgar conductor 

 Choir , trained Mr. Alexander Maitland , combined to|the Philharmonic concert March 11 , , 
 sing Bach cantata , Ze Angels , severally sang part- | recently - scored Handel Overture D minor 

 songs . — — Leith Amateurs ’ Orchestral Society , | orchestrally - accompanied Sea Pictures , Symphony 
 annual concert war , played Beethoven |in E flat played , programme closed Zix 

 Symphony , Mr. W. Gilchrist Cochrane conducting . — — | Ward Youth Suite . Miss Olga Haley singer , 
 date , Synod Hall , Mr. Moonie Choir closed | — — -The present series Vickers concerts closed 
 season programme - songs , including MacCunn | March 15 appearance De Reszke Singers , 
 garden , Festa flowery vale , | sang folk - songs , Elgar - songs , negro spirituals , 
 Stevens Oberon Fairyland . Mr. Horace Fellowes | nursery - rhyme parodies Herbert Hughes . Miss Don 
 played Edgar Barratt / ridean Legend . — — St. Andrew } Labbette sang . Mr. Zacharewitsch 
 Amateur Orchestra ! Society , fortieth season,| Miss Ethel Midgley gave violin _ pianoforte 
 conducted Mr. R. de la Haye , played Oderon | recital Crane Hall March 19.——The British Music 
 Overture , Overture 7%e Mastersingers , Beethoven | Society gave recital March 22 works Mr. Herbert 
 Symphony . Orchestra numbered players . | Howells . String Quartet , Lady Audrey Suite , 
 — — March 20 , Mr. Gavin Godfrey choir , latest | Pianoforte Quartet ( Carnegie publications ) , 
 addition local organizations , gave concert . | carol anthems , pianoforte miniature suite , Sarum 
 choir numbered voices , sang | Skefches , performed . McCullagh ( Quartet pro 

 Hiawatha ’ s Wedding- Feast - songs . Asmallorchestra| vided strings.——7Zhe Banner St. George 
 assisted , Mr. Arthur Jordan principal singer . | Rogers 7he Storm sung Post Office Choral 
 Society March 22 , conducted Mr. Matthews Williams 

 HAirax.—Bach Jesu , Price Treasure sung | experienced Wagnerians hall 

 St. Paul Choral Society , Mr. T. Newboult , on/ eighteenth concert , February 28 , Hofmann played 
 April 10 , St. Paul Church , | G major Beethoven ; time heard 

 Bax Garden Fand ; Richter old war- 
 | horses — Dvorak Symphonic Variations — heard 
 | fourteen years ’ silence.——March © brought Bach 
 . : ~- |B minor Mass ( epidemic coughing affecting platform 
 Redfern conducted , solo parts taken Miss | audience alike ) . Sung successive season , 
 Mary Foster , Mr. John Adams , Mr , Ilarold Williams . | j¢ failed reach high standard 1923 . soloists 
 concert opened Bach - Elgar Fugue , Mesdames Caroline Hatchard Muriel Brunskill , 
 included / Xhapsody Brahms . Messrs. Arthur Winter Horace Stevens.——The 
 twentieth concert , March 13 , notable fact 

 Philharmonic Society April 3 chosen plebiscite . Rat . ss 
 " ape ; - ts & hee . - gy , | win enthusiasti raise exacting critics . 
 included New /Vor / d Symphony , J / agic Flute - ent allt . 8 

 Meistersinger Overtures , Casse - Notsette Suite . Beethoven Pastoral Symphony , Strauss Don Juan , 
 Sie Weawe Weed conducted . | Brahms Gaudeamu Igitur ( Academi Festival ) 
 Overture — nature good ‘ breaking - 
 KIDDERMINSTER.—The Choral Society gave | finale — provideda programme making wide appeal . 
 concert season April II , programme Pension Fund concert March 20 consisted 7he / astfer 

 included Holst 7zvo Psa / ms Dvorak Ze Deum . Mr. | Sse ? Overture , Strauss / denleben , Saint - Saéns 
 J. Irving Glover conducted . Carneval des Animaux , Choral Symphony 
 ; Beethoven.——If fact 

 KiRKCALDY.—On March 19 Musical Society|additions remarkably small number choral 
 performed Brahms Xegwiem Adam Smith Hall . | concerts great choir - singing neighbourhood mention 
 Mr. Charles M. Cowe conducted choir orchestra,|should Brand Lane //iawatha 
 principal singers Miss Hilda Blake and|trilogy March 15 Sir Henry Wood , 
 Mr. Herbert Heyner . Stanford Zhe A’evenge also|the Manchester Vocal Society performance Bach 
 performed . Sine ye Lord , March 19 , Mr. H. M 

 459 

 Dawber.——During March B.N.O.C. given 
 extensive répertoire Opera House . Manchester , 
 unlike places , strong liking Verdi 
 Otello Falstaff ; performance . 
 house probably 
 performances given , instead 
 operas form stock - - trade companies 
 incapable Ofe//o . Probably experience 
 lost management future visits . 
 domain chamber music recorded 
 Tuesday noon - tide series ‘ ’ witha 
 score ; Catterall ( Quartet , 
 March I2 , gave believe ‘ one- 
 composer ’ chamber concert — Brahms evening 
 Quartet ( Op . 51 ) , early Sextet ( Op . 18 ) , mature 
 ( Quintet ( Op . 88 ) . time , , 
 authoritatively annotated programme pen 
 Mr. Samuel Langford . fourth Hamilton Harty 
 chamber concert Elgar minor Quintet played bythe 
 Edith Robinson Quartet Mr. Harty . players 
 past seasons advanced position 
 definite prominence . Mr. Harty 
 conception chamber music extends suitable 
 chamber orchestra , March 17 proved 
 justness theories superior adaptability 
 chamber concert - hall things 
 Grieg Ho / berg Suite ; Mozart D minor Divertimento 
 strings horns , Ziegiac Serenade 
 Tchaikovsky . audience discovered 
 Lesser Free Trade Hall ideal chamber 
 music city . April seventh 
 eighth recitals Beethoven Pianoforte Sonatas 
 continued Mr. Robert Gregory Mr. Isidor Cohn 

 OsseTT.—German A’ichard 7/7 . Overture opened 
 concert Orchestral Society March Mr. 
 Alfred Hemingway conducted 

 _ TorQquay.—The Winter Orchestra played Max Bruch 
 Violin Concerto G minor , Mr. Albert Sammons , 
 April 9 , Rimsky - Korsakow Cafriccio Espagnole . Mr. 
 Ernest Goss conducted 

 WELLINGBOROUGH . — Orchestral Society pro- 
 gramme April included Beethoven Prometheu 
 Overture , Grieg Pianoforte Concerto Miss 

 Constance Julyan soloist , Eric Coates Miniature Suite , 
 Purcell Déoclestam Overture , movement 
 Franck Pianoforte Quintet . conductor Rev. 
 Greville Cooke 

 NEW YORK 

 law appearing conductor , | decades ago . real vocal histrionic success 
 singers gave concert year Beethoven | jn opera Michael Bohnen , Kasper , 
 ninth Symphony , assistance Philadelphia|the wolf glen veritable ‘ Hell Kitchen . ’ 
 Orchestra baton Leopold Stokowski . introduction Weber /nvitation Dance gave 

 avery remarkable performance , Mr. Stokowski reading | Ballet exceptional opportunities . M. H. FLINT . 
 martial intensity vigour . | 
 interfere enjoyment movements , ‘ 
 lovely ddagio taken fast ( martial TORONTO 

 spirit prevailing ) poetry Mendelssohn Choir Toronto recently established 

 beautiful Beethoven Adagios lost . The| firmly eyes America . New 

 triumph , course , movement , the| York critics claim body priority 

 programme devoted cap / ella possible huge choir attend rehearsals week 

 music ( Palestrina , Byrd , Bach selections ) , Dr. Fricker | winter , sing night 
 conducting . second concert given entirely | weeks aggregate , simply love 
 Dr. Fricker baton , Philadelphia Orchestra furnishing music . New York choirs paid . tour 
 accompaniments needed . feature | year included Buffalo , New York ( nights ) , 
 second evening excerpts Hach B minor Mass. | Philadelphia , Baltimore . Mr. Stokowski created deep 
 Chis wonderful choir sings perfection attack , | impression performance Beethoven Choral 
 dignity sincerity solemn music , | Symphony Carnegie Hall evening , 
 strength power called , | course orchestra . following evening 
 criticise praise given . | Dr. Fricker given enthusiastic reception 

 wo 

 mixed programme contained excerpts 
 Bach B minor Mass. Buffalo Baltimore heard 
 cappella programmes . Mr. Tertius Noble opinion 
 worth recording . ‘ thousand congratulations 
 splendid triumph . enjoyed gorgeous work 
 choir — moment . Whatcontrol ! . . . 
 thousand thanks inspiring choral singing . 
 good let New Yorkers man 
 knows job head great choral body 

 New Symphony ‘ Twilights ’ given 
 Brahms Symphony . 3 , Afagic Flute William 
 Tell Overtures , ‘ 1812 ’ Beethoven /astoral 
 Symphony . soloists Moses Garten 
 Beethoven F major Romanza , Alfred Heather 
 ‘ Lohengrin Farewell , ’ Leo Smith Elgar Vio- 
 loncello Concerto . Kreisler , welcome , strangely 
 chose Grieg C minor Sonata Tchaikovsky 
 D major Concerto programme . usual 
 house packed , popular Viennese tit - bits 
 great demand 

 Hambourg Concert Society gave splendid evening 
 chamber music Massey Hall , Trio ( Messrs. 
 Geza de Kresz , Reginald Stewart , Boris Hambourg ) 
 played Beethoven E flat , Op . 70,No . 2 . Geza de Kresz 
 Norah Drewett heard favourite César Franck 
 Sonata 

 New York Symphony Orchestra chose - Wagner 
 programme return visit . American orchestras ( 
 exception Philadelphia ) losing ground 
 regular concerts . Galli - Curci , Ignaz 
 Friedman , Ernest Seitz heard recital . 
 Sophie Braslau completely captured audience 
 second time season . Eaton Choral Society ( Percy 
 Fletcher ) , Murray Kay Choral Society ( W. Crawford ) , 
 Masonic Male - Voice Choir ( E. R. Bowles ) 
 held yearly concerts . Florence MacBeth , Chicago 
 Opera , assisted - named Society 

