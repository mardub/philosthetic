OUTLINE PERFORMANCES 

 TugespAy Morninc : ELIJAH . Tuespay Eveninc : Brahms 
 SONG DESTINY ; Mr. Edward German NEW ORCHES- 
 TRAL WORK ( composed expressly Festival ) ; Beethoven 
 C MINOR SYMPHONY , . 5 ; Wagner MEISTERSINGER 
 OVERTURE ; ; Scene 3 , Act III . , DIE WALKURE ; Schumann 
 MANFRED OVERTURE 

 WEDNESDAY MorninG : Professor Stanford new REQUIEM 
 MASS ( time performance ) ; Bach Cantata O LIGHT 
 EVERLASTING ; Brahms SYMPHONY , . 1 . WEDNESDAY 
 EveENING : Purcell ’ KING ARTHUR MUSIC ( specially edited 
 Mr. J. A. Fuller Maitland Festival ) ; Cherubinits MEDEA 
 OVERTURE ; Beethoven LEONORA OVERTURE , . 3 

 TuHurRsDAyY Morninc : MESSIAH . Tuurspay Eveninc : Gluck 
 IPHIGENIA AULIS OVERTURE ; ; Arthur Somervell new 
 Cantata , ODE SEA ( composed expressly Festival ) ; 
 Wagner SIEGFRIEDIDYLL ; Mozart’sG MINOR SYMPHONY ; 
 Dvorak CARNIVAL OVERTURE 

 Sept. 15 . — ' ‘ Stronghold Sure ” ( Bach ) , Magnificat ( Hubert Parry ) , 
 Selection “ Parsifal ” ( Wagner ) . ‘ Judgment ” ( Spohr ) . 
 Evening : “ Elijah 

 Sept. 16.—Mass D ( Beethoven ) , Symphony , B minor ( Tschai- 
 kowsky ) , ‘ Creation ’ ( I. ) . Evening : ‘ ‘ Redemption 

 Sept. 17 . — ‘ Messiah . ” Evening : Chamber Concert Shirehall 

 complain autumnal weeks 
 work . Cherishing great respect 

 listen 
 wonder . Town - born , mayhap , , 
 cling breast mother . 
 Doubtless happy way , 
 , appointed season , instinct 
 opportunity hurry children fields 
 hedge - rows native haunts . 
 music , sonatas 
 Beethoven nocturnes Chopin come 
 open windows rose- 
 clad cottages , bespeaking presence 
 migrant distant city . country 
 music , form 
 Othello willing tolerate 

 music heard . ” know 

 MUSICAL TIMES.—Avceust 1 , 1897 

 doubt plumes polyphony , 
 bound admire masterfulness ; composer , like 
 Keats owl , ‘ ‘ feathers - cold . ” 
 music impress , sorry 
 long delayed final bar reached . second 
 performance cause alter opinion , 
 present consider Finale interesting 
 symphony . composer conducted , , 
 capital performance , warmly applauded 
 audience . Mr. Alexander Siloti played Beethoven E flat 
 Pianoforte Concerto fine technique hardly suf- 
 ficient expression ; Mdlle . Camilla Landi , admirable artist 
 , complete justice air ‘ ‘ Printemps 
 qui commence , ” Saint - Saéns ‘ ‘ Samson et Delila , ” 
 airs Handel ; pompous pointed per- 
 formance “ Meistersinger ” ’ overture , conducted 
 Sir Alexander Mackenzie , completed programme 

 LEEDS FESTIVAL CHOIR LONDON 

 pianoforte recital given Queen ( Small ) 
 Hall Miss Edie Barnett , pupil Miss Fanny Davies , 
 Friday afternoon , 2nd ult . juvenile executant 
 stated years age , 
 efforts scarcely amenable criticism . said , 
 , dawning ability mean order , 
 movement Bach Clavier Concerto 
 D minor Mendelssohn Capriccio Brillant B 
 minor ( Op . 22 ) Miss Barnett aid Miss Davies , 
 played second pianoforte arrangement 
 orchestral accompaniments . solos included 
 pieces Schumann , Sterndale Bennett , Stephen 
 Heller 

 Miss Gertrude Palmer , gave pianoforte recital 
 Queen ( Small ) Hall Saturday afternoon , 
 3rd ult . , comes Sydney , New South Wales . “ Advance , 
 Australia , ’ motto young 
 aspiring musicians hailing Antipodes , 
 reason Miss Gertrude Palmer 
 eventually leading place profession , 
 unquestionable ability . programme included 
 number minor pieces Schiitt , Chopin , Heller , Liszt , 
 Brahms , Beethoven Sonata D ( Op . , 
 . 3 ) Chopin Rondo C pianofortes ( Op . 
 73 ) , assisted Mr. Algernon Ashton 

 Miss Edith A. Greenhill responsible pianoforte 
 recital Salle Erard Tuesday evening , 6th ult . | 
 pianoforte recital evening somewhat a/ 
 novelty , Miss Greenhill secured large fashionable 
 audience . Commencing preludes Bach , Handel , 
 Mendelssohn , passed Brahms fine Variations 
 ona theme Schumann ( Op . g ) , 
 justice . Beethoven Sonata D minor ( Op . 31 , . 2 ) 
 played , lovely Adagio B flat especially 
 interpreted taste demands . Pieces Field , 
 Chopin , Liszt , Cramer , Steibelt , Henselt , Rubinstein 
 brought performance conclusion 

 ROYAL COLLEGE MUSIC 

 Co. , festival committee chosen 

 thirty tunes chants , anthems , 
 “ OQ love Lord ” ( Sullivan ) ‘ , soul , 
 land brightness ’ ? ( J. H. Roberts ) , 
 choruses , ‘ ‘ Worthy Lamb ” ‘ * Hallelujah 
 Father ’ ( Beethoven ) , rendered meetings . 
 singing afternoon evening gatherings 
 reached , , high level excellence , 
 remarkable choristers unaccustomed 
 sing orchestral accompaniment , previous united 
 rehearsal held . soprano voices 
 fine , tenor bass good , altos weak , 
 rendering hymn - tunes effective . 
 , , lack electrical religious 
 fervour distinguishes Welsh congregational 
 singing , light shade singing 
 minor lighter tunes . great choir 
 enjoy chant , unsatisfactory 
 printing words . effect ‘ Worthy 
 Lamb ” , , impressive , precision 
 attack , quality tone , heartiness singing 
 excellent . performance Beethoven ‘ Hallelujah ” 
 praiseworthy ; blemishes 
 detected trained ear comparatively 
 trifling . Mr. J. H. Roberts conducted anthem . 
 meetings Miss Kate Morgan , Dowlais 
 ( winner gold medal Chicago ) , sang . 
 success festival encourage Welsh musicians 
 persevere efforts encourage orchestral 
 music , step organisation 
 united festival — , Carnarvon Castle — ‘ 
 Messiah ” ’ performed choirs gathered 
 parts North Wales 

 MORS ET VITA ” ITALY 

 interesting papers read day 
 training choirboys , Mr. Walter 
 Henry Hall , organist choirmaster St. James 
 Church , New York , illustrate principles tone 
 production Mr. Hall platform dozen 
 choirboys , sang exercises anthems 
 manner toelicit hearty applause large 
 audience gathered hear latest ideas subject 
 tone production . Mr. Hall enthusiastic believer 
 method training develops “ thin ” ’ 
 ‘ head ” distinguished “ thick ” ‘ ‘ chest ” 
 register , plea attention given 
 important branch choirmaster duties . 
 speaker remarked , deprecated , temptation 
 conscientious organist “ flesh - pots ” 
 ear - tickling , sentimental novelties congregation 
 instead ‘ ‘ manna ” true standard works , 
 reason emotional nature 
 appealed music Church . 
 blemishes sensational mechanical organ playing 
 commented , speaker concluded 
 pointing great opportunities responsibilities 
 choirmasters , exhorted adoption 
 highest best standards , , 
 example influence , influence generations 
 come 

 ‘ grand orchestral concert ” evening attrac- 
 tion , special mention Mr. Harry Rowe 
 Shelley new Symphony E flat , instru- 
 mental compositions magnitude . work 
 original ideas , influence master , 
 Dvorak , Beethoven , little Wagner 
 traced 

 Saturday morning given Virgil Piano 
 recital , pianoforte recital Mr. William H. Sherwood , 
 song recital Mrs. Gerrit Smith , conference 
 ‘ * Musical Journalism , ” chairmanship Mr 

 unusually successful ; union existing | . lady , taking rest Carlsbad , 
 Owens College Royal Manchester College | signed engagement Opéra Comique 
 Music tending advantage Institutions , | months . ‘ Louise , ” new work M. Charpentier , 
 enabling Dr. Hiles , controls composition depart- | accepted performance M. Carvalho . Owing 
 ment , carry larger comprehensive | delicate health M. Daubé subject 
 scheme possible , ensuring the| time past , management appointed 
 attainment high executive skill acquaintance | assistant - conductor person M. Alexandre Luigini , 
 practical modern art University graduates , | hitherto conductor theatre Lyons , able 
 thorough versing inthe grammar construction music | musician . engagement entered 
 students College Ducie Street . , house Mdlle . de Lafond 

 Saturday , 3rd ult . , Vice - Chancellor conferred ; musical world capital having greatly 
 degree Mus . B. students only|put news discontinuance 
 completed years ’ course , | Lamoureux Concerts , erroneous reports having 
 satisfied examiners ‘ “ Exercises’’- submitted . | spread subject persons pretending 
 finished studies Owens | - informed , state actual facts , 
 College classes , send motets | : place , M. Lamoureux 
 originative talent tested ; andj know turn matters . 
 understood examinations junior under- | hope find , time great Exhibition 
 graduates second years marked | year 1900 held , head theatre 
 advance , exciting bright hopes gradually increasing | ‘ ‘ Der Ring des Nibelungen ” Wagner 
 attainment . College Music Principal , Mr. | works produced . necessary 
 Brodsky , reason gratulation . evenings | greater liberty time come , order 
 devoted public demonstration success | needful preparations undertaking , 
 work year . annual festival week | pay prolonged visits Germany engagement 
 College excites wide eager attention , | interpreters highest order , & c. equally im- 
 year orchestral rendering ( direction | portant famous orchestra , 
 Principal ) Brahms Symphony ( . 2 ) | , , desires series concerts 
 “ Leonora ” Overture ( . 3 ) , performance | periodically London Berlin . doubt way 
 Act Mozart ‘ “ Il Flauto magico ” operatic class , | found reconcile conflicting personal interests , 
 afforded convincing evidence general excellence | knows matters arranged . 
 talent discipline Institution . succeeding | ‘ Les Erynnies , ” Leconte de Lisle , Massenet 
 evenings programmes devoted mis- | music , produced Théatre d'Orange , 
 cellaneous display acquirements smaller groups | followed ‘ ‘ Antigone , ” music Saint - Saéns . 
 executants . Specially noticed ensemble play- | - named composer engaged , M. Louis Gallet 
 ing members Mr. Carl Fuchs ’ classes selections | librettist , elaboration work retracing history 
 chamber work Brahms , Schumann , Beethoven , | nineteenth century , view performance 
 Dvorak , ; pianoforte solo performances | coming World Exhibition 

 advanced pupils Miss Olga Néruda| Grand Prix de Rome awarded 
 Mr. Dayas ; finished playing | M. d'Olonne , pupil MM . Massenet Lenepveu , 
 young people enjoy advantage | second prize gone M. Crose Spinelli , 
 direct care Mr. Brodsky . work choir|is called deuxitéme second Grand Prix M. 
 light , satisfactory ; stress , | Schmitt 

 contests . eighteenth century music 

 morning , ‘ ‘ Stronghold Sure ’ ? ( Bach ) , Magnificat ( Dr. 
 Hubert Parry ) , selection ‘ ‘ Parsifal ” ’ ( Wagner ) , 
 ‘ ‘ Judgment ” ( Spohr ) ; evening devoted 
 Mendelssohn “ Elijah . ” Thursday morning , Mass 
 D ( Beethoven ) , Symphony B minor ( Tschaikowsky ) , 
 ‘ Creation ’ ( I. ) ; Gounod ‘ ‘ Redemption ” 
 occupy evening . ‘ ‘ Messiah ” given 
 Friday morning , festival conclude 
 evening chamber concert Shire Hall . 
 principal vocalists engaged Madame Albani , Miss 
 Anna Williams , Madame Medora Henson , Miss Hilda 
 Wilson , Miss Marie Brema , Miss Jessie King , Miss Marian 

 Watkin Mills , Mr. 
 Price 

 organ performances Town Hall £ 1,000 , 
 M. Auguste Wiegand , city organist , receives salary 
 £ 500 , annual profit £ 500 

 Fritz Steinbach , proposes series concerts 
 |shortly , Brahms symphonies works 
 | Bach , Beethoven , Schubert , Wagner programme . 
 | Professors Joachim Hausmann , Herr Felix Kraus , 
 Mr. Eugene d’Albert 
 | soloists . important new choral work Nor- 
 wegian composer , W. Stenhammar , entitled ‘ “ Snofrid , ” ’ 
 | produced Philharmonic Choir coming 
 | season.——An interesting valuable series ‘ * Lives 
 | Great Composers ” ’ issued newly 
 | founded Berlin publishing company ‘ Harmonie . ” Pro- 
 ' fessor H. Reimann , eminent organist , 
 |editor , contributors series 
 |Dr . Jadassohn , Dr. Bluthaupt , Professors Gernsheim 
 land L. Auer , Herren Otto Lessmann , , Niggli , 
 Wittmann , Capellmeister Volbach , ‘ “ ‘ La Mara , ” 
 numerous distinguished writers 

 Bources.—A monument unveiled June 20 , 
 erected composer Louis Lacombe , ceremony 
 including performance prelude march 
 deceased opera ‘ ‘ Winkelried , ” brought 
 years Geneva 

 Polish enthusiasts lately caused memorial 
 erected commemorating fact , bearing bronze 
 medallion - portrait composer 

 BRussELS.—Musical performances Hall 
 International Exhibition , acoustic defects 
 extent ameliorated , inaugurated officially , 
 15th ult . , grand concert , direction 
 M. Ysaye , included Schumann Symphony C , 
 overtures ‘ ‘ Freischiitz ’’ “ Tannhauser , ” much- 
 appreciated rendering , MM . Ysaye 
 Thomson , Bach Concerto D violins.——A 
 highly - appreciated performance given 
 unique orchestra clarinettists , thirty - seven number , 
 conductorship M. Poncelet , - known 
 professor Royal Conservatoire . Mozart G minor 
 Symphony , Adagio Beethoven ‘ ‘ Sonata Pathé- 
 tique ’ ? Weber ‘ ‘ Moto continuo , ” Rhapsody 
 Liszt effectively rendered curious ensemble 
 instrumentalists , performance attracted 
 numerous audience.——On 22nd ult . perform- 
 ance announced place M. Tinel drame 
 musical , ‘ * St. Godelive , ” ’ 300 executants , 
 direction composer 

 Caracas.—The capital Venezuela , 
 possesses Opera - house Philharmonic Society , 
 chiefly supported German residents , establish 
 Conservatoire Music aided Govern- | 
 ment grant 

 CARLSRUHE.—The season opera Hof - Theater | 
 came close June 13 performance , 
 enhanced prices , entire ‘ “ Nibelungen ” tetralogy . | 
 novelties coming season a| 
 comic opera somewhat awkward title “ Das | 
 Unmodglichste von Allem , ” Herr Anton Urspruch , | 
 brought Frankfort | 
 Weimar.—A number special performances opera 
 announced place , Herr Mottl direction , 
 September 5 Cctober 3 , works 
 produced Berlioz ‘ “ ‘ Les Troyens ” ’ ( parts ) , 
 Liszt ‘ St. Elizabeth ” ( scenic representation ) , Wagner | 
 “ Tannhauser , ” ‘ Lohengrin , ” ‘ Tristan , ” ‘ Die 
 Meistersinger , ’’ Beethoven ‘ Fidelio 

 CosurG.—A new - act opera , “ * Die Grille ” ( ‘ ‘ 
 Cricket ’’ ) , Herr Johannes Dobber , Capellmeister 
 Court Theatre , libretto founded story 
 Georges Sand , shortly brought 
 Leipzig 

 BACH . — “ Passion ” ( St. Matthew ) .. ee 

 BEETHOVEN . — “ Mount Olives ” ( en enki . ) 
 ( Tonic Sol- fa 

 BENNETT . — “ onal ( Paper boards , ts . 6d . ) 
 ELGAR . — “ King Olaf ” .. ‘ ( Tonic Sol - fa ) 
 GLUCK . — “ Orpheus ” .. . 4 ( Tonic Sol - fa ) 
 GRAUN , C. H. — “ Peden ’ al te Tod Jesu ” ) 
 HANDEL . — Messiah ” “ ( Paper boards , ts . “ - ) 
 — “ Solomon ” .. “ aa 

