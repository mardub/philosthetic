ConcErRT.—May 4 , 8.30 

 Overture—"Coriolan " .. ee ws ua ee .- Beethoven 

 symphony , . 6 , b minor , " Pathétique " Tschaikowsky 

 ir wonderful talent people display 
 elaborately futile investigation — count 
 time letter occur Shakespeare , 
 ascertain copy Times 

 paper entire earth . 
 laborious trifler recently discover m isa 
 important letter music , man 
 composer ’ begin — e.g. , Mozart , Méhul , 
 Meyerbeer , Mendelssohn , Marschner , Millocker , 
 Mascagni , Massenet , Mercadante , Molique , & c. M 
 undoubtedly good , b 
 impressive record . refer 
 Bach , Beethoven , Brahms , Byrd , Buononcini , 
 Bilow , Braham , Borodin , Bellini , Berlioz , Bizet , 
 Balfe , Boito . aspirant fame , 
 begin letter , 
 gratify knowledge success 
 way reduce disproportionate 
 prestige attach b m , 
 letter x , example , entirely devoid 
 celebrity present matter music , 
 stretch point favour Xylophone 

 Dr. Aldrich write catch tobacco . 
 hear tobacco catch . distin . 
 guished british composer , start railway 
 journey , wish avoid fume certain 
 weed , enter carriage word 
 " smoking " appear . occupant 
 lady , british composer 
 consider safe . journey 
 partake luncheon . end repast , 
 lady traveller turn dis- 
 tinguished british composer ask : " 
 object smoking ? " reply 
 , forthwith proceed light 
 cigarette . comment 
 species puffery practise fair 
 sex , assume precaution 
 composer - traveller end smoke 

 Vanderbilts — Jessie Vanderbilt 
 McNamee ilk — write compose 
 song . descendant Old Commodore , 
 famous millionaire having , fact , 
 great - uncle . advent Vanderbilt 
 music verse open new field 
 solace wealth weigh 

 AmeERIcAN critic scarcely expect pay 
 indiscriminate homage venerable illustrious 
 . free independent , especially 
 , write follow : ' * Beethoven 
 Sonata , Op . 110 , mean 
 popular great composer work , 
 reason . opening movement 
 devoid interest general listener . 
 cantabile molto espressivo express 
 particular . suggest exercise . 
 awful thought Jupiter nod come 
 mind reflect Beethoven 
 work . " real courage require , 

 transatlantic colleague prove possession . 
 regret associate 
 judgment 

 Ir state Arion Society Milwaukee 
 abandon idea perform Dr. Stanford 
 " requiem " Goring Thomas 
 ' Swan skylark . " reason assign , 
 probably Society find sacred work 
 master 

 pray act , entitle ' * Adelaide , " write 
 Beethoven song , recently 
 produce America . work 
 German , introduce great master 
 dramatis persone , Frau Lachner , 
 Beethoven housekeeper ; Clara , landlady 
 daughter , musician copyist ; 
 Adelaide , great lady master 
 love . course famous song introduce , 
 recorded history , , enter 
 scene . Beethoven sing ; 
 fill important place drama , 
 represent , production play , Mr. 
 Bispham , manner " pathetic , dignified , 
 touching 

 book , ' ' good music ? " Mr. W. J. 
 Henderson , distinguished american critic , 
 observe : " judge observation extend 
 tolerable number year , 
 thousand person attend piano recital , 
 real knowledge good piano 
 playing . " Mr. Henderson figure probably 
 design emphasis precision ; 
 certain ' treal knowledge " 
 fashionable pianist 
 ar 

 LAMOUREUX concert 

 result plébescite submit Mr. Robert 
 Newman Lamoureux concert Queen Hall , 
 2oth ult . , programme contain Beethoven 
 ' Egmont ' Overture , Tschaikowsky Sixth Symphony , 
 Saint - Saéns ' ' Danse Macabre , " Larghetto 
 Dvordk Serenade E ( Op . 22 ) , everlasting 
 ' ¢ Ride valkyrie . " Mr. Newman add 
 novelty shape Pianoforte Concerto F minor 
 ( . 2 ) M. Théodore Dubois , previously 
 perform England . work , 
 originally produce January 30 Paris Conserva- 
 toire concert , consist distinct movement . 
 possess considerable musical charm , 
 theme expressive graceful , treat 
 picturesque manner . number head Adagio 
 con sentimento profondissimo , sentiment 
 reach depth " profondissimo , " ' 
 genuine poetical , acceptable . 
 Scherzo spirit lurk Mendelssohn 
 " Midsummer Night Dream " music , robust 
 masculine character Finale provide effective contrast 
 form brilliant conclusion . 
 solo portion , grateful player , 
 excellently render Mdlle . Clotilde Kleeberg , 
 executant initial performance , 
 crisp touch vivacious style music peculiarly 
 accord . M. Lamoureux reading Tschaikowsky 
 ' * pathetic " ? Symphony notice length 
 month number Musicau TIMEs , , there- 
 fore , necessary occasion 
 interpretation second movement decidedly 
 , attention pay expression 
 accentuation grace . detail 
 orchestration audible remarkable clearness , 
 entirety performance worthy work 
 conductor fame . unnecessary 
 ideal rendering Saint - Saéns grotesque 
 symphonic poem " Le Danse Macabre , " reading 
 Beethoven overture Goethe drama ' ' Egmont " 
 satisfactory . deficient dignity , opening 
 Allegro slowly , expression 
 wanting masculine sentiment 

 CRYSTAL PALACE CONCERTS 

 TuE Saturday concert March 26 coincide 
 seventy - anniversary Beethoven death , 
 programme accordingly devote exclusively 
 composition master . symphony 
 " Pastoral , " Mr. Manns secure enjoyable 
 rendering , Lady Hallé loudly deservedly 
 applaud fine performance Violin Concerto . 
 vocalist afternoon , Miss Marie Berg , 
 successful début Clarchen song " ' Egmont " 
 early aria entitle ' ' Primo amore , " recently issue 
 supplemental volume Beethoven work 
 previously sing public country . remain 
 number " Egmont " " Fidelio ' ? overture 
 romance F violin orchestra , play 
 Lady Hallé . miserable weather , possibly 

 attraction University boat race , account 
 sparse attendance 

 Pianoforte Concerto b flat minor , Mendelssohn 
 " hear prayer , " _ selection Schubert 
 " * Rosamunde " ' music , Handel Largo G , 
 march chorus Wagner ' " Tannhauser . " 
 audience , , disappointingly small . M. , 
 Gabrilowitsch , undertake solo concerto , 
 prove , occasion english début 
 work Richter concert 1896 , executant 
 remarkable brilliancy ; Madame Lucile Hill , 
 solo Mendelssohn Psalm allot , introduce 
 pleasing song Mr. Garnet Cox , ex - student 
 Royal Academy Music . Mr. Manns conduct 
 habitual energy choir acquit 
 credit 

 Mr. Manns unfortunately prevent severe 
 indisposition direct concert oth ult . , 
 able substitute find Mr. F. H. Cowen , 
 conductor Hallé concert direct excellent 
 performance Beethoven Second Symphony , . 
 ture Gluck " Iphigénie en Aulide , " ' dance 
 ' ' Elysian Fields ' ? composer ' " Orfeo . " 
 pianist afternoon Mdlle . Clotilde Kleeberg , 
 brilliant technique charm expression secure 
 great success Chopin F minor Concerto . Miss 
 Rosa Green familiar air Saint - Saéns 
 ' ' Samson et Dalila ' moderate success , M. 
 Jacques Renard , principal violoncellist Crystal 
 Palace Band , play characteristic arabian dance 
 brother , M. Jean Renard , considerable skill 

 Miss Leonora Jackson , accomplished young american 
 violinist , hear Queen Hall 
 concert Salle Erard , début Syden- 
 ham 16th ult . signal success , choose 
 principal solo Bach Concerto violin , string , 
 clavier , extremely interesting work believe date 
 Céthen period composer . solo 
 play admirable style Miss Jackson , enter- 
 prise introduce practically novelty 
 England deserve cordial recognition . Mr. Manns , happily 
 restore usual health , direct excellent performance 
 Brahms noble Symphony D , overture 
 " Flauto Magico , " Sullivan Overture ' ' Di Ballo . " 
 Mr. Andrew Black vocalist , singe Henschel 
 effective ballad ' " ' Jung Dieterich ' ? song Mr. 
 Cowen incisive effective style 

 MONDAY SATURDAY popular concert 

 Tue record fortieth season entertainment 
 conclude brevity . concert 
 demand mention Saturday performance March 26 , 
 programme select entirely music 
 Beethoven commemoration anniversary 
 master death , occur March 26 , 1827 . 
 work render Quartet C minor ( op . 18 , . 4 ) , 
 mature Quintet C ( op . 29 ) , abstruse 
 Quartet minor ( op . 132 ) . Lieder 
 sing perfection Mrs. Henschel 

 Saturday performance season , 2nd 
 ult . , Joachim Berlin Quartet 
 advantage , begin Beethoven unsur- 
 passable quartet C ( op . 59 , . 3 ) end 
 Professor Villiers Stanford masterly quartet D minor 
 ( op . 64 ) . vocalist Madame Blanche Marchesi , 
 impart utmost significance song 
 composer 

 Brahms master specially honour 
 closing concert 4th ult . , date nearly 
 possible accordance anniversary 
 composer death . Joachim artist perform 
 somewhat intricate quartet C minor ( Op . 51 , 
 . 1 ) , quintet G ( op . 111 ) , sextet 
 b flat ( op . 18 ) . splendidly render 
 Joachim Quartet party , assistance well- 
 qualify artist . Dr. Joachim receive fervent greeting 
 come forward play hungarian 
 dance , audience gladly hear , 
 violinist firm refusal grant encore . 
 Madame Blanche Marchesi sing Brahms Lieder 
 perfection 

 second Mr. Mrs. Henschel vocal 
 recital place St. James Hall , Wednesday , 
 March 30 , attend . perform- 
 ance deserve record , , apart 
 merit execution , programme invariably 
 educational tendency , conventional song duet 
 avoid . antiquarian music 
 adequately represent song emanate J. W. 
 Franck , Handel , Cimarosa , Salvator Rosa , Paisiello , 
 Arne . interesting , , majority 
 audience song Schubert , Liszt , Davidoff , 
 Loewe , Mr. Henschel follow . 
 select sing purity style 
 characterise effort Mr. Mrs. Henschel 

 Mr. Moritz Rosenthal having recover injury 
 finger , time incapacitate play , 
 pianoforte recital London season 
 Monday afternoon , March 28 . richly endow german 
 artist trace indisposition , 
 course opening number — Mozart sonata , end 
 turkish March , mere child play . 
 Chopin Sonata b flat minor ( Op . 35 ) , 
 Funeral March , minor composition 
 polish master , include waltz D fiat , 
 contrapuntal study — , 
 independent new left hand . 
 deftly write , duly acknowledge , pass 
 disapproval . second recital , 4th ult . , 
 Mr. Rosenthal delightful reading Beethoven 
 sonata e flat ( Op . 81 ) , ' Les adieux , l’absence , et 
 le retour ' ; great success win 
 recital year , place 
 16th ult . commence Beethoven sonata e 
 ( op . tog ) , deliver requisite expression , 
 individuality true significance style 
 display Schumann ' " Etudes Symphoniques " 
 ( Op . 12 ) , fine interpretation scarcely 
 imagine , manipulation feeling 

 Mr. F. H. Cowen term , unhappily , 
 english Schubert matter song - writing , 
 certainly pen graceful charming lyric profusion . 
 thirty present song 
 Recital , St. James Hall , rst ult . , 
 time prove 
 Mr. Cowen facility aught diminish . 
 mention number impossible , com- 
 parison odious , attention 
 couple fact reference Mr. Cowen song . 
 invariable tunefulness voice 
 tastefulness difficulty accompani- 
 ment . executant enjoyable concert 
 Miss Florence Oliver , Miss Mabel Berrey , Miss Evangeline 
 Florence , Madame Medora Henson , Miss Clara Butt , Miss 
 Fanny Davies , Messrs. Edward Lloyd , Hirwen Jones , 
 Andrew Black , Santley 

 Mr. Paderewski hero hour 
 privilege draw large audience 
 know present pianoforte recital 
 Town Hall . occasion Messrs. Harrison 
 concert season , place March 28 . 
 remarkable performance vest 
 excerpt Chopin , include nocturne G 
 ( op . 57 , . 2 ) , study , . 6 , 8 , 9 , book ii . 
 ( op . 25 ) , Berceuse , great Polonaise flat 

 programme interest provide 
 Festival Choral Society orchestral 
 concert present series , Town 
 Hall March 24 , presence large apprecia- 
 tive audience , Dr. C. Swinnerton Heap skilful con- 
 ductorship . feature concert Mr. Leonard 
 Borwick scholarly performance Beethoven fourth 
 Pianoforte Concerto , admirably accompany 
 orchestra . principal novelty consist 
 render Tschaikowsky Symphony ( . 5 ) 
 e minor ( op . 64 ) , worthy companion " ' Pathétique . " ' 
 German symphonic poem " Hamlet , " write 
 Musical Festival , welcome piece , create 
 storm applause conclusion . Mr. Watkin 
 Mills vocalist 

 Miss Fanny Davies annual concert place 
 Masonic Hall March 22 , coadjutor 
 Dr. Joachim , eminent violinist . artist 
 truly magnificent performance Brahms Sonata 
 ( Op . 100 ) , respective soli Miss Fanny Davies 
 Dr. Joachim arouse enthusiasm 
 audience . Miss Louise Phillips prove accom- 
 plishe vocalist 

 Stainer ' crucifixion ' sing Lent 
 Church St. Mary Redcliffe , hear 
 city shortly publication 

 brief season chamber concert Miss Lock 
 bring close March 28 , talented 
 lady associate play work Beethoven , 
 Spohr , Dvorak , Max Bruch , Miss Lilian Havard 
 contribute song 

 prolonged preliminary discussion Committee 
 appoint prepare scheme Festival year 
 present plan guarantor end 
 March , unanimously decide hold tenth 
 Festival autumn 1899 , provide guarantee fund 
 £ 4,000 raise August 1 year 

 appearance pupil Glasgow 
 Atheneum School Music , evening March 25 , 
 excellent record St. Andrew 
 Hall fairly claim . eighth annual 
 concert connection Principal Allan MacBeth 
 prosperous Institution , function serve periodically 
 general working School . concert 
 , , educational course . Mr. 
 MacBeth , conduct notice , , 
 usual , devise interesting programme , include 
 Mendelssohn Pianoforte Concerto ( . 2 ) D minor 
 De Beriot Seventh Violin Concerto . 
 movement pertain familiar composition 
 fall different ( lady ) student , 
 experiment amply justify wonder- 
 fully artistic success . organ solo song 
 contribute evening , orchestra , 
 include thirty lady violinist , 
 distinct advance previous experience regard 
 fulness tone , firmness attack , careful attention 
 nuance . March 29 Glasgow Glee Catch 
 Club annual concert , direction 
 Councillor George Taggart . piece include Stevens 
 " cloud - capt tower , ' Horsley ' Celia arbour , " 
 Genée droll ' italian salad , " Wainwright " life 
 bumper , " rendering number mark 
 real artistic perception . evening 
 Partick Choral Society recital ' ' Spring " 
 section Haydn ' season , " selection 
 Handel ' ' Samson . " ' Mr. Terras interesting choir 
 welcome feature musical life 

 event season reserve evening 
 March 30 , Dr. Joachim famous Quartet appear 
 Queen Rooms . ' " society " turn large 
 number hear combination Mozart Quartet 
 G major , Brahms Op . 51 , . 2 , Beethoven 
 great B flat Quartet ( Op . 130 ) . , , 
 doubt large majority audience idea 
 function ! unfortunately , 
 secret chamber music ' second city 
 Empire " fall evil day . warm thank 
 promoter concert afford treat 
 rare artistic . March 31 member 
 Glasgow United Young Men Christian Association 
 Choir good performance Handel ' ' Samson " 
 City Hall . choir number 500 voice 
 skilful direction Mr. R. L. Reid . 
 4th ult . Stainer ' crucifixion ' ? 
 Free College Church choir train 
 Mr. J. Crossland Hirst , organist church ; 
 previous evening Stainer impressive popular work 
 render close usual service Helens- 
 burgh United Presbyterian Church . second concert 

 London — Professor Harmony . Mr. McEwen 
 Academy student , ' Charles Lucas " medalist , 
 act sub - professor Dr. Prout . Mr. McEwen 
 earn laurel teaching staff Glasgow 
 Athenzeum School Music 

 indisposition Mr. Willy Hess cause post- 
 ponement Miss Cantelo classical concert March 
 28 engagement Sefior Arbos , Mr. J. Holme , 
 Mr. Whitehouse , , Miss Cantelo pianoforte , 
 strong combination . programme include 
 Mendelssohn C minor Trio , Schumann Pianoforte Quartet 
 ( Op . 47 ) , Rubinstein Sonata ( Op . 18 ) violoncello 
 pianoforte , Christian Sinding Sonata ( Op . 27 ) 
 violin pianoforte . little know , 
 render Sefior Arbos Miss Cantelo receive 
 mark appreciation . Mr. Whitehouse 
 established favourite , playing Rubinstein 
 - know sonata enjoy 

 Messrs. G. Ellenberger Edwin Thorpe chamber 
 concert establish , conclude 
 concert , March 31 , alive anticipation 
 season work . pianist Miss Ellenberger , 
 Mr. Thorpe artistic rendering 
 Beethoven sonata G minor violoncello piano- 
 forte . solo Prelude , Impromptu , Etude 
 Chopin ; Dvordk ' ' Dumky " Trio ( Op . ) , 
 time month ago , repeat , sign 
 grow appreciation audience 
 remarkable fascinating work . Mr. Ellenberger 
 violin solo Adagio ( arrange suite D ) 
 Bach germanic dance Kreuz , 
 enjoy 

 MUSIC SHEFFIELD DISTRICT . 
 ( correspondent 

 Benton conductor . tst ult . interesting 
 revival Graun Passion music place St. Chad 
 Church , near Leeds . material entirely 
 home - ; principal , Master John Hall — remark- 
 ably capable treble , way — Messrs. Gaunt , Barnes , 
 Knowles , chorus , simply 
 member ordinary choir . equal 
 task evidence smooth performance 
 , Mr. H. Percy Richardson sympathetic 
 accompaniment organ . L 
 Williams cantata ' ' Gethsemane " subject 
 similar service Emmanuel Church , , 
 Yorkshire Cathedrals York , Ripon , Wakefield , 
 ambitious parish church diocese , 
 musical recognition 
 season 

 March 23 fine peculiarly sympathetic 
 performance Brahms ' " ' german ’' requiem , 
 Dr. Stanford conductorship , Leeds 
 Philharmonic Leeds Subscription joint - concert . 
 follow Dr. Parry setting Pope ' ' ode 
 St. Cecilia Day , " ' write , remember , 
 Leeds Festival 1889 . sing vigour 
 expression composer conductorship , 
 jack little steadiness restraint matter 
 speed - rate . principal , 
 excellent , Miss Palliser Mr. Kennerley Rumford . 
 March 29 Leeds Symphony Society concert . 
 amateur body , considerable musicianship 
 programme include Beethoven 
 Symphony , Mozart pianoforte concerto ( 
 Mr. C. Wilkinson pianist ) , overture Max 
 Bruch , Sterndale Bennett , Gounod . Mr. Elliott 
 leader solo violinist , Mr. A. E. Grimshaw 
 conduct ably . Mr. Grimshaw Mr. Bernard 
 Johnson , 6th ult . , Public Art 
 Gallery , recital music pianoforte 
 interest novelty , apart intrinsic 
 merit . play thoroughly musicianly fashion 
 piece Mozart , Schumann , Liszt , Saint - Saéns ; 
 Miss Enid Grimshaw vary programme pleasantly 
 song . Easter week Leeds Amateur 
 Operatic Society musical entertainment 
 exact description shape " Falka , ' ? 
 play crowded audience Grand Theatre . 
 cast efficient , easily fulfil small 
 histrionic demand comic opera ; main feature 
 production good chorus - singing 
 excellent ensemble , conductor , Mr. Waith- 
 man , stage manager , Mr. R. P. Oglesby , 
 severally responsible 

 Bradford flicker musical season 
 place March 25 , Gounod " * redemption " 
 Festival Choral Society . Miss Agnes 
 Nicholls , Miss Florence Oliver , Mrs. Powell , Messrs. 
 Sandbrook , Leyland , Uttley principal 
 good performance , conduct Mr. F. H. Cowen 
 usual ability usual energy . 
 Huddersfield , March 22 , Subscription Concerts 
 come end , fine band Grenadier Guards 
 supply programme artistic character 
 usually associate military band . highly interesting 
 revival Handel seldom - hear oratorio " Jephtha " 
 place Pudsey , March 28 , Mr. Jowett 
 direction . principal Madame Sadler - Fogg , 
 Miss Frood , Mrs. Ramsden , Mr. Brearley , Mr. Riley , 
 capable , pluck 
 vigour chorus deserve great praise . band , 
 usual , weak place . March 22 Keighley 
 Musical Union ' Elijah , " Mr. Benton 
 conductor , Miss Maggie Davies , Miss Alice Richardson , 
 Messrs. Kellett Andrew Black principal . 
 Harrogate , Mr. Zeldenrust appear Messrs. 
 Haddock musical evening March 21 , 
 31st exceptionally interesting programme offer 
 Messrs. Naylor Gutfeld chamber concert , 
 Mr. Herbert Parsons remarkably fine interpretation 
 Liszt Pianoforte Sonata B minor , violin 

 recital afford evening pure enjoyment 
 final subscription concert Wakefield March 24 . 
 musical product North East 
 Ridings far populous 
 vocally inclined West Riding , 
 record , promise 
 near future . ' Elijah ' York Musical 
 Society March 29 , Canon Hudson conductorship , 
 title - réle Mr. A. H. Gee , 
 principal Miss Gertrude Hughes , Miss Hannah 
 Jones , Mr. Gwilym Richards . Hull Vocal Society , 
 Dr. G. H. Smith conductor , , 
 date , Dvordk ' ' Stabat Mater , " 
 singular vogue Yorkshire Leeds 
 Festival 1895 , indicate good music 
 way popular esteem time allow . 
 associate Schubert ' ' unfinished " Symphony 
 Mendelssohn ' ' hear prayer , ’' principal 
 Miss Alice Simons , Miss Muriel Foster , Mr. C. Ellison , 
 Mr. A. F. Ferguson . Scarborough , Choral 
 Union , help voice Leeds Phil- 
 harmonic Society , ' ' Elijah " " Easter Monday , 
 chief soloist Madame Goodall , Miss E. Thornton , 
 Messrs. Brearley Thornton , Mr. Pitcher 
 conductor . Whitby Choral Society Gade 
 " Crusaders , " Mr. Hallgate conductorship , 
 March 30 , Miss Moorhouse , Mr. Reed , 
 Mr. Thornton principal , band usual 
 dimension , incomplete wood brass . 
 surprising difficulty feel large 
 West Riding town accentuate populous 
 lace 

 Bridlington Festival , important Yorkshire 
 event past month , place late 
 notice number , succeed , June 28 
 29 , eighth musical festival hold 
 little North Riding village Hovingham . Brahms 
 " german " Requiem , I. ' Creation , " 
 Stanford ' ' revenge " choral work ' . 
 great interest appearance 
 Dr. Joachim , accept Canon Hudson invitation 
 present , play Beethoven Concerto 
 Bach Chaconne . Miss Agnes Nicholls , Mrs. Burrell , 
 Mr. J. Reed , Mr. Francis Harford , Mr. Plunket Greene 
 vocalist engage , Mr. Leonard Borwick , 
 occasion , solo pianist . 
 choose principal piece Schumann Intro- 
 duction Allegro Appassionata Mozart Concerto , 
 beautiful work major , compose 1786 . 
 exact programme truly , experience 
 festival Canon Hudson conductorship 
 - ambitious 

 good work Miss Wakefield Kendal 
 competition festival provoke copy , 
 avowed imitation plan Yorkshire 
 district . start York , 
 25th 26th ult . , late notice present 
 issue . prize offer school , large small , 
 string quartet party , choral society town 
 village , male voice , female voice , mixed 
 choir . sight singing soloist 
 competition , , event meeting 
 deem successful repetition , 
 hope , feature doubt extend 
 choral competitor . Leyburn , picturesque 
 Wensleydale , similar " Tournament Song " 
 hold 28th inst . , organise Hon . Lucien Orde- 
 Powlett local committee . class 
 madrigal , anthem , quartet , child choir , solo sight 
 singing , violin solo , string quartet , 
 musicianship district thoroughly test 

 particular come hand interesting 
 musical festival hold Bergen , week 
 June 27 July 3 , connection 
 International Exhibition place summer 
 little norwegian town . grand concert 
 performance choral orchestral work , solo 
 number orchestra , great majority live 
 norwegian composer , conduct respective 
 work . programme include Symphony B flat 
 major , norwegian Rhapsody C , string quartet 
 Johann Svendsen ; Pianoforte Concerto minor , 
 scene ' Olav Trygvason " solo , chorus , 
 orchestra , song orchestral accompaniment 
 Edvard Grieg , important composition Johan 
 Selmer , Christian Sinding , Ole Olsen , Iver Holter , 
 Schjelderup , . orchestra 
 Concertgebouw , Amsterdam , choral force 
 supply united choral society Bergen . 
 soloist Mesdames Ellen Gulbranson , 
 Gmir - Harloff , Lie Nissen ; Herren Lammers 
 Schjétt 

 draft programme forthcoming Leeds Triennial 
 Musical Festival ( October 5 - 8) issue . 
 follow chief work perform : ' ' Elijah , " 
 Mendelssohn ; suite , Tschaikowsky ; pianoforte concerto , 
 Schumann ; ' Stabat Mater , " Palestrina ; symphony D 
 ( Prague ) , Mozart ; ' ' Blest pair siren , ’' Hubert Parry ; 
 symphony C ( . 9 ) , Schubert ; " Alexander feast , " 
 Handel ; Choral Symphony , Beethoven ; Wagner 
 selection . addition composition , 
 follow novelty , write festival , 
 announced : cantata , " Caractacus , " Edward Elgar ; Te 
 Deum , Stanford ; sacred ode , ' ' foe , deep 
 , ' Alan Gray ; symphonic poem , Humperdinck ; 
 new cantata Sir Arthur Sullivan , , usual , 
 conductor festival 

 Tue Duke Cambridge preside 1e6th anniversary 
 festival Royal Society Musicians , Hotel 
 Métropole , 23rd ult . chairman , propose 
 toast evening , Jubilee offering 
 decide double allowance . 
 absorb £ 7,000 capital , ask 
 hearer liberal intend 
 . evening hon . treasurer announce 
 subscription £ 700 . artist 
 assist gratuitously musical programme Miss 
 Alice Esty , Miss Lucie Johnstone , Mr. Walter Coward , 
 Mr. Edward Branscombe , Mr. Albert James , Mr. Edward 
 Dalzell , Mr. Robert Hilton ; Herr Liebling ( piano- 
 forte ) Mr. Henry ( violin ) . Mr. Fountain Meen 
 accompanist 

 important musical exhibition , proceed 
 establishment fund 
 erection monument Wagner Berlin , 
 open capital 7th inst . comprise 
 autograph , portrait , object interest 
 connection celebrate musician , note private 
 collector different country having send contribution . 
 exhibition remain open August 12 

 Messrs. GREENE Borwick vocal 
 pianoforte recital St. James Hall March 25 . 
 Mr. Greene sing attractive selection old 
 modern song ; Mr. Borwick fine 
 interpretation Beethoven variation fugue 
 E flat ( Op . 35 ) , base theme movement 
 ' ' Eroica " " Symphony , Liszt ' Etude d’éxecution 
 transcendante " F minor . Mr. S. Liddle 
 accompanist 

 Miss Grace Wonnacott , pupil Professor Klindworth 
 Berlin , successful pianoforte recital March 
 30 , Atheneum , Camden Road . Miss Wonnacott 
 play Beethoven Sonata e flat , Liszt Ballade B 
 minor , Chopin Scherzo b minor , - 
 encore . recitalist join Miss Margaret 
 J. Hollick sonata Mozart Grieg pianoforte 
 violin . Madame Antoinette Sterling vocalist 

 conclusion article ' " Structure 
 Instrumental Music , " Mr. W. H. Hadow , unavoidably 
 hold owe unusual pressure space 

 FRANKFORT - - Main.—Herr Richard Strauss conduct 
 new symphonic poem ' ' Don Quixote ' ? eleventh 
 subscription concert Museum month , 
 success far exceed achieve work 
 recent performance Cologne . Herr Hugo 
 Becker interpreter violoncello solo , 
 form important feature score 

 HamsBurc . — - know Hamburg Bach - Verein , 
 render excellent service cultivation 
 oratorio town , associate 
 late Hans von Bilow memorable performance 
 Beethoven Ninth Symphony , dissolve . 
 Society final concert March 22 , - fifth 
 anniversary foundation , Missa Solemnis 
 produce pen Herr Adolph Mehrkens , 
 conductor beginning 

 Leipzic.—The new opera , ' ' Das Unméglichste , " ' 
 Herr Anton Urspruch , senior professor 
 Raff Conservatorium , Frankfort , 
 receive production March 29 . Herr 
 Urspruch favourably know 
 dramatic composer opera ' ' Der Sturm , " Shake- 
 speare ' ' Tempest . ’——The know musician 
 theoretical author , Dr. Johannes Merkel , accept 
 professorship Conservatorium . Herr Fritz von 
 Bose , Carlsruhe , pianoforte class 
 Institution . Herr Zéllner , year 
 past conductor New York " Liederkranz , " 
 appoint conductorship University Choral 
 Society room Dr. Kretzschmar , resign 

 VIENNA.—The committee charge erection , 
 capital , monument Johannes Brahms 
 issue dignified earnest appeal lover 
 decease composer support undertaking . 
 concert devote object , 3rd ult . , 
 Madame Alice Barbi , number Brahms 
 Lieder admirably render gifted vocalist , 
 — Herr Carl Goldmark appoint chair 
 vacate death Brahms directorate 
 Gesellschaft der Musikfreunde.——Herr Gottfried 
 Preyer , worthy Capellmeister St. Stephen Cathedral , 
 celebrate ninety - birthday month excellent 
 health spirit . bear Hausbrunn , Lower 
 Austria , 1807 

 Wermar.—The seventh philharmonic concert 
 season , March 25 , conduct Herr Felix 
 Weingartner , eminent Berlin Capellmeister , prove 
 enormous success . Beethoven C minor Symphony , 
 ' Liszt symphonic poem " Tasso , " conductor 
 ‘ symphonic poem ' Die Gefilde der Seeligen " ( inspire 

 Bocklin painting ) include programme . 
 | WrespapeN.—Herr Albert Eibenschiitz , Berlin , 

 CrICKHOWELL.—The Crickhowell Choral Society 
 i. ii . Haydn ' creation ' ? mis- 
 cellaneous selection Clarence Hall , 13th ult . 
 Mr. G. R. Sinclair ( Hereford ) conductor , 
 soloist Miss Rosina Hammacott ( soprano ) , Mr. 
 Gwilym Richards ( tenor ) , Mr. A. Lord ( bass ) . Mr. 
 Donald Heins play solo Saint - Saéns Rondo 
 Capriccioso violin orchestra , Miss Hammacott 
 sing Jewel Song Gounod " Faust , ' ' 
 orchestral accompaniment 

 Croypon.—Miss Agnes I. Fennings , pupil Mr. R. I. 
 Rowe Croydon Conservatoire , pianoforte 
 recital Small Public Hall March 25 . 
 programme include Sonata Appassionata ( Beethoven ) , 
 piece Grieg , Weber , Schumann , 
 Liszt . Miss Fennings prove pianist 
 considerable attainment promise 
 future advancement . vocalist Miss Eleanor 
 G. Gibbs.——In connection Croydon 
 Conservatoire Music student ’ concert 
 afternoon rst ult . , Small Public Hall . 
 young performer display high average merit , 
 member teaching staff represent 
 pupil 

 DevizEs.—The second concert season 
 Devizes Musical Association place 13th 
 ult . , ' * Messiah " perform . chorus 
 enter work remarkably , concert 
 successful Society . 
 soloist Miss Stanley Lucas , Miss Lucie 
 Johnstone , Mr. G. Perrins , Mr. Douglas Hoare . Mr. 
 J. Duys lead orchestra , Mr. H. H. Baker conduct 
 commendable alertness precision 

 refined singing , reflect great credit conductor 
 Society , Mr. W. H. Speer . soloist Miss 
 Charlotte Dickens , Mrs. E. Gentle , Mr. H. Stubbs , 
 Mr. Charles Tree . Mr. Stanley Blagrove lead orchestra 
 Mr. George Rose preside organ 

 TUNBRIDGE WELLS.—The Orchestral Society at- 
 tractive resort , number - performer , 
 concert season Great Hall , March 31 . 
 programme comprehensive , include 
 Bach Suite D , Mackenzie ' ' Benedictus , " Schubert 
 b minor Symphony , Beethoven " Prometheus " 
 overture . Mrs. Adeney play solo Mendels- 
 sohn Capriccio pianoforte orchestra b minor 
 ( Op . 22 ) ; Miss Ferguson soloist Beethoven 
 Pianoforte Concerto e flat , Miss Helen Jaxon 
 contribute song acceptance . Mr. Frederick 
 Hunnibell conduct successful interesting 
 concert , Sir Walter Parratt Dr. Lloyd 

 J. | present 

 G. K.—The study horn seriously affect 
 voice . obtain class instrument , 
 crook , guinea 

 Pianist.—A short analysis Beethoven Andante f 
 find Ridley Prentice ' ' musician , " 
 Grade V. , p. 23 ( F. Curwen Sons 

 A. Y. z.—pronounce word ' ' live , " ' live - long , " 
 short vowel 

 expression phrasing , ALBERTO RANDEGGER . 1 . " Deh , 
 vieni alla Finestra ’' ; 2 . ' Fin Ch’han dal Vino " ; 3 . ' ' Madamina , " 
 book 4 . 2s . 
 RAUN , CHARLES — " Snow Queen . " 
 operetta . Children voice . word write Mrs , 
 GeorGE Martyn . Sol - fa , 6d . 
 ICHARDS , ARTHUR — " Waxwork Car . 
 nival . " " humorous operetta . word write BERNARD 
 Pace . Sol - fa , 8d . 
 OMERVELL , ARTHUR — ' " ode sea . " 
 poem write Laurence Binyon . Set music 
 Soprano Solo , Chorus , Orchestra . book word , 7s . 6d . 100 , 
 PEER , W. H.—Festival overture . 
 orchestra . score , 8s . z 
 ICKS , ERNEST A.—A Handbook Examina- 
 tion Music . contain 600 question , answer , 
 theory , harmony , counterpoint , form , fugue , acoustics , musical 
 history , organ construction , choir training ; 
 miscellaneous paper set Examining body . Bas . , 38 . 6d , 
 \ x JILHELMJ , AUGUST , BROWN , JAMES — 
 Modern School Violin . Section I. , Book i. 2s . 
 AREING , HERBERT W. — ' Court 
 . Summergold . " fairy operetta child . 
 word Isa J. PostGaTE . . 
 " Princess Snowflake ; , fate 
 Fairy Nicoletta . " fairy operetta . child . word 

 Isa J. POSTGATE . . . 
 
 word write Hett1e M. Hawkins . 1s . 6d . 
 SCHOOL music review . . 71 . con . 
 tain follow music notation : — " Song 
 " ( ' ' mailie " ) , Unison Song . BEETHOVEN ( Op . 52 , . 4 ) . 
 " evening . " trio . Fors.s.a . Franz Apt . specimen test 
 Pupil Teachers Schools . exercise change key . 14d . 
 OVELLO SCHOOL SONGS . — edit 
 W.G.McNavuGut . publish intwo form . A. Voice Parts , 
 Staff Tonic Sol - fa Notations , Pianoforte Accompaniment , 
 8vo size ; b. Voice Tonic Sol - fa Notation . A. B 

 . 476 . step atatime . - song . s.a . ) 
 E.J.TROuP| _ ig 
 » 477 . sweet pleasure . - song j ' 
 E. J. Troup 
 » 478 . love draw near . — — song 
 -J.TRoup| _ sg 
 » 479 . swift offence . - : 
 ong .. .. E. J. TRoup 
 » 480 . werea voice . - song ) 
 E.J.TRour ! _ sg 
 » 481 . evening song . - song j 
 E. J. TRouP 
 » 486 . - day . - song .. E. J. Troup 
 » 487 . wake morn . - son — d 
 E. J. TRoup 
 » 488 . spring song . - song E. J. TRoup 
 : 489 . truth gold . - — wd 
 Song .. pe nes e. E. J. TRoup 
 » 490 . true worth . - song E. J. TRour ) 
 » 491 . need hour . - song — i. 
 . J. Troup ) 
 » 492 . river flow . - song ) 
 . J. TRouP 1d 
 » 493 . law thy beauty ? - F 
 ong .. ‘ < .. E. J. Troup 
 » 494 . bell Mercy . - song 
 E.J.TrouP| _ iq 
 » » 495 . o bea sunbeam ? - é 
 ong .. bis .. . J. Troup 
 » 496 . beautiful face . - song ) 
 $ .J.TRouP| _ yg 
 » 497 . hold life like lily flower . two- } , 
 song ... os .. E. J. Troup 

 M. Wise 

 ash F. Bridge 
 Eaton Faning 
 -- Oliver King 
 ee . Croft 
 ee - . Bach 
 H. Lahee 
 Beethoven 
 Handel 

 J. Naylor 
 Myles B. Foster 
 J 

 DRAMATIC CANTATA 
 
 soprano , Tenor , Bass Soli , Chorus , Orchestra 

 L. VAN BEETHOVEN 

 english word write adapt 

 change cantata , english word , | 
 write adapt Mr. Paul England , object 

 place Beethoven music hand choral 
 society . . . . result purpose completely 
 achieve , Messrs. Novello depend good 
 reward enterprise . beauty 
 music -- greek - like simple outline — cantata 

 spread rapidly country , 

 DAILY CHRONICLE 

 treat hear music solid 
 graceful . ' ruin Athens " rank 
 Beethoven high achievement , 
 effective use barbaric 
 element chorus Dervishes — air , emphasise 
 wild accompaniment . . . . quaint turkish 
 March chorus , work reach 
 majestic termination , warmly applaud , 
 — elegant march chorus , ' * Twine 
 ye garland ’' — tell impulsive number 
 Dervishes , render appropriate 
 vigour energy . . . . work suitable 
 concert platform , adopt choral 
 society anxious include répertuire composition 
 high class 

 WESTMINSTER GAZETTE 

 consider rubbishy character production 
 frequently secure attention local choir , 
 astonishing beautiful work 
 Beethoven neglect assiduously — 
 doubtless absence hitherto decent 
 arrangement work purpose some- 
 thing matter . preparation 
 arrangement use night , , 
 difficulty overcome , assume 
 henceforward work , contain Beethoven 
 graceful spontaneous choral writing , hear 
 

 ATHENZUM . 
 needless state music ' ' Die Ruinen von 

 skill enjoyment , vast majority 
 | audience applaud , opportunity 

 Athen " display Beethoven lofty mood , | applaud , utmost enthusiasm . reason 
 master light manner bar | success far seek . Mr. Leoni melody 
 . consequently strain | unimpeachable suavity , add attraction 
 executant Wednesday , choral number | possess strong family likeness favourite number 
 render power spirit , favourite Dervishes |by variety popular composer , Wagner 
 chorus , course , encore . | Mascagni 

 LONDON : NOVELLO COMPANY LIMITED 

 W. T. BEST 

 content . 
 BEETHOVEN . — March | MeENDELSSOHN.—War March . 
 " Egmont . " rg — Funeral March 
 BEETHOVEN.—Funeral March . ( op . 103 ) . 
 Cuopin.—Funeral March ( op . 35 ) . | MEYERBEER . — March ' Le 

 HANDEL . — dead March | Prophéte 

 John E. West 

 Minuet , Pianoforte Sonata ( op . 10 , " . 3 ) - . Beethoven 

 introductory _ — 
 March .. ee 
 Sunday Song 

