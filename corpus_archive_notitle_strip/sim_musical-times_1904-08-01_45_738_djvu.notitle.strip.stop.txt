WEDNESDAY MORNING , 11.30 , Concerto ( Lloyd ) , New 
 Work ( Hubert Parry ) , ‘ ‘ Requiem " ’ ( Brahms 

 THURSDAY MORNING , 11.30 , ‘ ‘ Apostles ’ ( Elgar ) , 
 Symphony F ( Beethoven 

 THURSDAY EVENING , 8 , ‘ ‘ Holy Innocents " ’ 
 Brewer ) , ‘ ‘ Hymn Praise 

 FRIDAY EVENING.—Overture , ‘ ‘ Euryanthe ’’ ( Weber ) ; New 
 Cantata , ‘ ‘ Ballad Dundee ’’ ( Charles Wood ) ; Violin Concerto 
 ( Stanford ) ; Songs Sea ( Stanford ) ; Overture , ‘ ‘ Lustspiel ” ’ 
 ( Smetana 

 SATURDAY MORNING . — 
 D ( Beethoven 

 SATURDAY EVENING . — ‘ ‘ Golden Legend " ( Sullivan ) ; 
 Sixth Chandos Anthem ( Handel 

 Symphony B flat ( Beethoven ) ; Mass 

 SERIAL TICKET , admitting Concerts ... ee eee 
 SERIAL TICKET , admitting Seven Concerts ( excluding 

 FRIDAY EVENING.—Overture , 
 Cantata , ‘ ‘ Ballad Dundee ’ 
 ( Stanford ) ; 
 ( Smetana 

 SATURDAY MORNING . 
 D ( Beethoven 

 SATURDAY EVENING . — “ 
 Sixth Chandos Anthem ( Handel 

 New 
 Violin Concerto 
 Overture , ‘ ‘ Lustspiel 

 Symphony B flat ( Beethoven ) ; Mass 

 Golden Legend " ( Sullivan 

 Responses Commandments selected 
 Services composers Myles B. Foster , 
 Dr. Garrett , Ch . Gounod , Dr. E. J. Hopkins , Dr. C. H. Lloyd , 
 Merbecke , H. Smart , John E. West , S. S. Wesley , 

 Sir John Stainer Sevenfold Amen included , Vesper 
 Hymns Beethoven , Sullivan , ; concluding 
 Vestry Prayers S. S. Wesley Rev. Canon Hervey 

 PREFACE 

 ap 

 504 MUSICAL TIMES.—Avucust 1 , 1904 . 
 ‘ unbuttoned ’ letter written Beethoven } ‘ Turning Beethoven Wagner find 

 Sir John Falstaff , ’ nickname jocose composer 
 given friend Ignaz Schuppanzigh , 

 celebrated violinist . letter 
 best specimens irreproachable caligraphy 
 German characters , 

 deciphered enthusiastic Beethoven scholar , 
 Mr. J. S. Shedlock , kindly 
 following translation article 

 best Falstaff 

 rest affectionately 

 Beethoven . 
 Artaria Co 

 Sir John Falstaff , Messrs 

 Thayer tells Beethoven kept singular 
 kind friendship Schuppanzigh . useful 
 friendship 
 developed great mutual liking , 
 actual affection . good - looking man , 
 Schuppanzigh , years went , 
 stout , Falstaff joke Beethoven , 
 wrote Graf Brunswick words : 
 ‘ Schuppanzigh married . ‘ wife 
 fat . family ! ! ’ ‘ great 
 joker , ’ Grove calls Beethoven , found 
 opportunity practising ‘ rough drollery 

 corpulent friend , Schuppanzigh . 
 blank page Beethoven Pianoforte Sonata 

 1 ) ( Op . 28 ) , composer scrawled following 
 asinine composition 

 master represented original text 
 ‘ Tristan , ’ penned neatest hands 

 strong contrast erratic = 
 undecipherable ‘ pot - hooks hangers ’ 
 Beethoven . London letter Wagner , lent 
 Lady Cusins addressed Mr. G. F. 
 Anderson , Treasurer Philharmonic 
 Society , special interest . year 1855 , 
 memorable season Wagner conducted 

 concerts Philharmonic Society . 
 communication ( translated 

 F. G. E. 
 ( continued 

 following replies — genuineness 
 fully authenticate — answer questions 
 examination paper set examiner repute : — 
 Marenzio German lady introduced notes 
 music . lived 13th century A.D. 
 burnt martyr Rouen . 
 Palestrina son stock - broke 
 Beethoven 

 r brother 

 guite sure person lived ! Needless 
 came near receiving sound thrashing 
 fooling people saw little game 

 meeting Hungarian violinist 
 Eduard Reményi amusing details . 
 better characterize strange 
 clever charlatan — , - - way , German- 
 Hungarian Jew , Hoffmann — story 
 play bar theme 
 slow movement Beethoven Kreutzer Sonata , 
 ‘ Hungarian style 

 famous incident Joachim said 
 drawn young composer 
 closest , life - long friend brave , unfaltering 
 champion destined , 
 corrected Herr Kalbeck important par- 
 ticulars . Sonata Violin Pianoforte 
 C minor ( Op . 30 , . 2 ) Brahms trans- 
 posed pianoforte half - - tone higher , 
 memory ! Reményi , vo¢ Kreutzer 
 Sonata , Mr. Hadow beautiful monograph 
 ( ‘ Studies Modern Music , ’ Second Series ) states . 
 happened , supposed happened , 
 painstaking author unable , 
 years event , verify detail — Celle , 
 Hanover , Géttingen ; Joachim 
 present . great violinist met Johannes 
 Hanover , ideal friendship commenced 
 rich results art , 
 fraught happiness friends . life 
 ‘ Altenburg , ’ Liszt , Princess 
 Sayn - Wittgenstein house Weimar , described 
 considerable length , order contrast hero 
 unsophisticated nature simple mode existence 
 Piano King exotic life ‘ managed ’ 
 ‘ bon ange adorable , ’ aforesaid Russian 
 princess , Herr Kalbeck allows prejudice bias 
 amusing extent . doubt 
 young Brahms displayed remarkable 
 strength character decided escape 
 magic charm Liszt wonderful personality — 
 charm succumbed readily 
 completely rest mankind . found 
 impossible join chorus adulation 
 visitors Altenburg chanted daily 
 hourly greater glory Liszt composer , 
 fled . life , , enthusiastic 
 admirer Liszt pianist . remarked 
 occasion Klaus Groth , Low German poet : 
 ‘ play piano ( named 
 pianists mark ) , 
 fingers hands . ’ : ‘ 
 heard Liszt simply speak piano playing ; 
 comes , long comes 
 . playing 
 unique , incomparable , inimitable 

 Preliminary particulars Festivals 
 come hand — Gloucester Cardiff . 
 programme Gloucester meeting — held 
 September 4 g — include following 
 novelties , set forth order performances : 
 Magnificat Nunc dimittis ( Ivor A. Atkins ) ; ‘ 
 Song Zion ’ ( John E. West ) ; Festival Hymn 
 ( C. Lee Williams ) ; ‘ Thing availeth , 
 short oratorio ( C. Hubert H. Parry ) ; ‘ Holy 
 Innocents , ’ oratorio ( A. Herbert Brewer ) . 
 familiar works comprise ‘ Messiah ’ 
 Selection ( Handel ) ; ‘ Elijah ’ ‘ Hymn 
 Praise ’ ( Mendelssohn ) ; Te Deum ( Stanford ) ; ‘ 
 Time - spirit ’ ? ( Granville Bantock ): Organ Concerto 
 ( C. H. Lloyd ) ; German Requiem ( Brahms ): ‘ 
 Apostles ’ ( Elgar ) , & c. Mr. A. Herbert Brewer , 
 organist Gloucester Cathedral , occupy 
 usual place conductor time - honoured Festival 
 Choirs 

 hundredth anniversary birth George 
 Sand duly celebrated Paris month . 
 friendship Liszt Chopin 
 connected music , 
 wrote subject art . appreciation 
 Chopin , somewhat extravagant , lacking 
 interest . said : ‘ genius deepest 
 imbued feeling . makes 
 instrument speak language infinite . . . 
 individuality - exquisite Bach , 
 powerful Beethoven , dramatic 
 Weber . . . Mozart superior 
 , addition calmness health 
 brings — z.c . , life fulness 

 fourth Cardiff Triennial Festival fixed 
 September 21 , 22 , 23 24 . novelties 
 presented : Welsh Rhapsody 
 Orchestra ( German ) ; * Eve ’ ( Massenet ) , 
 time England ; ‘ East , ’ tone - poem 
 orchestra ( Hervey ) ; ‘ John Gilpin , choral ballad 
 ( Cowen ) ; ‘ Victory St. Garmon ’ ( Harry 
 Evans ) . addition foregoing scheme 
 embraces performances ‘ Elijah ’ ‘ Hymn 
 Praise , ‘ Midsummer Night Dream ’ music , 
 ‘ Samson Delilah , ‘ Dream Gerontius , 
 ‘ Faust ’ ( Schumann ) , ‘ Desert ’ ( Félicien David ) , 
 Requiem ( Verdi ) , Wagner selections , & c. Dr. Cowen 
 discharge duties conductor . gave 
 forecast Leeds Meeting — held October 
 5 8—-in June issue , completing trio 
 Musical Festivals shortly held , doubtless 
 usual success , England 

 Schubert occupied low place 
 Vienna Society , unknown short 
 lifetime , died early age thirty - , 
 left 600 + songs , Masses , dozen 
 Operas , Symphonies , mass Sonatas , 
 Quartets , miscellaneous music 
 uncountable . children poor 
 Viennese schoolmaster . married ; 
 poor money 
 dinner ; friends level ; 
 held post , settled income , 
 scantiest , months ; 
 dissipated man , fondly attached 
 belongings , idol friends ; 
 rarely travelled , reading scanty ; fact 
 heaven - born genius , 
 nature art , 
 hour minute life 
 directly devoted music . person short 
 stout , ordinary looking } ; poorly clothed , 
 probably little manners , certainly 
 modest retiring disposition possible 

 born 1797 , died 1828 , year 
 Beethoven . life passed 
 city Beethoven , time great 
 composer producing year year 
 remarkable works . , 
 said lived Beethoven shadow , composed 
 immense mass music enumerated , 
 great beauty absolute individuality 
 originality , gradually recognised 
 world hardly inferior great 
 contemporary , different . 
 said Beethoven woman 
 man , comparison happy . 
 slightly ! acquainted ; Beethoven 
 acquaintance Schubert music 
 death - bed , sure eye genius 
 experience recognised transcendent qualities , 
 expressed admiration 

 colossal Symphony C forms climax 
 Schubert achievements department 
 orchestral music , work per- 
 mitted ; composed commenced 
 March , 1828 , died following November . 
 culminating work Schubert life . 
 peculiar , unearthly tone 
 wild , mystical , tender melancholy marks 
 symphonic movements B minor ( . 8) 
 Entr’actes ‘ Rosamunde , ’ large 

 called work ‘ 10 ° , chain 
 evidence collected writer notice published 
 Atheneum November 1gth , 1881 , disproved , 
 assumed ‘ Gastein Symphony , ’ written place 
 summer 1825 , ‘ 9 , ’ existence . 
 discovery Beethoven youthful Cantatas shows 
 need hope cases 

 published Messrs. Breitkopf Haertel 
 splendid edition volumes , edited Dr. Mandyczewski , 
 Vienna , scholarlike accurate style , 
 indexes lists exacting investigator require . 
 considerable number songs printed time , 
 great interest 

 Ganz wie ein Fiaker ’ ; exactly like cabman — said Lachner 
 Mr. C. A. Barry 

 Spaun testimony fact Schubert having loudly 
 regretted Beethoven death Beethoven 

 Silastic efforts cause music seas 

 III . Scherzo Trio — Allegro vivace 

 Scherzo lost primitive meaning 
 ‘ joke , ’ independent movement , 
 equal importance , 
 large dimensions given Beethoven _ 
 Eroica . 7 , . 9 . content 
 quotations , calling attention 
 subsidiary passage , reason 

 appear . following idea 
 opening : — 
 . 16 . 
 Strings 

 IV . Finale — Allegro vivace 

 /7za / e forms astonishing climax 
 preceded . things strike 
 hearing ; _ , wonderful impetuosity 
 resistless force — difficult understand 
 man volcanic piece music 
 inside , bursting ; secondly , 
 marked character rhythm . 
 movements , Beethoven , 
 rhythm evident irresistible . opening 
 bars , sonorous clang , nobility 

 time come new feature — ‘ second 
 subject ’ movement , key G , preceded 
 marked notes horns , consisting 
 bars minims succeeded bars 
 crotchets 

 cres 

 strong relationship theme /7a / e 
 Beethoven Choral Symphony 

 Shortly , end 
 half /7v«/e , beautiful passage 
 basses gradually 
 octaves , force sound diminishing 
 time , orchestra /// simple 
 quartet strings . half moveinent 
 closes modulation E flat , clarinets , 
 tone makes shiver , begin 
 ‘ working - ’ second subject ( . 24 ) , 
 cast recall distinctly passage 
 alluded Ninth Symphony Beethoven . 
 trombones entirely 
 original manner , Beethoven left 
 instances . passage following 
 novelty trombone players 
 1828 

 . 25 . = 
 Tenor Trombone . — — = > Alto Trombone . — 

 ee 
 3ass Trombone 

 fact , Beethoven drum , Schubert 
 Symphony trombones . 
 effect , deepen 
 shadow , bring spot bright colour 
 , double parts choral music ; 
 released subordinate position , 
 given;them independent parts , new 
 office great family orchestra 

 return /7vale . development 
 second subject goes time , soon 
 minims begin force way , violin 
 triplets follow , movement starts , 
 rest moment till 
 rushed final catastrophe . near close 
 tremendous significance minims — 
 /z , fz , f= , fz=—appears ; manner 
 return Unison C widely intervening 
 notes wandered , repeat dreadful 
 strokes , like blows direful instrument 
 destruction , truly extraordinary . Coda begins 
 long crescendo commencing Ppp containing 
 progressive sections bars , 
 increasing uniformly force , AAA , AP , 7 , 72/ , 
 final climax crash reached 

 way , movement ( 7ema con variasion ? ) 
 ingenuity invention allied feeling 

 appearance spontaneously expressed . young 
 composer views , occasionally ,   orches- 
 tration , effects set Thames 
 fire , nearly ‘ come , ’ use jargon 
 orchestral specialist . work received degree 
 enthusiasm suggested birth veritable master- 
 piece . Let hope hero ‘ overwhelming 
 reception ’ live level head . 
 young composer achieved second success smooth 
 distinctly musical performance solo 
 Beethoven G major Pianoforte Concerto . Maria Yelland 
 sang ‘ Inflammatus ’ Dvorak ‘ Stabat Mater . ’ 
 powerful voice good quality , energetic , 
 assertive , way ejaculated words 
 wonder understood meaning . 
 difficult ‘ Chorus lower Maidens ’ Act II . ‘ Parsifal ’ 
 brightly lightly sung Choral Class , 
 complex pattern Wagner vocal polyphony stood 
 clearly 

 orchestra usual standard 
 Royal College Music . apparent vast deal 
 ‘ spirit , ’ degree roughness , especially 
 strings , greatly interfered enjoyment 
 Concerto Schumann C major Symphony ( . 2 ) , 
 closed evening music . draw 
 attention cacophonous ‘ tuning ’ 
 players indulge movements work ? 
 irritating habit veritable nuisance , 
 works , pauses dividing 
 movements , hear snatches works ( 
 programme ! ) played zw7 / h , travestied individual 
 players orchestra . time come utter 
 protest proceeding inartistic unworthy 
 great Music School 

 Shortly début country Master Vecsey 
 came rumours achievements 
 marvellous prodigy Master Florizel von Reuter , young 
 gentleman said wonderful 
 violinist little boy , composer 
 conductor boot , age ! wonder 
 large audience , included ( ) ueen 
 Alexandra , gathered Queen Hall June 29 
 expectancy unmixed curiosity 
 appearance England prodigy violinist - composer- 
 conductor 

 performance Beethoven ‘ Fidelio ’ 
 Overture Dr. Cowen direction Master Reuter 
 appearance , found slight - built boy 
 fair complexion flaxen curly hair , great contrast 
 dark sturdy Master Vecsey conceived . 
 began play Vieuxtemps Violin Concerto F : 
 soon manifest tone , 
 conscious efforts juvenile rival . 
 technical facility astounding , 
 prominently manifested keen sense rhythm 
 accentuation . Expressive passages rendered 
 great refinement , rapid s / accato notes runs 
 executed fascinating crispness clearness 
 attested extraordinarily developed nervous energy 
 young 

 sensational moment afternoon came 
 lad took baton conduct Symphony 
 F sharp minor , sharply tapped desk 
 huge orchestra matured musicians attention , 
 sight strange partook 
 incongruous ripple subdued merriment went 
 hall . Scarcely amusing 
 note punctilious care boy gave leads 
 entrances instruments energetic 
 gestures approached climax . scene 
 topsy - turveydom ! | Symphony chiefly remarkable 
 thematic material , principal subjects 
 melodious significant , symphonic character . 
 need scarcely added performance 
 boyish creation elicited enthusiastic applause , 
 Master Reuter sul : sequently heard Max Bruch 
 ‘ Scottish Fantasia ’ extra piece demanded 

 DucciANa.—-Mr . Arthur G. Hill ‘ Organ Cases 
 Organs Middle Ages Renaissance ’ ig ge 
 drawings following English organ cases : vol . 
 Westminster Abbey ( organ specially erected " 
 Coronation William M : uy 1677 ) ; Gloucester 
 Cathedral ; King Coliege , Cambridge ; Exeter Cathedral . 
 vol . ii.—Stanford Church , near Rugby ; Tewkesbury 
 Abbey ; Church St. Catherine Cree , Leadenhall 
 Street , London ; Eton College Chapel ( organ ) ; 
 Finedon Church , Northamptonshire 

 THELMA.—The following serve ‘ c / asstcad 
 lively duets pianoforte , vexy hard play ’ : 
 Beethoven — Sonata D ( Op . 6 ) Marches ( Op . 45 ) ; 
 Mozart — Sonatas D , B flat , C ; Hummel — Sonatas 
 E flat flat , Nocturne F ; Schubert — Marches , 
 Rondos , Variations , Divertissements Hongroise , 
 Polonaises ; Schumann — Bilder aus Osten Ballscenen ; 
 Weber — Pieces . original pianoforte duets , 
 arrangements 

 S. P. D.—The difficulty experience getting know 
 new songs overcome subscribing Musical 
 Circulating Library 

