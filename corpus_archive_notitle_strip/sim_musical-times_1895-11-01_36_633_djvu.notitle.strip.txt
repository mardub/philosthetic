ESSRS. HANN’S CHAMBER CONCERTS, 
Brixton Hall (Tenth Season), on Monpays, November 4, 18

and December 9, 1895, at 8 p.m., when the following works will be 
performed: Piano and Strings, Quartet in E flat (Mozart); Quartet in 
B flat, Op. 41 (Saint-Saéns); Trio in D minor, Op. 63 (Schumann); 
Sonate, P., Vio. in F, Op. 57 (Dvorak); Sonate, P., Cello in B flat, 
Op. 45 (Mendelssohn): S*+ings only, Quartet in F, Op. 18 (Beethoven) ; 
Quintet in B flat, Op. 87 (Mendelssohn) ; Quintet in D, No. 7 (Mozart

ee

Tue latest and by no means the least significant 
commentary on the confident declaration of a writer 
in the Nineteenth Century a few years ago that the 
‘* Wagner Bubble” was burst, comes from Paris. It 
is stated in the most authoritative way that M. 
Charles Lamoureux, the famous Conductor and 
Wagnerian propagandist, is about to build a new 
theatre on the model of that at Bayreuth. The 
plans are all decided upon, and it is expected that 
the theatre will be opened in 1898 with a performance 
of the “ Ring,” to be given without “cuts.” Certainly 
the conquest of France by Wagner, if somewhat 
tardy, is now complete. No more conspicuous proof 
of the dominating force of his genius could be found 
than such a victory, when the sensitive patriotism of 
the French and the aggressive Teutonism of Wagner 
are taken into account

Lovers of music, and more especially worshippers 
of Beethoven, will welcome the announcement that 
Messrs. Novello & Co. will shortly publish the book 
on Beethoven’s Symphonies upon which Sir George 
Grove has been engaged for some time past. It may 
be confidently asserted that his masterly and interest- 
ing analyses, written for the Richter and the Crystal 
Palace Saturday Concerts, have increased the enjoy- 
ment of thousands in these symphonies, as well as 
having greatly contributed to their present wide- 
spread appreciation in England. When it is said 
that these analyses may be regarded but as the pre- 
liminary sketches for the forthcoming volume, some 
idea may be gained of the value of Sir George Grove’s 
latest effort in the cause of the art he has loved so 
long and served so well

As a memorial to Henry Purcell, who was organist 
of Westminster Abbey for fifteen years, and who lies 
almost under the ‘‘Great Organ,” nothing could be 
more appropriate than to erect the two beautiful 
cases designed by Mr. Pearson, R.A., the sketches for 
which we are permitted to reproduce in our present 
number. The Abbey organ was rebuilt in 1884, but 
owing to lack of funds it was found impossible to add 
the cases. The idea has been warmly taken up 
by the Purcell Society and others, and already 
nearly one-half of the £2,000 required has been 
promised. It is to be hoped that the remainder of 
the money will be provided by the forthcoming 
Bi-centenary Commemoration Festival

Wesley’s fine Psalm, ‘In exitu Israel”; third, the Overture 
to “‘Die Zauberfléte.”” Concerning “ Visions,” enough has 
been said elsewhere. There is no advantage in flogging a 
dead horse, but let it be said that foreign composers really 
should not send our Festivals the sweepings of their studios. 
The Psalm and Overture were nobly rendered

Saturday morning. Some risky passages apart, the 
great Second Mass of Beethoven was impressively per- 
formed, the chorus, in particular, coming out of the diffi- 
culties in triumph. It was a tremendous effort at mastery 
of a work which, perhaps, will never entirely yield to 
human skill and energy. But if the conquest was not 
complete it proved highly honourable to all concerned, and 
should be considered, notwithstanding blemishes, as the 
proudest feather in the cap of the Festival. The soli were 
entrusted to Miss Henson, Miss Hilda Wilson, Mr. Ben 
Davies, and Mr. Black, who successfully discharged an 
arduous task, Schumann’s Symphony in B flat and Men- 
delssohn’s “‘ As the hart pants’? completed the programme

Saturday evening. The Festival closed with the 
“Creation” (Part 1) and the ‘‘ Golden Legend,” about 
which nothing need be said, while their efficient perform- 
ance can be taken for granted. Enough that these works 
were a fitting end of four days’ heavy labour and unques- 
tionable success. The grand orchestra, the grander 
chorus, the leading vocalists, and the resourceful conductor, 
Sir Arthur Sullivan, all deserved well of musical England, 
and may, in the language of the schools, “‘ go up one

CRYSTAL PALACE CONCERTS

Tue first of the fortieth annual series of the Saturday 
Concerts was held on the 12th ult., the programme being 
remarkable for its variety and interest. In one respect it 
was of historic interest, at least to Mr. Manns, who had 
himself played the violin obbligato to the Aria from 
Mozart’s “ Idomeneo,” sung on this occasion by Malle. 
Otta Brony, thirty-nine years and ten months previously ! 
Many things have happened in the musical world since 
1856, the year of Schumann’s death, but Mr. Manns 
happily goes on with a perennial continuity reminding us 
of Tennyson’s * Brook.” On the score of their novelty, 
priority of attention is claimed by two unpretentious pieces 
for orchestra, from the pen of Mr. J. F. Barnett, now per- 
formed for the first time with considerable success. The 
* Liebeslied,” with a flowing melody assigned to the violon- 
cellos, is one of those genial but obvious compositions in 
which the unexpected never happens. Of very much 
greater attractiveness and merit is the dance movement 
entitled ‘Im alten styl ’’—why not “In the olden style” ? 
—in which a piquant theme is subjected to treatment that 
is both graceful and ingenious. The harmonies in the 
Coda are hardly old-fashioned, but the general effect is 
excellent. Mr, Barnett was summoned to the platform 
and cordially applauded. Lalo’s ‘‘ Symphonie Espagnole,” 
a work comparatively unfamiliar to Sydenham hearers, 
served to introduce that accomplished artist M. Achille 
Rivarde, whose beautiful tone, elegant phrasing, and 
finished technique combined to produce a very favour- 
able impression on the audience. Lalo’s work is so full 
of graceful sentiment and picturesque colour that we 
wonder it is not more often played. Few artists, however, 
are equipped with the qualities necessary for its adequate 
interpretation. But of M. Rivarde’s claims to be included 
in the number there can be no question. His playing is 
marked by a fastidious purity of style and an absence of 
sentimentality rare in a French artist. His minor solo— 
Ernst’s very tinselly de-rangement of Hungarian airs—- 
supplied him with the opportunity for displaying his fine 
technique, his harmonics being wonderfully clean and true, 
but was otherwise of no artistic value. Mr. Manns secured 
a brilliant rendering of Berlioz’s ‘‘ Benvenuto Cellini” 
Overture, and, apart from a rather blurred rendering of the 
opening phrase, of Beethoven's C minor Symphony. 
Mdlle. Otta Brony, besides contributing the Mozart Aria 
mentioned above, was heard in a pretty but rather trivial 
Lied by Hans Harthan, and Brahms’s exquisite ‘‘ Verge- 
bliches Standchen,” a singularly futile English version of 
which, with a misleading title, was printed in the book of 
words. Mdlle. Brony sings conscientiously and with 
intelligence, but there is nothing about her voice or style 
which calls for more than moderate commendation. It 
may be noted here that one or two alterations have taken 
place in the band, the most striking being the retirement 
of Mr. Clinton, the well-known first clarinet player, who 
has been succeeded by Mr. Draper, an excellent young 
artist from the Royal College of Music

The second Concert falling within a day of the fortieth 
anniversary of the inauguration of the series on October 
20, 1855, a special programme was drawn up in honour of 
the occasion, and to emphasise Mr. Manns’s continuous

RICHTER CONCERTS. sa

FAITHFUL as ever, the lovers of orchestral music in § judicion 
general, and of Beethoven and Wagner in particular, § greatly 
thronged St. James’s Hall at the first of three autumnal § and in: 
Concerts, under the famous Viennese conductor, on Monday, § lobbies 
the 21st ult. For the sake of accuracy it should be said § those tc 
at once that the two masters named did not fill upthe § give ide 
entire programme, the most important feature being that § loverca 
remarkable work Tschaikowsky’s “‘ Symphonie Pathétique,” § departis 
which it has been well said has brought the Russian 
composer more fame after his death than he enjoyed during 
his life. The noble work was so magnificently played that 
the dictionary would have to be ransacked for superlatives THE 
in order to do justice to the interpretation. The Richter 
orchestra has never been heard to greater advantage, howeve 
Beethoven was represented by his glorious Overture was tha 
‘* Leonora” (No. 3), and Wagner by the Overture to “Die @ Young 
Meistersinger ” and the exquisite Good Friday music from § Siven at 
“ Parsifal.” These were all grandly rendered, as was @ Was ten 
Brahms’s genial ‘‘ Academic Festival’ Overture. Indeed, 
the whole Concert was one of the most artistically successful played. 
and thoroughly enjoyable that has ever been given in the mar

and con

THE season for these entertainments has commenced 
and soon they will be coming thick and fast. At present, 
however, we have only to notice two Recitals. The first 
was that of Madame Esperanza Kisch-Schorr, an English 
young lady notwithstanding her foreign name. This was 
given at the Steinway Hall on Tuesday, the 22nd ult., and 
was rendered interesting by the publication of a manifesto 
stting forth how Chopin’s “ Funeral” March should be 
played. When Rubinstein gave Recitals here he ignored 
the marks of expression given in most, if not all, editions, 
and commenced pianissimo, rose to a fortissimo, and then 
died away into silence. Madame Kisch-Schorr, fortified by 
the master’s opinion personally given to her, maintains 
that this reading is correct, the idea being that a funeral 
‘ortege approaches, stops at the cemetery, and, after the 
beautiful words of the Burial Service, slowly passes away. 
She acts with authority in one respect, for Chopin was 
negligent in putting on paper how his music should be 
played, and the reading just explained is certainly very 
expressive. Though pardonably nervous, Madame Kisch

orr displayed a pure and delicate touch and a refined 
style, not only in the B flat minor Sonata, but in Bach’s 
Chromatic Fantasia and Fugue, Beethoven’s Sonata in D 
minor (No. 2), and pieces by Moszkowski, Rubinstein, and 
er Composers

The Misses Sutro, who gave the first of three Recitals 
tuclusively of music for two pianofortes, at St. James’s

THE MUSICAL TIMES.—Novemser 1, 1895

in celebration of the victory of Henry V. The tune of 
“ Lillibulero,” a ballad which Dr. Percy declared 
‘‘contributed not a little towards the great Revolution of 
1688,” was, in the opinion of the lecturer, undoubtedly 
composed by Henry Purcell, a copy of the tune, 
signed by our national composer and dated 1689, being 
in the British Museum. The origin of the melody of 
“The British Grenadiers’? was unknown; apparently it 
had grown out of several old tunes. It might be dated 
1678, when the Grenadier company was added to the 
British Army. The earliest printed edition appeared in 
1700. The words of “ Auld Lang Syne”’ were certainly 
not written by Burns, as some had maintained. This was 
proved by a letter written by the poet. The melody to 
which the verses were now sung was composd by William 
Shield, and occurred in the overture to his opera ‘‘ Rosina,” 
published in 1783. It was true that some of the airs in 
this work were by other authors. Where this was the case 
Shield had acknowledged the source. ‘ The Last Rose of 
Summer” was an Irish tune associated, until Moore 
adapted it to his poem, with verses of a somewhat 
humorous character, entitled ‘‘ The Groves of Blarney.” It 
was commonly said that all old songs were written in the 
minor scale, but this was not true. Only about twenty- 
five per cent. of English, Scotch, and Irish tunes were 
really so; but the percentage was very much more in 
Scandinavian folk-tunes. Also referring to the origin of 
National Anthems the lecturer thought that English people 
scarcely appreciated the merits of John Bull’s tune. 
Beethoven had said: ‘‘I must show the English a little 
what a blessing they have in ‘God save the King.’”’ Its 
influence on the Continent was remarkable. It became the 
National air of Denmark and also that of Russia until 1830, 
when the hymn by Alexis Swoff was adopted, and Haydn 
was so impressed by it during his visits to England in 1791 
and 1794 that, on his return to Vienna, he became anxious 
to write a national hymn for Austria, and, securing a com- 
mission from the Court, produced, in 1797, the tune of ‘* God 
preserve the Emperor.” The tune of the American ‘Star 
spangled Banner’’ was originally written by J. Stafford 
Smith, and entitled ‘To Anacreon in heaven,’ composed 
for the Anacreontic Society, which held its meetings at the 
Crown and Anchor Tavern in the Strand towards the close 
of the last century

GRESHAM LECTURES

PROFESSOR BridGE began, on the 15th ult., his autumn 
series of Gresham Lectures, at the City of London School. 
The first two discourses were continuations of the history 
of the string quartet, a subject which has worthily occupied 
much of the Professor’s attention. Having reached the 
time of Beethoven, this composer’s first and last quartets 
were analysed and subsequently played. . Before referring 
in detail to these works, however, the lecturer made some 
interesting remarks, showing how greatly the development 
of instrumental forms was indebted to the sustained tone 
and extensive compass of the family of stringed instruments. 
The improvements effected in instruments and the increasing 
capabilities of executants exercised a far greater influence 
on composers than was commonly imagined. They acted 
and reacted on each other, and in a manner that made it 
almost impossible to adjust the respective share each took 
in the progress of the art. Until 1805 Beethoven was 
unable to write for the keyboard any note above F in alt. 
Higher notes were obtainable from the strings, and violinists 
might remember with a sense of satisfaction that they were 
the successors of the men who helped to unharness Pegasus 
from the plough, and gave the genius of Beethoven a larger 
heaven in which to wing its flight. When the new sonata 
form was evolved the conclusion jumped to was that form 
was everything; and hence, in the middle of the last 
century, the mechanism came to be regarded as of primary 
importance, regardless of the thought. Beethoven began 
where Haydn finished. It had been said that if there had 
been no French Revolution there would have been no 
Beethoven. But this was misleading. The French 
Revolution did not make Beethoven possible, but both were 
manifestations of the struggle for liberty and naturalness

rendered by Mr. Wall, Miss L. Wright, Mr. A. Hobday, 
and Mr. T. Werge

BERLIN.—Signor Sonzogno’s season of Italian opera _

at the Theater unter den Linden, and afterwards at the 
Neues Theater here, has not proved very successful, 
One of the principal novelties produced—viz., Mascagni’s 
‘Silvano ”—attracted some popular attention, but is con- 
sidered by critics but a weak reproduction of ‘‘ Cavalleria 
Rusticana,” both as regards the score and the libretto, 
——A young Russian violinist, M. Alexander Petchnikoff, 
caused quite a sensation at a Concert given by him 
at the Bechstein Hall last month, he being equally admired 
as an interpreter of classical and modern music. The 
enthusiasm displayed by the audience is shared by the 
press, who predict a most brilliant future for the young 
virtuoso. M. Petchnikoff, who is the son ofa humble soldier, 
and only twenty years of age, plays on a magnificent Stradi- 
varius, the gift of the Emperor of Russia.——Herr Arthur 
Nikisch conducted the first Philharmonic Concert of the 
season last month with much success, Beethoven’s “ Leo- 
nora” Overture (No. 3), Tschaikowsky’s Fifth Symphony, 
and the Overture to ‘‘ Tannhauser” being the principal 
pieces, while Josef Hofmann was the much-applauded 
pianist.——After being closed for four months the Royal 
Opera House was re-opened on the 23rd ult., when an 
excellent performance of Beethoven’s “ Fidelio ” was given 
in the presence of the Emperor and Empress. During the 
interim many improvements have been effected, not the 
least of which is the enlargement of the orchestra, which is 
now capable of holding 100 executants, It has also been 
lowered and provided with a double floor with a view to 
increased resonance. The new organ by Sauer will doubt- 
less often add to the enjoyment of the audiences and 
the increased facilities of ingress and egress will also 
add to their comfort. Both the theatre and the large 
concert-hall have profited by the brushes of the cleaner 
and painter

BrussELs.—Dr. Hans Richter will conduct one of the 
‘Concerts Populaires ’ during the present season, in the 
course of which the pianist, Signor Ferruccio Busoni, and 
Herr Willy Burmester will appear amongst other solo per- 
formers. Herr Edgar Tinel’s new oratorio, ‘ Godeleva,” 
will also be produced, for the first time, at one of these 
Concerts. —M. Franz Servais has completed the score of 
a grand opera, entitled ‘‘ L’Appollonide,” portions of which 
have already been performed with considerable success in 
Belgian concert-rooms. The work is to be first brought 
out during the present season at the Cassel Hof-Theater, 
under Herr Mottl’s direction

Moscow.—Vasya Pakelmann, a boy nine years of age, 
has just caused a sensation, on seeking admittance to the 
Conservatoire, by his marvellous violin playing. He is the 
son of a railway guard, entirely self-taught, and his course 
of regular instruction at the Conservatoire is now being 
paid for hy some benevolent amateurs in this town

Municu.— Among the more important new operatic 
works to be produced at the Hof-Theater during the present 
season are ‘‘Guntram,” by Richard Strauss; ‘* Kunihild,” 
by Cyrill Kistler; and Kienzl’s ‘‘ Der Evangelimann.” 
Much interest also attaches to the proposed re-mounting of 
the two “ Iphigenies ” of Gluck.—— The concert season ot 
the Musical Academy here commences on the Ist inst., 
when important excerpts will be given from “ Parsifal,’ 
under the direction of Herr Fischer. During the winter 
Beethoven’s nine Symphonies will be performed, under 
Herr Richard Strauss’s baton, the programmes also includ- 
ing an important new orchestral composition by the latter, 
entitled ‘‘ Till Eugenspiegel

PrRAGUE.—The 300th performance of Smetana’s ‘“ The 
Bartered Bride” has just been recorded at the National 
Theatre, this being the highest number of representations 
as yet achieved by any opera at this Institution. The 
work was first produced in 1886.——At the German 
Theatre, Dr. Kienzl’s new opera, ‘‘ Der Evangelimann,” was 
given for the first time here last month, under the direction 
of Herr Schalck, and in the presence of the composer, when 
it met with unqualified success

fees for which are one or two guineas, accordingly 
4% they meet once or twice a week. Professor Niecks

Sto deliver (1) twenty lectures on the History of Music 
(especially of the last two centuries) ; (2) twenty lectures 
® Musical Analysis (works of Beethoven, Schubert

in, and Wagner); (3) forty lectures on Harmony

Wuy two such trivialities as Haydn’s ‘“ Military” 
Symphony and Mozart’s ‘Seraglio’’ Overture were 
included in the first Concert of the Philharmonic Society, 
given on the 8th ult., is past understanding; and to employ 
to such an end the extremely fine band which Sir Charles 
Hallé has re-united seems little else than sheer waste of 
power. More acceptable were Schubert’s “‘Italian’’ and 
Dvorak’s “In der Natur’’ Overtures, the latter composition 
proving an extremely welcome novelty, In the perform- 
ance of Grieg’s Pianoforte Concerto in A minor Mr. 
Leonard Borwick found a congenial task, and the orchestra 
fairly divided the honours with the solo player. Miss 
Macintyre was the vocalist. At the second Concert, 
on the 22nd ult., two familiar overtures—Cherubini’s 
‘‘ Lodoiska’? and Smetana’s ‘‘ Lustspiel’’—were played, 
the more interesting numbers being an entr’acte by 
Kretschmer, a Haydn-Brahms set of Variations, and a 
Suite of Raff's, entitled ‘‘La fée d’amour.’’ Miss Berry 
was engaged as vocalist, and Sefior Sarasate as solo violin, 
his chief contribution being Mendelssohn’s Concerto, with 
his treatment of which everyone is familiar

The first Smoking Concert of the Liverpool Orchestral 
Society took place, under Mr. Rodewald, on the roth ult., 
Beethoven’s ‘ Pastoral”? Symphony and German’s new 
‘Gipsy Suite” being most happily laid under contribution. 
On the 29th the same excellent conductor was to direct the 
open rehearsal of the Societa Armonica, in the absence of 
its regular chief, and the venerable organisation is to be 
specially congratulated upon the process of rejuvenation 
which has been achieved in this, its forty-eighth year of 
existence. The Sunday Society gave its first two Orchestral 
Concerts of this, its tenth season, on the 13th and 2oth ult., 
in St. George’s Hall, under Mr. W. I. Argent, the band 
having been permanently enlarged to seventy performers. 
A week later, on the 27th ult., the clever Sisters Eissler 
played for the same Society

Such performances as those of the three Societies last- 
named emphasise what has been before stated herein— 
namely, that Liverpool must be looked upon as more 
orchestral than choral in its tastes. Nevertheless, it is 
well to note that the recently re-galvanised Musical Society, 
of which Mr. D. O. Parry is conductor, exhibits even greater 
signs of vitality than last season. The programme for that 
just inaugurated is to consist of Mendelssohn’s “ Elijah,” 
Handel’s ‘‘ Messiah,’? and Gounod’s ‘‘ Redemption.” So 
far, no other socigty of importance East of the Mersey has 
announced its existence; but on the Cheshire shore the 
Rock Ferry amateurs are working at Schumann’s “ Pil- 
gtimage of the rose” and Mendelssohn’s ‘‘O come, let us 
sing,” under Mr. Pemberton; while the Liscard Musical 
Society, under Mr. Argent, has selected Handel’s “ Judas” 
and Gounod’s ‘“‘ Redemption ” for its next Concerts

MUSIC IN MANCHESTER. 
(FROM OUR OWN CORRESPONDENT

At the Schiller Anstalt, the sweet sounds heralding the 
approach of winter have first been heard on the 7th ult., at 
a charming little Concert held under the direction of Herr 
Carl Fuchs, when Mr. Leonard Borwick played delightfully 
Schumann’s Toccata with other pieces, and, in conjunction 
with the director, Beethoven’s Sonata in A and Chopin’s 
Polonaise in C, both for clavier and violoncello; Mr. 
Plunket Greene contributing songs by Schubert and 
Rubinstein, together with some of the old Irish and 
German Lieder of which he is a special student. But Dr, 
Watson quickly followed the lead of our enthusiastic 
friends with the first of the four meetings of his Vocal 
Society, now enlarged to a choir of about fifty voices, well 
balanced and singing with evident enjoyment of the music 
and with the appreciation and finish gained during its 
twenty-eight past seasons. Although the performances of 
this Society appeal chiefly to lovers of choral music, yet 
the Concerts afford opportunities for the encouragement 
of local talent; and in the Sullivan “Te Deum” Miss 
Herod displayed a clear soprano voice of quite sufficient 
power and increasing steadiness. That the whole work 
fell rather coldly upon the audience was not in the slightest 
degree the fault of the soloists, of the choir, or of the 
Conductor

Mr. Pyne, in addition to the customary Saturday evening 
organ performances at the Town Hall, gives a conditional 
offer of Recitals on Tuesday evenings, which ought to 
attract many people who generally have some other 
engagement on the last night of the week

THE musical season in Nottingham opens with much 
Promise, but not without disappointments that call for

regret. As yet there is no sign from the committee of the 
Drawing-room Concerts, who have catered so earnestly 
and successfully in the past for the lovers of chamber 
music, nor from Mr. William Allen, whose self-denying 
efforts in the same line seem to be at an end. The Phil- 
harmonic Choir, though not disbanded, is not announcing 
any Concerts, and many will miss its delightful interpre- 
tations of unaccompanied choral music. Herr Richter 
brought his famed orchestra on the 22nd ult., and gave a 
strong programme of Wagner’s music and Beethoven’s 
Eighth Symphony, Mr. Edward Lloyd contributing vocal 
selections from ‘‘ Lohengrin” and ‘ Die Meistersinger

The season actually commenced on the 14th ult., with a 
Pianoforte Recital by M. de Greef, the Belgian virtuoso, 
at the Albert Hall

were successful in one or two instances in frustrating the 
attempts of persons who are doing a large illicit trade in 
Canada in shipping reprints of valuable music to the 
United States, thereby infringing the Copyright Act. 
Six large sacks which had been sent by express from 
Toronto to Montreal were mailed at the latter city with a 
view to eluding the customs officials. ‘* The mail was placed 
in six bags by local officials, sealed, and mailed to the 
United States officer at St. Alban’s, Vermont. Here the 
mail was found to be contraband, and was seized. The 
contents were valued at 5,oc0 dollars. Since the action of 
the Montreal authorities in the matter, the shippers have 
been sending these reprints of music to other points in 
Canadian territory by express from Toronto. On arrival 
the matter is mailed. So as to avoid the Montreal postal 
authorities, the shippers have been working on Sherbrooke 
Post Office and other important points near the lines. The 
Canadian authorities have aided the United States officials 
considerably in unearthing this means of defrauding the 
United States and breaking the Copyright Act of the 
country. It is estimated that many thousand dollars’ 
worth of reprints of valuable American music have found 
their way into the United States from Canada

Mr. Gorpdon TANNER, a very promising young English 
violinist, gave a successful Chamber Concert, on the Ist 
ult., at Princes’ Hall. Mr. Tanner played Sefor Sarasate’s 
Fantasia on Gounod’s “ Faust,” and other pieces by Godard, 
Zarzycki, and Bazzini with admirable expression and due 
brilliancy, and also gave an excellent rendering of the 
violin part in Grieg’s beautiful Sonata in F (Op. 8). The 
pianoforte part in this work was played with some skill by 
M. Sigismond de Seyfried, a Polish pianist, who was 
announced as making his first appearance in London on 
this occasion, but who in subsequent solos for his instrument 
failed to make much impression. The services of Mr. 
Adolph Schmid, an able violoncellist, were employed in 
the interpretations of Beethoven’s Pianoforte Trio in C 
minor (Op. 1, No. 3) and Gade’s Trio in A minor (Op. 29). 
Mr. Spencer Lorraine was the accompanist

THE committee of the Bayreuth Festspiele have now 
definitely fixed the dates of next year’s representations, 
which will mark the twentieth anniversary of their original 
institution. As was the case in 1876, the “ Nibelungen”’ 
Tetralogy will be given, with entirely new scenery and 
costumes, under the direction of Dr. Hans Richter. There 
will be five performances in all of the gigantic work, each 
taking place on four consecutive days—viz., the first between 
July 19 and 22; the second, July 26 to 29; the third, 
August 2 to 5; the fourth, August g to 12; and the final 
one, August 16 to 19. Among the artists who will take 
part are the brothers De Reszke, Miss Margaret Macintyre, 
and Miss Brema, other engagements being still pending. 
No representations of ‘ Parsifal’’ will take place

THE organ at Trinity College has, during the long 
vacation, been rebuilt by Messrs. Hill and Sons, and, on 
the roth ult., Mr. F. G. Mitford-Ogbourne gave a Recital 
of original organ compositions of a varied and interesting 
character

MapaME pD’ALBERT CarrENo will visit England this 
month, and Mons. Siloti, the Russian pianist, will be in 
London in December, when he will play at the Beethoven 
(Symphony) Concert

Mrs. HAMILTON Rosinson has been appointed Pro- 
fessor of Singing in King’s College (London), Ladies’ 
Department, Kensington Square

Sir,—A musical Festival, in the course of which 
Johannes Brahms accompanied on the pianoforte some 
of his own four-part songs, had his health drunk by enthu- 
siastic German musicians, and made a speech in returning 
thanks for it, is assuredly a great rarity. This, however, 
and much else of high musical interest, has just happened 
in connection with the first musical Festival of the 
Duchy of Saxe-Meiningen, held at Meiningen from 
September 27 to 29

The programme—headed as it was by three names only, 
‘“‘Bach, Beethoven, Brahms,” and carrying out, as it did, 
the exclusive choice thus implied with absolute literalness 
—seemed designed in some sort to instal Brahms beside 
Bach and Beethoven, and perhaps even, by implication, to 
bar any claim to a like honour which might be put forward 
on behalf of Wagner. While the music chosen for per- 
formance was attractive on this account as well as for its 
intrinsic merits, the Meiningen Committee were able to 
announce an exceptionally promising body of executants, 
and even some instrumentalists of leading European 
reputation, such as Dr. Joachim, Herr Hausmann (the 
violoncellist), and Herr Miuhlfeld (the clarinet player), 
The orchestra, numbering ninety performers, was to consist 
mainly of that of the Meiningen Court Theatre strengthened 
by contingents from Weimar, Hanover, Coburg, &c. _ The 
chorus, of 346 voices, were to come from choral societies 
in or about Meiningen—an excellent omen, considering the 
repute of the Thuringian people for musical talent, which 
goes back even beyond the time at which Bach himselt 
sprang from their stock. f

I went to Meiningen with high expectations, and 
found them so much more than realized that I hope you 
will allow me to try to give your readers some idea of what 
were the most salient features in three days of very note- 
worthy performance. I offer my experiences with the less 
hesitation because I was unable to learn that the Festival 
was attended by musical critics accredited by English 
journals, whose presence would have rendered my inter- 
vention uncalled for

In the course of two Chamber Concerts, Dr. Joachim and 
Herren Halir, Wirth, and Hausmann played three Quartets 
of Beethoven (Op. 59, 95, and 180). The sameexecutants, 
with Herr Bram Eldering, played Brahms’s second String 
Quintet, and, with Herr Miblfeld, his Clarinet Quintet

Op. 115). Herr Mihlfeld also played, with Herr Eugene 
d’Albert, Brahms’s Sonata for “.rinet and pianoforte 
(Op. 130). It appeared to meth: .e highest standard ot

artistic excellence, attainable only er most exceptionally 
favourable conditions, including . impulse of a common 
devotion, was reached in these performances, The 
compositions for strings, in particular, presented an 
exquisite unity of spirit in the bond of music such as I 
have never heard equalled. é

An Orchestral Concert given on the second night was 
remarkable for a fine performance, by Herr Eugene d’Albett, 
of Beethoven’s Pianoforte Concerto in E flat. The 
orchestra did their part in this work with extraordinary 
perfection, and, more particularly in the Allegro and final 
Rondo, developed an absolute identity of dashing rhythm 
and such youthful freshness and fire as completely carried 
one away. Finally, when it came to the orchestra’s turn 
to place on Brahms’s brow their own special garland in 
the shape of a performance of his first Symphony, they 
achieved an overwhelming success. Independently of com- 
plete technical mastery and a superb body of tone, there 
was a unanimity of feeling and a glow and passion at 
work which proved utterly irresistible, and set the audience 
cheering in uproarious delight. ; 7

For the chorus were set down Bach’s “ Passion” Music 
(St. Matthew) and his double chorus ‘‘ Nun ist das Heil,” 
Beethoven’s Mass in D, and Brahms’s ‘“ Triumphlied. 
They sang these works with unfailing accuracy of ine 
ation, perfect steadiness, excellent light and shade, an

Notable

_

admirable precision in taking up difficult leads and tackling 
awkward intervals without the least sign of wavering or 
hesitation. I was not unprepared for this in a work so 
completely a household word in Germany as Bach’s great 
“ Passion” Music, but must confess to having been fairly 
taken by surprise when I found the intractable choral 
difficulties of Beethoven’s Mass in D surmounted with 
equal completeness and heard the soprani attacking and 
sustaining the many ruthlessly long-drawn high A’s and B 
flats (A flat and A at English pitch) with a superb energy 
which held out unflaggingly to the end of the work. 
These results had, of course, not been attained without 
correspondingly assiduous preparation. The chorus began 
their rehearsals, I was told, a whole year beforehand, 
meeting regularly once a week, and sometimes, from over- 
flowing zeal, on Sunday as well. They were trained in six 
separate centres, under different instructors ; but Herr Fritz 
Steinbach, Capellmeister to the Duke of Saxe-Meiningen, 
who conducted the whole Festival with extraordinary 
energy and efficiency, had visited all the outlying sections 
in order to bring the requisite unity of conception into 
the work of the entire chorus

At a well-attended and most cheery subscription supper, 
which took place after the last Concert, the toast imme- 
diately following the health of the Duke was that 
of the Conductor. Its proposer narrated how “our 
Steinbach” had originated the idea of holding the 
Festival, stirred up everybody else, written innumer- 
able letters, and, in fact, made the Festival the 
success it had been. Herr Steinbach, in replying, 
modestly assigned to his collaborators large shares in the 
final result, and then, in touchingly affectionate terms, 
proposed the health of ‘our Master, Brahms,” whereupon 
many, especially of the younger convives, left their seats and 
crowded round the great composer in order to clink glasses 
with him. What he said in reply to the toast consisted, 
as far as I was able to hear and understand him, mainly of 
ingeniously constructed reasons designed to escape having 
tomake a speech at all. He did not, however, sit down 
without having spoken hearty words of thanks and recogni- 
tion, nor without having amused his audience by saying 
that, as Providence had not brought him into the world 
with so common a patronymic as Schmidt or Miiller, he 
could at least say without vanity that he was the first 
composer of his name

October 4, 1895

We had heard, from an English artist present at 
Meiningen, of the excellent performances given there in 
connection with the recent Festival, and on that account 
are all the more glad to publish the letter with which Pro- 
fessor Sedley Taylor has favoured us. It is said that our 
correspondent, after hearing Beethoven’s Mass in the 
German town, travelled post haste to Leeds to hear it a 
second time. A comparison of the two renderings from 
the pen of so competent a critic would be extremely 
interesting and valuable. We take the opportunity, while 
thanking Professor Taylor, to suggest that other travellers 
Into regions not visited by English professional critics 
might do far worse than send us their observations on 
notable musical doings.—Ep., M.T

Mr. Epwarp Excgar has accepted the invitation of the 
Committee of the North Staffordshire Musical Festival to 
write a new cantata for production at Hanley in October of 
Next year

KEIGHLEY.—The fourth annual West Riding Musical 
Competition, associated with the name of the late Mr. W. 
H. Summerscales, was held at the Keighley Institute on 
the 12th ult., when thirty-five competitors entered their 
names for the three medals and money prizes. Some 
excellent playing and singing were listened to by a 
numerous audience of friends and musical enthusiasts, and 
the awards, which appeared to give general satisfaction, 
were presented by Mr. Leolin Brigg. Mr. R. H. Moore 
fulfilled the duties of accompanist in an admirable manner

MELBOURNE, AUSTRALIA.—A Concert was given in 
Chalmer’s Church on August 29, when the choir was 
assisted by Miss A. Millar, Miss F. Mitchell, Mr. F. Holt, 
and Mr. J. Brierley, whose singing afforded manifest satis- 
faction. A string quartet, consisting of Messrs. A. Zelman, 
jun., Ryder, F. Tate, and C. Harrison, played Beethoven’s 
First Quartet in F (Op. 18), and organ solos were contributed 
by Mr. W. E. Nott

MontreEAL, Canapa.—Mr. E. A. Hilton, Organist and 
Choirmaster of Dominion Square Methodist Church, 
resumed his monthly Organ Recitals on September 28, 
when he was assisted by Miss Perego (organist), Messrs. 
W. E. Wilson, J. H. Smith, Audas, and Charles Kelly 
(vocalists). Among the organ pieces in a well selected 
programme were the Prelude to ‘“‘ Lohengrin”? and Men- 
delssohn’s First Organ Sonata

Hamilton Clarke 4d

Ly ” 4 
"Beethoven 3d

305. Moonlight ee oe eo oe

