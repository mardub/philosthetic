THE MUSICAL TIMES

Hymn for Whitsuntide. Woelfl’s Rondo, inC a 7 206 
na few days will be ready, the Hymn 12 Dussek’s Air with Variations, in i BS. ot Fare 
“Cc te, Cheat” uct Yomunlc, eomecned b 13, Sterkel’s Andante, in E flat 
a ST y INCE N° T NOVELLO, P y 14 Haydn’s Rondo, from Op. 17, in G. 
for Four Voi tag m Satine dha Cuan /1§ Beethoven’s Rondo, in C. , die we 
‘or Four Voices, with Acompanime gan. |16 Mozart's Three Waltzes a

London; J. A. Novello, A Thematique Catalogue of the above may be had

To be had (with all the musical publications of Dr. Mainzer) | 
of J. A. Novello, 69, Dean-street, Soho; or of Simpkin and | 
Marshall, Stationers’ Hall Court, London

USEFUL PRACTICE, Edited by R. Barnett. | 
No. ‘ ae : & 
1 Steibelt’s Sonata, in A. Op. 50 2 6) Instructions. 
2 Beethoven’s Sonata, in G. Op. 49 2 6} CHURCH PSALMODY. A Manual of the most sterling 
3 Haydn’s Rondo, inC. .. 2 6|Psalm and Hymn Tunes, chiefly in the Old Church Style. 
4 J.N. Hummel’s Romance and Rondo, i in 1G. 2 6) Selected, harmonized, and partly composed by Cuarzes STEG- 
5 Steibelt’s Sonata, in G, 2 6/GALL, of the Royal Academy of Music, and Organist of Christ 
6 Clementi’s Sonata, in F. Op. : 2 6/Chapel, St. Marylebone. This selection is more particularly 
7  Dussek’s Rondo (Rule Britwania), i in o. 2 6J/adapted to the Rev. W. J. Hall’s Selection of Psalms and 
8 Kalkbrenner’s Rondo, inC, .. 2 6|Hymns, now almost universally used in the Metropolitan 
9 Dussek’s Rondo, in E flat 2 6} Churches. 
10 ©Clementi’s Rondo, in C. Op, 21 2367 London: Coventry, 71, Dean-street, Soho

i ae ee ee

HAYDN’S MASS, each Number singly, Quarto size. 
i. 48. 6.—ii. 48.— ili. 38. 6d.—iv. 4s.—v. 5s.—vi. 4s. 6d.— 
vil. 38.—vili, 28.—ix. 4s.—x. 33.—xi. 1s. 6d.—xii. 38. 6d.— 
xiii. 38.—xiv. 38.—xv. 4s.—xvi. 55

BEETHOVEN in C. Folio, 8s. 6d.; the same Quarto, §s

BEETHOVEN in D, Folio, 14s

HUMMEL in E flat, 8s) HUMMEL in B flat, 7s

On the Ist of June will be commenced

THE THREE FAVORITE MASSES OF MOZART, HAYDN, AND BEETHOVEN. The Three 
Masses will form together a Volume similar to the ‘Judas Maccabeus,” or the “ Messiah ;’’ and, on their 
completion, will be bound, to correspond with the series, in parchment cloth.—In addition to the original Latin 
words, there is an English adaptation to each Mass

CHEAP ORATORIOS—-WORKS COMPLETED

