College open 9.30 a.m. 9.30 p.m. ' staff consist go‘professor . 
 isa 1 ull ¢ ere lady ’ choir , ORC HESTRA OPERA CLASS , DRAMATIC 
 class , string quartet class , class training conductor 

 recent performance include Coleridge - Taylor " Hiawatha Wedding - Feast , " Concertos 
 Beethoven , Mendelssohn , Grieg , Paganini 

 recital Colleg » Organ arrange 

 636 MUSICAL TIMES.—Octoser 1 , 1908 

 i'he present organist master choristers| consider good english tenor vocalist 
 Dr. J. Kendrick Pyne , hold office| day , John Braham . sing tenor 
 1875 biographical sketch|solo Beethoven ' Mount Olives ' 
 subjoin . Dr. Pyne shortly relinquish ! occasion performance England , 

 cathedral duty . succeed 
 Mr. Sydney H. Nichoison , act - organist 
 Carlisle Cathedral , enter duty 
 beginning year 

 distinguished organist , } shall composer ? 
 man wide culture patriotic citizen , | happily lag noble 
 Dr. Pyne win regard respect — } fight . music - lover look forward keen 
 far affection ? — people | interest production Elgar Symphony 
 Manchester , inscribe |in flat , soon produce , Dr. Richter , 
 heart prove himself|at Manchester . rare composer wait 
 benefactor city . ' | till fiftieth year turn attention 

 great art form . Brahms arrive late 
 |that point , Pianoforte concerto 
 |d minor , date early manhood , 
 SYMPHONY doom ? | originally mean Symphony . 
 | change Concerto young master 
 Symphony is| confess insufficiently verse 
 loom early death , champion | art instrumentation justice /ina/ 
 f Symphonic Poem year past } long buzz head , 
 try believe . , like | doubtless like glorious 
 good thing appertain } Fiva / e eventually use Symphony , 
 Art , originate Germany , bring to| ending , safely assume , 
 present remarkable development Richard | suggest apotheosis Schumann genius , 
 Strauss , plenty german musician | opening .4//egro Concerto , know 
 prefer grand old form / , deal , doubt , terrible tragedy 
 great master Haydn express deep | master year . 
 houghts , pour heart deathless } british composer 
 strain . continually read new ] symphony preparation , mention 
 symphony produce abroad ; are|of Dr. Walford Davies Mr. W. H. Bell ; 
 epoch - work , fact little is| work welcome effort singularly 
 breught forth worthy join | talented man . time ago Dr. F. H. Cowen 
 Brahms great work e minor , the| repute engage ' . 7 
 link chain masterpiece forge the|1d minor , ' surprised 
 classic , effort palpable| Sir Charles Stanford busy work 
 proof symphonic form lose | follow . 7 , ' Watts ' symphony . 
 fascination earnest musician . | conclusion , Symphony little 
 new symphony shortly performed,| fear later rival Symphonic Poem . 
 recently bring hearing , by| composer ' modern , ' thoroughly 
 Gustav Mahler , Xaver Scharwenka , August Bungert , | imbue z-/¢geis¢ possible ; 
 Ferdinand Hummel Felix Woyrsch . | late technical device finger ’ 
 — ' Passion music ' the|}end equipped heavy task 
 emulate fine achievement Richard 
 responsible Symphony c minor , produce | Strauss : 
 Altona , near Hamburg . work leading| worth listen , , 
 Hamburg paper hail seriousness ' the| great enduring form Symphony , express 
 long - expect Brahms . 5 , ' recall } completely , convincingly , 
 Hans von Biilow dictum great Johannes ’ | produce deep possible impression 
 Symphony , C minor , regard | audience effectually , zealous 
 Beethoven . 10 . Woyrsch certainly ] exponent symphonic poem . 
 composer type symphonic writer } ' quote Sir Hubert Parry : ' net likely 
 , wherefore hope | able follow Brahms 
 genuine melodic inspiration case go|severe uncompromising method : 
 hand hand profound contrapuntal | elastic 
 knowledge mastery form he|the old principle 
 unquestionably possess . | depart genuine type abstract 
 slavonic composer idle . gospodin| instrumental music ; room 
 Mili Balakirev publish Symphony individual expression good work 
 1 ) minor , . 2 , announce | } , hardly hope 
 performance Philadelphia ( U.S.A. ) Symphony | great composer future surpass 
 Orchestra Herr Pohlig ; Pan Josef Suk |symphonic triumph past , 

 work movement , bear fine , suggestive | field composition 

 enjoy musical treat , bring mean . 
 Handel Dettinven Te Deum — Purcell Jubilate D — 
 Hayes anthem , o worship Lord — Mendelssohn 

 beautiful anthem , ‘ hart pant delightful 
 old prece response Tallis , form musical 
 portion service . day public 
 opportunity hear follow work 
 entire Mendelssohn £ /ijah , ' Spring ' 
 Haydn season , Haydn creation , Beethoven Engedi 

 selection Crotch /a / estine , fresh 
 Messiah Handel . orchestra number 350 
 select instrumental vocal performer . 

 Thursday performance resume 

 Sir Charles 
 produce 
 Beethoven Violin 

 cathedral , morning 
 Stanford Stabat Mater ( Op . 
 Leeds musical festival 1907 
 oncerto ( soloist , Mischa Elman ) ; Dr. Walford 
 Das es ' everyman ' : evening Bach 
 fagnificat ; Mr. Ivor Atkins ' hymn faith ' 
 ‘ produc e Worcester festival , 1905 
 Mendelssohn ' hymn praise . ' detailed criticism 
 f work hardly necessary . place 

 performance rec ord , state 
 impressiveness Stabat Mater ( conduct 
 composer ) , ' Everyman , ' 
 deepen hear appropriate 
 surrounding stately cathedral afford . 
 Mischa Elman triumph performance 
 Beethoven concerto need state , 
 cadenza introduce sound trivial 
 athedral 

 delightful , evening , listen 
 strain dear old Bach noble 
 las charming flute 
 ir ' esurientes implevit bonis , ' contrast 
 ccompaniment jubilation 
 trumpeting fine work 
 great Cantor . inclusion Mr. Atkins ' hymn 

 th 

 Beethoven round ' ' coplanse " ( Beethoven 

 c¢ ducte , stone - deaf . ) ' addition 
 word ' ' word ' turn ' 
 sentence clear . help regret lack 
 patriotic feeling article ' Military Band . ' 

 remarkable activity technical attainment 
 Choral Unions form Evening Schools , 
 London County Council , evidence work 
 rehearse performance Spring . followin 
 list speak 

 BATTERSEA , CLAPHAM WANDSWORTH Choral 
 Union ( conductor : Mr. George Lane ) . Ruins Athens 
 ( Beethoven ) , wreck Hesperus ( / acCunn 

 East Lonpon CHORAL UNION ( conductor : Mr , G 
 Day - Winter ) . Flag England ( Arde 

 MUSIC LIVERPOOL . 
 ( ¢ CORRES NDENT 

 forecast come season operation place 
 honour Philharmonic Society , whict 
 announce series performance , commence 
 October 13 orchestral concert . choral 
 work perform , english composer represent 
 Cowen ' sleep beauty ' Brewer ' Sir Patrick Spens , ' 
 Strauss ' Wanderer Sturmlied , ' Beethovens 
 ' Fidelio ' ' Elijah ' 

 Orchestral Society arrange attractive scheme 
 line progressive policy suc ! 
 distinction concert . Mr. Granville Bantoc 

 ow 

 ASTLI 
 ORRI 
 Union announce ci incert 
 Handel ' Acis Galatea , ° C liffe * ' Ode 
 North - east wind , ' Rutland Boughton Folk - song 
 variation October ; February 4 , Verdi 
 ( time ) , February 24 , 
 ye Lord ’ Beethoven Choral 
 different orchestra engage , 
 Scottish , Halk 

 

 Postal Telegraph Choral Society , direction 
 Mr. E. L. Bainton , concert devote mainly 
 unaccompanied music . addition madrigal 
 16th 17th century , work Schubert , Schumann , 
 Cornelius , Brahms perform , modern 
 english music represent work Parry , 
 Granville Bantock , J. B. McEwen , G. von Holst , 
 W. H. Bell , Havergal Brian , Walford Davies , Joseph 
 Holbrooke Rutland Boughton . truly imposing list 

 Armstrong College Choral Society rehearse 
 Beethoven Mass C , Vaughan Williams ' 
 wn region , northumbrian folk - song 

 rateur Vocal Society intend perform Barnett 
 ' Ancien Jarrow Philharmonic Society 
 ve select Parry ' Juditl quasi novelty 

 judge prospectus issue , musical 
 season promise . city Sacred Harmonic 
 Society announce ' Samson Delilah ' ( Saint - Saéns ) , 
 ' hymn praise ' ( Mendelssohn ) , Bralms ' Requiem * 
 ' song destiny , ' addition Elgar ' dream 
 Gerontir society orchestral 

 concert , principal item Beethoven 

 eighth Symphony Schubert C , Delius 
 ' Brigg Fair , ' Elgar ' south ' overture , Jarnefelt 
 ' Preludium , ' intermezzo Sibelius 

 festival concert , festival church - service , dedication 

 University building , X&c . — include   Liszt 
 symphonic poem ' Festklange , ' originally 
 produce Jena February 3 , 1861 ; Beethoven choral 
 symphony , word University famous 
 professor , Schiller ; Handel ' Zadok , priest ' ; 
 Sonata double wind - quartet , Giovanni Gabrieli , 
 16th century venetian composer . 
 irrepressible Max Reger , uncanny fecundity 
 intellectual marvel present day , 
 include festival scheme . novelty 
 Germany strong present - day representative abstract 
 music setting 1ooth Psalm chorus 
 rchestra — , , movement 
 sing — ' Weihegesang ' ( consecration - song ) 
 contralto solo , chorus man voice , wind instrument , 
 word Professor Otto Liebmann . Professor Reger 
 create doctor University , honour , 
 musician , enjoy Robert 
 amann Hans von Biilow 

 new 

 purpose 

 F. W sonata organ S. de Lange , ! /KINS , J. E. — * ' tear , idle tear . " - song 
 " fine work , pany rank ( . * ee . Novello - Song Book . ) 3d . 
 wor berger Merk » COCK , GILBERT A. — ' ' beauty ' 
 x 3 \ annot study Stainer | + Gaagntess . : - song A.T.T.1 ( . 455 . zh 
 Organ Primer ( Novello ) , , addition treatise , | u7 . ) 2d . 
 ive ripti f church organ . ACH , J. S.—So condemnation . 
 i. o te sonata Beethoven , man ' Jesu , priceless treasure . ' ( . s09 . Novell 
 ur ISS ; 3 ' » & lar ascertal Octavo Choruses . } 3d . 
 publish ; enquiry . ANTOCk , GRANVILLE — ' wake serpent 
 . , - song . ( . 788 . Zhe Alusica 1 
 e. | .—The send t opening : é 
 ar ee t e . D , lair , HUGH Benedictus e flat . ( N 737 
 writter Novello Parish Choir Book . ) 3d . 
 m \ Bayt al Wi probat y | gg R , A. HERBERT — * Auf Wiedersehen . " | 
 year en Gecided Violin Pianoforte . 2s . 
 d know * Guild hit , JOIIN—3o0 caprice . Violin . 5 
 d e gull G F 
 w. vv reg " m YLERIDGE - TAYLOR , S. — * * Pixies . " par 
 c er song 5s.s . A. . 381 . Novello Trios , f 
 Female Voices . ) 3d . 
 — — ' * encincture twine leave . " - song f 
 content .A. ( . 352 . Novello trio , & c. , Fen 
 \ oice . } 3d . 
 — ' " * lambkin . " - song 
 . 383 . Novello trio , & c. , Female Voices 
 * sea - drif Rhapsody 8 voice A.A. 
 . 1076 . Novello - Song Book . ) 6d . 
 } ' , Sanctus , D. 3d . 
 ( p . 
 E LGAR , E. Follow Colours . arra 
 + Military Band ¢ cepesle A. STRETTON . 3s 
 " ~OUN ! " ad [ . ' Fat selection Con 
 ar edit arrange 
 hye " HUGH t Song . . 2 , 
 ntralto Baritone 2 
 kK ING , OLIV ER — * hark ! hark , soul Ant 
 " . ( . 141 . Novello Short Anthems . ) 14d 
 ENNARD , LADY BARRETT — ' dold 
 ~ Song Baritone > 
 [ ISH MAN , G \ Cricket Song . cal Mar wit 
 — < chorus N Chester Serie nis 
 | LOYD , C. H Eton Memorial March . Wor 
 A. C. AINGI * . Eton School Sor 

 

