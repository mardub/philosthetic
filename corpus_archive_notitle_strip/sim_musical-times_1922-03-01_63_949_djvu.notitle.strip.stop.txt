words 

 allusions previous musical material ideas 
 associated achieve 
 compared symphonic development 
 Beethoven , nearly , symbolic 
 development Bach . Dr. Fellowes page 216 

 

 fewer . pieces 
 freshness freedom expression remind 
 , strangely , personal little 
 | pieces Giles Farnaby — centuries 
 old — found Fitzwilliam 
 Virginal Book . Children pieces rule 
 figure recital programmes , melodic 
 beauty delicacy workmanship little 
 pieces Barték means unworthy 
 fine playing ; privileged 
 hear composer play 
 memorable experience depths real 
 master reveal simplest - music . 
 Pianists search novelties provide 
 technical problems addition rich musical 
 interest turn magnificent 
 set tudes ( 1920 : Universal ) , 
 Elegies ( 1910 : Rozsnyai ) , Diérges 
 ( 1910 : Rézsavolgyi 

 , , String Quartets 
 ( composed 1908 1917 respectively ) 
 | Barték singular genius revealed clearly . 
 fine chamber music written 
 years , Barték appears 
 composer , working lines indicated 
 Beethoven Quartets , achieved 
 technical perfection expression 
 original ideas idiom hisown . 
 high praise ; fully merited 
 profound arresting works 
 studied scores — , better , heard 
 performed superb Waldbauer Quartet — 
 testify . certainly significant 
 quartets published death 
 Beethoven 

 supposed 
 said , somewhat back- 
 ward appreciation Barték genius , 
 countrymen . 
 contrary Budapest places 
 hear music — private gatherings . 
 years professional colleagues looked 
 askance , spoke madness 
 mentioned , pedantic 
 opposition extent given way , 
 political disturbances prevented Barték 
 obtaining public recognition deserves , 
 Hungary , countries struggle 
 independence continuing 
 recently ended , impossible 
 remain outside sphere politics . 
 old regime authorities State- 
 supported School Music Budapest regarded 
 Barték marked disfavour , despite 
 eflorts friend Dohnanyi behalf , 
 persistently refused official appointment 

 p ! — _ — _ — — — — foce s / s 
 ee 

 considerable talent — W. T. Walton — 
 fail benficial effect ; Barték isa 
 “ modern ’ originality owes sen- 
 sationalism , eccentricity , ‘ revolutionary ’ ideas , 
 depend recognition 
 postulation world great 
 masters past rigidly excluded . , 
 , singularly free influence 
 contemporary composers ; 
 acquaintance work 1922 
 observe simplicity texture , directness 
 expression , freedom conventional 
 forms formule — dqualities , 
 conspicuous alike work English 
 virginalists 16th century Beethoven 
 19th , critics persuade 
 introduced music Stravinsky — bear 
 mind Barték best 
 characteristic work fourteen years old . 
 England recognition master 
 effect musicians musical 
 public country . Hungary , unlike 
 Germany , proper respect England 

 
 2 
 Vv poco sfz 
 

 hardy perennials 

 , Beethoven String Quartets 
 laboriously constricted thoughts mould 
 
 cleverly scored orchestra 
 secret , present concealed beneath 
 wild growth technical impossibilities . 
 transcribing , object reproduce 
 original , equivalent 
 medium . Busoni greatly praised 
 wise man 
 Schweitzer ; repeatedly mistake 
 trying reproduce manner original 
 revealing spiritual aspect . 
 Knowing organ characterised use 
 4 - ft . 16 - ft . tone , doubled 
 passage - work beauty contra- 
 puntal pianoforte style swept away . 

 feel necessary transcribing fiddle sonata 

 duty transcribers bring 
 music homes . blessing 
 amateurs transcribing extensively 

 allow transcriptions Beethoven Symphonies 

 transcriptions Beethoven Pianoforte 
 Sonatas , Schumann Symphonic Studies , Liszt 
 Rhapsodies ? peop'e long play works , 
 able play written . 
 practice dead , 
 able play 

 gramophone pianola solves 
 question music home . want feel 
 music fingers , 
 play lawn tennis played 
 difficulties swept 
 away composers worse , ¢.¢. , 
 |the absurdly difficult water- logged passage 
 ‘ left hand Brahms Rhapsody B minor 

 ES — Marcu 1 

 theory prodigies develop great | 
 musicians , age showed 
 great musical aptitude . began studies 
 age , student Vienna 
 Conservatoire , soon gained distinction | 
 fellows violinist , pianist , composer . In| 
 1872 deputation students — | 
 Mottl Emil Paur — sent to| 
 greet Wagner , selected 
 violins play historic performance 
 Beethoven ninth Symphony laying 
 foundation - stone Bayreuth Festspielhaus . 
 joined Vienna Opera Orchestra , 
 went chorus master Opera 
 House Leipsic . 
 EARLY SUCCESS 

 opportunity conducting 
 Jannhaduser orchestra protested 
 young . arranged , , 
 play rehearsal 
 decide . rehearsal enthusiastically 
 cheered , foundation - stone great 
 career conductor laid 

 MEMORABLE PERFORMANCES 

 present forget electrical effect 
 London performance Tchaikovsky fifth 
 Symphony . specially remembers certain 
 clarinet passage slow movement , 
 new way , nearly subsequent 
 conductors adopted . performance 
 lives memory Elgar 
 Symphony , gave rise 
 vehement differences opinion . reading 
 Tchaikovsky Francesca da Rimini , 
 hand , thing opinions 
 possible . swept audiences feet . 
 supposed readings classics 
 arbitrary ultra - modern . good 
 musician keen sense true beauty 
 . , , vivacity little 
 disconcerting accustomed restrained 
 playing . Beethoven Seventh showed 
 best , especially movement . 
 said special characteristics 
 interpretation fact , 
 speak , arrived classics Romantics 

 place digress moment 
 point — interesting fact — best 
 orchestral interpreters North German classics 
 Teutons — Richter , like Nikisch 
 Hungarian , Mottl _ Austrian , 
 Weingartner born Dalmatia , Levi 
 born Rhineland Jewish parentage 

 access note time . } letter suggests interesting 
 remember rejecting usual | unprofitable lines thought . wrong 
 way binding quavers . / , artistic , 

 instructive discussion . things matter beneath notice . 
 conclusion , word magical art| contrary , little importance 
 accompanist . way } discover immensely popular airs 
 play Elena Gerhardt unforgettable . catch public fancy . 
 able probe secret . free confess 
 suffer constitutional inability tell 
 . [ hardly know differences 
 GRESHAM COLLEGE MUSIC LECTURES } Daddy red , red nose Johnny likes 
 lect Sir Frederick Brid : , whisky neat hand , MNebéraska 
 ne lectures Sir Frederick Bridge given . * Sa ng ena Wy 
 Gresham College February 7 10 , ‘ eo giao Pee ogy + Fre 4 sae ae eae 
 subjects ‘ Overture ’ ‘ Pelham Humfrey . ’ Bs pret Mion Tl + tke dens ak ak te 
 development overture traced from|}o¢ cakes lie “ neglected dusty 
 Monteverde Purcell , second lecture shelves ? office - boy plough 
 overtures Gluck , Mozart , Mendelssobn.| examination class , lady typist 
 rhe chief interest course February 11,| conid bow ! questions . 
 music Shadwell opera . | presume connoisseur matters 
 founded Shakespeare Zemfest produced surprised learn know difference 
 1674 , given , instrumental music by| petween Handel Largo Le Sacre du Printemps , 
 Locke songs Banister } distinguish Beethoven slow 
 Humfrey known . fortunate ! movement Adagio Spohr , 
 chance missing music Humfrey discovered , account taste . anybody explain 
 library _ - 7 " Paris ® YOOt| dis seubhem amcical pepchehegs ? 
 ihe letter wan able prdem Se chit fig et te snus bet epmiel te 
 . . conversations gentlemen con- 
 interesting find . Humfrey set long ] nected manufacture sale 
 dialogue Devils , long masque.| wares , ‘ Bestsellers , ’ said , 
 music interest , | < notmake . nursed . ’ 
 — gear Pe . - . — aero pleased replied 
 Cneruses Uke Purcen . dere stringed ) | glad hear public taste 
 Sea = — — = em mouaily bad , ypc weak 
 ass watch = oir Frederick « taken ! cullible ; like told 
 foundation accompaniment . instru-| - omer time money better 
 mental movements excellently played Trinity | yses spoiling public taste . 
 ee eee ar ae ae oe = |some satisfaction learned 
 yocal work rformed Miss Peachy , — ; r “ Shee , alien 
 Miss Margaret Chaneaine , Mr. Herbert Theseven poaneper fens making ; - best - seller costs 
 ; * way advertisement , open 
 Mr. Bertram Mills . _ lecturer described | camouflaged , profits eaten 
 action play , reading amusing expenses . , hope 
 dialogue , music _ performed exactly trade particular kind poison gas suffer 
 occurs opera . Act 2 fine| , slump healthier , natural instincts 
 Sete > og 4 n Tempe winds — Bo community — firm faith — 
 inely set Purcell Zewfes ? ) sung } . ; el 
 Principal Devil . music Pietro Reggio , eee ee 
 Italian , lived England time opera 
 produced . sung Mr. Mills . 
 large audience attended — 
 — refused 

 costly ways advertising , 
 paying singers sing songs , making 
 anecdotes concerning songs . , 
 takes brains invent story 
 write song . stories 

 scraps consist mangled remains 
 standard compositions feel like throwing letter correspondence columns 

 things ; bits conventional melo-| Lord Stuart Wortley draws attention point 
 dramatic ‘ agits ’ anf ‘ hurries ’ tolerable } overlooked disposed 
 Oily mutilated Beethoven or| quicken pace old music . doubt 
 Wagner . appear ways in| quickening based sound general principles : 
 music cinema useful| ( a2 ) early composers intended sound 
 adjunct — eventually thoroughly artistic } brilliant sound - day ; ( 6 ) 
 experience . , build fine organ | general pace rapid music having increased , 
 engage gifted improviser supply musical|can obtain brilliance greater speed . 
 background . Second , dispense all/ result justifies step , 
 attempts follow screen , use good orchestral | good cases tonal balance devised 
 works interludes kind generalised| composer maintained 
 accompaniment picture . , best | adoption speed strike staid . 
 , engage composer ( real composer , | example , infrequent experience hear 
 syndicate jazz ragtime merchants ) write ) body strings playing rapidly thee 

 MUSICAL 

 result , sustained background far prominent.|a fly exaggeration , let support 

 /ina / e seventh Symphony , pace 
 marked Beethoven , apt 
 annoying moments prominence 
 brass parts rapid theme played 
 strings . quicker speed theme lost 
 altogether — fact , doubt readers 
 heard , save snatches . question 
 far received like 
 attention deserves . Beethoven example 
 toning brass , 
 far , conductors rule urge 
 players fresh excesses 

 Mr. Ernest Newman laid finger chief 
 weakness modern organ playing wrote 
 Swaday Times February 12 

 following notice Mr. Howard - Jones recital 
 Paris appeared /e .W / énéstre/ January 20 . 
 views foreign critics modern British 
 music sufficiently rare comment 
 Ireland Pianoforte Sonata worth reprinting 

 M. Howard - Jones est un artist délicat et souvent 
 subtil . Visiblement , l’effort d’interprétation est chez 
 lui avant tout d’ordre intellectuel . Apercevoir en 
 chaque ceuvre un ensemble de problémes , nt les 
 solutions devront ¢tre découvertes selon un ordre tres 
 strict ; puis , ces solutions atteintes faire disparaitre 
 toutes traces des démarches qui les preparcrent . 
 Aucun pédantism en effet , au milieu de cette incon- 
 testable réussite technique . Aprés avoir remarquable- 
 ment exécuté deux Préludes ect Fugues de Bach ( la 
 bémol , et si bémol ) , et la Sonate en ut diése mineur , 
 Op . 27 , de Beethoven , M. Howard - Jones fit applaudir 
 la trés interessante et vigoureuse Sonate d'un auteur 
 Anglais contemporain , John Ireland . Par la premicre 
 partie / legro moderato , nous sommes melés , des le 
 début , 4 une sorte de ruissellement sonore . De toutes 
 parts , des notes bondissantes — qui s’enchevetrent — et 
 jamais ne s’attardent . Mais voici qu’avec la seconde 
 partie , zon troppo /enfo , survient comme un scrupule 
 en face d’un tel élan . C’est tout d’un coup , apres la 
 période de joie impersonnelle et presque inconsciente , 
 une méditation dans la solitude . Et la troisiéme 
 partie , cow moto moderato , apporte la synthése de ce 
 qui jusque - l4 demeurait opposition pure . Retour , 
 désormais , 4 la possibilité de la joie ; mais cette joie 
 sera maintenant individualisée et lucide . Les formes 
 qui au commencement de l’ceuvre n’étaient que de 
 fuyantes ébauches , reparaissent isolée et distinctes . Le 
 récital se termina par de brillantes interprétations de 
 Debussy et de Chopin . M. Howard - Jones joua de 
 facon particuli¢rement remarquable la Mazurka en la 
 bémoi 

 Music Foreign Press 

 Berlioz ‘ fond romantic colour 
 forgets music . ’ ‘ 
 favourite composer know 
 music 

 Beethoven Sonatas ‘ badly written 
 pianoforte . truly , especially 

 ones , transcriptions orchestra . 
 lacking hand 

 Beethoven certainly heard , , , hope . ’ 
 finest character /arsifa/ belongs Klingsor . 
 ‘ marvellous rancorous hate . knows 
 men worth , weighs vows 
 chastity balance scorn . 

 human , moral personage drama 

 . 93 . loveliest tunes invented . 
 i. = . o 
 London Concerts 
 ROVAL PHILHARMONIC SOCIETY — JANUARY 260 

 Chose whoknow Vaughan Williams hard ; 
 expected /’as / ova/ Symphony remind 
 Beethoven . 6 Mendelssohn Spring Song 
 expect far away 
 examples proved case . jolly 
 folk - songs found duty , felt sure , 
 lots modal harmony strings 
 fifths common chords root position . 
 rough tart , trifle uncouth , 
 unmistakably pastoral . 
 , folk - song , jolly , 
 pastoral flavour ( conventional kind , 
 missing . movements good deal 
 common — , , result 
 lack sharp contrast interest . 
 thematic material slenderest 
 description — little wisps tune , 
 strong family likeness , looked 
 promising viewed nakedness programme 
 notes . composer astonishing things 
 , weaving fine polyphonic texture 
 producing curiously impressive sonorous 
 meszo - fortes . Yor work seldom 
 moved quickly great noise — rare thing 
 - day . short , , symphony — anothe1 
 rare thing . everybody music . 
 liked tremendously wondered 
 enthusiasm composer brought 
 time time . heard 
 joins neglected brother , Zedon Symphony 
 Dr. Adrian Boult obtained fine performance . 
 novelty Concerto - Fantasia pianoforte 
 orchestra Edgar Bainton , Miss Winifred 
 Christie brilliant keyboard , composer 
 conducting . proved effective work , 
 spoilt good following 
 Vaughan Williams work . Concerto effec- 
 tiveness depended largely externals , 
 brooding austerities Symphony 

 XUM 

 Ferruccio Busoni played solo Concerto 
 wonderfully singularly , accepting lightest 
 phrases face value . //// course 
 answer elaborate manner . 
 Chey sounded like school - boys repeating lines 
 Racine declamation French master 

 Bach Beethoven played Busoni 

 recitals February 4 11 , 
 |Chopin , Liszt , pieces , Bach 
 Goldberg Variations , Chromatic Fantasy Fugue 
 ind Beethoven ( D minor Sonata Op . 31 

 Op . 111 ) interest . , 
 lrest , prized man 

 SYMPHONY ORCHESTRA 

 Che concerts Mr. Walter } 
 Damrosch charge Orchestra formed | 
 corrective — greatly welcomed quarters — | 
 modern trend programme making . ‘ tricks 
 music tricks performance ’ isa 
 motto good deal said . 
 January 23 Mr. Damrosch gave Symphonies , 
 Beethoven . 7 Saint - Saéns C minor | 
 employs pianoforte organ , French work | 
 course resuscitated honour composer 
 memory . ina Haydn D major Violoncello 
 Concerto concert rose height , 
 Madame Suggia violoncellist . February 
 13 Busoni wferor Concerto sound | 
 unexpectedly poetic , Mr. Damrosch gave 
 vigorous reading Brahms Symphony 

 LONDON 

 BOHEMIAN STRING QUARTET 

 famous Quartet drew large audience 
 Wigmore Hall February 6 , playing Beethoven | 
 Op . 130 , adding Grand Fugue — surely | 
 worst , longest , written ; 
 Dame Ethel Smyth Quartet E minor , } 
 capital work heard ; } 
 Dvorak Quintet , Quartet joined 

 Miss Fanny Davies happy results . 

 MES — Marcu 1 1922 

 quartet - playing curiously unequal , Beethoven 

 work—-especially Fugue — giving bad 

 works ; higher speeds want 
 higher standards execution . familiar 
 style matter composition audience 
 demand executant . Speeding - lowers quality 
 execution giving ( 
 seemingly shorter duration ) place . 
 luck , , hear Richard Strauss , London , 
 1912 , conduct performance Mozart 
 Jupiter Symphony , bear recollection 
 éem / é far moderate usual got reading 
 bar revelation . 
 soon 

 3 . ) desire ( Schersos like ) 
 record speeds outdo competitors . ( 
 heard Scherzo 770 
 Beethoven ninth Symphony murdered way ? ) 
 Lastly , basest , managerial 
 need sacrifice music — , ¢.g . , case 
 Jarsifal London years ago — 
 catching late buses suburban trains 

 far dealt objective considerations : 
 let mention . Music , jealous mistresses , 
 brooks competitors distractions . 
 listen , certainly listen appreciation , fine 
 music amid babel social gathering crowded 
 acquaintances . hungry man listen , man 
 anxious train caught . Good music 
 supreme , . moment music 
 ceases preoccupation hour , magic 
 vanishes lost supremacy . Conductors aim 
 excessive speeds thing deprive 
 music ascendancy attention . 
 listen attention music chief executant 
 makes desire end ? Music 
 played presented unimportant . per- 
 formance sounds unimportant soon comes sound 
 perfunctory , perfunctory soon degenerates 
 trivial , end lose interest find 
 piece long dull 

 SIMPLICITY VERSUS INSIGNIFICANCE 

 Sitr,—I note February issue writer 
 | ‘ Occasional Notes ’ inclined dispute Mr. Scholes 
 | contention ‘ theme [ Beethoven fifth 
 Symphony , movement ] changed 

 

 MUSICAL TIMES — Marcn 1 i922 

 weakened Beethoven articles , 
 great . ’ strange thing , | advertisements . Secondly , Press country 
 , apparently gentlemen | false misleading statements Music Industries 
 noticed theme Aas changed precisely | statements calculated exalt foreign tl 
 Mr. Scholes suggests — Beethoven , bars } expense British musical instruments . ar oom 
 14 , 15 . _ _ |¥eaders , meeting statements columns 
 actual fact , , essential | local Papers , forward instantly 
 theme movement lie exact | Federation , steps immediately taken p dom 
 notes written Beethoven bars , } benefit music trades 
 suggested alteration Mr. Scholes ; possible | benefit profession , interests practically 
 modification regards pitch . subsists underlying | identical Music Industries 4 7 
 hythmic theme , best represented Apologising trespassing length 
 : valuable space . — , & c. , H. B. Dicky 

 Federation British Music Industries , 
 . ‘ . . » “ 117 - 123 71es g > 4 
 pitch . agree Mr. Scholes , , P y 3 , Great Portlaad Street , W. 1 . 
 opening altered suggests ( 

 Lewisham 

 plavers . Rehearsals , Friday evenings , 
 Works rehearsal , Sfec/7e Aride ( Dvorak ) , fifth 
 Symphony ( Beethoven ) , ec Conductor , William H 

 Adelaide 

 Schumann ( songs Mr. Howard Fry Pianoforte 
 Quintet led Miss Chester , pianoforte Mr. Russell 
 Chester ) , Chopin ( illustrations Miss Humby ) , Liszt 
 ( illustrations Miss MacEwan Mr. Reginald 
 Paul 

 annual meeting R.A.M. Club held 
 Saturday evening , January 28 . Club 
 important forthcoming Centenary celebrations 
 unanimously decided - elect Dr. H. W. Richards 
 president second year , special resolution 
 enabling Club adopt exceptional course pro 
 posedand carried . largely increased membership 
 great success attended gatherings past 
 year placed Clubin exceptionally strong position . 
 musical social meeting Club 
 present year held Saturday evening , February 11 , 
 probably brilliantly successful 
 meetings Club held , Duke Hall 
 crowded fullest extent . ‘ programme 
 exceptional interest excellence . 
 included delightful performance Beethoven Sonata 
 major violin pianoforte , M. Thibaut 
 Mr. Harold Craxton , dramatic presentation 
 scene Coriolanus Dame Genevieve Ward , 
 kindly contributed recitations 
 interval , possible memorable perform- 
 ance evening César Franck Violin 

 Pianoforte Sonata , refined musicianly interpretation 

 DusLin.—The Meter Concert Committee new 
 given Sunday afternoon concerts . 
 standard remained high , chief credit 
 Mr. Vincent O’Brien orchestra . 
 January 20 programme Italian , Mr. Lauritz 

 Melchior Signor Finzi vocal exponents . Miss Molly 
 Keegan Mr. Mostyn Thomas sang February 5 ; 
 following Sunday , Miss Lena Munro andl Mr. 
 Jackson Potter.——Chamber music repre- 
 sented . Royal Dublin Society offered Brodsky 
 Quartet , January 23 , Mozart , Brahms , 
 Beethoven ; Catterall Quartet fortnight later 

 

 ame 

 GLascow.—On January 21 Saturday night popular 
 concert Scottish Orchestra provided excellent 
 programme Sir Landon Ronald , included 
 Butterworth Shropsh : Lad Beethoven Violin 
 Concerto played Miss Harrison . — — January 28 
 Glasgow Choral Union joined forces Orchestra 
 gave performance David Stephen 
 Sir Patrick Spens . Ernest Austin Hymn Apollo 
 sung , orchestral numbers included 74e Good . 
 humoured Ladies ( Scarlatti ) , Goossens 7 o ’ Shanter , 
 Beethoven Rondino EF . flat wind instruments . 
 Mr. Wilfrid Senior conducted choral Mr. Julius 
 Harrison orchestral music.——In response numerous 
 requests programme Fel.ruary 4 adjusted 

 include Flgar second Symphony , finely played 

 course visit Strauss , ROCHESTER.—The Choral Society , Mr. C. Hylton 

 cordially welcomed London . / anchester | Stewart , building reputation enterprise 
 Guardian spared pains space justice an| programmes January 25 , included 
 artist . Brand Lane concert January 21 he| Parry unaccompanied Motets — Neves weather - beaten sail 
 conducted Don Juan 7i// Eulenspiegel , group | } Zhere 7s old Belief — Vaughan Williams 
 songs sung Miss Ethel Frank . reception ] English folk - song arrangements . Chamber music 
 cordial . Sir Henry Wood conducted Beethoven | played Pennington String Quartet 

 Violin Concerto Toscha Seidel . — — Hallé Sr . AUSTELL,—On January 26 Philharmonic Society 
 Mr. Hamilton Harty attentive Strauss — gave Hiawatha Wedding- Feast Death 
 music Lnock Arden ( recited Mr. Milton Rosmet ) | Ain haha , orchestra played movements 
 , weeks later , Don Quixote . occaston | Fyaydn Symphony B flat . Mr , W. Brennand Smith 
 ( February I1 ) provided Delius C minor Pianoforte | .oiducted 

 NEWrortT.—At second chamber music series | 
 January 23 Prof. Walford Davies gave explanatory talk 

 Beethoven Trio , Op . 97 , taking | : 
 Obituary 

 performance pianist Mr. Hubert Davies | 
 Mr. Arthur Williams . | _ 
 Norwicu.—The Festival chorus sang 7%e Revenge regret record following deaths : 
 Mr. Maddern Williams January 21.——César Franck Ratru Hl . BAKER , founder hon . secretary 
 Symphony played Philharmonic Socicty } Liverpool Church Choir Association , important 
 St. Andrew Hall January 26 , Dr. Frank Bates conducting . | annual Festivals arranged 1900 , amateur 
 NorrincHam.—Trios Haydn Brahms played | music , found time midst * busy ge 
 University College recently Mr. Arthur Catterall | life useful service community organizing 
 ( violin ) , Mr. J. C. Hock ( violoncello ) , Miss Cantelo | 8téat choir Liverpool Pageant , choirs 
 pianofo : te ) . . sang foundation - stone laying new Cathedral 
 : , , , , _ opening Gladstone Dock , occasions 
 Oxrorp.—Until Dr. Adrian C. Boult came British | .¢ Royal visits . rare blend business musical 
 Symphony Orchestra February 2 , Oxford qualities found scope member Phil- 
 acquaintance Butterworth 4 Shropshire Lad , little | harmonic Society committee . regretted death 
 masterpiece stands firm reputation especially heavy blow Church Choir Association 

 known * Euterpe 
 opening practices 

 Bride , Mr. W. H. Kerridge ( arranged February 
 11 ) , Mozart Aegutiem , Beethoven fifth Symphony 

 valuable lectures 

