OLVERHAMPTON TRIENNIAL MUSICAL 
 FESTIVAL.—Tuurspay Fripay Mornings Even- 
 ings , September 13 14 , 1853 

 MENDELSSOHN " ELIJAH . " 
 BEETHOVEN " MOUNT OLIVES . " 
 GOUNOD " MESSE SOLENNELLE . " 
 HUMMEL " ALMA VIRGO . " 
 MACFARREN " LADY LAKE . " 
 MACKENZIE " JASON 

 Miss ANNA WILLIAMS , Miss MARY DAVIES , 
 MapAME PATEY , Miss EMILIE LLOYD , 
 Mr. EDWARD LLOYD , Mr. JOSEPH MAAS , 
 sicnor FOL | Mr. FREDERIC KING . 
 band Chorus 300 performer . 
 conductor - - Dr. SWINNERTON HEAP . 
 detailed prospectus obtain Hon . secretary 

 MISS EMILY PAGET ( soprano ) 
 ( R.A.M. certificate Medalist Singing ) 
 cpen engagement Oratorios , Concerts , & c. 
 address , 19 , Lloyd Square , London . 
 MISS KATE PKOBERT ( soprano ) . 
 ( Certificate Solo Singing R.A.M. ) 
 open accept engagement Oratorio , Ballad , Con- 
 cert , address , 29 , North Road , Montpelier , Bristol . 
 MISS AGNES ROSS ( soprano ) . 
 Oratorios , Concerts , & c. , 32 , Henrietta Strect , Covent 
 Garden , W.C. 
 MISS S. a. SABEL ( soprano ) . 
 Oratorios , Concerts , lesson , \c . , 4 , West View , Hendon , N. 
 MRS . ALFRED J. SUTTON ( soprano ) 
 open engagement Concerts Oratorios , 
 54 , Duchess Road , Edgbaston , Birmingham . 
 MADAME CLARA WE5sT ( soprano ) . 
 MISS LOTTIE WEST ( contralto 

 Beethoven Villa , King Edward Road , Hackney , London 

 MISS AGNES MARY EVERIST ( contralto ) . 
 Operas , Concerts , Oratorios , & c. , address , 59 , Camden Square , N.W. , 
 Keppel Co , , 221 , Regent Street , W. 
 MISS ALICE KEAN ( contralto ) 
 ( pupil Odoardo Barri ) . 
 Oratorios , Concerts , dinner , Homes , & c. 
 address , 191 , Edgware Road , Hyde Park , W. 
 MISS MARGARET LEYLAND ( Contralto ) . 
 Oratorios , Concerts , & c. , address , 51 , Plymouth Grove , 
 Manchester 

 heart faithful ( mein gl aubiges Herz ‘ frohlocket ) ... Bach . 
 . Andantino 5th , Andante yth Sonatas Pleyel . 
 . Hero March .. " nd . Mendelssohn 

 II Rossini . 
 12 . Air et cheeur , ' * La Dame Blanche " 3oieldieu . 
 13 . Grande Marche Héroique C ... os ' < F. Schubert . 
 14 . Grande Marche Héroique D ... ase F. Schubert . 
 15 . overture " Berenice "   ... oe — Handel . 
 15 , overture ' ' Sosarmes " Handel . 
 17 . overture ' Alcina " ie son Handel . 
 18 , Gavotte overture " Otho " ' Handel . 
 19 . LaCarita ... Rossini . 
 20 . angel bright , ' pious orgy aa Handel . 
 21 . Ave Maria os dea F. Schubert . 
 22 . Aria . circa a.d. 17 Artonio Lotti . 
 23 . soul thirsteth God , Aria { ‘ nd Psalm — Mendelssohn . 
 24 . Gloria Excelsis , Mass G ; pe . Weber . 
 25 . fac ut portem , " ' Stabat Mater " ... Rossini . 
 26 . Pieta Signore , Oratorio San Giovanni Battista Stradella . 
 27 . overture " ee s Cxsar ' a= sa Handel . 
 24 % , Serenade F. Schubert . 
 206 aria ( 1765 ) exe os ome aes ae Gluck . 
 30 . Aria " Alcina " isi abs ans Handel 
 31 . Aria " Artaserse " ( 1730 ) ... Leonardo Vinci . 
 32 . Cantata sen ae pee io " Alessandro — 
 33 . Aria ( 1763 ) .. Gluck . 
 34 . Aria ( 1784 ) ... pen " Domenico Cimarosa . 
 35 . Diedi il coro , Aria ... S .. Handel . 
 36 . Siciliana Long . 
 37- Andante Long . 
 35 . Aria ( 1763 ) Padre Martini . 
 39 . Kyrie leison , " Mass sin G. es Schubert . 
 yo . Aria ( 1767 pow Gluck . 
 ae " Sanctus " " Hosanna " Mas 58 ( Op . 4 3 ) Andre . 
 42 . chorus " Mount Olives Beethoven . 
 43- Hie shall feed flock , ' Messiah " " . ... Handel . 
 44 . quoniam tu solus ( 1785 ) ... en ... Vincenzo Righini . 
 45 . H allelujah Chorus ' " ' mess jah " Handel . 
 40 . ' turn thy face , " ' ' shall teach , " ma ; agnify 
 Thee . " Anthems ; é J. Weldon . 
 47 . heaven tell , " creation " . Haydn . 
 33 . " Andante " ' ' allegretto , " Violin Sonata 
 major .. ' jas .. Handel . 
 49 . slow movement Symphony ( g6 ) ... Haydn . 
 50 . Andante con Variazioni , Notturno ( op . 34 ) Louis Spohr 
 51 . wie nahte mir der Schlummer . C. M. von Weber . 
 aria ... ean ' Comte de St. Germain . 
 52 . Marche Solennelle ( Op . 40 ) : F. Schubert 
 53 . Adagio Notturno ( op . 3 ) ... Louis Spohr . 
 54 . Ave " M aria , " Evening Service , " 300k Fa Cherubini . 
 55 . overture ' ' Samson , " Minuet ( 1742 ) .. Handel . 
 56 . arm Lord ese Haydn . 
 57 . Deh lasciao core , " Astianatte " ( 1727 ) " Giov anni Buononcini . 
 58 . Gloria Excelsis , mass . 2 G Schubert . 
 59 . il pensier sta negli oggetti , Aria ( 1792 ) . Haydn . 
 bo , Gloria Excelsis , twelfth Mass : . Mozart . 
 61 . iovely einai . Mendelssohn . 
 62 . Notturno ... « os F. Kalkbrenner . 
 63 . che faro senza euridice ‘ iia < < gluck . 
 64 , Aria flat : ... Louis Spohr 
 65 . Cujus animam F .. Rossini . 
 € 6 . Air Gavotte ( Orchestral Suite ) S. Bach 

 Loxpon : EDWIN ASHDOWN , 
 HANOVER SQUARE , W 

 siques du Flutiste 

 transcription work Haydn , Beethoven , 
 Mozait 

 Mendelssohn , & c. , & c. , 
 m 

 Ee Morceaux 

 HOLLAE NDE R , G.—Two concert piece Vislia 
 Piano , Op . 16 
 1 . nena 
 Tarantelle 
 HUBER , HANS . -album . 
 op . 6 . book 
 KOETTLITZ , M.—Humore 
 — Grazioso . Valse Piano . 
 LANGE , S. DE.—Variations 
 Organ . op . 34 .. 
 LASSEN , e.—ballet music * 
 op . 73 i-- 
 orchestral score 
 orchestral 
 piano solo 
 LEONARD , H.—Cadenza Beethoven Violin Concerto 
 — — - suite Violin Accompaniment Quartet Piano . 
 op . 53 : — 
 Quartet 
 Piano ... " ; ' 
 LEYBACH , J.—Danse Napolitaine Piano . op . 239 
 — — Les Nymphes . ape brill : unt . op . 24 
 LISZT , F.—Anncées de Pélerinage . Compos 
 Series . . , kaa « 
 MULLER , V.—Marcia religiosa V lieontie ' mal ee 
 sal iu m. op . 8 " ‘ sas 
 — Fantasia ugue ' Organ . op . ) ; 
 PAQUE , G.—La Jota aragonesa . brillant Fantasia V 
 cello Piano 
 RUBINSTEIN , A. 
 new edition .. Aes 
 — Ditto , arrange 4 Piano . _ sian ie ee 
 SCHELLEKENS , O.—Gavotte , ' Marie Elizabeth , " Piano 
 SCHMIDT , O.—Momento comtieane Piano . - 35 
 — — Regret . Etude Piano . op.3 . < < 
 — Wedding March Piano Duet . " op . 33 
 SCHOLZ , B.—Stin n eine Verlassenc Male yelies 
 S ments Piano . op 

 piece Piano Duet . 
 ; pen " - 
 ce Piano . 
 op.19 . 
 " God 

 conductor , choose . 
 comparatively easy " wield baton , " 
 member band feel Conductor 
 acomposer meaning , inspire confidence 
 enforce obedience , rule power 
 absolutely necessary ; , admit fact 
 basi deliberation , little doubt 
 right person find , 
 artistic success Society date 
 appointment 

 Richter Concerts extremely 
 attend , fairly 
 gain position London musical at- 
 ‘ traction . modern german school 
 satisfactorily represent programme , 
 work Beethoven — notably C minor Choral 
 Symphonies — place , 
 eminent composer treat neg- 
 lect . season new 
 reading standard work , 
 time accustomed ; Herr 
 Richter suppose tolerate 
 alteration interpolation introduce 
 pianoforte Beethoven Choral Fantasia , 
 ot reasonably presume 
 sanction conductor 

 year summary musical season 

 Castil - Blaze describe pure invention,|that work publish abroad — 
 thing certain Napoleon request | absence international copyright law — pro- 
 Paisiello successor . Lesueur was|duce publish american manager 
 fortunate man . publisher . understand Judge Lowell 

 1805 Cherubini organise conduct a|willingness grant injunction per- 
 great performance Mozart ' ' Requiem . " | formance ' redemption ' use 
 engagement quit French|the composer score arise opinion 
 capital Vienna , attract thither liberal offer | work suffer imperfect representation . 
 opera , produce personal | , decision cause renewal 
 superintendence . certain Baron von Braun come ] agitation subject international 
 Kaiserstadt expressly arrange affair , | copyright law shall protect work 
 Cherubini , wife , infant daughter|of foreign american composer _ regard 
 leave Paris , travel way Frankfort , Berlin , | artistic completeness , shall guarantee 
 Dresden , Prague . master write detail | adequate financial remuneration . isa 
 long roundabout journey , little | - know fact interested sub- 
 gain quote narrative interest- | ject — pain substan- 
 e ordinary traveller diary . that|tiate contemplate publication 
 party reach Vienna safe sound , July 27 , | article — lead book - publisher author 
 Cherubini lose time } arestronglyin favour passage ofan international 
 acquaintance venerable Haydn , nearing| copyright law . doubt music- 
 close career ; Beethoven , star | publisher composer anxious secure 
 rise zenith ; Hummel , in|such obvious protection publication . 
 glow popularity . soon bring out| understand opposition passage 
 " Les Deux Journées " ' Lodoiska , " ] international copyright law come England , 
 begin new opera Vienna stage , when| reason : english publisher insist 
 dark gigantic shadow Napoleon fall upon| retain publication home , secure 
 austrian capital like eclipse . know | printer , binder , paper - maker , & c. , 

 438 MUSICAL TIMES.—Aveusr 1 , 1883 

 time time effort esta- 
 blish uniformity print music , shall 
 truth universal language . 
 speak reform introduction 
 sign ' bind ' shall distinct 
 use ' slur ’' — initiate late Sir Stern- 
 dale Bennett — think 
 inestimable boon composer indicate pre- 
 cisely wish music engrave , 
 , equal importance , engraver 
 obey . want definite rule regard 
 direction performer , slur 
 extend end bar , instead end 
 phrase , ' ' tempo " passage 
 time precede bar accele- 
 rate retarded , accidental contradict 
 note alter signature 
 bar previously . instruc- 
 tion book tell acciaccatura distinguish 
 appoggiatura line draw 
 ; meet note 
 line . piece 
 note phrase mark * ' con fuoco " 
 remainder ' " ' con passione ’' ; 
 " rallentando , " direction 
 kind bar , ' rall . molto " occur , 
 time expect play inter- 
 vening bar remain mystery . ' fact 
 sin omission commission 
 composer , hurriedly throw music 
 expectation engraver 
 right . 
 manuscript correct par- 
 ticular engrave , 
 doubtful point draw attention 
 occur , know fault 
 lie 

 tue subject " Programme Music " 
 tolerably debate ; opinion differ 
 limit composition kind 
 o’erstep . scarcely think , how- 
 , question artistic 
 incongruity illustrate musical work 
 performance action scene event 
 composer intend depict . 
 time ago attempt London heighten 
 effect Beethoven * * Pastoral " Symphony 
 panorama ; enterprise , 
 expect , brief life , speedily forget . 
 late development idea 
 Cleveland , Ohio , , strangely , 
 oratorio , account performance 
 Haydn " creation , " read ' beginning 
 audience - room literal chaos darkness , 
 word ' let light , ' electric 
 light flash forth effective brilliancy 
 audience near break forth wild 
 demonstration deafening applause . 
 climax reach conclusion Oratorio , 
 ve come forth sing ' home , sweet home , ' 
 accompany piano . " 
 copy notice precisely appear ; 
 fact appear startling true . 
 hear accompany music scenery 
 action connection secular work ; 
 realism Oratoriois novelty . newspaper 
 paragraph quote 
 somewhat favour innovation ; idea 
 grow , ' " effect " 
 attempt Cleveland , evident 
 sensational violation sacred character 
 composition receive 
 ' " deafening applause 

 Musical Association found , 
 1874 , investigation discussion subject 
 connected art science Music . 
 object steadily view ; certainly 
 paper read meeting 
 good modern musical essay . 
 ask question — , , 
 frequently occur — 
   - consider article 
 virtually lose general public . true 
 end session little volume issue 
 title ' Proceedings musical 
 Association , " notice cover , 
 ' * price ( non - member ) shilling , " prove 
 perusal book know mainly 
 confine ¢onnecte Society . , 
 consider great importance 

 RICHTER CONCERTS 

 Concerts come end season onithe 
 2nd ult . , close , St. James Hall crowd 
 amateur , recognise generally fine per- 
 formance earnest continued effort lead 
 future usefulness . programme contain — usual 
 — Beethoven Choral Symphony , 
 great work entire " " . Herr 
 Richter reading " ninth " know 
 discussion , , good rendering 
 grant , remark need occupy time 
 space . let , , emphasise impression 
 orchestral movement , especially 
 Adagio , play . loveliness 
 embodiment music pure 
 beautiful , , exposition . 
 serve final Concert 
 memorable . rendering Allegro Scherzo 
 free blemish , fault , use 
 trite figure , spot sun , 
 appreciably lessen splendour luminary . 
 , course , place Finale apart , 
 , note fact voice 
 perfect , add expression regret Beethoven 
 completely ignore limit otf capacity . 

 Symphony receive enthusiasm , 
 describe furore mark applause 
 bestow , close , Herr Richter sub 

 remarkable , conductor 

 perfectly , point 
 precision creditable 
 favourable condition . Mendelssohn Psalm 
 Beethoven ' * emperor " Concerto step 
 high aim instruction 
 College . Mr. Alfred Hollins hear 
 connection blind student . 
 play Windsor Castle Queen 
 famous . occasion essay great 
 classical work instrument , execute 
 wonder audience flattering token 
 admiration . shall draw comparison 
 Mr. Hollins free disadvantage 
 labour , fair play 
 Beethoven work quality high order . rarely 
 pianist touch wrong note , passage 
 fire facility , general sense 

 comprehensive mastery 

 clarinet , oboe , no_bassoon . 
 inequality , course , inevitable 
 amateur orchestra redress professional 
 assistance , unmixed advantage . 
 excellent musician Dr. Gladstone helm 
 ' Society certain guide right direction 

 instrumental portion programme consist 
 work high class , include Mozart Symphony 
 D , no.5 ; Boieldieu Overture , ' * Caliph Bagdad " ; 
 Mendelssohn * Cornelius ’ March . Mr. N. Rush- 
 worth play finale Beethoven rarely hear Piano- 
 forte Concerto 6 flat , Mr. F. Rushworth , 
 movement Rubinstein Piano ' Cello Sonata D , 
 Op . 18 . invidious particularise , 
 especially programme performance 
 alike creditable . rendering part- 
 song glee evidence good training 
 Mr. F. H. Carter , compliment 
 - song ' * Waken , lord lady gay . " 
 audience large , include large number 
 hospital nurse , picturesque attire heighten 
 | appearance assemblage 

 448 

 ir fairly event pre- 
 sent special interest musical amateur 
 West Riding , preparation 
 forthcoming Leeds Musical Festival . series 
 rehearsal , thirty number , conclude 
 past month , chorus allow rest 
 week . work 

 rehearse include Beethoven Mass D , Mac- 
 farren ' ' King David , " Raff ' * End World , " 
 Rossini * * Stabat Mater , " Handel Selection , Mendels- 
 sohn ' Lobgesang , " Cellier 
 minor piece 

 Gray Elegy " 
 Beethoven Mass naturally 

 occupy great portion labour Mr. Broughton 
 chorus , result enormous difficulty 
 work clearly understand , successfully 
 overcome : eventually overcome 
 firm belief know capability 
 Yorkshire singer . chorus second 
 final Macfarren new work receive 
 publisher ( Messrs. Stanley Lucas Weber ) 
 carefully rehearse : contain excel- 
 lent specimen composer characteristic skill 
 musical dialogue , concluding number con- 
 spicuous fine fugue word " glory 
 Father , " augmentation theme 
 afford grand passage bass . Raff Oratorio 
 favourite , idea form 
 important orchestral number condensed 
 score publish ; chorus , , gene- 
 rally pronounce fine , 
 sufficient secure work place 
 good composition modern time . sorry 
 notice defect translation 
 German ; remedy , , fortunately easy 
 case . Handel Selection publish 
 Messrs. Novello , Ewer Co. , contain well- 
 know masterpiece . Barnby * * Lord King " ( psalm 
 g7 ) arrive , time writing 
 hand chorus 

 Sir Arthur Sullivan pay visit Leeds 20th 
 21st ult . , direct rehearsal chorus , 
 Beethoven Mass Raff Oratorio satis- 
 factorily . express highly 
 pleased singing , compliment Mr. Broughton 
 success reach 

 considerable difference opinion exist regard 
 refusal Committee engage Madame Albani 
 principal soprano . agree 
 term demand Mr. Gye undoubtedly high , 
 want assert decision 
 Committee somewhat hastily arrive , 
 wish gain oppor- 
 tunitie negotiation allow . , , 
 tolerably certain substitute Madame 
 Albani find Madame Valleria , 
 singe Handel Festival 
 great satisfaction . artist engage Miss 
 Anna Williams , Miss Annie Marriott , Madame Patey , 
 Miss Damian , Miss Hilda Wilson , Mr. Edward Lloyd , 
 Mr. Joseph Maas , Mr. Henry Blower , Mr. Frederic King , 
 Mr. Santley . skeleton programme follow : 
 Wednesday , October 10 — morning : Mendelssohn 
 " Elijah " ; evening : Cellier Cantata " Gray elegy , " 
 Beethoven Second Symphony , Mozart ' Zauberflote " 
 overture . Thursday , October 11 — morning : Raff 
 symphony - oratorio ' * end world , ' Handel 
 selection ; evening : Barnby ' * Lord King , " 
 Mozart Motett , Bach ' ' O guide Israel , " 
 Rossini " * Stabat Mater . " ' Friday , October 12 — morning : 
 Macfarren " ' King David ' ; Evening : Gade ' ' crusader , " 
 Schumann ' ' Genoveva " overture , Wagner ' * Tann- 
 hauser " March Chorus . Saturday , October 13 — Morn- 
 ing : Beethoven Mass D , Mendelssohn ' ' Lobgesang 

 Tue follow circular inspector school Eng- 
 land Wales issue Education 
 Department : ' 1 . consequence representation 
 colleague , support Dr. 
 Stainer , Inspector Music , lord decide 
 prolong year period 
 standard school present examination 
 singing division low assign 
 instruction date February 14 . 2 . September , 
 1884 , substitute September , 1883 , 
 paragraph number n.b.1 circular 219 . 3 . 
 lord agree small school ( average 
 attendance ) division 
 purpose instruction singing , similarly 
 divide purpose examination subject ; 
 provide , high division , fair proportion 
 scholar examine work Division iii . 
 ( September , 1884 ) Division 4 , ( Circular 219 , 
 N.B ; : 2,3 

 MUSICAL TIMES.—Avucusrt 1 , 1883 

 meeting hold Grosvenor House , Park Lane , 
 11th ult . , furtherance movement intro- 
 duce National Anthem India . temporary 
 absence Earl Shaftesbury , chair 
 Sir William Andrew . Canon Harford deliver address , 
 point advantage result 
 familiarise native India strain 
 National Anthem Great Britain . programme 
 include singing National Anthem 
 indian vernacular — Hindustani , Mirza Muhammad 
 Bakir Khan , Persia ; Sanscrit , Professor Max Muller ; 
 Bengali , Rajah Sourindro Mohun Tagore , Mus . Doc . , 
 Calcutta ; Guzerati , Kaikhosro Kabraji , Esq . , 
 Bombay . music use Dr. Bull , Dr. Arne , 
 Beethoven , Sir Julius Benedict , Sir Michael Costa , Gounod , 
 & c. performance ¢onducte Dr. Bridge , 
 organist Westminster Abbey , assist Mr. John 
 Thomas , harpist Queen . vocalist H.H. 
 Princess Hellen Rundhir Singh , Kapurthala ; Miss 
 Agnes Larkcom , Miss Alice Lakey , Miss Carrie Brown , 
 Miss Helen D’Alton , Miss Clara Myers , Miss Braham , 
 Miss Constance Dunn ; Mr. Percy Blandford , Mr. D’Arcy 
 Ferris , Mr. George Colville , Mr. R. Hilton , Mr. W. Fletcher 

 Mr. R. Griffin , Mr. Frederick Dunn . resolution express- 
 e decide approval proposal introduction | 
 National Anthem India Lord Mark | 
 Kerr , second Professor Ramuswami Raju , B.A. , | 
 Madras , unanimously carry 

 excellent rendering Dr. Stainer Cantata ' ' 
 Daughter Jairus " , direction Mr. 
 W. M. Sergison , evening service St. Peter 
 Church , Eaton Square , Dedication Festival , 
 Thursday , 6th ult . choir slightly 
 augmented occasion — circumstance , , 
 detract good effect service ; 
 accompaniment admirably render organ 
 Mr. Sergison . Barnby Magnificat Nunc dimittis 
 E flat ( write Sons Clergy Festival 
 1881 ) service use 

 Wolverhampton Triennial Musical Festival 
 announce hold Thursday Friday , September 
 13and14 . Mendelssohn " Elijah , " Beethoven * * Mount 
 Olives , " Gounod ' Messe Solennelle , ' " " Hummel 
 " Alma Virgo , " Macfarren ' Lady Lake , " 
 Mackenzie ' ' Jason " perform , vocalist 
 Miss Anna Williams , Miss Mary Davies , Madame 
 Patey , Miss Emilie Lloyd , Mr. E. Lloyd , Mr. J. Maas , 
 Mr. F. King , Signor Foli . band chorus 
 number 300 , direction Dr. 
 Swinnerton Heap 

 Sir JuLtivus Benepict present medal Saturday 
 evening , 21st ult . , St. George Hall , Regent Street , 
 successful student London Academy 
 Music . previously distribution musical per- 
 formance crowd audience , 
 direction Signor Romilli . Sir Julius express 
 highly gratified progress pupil , wish 
 Academy success future , pay 
 time - deserve compliment Principal , 
 Dr. Wylde 

 short time prize guinea offer 
 Council Hibernian Band Hope , Lower Abbey- 
 street , Dublin , good musical setting — song 
 chorus — verse write irish lady , 
 originate Blue Ribbon movement . Sir Robert 
 P. Stewart , Mus . Doc . ( organist Dublin Cathedral ) , 
 judge , award prize Mr. T. Simp- 
 son , organist musicseller Burnley . song , 
 print , sing forthcoming féte 
 Union 

 MADAME HELEN HopEKIRK avery successful Piano- 
 forte Recital Prince Hall , Piccadilly , Thursday , 
 June 28 . programme select work 
 Handel , Beethoven , Schubert , Chopin , Liszt , 
 item display advantage brilliant 
 refined playing concert - giver . Madame Hopekirk 
 interpretation Chopin Fantasia F minor Schu- 
 bert Fantasia C receive great 

 favour 

 understand service Gloucester Cathedral 
 Wednesday afternoon future sing lay 
 vicar , allow choir boy weekly half- 
 holiday . change adopt recom- 
 mendation Mr. C. L. Williams , organist 

 Miss MINNIE GOLpsmiITH , pupil Mr. George Mount , 
 Pianoforte Recital Marlborough Rooms 
 12th ult . composition , select work 
 Beethoven , Weber , Chopin , Prudent , Schulhoff , 
 Moszkowski , interpret highly efficient manner 

 read Times , matter having assume 
 practical form , work new Italian Opera House , 
 Victoria Embankment , shortly resume , 
 complete , possible , time Italian Opera 
 season 1884 

 MUSICAL TIMES.—Auveust 1 , 1883 

 Royal Opera House Vienna 303 perform- 
 ance place season , comprise 
 vépertoire 78 different work . 42 evening 
 devote Meyerbeer , 41 Wagner , 31 Verdi , 23 
 Mozart , 21 Gounod , 16 Donizetti , 15 Adam , 14 
 Boito , 10 Gluck , g Weber . Beethoven , 
 ' ' Fidelio , " content perform- 
 ance 

 director reconstitute Italian Opera 
 Paris , commence operation autumn , 
 negotiation Mr. Gye secure service 
 Madame Albani . artist engage 
 Mesdames Fidés - Devriés , Tremelli , Donadio , 
 Messieurs . Gayarre Reszké . celebrated baritone , 
 M. Maurel , soul undertaking 

 shoot , false love , care 

 414 . 
 415 . o Nymph ia ae Palestrina 
 416 . ye singer , great small oo Waelrent 
 417 . fie love aaa A. Macfarren 
 418 . Winds Autumn Rohs s Ob — 
 419 . softly fall shade ... oe ea aan E. Silas 
 420 . love little , love long Leigh Wilson 
 421 . shall tell love ? S.S. Wesley 
 422 . lover lass Josiah Booth 
 423 . love question reply ... J. B. Grant 
 424 . , loathe melancholy ... Henry Lahee 
 425 . evening song ... p ia , E. M. Hill 
 426 . welcome daw n Sunes ’ s _ Ly E. M. Hill 
 427 . charge Light Brigade rard Hecht 
 428 . beauty mountain .. Sir J. Goss 
 429 . o sweet Mary " ‘ ne ove « .. Sir J. Goss 
 430 . lo ! rosy - bosom ’ d heme « .. Sir J. Goss 
 431 . eye glow - worm lend Mink . ... Sir J. Goss 
 432 . bell St. Michael Tower Sir R. P. Stewart 
 433 . Cruiskeen Lawn ... Sir R. P. Stewa 
 434 . wi e Alt vitae s Hall Sir R. P. Stewart 
 435 . Ye Mariners Eng sland " va ue H. H. Pierson 
 436 . Vesper Hymn ... ne sae . Beethoven 
 437 . tho ’ sorrow oft befalis 

 438 . Swallows 

 BacuMANN , G. Ronde Bretonne 
 « . Marche Tcherkesse 

 Petit Menuet ... cock . 
 BakNetT , J.F. Harvest Festival Fantasia PaTey . 
 BEETHOVEN ... sonata C major < Lucas 

 » flat ma AjOr 

 o 6 PiTMAN . 
 Bunt , G. ... Lurette ... 1 6 J. Wituiams . 
 Crark , S. ... chorus ' Angels . 2 0 AUGENER , 
 CRUICKSHANK ' Twilight . ... 1 6 ForsytH. 
 - _ Bourrée c minor 6 . 
 CARMICHAEL Barcarole .. ens ove 2 o Lucas , 
 CAVENDISH Music Books . 
 . 53 . Short piece ... 1 0 Boosey , 
 yy 51 . sixth Pianoforte Album 1 o * 
 » 54 . seven Pieces A. P , 
 Wyman ove - £ 8 yi 
 » 55 » operatic Fantasias ... 0 * 
 iy BOs —   saon Schul 

 1 Se . ais ' Gotts- 
 chalk ... io 
 » 58 standard overture . 
 book 1 zo = 
 » 59 . seven piece C.D : 
 Blake .. ro ea 
 » 60 . marche american 
 composer ... Io Pe 
 » 61 . — ' Album Io pe 
 » , 62 . March Album . Io " * 
 » 63 . popular . ' piece x 0 e 
 » 67 . standard overture . 
 book 2 - £ 9 " 
 70 . seventh Pianoforte 
 Album .. SS es 
 Cuorin , F , ... Tarantelle , flat major . 1 6 Lucas , 
 Cocks ’ SranbarD Cxassicat Pieces . series . 
 . 1 . Sonata D ( Paradies ) 2 0 cock . 
 » 2 Minuets anamaaaanes 
 C se 6 " 
 » 3 . Minuets ( B Beethoven 
 G C 6 s 
 » 4 . Minuets ( Beethoven ) , 
 Minuet Trio 1 6 ve 
 » 5 . Presto(Paradies ) ... 1 6 ne 
 » 6 . Bourrée ( J. S. Bach ) ... 1 6 
 » 7 + Rondo alla Turca 
 ( Mozart ) ase 1 6 
 » 8 Rondo D ( Mozart ) 6 os 
 » 9 g. Allegro Sonata 
 . 6(F. Turini ) ... 1 6 e 
 y » 10 . Prestissimo(F.Turini ) 1 6 
 s. 206 March ( Mozart ) ws = 6 

 Dan animato ) 
 D(Galuppi ) _ ... 16 
 gigue e minor " 
 bat B. chai ) 
 6 J. Witiiams 

 F 

 C 
 - SON GS , & c 
 ? ° 
 cc 
 compose | compose 
 arrange PRICE | arrange PRict 
 x. Dulcedomum . s.a.7.B. ' ... ove « » G.A.Macfarren 1d./ 48 . Lordi vena vee S.A.T.B. G. A. Osborne 2d . h 
 2 . dead man . S.A.T.B. ee ' 1d.| 49 . Te DeuminF ... ee Jackson 2¢ , k 
 3 . girl leave . S.a . T.B .... 1d./50 . Te DeuminF ... iis ae sui ses ove Nares 2d 
 4 . British Grenadiers . s. A.T.B. ois ae < 1d.| 5x . charity ( La Carita ) . s.s.s . ... Poy eee aa Rossini 44 . 
 5 . long live England future Queen . s. A.T.B. Dr. Rimbault 2d . | 52 . Cordelia , A.T.T.B. sis ae .. GA , Osborne 4d . 
 6 . task end ( song chorus ) . A.T.B.B. ... « . Balfe 4d . | 53 . Iknow . s. A.T.B. wah Walter Hay 2d . 
 g- spake summer day . S.A.T.B. ... ons - . Abt 2d . ; 54 . chorus Handmaidens ( " F ridolin % ) A. Randegger 44 . 
 8 . soldier ’ chorus . T.T.B.B. aie ane Gounod 4d./55 . Offertory Sentences ... « » Edmund Rogers 4d . 
 9 . Kermesse ( scene " Faust " ) - 6d . | 56 . Red - Cross Knight Dr. Callcott 2d . 
 Io . , quit thy bower . 5S.A.T.B. - + Brinley Richards 4d . | 57 . Chough Crow Sey : Sir H.R. Bishop 34 . 
 11 . maiden , - woo . S.8.T.T.B. w G. A. Macfarren 2d . | 58 . " Carnovale " tne eee oe Rossini 2¢. 
 12 . Faggot- binder ’ Chorus va Gounod 4d . } 59 . softly fall moonligh ht cs ow ... Edmund Rogers 4d . 
 13 . sylvan hour ( female voice ) w- Joseph Robinson 6d . | 60 , air Himmel ... ae " se ; Henry Leslie 2d . 
 14 . Gipsy Chorus ... aie se .Balfe 4d.| 61 . Offertory sentence ... ose ai E. Sauerbrey 4d . 
 15 . Ave Maria ss se Arcadelt 1d . 62 . Resurrection e ( Villiers Stanford 6d . s 
 16 . hark ! herald angel sing . S.A.T.B. Mendelssohn 1d . ' 63 . boy . new patriotic Song H. HJ . — & W.M. Lutz 44 . 
 17 . England ( solo chorus ) . 5S.A.T.B. Sir J. Benedict 2d . | 64 . Menof Wales ... : meanaees 2d . 
 18 Shepherd Sabbath day . S.A.T.B. oe J. L. Hatton 2d . 65 . Dame Durden ... rr 1d . 
 i9 . thought childhood . s. A.T.B. ... oon Henry Smart 2d.| 66 . little farm till oe " ho ok xd . ¢ 
 20 . Spring return . S.A.T.B. ... * _ 2d . | 67 . simple maiden ... dine ae ace ' A. Macfarren 1d . » 
 21 . Anold Church Song . sS.a . T.B. - ss 2d . | 68 . Fair hebe nce ae = 1d , m 
 22 . Sabbath Bells . s.a . t.3 . ove ose eee - 2d . | 69 . love maiden ' fair ais ssa " 1d , aj 
 23 . Serenade . S.A.T.B. ... nse * 2d.| 70 . Jovial Man Kent pe aa ae + s 1d , k 
 24 . cold Autumnwind . § , a.t.b. ee ata * 2d.|71 . Oak andthe Ash . aise oe " 1d . 
 25 . Orpheus lute . s.s.s . eve « . Bennett Gilbert 2d.|72 , Heartofoak   ... ose ove eee " 1d . c 
 26 . lullaby . S.A.A. ve ea = 1d . | 73 . come sunset tree oe « . W.A. Philpott 4d . 
 27 . , " native land . s.a . T.B , G , A.Macfarren 1d.| 74 . . S.A.T.B. W. F. Banks 2d . cc 
 28 . March Men Harlech . s. AcT.B. cs Dr. Rimbault 2d . | 75 . pure , lovely innocence Il Rec di Lahore " ) . " chorus female ar 
 29 . God save queen . s. A.T.B. ake 4 1d . | voice ... . Massenet 4d . M 
 30 . Rule , Britannia , s.a . T.B. ... ase won ° 1d.| 76 . Love Idyl . s. . B. ens ane bie .R. Terry 2d . & 
 31 . retreat . 1.7.1.8 . ove ae . « » L.de Rille 2d.!77 , hail wood . a. TTB . b Yarwood 2d . E 
 32 . Lo ! mornis break . s.s.s . ooo os + . Cherubini 2d . | 78 , nearthe townof Taunton ... Dean Thomas J. Dudeney 2d 
 33 . Weare spirits . s.s.s .. site « . G.A.Macfarren 4d.| 79 , merry boy sea Yarwood 20 . Si 
 34 . Market chorus ( " Masaniello " ) . S.A.T.B. aie Auber 4d . ! 80 . Christ rise ( Easter Anthem ) . SATB . ae Berlioz 3¢. R 
 35- Prayer ( ' ' Masaniello " ) . S.A.T.B. ae ons + s id. | 81 . sun set o’er mountain ( ' ' Il Demonio " ' ) 
 30 . Water sprite . s.a.7.5 . sas ass Kiicken 2d . | A. Rubinstein 3¢. 7 
 37 . Eve glitter star . S.A.T.B. hk oon 2d . | 82 . hymn Nature - Beethoven 34 . 
 38 . primrose . s. A.T.B .. ove ove " 2d . | 83 . Michaelmas Day ( humorous ' Part- song , ' no.3 ) W. Maynard 4d . te 
 39 . O dewdrop bright . s.a.7.n . eee oe " 1d . | 84 . Sporting note ( humorous Part- Sane , . 2 ) 4a . un 
 40 . Sanctus , " Messe Solennelle . " ' s.a.t.3 . Rossini 4d./85 . Austrian National Hymn fe Hay ydn 44 . gr 
 41 . kyrie , ancient modern .. « Gill 2d.| 86 . Carol . s.s.c . ... 5 % . Joseph Robinson 4c . m 
 42 . sun ofmy soul . S.A.T.B. ase . Brinley Richards 2d.| 87 . bright- hair'd Morn . a.1.7.b. Theodor L. Clemens 3¢ sh 
 43 . ’ twas fancy , ocean spray . S.A. 1.8 . G. A. Osborne 2d . | 88 . oh Rest ( ' ' Velleda " ) . , ee .. C.H.Lenepveu 44 . Ke 
 44 . prayer sea . S.A.T.B. 2d.|89 . love reigneth . T.T.1.B. .. - .. C.G. Elsasser ( 4 . > 
 45 . OThou , - Mos¢in Egitto ’ ) Rossini 2d.| 90 . Joy Waltz . teer : 4s . Gd 
 46 . Guard Rhine . s.a . T.5 « G.A.Macfarren 1d.|91 . Star Bethlehem ( Christmas Carol ) ' Theodor 1 Cc ! emens 
 47 . german Fatherland . s.a . t. b. ae 99 1d , ! 92 , busy , curious , thirsty Fly . 1 .. 1.b. ie 3d . 
 th 
 St 
 N ° | 
 LONDON : ) 
 ce : 
 CHAPPELL & CO . , 50 , NEW BOND STREET , W. : 
 City BrancH—14 15 , POULTRY , E.C. ol 
 Mi 
 ( E.C. ) se 

 print Nov. ELLO , Ewer Co. , , 69 & ¥ 0 , Dean Street ( W. ) , publish att , , Berners Street ( Ww . ) , 80 & 81 , Queen Street 
 sell Kent ' Co. , , Paternoster Row ; Ditson Co. , Boston , New York , Philadelphia,.—Wednesday , August 1 , 1883 

