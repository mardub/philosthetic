MUSICAL TIMES 
 SINGING - CLASS CIRCULAR . 
 FEBRUARY 1 , 1878 

 BEETHOVEN MODERN SCHOOL . 
 H. HEaTHCOTE STATHAM 

 Amonc composers critics represent 
 modern “ ‘ spasmodic ” school ( adopt 
 epithet originally applied somewhat similar 
 development poetry ) regarded 
 settled unquestionable fact musical 
 generation Beethoven father 
 aggressive class musical reformers 
 prophets . coryphzus school — musician 
 , justly , conquered him- 
 self highest pedestal fame living 
 composers — distinctly posed successor 
 Beethoven . Wagner Opera , 
 told , Beethoven Symphony turned proper 
 channel , prove fertilising 
 stream future development music . 
 literary satellites revolve round sun 
 Bayreuth matter plainly 
 definitely . Instrumental music , assure , 
 spoke possible word penultimate 
 movement ‘ ‘ Ninth Symphony . ” composer 
 recognised fact ; , adding 
 final movement chorus obtain expression 
 ideas , stamped unaided instru- 
 mental form deficient . 
 resumption attempt purely instrumental 
 composition regarded progress back- 
 wards , attempted revival form art 
 deficient results illogical basis , 
 finally shelved ; career Beet- 
 hoven considered interesting valuable 
 mainly passage Wagnerian form , 
 transition restricted “ dance - forms ” ’ 
 composition vastness “ ‘ forest melody ” 
 unmarked form unshackled rhythm 

 early day express definite con- 
 clusions real value new form 
 dramatic music stir 
 world , place destined occupy 
 history art . possible , 
 limits article , 
 question necessity rhythmic form music , 
 involving philosophical disquisition 
 meaning basis art . 
 light Beethoven regarded 
 modern school , absolutely relation 
 theories , tangible comprehensible 
 subject discussion , 
 importance musical criticism ; 
 regard instrumental music end asa 
 means , aims achievements composer 
 , admit , carried highest point , 
 lasting interest history analysis 
 art 

 old notion Beethoven composer 
 essentially abnormal , mystical , given wild 
 flights startling surprises , hardly 
 eliminated popular musical mind . Whilst 
 misconstrued , 
 vaguely kind survival criticisms 
 directed orthodox 
 dilettanti half century ago , find genius 
 placed kind false half light 
 earnest esoteric school critics , represent 
 employed continual struggle 
 music meaning eventually found 
 possible addition words , ‘ tone - poet 

 limited shackled art fetters 
 musical form construction tended con- 
 stantly evade rise , achieving 
 highest efforts latest compositions 
 realised emancipation . 
 Waiving , observed , abstract question 
 musical forms , contended 
 view Beethoven position art absolutely 
 erroneous inconsistent facts presented 
 works ; essentially artist 
 temperament method , aiming highest 
 detailed finish greater 
 compositions ; clearness 
 musical form sacrificed ( later 
 works ) endeavour direct 
 emotional expression , represent advance , 
 deterioration , compared 
 form complete comprehensive 

 clause indictment course 
 points subjects opinion 
 criticism , questions fact . view 
 Beethoven probably re- 
 garded art greater career , 
 strong presumptive evidence 
 favour views advanced — evidence 
 said threefold nature , derived 
 know man artistic 
 habitudes , consideration style 
 compositions taken separately , com- 
 parison general tendency regarded 
 relation chronology 
 production 

 regard traits man 
 safe au sérieux isolated remarks 
 wayward man Beethoven spur 
 moment , petulance contra- 
 diction mystification . 
 remarks idea essentially 
 intentionally ‘ poetic basis ’ ? composer 
 founded ; having said , ‘ 
 fate knocks door ” regard 
 phrase , ‘ Read Shakespeare ‘ Tempest ’ ” reply 
 question key Sonatas : 
 remarks , motive wide 
 interpretation . Atall events , weight 
 expressions , equal weight 
 fact pronounced Handel , prac- 
 tical composers , “ greatest master 
 masters . ” record 
 Beethoven artistic life practice , find 
 shadow reason supposing set 
 idea having Mission , large M. 
 artist thorough respect 
 technique craft art . 
 sneered , moods , Albrechtsberger contra- 
 puntal studies , worked hard 
 ; systematic elaboration ideas 
 curiously shown late composition Finale 
 “ Eighth Symphony , ” brilliant pointed 
 subject appears sketch bald 
 uninteresting form 

 2 — _ | sere aes 

 fingers , “ 
 smoothly . passages 
 sound perlés , like pearl , played fewer 
 fingers ; wish different kind 
 jewel . ” True , lips inveighed occa- 
 sionally ‘ ‘ bravura , ” pre- 
 vent writing finest bravura Sonata 
 existence ( ‘ ‘ Waldstein ” ’ ) ; sus- 
 pected disliked composition 
 bravura et preterea nihil . suggestive , 
 regard question attitude com- 
 poser art , brief note 
 Ries way Finale 
 * * Sonata Appassionata ’’ came existence — 
 distinctly “ poetic , ” certainly , works . 
 taking long walk , composer 
 keeping kind howling , , 
 articulating distinct sounds . Ries 
 asking meant said , “ ‘ 
 thought subject movement 
 sonata . ” got home “ ran 
 piano taking hat , continued 
 storming keys hour , Finale , 
 admire , struck . turning 
 round , time noticing , said , ‘ 
 lesson - day , work . ’ ” 
 “ great movement 
 certain extent actually composed key- 
 board , evolved hour “ strum . ” 
 Sonatas concerning said , 
 * * Read Shakespeare ‘ Tempest 

 thoroughly ( come second 
 section illustration ) Beethoven possessed 
 . essentially artistic spirit work shows 
 acute critical perception means 
 end , need range piano- 
 forte Sonatas ample evidence . 
 cases desirous treating piano 
 orchestra — giving handling some- 
 thing breadth largeness style demanded 
 orchestral composition ( Introduction 
 opening Allegro “ Sonata Pathétique ” 
 marked instance this)—as rule 
 astonishing notice , turning familiar 
 pages , varied entirely suited 
 means mechanism instrument 
 themes ornamental devices 
 set . point , invention orna- 
 mental figures instrument , successor 
 composers piano ( Mendelssohn 
 frankly owned ) chance , 
 Chopin excepted ; , 
 far greater licence style manner Chopin 

 D minor , Op . 31 . fact ex- 
 pression regard works little connected 
 date manner reply 
 merely vague observation turn question 

 allowed , great sameness 
 figure passages compared Beethoven . 
 amid wonderful variety knotty 
 ungracious player . exception 
 Op . 106 Sonata isolated bits 
 late sonatas , passage , 
 brilliant difficult sense , lies 
 hand , presents unnecessary awkwardness 
 form mechanism . Contrast passages 
 written Schumann Brahms piano , 
 superior artistic fitness Beethoven style 
 comparison , speak generally 
 superior effectiveness . aside 
 great volume pianoforte Sonatas , mis- 
 cellaneous solo works , couple 
 , “ Variations Fugue Air 
 ‘ Prometheus ’ ” ( precursor “ Eroica ” 
 Finale ) “ Thirty - Variations Original 
 Theme C minor , ” find groundwork 
 series modern pianoforte effects . * 
 source pianoforte effect , , 
 said invented , 
 Thalberg claims credit , sustaining theme 
 raising dampers hands 
 employed ornamental excursions , 
 suffered enormous abuse popular writers 
 instrument . fact subject Beethoven 
 method treating favourite instrument far 
 large speak paragraph , thar 
 draw attention general evidence 
 affords artistlike method work close 
 attention details craft , import- 
 ance attached finish form suitability 
 means end 

 larger question symmetry form 
 composition , course easy 
 earlier works , Symphonies 
 pianoforte works earlier period , Beethoven 
 symmetrical form 
 Mozart ; recognised 
 later works . symmetrical 
 form meant draws move- 
 ments fixed plan adheres strictly . 
 Popular opinion fact credited 
 dependence precedent earlier works , 
 breaking away later ones , 
 consistent fact . face 
 compositions C minor Trio ( Op . 1 ) 
 major Sonata ( Op . 2 ) , nonsense talk 
 dependent Mozart Haydn outset . 
 youthful productions , 
 repeating style , allow appear 
 catalogue numbered compositions . 
 hand , easy wish 
 gain hearing musical mohstrosities remind 
 Symphonies C minor major 
 derided players executed , 
 concluded non - appreciation 
 works arose fromthe novelty , ears 
 day , extraordinary character themes them- 
 selves , great eccentricity 
 framework composition . movement 
 C minor Symphony irregular 
 plan , certainly ; final movement symmetry 
 . slow movement major 
 Symphony , intensely pathetic 
 thing Beethoven wrote ; find 
 passion allowed break bounds musical 

 “ Thirty - Variations ” Diabelli Waltz 
 events supply deficiency suggestion ; work 
 artistic grounds ranked . larger 
 scale loosely constructed , attracted 
 exaggerated interest account difficulties splendid 
 execution great players ; remark 
 apply Op . 106 Sonata , longest greatest 
 composer pianoforte solos 

 form ? trivial deviation 
 straight path bar — 
 Brass & ® oo 9 
 drums . Wood 

 felt superfluous 
 performance , characteristic , 
 relation feature movement ; 
 bar right 
 place naturally result 
 plan , symmetrical 
 incident chord wind instruments placed 
 opening close movement , portal 
 enter leave strange region 
 sighs consolations . work 
 called ‘ * Moonlight Sonata , ” style 
 form complete novelty 
 Beethoven produced — utterly un- 
 shadowed precedent pianoforte music — 
 find bar note dis- 
 placed , rigidly keeping 
 plan structure . form new , 
 complete form , strictly adhered 
 , find works 
 Bach , , come later series , 
 sets variations E C , Sonatas Op . 109 
 111 , latest pianoforte 
 solos Beethoven symmetrical form 
 Sonatas . * tempting 
 pleasant task carry argument calling 
 attention wonderful delicate elaboration 
 detail great Symphonies ; note 
 instrument individual precisely 
 suited genius ; observe infinite 
 variety little beauties graces 
 subordinate design , obtrude 
 : space forbids 

 ‘ Ninth Symphony ’ ” — modern 
 critics — * Beethoven work lead 
 , Finale expressly quit 
 field regular form pure instrumental 
 music , acknowledge thing played 
 ? ’ look absurdity argument . 
 Symphony wrote : chorus 
 , instrumental music 
 end . Suppose died writing “ Pastoral 
 Symphony ; ” course “ arrived 
 conclusion right province instrumental 
 music description nature . ” Suppose 
 died production ‘ Choral 
 Fantasia , ” , “ ‘ Beethoven 
 recognised pianoforte , unsupported voices , 
 place music future : ” 
 fortunately lived write finest 
 solo Sonatas . , Concerto 
 E flat greatest , 
 repeated ad nauseam recognised 
 old Concerto form worked , said 
 word form ; unfortunately M. Thayer 
 discovered manuscript nearly 
 movement later Concerto existence , 
 orthodox form far goes , opening 

 remarks written chanced 
 following passage , certainly apropos , Beethoven notes 
 contrapuntal studies , shows symmetry form 
 best works result deliberate ony formed judgment . 
 Excusing neglecting strict rules 
 theorists , says , ‘ “ ‘ Let supposed advocate imper- 
 tinent contempt great principles art , unchangeable . 
 time advances art advanced 
 things . Invention fancy denied rights 
 privileges schoolmen , theorists , barren critics 
 gladly deprive . ... advise composer 
 commonplace far - fetched ideas , oy bombastic 
 expression 

 engraved Crystal Palace pro- 
 grammes , piano commences 
 chain shakes , piece immoral bravura 
 display ! theories facts . 
 idea progress pure 
 music “ poetic basis ” music Beethoven , 
 evidence theory consistently carried , 
 illusion . Twist , fit . 
 “ Eroica ” admittedly “ poetic ” Sym- 
 phony , Finale irregular captious 
 form : Symphony 
 finished work wrote , purely 
 formal Symphonies . solo piano- 
 forte work ‘ “ Adieux Sonata ” 
 Polonaise C ; variations close 
 sonata purely music sake 
 music , devoid effort inner meaning , pre- 
 cise form , toccata Bach ; , 
 Diabelli Variations — piece 
 genre , inferior — progress contem- 
 poraneously “ Ninth Symphony . ” 
 extraordinary work , movement , 
 large vague outline , moulded 
 consistent form ; Scherzo nearly complete 
 form ; slow movement ex- 
 quisitely finished symmetrically developed , 
 beautiful , composer 
 works . regard Finale venture 
 assert , speaking reverential trust 
 unintelligent lover Beethoven , spite 
 noble pure beauty leading theme , 
 glimpses sublime musical perspective 
 open progress , movement 
 asserted ; 
 worthy position given 
 composer works ; 
 crude , harsh , formless ; 
 hear feel ( confess truth ) certain 
 sense relief ; educated 
 audience pretend think composer 
 crowning work allow 
 led clique critics theory 
 uphold 

 special point Beethoven 
 misrepresented said school critics . 
 try accomplice pre- 
 paration modern receipt composition called 
 ‘ * ‘ metamorphosis themes . ” contrivance 
 composer deficient original 
 spontaneous melodic ideas , 
 lucky hit melody , duty 
 movement a'whole Symphony , 
 repeating varieties form andrhythm , cutting 
 note putting , mauling 
 ways ; nct exercising wholesome 
 economy use ideas , gaining 
 credit having imparted remarkable “ unity 
 expression ” composition . crutch 
 found useful lean literary 
 followers Liszt Wagner felt compelled , 
 proving idols hereditary successors 
 Beethoven , leaned 
 crutch . instructive efforts . 
 , known - class pianist 
 writer indifferent English , play- 
 ing heard seldom English seen 
 , attempted little ago cut Beethoven 
 fit theory , article directed end 
 Macmillan ' Magazine . examples 
 critic able support 
 view , scarcely credited 
 attempt theme Fugue 
 B flat Sonata repetition theme 
 Allegro , arbitrarily picking certain notes 
 scale passages fugal theme empha 

 68 MUSICAL TIMES.—Fesruary 1 , 1878 

 

 2 . 
 Granting asserted resemblance passages , 
 easy , 
 “ theme , ” hardly “ phrase ; ” mere 
 battery chords close movement . 
 absurd critic , anxiety 
 discover looking , forgot fact 
 Adagio original movement — 
 Allegro originally followed well- 
 known Andante F , discarded 
 sonata long , present short 
 Adagio substituted . idea 
 movement evolved pro- 
 gress composition set aside . 
 straits people driven de- 
 termined find artificial theories Beethoven 
 music 

 foregoing remarks merely glance suggest 
 view subject far large illustrated 
 detail . , believed , unbiassed 
 study life works Beethoven , apart 
 attempt theory , shows 
 poet feeling expression , 
 essentially consummate artist musical form — 
 recognise , instances 
 latest compositions comparatively neglected 
 form finish , fallen 
 usual mark effect competent unpre- 
 judiced listeners — conclude , 
 future music store , 
 theory art excludes symmetrical form 
 finish subordination detail 
 claim based art Beethoven , 
 sense amplification systematic 
 reproduction tew comparative failures 

 MUSIC CONNECTION DANCING . 
 Cuarves K. SALAMAN . 
 ( Continued page 16 

 science telephones , 

 Beethoven Mendelssohn ? ” ‘ 

 pretelephonic mind “ oddity ” amazed 

 slightest desire deprive artists 
 legitimate claim public 
 patronage support ; help thinking 
 , advertise hold public 
 positions profess , occasion 
 publish fact ; , sooner 
 drop self - created titles better . 
 lately met suffice 
 illustrations . Passing old - established 
 “ celebrated bass , ” , aught know 
 contrary , “ celebrated ” musical regions 
 duty lead , light 
 vocalist , certainly known con- 
 cert - room , calls ‘ principal con- 
 tralto , Royal Italian Opera , Drury Lane ; Royal 
 English Opera , Covent Garden ; Sacred Harmonic 
 Society , & c. ” ” , assumed , 
 occupied position lyrical establishments , 
 shall glad enlightened matter ; 
 , record Italian Opera - houses 
 , “ principal contralto ” 
 Englishwoman . startled adver- 
 tisement ‘ ‘ song - writer composer , ” 
 songs told , seventeen years “ 
 rage quarter globe 
 proved principal successes singer note 
 appeared . ” know enormous 
 number vocalists indebted 
 gentleman ‘ ‘ successes , ” 
 presume “ ‘ singers note . ” , 
 styles ‘ - class pianiste vocaliste , ” 
 inserts “ M.andA.C.P. , ” 
 letters signify remotest 
 idea . , heard teacher , 
 letters look like kind diploma ; 
 , employ , 
 ask mean 

 letter friend Dwight Boston , Mr. 
 Thayer reveals particular motive led 
 write read Trieste paper Becthoven 
 familiar , substance , readers . 
 cause Herr Nohl , voluminous biographer 
 undoubted industry quoted 
 implied accuracy . Mr. Thayer allowed 
 Herr Nohl years printing slander 
 vituperation , canstandit nolonger . ‘ 
 publishing , ” writes eminent American , 
 “ articles newspapers monthlies , 
 delivering lectures writing volumes Beethoven , 
 finally volume biography , 
 embraced possible occasion exhibit 
 Beethoven brothers wives , 
 nephew , worst possible light . ” Mr. 
 Thayer ‘ felt duty , best occasion 
 baseless slander 
 vituperation endeavoured over- 
 whelm memory Johann van B. infamy . ” 
 adds , ‘ ‘ desired subject opinions 
 opposed prevailed years 

 MUSICAL TIM 

 77 

 relations brothers 
 Beethoven Maelzal thorough criticism 
 giving permanent place large work . ” 
 work promised shall wait anxiously ; 
 instalment 
 justice ; know Herr Nohl , 
 writes , lacks - balanced mind 
 necessary master craft , instead 
 ‘ biographer unreasoning worshipper 
 idols , speck flaw . 
 ‘ regarded light Mr. Thayer 
 ‘ ‘ Contribution Beethoven Literature , ” 
 calm convincing array facts , sufficient 
 prove 

 _ — _ 

 desire ventilate opinions 
 numerous correspondents system — 
 want system — printing “ accidentals ” 
 music , find useless insert 
 letters received 
 subject , multiply instances 
 careless manner , rule , temporary 
 contradictions original signature marked — 
 scarcely , , composers agreeing 
 method . Musical reformers know loth 
 admit interference liberty 
 subject ; important question “ pitch ” 
 continues settled according opinion 
 Conductor . years ago Sterndale Bennett , 
 seeing grave objection slur 
 bind represented sign , 
 slightly altered bind , per- 
 fectly distinct ; public opinion 
 | strong , ‘ “ ‘ new thing ” ’ — 

 thetic critic points “ nature | heard termed — completely disappeared 
 grand cataclysms , ” anda grotesque bassoon passes | rom recent editions _ works . _ 
 Caliban , Professor Ariel — * “ | called declare opinion subject 
 pert little fillip piccolo”—by means finds | “ accidentals , ” “ accidental 
 prompt recognition . critic received “ fillip ” | affects note placed sub- 
 boatswain whistle , spoke ; | S¢quent note pitch 
 , taking conjunction | bar . ” think advisable extend 
 preceding harp chords , launches poetic | influence accidental following bar 
 reference Ariel harp Caliban squeal . ; circumstances ; , instances 
 teally hard critics liable | ™ ay occur point note 
 mistakes , composers vague | influenced — , example , note 
 sketches stand things | concludes bar altered , 
 intended , imitate | bar begins note — repeti- 
 ‘ young modest draughtsman labelled | tion sign effectually remove doubt . 
 nondescript figures “ ‘ cat ” “ cow ” according 5 
 intention . Seriously , music degraded } believe annual publication 
 nonsense , men talent persist in| called ‘ ‘ Musical Directory ” 
 making noble art showman business , | kind issued surprised hear 
 ought independent attraction , incur grave | “ ‘ Musical Directory year 
 responsibility . |1794 ; ” prove valuable , 
 |its rarity , mention known 
 lately drawn attention | copy work library Sacred Har- 
 apocryphal stories told Beethoven , | monic Society . Apart scarcity , , 
 worth lively ingenious | book interest musical antiquarian , 
 fancy dealt Weber . time ago | accurate reflection state art 
 -veracious journal Le Gaulois came most|the time published presented glancing 

 surprising tale anent author “ Der Freischiitz . ” 
 appears Weber London 1811 — matter 
 biographers tell — 
 day , walking banks Thames , 
 took flute pocket began play . 
 approach young officers , , 
 everybody knows , uniform , 

 MONDAY POPULAR CONCERTS 

 opening Concert new year , took 
 ‘ place 7th ult . , Mdlle . Marie Krebs 
 appearance season instrument 
 , early age , associated . lady 
 pianist played marked refinement power execu- 
 tion Bach Italian Concerto pianoforte ; 
 : , company Madame Norman - Néruda 
 Signor Piatti , exponent Beethoven Trio B 
 flat ( Op . 97 ) , special favourite audience 
 concerts , amateurs generally . 
 performance , need scarcely , left 
 ‘ desired , hear work soon repeated 
 hands consummate artists . Signor 
 Piatti splendid tone calm artistic style appeared 
 fullest advantage rendering Sonata 
 Boccherini , applause followed 
 proved high estimation performer 
 held , proof needed . Mozart 
 String Quartett major ( . 5 ) formed 
 evening programme , executants Madame 
 Norman - Néruda , MM . Ries , Straus , Piatti 

 second evening concert month brought 
 welcome repetition Cherubini Quartett D minor , 
 stringed instruments , referred notice , 
 executants previous occasion . 
 Mdlle . Krebs , pianist , called forth 
 storm applause characteristic rendering 
 Gavotte variations ( minor ) Rameau , 
 , known history art , 
 figures rarely concert programmes . 
 interesting revivals kind gratefully received 
 musical public clearly proved 
 occasion question . Mdlle . Krebs judiciously responded 
 required encore substituting ‘ * Tambourin ” 
 representative Old French school . 
 lady associated MM . Straus , L. Ries 
 Zerbini , Piatti Brahms Quintett F minor , 
 follows matter course work 
 gifted German composer received excellent interpretation 

 Mr. Barton M‘Guckin turned sympathetic tenor voice 
 good account Romance Benedict songs 
 Mendelssohn . Monday 2ist ult . programme 
 headed interesting novelty , String Quartett 
 Signor Verdi , , original form , heard 
 time occasion , having previously , 
 exercise somewhat questionable taste , performed 
 orchestral complement stringed instruments 
 Crystal Palace Concerts . energetic ap- 
 plause , occasion performance 
 Popular Concert referred , accorded move- 
 ment Quartett sufficiently testified appreciation 
 numerous audience . confess 
 inability share inthis enthusiasm . work , byno 
 means deficient pleasing melodious phrases , loosely 
 held component parts ; want con- 
 tinuity thought apparent movement , per- 
 haps Fuga Finale , modern Italian 
 maestro proves deficient 
 knowledge counterpoint . Prestissimo , 
 incidental solo violoncello , sprightliness 
 animation , disturbed diversified 
 triviality melody assigned solo instru- 
 ment . interesting chamber work com- 
 poser ‘ ‘ Aida ” capitally played Madame Norman- 
 Néruda , MM . L. Ries , Straus , Piatti . remainder 
 programme consisted Sonata Handel ( 
 major ) violin , exquisitely played Madame Norman- 
 Néruda ; Beethoven Sonata G major ( Op . 29 ) 
 pianoforte , executed accustomed skill Mr. 
 Charles Hallé , , inj conjunction Madame Néruda , 
 concluded evening proceedings enjoyable 
 interpretation Bach Sonata major ( . 2 ) 
 pianoforte violin . Mr. Santley - known vocal 
 declamatory powers duly appreciated Mr. 
 Cowen ‘ “ ‘ Rainy day , ’ Schubert “ Erl - King . ” 
 evening month , 28th ult . , Herr Ignaz 
 Brill , eminent pianist successful composer 
 opera , appearance . , 
 , defer notice concert question 
 number 

 MAJESTY THEATRE 

 Music distinguished fact musi- 
 cal sounds result rhythmic movements 
 air , largest sense old theory ‘ music 
 spheres ’ application scientific fact 
 movements planets universe ; pro- 
 duct grand laws rhythmic motion , dis- 
 tinguish musical sounds opposition accidental ones , 
 attention rhythmical divisions notes 
 matter great importance 

 T think pupils high credit teachers , , 
 having recently engaged long examination 
 pupils , find - day great merit , 
 especially Adagio Beethoven Sonata . 
 songful piece piano , chief melody re- 
 quiring effect produced like asong . isa 
 high quality playing produce singing cha- 
 racter expression . excelled 
 Adagio , shows musical feeling , highly 
 worth cultivation given . equal 
 gifts , music powers varied , 
 rarely known persons totally musical apti- 
 tude . knew family utterly devoid power 
 perceiving musical sounds , unusual 
 case , persons called music deaf . 
 people able appreciate music degree ; 
 sure cultivation raise power far 
 higher standard rude natural state . 
 dangerous bad teaching gain bad habits ; 
 pupils fortunate having lady 
 head music - school distinguished musician high 
 esteem profession . proofs care 
 qualifications great cause confidence 
 skill teacher director teachers . think 
 institution fortunate having secured services , 
 thank Miss Macirone , profession , 
 work cause music , 
 care bestowed progress pupils 
 school 

 T know nervousness necessarily attends 

 Royal Society Musicians Great Britain , insti- 
 tuted 1738 maintenance aged indigent 
 musicians , widows orphans , monthly 
 meeting members apportioned sum exceeding £ 400 
 relieve wants distressed members musical 
 profession families , nearly fourth 
 granted non - members 
 legal claim onits funds . noble Society hold 
 140th Anniversary Festival Dinner 3rd , 
 Right Hon . Sir Alex . J. E. Cockburn , Lord Chief 
 Justice England , preside ; hoped 
 derive pleasure profit art 
 music contribute liberally aid funds 
 charitable Association work thoroughly 
 unostentatiously 

 concerts Philharmonic Society announced 
 commence St. James Hall 14th inst . 
 instrumental portion programmes con- 
 certs Easter given , detailed prospectus 
 issued . pianist new country , M. 
 Planté , perform Beethoven Concertos 
 concert , novelties mentioned . : 
 lovers standard works , , selections 
 concert present strong attraction ; 
 cling time - honoured traditions Society 
 glad find hour commencement 
 restored o’clock , morning con- 
 certs discontinued 

 seventy - monthly Concert Grosvenor 
 Choral Society held Grosvenor Hall Friday 
 18th ult . , principal feature ‘ ‘ Rose 
 Salency , ” Operetta written composed W. Chal- 
 mers Masters . work performed , band 
 chorus numbering 110 performers . soloists , . 
 Mrs. Stamp , Miss Kate Reed , Mr. A. Lawrence Fryer , Mr .. 
 W. Powell , Mr. Henry Baker , successful 
 respective parts . miscellaneous portion the- 
 programme included - songs vocal solos ; 
 instrumental pieces Mendelssohn ‘ ‘ Wedding March ” 
 Méhul Overture ‘ ‘ La Chasse du Jeune Henri , ’’ 
 played band . Mr. J. G. Callcott conducted 

 FOREIGN NOTES 

 present protracted period stagnation com- 
 merce , consequent want prosperity , 
 common countries , Germany 
 passing , fail injuriously felt 
 sphere artistic theatrical enterprise . 
 hear minor establishments appertaining 
 category obliged close doors , 
 notably Stadt Theater Bremen Gorlitz , 
 Carl Schultze Theater Hamburg . reason 
 projected foundation Bayreuth model school 
 music , inaugurated 
 present year , adjourned 
 period . Herr Wagner , letter directed 
 ‘ * Society Patrons ” undertakings , an- 
 nounces fact faithful allies , addi- 
 tion expresses hope new drama , ‘ “ Parsifal , ” 
 performed summer 1880 Bayreuth 
 Theater . hand there.is lack activity 
 shown directors art - establishments 
 subsidised Government sufficiently 
 supported public little affected 
 existing unfavourable circumstances . 
 class Stadt Theater Hamburg 
 time conspicuous enterprise production new 
 operatic works judicious revival old classics . 
 month , occasion - hundredth anniver- 
 sary foundation , institute question marked 
 event ‘ ‘ historic week opera , ” works 
 selected performance ‘ ‘ Venus und Adonis , ” 
 Keiser ; ‘ Almire , ” Handel ; ‘ Der betrogene 
 Cadi , ” Gluck ( night ) ; ‘ Die Jagd , ” 
 Adam Hiller ; ‘ ‘ Apotheker und Doctor , Ditters- 
 dorf ( second night ) ; ‘ ‘ Adrian von Ostade , ” Weigl ; 
 “ ‘ Die Entriihrung ” Mozart ( night ) ; ‘ Fidelio , ” 
 Beethoven ( fourth night ) ; ‘ ‘ Der Holzdieb , ” 
 Marsehner , Weber ‘ “ Freischitz ” ( fifth night ) ; 
 Wagner “ Lohengrin ” ( sixth night ) . 
 theatre course preparation following new 
 works contemporary German composers : ‘ Armin ” 
 ( Hofman ) , “ Golo ” ( Scholz ) , ‘ “ Die Walkire ” 
 ( Wagner ) . Cassel Court Theater interesting 
 series historic performances opera , referred 
 notes , continues . new comic opera entitled 
 ‘ * Die Offiziere der Kaiserin ” ( Officers Empress 

 Richard Wuerst , performed 
 2rst ult . , novelty 
 month Imperial Opera Berlin . Referring 
 fact , Le Menestrel thinks new work form apt 
 pendant Halévy “ ‘ Mousquetaires de la Reine , ” 
 lately revived Paris Opéra - Comique 

 Herr Brahms new Symphony ( . 2 ) lately 
 performed time Vienna Leipzig , 
 work exceedingly received , pro- 
 nounced connoisseurs superior composer 
 previous symphonic tone - picture 

 important sale autographs held 
 26th inst . Leipzig . collection , 
 interesting brought hammer , 
 include . names Beethoven , Mendelssohn , 
 Mozart . originally possession ot 
 Consul Wagener , patriotic bequest 
 valuable collection paintings State laid 
 foundation Berlin National Gallery 

 Franz Liszt Pesth , devoting 
 time instruction pianoforte 
 talented pupils , natives Hungary , 
 celebrated pianist tone - poet Weimar 
 Easter , Italy , returning Pesth 
 autumn . invitations perform concerts 
 positively declined 

 subjoin programmes concerts 
 taken place past month leading 
 institutions abroad 

 Paris.—Concert Populaire ( December 30 ): Symphonie 
 fantastique ( Berlioz ) ; Pianoforte Concerto E flat ( Beet- 
 hoven ) ; Le Désert , Ode symphonique ( David ) . Concert 
 du Conservatoire ( January 6 ): Symphony G minor 
 ( Mozart ) ; Overture ‘ ‘ Le Roi d’Ys ” ( Lalo ) ; Symphony 
 C minor ( Beethoven ) . Concert Populaire ( January 6 ): 
 Ode St. Cecilia ( Handel ) ; Overture ‘ “ Fingal ” 
 ( Mendelssohn ) . Concert du Chatelet ( January 6 ): ‘ ‘ La 
 Damnation de Faust ” ( Berlioz ) . Concert Populaire 
 ( January 13 ): Symphony C ( Beethoven ) ; Overture 
 ‘ “ * Manfred ” ( Schumann ) ; Fragments ‘ ‘ Seasons ” 
 ( Haydn ) . Concert du Conservatoire ( January 20 ): Sym- 
 phony G flat ( Schumann ) ; Fragment ‘ ‘ Orpheus ” 
 ( Gluck ) ; Symphony C major ( Beethoven ) . Concert 
 Populaire ( January 20 ): Symphony G ( Haydn ) ; Music 
 * Egmont ” ( Beethoven ) ; Overture ‘ * Francs - Inges ” 
 ( Berlioz 

 Leipzig.—Concert Gewandhaus ( January 1 ): 
 Prayer Martin Luther , orchestra chorus ( Men- 
 delssohn ) ; Overture , Op . 124 ( Beethoven ) ; Motett ( Chr . 
 Bach ) ; Concerto , played composer ( Brahms 

 Berlin.—Concert Symphonie - Kapelle ( December 
 15 ): Dramatic Symphony , ‘ ‘ Romeo Juliet ” ? ( Berlioz ) ; 
 Chorus ‘ ‘ Die Meistersinger ’ ” ’ ( Wagner ) ; Violin Con- 
 certo ( Bruch ) . Concert Sing - Akademie ( January 11 

 rincipal artists Mdlle . Alwina Valleria , Madame Lablache , 
 Malle . Parodi , Signor Runcio , Signor del Puente , Signor Rocca , 
 Signor Foli , M. Musin ( violinist ) , Signor Li Calsi ( pianist ) , 
 highly successful , Mdlle . Valleria Madame Lablache 
 receiving warmest applause , Signor Foli eliciting double 
 encore songs . members Society , 
 conductorship Sir Robert Stewart , performed great 
 credit satisfaction audience . concert 
 , , exceedingly enjoyable 

 BrirMINGHAM.—The annual performance Messiah 
 Festival Choral Society took place Town Hall , Wednesday , 
 December 26 . principal vocalists Mesdames Sinico 
 Patey , Mr. Vernon Rigby , Mr. Lander . Choruses 
 sung , orchestra good , Mr. Robinson trumpet obbligato 
 particularly admired . Mr. Stockley conducted skill 
 judgment . — — following evening Philharmonic Union gave 
 Mendelssohn Elijah , Miss Emma Beasley , Miss Orridge , 
 Mr. Seligmann , Mr. Cross principals . Miss Beasley sang 
 soprano solos taste effect , particularly Air “ Hear 
 ye , Israel . ” Miss Orridge , ber dramatic feeling , created marked 
 impression ; Messrs. Seligmann Cross acquitted 
 . concerted pieces , Mrs. Bellamy , Miss E. Bailey , 
 Mr. Woodhall , Mr. Carless assisted , remarkably given , 
 delicate Choruses beautifully sung , power 
 requisite heavier numbers . band efficient . Dr. 
 Heap conducted concerts , Mr. Stimpson rendered valuable 
 aid organ.mMessrs . Harrison , engage 
 artists subscription concerts , disappointed Madame 
 Nilsson Herr Wilhelmj concert season 
 3rd ult . , gave , compensation subscribers , gratis Invita- 
 tion Concert , Miss Robertson , Mdile . Enriquez , Mr. E. Lloyd , 
 Mr. F. H. Celli , M. Henri Ketten , Herr Wilhelmj engaged . 
 programme excellently performed . M. Ketten created 
 furore pianoforte - playing , Herr Wilhelmj met 
 usual enthusiastic reception . Encores numerous , 
 liberality concert - givers thoroughly appreciated 
 subscribers.——An able interesting lecture “ Musical 
 Drama ” given Mr. E. J. Breakspear members 
 Birmingham Fine Arts Guild gth ult . progress 
 musical drama traced earliest Greek plays latest 
 development theories Wagner.——Dr . Swinnerton Heap 
 gave Pianoforte Recital theatre Midland Institute 
 Thursday toth ult . programme included examples 
 Mozart , Beethoven , Schumann , Chopin , Schubert , Mendelssohn , 
 Reinecke , Raff , Rheinberger , Heller . admirably 
 executed , performer warmly applauded . Vocal solos 
 contributed Mr. J. H. Kearton , place Mr. Vernon Rigby , 
 suffering hoarseness . Mr , Pearce accompanied 

 BrRADFORD.—Mr . Midgley gave second Chamber Concert 
 Church Institute 7th ult . executants , addition Mr. 
 Midgley , , violin , Herr Ludwig Straus ; viola , Herr Otto Bern- 
 hardt ; violoncello , M. H. Vieuxtemps . programme con- 
 sisted exclusively chamber music , concert opened 
 Quartett E flat piano , violin , viola , violoncello , composed 
 Mr. A. C. Mackenzie , Edinburgh , new Bradford 
 audience . Beethoven Trio Strings ( Op . 9 , . 1 ) ; Bach Sonata 
 , piano violin ; Mendelssohn Variations Concertantes 
 ( Op . 17 ) D , piano violoncello ; Beethoven Trio D 
 Op . 70 , . 1 ) , piano , violin , violoncello , artistically 
 played , enthusiastically received 

 BRIGHOUSE.—A successful performance Handel Oratorio 
 Samson given Town Hall Choral Society 
 December 28 . artists engaged Mrs. Brook Myers , Madame 
 ‘ Galli , Mr. Verney Binns , Mr. Andrew McCall . Mr. O. Sladdin 
 conducted , Mr. Bowling led band 

 CARNARVON.—A grand Concert given recently erected 
 Pavilion 12th ult . following artists : Mdlle . Alwina 
 Valleria , Madame Lablache , Mdlle . Parodi , Signor Runcio , Signor 
 del Puente , Signor Rocca , Signor Foli , M. Musin ( violin ) , Mr. F , 
 H. Cowen ( pianist ) . programme contained selection Songs , 
 Duets , Quartetts , & c. , rendered efficient 
 manner , received greatest appreciation . M. Musin 
 violin solos warmly redemanded , brilliant execution gaining 
 admiration . instrumental portion concert , includ- 
 ing Mr. Cowen pianoforte accompaniments , treat 
 rarely met Principality 

 Cuiirton.—On Tuesday 8th ult . Classical Concerts 
 given Victoria Rooms Mr. J. C. Daniel . artists 
 Madame Norman - Néruda ( violin ) , Mr. Charles Hallé ( pianoforte ) , 
 Herr Franz Néruda ( violoncello ) , Mdlle . Enriquez ( vocalist ) , 
 programmes excellently arranged , included Beethoven 
 Grand Trios D , Op . 70 , . 1 , B flat , Op . 97 ; Mendelssohn 
 Grand Trio C minor , Op . 66 , Haydn G , Schubert Notturno 
 E flat . Mr. Hallé performances highly appreciated , 
 solos . Mdlle . Enriquez gave vocal music 
 usual artistic style — — annual Ladies ’ Night Bristol Madrigal 
 Society given Victoria Rooms Thursday roth ult . , 
 , usual , greatest treats afforded local musicians 
 course season . programme consisted composi- 
 tions , believe , previously given 
 Society , fact encores demanded 
 conceded testifies popularity selection . older 
 Madrigal writers represented Morley , Hilton , Weelkes , Luca 
 Marenzio , Wilbye , Ford , Saville . Samuel Wesley “ O sing unto 
 mie roundelaie ” represented later date , - songs 
 modern composers ( Leslie , Mendelssohn , Smart , G. A. Macfarren ) 
 included selection . slight 
 memory late R. L. de Pearsall , earliest 
 members Society , wrote popular 
 compositions , open night given works 
 performed . occasion charming Madrigal , ‘ saw 
 lovely Phillis , “ Sir Patrick Spens , ” repeated , Part- 
 song , ‘ O o’er downs free , ” included 
 programme . Mr. E. A. Harvey , Secretary , congratulated 
 excellence arrangements , Mr. D. W. Rootham 
 efficient manner conducted 

 Coventry . — Christmas Services St. John Baptist 
 Church commenced - choral evensong Eve , 
 Gounod “ Bethlehem ” sung Anthem , Matins 
 Christmas Day Sullivan Te Deum D E. H. Thorne Anthem 
 ‘ beginning Word . ” choral celebration Holy 
 Communion , Marbeck music , concluded Ser- 
 vice . Processional recessional hymns sung 
 Festival Services . choir , direction Mr. J. Finch 
 Thorne , performed musical portions Services careful 
 reverent manner.——A performance Messiah given 
 New Year Day Corn Exchange Musical Society . 
 solos sung Miss Agnes Larkcom , Madame Patey , Mr. Vernon 
 Rigby , Mr. Thurley Beale . band chorus , consisting 
 nearly 170 performers , highly efficient . Mr. Arthur Trickett , 
 F.C.O. , conducted 

 New Brompton , Kent.—An Evening Concert given 
 Public Hall , Tuesday 16th ult . , successful . 
 Overture , improvised selection , Mr. F. G. Chant , pianist 
 accompanist , admirably executed ; vocal music 
 sung Mr. H. Hoare Concert Glee Party , assisted Miss 
 Cissie Colesworthy . young vocalist possesses soprano voice 
 power considerable compass . songs enthusiasti- 
 cally encored . Mr. Tom Jobling Mr. W. H. Rowe fully deserved 
 applause received 

 New York . — Theodore Thomas Symphony Concert 
 Steinway Hall , 5th ult . , included following excellent works , 
 rendered : select movements Handel , 
 J. S. Bach Concerto pianos , performed Messrs. R. Hoff- 
 man , W. Mason , F. Dulcken ; masonic music Mozart ; Over- 
 ture , Coriolan , Beethoven ; Symphony Brahms , Op . 68 

 NortH MALverN.—The new organ , built Mr. John Nicholson , 
 Worcester , Holy Trinity Church , opened 15th ult . 
 Mr. W. Haynes , Organist Priory Church , Malvern . 
 choral service morning , 3.30 Organ Recital 
 given , excellent programme classical music performed 
 crowded audience 

 READY 

 BEETHOVEN FIDELIO 
 ( German English words 

 AUBER FRA DIAVOLO 
 ( French English words ) . 
 MOZART DON GIOVANNI 
 ( Italian English words ) . 
 BELLINI NORMA 
 ( Italian English words ) . 
 VERDI IL TROVATORE 
 ( Italian English words ) . 
 DONIZETTI LUCIA DI LAMMERMOOR 
 ( Italian English words 

 IX TRANSCRIPTIONS ORGAN ( 

 pedal obbligato ) works Beethoven , Handel , Mendels 

 sohn , andSpohr . GreorGe Sunn , Organist Brixton Church . 
 Price 2s . 6d . net . London : Novello , Ewer Co 

 MUSICAL TIMES.—Fesruary 1 , 1878 

 BEETHOVEN 

 SONATAS 

