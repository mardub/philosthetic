tions that this would be the chief amusement of the 
evening, the attendance at such a “ musical party” 
would be extremely limited

Ir, on the one hand, it is good that a competent 
writer upon a subject should prove his knowledge, it 
is equally good, on the other hand, that an incom- 
petent one should conceal his ignorance. Those in 
the habit of reading country newspapers, however, 
must be aware that, although the musical notices are 
but rarely entrusted to persons who know anything ot 
the art, technical words are constantly used which 
have no real bearing upon the subject, the most 
daring opinions are hazarded, and standard works 
are often criticised as if they had but just been 
brought before the public. It is true that when we 
read, as we have recently done in a London paper, 
that the actors in a farce at an amateur performance 
were ‘repeatedly encored,” it may be said that 
writing of the kind we have mentioned is not confined 
to reports on musical matters; but there can be no 
doubt that it is in remarks upon concerts where the 
critic most effectually displays his inefficiency. We 
recollect, for example, a notice upon Mendelssohn’s 
‘* Wedding March,” which, after a panegyric upon its 
merits, concludes with the bold assertion that it 
“deserves to be better known.” And now we light 
upon the critique on a musical performance in a 
paper published so near to the metropolis as scarcely 
to be called “in the country,” which contains, 
amongst other items—such, for instance, as that two 
young ladies ‘executed to perfection Mozart’s ‘Il 
Flauto Magico’”—an encouraging notice upon a 
composition of which some of us have already heard. 
It is called (we read) ‘‘ Beethoven’s movements from 
‘ Pastoral Symphony,’ ” a duet for two pianofortes. 
“It is a very pretty piece,’ we are told, “in the 
course of which the ‘ Village Dance’ is interrupted by 
a thunderstorm, which suddenly bursts with rain- 
drops and heavy peals, with the gradual dying away 
of the storm, and closes with the shepherds’ song of 
thanksgiving. The storm is natural, the gradual 
change exceedingly good, and altogether the piece is 
very effective.” So that if this work do not become 
“better known” it will be seen that it is not from 
any fault of the critic, who evidently appreciates 
good music when he hears it, and is determined 
to do his duty as a watchful guardian of the public 
taste

WHATEVER may be said of the acoustical properties 
of the Royal Albert Hall, there can be little doubt 
that no other concert-room is to be found in or out of 
the metropolis where demonstrations of such magni- 
tude as those organised for the reception of our dis- 
tinguished foreign visitors can be so successfully 
given. We have often as a duty recorded the mag- 
nificent effect of this building when brilliantly lighted 
and filled in every part by a fashionable and excited 
audience to welcome some person high in position 
and influence; and have now, as a pleasure, to notice 
a gathering, as remarkable both for numbers and 
enthusiasm, to welcome one equally high in the world 
ofart. Mr. Sims Reeves, whose concert on the 4th ult. 
attracted nearly ten thousand persons, is a vocalist 
whose intuitive musical perception has guided him 
so truthfully through a long career that he has not 
only eloquently, expounded but added a charm of his 
own to the tenor music of the many works in which 
he has been engaged. One of the most popular 
vocalists, he has never sought popularity at the

PHILHARMONIC SOCIETY

THE ninth concert, on the 25th June, included a very 
fine performance of Beethoven’s Violin Concerto by M. 
Leopold Auer, and an intelligent and effective rendering 
of Mendelssohn’s Pianoforte Concerto in G minor by Mr. 
Alfred Jaell, the orchestral pieces being Beethoven’s 
** Pastoral Symphony,” Dr. A. Sullivan’s Overture, “ In

Memoriam,” and Weber’s Overture, ‘‘ The Ruler of the 
Spirits.” At the concluding concert of the series, on the 
gth ult., the subscribers had an opportunity of hearing 
Herr Joachim’s ‘“ Elegiac Overture,” upon the merits of 
which we commented when it was given at Cambridge on 
the occasion of the composer taking his degree of Doctor 
of Music at the University. Mdlle. Marguerite Pommereul 
(who replaced Herr Wilhelmj, absent from indisposition) 
was highly successful in a violin solo, and Beethoven’s 
Pianoforte Concerto in C minor was well played by M. 
J. Wieniawski. At the first of the concerts under notice 
Madame Lemmens-Sherrington, and at the second Miss 
Catherine Penna and Mr. Santley, were the vocalists. Mr. 
W. G. Cusins received a cordial mark of recognition at the 
termination of the performance

MR. HENRY LESLIE’S CHOIR

Callcott), and “ The Mice in council” (Filby), the latter 
receiving an encore. The Andante and Allegro from 
Mendelssohn’s Sonata in B flat, for violoncello and piano- 
forte, were excellently played by Messrs. Finzi and J. G. 
Callcott. The solo vocalists were Miss M. Turner, L.A.M., 
Miss Kate Reed, Mrs. Leonard Hughes, Mr. Gabriel 
Thorpe, R.A.M., Mr. A. Baxter, and Mr. Henry Baker. 
Mr. J. G. Callcott conducted

A succEssFUL Service and Organ Recital was given at 
St. Mary’s Church, Haggerstone, on the 24th ult., under 
the direction of Mr. C. J. Frost, Mus. B., Cantab., the 
organist of the church. The choir was assisted by members 
of the West Hackney Parish Church Choir, and numbered 
between forty and fifty voices. The Service was Prout in 
F ; the Anthems were Gounod’s “‘ Send out Thy light” and 
Beethoven’s ‘‘ Hallelujah to the Father.” The Recital 
included Mendelssohn’s Third Sonata in A, Bach’s A minor 
Prelude and Fugue, Smart’s Fantasia in G major (with 
Choral), Lemmens’s Storm Fantasia, and a Fantasia in C 
minor, and Postlude in G minor, by the Organist. An Aria, 
‘*T will lay me down in peace”’ (also from Mr. Frost’s pen), 
was tastefully sung by one of the choir-boys. Mr. Frost’s 
playing was much appreciated by a numerous audience

THE new building in connection with the Royal Normal 
College for the Blind was opened by the Princess Louise 
on the 12th ult., when an excellent Concert was given, 
under the direction of Mr. F. J. Campbell, the Principal 
of the College. The meeting was addressed by the Duke 
of Westminster, Sir R. Alcock, the treasurer, and Mr. 
F. J. Campbell, who earnestly advocated the claims of the 
Charity. The institution is, unfortunately, still heavily in 
debt—nearly £7,000 being required to meet the liabilities— 
but it is hoped that, when the objects of the undertaking 
become more generally known, the efforts of those who 
have interested themselves so warmly in promoting this 
noble work will be supported as they deserve

Ketis.—The seventh Festival of the Meath Diocesan Church Choral 
Asscciation was held on the 3rd ult. in the Parish Church, when twelve 
choirs, numbering 120 voices, were assembled. Mr. W. H. Gater, 
Mus. Bac., Lic. Mus., choirmaster to the Association, presided at the 
organ. The music included Garrett’s Anthem “ The Lord is loving,” 
Hopkins’s Te Deum in G, chants by Macfarren, Fussell, and Cros- 
thwaite, and three hymns from the (Irish) Church Hymnal. The 
singing of the united choirs was very creditable. The sermon was 
Sy rsa by the Right Hon. and Most Rev. Lord Plunkett, Bishop 
of Meath

Leeps.—On Sunday, the rst ult., a large congregation assembled in 
Salem Chapel, Hunslet Lane, to hear a Service of Sacred Song by the 
Salem Choir. The selection included “ Hear my prayer,” “ Judge me, 
O God,” “As the hart pants” (Mendelssohn), ‘Oh, loving peace,” 
“ Sing unto God” (Handel), ‘“‘ Benedictus” (Weber’s Mass in G), “ Oh, 
be joyful in God” (Smart), “ Leave us not” (Stainer), ‘‘ Oh, Lord, how 
manifold” (Barnby), ‘‘God be merciful unto us” (Costa), and the 
“Hallelujah” from Beethoven’s Mount of Olives. The choir was 
specially augmented by several friends from the Madrigal and Phil- 
harmonic Societies, and numbered about sixty voices. The choruses 
went well, being given with excellent precision and taste. The solos 
were entrusted to Miss Jenny Winkworth, Madame Galli, Mr. T. 
Thompson, and Mr. J. Burniston, all of whom were highly effective. 
Mr. W. Toothill was an efficient Conductor, and Mr. J. Wilkinson pre- 
sided at the organ

MANCHESTER.—Mr. Horton C. Allison was presented, on the 23rd 
ult., with a testimonial consisting of a Cambridge University hood 
appropriate to his degree, accompanied by a handsomely illuminated 
address as follows, viz., “Presented to Horton C. Allison, Mus. B., 
Cantab., with a Bachelor’s Hood, on behalf of his Pupils, in commemo- 
ration of his taking his Degree as Bachelor of Music at the University 
of Cambridge, and as a recognition of the pleasant and efficient 
manner in which he has imparted instruction during the long period 
of his engagement at Oakleigh, Manchester, in teaching Harmony, 
Vocal and Pianoforte Music. (Signed) A. Delhavé, Ch. Delhavé 
(Principals), Higher Broughton, Manchester, July 23, 1877

MELBouRNE.—A new Society has been formed for the performance 
of works of a strictly classical nature, instrumental and vocal. 
The prospectus for this year includes Elijah, Euterpe, Israel in 
Egypt, Beethoven’s Choral Symphony, and Macfarren’s St. ohn the 
Baptist.” Mr. J. Summers (late Conductor of the Philharmonic 
Society) has been appointed Conductor.——At the Concert at the Town 
Hall, and also at that of the Metropolitan Liedertafel, at the 
Athenzum, the singing of Miss Christian is spoken of in the highest 
terms, the dramatic feeling she threw into the solo “O mio Fernando,” 
from La Favorita, suggesting the inquiry why she does not make her 
appearance at once upon the operaticstage. With her excellent voice, 
perfect culture, and power of passionate expression, there could be 
little doubt of her success

Nortu Berwick, N.B.—On Saturday, the 14th ult., Mr. Frank 
Bates gave an Organ Recital in S. Baldrea’s Church, which was 
attended by a large number of residents and visitors of this fashionable 
watering-place

CONTENTS OF BOOK IV. 
ANDANTE, “JULIUS CESAR” .. HANDEL. 
PRAYER, LARGHETTO MAESTOSO Be tint. 
MARCIA RELIGIOSA .. . GLuck

ANDANTE CANTABILE, “TRIO, 
C minor . BEETHOVEN. 
PRELUDE alae WILHELM Bacu

CONTENTS OF BOOK V

minor .. ay .. MENDELSSOHN. 
ANDANTE, from a 83. STEPHEN HELLER. 
“BUT THOU DIDST NOT LEAVE” Hanpet

CONTENTS OF BOOK VI. 
MARCH, from the Second Set of Sonatas HANDEL, 
PRELUDE . GEBHARDI. 
ANDANTE MOLTO CANTABILE, 
from Sonata, Op. 109 ‘i -. BEETHOVEN. 
LARGHETTO .. ee es «. WEBER. 
ANDANTE SOSTENUTO . MENDELSSOHN

Price of each Book, Four Shillings

Now ready, price Sixpence. 
= \7 HOSO DWELLETH UNDER THE 
DEFENCE OF THE MOST HIGH.” Festival Anthem, 
by Grorce Lomas, Mus. Bac., Oxon. 
London: Nov ELLO, Ewer and Co

BEETHOVEN’S SONATAS 
(NEW AND COMPLETE EDITION

EDITED AND FINGERED BY

and English words), sung by Mr. Santley, always encored, 4s. All 
post-free for half-price

ONGS with Italian and English words, by 
Beethoven, Campana, Fiori, Guercia, Lebeau, Mazzoni, Rotoli, 
Torti, Vaschetti, Zuccordi, &c

IST of VOCAL and INSTRUMENTAL MUSIC 
of every description from RICORDI’S GRAN CATALOGO, 
containing above 45,000 publications, gratis and post-free. 
RICORDI’S DEPOT, 23, CHARLES STREET, MIDDLESEX 
HOSPITAL, LONDON, W

NATALIA MACFARREN and BERTHOLD Tours. 
Price 2s. 6d. each ; or in scarlet cloth, 4s

NOW READY. 
BEETHOVEN’S FIDELIO 
(With German and English words). 
AUBER’S FRA DIAVOLO 
(With French and English words). 
MOZART’S DON GIOVANNI 
(With Italian and English words). 
BELLINI’S NORMA 
(With Italian and English words). 
VERDI’S IL TROVATORE 
(With Italian and English words). 
DONIZETTI’S LUCIA DI LAMMERMOOR 
(With Italian and English words

WEBER’S OBERON 
With the English words by J. R. PLaNcue, and Italian words as 
sung at Her Majesty’s Opera

