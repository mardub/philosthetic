Registered transmission abroad . Annual Subscription , Postage - free , 58 

 ROYAL CHORAL SOCIETY , ROYAL ACADEMY MUSIC , 
 ROYAL ALBERT HALL . TENTERDEN STREET , W. 
 Patron : Majesty Quzen . Instituted 1822 , Incorporated Royal Charter , 1830 . 
 \ : President : H.R.H. Duke Saxe - Cospurc GoTHA . Patrons : Majesty QUEEN Roya FaMILy , 
 g ( Duke EpinsurGH , K.G. ) President : H.R.H. Duke EpinBurGu , K.G , . 
 Conductor : Sir JosePH BARNBY . Principal : Dr. A. C , MACKENZIE , 
 te - SEASON , 1893 - 94 . eaiose Tons vemnens . iaihtiinid ‘ 
 — — _ ntry Forms information obtained Secretary . 
 \ PROSPECTUS . Fortnightly Concert , Saturday , October 21 , 8 . ain 
 Wve series comprise Concerts , following works | Lecture W. S. Rockstro , Esq . , Wednesday 3 . 
 performed : — November 2 , Berlioz ‘ ‘ Faust ” ; November 23 , F. W. RENAUT , Secretary . 
 “ Ecypt ” ; D b ee THA ” ; 3 
 ps , une ” : peal 18 , “ 5 se piehce ms earl : GUILDHALL SCHOOL MUSIC . 
 > “ REDEMPTION ” ; March 1 , “ REQuiIEM ” ( “ Mors et Vita ” ) Established Corporation London , 1880 . 
 “ Rossini “ “ STABAT MATER ” ; March 23 , “ MesstaH ” ; April 12 , ? Principal : Sir JosepH BARNBy , 
 ves Mackenzie ‘ BETHLEHEM ” ; April 26 , “ Ex1jan . ” Michaelmas Term ed Monday , September 25 . 
 Arrangements following artists : — eaataas fe 7 af pian mg apply Secretary . 
 d Madame ALBANI Mr. EDWARD LLOYD y CHARLES P SMITH , Secretary . 
 uy * NA WILLIAMS Mr. BEN DAVIES : ‘ , F 
 y HENSCHEL Mr. PHILIP NEWBURY Viet Se Fe . 
 Miss ESTHER PALLISER Mr. HENSCHEL 
 ally Madame MORAN - OLDEN Mr. WATKIN MILLS S NORTH STAFFORDSHIRE 
 Mis MARIAN McKENZIE Mr , ANDREW BLACK TRIENNIAL MUSICAL FESTIVAL . 
 Miss MARIE BREMA Mr. NORMAN SALMOND : ‘ 
 Miss AGNES JANSON Mr. DAVID BISPHAM President : Grace DUKE SUTHERLAND , 
 gry gee , VICTORIA HALL , HANLEY , October 19 20 , 1893 . 
 : Mr. H , » , 7 
 art Thursday Evening — Berlioz " Faust . ” | - 
 n 2 Band Chorus consist 1,000 performers . ‘ riday Morning . — mart “ Bride Dunkerron , ” ackenzie 
 v3 - heer roma s od ber production af Ds ; Pid ont Bi seo Hag Solo Violin Orchestra ; Beethoven 
 ackenzie new Oratorio “ Bethlehem , ” andel “ Jephtha ” P r Ta nie 
 art 17 = _ ama 7 time ae . Friday Evening . — “ Elijah . seassheiten 
 t found necessary revert Thursdays Society 
 o # performances instead Wednesdays , hoped change Miss MEDORA Se Miss ESTHER PALLISER . 
 Bier prove convenient subscribers general public . WARD LLO anne ow ae 
 4 Mf Concerts given , included Sub- Mr. ro viine > = Stier PIERCY . 
 ad - tion series . — ane Concerts : Mr. FFR ¢ Mr WATKIN MILLS LLIAM EVANS . 
 talls , 3 guineas ; Arena , 2 guineas ; Balcony ( reserved ) , 14 guineas . sol + = ~ 
 1 * Prices Concert : ros . 6d . , 7s . , 58 . , 48 . , Gallery Pro- Principal Solo Violin : Mr. WILLY HESS , 
 4 menade , 1s . Chorus 350 Performers . 
 ys Subscribers ’ names received , seats secured , pros- Conductor : Dr. SWINNERTON HEAP . 
 pectuses obtained Royal Albert Hall . Programmes application Mr. Geo . W. Bradford , Hon . Genera 
 1 ° fis Concert , Thursday , November 2 , . 8 . Berlioz “ Faust . ” Secretary , Miles Bank Chambers , Hanley . 
 2 ists : Madame Moran - Olden , Mr. Ben Davies , Mr. E. Epstein , 
 " Mr , Henschel . : : SOUTH LONDON MUSICAL CLUB . 
 CHELTENHAM PRIZE COMPETITION 
 ie 
 3 * TRIENNIAL MUSICAL FESTIVAL , Committee offers £ 10 tos . best Secular Composition 
 x October 16 , 17 , 18 , 19 , 20 . A.T.T.B.B. , Pianoforte Accompaniment , forwarded President , 
 » 2 Monday Evening.—Full Rehearsal . South London Musical Club , Gresham Hall , Brixton , London , S.W. , 
 nak ‘ Tuesday Evening.—Festival Ode ( Tours ) ; ‘ ‘ Stabat Mater ” ( Rossini ) ; | later Tuesday , December 5 , 1893 . 
 Hymn Praise ” ( Mendelssohn ) . work occupy minutes performance , 
 1 * Wednesday Evening . — ‘ ‘ Golden Legend ” ( Sullivan ) ; Choral | contain short incidental solos soli parts . words 
 Song ( Parts ) , ‘ Sweet Echo ” ( Dr. F. Iliffe ) ; Concerto | non - copyright , author date place 
 n 2 — eaeeett , & c. oisne : ees book found ’ 
 ursday Afternoon . — “ ‘ Messiah . ” stated . 
 Friday Evening . — “ Lohengrin ” ; Festival Ode ( Tours ) ; Grand | Prize awarded Committee appointed Club 
 } ction . . j R 
 tion . nd assisted Dr. J. F. Bridge , withheld works for- 
 » 4 Artists : Miss Thudichum , Miss Medora Henson , Miss Maggie | warded sufficient merit . 
 Davies , Miss Susan Harrhy , Miss Beatrice Gough ; Miss Hilda Wilson , | _ composition property published 
 3 Miss Jessie King , Miss Lucy Franklin Higgs , Miss Fanny Stephens , | Club , result competition , course , announced 
 WB td Madame Hope Glenn ; Mr. Edward Lloyd , Mr. Henry Piercy , Mr. | MusicaL TIMEs . ‘ : 
 Edwin Houghton , Mr. Ben Davies ; Mr. Bantock Pierpoint , Mr. vocal scores , clearly written set voice parts , dis- 
 -H. Brereton , Mr. H , Lane Wilson , Mr. Watkin Mills . tinguished motto , accompanied sealed envelo 
 io instrumentalists : Miss Neruda , Miss Amy Woodward , | bearing motto containing address 
 1 lo lists : Miss Olga Neruda , Mi Woodward , | beari d h d address 
 4 Mot ’ 59 ao Ya corte ) ; Mr. Theodore Carrington | composer , forwarded . 
 ’ ) ; Mr , W. Ellis ( Trumpet ) . 
 ” { amie Orchestra 00 performers , COLLEGE ORGANISTS . 
 1 * Ticket onductor , Mr , J. A. MATTHEWS . 
 S : I8 . , 2s . 6d . , 4s . , 5S. , 7S. 6d . , ros . 6d . PAGE ® 
 » 4 ammes , Tickets , vat telraation C. Westley Library , | Solo - playing Tests F.C.O. Examination January 
 Promenade , Cheltenham . Special trains parts reduced | : Prelude Fugue G major , Bach ( . 2 , Book II . , Peters ) ; 
 ites , ens y 25 , H. — — — sty Fo — Neer ar 
 = = ugener ) ; Choral , Variations , H. Smart ( Novello , Ewer Co. ) . 
 CRYSTAL PALACE SATURDAY CONCERTS . | ” College Library open daily ro 5 , Tuesdays 
 Wo THIRTY - EIGHTH ANNUAL SERIES Thursdays from7to9 . ; é 
 RLD - RENOWNED CONCERTS open SATURDAY , Members desiring practice College Organ obtain par- 
 ber 14 , 3 . Conductor , Mr. August Manns . Prospectus , | ticulars application . 
 ch Programmes Concerts Christmas , ] large room , sundry smaller rooms , hired concerts , 
 onc prices serial Transferable Tickets Seats | meetings , & c. terms , application Assistant 
 oe Concerts , sent , Ya trey application Manager , | Secretary . 
 oy AAS oP. Fa ea — dates Sen 8 BI » E. H. TURPIN , Hon . Secretary . 
 s , future , Shilling . art Street , Bloomsbury , W.C 

 _ 

 produced Court Theatre , Dresden , 
 immense success . ” suspected 
 time come weapons fiercely 
 wielded Meyerbeer , Halévy , 
 turned direction Saxon 
 apparition . ‘ Young England 
 early forties fight Meyerbeer , Halévy , 
 like ? ” Listen : ‘ M. Meyer- 
 beer , prodigious fluster 
 half - educated amateurs , paid claquers , would- 
 cognoscenti , poetasterical penny - - liners ? .. . 
 M. Meyerbeer composer moderately , 
 moderately , gifted natural powers , evincing 
 flimsy pretensions harmonist 
 contrapuntist . .. . found 
 Meyerbeeric ? ‘ Huguenots , ’ 
 cumbersome drawl monotonous commonplace , 
 Noah ark pure vulgarities dressed 
 species puppet importance vapid 
 inflated grotesquerie , Atheneumo - apotheosised 
 apology good historical opera , intolerable 
 infliction beginnings endings 
 middles ! ” Halévy , listen : “ fellow 
 solitary new idea , invention 
 bath commonplace swim , 
 conception fountain sprinkling shower 
 vulgarisms passer - , imagination 
 quagmire fail frog nourish- 
 ment , liveliness throw damp 
 afuneral .. . melodist null , har- 
 monist nuller , contrapuntist nullest , 
 rescued absolute nothingness tolerable 
 knowledge instrumentation . ” appeared , 
 started Edward James Loder ( wrote 
 operas ) ‘ ‘ better . ” ‘ instrumenta- 
 tion , ” writes editor , ‘ agree 
 considering tolerable . 
 idea miserably weak strikingly harsh , 
 M. Halévy 
 appears feeble copy Meyerbeer , , 
 possible , originality , melody , , , 
 claptrap .. . know 
 countrymen Barnett , Macfarren , & c. , & c. [ 
 E. J. L. ] , , justly tried 
 world , ” , 

 Thalberg , prominent representative foreign 
 “ pianism , ” comes large share Young 
 England objurgatory wealth . ‘ “ chief character- 
 istic method — stole Weber 
 Mendelssohn , Beethoven ( 
 better purpose ) , hyperbolised rhodo- 
 montade , stealing — laying somebody 
 subject , middle keyboard , 
 sprinkling shower small notes , 
 shower small notes — characteristic 
 eminently facile attainment , 
 years , imitators , like 
 model , incapable aught , sprung , 
 flight swallows , sprung , 
 blight locusts , , springing 
 springing , gathered skill ape 
 master monkey - like imitativity 
 profound Thalbergist able 
 distinguish . ” vials 
 hottest wrath poured certain Thal- 
 bergian Andante , known Andante young 
 ladies period . “ heard 
 maudlin monotonous , lachrymose lacka- 
 daisical , pitifully puling positively paralytic , 
 whining , whooping , whizzing , whurring , 
 wishy - washy wallow warm waterishness 
 dear , delightful , dead - - durance days 
 beloved old aunt , Tabitha , seat 
 harpsichord deal dolefully ‘ groans 

 wounded 

 Wuence shall spring English composer 
 write greater symphonies 

 Beethoven , operas shall reduce Wagner 

 music - dramas comparative position 
 music primeval man , know ; , judging 
 past , improbable early 
 years passed choirboy St. George , 
 Windsor . , number 
 famous musicians received early 
 musical education Chapel Royal makes 
 new arrangements choirboys con- 
 siderable musical importance . Accommodation 
 provided thirty boarders , - 
 choristers , provided 
 scholarships , value £ 40 annum , 
 reduce fees fortunate holders 
 £ 10 year . non - choristers pay annual 
 fees £ 70 . musical education 
 entrusted Sir Walter Parratt , 
 Mr. Ashley Bickersteth appointed Head 
 Master school , Mr. Gerald Harries 
 assistant . details scheme 
 attention bestowed secure 
 general welfare progress boys . ‘ school 
 intended sons clergy professional 
 men , examination entrance fixed 
 roth inst . best boys win , 
 England future great musician 

 present month unusually active 
 Festivals . Beginning 3rd 6th inst . 
 Norwich ( particulars fully 
 detailed columns ) , interest , fortnight 
 later — 16th 20th inst.—be transferred 
 Cheltenham , “ Golden Legend , ” “ Stabat 
 Mater , ” “ ‘ Hymn Praise , ” ‘ Messiah , ” 
 selection “ Lohengrin ” chief features , 
 novelty imparted production Festival 
 Ode Berthold Tours , choral song , “ Sweet 
 Echo , ” Dr. F. Iliffe . Conductor , Mr. J. A. Matthews . 
 17th 18th inst . - days ’ Festival 
 given Hovingham , Yorks , Sullivan 
 “ Golden Legend , ” ‘ “ Hymn Praise , ” Spohr 
 “ God , Thou art great , ” new cantata , “ 
 Rock - Buoy Bell , ” written occasion Dr. 
 Alan Gray , performed . 19th 2oth 
 inst . North Staffordshire Triennial Festival 
 held , include Berlioz ‘ Faust , ” Smart 
 j Bride Dunkerron , ” “ Elijah , ” Mackenzie 

 Pibroch , ” Beethoven C minor Symphony . 
 Conductor , Dr. Swinnerton Heap . Bristol 
 Triennial Festival , direction Sir Charles 
 Hallé , , 25th 28th inst . , form 
 fitting conclusion notable Festival month 

 Taxina text assertion Daily Tele- 
 iph “ Holiday Correspondent ” Whitby 
 . Musically inclined , , , backs 
 ielination , ” Mr. Henry Hallgate , Conductor 
 Choral Society , discourses Whitby 

 St. JAmEs Hatt.—Three Sarasate Concerts 
 given , direction Mr. Vert — 
 afternoons 14th inst . , November 13 , 
 December 4 . Concert eminent 
 Spanish virtuoso assistance Madame 
 Berthe Marx solo pianist 

 TueE London Symphony Concerts consist 
 series Concerts , extending November 8 
 April 5 . programmes highest 
 possible interest , Beethoven Wagner figuring , 
 course , prominently composers 
 represented . programmes — February 8 
 April 5 — notable importance , 
 - named date “ memoriam , 
 Richard Wagner , ” Prelude Good Friday 
 Music “ Parsifal , ’’ Prelude [ solde 
 death “ Tristan , ” “ * Walkiirenritt ’ ” ’ 
 given , , fitting tribute , Beethoven 
 ‘ ‘ Eroica ” Symphony occupy central position . 
 date programme devoted 
 Beethoven , comprise Overture 
 ‘ ‘ Coriolanus , ” Pianoforte Concerto G ( . 4 ) , 
 Choral Symphony . solo artists present 
 announced Mdlle . Frida Scotta , Miss Beatrice 
 Langley , M. Paderewski , M. César Thomson , Miss 
 Ilona Eibenschiitz , M. Emile Sauret , Mr. Leonard 
 Borwick . choir organised Mr. Henschel 
 assist necessary , , needless add , 
 artist , eminent capacities , conduct 
 Concerts 

 Popular Concerts resumed 
 16th inst . , London Ballad Concerts 

 Characteristic Piece ( Mendelssohn ) , Gerald 
 Walenn String Quartet F minor . songs 
 include Schumann “ Belshazzar ” Gounod 
 “ Valley . ” Artists : Mrs. Frances Ralph Miss 
 Walenn ; Messrs. Franklin Clive , Gerald , Arthur , 
 Herbert Walenn . Accompanist , Mr. Smith 
 Webster . , truly , deserving enterprise , 
 cordially recommend support 

 PATERSON SoNs ’ ORCHESTRAL CoNCERTS . — 
 enterprising caterers Edinburgh g0 
 indebted announce series Concerts , 
 beginning November 6 , ending March 26 . band 
 Scottish Orchestral Society , con . 
 ducted Mr. Henschel . Works announced include 
 Berlioz “ Faust ” ( time Edinburgh ) ; Dr , 
 Mackenzie “ Story Sayid ” ; Symphonies 
 Brahms , Haydn , Beethoven ; Overtures 
 “ King Lear , ” “ Ossian , ” “ * Egmont , ” “ Les Abencer- 
 ages , ” ‘ ‘ Euryanthe ’’ ; orchestral selec . 
 tions , Concertos F. Hiller , Rubinstein , 
 Max Bruch . Numerous artists eminence 
 engaged , season , equipped , runa 
 triumphant course 

 Mr. Epwarp GerMANn charming Dances 
 ‘ “ ‘ Henry VIII . ” music weeks ago performed 
 Bruxelles Harmonie Communale , conducted 
 M. Fritz Sennewald . programme announced 
 terms : ( ) ‘ Danse mauresque , ” 
 ( b ) “ Danse des Bergers , ” ( c ) ‘ ‘ Danse aux Flambeaux , 
 de Vopéva Henry VIII . de Saint - Saéns , arr . par Edward 
 German . ” mixing Shakespeare play , 
 M. Saint - Saéns opera , clever young com- 
 poser music good , translation ‘ Morris 
 Dance ” ‘ Danse mauresque ” better . 
 correct according 
 dictionaries ( dance deriving 
 Moors , probably originated ) , 
 audience , unacquainted 
 old English dance , expected 
 Oriental , surprised ‘ “ Saint- 
 Saéns ” ’ failure impart requisite local colour 
 piece , , perchance , Mr. German typically 
 English strains actually suggested Eastern music 
 

 MUSICAL TIMES.—Ocroser 1 , 1893 

 598 
 , “ opinion undoubted judges , ” | work Fischer , Dresden organist , departed 
 best Europe . Archdeacon estimates |life Christmas . second given “ b 
 10,000 persons attend metropolitan Cathedral |tequest ” influential local gentleman , heard 
 piece Germany wished neighbours | 
 Sunday . attracted Sora 1 . Hecti aidiien a. 
 music , note sentence : “ | 28 ° Dave @ pleasant recollection concerning . ’ 
 7 J : novelties performed solitary secular : 
 evenings Cathedral , , | Concert remains Festivals , , 1 
 frequently happens , preacher young , | . ; Worcester , given Public Hall Wednesday 1 
 unknown curate . ” evening . large audience occasion , 4 
 arr Dr , Parry Overture ave meee hearing , f 
 : : . man personally esteemed sure , 
 TuE Middlesbrough Musical Union announces doind , sek mb Glee hn CU , 
 Subscription Concerts , beginning December 13 , basis ” , knowledge , given . ha ! 
 “ Pied Pi f Hamelin ” ’ . ; " ag Ppens , d ! 
 Mr. R. H. Walthew “ Pied Piper . | , clue needed S 
 ( produced season Highbury Philharmonic | enjoyment music , like Schubert “ Unfinished ” “ 
 Society ) given . Mr. Kilburn , usual , Symphony , clearly suggests conflict m 
 Conductor . Society deserves light darkness . nature contending \ + 
 encouragement efforts performing works | elements hearer judge , gi 
 native composers . plainly felt strenuous , agitated leading subject , 
 — strong treatment , melodious , graceful , $ 
 : tender second theme . Overture worked , 
 understand production Mr. F. H. | gai ) command necessary means , gives impression pe 
 Cowen opera “ Signa ” Dal Verme Theatre , | masterly composition esteemed 
 Milan , definitely fixed week this|the better known . regard “ Gretchen m 
 month . Dom , ” pronounce apt example forcible- Plu 
 — feeble music . ee ape puts 7 oe ~~ deal Jes 
 3 . machinery , sets huge clatter wheels strc 
 News comes Exeter Oratorio Society | cranks , pouine anil speaking ground . imp 
 , time depression , got head | mountain labours , , ‘ mouse Wo 
 water . ‘ providing liabilities , ” writes | come forth . ” organ curiously ineffective “ LL , 
 correspondent , ‘ ‘ balance bank . ” considering organist wrote . “ Gretchen audi 
 goes encourage hope triennial festival |im Dom , ” good - bye ! Unc 
 far South - West . performances given Cathedral , exce 
 — , according unvarying rule , devoted ‘ Elijah , ” Ane 
 ; ; , according unvarying rule , attendance ¢ 
 machinery — steam pipe , large — second largest week , point fact ; num 
 precise — working “ Die Walkiire ” | honours taken “ Messiah ” Friday . whic 
 Grand Opéra , gave way evening , | idle consume time space details , 
 followed deafening hiss escaping vapour . | especially chief solos tried capable s¢ 
 gods Walhalla comport with|hands Madame Albani , Miss Anna Williams , Mr. justic 
 dignity new circumstances . - Father | Lloyd , Mr. Watkin Mills , rendering | 
 Wotan jumped intothe orchestra musicians , | Prophet music favourably regarded . con- Orato 
 stately Briinnhilde rushed stage dear — = — _ nn ee praise prorat Cote 
 : Ss ole , y orc 
 life . , , penny worse . foil refined sympathetic singing ‘ Woe | 
 pe unto , ” Miss Jessie King , young beginner , Brahn 
 7 apparently , future . choruses given fairly Pra 
 Hove Brighton relatively “ ” | conditions set control in- dra 
 matter open - air music . near | experienced Conductor . Mr. Hugh Blair , Organist “ Requ 
 neighbours kept bands - foot summer , | Worcester Cathedral , capable man , , like , 
 Brighton enterprise described | needs practice difficult craft chef d’orchestre . associa 
 absolute failure , Hove account shows surplus | arrive time , present , heard | 
 £ 150 . reason difference . eon ig ep ( ind rae sel — ioe pron 
 : - chorus ( including 1oo singers Lee le 
 Let larger community find amend . attacked work . elan aenaliat pon devoted solos , 
 ee Beethoven Symphony Handel “ Israel wholly 
 Egypt . ” performance Symphony , 
 “ Old Man ” complains print ‘ Robert | effect , marred Conductor slow tempi , apr 
 le Diable ” called ‘ * Roberto il Diavolo . ” reeeapees4 sear fe ane porte Ie rs acd 
 : * ave great orchestral works recognised fit t. Lk 
 ee ae ae oe ee Cee ina anual dhe . Created Heaven inspiration , presided 
 certainly fit house Lord Heaven “ 
 worshipped , pod age = urd 
 slow tempi lest offence . e F 
 : sintaatachsiatedieaiatats ticiatiadiaadhiabiaenes Perpend , Mr. Blane Handel Oratorio great _ , taking p 
 managers 170th meeting Choirs | Deliverance , massive double choruses , better Friday ey 
 charged erring direction ! effect anticipated view comparatively small eloqu 
 exaggerated enterprise . matter , , they|means . performance , course , helped Praise ; 
 showed enterprise , production new | resonant qualities Cathedral . chorus - singet Deum 
 works following lead Gloucester year and|in building equal concert - room , al Scasion , 
 trusting choral music local singers . | Crystal Palace . ensemble e : 
 committee perceived good reasons extreme caution . | lack requisite power majesty asthe tradit ; 
 Times bad , especially agricultural districts , | numerical force engaged suggest . solos ie prese 
 squires yeomen little money spend amuse- | entrusted Miss Williams , Madame Cole , Mr. Lloyd , | 
 ments , policy putting forward known | Mr. Ineson , Mr. Brereton . artists Mr. Lloyd » Mack 
 tried attractions , possibly , recognised | took chief honours superb rendering ‘ ‘ enemy Offert 
 imperative . , programme | said . ” ‘ tvice b 
 ] Festival contained new work — Overture Dr.| | Wednesday proceedings opened Bach Massin J bright 
 Parry — composition heard | B minor — performed according Leeds version 1886 EF built N 
 country — pretentious unduly favoured symphonic | 1892 . choice work bold Y 
 piece , ‘ ‘ Gretchen m Dom , ” orchestra organ , | risky , , Conductor tempi agai pedals 

 ] 
 

 excellence characterised initial 

 formances Mr. Farley Sinkins Promenade Concerts 
 sustained past month . pro- 
 grammes presented admirably varied selection 
 music , artistic value , execu- 
 tion great merit . important 
 works performed Beethoven , Sixth , 
 Seventh Symphonies , Mendelssohn “ Italian ” 
 ‘ Scotch ’ ? Symphonies , Andante Finale 
 Mr. Cowen “ Welsh ” Symphony , Haydn “ Military ” 
 Symphony , Schumann Fourth Symphony , Overtures 
 “ Magic Flute , ” Berlioz ‘ ‘ Carneval Romain , ” 
 Bennett ‘ “ ‘ Naiads , ” Sullivan “ Ouvertura di Ballo , ” 
 Schubert ‘ “ Rosamunde , ” Beethoven ‘ Prometheus ” 
 ‘ Leonora , ” Cliffe ‘ Cloud Sunshine , ” 
 Cherubini ‘ Anacreon , ” Mendelssohn ‘ ‘ Midsummer 
 Night Dream ” “ Ruy Blas , ” Weber ‘ “ ‘ Euryanthe , ” 
 Auber “ Fra Diavolo , ” Wallace ‘ ‘ Lurline , ” C. 
 Ould “ Robert Macaire . ” orchestral works 
 interest heard , including Dr. A. C. Mackenzie 
 Scotch Rhapsody , Brahms Hungarian Dances , 
 Mr. William Wallace clever - elaborated Sym- 
 phonic Poem “ Passing Beatrice , ” Dr. Hubert Parry 
 incidental music ‘ ‘ Hypatia , ” graceful ‘ Serenade 
 Espagnole ” Mr. G. R. Betjemann , Mr. E. German 
 Dances incidental music ‘ ‘ Henry VIII . , ” cha- 
 racteristic Gavotte E minor piquant Minuet G 
 M. De Nevers , ‘ ‘ Danza Esotica , ” French 
 style , Mascagni , - named played 
 time England 12th ult . addition 
 performances Wagner Concerts 
 given , usual excerpts master works 
 excellently interpreted . solo instrumentalists 
 Messrs. Slivinski , F. Dawson , M. Wolff , 
 P. M. Cathie , - named promising young violinist 
 Royal Academy Music . Foremost 
 vocalists course placed Mr. Sims Reeves , , 
 spite oft - repeated farewells , fresh memories 
 people , - appearance Concerts 
 11th ult . , subsequent Concerts received 
 utmost enthusiasm . sang ‘ ‘ Tom Bowling , ” 
 songs long asso- 
 ciated , beauty phrasing artistic finish 
 greatly contributed reputation ’ ; 
 voice , remember prime , 
 echo richness power . vocalists 
 repeatedly appeared Mesdames Marie 
 Roze , Palliser , Valleria , Giulia Valda , Ella Russell , Belle 
 Cole , Marian McKenzie ; Messrs. Philip Newbury , 
 Braxton Smith , Thorndike , Oudin , Bispham , Pierpoint , 
 Meister Glee Singers , Master Cyril Tyler . 
 Mr. F. H. Cowen selection music 
 fine performances Mr. Betjemann .have secured 
 spoken high approval , success 
 Concerts welcomed sign growing 
 appreciation high - class orchestral music 

 SAINT - SAEN$ “ SAMSON ET DALILA 

 Festival Choral Society , single instance , 
 venture outside present répertoire . time , 
 commendable list works forward , comprising 
 interesting revivals — viz . , Dr. Parry ‘ Judith , ” ’ 
 Max “ Bruch “ Lay Bell , ’’ Gade “ Crusaders ” 
 ( heard Festival 1876 ) , Rossini 
 “ “ Stabat Mater . ’ Concert , 12th inst . , 
 , , devoted performance “ Elijah , ” 
 ‘ ‘ safest ” work class 
 produced . ‘ doubt , play ‘ Elijah , ’ ” ’ 
 given sound advice Birmingham choral society 
 manager conductor . Dr. Stanford ‘ ‘ . Revenge ” 
 serves fill spare time left Max Bruch work 
 Concerts 

 Mr. Stockley , Orchestral Concerts appeal culti- 
 vated tastes highest musical treats 
 season , details orchestral 
 works produced Concerts subscrip- 
 tion series . learn , , time going 
 press , important orchestral selections 
 Mr. Stockley proposes introduce Professor Villiers 
 Stanford ‘ Irish ’ ? Symphony , Dr. Hubert Parry 
 “ ‘ Hypatia ” music , written production play 
 Haymarket Theatre ( works conducted 
 respective composers ) , Wagner “ ‘ Walkiirenritt ” 
 ‘ “ ‘ Tannhauser ” Overture . Interesting practically 
 novel productions older ‘ classic ” school 
 “ Rhenish ” Symphony Schumann “ Reforma- 
 tion ” ? Symphony Mendelssohn . Beethoven 
 represented Fifth Symphony ; far , attractive 
 list 

 Messrs. Harrison , Concerts number 
 important social events year , offer usual imposing 
 annual list artists . names artists , 
 little exception , renown established repute , 
 thought necessary particulars 
 music artists perform ; necessary 
 respect , difficulty enterprising 
 managers meet finding sufficient 
 seating accommodation fashionable clientéle . 
 year , believe , applications ballot greatly 
 exceeded number allotable seats hall . 
 Madame Melba , Concert season gave 
 peculiar proof lack interest novel 
 public , returns satisfy ay £ 1 

 season commencing promises un- 
 usually active . , , true fewer announce- 
 ments ordinary regard 
 Saturday evening rivalries ; merely means 
 musical attractions hurtfully concentrated , 
 provision nights , 
 avoidance keen competition winters 
 wise . change repeatedly 
 advocated , difficult understand object ot 
 crowding performances , appealing class ot 
 amateurs , night , leaving bare like enter- 
 tainment rest week , Resolute perseverance 
 attempt establish , firm basis , good 
 cheap Concerts earlier week 
 certainly succeed 

 Sir Charles Hallé prospectus , course , claims 
 attention ; like promises Concert - givers , 
 shows sense necessity moving . solo 
 engagements bring established favourites — 
 Lady Hallé , Messrs. Joachim , Sarasate , Hess , Siloti , F. 
 Dawson , Borwick ; Misses Palliser , Landi , Anna 
 Williams , Madame Albani ; Concert 
 ( November 2 ) Miss Emma Juch doubtless vindicate 
 claim honourable position . : important , 
 , , , British works 
 submitted — viz . , Sullivan ‘ * Macbeth ’’ Overture 
 Stanford ‘ Irish ’ ? Symphony . _ 
 time come greater research national 
 library deemed prudent . Symphony 
 Sgambati , characteristic pieces Smetana , 
 lighter orchestral novelties promised ; choral 
 efforts include Beethoven Choral Symphony 
 Handel “ ' Israel Egypt , ’’ works delight 
 choirmaster ( Mr. R. H. Wilson ) choir . ot 
 Liszt “ St. Elizabeth ” repeatedly given 
 Athenzum finished choir Dr. Hiles 
 presided , great desire excited hear 
 complete work written thoughtful 
 strain . ‘ Flying Dutchman ” revived , 
 Rubinstein ‘ * ‘ Tower Babel ’ ? possibly prove ot 
 higher calibre “ Paradise lost ” 
 years ago 

 addition arduous work connection 
 Thursday Evening Choir , Mr. Wilson undertakes 
 direction Openshaw Choral Union , 
 rehearsing Cowen “ ‘ Rose Maiden ” , usual , “ 
 Messiah ” Christmas ; smaller association 
 Cheadle , essay “ ‘ Carmen , ” ‘ ‘ Esmeralda ” ot 
 Goring Thomas , Dvordk “ Spectre Bride 

 Mr. F. Marshall - Ward appointed Conductor 
 Mansfield Harmonic Society , commence 
 rehearsals October Spohr ‘ ‘ Judgment 

 Mr. William Allen announces important engagements 
 coming series Classical Concerts , 
 chamber music wind instruments predominate , 
 Beethoven Septet Quintet chief ‘ pieces . 
 Herr Ellenberger continues Chamber Concerts , 
 fixed public favour , owing judgment 
 care displayed performance selection 
 music . coming season busy , 
 lace music increasingly 
 public 

 MUSIC SHEFFIELD DISTRICT . 
 ( CORRESPONDENT 

 arrival M. Alex . Guilmant Chicago gave 
 dignity importance Organ Recitals Festival 
 Hall . Unfortunately stay limited 
 Recitals number . Festival Hall fairly filled 
 audience mildly enthusiastic . arranged 
 M. Guilmant shall Recitals 
 representative hall church great cities . 
 New York Recital Old South 
 Church , Dr. Gerril Smith presides ; Mr. William 
 C. Carl , pupil Guilmant , making 
 loyal efforts secure United Presbyterian , 
 hoped successfully 

 successful series Concerts given annually 
 Hann family , Brixton Hall , regular 
 institution , eighth season commence Mon- 
 day , 30th inst . , dates November 20 
 December 11 . intended perform following 
 works season : Quartet , E flat ( Op . 38 ) , Rhein- 
 berger ; Quartet , E flat ( Op . 87 ) , Dvorak ; Trio F , B. 
 Godard ; Sonata pianoforte violin , D minor , 
 Schumann ; Sonata pianoforte violoncello , D 
 major , Mendelssohn ; Sonata pianoforte ( Op . 814 ) , 
 Beethoven ; Quartet D minor ( Op . 76 ) , Haydn ; Quartet 
 minor ( Op . 41 ) , Schumann ; Quartet major 
 ( Op . 18 ) , Beethoven 

 Mr. HERMANN SMITH , years past 
 devoted time study practical 
 musical acoustics , publish short treatise 
 “ art tuning Pianoforte . ” Judging 
 advance sheets index forwarded , professional 
 amateur tuner find pages work 
 knowledge necessary household 
 instrument harmonious nature permit ; save 
 knowledge derivable experience , 
 advise amateur gain school room , 
 pianoforte 

 Concerto en Fa ¢. Pour Piano et Orchestre . Par 
 | Sigismond Stojowski . Op . 3 . 
 [ Stanley Lucas , Weber Co 

 GLANCE pages shows 
 music “ higher development ’’ ; 
 composer virtuoso , , , work dedi- 
 cated Antoine Rubinstein . Difficulties written mere 
 | , sake astonishing groundlings , 
 | condemned ; forgotten players 
 having perfect command keyboard expected 
 write ordinary folk . question , far 
 difficulties means end ? Concerto 
 notice impossible form definite opinion , 
 | inasmuch orchestra represented second 
 pianoforte . work opens mysterious 
 theme given harmony orchestra—(was 
 composer thinking certain slow movement 
 Beethoven quartets ? ) — constitutes 
 principal theme movement , treated great 
 skill variety . second theme , orthodox rela- 
 tive ‘ major , evolved , forms admirable 
 contrast . Thesecond movement , Romanza , D flat — 
 i.e. , enharmonic change C sharp . opening can- 
 tabile theme , Chopinesque character , charm ; 
 given orchestra . middle section , 
 pitt mosso , power , return , vid short 
 showy Cadenza , opening theme , pre- 
 sented ornamented fashion . closing movement 
 Allegro con fuoco , storm stress ; 
 hands great pianist stand poor chance . 
 hoped Sigismond Stojowski soon 
 opportunity presenting work English 
 audience 

 Sonate ( . 2 , G dur ) . Fir Pianoforte , und Violoncello , 
 Von Algernon Ashton . Op . 75 

 Theater Unter den Linden , Berlin , opened 
 autumn season August 30 Sir Arthur Sullivan 
 opera ‘ ‘ Gondoliers , ” successful 

 Herr Generalmusikdirektor Ernst Schuch , Dresden 
 Opera , celebrated , gth ult . , - fifth anniversary 
 début conductor . Herr Schuch , 
 best conductors present day , mainly 
 instrumental making Dresden Opera — 
 model kind . work conducted , 
 1868 , Breslau , musical farce having absurd 
 title ‘ 1733 Thaler 224 Silbergroschen ” ; “ jubilee ” 
 performance selected Beethoven ‘ ‘ Fidelio 

 Eugen d’Albert completed new choral work 
 entitled ‘ “ ‘ Der Mensch und das Leben ” ( ‘ Man 
 Life ” ) . text Otto Ludwig 

 reported famous old Gewandhaus Leipzig , 
 J. A. Hiller , Mendelssohn , Schumann , Ferdinand 
 Hiller , Gade , Rietz , Reinecke , conducted 
 countless concerts , turned commercial 
 exhibition room , having century devoted 
 music 

 Professor C. Halir , Weimar , dis- 
 tinguished German violinists , completed Violin 
 Concerto play public month . 
 shortly Concerts Leipzig , 
 play Concertos . Beethoven , Brahms , 
 Paganini , Spohr , Tschaikowsky , Lalo com- 
 posers selected 

 Anton Rubinstein , staying Italy tending 
 sick son , delivered score sacred 
 opera ‘ ‘ Christus ” Leipzig publisher 

 CORRESPONDENCE 

 WRONG BAR BEETHOVEN SEVENTH 
 SYMPHONY 

 EDITOR ‘ ‘ MUSICAL TIMES 

 S1r,—Allow assure Mr. Silas desire 
 deprive slightest honour glory connection 
 discovery . discovered years ago 
 perfectly true , mentioned fact met 
 rejoinder vexes present dis- 
 coverer — , - known 

 Sir George Grove , believe , claims freedom 
 errors Beethoven published works , existing 

 editions compared sure discrepancies 

 EDITOR ‘ “ ‘ MUSICAL TIMES 

 S1r,—I exceedingly interested subject raised 
 Mr. Silas ve Beethoven Seventh Symphony . 
 remember , student , conducting local orchestra 
 rehearsal work , wondering undoubted 
 discord produced playing bar question according 
 score parts , commented 
 players 

 feels ‘ ‘ treading sacred ground ” 
 playing music Beethoven , sure 
 feeling awe cause acknowledged 
 great professors art willing accept 
 correct , doubt explain satisfactorily , 
 passage modern composer 
 allow unchallenged . slavery 
 tradition wise question . studying 
 late Thomas Wingham mentioned subject , 
 said thought likely 
 mistake engraver 

 Mr. Silas undoubtedly correct inferring bar 
 generated grave doubts composer mean- 
 ing inthe minds musicians 
 arranging score pianoforte , & c. , 
 old copy arrangement pianoforte duet 
 W. Watts , published Chappell & Co. 
 “ ‘ Chord D ” omitted entirely , 
 Hummel Kalkbrenner , suggestion 
 bar score mistake evidently occurred , 
 “ ” chord continued semiquaver 
 bar 

 SEA eee [ je — 
 Py zZ ao 

 hope , Sir , kindly allow subject space 
 long possible , opinions , 
 feeling sure musicians agree trouble 
 worth taking reasonably correct version 
 bar monumental work Beethoven Seventh 
 Symphony.—Yours truly 

 H. Witcox Lawrance 

 Cape Town.—The Dean Cape Town , 4th ult . , presented 
 Mr. Barrow - Dowling handsomely mounted baton recognition 
 services Organist Choirmaster Cathedral 

 CuristcuurcH , New ZEALAND.—Mr . Wallace , July 20 , com- 
 menced series classical Chamber Concerts promises prove 
 highly successful . Schumann Pianoforte Violin Sonata D 
 minor , Beethoven Sonata “ Pathétique , ” Tartini ‘ Devil Trill ” 
 Sonata , Haydn “ Emperor ” Quartet ‘ included 

 rogramme , excellently rendered . following week , 

 CO 

 638 MUSICAL TIMES.—Ocroser 1 , 1893 . 
 ASHTON , A.—Cavatina . Violin Pianoforte . Op . 61 - . 
 BIRD , wr gp 3 . Orchestra . ee Piano- 
 orte Pp . 32 ° . 
 PUBLISHED WORKS BRAHMS , J.—Clarinet Quintet : Op . II5 . “ Arranged Duet ai : 
 Clarinet Pianoforte 16 o 
 eo ggg Fy Memoriam . ” " Adagio . Violin 
 rchestra . Op 65 : — 
 Score . ’ sa ite 06 te oe + » 1 0 
 PA D E R E S Orchetsral Parts . ee ue + . 20 0 ay 
 ‘ ‘ \ V ‘ e | — Thesame . Violin Pianoforte .. ee ° 8 o th 
 CUI , CESAR Quartet . Violins , Viola , Violon- 
 cello . Op . 45 ° — oon 
 ad S te ee oe oe oo 
 | Praeludium Menuetto io ‘ Se > rr : cy * ere Parts ae _ ve Ls ee oe “ 8 ; 
 | Elegie ( Op . 4 ) od es de - . 3 o| FORSTER , D psa — Die Rose von Pontevedra ” : — 
 ; Krakowiak ( Op . 5 , . d .. ee hg 7 ” . 300 ocal Score , German words .. es Metis o 
 | Mazurka ( Op . 5,No.2 ) .- « + ee wee x Soe GERMER 7 Overture . Pianoforte Solo ... 30 
 4 — Collection Favourite Compositions E : [ 
 H Krakowiak ( Op . 5 , . 3 ) .. ns - oe 0 VAN BEETHOVEN , F , ScuuBert , R. SCHUMANN . Duets 
 | Introduction Toccato ( Op . 6 ) D » * od se 420 tor Pianoforte . Arranged Purposes Instruction . 
 : Krakowiak ( Op . 9 , . 1 ) .. oe . net 2 6 
 j ont ty “ ) . ” . : “ copa , aes Pastorale . Violin Pianoforte . : N 
 feo 4 9 ; mo . 5 6 de vs - 3 oj — & Mabel . Violin Pianoforte . Op . 138 , ' 
 azurka ( Op.9 , No.4 ) .. oe ie oe oe ee o 3 O +2 . oo * “ . . 0 
 Krakowiak ( Op . 9 , . 5 ) .. . - - bes ieee — Marche deo Highlanders . Violin Pianoforte . ‘ 
 4 p. 138 , 2s éo os oe ee io 
 cmp ei 9 , . 6 ) > 53 ) se 8 + » 4 O| — _ Fantasia , Parts . “ Op . 143 : — . 
 ~ yl ( Op . 8) 5640 Ballade , Intermezzo , Scherzo - Final . . complete 8 o 
 é — = pba : > - 7 ” sid - ae oe aa Suite , . 2 ( E flat ) . Violin — 
 u Soir ( Op.10,No.1 ) .. wa Ae BL co t 12 0 
 Chant d’Amour ( Op . 10 , . 2 ) . . . . 7 7 : 7 GURLITT , C.—Two Miniature Trios . Pianoforte , Violin , 
 ! Violoncello , Op . 200 : — 
 qq Scherzino ( Op . 10 , . 3 ) .. = bs “ < FO ; . 2 . InG + » 4 0 
 H Barcarolle ( Op . 10 , 4 ) .. oe - ‘ dx < 9 8 HOLLAENDER , G.—Six Easy Violin Pieces . Op . 48 : — las 
 Caprice Valse ( Op.10 , No.5 ) .. 6 tes + 40 . 1 . Melodie .. ee cee hi 
 Variations et Fugue ( Op 1 ) .. . oa let Geburtstagmarsch ie ie abi Aes sn 
 Tatra Album ( Op . 12 ) . Books 1 2 , ‘ Solo ie oe ench 60 # z Kinderlied ‘ ol ss 4 oY ; ; col 
 . Dans le Désert ( Toccata , Op . 15 ) ‘ % F te ss aD 0 » 5 . Gavotte .. . . . « 20 yel 
 Concerto minor , Piano Orchestra . cee pcea 6 . Walzer .. RRM ae RS | 1 
 t iat HOTH , G.—Suite Old Style . Pianoforte . Op . 6 : — 1 m } 
 Ditto , Pianos .. és wh ‘ 20 0 
 : ’ . 1 . Preludium ; . 2 . Menuet ; _ 3 . Sarabande ; ad 
 : MINUET ( Op . 14 , . 1 ) mm ~ ok ee 4 0 . 4 . Gavotte + » complete 4 0 : 
 Sarabande ( Op . 14 , . 2 ) - - cite . 4 0| — — Pieces . Pianoforte . Op . 8 : — pol 
 Caprice ( Op . 14 , No.3 ) .. bes ‘ 7 ~ - . 5 . 1 . Preludium ; . 2 . Ariette . 3 . — _ — — - 
 Burl 4 2s die ge < 
 4 urlesque ( Op . 14 , No.4 ) “ - 4 © ] KIRCHNER , THEODOR.—Orgelcompositionen . Op . 89 : — anc 
 Intermezzo Pollaco ( Op . 14 , . 5 ) - 40 Heft 2 . ( Lyrische en , Nos . 5—8 ) ; _ 3 . ( Lyrische 
 i. Cracovienne Fantastique ( Op . 14 , No.6 ) .. ss Blatter , Nos . 9—13 ) . 3 « . 4 0 
 Légende ( Op . 16 , No.1 ) .. De ™ ¢ . . 4 @ LIADOW , A.—Valse badinage . Pianoforte .. Op . 3 20 ¢ 
 Mélodie ( Op . 16 , . 2 ) .. “ ie os . * bea 8 MARKUS , C.—Gavotte . Violoncello il " Plano des 
 é Ax oe * ? ‘ orte p. 30 .. 4 0 
 Theme varié ( Op.16,No.3 ) ..   « . ieee ee : § « | — — . Violoncellos ( Pianoforte ad lib . ) 1 . 5 0 mu 
 te ee - . 4 ) .. - ve ‘ . 4 0 eal 0 E.—Three Pieces . Violin Pianoforte . 
 n Moment Musica SAO bewE - iwe ‘ 2 . aad con 
 ; ii . 1 . ee ve ea : « 82 ita 
 f " ‘ . 2 . Berceuse slave .. 30 : 
 Songs , English , German , Polish words ( Op . 7 ) -- 60 MOZART , W. A.—Variations F. “ Arranged ‘ Piano- lea : 
 fortes Joser RHEINBERGER ae 1§ 0 
 MUSSO , L.—Two Pieces . Violin Pianoforte : — pos 
 PIANOFORTE DUETS . . 1 . ” ee tie ie ae v6 ie Pr ) : : 
 Danese Polousions ( Oe 5 ) iene ar . aad a8 PALASCHKO , J. — Stimmungen . ” Pieces , Violin eac ! 
 ete GNad .. oon co ve sf Pianofoite , = 2 0 
 Dances Polonaises ( Op . 9 ) . Books I. II . 6 oo “ 7 0 2 . Elfentanz ¥ ve # n 3 0 Mo : 
 Tatra Album ( Op.12 ) . BooksI.andII . .. .. .. 4 » 8 0 » 3 . Tarantella wipe Moree ? Maran hiek oe 320 
 Menuét ( Of:14,No.3 ) .. 06 06s ed 4 0 » 4 Walser . ee ee Oe eon 
 = 5 . Spanisches Stindchen ate “ i@n Sch 
 B 208 345 ee 
 RUFER , P.—Adagio . Violin Organ os “ 3 20 cha : 
 PIANOFORTE VIOLIN . SCHUTT , E. — “ Poesies d ’ ite , " Pianoforte Pieces P 
 " oe oe oe oe oe es t 0 : 
 Sonata ( Op . 13 ) .. oa ei ea : Lie ie . 13 — : — complete witl 
 Mélodie ( Op . 16,No.2 ) .. = ... ws wee wee OO No.1 . Visiond’Automne .. ws wet 28 nun 
 Krakowiak ( Op . 9 , . 5 ) se , 8 0 » 2 Au village se ee ee ee , 8 . 
 ee ee » 3 Cantique d'amour ... » ws wet BO ser 
 Mélodie ( Op.8,No.3 ) .. « 2 ss 3 0 4 : Sion ve Se 
 } Menuet ( Op . 14 , No.1 ) « 2 ... ee “ Gy PeScl xn gto . ue . don 
 iq nscHaikoWwskt , ® : — 18 Morceaux de Piano . Op . 7a — é cont 
 SS o. 1 . Impromptu ... . ee oe 4 
 oe 0 
 PIANOFORTE VIOLONCELLO . » Fondren Reproitins cite aa b 
 Menuet ( Op . 14 , No.1 ) .. 4 . Danse caractéristique & oe 8e ae pu 
 Mélodie ( 0 8 , N Pena ! « kee : 4 0 » 5 . » Méditation : Fe sche 
 D GANAS ) eerste < 4 der debe kere gas eel 800 ” 6 . Mazurque pour danser | ae Th 
 Fs ” z eee de Concert Ps 5 ilo 
 » 8 Dialogue 3 
 ORGAN O " 9 . Un poco di Schumann dn » aus - 
 R HARMONIUM . \ , 10 . Scherzo - Fantaisie ..   ..   .. + » « + 80 educ 
 . eh ee , ee ee ee . = e rh ee 
 » 12 . L’Espiégle pe aa ‘ es ee ue Ae 
 Menuet ( Op . 14 , No.1 ) .. ~ $ s Ss 3 sf 0 13 . Echo rustique .. ut > : : : 
 | > » 14 . Chant élégiaque . 
 4 N.B.—The new “ Fantasia ” issued shortly . » 15 . Un poco di Chopin amen ieee 8 : Pe 
 } rig : ( 6 aoe Bene ! temps .. Foe amb ee : ; 4 colle 
 er » 17 . Passé Lointain . F . 
 18 . Scéne dansante ( Invitation ‘ au Trépak ) 40 
 WILLCOCKS & CO . , LI MITE D , " Sold hait - price , marked net . 
 42 , BERNERS STREET , LONDON , W. LONDON & NEW YORK : NOVELLO , EWER CO 

 n 

