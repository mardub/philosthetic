WEDNESDAY.—11.30, Coleridge-Taylor’s new Orchestral Piece; 
Brahms’s “ Requiem”; Elgar’s‘‘ Light of Life” ; Dvorak’s “ Te 
Deum.” 8.0, Public Hall.—Miscellaneous Concert, with Wagner 
Selections

THURSDAY.—11.30, Parker’s “Hora Novissima’; Palestrina’s 
“Stabat Mater’’; Beethoven's 7th Symphony; Spohr's “ Last 
Judgment.” 7.30, Bach’s  God’s Time is Best’’; Parry’s “ Blest 
Pair of Sirens’; Mendelssohn’s “ Hymn of Praise

FRIDAY.—11.30, “The Messiah

A full Professional Orchestra of about 40 will be engaged for the 
Choral Concerts, and it is hoped, by including in the programmes 
Works of importance which are comparatively seldom heard in 
London, that a special interest in this Society will be awakened in all 
lovers of music

The proposed programme for the first concert, to be held about the 
end of November, will consist of Cowen’s ‘‘ Sleeping Beauty,” Goetz’s 
Symphony in F (“‘ Spring’’), and Mendelssohn's Psalm ‘t When Israel 
out of Egypt came.”’ For the second choral concert it is proposed to 
select Beethoven’s Mass in C or Parry’s ‘St. Cecilia’s Day” and 
Bach's Cantata “Sleepers, wake

For the Chamber Concert a first-class String Quartet will be 
engaged, and as the expense of these three concerts will be considet- 
able, it is hoped that a large amount of support will be forthcoming

EW ORGAN STUDIO, with Three-manual 
PIPE ORGAN, blown by water. Terms for Practice, 1s. per 
hour. At Smyth’s Music Depét, 54, London Road, Forest Hill, S.E

OCK OF HAIR of L. v. BEETHOVEN. 
Guaranteed genuine. Address, W. S., Woodbine Cottage, 
High Street, North Dulwich

CAss OFFER WANTED for 43 Vols. MUSICAL 
STANDARD from commencement, Aug. 2, 1862, to Dec. 29

Bodleian, which is on the same scale of com- 
pleteness that characterises the whole produc- 
tion. The excellent manner in which the book 
is presented to the reader calls forth the 
highest praise—its printing, engraving, and 
internal appearance blend in perfect harmony 
with its handsome gold and white exterior. 
Finally, not only may this be pronounced a 
monumental work, but it is a credit to English 
scholarship ; moreover, the volume is one that 
adds further renown to a name honoured in 
English music—the name of Stainer

THE British Museum has recently acquired an 
interesting collection of manuscripts that formerly 
belonged to George Thomson, of Edinburgh, “the 
friend of Burns.” It will be remembered that 
Thomson, an enthusiastic and successful collector of 
the melodies of Scotland, Wales, and Ireland, secured 
the services of Beethoven, Haydn, Weber, Hummel, 
and some lesser lights to write symphonies and 
instrumental accompaniments to these folk-songs. 
The letters and musical manuscripts that Thomson 
received from his distinguished arrangers form the 
present collection, though we understand that there 
are no musical autographs by Beethoven. Extracts 
from the correspondence and the history of these 
“ arrangements ’”’ have been given by Mr. J. Cuthbert 
Hadden in his * Life” of George Thomson. One 
letter from Hummel, not mentioned by Mr. Hadden, 
may, however, be quoted in its unaltered English. 
The letter, which refers to ‘‘ Three Scotsch Songs, 
accompany’d by J. N. Hummel,” and is dated 
‘“Weimar, the 4th April, 1832,” took fourteen days 
on its journey to Edinburgh. Hummel writes

Dear Mr. Thomson

THE Feis Ceoil has issued the syllabus of composers’ 
Competitions for the 1900 Festival (the fourth). The 
prizes offered are for the best cantata for solo voices, 
chorus, and orchestra; for the best orchestral com- 
position, string quartet, and twelve or thirteen minor 
forms of composition. The money prizes amount in 
value to close upon £100, but composers are more 
likely to be attracted to compete by the fact that the 
Feis committee practically undertakes to produce the 
compositions under the best possible conditions at 
the expense of the Association. Composers are thus 
enabled to get a hearing for their works under 
favourable auspices. All particulars can be obtained 
from the Hon. Secretaries, Feis Ceoil Office, 10, 
Lincoln Place, Dublin

SEVERAL of our Metropolitan and Provincial con- 
temporaries have referred to our July number in 
terms of gratifying appreciation. A London daily 
newspaper, in mentioning the fac-simile reproduction 
of the old Philharmonic programme of May 27, 1844, 
states that ‘ Richter, at the age of thirteen, played 
Beethoven’s Violin Concerto, with Mendelssohn at 
the conductor’s desk.” To have accomplished this 
feat Richter must have been a prodigy of prodigies, 
as at that time he was only thirteen months old. 
No: the statement is, we fear, purely Fiddle-de-dee. 
It was the thirteen-year-old Joachim, and not the 
thirteen months Richter, who played on that eventful 
occasion fifty-five years ago

THE first Swaledale Tournament of Song will be 
held at Richmond, Yorkshire, on September 27 and 
28. The various competitions, for senior and junior 
candidates, and numbering in all eighteen classes, will 
include choral and solo singing, sight reading, piano- 
forte playing, violin playing, &c. Full particulars 
may be obtained from the hon. secretaries, Misses C. 
and M. Yeoman, Prior House, Richmond, Yorkshire. 
The object of the tournament is stated to be ‘the 
encouragement of vocal and instrumental music in 
Swaledale and the surrounding districts.” It will 
have many sympathisers and well-wishers

Evening (in the Public Hall): Miscellaneous concert, 
with Wagner selection

THurspay.—Morning : *‘* Hora Novissima,”’ Horatio W. 
Parker ; ‘‘ Stabat Mater,” Palestrina; Symphony in A 
(No. 7), Beethoven ; ‘‘ Last Judgment,”’ Spohr. 
Evening: ‘‘God’s Time is the Best,” Bach; * ‘ Blest 
Pair of Sirens,” Hubert Parry; ‘“‘ Hymn of Praise,” 
Mendelssohn

Fripay.—Morning : ‘‘ Messiah,”’ Handel

TuHurspAy.—Morning: ‘Samson and Delilah,” Saint- 
Saéns; miscellaneous selection

Evening: “The Golden Legend,” Sullivan; The 
Choral Symphony, Beethoven

Fripay.—Morning : *** King Saul,”’ Hubert Parry. 
Evening: Wagner selection; ‘‘ Hymn of Praise,” 
Mendelssohn

great pleasure, and that I shall esteem it a great 
honour, if anything of mine is given at Paris, and, 
especially, if aman like Habeneck interests himself in | 
i. Please say many, many kind things to him from | 
me. Formerly I was much attached to him, and | 
already at that time he was so kind and considerate, | 
that ever since I have thought of him, not only with | 
respect and admiration, but also with true thankful. | 
ness. Please tell him that, and give him my best 
and heartfelt compliments

Mendelssohn, in his letter, suggests that his 
“Fingal’s Cave ” Overture should be given, and also 
that Habeneck should try over his four, or at any rate 
three overtures, so as to select the one best suited to 
the artists of the orchestra. And he adds: “ Tell 
me if he accedes to my request. In that case please 
correct a misprint in the score and parts of the 
Meeresstille (Calme de la mer). The allegro should 
have the time signature @, instead of @; the move- 
ment is very rapid; a half-bar is about equal to a 
trotchet in the finale of Beethoven’s Symphony 
in A.” His reference to the proposed opera is 
interesting: ‘* You want me to write an opera for | 
Paris! You know how I long to have a libretto | 
fom Scribe; you know that for years I have been 
ardently searching fora genuinely beautiful, dramatic

This was actually done within a fortnight of the receipt of | 
Mendelssohn’s letter, when the ‘Fingal’s Cave” Overture was | 
performed at the Conservatoire, on February 20, 1842, and, more- | 
over, it was repeated on March 27. Habeneck appears to have been | 
stnuinely interested, for the symphonies followed, and on January 9, 
148, there was an In Memoriam concert, devoted entirely to) 
Mendelssohn’s music. The programme included the “Scotch” | 
Symphony, the Violin Concerto, the “ Fingal’s Cave” Overture, and | 
sections from “ St. Paul

the public worship of ‘‘ the Giver of all good 
gifts

The music performed at St. John’s Church, Duncan 
Terrace, Islington, has maintained a position com- 
manding respect ever since the days when Canon 
Oakeley did much to preserve and develop the music 
of the Latin Church schools. The masses and other 
works recently sung there have included specimens 
by Palestrina, Vittoria, Carissimi, Haydn, Mozart, 
Beethoven, Weber, Hummel, Cherubini, Mendels- 
sohn, &c. Orchestral accompaniments have long 
and consistently characterised the musical arrange- 
ments of this church; and it has been for some time 
the rule to perform pieces from the classical 
symphonies, save, of course, in Advent and Lent, as 
Postludes upon those occasions when the orchestra 
is employed

In these days it seems more natural to expect from 
Hong Kong news of perplexing political problems 
than evidences of the advance of Church music. All 
the same, welcome news of special choral services 
come to hand from time to time, and the scheme of 
one such service at St. John’s Cathedral, in the city 
of tea and opium, includes Mendelssohn’s “ Hear my 
Prayer,” Stainer’s cantata “ The Daughter of Jairus,” 
and Sullivan’s “O gladsome Light,” from ‘The 
Golden Legend,” given with Mr. G. Grimble as 
organist and Mr. A. G. Ward as conductor. From 
Sydney, N.S.W., and other remote places similar 
tidings of the welcome development of the Church 
musical service are to hand

LONDON CONCERTS AND RECITALS. 
CONCERTS

A FEw lines must suffice in which to record the last of 
the season’s RICHTER concerts, on June 26, at St. James’s 
Hall. The programme opened with Peter Cornelius’s 
exhilarating Overture to ‘‘The Barber of Bagdad”’ and 
included Liszt’s impressive Fifth Hungarian Rhapsody in 
E minor; Rienzi’s Prayer, the songs ‘‘Traume” and 
“Der Engel,” and the great love duet from ‘ Die 
Walkiire,” by Wagner; and Beethoven’s Fifth Symphony. 
Our readers know how such a programme is performed 
under Dr. Richter’s direction, but even the most regular 
frequenter of these concerts cannot possibly have called to 
mind a greater performance of Beethoven’s masterpiece 
than the Viennese conductor gave us on this occasion. 
It was worthy of ‘‘the greatest Symphony ” ever written. 
Madame Ella Russell and Mr. Ben Davies were the 
vocalists

Mr. CHARLES WILLIAMs’s recently formed orchestra in 
connection with the Passmore Edwards Settlement, Blooms- 
bury, gave a performance, on June 23, at St. James’s Hall, 
in aid of a fund for providing free concerts in poor districts

544 THE MUSICAL TIMES.—Aucust 1, 1899

NOVELLO’S PARISH CHOIR BOOK.—(continued.) 
Cantate Domino and Deus Misereatur. 
172. Baytey, Ws.,inF . oe 6d.| 359. Mammart, E., in B flat 6d. | 233. Tozer, F., in F o- 4 
315. ELvey, Sir GrorGE, ‘inD: 6d. {| 191. STEGGALL, CHARLES, inC .. oe 6d. | 243. TRIMNELL, T. Tatuis, inE- oo 
304. Goss, Sir J., inc. Gd. 
bymns, 
286. AsHTon, A. T. Lee. VesperHymn .. ee ee 1d.{ 270. Jorpan, C. Warwick. Processional Hymn (with pocaied 
200. *BARNBY, The Sower went forth sete ee ee oo Bad, Sentence) .. 3d. 
218. *BarNnBY,J. Crossingthe bar .. ae “ -. 14d. | 323. Mann, A. H. Twelve Popular Hymns ‘with Tunes, Part 3d. 
361. BARNBY, O Perfect Love se oa ee 4S. 324 Mann, A. H. Twelve 4 juan Hymns with Tunes, Part2 34, 
365 Barney, Let all our brethren join i in one . oe 1d. | 339. Martin, G. C. ‘How shall we teach our children .. ie 
329. BEETHOVEN, Vesper Hymn. (Two versions) - oo 1d. | 360. Martin, G.C. The Parish Hymn( Words only, 2s. 6d. per r00) I 
211. Best, W.T. Jesus Christ isrisen to-day . «+ 3d. | 226. PETTNAN, Epcar. Four Evening Hymn Tunes .. 2d. 
298. Best, W.T. Abide with me! fast falls the eventide +» 14d. | 227, Petrman, Epaar. The strain upraise 7 and praise .. 3d, 
299. Best, W. T. Dies Ire, dies illa! (“ Day of wrath! O day 366. Purpay, C.H. Lead, kindly Light . I 
ofmourning”)  . ee oe «. 14d. | 261. STEANE, Bruce. Vesper Hymn. (To be sung after th the 
370. Brewer, A. H. Hymn Tune, “ Esther” ee oo 8Gs Blessing) 1d, 
202. BROWN, ‘A. H. All glory, laud, and honour .. ee «» 14d. | 368, Stocks, W. H. Paraphrase of the “ Anima Christi” ‘ "add. 
280. Brown, A. H. Ride on! Ride on in Majesty oo »» 14d. | 354. *SuLLIVAN, ARTHUR. Onward, Christian soldiers .. Id, 
252. Cray, F. Tlove to hear the story (for Children) .. 3d. | 357. *SuULLIVAN, ARTHUR. Ditto. (Welsh words) id 
325 CRAMENT, J. M. Swiftly the moments of my life are fying 2d. | 358. Sutiivan, A, Hymn Tune, “ Bishopgarth” oo Id, 
281. Euvan- SMITH, Lady. Two Flower Service Hymns oe 2d. | 257, TILLEARD, J. Through the day iad love has spared us .. A 
193. GOODHART, A.M. Lord of all being! throned afar.. ° 2 add. 192. Ture, J. Father of life .. 
386. Hunt, H. G. Bonavia. Dies Ira (Day of mourning) oe 3d. | 264. Vincor, A. L. Vesper Fiyma. (To “be sung after me? 
Blessing) ee a oe eo oe Id 
Lord's Prayer. 
197. Brinae, J. F. (with Apostles’ Creed) .. 0 ve » 14d. | 240. Haynes BAtTIsSoN, in G (Unison) rT ey err ee | 
167. oman V. (with Apostles’ Creed).. ae ee ‘e 
364. Fievp,J.T.,inA .. oo oe oe oe + .. ad.|220. Starner, J. (from Office of the Holy Communion) .. oe Idd. 
Miscellaneous, 
376. Grace. “Forthese and all” (from the Laudi re. 1d. | 339. How SHALL WE TEACH OUR CHILDREN ("‘ Queen beeen 
242. CHorAL Graces. Rev. J.B. Dykes .. 14d. Nurses’”’). G.C. Martin as ee 
302. Farrx, Duty, AND Prayer. Myles B. Foster’ 2d.| 256. Music ror MARRIAGE SERVICE. J. Baden Powell . oo 20 
269. Four EASTER CAROLS. J. T. Field .. .» 2d. | 306. Sow1ne AND Reapine (Harvest Carol). J. M. Crament :. 2d. 
305. MAKE MELODY WITHIN YOUR HEARTS (Harvest Carol) 292. THe Cuurcu Catecuism. Myles B. Foster.. oe o. 3d. 
Rev. F.A. J. Hervey 2d. | 284. Two Conciupinc AMens. Ch, Gounod ee ee «oo he 
* Numbers marked thus * to be had in Tonic Sol-fa, 1d., 14d., and 2d. each. 
EDITED BY SIR GEORGE C. MARTIN, 
Organist of St. Paul’s Cathedral. 
OF THE OFFICE FOR THE 
(INCLUDING BENEDICTUS AND AGNUS DEI) 
FOR 
Price One Shilling each :— 
xy. J. Baptiste Cavxin, in C. 14. Rev. E. V. HAtt, in C. 27. Cuares L, Naytor, in D. 
2. Dr. GARRETT, in A. 15. F. CHAMPNEYS, in G. 28. Rev. E. V. HAL, in E flat. 
3. J. STarner, in F. 16. C. L. Witviams, in G. 29. G.F.WeEsLEY Marti, in E flat. 
4. Sir GeorGE ELvey, in E. 17. ALFRED REDHEAD, in D. 30. J. VARLEY Roserts, in D. 
5. BERTHOLD Tours, in C. 18. Huu Brarr, in F. 31. P. E. HuGHEs, in E. 
6. W.A.C, CruicksHank, in E flat.| 19. J. R. Avsop, in E. 32. G.M. Livett, in G. 
7. Rev. H. H. Woopwarp, in D. | 20. F. Ixirre, in C. 33. T. Lestie CarPENnTER, in C. 
8. Battison Haynes, in E flat. 21. Huu Buarr, inG (Male Voices).| 34. G. F. WESLEY Martin, in E. 
g. Kina Hatt, in C. 22. I. H. StamMers, in E flat. 35. Sir GeorGce C. Marti, in C. 
to. J. F. Bripag, in D. 23. F. Tozer, in G (Treble Voices). | 36. ARTHUR E. GopFrey, in E flat. 
11. G. J. BenneTT, in B flat. 24. J. W. Evuiort, in D. 37- Myves B. Foster, in C, 
12. Harvey Lonr, in A minor. 25. A.C. FisHER, in E flat. 
13. J. T. Fievp, in F. 26. Myves B. Foster, in E flat

Lonpon: NOVELLO AND COMPANY, LiMiTED

tasteful performance of an air from Mozart’s “Il Re 
Pastore ’’ and Michael Arne’s ‘‘ The lass with the delicate 
air.’ Miss Newport and Miss Wehner, the last-named 
doing full justice to Massenet’s ‘‘Pensée d’Automne,”’ 
also ably acquitted themselves. The vocal pieces were 
vatied by instrumental contributions by the Chaigneau 
Trio

Miss JANOTHA’S concert at Queen’s Hall, on the 17th 
ult., though among the last of the season, was more 
interesting than many by which it had been preceded. 
An important feature was Bach’s Concerto in D minor for 
three pianofortes, again played by Miss Janotha, Lady 
Randolph Churchill, and Mrs. Craigie, with an orchestra 
fom the Royal College of Music, conducted by Sir 
Walter Parratt. The performance was thoroughly credit- 
able to all concerned. Besides contributing some piano- 
forte solos, Miss Janotha opened the concert with 
Beethoven’s “‘ Kreutzer’? Sonata, in which her companion 
was Miss Leonora Jackson. Madame Ella Russell did 
justice to Miss Janotha’s ‘‘Ave Maria,’’ and among others 
who appeared were the Sisters Ravogli, Mr. David 
Bispham, and Mr. Whitney Mockridge

A number of pupils of the Royal Normal College for the 
Blind gave a concert at the Crystal Palace, on the rgth ult., 
in connection with the Festival of the Institution. Some 
ladies of the choir sang Wilbye’s madrigal ‘‘ The Nightin- 
gale”? steadily, and equal success marked the rendering 
by the whole of the choralists (totalling over forty) of 
Benet’s madrigal * All creatures now are merry-minded.”’ 
In Mendelssohn’s cantata ‘ Saviour of sinners ’’ and Liszt’s 
“Chorus of Reapers’ the Crystal Palace Orchestra, con- 
ducted by Mr. Manns, took part. The same instrumentalists 
also played the accompaniments of the second and third 
movements of Raff’s Suite in E flat for pianoforte and 
orchestra (Op. 200), the respective solo parts of which were 
spiritedly executed by Mr. Leonard Pegg and Mr. Horace 
Watling. Saint-Saéns’s “Variations on a Theme from 
Beethoven,” for two pianofortes, were played by the Misses 
Mabel Davis and Emily Lucas, and the last-named also 
gave the Introduction and Allegro of Guilmant’s First 
Symphony for the organ

The Royal Academy of Music students’ chamber concert 
at St. James’s Hall, on the 2oth ult., provided a hearing 
for several compositions still in manuscript. The first 
movement of a sonata for violin and pianoforte, by Miss 
Marion White, proved a well-devised and interesting work, 
symmetrical in form and rich in expression. It was ably 
interpreted by Miss Marian Jay and the composer. Mr. 
Herbert Ivey was represented by settings of Tennyson’s 
“A voice by the cedar tree,” ‘ Birds in the High Hall 
garden,” and ‘‘I said to the lily,” of which the second was 
the most distinctive. The two pianoforte sketches by Mr. 
Ernest Torrence, ‘‘ Sorrowful reverie’ and ‘‘ Waltz,” were 
slight productions rather deficient in character. Midway 
in the programme came a lengthy ‘‘ Symphonie Spirituelle”’ 
for strings, by Asger Hamerik, performed by the Ensemble 
Class conducted by Mr. Emile Sauret

Herr Geora LIeBLInG gave a pianoforte recital on 
June 27, at St. James’s Hall, when he was heard to most 
advantage in some clever and showy pieces of his own

Mr. CHARLES FoErsTER, who gave his first pianoforte 
recital on June 29, at the Salle Erard, is not so brilliant a 
Pianist as some who have appeared this season; but in 
Beethoven’s ‘‘ Waldstein ” Sonata, Chopin’s Berceuse and 
Polonaise, and in Liszt’s second ‘‘ Hungarian”? Rhapsody 
he showed himself a cultured and artistic player

The Misses Cerasotr showed at their recital on the 
Ist ult., at St. James’s Hall, that they are making satis- 
factory progress as pianists. They played duets with 
delightful unanimity, and in several solos showed a 
sympathetic and expressive style. Their most obvious 
fault at present is a tendency to hurry in rapid passages. 
Mr. Huco HeE1nz’s postponed vocal recital was held on 
the 3rd ult., at the Salle Erard, and proved very enjoyable. 
His rendering of four songs of Brahms-like character by 
Mr. Walter Imboden was particularly good. Herr von 
Dulong also sang with charming refinement, and some clever 
violin playing was contributed by Mr. Kalman Roth-Ronay, 
a Hungarian

Mr. Herbert Fryer played with so much sympathy 
and skill at his pianoforte recital, on the 4th ult., that the 
future progress of this young pianist will be watched with 
interest. His programme included Schumann’s Etudes 
Symphoniques and a good selection of pieces by Brahms 
and Chopin. An expressive ‘‘Lament” and a vivacious 
Scherzo Caprice of his own composition further increased 
the good impression made by his playing and of his 
abilities

Miss LILLIAN BLAUVELT’s vocal recital, on the 4th ult., 
at the Queen’s Hall, bore witness to the versatility and 
intellectuality of this artist no less than to the beauty of 
her voice and its high training. Her songs were selected 
from several epochs of musical history, extending from 
Pergolesi to Saint-Saéns, and were interpreted in a most 
finished manner. Miss Blauvelt was assisted by M. Ysaye, 
who gave magnificent renderings of the solo part of 
Vieuxtemps’s Fourth Violin Concerto in D minor, Bach’s 
famous Chaconne in the same key, and Beethoven’s 
Romance in G. It may be mentioned that Miss Blauvelt 
was commanded to sing before the Queen, at Windsor 
Castle, on the 17th ult., when she sang ‘‘ Sicilian Vespers,” 
Verdi, and ‘‘Connais tu le pays” (‘‘ Mignon’’), Thomas, 
accompanied by the Queen’s Band, under the conductor- 
ship of Sir Walter Parratt

Miss EpirH KILMINSTER, who gave a recital on the 8th 
ult., at the Salle Erard, is a pianist of decided promise. 
Her interpretation of Beethoven’s ‘‘ Waldstein” Sonata 
showed intelligence and taste, and her programme testified 
to commendable desire to avoid hackneyed pieces

Recitals have also been given at Steinway HALt, by 
Madame Riss-Arbeau, on June 27; Mr. Rudolph Loman 
(second pianoforte recital), on the 6th ult. ; and Miss Marion 
Gordon (vocal), on the 12th ult.; at the SALLE Erarp, by 
Signor Chiti (violin), on the 6th:ult

There were several orchestral pieces, chief among them 
being the “ Italian’? Symphony of Mendelssohn, which 
proved the happiest effort of the band, and was played 
with remarkable clearness and spirit. The curiosity of the 
festival was a long-lost overture by Weber, written soon 
after he became conductor at Prague, as part of the cele- 
brations which followed the defeat of Napoleon at the 
battle of Leipzig. A pure piéce d’occasion, it seems to 
have been put aside as soon as the occasion had passed, 
and Jahns, when accumulating material for his monu- 
mental ‘ Catalogue,” could find no trace of it. Quite 
recently, however, some parts have been found, and 
from them a Dresden amateur, Mr. Klemm, has 
constructed a score, which enabled “ 1813” to be introduced 
to England eighty-six years after it was, to use Weber’s 
own word, “arranged.” This word, by the way, points to 
the employment of existing material; but it is a singular 
fact that the main theme is that of the well known part- 
song ‘‘Liitzow’s Wildejagd,” which Weber wrote (to 
K6rner’s words) just a year later. From this it would 
appear as if the song were a quotation from the overture, 
instead of the reverse being the case. The overture is a 
rather noisy, jubilant, and even rollicking piece, no more 
representative of Weber than was to be expected in a 
composition written almost ‘‘ while you wait,” so short was 
the notice allowed the composer. It would be a godsend 
to a military band

There were two instrumental soloists. In place of Mr. 
Kruse, who was taken ill, Miss Norah Clench came at the 
eleventh hour and played Mendelssohn’s Violin Concerto, 
Bach’s A minor Concerto, and Beethoven’s Romance in G 
with genuinely artistic feeling and much technical ability, 
Mr. Carl Fuchs, of Manchester, was heard in the Andante 
of Molique’s Violoncello Concerto, and in a less familiar 
concerto by Haydn, which he has recently been playing 
with deserved success, Among the vocal solos there must 
be mentioned a dramatic setting, by Dr. Charles Wood, of 
Walt Whitman’s ‘ Ethiopia saluting the colours,” which 
well suited Mr. Plunket Greene’s emotional style

THE NATIONAL EISTEDDFOD. 
(FROM OUR OWN CORRESPONDENT

THE ROYAL COLLEGE OF MUSIC CONCERT

Ir never rains but it pours! Variations more or less 
learned and interesting on themes more or less original 
are just now being produced in unprecedented and alarming 
quantities, and it would really be a welcome change to 
have a little more ‘‘theme”’ and continuity, and a little 
less  varying’’ in some of the novelties upon which we 
are called to sit in judgment. At the final concert of the 
summer term, on the 18th ult., we were regaled with yet 
two more sets—viz., one for orchestra on the air ‘* Old 
King Cole,” by a student, Nicholas Gatty ; and a 
second for violoncello solo and orchestra by Boéllmann. 
Mr. Gatty’s work displays good workmanship, plenty of 
ingenuity, and some rather conventional humour. He 
certainly succeeded in causing us to make a mental note 
of his name so as to watch his future doings with interest 
and hope.. R. Purcell Jones played the solo part in 
Boéllmann’s variations extremely well; he is indeed a violon- 
cellist of exceptional promise. In Schumann’s most lovely 
Pianoforte Concerto, Florence Smith, a very youthful 
student, displayed a surprising amount of brilliancy, 
passion, and poetry, and if her physical strength was 
not quite equal to the great demands made upon it by 
the superb Finale, she may yet be credited with a most 
excellent achievement. Ivor Foster sang Handel’s ‘‘Del 
minacciar del Vento’? (‘Ottone”). His well trained, 
ringing baritone, and manly and artistic style did full 
justice to the music. Agnes Nicholls, Gwilym Evans, 
and Madoc Davies were heard in Beethoven’s trio ‘‘ Tre- 
mate, empj, tremate,” and the orchestra gave creditable 
performances of Schumann’s ‘‘Genoveva” Overture and 
Mozart’s ‘‘ Jupiter” Symphony. But what is the matter 
with the College strings? they certainly do not display 
the same tone and technique as of yore

TONIC SOL-FA FESTIVAL

statue will be formally handed over to the town by the 
honorary president of the memorial committee, Dr. 
Joseph Joachim. On returning to the church at noon 
Brahms’s “ Triumphlied” will be performed. In the 
evening, at 7 p.m., an orchestral concert will be given in 
the Court Theatre, with a programme consisting exclusively 
of works by Brahms—viz., the ‘‘ Tragic” Overture, the 
four “Serious Songs” (Dr. Kraus), the Concerto for 
pianoforte and orchestra (No. 2) in B flat (Eugene d’Albert), 
the Rhapsody for alto solo and male choir, and the 
Symphony (No. 2) in D

On October 8, at 11.15 a.m., there will be a performance 
of chamber music by the Joachim Quartet in the Court 
Theatre, the programme of which includes a String Quartet 
in C major by Haydn, Brahms’s Trio for clarinet, violon- 
cello, and pianoforte, and Beethoven’s Quartet in C sharp 
minor. In the evening Bach’s cantata ‘‘ Gleich wie der 
Regen und Schnee vom Himmel fallt,’’ Beethoven’s Violin 
Concerto (Dr. Joachim), and the Ninth Symphony will be 
performed in the Court Theatre

On October g, at 11 a.m., Mozart’s String Quartet in 
G minor, a Quartet by Schumann, and Schubert’s String 
Quintet will be heard at a second chamber music concert. 
In the evening there will be an orchestral concert, when 
the programme will include Schubert’s Symphony in B 
minor (Unfinished), Mozart’s Pianoforte Concerto in A 
major (Leonard Borwick), vocal pieces, Brahms’s Variations 
on a theme by Haydn, and his Symphony (No. 4) in E 
minor

A festival performance of Beethoven's “ Fidelio” is in 
view for October 10, and from October 6 to 10 a collection 
of portraits and busts of Brahms, with other interesting 
mementos of the master, will be on view. The whole 
festival will be under the direction of the general music- 
director, Fritz Steinbach, and seats for the six concerts 
may be obtained, at a cost of 25s., of the banker, B. M. 
Strupp, in Meiningen

THE INTERNATIONAL CONGRESS OF WOMEN

Dr. Buck commenced his duties as organist of the 
Cathedral on June 25, upon which occasion the Mayor 
and Corporation attended in state in order to celebrate the 
honour bestowed upon the city by the chief magistrate 
being created Lord Mayor. In order to enable the lay 
clerks to take their summer holiday there will be no 
musical services in the Cathedral from the 31st ult. until 
the 12th inst

Master Frank Merrick, thirteen years of age, son of Dr. 
Merrick, of Clifton, gave a pianoforte recital on June 27, at 
the Victoria Rooms. In November, 1895, when nine years 
of age, he gave his first recital at Clifton, and, in the mean- 
time, M. Paderewski, who was charmed with his playing, 
gave him a letter of introduction to his own instructor, 
Professor Leschetitzky, at Vienna, under whom Master 
Merrick has been studying with success. His re-appearance, 
locally, was welcomed by a large and enthusiastic audience. 
He performed compositions by Bach, Mozart, Beethoven, 
Weber, and other masters with pleasing confidence, and 
his efforts were characterised by considerable brilliancy. 
It is probable that he will return to Vienna to continue his 
studies. P

Midsomer Norton and District Choral Society had a 
concert in the grounds of Norton House, on June 27. The 
programme was made up of glees and part-songs, sung by 
the choir, and solos contributed by Miss Florence Cromey 
(soprano), of Bristol, Mr. Trowbridge (bass), of Wells 
Cathedral, and Mr. W. J. Kidner, conductor of the Society. 
Mrs. J. Thatcher was at the pianoforte

BERLIN.—A new music-drama in five acts, ‘“ Die ver- 
sunkene Glocke,” by Heinrich Zollner, was brought out 
at the West-End Theatre, on the 8th ult., and its reception 
by a numerous audience was highly favourable. The 
libretto, from the pen of the composer, is founded upon 
Gerhart Hauptmann’s popular drama. At the same 
theatre Herr Bertram, the whilom Munich baritone, and 
his wife (Frau Moran-Olden) have made several appear- 
ances recently, with considerable success. A new opera, 
“Cain,”’ by Eugene d’Albert, to which H. Bluthaupt has 
written the libretto, has been accepted for performance at 
the Royal Theatre——The proceeds of the concert (about 
5,400 marks) recently given in honour of Dr. Joachim have, 
in accordance with the expressed desire of the great 
violinist, been divided between the fund for the projected

joint Haydn-Mozart-Beethoven monument, in Berlin, and 
that for the Goethe Monument, to be raised in Strassburg

CoLoGNne.—A new Overture to Shakespeare’s ‘‘ Tempest,” 
by Anton Urspruch, was recently produced at the fourth 
popular symphony concert, under Dr. Wiillner’s direction, 
and greatly appreciated by reason of its impressive themes 
and highly effective instrumentation

Warwickx.—On the evening of the 11th ult. Mr. John 
Hills, on behalf of the Warwick Musical Society, presented 
Mr. W. H. H. Bellamy with a gratifying testimonial upon 
his resignation of the conductorship of the Society, after 
holding it for thirteen years. The present took the form 
of a handsome solid silver inkstand and pen-holder, bearing 
a suitable inscription

WELLINGTON (NEW ZEALAND).—The Orchestral Society, 
under the able conductorship of Mr. Maughan Barnett, 
gave a concert in the Opera House on June 6. The pro- 
gramme included Beethoven’s Fourth Symphony, Wagner’s 
“Flying Dutchman” Overture, and works by Handel

Au

CONTENTS, 
from | MENDELSSOHN.—War March, 
| MENDELSSOHN.—Funeral March

BEETHOVEN, — March 
“Egmont.” 
BEETHOVEN.—Funeral March. | (Op. 103

Cuopin.—Funeral March(Op. 35). | MEYERBEER.—March from “Le 
HANDEL, — Dead March from | Prophéte

London: NoveLLo anp Company, Limited

Funeral March (Sonata, Op. 26) .. Beethoven. 
. Ditto (Sonata, Op. 35)-- Chopin. 
Dead March (“ Saul’’) Handel

a. Cc. Mackenzie

7. Jubilant March

BOOK 7. 
1. Impromptu... P John E. West 
2. Minuet, from Op. 10, No. 3 Beethoven 
3. Introductory Voluntary S. J. Rowton 
4. March .. +. Oliver O. Brooksbank 
5. Sunday Song .. -Max Oesten 
6. Minuet and Trio, from Op, 9 No. 1 Haydn 
7. Pastorale we Alfred W. Tomlyn 
8. Religioso «e -. T.L. Southgate 
BOOK 8 
1. Elegy .. ae ee ..C. H. Lloyd 
2. Rondo .. -» Couperin 
3. Andante Grazioso "Frederick A. Keene 
4. Soft Voluntary.. “ Ferris Tozer 
5. Allegretto Pastorale Ww. John Reynolds | 
6. Andante in F (Quartet in D minor) Mozart | 
7. Meditation ' olstenholme 
8. Finale, from Pf. ™ (Op. 88) Schumann 
g. Canon .. F .. Battison Haynes 
* BOOK 9. 
1. Larghetto F. Cunningham Woods 
2. Recessional March «» E.H. Fellowes 
3. Allmen,allthings .. Mendelssohn 
4. Allegro poco maestoso W. G. Cusins 
5. Communion .. -- Alfred R. Gaul 
6. Andante con moto John Francis Barnett 
7. Andante Religioso ..Alfred W. Tomlyn 
8. Evensong - +» Cuthbert Harris 
g. Minuet (Quartet i inG minor) Schubert 
o. MelodyinA .. «»  W.H. Callcott 
BOOK 10. 
1. Allegro moderato ee .. E. Bunnett 
2. Opening Voluntary .. Ferris Tozer 
3. While my watch I am keeping Gounod 
4. Meditation .. E. D’Evry 
5. Slow Air, from Suite de Piéces Lully 
6. Allegretto Pastorale .. H.A. J. — 
7. Allegretto Grazioso . Mozart 
8. Hallelujah Chorus (“ Messiah ") Handel 
BOOK 11, 
1. Pastorale ee .» Battison Haynes 
2. Gavotte .. ae ee re Boyce 
3. Evensong . Kate Boundy 
4. Minuet (Organ Concerto i in B flat) Handel 
5. Allegretto 7 Oliver Brooksbank 
6. Marche Funébre (Op. 72, No.2) Chopin 
7. Communion . ..W. Wolstenholme 
8. Prelude in E minor and Chorale Bach 
g. Andantecon moto .. G.A. Macfarren 
BOOK 12. 
1. Berceuse (Op. 77, No. 3) A. Guilmant 
2. Introductory Voluntary Hamilton Clarke 
3. Prayer .. re Bruce Steane 
4. Melody .. on Coleridge- Taylor 
5. Eventide “e +» Clowes Bayley 
6. Postlude.. ° Josiah Booth

SW. John Reynolds

BOOK 18. 
FUNERAL MARCHES

Funeral March (Sonata, Op. 26) Beethoven

Ditto (Sonata, Op. 35) a Chopin 
3. Dead March (# Saul’’) “a Handel

