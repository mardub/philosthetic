HANOVER SQUARE ROOMS , SATURDAY , JUNE 4 

 programme include sonata G minor , Piano Violoncello 
 ( Beethoven ) ; trio C minor , Piano . Violin Violoncello ( Men- 
 delssohn ) ; variation , piano ( Schumann ) ; Kreutzer Sonata , 
 Piano Violin ( Beethoven ) ; New sacred Songs Piano 
 Solos ( Walter Macfarren 

 artist : 
 Madame SAINTOON - DOLBY , 
 Mr. W. H. CUMMINGS , 
 M. SAINTON , Herr DAUBERT , 
 Miss JOSEPHINE WILLIAMS , 
 d 

 come correspondent 
 write subject con- 
 fidently believe right reply ; 
 accuse want courtesy 
 pass communication , let 
 reader idea . 
 ask kindly ascertain exact 
 height Mr. Sims Reeves ; 
 enquire time celebrated vocalist 
 sing certain ballad ; 
 good music - paper procure . , 
 suppose genuine question , 
 imagine editor musical journal 
 attend ' ? leave office , 
 piece tape pocket , Mr. Sims 
 Reeves residence measure ? shall 
 expect ask kindly search 
 programme concert appear 
 , carefully note number time 
 sing ballad question ? 
 desire shall specimen music - paper 
 shop London , test , 
 good ? genuine 
 communication , writer 
 little acquaint duty editor ; 
 joke , 
 bad . case author 
 scare : ly astonish receive reply — 
 tell answer question 
 decide bet , de wish encourage 

 organist justify retain key 
 instrument refuse let anybody play 
 ; poor music - lover , save 
 little money , buy harmonium 
 pianoforte ; vocalist " " G , 
 proceed " " ; 
 lately ask equivalent person 
 likely receive stock old music , 
 question evidently address 
 useful little publication , Exchange 
 Mart , find lady glad 
 accept volume Beethoven Sonatas exchange 
 real persian cat 

 correspondent 
 trouble send notice concert 
 musical event , write illegibly 
 compel guess half content 
 letter . supposition , presume , 
 power writing , like language , bestow 
 purpose disguise thought , 
 recourse sort method accom- 
 plishe object : writing simply series 
 sharp point , slight attempt letter 
 eviden'ly studiously avoid ; 
 resemble copy - book style , adorn flourish 
 word , impossible 
 find letter end ornament 
 begin . encounter 
 describe " defiant " style ; 
 ( generally write enormous sheet paper ) 
 letter stand bolt upright , large , 
 perfectly illegible , challenge 
 read wrongly peril . 
 handwriting come , peculiarity 
 space describe , 
 characteristic , owner 
 unite common object — necessity 
 conceal write . 
 reasonably consider good 
 read epistle , author , 
 arule , satisfied ; rarely case : 
 Brown , conduct concert country , 
 ' " ' usual ability , " content 
 print ' " Brune " ( refer 
 letter , particular reason 
 ' Bones " ) forthwith there- 
 fore come indignant letter remonstrance , 
 hope justice rectify 
 error , ' ' careful future . " 
 course notice admonition 
 ' doubt 
 write subject fancy 
 ill - use consequence ) , 
 remember remedy rest . 
 pain read send 
 correspondent write , 
 print correctly ; , 
 require , , little care 
 send report musical performance , 
 stipulate compact shall 
 equally fair party 

 sort tacit threat speak 
 , dreadful happen . 
 author tell think 
 work — ( ' ' little pianoforte piece , " 
 , " exceedingly pretty , original " ) 
 exception , composer generally 
 prefer opinion . 
 piece review reasonable time 
 send , letter write , 
 request sufficient reasou 
 delay , enclose stamp reply . 
 occasionally forgive neglect work , 
 condition notice new ; ona 
 recent occasion , composer regret 
 pass Waltz , trust 
 review Polka " number . " 
 importunate correspondent 
 perform duty conscientiously : 
 spite immense quantity music send , 
 throw aside consideration , 
 composer submit work 
 review , imagine 
 entitled expression opinion , 
 position performer 
 public concert , execution 
 piece , insist member 
 audience audible sign approval 
 dissent 

 afraid useless 
 person favour write account 
 musical performance infiuitely 
 prefer statement occur elaborate 
 criticism , merely express hope 
 feeling 
 possible , particularly 
 try know . let set 
 learn correspondent right inform 
 song mention sing 
 applause suabian melody , 
 compose ' ' Suabian ; " Mozart 
 write J ! Barbiere di Siviglia , ' ' questa 
 tomba , " Beethoven , Opera 
 £ idelio 

 lately , find , 
 custom vocalist puff adver- 
 tisement column newspaper , state date 
 place engagement sing certain 
 song ; presume desirous 
 use Musical Times vehicle record 
 success ; receive little packet , 
 artist relative 
 friend , contain short account concert 
 sing , long account 
 singing . let portion 
 journal devote country news , desire 
 chronicle opinion event 
 event ; , interesting 
 progress vocalist , 
 progress art infinitely import- 
 ance reader . beg , , announce 
 shali future decline insert account 
 performance , obvious purpose , 
 cover notice concert , advertise 
 singer 

 MUSICAL TIMES.—Jvcne 1 , 1870 . 491 

 fugue conclude work ( repetition the| english concert - giver insist " 
 opening choral movement ) bold scholastic | Power Sound , " perform excellent effect , 
 high degree , amply prove composer can|Beethoven Symphony D great 

 cast thought fine old solid form write | orchestral work . 
 vigour | 
 brilliancy treatment voice 

 concert Society , 25th April 

 commence Schumann fine unequal Symphony | 
 E flat , receive warinth | 
 expect philharmonic audience . 
 feature programme Madame Schumann 
 truly intellectual reading Beethoven Pianoforte ( on- 
 certo G. merit performance 
 searcely think opinion exist . 
 ' ¢ preislie , " sing Dr. Gunz , favourable 
 specimen Wagner " Die Meistersinger , " trust 
 duty compel hear rest Opera , 
 assuredly pleasure prompt hazard 
 experiment . 9th ult . fourth concert.was 
 , Prince Princess Wales present 
 symphony Haydn , D , Beethoven 
 - welcome " Pastoral . " Schumann Pianoforte Con- 
 certo , minor , play Madame Auspitz 
 Kolar , lady 
 performance Crystal Palace . applause 
 spontaneously enthusiastic movement , 
 compel return platform receive 
 renew congratulation audience . Madlle . Ilma 
 di Murska vocalist , charm everybody 
 exquisite singing hungarian air . 
 fifth concert , 28rd ult . , Spohr Symphony , 

 principal vocalist , Madame 

 MUSICAL TIMES.—Jvnz 1 , 1870 

 vocal department Miss Edith Wynne , Miss Fanny 
 Armytage , Mr. Harley Vinning , instru- 
 mental department Mr Lazarus ( ( larionet ) , Mr. 
 Barrett ( obo ) , Mr. Charles Harper ( horn ) , Mr. Hut- 
 chins ( bassoon ) . Mr. Prentice solo piece Bach 
 Prelude fugue E ( . 9 , book 2 ) , Beethoven 
 Sonata c major ( Op . 53 ) , Mendelssohn Capriccio 
 E major ( . 2 , op . 33 ) , 
 thoroughly successful . sustain pianoforte 
 Weber Sonata E flat ( Op . 48 ) , pianoforte 
 clarionet , Larghetto Mozart Quintett E 
 flat , pianoforte , oboe , clarionet , horn , bassoon , 
 Beethoven Quintett E flat ( Op . 16 ) , 
 instrument . effective piece vocal selec- 
 tion M.S. song Madame Sainton - Dolby 
 ( excellently Miss Edith Wynne ) , second 
 , " , " unanimously re- 
 demand 

 Miss Emma Busby concert 
 17th ult . , Hanover Square Rooms . prin 
 cipal vocalist Madlle . Angelina Salvi Madame 
 Anna Jewell ( place Madlle . Florence Lancia . 
 unavoidably absent ) , instrumentalist ( 
 concert - giver , sustain pianoforte 
 concerted piece ) Mr. Carrodus ( violin ) . Signor 
 Pezze ( violoncello ) . programme select , 
 Miss Busby favourably receive 
 performance 

 pathie audience ; composer Trio 
 C minor ( join Herr Louis Ries 
 ( violin ) , M. Paque ( violoncello ) playing elicit 
 warm demonstration applause . equally 
 successful Ferdinand Hiller duet pianoforte 
 ( conjunction Mr. Deacon ) , brilliant artistic 
 composition assuredly way 
 desire mere display . Mr , 
 Thorne favourably know composer 
 sacred music ; judge reception 
 " Noél , " sing Madame Rudersdorff , chorus 
 ( harp obdligato , Mr. Cheshire ) , little 
 doubt public endorse high opinion 
 work frequently express 
 journal . - song , , " dream home , " 
 excellent specimen pure choral writing , 
 receive deserve applause . solo play 
 violin Herr Ries , violoncello M , 
 Paque , success . Madame Rudersdorff , Miss 
 Ida ‘ horne Madlle . Enriques vocalist , 
 Signor Randegger conductor 

 Mr. Water Macrarren second 
 Matinées , Hanover Square Rooms , 
 7th ult . , large audience . instrumental 
 department ably assist Mr. Henry Holmes 
 ( violin ) , Mr. Burnett ( viola ) , Signor Pezze ( violoncello ) , 
 Mr , Stephen Kemp , Royal Academy Music , 
 ( pianoforte ) . Mr. Macfarren solo Beethoven 
 " sonata pathétique , " elegant trifle , 
 ' music Lake " " spinning song , " ( charming 
 little piece . perform time ) , " 
 polonaise , " warmly deservedly 
 applaud . Mendelssohn variation D ( Op . 17 ) , 
 pianoforte violoncello , Schumann quartett e flat , 
 pianoforte , violin , viola violoncello , Beet- 
 hoven sonata G , pianoforte violin , 
 interesting item programme . piano- 
 forte work sustain Mr. Mac- 
 farren true artistic feelig , power interpret 
 varied style composition display 
 utmost advantage . Mr. Cipriani Potter capitally 
 write duet pianoforte ( concert- 
 giver join highly talented pupil , Mr. Kemp ) , 
 enable pay graceful tribute master 
 introduce excellent music , 
 neglect concert - room . singer 
 Miss Robertine Henderson , encore beautiful 
 sung « « Star Message , " composition Mr. 
 Berthold Tours ; Miss Sinclair , receive 
 compliment Mr , Macfarren new sacred song , 
 " Lord Shepherd . " vocalist , 
 rapidly rise public favour , 

 Schubert song exquisite taste , 
 | applaud . omit mention 

 Mr. E. H. Tuornr , late Organist 
 Chichester Cathedral , concert London 
 2nd ult , Hanover Square Rooms , pro- 
 gramme select eminently calculate prove 
 concert - giver right place : ngst exponent 
 classical music long award , 
 city year re- 
 . town excellent Pianoforte 
 Recitals know highly value . Mr 

 Thorne performance Beethoven solo Sonatain flat 

 Mr. Henry Lestiz Concert , 29th 
 April , peculiarly attractive general public , 
 list solo vocalist include Mesdames 
 Sinico , Trebelli Bettini , Madle . Ilma di Murska . Signori 
 Mongini , Bettini , Foli Castelli . madrigal 
 - song excellently sing choir ; 
 programme , scarcely select 
 admirer Mr. Leslie finely train body vocalist , 
 varied interest . ' l'wo concert 
 " summer series " , 
 month success , Rossini " Stabat Mater " ’ 
 principal work performance . Madame 
 Sinico , ably supply place Madlle . Christine 
 Nilsson ( tov ill appear ) sing 
 , excellent effect ; familiar tenor 
 bass solo popular work receive ample justice 

 success ; Mr. James Robinson officiate 
 conductor . Mr. Hallam possess good tenor voice , 
 judge enthusiastic manner 
 receive , succeed favourite 
 audience . concert respect 
 decide success 

 tue Concert Schubert Society 
 present season place Beethoven Rooms , 
 28th April . ' vocalist Miss Amy Strang- 
 ways ( successful début ) , Miss A. Oxley , 
 M. Leonee Waldeck , Mr. Stedinan ; instru 
 mentalist Madame Sidney R. ! ' ratten ( guiar ) , Herr 
 Schrattenholz Mr. Charles Malcolm ( pianotorte ) , Herr 
 Josef Ludwig ( violin ) , Herr Schuberth ( viol » ncello ) . 
 devote work Spohr , 
 second miscellaneous selection , 
 appreciate large fashionable audience 

 Monday evening , 9th ult . , concert 
 Manor Rooms , Hackney ( use 
 hand - omely present occasion ) , 
 aid Building Fund Christ Church , Clapton , 
 Rev. G. Preston Reed incumbent . 
 programme varied attractive , evident 
 satisfaction audience , somewhat demon 
 strative approbation . Miss Jopp , Mrs. Batchelor , 
 Miss Helling , Miss Lancaster , Miss Weller , Miss Edwards , 
 Miss Bartholomew , Mr. F. O. Stevens , ' ir . Willis , Mr. J. 
 J. Stevens , Mr Hagon , Mr. Ornstein , acecompa- 
 nist Mr. J. A. Jopp , deserve honourable mention 
 effort evening , performance , 
 , highly satisfactory . weare glad record 
 Concert add nearly £ 40 fund need 
 help . Mr. W. C. Batchelor conductor 

 Robert Fieldwick 

 goop word duet , 
 carefully arrange t » lie easily hand 
 achild , air exceedingly select . 
 french air , waltz Beethoven , portion Haydn 
 Military Symphony , " Florence Waltz , " Mr. 
 Fieldwick . Minuet Don Giovanni , con- 
 taine book . f rm sufficiently varied selection 
 exacting little performer 

 Evening Sea . Reverie , Pianoforte . 
 compose Gustav Wolff 

 AperpeeN.—A excellent performance Men- 
 delsrohn Oratorio , St. Paul , Choral Union 
 29th April , large thoroughly appreciative audi- 
 ence : Madame Corri em'nently successful air , " Jerusa- 
 lem ; " Miss Harrison ' " ' Lord mindful " 
 good feeling expression elicit encore ; Messrs. 
 Parkinson , Haydn Corri Henry Corri justice 
 trying solo allot . chorus 
 utmost precision effect . band complete efficient ; 
 anda good word Mr. Adlington organ accompa- 
 niment , judiciously play . Mr. justice jun . , 
 lead band ability ; credit 
 careful manner train choir 

 Ay.espury.—The member Vale Aylesbury 
 Sacred Harmonic S.ciety second concert 
 season Corn Exchange 17th ult Beethoven Mass 
 C. selection Liijah perform suc : ess , 
 p incipal vocalist Mrs , Parslow , Miss Terry , Mrs Taylor 
 Mrs , S G. Payne . Mr. Wootton , Mr. Ingram , Mr. C. Butler , 
 Mr Smith . chorus excellently render ; 
 Mr. , P. C. Corfe conduct concert ability 

 Baxcor.—The Bangor Choral Society 
 Concert season onthe 26th April , distinguished patron- 
 age . professional vocalist- engage occasion — 
 Madame Billinie Porter Mr J. Halliday , principal tenor 
 Chester Cathedral Madame Billinie Porter highly successful 
 " softly sigh " ( Weber ) * Litile Willie . " song 
 encore . duet Mr. Halliday , " sound harps 
 angelical " ( Donizetti ) , encore voice displayedtomu : h 
 advantage . Mr. Halliday Misses Hopson likewise 
 commen:‘ed rendering solo music allot . 
 Choral Society sing glee - song , th : 
 effective Bishop ' allegiance swear . ' 
 Miss Swainson pianofo'te soo ( encore ) ; overture 
 perform duet Mr. Haywood Mr. Binns , Miss 
 Swainson ably accompany , Mr. T. S. Binns , Organist St 
 James , Upper Bangor , conduct 

 Beprorp.—The member Bedford Amateur 
 Mu - ical Society recently Concert fourth season 
 Assemb y Rooms , effect . work select 
 Haysin Creation . principal vocalist Miss Sofia 
 Vinta , Mr. E. Lloyd , Mr. Lewis Thomas , 
 highly efficient exacting solo music 
 entrust . Mr. P. H. Diemer , R.A.M. , conduct work 
 ski!l ; intelligence great portion success 
 performance attribute 

 Bessxs.o’-rH’-Barn , NEAR Prestwich.—Mr . W. T. Best 
 ( org - nist St. George Hall , Liverpool ) highly interest- 
 e Organ Recitals Monday afternoon evening , 23rd ult 
 programme selec e work eminent 
 Masters , include name- Bach . Beethoven , Mendel - sobn 
 Handel , hc . performance listen attention 
 bv lovee audience , evening Mr. Best assist 
 efficient choir 

 Bisnop Srorrrorp.—On Thursday evening , 5th ult . , 
 concert Miss Amy Perry , pupil Mr. Lansdowne 
 Cottell , conduct . Miss Perry prove piamst 
 intelligence ; receive flattering mark 
 approval patron . vocalist Miss Alexandrina 
 Dwight . Mdlle . Marie D'Elise , Mr. J. H. Sutcliffe , Mr. F. Childer- 
 stone , Mr. C. J. Bishenden ; solo viol : ni - t , Master W. Parker . 
 speuke generally , performer render music 
 ability . lady singer popular . encore 
 enthusiasm . Mr. Sutcl ffe solo highly effective . 
 - song duet applaud , 
 - demand 

 Brooxtyn , New Yorx.—The fifth concert 
 twelfth season Prilharmonic Society 
 Academy Music , audience crowded fashionable 
 usually attract performance . Beethoven Symphony 
 C minor play degree « f accuracy finish seldum 
 equal » t concert Brooklyn Society . vocal 
 solo render ; " ( rest Lord , " Miss 
 Adelaide Phillips , succeed forth enthusiastic 
 encore . pianoforte playing Mr. Richard Hoffman 
 enjoyable programme , mark great 
 delicacy expression total freedom meretricious orna- 
 ment . Mr , Carl Bergmann occupy usual positiun conductor 

 Burstem.—A successful performance Handel 
 Oratorio . Samson Potteries Tonic Sol - fa Choris . 
 ter , 27th April , Town Hall . princijal vocalist 
 Miss + mpsall , Miss Franklein , Mr. Nelson Varley Mr. 
 Brandon . ' lhe chorus execute . especi- 
 ally " awake trumpet lofty sound . " " fix everlasting 
 seat , " * * weep Israel . " Mr. J W. Powell conduct work 
 usual ability . performance aid 
 fund Wedgwood Institute 

 reflect high credit choir . efficient training 
 Mr. J. Munns , Organist St. John Church 

 Dousiix.—The member St. Jude Choral Union 
 second Concert season | ecture Hall , South 
 Richmond , Wednesday evening . the4th ult . programme 
 vary sele - te , performance reflect credit 
 conductor , Mr. R. R. Mactagun , Organist Church . 
 beforethe termination concert , Rev. T. Mills Vicar 
 St. Jude , present Mr. Maclagen address purse 
 sovereign , behalf member , previous departure 
 Montreal , appoint Organist ( hrist 
 Church Cath+dral — — excelient concert ' Monthly 
 Popular " series . Messrs. Gunn , Exh bition 
 Palace . 5th 6th ult . , offer utmost attraction 
 lover classical music programme select exclusively 
 work good writr . spec'al feature 
 performance appearance Mr. Silas , artist 
 reputation pianist composer establish 
 London appearance 1n Dublin naturally look 
 forward interest . respect success 
 decisive . Rubinetein ' ' Melody F " encore , 
 , response , perform equ»l effect Gavott » E 
 minor , composition . Mendelssohn * Andante con 
 variazioni D , " pianoforte violoncello . Beethoven 
 Trio b flat . violin , viola , violoncello , 
 ably sustain pianoforte , alxo highly 
 successful . second play : d svlo 
 composition . " * Prelude Impromptu , " , encore , 
 Impromptu f sharp major . sec nd 
 concert Mr. Silas play Impromptu , graceful piece 
 * * Malvina . " « nd Chopin * * La Berceu - e , " ws 
 warmly deservedly applaud . musical succe 8 
 concert undvulted ; sincerely trust interesting 
 instructive perfurmance continue support 
 deserve 

 Eastsournét.—The Eastbourne Tonic Sol - Fa class . 
 conduc’e Mr. S. Evershed . bring study close 
 pa - t month publicrehearsal Trinity Schools 
 pupii , regular attendance 
 course , sing thirty piece , , consider short 
 time class instruction . bo h time tune 
 rem=«kably we'l . end . f class 
 sight - singing exercise compose conductor . 
 sol - fae cifferent , collectively . finally , 
 minute , sing t » word remarkable precision 
 effect . ihe second choral piece 
 excel ently reflect utmost credit Mr , Evershed , 
 energetic conduct r class 

 Giascow.—A Concert Sacred Music 
 5th ult . , Queen Park Church , Sullivan Oratorio , 
 Prod gal Son , , able direction Mr. R. Donald- 
 ron , success . absence instrumental accom- 
 paniment , Mr. Berger pianoforte , Mr. Cha : les Ferguson 
 organ , good idea effect score ag 
 imagine circumstance , + election miseel- 
 janeous music follow , proceed performance 
 Choir Fund.——Txe Glasgow Choral Union 
 performance custom lead public 
 expect . nave Cathedral , Saturday afternoon , 
 14th ult . programme consist exclusively selection 
 Mendelssohn Elijah solo render , 
 chorus utmost precision effect . 
 unaccompanied trio , ' * * lift thine eye , " especially worthy 
 commendation , singer Miss Margaretta Smyth , Miss 
 Turney . Miss M‘Nauchten . Mr. Lambeth usual , con- 
 ducte , Mr. Charles Ferguson preside harmonium 

 GopMancHEsTER.—On 28th April Godman- 
 chester Choir Concert Classical Music large 
 audience distinvuished patr nage . open 
 Beethoven Overtare man Prometheus , play 
 pianoforte durt < srs . Ding Chesterfield ; second 
 commence Beethoven ' * Sonata Pathétique , " perform 
 Mr. Albert Chesterfield . music render 
 highly satisfactory manner ; great praise Mr , A. 
 Chest+rfield ( Organist ) bring choir present state 
 efficiency 

 Goo.tt.—On Tuesday evening , 8rd ult . , enter- 
 tainment , consist music intersperse reading , 
 Scientific Ha'l , aid fund provide work 
 implement carpenter suffer severe loss fire 
 Mr. M. Pearson shipbutiding yard , princip«l vocalist 
 Mrs. Ibbotson . Miss Smith , Messrs , Hopley , Sutcliffe 
 Greenwood , al ! highly successful , : eiving 
 encore Mr. Jillot vielin solo ( excellently accompany Mrs. 
 Lindsay ) marked feature programme ; violin 
 duet , Messrs. Jillot ani Lindsay , enthusiastically re- 
 demand vocal music accompany Miss Milnes 

 shortly publish 

 handbook BEETHOVEN 

 symphony , amateur . Grorce Grove , 
 Secretary Crystal Palace Company 

 Macmillan Co. , London 

 sixth THOUSAND , price 3 . , bevelled cloth , illustrate , 
 treet PLIANOFORTHE , account 
 Ancient Music Musical Instruments . EpGar 
 BrinsMEAD . 
 " contain créme de la créme subject . ”—Eraminer . 
 " subject handle , popular form . ”—Grapiic . 
 " serviceable pleasant . ”—Daily Telegraph . 
 " interesting . " — Echo . 
 " good account piano . ”—Morning Advertiser . 
 " everybody interested music read valuable little 
 book . " — Musical World . 
 " interesting . ”—Court Circular . 
 Cassell , Petter Galpin , London New York . 
 hotograph musical celebrity . 
 — NOTICE.—Mr . JOHN TOWERS , answer numerous 
 enquiry respect work , announce herewith follow- 
 ing Portraits comprise Collection : — Albrechtsberger , 
 Arne , Bacu , BeETHoveN , Boyce , Cherubini , Clementi , Corelli , 
 Dibdin , Dussek , Gluck . HANDEL , Hasse , Haypn , Hummel , Marcello 
 Mendelssohn , Meyerbeer , Mozart , Paganini , Palestrina , H. Purcell , 
 eau , Reichardt , Remberg , Rossini , Salieri , Schubert , Spohr , 
 Steibelt , Weber . , thirty - portrait 
 group large Photograph — Series Photographs , 
 price seven sixpence publish ; 
 subscriber shilling , payable delivery work . 
 immediate application request undersigned , 
 subscription list shortly close . JOHN TOWERS . 
 Alderley Edge , Manchester 

 ENOR ALLO.—Two gentleman require 

 London : Ashdown Parry . Hanover - square 

 publish . 
 ILLIAM HUTCHINS CALLCOTT new 
 HARMONIUM work.—secular sacred melody , 
 arrange Harmonium Chsmber Organ contain 
 advir d subject , W. 8S. Bennett , Beethoven . Bellini , Bishop , 
 Blumeithsl , W. H. Calicott . Dr. Calcot , Dr. Crotch , Miss Davis , 
 Himme ! , Handel , Horsley , Hulah . Knight , Mozart , Mend+lssohn , 
 Rossini , Schumann , Schubert . Shield , Verdi , Weber , english Mrlo- 
 die , scutch melody , german melody , irish melody , russian 
 melody 

 dwelling - place 
 sweet souna harmony . ”—WorpDsworta . 
 Vrice lUs , 6d , net 
 London : Lamborn Cock ( ' o , 62 63 . New Bond - street 

