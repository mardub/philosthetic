invariably write high 
 absolutely impracticable - - day . 
 require player rapid semiquaver passage 
 great difficulty , frequently write high 
 c , d trumpet . 
 doubt passage 
 possible . opportunity time 
 talk subject great 
 live authority matter , Mr. Thomas Harper , 
 inform , use suitable 
 mouth - piece passage undoubtedly 
 play practice , modern composer 
 write forthe instrument different fashion 
 performer play Bach passage 
 lose command lip necessary music 
 present day . , , doubtful 
 shall opportunity hear , 
 conceive , effect high trumpet 
 note , ring clear , 
 impart music peculiar brilliancy 
 

 " Kyrie " present Mass write 
 distinct movement , plan find adopt 
 exceptional case , , instance , Beethoven 
 Mass D , Cherubini second masse , 
 unusual length . move- 
 ment chorus ( treble , alto , 
 tenor , bass ) accompany string , flute , oboi 
 d’amore , bassoon , organ . bar in- 
 troduction , adagio , needless quote , 
 largo introduce long prelude - 
 bar orchestra , voice enter fugally , 
 subject tenor 

 ky - ri - e , e - le - 

 principal feature concert rst ult . 
 appearance russian pianist Rubinstein , 
 absence year cause , freely 
 state interval , coldness 
 receive visit England . scarcely 
 imagine reason complain 
 welcome present occasion , 
 applause greet completely silence 
 dissentient believe marvellous 
 power temper judgment 
 separate intellectual exponent art miracu- 
 lous worker wonder . regret Concerto 
 E flat , perform , late , 
 rhapsodical style exhibit work 
 deepen hope cure . movement 
 utterly interest , Finale common- 
 place theme attention solely centre 
 dazzlingly brilliant passage dress . 
 Andante melodious phrase ; , 
 contrast , possess musical value ; vocif- 
 erous mark approbation accord indiscriminately 
 Concerto , judge demeanour 
 listener , great 
 work art present . pass 
 composition composer , Herr 
 Rubinstein grasp instrument command tone 
 display advantageously rendering 
 Concerto time elapse 

 concert cool enthusi- 
 asm audience sufficiently appreciate 
 demonstrative beauty Beethoven Symphony , 
 ( . 7 ) , second commence . 
 remember time classical feeling rule 
 counsel Philharmonic Society 
 prevent introduction showy , pretentious , 
 shallow music Concerto ; modern Germany 
 invade country , bad ; 
 destined listen good , bad , indifferent 
 work invader bring , express 
 hope difficulty Herr Rubinstein recent 
 importation stupendous com- 
 poser dare grapple public 
 audience . prove style equally 
 easy , great pianist play , calm 
 artistic feeling , theme Haydn , variation ; 
 follow rendering Beethoven " ' Waldstein " 
 Sonata , , accord reading 
 modestly fancy understand true 
 meaning composer work , evidence 
 right accept great executive artist 
 day . vocalist concert Miss Catherine 
 Penna , sing grace refinement 
 expressive song Herr Rubinstein . programme 
 fourth concert , 15th ult . , include Bach 
 ' ' Suite ' ? b minor , Mendelssohn ' ' Scotch Sym- 
 phony , " solo pianist Herr Barth , create 
 marked effect performance Henselt Concerto 
 f minor 

 BACH MASS B minor 

 visit Herr Rubinstein country , 
 absence year , undoubtedly 
 event musical season . excitement 
 produce recollection performance 
 artist London , great pianist . 
 recital St. James Hall , 
 fifth announce place 2oth , 
 press . programme recital 
 worth , versatility Herr Rubin- 
 stein , complete command style playing 

 ReciraL , 3.—prelude Fugues , J. S. 
 Bach ; Rondo minor , Mozart ; Gigue major , 
 Handel ; sonata F minor , Op . 57 , Beethoven ; Kreis- 
 leriana , Schumann ; sonata b flat minor , Chopin ; 
 etude , Chopin ; miniature , Caprice , Barcarolle , 
 Valse Caprice , Rubinstein 

 Seconp REcITAL , 10.—variation , Handel ; Sonata 
 E , Op . 109 , Beethoven ; Etudes Symphoniques , Schu- 
 mann ; Momens Musicales , Schubert ; Scherzo Capriccio , 
 Mendelssohn ; Nocturne , Field ; Polacca , Weber ; Pre- 
 lude , Ballades Etudes , Chopin ; Leonore , 5th Bar- 
 carolle , Tarantelle , Rubinstein 

 RECITAL , 16.—Fantasia , Op . 15 , Schubert ; 
 Sonata ( moonlight ) , Beethoven ; Variations Sérieuses , 
 Mendelssohn ; Nocturnes Polonaises , Chopin ; Car- 
 naval , Schumann ; Suite , Romance , etude , Rubinstein 

 FourtH ReciTaL , 25.—prelude fugue , 
 Rubinstein ; sonata flat , Weber ; ' " * Warum , " " Vogel 
 als Prophet , " ' abend , " ' ' ' Traumeswirren , " Schu- 
 mann ; sonata c minor , Op . 111 , Beethoven ; Nocturne , 
 Field ; Etude , Thalberg ; ' ' Chanson d'Amour , " " Si 
 oiseau j’étais , " Henselt ; Nocturne , Mazurka , Valse , 
 Etudes , Chopin ; Barcarolle ' Erl - K6énig , " Schubert- 
 Liszt ; Rhapsodie Hongroise , Liszt 

 MUSICAL TIMES.—Jvnz 1 , 1876 . 501 

 Rubinstein small composition piano , 
 , , introduce 
 recital , general interesting enjoy- 
 able large work . man consider- 
 able inventive power , indisputable ; failing 
 handling elaborate musical form . 
 want technical mastery apparent piece 
 small dimension , trifle ' * Minia- 
 ture , " ' ' caprice , " & c. , thoroughly successful 

 addition recital , Herr Rubinstein 
 hear Concert M. Wieniawski St. 
 James Hall 2oth ult . performance 
 occasion great artist Beethoven cele- 
 brate ' " Kreutzer ' sonata magnificent 
 recollection , alike vigour passion 
 Allegros , exquisite taste delicacy 
 - know variation . Herr Rubinstein 
 work hear good , admirably 
 second M. Wieniawski , appear play 
 finely . great pianist 
 selection short piece , 
 previously hear recital . Concert include 
 Haydn quartett E , ( op . 77 , . 2 ) , solo 
 excellently play Concert - giver , vocal music 
 Mdlle . Thekla Friedlander 

 continental engagement Herr Rubinstein compel 
 leave country early present month . 
 hope extraordinary success 
 meet induce soon repeat visit 

 Masses Hummel resemble general 
 characteristic later church composition Haydn 

 Mozart . untrue 
 work high inspiration ; unfair depre- 
 ciate , writer , describe 
 mere elegant scholastic exercise . Hummel , like 
 Beethoven Schubert Masses , touch feeling 
 deeply , sacred music dignified refined , 
 abound pleasing , strikingly original 
 melody , free occasional secular tinge 
 place mar effect fine Mozart 
 Haydn Masses . Mass ( b flat , op . 77 ) 
 second ( e flat , op . 80 ) , long 
 know , hear catholic church 
 country . unaccountable thing work 
 , score print 
 year , publish vocal 
 score , country continent . 
 neglect surprising Mass D 
 consider degree inferior predecessor 

 like Cherubini great Mass key , pre- 
 sent work commence " Kyrie ' ? D minor . 
 Hummel avoid error , common 
 countryman , write lively , nay , jovial , move- 
 ment , Mass. * ' Kyries " ' 
 Haydn second Masses , delightful 
 music , regard thoughtful 
 hearer utterly keeping 
 spirit text . present piece , contrary , 
 character wholly devotional . music 
 melodious , ( far judge indi- 
 cation scoring pianoforte accompani- 
 ment ) charmingly instrument . imitative passage 
 ' " ' Christe " bear slight resemblance 
 middle portion ' " Kyrie " Haydn " imperial " 
 Mass. return subject change D 
 minor D major happy effect . 
 movement reckon good portion 
 Mass 

 ALFRED ALLEN * " GAVOTTE . " 
 EDITOR MUSICAL TIMES 

 s1r,—in reviewer notice " Gavotte Festi- 
 vale " month " ' Musical Times , " : ' ' 
 Mr. Allen doubt chord ought write , let 
 refer Beethoven sonata ED ( Op . 31 , . 3 ) , 
 fifth . bar movement , 
 surprised , think , find FZ instead g 

 having refer bar question , find follow- 
 e progression 

 BENHILTON.—The concert Choral Society 
 2nd ult . , Macfarren Day perform great 
 success . gem evening C. E. Stephens Duo 
 Pianoforte , play member Society . 
 concert conductorship Mr. E. M. Lott 

 BirKENHEAD.—A musical recital Saturday , April 2 g , 
 Music Hall , Mr. J. C. Bond - Andrews , assist Mr. C. de 
 Wolfe - King , vocalist . programme contain selection 
 work Beethoven , Mozart , Schumann , Liszt , Bach , Chopin , 
 composition concert - giver receive . 
 Mr. A. E. Tozer accompany vocai music 

 BLECHINGLEY.—The New Organ recently erect Messrs. J. W. 
 Walker Sons Parish Church St. Mary , open 
 26th April Rev. Sir F. A. Gore Ouseley , Bart . , Mus . 
 Doc . , & c. service fully choral . response Tallis , 
 Proper Psalms , xlvii . , xcviii . , cl . , sing chant 
 Sir F. Ouseley , E. Lake , Humpbreys . Te Deum 
 Jubilate Dykes Service F , Monk . Sir F. Gore Ouseley 
 preach sermon , play varied selection music 
 new organ . service accompany Mr. Geo . Ernest Lake , 
 organist director choir , play original pre- 
 lude Processional Hymn , Recessional Mendelssohn 
 Athalie March . organ gift Sir George MacCleay 

 Haydn ) . instrumentalist Messrs. H. Bruton , Bartlett , 
 illiams ( violin ) , Pollock ( harp ) , J. A. Matthews . Mr. 
 Matthews conduct usual 

 Crirron.—On Thursday , 4th ult . , Mr. Douglas Pomeroy 
 second Pianoforte Recital large room Regent - street . 
 rogramme include selection Beethoven , Bach , Handel 

 eller , Chopin , conclude performer composi- 
 tion , ' ' Allemande , D minor , " ' Grand Valse , " . 2 , " Elf 
 dance . " piece encore , concert 
 complete success . crowded audience.——On Friday 
 evening , 12th ult . , Mr. James C. Daniel bring winter enter- 
 tainment close present season Pianoforte Recital 
 Mdile . Marie Krebs . programme contain selection 
 work Beethoven , Bach , Handel , Mendelssohn , Sir Sterndale 
 | ea Schumann , & c. ' performance great satis- 
 action 

 CockERMOUTH.—The member Harmonic Society 
 sixteenth concert 2nd ult . , new Public Hall , Station 
 Street , large audience , occasion opening 
 hall . devote song , duet , - song , & c. , 
 second Sir S. Bennett Cantata , Queen . solo 
 ably fill local amateur , Miss A. Robinson 
 " Queen , " Miss S. Frostick ' Queen , " Mr. Wilkinson 
 * Lover , " Mr. C. J. Lewthwaite " Robin Hood . " Mr. P. T. Free- 
 man conduct , Miss Cooper preside piano.——On 
 16th ult . tea Concert hold practice - room , Station 
 Road , - select programme vocal instrumental 
 music provide . vice - president , Rev. Canon Hoskins , 
 address , review history Society 
 commencement , conclude present , behalf Society , 
 handsome testimonial Miss Cooper valuable service 
 piano . Society form year ago , mainly 
 effort treasurer , Mr. C. J. Lewthwaite , 
 educate musical taste town neighbourhood 

 STRATFORD.—The West Ham Philharmonic Society conclude 
 eighth season , Tuesday evening , 16th ult . , performance 
 Cowen Cantata , Rose Maiden , Town Hall , 
 conductorship Mr. Oe S. Bates . principal vocalist , 
 Miss Annie Sinclair , Miss Marion Severn , Mr. A. Kenningham , 
 Mr. J. A. Latta . chorus Cantata , - song 
 second programme exceedingly sing ; Mr. 
 Fred . C. Kitson accompany . rehearsal Society , 
 previous Concert , Mr. J. S. Bates present 
 member ivory bdton , handsomely mount silver , 
 elegant cruet - stand , mark respect esteem 

 Sursiton.—Mr . R. S. Hart annual Concert place 16th 
 ult . Christ Church Schools , assist Miss Mary 
 Davies , Miss Dones , Mr. Stedman , M. Victor Buziau , Herr Ruders- 
 dorff , Mr. H. F. Frost . Miss Davies singing 
 Rubinstein " ' thou art like unto flower ; " join Mr. 
 Stedman duet ' maye , " success 
 Concert . Miss Dones admirable contralto voice hear advan- 
 tage song , Mr. Stedman recall singing 
 " Love request . " instrumental portion programme , 
 include Beethoven trio , Op . 1 , . 2 , & c. , justice 
 Messrs. Buziau , Rudersdorff , Hart , play usual 
 masterly manner , omit recognize good service 
 render Mr. Frost accompanist Mr. Hart support 
 élite neighbourhood 

 UTTOXETER , STAFFORDSHIRE.—The parish church 
 fit new set chiming machinery , design manufacture 
 Mr. Smith , Midland Clock Works , Derby . seven appropriate tune 
 play day week 

 West Bromwicu.—Miss Emma Bailey excellent Concert 
 Town Hall rst ult . , assist Madame Thaddeus 
 Wells , Mr. Henry Nicholson , Signor Ronzi , Miss Barbara Bailey . 
 Miss Emma Bailey highly successful Ronzi Romanza , " ' non 
 m’ami picé ! " ( accompany composer ) , Barnby " 
 tide come . " Madame Thaddeus Wells artistic 
 rendering Sullivan " let dream , " old english 
 - pole song , ' ' come , lass lad 

 WEYBRIDGE.—A successful Concert Mr. H. P. 
 G. Brooke , organist director choir St. James Church , 
 Thursday evening , April 27 , mansion Rev. T. Spyers , 
 D.D. , distinguished patronage . excellent programme 
 provide , - song render . " dream " 
 ( Cowen ) , effectively sing Madame Ashton . Mr. Henry Ash- 
 ton , Mr. C. L. Morgan , Mr. R. Burns , Rev. W. Money 
 contribute song duet . Miss Alice Wilks ( pupil Mr. H. P. G. 
 Brooke ) play violin solo , accompany Mrs. Wilks , taste 
 expression . Mr. Walter Fitton , R.A.M. ( Silver Medallist , Potter 
 Exhibitioner Royal Academy Music , & c. ) , play Beethoven 
 " sonata flat , " Op . 110 , piece Schumann , W. Macfarren , 
 Weber , brilliant manner . Mr. Brooke conduct , 
 accompany pianoforte usual skill ability 

 WorcresTER.—The Philharmonic Society performance 
 Elijah , Music Hall , 23rd ult . vocalist Miss 
 Jessie Jones , Miss Bertha Griffiths , Mr. T. A. Smith , Mr. Brandon , 
 respective solo success . band 
 chorus efficient . Mr. conduct 

