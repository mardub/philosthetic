Aud Singing Class Circular . 
 AUGUST 1 , 1868 

 life BEE 
 R. M. HAYLEY . 
 ( continue p. 442 . ) 
 BEETHOVEN acquaintance Ferdinand Rics 
 commence 1800 , thirty , 
 height reputation . Beethoven 
 acquaint father Franz Ries , 
 violinist Elector Chapel Bonn . 
 young man , father 

 incident ITHOVEN 

 send Vienna profit instruction | 
 wait } 
 8 letter iatroduction , |1 

 celebrated townsman . 
 Beethoven present hi 
 find employ Oratorio 
 Mount Olives , produce . Beet- 
 hoven having look letter emarke , ' 
 moment write father , tell 
 forget mother death . 
 auswer suffice present . " 
 learn — father , Bect 
 mother die , render assistance family , 
 distr ant circumstance . ITTis subse — , 
 conduct Ries kind itiendly ex- 
 treme ; spite unconquerable repugnance 
 teachis g , devote zealously 
 young friend education ; , re- 
 markable , notwithstanding hasty impetuous ) 
 temper , Ries lesson 
 inildness patience 

 Ries interesting account production 
 Beethoven Oratorio . day 
 performance , Beethoven send ir 
 morning . find sit 

 ask want , receive 

 sheet composer 
 write , use 

 concert . ' rehearsal begin o'clock , 
 include , Oratorio , 
 composition Beethoven . tedious 
 affair , half - past , musician 
 exhaust , begin exhibit _ — d 
 faction . spirit , , revive 
 abundant supply fr : shment , ad 
 provide Prince Lichnowsky , attend 
 rekearzal commencement , t 
 anothe attempt th roug + h Oratorio . 
 " ft thing kind Beethoven 
 , " t Prince , " ' perform 
 d mazner worthy . " length , | 
 o'elock , ¢ concert commence , fame 
 Beethoven thenceforward establish 

 se combine produce 

 fort ] iness heart , sad draw- 
 happiness prec carious state 

 health , saring , even- 
 tually end subject 
 write letter eel : — ' ' envi- 
 ous demon ill - health play ugly trick . 
 hearing bad bad , 
 year . . . . . . 
 end Heaven know . continue , 
 come spring . 
 house pretty place country , 
 haif year peasant . 
 cure . resignation ! resource ! 
 remain . " difficulty hearing , 
 Beethoven ae ty way connect 
 - comp ! laint year 
 labour ; _ refore place 

 good medical treatment , view reinstate 

 bodily health ; , dismay , find 
 improve whilst state hearing re- 
 maine . calamity , 
 , feel ; luter letter , 
 affliction attain height , cease 
 allude 

 Beethoven long Vienna find 
 active friend patron Prince Lichnowsky , 
 mention . nobleman receive 
 house , continue reside till 
 year 1799 . Prince great patron art , 
 excellent judge music . play piano , 
 diligently study work Beethoven , per- 
 form great ability . endeavour 
 convince young artist , attention 
 frequently direct difficulty 
 composition , style 
 require amendment . Friday 
 Prince matinée musicale residence . be- 
 sides musician engage 
 occasion , Beethoven usually present , gladly 
 hear criticism work . 
 great composition bring 
 notice , person opportunity 
 hear play . occa- 
 sion Haydn hear perform 
 sonata , Beethoven dedicate . 
 rapidity Beethoven play sight 
 difficult music theme universal 
 astonishment , remark 
 play presto passage 
 , quickly 
 possibly read successive note , re- 
 port reply " neces- 
 sary . read quickly 
 thousand mistake printing ; 
 notice know language 

 great progress Beethoven 
 advantage sound 
 instruction Albrechtsberger , subsequently 
 great master Haydn , year 
 lay foundation fame , musician , 
 series production reflect equal honour 
 teacher scholar . death 
 1801 , Elector Cologne , kind assist- 
 ance Beethoven greatly indebted suc- 
 cess , found definitively settle Vienna , 
 city feel partiality , 
 native city present ad- 
 vantage , affection friend scene 
 boyhood prompt 
 place abode preference . anxiety 
 mean subsistence end , 
 popularity composer great 
 ready sale work , receive 
 demand . lack 
 power express idea burn 
 , soul music catch in- 
 spiration amidst beautiful scene nature , 
 charm love , compose 
 great ease . open air beneath 
 canopy heaven , amidst fire 
 ardent imagination , conceive thrilling 
 melody celebrated . 
 idea commit write spot , 
 develop extend , ac- 
 count freshness propriety 
 music distinguish . consummate art 
 overcome difficulty , playing , 
 time remarkable delicacy , 

 brilliant . possess uncommon power 
 vary subject carry extent 
 completeness . respect , Mozart , 
 unequalled modern player 

 imagine Beethoven , pos- 
 sesse ample income gain 
 profession , trouble b 
 pecuniary difficulty ; 
 case . bring straitened circum . 
 stance , dependent bounty 
 , know value money , 
 time economical . 
 case particularly residence Prince 
 Lichnowsky . ' dinner hour o'clock ; 
 appear table hour regard 
 Beethoven kind limitation freedom , 
 irksome restraint wayward 
 nature rebel . , consequently , 
 dine tavern , pay 
 exorbitant price bad fare 
 kind host provide 

 peculiar irritability temper 
 striking contrast overstraining liberality , 
 frequently involve awkward embarass- 
 ment . consequence , -first 
 ebullition anger , induce 
 listen reasonable explanation matter 
 dispute , apology disproportione 
 offence commit . 
 peculiarity arise cause . constantly 
 wrap musical idea , 
 totally ignorant business domestic economy , 
 know little pass 
 ; addition , deafness 
 suspicious , lead think 
 impose . violence temper 
 occasion lead unpleasant scene . 
 quarrel waiter tavern 
 coffee - house , denounce 
 thief pickpocket , ultimately 
 peace handsome douceur . place 
 know , people accustomed 
 eccentricity , way . let- 
 ter time despond- 
 e . occasionally , short time , regain 
 happy frame mind , momentary serenity 
 soon vanish look forward future , 
 consolation prospect . hope 
 attain perfect health faint 
 faint , perceive favourite 
 scheme realize . write 
 mood friend , , ' ' lead somewhat 
 pleasant life . scarcely conceive 
 desolate dreary life 
 year . infirmity haunt like spectre ; 
 flee man misanthrope , spite 
 , nature . change 
 work dear charming girl love , 
 [ love return . 
 past year live happy mo- 
 ment , time feel mar- 
 riage happy . 
 marry ; roam little 
 . ear , 
 long ago travel half world , 
 begin . great pleasure 
 work profession , exhibit result 
 world . believe 
 happy Bonn . 
 happy ? care annoy 

 face , feel miserable 

 remarkable contrast de- 
 spaire line business - like charac- 
 ter humour characterize letter 
 write Beethoven , early 
 residence Vienna . time creative 
 power maturity ; letter address 
 musician publisher Leipsig , 
 write : — ' ' right heartily thank 
 good opinion form pro- 
 duction , wish deserve . 
 likewise glad hear undertaking , 
 wish money profession 
 fall lot genuine artist 
 flow coffer mere shop - keeper . 
 intention publish work Sebastian Bach 
 heart good , beat unison 
 great noble work Patriarch music . 
 t hope , soon hear golden peace pro- 
 claim , able contribute greatly suc- 
 cess undertaking , seek subscri- 
 ber . " merry mood write 
 friend , reconciled 
 short estrangement , having lend 
 ready ear idle rumour detrimental 
 Beethoven character . ' ' mad , gentle- 
 man ? ' propose write sonata ! 
 fury revolution — 
 , world return old 
 routine , Buonaparte conclude con- 
 cordat Pope ! — sonata ! 
 messa tre voce , vesper , 
 instantly pen hand , write 
 credo . sonata 
 renascent christian time ! ty 

 gaiety , uafrequently , follow 
 great nervous irritability , chiefly en- 
 couraged return bodily suffering . 
 period complete 
 symphony ( sinfonia eroica ) whilst reside Neile- 
 jenstadt , village near Vienna . composing , 
 Beethoven endeavour concentrate 
 idea particular locality , object , 
 music definite aim . , notwith- 
 stand , habit ridicule 
 abuse term , ' musical painting . " 
 great master - piece , like Haydn Creation 
 season , escape censure 
 score ; dispose acknow- 
 ledge Haydn great merit , especially chorus , 
 Beethoven find word sufficiently 
 extol 

 composition Symphony 
 allude , object evoke imagina- 
 tion Napoleon Buonaparie , time 
 Consul . Beethoven admiration 
 great , accustom compare 
 consul antiquity time 
 Romans . score Symphony lie 
 table . title page , , inscribe 
 word , ' " Buonaparte , " , ' Louis 
 v. Beethoven . " intervene space entirely 
 blank , question friend 
 hiatus fill . 
 length bring intelligence Buona 

 tify ambition . place 
 high . tyrant . " 
 word Beethoven snatch title page 
 Symphony table , tear piece , 
 throw ground 

 Beethoven residence Vienna , Steibelt , 
 Imperial Chapel St. Petersburg , having 
 considerable stay Paris , arrive . 
 Beethoven friend alarm lest presence 
 celebrate composer , zenith 
 fame , detract reputation 
 protegé . interest excite 
 expect competition artist . 
 musical party Count Fries , 
 rival meet time . Beethoven 
 play trio B flat ( op . 11 . ) , 
 previously hear , 
 popular work . Stcibelt listen condescend- 
 ingly , pay author compliment 
 air man convince superiority . 
 play quintet composition , 
 extemporize , produce effect 
 tremolo chord , 
 novelty . Beethoven urge play , 
 refuse . day performer meet 
 house Count Fries . Steibelt , having , 
 applause , play quintet , com- 
 mence brilliant fantasia , 
 subject italian air , form theme 
 Beethoven variation - mention trio . 
 pointed challenge , provoke 
 Beethoven friend importune 
 gauntlet . irritate 
 deliberate act defiance , hold 
 long , pianoforte , seize 
 violoncello Steibelt quintet , 
 lie music - stand , turn ( designedly 
 accident ) upside . finger 
 strike note , 
 form subject , yield inspi- 
 ration moment , pour forth torrent 
 bright beautiful thought , Steibelt , con- 
 found overwhelm , leave room 
 finish . time avoid Beetho- 
 ven presence , accept invitation , 
 express condition rival 
 party 

 Beethoven , , exhibit kindli- 
 ness disposition ; time excessively 
 irritable , annoy 
 failure performance composition . 
 concert , produce 
 time fantasia pianoforte , orchestral 
 accompaniment , chorus , player 
 clarionet miscount bar ; solo 
 passage , blunder apparent . Beet- 
 hoven start upin rage , abuse band 
 vociferously hear 
 audience . ' , " length exclaim , 
 voice thunder , musician overawe 
 stern look commanding tone , submissively 
 obey . ' repeat , 
 perfect success ; soon concert 

 performer , indignant treat 

 declare play 

 parte create Emperor . hear } direction . anger , , short 
 Beethoven fall rage , exclaim , ' is| duration ; soon Beethoven finish new 

 common man — trample 

 MUSICAL TIMES.—Aveusr 1 , 1868 

 leadership . occasion similar 
 scene place , time orchestra 
 measure enraged , persist refuse 
 play guidance Beethoven ; , 
 desirous hear rehearsal piece , 
 compel present , oblige 
 remain ante - room . long time be- 
 fore quarrel . 
 capricious peevish nature Beethoven 
 character notice come 
 contact , source great trouble 
 friend . Albrechtsberger , study 
 counterpoint , Salieri , instruct com- 
 position , agree lament peculiarity 
 disposition , wayward pay little 
 attention subject study place ; 
 learn experience 
 arrive short read , 
 choose guide judgment . 
 singular act mistaken notion 
 rule fetter genius . Rossini 
 careless student , use annoy master , 
 venerable Padre Mattie , Beethoven 
 Albrechtsberger , trust harmonious effect 
 guidance perception 
 dicta theorist . read 
 score , observe infringement rule , 
 instead correct , mark 
 cross , write margin " soddisfazione de ’ 
 pedante . " ' ' satisfaction pedant ! " 
 fact theorist attach 
 importance arbitrary rule , beginner , 
 warmth imagination , disgust , 
 endeavour avoid restraint yield 
 authority , inward impulse 
 matter taste propriety . ' ' good 
 sound rule , " observe , 
 — ' ' susceptible 
 general application — yield im- 
 pulse high order genius ' snatch 
 grace reach art ; ' frequent 
 disregard law harmony , establish 
 teach school , find thé work 
 modern musician , great measure fault 
 law , long 
 applicable practice composition . can- 
 , conceive absurd 
 present system scholastic discipline , 
 young musician subject . genius 
 cramp confine long course pre- 
 cept , restriction , prohibition found 
 practice seventeenth century , tell 
 antiquated obsolete , 
 long occasion mind . " 
 wonder , , man exuberant 
 fancy Beethoven look distrust 
 study tend cramp fertile 
 imagination , forgive boldness 
 independence , exercise risk 
 sacrifice principle art . 
 ( 70 continue . ) 
 SACRED MUSIC . 
 Henry C , Luxy 

 juvenile day remember sit 
 window house lady , organ 
 street begin peal forth doleful 
 succession sound mortal 

 e fit " fireside " 
 feel religious word unite reli- 
 gious music choose , impress 
 sacredness trust 

 good , possible , widen 
 collection music occasional introduc- 
 tion composition , , " sacred " 
 composer , capable produce 
 true feeling devotion . agree 
 Mr. Macfarren ( ) as- 
 sertion , word music 
 originally set , intrude mind 
 note hear ; particularly 
 wish enforce fact instrumental composi- 
 tion character originally stamp 
 . true certain " social surrounding " 
 cling ; , abstractedly , 
 character composition determine 
 feeling express . slow movement 
 Beethoven Sonatas , Bach work , 
 Mendelssohn ' Lieder ohne worte , " 
 instance , truly religious ; proof 
 Gounod write " ' Ave Maria " Bach 
 prelude , C. admission work 
 tend materially elevate tone 
 Sunday evening music . opening remark 
 absurdity suppose sacred 
 absolutely dull , , sure , en- 
 dorse true religion heart ; 
 appeal . subject 
 deep interest ; , calmly re- 
 fect , truth obvious , 
 man necessarily religious 
 — £ 0 music necessarily religious 
 doleful 

 MAJESTY OPERA 

 PHILHARMONIC SOCIETY 

 Tue extra complimentary Concert sub- 
 scriber St James Hall , 17th ult . , prove 
 good performance season , spite 
 disappointment feel numerous audience 
 absence Madlle . Titiens , indisposition . 
 « Jupiter " Symphony Mozart , place pro- 
 gramme , play delicacy precision perfectly 
 delightful listen ; composition , 
 day musical mysticism , refreshing . | 
 Professor Bennett ' * Fantasia Overture " illustrative 
 Paradise Peri , genuine piece 
 pure writing spring school composition 
 die . theinstrumentation charming through- 
 ; Philharmonic Society , 1862 it| 
 expressly compose , sub-| 
 scriber public , frequent opportunity } 
 hear . Mendelssohn " Italian Symphony , " 
 excellently perform , attractive feature 
 prograrame ; Weber " Jubilee " Overture serve 
 play audience hall , musician 
 know deserving office . Mr. Charles 
 Hallé performance Beethoven pianoforte Concerto 
 G , , usual , faultlessly correct mechanism , 
 unmarked startling reading offend 
 fastidious listener , introduce Beethoven 
 cadence , master difficulty 
 utmost ease . consequence Madlle . Titiens ’ absence , 
 Madame Trebelli - Bettini Page song Les 
 Huguenots , " di tanti palpiti , ' " receive 
 applause . Madlle . Nilsson ( obviously 
 suffer indisposition ) sing Beethoven Scena , " ah 
 perfido , " wonderful effect , rouse audience per- 
 fect enthusiasm , prove fully capable 
 sustain high place Concert - room 
 occupy operatic stage . Mr. Santley 
 fine rendering Handel song trom La Resurrezzione , 
 " © O voi dell ’ Erebo , " ( speak occasion 
 performance late Handel Festival ) 
 sing equal effect air , " Sei vendicata , " 
 Dinorah . evening Mr. Cusins conduct 
 steadiness decision purpose 
 highly commend 

 gallery illustration 

 bon , carefully reproduce . surely chinese 
 blunder way inartistic reproduction old 
 work able architect , ought act warning 
 ' unfortunately general mistake . 
 consider endeavour simple easy , 
 composer forget artist , 
 bound throw feeling work 
 able , injure clearness simplicity 
 . , , entire range 
 Psalms Hymns praise equal Ze deum 
 laudamus loftiness thought , variety expression , 
 ' , mere vehicle musical exposition , 
 valuable ; , consider offering 
 Creator , vastness work increase 
 materially . view question hardly 
 estimate , commend thought thou . 
 sand - - tiro worry life musical 
 man standing , send small comgeninn 
 modestly hint " Sanetus , " 
 great heaven ! suppose Eternal Song , 
 sing " Angels Archangel , company 
 heaven , " ean adequately rej present arrangement 

 lof trash , inspire genius Beethoven fail 

 encompass . thought shoud sink 
 deeply heart churci - musician absolutely 
 necessary produce ed deserving 
 sacred music . consideration 
 influence production Z 
 Deums ascertain . 
 ( lo continue . ) 
 de L’Espérance . Melodie pour Piano . Par J. 
 Schiffmacher 

 protest principle pianoforte piece 

 harmonize choir . , although|<acred christen religious title . 
 view interpolation , fit excellently | « meditation " " Sabbath Eve , " Mr , f. 
 place Cantata ; , piece quiet part-| Parker , graceful little melody , fairly 
 writing , welcome . _ follow fine , but| jt , place unpretentious pianoforte piece 
 brief , chorus b minor , " believe , " ( remarkable for| day ; Sunday evening 
 excellent treatment word , especi-| Monday morning , Tuesday afternoon ; al 
 ally beautiful conclusion , voice drop| feel duty raise voice appeal 
 unison dominant key - note ) tenor| taste person shrink play 
 solo , " salvation nigh fear Him,”| heavenly slow movement Mozart Beethoven 
 placid nature excellent contrast | Sunday , contentedly linger Mr. Parker « Medi . 
 ' choral effect surround . chorus , " 1 ’ tation , " publish sacred magazine , 

 Lord , " precede reminiscence | entitle « Sabbath Eve . " admire 

 master choir , overcome difficulty ! singe hymn , " Christ corner stone , " ( Handel ) . 
 + difficult Church service . Bishop Heber’s| Preces ( Tallis ) , Venite G ( J. ' Curle ) , psalm , chant 

 prinity hymn , Dr. Dykes ’ Tune , " Nicoea , " } D ( J. Hopkins ) , Ze Deum C ( Woodward ) 
 yerse Keble Evening hymn , " old Com-|Turle , F ' ; Benedictus G ( W. Morgan ) ; anthem , 
 mandment ’ tune , " sing accurately , accord | ' * worthy Lamb " ( Handel ) ; hymm betore sermon , 
 musical mark expression print the| < Hark , sound Holy voice " ( J. Langran ) . evening 
 word . boy , time little unsteady the| service , Anthem , " arise , ye people , clap hand " 
 second treble Dr. Croft means easy|(Dr . W. Hayes ) ; prece Morning service : psalm , 
 Anthem , " cry aloud , shout , " recover ] Magnificat , nune dimittis , gregorian music ; Anthem , 
 immediately , point } hallelujah . Father " ( Beethoven ) . hymn 
 entrance fugue praiseworthy manner . the| ofiertory , ' ' holy , holy , holy , Lord God Almighty " ( John 
 decided step advance year ; we| Hopkins ) . hymn offertorv , " rejoice - day 
 hope earnest progress . prayers|one accord " ( Mendelssohn ) ; hymn , " sun soul " 
 intone Rev. J. A. Seaton , curate Hors-|(J. ’ . Bridge ) , ' sing Alleluia forth duteous praise ’' 
 forth ; sermon preach Rev. T. B.|(J. Barnby ) . request , morning anthem re- 
 Ferris , reetor Guiseley . Mir . Wm . Stables , organist of| peate . evening , excellent selection part- 
 Kirkstall church , choir - master Union , | music sing St. Mark School - Room . 
 exertion success festival great measure : oo 
 , conduct . Mr. Brown , organist Otley Parish } OrGan Apporntments , & c.—Mr . John C. 
 Church , preside organ . ' choir include | Ward , Organist Christ Church , Hampstead , Quebee 
 Union , agricultural village , number | Chapel —— Mr. H. G. Gilmore Portland Cathedral , 
 103 voice . Maine , U.S. — Mr. J. Smart , Organist choirmaster 
 sole sem 2 . . _ |to Parish Church , Newport , Salop.——Mr . J. F. 
 Oxrorp.—An exercise fo1 degree Mus . Goodban , a.1t . a.m. , Charch st John Evan- 
 Bae . , compose Mr. John Abram , New College , gelist , Paddington . — Mr. Alfred Cox , St. , Stephen 

 Paina ¢ Rinein Sir er Bears ss " apap 
 perform 4 _ os ore ag nae » the| Clapham Park.——Mr . Roger Manthorp , late organist 
 — gd ed woe ah _ ci r. — — , , Lhe St. Runwald , St Leonard , Colechester —— Vir T. J. 
 exercise entitle , " ' Jerusalem , City God , " } Chignell St. Runwald , Colchester —— Mr. William 

