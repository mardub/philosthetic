use prefer Pianos 

 BEETHOVEN HAYDN R. SCHUMANN 
 WEBER CHOPIN WAGNER 
 MENDELSSOHN — LISZT VON BULOW 
 DVORAK HELLER DE PACHMANN 
 CRAMER HENSELT HALLE 
 BENNETT Mpme . SCHUMANN SULLIVAN 

 price list illustration application , 
 33 , GREAT PULTENEY STREET ( near Regent Street ) , 
 LONDON , W 

 find operatic class Institution , 
 experienced direction Mr. 
 G.H.Betjemann . performance selection 

 " Water Carrier " Cherubini , " Faust " 
 Gounod , " Aida " Verdi , ' Fidelio " 
 Beethoven remarkable proof value 
 | teaching great interest work 
 | student intend devote 
 ( dramatic music . ' award bronze 
 |silver medal , exertion direction , 
 highly appreciate 

 Society Arts , pioneer ct 
 | musical examination , grant certificate 
 |to candidate moderate pretension , entere 
 far spirit encourage direct 
 effort , award obtain marl 
 practical examination bronze medal 
 } distinction special artistic value . 
 institution commendable 
 work , musical reward kind 
 lattainable work accord 
 capacity . Concerts 
 |given past year design 
 great measure vast educational move- 
 ment , ample reason believe 
 exertion direction 
 reward 

 premier Orchestral Society England , 
 Philharmonic Society , successful 
 season , artistically financially . guar- 
 antee fund draw , 
 reason belief body 
 initiate performance music high 
 class hold lead remain pattern | 
 follow 

 Richter Concerts attractive 
 excellence performance work 
 present . prominence 
 work Wagner Beethoven , new composi- 
 tion find attentive hearing . 
 satisfactory interesting novelty 

 Concerts , Finsbury Choral Association , 
 like body , good suit service jn 
 extend knowledge important musical work 
 locality 

 Hans von Bilow ... prodigious musician , 
 cherish religious admiration idol 
 ( Wagner ) . . . wholly slave creature . 
 surprising wife , contact 
 burn adoration , conceive passion 
 performance ' Tristan ' 1865 , 
 possessed ' mastersinger ' pro- 
 duce , run husband Lucerne . 
 precipitate flight thunderstroke 
 man artist ... artist prevail 
 man . ' permissible 
 kill , ' Biilow , heroic resignation , ' 
 strike . ' 
 master 
 heart life , bear hostility genius , 
 , later , grief abate , - marry , 
 exert success 
 wagnerian cause . " philosophical 
 comfortable circum- 
 stance 

 year 1870 publication 
 literary work Wagner — , ' * Ueber das Diri- 
 geren , " veritable polemic , raise storm 
 controversy ; , ' ' Beethoven , " valuable 
 contribution study subject , hardly 
 adapt popular reading . 1871 Wagner com- 
 pose ' ' Siegfried Idyll , " circumstance 
 known repetition . time , , 
 shape mind practical measure 
 building theatre devote 
 proper performance work , ' " Nibelungen " 
 especially , colossal production 
 fair way regard tour opera 
 great lyric drama section . year 
 previously — , 1867 — step 
 carry project , vastness 
 design prepare architect , Semper , 
 consequent expense , alarm King Ludwig 
 monarch , aware impossibility 
 obtain money Parliament 
 purpose , allow matter drop . 
 master resolve appeal 
 german nation , patriotism pride 
 secure end view properly excite . 
 1870 follow year event play right 
 hand . - union Germany 
 Emperor dazzle public vision , special 
 germanic genius Wagner , pre- 
 tension , national hero . hardly 

 surprising composer dream 

 new theatre build money 
 good deal , estimate sum 300,000 
 | thaler , , roughly , £ 45,000 . raise 
 issue share , confer certain privilege , 
 establishment Wagner Societies 
 Germany abroad , raise 
 subscription Concerts aid fund . 
 Wagner personal appeal , 
 direct Concerts fragment 
 ' ' Nibelungen " perform , delivery 
 address banquet , & c. purpose 
 way inflame zeal partisan , cer- 
 tainly succeed 

 stone new theatre lay 
 Wagner , - ninth birthday ( 22 , 
 1872 ) . great ceremony attend proceeding , 
 2,000 person assist . performance 
 ' " Kaisermarsch " Beethoven ninth 
 Symphony ( new version R. W. ) 
 old margravian theatre , King Ludwig 

 3 & 2 3 72 

 bear little thinking people accept fact mild surprise 
 " puff . " realisation | attempt account anomaly , 
 hope , reward struggle ; | scene — , 
 swear artistic creed , refuse | , small practical experience 
 sympathy reward strenuously fight | expense involve concert , hire 
 . Jullien liken royal proclamation address | hall , outlay advertisement programme , 
 dismiss Bayreuth gathering : & c.—the unsatisfactoriness present state 
 " Jt impossible shake hand affair inevitably chronic source mingle 
 personally leave member superb irritation pity . work , 
 assemblage artist , happy day , | concert , nineteen case , 

 come distant country , 
 group celebrate great Beethoven . 
 equally difficult convey , 
 writing , farewell wish . thank friend , vocal- 
 ist andinstrumentalist , , north south , 
 east west , Berlinto Vienna , Pesthto Mann- 
 heim , respond invitation noble 
 artistic solemnity . " sympathetic french bio- 
 grapher waggishly add : ' , write , 
 publish Bayreuth , 24th , 1872 . " 
 slily remind Wagner write : ' 
 nature destiny vow 
 concentration solitude work , regard 
 absolutely improper exterior enterprise . " 
 master know thing , apparently , . 
 , accord Dr. Puschmann , 
 day absolutely mad 

 dour belief 
 cious persecution . ' madness Bayreuth , 
 , good deal method 

 venture guess , production mean | 
 list work introduce England 
 Festival follow . list head " St. Paul , " 
 business , credit produce 
 Mendelssohn oratorio belong Liverpool , 
 perform 1836 

 procramme forthcoming Birmingham | 
 Festival , October 6 , 7 , 8 , 9:—Tuesday — morning , | 
 " Elijah " ; evening , Mackenzie ' Veni , Creator " ' ; | 
 Beethoven Violin Concerto ; Bennett " Naiades " | 
 overture ; Brahms Symphony , no.3 . Wednesday — | 
 morning , Bach St. Matthew " Passion " ; Evening , | 
 Stanford " Eden . " Thursday — morning , ' ' Messiah " ; | 
 Evening , Parry ' " ' bl pair " miscellaneous . | 
 Friday — morning , Dvorak * * requiem ’' ; Beethoven | 
 symphony , . 7 , & c. ; evening , Berlioz ' Faust 

 vocalist atthe Birmingham Festival : — Mesdames 
 Albani , Williams , Brereton , Macintyre , Wilson , Glenn ; 
 Messrs. Lloyd , McKay , Santley , Mills , Brereton , 
 Henschel . solo violin , Joachim ; organist , Perkins ; 
 chorusmaster , Stockley : conductor , Richter . chorus : 
 soprano , 100 ; alto , ; tenor , 90 ; bass , — 
 total , 370 . orchestra : string , 86 ; wind , 34 ; harp , 
 2 ; drum , 3 — total , 125 . grand total , 495 

 TeREFORD Festival , September 8 , 9 , 10 , ii . 
 programme : Tuesday — morning , " St. Paul " ; even- 
 ing , Stanford " Battle Baltic ' ? miscel- 
 laneous . Wednesday — morning , Mozart ' ' requiem " ; 
 Beethoven Symphony . 3 ; Edwards " praise 
 holy " ; prelude ' Parsifal " ’ ( ! ) ; Sullivan 
 " Te Deum " ; Evening , Stainer St. Mary Magda- 
 len ’’ ; " Lobgesang . " Thursday — morning , Lloyd 
 " song judgment , " Bach motet , Mackenzie 
 Benedictus ; Parry " De Profundis , " Spohr 
 " Calvary " ; Evening , " Elijah . " Friday — morning , 
 " Messiah " ; Evening , chamber concert . 
 work mention Edwards , Lloyd , 
 Parry new 

 VocaLists Hereford Festival : — Mesdames 

 RUBINSTEIN credit following remark : 
 " work " ' — i.c . , Wagner , Liszt , Ber- 
 lioz — * stand obstacle way progress 
 music . .. . let Wagner , . 
 composition Wagner place 
 decorative element ; music , properly , 
 second place ... . wish thing 

 state advent 
 composer . wish 
 time master world music 
 Beethoven , Gluck , Weber , Mendelssohn , 
 Schubert , Schumann . " Rubinstein need 
 despair . sign reaction , , 
 sense , need mean retrogression 

 general meeting Philharmonic Society , 
 13thult . , following gentleman choose 
 director ensue year : Messrs. F. Berger , 
 Cummings , Gardner , Goldschmidt , Franklin Taylor , 
 John Thomas , C. E. Stephens . vacancy 
 list member fill election Mr. 
 W. H. Brereton , female associate 
 appointment Miss Hilda Wilson Mrs. 
 Mudie - Bolingbroke . meeting new 
 director unanimously resolve invite Mr. 
 Joseph Bennett continue service annotator 
 programme season 

 Tue " Strad " violin recently figure 
 Glasgow law case , state , sell 
 foreign collector £ 500 . instrument 
 , remember , 
 rim violin belly , 
 portion undoubtedly work famous 
 italian maker . originally belong 
 , course , doubtful , foreign 
 buyer content Glasgow 
 purchaser repudiate bargain 

 QUEEN ANNE dead , , accord New 
 York paper , illustrious people , generally 
 understand pass away , alive 
 resident Empire City . London Musical 
 News occasion lately remark : ' 
 american contemporary seek inspiration 
 german set essay rule music New 
 York ? " answer come : ' ' german 
 set ' good , consider Bach , 
 Mozart , Beethoven , Wagner , Brahms 

 copy circular issue Mr. C. A. Harris , 
 canadian impresario , announce famous 
 solo boy , F. Williams : ' " special engagement ! — 
 permission Vicar Choral Westminster 
 Abbey — special engagement London Soprano 
 Solo Boy , Master Frederic Williams , " & c. 
 ingenious , assert imply connec- 
 tion Williams Abbey rest 
 unmeaning clause , " permission Vicar 
 Choral Westminster Abbey 

 PHILHARMONIC SOCIETY 

 June 27 , late notice July issue , 
 seventh final Concert Society season 
 place . afternoon performance , 
 attractive rival Opera Concert Albert Hall ; 
 large attendance , wind - pass 
 successfully warm friend Institution 
 desire . programme general 
 particular interest , mean 
 work artist virtual monopoly attention . 
 familiar , example , orchestral selection , 
 begin Grieg Overture , " Autumn , ' 
 end Beethoven Symphony F ( . 8) , capital 
 performance Mr. Cowen 
 direction . let baton 
 Mr. Cowen hand entire Concert . 
 recognise advantage . policy 
 director secure presence service , chef 
 d’orchestre , composer recommend ; 
 question fact change 
 conductor Concert , executive point 
 view , drawback . recognise act 
 Richter Concerts , director Philhar- 
 monic matter consideration 
 deserve . Concertos programme 
 speak-—the Violin Concerto Beethoven , 
 play Mr. Ondricek , Chopin E minor 
 pianoforte , Madame de Pachmann soloist . 
 perform fashion mark distinction 
 individuality , - particular . 
 understand , , artist concern indulge 

 id celia eas 

 RICHTER CONCERTS 

 ar seventh Concert season , Monday | 
 evening , 6th ult . , St. James Hall , Wagner | 
 represent particularly interesting scene 
 second Act " * Gétterdimmerung , " Hagen 
 noisily summon vassal welcome Gunther | 
 bride , imposing conclude passage " die 
 Meistersinger . " " , Mr. Max Heinrich | 
 platform possible music 
 prominent character , appro- 
 priately dignified interpretation strain | 
 estimable cobbler , Hans Sachs . ' choral | 
 firmly sing , ready attention Dr. Richter wish , | 
 wanting execution vigorous | 
 orchestration . , , ' ' metal 
 attractive ' Cherubini overture ' ' Medea " 
 peerless C minor Symphony Beethoven . operatic | 
 Prelude hear advantageous cir- 
 cumstance evoke regret noble | 
 composition belong | 
 sufficient demand revival remunerative , 
 dramatic singer capable play heroine 
 indicate . Dr. Richter reading Symphony 
 happily familiar detail necessary . 
 point usually emphasize bring 
 tell effect , impression | 
 listener unmistakable . scena " Gli | 
 angui d’inferno , " " Il Flauto Magico , " air ' " o 
 grant dust fall , " Dvorak Oratorio * St. 
 Ludmila , " introduce London concert - room Miss 
 Clementine de Vere , american vocalist extensive 
 compass claim musical favour . 
 natural taste cultivated expressiveness , convincing 
 proof afford débutante modern 
 selection , reception obtain large 
 audience accord merit performance 

 attractive feature programme 13th ult . 
 Suite arrange Grieg number 
 compose Ibsen drama * Peer Gynt . ' 
 year work great favourite metro- 
 politan concert - room , previously 
 adopt Dr. Richter , curiosity manifest 
 respect treatment receive . piquancy 
 requisite Anitra dance , exciting 
 movement label ' hall mountain King . " 
 pace - trifle fast 
 customary country , slight 
 extent militate effectiveness . result 
 performance , , satisfactory 
 audience ask repetition stirring finale ; 
 conductor decline accede . Wagner 
 excerpt beautiful ' Charfreitagszauber , " 
 ' Parsifal , " ' composition admir- 
 able balance band understanding 
 player chief particularly observable . 
 worth come distance hear music elevated 
 style deliver unwavering smoothness 
 delicacy . Symphony Schumann e flat ( 
 " rhenish " ' ) , acceptable , 
 play clear reflection spirit 
 composer . orchestral piece Deethoven 
 " Coriolan ’' Overture . composer aria , ' ah , 
 Perfido , " soprano laudably anxious 
 master , dramatic energy Madame 
 Katherine van Arnhem 

 beat . composer discard strident 
 } . 7 . . . . 
 tone , , joyful strain mark victory , end ina 

 dignified elevated style accord poet 
 apostrophe fall brave . national ring combine 
 singular picturesqueness orchestration pervade 
 . comparative brevity composition 
 preclude Professor Stanford exhibit high quali- 
 tie art divers place , , choral portion 
 generally straightforward grateful nature , 
 little doubt winter shall frequently 
 hear ' ' Battle Baltic . " success 
 baton Dr. Richter , able secure fair , 
 altogether blameless , performance , decide , 
 composer warmly congratulate close . 
 | ballad September Hereford meeting 
 | Choirs , Professor Stanford conduct . 
 | programme 2oth ult . far 
 | overture " ' Euryanthe , " Lohengrin declaration 
 mission ( effectively sing Mr. Barton McGuckin ) , 
 | Wagner " Kaisermarsch , " Beethoven colossal 
 | Ninth Symphony . vocal quartet final move- 
 j|ment consist Miss Alice Esty , Miss 
 | Damian , Mr. Barton McGuckin , Mr. Watkin Mills , , 
 course , Richter Chorus . instrumentally 
 | execution masterpiece — specially Scherzo 
 jand Adagio — sustain prestige Concerts . 
 | enthusiastic leave - taking await Dr. Richter 

 ROYAL COLLEGE MUSIC 

 Tue Concert Chamber Music student 
 2nd ult . , chief feature , Schumann Quintet , 
 pianoforte play accuracy spirit 
 Miss Edith Green , string receive 
 fair interpretation Misses Jessie Grimson , Beatrice 
 Chattock , Maud Fletcher , Mr. Leonard Fowles . 
 performance Brahms fine little know Sona 
 E minor , pianoforte violoncello , Misses Mary 
 Cracroft Maud Fletcher , correct refined , 
 somewhat timid . movement suite C minor , 
 Rheinberger , organ , violin , violoncello , 
 play Misses Agnes Dobree , I. Donkersley , 
 M. Fletcher ; Miss Maud Branwell play short piece 
 Liszt Rubinstein considerable fluency . Thome 
 song " Les Perles d’Or " exhibit advantage bright 
 pure voice Miss Annie Lawson ; Mr. Stanley Cooke 
 voice , , hardly powerful Handel 
 ‘ Honour arm 

 follow Concert , 16th ult . , orchestral , 
 justify praise 
 occasion bestow performance . admirable 
 quality string utmost advantage 
 vigorous Beethoven Eighth Symphony 

 474 

 PIANOFORTE recital 

 Tue Handel Festival deter pianist 
 recital week , Thursday , June 
 25 , , Mr. Schénberger , St. James 
 Hall , second Mr. Dawson series Stein- 
 way Hall . german executant begin badly 
 Tausig derangement Bach Organ Prelude Fugue 
 D minor , programme 
 worthy alike selection interpretation . special 
 praise Mr. Schénberger masterly performance 
 Weber neglected beautiful Sonata flat , 
 Beethoven thirty - Variations C minor , Mozart 
 Rondo minor , play welcome refine- 
 ment grace , Schumann difficult Toccata 
 C. encore demand - piece 
 pianist respond Chopin Etude C minor 
 Op . 10 ) , . 12 

 Mr. Frederick Dawson programme include 
 Weber rarely hear sonata — , C ( Op . 24 ) , 
 conclude celebrated ' ' Perpetuum Mobile , " 
 Schumann ' " Carnaval , " Sterndale Bennett 
 sketch , ' Lake , " ' ' Millstream , ' ' " Fountain . " 
 play manner tend 
 confirm good opinion previously form young 
 english pianist 

 St. James Hall fill Mr. Paderewski | play , finale — strange bizarre presto 

 Recital June 30 , polish pianist excellent 
 form , good effort Beethoven ' Sonata 
 appassionata , " rarely hear 
 brilliant intellectual expressive interpre- 
 tation . performance Schumann ' " Carnaval " 
 open question , time exaggerated 
 fantastic , important section , 
 ' « * promenade , " , unexplained reason , omit . 
 Chopin Nocturne b major polonaise fiat 
 course beautifully play , little 
 praise reading Schubert minor piece 

 great thing whisper concern Mr. Sto- 
 jowski , polish executant , 
 appearance London following afternoon 
 Princes ’ Hall . win " Prix de Rome " Paris 
 Conservatoire , - year age . 
 scarcely expectation fully realise , 
 present Mr. Sto- 
 jowski teach - rate technical 
 capacity . , , require 
 work Schumann Etudes Symphoniques , nearly 
 pianist attempt , interpret satisfac- 
 torily . young performer commendable 
 Beethoven sonata F ( Op . 54 ) , rarely hear work , 
 require little nimble finger . reading 
 Chopin piece lack distinction , Mr. Stojowski 
 favourable impression trifle 
 pen , , pretty Serenade , encore 

 4th ult . annual performance pupil 
 Mr. Oscar Beringer Academy Higher Development 
 Pianoforte Playing place Marlborough Rooms . 
 excellent manipulation display , young 
 player testify soundness Mr. Beringer system 
 instruction . iti respect invidious parti- 
 cularise occasion ; impel 
 attention exceptional promise display Miss 
 Sybil Palliser , movement Chopin Concerto 
 E minor , orchestral accompaniment 
 play second pianoforte Mr. Beringer 

 fairly large audience attend Madame de Pachmann 
 Recital season 7th ult . young 
 english pianist greatly benefit tuition 
 gift husband unquestionable , 
 scarcely ease Beethoven sonata flat , 
 Funeral March ( Op . 26 ) , display crisp , pearly 
 touch refinement style Chopin Ballade 

 G minor Etudes ; hear 

 Princes ’ Hall , Mr. Michael Esposito , italian artist 

 resident Dublin , believe highly esteem 
 performer teacher . technique per- 
 fectly sound , 
 interest execution Beethoven sonata C minor 
 ( Op . 11 ) , Schumann inevitable Etudes Symphoniques . 
 note correctly play spirit music 
 wanting . Sonata G , pianoforte violin , 
 pianist pen , prove pleasing com- 
 poser . construction work remarkable 
 elaboration , theme elegant purely italian 
 manner , movement symmetrical 
 form 

 Recital season come brilliant termination 
 11th ult . , Mr. Paderewski performance con- 
 siste entirely Chopin music . announcement 
 effect draw audience 
 witness St. James Hall Pianoforte Recital 
 Rubinstein pay visit 1886 . 
 remarkable size assemblage _ 
 abound enthusiasm . mere succés d’estime . 
 fellow countryman Chopin prove 
 thoroughly master spirit music , 
 present playing doubtless come light 
 arevelation . noteworthy performance 
 Sonata b flat minor . sentimentality 
 Marche Funtbre exaggerated , 
 movement Scherzo magnificently 

 special service hold St. Agnes ’ , Kennington 
 Park , Sundays , 12th ioth ult . , celebrate 
 eighteenth anniversary foundation church 
 parish . Mozart Communion Service B flat ( . 7 ) 
 sing Sunday , 12th ult . , orchestral 
 accompaniment , solo Mrs. H. Tate 

 Messrs. Jones , J. Wint , Plant . Mr. W. W. 
 Hedgcock organ . offertory Beethoven 
 ' " ' hallelujah " " ' Mount Olives " ' excellently 
 sing choir , anthem ' Thee , Chery- 
 bim , " Handel Dettingen Te Deum . Com . 
 munion Service sing 1gth ult . Weber G , 
 orchestral accompaniment . Spohr * 
 lovely thy dwelling " ? sing offertory , 
 Gounod " Marche Militaire ' ? voluntary . 
 Anthem evensong ' Lord God Heaven 
 Earth , " Spohr " judgment 

 tue gentleman St. Paul Cathedral choir 
 excellent Concert Portman Rooms June 30 . 
 programme admirably arrange , include number 
 specimen english glee , ' Ossian hymn 
 Sun , " Goss ; ' " strike lyre , " Cooke ; 
 noble chorus music " antigone , " Mendelssohn , 
 " Fair Semele high - bear Son , " , like con- 
 certe vocal piece , magnificently sing . addition 
 St. Paul Choir , Madame Clara Samuell , Miss 
 Hilda Wilson , Mr. Barton McGuckin , Mr. Watkin 
 Mills lend valuable aid ; Miss Kate Chaplin solo 
 violinist , Mr. A. Izard solo pianist . Mr. Hodge 
 assist accompanist , Mr. F. Walker conduct , 
 hope gentleman choir , scem 
 possess good tradition art glee singing , 
 think advisable Concerts like character 
 season 

 tue charming instructive Vocal Recitals Mr. 
 Mrs. Henschel maintain attractiveness , extra 
 performance St. James Hall , 3rd ult . , 
 attend large audience . seriatim 
 lengthy programme occupy large space , 
 unnecessary , artistic ' ' Ehepaar , " ' 
 term Germany , render faultless 
 manner set . attention , 
 , draw delightful piece english 
 composer : ' * Song , " A. Hervey , ' O sun , 
 waken’st , " F. Corder , Massenet singularly 
 piquant " * Serenade de Zanetto , " ' exquisitely 
 sing Mrs. Henschel . composer place 
 contribution Handel , Mozart , Beethoven , Mendels- 
 sohn , Schubert , Brahms , Loewe , Paderewski , Auber , 
 Schumann , Rubinstein , Boieldieu , Mr. Henschel — 
 goodly list 

 new organ build Messrs. Ginns 
 Bros. , Merton , Balham Congregational Church , 
 open 23rd ult . , concert consist 
 vocal instrumental music . instrument 
 ably play Mr. A. J. Crabb , Organist 
 Church , programme commence Te 
 Deum compose . ' choir sing " Judge 
 , o God , " ' * hear prayer ’' ( Mendelssohn ) , 
 solo Miss Annie Swinfen , 
 sing Handel " let bright seraphim . " 
 overture Handel ' ' occasional " oratorio 
 organ solo perform Mr. Crabb , 
 ' * hallelujah ' Chorus ( Beethoven ) sing 
 choir , Concert bring conclusion 
 hymn ' let man praise Lord . " Mr. J. Holland 
 Rose conduct concerted piece 

 enjoyable Vocal Recital Miss Liza 
 Lehmann Princes ’ Hall Friday , roth ult . , , 
 notwithstanding excitement cause german 
 Emperor visit City , fashionably 
 attend . justly favourite soprano singer hear 
 song Gluck — Glick print programme — 
 Giovannini , Gounod , Bishop , Boyce , 
 new duet , " freedom love , " pen , 
 tastefully write song . charac- 
 teristic energetic old hungarian melody , arrange 
 I. Corbay , sing splendid declamatory emphasis 
 Mr. Plunket Greene , tone 
 distinctly tragic , warmly receive . Mr. Von 
 zur Mihlen contribute Sicgmund ' Liebeslied " ' 
 " Die Walkire , " lyric 

 annually Milan Conservatorio purpose 
 study pianoforte 

 annual concour different class 
 Brussels Conservatoire place month , 
 presidency M. Gevaert , unusual number 
 candidate honour occasion . piano- 
 forte class M. Gurickx , worthy pupil successor 
 Conservatoire late Auguste Dupont , 
 prize , avec distinction , gain Miss Bles , play 
 ( obligatory ) movement Concerto 
 Hummel , fantasia minor Beethoven ; 
 young sister obtain prize , avec ja 
 plus grande distinction , class M. E. Colyns , 
 interpretation introduction Andante fourth 
 Violin Concerto Vieuxtemps elicit enthusias ic 
 plaudit audience . belgian press , add , 
 unanimous praise performance 

 Sefior Jimenez . author enthusiastically 

 content 

 BOOK xxxvii . book xxxix . book xli . 
 Adagio . Fantasia ' 54th Quartet ) . u : Haydn | Aria ee ua Spohr 
 ale , " Zauberflote " ' Moz art | Chorus- lw le ome p ae Spohr 
 dante . ' Cleme nza di T ito " hony .. Avo 1 pleasing snare ’ Pe Mozart 
 " wd rais se Thine . " hymn " Spoh ir M. Hau ; omé .. H. Reber 
 " lovely Thy iar fair , " ; , op . rrr Hummel Hummel 
 84th Psalm ° c Spohr | Anc ap ho ny Hummel 
 March . Spohr ay F. Schubert Ar 
 Adagio non T roppo . Qu tet , Ac . J. B. Van Bree Andreas Romberg 
 op . 44 , . ge delssohn | ky ie Andante . sonata , op . 147 .. F. Schubert 
 dag ist Quz artet , Op . . Kalliwoda -nnelle .. es .. G. A. Naumann Al tee + 
 -S. Wesley op.1r .. Carl Geissler | Larghetto . sonatina . op . 28 
 con certo , " Op . 89 .. Hummel BS oP .. J. Mendel lor Troppo . II . 
 theme original . op . 35   .. Schubert op . 1 Weber Sonat : 
 Adagio quasi Andante . | es 7 .. Kuhlau | Andante . quartet , op . 45 , no.2 .. 
 Ist Quintet , op . 4 Beethoven | book XLII 

 BOOK XXXVIII . 
 Adagio . sonata , op . ¢ 
 moderato dolce . sacred 
 Cantata = 
 Larghetto . t , o ; 150 
 " O Saving Vi ictim ' 
 Solo Chorus 

 A. Fesca 

 atina , Op . 12 , 
 .M P Beethoven 
 ‘ ae F. Kuhl reia pi = . t. Adams 
 S ig cach ; , . 1 ye Ve lumes , cloth , 5 . . 
 . 37 42 , Volume , cloth , 5 

 LONDON NE EW YORK : NOV ELLO , EWER CO 

