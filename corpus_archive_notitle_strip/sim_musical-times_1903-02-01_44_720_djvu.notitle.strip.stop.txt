wrong 
 common view styles periods 
 originated single individuals , 
 reckoned certain year . course , 
 discovery . authors writing 
 histories reigns centuries , 
 form rational divisions , critics 
 attributing individual , time , 
 belongs groups men generations . 
 unwarrantably attri- 
 buted Palestrina , Handel , Wagner ! 
 cases masters 
 remarkable , eminence 
 based novelty 
 materials devices . , un- 
 exceptionable theory regards formation 
 styles means secures satisfactory practice 
 historical treatment . 
 reminded author , pointing 

 dates beginning new style 
 middle nineteenth century . 
 undoubtedly 1850 Wagner produced 
 advanced works — ‘ Ring 
 Nibelung , ’ ‘ Tristan Isolde , ’ ‘ 
 Meistersinger , ‘ Parsifal ’ — Liszt 
 symphonic poems , symphonies , oratorios 
 masses ; 1850 , Berlioz , Chopin , 
 Schumann , time 
 time , work , 
 factors evolution 
 fairness reason ignored . 
 composers named , consider 
 Beethoven , Weber , Schubert , Spohr , 
 Meyerbeer , 
 way new 

 remains preliminary 
 remark — : invention new 
 technical means arises impulses — 
 need expression desire novelty . 
 need expression produces necessarily 
 substantial lastingly valuable results . 
 results desire novelty , 
 considerable extent trifling 
 transient , supply materials serviceable 
 nobler purposes art . , 
 artificer invents purpose , 
 poet - musician utilises , merely 
 piquant trait born play - impulse 
 significant vocable language . 
 passing strange nowadays , art 
 music advanced expressional stage , 
 meet people view 
 technical means apart ex- 
 pression . Composers constantly 
 use new technical means told 
 level age . 
 critics ask new 
 technical means appropriate cases . 
 Unfortunately composers , rule , 
 worse critics . follow indis- 
 criminately fashion , transfer unhesitatingly 
 voluptuous accents Tristan Isolde 
 love duet child prayer , gorgeous 
 pomp Walhalla rustic idyll . 
 irreconcilableness obvious , 
 . tyro able under- 
 stand simple expressed 
 simple , naive naive , 
 tranquil tranquil , complex , 
 passionate , turbulent ; , 
 masters craft fail . 
 common present - day music 
 illustrations ‘ ado . ’ 
 instructive example todo Humper- 
 dinck wonderfully clever opera ‘ Hansel 
 Gretel , ’ folk - tunes wedded 
 Wagnerian dramatic symphony — , 
 union attempted contradictories . 
 good reason proud thankful 
 technical acquisitions music . 
 wish proud thankful 
 use followers 
 great masters invented improved 

 Mozart expressed dramatic works 
 kinds moods , predominating personal 
 moods gentle moods — gentle moods 

 gay sad , strong bias tender 
 sweetly melancholy . Technically con- 
 sidered , master music radically 
 essentially diatonic ; harmony rule 
 chordal , contrapuntal , solid , liquid , 
 consisting pillars supporting 
 melody , subcurrent river 
 floats , form highest degree 
 architectural , unsurpassed 
 definiteness lucidity combined har- 
 moniousness . , radical diatonicism 
 admits frequent touches chromaticism ; 
 prevailing harmonic texture means 
 altogether excludes counterpoint 
 fugue , - pervading euphoniousness 
 gives way momentarily 
 exceptional harshness*—clashing crashing 
 seconds , major minor , favourite 
 device . , chromaticism serves 
 Mozart express dramatic works fear , 
 gruesomeness , entreaty , 
 particularly ministers moods 
 indulges readily fondly . accen- 
 tuates tender softness smoothness , 
 clinging affectionateness insinuating 
 complaints requests knew 
 express general character lines , 
 shadings colours . Mozart 
 fond essentially chromatic harmony , 
 chord diminished seventh , 
 minor mode . uses chord 
 German sixth . important 
 note Mozart use chromaticism , 
 important note limited extent use 
 — limited quantitatively qualitatively . 
 Beethoven individuality 
 type Mozart , accordance 
 find music grand , sublime , virile , nobly 
 profoundly impassionate . new worlds 
 opened required revelation 
 new applications extensions old 
 means . Boldness , freedom , vigour speak 
 — pronounced 
 diatonicism , simple harmony , 
 immensely developed rhythm , form orches- 
 tration . Beethoven add 
 number instruments , evolves 
 undreamt - capacities qualities character . 
 Think bowed instruments orchestra , 
 oboe , bassoon , horn , kettle- 
 drums , 
 . Think won- 
 derful combinations educated , en- 
 nobled , invigorated individuals . 
 advanced life art , amazes 
 form life - like 
 organism , increasing vastness propor- 
 tion , numerousness members , neglect 
 tradition . distance 
 ninth symphony , trios 
 quartets ! connection form , 
 departure orthodox key - distribution 
 calls special notice . Beethoven 
 achieved rhythm easily realised . 
 recollection symphony movements 

 96 _ MUSICAL TIMES.—FEsruvary 1 , 1903 

 foregoing added Herr Richard 
 Strauss shortly produce Berlin Dr. Elgar 
 ‘ Military Marches , ’ Sir Charles Stanford ‘ Trish 
 Rhapsody 

 recent number Die Musik ( Beethoven 
 Heft , Jahr 2 , . 6 ) articles 
 special interest . Dr. Alfr . Chr . 
 Kalischer , long hitherto unpublished 
 letter Beethoven given explanatory notes . 
 communication concerns education 
 master nephew , Carl , dated February 1 , 
 1818 , , contents clearly , written 

 following year . addressed magistrate 
 Vienna action concerning 
 guardianship nephew brought . 
 letter , long , couched dignified 
 terms ; Beethoven shows intense love 
 youth , strong desire train 
 honour tothe State . Dr. Kalischer 
 recently found copy letter 
 papers Royal Library Berlin ; 
 written Von Kochel : ‘ Copied original , 
 entirely Beethoven handwriting , addressed 
 court justice 

 Prospectus Philharmonic Society — 
 ninety - year existence — 
 document Directors - wishers 
 congratulated . especially case 
 regard eclectic nature scheme 
 names native composers 

 MUSICAL TIMES.—FeEsruary 1 , 1903 

 plébiscite programme arranged 
 Newcastle Gateshead Choral Union 
 orchestral concert conducted Dr. Richter 
 11th inst . Purchasers tickets right 
 vote certain pieces set 
 heads : Symphonies ( vote ) , Overtures ( 
 votes ) , Intermezzi ( votes ) . result 
 Beethoven ‘ Eroica ’ heads symphony poll 
 137 votes 81 given Tchaikovsky 
 . 5 . Overture section , Wagner ( ‘ Parsifal ’ ) 
 Mendelssohn ( ‘ Midsummer Night Dream ’ ) 
 scored — 107 votes , 
 o4 . Wagner takes lead 
 selected Intermezzi poll 240 , followed 
 Richard Strauss , 129 expected audience 
 wish hear ‘ Tod und Verklarung . ’ 
 interest years 
 selected works written ages 
 composers time 

 Beethoven ‘ Eroica ’ 1803 . Aged 33 
 Mendelssohn ‘ Midsummer 
 Night Dream ’ 1826 . op 
 Wagner .. ‘ Die Walkire ’ 1854 . 24 
 ge Ave ‘ Parsifal ’ 1878 . 105 
 Richard Strauss . ‘ Tod und 
 Verklarung . ’ 1890 . ve RS 

 Mr. T. W. Taphouse delving 
 old files Jackson Oxford Journal . following 
 extracts instrumental 
 finding curiosities 

 correspondent sending cutting 
 London newspaper refers incident 
 described ‘ new organ stop . ’ says : 
 ‘ expect man bellowed alive 
 , ’ adding ‘ died want 
 wind . ’ said ‘ attendant ’ bellowed 
 bellows , vox humana called 
 play , manner cause 
 organist feel tremulant 

 BEETHOVEN BADEN 

 recent publication , heading , 
 pamphlet * Dr. Hermann Rollett , brought 
 recollection pleasant interesting 
 day , August , 1892 , spent 
 friend late Sir George Grove . 
 time visiting Musical Exhibition 
 Vienna , brought instruction 
 satisfaction ; , - - bye , worth 
 remarking analytical programmes came 
 use city . Sir George quest 
 information Dr. Rollett written 
 Beethoven . Failing acquire Vienna , 
 started morning Baden called 
 Dr. Rollett . received kindly , 
 readily furnished Sir George information 
 required . hearing bound 
 Helenen Thal , volunteered accompany . 
 jumped , invited lunch . 
 declined , said early 
 dinner home join Garden 
 Restaurant entrance Helenen Thal . 
 kept word , came 
 guide living men 

 Beethoven Baden . ” Biographischen und stadtgeschtlichen 
 Beitrag von Dr. Hermann Rollett , Stadtarchivar Baden bei 

 Wien . Vienna : Carl Gerald Sohn , 1902 

 103 

 early youth seen Beethoven flesh . 
 gave lucid account meeting 
 Beethoven . walk Baden 
 mother nurse , Beethoven , imposing 
 figure , pointed , strongly 
 impressed 
 hold great genius highest venera- 
 tion . life failed , 
 present pamphlet writings 
 Beethoven sufficiently prove . 
 memorable enjoyable afternoon spent 
 Helenen Thal , beguiled 
 interesting conversation , pointed 
 different spots known 
 intimately associated Beethoven , 
 summers resided Baden — _ noted 
 health resort — spent time 
 beautiful Helenen Thal , sketched 
 greater Ninth Symphony 
 works . o’clock 
 got Baden , Dr. Rollett 
 kindly escorted house Vienna 
 banker , summer quarters Baden , 
 engaged dine . 
 probably written memorable 
 day time , felt sure Sir George 
 Grove 

 aim speak 
 wanderings Helenen Thal memorable 
 day — said good deal — 
 words Dr. Rollett recent 
 pamphlet ‘ Beethoven Baden 

 Meyer Conversations - Lexicon 1879 
 Hermann Rollett Austrian poet , 
 born August 20 , 1819 , Baden , near Vienna , 
 city received early education . 
 long list poetical works given , 
 word said musical writings . 
 1848 , thereabouts , allied 
 poetry revolutionary character , 
 escape censorship flee country . 
 residing places Germany 
 Switzerland returned , 1854 , 
 native place , resides Stadtarchivar , 
 icy Keeper Town Records , post 
 probably provided opportunities investi- 
 gating Beethoven connection Baden 
 open 

 prefatory note learn Dr. Rollett 
 pamphlet ‘ Beethoven Baden ’ origin 
 article contributed December , 1870 , 
 Baden weekly newspaper , Badener Bote 
 ( Baden Messenger ’ ) commemoration Beet- 
 hoven - hundredth birthday . soon ap- 
 peared pamphlet form . 
 print , second enlarged edition issued 
 1goz . contains ( 1 ) result Dr. Rollett 
 researches history Beethoven 
 summer visits Baden ; ( 2 ) references , 
 tearly , passages heading 
 contained writings Beethoven biographers : 
 Nohl , Kéchel , Schindler , Ries , & c. , 
 collated length ; ( 3 ) account ceremony 
 accompanied unveiling , July 1 , 1900 , 
 Beethoven - rock erected great master 
 memory Helenen Thal ; ( 4 ) inclusion 
 ) Aletter Beethoven , dating 1814 , 
 believes previously published ; 
 ( 3 ) illustrations houses 
 Beethoven time time occupied Baden , 
 Beethoven - rock stands 

 students Beethoven , especially 
 biographical turn mind , booklet Dr. 
 Hermann Rollett prove extremely interesting 
 instructive . C. A. B 

 Church Organ Music 

 letters terms endearment , com- 
 mon lovers ’ letters , dvamatis 
 personae attractive , readers thing 
 find occasionally monotonous . 
 volume runs vein musical news , 
 told fresh , ingenuous manner , thoroughly 
 idea eyes 
 sent reading , love story forms 
 sympathetic surrounding 

 venture refer passages , 
 soas spoil enjoyment read 
 book . story known Schumann visiting 
 Beethoven grave went Vienna 1839 , 
 finding pen ; , year , 
 written Clara , giving concerts city , 
 ‘ Listen , request . visit 
 Schubert ? Beethoven ? myrtle 
 branches , bind pairs , place 
 graves — slowly utter 
 — word — understand 

 Clara father arrived Vienna 
 end 1837 , young lady writes 
 concert , adds : ‘ Mendelssohn unknown 
 , ‘ ‘ Songs Words ” ? lie idle shelves 
 music shops — sing ! ‘ ‘ Mid- 
 summer Night Dream ’’ overture performed , 
 utter failure ... . wished play some- 
 thing concert , dare 
 venture public . ’ - - days 
 statement sounds strange ! Apropos 
 Mendelssohn , read Paris 1832 . Clara 
 visited French capital year , 
 thirteen years old . soirée ( March 14 ) 
 Mendelssohn Octet performed , met 
 composer company Chopin Hiller , 
 read artists ’ room saw , merry 
 schoolboy mood 

 MUSICAL TIMES.—FeEpruary , 1903 

 ASPECTS BEETHOVEN 
 INSTRUMENTAL FORMS 

 Mr. Gustav Ernest read exhaustive paper 
 subject Musical Association 2oth 
 ult . , Mr. Clifford B. Edgar chair . following 
 authoritative digest discourse 

 centuries claim Beethoven , 
 18th worship form , 19th 
 worship idea . Beethoven meet , 
 find greatest representative , 
 leading principles reconciled . 
 great ideas agitating time 
 instrumental shaping entire mode thinking , 
 entire personality , instrumental 
 shaping traditional forms . forms 
 largely outcome reflection ideas 

 lecturer referred prejudice existing 
 modern musical mind preconceived forms . 
 ‘ free , ’ asked , ‘ lives outside law , 
 accepts shapes course , 
 accordance demands indi- 
 viduality ? ' necessary , course , 
 laws conditioned Art , Art 
 laws . regard music particularly , 
 object laws away difficulties 
 prevent easily quickly compre- 
 hended arts . difficulties rest 
 fact firstly models nature , 
 secondly creations come , , existence bar 
 bar listen , making impossible 
 gain impression work asa 
 . way overcoming diffi- 
 culties , introducing Music element 
 Symmetry , perfectly expressed formula 
 - b - , i.e. , grouping similar parts ( ) 
 different ( b ) . formula , basis 
 instrumental forms , gives distinct easily 
 recognisable outline composition , offers 
 assistance listener , necessity modern 
 composers acknowledge adding explanatory remarks 
 ? music basing - known poetical 
 work , picture historical event . formula 
 equally adapted Arts , notably Painting 
 Architecture , Gothic Cathedral exemplifying 
 clearly Sonata movement . Wherefore , said 
 lecturer , Architecture general called frozen 
 Music , Gothic Cathedral frozen Sonata 

 Beethoven instinctively recognised advantages 
 possibilities forms handed 
 Mozart Haydn , accepted pure 
 simple . Life days lying , 
 vista golden hopes ; deeper problems 
 concern , old forms sufficed 
 . Soon changes , tragedy life 
 begins ; clouds ultimately shut 
 ray earthly happiness gathering 
 , ‘ fate knocking door ’ : 
 nature undergoes change , change moral 
 growth , trace men . old 
 forms soon appear narrow , conventional , 
 leaving general outlines intact , begins 
 extend , add accordance new 
 demands personality makes medium expres- 
 sion . ‘ Sonata form ’ final return 
 subjects original form incapable ex- 
 pressing life drama ; drama means action , action 
 means development change . overcomes 
 difficulty introducing elaborate Codas , 
 frequently contain climax work ( 3rd 
 movement Moonlight Sonata , 1st movement 
 ‘ Appassionata ’ ) . similar reasons replaces 
 placid Minuet pliable Scherzo , 
 wild , weird humour finds expression , 
 equally suave , idyllic rondo forms 
 ( ‘ Sonata - form , ’ theme variations , fugue ) 
 progress work demands . period , 
 misfortunes thicken , intercourse 
 difficult , gradually with- 
 draws world , 
 begins live new life — life creation , 

 world making . old world 
 disillusions vanishes , life beautiful , 
 uplifts voice praise . music 
 reflection dreams , echo voices 
 . accustomed modes expression longer 
 suffice , works assume essentially different formal 
 aspects ( Sonata , Op . 109 , 110 , Ninth Symphony , 
 Quartets ) . , , principal phases 
 Beethoven life principal stages develop- 
 ment musical style : , form reigns 
 supreme , second , form feeling ( idea ) appear 
 happy union , , feeling reigns supreme 

 Mr. Ernest — having explained frequent use 
 fugal form period — turned 
 different subject : proportions Beethoven 
 works . 1854 , Adolf Zeising discovery 
 - proportioned human body exem- 
 plifies law ‘ golden mean , ’ 7.¢. , smaller 
 ( upper ) stands ratio larger 
 ( lower ) body . 
 law found equally carried master- 
 pieces — Architecture , & c.—ofall ages . Emil Naumann 
 tried 1869 apply music , 
 results little convincing attempt 
 attracted attention . lecturer recently 
 took subject thoroughly systematically 
 Naumann results altogether surprising . 
 Sonata - form distinct parts ( , 
 working - , reprise Coda ) important 
 apply test , point find 
 larger ( plus working - , ) 
 stood ratio smaller 
 movement larger . simple process , 
 space explain 
 , Mr. Ernest , counting number bars 
 movement , calculated long 
 parts , compared actual 
 figures . found 1st movement 
 ‘ Pathetic ’ Sonata ( counting introduction ) 
 299 bars , , according law golden 
 mean , divided parts 115 184 
 bars respectively , , plus 
 working - , counts 184 , reprise , plus Coda , 115 
 bars . figures agree accurately 
 instance goes saying . science 
 taken account inaccuracy evidence 
 afforded senses . Zeising allowed 
 difference actual arithmetical 
 figures , perceived eye 
 ear ; margin fixed 34 , , Naumann , regard 
 music , ; 4 , calculations , 
 Mr. Ernest allowed # 4 , 
 instances # 1 , . final result , 
 found - movements examined 
 proportions - accordance law 
 golden mean . thirteen 
 remaining ones , Mr. Ernest discovered parts 
 way curiously symmetrically 
 constructed . instance : 
 movement rst 2nd Pianoforte Sonatas 
 working - exactly number bars 
 reprise plus Coda 

 considering general questions involved 
 Zeising discovery , Mr. Ernest emphasised fact 
 masters knowledge 
 law , , mysterious compulsion , 
 applied . , furthermore , 
 accept fresh proof unity Arts 
 continuity general principles , found 
 Gothic Cathedral 12th Sonata 
 19th century law exemplified 

 pianoforte trio G Dr. Alan Gray . work 
 consists movement , , , 
 includes clearly - defined sections , severally 
 headed Andante sostenuto Allegyo vivace . Musically 

 engaging , 
 written . instrumental works Sir Hubert 
 Parry early attractive pianoforte trio B minor , 
 dating 1884 , Beethoven rarely - heard 
 quintet pianoforte wind instruments E fiat , 
 composed 1797 . compositions excellently 
 interpreted , ‘ London Trio , ’ 
 Miss Amina Goodwin Messrs Clinton , Borsdorf , 
 Malsch , Wotton 

 concert 15th ult . peculiarly at- 
 tractive engagement Brompton Oratory 
 Choir , , skilful direction Mr. Barclay 
 Jones , sang selection motets , 
 fine . Specially entitled ‘ Christus factus 
 est , ’ old Roman master Felice Anerio , 1594 
 succeeded Palestrina composer Papal Chapel . 
 excerpt remarkable tender delicate beauty , 
 devotionally rendered , unaccompanied , 
 Oratory Choir . noble examples early sacred 
 music ‘ Justorum animae , ’ William Byrd , 
 ‘ Exaltabo Te , Domine , ’ Palestrina . Mention 
 ‘ Amavit Sapientiam , ’ Thomas Wingham , 
 ‘ Os Justi Meditabitur , ’ Mr. E. d’Evry , present 
 Organist Oratory , presided organ . 
 distinction imparted evening 
 performances Suite D violin piano- 
 forte Mr. Arthur Hinton , song entitled ‘ 
 Reverie East , ’ Sir Alexander Mackenzie . 
 Suite pleasing - written composition . 
 comprises movements , built 
 melodious themes tersely pointedly developed . 
 Miss Maud Powell Miss Katharine Goodson played 
 expression brightness , 
 applauded . Sir Alexander Mackenzie set lines 
 Mr. Owen Seaman , appeared Punch con- 
 nection recent Delhi Durbar , 
 remarkably clever use intervals common Hindoo 
 scales . Miss Ethel Wood sang manifest apprecia- 
 tion requirements style , composer 
 accompanied . Sir Alexander music , 
 added , appearance Punch . 
 vocalists Miss Gwendolen Maude 

 New Year Day concert chief feature 
 repetition performance , careful direction 
 Mr. Henry J. Wood , Richard Strauss tone - poem 
 ‘ Heldenleben , ’ work caused great 
 sensation produced ( England ) 
 December 6 , conducted composer . 
 performance took minutes longer occasion 
 notice , beauties , opinion 
 , defects manifest . Professor 
 Carl Halir played violin solo ‘ Heldenleben , ’ 
 solo Spohr Violin Concerto minor . 
 remainder concert , excellent achievement , 
 detailed notice 

 17th ult . Herr Kreisler gave masterly 
 rendering Beethoven Violin Concerto , 
 programme included Richard Strauss tone - poem 
 ‘ Tod und Verklarung , ’ Goldmark ‘ Sakuntala ’ 
 Overture . Mr. Wood skilfully conducted usual 

 interesting concert given Parish Room , 
 Teddington , roth ult . , Mr. W. A. Everington 
 Amateur Male - Voice Double Quartet Party 
 success . glees - songs sung 
 : — ' Hark , jolly shepherds ’ ( Brewer ) , ‘ Knight 
 song ’ ( Pressey ) , ‘ Sweet , love ’ ( Harris ) , ‘ Cold 
 Cadwallo tongue ’ ( Horsley ) , ‘ Requital ’ — Corona- 
 tion Prize Glee Dr. Alfred King—‘A Franklyn 
 dogge ’ ( Mackenzie ) , ‘ Discord , dire sister ’ ( Webbe ) , 
 Come , gentle zephyr ’ ( Horsley ) , ‘ Music pow’rful ’ 
 ( Walmisley ) , ‘ Goslings ’ ( Bridge ) , ‘ Sappho 
 tun’d ’ ( Danby ) , ‘ Jack Jill ’ ( Jarvis ) , ‘ Good- 
 night , beloved ’ ( Foster 

 Great interest taken performance 
 ‘ Dream Gerontius ’ Choral Union 
 12th ult . , feeling minds hearers 
 truly great work born 
 world art . performance fine 
 complex composition , Choral Union achieved signal 
 distinction , honour falls share Mr. 
 Collinson infinite pains taken 
 preparation choral work . Great praise 
 soloists — Miss Muriel Foster , Mr. John 
 Coates , Mr. Robert Burnett — orchestra 
 remarkably fine renderings parts 

 concerts , conducted 
 Dr. Cowen , conspicuously fine performances 
 given , works , Mozart Symphony 
 D Brahms F ( . 3 ) . pianist 
 December 29 M. Edouard Risler ( appear- 
 ance ) , showed astonishing technique great powers 
 exposition E flat Concerto Beethoven , 
 Brahms ' ‘ Rhapsodie ’ ( Op . 79 ) . 
 concert heard Mackenzie charming ‘ Cricket 
 Hearth ’ Overture time . programme 
 5th ult . , exception numbers ( 
 ‘ Danse Macabre ’ Saint - Saéns , Scena 
 Goldmark ‘ Queen Sheba ’ ) devoted entirely 
 Tchaikovsky , included ‘ Romeo Juliet ’ 
 Overture Symphony.in D , . 3 , superbly 
 played . leader ihe orchestra , Mr. Maurice Sons , 
 appeared soloist rgth ult . , 
 Mendelssohn Concerto Corelli Variations ‘ La 
 Folie , ’ demonstrated fact 
 violinist force 

 event considerable interest Dinner given 
 Edinburgh Society Musicians honour 
 Dr. Cowen 16th ult . distinguished company 
 musicians music - lovers , numbering considerably 
 , assembled presidency 
 Mr. J. A. Moonie , good speaking heard 
 fine music discoursed 

 MUSIC GLASGOW . 
 ( CORRESPONDENT 

 regarded important 
 events season appearance Herr Richard 
 Strauss conductor fifth classical concert , 
 December 23 . exception Spohr overture 
 ‘ Jessonda ’ Beethoven Symphony . 8 , 
 programme consisted Richard Strauss works — 
 viz . , Sérénade wind instruments , tone - poems 
 ‘ Don Juan ’ ‘ Death Transfiguration , ’ 
 heard time . 
 composer inspiring beat , playing Scottish 
 Orchestra reached highest level . reappearance 
 Herr Kreisler solo violinist ( Max Bruch 
 Violin Concerto G minor , Saint - Saéns ‘ Rondo 
 Capriccioso , ’ ) popular programme included 
 Beethoven ‘ Pastoral ’ Symphony Tchaikovsky 
 ‘ Capriccio Italien , ’ attracted large audience 
 sixth concert Christmas Day . seventh classical 
 concert , December 30 , notable chiefly 
 appearance M. Edouard Risler , 
 performance Beethoven Pianoforte Concerto 
 E flat deep impression . programme likewise 
 included novelty shape Mozart overture 
 entr'actes ‘ Les petits riens ’ Berlioz Symphony 
 ‘ Harold Italy , ’ viola obbligato work 
 finely played Mr. Maurice Sons 

 new year opened Choral Union time- 
 honoured performance ‘ Messiah , ’ rst ult . , 
 use word ‘ time - honoured , ’ 
 Choral Union originate years ago 
 ‘ Society performing Messiah ’ ? famous 
 oratorio sung Glasgow pioneers 
 City Hall , April 2 , 1844 . ‘ Messiah ’ 
 given 5th ult . Young Men 
 Christian Association Choir , energetic 
 direction Mr. R. L. Reid . soloist 
 ninth classical concert , 6th ult . , Lady 
 Hallé , , supported excellently Scottish 
 Orchestra , gave ideal reading Mozart Violin 
 Concerto . 6 E flat . concert , Tchaikovsky 
 ‘ Polish ’ Symphony given time 
 Glasgow , , beautifully played , evoked 
 moderate enthusiasm . run foreign 
 virtuosi , appearance Miss Fanny Davies 
 tenth classical concert , 13th ‘ ult . , 
 welcome . programme contained 
 novelties — lack season — 
 satisfactory . Miss 
 Davies , giving fine performance solo 
 Schumann Pianoforte Concerto , contributed 
 familiar solos leading pianists wo nt 
 ignore . - rate reading Gade Symphony 
 . 1 C minor , Dvordk Slavonic Rhapsody 
 . 3 flat , given baton Mr. Maurice 
 Sons , conducted performance absence 
 Dr. Cowen 

 rformances Constabulary band . Dr. Peace 
 recitals Saturdays attended thousands , 
 Mr. Crowley band best organisations 
 kind extant , plays huge gatherings . 
 felt , whilst adhering given 
 happiness past , time come 
 Liverpool music . 
 acclaimed suggestion ‘ municipal orchestra 

 seventh concert Philharmonic Society took 
 place 13th ult . , Lady Hallé gave 
 distinguished rendering solo Mozart 
 Violin concerto . 6 , E flat . Mr. Lloyd Chandos 
 vocalist , orchestra , Dr. Cowen , 
 gave Berlioz Symphony ‘ Harold Italy , ’ Beethoven 
 overture ‘ Leonora ’ No.1 , Mackenzie overture ‘ 
 Cricket Hearth . ’ Mr. Simon Speelman distin- 
 guished fine performance viola 
 Berlioz Symphony . chorus , hard 
 work Elgar ‘ Dream Gerontius , ’ sang Max Bruch 
 ‘ Jordan banks 

 Mr. Ernest Schiever penultimate concert given 
 17th ult.in College Music . programme 
 included Tchaikovsky String Quartet . 2 , F , 
 Richard Strauss Pianoforte Quartet C minor ( Op . 13 ) . 
 solo violoncellist Mr. Walter Hatton , Mr. 
 Isodor Cohn pianoforte 

 Hallé concert 8th ult . soloist 
 Lady Hallé , delighted enormous audience 
 rendering Mozart E flat Violin Concerto , 
 brilliant performance given Glazounow ' ‘ Carnival ’ 
 Overture . concert consisted 

 Beethoven ‘ Pastoral ’ Symphony , 
 Glazounow Overture Liszt Hungarian Rhapsody 
 ( . 2 orchestral , . 12 pianoforte series ) , 
 Dr. Richter conducted like giant refreshed 
 Christmas holidays 

 large 

 15th ult . soloists Misses Agnes Nicholls 
 Muriel Foster , Messrs. William Green Andrew 
 Black . choir sang , orchestra played 

 , performance applauded . 
 ‘ Coronation Ode , ’ given interval , afforded 
 good contrast , broad , hearty , popular ring . 
 soloists heard greater advantage . 
 longer works , Miss Foster sang ‘ Che 
 fard ’ striking success , Mr. Green gave 
 highly acceptable rendering Wagner ‘ Preislied . ’ 
 Brodsky Concert 14th ult . brought 
 record audience , principal attraction 
 association Lady Hallé Dr. Brodsky Bach 
 Concerto violins . scarcely imagine 
 finer performance given noble work , 
 Quartet surpassed Beethoven 

 Op . 130 wonderfully charming 
 work quartet form Tchaikovsky 
 referred Mrs. Newmarch recent article 

 MUSIC YORKSHIRE . 
 ( CORRESPONDENT 

 customary Handelian ‘ boom ’ Christmas 
 time music chronicle . 
 3rd ult . Huddersfield Glee Madrigal 
 Society gave concert , sang - songs 
 like admirable neatness perfection 
 ensemble Mr. Ibeson Mr. Armitage . 
 sensation evening , , remarkable 
 organ - playing Mr. David Clegg , virtuosity 
 great , method effects hardly keeping 
 character instrument.——On 7th ult . 
 Cleckheaton Orchestral Society , Mr. C. Stott , 
 gave aconcert , chiefly light inartistic music , 
 Miss Ada Crossley gave genuine pleasure 
 admirable singing.—On 12th ult . Mr. Edgar Had- 
 dock gave new series chamber concerts , 
 violin sonatas Beethoven , Brahms , Dvorak , 
 Rubinstein played . Mr. Haddock , course , 
 violinist , pianist clever young artist , 
 Mr. Edward Isaacs , prowess wasstill strikingly 
 displayed following evening concert- 
 giver ‘ Musical Evenings , ’ Miss Marie Brema 
 vocalist , Mr. Boris Hambourg violoncellist . 
 14th ult . Morley Choral Society , Mr. 
 Alfred Benton conductor , gave creditable 
 performance Goring Thomas ‘ Swan 
 Skylark , ’ principal parts taken 
 Miss Wormald , Miss Appleyard , Messrs. Fallas 
 Broadhead 

 Miscellaneous 

 BERLIN . 
 recent sitting committee Wagner 
 Memorial announced Professor Dr. Fritz 
 Volbach , Mayence , accepted invitation 
 compose hymn ceremony October , 
 Berlin Sangerbund , meeting 
 January 3 , unanimously resolved perform 
 music , occasion strengthened 
 byaboys’choir . Letters read Richard 
 Strauss , Arthur Nikisch , Victorin de Jonciéres , 
 Anton Dvorak , accepting invitation mem- 
 bers international honorary committee 
 unveiling Richard Wagner monument 

 BONN . 
 Beethoven House Society 
 Beethoven Festival place 17 - 2r . 
 Joachim Quartet perform master quartets , 
 programme illustrating periods 
 Beethoven work conveniently roughly 
 divided 

 arranged 

 PRESSBURG 

 read Neue Musik - Zeitung Beethoven 
 ‘ Missa solemnis ’ performed entirety St. 
 Cecilia day cathedral , constituting 
 liturgical service . given 1835 , 
 1891 performed year . 
 Church musical societies took , conductor 
 Dr. Eugen Kossow 

 SANGERSHAUSEN 

 DuNEDIN , NEW ZEALAND . — Dunedin Musical , 
 Literary , Elocutionary Competitions Society held 
 Annual Festival Victoria Hall St. Mat- 
 thew School week November . 
 300 competitors heard pianoforte , violin , 
 vocal sections , number local choirs entered 
 Choir Contest . Mr. Maughan Barnett , 
 Wellington , judge 

 HASLINGDEN.—Miss Mary Spencer gave Second 
 Annual Chamber Concert Public Hall 14th 
 ult . , artists , , Mr. John Lawson 
 ( violinist ) , Mr. William Warburton ( violoncellist ) ; 
 Miss Ellen Sellars vocalist . programme 
 included Grieg Sonata C minor pianoforte 
 violin , Beethoven Trio C minor , Chopin 
 Introduction Polonaise pianoforte violoncello , 
 played Miss Spencer colleagues 
 avery brilliant manner . Mr. George Oldham 
 efficient accompanist 

 Newport ( Mon . ) . — Musical Society gave 
 performance ‘ Elijah ’ Tredegar Hall 
 15th ult . solo vocalists Miss Winifred Ludlam , 
 Miss Minnie Chamberlain , Mr. Tom Child , Mr. 
 Arthur Deane , Youth sung 
 Master Charles Goulding . Mr. E. G. R. Richards 
 conducted 

 TEACHER SINGING.—The operas ‘ Le Perle de 
 Bresil ’ ( David ) ‘ Lakmé ’ ( Delibes ) published , 
 price { 1 net ; songs work 
 obtained separate numbers . operas 
 ‘ Il Seraglio , ’ ‘ Cosi fan tutte , ’ ‘ Idomeneo ’ 
 published Italian German words 
 2s . 3s . ; songs issued separately 

 R. W. P.—(1 ) 6 - 4 time duple rhythm , 
 accents naturally fall fourth beats 
 bar . ( 2 ) ‘ afraid ’ ( Mendelssohn ) arranged 
 organ Dr. Steggall ; know 
 similar transcription Beethoven ‘ Kreutzer Sonata , ’ 
 second movement 

 C. E. M. J.—We sorry names 
 organ - builders teachers 

 CONTENTS 

 _ — — _ — _ Page 
 Salisbury Cathedral ( Illustrated ) na oe ne ost . oT 
 Dr. Maurice Greene ( special Portrait ) 89 
 Development Musical Styles , Mozart end ot 
 Nineteenth Century 93 
 Board Education Musical Instruction Tre 1ining 
 Colleges ... nce ae ; “ = OY 
 Occasional Notes ( imoanitions 99 
 Beethoven Baden oe ee 102 
 Church Organ Music ... ou 103 
 Reviews oes = Ps 113 
 Correspondence 115 
 Incorporated Society Musicians Se 115 
 Aspects Beethoven Instrumental Forms . 116 
 London Suburban Concerts ... 117 
 Music America 118 
 ” Vienna 118 
 Pr Bristol 119 
 es Edinburgh 119 
 ms Glasgow ase 120 
 ee Gloucester District 120 
 nA Liverpool District ... ie ane aus ae ase | 120 
 RS Manchester ree mas 121 
 Norwich District ... 121 
 Yorkshire .. 122 
 Miscellaneous 122 
 Foreign Notes ant ove ove ove 123 
 Country Colonial News ee ove 123 
 Obituary 124 
 Answers Corre sponde nts 124 
 Music published month nA oe « 25 
 Easter Anthem—‘O death , thy sting?’—A. Herbert 
 Brewer oa ree = ase aa 205 

 GERTRUDE DRINKWATER 

 cele br : ated L : argo — Ombra 

 Schumann 
 Beethoven 
 Mendelssohn 
 Handel 

 Handel 
 Giordani 
 Mozart 
 Beethoven 
 Schubert 
 “ Mendelssohn 
 Purcell 

 W. S. Bennett 
 Mendelssohn 
 Schubert 
 Handel 

