M 

 party . 19 , Gord lon Street , Gordon Square , 
 M ADAME CLARA WEST aawaae and MISS 
 LOTTIE WEST ( Contralto ) , of the Crystal Palace , the 
 h of Hackney Choral Association Concerts , the Morley Hall 
 erts , & c. , accept engagement for Oratorios , Cantatas , Ballads , 
 & . address , Beethoven Villa , King Edward Road , Hackney . 
 yess EVELYN MORDAUNT ( Contralto ) beg 
 a to announce her change of residence tto 5 , Ra ayner 
 Street , Ripon , where all communication respect Concerts shou 
 beaddresse . N.B. letter unanswered have not be receive . 
 ISS CLARA WOLLASTON ( Contralto ) will 

 sir ng at The Spa , Scarborough , from Novembe 

 more than mere curiosity . indeed , the disposition 
 thus generally exhibit by the educated portion of 
 the community be apt to assume the form of self- 
 depreciation whenever our national musical status be 
 touch upon ; a fact the existence of which will , 
 without enter more minutely upon the subject , be 
 sufficiently account for by a reference to the num- 
 ber of composer of original genius produce by this 
 country during the last two or three hundred year 
 as compare with that of the great master who 
 have spring up during the same period in Italy , 
 France , and , above all , in Germany . ifthe result of 
 such a comparison alone be accept as conclusive 
 evidence , then , undoubtedly , we can scarcely as yet 
 lay claim to the title of a musical nation . that it 
 have be so accept by not a few of our critic in 
 the past , whose experience of english national life 
 have be , to say the least , a very limited one , we 
 be well aware ; that it should continue to be 
 so accept in some quarter , notwithstanding 
 the manifestly rapid stride in general music culti- 
 vation which this country have be make of late 
 year , be , however , a circumstance which some- 
 what detract from the merit and the instructive 
 value of the stricture periodically pass upon we as 
 an unmusical people . when , in the year 1746 , Gluck 
 produce one of his early opera in London with 
 but scanty success , Handel be report to have 
 remark to his young compatriot , ' ' you have 
 give yourself too much trouble with this work , the 
 like of which be not appreciate here . in order to 
 please the English , you must think of something more 
 demonstrative and more calculated to stir coarse 
 nerve . " the story be relate from hearsay by 
 Reichardt , and bear intrinsic evidence of be 
 totally unfounded . Handel understand the genius of 
 the inhabitant of these island — for whom he have 
 write his sublimest masterpiece — much well . 
 but , si non ¢ vero , the story be at least ben trovato as 
 show the kind of estimation in which english 
 musical taste be hold on the Continent in the past 
 century . with somewhat altered condition , the 
 same opinion concern ourselves may be safely 
 say to be still prevail in the mind of a majority 
 of our continental neighbour at the present day 

 if , as we have state , there exist a tendency on 
 our part to underrate our own musical capacity , 
 there be , on the other hand , no lack of self - assurance 
 to be discern in the writing of some of our most 
 recent critic . Dr. von Biilow , some time since , 
 furnish a graceful example of this fact by the pub- 
 lication in the Leipzig Signale of a series of letter 
 concern the musical institution of the ' land of 
 fog , " as he politely denominate this country , which 
 do more credit to his dialectical power than to his 
 artistic individuality , and the chief purpose of which 
 seem to be to serve as a convenient background from 
 whence the central figure of the versatile pianist him- 
 self might stand out the more effectively . great man 
 invariably have imitator . no soon have Hans von 
 Bilow , some year ago , accomplish his tour de force 
 of play from memory six of Beethoven ’s sonata 
 at a stretch , than we be tell in our " foreign 
 Notes " that ' the eminent austrian pianist , Herr 
 Bonawitz , " have do the same thing at Vienna . the 
 last - name gentleman have now likewise emulate his 
 prototype in his critical excursion in England , and 
 have communicate his experience of our musical 
 capacity , taste , habit , and institution to his 
 friend of the Wiener Signale . the subject he have 
 choose for his inquiry lie , it be true , in a small 
 compass , and could be easily circumvent by a 
 foreigner in a week or two , especially if he be an 
 eminent pianist follow in the footstep of another 
 still more eminent . but Herr Bonawitz have go 

 to work more conscientiously . he arrive in this 

 Mr Ma ickenzie ’s Cantata ' ' Jason 
 , however , the fame and popt of 
 M. Gounod , toge der with the wide - spread discussion of h 
 Oratorio , ' * the redemption , " have stimulate public curiosity 
 in an exceptional manner , the french composer ’s ma 
 num opus be include . for the rest , there be ' ' Elijah 
 h a leading daily contemporary describe with perfect 

 whic 
 truth ' a financial not less than an artistic necessity 
 y siah " ' ( of course ) , Beethoven ’s Mass in D , 
 ' Moses in sy Pts " Haydn 's ' ' Spring , " anda 

 number of classical and modern co1 ee for orchest 
 alone . it be hard to find fault with such a 
 ; , since it represent almost every school 
 be a school — to which the ' * ch ve 

 every seat in the 

 which the ' Bene 
 Madame Albani , 
 ) Hilton may be p 
 exact a work , they then 
 gether , the perform ince of the 
 there may ¥ e no disgrace in defeat by Beethoven 's e 
 jordinary music , but nothing s 1 

 repens 

 the MUSICAL TIMES.—Novempser 1 , 1882 

 utilise such excellent material to the utmost . Mr. Hallé’s| telligent singing and appropriate dignity of style . thp 
 rendering of the pianoforte solo in Beethoven ’s work be | easy chorus be give with spirit , and , generally speak 

 e , the performance be amongst the good of the wee 

 the MUSICAL TIMES.—Novemser 1 , 1882 

 Mr. Beringer to say that he have evidently study the 
 work carefully , and in many respect do justice to his 
 difficult task . to master that task in all its exigency 
 would require the strength of a giant and the intellectual 
 grasp of a ' " ' subtle - souled psychologist . " the remainder 
 of the programme include Beethoven ’s Symphony in A , 
 Mr. Lloyd ’s admirable rendering of Berlioz ? ’ hymn to 
 Happiness from ' Lelio , " and the ' * prize song " from 
 Wagner 's " Die Meistersinger 

 the most important number of the second Concert of 
 the series ( 21st ult . ) be the Symphony in D ( no . 6 , 
 op . 18 g ) by Raff , play for the first time at the Palace 
 and in England generally . the poetic import of the work 
 be sufficiently indicate by its matter , thus imperfectly 
 englishe in the programme — * life , Aspiration , suffering , 
 struggle , death , fame " ; its musical structure be that of 
 its predecessor from the same pen . Raff be an excellent 
 musician , who fall short of be a great musician only 
 through want of concentration and originality . he deve- 
 lope no style of his own , but could express himself in 
 almost any style he choose to adopt . there be no individual 
 creation in his Sixth Symphony , but there be plenty of ex- 
 cellent music . Madame Ida Bloch be the pianist , and 
 Miss Ella Lemmens the vocalist of the Concert 

 for abstract music of the high type 

 the performance in question open with a fine render- 
 ing on the part of Madame Norman - Néruda and M 
 Ries , Holla ler , Zerbini , Pezze , and Piatti , of Brahms ’s s 
 Sextet in G major ( Op . 36 ) , one of the most finished and 
 most profound among the chamber composition of modern 
 production , and a work , moreover , illustrate in a marked 
 degree the peculiar intermediate position occupy by its 
 composer b etween the classical Beethoven and the romantic 
 Schumann . Mdlle , Janotha be the pianist , and gain her 
 usual well - deserve applause in Mendelsschn ’s charac- 
 teristic ' ' variation Sérieuses ’' ; another instrumental solo 
 be contribute by Madame Norman - Neéruda , who play , 
 for the third time at these Concerts , the prelude , romance , 
 and Scherzo ( from Op . 27 ) for violin , with pianoforte accom- 
 paniment , by Franz Ries . the three movement , although 
 not conspicuous for originality of thought , be exceedingly 
 well - write for the instrument , and receive a masterly 
 interpretation at the hand of the lady executant , whose 
 exquisite ' ' singing " of the romance more especially elicit 
 enthusiastic applause . song by Handel , Schumann , and 
 Mendelssohn be well declaim by Miss Carlotta 
 Elliot ; and the Concert conclude with a capital perform- 
 ance of Haydn ’s bright and genial string quartet in D 
 minor ( Op . 42 ) , in which the lady violinist and MM . Ries , 
 Hollander , and Piatti take the respective part 

 the programme of the follow Saturday afternoon 
 Concert include a very thoughtful reading , on the part of 
 Mdlle . Janotha , of Beeth 1oven ’s pianoforte " sonata ( Op . 27 ) 
 in e flat , the fine impression produce by which be , how- 
 ever , partly mar by a portion of the audience insist 

 ie 
 finished rendering of the violoncello Sonata in D major , 
 by Locatelli , in ' which he be ably support by M , , 
 Zerbini , who play the pianoforte accompaniment . the 
 performance open with Schumann ’s string quartet in4 
 minor ( Op . 41 , no . 1 ) , execute by Madame Norman . 
 Néruda and MM . Ries , Hollander , and Piatti , and cop , 
 clude with Mozart ’s charming Sonata for pianoforte and 
 violin , in F major , in which the two lady executant of 
 the Concert take part . Mr. Santley create the usual 
 furore in his favourite song ' * o , ruddy than the cherry " 
 and Gounod ’s " maid of Athens , " ' to which he add , 
 in response to several recall , the chanson arabe * medjé " 
 by the same composer 

 on Monday evening , the 23rd ult . , Beethoven ’s string 
 Quartet in e flat ( Op . 74 , no . 10 ) , know as the " Harfes 
 Quartet,’’on account of the arpeggio passage for the first 
 violin for which the Allegro movement be cor aspicuous , one 
 of the most characteristic work of the master ’s so - call 
 second period , form the first item in the programme , 
 the pianoforte Sonata in E minor ( Op . 90 ) by the same 
 composer , consist of two movement only , and associ . 
 ate by tradition with an amorous episode in the life of 
 Count Lichnowski , to whom the work be dedicate , be 
 play with her usual poetic feeling and refinement by 
 Mdlle . Janotha . again the most noisy portion of the 
 audience testify to its want of good taste by repeat 
 call for an encore , a demand to which the performer 
 finally retaliate , as we take it , by play some dashing 
 variation on ' * Home , sweet home , " which , singularly out 
 of place though they appear in these surrounding , be 
 probably intend as a well - merit reb to the noisy 
 clamourer , rather than as an affront to that section of the 
 audience which manifest its disapprov some slight 

 hiss . however that may be , it be high time that some 
 effectual measure be adopt to check this an . 
 nually increase and , from an artistic point of view , 
 most objectionable practice . other number of the 

 evening ’s programme be a violoncello Sonata in F 
 major , by Porpora , play in his accustomed masterly style 

 by Signor Piatti , and Schumann ’s Sonata for piano . 
 forte and violin , in a minor ( Op . 105 ) , in which the lady 
 pianist be associate with Madame Norman - Néruda , 
 who also lead the performance of Beethoven ’s Quartet . 
 Miss Santley be the vocalist , and contribute song by 
 Handel , and Maude V. White . the young singer have make 
 decide progress since last we hear she at these concert , 
 her voice having gain somewhat in fulness and timbre , 
 and her execution in general finish . Mr. Zerbini , on the 
 three occasion here refer to , be a most efficient 
 . | accompanist 

 the javanese " GAMELAN 

 gesture m artist ; but , ; rally , in 
 regard to n ance , melodic p ! and 
 bleat quality esemble exactiy the half- 
 caste and degenerate descendz cate of the Incas . 
 MUSIC in BRISTOL . 
 ( from our own correspondent 

 the ason may be say to have com- 
 m ence h the performance eof Beethoven ’s 
 opera ' lie atre Royal , by th 1eca | Rosa 
 com ] this company remained - hose a full 
 house night , especially on the 4 i d 6th ult . , when 

 hey ¢ ner ’s * Flying Dutchman " | Gou nod ’s 
 " Faust . " next event of interest be th t Monday 

 Julia Jones , Madame Rosa Bailey , Mr. Harper Kearton , 
 and Mr. Montague Worlock be the soloist . there wasa 
 large attendance 

 the second Monday Popular Concert take place on the 
 23rd _ ult . , when the follow programme be perform : 
 overture , ' ' Oberon , " Weber ; Symphony , no . 3 , in e flat , 
 " * Eroica , ' " ' Beethoven ; Recitative and air , ' * che faro , " 
 Gluck ; overture to ' ' Tannhiuser , " Wagner ; Minuet and 
 Trio , Prout ; Aria , " ' Di tanti palpiti , ' " Rossini ; Albumblatt , 
 Wagener ; and Landler , ' ' Grossmutterchen , " Langer ( by 
 request ) ; song , " the lost chord , " Sullivan ; overture to 
 ' ' Zampa , ’' Herold . the drum be frequently too loud 
 and not always in tune in the symphony , and there be 
 occasionally a want of attack in the first movement , which 
 go the least well of the four . the flute in the ' ' second 
 subject ' of the finale be very good . in the overture 
 to Oberon " the same over - loudness of the drum be 
 noticeable , but the overture to ' * Tannhauser , " ' a standing 

 favourite in Bristol , be altogether successful . Mr , Prout ’s 
 graceful Minuet and Trio be play con amore , and hada | 
 warm reception from the audience . Miss Hilda Wilson ’s | 
 name be familiar to your reader in connection with the | 
 Hereford Festival . she be the only vocalist on this | 
 occasion , and sustain her reputation 

 aa Messiah , " 
 1D. there 
 ibscriver to the 

 Haydn ’s * * creation , " and Beethoven ’s Ma 

 sht Subscription Concerts ; and st 

 the programme of the two Richter Concerts announce 

 exceptional interest to all lover of high - class music . at 
 | the first the Introduction to Wagner 's * Parsifal " ' will be 
 give for the first time in England , and the programme 
 also contain the Introduction to the third act of " Dit 
 Meistersinger , " ' Mr. Villiers Stanford ’s Orchestral Serenade 
 ( produce at the recent Birmingham Festival ) , Liszt 's 
 hungarian Rhapsody in F , and Beethoven 's ' Eroica " 
 | Symphony . the programme of the second Concert com . 
 | prise Brahms ’s new Pianoforte Concerto , to be play by 
 | Mr. E. Dannreuther , the introduction and Closing scent 
 |from " Tristan und Isolde , " ' Beethoven ’ s C minor Sym . 
 | phony , and Weber 's Overture to ' Euryanthe . " the 
 ; orchestra will be lead by Herr Ernst Schiever , and her 
 | Hans Richter will , as usual , be the conductor 

 be hold at Mr. Evan Spicer ’s , Tulse Hill , on the 17th ult , 
 when Mr. C. Dowdeswell lecture on ' ' Richard Wagner 

 c7th ult 

 Wagner ndere by Mr. George Whillier and Master Busby ) , 
 by Miss@beethoven ’s ' * hallelujah " chorus ( ' * engedi ’' ) , Tallis ’s 
 - Walter ( Festal response , and special Psalms and Hymns . the 

 husic ( which be under the direction of the organist and 
 hoirmaster , Mr. J. R. Griffiths ) be well render by the 
 hoir , and satisfaction . the church be 

 sufficiently appreciate to repay the heavy expense at- 
 tendant on its translation and publication . " these w ord 
 suggest a thought which must occur to the mind of every 
 amateur who , not be a reader of German , have long 
 regret that the masterpiece of musical bi — é 

 seal to he , and who have often be tell that nos 
 cient public exist in this country to warrant the co 
 produce they in the vernacular . for ao uch music- 
 lover have hear with desire , and well nigh despair , 
 not only of Jahn ’s ' Mozart , " but the still incomplete 
 ' Haydn , " by Pohl , and other monument of true german 
 research and conscientiousness in the d of musical 
 literature . there never be , however , any occasion for 
 despair . slowly , perhaps , yet surely , a musical reading 
 public have form itself ; and the freq appearance 
 in English of work like Schumann ’s collect essay 
 and criticism and Wagner ’s * ' Beethoven ' " ' give earnest 
 of a time when still " oreater thing would be do . 
 that period have arrive — the three handsome volume 
 before we be proof of it , and we shall be greatly mistake 
 if their success do not demonstrate the absence of any 
 reason why english amateur should , in regard of musical 
 literature , be less fortunate than their german brother . 
 an english reviewer be not likely to forget that we have 

 1 
 15 

 Transcription of Mendelssohn Bartholdy ' ’s Prayer from 
 the Opera * * Melusine 

 transcription of Mendelssohn Bartholdy ’s Song of Love . 
 arrange for the piano by S. Kahlenberg . [ B. Williams . ] 
 ' | , we have often call attention in this journal to the 
 " ' } ' act of musical publication be issue — sometimes with 
 " } ) and sometimes without the name of the arranger — in which 
 the note of many of our eminent composer be make to 
 , sve a purpose utterly foreign to that intend by their 
 / author . word , for example , have be fit to Mendels- 
 | Sohn ’s " song without word " ; theme by Beethoven 
 set to commonplace verse by commonplace poet ; and 

 set fort 
 ithoriti¢ 
 y namés 
 ample « i 

 619 g 

 ninteresting collection of autograph recently sell by 
 tion at Munich include those of Haydn , Mozart , and 
 per , which realise the follow price , viz . : Mro2 , 
 2,and M52 respectively . " 
 another valu able collection of autograph of musician 
 fi poet be recently place under the hammer by the 
 , of Liepmannssohn , of Berlin , it comprise letter , 
 .from the pen of Beethoven , Cherubini , Spontini , 
 yerbeer , Gounod , Tartini , Weber , Mendelssohn , Zelter , 
 ksini , Schumann , Wagner , and other composer , 
 ide those of numerous literary celel include 
 arles Dickens . amongst the most interesting number 
 d be : Symphony of Mozart ’s for m1005 , 
 ethoven ’s setting to " Freudvoll und Leidvoll " from 
 " for m500 , three manuscript by Mendelssohn 
 5 , and M4 oo respectively , an tograph by 
 Ryerbeer M15 , and one by Spohr Mros5 
 Mr. E. D’Albert , who ha be 
 s german town , have meet everyw ! 
 # husiastic recept 10n . 
 he Leipzig S \ new opera 
 ich have appez ure during the last a very small 
 mber only have succeed in establishin g themselves per- 
 nently on our lyrical stage . among they Bizet ’s * 
 n , ' Goetz ’s ' the Taming of the Shrew , ' Goldmark ’s 
 igin von Saba , ' Brill ’s ' Das goldene 
 s ' Der Rattenfanger von Hamelin , 
 st - mention work , althoug ! 
 h equal success , posse 
 scome either at once 
 t répertoire , or at all event to 
 mtime to time . of Wagner ’s ' N 
 Jalkire " alone have so far succeed 1 
 ppertoire - opera ' on the comparatively 
 vet , the entire work have be prox luce : 1 
 pee i theater at Hanover recommence its per form . | 
 ce , last month , with Gluck ’s * * Iphigénie en Aulide . " 
 e first performance at Leipzig of Ri 
 Mie Maccabier " be to take place on the 
 ne work ewise shortly to be produce at 

 A 
 " i 

 S i m 
 ssor 

 Sg ali 
 ' ell . know — -Unterrichtsbricfe , edi y H. Mann 
 imer , have be publish by Novello , ewer nad " a 
 the followin g , accord to the Wiener Signale , be the 
 ork to be produce during the present season by the 
 hilharmonic Society of Vienna : Ciaconna Bach , in- 
 tumente by Raff ) ; overture " Coriolan " and * * Leonore , " 
 2 ; syn mphonie no . 2 , 3 , and8 ( Beethoven ) ; ' ' Romeo 
 and Juliet , ' ' three scene ( Berlioz ) ; Serenade No . 1 ( Brahms ) ; 
 i rtur res | ' ' Faniska " ' and ' ' Der portugiesische Gasthof " 
 * * Legenden " ( Dvorak , first performance ) ; 
 ( e sser ) ; Serenade No . 2 ( R. Fuchs ) ; overture 
 Venls ( Gade ) ; Symphony ' Landliche Hoch 

 stein ’s Opera | 
 the Dresden 

 we subjoin , as usual , the programme of concert * re- 
 cently give at some of the lead institution abroad 

 Paris.—Concert Populaire ( October 15 ): symphony , c major ( Beet- 
 hoven ) ; Chant du Soir ( Schumann ) ; Suite Algérienne ( Saint - Saéns ) ; 
 prelude , ' ' Lohengrin " ( Wagner ) ; overture , ' ' Oberon " ( Weber ) . 
 Chatelet - Concert ( October 22 ): Symphony , c minor ( Beethoven ) ; 
 Violin Concerto ( Mendelssohn ) ; prelude , ' " Parsifal ' " ' ( Wagner ) ; 
 Nocturne , transpose for violin ( Chopin ) ; Spanish Dance ( Sarasate ) ; 
 fragment from " Roméo et Juliette " ( Berlioz ) . Lamoureux Concert 
 ( October 22 ): symphony , f major ( Beethoven ) ; overture , ' Carnaval 
 Romain " ( Berlioz ) ; concerto , d minor ( Rubinstein ) ; minuet for 
 string ( Handel ) ; Preiude , " Parsifal " ( Wagner ) ; Finale of Diver- 
 tissement ' ' Les Erinnyes " ( Massenet ) . Concert Populaire ( October 
 22 ): symphony , D major ( Beethoven ) ; Concerto Symphonique for 
 pianoforte ( Litoff ) ; Airs de Danse , ' ' Les Fétes d'Hébé " ( Rameau ) ; 
 Prelude , ' " Parsifal ' ( Wagner ) ; overture , ' ' Patrie " ( Bizet 

 Leipzig.—First Gewandhaus Concert ( October 5 ): Symphony , e 
 flat major ( Haydn ) ; Toccata , f major ( Bach ) ; ' Gesangscene " for 
 violin ( Spohr ) ; adagio and rondo from first Violin Concerto ( Vieux- 
 temps ) ; Symphony , no . 8 ( Beethoven ) ; violinist , Madame Norman- 
 Néruda . Concert of the Pianist Camillo S. Engel ( October 11 ): 
 sonata , e flat major , op . 7 ( Beethoven ) ; Lied ohne Worte , no . 1 
 ( Mendelssohn ) ; Gavotte ( Reinecke ) ; Scherzo ( Schubert ) ; Impromptu 
 ( Chopin ) ; Tarantella ( Liszt ) ; vocal soli . second Gewandhaus Con- 
 cert ( October 12 ): overture , ' ' water Carrier ' ( Cherubini ) ; Lieder 
 ( Schubert and Brahms ) ; Violoncello Concerto ( Molique ) ; piece for 
 Violoncello ( Schumann , Reinecke , Klengel ) ; symphony , ' in the 
 Forest " ( Raff ) . Gewandhaus Concert ( October 19 ): ' ' Fest - Ouver- 
 ture " ( A. Dietrich ) ; air from ' ' La Sonnambula " ( Bellini ) ; and " Le 
 Démon " ( Rubinstein ) ; " Carnival of Venice " ? ( Benedict ) ; concerto , 
 e flat major ( Beethoven ) ; rhapsody ( Liszt ) ; symphony , b flat major 
 ( Schumann 

 contribution intend for this column should indicate clearly the 
 place and date of performance , as otherwise they can not be insert 

 Chopin , Mendelssohn , Schumann , Bendel , Liszt ) ; song , " Sch 
 Kronos , " " Das Rosenband , " " Gruppe aus dem Tartarus " ( S 
 " Frihlingsfahrt " ( Schumann ) ; " Jouis , " from " La Lyre et laH 
 ( Saint - Saéns ) ; Uniberwindlich ( Brahms 

 Boston , Mass.—First Concert of the Symphony Orchestra , 
 direction of Herr Henschel ( October 7 ): overture , " Zur Wei 
 Hauses " ( Beethoven ) ; Pianoforte Concerto , Op . 54 ( Schumann 
 sian " ? Symphony , g minor ( Rubinstein ) ; Bagatelles for piar 
 ( Beethoven ) ; rhapsody , no . 8 ( Liszt ) ; hungarian dance ( br 
 second concert of the Symphony Orchestra ( October 14 ): 
 and prayer from " Rienzi " ( Wagner ) ; symphony , no . 1(b 
 Chaconne et Rigodon from " Aline , Reine de Golconde " ( Mo 
 Overture and Air from " ' Masaniello " ( Auber 

 CORRESPONDENCE 

 be sustain by Mr. A. D. Coleric 
 and Miss Wadhz 

 be a green hill far away " and " Ave Veru 
 d by in the Lor and Cowen ’s 
 the chorus be give with feeling and accuracy . the room be | Lambeth conducted.—At the second of the Monday Eve 
 crowded , nce very appreciative . it be purpose to repeat cert , on the 16th ult . , Signor Foli be the vocalist . Mr. J. Bat 
 the Oratorio ¢ son , Fephtha never having be hear in | Mus . B. , who act as accompanist , also contribute an organ sou . 
 Derbyshire betore . : Portock.—A Harvest Thanksgiving Service be hold on the 
 MELBoURNE.—Two concert have be give by the Metropolitan | ult . , with an orchestra of about eightec n instrument . have 
 rtafel rai conduct by Mr. Julius Herz . the first , on | overture to Samson and the March in Jos hua be well playe 3 
 August 2 , be give in the Town Hall . in addition to the part - s inge the band . the anthem , Mendelssohn ’s " hear my _ prayer , the 
 of the Liedert Martina Simonsen sing Mozart 's " Gli ang excellently perform , the solo be sing by Walter Hook , a meqm 
 d ’ inferno , " and h uder give Meyerbeer ’s " ah ! 1 Ma of the Chardstock choir . the hymn be acc " yy by the ot 
 Vogrich play ( 4 ) Nocturne , D flat , Op . 27 ( Chopi sta scato tra , the music having be arrange by the Rev. Hook , 
 ( Vogrich ) and all 4S Ay Bril ant , from the Concerto ( Henselt ) . the enn gh ye nout . after the blessing have be p sit the re 
 part - song include Ischirct s Cantata , ' * God , Love , and Fatherlan he Rev. W. Hook ) announce that a selection of music woul 
 Kiicken ’s ' ' Soldier ’s Song , " Cavaliers ’ Chorus from " Roberto il | os aye by the orchestra , and all be invite to stay and listen . BBeiey ; 
 Diavolo " ( Meye rbeer ) , Hatton ’s " Sailor ’s Song " and " Warrior 's invitation be accept by nearly the whole congregation ; ani tt , 
 song , " Huntsmen ’s Chorus from ' " Der Freischiitz " ( Weber ) , | first movement of Beethoven 's septet and the slow movemetiert \ 
 & c.——the ond Concert be hold in the Atheneum Hall , on | the same composer ’s Secon i d Symphony be perform . the f§lay ¢ 
 Wednesday , st 23 . the soloist comprise Mr. T. H. Guenett | cessful result of this service show that even in a small parish , 4 
 ( hon . pianist ) , Mr. G. Weston ( violin ) , and member of the Liedertafel , | work and « rgy , agood choir can be organise , and good musi 
 with Mr. C. = El sag og nag o - oo the part - song include Dr. | sente to the people . 
 Garrett 's vocal waltz ' ' Hope , " Elsasser ’s arrangement of Gumbert ’s ee e sates ili i 
 ' Frohsinn " ( first time ) , Hatton ’s " ' absence , " Kiicken ’s ' * Wanderer 's | , PL ao speci Sige gf oe il Concert w hi iven at the Mecha 
 Song , " & c.——The society have in rehearsal a chorus from Berlioz ’ Mic oa : ee on the coe be ad 4 vocali ree ue oh i solo pil 
 * " Damnation of Faust " and a Cantata by Mr. Alfred Plumpton . Miss Kate B. Hearder , and Messrs. Hearder and Morris ; solo pe 
 ? I Mrs. Hy . ed ; conductor , Signor Brizzi and Mr. F. N. 
 Mucu Bircu.—On Michaelmas Day a Festival service be hold | accompanist , Mr. C. Clemens , the encore be numerous , até 
 in the Parish Church to celebrate the Harvest Thanksgiving and the | artist highly appreciate 

 a 

 a valuable selection in KOBERT COCKS and CO . ’S they 
 CATALOGUE of Violin Music . ninety page . Includin 

 tion Auber , Baillot , Beethoven , Boccherini , Boie Idieu 

 Cam ; li , Corelli , De Beriot , Ernst , Ghys , Handel , Haydn 1 

 London : ROBERT Coces s and Co , , 6 , New Burlington Streety 
 r 4 ww ort h of MU SIC for 5 

 Gounod , Smart , Hatton , Brinley Rit 
 Kuhe , Mendelssohn , Rubinstein , L amothe , Beethoven , & c 

 good Compo 
 song and piece , 
 A. this stle wood , 5 , Hunter ’ Ss L ane , Birmingh am 

 compose 

 VAN BEETHOVEN 

 glish version by NATALIA MaAcrarre 

