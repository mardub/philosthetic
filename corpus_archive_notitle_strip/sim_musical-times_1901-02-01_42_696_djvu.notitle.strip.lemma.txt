and 

 BEETHOVEN ’s 
 NINTH ( choral ) symphony 

 artist 

 Lent Half - term begin Monday , February 18 . entrance , Wednes- 
 day , February 13 , at 2 

 lecture by Walter Macfarren , Esq . , F.R.A.M. , on " Beethoven ’s 
 thirty - two Solo sonata , " every Wednesday until end of March 

 Organ recital , Monday , February 11 , at 3 

 sixth concert . 
 SATURDAY , FEBRUARY 4 g , ArT 3 

 Huldigungsmarsch re re Wagner . 
 symphony , no . 5 , in e minor .. Tschaikowsky . 
 Violin Concerto in D ; aa aca Brahms . 
 overture — ' the Taming of the Shrew " . Percy Pitt . 
 Voca.ist : Miss FLORENCE SCHMIDT . 
 Soto Viotin : Lady HALLE . 
 SEVENTH concert . 
 SATURDAY , MARCH 2 , aT 3 . 
 new Overture — " the Butterfly 's Ball " F. H. Cowen . 
 ( first performance . ) 
 symphony in C ( the " Jupiter " ) = Mozart . 
 Pianoforte concerto in b flat minor .. Tschaikowsky . 
 Verwandlungs Musik ( " Parsifal " ' ) Wagner . 
 voca.ist : Madame AMY SHERWIN . 
 Soto P1anoForTE : Signor BUSONI . 
 eighth concert . 
 SATURDAY , MARCH 16 , art 3 . 
 overture — " ' Iphigenia in Aulis " xs es Gluck . 
 symphony , no . 9 , in d minor ( the " Choral " ) .. Beethoven . 
 Violin Concerto in g minor(no.1 ) .. " 6 p .. Max Bruch . 
 serenade in e flat ( Op . 11 ) for Wind Instruments .. Richard Strauss . 
 Ride of the Valkyries os " a ee Pe Wagner . 
 Madame LILLIAN BLAUVELT . 
 Madame KIRKBY LUNN . 
 vocatistt 

 Mr. LLOYD CHANDOS . 
 Mr. DANIEL PRICE . 
 Soto Viotin : Madame VON STOSCH 

 d be iende 
 span ' Wiapenr , tthe gene empentall We - telr 4a | regard as a sister , and who warmly befriende 

 he . as director of the Cherubini Society , 
 which this lady have found for the propagation 
 of german classical music ( Beethoven , & c. ) , he 
 do his good to make Florence a musical place 
 from his german point of view , by institute 
 performance of german classical work , both 
 chamber and orchestral , and with a decide 
 amount of success 

 though he regard Florence as a temporary 

 music , of course , he could not appreciate , and 
 | inall direction do his good to germanise it by 

 bring forward german masterpiece , especi- 
 ally of Beethoven , which have not be hear 
 there before . to a great extent his effort 
 be crown with remarkable success . so 
 frequently be he on the move , that at this 
 time he habitually sign his letter to Madame 
 Laussot with ' Quicksilver , ' or one of its many 
 varied equivalent 

 during his wandering in Italy he be 
 assiduous in study the language of his 
 adopt country , and this he do with such 
 success that he not only learn to speak it 
 fluently , but also to correspond in it , as his 
 many letter to his old friend and pupil , 
 Giuseppi Buonamici ( all write in Italian ) , 
 sufficiently testify . great be his linguistic 
 acquirement . during his visit to England he 
 not only learn our language , even to the use 
 of ' slang ' term , so as to be able to speak it 
 fluently and correspond in it , but also study 
 Spanish . with French he have be familiar 
 from his early youth 

 the brother Stephen and George Job Elvey be 
 native of Canterbury and chorister in the Cathedral ; 
 and a wayfarer pass along Best Lane early in 
 January , 1760 , might have learn from the gossip that 
 Shrubsole , the blacksmith , have receive an addition 
 to his family — a son , who have since become widely 
 know as William Shrubsole , the composer of the 
 hymn tune ' Miles ’s Lane . ' but one ’s pleasant 
 retrospect of these bygone time be break by strain 
 of music which prelude the special service on this 
 first New Year ’s Day of the new century 

 the municipal and military authority of Canter- 
 bury co - operate with the Cathedral dignitary in 
 make the morning service ( at 11.30 a.m. ) worthy 
 of the city and the occasion . the Mayor and 
 Corporation attend in state and there be con- 
 tingent from cavalry and infantry regiment . the 
 local clergy muster in strong force , chief among 
 they be the venerable Primate ( who vigorously 
 preach an extempore sermon of thirty minute ’ 
 duration ) and Dean Farrar . as it be a Precum day , 
 the prayer be intone by a canon — Canon Mason — 
 instead of by a minor canon . the specially aug- 
 mente choir consist of about 140 singer , of whom 
 forty be lady . the band of the Royal Engineers 
 ( Chatham division ) furnish the player on brass 
 instrument — the corniter and sackbutter — which , 
 in combination with the organ , produce an excellent 
 accompaniment throughout the 
 service . the Te Deum and Benedictus , a typical 
 festal setting and quite modern in character , be 
 from the pen of Mr. H. C. Perrin , organist of 
 the cathedral , while the anthem — a setting of the 
 word ' o Lord , thy word endureth for ever in 
 heaven ' — have be specially compose for the 
 occasion by Sir Frederick Bridge , who conduct it 
 in person . after the Archbishop ’s sermon , a hymn , 
 write for the occasion by Dean Farrar , be sing to 
 a pleasing tune — also new — by Mr. Perrin , and the 
 voluntary , play by ' brass and organ , ' be select 
 from Beethoven and Gounod . with the exception of 

 the anthem , Mr. Perrin conduct throughout , and 

 97 

 hort duration , its introduction into opera and play save 
 Hit from oblivion . to give a few instance — Beethoven in 
 Fidelio , Weber in Der Freischiitz and Preciosa , Marschner 
 hin Der Vampyr and Hans Heiling , Mendelssohn in a 
 j Midsummer Night 's Dream , and Schumannin Manfred . but 
 ' shout the middle of the nineteenth century composer begin 
 f again to cultivate the melodramatic form independently . 
 it will suffice to mention here Schumann , Liszt , Grieg , 
 ‘ Mackenzie , and Richard Strauss . Mackenzie can not be 
 ‘ name without mention his felicitous utilisation of the 
 melodramatic form in his cantata , the dream of Fubal 

 the aim of the melodramatic treatment be to emphasise 
 what the word express , and to supply what they only 
 imply or hint at ; music be superior to speech in 
 emotional expression , and also able to add descriptive 
 ‘ touch . as a rule , the most important part of melo- 
 dramatic music be hear in the pause of the recitation , 
 during the speaking the music be either silent or subordinate , 
 consist oftenest only of a few sustained harmony . 
 the procedure of composer differ , however , in this 
 respect very considerably . to show how much they 
 sometimes differ , Massenet ’s opera Manon may be instance , 
 where the continuous music accompany the purely 
 conversational speak dialogue be neutral with regard to 
 expression , and serve only to bridge over the musical 
 silence that otherwise would arise between the lyrical and 
 dramatic portion of the work . this , however , be melo- 
 dramatic treatment only in form , not in spirit 

 setting of the 1ooth Psalm . eighteen year later ( I 
 1878 ) he adapt a portion of this exercise to forp 
 an independent eight - part motet , which be py } . 
 lishe by Messrs. Novello . 
 his degree of Doctor of Music , the examiner on thy 
 occasion be Sir F. A. Gore Ouseley , Dr. Corfe . 
 and Dr. Stainer — the last - name then a young ma 
 of twenty - seven . when , in 1876 , the University of 
 London decide to confer degree in Music , Dr , Pole , 
 at the request of the authority , draw up the Regul . 
 tion and become one of the first examiners.—(Vii , 
 the Musica Times , August , 1886 , p. 461 

 it be now time to turn to the literary side of Dr , 
 as far back as 1855 he write 
 analytical programme for the concert of the New 
 Philharmonic Society , among the work analyse 
 be the last seven symphony of Beethoven and , 
 new symphony by Gounod , then a ' new man ' in this 
 in 1857 he write , for the Crystal Palace 
 Company , an analysis of ' Judas Maccabzus ' and in 
 1879 he publish his ' the Philosophy of Music ' 
 he write eleven article for Sir George Grove ’ 
 ' Dictionary of Music and Musicians . ' 
 to the article entitle ' Musical Degrees in the 
 University of London ' ( already refer to ) , Dr. Pole 
 contribute to the MusicaL TIMES a paper on 
 ' Professional Musicians and Musical Amateurs ' 
 he write a very valuable and 
 interesting monograph on Mozart ’s ' requiem , ' which 
 originally appear in the column of this journal 

 in 1857 he proceed t 

 the story of the life of Wagner be in many way a 
 remarkable one . his uncle Adolf be a man of great 
 culture , and one who , like his famous nephew , ' ever 
 battle valiantly against the vulgar , bad , and superficial . ' 
 his father love poetry and the drama , and , occasionally , 
 take part himself in private theatrical , while Geyer , the 
 father - in - law , be both painter and actor . so that from his 
 early day the boy live under condition which seem 
 most favourable for the development of his latent genius . 
 Wagner be not quite five year old when his sister Rosalie 
 make her début on the stage , and she too , by the career 
 which she have embrace , and by her sisterly affection , 
 help in the same direction . young Richard go to 
 school at Dresden and be account good iz literis ; we 
 find he also especially attract by greek mythology and 
 history ; it be there , too , that he hear ' Der Freischittz , ' 
 and there that he see the composer conduct his opera , 
 and from that time forth music exert powerful influence 
 over hims 

 then he study Shakespeare , make acquaintance with 
 the glorious music of Beethoven , write an overture which 
 be perform under Dorn at the Leipzig Court Theatre , 
 other overture which be produce at the Gewandhaus , 
 Leipzig ; and , after some further attempt at instrumental 
 music , we find he at work — and before he be out of his 
 teen — on a first opera , ' Die Hochzeit . ' sister Rosalie 

 however , disapprove of the poem , and it be destroy 

 Ludwig van Beethovens Leben . von Alexander Wheelock 
 Thayer . Zweite Auflage . neu bearbeitet und erganzt von 
 Hermann Deiters . Erster Band 

 Berlin : W. Weber 

 Ir be now thirty - five year since the first volume appear 
 of Thayer ’s ' Life of Beethoven ' ; after six year follow 
 the second , and then after seven year the third , carry 
 the biography only down to the year 1816 . the author 
 live until 1897 , but business matter — he be United 
 States Consul at Trieste — the difficulty and delay in 
 collect material , and , finally , the infirmity of age pre . 
 vent Thayer from complete the opus magnum of his 
 own life . Dr. Deiters , fortunately , undertake to write the 
 fourth and last volume , make use , naturally , of all the 
 document , paper , and note leave by Thayer . he also 
 undertake to revise the three volume already issue , and the 
 first fruit of his labour of love be now before we . Thayer 
 himself have perceive the necessity of such revision , and 
 have already set to work on the first volume ; besides 
 correction and addition , he have actually re - write the 
 first chapter . it be always a task more or less difficult for 
 one man to complete what another have begin , but Dr. 
 Deiters enjoy special advantage . he be not only the 
 translator into German of the original english manuscript , 
 but from the beginning , in 1866 , he be in constant inter . 
 course with Thayer . how much the latter value his 
 counsel and trust he may be see from Thayer ’s letter 
 address to he in the first edition of the first volume , 
 and we may add that before his death the author express 
 the wish that his work should be revise and complete by 
 Dr. Deiters . in the preface to the volume under notice the 
 latter fully explain the kind of change which have be 
 make ; reader , however , will find most of the important 
 one in sign foot - note 

 the original text have be materially alter in one 
 respect . Theodor Fischer and Beethoven ’s father be 

 then come ' Die Feen’—never perform during Wagner 's | comrade in their youth , and , in after year , when both 
 lifetime , with the exception of the overture , which be | be marry , the Beethoven family live in Fischer ’s house 

 play at the Gewandhaus — and ' Das Liebesverbot , ' only | after the birth of the composer . caecilia ( 1762 - 1845 ) and 
 once perform , and miserably , at Magdeburg . in May , | 
 1836 , the composer be at Berlin , ' without the small | 
 certain prospect . ' he be expect to be deputy - conductor 

 but he be not ; 
 appointed , and in despair he go as capellmeister to | 
 Konigsberg , and next to Riga ina similar capacity . itwa | appendix to his first volume , evidently think they a | 
 in the latter town that he write down the text of ' Rienzi . ' | ! mixture of reality and romance 

 Gottfried ( 1780 - 1845 ) — son and daughter of Theodor 
 Fischer — remember much , talk much , and write much 
 about that family , when the Beethoven and the Fischer 
 child use to be constantly together . before they die 
 the two Fischers write down their reminiscence of 
 those olden day . Thayer , who publish they in an 

 Dr. Deiters , however , 
 having come to the conclusion that the writer be 
 thoroughly honest , and that although a certain allowance 
 must be make for the trick time play with the most honest 
 memory , their story be in the main true ; he have , in fact 

 whole Fischer text in an appendix , as in the first edition 

 though mark with initial ' ' H. D. " in bracket after the 
 heading , he have incorporate portion of it into the text . 
 there be , for instance , the charming description of the home 
 festivity on the anniversary of the birthday of Beethoven ’s 
 mother . and then , again , how interesting be the simple 

 statement of Gottfried , that ' in the house Rheinstrasse , 934 , | 
 Mozart be often the subject of conversation . ' ' the 

 the Recital Series of Transcriptions for the Organ 

 eleven number . ) by Edwin H. Lemare . 
 [ Novello and Company , Limited . ] 
 OverTuREs figure largely in this the late instalment ot 
 these popular organ arrangement . the ' Coriolan ' and 
 ' Prometheus ' of Beethoven , the ' Don Giovanni ' of 
 Mozart , the ' Stradella ' of Flotow , and last , but not least , 
 the ' in memoriam ' of Sullivan be all transcribe with 

 an ex 
 pleasi : 
 mainté 
 theme 
 panied 
 tranqu 
 name | 
 excelle 
 sO mas 

 by oUR YORKSHIRE CORRESPONDENT 

 the appointment of a successor to Sir Arthur Sullivan 
 in the conductorship of the Leeds Musical Festival have for 
 some time past occupy more mind than those of the 
 committee . as everyone be by this time aware , their 
 choice have fall upon Dr. Stanford . it be an unsolicited 
 honour , and we believe we be right in say that it be 
 confer without any of the contention which too often 
 accompany these matter . Dr. Stanford be , however , no 
 stranger to Leeds or its festival . accord to Messrs. 
 Spark and Bennett ’s ' history of the Leeds musical Fes- 
 tivals , ' he be first represent in the programme in 1883 , 
 when the beautiful song , ' there ’ a bower of rose , ' be 
 se by Miss Anna Williams . this be but a prelude 
 to more important thing . in 1886 ' the revenge ' be 
 produce at Leeds , in 1889 ' the voyage of Maeldune , ' 
 and in 1898 a Te Deum ; in each case for the first time . 
 in 1897 he form a close connection with the West 
 Riding town , be invite to take up the conductorship 
 of the Leeds Philharmonic Society , the premier choral 
 society in the town . under he the Society have give , in 
 addition to the usual stock oratorio , performance of 
 Brahms ’s ' german Requiem , ' Bach ’s ' Wachet auf , ' 
 Beethoven ’s great Mass in D and Choral Symphony , 
 the ' childhood of Christ , ' of Berlioz , the Prologue to 
 Boito ’s ' Mefistofele , ' and Dr. Stanford ’s own Requiem 
 and Elegiac Ode . during these three year and more he 
 have thoroughly gain the confidence of his chorus , and , 
 with the help of an excellent choirmaster in Mr. Fricker , 
 the Town Hall organist , he have materially advance the 
 tliciency of the Society , in addition to so influence the 
 programme as to make they by far the most interesting 
 of their kind in the West Riding . it be to be hope he will 
 be allow to exercise a similarly good influence on the 
 festival programme ; but the way of festival committee 
 ae peculiar , especially when the independence of the 
 Yorkshire character have to be take into account 

 so far only three work have be choose : Handel ’s 
 ' Messiah , ' Beethoven ’s Mass in D , and Bach ’s cantata 
 ' Wachet auf ' ( ' sleeper , wake ' ) . no new departure be 
 involved in any of these , for at Leeds the custom seems to 
 te establish of alternate ' the Messiah ' with ' Elijah ' 
 and Beethoven ’s Mass in D with Bach ’s in b minor 

 as regard work specially commission for the Festival , 
 the committee have be singularly unfortunate . Composi- 
 tion be invite from Dvorak , Sir A. C. Mackenzie , Mr. 
 Edward German , and Mr. Coleridge - Taylor . Dvorak , 
 perhaps recollect his experience of . Leeds in 1886 , do 
 not even reply to the request , but the other three consent . 
 some time ago Sir Alexander Mackenzie find himself 
 unable to complete the cantata he have begin , on the subject 
 of ' Balder , the Sun - God , ' and now Mr. Edward German , 
 who have begin work on a violin concerto , ask to be 
 excuse in order that he may undertake the more pressing 
 task of finish Sir Arthur Sullivan ’s last Savoy operetta . 
 so that only one of the quartet remain faithful , Mr. 
 Coleridge - Taylor , who be busily engage upon his cantata , 
 base on Longfellow ’s poem , ' the blind Girl of Castel 
 Cuillé . ' no doubt the committee will endeavour to supply 
 the deficiency , and we imagine there be plenty of young 
 composer , with unheard masterpiece in their portfolio , 
 quite ready to step into the breach 

 SAINT - SAENS ’S QUARTET in E MINOR 

 special interest be impart to the second appearance 
 of the party by the first production of a Quartet for string 
 in E minor ( Op . 112 ) by M. Saint - Saéns . this be dedicate 
 to M. Ysaye , and , it may be add , have manifestly be 
 write largely with regard to his exceptional ability . 
 the third and fourth movement belong to the class 
 commonly know as ' solo quartet , ' but it need scarcely 
 be say that this do not diminish their attractiveness to 
 an audience when such an artist as M. Ysaye be leader . 
 the first violin be also favour in the opening number , 
 but not excessively , and , although somewhat loose in 
 design , this portion hold the attention , chiefly by reason 
 of its poetically suggestive character . the second move- 
 ment be remarkably clever , and , be it add , interesting . 
 it be in Scherzo form , and include in the trio portion an 
 effective fughetta . M. Gabriel Fauré ’s refined Quartet in 
 G minor ( Op . 45 ) be also perform on this occasion , and 
 at the concert on the rgth ult . the ensemble work choose 
 be Beethoven ’s Quartet in G ( Op . 18 , no . 2 ) and Brahms ’s 
 Pianoforte Trio in C minor ( Op . ror ) . the solo pianist on 
 the two last - name occasion be Herr Schénberger , and 
 the vocalist have be Miss Louise Dale , Madame Lillian 
 Blauvelt , and Mr. Meux 

 LONDON and SUBURBAN concert , & c 

 amoncst the musical event of the past few week which 
 have attract general attention , the foremost place must 
 be accord to the performance of Herr Gustav Mahler ’s 
 First Symphony , by the Philharmonic Orchestra , of which 
 he be the conductor . already more than a twelvemonth have 
 elapse since the viennese public be make acquainted , 
 by the same excellent body of instrumentalist , aid bya 
 mix choir and solo vocalist ( for the score require all 
 these ) , with Mahler ’s Second Symphony — a work charac- 
 terise by somewhat unbridled fancy , a highly develop 
 sense of the poetic and picturesque , an amazing skill and 
 raffinement in the orchestration , but somewhat deficient 
 withal in original musical invention . in his early work , 
 recently introduce tous , the composer have content himself 
 with the employment of more modest mean . dispense 
 altogether with the choral element , the symphony exhibit , 
 in its purely orchestral movement , a deep pathos , 
 combine with great brilliancy of colour , while , on the 
 other hand , its positive musical content be even more 
 problematical than in the case of the later work 

 another noteworthy event be the performance — for the 
 first time in the city in which the master write they | — 
 of the complete cycle of Beethoven ’s symphony , in 
 consecutive order , by the very active new Concert - Verein . 
 these performance , which be not yet complete , be 
 under the conductorship of Herr Ferdinand Loewe , who 
 be prove himself eminently worthy of so interesting and 
 significant an occasion . the anniversary of Beethoven 's 
 death be signalise in a special manner by the Gesell- 
 schaft der Musikfreunde , who on that occasion announce 
 the result of a competition promote by that Institution ; 
 the prize of 2,000 crown be award , for a Symphony 
 in E major , to a hitherto quite unknown composer , Hert 
 Franz Schmidt , a member of the Opera orchestra . the 
 new work , the merit of which have be specially com- 
 mend by the jury , will doubtless ere long be produce in 

 ublic . 
 , a number of more or less interesting choral piece , some 
 with pianoforte accompaniment and other unaccompanied , 
 be produce at a concert of the Singakademie . amongst 
 those of more remote origin , two old french ' Brunettes , 
 the composer of which be unknown , attract the most 
 favourable attention ; while amongst the modern one , 
 the palm be give toa chorus for female voice , entitle 
 ' Windzauber , ' by Heinrich Rietsch , the newly appoint 
 professor of musical science at Prague University , and toa 

 amongst the virtuoso who have recently favour we 
 with their visit , the great popular success have be 
 achieve by Jan Kubelik , whose recital have be invariably 
 crowd by enthusiastic audierice . it be true that , from 
 a purely musical point of view , the young artist do not 
 reveal to we much that be new ; but genuine virtuosity , 
 such as his , will always exercise its great power of 
 attraction . Eugen Gura , the Munich ' Meistersanger , ' 
 give a recital , in which he interpret , in his superb 
 manner , a number of Loewe ’s Balladen , as well as song 
 by Hugo Wolf 

 some special interest attach to the visit pay we by 
 the young belgian pianist , Emile Bosquet . as the winner 
 of the Rubinstein prize , award in Vienna last summer , 
 the young artist have be invite to appear before a 
 large public here , in a chamber concert , and although , 
 in his interpretation of Beethoven ’s Concerto in E flat 
 major and participation in Schumann ’s Quintet , he can not 
 be say to have exhibit exceptional merit , he neverthe- 
 less prove himself an artist of much culture and refinement , 
 whose technique be irreproachable . another interesting and 
 sympathetic acquaintance we make in the person of a young 
 composer and pianist , Mr. Donald Tovey , of London , who , 
 in concert give by Fraulein Fillunger , be the unassum- 
 e but truly excellent accompanist . his introduction in 
 leading musical circle here have gain for he the reputa- 
 tion of a musician of more than ordinary attainment as 
 one who be gifted and already considerably advanced asa 
 composer and as an expert pianist . a characteristic and 
 very interesting pianoforte quartet , by another young and 
 promising composer , of czech nationality — Victor Novak 
 — attract the attention of connoisseur at its performance 
 by the Tonkiinstler - Verein , an association which render 
 good service in encourage young and , as yet , little know 
 composer 

 something of a sensation be produce by the début 
 here of a girl violinist , the twelve year old Steffi Geyer , of 
 Budapest . the picture of robust health and strong 
 physical development , her appearance be the very opposite 
 of that associate with the youthful prodigy . simple and 
 perfectly natural , too , be her playing , which be distinguish 
 by marvellous purity of tone , singularly mature power of 
 expression , and an unerring and , as it be , matter - of - fact 
 precision in surmount the great technical difficulty . 
 ararely and magnificently gifted child , truly 

 the Brodsky Quartet , from Manchester ( Messrs. 
 Brodsky , Briggs , Speelman , and Fuchs ) , perform at each 
 concert a number of the fine piece of chamber music , 
 and their fine tone and perfect ensemble playing be 
 worthy of their great reputation . a few song by Miss 
 McKisack give variety to the programme 

 at the first concert the work be : — Beethoven , string 
 Quartet in C ( op . 59 , no . 3 ) ; Bach , ' Ciaconna ' ( Mr. 
 Brodsky ) ; Schumann , Quintet ( op . 44 ) , Dr. Walker 
 ( pianoforte ) . at the second concert : — Mozart , String 
 Quartet in C ; Marcello , Sonata in F ( Mr. Fuchs ) ; Schubert , 
 variation from String Quartet in D minor ( ' Der Tod 
 und das Madchen ' ) ; Dvorak , Quintet ( op . 81 

 MUSIC in BIRMINGHAM . 
 ( from our own correspondent 

 from our own correspondent 

 the concert conclude with what must be consider 
 |as the good performance of Beethoven ’s Septet give for 
 many year in Dublin . the performer , in addition to 
 those already name , be Monsieur Grisard ( viola ) , Mr , 
 Conroy ( clarinet ) , Mr. Taylor ( bassoon ) , Mr. Morrissy ( horn ) , 
 and Mr. May ( bass ) . a very large and appreciative 
 audience attend and the success of the first meeting of 
 the Union augur well for its future 

 at the second concert , on the 18th ult . , Brahms ’s beay . 
 | tiful Pianoforte Quartet be play by Messrs. Wilhelm , 
 Grisard , Bast , and Esposito . it be avery fine performance , 
 Beethoven ’s Sonata for pianoforte and violin and Haydn 's 
 String Quartet in G minor be also play 

 the syllabus of the Feis Ceoil include some new feature 
 of interest : a prize for the good performance of any com . 
 bination of wind instrument , two competition for trio 
 ( pianoforte and string ) , a viola competition , and a special 
 prize for the good singing of an irish character song — 
 addition to the usual competition . the test for school 
 choir have be make simple and the sight test in the 
 vocal trio and quartet have beeneliminate . the adjudi . 
 cator be as follow : — Professor Ebenezer Prout , Mr , 
 Ivor Atkins , Mr. Denis O’Sullivan , Mr. Carl Fuchs , Mr , 
 Oscar Beringer , Mr. J. Ord Hume , Mr. Brendan Rogers , 
 Mr. R. Young , and Mr. P. J. M‘Call . 
 | the prize for the good work for soli , chorus , and orchestra 
 | offer by the Feis Ceoil Association have just be win by 
 | Carl Gilbert Hardebech , of Belfast , who have frequently 
 | win small prize at former Festivals . the work be very 
 | fully score , with solo for mezzo - soprano , tenor , and bass , 
 the title be ' the red Hand of Ulster , ' and treat of a 
 favourite irish legend . the work be to be perform at the 
 Festival next May 

 _ — _ — — _ — — _ 

 on the 
 performance 
 chief solo ' 
 evidence | 
 ain admis 
 in the hall . 
 representin 
 of Music , 
 17th ult . 
 ( no . 1 , op 
 Sharpe c¢ 
 Beethoven 
 audience tc 
 Miss Hele 
 act as de 

 Dr. Ric 
 enlightene 
 work as 1 
 with select 
 hitherto w 
 note must 
 Symphony 
 a decid 
 previous mr 
 of Bach fo 
 part . it 
 listen t 
 Fuchs , altl 
 work for 
 Lalo , have 
 incoherent 
 at the Hal 
 Lillian Ble 

 12!i 

 on the 16th ult . the Choral Union give a ' popular ' 
 performance of ' the Messiah , ' with Mr. Andrew Black as 
 chief soloist . the drawing power of the oratorio be 
 evidence by the fact that the number of people unable to 
 gain admission be almost as great as that accommodate 
 inthe hall . Messrs. Cole , Melville , Joachim , and Augless , 
 represent the string quartet of the Atheneum School 
 of Music , give an enjoyable Chamber concert on the 
 7th ult . the programme include Haydn ’s Quartet 
 ( no . 1 , Op . 74 ) and Beethoven ’s trio in e flat . Mr. J. W. 
 Sharpe contribute vocal solo . a performance of 
 Beethoven ’s Pastoral Symphony attract an enormous 
 audience to the Popular Orchestral concert on the rgth ult . 
 Miss Helen Jaxon be vocalist , and Mr. Maurice Sons 
 act as deputy - conductor 

 MUSIC IN MANCHESTER . 
 ( from our own correspondent 

 Dr. RICHTER have recently present we with yet more 
 enlightened and finished interpretation of such classical 
 work as we may never too fully understand , alternately 
 with selection from both old and more modern school 
 hitherto withhold from we . among the former especial 
 note must be make of the rendering of Beethoven ’s eighth 
 Symphony , on the 17th ult . , so admirably contrast with 
 the decidedly noisy Sinding Symphony in D minor of the 
 previous meeting and with the staid , sober orchestral suite 
 of Bach for string , oboe , drum , and with three trumpet 
 part . it be always gratifying to have an opportunity of 
 listen to the charming violoncello playing of Mr. Carl 
 Fuchs , although one ever regret the dearth of really great 
 work for his instrument . the concerto in D minor , of 
 Lalo , have many interesting passage ; although somewhat 
 incoherent and unsatisfactory as a whole . the vocalist 
 at the Hallé concert during January have be Madame 
 Lillian Blauvelt , Miss Brema , and Mr. Santley 

 as usual , Mr. Lane 's patron flock to his excellent and 

 SONDERSHAUSEN.—At a concert give by the Conserva- 
 torium , under the direction of Professor Schroeder , a new 
 composition for chorus and orchestra , ' the Lady of Castle 
 Windeck ' ( found upon Chamisso ’s ballad ) , by Rudolph 
 Werner , be receive with much favour . the same 
 concert include the performance of a ' hymn to life , ' for 
 chorus and orchestra , by the late Friedrich Nietzsche , a 
 somewhat ambitious , but distinctly amateurish production , 
 scarcely calculate to place the vaunted musicianship of 
 the famous poet - philosopher in a very favourable light 

 TEPLITz.—Commemorative tablet have be affix to 
 the house at Schénau , know by the sign of ' Zur Harfe ' 
 and ' Zur Eiche , ' in which Beethoven reside during the 
 summer month of 1811 and 1812 

 miscellaneous 

 the Society , give a most interesting analysis of musical 
 form , which he illustrate at the pianoforte by play 

 excerpt from the work of Handel , Bach , Beethoven , 
 and Mendelssohn . the vocal illustration be give in a 
 refined manner by Miss Beatrice May and a quartet of 
 male voice — Messrs. Oakeley , Turner , Bennetts , and 
 Stanley Smith . before the performance Mr. Gilbert give , 
 in concise and lucid language , a description of each class of 
 composition , which range from canon and fugue to the 
 sonata and song without word , and which include the 
 madrigal , the part - song , the glee , the early italian song , 
 the ballad , and the modern song 

 an exceedingly pleasant evening be spend on the 
 17th ult . , at the residence of Dr. W. G. McNaught , when 
 a private representation be give of an original operetta , 
 in two act , entitle ' Lucette , ' compose by Mr. William 
 McNaught . the libretto , by Mr. W. G. Rothery , be 
 smartly write and very much up - to - date , and the music 
 reveal in the youthful composer ( who , we understand , 
 be quite untrained in composition ) a considerable gift of 
 melody and distinct capacity in writing which should 
 surely induce he to far cultivate these gift . it 
 would be invidious to specially mention any of the 
 performer ; sufficeth it to say that all work together 
 with earnestness and spirit and that the result be highly 
 satisfactory 

 M. JuLes Barsigr , the distinguished opera librettist , 
 die on the 16th ult . , at the age of seventy - eight . ' Faust ' 
 alone will carry his name down to posterity , but he also write 
 the libretti of ' Roméo et Juliette , ' ' Noces de Jeannette , ' 
 ' Paul et Virginie , ' and many other opera book 

 the death occur , on December 26 , at St. Louis 
 ( U.S. ) , of Professor AuGust WatLpaueEr , founder and , 
 for many year , director of the Beethoven Conservatory in 
 that city , aged seventy - five 

 the death of Dr. W. Pote be refer to on another page 

 WINCANTON.—The Christmastide cantata , by Dr , Pearce , 
 be perform in the Parish Church , on the 15th ult . , 
 when Mr. Chuter ( organist of Sherborne Abbey ) accom- 
 panie , and Mr , E. Harold Melling , organist of the church , 
 conduct 

 answer to correspondent . 
 G. L.—(1 ) the passage in Beethoven 's Rondo for piano- 
 forte in C , Op . 51 , no . 1 , bar 24 , group 3 and 4 , may 

 be play : — 
 - Z _ -@- -@ jako , » ee a 
 -2p - ejpe0 tit tee 

 ORGAN . 
 the VILLAGE ORGANIST.—Boox 18 

 1 , Funeral March ( sonata , op.26 ) .. ae oe - . Beethoven 

 2 . Ditto ( sonata , Op . 35 ) .. we ee " « Chopin . 
 3 . dead March ( " ' Saul ’' ) si aes oa a Handel . 
 4 . Funeral March ( ' ' story of Sayid " ' ) wa . A.C. Mackenzie . 
 5 . Ditto ( ' ' Liederohne Worte,’’No.27 )   .. oe Mendelssohn 

 ORGAN MUSIC 

 the VILLAGE ORGANIST.”—Book 18 , contain Funeral 
 marche by Handel , Beethoven , Chopin , Mendelssohn , and 
 Mackenzie . 1s 

 BEETHOVEN.—Funeral March on the death of a hero ( Op . 26 ) , 
 arrange by W. T. Best ( Best , 83 ) , together with Schubert 's 
 March in b minor . 2s 

 Funeral March ( op . 26 ) , arrange by W. J. Westbrook 

 SCHUBERT . — Grand Funeral March , arrange by E. Silas . ts 

 March in b minor ( op . 27 , no . 1 ) , arrange by W. T. Best 
 ( Best , 83 ) , together with Beethoven 's Funeral March . 2s 

 Marche Solenne'le ( op . 40 , no . 5 ) , arrange by W. T. Best . 2s 

 ConTENTS.—Book ii . ( just PuBiisHED 

 Birthday March .. Schumann , 
 british Boys ’ March Richards , 
 Dessauer March .. ne ve ve ae 
 Gavotte .. oe ee ore a re « - Elvey . 
 Gavotte se ae ce os ee Handel . 
 Gipsy March . ' be a we + » Weber . 
 Hohenfriedberger " March ee ae 
 March from ' Judas Maccabeus " .. Handel . 
 March from " Le Nozze di " ane ne a Mozart . 
 ae arg al ais ' ea Schubert . 
 arseillaise , La mae - ae ' - Rouget de Lisle , 
 Merry Peasant , the .. ae at oe pe nny 
 turkish March . Beethoven . 
 Ye Mariners of England Pierson 

 wedding March Mendelssohn 

