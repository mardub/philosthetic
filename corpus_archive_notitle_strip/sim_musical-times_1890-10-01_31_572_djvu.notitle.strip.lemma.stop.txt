vocalist , Madame Valleria . V ioloncello , Herr Julius Klengel . | WEDNESDAY , THURSDAY , FRIDAY , SATURDAY , 
 Crystal Palace Orchestra . conductor , Mr. August Manns . Stal ! October 22 , 23 , 24 , 25 , 1890 . 
 ticket ( transferable ) Concerts , Guineas 

 pei 7 Wepnespay Morninc — REDEMPTION . Gounod . 
 CHELTENHAM MUSICAL FESTIVAL , | Wepsespay Evexisc — GRAND MISCE LLANEOUS CONCERT . 
 1890 Tuurspay Morninc — ELIJAH ae Mendelssohn . 
 2 | Fripay Morninc — JUDITH C. Hubert H. Parry . 
 president : Sir Hersert Oaketey , Mus . Doc | Zespay Evewno — Tle sy DEN LEGEND . + » Sullivan . 
 musical Director : Mr. J. A. MATTHEWS . | SaturDay Morninc — MESSIAH ase +2 Handel . 
 . owe - . principal performer . 
 TUESDAY EVENING , OcTOBER 28 . | 
 | Mac 3 
 creation ( 1 ) , HAYDN . | se naeicetes . 
 CONCERTO D ( Viottn OrcuEstRA ) , BEETHOVEN , Miss HILDA WILSON . Madame HOPE - GLENN . 
 STABAT MATER , DVORAK . | Mr. EDWARD LLOYD . 
 - | Mr. IVER — , Mr. ANDREW BLACK 

 VeEpDNESDAY evening , OCTOBER 29 . . WATKIN MILLS 
 PENTANCE NINEVEH , Professor J. F. BRIDGE Mr. B. PIERPOINT : Mr. MONTAGUE WORLOCK . 
 ( conduct composer 

 exist note , eventually utterly 

 lost . need far time old 
 Egypt illustrate vanishment . little 
 know effect produce instru- 
 mental music use ( ) elizabethan Masques ; 
 judge " Messiah " 
 sound like multitude oboe 
 bassoon Handel employ , compare 
 meagre provision string ; come later 
 time , Ries Czerny describe Beethoven extem- 
 poraneous music fine wonderful 
 commit paper — alas ! lose 
 . remembrance Handel , 
 Mendelssohn marvellous improvisation ? 
 music note — prick , forefather 
 term — soon forgotten lost 

 586 MUSICAL TIMES.—Ocroser 1 , 18go 

 ES.—OcroBer 1 , 18go . 591 

 BEETHOVEN - HOUSE society 
 BONN 

 apart interest attach 
 birthplace Beethoven , Bonn memorable 
 Beethoven Festivals hold 
 honour great master . , con- 
 ducte Liszt Spohr , place 1845 , 
 occasion unveiling monument 
 erect Beethoven memory . ' second , 
 conduct Ferdinand von Hiller , tollowe 1871 , 
 commemoration centenary Beethoven 
 birth . , primary object 
 augment fund raise 
 purchase house Beethoven bear , 
 establishment Beethoven Museum , 
 hold . Festivals 
 orchestral choral ; 
 devote exclusively master chamber music , 
 instrumental vocal . 
 unnecessary date speak detail , 
 point strange chance retrospective 

 account 1845 appear MusIcat 

 devise representative pro 

 gramme , array executive talent 
 presentation , success Festival , 
 artistic point view , assure . 
 satisfactory add financially equally 
 successful , sum 16,000 mark ( f{800 ) having 
 net fund . unique scope , 
 illustrate means important 
 3eethoven artistic achievement , 
 seldom figure Festival occasion , 
 cause unusual satisfaction 
 Germany , Beethoven chamber music 
 generally familiar , thank Mr. S. Arthur 
 Chappell ' popular " Concerts , England . 
 preliminary remark turn 
 main subject notice — Beethoven- 
 House Society , german appellation , 
 " Verein Beethoven - Haus . " till 

 news Beethoven death , place 
 Vienna March 26 , 1827 , reach Bonn 

 worthy burgher city fully realise loss 
 |they sustain , honour accrue 
 |to city birthplace great master . 
 | time feel thoroughly proud 
 [ townsman , , young man 
 | - , descendant obscure im- 
 | poverished family , - - thirty year 
 | migrate Vienna , 
 | famous musical world great 
 jlive composer . foresee lustre 
 | memory great aman shed city , 
 | soon begin investigate antecedent 

 search record house 
 |born . day ancient Greece , seven 
 | city contest honour birthplace 
 |Homer , inhabitant separate 
 | street Bonn , time 
 | Beethoven family reside , assert claim 
 |to Beethoven having bear midst . 

 long time house Rheingasse , 
 | mother die , believe birthplace , 
 |andatablet effect affixedtoit . subsequently 
 claim favour house Bonn- 
 | gasse , Beethoven family previously in- 
 |habite . 1838 , thereabout , question 
 | fiercely debate house 
 |the master veritable birthplace . length Mr. 
 A. W. Thayer , Beethoven enthusiastic 
 |indefatigable biographer , come rescue , 
 lafter diligently search municipal eccle 

 siastical record , conclusively prove favour 
 | house Bonngasse . tablet record fact 
 | accordingly affix occasion 
 | Centenary Festival 1571 

 , , spring 1859 , 
 | honorary presidency Dr. Joseph Joachim , 
 society establish Bonn purchase 
 |the house , preservation , 

 memorial great master light 
 init . Festival , ex- 
 { tensive loan collection Beethoven relic 
 bring , throw open 
 view , limited number visitor 
 ithe time . date immense progress 
 | work restore , far 
 | possible , exact condition bear period 
 | Beethoven childhood . 
 progress chiefly 
 |the laudable exertion enthusiasm Herr 
 | Wilhelm Kuppe , musician long resident Bonn , 
 | , energy perseverance 
 | display exercise art profession , 
 |has acquire competence , enable 
 | devote great time superintend 
 ithe work restoration progress , 
 admit undertaking honorary curator- 
 | ship house content . 
 | < recent visit , portion 
 | Beethoven family occupy , 
 open public August , 
 extreme interest . , , describe 
 double house , tolerably commodious 
 | house , face street , small 
 rear look small 
 garden . time Beethoven family 
 residence , ground floor 
 floor occupy Clasen , dealer lace , 
 owner house . Salomon , famous 
 connection Haydn , symphony 
 bring England , family , in- 
 habit second floor . Beethoven family 
 dwell small tenement 

 small room contain fully 
 large hold small collection Beethoven 
 relic Society acquire , 

 92 MUSICAL TIMES.—Ocvoser 1 , 1&go 

 present regard form 

 nucleus large collection , 
 opportunity arise fund permit , hope 
 acquire , accommodation 
 large room house 
 amply serve . Beethoven relic 
 costly difficult meet , long 
 time probably elapse promoter 
 scheme hope 
 possession complete Beethoven Museum 
 desire . collection , present 
 small , contain , way 
 original manuscript , bust , portrait , & c. , 
 fail interest student Beethoven ; 
 calculate rouse visitor 
 sympathy commiseration mean 
 sordid surrounding great composer early 
 youth . surely shed tear behold 
 wretched garret — small lean - attic roof — 
 Beethoven tohave bear . scarcely 
 affect sight ear - trumpet , manu- 
 facture Maelzel 1812 - 14 , account 
 deafness Beethoven use time 
 death . number , size 
 shape . add horror , 
 , metal band furnish , 
 purpose attach head 
 play pianoforte conduct . special 
 point interest allusion grand 
 pianoforte specially manufacture master 
 Graf Vienna . peculiarity consist fact 
 string note . 
 addition ordinary so- loud soft 
 pedal , pianissimo , echo , pedal , act | 
 damper string , for- 
 merly furnish resonator , long 
 agoremoved . noless interesting isthe quartet string 
 instrument ce lebrate Schuppanzigh 
 quartet party habit play . con- 
 sist violin Nicolaus Amati , 1690 ; vioiin 
 Jos . Guarnerius , fil . Andreas , Cremona , 1718 ; viola 
 Vicenzo Ruger detto il , Cremona , 1690 ; 
 violoncello Andreas Guarnerius , Cremona , 1675 , 
 bow , & c. ' violin bear 
 Beethoven seal neck ; 
 big b scratch , presum- 
 ably point pen - knife . easy 
 extend list curiosity , think 

 house content 

 pronounce opinion . 
 profession impostor , charlatan 

 correct , 
 Richter Concerts 
 Beethoven graciously admit com 

 dilettante , class charlatan | panionship Wagner 

 edify ungodly Yeovil ! jarrange , singer large 
 — — | proportion property 

 Le Ménestrel announce immediate issue | 
 Breitkopf Hartel unpublished work | example respect composer inten- 
 Beethoven — arrangement pianoforte | tion : — Vienna , lately , ' * La Juive " perform 
 Pianoforte Concerto e flat , , score , | serenade , duet , 
 movement Pianoforte Concerto d,/number omit , ballet repre- 
 presumably Beethoven know | sente pas de deux dance music 
 begun subsequent completion ! Halévy ! 
 " Emperor 

 BoLp journalist Buenos Ayres , write 
 Wuen Scharwenka " party gentleman " | opera company , declare feminine artist 
 enter Concert - room Brighton Beach , " inci- | t9 assemblage Junos Venuses , , 
 dentally considerable noise , " Anton Seidl , | unhappily , Lucrezias rare Mes- 
 conductor , look annoyed , , recognise | salina . hope writer " clear 
 pianist , forthwith play arrangement Chopin | * speed werd print . 
 " Funeral March ! ' ? american contemporary | 
 7 Ic 6 " 2gaee incide " 
 ail aeiiialals M. Maxime Lecomte draw bill lay- 
 ae . |e tax franc organ , pianoforte , 
 horror horror ’ head accumulate ! acertain } 44 harmonium . claim revenue 
 man Kuhmeyer , Presburg , invent | obtain , far possible , article luxury . 
 machine play like pianoforte , produce | 4 cynical commentator add " especially troim 
 sound violin , viola , violon- | instrument torture . " 
 cello , conceal body instrument . | 
 — ORTH TE : SECON SE RTE 6 g ce ci ) xs nucleus valu- 
 — able museum Dieppe , shape rich Louis 
 Ir difficult mistake " true inwardness " Quinze furniture , jewellery , paisngt , é . large library , 
 follow advertisement , recently appear | 2 collection autograph , MS . ' Soldiers 
 ae 2 : : y apr Chorus " Faust , " MS . march Mozart . 
 France : ' " * grand opera manuscript , un- Th Pre Rite gad ie pe ? 
 publish complete , orchestration , COMICON 5 VEER £ 4,000 . 
 work composer recently dead , sale secretly . 
 strictly confidential . address . A. M. , Poste Restante , 
 Marseilles . " chance 

 598 MUSICAL TIMES.—Ocroser 1 , 1890 

 torious execution Beethoven ' " ah , pertido ! " ' Mr. 
 Edward Elgar personal introduction romantic 
 Concert - Overture , entitle ' Froissart . " Mr. Elgar , 
 Worcester , , believe , resident 
 London , , hope , , opportunity , 
 expect mark . overture 
 course chivalric style , , , commend- 
 able try manner 
 expression . , surprise — 
 mark youth inexperience ; , 
 thought study , Mr. Elgar good work . 
 acquire great coherence idea , concise- 
 ness utterance — inevitable sign master , 
 attain extended arduous effort . 
 effort , doubt , Mr. Elgar trust , ' ' Froissart " ' 
 applaud — Prophet honour 
 country . second programme 
 Introduction act " * Lohengrin " ; Grieg 
 Suite ' " Peer Gynt , ' new - chorus , ' lo 
 morning , " compose Mr. C. Harford Lloyd . 
 interesting effective example pure vocal writing , 
 style combine extent science 
 ancient madrigal grace charm modern 
 - song . Mr. Lloyd counterpoint facile , 
 " voice " freely amid difficulty 
 , general etfect music high 
 degree pleasing . pende composer important 
 production , amateur glad example 
 taste skill ' morning . " piece capitally 
 sing Leeds people 

 Thursday morning bring novelty 
 Festival — Professor Bridge Oratorio ' ' Repentance 
 Nineveh ’' — hear come musician 
 stress work enjoyment ofrest . curiosity 
 natural . composer 
 scale , accomplish excite 
 expectation success . far expectation 
 met question different people 
 answer exactly , doubt 
 ' " * Nineveh " advance composer position 

 large standard composition programme 
 Mozart * requiem 

 . 
 contain ' * golden legend , " 
 " Creation . " , new 
 Cantata , occupy morning . ' evening Concerts 
 miscellaneous character , 
 important feature Mendelssohn Violin Concerto , 
 Beethoven Symphony , Parry " bl Pair 
 siren , " Stanford " Revenge , ' Mendelssohn ' Judge 
 , o God , " Sullivan overture , * \ [ n memoriam . " 
 ( hese certainly good Festival . 
 Mesdames Nordica , Damian , Macintyre : 
 Lloyd , Mills , Foli engage solcist ; 

 

 Tue local musical season bid fair singular 
 activity . look , , previous record 
 break , , case , concert - giver 
 find collision matter 
 date . example , Sarasate , Sir Charles Hallé 
 orchestra ( performance ) , Carl Rosa Opera 
 Company appeal week — large 
 order Glasgow amateur 

 forecast Choral Orchestral Concert scheme 
 month Musica . time , list 
 choral work Verdi ' ' requiem " add . 
 evening Dr. Hubert Parry 
 ' Ode St. Cecilia Day . " " orchestral programme , 
 carefully draw guidance Mr. August 
 Manns , find include Dvordk new Symphony , 
 . 4 , G ; Spohr Violin Concerto , no.g ; Mr. Fred . 
 Cliffe tone - picture , ' " ' cloud sunshine ' " ' ; Mr. Staven- 
 hagen new scena soprano orchestra , ' " Suleika " ' ; 
 Henselt Pianoforte Concerto ; selection Raff 
 ' " Ttalian ’' Suite , Saint - Saéns Ballet music 
 ' ¢ Ascanio , " Beethoven Symphonies , . 7 8 . 
 list solo violinist far strengthen 
 engagement Mr. Emil Sauret Herr Hans Wessely . 
 service Mr. Santley secure , 
 interest note great english baritone , 
 hear Concerts year , 
 appearance return 
 australian tour . date Guarantee Fund 
 £ 3,335 , £ 1,000 time year 

 thirty - seventh season City Hall Saturday 
 Evening Concerts commence 2oth ult . , 
 excellent company , include Mr. Ffrangcon Davies , 
 new baritone , appear . Mr. Airlie book , 
 understand , important engagement 
 popular - conduct Concerts , 
 line business small choral society 
 renew enterprise . 
 Mr. A. R. Gaul , good fortune merit , 
 fore Cantatas , 
 church choir having late sacred 
 work , ' * Virgins . " St. george’s- 
 - - Fields , Shamrock Street , John Street , Kelvin- 
 grove Street United Presbyterian Church Choirs . Mr. 
 Gaul perennial Cantata , " Holy City , " 
 find favour Strathbungo Parish Church Choir ; 
 Weber Mass G rehearse Pollock 
 Street U.P. Church Musical Association ; Mr. Louis N. 
 Parker * Silvia " fix Eglinton 
 Street Congregational Church Choir , Cath- 
 cart Musical Association ; Bridgeton Choral 
 Union produce season Handel ' ' Joshua 

 thought begin turn 
 winter Concert room , occupy 
 anticipation keen musical delight . rumour 
 expect cold month begin 
 { ly , hope excite novelty 
 reach long ear , excellent feasting 
 know love classical dainty . large St. 
 James Hall , elaborately decorate , Mr. 
 T. A. Barrett resume entertainment 
 season prove attractive , announce great 
 number engagement ought satisfy friend 
 ensure diminution vigour hitherto 
 characterise undertaking . Carl Rosa 
 Company occupy Prince Theatre , , 
 probably , ere leave , depart little boldly 
 old list familiar work . especially 
 welcome revival Goring Thomas ' Nadeshda , " ' 
 , encomium bestow , 
 strangely withhold late 

 Sir Charles Hallé announce , usual , 
 Thursday month open season , 
 , doubt , meet congratulation friend 
 return triumph Australia immediately 
 conduct hope successful 
 Festival Bristol . Concerts 
 enrich orchestral novelty exhibit 
 splendid orchestra undiminished force finish 
 performance doubt ; promise Dr. Parry 
 " Judith ' ? Brahms ' " ' requiem " Christmas 
 welcome ; revival Beethoven great 
 Mass D evidence able Choir 
 Director , Mr. R. H. Wilson , grasp rein firmly 
 hopefully . Choral Concert , November 6 , 
 " Judas " ; follow , course , 
 " Elijah , " customary performance ' 
 Messiah " December 18 19 

 open twentieth campaign Mr. de Jong 
 secure service Mr. George Grossmith , 
 crowd Free Trade Hall utmost capacity ; 
 winter Orchestral 
 Ballad Concerts , new precocity , Max 
 Hambourg , engage 

 week Worcester Festival , 
 thirty - annual Musical Festival Worcester , 
 Massachusetts , place , Conductorship 
 Mr. Carl Zerrahn . choral work perform 
 ' Israel Egypt " ( selection ) , ' ' Elijah , " ' * golden 
 Legend , " " Erl - King Daughter , ' J. C. D. Parker 
 " * 1\¢demption Hymn , " chorus " Lohengrin ' 
 * * Tannhauser . " ' principal instrumental composi 

 tion select Schumann Symphony E flat ( . 3 ) , 
 | Beethoven Seventh Symphony , Suite string 
 orchestra , Victor Herbert , Assistant Conductor 
 Festival ; Rubinstein ' ' Bal Costumé , " ' ' 
 Island Fantasy , " J. K. Paine 

 _ _ city dead season come end . 
 | exception comic opera 
 small theatre , music lover oblige spend 
 dog daysin hot New York Strauss 
 | Concerts immense Madison Square Garden Hall 
 | appease musical appetite . Strauss Concerts 
 | artistic success . band small 
 | large hall , play . perform 
 | satisfactorily Strauss waltz , 
 heard effectively Thomas 
 Concerts Lenox Lyceum season . ' Strauss 
 Concerts soon end , place popular 
 Conductor , Herr Anton Seidl , commence , 
 2oth inst . , series Orchestral Concerts , band 
 number 100 performer . " ' Seidl ’' Concerts 
 | shall soon regular Sunday night ' " ' Thomas " 
 Concerts Lenox Lyceum , Mr. Franko promise 
 lseries Orchestral Concerts light character . 
 | Concerts annually 
 | Boston Symphony Orchestra Philharmonic 
 Symphony Societies , amateur soon 
 plenty opportunity revel realm 
 favourite art 

 New York Music Hall , laying founda- 
 tion stone report letter , ready 
 occupancy January 1 , 1891 . handsome 
 building fashionable city , hold 
 wall separate hall accommodate 
 respectively 3,000 , 1,200 , 500 person , Hall 
 | separate lobby , ticket office , entrance . 
 | home Oratorio , Symphony , Philharmonic 
 | Societies 

 Tue Hornsey Rise Amateur Orchestral Society ( Con- 
 |ductor , Mr. Arthur Moody ) propose include 
 | work perform season , com- 
 | mence rst inst . , following work : symphony , 
 |Haydn . 2 , D ; Beethoven . 1 , C ; 
 Haydn ' " Military , " Haydn ' * Farewell ' ' ' Toy 
 | Symphony , " & c. ; overture , ' William Tell , " ' * Masa- 
 | niello , " ' " * Poet Peasant , ' ' * Semiramide , " & c. ; 
 lselection ' nautical Songs , "   ' Carmen 

 bohemian Girl , " " Pinafore , ' & c. ; ' * War March ot 

 end March 21 . Monday Concerts 
 commence instead half - past , 
 Saturday Concerts begin 

 fifth series Messrs. Hann Chamber Concerts 
 Brixton Hall , Brixton , 28th 
 inst . , November 18 December 9 . following 
 work , , perform : quartet 
 Schubert , Mozart , Haydn , Mendelssohn ; 
 trio b flat , Beethoven ( Op . 97 ) ; Quintet ( pianoforte 
 string ) , Dvorak , & c. vocal selection 
 Concert 

 S1cNnor LaGo announce opening opera season 
 Covent Garden 18th inst . opera likely 
 produce Ponchielli ' ' Gioconda , " Wagner 
 " ' Tannhauser , " Verdi ' Othello , " , probably , 
 Gluck ' ' Orfeo . " " , artist engage Madame 
 Albani , Miss Damian , Miss Fanny Moody ; Messrs. 
 Manners , Galassi , Giannini , Maurel 

 year ago prussian Government purchase | 
 fom Herr Paul de Witt , Leipzig , interesting | 
 collection antique musical instrument , | 
 delight connoisseur visit Berlin | 
 Museum . comparatively short time | 
 elapse , indefatigable collector succeed bring- 
 e second equally valuable collection 
 , exercise praiseworthy public spirit , | 
 likewise acquire authority | 
 museum question 

 highly interesting sale autograph eminent musi- 
 cians announce firm Liepmannssohn , 
 Berlin , place 13thinst . include important | 
 number Beethoven , Berlioz , Cherubini , Chopin , Men- | 
 delssohn , Meyerbeer , Schumann , Verdi , Wagner , C. M. von 

 Weber , 

 recent discovery egyptian flute thei 

 Great Composers — Wagner ee ee ee ae £ 
 | Advance , Chicago ! .. = ee e ee ee wa 59 
 Beethoven - House Society Bonn 591 
 | occasional note .. aa aa oe oe ae es 502 
 | fact , rumour , rem : " s £ g2 
 Worcester Musical Festival ae sof 
 Coming Festivals da ‘ ea - . 599 
 report Associated boa die Royal Royal 
 College Local : nation Music + . 599 
 | Dr. Mackenzie music " Ravenswood " .. oa ant 
 | Globe Theatre ax x “ 601 
 | Tonic Sol - fa Jubilee 1 601 

 strument world , | 
 construct Messrs. Hill Son , London , builder 

 18 , December 9 ( Tuesday ) , 1890 , 8 p.m. following work 

 perform : quartet , Schubert , Mend : ' Issohn , Mozart , Haydn ; 
 trio b flat , Beethoven ( op . 97 ) ; Quintet ( piz inoforte st rings ) , 
 a. Dvorak , & c 

 M USIC SCHOOL.—CHURCH ENGLAND 

 analysis form 
 display 

 BEETHOVEN 

 thirty - PIANOFORTE 

 VINCENT 

 study piece require examination : — 
 study . 
 BACH.—Invention , . 9 , f minor 
 CRAMER.—In , . 13 ; 
 CHOPIN.—Prelude F ‘ sharp , Op . 28 , . 13 
 CLEMENTI — B , Gradus ad Parnassum , . 28 
 CZERNY.- F , op . 299 , . 4 
 BERTINI . ine , op . 29 , . 2 
 CR . \ME R.—In c minor , . 4 . 
 MOSCHELES,.—In E , op . 70 , . 
 PIE CES . 
 BEETHOVEN.—Rondo C , op . 51 , . 1 sis oe " 308 
 MENDELSSOHN,—Lied ohne W orte , b minor os oa . 
 PARADIES.—Sonata F , . 5 - xe 
 BACH.—Pre 7 f ugue x minor ' : 
 BEETHOVEN.- Allegro Sonata fe flat , op . 31 
 SCHUMANN.—Novellette E , op . 21 , . 7 

 BEETHOVEN.—Quant e piu bello , variation 
 MOZART 

 Allegro maestoso Sonata minor .. 
 SCHUBERT.—Impromptu b flat , op . 142 , . 3 
 EXAMINATIONS NATIONAL SOCIETY 
 PROFESSIONAL MUSICIANS : — 
 SCARLATTI.—Harpsichord lesson d , . 16 .. ' 3 
 Harpsichord lesson e minor , . 18 2 
 Harpsichord Lesson D , . 21 . ee " « 2 
 Dr. C. — ty Vivace .. aie 4 4 
 Etude Melodieuse 4 
 " INCE NT , G. F.—Con Energia ae ne : 
 staccato e legato .. s ' ey iar 

 new foreign PUBLICATIONS.| 

 BECKER , ALBERT.—Auf Kaiser Friederich Tod ; funeral 
 March . Orchestra chorus . op . 60 : — 
 score ae vn ne ‘ a5 - o 
 BEETHOVEN , L. v — overture Ballet ' 
 Creatures Prometheus . " op . 43 . arrange 
 Pianofortes ( hand ) .. ed " 
 DANCLA , C.—Three short piece . Violin Pianoforte 

 op.177 . no.1 . L'Eglantine ; no.2 . La Violette ; . 3 

