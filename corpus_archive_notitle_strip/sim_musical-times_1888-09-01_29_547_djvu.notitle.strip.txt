Pupil of the late Signor Chiaromonte, Conservatoire de Bruxelles

and Mr. Visetti, Royal College of Music). } 2 2 INING (Princi 385 
For Singing Lessons, At Homes, Schools, &c., address, 1, Harewood Psi - — N ae Te reeee boner 
Street, Harewood Square, N.W. RLS AE UE emer bl Said ec ely 
= = al MR. W. H. BURGON (Bass). 
. M ISS ELLIOT RICHARDS (Soprano). Permanent address, 8, Marlboro’ Road, Bedford Park, W. 
For Oratorios, Concerts, &c., address, 9, Oakley Street, Northampton. | MR. C. D. COLLET (B 
MISS FANNIE SELLERS (Soprano). Teacher of Singing, 7, Coleridge ii ad, ’ aes Park, N. 
For Oratorios, Classical and Ballad Concerts, Crag Cottage, Knaresbro’, | ———————_—— MR R - E. JACK SON "(Bs ————______——_ 
ra ; ~E ER ers x \ N (bass) 
MISS MARION STEAD (Soprano). ' x | Accepts engagements for Oratorios, Concerts, Dinners, &c. For 
For Concerts, Oratorios, &c., address, care of W. S. Child, Esq., | terms and vacant dates address, Principal Bass, New College, “Oxford. d 
Hon. Sec, Musical ‘Society, York. Pr ress notices. | MR. HOWARD LEES (B - 
a > M Ik. 1H (bass). 
M ISS LILY M ARSHAL ? WARD (Soprano), For Oratorios, Concerts, &c., address, Delph, Manchester. 
MISS JESSIE MARSHALL-WARD (Cont ralto), | MR. T. E. MACKIE (Bass) 
80, Addison Street, Nottingham. | For Oratorios, Concerts, &c., address, Cheapside, Worksop, Notts. 
MADAME CLARA WEST (Soprano), le MR. FRANK MAY (Bass) 
MISS LOTTIE WEST (Contralto), | and the London Oratorio and Ballad Union, under his direction. 
Beethoven Villa, King Edward Road, Hackney. | For Oratorios, Concerts, &c., address, 14, Hanover Street, W

Telegraphic address, ‘‘ lolas, London.” 
MR. HENRY POPE (Bass), 
20, Bishop’s Road, W.; or Mr. W. B. Healey, 10a, Warwick Street, W. 
(Madame Adelaide Mullen). Mk. EGBERT ROBERTS (Bass), 
For Oratorio, Grand Opera, Ballads, &c., address, 47, Ladbroke Rd., W, 51, Pentonville Road, N

line is quite finished, and I am dying to begin: of 
course I sometimes almost despair of doing justice 
to this great tragic story. For it has now become 
intensely tragic, though free from bloodshed and the 
usual stage effects. I am quite excited over all the 
characters, which I have got to mould in music, and 
you will be so too.” Later: ‘‘ The text for the opera 
worries me. Becker brought me a specimen the 
other day, and I could see that the affair has not 
made much progress. I have a perfect horror of 
setting weak verses to music. I don’t want a great 
poet, but I must have sound language and sentiments. 
Well, I shall certainly not give up my beautiful plan, 
and I feel that I have got plenty of dramatic instinct. 
You will be surprised to hear some of my ensembles.” 
At this point, as regards the opera, the letters cease, 
and we can only add: Alas for human wishes, which 
are vanity

The letters to Clara Wieck throw light not only 
upon Schumann’s music, but also upon his person- 
ality. Here, for example, is a significant extract: 
“Your father calls me phlegmatic? ‘Carnaval ’ 
and phlegmatic! I’ sharp minor sonata and phleg- 
matic! Being in love with such a girl and phleg- 
matic! And you can listen calmly to all this? He 
says that I have written nothing in the Zeitung for 
six weeks. In the first place, that is not true: 
secondly, even if it were, how does he know what 
other work I have been doing? And, after all, where 
am I always to get something to write about? Up 
to the present time the Zeitung has had about eighty 
sheets of my own ideas, not counting the rest of my 
editorial work, besides which I have finished ten 
great compositions in two years, and they have cost 
me some heart’s blood. ‘To add to all this, I have 
given several hours’ hard study every day to Bach 
and Beethoven, and to my own work. . Iama 
young man of twenty-eight with a very active mind, 
and an artist to boot; yet for eight years I have not 
been out of Saxony, and have been sitting still, 
saving my money, without a thought of spending it 
on amusement, and quietly going my own way, as 
usual. And do you mean to say that all my industry 
and simplicity, and all that I have done, is quite lost 
upon your father?” With this spirited defence of 
himself against the charge of mental slowness and 
indolence may be bracketed Schumann’s apology, in 
anticipation of married life, for his shyness and 
reserve in society: “I like being among noble and 
high-born people, as long as they want nothing more 
of me than simple politeness. I certainly cannot be 
always flattering and bowing and scraping, and am 
quite ignorant of all tricks of manner. But where 
real artistic simplicity is tolerated, I feel quite com- 
fortable, and can express myself very fairly. I merely 
mean to say by all this that, by-and-by, it will 
be a real pleasure to me to escort you here, there, 
and everywhere, if you should wish me to. And you 
will manage all the rest, for I know perfectly well 
that you can behave like a princess if necessary.” 
There is another passage on the same subject: “I 
should like to confide a good deal more to you about 
myself and my character, and tell you how, some- 
times, people cannot make me out, and how I often 
accept signs of the deepest affection with coldness 
and reserve, and constantly offend and repulse the very 
people who have the kindest intentions towards me. 
I have so often wondered why this should be, and 
reproached myself with it; for, in myself, I feel the 
smallest kindness, and appreciate every look, and 
the faintest movement of the heart; and yet I am so 
often wanting in words and forms. But you well 
know how to treat me, and will forgive me, I am 
sure. For I have not got a bad heart, and love ail 
that is good and beautiful with my whole soul. Well

XUM

But there is the rub, 7 his place. If all amateurs 
felt that it was possible for them not to be in their 
place, artists would have no cause of complaint; 
but we cannot say that this is the case now. ‘* What 
is sport to you is death to me,” as the frog in the fable 
said to the stone-throwing boy; and I think amateurs 
are much in danger of forgetting that the pro- 
fessional musician has to live by his art. It is very 
pleasant to Miss So-and-So to be asked to sing at a 
concert in the Shire Hall in the cause of charity, and 
certainly very pleasant for the charity in question to 
receive a substantial sum from the proceeds of the said 
concert, but when we find that a concert by the local 
artists either has to be given up or is a failure in con- 
sequence, we feel that there is something not quite 
right somewhere. Of course, if an audience were 
attracted solely by the music they were to hear, 
the professional concert would draw the larger 
number, but unfortunately this is not the case. 
There are still a great many persons who like to hear 
music, and yet * whose notions of ” it, as we quoted 
before, ‘‘are so very uncertain that” they “do not 
know what it is” they “like,” and so they buy tickets 
to please their friends, and believe that Miss So-and- 
So 1s rather superior to Albani, and Mr. Dash to 
Joachim, because they themselves say so. Of course 
there are amateurs almost as much worth hearing as 
artists, and to these few artists are glad to listen, but 
they are and always must be few. Even if they have 
a musical organisation, other occupations leave too 
little time for sufficient study, and of those who have 
the leisure only a small percentage have been put in 
proper training early enough to attain that certainty 
which characterises the artist. In fact, nothing 
is more painful to the real lover of music than 
the ever-widening gap between his musical percep- 
tions and his execution. Ears take, as it were, no 
time to cultivate, and fingers and throats do—so as 
the years go by each note of music heard leads on 
the brain to greater understanding, while less and 
less time can be devoted to actual practice. When 
this stage has been reached, little pleasure can be 
gained from the performances of any but artists, and 
if amateurs all reached it they would give up in 
disgust producing themselves in public at all, except 
where they can be of essential service, and that is in 
the chorus or the band. 
cultivation than we have at present reached, and I 
think that idea is a comforting one to the professional 
musician. What a much more hopeful task it would 
be, for instance, to train young ladies to take their 
part in a symphony or concerto than to teach them 
pieces for drawing-room performance, an exertion 
which tends to no really artistic end, but too often 
ministers to the vanity of the executant, and tortures 
the tympanum of the listener. Were this the case, 
the amateur would not only provide the artist with 
bread, as he always must do if the artist is to exist, 
but with those opportunities of production which are 
the stimulus and nourishment of artistic life, as 
food is of the natural life. M.€.C

To be accused of being a “ purist” is scarcely, per- 
haps, usually considered a matter to rejoice over ; and 
yet when on hearing Beethoven's song ‘* Adelaida”’ 
sung by a baritone in G (having long felt it to be a 
tenor song in B flat) we ventured to remark that the 
character of the composition was destroyed, the term 
“purist ” was applied to us in derision—we remember 
to have felt it in the highest degree complimentary. 
There can be no doubt that musical conservatism— 
like all other conservatism—is only injurious to a 
cause when directed to the preservation of ideas 
which are protected solely by their age; but in the

This is a higher plane of

xpression of the sufferings and consolations of earthly 
life. A profound silence and stillness reigned till the last 
lingering chord had died away. The orchestra must be 
congratulated, together with Dr. J. C. Bridge, their Con- 
ductor, upon a performance which came not far short of

the highest standard. ‘* Engedi”’ followed, but the 
effect of Beethoven’s piece was sadly lessened by the 
use of a libretto adapted to accommodate English

punctiliousness as it once prevailed, instead of that 
to which the master his music. I have nothing 
to say against David in his proper place. The Shepherd- 
King is a fit personage for oratorio, but here he and his 
persecution by Saul are a poor substitute for the Saviour of 
the world and His sutterings. The performance of 
** Engedi ” was a fairly good one, especially with regard to

CHESTER MUSICAL FESTIVAL. 
(FROM OUR SPECIAL CORRESPONDENT

THE last day (Friday, July 27) of this Festival was, in 
more than one respect, the most satisfactory of the three, 
although the weather, which had before done nothing worse 
than threaten, declared open war and entered upon very 
active hostilities. Not to this cause, however, should be | 
attributed a rather scanty attendance at the Cathedral in | 
the morning, since it is a fact that there was a much 
larger gathering in the evening when rain was doing its | 
worst. Nor can it be said that the first programme of the | 
day wanted attractiveness. Beethoven’s ‘Mount of | 
Olives” (Engedi”) and Mendelssohn’s “ Hymn of | 
Praise”? are stock pieces at Festivals, both are assured

iit “went

552 : THE MUSICAL TIMES.—Sepremper 1, 1888

his case before a jury of experts. Shortly after the pub- | composer, however, that the name of Friedrich Wilhelm 
lication of this extensive work Mr. Chappell retired from | Jahns willbe moreimperishably associated—viz., Carl Maria 
active business, and henceforth devoted his whole time to his | von Weber. His analytical! and critical catalogue of the 
favourite pursuit. He was elected a Fellow of the Society of| entire works of that master, entitled ‘Carl Maria von 
Antiquaries, and became a member of the old City Com- | Weber in seinen Werken,” published in 1871, is a standard 
pany of Musicians, the Camden, and other learned | work, being at once an absolutely reliable authority upon 
societies. In these he did active work, reading occasional | the subject of which it treats, and a monument of loving, 
papers and advising as to the societies’ publications. | minute, and most painstaking research on the part of an 
He edited ** The Roxburghe Ballads, ‘*‘ The Crown Garland | enthusiastic specialist. A brief, but interesting sketch of 
of Golden Roses,” ‘‘The Dancing Master’; supplied | the life of Weber we also owe to the same pen. During 
notes to D’Urfey’s “Pills to purge Melancholy,” and | the last ten years of his life, the deceased musician was 
assisted in editing Bishop Percy’s manuscript collection | engaged upon collecting material for a supplementary 
of “ Ballads and Romances.” In connection with his | volume to his above-named most important work, which, 
studies on the subject of ancient Greek music, he wrote a | it is to be hoped, he has left in a sufficiently advanced state 
treatise, ‘‘On the use of the Greek Language, written | to render its publication possible. In his private character, 
Phonetically in the early Service Books of the Church of] to know the deceased musician was to love and esteem him 
England.” This speculative work occasioned a good deal | for the singular amiability of his nature and the healthy 
of controversy among scholars, and Chappell’s theories and | idealism with which he viewed his art. 
his dealings with the pueumes did not meet with universal| The death is announced of M. Isaac Srrauss, musician, 
acceptance. In 1873 he assisted in founding ‘The Musical}on the rith ult. Though born at Strasburg, in 1806, 
Association,” and was a constant attendant at the meetings | of Jewish parents, he early became known as a French 
of the Society, speaking and taking part in the discussions. | composer and as the director of the Court balls under the 
A remarkable paper he read in the Session 1877-8, ‘* Music | Second Empire. He first appeared at Paris in 1827 with 
a Science of Numbers,” showed that he had investigated | several Alsatian musicians, who acquired great popularity 
the acoustic as well as the strictly historical side of the | for their rendering of famous pieces of Haydn, Mozart, 
art. At this meeting he drew attention to an harmonium | Beethoven, &c. Strauss subsequently produced light 
constructed with two keyboards, one giving the tempered, | musical pieces, dances, &c., of his own, which were soon in 
and the other the harmonic scale. The last work to] great vogue in the salons and assemblies. For some 
which Chappell’s name is attached is “* The History of | time he occupied the post of first violin at the Théatre 
Music” (Art and Science), Vol. I., (Simpkin and Marshall). | Italien, and was then chosen chief of the orchestra and 
This volume displayed an immense deal of research | director of the grand musical fétes. During the summer 
and reading of the classics, and it was enriched with | season, when the theatre was closed, M. Strauss conducted 
several engravings and illustrations of the ancient scales, | the balls and concerts given at Aix-les-Bains, in the Savoy, 
The theory put forth as to the connection of Greek | All through the Second Empire he was chief of the Court 
music with modern art met with considerable opposi- | balls, and succeeded Musard in the direction of the masked 
tion, and a lengthy discussion took place in Concordia | balls given at the Grand Opéra. He was not related to the 
on account of the attack Chappell had made on Helm- | Strauss family of Vienna. 
holtz’s theories of vibrations and derived intervals. Mr. WILLIAM FULLERTON died suddenly on the 25th 
However, the book shows that its author possessed }ult. He was the only son of the well-known Judge Fuller- 
a large amount of knowledge over subjects ranging | ton, of New York, and had resided in London for many 
from the corruptions of the texts of the early Greek writers | years. He was the composer of the ‘‘ Lady of the Locket,” 
down to the latest discoveries in acoustics. The projected | produced at the Empire Theatre. He had also completed 
volumes on ‘“‘ Hebrew Music,” to be edited by Dr. Ginsburg, | another opera, ‘‘ Waldemar.” 
and ‘* Medizval Music,” by Dr. Rimbault, were never issued.| We regret to chronicle the death of Mr. A. J. Puasry, 
Incessant writing caused Mr. Chappell to have that dis- | which took place at Chester on the 17th ult. He played 
tressing malady known as “ writers’ palsy”; but, strange to | the Ophicleide in the Coldstream Guards’ band at the early 
say, after suffering from it for some years it left him, and | age of fifteen, and it was while there that he took up the 
he was able to write as of old. In his delightful country Saxhorn baritone of Courtois, remodelled and renamed it 
house at Weybridge he spent the latter part of his life, | the Euphonium, an instrument now so popular in our mili- 
passing many pleasant hours in his well-stocked library, a | tary bands. In 1862 he entered the Crystal Palace orches- 
student to the last. He was ever ready to advise and help | tra, and played there untillately. Mr. Phasey was a skilful 
those occupied in inquiries into the art he loved so well. | performer on all brass instruments, and has done much in

he writer of this notice will not readily forget the encou- | helping to form and encourage brass wind bands. 
ragement and many marks of kindness William Chappell 
extended to him in common with others engaged in investi- 
gations into the history of English music

Herne Bay.—An Organ Recital of pieces by Handel, Dubois, Smart, 
Mendelssohn, Chopin, and Salomé was given at the Parish Church by 
Mr. E. A. Cruttenden, Organist, on the 17th ult

Lerps.—The Organ Recitals by Dr. Spark, the borough organist, 
were brought to a close for the season on Saturday evening, July 28. 
Pieces by Bach, Mendelssohn, Smart, Beethoven, Ambroise 
Thomas, and Haydn were given, together with the organist’s well- 
known “Jubilee Fantasia on National Airs,” which appropriately 
closes with ‘God save the Queen.” During the past term the 
Recitals have been regularly attended by a larger number of persons 
than heretofore—showing their advance in popularity and their value 
in musically educating the teeming population of Leeds

Marcatr.—On Monday evening, the 2oth ult., a small but very 
appreciative audience welcomed the opening of Mr. Horace Cadogan’s 
Concert tourat the Foresters’ Hall. The programme included songs 
by Lassen, David, Tosti, M. Carmichael, &c., admirably rendered by 
Miss Agnes I.arkcom and Mr. George Aspinall; violin pieces by 
Master Felix Barowski, and some admirable recitations by Mr. Mark 
Ambient, the well-known Cambridge elocutionist and dramatic author. 
The Concert-giver set himself a heavy task in pianoforte music bs 
Handel, Mendelssohn, Liszt, Chopin, and Cadogan, gaining much 
applause by its successful issue

for C » The word ale 
encouragement of art and artist rCh Ta, ine wOores sere 
the following list of co rsit 
ancient ani modern composers RO B ye th

for the efforts of the Orpheus Club, 
treasures to the Sydney public :—Quin 
and Schumann’s hia 44, in E flat. 
Beethoven’s C minor, Caron in A m 
Beethoven's Op. in B flat; Beeth ower 
‘ Je), Mendel ohn’ 
r Fe. * vf 
‘Grie ’ 
tt wo pianos), Kubi: 
*s Sonata in D

t to

NY USIC. Half-price, Carriage-paid: Bach's * My

Spirit"; Schiitz’s ‘ Passion”; Third Part,“ re se of Sharon” 
Cherubini’s Fourth Mass ; Schumann's * Minstrel's Curse”; Rhein: 
berger’s ‘‘ Christophorus” (parts) ; Stanford’s “ Three Holy Children” 
(parts); Beethoven’s Choral Fantasia and Cantata, 1702. Part-songs: 
7 God save the Queen”; “Softly falls’ (Silas); ‘When all alone”; 
‘How sweet the moonlight”; ‘ Come, see what pleasure”; ‘It was a 
lover” ( Barnby); Shepherds’ Chorus, * Rosamunde”’; “ To take the 
air”; “‘Flow, O my tears,” ‘‘ Gipsy Life” (Schumann); Schein’s Part- 
Songs; “ Now i is the month ”: Brahms’s Part-songs

London: Couzarp, 14, Finboro’ Road, S.W

Marcia fantastica. Op. st. Arranged for Pianoforte Duet 
by A. Riedel... S oe Se 
BECKER, A.—Three Fugues. | “Op. ‘S40 For the Organ = B38

BEETHOVEN.—Eight Preces. Selected from P osthumous 
Works. Arranged for Violoncello and Pianoforte by 
J. Werner ah a oe ee 
— Two Bagatelles, and Piece in A minor. For Pianoforte

each I 0 
—— Allegretto, and “ Joy, Sorrow.” For Pianoforte ... ee. 
—— Piece in B, and Six Ecossaises. For Pianoforte ... +“ De 
— Waltz in E flat and D; Ecossaise in E flatand G. For 
Pianoforte pert Pe “ each 1: O 
— Allemande in A. For Pianoforte ne casi ae Aer Oar, 
—-. Six Allemandes. For Pianoforte and Violin... ae ee | 
— Two-part Fugue, For the Organ - r 6 
— Chorus from “Coronation Cantata.” Arrang ved for Men's 
Voices, with Pianoforte Accompaniment by K. Weinwurm 5 0

T. ANDERTON. 
YULE TIDE. 
THE NORMAN BARON

BEETHOVEN. 
PRAISE OF MUSIC 
J. F. BRIDGE

THE

