MONDAY popular concert 

 tue appearance of Madame Schumann at these Concerts , on the 
 { th ult . , be an event so interesting to all who can appreciate the 
 high class of pianoforte playing , that there can be little wonder 
 at the number of artist assemble on the occasion , or at the excite- 
 ment cause even amongst the constant frequenter of these enter- 
 tainment , by the novel sensation of listen to an executant 
 whose reading be not as familiar to they as that of the clergy- 
 man whom they * sit under " on the precede day . in truth , there 
 can be little doubt that fresh interest be create in an audience by 
 allow the work of the great composer to be interpret by 
 various artist . there be always a difference in the reading of 
 two pianist of an equal amount of intellectual power ; and 
 judgment in music , as in all other art , be strengthen by 
 comparison , 
 most intelligent live pianist have be long concede by all 
 whose opinion be worth record . for many year she have hold 
 the high rank in Germany ; and that this position have not be 
 derive from her alliance with the composer , Robert Schumann , be 
 prove by the fact that , as Madile . Clara Wieck , she be universally 
 recognise as one of the great artist of her day . perhaps no 
 one of the pianoforte sonata of Beethoven could have be so 
 adinirably calculate to display Madame Schumann 's varied power 
 of colouring on the instrument as the one she select on the occa- 
 sion of her first appearance this season . the sonata in D minor , 
 no . 2 , op . 29 , be certainly one require the most finished executive 
 power ; but it also demand an intellectual appreciation of its 
 meaning , and a dramatic faculty too rarely ally with mere facility 
 of execution . the alternation of Adugio and Allegro in the first 
 movement , with the occasional snatch of recitatire , can be readily 
 play precisely as Beethoven have write they ; but the power of 
 sympathize with the composer so as to re- produce his varied 

 hall : and certainly the rapidly increase coloured 

 glee , madrigal , or part - song , for four Voices ( S.A.T.B. , unless otherwise express ) . 
 ant 
 may 223 acanadian boat sone tg eee ) T.Moore | 220 hurrah tor merry Engiand F. Berger 

 d to 56 a Christmas madrigal ... .... T.Ions| 75 in good truth , when fondly love = | 18 , 20 & 21 sweet he n uckin g 
 i ior Adieu , ye stream ( A.T.T.B. ) ‘ Atterbury | Palestrina | ( SEATS ae . ; " Ww ilbye 
 pect , 117 Alittle song of thankfulness ( s.s.s . ) Parry | 418 in memoriam ( Tietjens ) J.L.Roeckel | 226 takecare .. . " Mrs , a B : arth olomew 
 vely " 187 all among the barley ... E. Stirling | 374 in the merry spring Ravenscroft | 254 tell I , where be 5 fat ncy breed ? do . 
 } < a , b19 a selection of five catch various 1 inthese delightful , pleas santgrovesPurcell 399 tell I , Flora « . « Ciro Pinsuti 
 Lave 9 amidst the myrtle ( s. A.T.T.B. ) Battishill ier { I see the moon rise clear A. R. Gau ! | , , _ ( the angel breathe on flower J. Naylor 
 it all 93 April be in my mistress ’ face T. Morley | " 2 " { oh , for the sword ... .... A. R. Gaul’| 2-/ ( | May Carol ... ... : Dr. Garrett 
 onter 308 a vintage song ( Loreley ) ( T.T.B.B. ) 51 Italy ( s. Soloand Chorus ) V. Novello , 122 the -B attl le of the : Bal tic Cc . a. M acirone 
 enter Mendelssohn | 493 ttis thle ws A. C. Mackenzie | } 115 the 4 Car 
 o the 78 Awake , olianlyre ... ... J. Danby | 422 it be aloverand hislass B. Luard Selby B. ru 
 ysread | 282 awake , my love , awake t H. Walker | 2 just like love ... Davy and V. Novello | 324 the cambrian plume Brinley Richards 
 406 a wreath for Christmas 430 King Winter ... ... Seymour Egerton | 212 The“Carnovale " ... ... ... Rossini 
 ople . Mrs. M. Bartholomew | 371 lady , see on every side Marenzio | 102 the Christmas fairy ... C. Goodban 
 only 38 breathe soft , ye wind ( s.s . B. ) W. Paxton | 378 Lake and W aterfall . E.H. Thorne | 49 thecloud - capt towers(aa . TT.BB ) Stevens 
 i 13 ° ] the Blue Bells of Scotland Neithardt 152 let we all to the field ( s.8.s . ) Ferrari | 428 the cloud that wrap ... .. S. Reay 
 lmire 191 breathe soft , ye wind .. S. Webbe | 208 lightly tread , onward creep 356 the Cuckoo sing ... G. A. Macfarren 
 ny of 68 Britons , strike home ... ... Purcell | ( zitti , zitti , STB . ) ... Rossini | , _ . § the dream } E. Stirl 
 ad 209 ) oh , be I but a drop of dew Cummings ( lightly tread , ‘ ti hallow’d | " 2 Red — | ie = - Stirling 
 Sail , § 360 by Celia ’s arbour ( a.1.1.b. ) W. Horsley | 230 }   ground(s.s . B. ) ... ... J. Scotland | 313 thedreamof home ... E. H. Thorne 
 1eard — 130 calm be the glassy ocean ( s. Solo | ( lineson the pleasu:«3 of music C. Stokes | 387 the f uir flower ... Sir R. P. Stewart 
 rus | and chorus ) ... we eos Mozart ; 359 loose the sail , rest the oar J. F. Bridge | 424 the Ferry Maiden ... ... A. R. Gaul 
 . & 83 catch on tobacco ee oe Dr , Aldrich | va { Lordly gallant ( s.s . B. ) ... Callcott 79 the Fisherman ( s.s.s . ) 
 ation | 134 Christmas ... ... ... G.A. Macfarren | + anda Round by ... .... T. Goodban | 109 the Flower greeting ( s.s.s . ) Curschmann 
 ough § 252 Chloe and Corinna ... ... E. J. Hopkins | 335 pes I little , love I ee H. Lahee | 210 the Forester ... E. Stirling 
 5 " § 363 Chorus of Houris ( s.s .. A. ) | 413 . Lullaby J. Barnby | 323 th he lark ’s aloft ( A ma uy Carol ) Walker 
 now | ( Paradise and the Peri ) Schumann ; gg Lutzow ’s Wild Chase ( t.7.2.p :) . Weber 427 e lark now leave J.G. Callcott 
 place 164 come again , sweet love J. Douland | 20 Maidens fair of Mantua ’s city 250 ne last night of the year A. Sullivan 
 . : 375 come , Dorothy , come , ici olkslied ; ( S.S.A.T.B. ) Gastoldi | 416 1e lover to his Mistress John Bennet 
 uke ' 53 come , followme ... .. V. Horsley ; 289 March of the Men of Harlech 106 he Lullaby Storace and V. Novello 
 the | 260 come , gentle zephyr ( a.1.7.8 . W. Horsley | 89 mark you her eye ( duet a. T. | 344 he Maiden of the " Fleur de lys " 
 Pole 4o come if you dare ( T. Solo and | and Chorus , A.A.T.T.B. ) Spof forth E. A. Sydenham 
 orey Chorus ) ... « .. Purcell | 128 mark the merry elf ( s.s . B. ) Callcott | 140 e May - fly ( S.S.B. ) ... « .. Dr. Callcott 
 read | 287 come , let we be merry ( Twelfth | 45 May Day Miller 97 the Nightingale ( s. S. A. ) T. Bateson 
 raise Night Song ) _ .. . L. de Pearsall } 48 Monodyon Mendel ssohn ( t.1 .B.B. ) | 317 he ting kiss ... E. A. Sydenham 
 are 263 come , let we jointhe saaiee W. Beale F. Schneider | 47 there lead treucitaletates s. B.)Dr . Callcott 
 say , 3 . Down ina flow’ry vale ... ... Festa | 79 music for Trebles or other equal voice | 83 the silver swan ( s. A.T.1.B. ) O. Gibbons 
 » the 3 * down ina flow'ry vale ( A.T.T - B. ) Festa | 111 my love be like the red rose Knyvett | 222 . the sleep of the flower B. Congreve 
 y 146 Dulce Domum ... ... we J. Reading | 199 now Autumn strew .. E , . Stirling | 232 Thesong af the Popp ! ies E. Stirling 
 od by 284 Eventide ... . ( C.Goodban | I now be the m oath « en 1304 the Owl ... - « we ey oes 
 have | 171 Fair Flora deck ( A.T - b. ) se Danby | 87 ( S : AT - EB ) « x Morley | 319 the Spring ... ... . - Henry Lahee 
 7 172 fair and noble lady htieneg cha- | law ake , s\ " Joh n Douland 305 the Swal llow ... Prince de Pol lignac 
 Ltious telaine ) ... ... oon Rossini | 218 now , onow , I need must part Douland | 382 . the three Chafers ( 1.T.8.B. ) H. Truhn 
 yoms , | 329 Fair Katie axe ae F. " W. Elliott | 21 now pray we for our country E. Flower | 412 . the Urchins ’ dance ... J. L. Hatton 
 both | 43 farewell ... ... ... German Volkslied | 22 now the bright — star | 302 the victor ’ return ... .- Mendelssohn 
 . 343 field flower ... . .. F. Stanislaus | ( S.S.A.T.B. ) Greville | 295 the welcome home ... ... R. Haking 
 alists © 239 five time by the taper ’s light S. Storace | 60 nymphsof the forest ( AT.T.B. ) W.Horsley | 57 the wait ... ... .. « Saville 
 nicht 34 Flora give I fair flower a { oon spring inall her glory J. Arkadelt | 397 the last wild rose . G. A. Macfarren 
 ; ( S.S.A.T.B. ) ose ss . eee eee = Wilbye | " 2 ( come , let we alla - maye go L. Atterbury | 415 the Wanderer ’s Night Song 
 their 400 Flow , omytear ... ... _ John Benet | 167 O’erdesert plain ... ... H. Walrent | S. von Wartensee 
 311 Forthenewyear ... ... Mendelssohn | 352 oh , the flowery month ofJune Jackson | 425 the be rach ely ) Sir J. Benedict 
 9 four round for three voice | 115 oh , the roast beef of old England | 132 this pleasant month of May ( a.t.t.b. } 
 " ( full fathom five(s . Soloand Cho . ) Purcell | 234 old May - day ooo cee 6SIF J. Benedict | Beale 
 124 j come untothese yellow sands(do)purcell | 307 once upon my cheek ... Dr. Callcott | 293 this world be alla fleeting show Waley 
 " * ) gently touch the warbling lyre 1300 omy luve ’s like ared , red rose Dr. Garrett | 71 thy voice , o harmony .. ... S. Webbe 
 ( ATP bs oss ° Geminiani | 13 onanny , wilt thou gang with I Harrison | 236 to fair Fidele ’s grassy tomb . ( a. T.T.B. ) 
 on ti€ — 339 ' gentle wind around her hover Emanuel | 315 Osingagain that simplesong Dr. Garrett T. F. Walmisley 
 ate the | 362 Gipsy chorus aieaueing ove Weber | 154 Partant pour la Syrie 197 toour next merry meeting ( A.T.T.B. ) 
 wonder | 411 Gipsy life ... .. é R. Schumann | 385 parting and meeting Mendelssohn H. Phillips 
 excite » | 436 glorious Apollo ( A.T.B. ) S. Webbe | 390 parting and meeting R. Schumann | 142 . to Rome ’s immortal leader . Mozart 
 enter | 31 Godsavethe Queen ’ . V. Novello 9 pleasure of Innocence ( s.s . b. ) Weber ] 7 ld , long life(3 equal voice ) Webbe 
 cutant — 73 go , faithless Clori ( p erfida 2 & 43 popular ode to Pope Pius IX.Rossini | 33 reast je oa Ma alkin 
 clergy- Clori ) ( s.s.s . ) . e Cherubini | 176 protect we , ye Powers ( A.B.B. ) Rossini | al urcell 
 1 , there ! 206 goodmorrow tomy lady bright Macirone | 351 raise again the bold refrain | 416 ' other morning very ‘ early ae , hibs : it , 
 nee by 196 goodnight , beloved(T.T.B.B. ) Dr. Monk | russian Melody King of Navarre 
 te by 367 good night , beioved ... C. Pinsuti | 365 requite dlove .. ww . « . F.Archer| gt Twelfth Night song ... ... V. Novello 
 e of | 348 good night , farewell Dr. G. Garrett | 30 rule , Britannia .. w+ V. Novello | 298 up , brother , up ( Christmas ) ] . B. Calkin 
 r ; and ! 383 goodnight ... ... ... Gustave Carulli ! 55 see the chariot at hand ... W. Horsley | 113 lasi via di qua ( Tickling Trio ) Martini 
 ie by 193 Great Bashaw — Al Bascia ... Mozart | 36 see the cor es x hero come Handel | 215 n \ , oatman ’s evening song 
 rst the ? 3 hail ! allhail ! thou merry month | 126 seeour oar ... ... Sir J. Stevenson B. . § S. Bach 
 by all of May ( S.S.B. ) ... ... « . Weber } 81 sighno more , lady ( s.s . A.T.B ) Stevens | 258 Ven etian bocteoan ’ sev vening ' songHatt n 
 be hold » 184 hail , blush goddess ... .. Paxton | 275 sigh no more , lady G. A. Macfarren | 291 wake thee , my dear Clara Gottschalk 
 not be 189 hail , hallowed fane ... ... Mornington | 73 since first I see your face T.Ford | 79 weel may the keel row ( s.s . b. ) we 
 ann , be 5 hail ! smile morn .. hia R. Spofforth | 350 sing lullaby Pe C. E. Horsley | 362 we happyshepherdswain J. _ ere 
 ersally 203 happy be our soldier band ( sella | 377 . Singon , with cheerful strain Mendelssohn 332 we watch her breathe L. Kerbu 
 aps no vita Militar ) ... ... .. Mozart | 364 silent night ose J. Barnby | 201 what mournful thought ( s.s . A.T.B. ) fons 
 een 80 7 hark ! thebonny ( Gequal voice ) Aldrich } 245 sleep , while the soft even ing 280 when the sunsinkstorest J. F. Bridge 
 powers ) 67 hark ! the lark . Dr. Cooke | breeze .. .. Sir H. R. Bishop | 14 & 15 when wind breathe soft 
 e occas 338 Hark ! hark ! the lark(s.s.c . ) E.H. Thorne | 278 snow - flake ... ... ... Arthur Cottam ( S.A.T.T.B. ) « es ee Webbe 
 minor , 267 Hark ! to the roll drum ( 3 16 soldier , brave and gallant be 138 who come so dark ? ( A.t . B.)_Callcott 
 ecutive voice ) .. .. « . Sir H. R. Bishop ( S.S4.0.B )   « .. + Gastoldi | 246 who be Sylvia ? « -   G. A. Macfarren 
 of its ) 150 Harvest home ... . « + Dr. Ions | 396 soldier ’ love ( 4.1.7 . B. ) ... ... Kiicken fleet ( Trebles ) J. Barnby 
 facility , 85 Hastethee , nymph ( Solo & Cho . ) Handel | 341 songtospring .. F rancesco Berger | 400 ther it be parting flush H. Lahee 
 he first ’ 186 hear , holy power ( s.s.7.T.B. ) Auber | 3 soon as I careless stray ; 7 / gentle evergreen ( 3 voice ) Hayes 
 readily ii here I in cool grot ... ... Mornington | 45 Spring ’s delight ( s. A.T.B. ) 62 gently whisper ( s. S. or T.T.B. ) 
 wer of here be a health unto his majesty | 145 spring 's delight ( T.T.B.B. ) ... Miller . Whittaker 
 vary eo ( S.G.B. } cor cee ace tne J. Saville | 216 spring - time ( T.7.B.B. ) ... Beethoven | 363 wreathe ye the step to great Allah ’s 
 he have how merrily we ‘ live ( T.T.B. } M. Este | 353 summer and Winter a B. Tours throne ( s. S.A.A. ) ... « .. R. Schumann 
 418 Holly berry ( Christmas Carol ) } 415 Summer eve ... ... J.L. Hatton | 389 ye little bird that sit and sing R. Mana 
 W.J. Westbrook | 95 summer isa- come i in ig 4 treble ) 404 ye maiden , haste ( s.s . A. ) ( Les 
 402 Home ... .. ... Sir Julius Benedict | 272 Sweetandlow ... .. ... J. Barnby Huguenots ) ... oe ase Meyerbeer 
 174 how gently the moonlight ... F. Paer 7 sweet enslaver ( Round ) - J. Atterbury | 64 ye spot snake .. Stevens 
 156 howsleepthe brave ... Dr. Cooke | 270 sweete flower , ye be too faire 242 Ye Shepherds tell me(trio ) J. Mazzinghi 
 79 huntsman ’s chorus ( s.5.5 .. ).- Weber | ( S.S.4.T.B. ) ... Dr. T. A. Walmisley 

 xum 

 choose , belong only to that order of genius of which Mendelssohn | Sherrington and Madame Sainton - Dolby ) and an el 

 be the brigut exanple , and to which Madam : Schumann , 
 although in a less degree , may fairly lay claim . 12 beautiful 
 Adagio , in b flat , be givea with a tenderness of feeling con- 
 traste most powerfully with the restless impetuosity of the last 
 movement ; which , whether suggest or not by the commonplace 
 circumstance of a horseman galloppe past the window of the com- 
 poser ( a3 relate by Schindler ) , be certainly one of the very good of 
 Beethoven 's rapid flzale . the clearness of Madame Schumann 's 
 playing be here wonderfully apparent , the design of the composer 
 never for one moment be lose in the whiriwind of passage 
 require the utmost digital dexterity ; and the spontaneous and 
 prolonged applause which burst forth from the audience at the 
 conclusion of the Sonata appeared bestow not oaly as a tribute 
 to the marvellous power of the executant , but as a thank - offer , 
 for aid her hearer in the endeavour to comprehend one of the 
 most poetical aad dramatic work of Beethoven . the two romance 
 of Schumann be exceedingly graceful specimen of his small 
 work . they be originally write for o0e , violin or clarionet ; 
 and perform as they be at this concert , for violin and pianoforte , 
 by Herr Joachim and Madame Schumann , could scarcely be hear 
 togreater advantage . they form two of a set of three ; and of those 
 select on this occasion we infinitely prefer the one in a major — 
 that in A minor be somewhat too broken and indistinct in desiga 
 fora work of this trifling nature . ' they be , however , essentially 
 " Schumannite " in character ; and overtlowe with those plain- 
 tively melodious phrase for which this composer be so remarkable . 
 the last piece in the programme be Beethoven 's Trio in e flat , 
 which play to perfection as it be by Madame Schumann , Herr 
 Joachim , and Signor Piatti , be so attractive as to keep the 
 majority of the audience in their seat to the very last note ; and 
 the applause with which Madame Schumann be greet on her 
 retirement from the orchestra must have convince she how 
 thoroughly those artistic quality , which have earn for she so 
 high a name in her native land , be fully appreciate by those 
 whose attendance at these concert — unquestionably the most 
 intellectual in London — have prove that their opinion be entitle 
 to respect 

 we have leave ourselves but small space to speak of the per- 
 formance of Spohr ’s Double Quartet in e minor — one of the most 
 exquisite of the composition of this class the composer have be- 
 queathe to we — but when we say that it be play by MM . 
 Joachim , Ries , H. Blagrove , Piatti , Pollitzer , Wiener , Zsrbini , and 
 Paque , there can be little doubt that the execution of the work be 
 little short of positive perfection . a word of praise too must be 
 award to Miss Edith Wynne , who sing Sullivan 's * * Orpheus with 
 his lute , " and Schubert 's * * Young Nun " so exceedingly well as to 
 receive an encore in both ; an honour , however , which she only 
 avail herself of in Sullivan 's song . in each of these composition 
 she be accompany by Mr. Benedict , with the retinement of style 
 to which he have now so thoroughly accustom the audience at 
 these concert 

 GENOA 

 it be with pleasure that we announce the complete success which 
 have crown Signor Lavagnino ’s experiment of give a series of 
 Classical Concerts here . at the time we write , four have already 
 take place ; afford opportunity for introduce to a genoese 
 audience quartett by Mozart , Beethoven , and Mendelssohn , trio 
 by Hummel and Mendelssohn , and a concerto by De Beriot ; the 
 first violin part in each be alternately execute by Signor 
 Lavagnino and Signor Macera , the viola by Signor Marenco , and 
 the violoncello by Signor Ratto 

 one of the chief grace that distinguish these concert be , that , in 
 order to promote the laudable desire show by Maestro Lavagnino 
 to establish a taste for sterling music , several distinguished 
 amateur have give their service in combination with those of the 
 professional executant engage for the occasion ; so that the piano- 
 forte part in Mendelssohn 's Trio , Hummel 's Trio , Mozart 's Quartett , 
 and De Beriot ’s Concerto , be perform by Miss Mercier . a lady 
 whose playing be of rare excellence , while Contessa Amelia Brauca- 
 leone sing the grand air from Meyerbeer ’s Profeta , and several other 
 vocal amateur take part in the perfurmance of Mozart 's 12th Mass , 
 which form the third concert of the series . this last - name musical 
 treat give such universal satisfaction , that Professor Lavagnino 
 have be prevail upon to promise arepetition of the Mass perform 
 ance in a still large space ; forthough the picture gallery at Villa 
 Novello be of considerable dimension , it be not find large enough 
 to contain the numerous audience that assemble to hear Mozart 's 
 no . 12 , a portion of whom have to overflow into adjoining room . 
 Cherubini 's lovely " * Ave Maria , " with clarionet obbligato , be the 
 offertory - piece select for the oceasion of the Mass performance ; 
 and Miss Sabilla Novello ’s purity of voice with devotional expression 
 be admirably second by the pure tone and as pure execution of 
 Signor Celsetti ’s clarionet 

 a Mornine Concert be give by the Blind 
 pupil at the School of the Society for teach the Blind to read , 
 Upper Avenue - road , Regent's - park , on the 12th ult . the programme 
 contain many piece by the great master , and the manner in 
 which they be perform reflect great credit on the pupil and 
 their instructor , Mr. E. Barnes . during the interval between the 
 part the pupil read from emboss type , and show considerable 
 improvement . the chair be take by the Rev. Henry Sharpe 

 Tue West Central Choral Society give a Concert , 
 on behalf of the Somer ’s Town Brill Mission Schools , at St. James 's 
 Hall , on the 15th ult . the principal announce be Miss E. 
 Robertson , Madame Sainton Dolby , Madame Patey Whytock , Miss 
 Ada Percival , Mr. R. Muir , Mr. Ralph Wilkinson , Mr. J. T. Beale , 
 and Mr. J. G. Patey , vocalist ; and Signor Piatti and Mons , Sainton 
 instrumentalist — the latter gentleman be , however , unable to 
 attend , owe to indisposition . Mr. G. H. Robinson be the 
 pianist , and Mr. Constantine be the conductor . the programme 
 consist of Beethoven ’s Mass in C , and a selection ; the Mass be 
 accompany by a small band . the second part - of the concert 
 appear to please the audience most , which might be attribute to 
 the fact of the solo singer and Signor Piatti , violoncellist , be 
 well know artist , who invariably give satisfaction . the attend- 
 ance be large 

 Aw Evening Concert be give at the St. James 's 
 National School - room , Hatcham , on Thursday , the 7th ult . , in aid 
 of the school fund , by the band of the Orpheus Amateur Musical 
 Society , assist by Miss P. H. Wilson , Messrs. Booth , Chabot , 
 Jones , and Willis . the programme contain some excellent 
 instrumental piece , the good perform be the overture to 
 Auber ’s Masaniello and Blancheteau ’s Cleopatra . Miss Wilson 's 
 singing of ' * happy be thy dream " and ' thy voice be near , " be 
 particularly effective . Mr. A. Booth , a gentleman with a good 
 tenor voice , acquit himself well in Wallace 's ' ' home of my 
 heart , " ( Lurline , ) and Mendelssohn ’s ' ' Garland . " Gounod ’s Vagareth 
 be remarkably well render by Mr. T. Willis , and deservedly 
 encore . the promoter of the concert , Mr. E. Chabot , favour his 
 friend with " ' if doughty deed , " by Sullivan ; and Mr. Stanley 
 Jones sing Henry Smart 's ' ' rhine Maiden , " with excellent taste 

 BrrmMincuamM.—On Monday , January 28th , the 
 organ in St. Thomas 's church , which have be re - build and enlarge 
 by Mr. Besward , be open by Mr. A. J. Sutton , the organist 
 the instrument be well balanced as regard the stop ; the tone be 
 rich , full , and mellow , and we believe that it give complete 
 satisfaction to the large congregation who attend the service 

 Botton . — the Philharmonic Society give its 
 second Concert for the season in the Temperance Hall , on Tuesday 
 evening , the 5th ult . the programme include Weber 's Overturt 
 Preciosa , a portion of one of Beethoven 's symphony , « c. , all 
 be well play by the band . the vocalist be Miss < Anni¢ 
 Cox , Miss Kate Wynne , and Mr. Patey . Mr. Ellis Roberts play 
 several solo on the harp with great effect . Mr. Staton conduct 

 Boston Spa , YorxsurrE.—On Monday evening 

 CanTerbury.—A very successful concert be give 
 on the 22nd January by Messrs. Cross and Gough , of the Cathedral 
 choir , assist in the vocal department by Madame Rudersdorff , 
 Mdlle . Anna Drasdil , Mr. Montem Smith , and Mr. Lawler . Ina 
 most enthusiastic notice of the entertainment from a local news- 
 paper , which have be forward to we , we be tell that the concert 
 giver have encounter an " antagonistic spirit of opposition from 
 some of the musical professor in Canterbury ; " and , if so , we be 
 additionally anxious that their effort to provide a really good 
 concert should be know , although we would willingly dispense with 
 the laudatory remark append to the name of each of the vocalist 
 inthe programme . Messrs. Cross and Gough appear to have be 
 exceedingly well receive in all their song ; and the concert give 
 the utmost satisfaction to a numerous audience . the vocal music 
 be accompany on the pianoforte by Signor Randegger , describe 
 in the programme as ' the talented conductor , pianist , and com- 
 poser 

 Cuirton.—On Thursday evening , January 31st , 
 the Septett Society give its second Concert for the season in the 
 Victoria room . the number of the performer be increase on 
 this occasion in order to perform Spohr ’s Nonett in F , the scherzo 
 of which be play so well as to be re - demand . Beethoven 's 
 Septett in FE flat , beautifully execute , conclude a most excellent 
 concert . the performer be — Messrs. H. and R. Blagrove , and 
 Messrs. Radcliffe , A. Nicholson , J. O. Brooke , Hutchins , C. Harper , 
 A. W. Waite , and : A , C. White 

 CockerMourH.—On Thursday evening , the 7th 
 ult . , a Concert be give in the Free Masons ’ Hall by Mr. Wales . 
 there be a large and fashionable audience . the vocalist be 
 Miss Maria Wilson , Messrs. David Lambert , Coverdale , Barton , and 
 Taylor . Miss Wilson sing the Scotch ballad , * within a mile of 
 Edinboro ’ town , " very sweetly ; it be warmly encore , when she 
 give ' ' home sweet home . " Mr. Lambert sing ' ' the Bell - ringer " 
 and " the Holy Friar , " and the audience be amuse with his irish 
 song , * * I be not myself at all . " the song allot to Mr. Barton be 
 " oh whisper what thou feelest " ' and ' * thou art so near and yet so 
 far , " the latter be re - demand . Mr. Wales play a Fantasia on 
 american air on the flute , and also a brilliant solo from Traviata . 
 Miss Wales preside at the pianoforte 

 Bell , and Mr. John Wilkinson ; aud Miss | 
 Wurtzburg play Bach 's Gigue in G major on the pianoforte . 
 Dr. Spark be unfortunately unable to preside , in consequence of | 
 a domestic bereavement , Mr. Bowling kindly undertake the office | 
 of conductor 

 LiverrooL.—The second subscription coucert for| 
 1867 of the Philharmonic Society be give on the 5th ult . , and | 
 go off with considerable spirit , though the more musical part of | 
 the audience have to complain of the absence of the usual Sinfonia , 
 for which they have doubtless a right to look , possess : the 
 society do , an orchestra so well qualified to interpret high class 
 composition . the principal instrumental feature of the evening 's 
 programme be Mendelssohn 's Pianoforte Concerto in G minor , 
 perfurme with the most consummate feeling by Madame Schumann , 
 who also give with great effect ber husband 's * ' Romanze in D 
 minor , " Hiller ' ’s ' Zur Guitarre , " and " Scherzo , " by Weber . | 
 Spohr ’s overture to " Der Berggeist , " and Beethoven 's first write | 
 to ' ' Leonora , " be play with great fire , as well as Meyerbeer 's 
 " Schiller - fest Marsch . " the principal vocal performer be Malle . | 
 Sinico , whose clever singing have make she a great favourite with | 
 this audience , and Signor Foli , who sing Mozart ’s ' * per questa | 
 bella mano , " and Handel 's * O ruddy than the cherry , " in very | 
 excellent style . the choral music be also well perform , the | 
 most remarkable work be Gounod ’s * * here by Babylon 's Wave 

 Maipstone.—On the evening of Tuesday , the 5th| 
 ult . , a performance in aid of the fund for a new organ be give at 
 the Lecture H : King - street , when a selection from Costa 's Lu 
 and Haydn 's first Mass be perform with orchestral accompani- | 
 ment . Mr. J. H. Steer conduct , and Mr. George Tolhurst pre- | 
 side at the harmonium 

 third edition . in paper cover , ls . , or whole cloth , 
 7 . WESTMINSTER ABBEY CHANT 

 BOOK , contain 189 single and double chant , by the 
 follow author : — Alcock , Aldrich , Attwood , ion m , Bach , Ba : 
 row , Battishill , Beethoven , Bellamy , Bentinck , Blow , Boyce , 
 Brownsmith , Chard , Dr. Cooke , R. ' voke , Corfe , Coward , Croft , 
 Crotch , Davy , Dupuis , Dyce , Ebdon , Elvey , Farrant , Felton , Fita 

 herbert , Flintoft , Foster , ' Gibbons , Goodson , Goss , Greene , Gres gory , 
 P. Hayes , W. Hayes , Henley , Hindle , Hine , Hopkins , Kelway , Kent , 
 King , Lamb , Langdon , Lawes , Lee , Lemon , Macfarren , Miller , 
 E. G. Monk , Morley , Mornington , Nares , Norris , Ouseley , H. Purcell 

 Morar 
 6 . ) |79 Andante from Sonata , op . i 

 4 , miserere pen whe - ove ay ove " Allegri . | 5 58 Adagio , Quartett , op . 2 , no . 
 6 . Jerusalem the golden e a os ove 59 Hallelujen . amen , from Judas ) 89 air — " but the Lord be ming , 
 6 . Qui tollis ( twelfth Mass ) ai ¥ r we Mozart . Maccabeeus [ dussek| . , ful . "   AMendelssohn 
 4 , Eia Mater ( Stabat Mate ) ais ‘ ee ie Rossini . | 60 Adagio , L'Invocation Sonata.| 81 Chorale — ' sleeper , awake ' 
 : + gw — " we eee eee » | 61 Andantino from Quartett , op.| 82 Marche Funéb eda on 
 . pro peccatis ee poss ioe sve 3 , no.1 . ( Pedal ' ra . ( reda 
 10 . Chorale from ' Faust " sie es 1 Gounod . | 0.1 , ( Pedal Obb . ) Haydn r ; my 
 11 , Veni , Sancte Spiritus ss se Jomelli . | Boox viii . | 88 Adagio movement . Rinck : 
 12 , Benedictus ( Messe Solennelle ) _ ove ove + + Gounod . | 62 Adagio from Quartett , op . 20 . Boox xi . a 
 13 . Domine Fili " ose eee o0e ovo 99 63 Aria . R. Schumann [ haydn| 84 Aria , La Serenade . Beethoum . 
 14 . Domine salvum fac , , .. sa = por I 64 Adagio from Quartett , op . 64 , 4 Andante , from Sonata . mor 
 15 . Ave Maria ... te ee Mozart . no.5 . Haydn 6 full Voluntary . a Hesse 42 , Ki 
 16 . the prayer in * Masaniello " ... for « » Auber . | 65 Andante . Adolph Hesse | $ 7 Andante , from First Con — — 
 17 , chorale from " the Hugenots " sis vee Luther . | 66 Andante from Quartett , Op . 17,| : M N 
 18 . ecce panis on ove nee ove Himmel . no . 3 . Haydn | 88 Aria . Haydn on 
 1 . Christmas hymn ... ove wee eee " Mendelssohn . 67 the Wedding March . ( pe-| 89 Andante from op.14 . M 85 , Po ' 
 20 . prayer ( Mosé in castiigias eos ane oe Rossini . dal Obb . ) Afendelssohn . | 90 Air—‘In native worth " — f ; 
 21 . LaCarita ... ra sia ve Prag ; mm _ — wr N 
 22 . Sancta Maria ... eke ai « . Mozart . Boox ix . | x xil . f 
 23 . how lovely be the messenger sve ia _ — — 7 willin ; 
 24 , Jerusalem " | 69 ee ae got in| ae oy trom ete distri 
 GRD by B flat . Haydn | 92 " blessing and honour , " f ~-ceeaal 
 EDWARD t r MBAULT . | 70 et incarnatus est . Dini | the Last Judgment . Spohr ‘ 
 71 Andantefrom Quartett , Op . 74 , | | 98 Siciliano , op . 45 . Reissiger ’ 1 
 price 1 . 6d . each . no . 2 . Haydn | 94 Air Antique . F. Couperin afl 
 - — — --- — — | 72 Te Ergo , from a Te Deum . : = aovel 
 a new work for the ORGAN . A. Romberg | 96 Aria Aug. Freyer O ! 
 3 mt ) | 73 fugue . J. £ . Eberlin | 97 Air — " Jerusalem . "   ( Peiqe 
 the ORGANIST?S PORTFOLIO , | % AsseiotromQuartett , op 64 | | . bb . ) Mendetootn [ Beall expens 
 . 0 . > , \ ¢ 
 a selection of open and conclude voluntary by the most - , saci am a 
 pec em composer , ancient and modern . adapt chiefly for the | office , | 
 use of amateur , by Dr RuweavLt . each number contain from seven | ntent 
 to twelve favourite voluntary , include one arrange expressly | " - Venn , ts ox BOR 0 ] 
 with pedal obbligato . no . 1 to 18 be now ready , price 3 , each ; or Boox xiii . | book xvi . . 
 in three volume , each contain six number , and bind in cloth , | | 122 Aria . Haydn — 
 price 12 . each . a thematic list of content may be have on appli- | 99 Prelude . J. G. % | i 
 cation to Chappell and Co. , 50 , New Bond - street . " ; 100 romance , ieaatiee a 14 . ) | te Sutt Voluntary Sy 4 phe 
 content of first volume , in se book . | Haydn| 125 Soft Voluntary . bo musica 
 boor L } - es pe ~~ " Beethoven 126 Aria . Spohr I 
 125 Adagio a Rinck | astoral Air . Geminiant 1 . 
 1 slow movement . Mozart | 96 — — ' movement . ' Mocart | 103 Andante , Septuor , Op . 74 pd - _ e , o — 
 : a Fe by Spohr 127 prelude . Anonymous | Hummel\ 199 Aria . ia Cathed . 
 ow melody . | Mendelssohn | 98 Dona Nobis from the3rd Mass. | 104 Aria . Battishill |130 " how lovely be th per ann 
 4 Gloria from a Mass in B fiat . | Rummel | 105 Adagio , Quartet , Op . 50 | senger . " " Pedal 0 educati 
 5 Benedictus , Mase N _ — 29 Slow Movement . Spohr Haydn } Mende the Rev 
 : ene " mg eat 0.1 . we 7/30 Andante Movement . Ceerny | 106 ' let their celestial concert . " ; I be cons 
 .ndantino , from Sonata . Dussek 3 ) Andante from a Quartet . | ( Pedal Obb . ) Handel ) Boox XVII . — _ 
 7 prelude . C. F. Gluthmann | 39 Aria . Spohr [ Haydn | 1131 a kt 
 8 ye Movement from the 7th ) 33 ' the Hallelujah Chorus . ( Pedal ndante , Sonate , of Vv 
 ymphony . Haydn | 7 ) 
 © Gloria la Excsisis from the 1st ! Obb . ) — — . | Boor xiv . 132 o jesu fili redemptor . _ — 
 Mass. ( Pedal Obb . ) Haydn/ ook V. 107 Aria , A. Hesse — — 
 [ 34 Aria , Spohr 108 Aria , from op . 18 . Beethoven| 1 ° 8 Adagio , Sestet , op . 81 . LA 
 Boox ii . | 35 Pastorale . Zipoli 109 slow March . Steibelt 1 A ! 
 rey es | 36 prelude . J. G. Hereog 110 Aria . Mendelssohn 34 Aria , Quaret , op . 10 . to Trai 
 . | 87 introductionand fugue . Rinek | 111 " fix in his everlasting ) 13 > Avia . 4 . Hesse 
 11 Blessedarethe depart . Spohr ! 33 Adagio movement . Rinck | seat " Hand ne ) 186 Aria . J. @. Horsog moe wo 
 12 full piece . Rinck | 89 Aria from P.F. Sonata , Mozart | 112 solemn March . Dr. B 187 prelude Rinck sm 
 13 slow movement from the Ist| 49 Arietta . Haydn 113 i yr . Boyee 1138 Adagio from clarinet Parsona 
 symphony . Haydn | 41 Slow movement . Beethoven mpromptu , op . 142 . ( pedal certo . ( Pedal0 HO 
 14 air semplice . Pleyel | 42 Slow movement . Beethoven eS M 
 15 holy ! holy ! Handel | 43 fugue . J. £ . Boerlin of Warr 
 16 air . Spohr | 44 Agnus Dei from the 4th Mass Boor XV . paiectigg 1 a 
 17 Slow movement from Sonata,| ( Pedal Obb . ) Mozart vase 139 Aria , Cantabile . Beethovs emma 
 op.2 . ( Pedal Obb . ) Beethoven " | 140 Adagio from the Lobgesag Church 
 4s pena weet 114 Aria , Quartett , op.77 . Haydn| wish | 
 Boox iii . | 46 e arch in Saul . Handel | 115 " thou shalt bring they in . ”| 141 Aria . Kalkbrenner | 
 Ariafroma Quartett . Buthoven Israel in Egypt . Handel | 142 benedi aad 
 18 slow movement . J. G. Meister 47 prelude . J. S. Bach 116 Andante Beene ! ic enedictus , Mass no . 1 . 
 19 Amplius lava me . Sarti 148 Aria . Haydn 117 Larghetto , syn i ny in D as Al 
 20 the Marvellous Works . Haydn| 49 Allegro Movement . Rinck a an SS 2 a pee f 
 21 Andante Movement . | 50 Slow movement . 118 prelude . A. Hess wihooen See Frease . J. 5 . Bieri ham , 8 
 Sendilaoks LWOOS | de . A. Hesse ; | 145 full Voluntary . A. Hess Sa 
 ‘ lauerbrey | 119 Adagio Movement . Rinck | 146 Ave Mari chiefly . 
 22 Agnus Dei . Mozart | 51 Slow Movement . W. Volckmar | 120 Aria Varied . ( pe ery & moder 
 23 Slow Movement . Bodenschatz| 52 Slow Movement . Haydn ‘ ba ee ee ee oe coe employm 
 24 Gloria in Excelsis . ( Pedal/53 Ave Verum . Mozart [ a. a | | 121 Soft Voluntary , A. 6 ee | he ey ee Tarbutt , 
 Obb . ) Mozart | 54 Full Voluntary . ( Pedal Obb . ) ! a oe , | Obb . ) a 
 ( RG 
 e 50 , NEW BOND-8TREET . LONDON . uo 
 rs , t 

 publish by Novurro & Co. , 69 , Dean - street , Soho , and 35 , Poultry . sell alse by Kant and Co. , Paternoster Row 

