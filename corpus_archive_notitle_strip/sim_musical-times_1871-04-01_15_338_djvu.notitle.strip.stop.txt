AUBER FRA DIAVOLO 

 BEETHOVEN FIDELIO 

 London : Novello , Ewer Co 

 SUBSCRIPTION CONCERT 
 FRIDAY , 65 

 BEETHOVEN MASS D 
 CHORAL SYMPHONY . 
 MR . BARNBY 

 Doors open 7 , commence 8 o’clock 

 Crampton . Price 3d . Post - free Pitman , 20 , Paternoster - row 

 bas DU PRINTEMPS , Wituram Was- 
 seRzUG . Saloon Waltz Pianoforte , price 4s . ; post- 
 free , 25 stamps . Augener , & c. , Beethoven House , Newgate- “ street , 
 Composer , 14 , Maryland - road , St. Peter’s - park , W. Mr. 
 Wasserzug . pupil Leipzig Conservatoire , continues 
 instruction Pianoforte Harmony 

 IRANA NEW SONGS 5.4.7.3 

 CATHEDRAL GEMS , LOUIS DUPUIS . 
 NANTES.—Fantasia , subjects Mozart 12th Mass 
 WURMS.—Fantasia , subjects Weber Mass G 
 ROTTERDAM . cannery subjects Haydn 3rd 

 Imperial ) Mass 
 MALAGA.—Fantasia , ‘ subjects Beethoven ’ Mass 

 nC 
 ROUEN . — Fantasia , “ subjects Gounod “ Messe 
 Solennelle « 
 CAEN.—Fantasia , subjects Rossini Stabat Mater 
 MAYENCE.—Fantasia , ne Haydn 1st 
 Mass , B flat 
 COLOGNE . — Fantasia , ‘ subjects . ‘ Mozart 1st 
 Mass , C oe 
 9 . PARIS . — Fantasia , subjects Mendelssohn ’ 3 ; Hymn 
 Praise 
 10 . LONDON . — Fantasia , ’ subjects Handel Messiah 
 11 . NORWICH.—Fantasia , subjects Haydn Creation 
 12 . CANTERBURY . ( savin saison Spohr 
 pants Hart .. oo cco oe 
 Beautifully illustrated Covure , views celebrated Cathedrals . 
 “ pieces , like Felix Gantier , reviewed time 
 ago Musical Times , especially intended young players , 
 , think , found equally acceptable teachers 
 pupils . written form little Fantasias , com- 
 mencing short introduction . illustrations , printed 
 colours , exceedingly beautiful . ”—Musical Times , Feb. , 1870 

 PHILHARMONIC SOCIETY 

 fiftv - ninth season Society commenced St , 
 James Hall 8th ult . , special interest given 
 concert presence M. Gounod , 
 accepted invitation conduct pieces , 
 exhibition fine bust Beethoven , 
 gracefully presented Society lady 
 Pesth , acknowledgment intimate connection 
 great master Institution , 
 artistic career , days 
 illness , help spontaneously offered ten- 
 derness delicacy sank deeply heart 
 dying composer , reflected enduring honour 
 donors 

 reception awarded M. Gounod con- 
 vinced high estimation talents 
 held country .. applause bestowed 
 symphony D , opened concert , , 
 , accepted mere complimentary welcome . 
 early work , beauties , 
 written clear defined purpose 

 baritone vocalist attempts 
 placed disadvantage . Scena Cava 

 tina , “ Far greater lowly state , ” “ Irene , ” 
 work M. Gounod included 
 programme , given Miss Edith 
 Wynne elicit warmest applause . 
 request lady presented bust Beethoven , 
 composer symphony C minor performed — 
 hardly heard 

 express 

 MUSICAL TIMES.—Aprin 1 , 1871 . 45 

 occasions — Herr Joachim played Mendelssohn 
 Violin Concerto perfection tone , executive 
 power , intellectual appreciation author mean- 
 ing place head living violinists . 
 Mr. G. A. Macfarren highly dramatic Scena , “ child 
 fled , ” Opera “ Robin Hood , ” sung 
 good effect Mr. Santley ; concert conciuded 
 Weber Overture , “ ‘ Ruler Spirits . ” Mr. 
 W .. G. Cusins , conductor Society , warmly 
 welcomed entrance orchestra . ‘ per- 
 formance attended Princess Wales , 
 Princess Christian , Prince Arthur . second 
 concert , took place 22nd ult . , performance 
 commenced Mendelssohn “ Reformation Sym- 
 phony , ” movement received 
 marked applause , melodious “ Allegro vivace ” , 
 usual , encored . orchestral pieces 
 Sir W. Sterndale Bennett highly imaginative 
 masterly overture “ Wood - Nymphs , ’’ Mozart 
 “ Jupiter ” symphony ( universally called ) . 
 Weber Overture “ Oberon . ” Madame Schu- 
 mann rendering Beethoven Pianoforte Concerto C 
 minor , interesting features 
 evening . Madame Parepa - Rosa , ap- 
 peared time return America , 
 unfortunately ill fulfi : engagement , 
 place supplied Madame Lemmens - Sherrington , 
 gave “ Deh vieni non tardar ” ( ‘ Le Nozze di Figaro ” ) , 
 “ Ah quelle nuit ” ( ‘ Le Domino Noir ” ) 
 excellent effect . vocalist M. Jules Lefort 

 MR . HENRY LESLIE CONCERTS 

 ADELAIDE.—The Christmas performance Handel 
 Messiah , Philharmonic Society , decided success . 
 orchestra consisted 29 instrumentalists 100 vocalists . 
 principal singers — Mrs. G. T. Harris , Miss Effield , Miss Nimmo , 
 Miss Vaughan , Mrs. Walkley , Messrs. C. Lyons , E. H. Hallack , L. 
 Grayson , G. C. Smith , C. C. Gooden , J. Brooks W. S. Dyer — 
 gave effect solos , choruses rendered 
 considerable power precision . Mr. E. Spiller acted con- 
 ductor , Mr. R. B. White es leader , Mr. James Shakespeare 
 presided harmonium 

 AYLEsBuURY.—The members Vale Aylesbury 
 Sacred Harmonic Society gave concert present 
 year 2lst February , Corn Exchange . works 
 selected performance Beethoven Mount Olives 
 ( Engedi ) Sullivan Prodigal Son . named com- 
 position principal parts excellently sung Miss Clare , 
 Mrs. Taylor , Messrs. Wootton , Ingram , Gibbons Taylor ; 
 Beethoven Oratorio , tenor music efficiently given 
 Mr. Ingram , Mrs. Parslow , trying soprano solos , 
 entitled highest commendation . success concert 
 — aided energetic intelligent conducting Mr. 
 orfe 

 BrruincoamM.—'The Postmen Annual Concert 
 given Town Hall , Birmingham , 2nd ult . Professor 
 Bennett Queen formed programme , 
 solo music interpreted Madlle . Liebhart , Mdlle . 
 Drasdil , Mr. Vernon Rigby , Mr. Orlando Christian . band 
 chorus voices direction Mr. C. 
 Heap . ballads introduced second , 
 violin solo Herr Pettersson . members Philharmonic 
 ae gave services occasion , Hall 
 ed 

 Rose Castile , spirit ; Mr. Atkinson displayed great 
 vigour * * Let like soldier fall , ” - demanded . 
 duet , ‘ * Love war , ” - named gentleman Mr. 
 Cook , sung . band talented conductor , 
 Mr. J. Sidney Jones , added greatly tothe merit concert ; 
 overture Rossini Stabat Mater , selection Don Giovanni , 
 played . Jullien ‘ Royal Irish Quadrilles , ” Night- 
 ingale Valse , Corporal McCain excellent piccolo solo , 
 applauded . Mr. Tom Dodds played pianoforte accompani- 
 ments usual ability . evening gratifying 
 announcement Mr. Bellchambers , hon . treasurer 
 tothe Association , surplus £ 50 
 handed Infirmary.——Onge largest concert 
 audiences seen Leeds Town Hallassembled Saturday , 
 18th ult . , Madrigal Motett Society occupied 
 orehestra . concert comprised sacred pieces , 
 included Handel duet , ‘ ‘ O lovely reace , ” sung Miss 
 Blakeley Miss Anyon . Dr. Spark Easter Anthem ad- 
 nirably performed , Miss Newell singing solo taste 
 effect . wedding music , given honour royal 
 marriage , included Benedict ‘ ‘ Wedding Chorus ” St. Cecilia 
 asongcomposed Mrs. Dodds , effectively given Miss Hiles ; 
 Mendelssohn Wedding March , spiritedly played Dr. Spark , 
 greatly applauded ; new Festal - song , Doctor , ‘ * Merrily 
 bells ring , ” pieces appropriate 
 tothe occasion . concert concluded ‘ ‘ Marseillaise , ” 
 “ Rhine Watch , ” “ ‘ Rule Britannia ” , singing 
 produced great excitement 

 Quartet strings , Mendelssohn E flat , No.3 . ( Scherzo , encored ) ; 
 Song ( Handel ) * * Let wander , unseen ” ; Sonata , major , 
 ( Herr Joachim ) violin pianoforte accompaniment , Handel , 
 ( Allegro , encored ) ; Aria ( Mozart ) ‘ * Voi che sapete , ” ( encored ) ; 
 Novelette ( Schumann ) F major , op . 21 , pianoforte ; 
 Impromptu ‘ ‘ Zur guitarre ” ( encored ) replaced Mendelssohn 
 - called * * Bee Weading ; ” Sonata , F , op . 24 pianoforte 
 violin ( Beethoven ) , Madame Schumannand Herr Joachim , ( Scherzo , 
 encored ) ; Song ( Benedict ) ‘ ‘ ” ; Quartet , D major , op . 
 64 , , 11 , strings(Haydn ) . concert appreciated 
 crowded audience.—THE fifth Subscription Concert Phil- 
 harmonic Society took place 2lst ult . , mainly 
 devoted Sacred Music . solo artists Miss Edith Wynne , 
 Madame Patey , Mr. Vernon Rigby , Mr. Santley . 
 interesting feature evening performance M. 
 Gounod “ ‘ Cecilian Mass , ” conducted composer , 
 reception enthusiastic . Mass went , exception 
 uncertainty vocal intonation — consequence 
 difference pitch , slightly lowered , 
 suit tne organ . ( habitual 
 pitch . ) orchestral works Weber ‘ Jubilee ’ Overture ; 
 Spohr little known beautiful * * Concert Overture , ” Op . 126 ; 
 Sinfonia Joseph , C , E. Horsley , ” Mendelssohn ‘ * Wed- 
 ding March , ” compliment , course , Princess Louise . 
 miscellaneous choruses second ‘ ‘ loud 
 voice Jephtha , Hallelujah Mount Olives 
 ( Beethoven ) went steadily . solos received , especially 
 Mr. Santley fire singing Pergolesi ‘ ‘ Sanctum et terribile ” 
 Madame Patey delicate rendering ‘ ‘ Evening Prayer ” 
 £ li , lady , , encored Gounod new Sacred 
 Song , ‘ * ‘ green hill far away 

 Lonertpee.—A concert given connection 
 Longridge Working Men Club Saturday evening , 11th 
 ult . , Boys ’ School . vocalists Mr. J. Edelston 
 choir tonic sol - faists . Mr. T. Woolman ably presided 
 pianoforte . programme consisted glees , songs , & c. , 
 interspersed occasional pianoforte solos , songs , 
 pieces . Master P. Edelston ( years age ) played 
 fantasia forth special admiration , Mr. T. 
 Woolman performed Rondo Weber skill . Daring 
 interval parts , Mr. Edelston gave explanation 
 principles tonic sol - fa notation , concluding 
 succeeded getting company sing direction 
 ashort piece . large attendance , gathering 
 presided Mr. Owtram , President Club . 
 concert Rev. W. C. Bache , vicar , proposed vote thanks 
 Mr. Edelston associates , spoke eulogistic terms 
 manner Mr. Edelston explained principles 
 system practised . votes having duly accorded 
 acknowledged , proceedings terminated 

 Saissury.—A performance Handel Judas Maccabey 
 given 2lst ult . Sarum Choral Society . 
 soloists Miss Lanham , Messrs. Cummings Brandon , 
 acquitted thoroughly satisfactory manner , 
 band , , efficient , entire performance , 
 conducted Mr. Read , , appreciated numeroyg 
 audience 

 SuEerriretD.—A Pianoforte Recital given Miss 
 Clara Linley , 8th ult . , Music Hall . programme 
 comprised selections important works Thal 
 Beethoven , Mendelssohn ‘ Clementi , Wehli ‘ Lurline * 
 Rhenish Song ( variations , Kies ) , March Tanhauser ( Liszt ) , 
 audience applauded ‘ ‘ Rhenish Song , ” encoie 
 demanded , Miss Linley acceded giving Chevalier de 
 Kontski duet Faust . performance thoroughly 
 successful 

 Sr . Hetens.—In acknowledgment gratuitous 
 services rendered Mrs. Cole , organist Parish Church , 
 bracelet presented concert 
 20th February , accompanied complimentary speech 
 churchwardens , Dr. Chevalier . gift manufag 
 tured Messrs. Watherston Sons , ( Pall Mall ) , cost £ 4 
 raised oval centre pale blue enamel , se 
 opals diamonds ; centre ornament ar 
 smaller groupings similar character , forming chaste 
 beautiful specimen high - class workmanship 

