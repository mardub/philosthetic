Violin Concerto 

 f rithjof ’ oe 2 an. . Maw Bruch . 
 principal : Mrs. Henry J. Woop , Mr. FReDERIC AUSTIN . 
 * * Nanie : ; oi Brahms . 
 closing scene from ' * Eugene Onegin Tschaike ywsky . 
 Mrs. Henry J. Wor yp , Mr. FreEpERIC AUSTIN . 
 orchestral item 
 FRIDAY morning , Ocusbin 6 . 
 * requiem " ' Mozart . 
 sian , 7 % Mrs. HENRY I. Woop , Miss " MurtEL Foster , 
 Mr. WILLIAM GREEN , Mr. H. LANt WILSON . 
 i'wo Kight - part chorus . Felix Weingartner . 
 ( a ) the House of Dreams . 
 ( b ) the Song of the Storm . 
 * * Eroica " Symphony Beethoven . 
 FRIDAY EVENING , October 6 . 
 aust " : pe , serlioz . 
 principal : Madame Dr Vere , Mr. Joun Coarrs , Mr. Joseru 
 AYCETT , Mr. ANDREW BL ACK 

 morning 

 Symphony Fantastic ( Berlioz ) and Sequel , 
 * * Lelio , " CEdipus at Colonos ( Mendelssohn 

 Taillefer " ( Strauss ) , Violin Concerto in D 
 major ( Beethoven ) , ' ' Dream of Gerontius 

 Elgar 

 Mecpa " night . * * Marino Faliero " 
 ( Holbrooke ) , Concerto ( Liszt ) , double 
 Concerto ( Mozart ) , & c. , & c 

 Grand Mass in C minor ( Mozart ) , ' 
 ( Beethoven 

 Lohengrin " ( Wagner ) . 
 " MESSIAH " ( Handel ) . 
 ini , Ada ( rossley , Muriel Foster , 
 oates , Ben Davies , Wm . Green , Ffrangcon- 
 and Andre w Black . Mr. Lawrence 
 Misses Adela and Mathilde Verne , and 

 SEASON 1905 - 6 INCLUDE 

 CH Dr. G. J. Bennett , Lincoln , and other conductor . 
 Apply , 32 , Cranbourne Road , Muswell Hill , London . WORCESTER FESTIVAL 
 LO IBENCIH TATIZO SR Sei can be 
 MISS FLORENCE BOWNESS ( Serr 0 AN 19 
 bir Ane ANO VOCALIST ) . SHEFFIELD FESTIVAL 
 ratorio , C concert , & e. 
 Peers Clough H : af E , umb - i in - Rossendale , Manchester . ( Ocr . 4 and 5 ) 
 FEL- _ 
 wae MISS ' concert old ' 
 r and . HALLE CONCERTS , MANCHESTER 
 al ~ de = ) y ~ = = " . = x , 
 middle GER I RL DE FLET CHER ( FEB . 15 , 1906 ) 
 v. 16 . ( SOPRANO ) . pupil of Miss ANNA WILLIAMS . 44 si : : pre 
 lus . D. , oratorio , concert , & c. for term , apply , 24 , Sutton Court , MISSA SOLEMNIS " ( BEETHOVEN ) 
 Chiswick , W. — _ . IIDC DID , fkawerottre 
 serner ' IT have much pleasure in recommend Miss Gertrude Fletcher as a BRADFORD SUBSCRIPTION concert 
 solo soprano . Miss Fletcher be an earnest worker , good musician , ( Marcu ear 
 possess an excellent voice , and would , I be sure , give satisfaction in E BRS Se Sey 
 everything she undertake . " ALLEN GILL . " " LES BEATITUDES " ( C. Franck 

 1 . ) MISS ESTELLA LINDEN 
 PRINCIPAL SOPRANO SOLOIST , Southwark Cathedral ( last 
 six year 

 honourable distinction of having train one of 
 ithe great musician the world ever see 

 consideration of the father of Beethoven , 
 | Mendelssohn , and some other composer must 
 be defer till next month 

 english CHURCH exhibition 
 AT ST . ALBANS 

 overture to the Isles of Fingal ( MS 

 Mendelssohn Bartholdy . 
 the overture be primarily dedicate to the 
 Philharmonic Society , and subsequently to the Crown 
 Prince of Prussia , afterwards Frederick William IV . , 
 King of Prussia ; therefore the work follow the 
 example of Beethoven ’s Choral Symphony which , it 
 will be remember , be first dedicate to the 
 * hilharmonic Society , but the print score be 
 inscribe to Frederick William the 7%7rd , King of 
 Prussia . the Hurmonicon record the first per 
 formance of the overture in these word : 
 Mendelssohn , write for these 
 hear for the first time , a 

 the overture of M. 
 Concerts , now 
 circumstance which ought to have be notice in the 
 program , for the dry announcement contain in the 
 letter ' MS . , ' say little : indeed it may signify that , 
 whatever the age of the composition , it have never be 
 think worth print . the idea of this work be 
 suggest to the author while he be in the most 
 northern part of Scotland , on a wild , desolate coast , 
 where nothing be hear but the howling of the wind and 
 and nothing live see , except 

 the A / heneum notice of the first performance may 
 also be quote 

 a MS . composition by Mendelssohn , 
 ' overture to the isle of Fingal , " be perform for the 
 first time in this country . the burthen of the com- 
 position strongly remind we of Beethoven . towards 
 the end it be well work with figurative passage for 
 violin , the subject be sustain by the wind instru- 
 ment — but as descriptive music it be decidedly a 
 failure . ( Atheneum , May 19 , 1832 

 entitle 

 thus each poem must come as a ' message ' to 
 M. Blumenthal ere he can set it to music 

 Haydn , Mozart , and Beethoven live and labour 
 and die in Vienna , but they be not native of that 
 city . Vienna , however , can boast of having give 
 birth to Franz Schubert , and among the treasure 
 which he bequeath to the world there be many 
 waltz . but at the beginning of the 19th century , 
 while Schubert be yet a child , two man be bear 
 whose dance music soon attain world - wide celebrity 

 melody by the grateful inhabitant of this city 

 532 the MUSICAL TIMES.—Avcust 1 , 1905 

 the point by give it a title . Mendelssohn approve 
 of ' programme - music , ' and justify it by the example 
 of his great predecessor . ' when Beethoven , ' say he , 
 ' have open the road in the Pastoral Symphony , it 
 be impossible not to go far . ' and he be right ! 
 the work even call forth a warm eulogy from 
 Wagner — not always his eulogist . ' the Hebrides , ' 
 say he , ' be Mendelssohn ’s masterpiece . wonderful 
 imagination and delicate feeling be there present 
 with consummate art . " and surely in this overture 
 we need not be tell that Mendelssohn have write a 
 piece of descriptive music which can hardly be 
 surpass as long as music remain what it be 

 to I , ' say he , ' the fine object in nature be , and 
 always will be , the sea . I love it almost more than 
 the sky . " of his four concert overture two be 
 sea - piece . and yet what variety ! the ocean of 
 ' the Calm Sea and Prosperous voyage ' be an ocean 
 of no time and no quarter of the globe — a truly ideal 
 sea . but the ' Hebrides ' overture be as local as the 
 other be universal . it be not only full of the sight and 
 sound of those northern island , those sombre shore , 
 and gray sky , and moan , uncertain wind and 
 busy wave ; but it be pervade with the cere , lonely 
 feeling that make the northern maritime region so 
 peculiar . and yet , after its northern character be well 
 establish , what a burst of softness come over the 
 picture ! it must be the warmth and colouring of 
 Italy , where he elaborate and mature his com- 
 position . the sweet air of the south blow upon he 
 while he be meditate or work at his highland 
 theme ; and he forget the rude north , and the italian 
 sun shine , and the scene change from the cloudy 
 sky and the lash breaker of Staffa to the Bay of 
 Naples , blue and calm , and Galatea and her Nymphs 
 and Nereides sail over the surface , and the note of 
 their sound shell re - echo along the sunny shore 
 and float over the bosom of the bay . ( see example 
 no . 3 . ) but hardly have he see and record this 
 vision of the Old World before he remember how 
 unreal it be , how it must come to an end — aas already 
 come to an end ; and a sigh of regret escape he , 
 and he turn from the lovely , voluptuous southern 
 picture back to the stern , gray sea and barren , sound 
 shore , and melancholy sentiment of the north again 

 first step for the Violin ' should prove to be a 
 useful instruction book . it comprise a number of easy 
 and progressive study and a collection of melodious 
 piece with pianoforte accompaniment . Mr. Kreuz 
 be an experienced teacher and an accomplished musician , 
 and he have embody in this primer the result of 
 vear of observation of the difficulty meet with by beginner . 
 he consider that in instruction book , as a rule , too many 
 difficulty be introduce at each successive step . his 
 course be divide into twenty well define and grade step , 
 and the technical instruction be accompany by explanation 
 of musical theory . but the logic of violin fingering and not 
 that of abstract musical notation govern the gradation of the 
 course . thus one of the first exercise be in the key of FE on 
 the FE string , the step be base upon the idea of finger 
 osture . in this way a pupil be lead on to play many scale 
 without difficulty . an attractive speciality of the course be 
 that a pianoforte accompaniment be provide to the exercise . 
 this will be an incentive to home practice . all the valuable 
 exercise and melodious study so treat be in the 
 instruction book and be also publish separately with 
 the pianoforte accompaniment . they be thus available for 
 use as a supplement to other course 

 Beethoven . by Ernest Walker . 
 [ Philip Wellby 

 this little volume , belong to the ' Music of the master ' 
 series edit by Mr. Wakeling Dry , concern , as the title 
 show , only the master ’s music . limited space compel the 
 author to be brief , yet on every page he show himself well 
 acquainted with Beethoven ’s art - work . it would be 
 impossible , and indeed unnecessary , to review this thoughtfully 
 write book in detail . we therefore just comment on one 
 or two passage . in the interesting chapter on the piano- 
 forte sonata , for instance , the author speak of op . 2 , no . 3 
 in C. as ' technically very brilliant ' ; but we be surprised 
 that he do not specially single out the Adagzo , which 
 seem to we on a far high plane than the rest of the work . 
 again , we can not agree with his opinion respect the 
 sonate ' Pathétique * as ' vastly superior ' to that in C minor , 
 Op . 10 , no . 1 . but after all there be truth in the old 
 proverb , qzsot homitnes , tot sententiv . attention , by - the- 
 way , be call to the thematic similarity between the open 
 note of the first three movement of op . 106 . but surely 
 the fugue theme in the # 7 / a / e might also have be include . 
 in refer to Beethoven ’s music as a whole , Dr. Walker 
 touch on the ' burn question ' of the ' meaning ' of 
 instrumental music . he think it highly probable that all 
 the master ’s statement record by the friend of Beethoven 
 be ' mere joke . ' with this we can not agree , although 
 we feel sure that the composer would have look with 
 contempt on some of the ' meaning ' assign by various 
 writer to many of his work . neither do we think his 
 remark to Charles Neate , 7¢ always work to a picture in 
 his mind , ' cryptic , ' ' difficult ' to understand . 
 ' chronological Table of Ludwig van Beethoven ’s Life , ' but 
 even for a small volume too brief , and refer principally 
 to some work , their completion , production , or publication 

 decline now , the Sun ’s bright Wheel . lo , now the Shades 
 of Night be swiftly fade . come , Holy Ghost . english 
 word by the Rev. John Anketell . music by Horatio 
 Parker . op . 58 

 Mischa Elman conclude — at ( ) ueen ’s Hall , on July 6 — his | 
 recent remarkable series of violin recital , when , in association 

 with Miss Adela Verne , he play Beethoven ’s ' Kreutzer ' 
 sonata . if , as might almost be expect , his reading be 
 lack in virility , it possess individuality of an attractive 

 kind , and technically it be a marvellous performance for a 

 tee . 27 , iit 

 i BEETHOVEN 1 
 8 . Ss a " ) A.C. MACKENZIE 1 
 9 . finale ( ' fo may we once again bor BLest pair oF 
 srren " - . H. H. PARRY 
 10 NOTTURNO.—‘‘A MipsumMer NiGut ’s DrEAM " 
 MENDELSSOHN 1 

 to be continue 

 the response to the commandment have be select from the service of the above composer as well as those 
 of Myles B. Foster , Dr. Garrett , Ch . Gounod , Dr. E. J. Hopkins , Dr. C. H. Lloyd , Merbecke , H. Smart , John E. West , 
 S. S. Wesley , and other 

 Sir John Stainer ’s sevenfold amen be also include , as well as Vesper Hymns by Beethoven , Sullivan , and other ; 
 conclude with two Vestry Prayers by S. S. Wesley and the Rev. Canon Hervey 

 PREFACE 

 EDWARD ELGAR 

 HORATIC ) PA RK e R. arrange by G. R. Sinccair .. I 
 11 . finale from SYMPHONY no.5 ... BEETHOVEN 
 arrange by A. B. PLant zs ae e « 3 

 price , each , two shilling net . 
 : ( to be continue 

