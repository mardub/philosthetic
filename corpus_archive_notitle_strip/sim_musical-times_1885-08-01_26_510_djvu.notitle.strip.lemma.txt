child , compose for this Festival by Mr. C. Villiers Stanford . conpuctror , Mr. MACKENZIE 

 Beethoven 's CHORAL symphony 

 FRIDAY EVENING.—MORS et VITA 

 Oratorios , & c. , Coundon Street , Coventry 

 MISS FANNIE SELLERS ( soprano ) . 
 Vor Oratorios , Concerts , & c. , adc Iress , Crag Cottage , Knaresboro ' . 
 MADAME CLARA WEST ( soprano ) . 
 Beethoven Villa , King Edward Road , Hackney . 
 MISS LOUISA BOWMONT ( contralto ) . 
 ( principal of St. Peter ’s , Manchester ) . 
 address , 51 , Mercer Street , Hulme , Manchester . 
 MISS LYDIA DAVIS ( Contralto ) . 
 ( l’rom the Royal Academy of Music . ) 
 for Concerts or Oratorios . 56 , Grant Road , Clapham Juncti 
 MADAME TALBOL LEGG ( Contralto ) . 
 for Concerts , & c. , 94 , Lenthall Road , Dalston . 
 MISS PATTIE MICHIE , L.A.M. ( Contralto ) . 
 ( pupil of Signor Schira . ) 
 for Concerts , Oratorios , & e , . , 68 , Park Walk , Fulham Road , S.W 

 on 

 BACH , P. E.—Presto from D major isiaiade arrange for 
 two Pianofortes , eight hand , by J. Knorr 

 hw ) ttt BEETHOVEN.—Kondino for Wind Instruments . Arrar 
 FACT ‘ ns two Pianofortes , eight hand , by J. Knorr 

 BOHM , C. — " t ‘ rinzchen . " Anea Piesatut > 2 
 . ! 7 4 } i 1co% — " Inthe Twil bay » 323 , no . rag oo pianoforte 
 d ui /v/ ~ " dear Home . Landler . op . 325 . for Pianof 
 — " love song . ' vO. I. for P ianoforte 
 — " Das Deutsche Jah twelve 

 qa 

 the Richter Concerts have this season be tho- 
 roughly successful — certainly artistically , and , we 
 have every reason to believe , financially . Wagner 
 and many other of the modern german composer 
 have be well represent ; but there have be a 
 satisfactory variety in the selection , even a Sym- 
 phony of Haydn ’s having have a place in one of the 
 programme . the orchestral work have be ren- 
 dere with a perfection to which Herr Richter have 
 now accustom his audience — Beethoven ’s Choral 
 Symphony more especially ; and , on the whole , the 
 vocal music have be extremely good . an autumn 
 series of Concerts be announce to take place in 
 October and November next , and the usual summer 
 series of nine Concerts will commence in May , 1856 

 the Sacred Harmonic Society give a proot of the 
 desire of the director to recognise the claim of 
 english composer by produce Mackenzie ’s " Rose 
 of Sharon " ( which have previously be hear only at 
 the Norwich lestival ) at the first Concert . Derlioz ’s 
 * T’Enfance de Christ ' ? and Handel 's ' " Belshazzar " ' 
 be welcome revival during the season ; but the 
 vood efiect of the engagement of Mr. W. H. Cummings 
 to direct the rchearsal of the choir be greatly 
 mar by the placing of Mr. Charles Hallé at the 
 conductor 's desk for the Concerts . either of these 
 excellent artist would undoubtedly have produce 
 highly satisfactory result have only one be respon- 
 sible for the performance ; and we be therefore 
 eratified to find that Mr. Cummings have be ap- 
 point sole Conductor for next season . not only 
 for the sake of the reputation of the Society , but in 
 justice to the member of a really able choir , this be 
 a wise resolution , and the director be to be warmly 
 commend for their prompt action in the matter 

 but as it have 

 ment of the art , which be costly and diificult to 
 realise , require a concentration of attention which be 
 | hardly to be expect from those toil for their 
 very existence . in this sphere then there will remain 
 a wide scope for leisured and philanthropic amateur . 
 from they the initiative must come , as it have come 
 hitherto , and their assistance as organiser , as member 
 of the chorus , and , in exceptional case , as soloist , 
 will continue to be both welcome and valuable . but 
 for the adequate presentment of good music , by 
 which alone it can be bring home to the masse , 
 the employment of professional talent — vocal , and , 
 to a still great extent , instrumental — be , in the 
 present state of thing , an absolute necessity . 
 | while , then , a deep debt of gratitude be due to 
 the great artist who from time to time have 
 gratuitously volunteer their service in this 
 cause , it have be conclusively establish by the 
 financial prosperity which attach in former year 
 to the Birmingham Musical Association , and which 
 still attach to the Working Men ’s Concerts in 
 Manchester , that the almost exclusive employment of 
 efficient pay professional performer be compatible 
 with an admission fee range as low as a few penny , 
 and never higherthana shilling . some brief account 
 of the work contemplate and achieve by these 
 ; excellent organisation may be not without interest 
 to our reader . with the operation of the London 
 | Societies there be more opportunity for become 
 familiar , through the medium of report of such 
 |meeting as that hold last month at the Mansion 
 House , or article like that on the work do by the 
 | Kyrle Society , by Miss Octavia Hill ( Nineteenth Century , 
 | May , 1884 ) , and we have , therefore , abstain from 
 any detailed account of their doing . but the case be 
 different with regard to the province . moreover , 
 since apart from their reputation as musical centre , 
 Birmingham be generally consider the home of 
 organisation , and Manchester of business - like common 
 sense , the consideration of their exertion in this 
 sphere should not be without its lesson . and , lastly , 
 the operation of cause be easy to observe in 
 small field , where concentration and federation be 
 easy than in our bulky metropolis . the Birmingham 
 | Musical Association be establish in 1879 , w ith the 
 two - fold purpose of provide a continuous series of 
 | cheap Concerts , and give instruction through the 
 medium of class and a musical library . the first 
 half of this programme be admirably fulfil from 
 the outset , by the performance of oratorio , and instru- 
 mental and vocal music of a uniformly high level , 
 with efficient principal . the promise of a library 
 and instruction class , however , be _ speedily 
 abandon . believe , as we do , that this part of 
 the scheme be fully as important as the other , we 
 | earnestly trust that it may be revive . we do not 
 grudge one penny of the large sum of public money 
 lay out in the purchase of good picture at Birming- 
 ; ham , but we should be glad ' to think that her citizen 
 ; have the same access to the thought of Beethoven 
 as they have to the gorgeous landscape of Miiller . 
 a musical library be an incalculable boon to a student , 
 who can not afford to purchase a score , inasmuch as 
 it not infrequently afford he the only means of 
 realise the intention of the composer 

 at the Concerts give by the Association , none 
 but professional soloist appear save in very rare 
 | case , and , as a rule , full fee be pay . the local 
 choral society loyally assist at the Saturday night 
 |Concerts , illustrate the advantage of a sort of 
 federal system , and the finance of the Association 
 remain on a sound footing for the first four season 
 of its existence . we know that post hoc ergo propter 
 hoc be not a safe form of argument , but it be curious 
 to notice that the wane prosperity of this organi 

 in the early application of ie : to 

 symphony of Beethoven , or the drama of Wagner , 
 the art generally have to pass through a further 
 probation 

 throughout this survey , so far , we see that the 
 theme or melodic outline be the unit and animate 
 principle of the musical structure . in the endeavour 
 to enunciate more than one theme simultaneously , 
 arose polyphonal effect , which , in the form of the 
 fugue , betray , for the first time a musical structure 
 that can stand , if need be , independently of literary 
 accessory . in the practice of the fugue , fundamental 
 element of harmonic effect in the form of chord 
 | be discover . ' these , the residuum of polyphony , 
 be apply in a special way to display melody in 
 j-|the form of the Choral , and , notwithstanding they 
 |gave breadth and a certain vague grandeur to the 
 theme , they often tend rather to obscure than to 
 display the melodic intention , because their march 
 do not always conspire with that of the note of 
 the melody to uphold , for the time be , a single 
 standard of symmetry in the form of the key - note ; 
 and the theme be only strengthen unequivocally 
 on the rhythmic side . by the discovery of the chord 
 of the Dominant 7th , theme and its harmonic environ- 
 ment be bring at length into mutua wi a 
 combination . by aid of the art of accompaniment it 
 become possible to display the mine outline in this 
 combination with peculiar distinctness , asi inrecitative , 
 air , duet , and concerted music . this famous inven- 
 tion open the way for the development of dramatic 
 music , which up to this point could only attempt to 
 articulate in the diffuse archaic accent of fugue and 
 dmitte far 

 cally , contain in the order here give the name of 

 Bach , Scarlatti , Handel , and Clementi , Beethoven , 
 Chopin , and Schumann — and , finally , those of the great 
 modern virtuoso , Thalberg , Liszt , and Rubinstein . the 
 object be , naturally , to display to a critical London 
 audience the power of the italian artist in every stvle 
 of music . the verdict we ourselves hear more than 
 once in the concert - room , in reference to Signor Cesi ’s 
 playing , be — ' marvellous , all round . " he possess 
 reat strength of wrist and an extraordinary facility 
 in play rapid octave passage with a minimum of 
 apparent effort ; still , the prevail charm in Signor 
 Cesi ’s performance be in its expression — in his liquid 

 touch , and the clearness with which , in the most 
 delicate passage , every feature in the harmony be 
 enounce . Signor Cesi be thoroughly at home in the 

 music of Scarlatti ; his inclination seem to lean to that 
 classic period . he play , too , Beethoven ’s ' Sonata 
 quasi Fantasia " with verve and intelligence ; but to his 
 London audience perhaps the great novelty and treat 
 be his rendering of Handel ’s ' ' gavotta variata , ’' Chopin ’s 
 " Larghetto " ' from his second Concerto , the same author 's 
 Polonaise ( Opus 44 ) , and an entr’acte from ' * Mignon , " 
 arrange by the performer himself . Signor Cesi be the 
 principal Professor at the Royal College of Music in 
 Naples ; and in that city his Concerts of Classical Music 
 enjoy a high reputation . he be also director of the 
 Archivio Musicale of Naples , a musical periodical which 
 commence two or three year ago in an ambitious 
 tone , in the high degree praiseworthy . Signor Cesi leave 
 London for Italy soon after his Concert of the 15th ult . , but 
 he promise to return another year at an early period of 
 the musical season , when we trust to our credit as a well- 
 breed people he will meet with a hospitable reception 

 the NEW GUILDHALL SCHOOL of MUSIC 

 d be train , and also for the able 
 manner in which he conduct 

 Tuer Chevalier Leonhard Emil Bach give his second and 
 last Concert this season on the roth u at St. James ’s 
 Hall , assist by Madame Antoinette Ste Miss Medora 
 Henson , Signor Novara , and a select orchestra preside 
 over by Signor Randegger . Beethoven 's pianoforte Con- 
 certo in C minor ( Op . 37 ) , piece by Chopin and Liszt , 
 and a Capriccio Polonaise for pianoforte and orchestra , of 
 his own composition , afford an opportunity to the concert- 
 giver to manifest his distinguished and versatile talent 
 both as a performer and a composer of considerable merit . 
 in the latter capacity his success , on the present occasion , 
 culminate in the charming rendering by Madame An- 
 toinette Sterling of four nursery song ( form part of a 
 collection entitle ' ' carol of cradleland , ' about to be 
 publish by Novello , Ewer & Co. ) , one of which , " our 
 baby , " be vociferously encore , and there can be little 
 room for doubt that these simply yet effectively write 
 songlet will soon become popular in the quarter for which 
 they be more especially intend . the programme also 
 include a very efficient interpretation of the prelude to 
 Act III . , andthe Banquet Dance from Sir Arthur Sullivan ’s 
 highly characteristic ' ' Tempest " music ; Signor Randegge 

 conduct with his usual ability and tact 

 an excellent recital be give by the pupil of the 

 Harrow Music School ( South Hampstead Branch ) , on 
 | Wednesday evening , the 15th ult . , at the Eyre Arms 
 | Assembly Rooms . the selection of music be of a high 
 | class , be take from the work of Bach , Beethoven , 
 | Mendelssohn , Brahms , Schumann , Mozart , Raff , & c. , 
 and the performance be of more than average merit . 
 amongst the instrumental item most worthy of mention 
 be a duet for two piano , Schumann ’s Andante and 
 variation , Miss Florence Stephens and Miss Theresa 
 Slocombe ; a Sarabande by John Farmer , Miss Jessie 
 Oliver ; the pianoforte performance by Miss Ethel 
 Stephens , Miss Annie White , and Miss Annie Benito , and 
 | the rendering of the Adagio from Spohr ’s ninth Concerto 
 | ( violin ) , by Master Alfred Slocombe . vocal piece be 
 contribute by Miss Minnie Spackman , Miss Katie Howell , 
 | Miss Carrie Stephens , Mrs. Brooks , Mr. Cuthbert Wills , 
 and Mr. David Strong , the last name gentleman sing- 
 e with great effect Kicken ’s ' " bird , fly from hence " 
 ( violoncello obbligato , Mr. Trust ) . a duet for two piano- 
 fortis be also excellently play by Miss H. L. Fox and 
 Miss Bessie Frost , the principal of the school , who deserve 
 | warm praise for the efficient manner in which they be 
 | carry on the work of the institution 

 i 

 WE sincerely hope that the appeal for fund to purchase 
 an organ for the Cathedral of the Holy Trinity , commonly 
 call Christ Church , Dublin , will be most liberally re- 
 sponde to . it be well know that there do not exist in 
 the United Kingdom a more beautiful specimen of eccle- 
 siastical architecture than this Cathedral ; and the effort 
 of the Dean and Chapter and the Cathedral Board to 
 obtain an instrument worthy of such a magnificent structure 
 should receive the earnest support of all who desire that 
 the worship maintain in our Cathedrals for so many 
 century shall not be allow to deteriorate . it be esti- 
 mate that at least £ 1,000 will be require for this object ; 
 and subscription will be receive by any member of the 
 Organ Committee ( which be head by his Grace the Lord 
 Archbishop of Dublin , Dean ) , by the Secretary , Frederick 
 W. Leeper , Esq . , Diocesan Offices , 17 , Lower Baggott 
 Street , Dublin , or at the Royal Bank , Dublin 

 Tue Wycliffe Chapel Choir give an evening Concert 
 in the chapel , Philpot Street , Commercial Road East , on 
 Tuesday , June 30 . the programme include the music 
 which gain the choir the first prize and bronze medal at 
 the late East London Industrial Exhibition . Beethoven 's 
 Sonata , no . 2 , be admirably play by Mr. C. H. Row- 
 cliffe , Organist of St. Luke ’s , Hackney , and this as well as 
 the other item in the programme be well receive . 
 the Rev. Charles Lemaire , the pastor , in propose a vote 
 of thank to the choirmaster , Mr. George Merritt , G.T.S.C. , 
 the soloist , and choir , express a hope that the Concert 
 would be repeat in the early autumn 

 at the Albert Palace , ' ' the Messiah " be perform 
 on the 1sth ult . , with Miss Albu , Miss Clara Myers , Mr. 
 Abercrombie , and Mr. Bantock Pierpoint as principal 
 soloist , on the 25th ult . , Mr. W. Carter ’s " Placida " 
 and Rossini ’s ' ' Stabat Mater ’' be give , the soloist 
 be Miss Pattie Winter , Madame Antoinette Sterling , 
 Signor Fabrini , and Mr. Watkin Mills . Mr. Carter con- 
 ducte , and Mr. A. J. Caldicott preside at the organ 

 Max Bruch ’s new Oratorio , ' Achilles , ' meet with a 
 very favourable reception upon its recent performance at 
 the Bonn Festival , notwithstanding the undue length 
 from which the work be say somewhat to suffer 

 a rare celebration — that of the seventieth anniversary of 
 his active connection with a leading art - institution — have 
 just fall to the share of Capellmeister Louis Schlcesser , 
 of the Darmstadt Hof - Theater , which establishment he 
 enter as an orchestral member on June 18 , 1815 . the 
 veteran conductor , who be also well know as a composer 
 of much merit , cherish personal reminiscence of nearly 
 every distinguished composer of the present century , from 
 Beethoven , Weber , Schubert , and Cherubini ( he having be 
 a pupil of the latter ) , to Berlioz and Richard Wagner . 
 it be needless to say that on the occasion in question Herr 
 Schlcesser be the recipient of numerous token of the 
 esteem in which he be hold , both in his own country 
 and elsewhere . we may add that , notwithstanding his 
 eighty - five year , the Maéstro ( as witness his article on 
 * * the Rose of Sharon , " of which we give a resumé in our 
 last number ) still follow the musical current of the day 
 with undiminished attention and sympathy , wield an able 
 literary pen in the good interest of the art to which his 
 whole life have be devote . long may he continue to do 
 so 

 at a Festival Concert recently give in connection with 
 the musical section of the Berlin Royal Academy of Arts , 
 the programme include a performance of a pianoforte 
 trio by our countryman Mr. George Bennett , which be 
 well receive . the executant be Miss Emily Shinner , 
 Herren Berger and Koch 

 Seman u.—the seventh season of Mr. Julian Adams 's Orches- 
 tral Concerts commence at the Devonshire Park , on June 30 , with 
 brilliant suc in every respect Mr. Adams thoroughly maintain 
 the reputation he have gain both as a Conductor and pianist ; and there 
 isevery reason to believe that the present series of perform : ances will 
 not only equal , but eclipse those which have precede they . it need 
 scarcely be say that Mr. Adams ’s reception by the large audience 
 assemble be most enthusiastic 

 FaLKirkK.—A very successful Concert by Mr. J. Watson prt 
 vupils be give on Friday , June 2 26 , in the bas oily s ’ Hall . Int 
 — division the work select be by Haydn , Beethoven , Cle 

 enti , & c. the chief item of interest in fi Senior Division be 
 the overture to Der Freischiits ( Weber ) , for eight hand ( two 
 piano ) , and harmonium ; three mov — s from the septet , Op . 20 
 ( Beethoven ) , for two piano , and We ber ’ s " Polacca Brillante " in e 
 all of which be excellently play . ' with verdure clothe " ( Haydn ) , 
 and " my heart ever fa ithful " ( Bach ) , be admirably render by 
 Miss Brown and Miss Wilson . at the close the Chairman ( Rev. 
 James Aitchison ) distribute the certificate from Trinity College 
 London , gain by student at this centre , and also a few prize 

 fo_kxestonr,—special service be hold at the Parish Church , on 
 the 15th ult . , on the occasion of the re - opening of the Sanctuary after 
 its renovation and embellishment . at the evening service the Det- 
 tingen Te Deum be perform , Mr. Dugard conduct , and a 
 String band , lead by Mr. J. Rk . C. Roberts , supplement the organ , at 
 which Miss Daly preside 

