WeDNESDAY EventinG (Public Hall).—Parry’s 
DAY (conducted by the Composer) and 
SELECTION

Tuurspay Morntnc.—Bridge’s REPENTANCE OF 
(conducted by the pei and Beethoven's ENGEDI

1k H

NIGHT AT

Beethoven's

CECILIA'S | 
MISCELLANEOUS

and the new Concerto for pianoforte and orchestra

by Jacob Rosenhain. There was also a fine Over- 
ture to “ Richard the Third,” by Edward German, 
which was repeated later at one of the Phil- 
harmonic Concerts. Hamish MacCunn’s Overture 
“The Land of the Mountain and the Flood,” 
and his Cantata ‘Bonny Kilmeny”; Goldmark’s 
new Symphony “Im Frihling,’ together with the 
number of standard works by Beethoven, Haydn, 
Mozart Schumann, Schubert, Mackenzie, and other 
classical composers, testified to special activity and 
the desire to keep pace with the public demand for 
novelty. The season concluded, as usual, with a 
Concert for the benefit of Mr. Manns, the able Con- 
ductor, when a new Symphony, by E. M. Smyth, was 
included in the programme. 
“St. Paul,” conducted by Mr. Manns, on June 21, by 
a body of vocalists and instrumentalists, numbering 
3,500 performers, including in their ranks a choir of 
500 boys, and a large contingent of singers from 
Bristol, was among the most memorable events of the 
season. The success was even greater than upon the 
occasion when “ Elijah’ was presented under like 
conditions, and points to a possible Mendelssohn 
Festival on the same scale as that given in honour 
of Handel as likely to be attractive and remunerative 
in the future

Novello’s Oratorio Concerts, after a period of 
spirited activity during four years, having been 
discontinued, the interest in the production of im- 
portant new choral works became transferred to the 
Royal Choral Society. That body gave a number of 
successful performances of important works, such as 
Benoit’s * Lucifer,” Berlioz’s “ Faust,” Parry’s “ St. 
Cecilia,” Stanford’s ‘Voyage of Maeldune,” Mac- 
kenzie’s ‘ Cotter’s Saturday Night,” and ‘The 
Dream of Jubal,’ under the baton of the composer; 
Sullivan’s * Golden Legend,” and Handel's “ Messiah” 
and “ Israel in Egypt,” ina manner which maintained

altogether the musical soil of Belgium, barren though 
it has hitherto been, with the excepticn of some 
individual talents, I can only advise you again to 
protest absolutely against a performance of your 
works under any direction but your own.” With 
this the matter of a Brussels “ Lohengrin’”’ hung up, 
not to be taken down till nearly twenty years later 
1870

We now get on the track of “ Siegfried.’ Wagner’s 
continuance at this work was, on his own showing, 
imperilled by pecuniary circumstances. Liszt had 
said something of obtaining help from the Grand 
Duchess of Weimar, and, on March g, Wagner 
declared that he “‘ must now know whether | am to look 
for its fulfilment or to abandon it altogether.” There- 
upon followed an exposition of his financial state: 
“ During the whole of six months, after spending the 
honorarium for the production of ‘Lohengrin’ at 
Weimar, I have lived entirely by the assistance of 
Frau R. in D., because latterly I have not been able 
to earn anything beyond a small fee for conducting 
two of Beethoven’s Symphonies at the miserable 
Concerts here (Zurich). I know that my Dresden 
friend has for the present exhausted herself, because 
the family is not wealthy, but has only just a 
sufficient income, which, moreover, owing to some 
awkward complications with Russia, is at present 
placed in jeopardy. I am therefore compelled to try 
and make money at any price, and should have to 
abandon a task like the composition of ‘ Siegfried’ 
which, in a pecuniary sense, is useless. If I were to 
have any inclination for a task undertaken for the 
sake of money, it would have to be so-called ‘ esthetic 
literature,’ and in order to get money for such litera- 
ture I should have to spend «all my time in writing for 
magazines at so much ‘per sheet.’ The thought is 
very humiliating.” But not, apparently, that of living 
upon friends who have only just enough for them- 
selves. Wagner goes on to say that if he is to proceed 
with an artistic work—he must know his position. 
Wherefore he begs Liszt to say what he can do in the 
matter of cash. ‘ If you are compelled by the state 
of affairs to tell me that your plan cannot now be

realised, and that therefore I must not hope for

more easily ‘ understanded of the people.” Here 
we have the point of view of the illiterate musician, 
who regards words in the light of a necessary evil. 
The writer in the Scots Observer is at the opposite 
pole. As he puts it: Scots song is one} 
thing; Scots music is another. And if this is his | 
deliberate opinion of melodies which have stood the 
test of time, what would he say of the musical plati- 
tudes which are often linked by fifth-rate ballad | 
composers to really noble words? We cannot | 
endorse his proposal, but we welcome it if only for 
the stimulating effect it may have upon those who | 
imagine that the words are of no consequence, | 
whereas in a vast number of cases, ancient and

pencil, names of singers, &c., in Handel’s own 
writing; and also many which had hitherto never 
found their way into public collections. Among the 
latter is Loewe’s ballad ‘ Odin’s Ride.” Of auto- 
graph signatures, the most amusing is that given by 
Liszt to Mr. G. Lichtenstein: ‘ Against albums, 
autograph collections, and such like, strenuously 
protests Iranz Liszt.” There are also letters and 
signatures of Beethoven, Spohr, Mendelssohn, Stern- 
dale Bennett, Tausig, Wagner, Berlioz, &c

Tue Brighton Town Council has decided to offer a 
subsidy of £300 a year to any venturesome musician 
who will run a Town Band of thirty performers, and

It is wonderful how tenacious of life is the old 
aristocratic view of musicians as being on the same 
plane with acrobats, jugglers, mountebanks, ‘“ and 
other rubbish,” to quote the comprehensive descrip- 
tion of Frederick I., of Prussia. That this is the case 
may best be proved by pointing to the style of enter- 
tainments which alternate with music, often by very 
fine artists, at the receptions of those who belong to 
“Society.” A little while ago it was by no means an

If then “beating with the regularity of a metro- 
nome ” can never lead to the intellectual rendering 
of a great orchestral composition, how could such a 
masterpiece as Beethoven’s Choral Symphony, for 
example, be conducted by merely ‘pressing a 
button

THERE is much to be said in favour of the practice 
of performing operas in the language in which the 
libretto was originally written. And in these days of 
polyglot companies the linguistic difficulties of the 
situation are by no means so serious as they would 
have been, say, twenty years ago. Still, in spite of 
the advance of education, performances on these 
lines are not altogether exempt from imperfections 
when a large proportion of the artists are obliged to 
employ a language other than their own, especially 
if that language be French. This fact was amusingly 
brought home to the audience on the occasion of the 
recent performance of ‘* Hamlet,” at Covent Garden. 
The gentleman who took the part of the Ghost 
declaimed his lines with adequate impressiveness and 
with the most praiseworthy articulateness, but in

But we did not know that it had fallen so low as 
appeared not long since at a book sale. For many

standard works in good condition the dealers would 
hardly make a bid. By way of encouragement to 
those who contemplate writing or collecting books 
on musical subjects, we give some of the prices from 
the marked catalogue of a curious observer : ‘* Memoir 
of Balfe,” by C. L. Kenney, and ditto by W. A. 
) Barrett, 4s. Nohl’s ‘* Beethoven,” Letters of Beet- 
| hoven, and Life, by Wagner, 11s. Burney’s ** Present 
State of Music in Germany,” &c.; ‘ Present State 
of Music in France and Italy,” and ‘ Handel Com- 
memoration,” 4 vols., 13s. Busby’s ‘ Anecdotes

4s. Karasowski’s “ Chopin,” and * Life of Chopin.” 
|by Liszt, 3s. Ella’s ‘* Musical Sketches,” 2s

470 . THE MUSICAL TIMES.—Aveust 1, 18go

Promoters of musical festivals across the Atlantic | devoted respectively to the ‘‘ Redemption,” “ Elijah,” 
should make their account with the weather betore- | ‘ Judith,” and ‘‘The Messiah ’’—a selection which, 
hand; conciliating the cyclone and terrifying the | while not attractive to the quidnunc, has doubtless 
tornado. Here is a recent report from the land of | been made with due regard to local preference. In 
big things in climatic disturbances: ‘“ News from|the evening programmes are Beethoven’s Eighth 
Jacksonville, Ill., reaches us of how a chorus jubilee | Symphony, Grieg’s Suite ‘ Peer Gynt,” Schubert's 
was broken up and became panic stricken in that |Symphony in B minor, and the ‘Golden Legend.” 
place, on June 11. The jubilee was gotten up and|It is to be hoped that the dissensions unhappily 
arranged by Professor Harris, a prominent teacher of | existing among musical Bristolians will not operate 
choruses in that vicinity. About six hundred singers | against the success of the Festival. 
had been engaged and carefully trained, gathering | = 
from the country around for miles. Several soloists) ,. ‘ 
of ability had been secured from Chicago and other) THE Globe has drawn attention to Ma ges se 
cities. The programme was scarcely under headway, ceeding on the part of — ~~ Pood Put ae 
when a violent storm suddenly came up and in an| What is called the ‘Educational loo und As 
instant everything was confusion. The lightning | well as giving free dinners to poor children in 
was vivid and the thunder tremendous. Inthe midst | Board Schools, they require the small guests to 
of it the immense tent under which the jubilee was learn and sing ditties bearing upon the much 
held was blown down, carrying to destruction piano- neglected snags of apes gg egies a. 
fortes and organs, and crushing men, women, and One of these effusions begins, ne —. oO ‘hi 9 
children together in a heap. It was almost mira- | Weat- meal bread ’—an oe e fact, which, 
culous that no one was killed, but a great many were | with ¢ others of a like nature, ig aa e€ put 7 wc 
severely bruised and few escaped without receiving |'MSenious ways by educational tood-poets. Here is 
injuries.” Our own climate is not one of ideal |# contribution of our own : 
perfection, but it never does anything like this. ie eagles bnemmcy itis Then

And only half the price

Tue (Canadian) Musical Fournal has the following: 
**The Last Night at Bethany,’ by Mr. C. Lee Williams, 
is said to be a most beautiful work, full of devotional 
music, good yet simple. Those who heard it during 
the last Lenten season in London pronounce it 
unequalled in devout and touching solemnity—a 
little idyll, with many charming touches. Who will 
be the first to introduce it to Canadians

An American musical journal is prepared for the 
rise, in America, of ‘*a school of composers of music 
who will unite, in the work they produce, the grandeur 
of Bach, the mathematical ingeniousness (!!) and 
originality of Beethoven, and the genius of Wagner 
without the latter composer’s controversy-creating 
characteristics.” This is a large order upon the 
future, and we hope it will be honoured

Tuts is a clipping: ‘An old gentleman, speaking 
to a young lady and commenting upon her freshness 
and good looks, remarked: ‘Ah, my dear, may you 
long retain them. Yours is a happy period of life. 
You know nothing yet of the jealousies, the heart- 
burnings, the contentions, the rivalries that beset the 
pathway of existence.’ ‘Don’t I, though,’ she inter- 
rupted ; ‘I want you to understand that I belong toa 
church choir

PHILHARMONIC SOCIETY

THE final Concert of this Society’s seventy-eighth season 
took place in St. James’s Hall on June 28, and was, like all | 
its predecessors, well attended. Beethoven’s Choral Sym- | 
phony was the principal feature in the programme, and the | 
last piece, since nothing could well come after the colossus. | 
It is needless to urge that the artists composing the orches- 
tra, being of the ablest and knowing their task thoroughly 
well, were the best possible material with which to | 
prepare a consistent and adequate interpretation of the | 
work, or, at any rate, of its instrumental movements. | 
Upon the general result of his labours in this re- 
spect Mr. Cowen may be congratulated. The reading | 
may be compared with others, and points of difference 
appear, but, on the whole, the audience had reason to feel } 
satisfied. Many present retired before the vocal Finale. | 
We can hardly blame them, except on the point of respect | 
for Beethoven, since the setting of Schiller’s Ode must | 
always be more or less painful hearing. At the close of | 
the performance Mr. Cowen was the object of a demonstra- | 
tion inspired by the able work he has done throughout the | 
season, The “Choral” was preceded by Macfarren’s | 
spirited “ Chevy Chase,”’ Overture; Spohr’s Ninth Concerto, | 
beautifully played by Mr. Ysaye, and Costa’s Quartet ‘* Ecco | 
quel fiero istante,” sung by the artists engaged for the | 
Symphony—Miss Fillunger, Miss Hilda Wilson, Mr. | 
Humphreys, and Mr. F, H. Morton

The Concerts of the Society have again been so far | 
successful that no demand upon the guarantors is necessary | 
—a state of things upon which the directors have reason to | 
felicitate themselves

so full of characteristic beauty and freshness ought to be

appreciated, even by a Richter audience, which seems to 
pin its faith to Beethoven and Wagner alone. The Bay- 
reuth master was represented by the rather unsatisfactory 
‘* Faust’ Overture, Herr Richter’s popular selection from 
the “* Nibelung’s Ring,” and, for the first time, the scene 
between Eva and Hans Sachs, from the second act of 
‘“‘Die Meistersinger.” This melodious episode was sung 
to perfection by Mr. and Mrs. Henschel, the lady also 
winning much applause for her exquisite rendering of 
Liszt’s poetical song ‘ Die Loreley.” Mr. Henschel’s 
expressive duet, ‘‘O weep for those that wept,” sung by 
the composer and Mr. Andrew Black, completed the 
programme of this admirable Concert

The most successful season of these Concerts came to a 
triumphant termination on the r4th ult., the attractions of

Beethoven’s Choral Symphony and a Wagner selection

bringing together one of the largest and most brilliant 
assemblages ever seen in St. James’s Hall. It may be 
doubted whether the colossal Ninth Symphony has 
ever before received such a magnificent interpretation in 
London. Notonly were the orchestral movements superbly 
played, but the choir sang with surprising vigour, maintain- 
ing good intonation, even in the most cruelly trying 
passages. A large amount of justice was rendered to the

THE MUSICAL TIMES.—Aveust 1, 18go. 475

pianists to regard their instrument as an enemy deserving |in the programme, was not performed, owing to the non- 
of punishment is increasing, and must be checked, or | arrival of the second pianoforte for the accompaniments. 
pianoforte playing will no longer be numbered among the | It is said that Mr. Paderewski has undertaken the respon- 
legitimate branches of musical art. |sible task of training this gifted lad. If so, he should 
On the evening of this day Mr. Ernst Denhof, a new- | insist upon his removal from the too stimulating atmos- 
comer, presented himself at the Princes’ Hall, his entertain- | phere of the public concert-room for some years to come. 
ment however partaking of the nature of a Chamber 
Concert, the programme including vocal pieces, carefully 
rendered by Mr. C. Copland; violin solos, tolerably well ROYAL COLLEGE OF MUSIC. 
played by Mr. Max Reichel; and a Sonata in D, for piano- 
forte and violin, by Adolf Reichel, the father of the violinist.| Tue laying of the foundation-stone of the new building 
In this work the influence of Beethoven’s early manner is | of the Royal College of Music by the Prince of Wales, on 
so plainly discernible that the music cannot be said to|the 8th ult., was an important event in the history of that 
possess any intrinsic value. Mr. Denhof began badly, his| institution. There was a large gathering of artistic 
rendering of Beethoven’s Sonata in D minor (Op. 31, No. 2) | celebrities, and the large marquee under which the cere- 
being almost beneath criticism. Some piecesof Schumann | mony took place enclosed a great number of visitors. 
received more justice; and others, by Chopin, showed a| When the Prince and Princess of Wales, accompanied by 
good deal of feeling and intelligence on the part of the |their two daughters and the Duke of Edinburgh, and 
executant. A very obstreperous street organ may have | followed by the Archbishop of Canterbury, Lord Lathom, 
disturbed Mr. Denhof at the outset, and final judgment | Sir George Grove, and Sir Frederick Leighton entered the

as to his artistic capacity may therefore be reserved. |tent, the College orchestra, under Professor Villiers 
On the ist ult. Miss Else Sonntag gave a second | Stanford, began Beethoven’s Overture ‘‘ The Consecration 
Recital at the Steinway Hall. There may be reasons why | of the House.’ At its close the Archbishop offered a short

this very young lady should give public performances, but | prayer, read a Collect, and recited the Lord’s Prayer, after 
they are not apparent to ordinary musical folk. On this| which Mr. Samson Fox in his address recapitulated the 
occasion she played pieces by Liszt ina style calculated to | story of his own liberality, when he said— 
astonish the pupils of the Weimar virtuoso, and if Miss; ‘I have the honour of requesting that you may be 
Sonntag’s rendering of Beethoven’s ‘‘ Waldstein’’ Sonata | graciously pleased to lay the first stone of the building 
was correct, it is incumbent on all the recognised pianists of | which it is my privilege and pleasure to provide for the 
the day to resume their studies. ‘The Recital giver had the | College founded by your Royal Highness for the promotion 
invaluable assistance of Mrs. Henschel, whose singing was | of music in the United Kingdom. 
at any rate not open to cavil or criticism. ‘*Anxious as I was to assist as best I could in the 
The next to claim attention was Miss Jeanne Douste at | advancement of the cultivation of the musical art for the 
the Princes’ Hall, on the gth ult. This young performer, | benefit of my countrymen, I believed that I could most 
who first came before the public as an infant prodigy some | effectually do so by furthering in some practical way the 
dozen or more years ago, has shown steady artistic progress, | interests of the Royal College of Music. 
and may now be regarded as a thoroughly sound pianist ‘“My attention had already been called to the highly 
At her recent Recital she played Beethoven's ‘* Waldstein ” | satisfactory system under which its finances were regu- 
Sonata in a somewhat quiet but very refined manner, and | lated, and the favourable opinion which I formed of its 
she was even more successful in smaller pieces by Chopin, | management and efficiency was further strengthened by a 
Tschaikowsky, Grieg, and others. She also joined her | visit to the College itself, and by a performance by the 
sister, Miss Louise Douste, in some pieces for four hands, | pupils at a Concert in the Princes’ Hall on December 10, 
M. Réné Ortmans, a violinist possessing a refined if not | 1887. 
very vigorous style of execution, played some solos with} ‘‘A yet closer acquaintance with the College showed 
much acceptance. |me that its operations, successful as they were, were 
It was perhaps fitting that so remarkable a season of | hampered by the very inadequate accommodation pro- 
pianism should conclude with the appearance of a new | vided by the present house ; and after much consideration 
prodigy. No doubt the phenomenal success of little Hof-|of the subject I expressed to Sir George Grove, the 
mann three years ago induced many musicians possessing | Director, my desire to present to your Royal Highness, as 
talented children to force on their studies in order, if | President of the College, a sum of £30,000 for the purpose 
possible, to share in the rich harvest which seemed at | of a new building. 
disposal. The official statement with regard to Max| ‘‘After your Royal Highness’s acceptance of that gift, 
Hambourg, who appeared at the Princes’ Hall on the 12th | and the interview with which you were graciously pleased 
ult., is very simple. He is a native of Bogoutchar, in | to honour me at Marlborough House on January 12, 1888, 
South Russia, and two years and a half ago he commenced | a memorial from the President and Council of the College 
to study the pianoforte under his father, who is a professor | was presented to the Royal Commissioners of 1851, asking 
at the Moscow Conservatoire. That his studies have been for ground suitable for the erection of a new College 
altogether well directed cannot be said. In Bach’s Chro- | building, which finally resulted in the very generous offer 
matic Fantasia and Fugue he displayed wonderful manipu- | of the magnificent site on which we now stand. 
lative capacity; and in some pieces by Schumann, and| ‘Sir Arthur Blomfield was, with your Royal Highness’s 
Chopin’s Waltz in A minor the little executant evinced the | sanction, nominated to be the architect of the new 
possession of natural gifts which, if carefully developed, | buildings, and on May 15, 1889, the sketch of the elevation 
should in due course place him in the front rank of pianists. | and the corresponding block plan, prepared by him in 
Though somewhat feeble, his touch is remarkable for sensi- | accordance with the Director's requirements, were 
bility, and he seemed to feel the music with all the intelli- | approved of by the Building Committee of the College. 
gence and artistic perception of a mature performer. But| ‘As it appeared, however, that those plans could not be 
even in his best efforts Max Hambourg’s playing was | carried out for less than £45,000, it gave me much pleasure 
defaced by mannerisms, which at times were very un- | to place in your Royal Highness’s hands the whole of that 
pleasant, and his rendering of Beethoven’s Sonata in A flat, | amount on May 18, 1589. 
with the Funeral March, was a sorry exhibition. ‘| have now the honour to beg your Royal Highness to 
The second Recital, on the 21st ult., fully confirmed the | take the first step towards the realisation of my wishes, 
initial impression as to Max Hambourg’s remarkable | and to lay the first stone of what I trust may prove to be a 
natural gifts. The programme was less exacting, and the | home not unworthy of so important a national institution 
little pianist was consequently not so overweighted as on | as the Royal College of Music. 
the previous occasion. In Haydn’s Variations in F minor, “It may not be uninteresting to your Royal Highness 
some trifles by Chopin and Schumann, and two of Bach’s | to know that this trowel, which I have now the honour of 
Forty-eight Preludes and Fugues, he displayed really | handing to you, is made from the metal of the corrugated 
marvellous insight into the spirit of the music, even the | boiler-flues of the troopship Praetoria, which, owing to her 
excessive employment of the rubato and other exaggera- | possessing those appliances to her boilers which I had 
tions of style showing the sensitiveness of his artistic | then but recently invented, was enabled to convey the gist 
organisation. Mozart’s Concerto in D minor, which was | Highlanders to Durban for the Zulu war, in 1879, with

476

485

of praise for the artistic earnestness which she threw into 
her singing; her declamation was also excellent. The 
Concert closed with Beethoven’s rarely heard Overture 
known as ‘“‘ Leonora, No. 2,” the first of the four composed 
by him for his only opera. Professor Stanford conducted

The terminal examinations of the Royal College of 
Music finished on the 25th ult. The Council Exhibition 
of £20 for students of six terms’ standing was awarded to 
Gertrude Brown (pianoforte), Lilian Wright (violin), and 
Cyril Miller (organ). The Council Exhibition of £15 for 
students of three terms’ standing was awarded to Harold 
Phillips (organ) and Kathleen Thomas (violin). The 
Brinsmead pianoforte (presented by Messrs. Brinsmead) was 
awarded to Ethel Sharpe. The London Musical Society’s 
prize of three guineas for singing was awarded to Charles 
McGrath

ness. The class was originated by the late Mr. Richard 
Rickard, a master at King Edward’s High School here. 
Mr. Rickard, besides being a clever mathematician, was an 
accomplished and enthusiastic musical amateur, a virtuoso 
on the flute, an excellent violinist, and a ripe musical 
scholar. Another, and later, venture of Mr. Rickard’s was 
the establishment of the penny violin class, which at one 
time numbered 500 members; from that to the present 
School of Music at the Institute was but a natural transi- 
tion, and to no one was more credit due than to the genial 
gentleman who was lost to us on June 4 last

On Friday evening, the 11th ult., a Concert was given in 
the Exchange Assembly Room by the choir and orchestra 
of the Oratory, Edgbaston. The Concert was in aid of the 
Children’s Hospital, and the programme consisted of 
portions of masses and motets by Beethoven, Mozart, 
Hummel, Cherubini, Niedermeyer, Reissiger, Costa, 
Fihrer, and Gounod; and also included a Salve Regina 
and an Ave Maria, by Mr. William Sewell, the talented 
Organist of the Oratory Church, who acted as accompanist, 
the Rev. R. G. Bellasis conducting

There was a special service at the Stratford Road Baptist 
Church on Thursday evening, June 26, when a new organ 
was opened by Mr. W. Astley Langston, vocal selections 
being given by Miss Mabel Grove. The organ, built by 
Messrs. Conacher & Co., to the specification of Mr. Lang- 
ston and Mr. Stockley, has three manuals and pedal, 
the great and swell organs having each ten stops, the choir 
six, and the pedal three. There are six couplers

Choral Union (Conductor, Mr. McArthur), which attracted 
a large audience by a programme of Scottish songs, 
and the Edinburgh Amateur Opera Company, under Mr, 
Charles Hamilton, which filled the hall at two perform. 
ances

A pleasant feature in the musical arrangements was pro- 
vided by Messrs. Broadwood, on whose invitation Mr, 
Townsend, Mr. Otto Schweizer, Mr. James Peter, and Herr 
Eugen Woycke gave Recitals on the splendid specimen of 
the firm’s pianofortes in the Grand Hall. The conditions 
are naturally not ideal for such an entertainment, and Mr, 
Townsend’s usually brilliant playing suffered from the 
depressing atmosphere. Mr. Schweizer resisted the 
influences of his surroundings better, and his playing of 
his own ‘“ Valsette’’ and his new set of ‘* Scotch Songs and 
Dances ’’—some of them beautifully arranged—was much 
appreciated. Herr Woycke was assisted by his son, Victor, 
and thus was able to afford the variety of Violin Sonatas 
(Schubert and Beethoven). Herr Woycke played selec- 
tions from his own compositions and those of Chopin and 
Liszt

Of all the bands which have been hitherto engaged, that 
of the Belgian ‘‘ Guides’’ has proved the favourite. Pro. 
bably the ill treatment they received at the hands of those 
in charge of their movements helped their magnificent 
playing to evoke most unwonted enthusiasm. Their pro

moters of the Concert may be congratulated upon a 
success both artistic and financial

Sir CHARLES AND Lapy Hat Lé have been received 
with enthusiasm in Australia. The Melbourne Australa- 
sian says: ‘“ The first appearance of two such artists 
as Sir Charles and Lady Hallé is of sufficient import- 
|ance to mark an epoch in the musical history of 
| Australia. On Thursday, May 22, the Town Hall 
| contained a large and brilliant assemblage. Sir Charles 
Hallé, who, on stepping on the platform, received 
an ovation which kept him bowing for some minutes, 
selected for his first performance in Australia Beethoven's 
colossal ‘ Waldstein’ Sonata. Three Chopin morceaua 
were also given—namely, the Nocturne in F sharp and the 
Valses in C sharp and D flat. Shouts of applause followed. 
and Sir Charles returned to the pianoforte and delighted 
|his listeners with Mendelssohn’s ‘ Bees’ Wedding.’ 
| Madame Norman-Néruda (Lady Hallé), the ‘Queen of 
Violinists,’ on making her appearance, received the same 
honours as her husband. Her solos were the Fantasie 
Caprice in A (Vieuxtemps), the Andante, with variations, 
from Mozart’s Sonata in F major, for pianoforte and violin, 
'and the famous ‘ Kreutzer’ Sonata (Beethoven), given to 
| the accompaniment of her husband

THE Dedication Festival week at the Church of St. 
Peter’s, Eaton Square, has for many years past culminated 
in a performance of an Oratorio with full orchestral 
accompaniments. This year, on Friday, the 4th ult., 
‘Elijah’? was given, under the direction of Mr. W. de 
Manby Sergison, Organist and Director of the choir, the 
orchestra consisting of some of the best English profes- 
| sional players, and the chorus of the members of the three 
parochial choirs of St. Peter’s. The solos were taken by 
pupils of Mr. Sergison’s. Mr. Charles Ackerman, of 
Westminster Abbey and St. Peter’s Choirs, whose reading 
|of the part of the Prophet was dignified and dramatic, 
exhibited very fully the capabilities of his voice. Mr. 
Gregory Hast did justice to the tenor solos, and the 
choristers of the church, Masters Simmonds, Wood, and 
Long, gave sympathetic readings of the soprano and alto 
parts. Mr. Noble (of St. John’s, Wilton Road) presided 
at the great organ. The choruses were attacked with a 
precision only attained by careful training. The whole 
| performance was characterised by smoothness, and the 
| demeanour of the immense congregation, as well as that ot 
| of the performers, was conspicuously reverent and attentive

THE Epping Forest Church Choir Association held its 
tenth annual Festival in Canterbury Cathedral, on the 5th 
ult. A special train was provided by the Great Eastern 
Railway Company from Loughton. The choir numbered 
about 300 voices, and the service was one of the most 
successful held by the Association. The Magnificat and 
Nunc dimittis were by Stanford, in B flat, and the Anthem 
was taken from Stainer’s ‘“‘ Daughter of Jairus”’ (** Awake, 
thou that sleepest”), the solo preceding it being very 
effectively rendered by Mr. Charles Strong. The Te Deum, 
sung at the close of the service, was Warwick Jordan's 
festival setting in C. Brass instruments were used in this 
and other parts of the service. Dr. Longhurst and Mr. H, 
Riding presided at the organ, and the whole of the musical 
portion of the service was under the direction of the 
organising Choirmaster and Conductor, Mr. J. W. Ullyett

Messrs. Lupwic AND WHITEHOUSE brought their series 
of Chamber Concerts, at the Princes’ Hall, to an effective 
conclusion on the 8th ult., the programme being attractive, 
and the audience large and enthusiastic. Dvorak’s genial 
and original Quintet in A (Op. 81) was vigorously rendered 
by the Concert-givers, assisted by Messrs. Collins and 
Gibson and Miss Fanny Davies, the pianoforte part being 
especially well played. Encores were freely asked and 
liberally granted for nearly all the instrumental solos, and 
Mr. Plunket Greene received warm applause for his artistic 
rendering of songs by Battison Haynes, Stanford, and other 
composers. Beethoven’s Quintet in C (Op. 29), excellently 
played, completed the programme

THE Princes’ Concert Society, one of the best of the 
many musical clubs at present in existence, gave its last 
Concert until October on the 24th ult. The Concert was 
under the direction of the honorary musical Conductors, 
which were for this occasion represented by Messrs. 
Herbert Thorndike and Alfred Caldicott. Tie programme, 
which was of equal merit to those of the previous Concerts, 
was varied and of sustained interest. Much pleasure was 
given by the artists, who were Mrs. Dyke, Mrs. Helen 
Trust, Mr. William Nichol, Mr. Arthur Oswald, Mr. 
Herbert Thorndike, with Mr. Tivadar Nachéz as violinist, 
and Mr. Raphael Roche, accompanist. The audience was 
numerous and highly appreciative

The municipality of Vienna have opened a competi- 
tion for the monument to be erected in that town to the 
memory of Mozart; prizes of eight thousand, one 
thousand, and five hundred florins respectively being 
offered for the three most approved designs

Two very successful performances of Beethoven’s 
‘‘ Fidelio”? were given last month at Homburg by pupils, 
past and present, of the Raff-Conservatorium, of Frankfurt- 
on- Main, under the direction of Herr Maximilian Fleisch

Two newly-discovered sacred numbers, from the pen of 
Franz Schubert, obtained a first hearing at the recent 
annual meeting of the German Allgemeine Tonkinstler 
Verein, held at Eisenach. The compositions are a Tantum 
ergo and an Offertory, both for chorus and orchestra, and 
emanating from the year 1828; one of the most produc- 
tive, as it was also the last, in the career of this prolific 
master

Biacksurn.—At St. Peter's Church, on the Dedication Festival, 
June 29, anew Communion Service, composed by the Or t 
of the Church, Mr. Wm. Tattersall, was given, for the first time, by a 
select choir trained by the composer. At a later Service Tours’s Com- 
munion Service in F was sung

BOURNEMOUTH, 
Recital, on the occasion of opening the new organ chamber, was given 
by Mr. G. E. Lyle, Organist of Sherborne Abbey, on Monday, J une 
Programme: Overture to Artaxerxes, Arne; Romance, Richmond: 
Allegretto from the “ Military” Symphony, Haydn; Adagio from 
Sonata, No. 1, Beethoven; The Cuckoo and Nightingale Concerto (two 
movements), Handel; ‘Songs in the Night,” Spinney; Gavotte 
D major, Bach; Andante from the “Surprise " Symphony, Haydn; 
Offertoire in E minor, Batiste; Nocturne in G minor, Chopin; 
Andante from Sonata in C, Mozart; The Abbey March (from the 
‘“* Abbey Chimes "), G. E. Lyle

Curist Cuurcu.—The local district Festival in connection with the 
Union was held at Christ Church, on Wednesday, the 16th ult. Th 
service was fully choral. There was a very large choir, composed of 
representatives from the other churches in the neighbourhood. The 
*, H. Hitchens, of Cantert 
whilst the organ was presided over by Mr. R. K. Simons, Or 
The Preces and Responses were accord 
the Exeter use. Special Psalms were used, the chants beit 
Arnold in E flat, Hayes in F, and Elvey in E flat. The Magn

Thom

SAWBRIDGEWORTH.—An Organ Recital was given in the Parish 
Church by Mr. A. C. Edwards, Organist of St. John’s, 
Thursday, June 26, and repeated with some additions the following 
day. The programme included some vocal and violin solos, and the 
choir of the church sang two choruses from “Hi: avdn's Creat ion, under 
the leadership of Mr. J. Bell, Organist and Choirmaster The organ 
pieces were Barcarolle, Fourth Concerto(W.S. Bennett), 
alla Handel (Guilmant), Andante in D (Silas), Fugue 
March in B flat (Silas), Postlude in C (Smart), I 
(Salomé). The whole of these were played by Mr. 
attentive and appreciative audience. Miss F oh r 
buted Air in G (Beethoven) and an Air by Choy 
sang “O rest in the Lord” and “ But the Lord is min tf 1” 
power and expression

in D (Bach), 
Cheur in G 
tdwards to an 
violin) c ntri- 
s Wightwick

