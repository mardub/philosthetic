it be no use , however , our take the line that 
 one can think rather little of a certain work of a 
 composer and yet not be ' anti ' that composer . 
 the out - and - out Stravinsky partisan , themselves 
 man of extreme view , could not conceive how 
 other people could be man of moderate view . as 
 we do not accept all the late work of Stravinsky 
 as plenary inspiration , it follow that we must be 
 swear foe of Stravinsky in everything . so it 
 come about that there be war in heaven . as 
 little of Stravinsky have be perform outside 
 London , the ordinary lover of music must 
 sometimes have long for someone to tell he all 
 about the war , and what we slay each other for 

 order that the little one may become conspicuous . 
 there must be a complete transvaluation of value . 
 | art have be " serious " too long : now let we play 
 | the fool , in season and out of season ; let we deny 
 everything , turn all our value upside - down . ' * for 
 the " late fiction " public , Shakespeare be out of 
 date and unreadable : for its musical counterpart , 
 Bach be a fossil and Beethoven a mummy . ' a 
 certain section of critic be always after the stunt 
 and the scoop ; and as there be no stunt and no 
 scoop to be get out of the composer of the past , 
 whom everyone know , resort be have to a few 
 composer of the present who be not widely know , 
 the hope be that the current ignorance of they 

 SS ee a ere 

 the MUSICAL TIMES — Novemser 1 1920 731 

 if we want the pure milk of the Stravinsky | show he the library . ' as for the theory of the 
 dogma , however , we must go not to Mr. Evans /mew music , Mr. Henry really have no need to argue 
 but to Mr. Leigh Henry . his be a curious mind,| with we . unlike he , we do not worry in the 
 that apparently be quite insensitive to much of the| least about theory ; all we be concern about 
 great music of the world . he especially abhors| be whether this or that work be a decent specimen 
 Elgar , Beethoven , and Brahms : no one but a| of its own theory . 
 comic artist could do justice to that afternoon in | Mr. Henry imagine that he have to insist on the 
 Mr. Leigh Henry ’s life when , as he tell we , flee ! principle that music can now be write in a way 
 from one concert - room to escape the boredom of | different from the old . we agree ; with the 
 Handel and Brahms , he enter another only to ! proviso that the Stravinsky way be still only one 
 find — Beethoven ! for Mr. Henry , Elgar be | way among other . let I quote Mr. Henry ’s 
 vulgar , banal ; Brahms ’s first Pianoforte c oncerto | own statement of what Stravinsky have be do , 
 be ' intolerably dull and utterly platitudinary’;|or try to do , in some of the work that be 
 seethoven ’s seventh symphony be * bombastic ak | give at M. Ansermet ’s concert 

 P ~ 3 . ' « ~ 
 sententious ' ; and so on and soon . iti be open to Flere [ in the ' these Pieces for String Quastet ' ? 
 Mr. Henry , of course , to reply to this , Yow say . ; . 
 : ' Stravinsky break finally with the academic theory of 
 you do not think much of certain music . how| ~ ° : Acie i : 
 : , , , instrumental writing . the old form be entirely 
 then can you deny I the right to think little of . . rave : 
 ee te . . ae scrap , with all their convention : the stereotyped 
 corn other music be we do not deny he the procedure of thematic development be replace by a 
 right , however ; we simply hazard the opinion that broad tonal design base on sound - colour quantity : 
 when most of the good musician af the leet Ghyll . ie cent of ' ceiin " and ' tenietes ' 
 or a hundred year say one thing and Mr. Henry | subject - matter be supersede by a decorative juxta- 
 another , it be at least not improbable that it be position of instrumental colour , derive from a 
 Mr. Henry who be wrong . for we who disagree particularised observation of the individual timbre- 
 with he have this advantage over he , that whereas| quality of each : and from this treatment each 
 he be blind to the virtue of a great deal of the } ' movement ' of the ' three Pieces ' obtain a unity 
 sort of music that ze like , we be quite as appre- approximate to what one term ' the balance of 
 ciative as he be of a great deal of the sort of value in painting ' : the old unimaginative ( szc ’ ) 
 music that 4e like . our musical sense , we ! symmetry of theme - repetition and response be lay aside . 
 venture to think , be a little more comprehensive 
 than his . we do not find a liking for Stravinsky| there be nothing so startlingly new to we in all 
 or Bela Bartok incompatible with a liking for|this as Mr. Henry imagine . we have be 
 Cimarosa , the tender charm , grace , and wit of | familiar with it for year in painting , and we have 
 whose music have no more effect on Mr. Leigh ! watch various attempt to practise it in music . 
 Henry than a fairy - tale would have on a horse . |it be natural that the grow impatience 
 Mr. Henry hear only the note : they lead to no of all of we with roundabout process should 
 imaginative reaction in he . he remind I of|lead musician to ask whether much of the 
 the gentleman in the poem : /once customary connective tissue of music could 
 not be do away with , whether the line of form 
 could not be abstract and simplify as they 
 /have be in painting , whether harmony could 
 /not be press close together , whether orchestral 

 well , what be it that this gentleman of obviously | colour could not take over some of the function 
 limited imagination be storm poly syllabically at | formally allot to line . as I have sad already , 
 us about ? he have a theory of what music should| we have an open mind for any new y theory or 
 and what it should not be : it be the theory of his little ' method : all we ask be that the result shall be 
 franco- russian - italian clan . music should not something more than an illustration of the theory , 
 be ' literary , ' and it should not build itself up in| — that it shall be an art - work that justify itself to 
 the way the old symphony do . as this means| our ear and our brain . we watch with respectful 
 scoring off the slate a great deal of the music | interest the experiment of the great man , with- 
 that we think among the fine ever write , we| out always think the experiment a success . 
 be sorry we can not oblige Mr. Henry to that| Schénberg have be try for some year to take 
 extent . his courteous reply , of course , be that we | short cut in harmony . we know that this have 
 who still think Beethoven and Brahms and Wagner | always be one of the way in which new chord 
 and Strauss rather fine fellow be merely old| have come to be add to the harmonic vocabu- 
 woman who ought to have die — or , at any rate,|lary . but it do not necessarily follow that 
 go into a musical Home of rest — in August , | because the telesc oping of some progression have 
 1914 . he picture we as hopeless conservative | ' be justify by time that every piece of such 
 who know nothing of the new music . to that | telescoping will be justify . as ~ Autolycus ’ 
 I can only reply after the manner of gibbon’s| | point out in the October number of J / wsica/ 
 answer to the critic who doubt whether the | opinion , in a devastating reply to Mr. Leigh 
 ag have read all the book he have anienell Henry , we may dispense with connective word up 

 the Decline and Fall ' : " if Mr. Leigh Henry| to a point with a real gain in concision and force , 
 will call some day when I be out , my servant shall | but it be easy to go beyond this point and become 

 they be there in nature , waiting| perhaps to cover their nakedness . in rag - time it be 

 not the ' rag ' that bore we to desolation ; on the 
 contrary , it be the poor old shred of a conventional 
 musical sentence that be be trick out and 
 rag just because it be so appallingly wear out . 
 we be in the position of harmonic builder pre- 
 eminently to - day , as they be in the early 17th 
 century , but with a vastly great task ahead , and 
 a vastly great heritage behind we . it be not 
 necessary to scrap the minor and major scale ; we 
 need only to realise that they be two out of exactly 
 four hundred and sixty - two seven - note sounding , 
 not one of which be without its significance , not one 
 without its serviceability to a master - builder , though 
 probably no other two be more worth future 
 composer ’ attention than the two upon which Bach 
 build his ° forty - eight , ' Beethoven his © nine , ' and 
 Wagner his ' Ring . ' let we take out of ou 
 treasure thing new and old , since all real musician 
 necessarily become ' disciple of the Kingdom 

 the R.C.M. 
 such Foundationers class of three ( 3 ) in one 
 hour , each class twice a week . it be under 
 st od : 
 ( a. ) that I adopt class - teaching ; 
 ( 4 . ) that the grouping of the pupil in each 
 class depend upon myself ; 
 ( c. ) that the above - name two hour a week 
 for each class constitute the minimum 
 of time require , and that subsequently , 
 accord to circumstance , an increase 
 of time for tuition may be requisite . 
 " upon this footing the maximum of time I 
 should be able personally to devote to the say 
 class - teaching would be four day a week , three 
 ( 3 ) hour each of the four day . consequently as 
 a maximum I would teach 18 pupil in six class 

 Fontaine — Gounod ’s ' Faust ' Ballet music , and 
 Chaminade ’s ' Interméde . the Chief Commoner , 
 Mr. Deputy Harry Bird , also sing . we _ wish 

 musical performance by civic dignitary be so 
 common as to call for no remark . when our Mayors 
 and Mayoresses as a body can do their part in the 
 musical side of municipal social gathering , we shall 
 find the art receive from local govern body the 
 encouragement due to so powerful a factor in the life 
 of the community . and we shall be spare such 
 futility as this little bit of dialogue quote by 
 Mr. Scholes in the Odserver of October 17 : 
 councillor — — plead that the new 
 should give light music than the old orchestra . 
 the chairman : that will necessarily be so with 
 the small number of instrument . 
 as Mr. Scholes say , if the musical resource be 
 still far reduce to a mere body of four string 
 player , Councillor Dash would no doubt be happy 
 to hear such light music as a Beethoven quartet . 
 municipal music be an old theme of discussion and 
 by no means new as a practice . it have have to meet 
 opposition , amount at time to angry clamour , 
 from non - musical ratepayer and the councillor who 
 mentally represent they , and now and then it have 
 fall before the onslaught . there be centre where 

 septet 

 a few other concert must be mention briefly . 
 the Chopin recital of Miss Myra Hess and Miss 
 Irene Scharrer serve to begin the season well from 
 the point of view of native executive work . a recital 
 have also be give by Mr. Mark Hambourg , and 
 naturally draw an overcrowded audience to Wigmore 
 Hall . M. Siloti make a welcome reappearance 
 after many year . as he be make some more 
 appearance next month , criticism may be defer . 
 the recital of Captain Herbert Heyner still far 
 consolidate his position 

 the playing of the Flonzaley Quartet at its only 
 concert on October 18 be as near perfection as 
 possible . it be true that these artist have have special 
 opportunity , and that our own quartet with equal 
 chance would play as well from the point of view of 
 technique and with a little more individual impulse , 
 but the fact remain that they be unapproachable at 
 present . their playing of Beethoven ’s posthumous 
 Quartet , Op . 135 , be a marvel of lucidity and 
 extraordinary in its combination of strength and 
 beauty 

 one curious little concert remain to be mention . 
 Messrs. Rudall Carte & Co. invite a party of friend 
 to hear a combination of twenty - four flute , play 
 by some of the good flautist in London . the 
 instrument be make for a band in the North of 
 Ireland , where they will doubtless play their part in 
 the political crisis . ' the variety of tone - colour which 
 can be produce by sucha combination occasion far 
 more surprise amongst student of orchestral timbre 
 than the fine dynamic shade which the player 
 command 

 Letters to the Lditor 

 the IDEAS of M. VINCENT D’INDY 
 Sir,—I find a great mistake in the translation of my 
 little work , ' Les idées de M. d’Indy ' ( .l / ustcal 7imes for 
 August , p. 524 ) . I read : 
 when Beethoven write 

 we find 

 ti the contrary ! 
 the second writing be Beethoven ’s ; the first ( alter ) be 
 Mr. Fred Gostelow , Luton Parish Church — Fugue in D , | the writing of M. d’Indy.—Very truly yours 

 C. Satnr - SAENs , 
 Rue de Courcelles 83 47 , Paris 

 Obituary 
 we regret to record the follow death 

 James RICHARD SIMpsON , of the well - know Edinburgh : 
 firm of Methven Simpson , Ltd. Edinburgh owe much to 
 his long - sustain activity in concert - giving . he promote 
 the Edinburgh Classical Concerts which continue a healthy 
 and useful existence from 1906 to the war , and have since 
 be resume . the Beethoven Festival under Balling be 
 a development of this series 

 MAX Brucu , bear in 1838 , composer of symphony , 
 concertos , oratorio , a large number of small choral work , 
 and opera . he be know in this country chiefly by his 
 frequently - play Violin Concerto in G minor , and in a less 
 degree by his ' Kol Nidrei ' for violoncello and orchestra , and 
 a Violin Concerto in D minor . from 1880 to 1883 he live 
 in England as conductor of the Liverpool Philharmonic 
 Society . in 1893 he receive an honorary degree from 
 Cambridge University 

 indication be not want that a degree of activity be 
 be infuse into the musical life of the city , and already 
 there be augury for a successful season 

 the Pump Room concert season open the last week in 
 September , and it be specially interesting to note that the 
 musical director , Mr. Jan Hurst , be arrange a series of 
 chamber concert for Thursday afternoon , in addition to the 
 daily musical routine . the programme for the first of these 
 concert , September 30 , include Dvorak ’s String Quartet , 
 Brahms ’s Quintet for pianoforte and string , and a Sonata 
 by Rubinstein for pianoforte and ’ cello ; while on the 
 follow Thursday , October 7 , Arensky ’s Trio ( Op . 32 ) for 
 violin , cello , and pianoforte , Mozart ’s String Quartet ( no . 13 ) , 
 Schumann ’s quintet in e flat ( Op . 44 ) , for pianoforte and 
 string , and Beethoven ’s sonata in F for violin and 
 pianoforte , be perform 

 on October 8 , at the Pump Room , under the presidency 
 of Major - Gen. L. J. E. Bradshaw , C.B. , the inaugural 
 lecture of the Bath centre of the British Music Society be 
 give by the Rev. Dr. Edmund Fellowes on * the english 
 Madrigal Composers . ' no less interesting and instructive 
 be the follow madrigal , illustrative of the lecture , sing 
 by a choir under the conductorship of the Abbey organist , 
 Mr. A. E. New 

 Mark Hambourg ’s Pianoforte Recital in the Town Hall 
 on October 4 unfortunately clash with the Beecham 
 opera , a fact which no doubt account for the smallness 
 of the audience . the concert - giver be in his good form , 
 and have rarely be hear here to great advantage 

 the first of a series of five chamber concert under the 
 auspex of the Birmingham Chamber Concert Society , the 
 executive again be the famous Catterall String Quartet , 
 take place at the Royal Society of Artists ’ Exhibition Room 
 on October 11 . the quartet choose be Beethoven ’s 
 in A minor , Op . 132 , Arensky ’s in G major , Op . ii , and 
 Brahms ’s Op . 51 , no . 2 , in A minor . no fine choice could 
 have be make , and the performer play con amore 

 some excellent and artistic part - singing by Mr. W. E. 
 Robinson ’s splendid choir be hear at the Midland 

 at the annual meeting of the Coventry Musical Club a 
 satisfactory balance in hand be report . the official 
 be all re - elect . the resignation of Mr. E. Kelsey , 
 conductor of the club ’s orchestra , be receive with regret 

 the Armstrong - Siddeley Orchestra , under the baton of 
 Mr. Mathew Stevenson , give its first concert at Parkside 
 on October 16 , the programme include ' Ballet Russe ' 
 ( Luigini ) , Pizzicato ' Sylvia ' ( Délibes ) , ' Finlandia , ' and 
 ' La Fileuse ' ( Mendelssohn ) . Miss Anne Godfrey contribute 
 violin solo , the vocalist be Mrs. Aron , and Mrs. Gordon 
 Vickers - Jones act as accompanist . the programme to 
 be give at the four forthcoming concert of the Coventry 
 Chamber Music Society include quartet by Elgar , Haydn , 
 Dvorak , Beethoven , and Debussy . in addition , Mozart ’s 
 Quintet in G minor be to figure in the first programme , and 
 Beethoven ’s Trio in b flat and Tchaikovsky ’s ' elegiac ' 
 Trio be to be hear at subsequent concert . the vocalist 
 engage include Miss Margaret Harrison and Mr. Herbert 
 Simmonds 

 musical affair in the district be in an active and healthy 
 condition . Rugby Amateur Operatic Society be to perform 
 ' the Gondoliers , ' while the Philharmonic Society intend 
 to perform three notable work in Brahms ’s ' Song of 
 Destiny , ' ' bl pair of siren ' ( Parry ) , and Stanford ’s 
 ' the revenge . ' the last - name work be also to figure in 
 the programme of the Kenilworth Choral Society . Mr. 
 Herbert Morris , organist of St. David ’s Cathedral , give an 
 interesting organ recital in Kenilworth Parish Church on 
 October 5 . Nuneaton amateur have ' the mikado ' next 
 on their list for production . there be also promise 
 concert from Leamington organization . with so many 
 varied arrangement in view the season , which have open 
 so successfully in Warwickshire , should prove very 
 satisfactory 

 a welcome innovation in the series of weekly popular 
 concert in Plymouth Guildhall be make on October 9 , 
 when Mr. Charles Tree give a lecture on ' song and how 
 to sing they , ' illustrate by himself and Madame Nellie 
 Stephenson , with Mr. H. Moreton at the organ and 
 pianoforte 

 on September 30 the string band of the R.G.A. , 
 Plymouth Division , give two concert to raise fund for the 
 widow and child of Band - Sergeant - Major Coventry , who 
 recently die very suddenly . under his direction the 
 standard of the playing have be fully maintain . the 
 former bandmaster , Mr. R. G. Evans , now Lieut.-Director 
 of Music , Coldstream Guards , come to conduct some of the 
 item , the remainder be direct by Mr. H. Eldridge , 
 the newly - appoint bandmaster . Sullivan ’s ' in memoriam ' 
 overture , Beethoven ’s Symphony No . 1 , the ' Casse 
 Noisette ' Suite , and Schubert ’s . ' unfinished ' Symphony 
 be in the programme . Mr. George East play the solo 
 in Mendelssohn ’s Violin Concerto , and Miss Copner be 

 conductor 

 Mrs. Ernest Bullock , wife of the organist of Exeter 
 Cathedral , be the pianist at a pianoforte and vocal recital 
 at Sidmouth on October 7 , the event be one of a series of 
 concert organize by an enterprising committee of lady . 
 Mrs. Bullock play the Schumann ' Papillons , ' a Brahms 
 Scherzo ( e flat minor ) , a Nocturne by Grieg , and close 
 with a somewhat startling but attractive novelty by Albeniz , 
 a ' Féte de Dieu de Seville . ' Mr. Steuart Wilson sing a 
 group of german lieder in English , and among other thing 
 two song by Armstrong - Gibbs ( ' the linnet ' and ' the 
 bell ' ) , and ' holy Thursday , ' by Charles Wood . Miss E. 
 Martin accompany 

 a series of fine ' international Celebrity ' concert have 
 be arrange for Torquay and Plymouth . the first of 
 these be a recital by Pachmann , give at Torquay on 
 October 9 , and repeat at Plymouth on October 11 . the 
 Chopin number be representative of the less hackneyed 
 item ; the Beethoven Sonata in D minor , Op . 31 , no . 2 , 
 be beautiful , but not satisfying in interpretation , and the 
 same may be say of Liszt ’s * rhapsodie Hongroise , ' no . 8 

 at Plymouth , the Orpheus Choir conduct by Mr. 
 David Parkes give an electric and wonderfully conceive 
 reading of Elgar ’s ' the reveille , ' a quiet piece by 
 David Parkes , ' when twilight dew , ' and Boulanger ’s 
 ' Cyrus in Babylon 

 miss 

 song contribute by well - know local vocalist , among 
 | whom have be Miss Nancy Weir , and Messrs. Philip 
 | Malcolm and Robert Walker . the proceed be to be 
 | devote to the fund of the Infirmaries . an event worthy 
 of record be the first performance on any stage of ' the 
 | Hawaiian Maid , ' a comic opera by Mr. Purcell J. Mansfield , 
 the accomplished organist of Paisley Abbey . Mr. Mansfield ’s 
 tuneful work be creditably perform by the Glasgow 
 Operatic Society at the Theatre Royal during the week 
 October 11 - 16 , our distinguished townsman , Mr. Frederic 
 Lamond , give a pianoforte recital on October 13 , his 
 programme include a Chopin and a Liszt group , a 
 Beethoven Sonata , and the Brahms Variations on a theme 
 by Paganini . at his return visit in December , Mr. Lamond 
 will play a Beethoven programme . with the exception of 
 the Flonzaley ( Quartet concert at the Royal Institute of Fine 
 Arts on October 7 , other music - making of the month 

 have be chiefly ballad concert , under the auspex of the 

 on October 14 the Hallé season open , a very full 
 audience be present . Mr. Hamilton Harty have take 

 control , and we find an orchestra approach pre - war| when a choir of seventy man draw exclusively from its 
 size . the various lady who come to the rescue during|employée sing typical miniature such as Hegar ’s 
 1916 - 18 have now retire in favour of the man who have | ' Phantom Host , ' MacDowell ’s ' dance of gnome , ' Xc , 
 return to peaceful pursuit . Mr. Arthur Catterall be still ! like the Quinlan concert , however , its main interest be 
 leader until the spring . when he go to the Boston | instrumental , Miss Pierce and Mr. Chas . Kelly , formerly 
 Symphony Orchestra . we miss Mr. Walter Halton as first | contemporary student here , be associate in two new and 
 ‘ cello . the wood department be as last year , and at the| highly interesting work for two pianoforte , deserving of 
 opening concert these player quite surpass them-| wide recognition — Arnold Bax ’s ' Moy Mell ' and Mélan 
 self . the ' Meistersinger ' Overture , Beethoven ’s no . 7 , | Guéronet ’s ' Tourbillon . ' Miss Mignon Nevada and Mr , 
 and Strauss ’s ' Don Juan ' provide work of nobility and| Edgar Coyle be the vocalist , 
 splendour , and the geniality of the Strauss reading be most in town surround Manchester , musical life seem this 
 surprising consider that it have not be touch for seven | autumn to have awaken rather early , quite apart from 
 year . Hlarty ’s ' Meistersinger ' incline rather to pomposity , | the visit of tour concert - group . at New Mills , a little 
 and overwhelmed rather than thrilled , lack as it do | manufacture village on the Derbyshire border , but still 
 that vital magnetic quality easier feel than explain . | under mancunian influence in matter artistic , a flourish 
 the Beethoven be worthy to follow a great tradition , | orchestral society direct by Mr. J. Walters Baguley , bring 
 and ultra - classicist in our midst will not be able / to the artizan and residential people music of worth and 
 to cavil at Harty ’s Beethoven . it be all sane and | distinction . ' Scheherazade ' in this upland manufacturing 
 well - balance , and we get genuine imaginative //anzssi7mos | little town be certainly something to make one rub his 
 that be rathe : wonderful so early in the season , because | eye . 
 there be frequent evidence that the band have not yet at Bolton the Choral Union now hasan amateur conductor 
 * find itself . " Miss Agnes Nicholls be the incomparably | of distinction in Mr. John Booth , one of the town ’s iron- 
 great romantic artist in Weber ’s ' Ocean ' aria , and in the| master . man of this stamp , combine ardour , culture , 
 negro ' spiritual ' or plantation ditty of the cotton fields| and mean , do not stick at trifle ; . on October 15 Mr. 
 ( especially ' swing low , sweet chariot ' ) , reveal a hitherto | Hamilton Harty and the Hallé bandattende , and give the 
 unexpected strain of emotionalism . I hear this very| ' Mystic Trumpeter ' and Strauss ’s ' Don Juan ' as well . 
 song do by a quartet of negress on a hot October ! Mr. Charles Risegari conduct here another society , and a 
 afternoon in a well - wooded grove adjoin a cotton/ flourish amateur orchestral society exist under the 
 plantation last year in Georgia . it then have a haunting , | guidance of Mr. Andrew Morris . 
 plaintive quality . unlike some of the more famous the municipalization of the Hallé band continue to 
 plantation ditty , it do not strike I as be so well able | excite interest , judge by newspaper correspondence . 
 to stand the transplanting process to our sophisticated | semi - municipalization have be advocate , and another 
 concert - hall . perhaps these thing be well leave to flourish | quite good idea moot would associate the band with the 
 in their native soil . County Palatine , with proportionate guarantee from the 
 the British Music Society have arrange some six evening | town visit , not omit the famous seaside resort , 
 during the winter , and be gradually exende its membership . | which provide a summer livelihood for many player . 
 on October 1 the Catterall Quartet play Tanéiév in 
 A minor and Dr. Ernest Walker ’s ' Phantasy ' Quartet 

 among recitalist prominence must be give to Miss NEWCASTLE - ON - TYNE and DISTRICT 
 Muriel Robinson for a discriminating series of Schubert oiee   aaeer ES Ye se 
 song , especially the ' song of the Mill , ' with clarinet the Newcastle Bach Choir promise an interesting series 

 the MUSICAL TIMES — Novemser 1 1920 775 

 quite unnecessary , but if it be possible to get a natal Beethoven ’s Sonata in D minor ( play with music on desk ) 
 declaration of the feature of the evening , many hearer | and a Chopin group have pride of place in the programme . 
 would probably indicate the violin solo of M. Melsa , whose | the enthusiasm of the audience be as great as ever , and 

 brilliant technique find ample scope in Wieniawski ’s | 
 polonaise in D major , and Paganini ’s ' Witch ’s dance 

 Quartet party appear ; and at the third , on October 10 , 
 Porth Madrigal Glee Party give its service under the 
 leadership of Mr. Joseph Bowen 

 the Cardiff Chamber Music Society open _ its 
 seventeenth season on October 13 at the Girls ’ High School , 
 when the Flonzaley String Quartet delight a keen and 
 appreciative audience with exquisite interpretation of the 
 quartet of Haydn ( op . 76 , no . 5 ) , Schumann ( op . 41 , 
 no . 3 ) , and Beethoven ( op . 18 , no . 6 ) . the Society 
 deserve the great praise for foster this class of music 
 in the city 

 other concert hold be : a miscellaneous concert , on 
 October 7 , at Bethania , Dowlais , with Madame Elsa Stralia 
 and Messrs. Walter Hyde and Herbert Brown as vocalist : 
 while to a crowded audience at Noddfa , Treorchy , on 
 October 11 , the celebrated Williamstown Male - Voice Party , 
 under the conductorship of Mr. Ted Lewis , and assist by 
 artist of local repute , perform a 
 successfully 

 this be the only series that pay its way last season , 
 | this year the eight concert be to be under the alternate 
 conductorship of Mr. Hamilton Harty and Mr. Goossens , 
 } and a safe but generally interesting programme have be 
 arrange , consist largely of well - know work , but 
 include a rather large proportion of more or less 
 unfamiliar thing . the Leeds Philharmonic Society be 
 play for safety , and give ' Elijah ' and ' Messiah , ' 
 reserve some interesting thing for its third and fourth 
 concert — Parry ’s * St. Cecilia ’s Day , ' Holst ’s ' Hymn of 
 Jesus , ' and Purcell ’s ' Dido and /Eneas 

 the Leeds Choral Union promise nothing new , but 
 3Jantock ’s ' Omar Khayyam ' and Verdi ’s ' Requiem , ' with 
 * Messiah ’ and Gounod ’s * Faust , ' should prove generally 
 attractive . the most important event of the Leeds season 
 will , for many , be the visit of the London String Quartet to 
 repeat its ' Beethoven Festival ' next January , and give all 
 the master ’s string quartet at a series of six concert 

 the Bradford Subscription Concerts promise to retain 
 their pre - eminence in the West Riding , and in addition to 
 the usual eight concert , at four of which the Hallé 
 Orchestra will appear , have arrange for a supplementary 
 series of chamber concert , in which the Flonzaley , London , 
 and Catterall ( uartet will take part . the Bradford 
 Permanent Orchestra , under the conductorship of Mr. 
 Julius Harrison and Mr. Julian Clifford , will give five 
 concert , and , as usual , will pay some attention to native 
 composer , in addition to include many familiar and 
 welcome classic 

 the concert season bid fair to become an exceedingly 
 interesting one , if we may judge from the prospectus which 
 continue to be issue . at all event there be a decide 
 chance of everything second - rate be push aside for the 
 rather prosaic reason that , on account of the rate of exchange , 
 artist obviously prefer earn their fee here rather than | 
 elsewhere on the continent . besides this , we expect to 
 hear a great many new musical work of importance 

 to celebrate the hundred and fiftieth anniversary of 
 Beethoven ’s birthday it be propose to inaugurate an almost 
 unique Beethoven festival comprise no less than twenty - four 
 concert . fourteen programme alone be to be devote to 
 the master ’s chamber music , in which will be hear the 
 whole of his string quartet , the pianoforte trio , the violin 
 sonata , and the violoncello sonata . the orchestral 

 composition , on the other hand , will be flank by the 
 * Missa solemnis , ' the Choral Fantasia , and the song - cycle , 
 " An die ferne Geliebte . ' an additional feature of interest 
 at the chamber concert will lie in the fact that some of the 
 most notable quartet and trio society have be engage 
 to alternate with our own . the list contain among other 
 the Quatuor Poulet ( Paris ) , the Rosé Quartet ( Vienna ) , the 
 famous ' Bohemians , ' and the Budapest Quartet . this 
 novel arrangement may cause a lack of homogeneity which , 
 however , will be amply compensate by the opportunity for 
 compare difference in the reading of Beethoven ’s work 

 not much can yet be say of the National Opera , the 
 headquarter of which be now at the Hague . information 
 have reach we from thence that a new opera by Eugen 
 d’Albert have be produce . it will probably be give at 
 Amsterdam in the course of a few week . it may be say 
 here that the term ' new , ' as apply to an operatic work of 
 d’Albert ’s , hold good only for a very short time , have in 
 view the tremendous rate at which this lightning composer 
 pour forth his idea 

 GRAHAMSTOWN , S.A.—On September 11 , the Grahams- 
 town Training College give a highly appreciated concert | 
 at which the work play and 

 sing represent the | 
 follow composer — Mozart ( the G minor Symphony ) , | 
 Grainger , Sinding ( a movement from the e minor Pianoforte | 
 ( Quintet ) , John Ireland ( part - song ) , Quilter , Godard , | 
 Beethoven , Ronald , Purcell , Coleridge - Taylor , Liszt , | 
 Cowen , Rubinstein , and Lederer . the high standard of 
 programme and performance be creditable to the College 

 Association on August 14 , the choir , under Mr. J. F. 
 | Proudman , sing Dudley Buck ’s ' hymn to music ' and an 
 | Sevenggeenant of ' the lass of Richmond Hill . ' an orchestra 
 j and a military band play a good selection , include the 
 | ' Peer Gynt ' Suite , Luigini ’s ' Ballet Egyptienne , ' and the 
 | * William Tell ’ Ballet music 

