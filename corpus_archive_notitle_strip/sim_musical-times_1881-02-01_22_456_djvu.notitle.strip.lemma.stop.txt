scribe scene , eloquent : — soul Schubert come 
 " imagine garden mile in| passionate invocation , soul love . 
 extent , tree num-| 1827 — year death — Schubert 

 berless castle country house ; imagine river | spend happy time Vienna , place 
 creep like serpent twine fold ; think } sojourn Gratz , guest 
 meadow plain cover , like carpet , | Dr. Madame Carl Pachler . Pachlers 
 lovely colour , grand massive | musical hospitable people ; 
 rock encircle like band ; lastly,|it curious circumstance , having 
 mile huge tree range row — girt| deprive Beethoven expected company 
 range lofty mountain , | master death , prevail Schubert 
 sentinel watch exquisite valley . | vacant place — soon follow 
 imagine faint conception } Beethoven world . Schubert 
 unspeakable beauty . " previous acquaintance Madame Pachler ; 
 remarkable thing Salzburg|he enjoy new friend 
 letter Mozart ap- | thoroughly , return Vienna bring fit 
 pear init . birthplace great composer , | depression pathos 
 Schubert , think , | plainness letter address late host : — 
 , , matter fact , ; ' begin find far 
 forget altogether . Schubert and| happy comfortable Gratz , Vienna 
 weep tomb comparatively insignificant man | exactly suit . certainly , 
 like Michael Haydn , look sign | big , account heart , 
 recognition Mozart ? true 1825 } sincerity , candour , genuine thought feeling , 
 Salzburg vast shrine dedicate } rational talk , utterly lack intellectual 
 great composer . statue Platz } achievement . ascertain exactly 
 near cathedral , gild inscription | people clever stupid , ’ deal 
 house Mozart bear | petty , poor gossip — real cheerfulness seldom , 
 live , public chime ring his|if , come . possible , doubt , 
 melody , bust portrait visible | blame , slow art 
 second shop , Mozart museum rank high } thaw . Gratz soon learn appreciate 
 " lion " place . Schubert |the absence artifice conventional way . 
 know illustrious predecessor | stay long , course , 
 little city immortal , site , to|more profoundly penetrate happiness 
 musical pilgrim , holy ground . lose our-|such perfect freedom constraint . come 
 self vain conjecture silence . ! | particular , shall forget happy time pass 
 puzzle hard solve . |with dear wife , sturdy Pachleross 
 subsequent letter Schubert describe | small Faust . ' 
 force mountainous scenery Austrian | clearly , word , 
 bavarian Tyrol , | man weary struggle cold heartless 
 quote need characteristic extract , | world , longing rest petty care , 
 reference Pass Luez : — | face trouble blessed taste 
 ' painfully slowly crawl high | sympathy love . 
 mountain , huge mountain | letter Schubert date fatal 
 , think world | year 1828 come , , cha- 
 nail board , look , | racteristically , appeal composer 
 having scale high mountain , | behalf brother Carl , painter . ad- 
 fearful ravine , , time , | dress Herr Anselm Hiittenbrenner , Gratz : — 
 feel palpitation heart . recover little } ' ' surprised writing . 
 shock , mighty rampart | , , write , motive self- 
 rock , , distance , shut like a| interest . , listen . place drawing- 
 blind alley , puzzle head vain| master Normal - Hauptschule Gritz , 
 think possibly outlet or|see , vacant , candidate summon . 
 passage . amidst awful scene nature man | brother Carl , possibly know , like 
 seek perpetuate memory } post . clever landscape - painter 
 dreadful inhuman action . } good draughtsman . help 
 Bavarians Salzach , / matter eternally oblige . 
 Tyrolese , river roar deep } brother married man child ; 
 , inflict dreadful murderous } obtain regular salaried employment ex- 
 slaughter , whilst Tyrolese , secrete holes| tremely agreeable ... . repeat request , 
 rock , utter hellish cry , fire | bear mind kindness 
 3avarian , strive win Pass , } brother look . " 
 fall wound abyss month later Schubert die , earth 
 shot proceed . shameful action , which}the poor loss good gifted , 
 continue day week , try } certainly ' ' enemy , " 
 mark build chapel bavarian | enemy modified 

 72 

 quartet symphony , | day hold imperative musical 

 introduce minuet , usual credit 
 composer merit introduction . dance- | 
 movement , , meet | 
 Allegro , Adagio , Presto " sonata " a| 
 remote period ; doubtless novel | 
 artistic treatment bestow Haydn | 
 special dance - movement , lead perma- 
 nent adoption quartet Symphony . | 
 , , Suite , | 
 refer , furnish suggestion , | 
 , , derivative , Cassazione | 
 Divertimento , combination numerous movement , | 
 embrace Sonata Suite , find | 
 perfect illustration Beethoven Septet . | 
 reason Haydn choose | 
 Minuet concomitant recognise 
 movement Sonata - form far seek . | 
 favourite dance aristocracy | 
 time , strain genuine Minuet 
 mind somewhat comical grandezza | 
 period . Minuet , adopt | 
 peasantry , vogue 
 townspeople . Haydn gain early | 
 popularity circle write minuet . | 
 , sonata , Haydn Minuet 
 embody national joviality Viennese , 
 time retain distinct 
 characteristic dance movement , regard 
 artistic form . manner Haydn , 
 consummate skill , raise minuet complete 
 picture dance , popular life 
 joyous humour , sentimental 
 emotion . " trio , " hand , 
 range Haydn minuet 
 especially remind ' Volkslied proper , 
 combine cheerfulness melancholy . 
 notwithstanding , , attractive form 
 fascinating humorousness , admit 
 minuet ( Scherzo Beet- 
 hoven develop ) retain 
 character insert piece , intend 
 mediate , , movement 
 opposite mental disposition , prepare mind 
 listener reception new tone - picture , 
 Finale . regard , minuet , introduce 
 Haydn , appear true significance im- 
 portance . preserve natural division 
 symphony , ' " ' drama tone . " 
 proof need , apparent 
 look position composer 
 assign minuet . , symphony 

 occasionally place adagio , 
 weighty nature movement require 

 Austria ) request furnish year } sympathy man 
 i776 , consider } place position compel per- 
 celebrate composer native country . ! form duty nature profoundly 
 direct attention awhile posi-| ignorant ; sympathy 
 tion composer princely patron , } deliberately place . 
 shall time effort ! time recollect ask musical 
 establish claim fatherhood modern | critic journal informa- 
 symphony . tion " dominant octave " ; , , 

 residence Eisenstadt , Hungary , | tell happen horn 
 Princes Esterhazy maintain orchestra modest | play key C , sound right 
 proportion , , , Haydn direc-|the instrument . paper de- 
 tion gradually assume great importance and/| vote exclusively music publish 
 perfection . Prince Nicolaus , composer } London scarcely , , rest satisfied 
 serve nearly thirty year , , , | service writer art ; 
 prove faithtully devoted evident linger country . 
 genius , indefatigable appreciation | fact furnish instance ; usually 
 production capellmeister , thus| standard work perform , 
 actually " ' Herren , ' Beet- | person " " musical notice evidently 
 hoven mind year 1809 | feel bind review . following opinion 
 receive acall Cassel , expect ; Mozart " Jupiter ' Symphony , extract 
 payment annuity purpose the}a paper , sufficiently illustrate 
 ‘ * invention new work . " Prince Nicolaus , in| position : ' ' Jupiter Symphony mas- 
 fact , imaginary gentleman have|terpiece instrumental production . 
 Beethoven case , " ' co - originator | peculiar richness harmony , obtain 
 important new work " write composer | use frequent chromatic chord . Symphony 
 auspex . love art science far}embrace movement , successive 
 exceed - know predilection display ot | change tell effect , subject 
 princely splendour . word cite | work . " satisfactory 
 speak Griessinger , Haydn | critic confine 
 hasset patrona significant memorial : ' ' Prince | general praise work ; , necessary 
 satisfied composition ; meet en- | extend notice , enlarge dress 
 couragement ; chief orchestra enable | " fair " portion audience . certainly non- 
 try experiment , observe produce | musical listener competent judge 
 effect weaken ; improve , add , cur- | beauty " ' Jupiter ’' Symphony writer 
 tail , risk . Set apart world , near | notice ; musical , conversant 
 shake faith , perplex | Mozart score , discover long ago 
 doubt , bind original . " | " peculiar richness harmony " 
 reference chiefly technical | mean produce " use frequent chromatic 
 acquirement art , power orchestra- | chord . " 
 tion , important question artistic 
 form composition cast , with-| alight railway station month ago , 
 advantage , establish | attention direct alarming noise 
 simple framework quartet pianoforte |a crowd person surround carriage 
 sonata , doubt fact relate | forward endot train ; inquire cause — 
 word important bearing | fear accident happen — 
 development instrumental music generally . | inform " ' Salvation Army . " 
 attempt , , analyse | follow ' ' army , " surprised find 
 ultimate artistic , i.e. , poetic , value symphonic | , entrance station , instead dis- 
 form , brief reference Haydn mas- | persing , privileged disturber public 
 terly treatment orchestra . peace form double file , march 

 , consult Griessinger biographical no-| crowded street roar sacred word 
 tice , learn stay school- | voice , , tumult frighten 
 master Hainburg , previously | passenger pathway , horse 
 refer , boy Haydn " receive instruction in|in road , conclude solely 
 use nearly wind string instrument , | intent " save " . 
 beating kettle - drum . " fruit seed sow England Messrs. Moody 
 Capellhaus Vienna , , accord own|and Sankey scarcely doubt ; 
 statement , enjoy advantage tuition| difficult matter , , 
 ' ' competent master instrument . " | believe religion , pure sense , 
 , , , frequently oblige | block public street en- 
 sheer necessity performance | dangere life passenger , protect them- 
 popular dance festive occasion small | self effectually intolerable nuisance . 
 viennese band . home| content remain perfectly passive 
 orchestra practise composer had|the matter , hope fanatical demon- 
 . , - adapt for|stration die gradually ; , unfor- 
 orchestra early minuet , he|tunately , authentic information arrive 
 , measure , follow example pre- | Messrs. Moody Sankey visit 
 decessor regard grouping colouring | , conclude open - air 
 instrumental body , individual share merit | exhibition increase diminish . 
 respect " original " | dwell effect music 
 final adoption sonata - form as|a certain extent bar progress work 
 fundamental framework entire range of| high class multitude — true art 

 MONDAY popular concert 

 Concerts , announce , resume 
 roth ult . , occasion Mdlle . Marie Krebs 
 appear time season pianist , 
 warmly receive entrance , 
 performance Beethoven familiar ' * Waldstein " 
 Sonata ( Op . 53 ) , opening allegro , how- 
 , play great speed , cost indistinct 
 phrase . , , 
 reading work admirable , deservedly 
 applaud audience , satisfied 
 lady substitute piece response encore . 
 Concert open Mozart String Quintet G 
 minor , . 6 , admirably play Madame Norman- 
 Néruda , MM . Ries , Straus , Zerbini , Piatti ; con- 
 clude Beethoven favourite Serenade Trio D 
 major ( Op . 8) , execution lady violinist 
 associate MM . Straus Piatti . Miss Hope 
 Glenn vocalist 

 follow Concert ( 17th ult . ) , Herr Jean 
 Becker , absence year , reappear 
 violinist , prove equal great 

 bute Mendelssohn * Aerndtelied " Bach * Willst 
 | du dein herz mir schenken , " song Grieg 
 | Rubinstein 

 Concert ( 24th ult . ) include Schubert 
 | quartet minor ( Op . 29 ) string , play 
 |capital style Madame Norman - Néruda . MM . Ries , 
 Straus , Piatti ; Beethoven trio G major 
 ( Op . 9 ) , . 1 , lady violinist asso- 
 ciate MM . Straus Piatti , performance 
 enthusiastically receive . Miss Dora 
 Schirmacher , pianist , meet - deserve 
 applause spirited interpretation Beethoven Varia- 
 tions C minor ; instrumental solo ex- 
 quisite rendering , Madame Norman - Néruda , 
 Handel Violin Sonata D major , accompany 
 pianoforte M. Zerbini , officiate Conductor . 
 Mr. Frank Boyle vocalist , receive 
 audience interpretation song Handel , 
 Mendelssohn , Haydn . thin attend- 
 ance , owe , doubt , inclemency weather 

 BOROUGH HACKNEY CHORAL ASSOCIATION 

 Tue second Concert season 
 Shoreditch Town Hall 25th ult . programme 
 commence Hofmann Cantata , ' Melusina , " 
 | time performance , orchestra , London . 
 excessive melodiousness work , delicate 
 fanciful instrumentation , especially fitted 
 choir band Mr. Prout ’ > ¢ rection , 
 rendering reflect high credit .po . concern . 
 particularly mention charming singing Miss 
 Annie Marriott principal , solo 
 fall share true artistic feeling , 
 elicit warm expression approval 
 audience . Miss Marian McKenzie , Mr. Thurley Beale , 
 Mr. Frederick Bevan efficient , Mr. 
 Beale , good effect , solo , ' * Linden 
 whisper " ' ; fine contralto voice Miss McKenzie 
 hear advantage solo 
 allot . chorus admirably sing , 
 , , render impossible select 
 especial praise ; band thoroughly 
 control conductor . work 
 deep impression audience , trust 
 production Concert ensure permanent 
 place repertory Choral Societies . second 
 comprise Beethoven overture ' " ' Egmont , " Gade 
 chorus , ' * Spring Message ’' ; Mr. Prout MS . Minuet 
 Trio ( repeat desire ) ; A. Goring Thomas Scena , 
 ' * hero leander " ( sing dramatic feeling 
 Miss Marriott ) ; Dr. F. E. Gladstone fine chorus , 
 " wet sheet flow sea " ; Nicolai Over- 
 ture " Merry wife Windsor . " Mr. Prout 
 , usual , able conductor 

 BERLIOZ " L’ENFANCE du CHRIST 

 dren lead master , small difficulty 

 Mr. James SuaAw Concert place Vestry 
 | Hall , Haverstock Hill , Monday , roth ult . , 
 | prove respect successful . bénéficiaire 
 | Organist Choirmaster Parish Church Hamp- 
 | stead , raise musical portion service 
 high standard proficiency . programme com- 
 mence Mendelssohn Motett ' ' hear prayer , " 
 solo admirably sing Miss Anna Williams , 
 choir consist select voice . 
 soloist Miss Cathcart , Miss Hughes , Mr. Bernard 
 Lane , Mr. Thurley Beale . - gentleman 
 highly effective Blumenthal ' ' requital " Beet- 
 hoven " Adelaide . " " Miss Anna Williams hear 
 great advantage Weber scena ' " softly sigh , " 
 F. Clay ' ' wander mountain , " 
 encore substitute marzial ’ ballad 
 " ' ti shower . " Mr. Beale spirited 
 rendering ' * O ruddy cherry , ' introduce 
 excellent new song write Mr. Shaw , 
 ' wet sheet flow sea . " Miss Cathcart 
 sing new melodious song composer 
 entitle ' ' Constancy , " principal novelty Con- 
 cert , , ' ' Thanksgiving ode " ( ' non nobis 
 domine ’' ) , originally compose ' house - warming , " 
 inaugural domestic festival , Mr. Shaw , consist 
 overture , solo voice , chorus . ' work 
 efficiently render , deserve hear . Herr 
 Otto Booth Mr. Boatwright , Mr. Shaw 
 pianoforte , play Beethoven Trio C minor , Mr. 
 Shaw play Schumann Novelletten 
 Heller ' La Truite " taste expression . 
 Mr. W. S. Hoyte accompanist 

 ar Christmas examination University 
 London , candidate , Mr. H. Keatley Moore , Croydon , 
 Mr. W. H. Hunt , Birkenhead , pass 
 examiner ( Dr. Stainerand Dr. Pole ) qualified receive 
 Mus . Bac . degree . musical degree 
 University . examination divide 
 , year interval . 
 theoretical , comprise physical nature musical 
 sound , general principle music ; second 
 | practical , refer harmony , counterpoint , 
 | form , instrumentation , knowledge standard work , & c. ; 
 include composition approve exercise . 
 | somewhat remarkable 1878 1879 
 | candidate creditably answer theoretical test , 
 j mention succeed pass 
 practical . understand examiner 
 | determined standard practical musician- 
 | like attainment level fully high demand 
 |in great Universities kingdom , 
 | musical value degree fully maintain . 
 | follow candidate pass B. Mus Exami- 
 nation ( Division ): John Hatchwell , private study ; 
 Augustus Hayter Walker , private study . ( Second Division ) : 
 Joseph Rosamond Adie , University College 

 sacred music , important Mendels- 
 sohn " Festgesang " ' ( originally write male voice 
 military band ) , chorus , orchestral 
 accompaniment adapt conductor . second 

 comprise , item , Beethoven ' ' Caim 

 Sea Prosperous Voyage , " Auber overture ' ' La Sirtne , " ' 
 Mendelssohn Pianoforte Concerto G minor ( excellently 
 play Miss Rhodes ) , new song , * * King 
 night , " A. J. Dye , sing Mr. Prenton . 
 vocalist Madame Adeline Paget , Miss Coyte 
 Turner , Mr. C. A. White , receive . 
 Mr. S. Dean Grimson lead band , Mrs. Alfred Dye 
 preside piano , Mr. Alfred J. Dye conduct . 
 J. F. Barnett " ancient Mariner " J. G. 
 ' golden Harvest " announce Concert 

 newly complete organ St. Faith Church , 
 Stoke Newington , use ' time Christmas 
 Day , formally open Thursday evening , De- 
 cember 30 . instrument build Messrs. Hill 
 Son , manual , nineteen stop , 
 coupler , composition pedal . Mr. Thomas 
 Morley , Harmondsworth , Slough , late St. 
 Alban Holborn , play excellent style selection 
 music , include Handel ' ' occasional ’' Overture , 
 Pastorale Corelli eighth Concerto , Offer- 
 toire Lott Chinner . Mr. Henry Knott , organist 
 St. Faith , perform Léfebure - Wely Offertoire G , 
 Mendelssohr War March , accompany 
 | choir church solo chorus anthem 

 Miss JosepuHineé AGABEG evening Concert 
 Steinway Hall.on 17th ult . , appreciative 
 audience . Madame Ex lith W ynne unfortunately pre- 
 vent indisposition singe , vocalist 
 programme — Miss Cecilia Fuller , Miss 
 Marian Williams , Mr. James Sauvage , Mr. Frank 
 Quatremayne — highly successful . concert- 
 giver pianoforte - playing course marked feature 
 selection , rendering Beethoven ' * Moonlight 
 Sonata " ' especially warmly deservedly applaud . 
 Miss Agabeg assist instrumental department 
 Mdlle . Bertha Brousil ( violin ) , Mr. Wilhelm Ganz 
 ( pianoforte ) , M. Gustav Libotton ( violoncello 

 candidate recent Christmas Examination 

 Musicat Entertainment , kindly provide Mr. 
 Walter Clifford , support Madame Edith Touzeau , 
 Mr. Faulkner Leigh , Signor Nicola Ferri , Mr. W. H. 
 Romaine - Walker , patient Brompton 
 Hospital 18th ult . item receive 
 warm mark approval . Signor Ferri act 
 accompanist evening 

 Mr. RicHARD LEMAIRE secure service Mr. 
 Charles Hallé Pianoforte Recital Erith Public 
 Hall , roth ult . programme include 
 Waldstein Sonata ( Beethoven ) , Nocturne F sharp , 
 Grand Polonaise flat ( Chopin ) , Impromptu ( Schubert ) , 
 Rondo C ( Weber ) . Mr. Kenningham 
 vocalist 

 Mr. SuLLivan ' " Martyr Antioch ' ? 
 St. James Hall March 18 , conductorship 
 composer . chorus sing Mr. Faulkner 
 Leigh choir , principal vocalist 
 original performance work 
 Leeds Festival 

 doubt key 
 singer . Avery slight examination Voice Modu- 
 lator page 106 ' ' Standard Course " 
 clear . itis know compass human voice 
 ( like instrument ) divide certain * ' break " 
 ' ' register , " high importance 
 break cross right 
 place . Voice Modulator key 
 cross break degree scale , 
 , consequently , key 
 singer . , example , key c ( practi- 
 cally identical , observe , clarinet - player 
 change instrument ) . key ( Voice 
 Modulator right ) dominant submediant 
 sing safely " thick " register ; C 
 " ' thin . " " circumstance 
 difference ' ' effect ' music sing , 
 explain song , write low voice , lose 
 charm transpose high key , vice versd . 
 advise think Tonic Sol - fa 
 look closely matter , compare them- 
 self crossing break possible pair 
 keys.—I , Sir , obedient servant , 
 J. Conway Brown , L. Mus . F.T.C.L 

 THAYER " BEETHOVEN . " 
 editor ' ' MUSICAL TIMES 

 S1r,—Your numerous reader sorry hear 
 communicate fact little hope 
 immediate publication fourth volume 
 important work . Mr. Thayer suffer greatly 
 present old cerebral disease , unfit 
 work inseparable official 
 position American Consul Trieste . illness 
 short possible duration 

 BIRMINGHAM.—The annual performance Messiah , 
 Festival Choral Society , place Town Hall , Monday 

 December 27 . principal vocalist Miss Marriott , Miss 
 Orridge , Mr. Maas , Mr. Thurley Beale . large 
 orchestra , Mr. Robinson solo trumpet . Mr. Stimpson 
 organ , Mr. Stockley conduct . performance 
 fine , , usual , overflow audience . 
 Concert current series Philharmonic 
 Union Town Hall , Tuesday , December 23 . programme 
 include Haydn creation , miscellaneous 
 selection . ' Miss Mary Davies , Mr. Maas , Mr. Blower 
 vocal principal . ' band chorus , Concert 
 successfully carry conductorship Dr. 
 C. S. Heap.——Mr . H. R. Bickley , popular alto , Concert 
 Town Hall , Friday , December 31 . executant comprise 
 Birmingham Glee Union ( Messrs. Bickley , Woodhall , Young , 
 Campion ) Miss Emilie Lloyd , vocalist , band 2nd 
 Life Guards , direction Mr. W. Winterbottom . mis- 
 cellaneous programme provide , afford satisfaction 
 large audience.——mr . Stockley second Orchestral Concert 
 place Town Hall , Thursday , 2oth ult . programme 
 comprise Mozart Symphony G minor , Mendelssohn Overture 
 Ruy Blas , Beethoven Leonora ( . 3 ) , , novelty , Berlioz 
 " Danse des Sylphes , " La Damnation de Faust , Largo 
 arrange Handel , Hellmesberger . vocalist Frau- 
 lein Heffelmann , Madame Patey , Mr. Maas . orchestra , 
 number upwards , , exception , con- 
 sist local performer , highly efficient , reflect 
 great credit organiser conductor , Mr. W. C. Stockley . 
 — — cheap Concerts Musical Association continue 
 successful career . noticeable recent performance 
 repetition , desire , Mendelssohn Elijah , place 
 Saturday , 15th ult . , audience number close 3,000 . 
 Holte Choral Society , direction Mr. C. J. Stevens , 
 performers.——On Saturday , 22nd ult . , Midland Musical 
 Society , conduct Mr. H. M. Stevenson , Miss Fraser 
 Brunner , Mr. S. Roper , Mr. Horrex , principal , 
 successful performance , band organ , Haydn Creati : 
 large enthusiastic audience 

 31sHoP AUCKLAND.—On Monday , December 20 , Auckland 
 Musical Society Christmas performance Handel Messiah , 
 band chorus 150 performer , conductorship 
 Mr. Kilburn : principal , Miss H. Tomlinson , Miss M. Tomlinson , 
 Mr. Verney Binns , Mr. John Burgin , Mr. J. H. Brotherton , Mr. 
 W. Brotherton , Mdlle . Bertha Brousil . performance 
 respect marked success , chorus especially render 
 great intelligence spirit . hall crowd 
 Dart 

 table ; instrumentalist Mr. A. W. Waite ( violin ) , Mr. Louis 
 Waite ( violoncello ) , Mr. J. O. Brooke ( clarinet ) , Mr. Owen Williams 
 ( organ ) , Mr. J. V. Tittle ( piano ) . excellent programme 
 arrange , execution exceedingly good 

 Dar_incron.—On Tuesday evening , December 21 , Pianoforte 
 recital Mr. W. Crawford , jun . , Mechanics ’ Hall . 
 programme embrace selection work Beethoven , 
 Donizetti , Mendelssohn . performance highly meritorious , 
 facile mastery art , difficult passage 
 execute brilliancy softness leave desire . 
 cornet solo , Mr. J. Crawford , admirably render . 
 vocal portion entertainment divide Mr. 
 Nutton , Durham Cathedral , Mr. Bowness . Master Blackett 
 astonish everybody masterly execution violin . 
 handle bow finish adept , play boldly , firmly , 
 correctly , reflect high credit tutor , Mr. W. Craw- 
 ford . Mr. Boynes act accompanist 

 DoncastER.—The Messiah perform r1th ult . , 

 EccLes.—The member Vocal Society Concert 
 season Monday evening , roth ult . , Co - operative 
 Hall , attend . principal vocalist Miss 
 Clara Samuell Mr. Kendal Thompson ; Conductor soloist , Mr. 
 C. T. Sutcliffe , F.C.O. ; accompanist , Mr. J. Barlow . Miss Samuell , 
 appearance Eccles , highly successful 
 appreciate . Mr. Kendal Thompson receive encore 
 song , " sailor grave " " death Nelson . " piece 
 sing Vocal Society steadily , careful 
 training . Mr. Sutcliffe solo tastefully carefully per- 
 form , ' " ' Abend Glocken " encore 

 EpINBURGH.—Professor Sir Herbert Oakeley Organ recital 
 University Music Class - room , Park Place , 13th ult . , 
 large attendance . programme include Handel air 
 adapt ' Lord , remember David , " Haydn canzonetta , . 3 , 
 Minuetto trio D major , Beethoven romance 
 violin , Op . 40 

 EGLOSHAYLE , CoRNWALL.—A performance Mendelssohn St. 
 Paul Parish Church roth ult . church 
 choir , assist Rev. E. S. Shuttleworth , Mr. T. Craddock , 
 Mus . Bac . , Messrs. Ward , R. Williams , H. Williams , 
 Upton choir , Torquay . attendance , spite severe weather , 
 good 

 RepruTH.—On Tuesday , 11th ult . , Mr. T. J. Thuell 
 benefit Concert Druids ’ Hall , principal artist Miss 
 Bell , Miss Marion McReggie , Mr. Edwin A. U’glow , 
 assist amateur ; vocal solo warmly ap- 
 plaude , redemande , - know hungarian 
 dance , pianoforte duet Mr. Thuell Mr. W. H. 
 Lanyon , success evening . room 
 crowded , Mr. Thuell , conduct , occupy usual 
 post accompanist 

 REtTFOoRD.—The Choral Society Concert season 
 Thursday , December 23 , Mendelssohn Athalie per- 
 form . Mr. F. T. Clark recite dramatic portion , 
 soloist Miss Jenkinson , Miss Birkett , Miss Cross , Miss 
 Batley . Mr. Hamilton White conduct . second , Miss 
 Denman brilliant rendering Beethoven Polonaise C , 
 encore . Mr. Spence sing taste " love eye " 
 ( Acis Galatea ) , Mr. Denman ' Peter , hermit , " chorus 
 add arrange conductor . Miss Jenkinson ex- 
 pressive delivery Gounod " Ave Maria , " subdued accompani- 
 ment chorus , organ , piano receive enthusiasm 

 RocHDALE.—On Wednesday , 12th ult . , second Subscription 
 Concert place Town Hall , large audience . 
 instrumentalist Madame Norman - Néruda , Mr. Chas . Hallé , 
 band . vocalist Mr. Arthur Oswald . — — Friday 
 evening , 21st ult . , Subscription Concert 
 place Town Hall . vocalist Madame Trebelli , Miss 
 Anna Williams , Signori Vizzani Zoboii , instrumentalist 
 Signor Bisaccia M. Ovide Musin 

 SILONDALE , NorRTH STAFFORDSHIRE.—The Silondale Choral Society 
 ave excellent performance Messiah primitive 
 Methodist Chapel Monday evening , 17th ult . principal 
 vocalist Miss R. Green , Mrs. T. Taylor , Miss Vyjars , Mr. 
 Whiston , Mr. Pountney . Mr. R. Sourbutts play trumpet 
 obbligato , Dr. W. T. Belcher , Birmingham , preside 
 organ . Mr. F. Mountford conductor 

 SovutTHport.—On roth ult . Mr. A. E. Bartle Pianoforte 
 Recital Winter Gardens , play Beethoven 
 Sonata C , Polonaise , Nocturne , Valse Chopin , 
 Schumann Novelletten , Mendelssohn " Lieder ohne Worte , " 
 " Paraphrase de Concert " Rigoletto Liszt , Scherzo 
 movement Litolff fifth Concerto.——On 22nd ult . , Miss 
 Nellie Arthur Miss Mathilde Lemon sing Winter Gardens ’ 
 Popular Concert , Mr. J. T. Carrodus play couple 
 violin solos.——The Carl Rosa Opera Company open short stay 
 night Pavilion Theatre 24th ult . Mignon , 
 Miss Georgina Burns Filina Miss Julia Gaylord 
 title réle , successful . Mr. J. W. Turner Wilhelm , 
 fill Miss Josephine Yorke , Mr. Leslie Crotty , 
 Mr. Charles Lyall . 25th , Bohemian Girl pro- 
 duce , Miss Clara Perry Arline ; 26th Carmen 
 perform 

 STRATFORD - - Avon.—Mendelssohn Athalie perform 
 Choral Society 18th ult . , inclement state 
 weather prevent attendance member ( 
 reduce audience somewhat scanty assembly ) , work 
 efficiently render . solo creditably ren- 
 dere , Miss McLandsborough , Mrs. Cranmer , Miss Stuart , 
 lyric recite Mr. Charles Fry . second Concert 
 miscellaneous , include , piece , effective 
 - song Mr. Caseley , entitle ' Dream . " solo contri- 
 bute Miss McLandsborough Miss Stuart , Mr. Charles Fry 
 recite scene School Scandal Hamlet , 
 , especially , warmly applaud . accompaniment 
 pianoforte efficiently play Mr. Marics ; Mr. 
 Caseley , conduct performance care , join Mr. 
 mary harmonium Overture War March Athalie 

 angels sing , " " unto child bear , " " glory God 
 high , ' Messiah , Mendelssohn Kyrie Gloria . 
 excerpt finely , degree finish 

 indicate careful rehearsal . Mr. Doward play Handel Pastoral 
 Symphony , Lemmens Allegretto B flat , offertory voluntary , 
 Beethoven Adagio Septet , conclude voluntary , 
 perform usual artistic manner.—at 
 service following morning , Dean preach onthe martyrdom 
 St. Stephen ard institution order deacon . choir 
 large , good musical selection , neverthe- 
 . Mrs. Davis solo vocalist , sing effectively 
 " behold virgin , " " o thou tellest . " . organ solo 
 Andante Mendelssohn G minor Concerto , Guilmant : r- 
 toire D , Hesse Prelude fugue . evening Rev. 
 Mr. Rainsford preach large congregation . solo vocalist 
 Miss White Romanza Mozart second Concerto 
 principal organ number.——On New Year Day member 
 choir St. James Cathedral present Mr. Doward purse 
 money , address expressive appreciation merit 
 musician kindness courtesy capacity 
 instructor 

 Totnes.—A successful Concert , direction Miss 
 Dinah Shapley , R.A.M. , Assembly Rooms , Seymour 
 Hotel , Friday evening , 14th ult . vocalist Miss Berrie 
 Stephens , Misses Hicks , Rev. W. Watkins . Miss Shapley 
 prove thorough artist pianoforte solo , dis- 
 play - train voice Cowen song ' ' Land . " Mr. 
 T. B. Knott contribute effective pianoforte solo ' Troisiéme 
 Ballade flat " ( Chopin ) . vocal trio , ' Ave Maria " ( Dinah M. 
 Shapley ) , effectively render Miss Berrie Stephens , Miss 
 M. Hicks , Miss Gertrude Hicks 

 38 . Gigue e major , " Suites Frangaises . " 
 39 . Fantasia c minor , 
 40 . Bourrée e flat major , " Fourth Sonata Violoncello . " 
 price shilling . 
 London : Nove ELLO , Ewer Co. 
 Octavo . price yo shilling " Sixpenc ce 

 BEETHOVEN 
 CHORAL SYMPHONY 

 english GERMAN word 
 english Version 
 NATALIA MACFARREN 
 Pianoforte Arrangement 
 BERTHOLD TOURS 

 GEORGE GROVE , D.C.L 

 s. d 
 | pean hay ol cn 4 © ° ] 768 page , Svo ( double column ) , Cloth , hlustration 
 2 de ove : ose iemen 4 0 : . ee , es 
 3 . posthumous Rondo ’ b flat ... oo .. Mozart 4 0 Music Type Woodcut . 
 4 . Sonataim Di(Op . 47 ) ccc serene , tte 5 , 0 publish Quarterly Parts . 
 6 . Sonatina ow . 5 anaes Parts I. X1I. , seady , price 3 . 6d . . 
 7 . Bourrée minor ( Suites Anglaises ) = ae : 2 Cloth Cases Binding vol . I. & IL , price 1 , . 
 8 Sonatina G . » Beethoven 2 6 wee vIAT SS -¢ . — " ' 
 g. Echo ( Partita b minor ) .. « s « ts Bach 2 6 follow ing 
 10 . Sonatina F ( op . 38 ) ... oe aa Clementi 4 0 contributor : 
 4 sarap al ea ee ce : ; ss Sir Jutius Benepict . CuHartes Mackeson , Esq . , F.S.S. 
 13 . prelude caprice e c minor ( 1st Partita ) ove Zach 4 0 cing resnebibl 4 JENNETT x. aon 1 oo 
 14 . sonata e minor ass : .. Haydn 5 © BENS SR censor aa eee ae aes aa — 
 15 . l’adieu ae pe oe ... Dussek 3 0 d eas Huai tes Giese Mi ; si iy eed ALI 
 10 . minuet c d _ es oa .. Beethoven 3 0 eg 8 cao age ss la ue : seo : separ e 
 17 . La contemplazione ae es ee Hummel 4 0 sa ae CHAPPEt : ' esa . BSA 
 18 . abschie ... Schumann 3 0| meee Piracy IE sq . eat ni 2 
 1 g. Allegro , Sarabande , Scherzo minor ( grd p — ; M. GusTAvVE Cuovour t , keeper RBERT S. O. Ae EY , | 
 : site ° | Museum Conser- Doc . , Profe -ssor Music 
 20 soe ee flat ( op . 7 pe pi bis = : 7 | _ vatoire de Musique , Paris . _ Unive rsity Edinburgh 
 22 . Rondo capriccio ( op . bole Haciticwea = 6 ARTHUR Deke Cor ERIDGE , Esq . , ¥ * SIR FREDERIC va , 
 23 . Souvenir .. Schumann 2 0 db rcimcevsty pewrce E Ovsetry , Batt . Uni 
 24 . Allegro , Sarabande , Passacaille G minor (; p — ind | delssohn Scholar , 1875 - 70 . versity nha . 
 ro | x IUR SRAWFORD , B 3 y , Esq . 
 25 . Gavotte Musette D minor ( Suites Anglaises , . 6 ) | " oo xc ARTHUR CRAwroRI Henn Ens ge ze ving 
 ( Bach ) 3 0 Wits H. Cum Es Ei Jous Paysty ] Bar 
 | { MINGS , DWARI \YNE , Esq . , Bar- 
 * oo hog gghe San come Senate , ai 13 ) pene 4 © } W. G. Cusins , Esq . , Condu cael rister - at- W. digas 
 + fa ce , Steibelt 2 g| « Philharmonic _ Society ; Rae . Huan Prarsox , Canon ot 
 206 . eae p ot 3 1 musi o ’ indsor . 
 29 . Presto flat ( Sonata . 6 ) ... « . Haydn 3 0 Pano Music Epi bib ; H. Pemse Eso 
 30 . sonata C ( Op . 53 ) ae ésia ~~ « . Woelfl 5 0 Epwarp DANNREUTHER , Esq . 0.c. 3 ieee , d 
 31 . Saxon Air variation .. .. « . « Dussek 4 o|}izge PauL DAVID . Miss Pusat 
 2 . Passepied ( Partita B minor ) ie es , en Jane ge WA Dikaseoe Pian CG . Pour . Eibeaten ic 
 33 . MinuetsinGandE flat ... .. .. Beethoven 3 o/ fiwsrp H. Di jou Esq ‘ : etehath . dee Waal 
 34 . Rondo Brillant B flat(op . 107 ) ... Hummel 4 © ] 1 SurHerLaAND Epwaxps , Esq ; ' Wasa 9 
 35 . Toccatain ( Sonata No.6 ) ... Paradies 3 0| teer Piensa Peon , Ban - Poise , baa . ons 
 36 . Gigue F sharp minor ( suite . 6 ) . - Handel 2 0 } " Oppanist Chapel ' Royal , Mua : soc weiss hea 
 37 . invitation la valse ( Aufforderung zum Tanze ) . . Weber 4 0 savon . P Wicror pe Poxticny . E 
 - simcity altaleg e flat 7 sis Ls ii tt . a. FuLLER - MAITLAND , Esq . EsENeZzER Provt , Esq . 
 39 : na eee * 5 2 ry t > } x ¢ 
 40 . nocturne e flat ( Op . ' " . 2 ) ae . Chopin 2 o Cuartes Autan Purre , Esq . , Cina bi ws : nog " x 
 41 . Aria ( 4th Partita ) . ane pee ne wis Bach 2 ¢ Basrister - t - Law , seperti eee 
 42 . La Galante , Ronde ( op . oe Hummel § 0 . — — Ling " Vienna . F. Ris = AUI .T , Esq . , 
 43 . Rondo brillante e flat 62 ) ass ss ae , Weer 4 0 ae recs vas Bi 5 tore , Master Lutat Ricct , Esc 
 44 . Wiegenliedchen ( op . 124 ) + » a. Schumann 2 ¢ " et tlie Child ian ihe Chancls | WS . moc ee 2 
 45 . Aria con variazioni ( op . 107 , no.3 ) ... Hummel 4 o Rois = e W. Barciay Sourre , Esc 
 46 . Getwes Study ? ; c ORIDEIO Ol Gio ree sean aen Svicna Fac . 
 > . Sines ( 1st Partita ) sa pg Ses . ' Bach 2 6 ) soon po ee 
 43 polonaise C ( Op . 89 ) eet . ¢ ; FERDINAND HILLeER , Cologne . Sir Ropert P. Stewart , Mus . 
 ve 4 eee ta aon " s 4 ' Jo Sé Musi r 
 49 . prelude fugue D ; ona .. Mendelssohn 4 © h. J. Hirxins , sa : Esa bli — Musk 
 50 . Gigue b flat ( 1st partita ) ... os - « Bach 3 o e bw ARD . te nr aes wins oo hy poe sq . , M.B 
 51 . Marche funébre ( Sonata , bait 35 ) .. Chopin 3 oo ] p ree h sa e ws Acie ° Skaiad " — eerie 
 2 . Grand Polonaise e flat e Weber 4 0 va iota ahead es esas eee eee 
 : ' Feninc di balio bic em aige Francis HUEFFER , Esq . Esq . , Mus . Doc . , principal 
 Po hao : protien tiieen Sanat , Op . sai ase ne , 8 Joun Huttan , Esq . , ll.d. National Training School 
 55 . Arabesque ( op . 15 ) Schumann 3 0 | WALLA te ie ren ane Katia 
 56 . variation original theme F F ( op . 3 ) Beethoven 4 0 socie ty . Seer ALEXANDER W. THs 
 sf , Navitions Fminon ( om news Adaya 4 ° ] vA Yak , Esq , Boston , Mass , " United States Consul 
 59 . impromptu b flat ( op . 142 , " . 3 ) Px Schubert 4 Phin : 3 + ecky , esc porconing ote Le Pee 
 60 . Polacca brillante e ( op . 72 ) .. ' ns .. Weber 4 o ] teyey 1 . Esuca sn Esq . Miss BertHa THomas 
 61 . Bagatelle e flat ( op . 33 , no.1 ) . Beethoven 3 0 ) Granny Lucas . Eda , sect tary c \. w ra 
 62 . il moto continuo ( Sonata , iad 24 ) oes .. Weber 4 0 hee hilt + es s c = Wart , kK Library 
 63 . Schlummerlied ( op . 124 ) Schumann 3 0 } - , ° ? Pbilharmonic Society . ape cea et aaa 
 63 . Capriccio F ( op . 49 ) 7 te h GEorGE ALEXANDER MACFARREN , 3oston , Mass. , U.S.A. 
 ree sual " Quant ' 2p te " = oe Wants oo ; bs Mus . Doc . , Professor Music Mrs. EpMonp WODEHOUSE . 
 65 . menuetto b minor ( op . 78 ) .. eS Giuket 3 ¢ | hy alata Cambridge , Tue Epitor . 
 67 . musical sketch ... . Mendelssohn 3 0 ] ( 7 ? bs ee ee 
 68 . variation , " harmonious Blackamith ' " ' .. : .. Handel 3 0 Dr. Grove dictionary boon intelligent lover 
 69 . sonata b flat ( op . 38 , . 2 ) os ada " Clementi 4 0 music . ”—Saturday Review . 
 70 . Andante ( op . 35 ) ... ae Beethoven 4 0 " modestly ' dictionary ' fairly 
 31 . Rondo cabetce ( Sonata , op . 45 , " . 1 ) Dussek 4 0 entitle ' E ncyclopedia ' editor include scheme 
 ~2 , variation sérieuses ( o feed : . Mendelssol 6 belong music , ally , distantly 
 " . . p. 54 ) en : relate . great mistake regard work useful 
 edit finger purpose reference . collection interesting 
 valuable article kind musical subject , amateur , 
 hi kT’I > hy IDTN genuine interest art profess love , read 
 VU AL E R M AC } : AR r e n e beginning end . ' ' — Dails News . 
 " leave ' Dictionary ' recommend heartily 
 reader instruction amusement . " — Tintes . 
 t ° int 2 y " strike reader , , fulness 
 Lonpon . ASH DOW N & PAR RY ’ completeness information collect . " examiner . 
 HANOVER SQUARE . MACMILLAN CO . , LONDON 

 XUM 

 sc 
 ADAM , A.—Le Postillon de Lonjumeau . Vocal score . 8vo 

 french german word ren os et 10 © 
 ASCHER , F.—La Diva . Valse Piano Sete . op . 133 4 6 
 — — Pompejana . Valse Piano Solo . op . 13 5 0 
 — Vineta . réverie fantastique Piano Solo , Op . | 1 \t 3.0 
 BEETHOVEN.—Cavatine et Alla Danza Tedesca 

 Quartet . op . 130 , arrange Piano Violin 

