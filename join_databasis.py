#import package to manipulate csv files
import csv
#import package to manipulate dataframes
import pandas as pd
#Open the csv file
import spacy
import dateparser
nlp = spacy.load('en_core_web_sm')
df = pd.read_csv('nineteenth.csv', sep=',')
#Create another dataframe with the columns named [Databasis	Publication	Title*	Link (may be missing)	Author*	Date	Text]
df2 = pd.DataFrame(columns=['Databasis','Publication','Title','Link','Author','Date','Text'])
"""
Loop that transforms the csv c19 like this:
web-scraper-order	web-scraper-start-url	title	Periodical_title	publication_details	location	full_text	Doc	Doc-href
1675091086-1173	http://c19index.chadwyck.com/dicals&forward=quickfull&pageSize=600&PageNumber=1	MUSIC	Saturday Review of Politics, Literature, Science and Art	152:3970 (1931:Nov. 28) pg. 698	null	THE development of the symphony since Beethoven shows a perpetual struggle to reconcile classical and romantic ideals, in other words an attempt to make the music first and foremost an expression or illustration of a definite story or "programme," while at the same time conforming to the structural laws of sonata form.	MUSIC	http://c19index.chadwyck.com/fullrec/fullrec.do?id=e951-1931-152-70-024149&resultNum=501&entries=501&area=periodicals&forward=quick&queryId=../session/1675090670_20841
into this:
Databasis	Publication	Title	Link	Author	Date	Text
c19	Saturday Review of Politics, Literature, Science and Art	MUSIC	"Null"	"Null"	152:3970 (1931:Nov. 28) pg. 698	THE development of the symphony since Beethoven shows a perpetual struggle to reconcile classical and romantic ideals, in other words an attempt to make the music first and foremost an expression or illustration of a definite story or "programme," while at the same time conforming to the structural laws of sonata form.
"""
#loop over each raw of the dataframe df and create a new raw in df2
#in this new raw enter the value "c19" in the column "Databasis", 
#enter the value of the column "Periodiocal" in the column "Publication",
#enter the value of the column "Title" in the column "Title",
#enter the value of the column "Link" in the column "Link",
#enter the value of the column "Author" in the column "Author",
#enter the value of the column "Date" in the column "Date",
#enter the value of the column "Text" in the column "Text"

#get the number of rows of the dataframe df
n = df.shape[0]
for i in range(0,n):
    df2.loc[i,'Databasis'] = "c19"
    #add the value of the column Periodiocal in the column Publication
    df2.loc[i,'Title'] = df.loc[i,'title']
    df2.loc[i,'Publication'] = df.loc[i,'Periodical_title']
    df2.loc[i,'Link'] = "Null"
    df2.loc[i,'Author'] = "Null"
    s = df.loc[i,'publication_details']
    
    date=s[s.find("(")+1:s.find(")")]
    date= dateparser.parse(date.replace(":"," "))
    if date is not None:

    #format the date into YYYY-MM-DD
        date = date.strftime("%Y-%m-%d")
    else:
        date = "Error parsing date"
    df2.loc[i,'Date'] = date
    doc = nlp(df.loc[i,'publication_details'])
    df2.loc[i,'Text'] = df.loc[i,'full_text']
    #df2.loc[i,'Date'] = df.loc[i,'publication_details']
    df2.loc[i,'Text'] = df.loc[i,'full_text']

#df = pd.read_csv('nineteenth_fev2.csv', sep=',')
#write the dataframe df2 into a csv file

df = pd.read_csv('archive.csv', sep=',')
df3 = pd.DataFrame(columns=['Databasis','Publication','Title','Link','Author','Date','Text'])

#get the number of rows of the dataframe df
n = df.shape[0]
for i in range(0,n):
    df3.loc[i,'Databasis'] = "Archive"
    #add the value of the column Periodiocal in the column Publication
    df3.loc[i,'Title'] = "Null"
    df3.loc[i,'Publication'] = df.loc[i,'Title']
    df3.loc[i,'Link'] = df.loc[i,'entry_url-href']
    link = df.loc[i,'entry_url-href']
    df3.loc[i,'Author'] = "Null"
    df3.loc[i,'Date'] = df.loc[i,'date']
    cleaned_link = link.replace("https://archive.org/details/", "")
    cleaned_link = cleaned_link.replace("?q=beethoven+sonata", "")
    clink = "corpus_archive_notitle_strip/"+cleaned_link+"_djvu.notitle.strip.txt"

    text = open(clink, "r").read()
    """ pick the text from the files like:
    sim_musical-times_1998-2000_139-141_cumulative-index_djvu.notitle.strip
    """
    df3.loc[i,'Text'] = text

df = pd.read_csv('gale_All.csv', sep=',')
df4 = pd.DataFrame(columns=['Databasis','Publication','Title','Link','Author','Date','Text'])
#get the number of rows of the dataframe df
n = df.shape[0]
for i in range(0,n):
    # Blocks to handle mixed up columns Date Authors and Issue
    date="Null"
    try:
        if "Date:" in df.loc[i,'Issue']:
            date = df.loc[i,'Issue']
        elif "Date:" in df.loc[i,'Date']:
            date = df.loc[i,'Date']
    except:
        if "Date:" in df.loc[i,'Date']:
            date = df.loc[i,'Date']
    date = dateparser.parse(date.replace("Date:","").replace("\r",""))
    date = date.strftime("%Y-%m-%d")
    df4.loc[i,'Date'] = date
    df4.loc[i,'Databasis'] = "Gale"
    author = "Null"
    try:
        if "Author" in df.loc[i,'author']:
            author = df.loc[i,'author']
        elif "Author" in df.loc[i,'Date']:
            author = df.loc[i,'Date']
    except:
        if "Author" in df.loc[i,'Date']:
            author = df.loc[i,'Date']
    """
    remove the Author: from the string
    Authors:
                
            
            
                
                    Alfred Brendel and Noel Goodwin
    >>> "Alfred Brendel and Noel Goodwin"
    or
    Author:
                
            
            
                
                    Alfred Brendel and Noel Goodwin
    >>> "Alfred Brendel and Noel Goodwin"
    """
    df4.loc[i,'Author'] = author.replace("Author:","").replace("Authors:","").strip()
    publication = df.loc[i,'Publication'].replace("Publication:","").split()
    publication = ' '.join(publication) 
    df4.loc[i,'Publication'] = publication
    df4.loc[i,'Title'] = df.loc[i,'title']
    df4.loc[i,'Link'] = df.loc[i,'result-href']
    df4.loc[i,'Text'] = df.loc[i,'ocr']




#concatenate df2, df3 and df4 and write the result in a csv file
df5 = pd.concat([df2,df3,df4], ignore_index=True)
df5.to_csv('fusion.csv', sep=',', encoding='utf-8', index=False)