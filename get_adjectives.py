"""
Program that opens the fusion.csv file and the interesting_adjectives.csv file 
and makes statistics out of it.
"""
import csv
#import package to manipulate dataframes
import pandas as pd
#Open the csv file
import spacy
import dateparser
import sys
import numpy as np

#packages for wordcloud
import os
from PIL import Image
from os import path
from wordcloud import WordCloud, ImageColorGenerator
import matplotlib.pyplot as plt

#import spacy for english
nlp = spacy.load('en_core_web_sm')
#open file
df = pd.read_csv('mini_fusion.csv', sep=',')
adjectif = "nice"
#add the column "nice" to the dataframe
df[adjectif] = 0
#for each row df
for index, row in df.iterrows():
    #extract the 7th column (the text)
    # and convert it to a string
    text = row[6]
    doc = nlp(text)
    #make a list of the lemmatized adjectives of the text
    adjectives = [token.lemma_ for token in doc if token.pos_ == "ADJ"]
    #count the number of times the adjectif is used
    adjectif_count = adjectives.count(adjectif)
    #add the number to the dataframe
    df.at[index, adjectif] = adjectif_count

#output dataframe
df.to_csv("mini_fusion_with_adj.csv", sep=',', encoding='utf-8', index=False)


