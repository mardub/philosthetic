


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


PUBLISHED ON THE FIRST OF EVERY MONTH


BRISTOL MUSICAL FESTIVAL


COLSTON HALL, 


OCTOBER. 21, 22, 23, AND 24, 1873


MADAME LEMMENS- SHERRINGTON 


MADAME OTTO-ALVSLEBEN - MISS JULIA WIGAN 


MADAME PATEY - MISS ENRIQUEZ 


MR. SIMS REEVES - MR. VERNON RIGBY 


MR. EDWARD LLOYD - MR. LEWIS THOMAS 


MR. SANTLEYY


ORGANIST - MR. GEORGE RISELEY


BAND AND CHORUS OF UPWARDS OF 400 PERFORMERS. 


CHORUS MASTER - MR. A. STONE. 


CONDUCTOR - - MR. CHARLES HALLE


OF PRAISE.” On THURSDAY EVENING, at Eight o’clock, a Miscellaneous Selection, and 
Rossini’s ‘‘ STABAT MATER

The Selections include—Overtures to Weber’s “ Der Freyschiitz” and “ Euryanthe,” Wagner's 
“Tannhiuser,” Rossini’s **Guillaume Tell,” Mendelssohn’s ‘“‘Midsummer Night’s Dream” and “ Meere- 
stille,”” and Beethoven’s “ Leonora; Mozart’s Symphony in E flat, and Beethoven's Symphony in C minor

THE WHOLE PERFORMED BY MR. CHARLES HALLE’S COMPLETE ORCHESTRA




PROFESSIONAL NOTICES. 


MR. J. TILLEARD


MADAME HELENA WALKER 


M USIC ENGRAVED, PRINTED, AND PUB. 


RUSSELL’S MUSICAL INSTRUMENTS. E 


ROGER’S CELEBRATED VIOLIN STRINGS


W. SNELL’S IMPROVED HARMONIUMS


GRATEFUL—COMFORTING. 


P O A. 


BREAKFAST


231


RGAN, PIANOFORTE, HARMONIUM, HAR- 


IMPORTANT TO THE MUSIC TRADE AND PUBLIC IN 


GENERAL


QUARTERLY SALE OF MUSICAL PROPERTY. 


CHORAL SOCIETY


B RIXTON 


ADAME SAINTON-DOLBY’S VOCAL 


232


THE RGAN 


OF THE CHURCH, RGAN 


TOGETHER WITH ANT 


AND 


. G@CARBK 


EDWIN GEORGE MONK. ORG 


GREEN


FROM INTERVALS TO COUNTERPOINT.” 


SCIENCE AND PRACTICE OF MUSIC, 


SIR JOHN HAWKINS


233


C. JEFFERYS, 67, BERNERS ST


NEW PUBLICATIONS. 


MICHAEL WATSON


DIVINE TREASURES :— 


THE LYRIC STAGE


LOUIS DUPUIS. 


CATHEDRAL GEMS


GRAND OPERATIC DUETS


WILLIE PAPE


JOHN OWENMELODIOUS MELODIES

Twenty Subjects selected from the Works of Beethoven

Mendelssohn, Schumann, Heller, &c., arranged for th 
Organ or Harmonium. Price 2s. 6d. nett




SONGS FOR CHILDREN


LONDON: C. JEFFERYS


THE BERNERS PIANOFORTE GALLERY


67, BERNERS STREET, W


234


ROYAL ALBERT HALL


CHORAL SOCIETY. 


CONDUCTOR MR. BARNBY


THE COUNCIL OF THE ROYAL ALBERT HALL


PRESIDENT


THE EARL OF FEVERSHAM. 


THE LORD CLARENCE EDWARD PAGET, K.C.B. 


DR. LYON PLAYFAIR, C.B., M.P


A SERIES OF ELEVEN


GRAND ORATORIO CONCERTS


WILL BE GIVEN AT THE 


ROYAL ALBERT HALL


HANDEL’S THEODORA 


BACH’S CHRISTMAS ORATORIO 


MENDELSSOHN’S ELIJAH 


HAYDN’S CREATION 


HANDEL’S MESSIAH 


HANDEL’S ISRAEL IN EGYPT 


ROSSINI’S STABAT MATER 


MENDELSSOHN’S ST. PAUL 


THE CONCERTS WILL TAKE PLACE ON THE FOLLOW- 


ING DATES


1873


THURSDAY, OCTOBER 39


NOVEMBER 13


1874. 


THURSDAY, JANUARY 8. 


22


19


BAND AND CHORUS OF 1,200 PERFORMERS. 


ORGANIST, DR. STAINER


SUBSCRIPTION FOR THE SERIES


THE FIRST SUBSCRIPTION CONCERT 


WILL TAKE PLACE ON 


THURSDAY, OCTOBER 30


5 WHEN


HANDEL'S THEODORA


SCHOOL


OUTHWELL COLLEGIATE CHURCH= 


THE


235


HOOL


ED


MENT. 


FE MENT, 


236


237nstrumented, simple and melodious piece, which 
accomplishes all it aspires to, and prepares the 
curs, cogjaudience for the character ofthe work. The Overture 
hich it@strikes us as fragmentary, although, as in all Mr. 
‘the who Sullivan’s orchestral music, we have the evidence of 
harmongmatured skill in the management of the instruments. 
and feei'\No praise can be too great for the manner in which 
on, ledbgthe principal soprano, tenor and bass solos were 
anos, agsung by Madlle. Titiens, Mr. Sims Reeves, Mr. 
hout ove Cummings and Mr. Santley ; and although we should 
e coda have infinitely preferred Madame Patey in the 
ss wasopcontralto part, Madame Trebelli had evidently 
Jemande§ well studied the music, and rendered it throughout 
There if with an earnest desire to do justice to the composer. 
ses inthg In the secondary parts Mr. Briggs rendered valuable 
1ovement} aid; and band and chorus were alike faultless, not- 
voices op withstanding the few rehearsals which could have 
ewhat ng been given to the work. ‘The rule at these Festivals 
ul.” Th that the power of demanding an encore shall rest solely 
he sun tg@with the President possibly limited the repetitions of 
cample @ pieces during the morning—no doubt to the advantage 
oncluding of the general effect of the work—but that the Earl 
»propriaif of Shrewsbury should think it necessary at the end of 
ve canny the first part to apologise to Mr. Sullivan for not 
eatment \having multiplied the encores struck every one as so 
or femaleutterly absurd that we much doubt whether on a 
was ty future occasion the general body of listeners will 
leed fullj rest satisfied with the exercise of a privilege which 
can b virtually takes away from them the right of expressing 
1¢ voice the slightest opinion upon a work submitted for their 
the com judgment. The Duke of Edinburgh sat with the 
idition @ President’s party, and joined with the entire audience 
it, on thin the overwhelming applause with which Mr. Sulli- 
cquisitel] van, who conducted his Oratorio, was greeted at the 
od to tconclusion of the performance. Congratulations 
-ain froupon the success of the work were loud and frequent 
on, andgamongst those in authority at the Festival; and if 
s’ genet the composer—in imitation of his great predecessor 
io affonlg Mendelssohn, who left the hall after the production 
mposety of Elijah ” twenty-seven years ago, with the same 
1as mawpsound ringing in his ears—can re-consider and calmly 
‘interes analyse the merits and demerits of his Oratorio, when 
The lon temoved from the flattery of his friends, he is truly 
ch clergin the right road to attain that eminence which he 
itley, tigeovets, and which, with his exceptional talents, he 
ttentiojhas a right to aspire to

vocal A fine performance of Beethoven’s Symphony in 
It. IM’ minor commenced the miscellaneous concert on 
me bom Wednesday evening; and Mr. G. A. Macfarren’s 
roughowYverture to “St. John the Baptist,” and that of 
, phras@-Aossini to the “‘ Siege of Corinth ” were also included 
ye cfu the selection of orchestral pieces. The only

oticeabipmovelty was the posthumous ‘Hymn of Peace,” by 
3, “ {iPsossini, which was finely given by Mr. Santley and 
but pr@ee choir. The bold and tuneful character of the




238


239


240


241The Oratorio was followed by the fourth and fifth 
movements of Spohr’s Symphony, ‘‘ The Consecration 
of Sound,” which were excellently played, but 
scarcely perhaps seemed to tone with the character 
which should distinguish the Cathedral performances 
at a Festival. The same composer’s Cantata, “ The 
Christian’s Prayer,” afforded a real pleasure to the 
lovers of Spohr’s music. Written for four solo voices, 
chorus and full orchestra, it has infinite variety for 
so small a work; and sung as it was on this occasion 
by a thoroughly trained choir and such principal 
vocalists as Miss Edith Wynne, Miss Enriquez, Mr. 
Montem Smith and Signor Agnesi, the luscious 
stream of melody which runs throughout the solos, 
and the delicacy of the choral effects delighted every 
listener, and made us once more wonder how it can 
be that such an exquisite creation as this should be 
so rarely heard. Presuming that we are mainly in- 
debted to Mr. Townshend Smith (whose love of 
Spohr’s compositions is well known) for this treat, 
whilst acknowledging our obligations to him, let us 
return thanks also for a hearing of the work which 
concluded the morning’s performance—Handel’s Sixth 
“Chandos Anthem ”—the opening chorus of which 
at least should preserve it from the neglect with 
which it has been so long treated. The solos, too, 
are full of true religious feeling; and the chorus, ‘* Ye 
boundless realms of joy,” is a thoroughly representa- 
tive composition. The additional accompaniments 
by Mr. E. Silas, which were used on this occasion, 
gave much richness to the orchestral effects, and 
materially enhanced the enjoyment of the music. 
The Anthem was finely sung—Miss Edith Wynne, 
Miss Enriquez, Mr. E. Lloyd and Signor Agnesi 
being principal vocalists—and produced a marked 
effect upon the listeners, most of whom—to their 
credit be it said—remained to the end

At the evening concert Beethoven’s Symphony in 
C minor was excellently given, and received with an 
enthusiasm which shows that there is more danger 
of the authorities of the Three Choir Festivals under- 
rating than over-rating the musical capacity of their 
audiences. Mendelssohn’s Overture to the ‘* Mid- 
summer Night’s Dream” was also included in the 
programme; and a popular selection of vocal pieces 
was contributed by Madlle. Titiens (who volunteered 
to sing Professor Oakeley’s song, “ Tears, idle tears,” 
which was not set down for her), Madlle. Bartkowska, 
Miss Edith Wynne, Madame Trebelli, Miss Enriquez, 
Mr. W. H. Cummings, Mr. Montem Smith and Signor 
Agnesi. During the evening Lord Bateman, Presi- 
dent of the Festival (who had previously announced 
Madlle. Titiens’s offer to sing Professor Oakeley’s 
song), appeared upon the platform and said that he 
had been deputed by the choir and members of the 
orchestra, as well as by several other persons, to ex- 
press their high appreciation of the merit of Sir

Frederick Ouseley’s Oratorio ‘‘ Hagar,” which had




242As further donations may still be received, the 
exact amount realised by the Festival for the Charity 
can scarcely yet be known ; but the sum at present 
collected is, we are glad to say, upwards of £1070

AvttHoucH the Exhibition Concerts at the Royal 
Albert Hall are rapidly approaching their termina. 
tion—the last day of the present month, when the 
Exhibition closes, being also the final musical per. 
formance—there has been no diminution in the 
interest of the programmes. The principles an. 
nounced in the opening prospectus—that of produc. 
ing new works, as well as those already stamped with 
public approval, and of occasionally introducing 
comparatively unknown vocalists and instrumen- 
talists—have been rigidly adhered to, and with a 
result which we believe to have been highly bene. 
ficial both to art and artists. Amongst the com. 
positions given during the past month have been 
Mozart’s “Jupiter” Symphony, Gade’s Symphony in 
B flat, Beethoven’s “ Pastoral Symphony” and Over. 
ture to ““Coriolanus,” G. A. Macfarren’s Overtures to 
“Chevy Chase” and “ Robin Hood,” and a new Con- 
cert Overture by M. Duvivier. Mr. Arthur Barth’s 
excellent rendering of Dr. Hiller’s Pianoforte Con- 
certo in F sharp minor must also be mentioned; and 
the following vocalists—Miss Emrick, Mr. Albert 
James, Mr. F. Penna and Mr. Charles Beckett—have 
been received with warm and well deserved applause. 
Mr. Barnby, in addition to the arduous duties of con- 
ducting, has shown so much energy in producing these 
constantly varied attractions, and has evinced such 
sound judgment in the selection of new works for 
performance, that we cannot but hope for a renewal 
of concerts which, although at first an experiment, 
have proved so thoroughly enjoyable

WE understand that Mr. Barnby has accepted the offer 
to read a paper on Church Music at the Church Congiess, 
to be held in Bath during the present month

As the recent letter of M. Gounod to the Paris ‘“‘ Figaro” 
respecting the case ‘“ Littleton v. Gounod,” might lead to 
the supposition either that the plaintiff had given up his 
claim for damages and costs, or was acquainted with the 
person who had satisfied the demand, we think it right to 
state that the full amount was paid, in cash, by a solicitor 
to Messrs. Shaen and Roséoe, ‘‘on behalf of M. Gounod’s 
friends,” and that neither Mr. Littleton nor his pro- 
fessional advisers have received the slightest intimation 
as to the name of the donor

THE prospectus of the eighteenth series of Saturday 
concerts at the Crystal Palace announces that there will 
be twenty-five concerts, eleven before and fourteen after 
Christmas, the first being fixed for the 4th inst. The 
band and chorus will be of the same strength as heretofore, 
and the conductorship will remain in the able hands of 
Mr. Manns. Among the works intended to be performed 
are the following, many of them produced at the Crystal 
Palace for the first time:—Handel: the Oratorio ot 
‘‘ Theodora,”’ with additional accompaniments by Dr. Fer- 
dinand Hiller; Bach: Pianoforte Concerto in F minor; 
Haydn : two Symphonies not yet performed, and Selection 
from the ‘‘Seven Last Words ;” Mozart: Symphonies in 
C major (No. 6) and G minor, Pianoforte Concerto, in 
flat (1785); Beethoven: Symphonies Nos. 3, 6, 7, and, the 
Septett, the ‘ Praise of Music””—a Cantata for Solos, 
Chorus, and Orchestra—Chorus for female voices from 
“« King Stephen,” the Egmont Music; Schubert: Symphony 
in C (No. 9), the Octett (for Strings and Wind); Mea

The Musical Times, October 1, 1873




SS 


SWEET IS THY MERCY


- ALTO. 


TENOR. 


3D - - | - I 4, | = 


1 LAWN


SED


247


_—_—_


RL SSS SEall

Bacu, J. S. Sinfonia in D major, Overture and Suite 
in B minor, for Flute and Strings, and Concerto in C 
minor, for two Pianofortes (Messrs. Bird and Goodban).— 
BEETHOVEN. Overtures : Egmont, Die Weihe des 
Hauses, Fidelio (No. 4!, Leonora (No. 3), King Stephen, 
Prometheus and Coriolanus. Symphonies Nos. 1 to 8, and 
the Pianoforte Concerto in C minor (Miss E. Busby).— 
Mozart. Overtures: Die Zauberfléte, La Clemenza di Tito, 
Figaro, Cosi fan tutte and Idomeneo. Symphonies: in D, 
G minor and Jupiter. Concerto in C major (Mr. W. H. 
Thomas).—Haypn. Symphonies: No. 8, in B flat (Sala- 
mon’s Set), and Surprise.—HAanpEL. Organ Concerto 
No 1 (Mr. W. T. Best)—MENDELSSOHN. Overtures: Ruy 
Blas, Midsummer Night’s Dream, Hebrides, Calm sea and 
prosperous voyage, Melusine, St. Paul and Trumpet in C, 
Symphonies: Scotch, Reformation and Italian. Concertos: 
G minor( Miss E. Barnett, Mr. E. H. Thorne), D minor (Miss 
Le Brun). Capriccio Brillant in B minor (Madame N. Voa- 
rino). Cornelius March, and March in Athalie.—Sponur. 
Overtures: Jessondaand Faust, and Historical Symphony. 
—ScHUMANN. Overture: Manfred. Symphonies: No. 1, in 
B flat, and No. 3, in E flat (Rhenish) ; and Concerto in A 
minor (Mr. F. Rummel).—Scuuspert. Overture: Rosa- 
munde. Unfinished Symphony in B minor, Ballet Air 
in G, Rosamunde; Fantasia in C major, arranged for Piano- 
forte and Orchestra by Liszt (Madame E. Oswald), and 
March, E flat, arranged for Orchestra by E, Hecht.— 
WEBER. Overtures: Oberon, Der Freischiitz, Jubilee, 
Euryanthe, Turandot, Peter Schmoll; and Concertstuck 
for pianoforte (Chev. de Kontski, Miss M. Rock).—AuBER. 
Overtures: Exhibition, E major; Les Diamans de la Cou- 
ronne, Fra Diavolo, Masaniello, Le Cheval de Bronze, 
Zanetta.— Rossini. Overtures: William Tell, Siege of 
Corinth, Semiramide, Tancredi, Cenerentola.—Gounop. 
Overture: Le Medecin Malgré lui; March, Reine de Saba, 
and Saltarello—WaGNER. Overture: Flying Dutchman 
and Selection from Lohengrin (arranged by Signor Arditi!,— 
HIL_Ler. Symphony: E minor (Op. 67), and Concerto in F 
sharp minor(Mr.A. J. Barth).—Rierz. Overture: Lustspiel in 
B flat. CHERUBINI. Overtures: Les deux Journées, L’hotel- 
lerie Portugaise, and Anacreon.—H£Ero vp. Overtures: Le pré 
aux Clercs and Zampa.—BEnNNeETT, Sir W. S. Overture: 
Les Naiades; Concerto, No. 4, in F minor (Mr. W. 
Carter'.—FLorow. Overture : Stradella.—MEYERBEER. 
March, Prophéte-—Gapr. Symphony in B flat.—Bertvioz. 
Overture: Waverley.—Tuomas, A. Overture: Mignon.— 
Cusins, W.G. Concerto in A minor (Miss Jessie Morison).— 
Barnett, J. F. Symphony in A minor.—Lito.trr. Con- 
certo No. 3, National Hollandais (Mr. W. Coenen),— 
Benepict, Sir J. Overture: Macbeth. — BoreLpiEv

____

Overture : La Dame Blanche.—MACcFARREN, G.A. Over. 
tures: She Stoops to Conquer, Robin Hood, and Ch 
Chase.—SuLtivan, A.S. Tempest Music.—E vey, SirG, j, 
Festal March.—Liszr. Fest Marsch.—MEHUL. Over. 
ture: Le Jeune Henri.—Dvuvivier. Concert Overture, in 
A major.—Nico.al. Overture: Merry Wives of Windsor,— 
OpertTHuR, C. Overture: Rubezahl.—Cowen, F, 
Symphony in C minor.— Brion, R. Forsey. (Cop. 
cert Overture, in C minor.—Barry, C. A. Birthday 
March.—GapsBy, Henry. Overture: Andromeda, anj 
Symphony in C minor.—Tuou.eEss, A. H. Concerto jy 
E flat (Mr. A. H. Thouless)—Summe_rs, J. L. Concer 
Overture in F.— CLARKE, J. HAMILTON. Symphony 
in F

Many highly interesting works, which do not appex 
in the above list, will be performed during the present 
month, which concludes the season. Amongst these may 
be mentioned—Sir W. S. Bennett’s Caprice in E Major 
(Mr. E. H. Thorne), and his Concerto in F minor 
(Mr. George Wheeldon); Brahms’s Second Serenade 
in A, for Small Orchestra; Beethoven’s Pianoforte Con. 
certos in G major and E flat, also his Triple Concerto 
for Pianoforte, Violin, and Violoncello; Liszt’s Hungarian

Fantasia for Pianoforte and Orchestra (Mr. W. Coenen




REVIEWS


249


251


HENRY FARMER, NOTTINGHAM


252


LEAMINGTON (NO PUBLISHER


ORIGINAL CORRESPONDENCE


THE “MOVEABLE DO.” 


TO THE EDITOR OF THE MUSICAL TIMES


TO CORRESPONDENTS


BRIEF SUMMARY OF COUNTRY NEWS


TO MUSICAL AUTHORS. 


DURING THE LAST MONTH


OW LOVELY ARE THY HABITATIONS. 


ADVENT HYMNS


254


HE ANGLICAN CHORAL SERVICE BOOK, 


USELEY AND MONK’S: PSALTER AND 


HE PSALTER, PROPER PSALMS, HYMNS, 


OULE’S DIRECTORIUM CHORI ANGLI- 


OULE’S DIRECTORIUM CHORI ANGLI- 


HE ORDER FOR THE HOLY COMMUNION. 


SACRED MUSIC BY EDWARD LAWRANCE, 


ADDITIONS TO THE 


EV. T. HELMORE’S PLAIN SONG WORKS


255


O ANCIENT "AND MODERN CHANTS


A. MACFARREN’S TWO-PART ANTHEMS 


R. HOLLOWAY’S CHRISTMAS ANTHEM, 


HE GOOD TEMPLARS’ FESTIVAL SONG


CHURCH ANTHEMS, ETC


BY THE BEST COMPOSERS, ANCIENT AND MODERN. 


EDITED BY HENRY ALLON, D.D


NOTICE


THE


CATHEDRAL PSALTER


POINTED FOR CHANTING


BY


PUBLISHED WITH


THE VERY REV. THE


THE SANCTION OF


DEAN OF ST. PAUL'S


AND


THE VERY REV. THE DEAN OF WESTMINSTER


LONDON: NOVELLO, EWER AND CO., 1, BERNERS STREET


TO CHORAL SOCIETIES. 


H. BIRCH’S POPULAR OPERETTAS, 


NE


HA


DOW


257


4 NOW READY. 


NEW EDITION, CORRECTED TO UNE, 1873, 


NOVELLO, EWER & CO.’S 


CONTAINING 


SACRED MUSIC WITH LATIN WORDS. 


GRATIS ON APPLICATION, OR SENT POST FREE FOR ONE STAMP. 


JUST PUBLISHED, AN OCTAVO EDITION OF


HANDEL'S THEODORA


NOW READY


HANDEL’S BELSHAZZAR


SOUND THE LOUD TIMBREL


THE MELODY COMPOSED BY AVISON. 


MOZART’S MASSES


HAYDN’S MASSES


“ITHE MESSIAH 


OHN A ’ 


DOWN IN A FLOW’RY VALE. 


NOVELLO, EWER AND CO.’S ONLY COMPLETE AND 


UNIFORM EDITION OF


MENDELSSOHN’S


THIRTEEN TWO-PART SONGS


PSYCHE: 


A DRAMATIC CANTATA


FOR SOLO VOICES AND CHORUS. 


THE MUSIC BY J. F. H. READ. 


WILLIAM J. YOUNG’S 


NEW EDITIONS (OCTAVO


258


NOW READY


NOVELLO’S OCTAVO ANTHEMS


PRICE SEVEN SHILLINGS EACH


CONTENTS OF VOL. I


CONTENTS OF VOL. IL


CONTENTS OF VOL. III


NOVELLO’S LIBRARY 


FOR THE DIFFUSION OF 


: . TWEN’ 


HERUBINIS TREATISE ON COUNTER NEW C 


R. MARX’S GENERAL MUSICAL INSTRUC. 


IV. @ FIFTY-1 


ERLIOZ, TREATISE ‘ON MODERN INSTRU. 


R. CROTCH’S ELEMENTS OF MUSICAL | 


ABILLA NOVELLO’S VOICE AND VOCAL 


XI. 


BRAHA! 


__


CRAMPT\ 


PRACTICAL SERIES


E PAI 


LL TH 


INK’S PRACTICAL ORGAN SCHOOL. (0 


ALKBRENNER’S METHOD OF LEARN BHORAL 


TMUSICAL PUBLICATIONS. 


FOR THE ORGAN OR HARMONIUM


NSTRU- SMALL SOCIETIES AND THE FAMILY CIRCLE


IMPORTANT TO CHORAL SOCIETIES


SPITMAN’S CATALOGUE OF HANDSOMELY BOUND VOLUMES, SUITABLE FOR PRESENTATION, 


4 SENT FREE ON APPLICATION


LONDON: F. PITMAN, 20, PATERNOSTER ROW, E.C


FRIDOLIN


A DRAMATIC CANTATA


COMPOSED BY


ALBERTO RANDEGGER


PIANOFORTE., 


CHAPPELL & CO


THE ALEXANDRE NEW SEVEN-GUINEA ORGAN HARMONIUM. 


SOLID OAK CASE, FIVE OCTAVES, AND TWO FOOT-BOARDS


CHAPPELL & CO., 50, NEW BOND STREET


NEW WORK FOR SINGING CLASSES


CHAPPELL’S PENNY OPERATIC PART-SONGS. 


CT. 


OP


T.M 


LTC 


LTC 


TN


CHAPPELL & CO., 50, NEW BOND STREET, LONDON