


 LON 


 the MUSICAL TIMES 


 AND SINGING - class circular . 


 found in 1844 . 


 publish on the first of every month 


 ROYAL CHORAL SOCIETY . ROYAL 


 MANCHESTER COLLEGE of MUSIC . 


 P. QUARRY , 


 BIRMINGHAM & MIDLAND INSTITUTE 


 session 1916 - 1917 


 the ASSOCIATED BOARD 


 of the R.A.M. and R.C.M. 


 for local EXAMINATIONS in MUSIC 


 the ROYAL COLLEGE of MUSIC PATRON 's FUND — — _ — . 


 UNIVERSITY of DURHAM . 


 the ROYAL COLLEGE of ORGANISTS SST 


 the LONDON COLLEGE for chorister . LONDON COLLEGE of MUSIC , 


 incorporate guild of CHURCH MR . W. H. BREARE 


 EE 


 CHOO 


 ING 


 MY 


 W. H 


 M. MACDONALD SMITH 


 DR . A. EAGLEFIELD HULL 


 evision of MUSICAL composition 


 L.R.A.M. ( paper work ) . 


 A.R.C.M. ( paper work ) . _ 


 F.R.C.O. SPECIALIST in CORRESPONDENCE 


 DR . LEWIS ’ text - BOOKS 


 improvisation 


 S ! ; MARTIN -IN - the - FIELDS CHURCH 


 HE ORGANIST OF SOUTHWARK 


 GRAND PIPE 


 356 the MUSICAL 


 RUSSIAN NATIONAL H\ MN , 


 WEBSTER ’S 


 child 's primer 


 theory of MUSIC 


 twenty - third thousand 


 a practical guide 


 to the 


 theory of MUSIC 


 containing 


 numerous test - question with answer 


 JULIA A. O'NEILL 


 the TIMES . 


 the morning POST 


 MUSICAL opinion 


 the LADY . 


 OUR VILLAG SE ] in SUMMER 


 six easy piece 


 PIANOFORTE 


 compose by 


 CLEMENT M. SPURLING 


 OUR MUSIC reading LADDER for beginner 


 DOLL’S - HOUSE suite 


 five little piece 


 LITTLE FOLK 


 FOR 


 PIANOFORTE SOLO . 


 compose by 


 HUBERT BATH . 


 RAINBOW 


 WOODLAND DANCES 


 a set of easy piece 


 FOR the 


 PIANOFORTE 


 compose by 


 ERNEST NEWTON . 


 NOVELLO’S. ELEMENTARY music 


 manual 


 physical exercise , dance , 


 AND GAMES 


 INFANT S 


 ALEXANDER 


 chool 


 MARGARET HUGHES 


 the music arrange by 


 SE 


 just publish . 


 | the spirit of ENGLAND 


 to WOMEN | for the FALLEN 


 set to MUSIC for TENOR or SOPRANO SOLO , CHORUS , and ORCHESTRA 


 BY 


 OP . 80 


 DAILY TELEGRAPH 


 MORNING POST 


 DAILY CHRONICLE 


 EDWARD 


 PRESS notice 


 ELGAR 


 AND 


 DAILY NEWS 


 OBSERVER 


 GLOBE 


 BIRMINGHAM DAILY POST 


 the MUSICAL 


 I , 1916 


 the EARL of SHAFTESBURY , 


 the RT . HON , K.P 


 SHELLEY FISHER , 


 JUST 


 ORGAN WORKS 


 publish 


 the 


 JOHN SEBASTIAN BACH . 


 the CHORALE PRELUDES 


 WITH an introduction by 


 ERNEST NEWMAN . 


 BOOK XV . 


 ORGELBUCHLEIN 


 IVOR ATKINS 


 BROADWOOD 


 PIANOS 


 PIANO NOVELTIES 


 PORTRAIT ! BOSWORTH EDIUTION , 


 the LYRIC ORGANIST . 


 classic . 


 BOSWORTH & CO , , 


 now ready . 


 PART I. 


 OF A 


 BACH ’S 


 48 


 prelude & fugue 


 the well - temper CLAVICIIORD 


 24 


 0N , W , 


 RINCIPLES 


 » method 


 ere of 


 359 


 AND SINGING - CLASS circular . 


 AUGUST 1 , 1916 


 BRAHMS and the WALTZ . yaltz . doubtless man could have invent a more 
 enjoyable little musical form , but doubtless man never 
 dd ; and unless I be horribly mistaken , it will see a 
 few of the more solemn form out . Macaulay ’s New 
 Jealander will have long outgrow such transitory 
 thing as the symphony and the opera ; but if there be 
 my humanity leave in he he will still glow at the first 
 bar of a good waltz . a musical history in those 
 distant day may even have a chapter in which the 
 ymphony , the symphonic - poem , and the opera be 
 write about in much the same detached yet 
 wondering tone in which scientist now write of the 
 jora of the pliocene age : we know such thing once 
 exist , but we can not quite imagine what they look 
 ike . the musical historian in the New Zealander ’s 
 epoch will no doubt point out how in the mental 
 world , no less than in the physical , the large and 
 more unwieldy organism have all come to an 
 inexorable end . first go the mastodon and the 
 pterodactyl ; then leviathan and behemoth ; then the 
 epic ; then the three - volume novel ; then the english 
 oratorio ; then the Dreadnought ; then the Triennial 
 Festival ; then,—long after , of course — the opera and 
 the symphony . ' the New Zealander may hold that a 
 composer can say anything that be worth say in five 
 minute at the most ; and music may have revert to 
 the two fountain - head from which it sprang—-the song 
 and the dance , both in a highly concentrated form . 
 this stage of evolution in music will probably coincide 
 with the abolition of six - course dinner and the concen- 
 tration of a day ’s nourishment into a tabloid 

 when that time come I think the waltz will still 
 have for humanity a charm that no other dance 
 possess . other dance we tolerate ; the waltz be a 
 necessity of life . it be difficult to realize that there 
 be a time when the waltz be not , just as there be 
 a time when tobacco , so far as Europe be concern , 
 be not . but that be simply a testimony to the worth 
 of the waltz ; Nature try her ' prentice hand on many 
 charming things,—woman , wine , tobacco , bird and 
 flowers,—and then she make the waltz . the 
 composer as well as the public have all be 
 enamoured of it : I think I be correct in say 
 that there be no other dance form they have so 
 persistently delight in . they may turn up a 
 contemptuous nose at other dance ; but most great 
 composer would be as ashamed to admit they have 
 never compose a waltz as to have it say of they 
 that they have never kiss a girl ; indeed , a 
 psychologist might not unreasonably infer that the 
 composer who have be guilty of the one neglect 
 have be guilty also of the other,—their music 
 generally show it . in the day when the waltz be 
 just begin to rear its timid head , Mozart and 
 Beethoven condescend to be aware of its existence . 
 since then Schubert , Weber , Schumann , Brahms , 
 Chopin , Wagner , Strauss , Tchaikovsky , Berlioz , 
 Debussy , Ravel , Glazounov — to name only a few of 
 the man who have make musical history — have all 
 dally with the waltz at some time or other 

 perhaps they have do wisely , for it may be that 
 the waltz of some of these man will be remember 
 when their more ambitious work be forget . and 
 we may notea rather curious thing — that it be precisely 
 the giant who have handle this toy with the most 
 delight for themselves and the most charm for we . 
 nothing , indeed , be more delightful than the giant in 
 the relaxation of his small or light work 
 Tchaikovsky in the ' Casse - Noisette ' Suite , Wagner in 
 the ' Siegfried Idyll , ' Bach in the ' invention , ' Handel 
 in ' Acis , ' Strauss in one or two of his song , and — 
 good example , perhaps , of all — Brahms in his waltz . 
 for Brahms be the most serious of they all ; he 
 carry seriousness , indeed , to the verge of dullness 
 now and then ; and his waltz derive a special charm 
 from their mere contrast with his work as a whole , 
 the charm of the occasional smile on a face that as a 
 rule be keep too grim . Sir Robert Peel occasionally 
 smile , and Daniel O’Connell ’s description of his smile 
 be ' the silver plate ona coffin . ' the Brahms waltz 
 be the silver plate on the coffin of the ' german 
 Requiem ' and the ' Vier Ernste Gesiinge 




 386 


 361its rhythmic analysis be show by the square phrase 
 mark label a and B , with the figure 1 , 2 , 3 to 
 show repetition and variant of the two contrast 

 idea it contain . roughly speak it conform to 
 the same a b a scheme of Mr. Sorabji ’s indian 
 fragment ; it be more definite because more developed , 
 and it contain no contradiction of establish tonality . 
 yet it require a somewhat keen ear to recognise these 
 obvious quality in the harmonic setting give it by 
 its composer , and possibly many people who have 
 hear it in the concert - room more than once may 
 require to be tell when they see it set down here that 
 it be the principal tune of Mr. Eugene Goossens 's 
 quartet movement ' by the tarn . ' the difficulty to 
 the listener be certainly not due to any incapacity on 
 the part of the composer to think in clear melodic 
 term , but to his complex sense of harmony . his 
 subtlety in this direction wipe clean away the plain 
 harmonic concept which the tune in its ungarnished 
 form present to the ordinary hearer . this method be 
 typical of a great mass of modern music which must 
 be distinguish from the post - impressionist category 
 refer to above . I doubt whether Mr. Scott be quite 
 just in complain of the thematic poverty of modern 
 music as compare with that of a hundred year ago , 
 because in any comparison of that kind one be tempt 
 to place the ordinary modern work against the 
 exceptional masterpiece which have survive because 
 it be exceptional . Mr. Goossens ’s tune in ' by the 
 tarn ' may not be a great tune , but it be a tune which 
 will stand comparison with the invention of the 
 thousand and one less light who surround the 
 throne of Beethoven ; the difference be that when 
 they write a tune they turn the harmonic limelight 
 straight on to it . the modern creator of a tune 
 dispose his light more diversely ; sometimes , as in 
 this instance , let the tune fall among dark shadow 

 a study of modern music — for example , the later 
 pianoforte sonata of Scriabin — often show one that 
 the tune , or at any rate the thematic development , be 
 consistent and that appreciation would be easy and 
 one ’s power of remember the detail far great if 
 only one could get on familiar term with the harmonic 
 idiom . the doctrine of the composer be that we , the 
 humble listener , should make it our business to grow 
 up to whatever harmonic idiom they in their wisdom 
 may choose to adopt , but I doubt whether that be 
 sound doctrine . for there be fairly strong evidence 
 that a great many of they be not themselves on term 
 of easy familiarity with their own harmonic idiom . 
 one have only to examine the harmony with which 
 british composer endow the folk - song they frequently 
 quote to realise that many of they be exceedingly 
 uncertain as to the good point on which to throw their 
 light and shade of harmony , and the haphazardness 




 WILLIAM STERNDALE BENNETT , 1816 - 1875 . 


 6 - 1875 


 363 


 GERMANY : her music . 


 364the MUSICAL TIMES.—Avcust 1 , 1916 

 uncertain tone the criminality of a crass commer- 
 cialism . even Beethoven , with his late breath , 
 sang aloud of a world - wide brotherhood of man . 
 yet it be no new thing,—this refusal of a nation to 
 hearken to its own well - mean prophet 

 iii 

 _ but there be another aspect of the subject . be 
 it not a fact that every department of man ’s mental 
 activity can have a life of its own?—birth , maturity , 
 and decay 

 we see it in the intellectual period of Greece : 
 Socrates , Plato , and Aristotle . in Germany : Kant , 
 Fichte , and Hegel ; and in her music : Bach , 
 Beethoven , and Wagner . and after Wagner , what ? 
 have Prussianism exercise its baneful influence on the 
 once humanistic Germany , as far as her idealistic 
 music be concern ? how have militarism affect 
 music in that tuneful land 

 we must look to Strauss more especially for the 
 answer , since he well represent the afterglow of her 
 more glorious sunshine of musical beauty . for Strauss 
 undoubtedly voice the modern spirit of Germany . he 
 be of berlin,—bombastic , blatant , give to a brutish 
 outspokenness , and prone tocynicalcandour . though 
 at time there steal into his music strain of sanity , light 
 up with occasional flash of a very real beauty , 
 without doubt there be decide trace of a negative , 
 bastard philosophy . his art be vitiate by periodic 
 outburst of brutal discordance , and ofttime by a total 
 disregard for harmonial rectitude . in his writing 
 we detect a sinister revolt against interior law 
 and order . in his music we be treat to a 
 decidedly unpleasant heap - up cacophony , betray 
 an orgy of brutish forcefulness . indeed , in his 
 symphonic work , we be at time overwhelm by 
 an orchestral gluttony out of all proportion to the 
 nicety and limit of our aural capacity . many 
 an otherwise deliciously tender passage be obscure 
 and render of no account by a prior moment of 
 feverish revolt . the ear be bruise and batter by 
 an inartistic anarchy of noise , that render nugatory 
 any afterthought of possible loveliness . he be in his 
 heart , arrogant : in his inmost soul , pessimistic . his 
 nature betray a careless indifference towards the 
 sanctity of life : he seem to toy with the diviner 
 affirmation of the soul . he be more clever than 
 deep : brilliant rather than sincere . his ' also 
 sprach Zarathustra ' end on the note of agnosticism- 
 nescience . and in view of the forego , the follow 
 passage from his compatriot Novalis be of vital 
 interest 




 IV 


 music in RUSSIA 


 365 


 munificent 


 BEQUEST 


 the centenary of SCHUBERT 's 


 ‘ ERLKONIG 


 2242s 


 = 33 = = = 


 366Alfred R. Gaul , in his setting of ' Erlking ' for double 
 choir , use Walter Scott ’s version 

 Beethoven ’s admiration for Goethe be well known , 
 and at an early period of his art - career he make two 
 sketch for ' Erlkénig , ' one very long ; the various 
 * & c. ' mark in it prevent one , however , from know 
 what the song might have become in its final shape 

 can any of our reader inform we of the date of the 
 publication of Goethe ’s poem ? — Eb . , 4/.7 

 among the document leave by Thayer be a letter from 
 Hiittenbrenner to Ferdinand Luib , in which the writer tell how he 
 and Schubert go to see Beethoven eight day before the death of 
 the latter . Schindler ask Beethoven whom he would like to see 
 first , and the die composer reply , ' let Schubert come first . ' 
 Hiittenbrenner , an ardent admirer of his friend 's music , can not have 

 occasional wote 




 368 


 communion service 


 the canticle 


 anthem 


 369 


 SIR GEORGE HENSCHEL ’S new ' MASS 


 ST . MICHAEL ’S COLLEGE , TENBURY . 


 reconstruction of the ORGAN 


 37 


 accessory 


 371 


 ORGAN RECITALSMr . W. J. Wightman , at North Walsham Parish Church — 
 overture in c major , Ho//ins ; ' question ' and ' answer , ' 
 Wolstenholme ; Canzona , Wheeldon ; Offertoire in d flat , 
 Salomé 

 Mr. Albert Orton , at Walton Parish Church — variation 
 from the Septuor , Beethoven ; Toccata in F , Bach ; 
 Funeral March in a minor , A / endelssohn 

 Mr. Henry Riding , at Chigwell Parish Church — Andante , 
 Boyce ; Adagietto , Bizet ; Military March , Gounod 




 appointment 


 book receive 


 134 


 , 212 . 


 short anthem for four voice 


 o ye that love the LORD 


 p p 4 


 = : ss os eos = = 


 WU w 


 why SHAH RED RRR 


 o ye that love the LORD 


 o ye that love the LORD 


 377 


 instrument with sympathetic string . 


 to the editor of ' the MUSICAL TIMES 


 DR . TERRY and DR . HABERL . 


 to the editor of ' the MUSICAL TIMES 


 HANDEL at CANONS 


 to the editor of ' the MUSICAL TIMES . ' 


 CHURCHILL SIBLEY 


 378TuRLOGH MACSWEENEY , the most famous of present- 
 day irish piper , on July 3 , aged eighty - nine . bear at 
 Gweedore , Co. Donegal , in 1829 , Mr. MacSweeney , 
 who could claim descent from an irish Prince , be the last 
 of the celebrated Ulster piper , and get first prize at the 
 Chicago Exhibition in 1893 . his eccentricity for sixty 
 year be well know , but as a performer he be inimitable 

 EDWARD J. DE Coprett , of New York , the generous 
 patron of the famous Flonzaley Quartet , on April 30 . the 
 sad event occur suddenly , at his residence , soon after the 
 quartet party have play a Beethoven Quartet . Mr. Coppett 
 be of swiss origin , and the player be name after his 
 home in Switzerland . he be a man of great wealth 

 in our July issue we record the death , at the front , of 
 Mr. Bates , eld son of Mr. James Bates , the head of the 
 London College for Choristers . we now , with deep regret , 
 record the sudden death of Mr. James Bates ’s wife . deep 
 sympathy be feel with Mr. Bates in this double affliction 




 the late DR . MACLEAN . 


 HON . SEC . INTERNATIONAL MUSICAL SOCIETY 


 XUM 


 379 


 THOMAS weelke 


 siegfrie ' in the OPEN AIR , at ST . LOUIS . a HANDEL FESTIVAL 


 OM , 


 » AC 


 381 


 ROYAL ACADEMY of MUSIC 


 ROYAL COLLEGE of MUSIC 


 TRINITY COLLEGE of MUSIC 


 the ASSOCIATED BOARD of the ROYAL 


 ACADEMY of MUSIC and the ROYAL 


 COLLEGE of MUSIC 


 BEECHAM OPERA SEASON — ALDWYCH 


 THEATREMisses Ethel Toms ( Feodor ) , D. Ellinger ( Xenia ) , Edith Clegg 

 382 the MUSICAL TIMES.—Avcusr 1 , 1916 . 
 ( a nurse and the hostess ) , Messrs. Maurice D’Oisly } Mr. Mark Hambourg play the b flat minor Pianoforte char 
 ( Chinsky ) , Powell Edwards ( Piméne ) , Frederick Blamey | Concerto at the Tchaikovsky concert give on June 24 by the cent 
 ( Grigori ) Miss Olive Townend ( Marina ) , Mr. Robert Radford | Symphony Orchestra name after its conductor , " Mr , hear 
 ( Varlaam ) , and Mr. Alfred Heather ( Missail ) . the chorus , | George Shapiro , the inevitable ' Pathétique ' be A 
 which include member of the Alexandra Palace Choir , | play . St. 
 be exceptionally good . altogether the performance ] M. Maaskov ( violin ) and Mlle . Morvay ( pianoforte ) give ladi 
 be a considerable achievement . the scenery and costume | a fine recital on June 26 . eat 
 be very attractive . Mr. Eugene Goossens , jun . , again ] Miss Fanny Davies give a recital on June 27 . her pro . prov 
 conduct . gramme be classical , a type of music in the execution of cons 
 on July 6a revival of Puccini ’s ' Manon ' be well receive . | which she excel . dav 
 the name - part be very attractively sing by Madame Brola,| _ Miss Emma Barnett give she annual pianoforte recital on and 
 and Mr. Maurice O’Oisly be an excellent Des Grieux . Mr. { June 28 . she play the ' Waldstein ' Sonata with her choi 
 Percy Pitt conduct . another welcome revival be that of | usual ability , and some interesting new piece by her exce 
 Gounod ’s ' Romeo and Juliet , ' on July 11 . Miss Miriam | brother , John Francis Barnett . by I 
 Licette be a well - equip Juliet and Mr. Webster| Miss Harriet Cohen ( pianoforte ) and Miss Winifred Small of ¥ 
 Millar be an effective Romeo . Sir Thomas Beecham | ( violin ) , at a joint recital give on June 28 , play man 
 conduct . J. = McEwen ’s Sonata in F. Mr. Albert Fransella rem 
 eee eee perform some flute solo . her- 
 the ORIANA MADRIGAL SOCIETY . the only vocal recital give by Madame Kirkby Lunn Var 
 this Society give another well - design concert on July 5 . | this season take place on June 29 . a fine programme of triac 
 Weelkes , Vautor , Morley , Bishop , Grainger , Vaughan ] about twenty ' ancient and modern ' number be play 
 Williams , R. J. S. Stevens ( it be not often that this| splendidly interpret . Mis 
 composer ’s ' from Oberon in Fairyland ' be sing ) , and the association of Mr. William Murdoch and Mz . Albert Sarg 
 Elgar ( ' go , song of mine ' and ' weary wind of the|Sammons at recital amply fulfil its promise of fine T 
 West ' ) be represent . Mr. Herbert Withers play a| thing . Dohnanyi ’s sonata in c sharp minor be one item , Ap } 
 sonata for Violoncello by De Fesch ( early 18th century ) . | it be brave thus to include a composition by a live alien Roc 
 Mr. Kennedy Scott conduct . enemy . viol 
 nn a the farewell recital give on July 4 by Mr. York Bowen Mr. 
 before he take his place in the Army deserve much more Son 
 London Concerts , attention than we oa able to give to it . he play much pre ’ 
 modern music , and for the first time his Third Suite and Fra 
 the London Trio , Madame Amina Goodwin ( pianoforte ) , | Suite Mignonne . his wife sing a group of new song by arti 
 Mr. Pecskai ( violin ) , and Mr. Whitehouse ( ’ cello ) , give } Kathleen Richards . bal 
 the last subscription concert of the season on June 21.| Miss Irene Scharrer and Miss Myra Hess combine on 
 Brahms ’s Trio in C major ( Op . 87 ) , and Sergei Taneiev ’s | July 6 to give a recital of duet for two pianoforte . the 
 Trio in D major ( first time at these concert ) be the chief | programme include Bach ’s C minor Concerto , Saint - Saéns ’s 
 item of an excellently varied programme . the Trio party | ' variation on a Theme by Beethoven , ' and a group of three T 
 be atits good . Miss Elsie Illingworth sing and display | short piece , ' En Blanc et Noir , ' by Debussy . the quality of coir 
 a strong voice which at time be too liberally dispense . | the execution be all that be expect of two such his 
 but she have gift . distinguished artist . eve 
 the War Emergency Concerts continue to be given| Mr. Dario , a brazilian pianist , give a recital on July 9 . inst 
 very frequently . Mr. Isidore de Lara have wonderful } he play very well , but with a rather uncontrolled style . rela 
 perseverance . on June 23 , the british String ( uartet | he can hardly be say to justify the preliminary announcement . 4 
 play a sextet in A major , compose by W. H. Reed.| Miss Winifred Fisher ( mezzo - soprano ) sing daintily at her sim 
 it be a lucid piece of music with no affectation of extreme | first recital on July 9 . she have an agreeable voice and a Cor 
 modernity . three nocturne for String Quartet , from the | persuasive style . an excellent programme be present . per ! 
 pen of Arthur Trowell , be another new work that show } the pupil of Mr. Tobias Matthay ’s famous Pianoforte Ove 
 ability . at a concert give on July 6 , John Collett 's | School give three invitation concert in July ( 12 , 14 , and 20 ) . ( w : 
 sonata in A , for violin and pianoforte , be a feature . it isa } we can only express our general praise for the admirable Mo 
 fine specimen of 18th - century writing , and it be well play | result of this school of training . a 
 by Miss Dorothea Walwyn and Mr. Percival Garratt . a ] the somewhat over - herald appearance of Madame am : 
 new feature at recent concert have be the appearance of a } Tamisa take place at Queen ’s Hall on July 20 . she hasa res ] 
 ' Prima Donna ' choir , singe trio , & c. , with great | good voice , and sing moderately well . on | 
 effect . Elgar ’s ' the snow ' and ' fly , singe bird ' be alb 
 popular number of the repertory . on July 13 a string a a = 
 Juartet by G. O’Connor Morris be perform , and mad e 
 2 good impression . perform , and make ] music in the province . -- 
 the London String Quartet on June 24 play Schubert ’s ( by our own correspondent . ) acq 
 welcome Quintet in C and Chausson ’s Concerto for Pianoforte , pet 
 Violin , and String ( Quartet . Mr. Moiseivich , and his wife , ape 
 Miss Daisy Kennedy , supply respectively the pianoforte and BIRMINGHAM . . Ura 
 solo violin part . a new series of four concert be begin } chamber music concert have of late assume an important bod 
 on July 3 . besides some Mozart - Brahms , McEwen’s| development at Birmingham , and in a measure good work ' f # Ch 
 ' Biscay ' Quartet be play . at the second concert , on| have be do in that direction by the Midland Institute have 
 July 10 , ( quartet by Tchaikovsky and Schubert and a ] School of Music . in the first instance , the student attend cho 
 Phantasy Quartet by Frank Bridge for pianoforte ( Miss Ethel } Mr. Max Mossel ’s chamber - music class give quite an the 
 Hobday ) and string be perform . on July 17 Beethoven | interesting concert in the large lecture theatre of the pro 
 and Schumann be draw upon , and Eugene Goossens’s| Midland Institute on June 22 , and two day later a similar I 
 Phantasy Quartet be in the programme . concert be hold in the same theatre by the School of Music Bor 
 RECITALS . pupil . owe to the call of the Army , the performer este 
 Miss Gertrude Peppercorn give a highly temperamental | with one exception be lady . the programme provide mo : 
 performance of César Franck ’s prelude , Chorale , and Fugue | by Mr. Max Mossel ’s class consist of Brahms ’s Trio in Mr 
 at her recital on June 22 . a Pavane and a Valse by Ravel | c minor , two movement of Saint - Saéns ’s Quartet in b flat stu 
 and three of Rebikov ’s ' mood sketch ' be interesting | major , and Brahms ’s Pianoforte Quintet in F minor . the littl 
 item finely play . whole performance show much earnest endeavour and con 
 Mr. Mario Lorenzi be a distinguished harpist . his recital | progressive terdency in ensemble playing . the concert dep 
 on June 22 exhibit his exceptional command of his| give by the School of Music pupil reflect much credit mu : 
 instrument . one item of the programme be play by | upon pupil and teacher , and be on the whole of a pleasing Tu 
 eight of his pupil and himself . character , the programme include Mozart 's String Quartet M 
 Miss Elsie Williams , a débutante contralto , make a good ] in D minor , and the same composer 's String ( Quartet in mu 
 impression at her recital on June 22 . she have be well teach . |G minor . Dvordk ’s Terzett for violin and viola , and @ the 

 24 by the 
 tor , Mr. 
 jue ’ be 




 383 


 BOURNEMOUTH 


 BRISTOL 


 DEVON and CORNWALL . 


 DEVON 


 384 


 CORNWALL 


 MANCHESTER and DISTRICTAnd now to the outline of next winter ’s season , as already 
 reveal . at Hallé ’s the rival ambition of conductor last 
 year in the matter of novelty lead to these assume a large 
 proportion than be originally contemplate ; this year 
 will witness great restraint in this matter . there 
 be to be only four choral concert , one of they 
 Wagnerian ; ' Gerontius , ' under the composer ; ' Messiah , ' 
 cenducte by Beecham — a great ' novelty ' this , the more so 

 Miss Mignon Nevada will sing the soprano solo ; 
 Mlynarski will introduce Bantock ’s new ' hebride ' 
 Symphony ; Beecham will play in their entirety the 
 ' Petrouchka ' and ' L’Oiseau de Feu ' of Stravinsky , as well 
 as Symphonies by Mozart , Brahms No . 1 , and Beethoven 
 no . 3 ; Landon Ronald give Beethoven no . 7 , and 
 Savonov a liberal dose of Tchaikovsky and the classic . 
 there be a reasonable expectation that Paderewski , Ysaye , 
 Pachmann , and Busoni will appear , and doubtless some ot 
 the singer from the Opera . the orchestral concert of the 
 Gentlemen ’s series will be conduct by Messrs. Goossens , 
 Harty , Mlynarski , and Ronald , and the engagement be 
 expected of Miss Rosina Buckman , Miss Dorothy Moulton , 
 and Madame Mignon Nevada as vocalist , as well as 
 Madame Renée Chemet , Messrs. Forbes , Rubinstein , 
 Savonov , Sammons , and the London Philharmonic Quartet 

 Mr. Montagu - Nathan ’s series of five lecture on ' russian 
 composer , ' give at the Boudoir Theatre under the auspex 
 of the Russian Society , conclude with an account 0 




 ade ! 


 PAR 


 TIM 


 xum 


 385 


 content 


 special notice . 


 386 


 dure the last month 


 LLOTEY - PAPPOE , J 


 W. G 


 publish for 


 the H. W. GRAY CO . , NEW YORK 


 SONGS of LOVE 


 ( LIEBESLIEDER ) 


 WALTZES 


 for PIANOFORTE DUET 


 ( WITH QUARTET or CHORUS 4D LIB 


 G. ROTHERY 


 JOHANNES BRAHMS 


 the CROWN of EMPIRE 


 FOR CHORUS with PIANOFORTE or ORCHESTRAL 


 ACCOMPANIMENT . 


 the word by 


 FREDERICK GEORGE SCOTT , 


 the MUSIC by 


 EATON FANING 


 the MUSICAL TIMES 


 SCALE , of term for advertisement 


 LIB 


 IRE 


 ESTRAL 


 : " O.M 


 ENTS 


 serie 


 BY 


 POPULAR CHURCH MUSIC 


 J. H. MAUNDER 


 SERVICES . 


 ORGAN . 


 CHURCH CANTATA , 


 388 


 the " lute " " ' serie of HARVEST ANTHEMS 


 ES 


 BEBRRRERRRRRRR RR 


 FOR SO 


 FOR 


 FOR T 


 LET 


 PRRER ER 


 BEPRRR EERE 


 RE 


 PERRRRRRRERRR RRR 


 RRRRRREERE 


 EE 


 HARVEST festival MUSIC 


 CANTATAS . 


 SONG of thanksgive HARVEST CANTATA 


 for SOPRANO , TENOR , and BASS ( or CONTRALTO ) SOLI for SOPRANO ( or TENOR ) and CONTRALTO ( or 


 and CHORUS BARITONE ) SOLI and CHORUS 


 SHAPCOTT WENSLEY JULIUS HARRISON . 


 A GOLDEN HARVEST 


 BY 


 for TENOR and BASS SOLI and CHORUS JOHN E. WEST 


 the word and hymn select and write by 


 HENRY KNIGHT 


 the music compose by a song of thanksgive 


 THOMAS ADAMS , for CHORUS and ORCHESTRA 


 for TENOR and BASS SOLI , CHORUS , and ORGAN the GLEANER ’ s HARVEST 


 BY 


 THOMAS ADAMS . for — — — voice 


 HARVEST - TIDE the JUBILEE CANTATA 


 for TENOR and BASS SOLI , CHORUS , and ORGAN or 


 SMALL ORCHESTRA for SOLO voice , CHORUS , and ORCHESTRA 


 BY 


 C. M. VON WEBER 


 BY 


 HUGH BLAIR . — — — — 


 HARVEST CANTATA a harvest song 


 for CHORUS , semi - chorus , and ORGAN for SOPRANO SOLO and CHORUS 


 by by 


 GEORGE GARRETT . C. LEE WILLIAMS . 


 TWELVE hymn for HARVEST | the sower go forth SOWING 


 the joy of HARVEST and SEA 


 a HARVEST HYMN of praise | COME , YE thankful PEOPLE , come 


 compose by 


 BASIL HARWOOD 


 original composition for the ORGAN 


 twelve miniature 


 ORGAN 


 compose by 


 H. M. HIGGS 


 BDWYN A. CLARE 'S 


 VERY popular harvest anthem 


 chorale prelude 


 for the ORGAN 


 C. HUBERT H. PARRY 


 first set . 


 original composition for the ORGAN 


 CHORALE 


 . CHORALE 


 . CHORALE 


 . CHORALE 


 . CHORALE 


 . CHORALE 


 7 . CHORALE 


 second set . 


 original composition for the ORGAN 


 PART IV 


 containing 


 forty - three country dance 


 FROM 


 the ENGLISH DANCING MASTER 


 ( 1650 - 1728 


 describe by 


 CECIL J 


 SHARP 


 AND 


 GEORGE BUTTERWORTH 


 COUNTRY DANCE TUNES 


 FROM 


 the 


 ENGLISH 


 DANCING MASTER 


 1650 - 1728 ) . 


 arrange for the PIANOFORTE BY 


 CECIL J. SHARP 


 SETS VII . and VIII . 


 N.B 


 FS 


 CARS ! 


 COLE 


 COWE 


 COWE 


 DAVII 


 DAVII 


 ELGA 


 ELGA 


 ELGA 


 ELGA 


 ELGA 


 FLET 


 HOLI 


 HOLS 


 JOHN 


 LEM : 


 LEM : 


 LEM ! 


 REEL 


 WEN ! 


 WES1 


 WEST 


 NOVELLOS MUSIC 


 N FOR 


 ORGAN 


 YRGAN abbreviation . 


 F.S. F.O. S.0 . P.S. P.C. 


 392 


 ORGAN 


 MUSIC 


 british composer 


 suitable for 


 funeral or MEMORIAL service 


 LEMARE , E. 


 FOR 


 soldier , SAILORS , school , HOMES 


 ETC 


 CONTAINING 


 NATIONAL ANTHEMS , ETC . , of the ally 


 MARCHING song . 


 NATIONAL and FOLK - SONGS 


 HYMNS 


 PRICE ONE shilling 


 MADE in ENGLAND 


 CONTR 


 STRONG 


 YOR 


 MICH 


 GUIL 


 JOHN C 


 COM 


 PRIVA 


 TRAIN 


 OPERA , 


 H , SA