


HE MUSICAL TIME


AND SINGING-CLASS CIRCULAR. 


PUBLISHED ON THE FIRST OF EVERY MONTH


MUSICAL LIBRARY


EDITED BY CHARLES HALLESECTION V.—“ Very difficult.” 
s. d.| No

o . Bach, J. S.—Fantasia Chromatica in D minor 
4 ©} 10. Henselt, AA—Romanza and Study in F sharp, Op. 2...... 
: D -M 11. Chopin, F.—Scherzo in B flat minor, Op. 31 ...-..--+++ 
“Hell min 6 In the Press. 
+ Beth, St.— 4 ©|12. Schumann, R.—Romanza, in D Minor, Op. 32 
A oven, 13. Mendelssohn, F.—Capriccio, in F sharp minor, Op. 5 “si 
& Men ie) 6 ©} 14. Beethoven, L. van.—Grand Sonata, in E major, Op. 109

13. Schumann, R.—Two Caprices in C and E, from Op. 5 
x ay FE iP 4 16. Bach, S.—Prelude and Fugue, in A minor 
eber, C. M. 17. Chopin, F.—Impromptu in G flat, Op. 51 
tab = 4 18, Liszt.—Three Hungarian Airs ............0iesseessseessees eee akon 
The title « Very Difficult” is not meant to convey the idea that this Section will provide pieces of the extreme difficulty suited to 
q na’ cases only (this being beyond the scope of a “ School”); it is by taxing in a high degree the general Student's intellectual 
Fee oa as well as their mechanica! powers that the works included will be found “ very difficult” to play well




BH FORSYTH BROTHERS, 


TH 


MU 


SIC 


AL 


IMES.—J 


ANUAR 


YI 


» 187 


M FESS 


735


FABRICATION SUPERIEURE ET ARTISTIQUE


MILLEREAU & CO., OF PARIS. 


MUSICAL INSTRUMENTS FOR ARTISTS. 


IMPORTANT TO THE MUSIC TRADE. 


RGAN, PIANOFORTE, HARMONIUM, HAR


HE CREATION.—SECOND-HAND VOICE


REDUCED PRICES OF —— 


OUSELEY AND MONRK’S


POINTED PSALTER


ROY


D'AM! 


NOVELLO, EWER AND CO., 1, BERNERS STREET (W.), AND 35, POULTRY (EC) | 4) <, 


737


NEW SONGS


COMPOSED EXPRESSLY FOR THE


CONCERTS


SPA, R—CADA IL TIRANNO REGNO 


WHITNEY


LUMENTHAL, J.—THE LAMENT OF 


OUNOD, CH.—_THERE IS A GREEN


ATTON, J. L—FOR THEE ALONE I 


ROSE MARIE


TO


YDENHAM, E. A.—E 


OURS, B.—STARS OF T J 


ROYAL ALBERT HALLC. JEFFERYS, 67, BERNERS ST. 
JUST PUBLISHED

NEW PIECES, sy LOUIS DUPUIS. 
GEMS OF SACRED ART :— 
No. 1. HENRY VII.’S CHAPEL ... aa Sue 
Beethoven's “ Hallelujah,” from Mount qtcge 
2. ST. GEORGE’S —— EL

Jackson’s Te Deum




4- MUNICH CATHEDRAL ... 


5. FREIBURG CATHEDRAL 


MELODIOUS MELODIES, 


ARRANGED FOR ORGAN OR HARMONIUM


PRICE TWOPENCE EACH


MICHAEL WATSON


THE ELEMENTS OF MUSIC 


SYSTEMATICALLY EXPLAINED BY 


HENRY C. LUNN


NEW SONGS. 


JUST PUBLISHED


JEFFERYS’S M LUSICAL JOURNAL


OTHER NUMBERS IN THE PRESS


PRICE ONE SHILLING EACH, NETT


C. JEFFERYS & CO."S ENGLISH MODEL HAR- 


67, BERNERS STREET, W


LOUCESTER COUNTY ASYLUM.—FEMALE 


EREFORD CATHEDRAL.—CHOIR BOYS.— 


ORK MINSTER CHORISTER SCHOOL


T. MICHAEL’S COLLEGE, TENBURY.—A


YY SELINGTON COLLEGE, WOKINGHAM


M USIC TRADE.—PIANOFORTE TUNER 


TRULY 


739


THE MUSICAL TIMES 


AND SINGING-CLASS CIRCULAR. 


JANUARY 1, 1875


THE LICENCE OF ARTISTS. hael; the veriest dolt can feel somewhere in his

soul the force of purity and beauty. Nor is 
there a missing link between you and the god-like 
nobleness of the Apollo Belvidere, or the inimitable 
tenderness and sorrow of Milton’s Lycidas. In all 
cases of which these are types, your mind is directly 
mm rapport with that of the creator, and nothing can 
come between to affect the natural working of one 
upon the other. But music, poor music! When 
Beethoven rose on the eagle wings of his genius to 
the height of the Ninth Symphony, what was the 
immediate result? A mass of paper covered with 
ink-stains ; differing no way in appearance from that 
other mass of paper which, because containing an 
abortive oratorio by Jones, has gone to the butter- 
man’s, Nay, the chances are that Jones’s work, 
neatly written and handsomely bound in joyful anti- 
cipation of British Museum honours, touches the 
butterman’s heart, and finds a place on his book- 
shelf, “to fill up,” while the Ninth Symphony MS. 
is distributed to his customers. Here’s the rub

hoven can do no more with his majestic art- 
tteation, and it remains speechless and unlovely— 
achaos of blots and lines—save to the very few who 
hold the key to its meaning and know which way to 
tum it in the lock. Now come the intermediaries




741


THE HANOVER-SQUARE ROOMS. 


ROYAL ALBERT HALL CONCERTS


MENDE 


743


SB


ENGLISH MUSIC AT THE ALBERT HALL. 


SIGMUND MENKES


CRYSTAL PALACE


ROYAL ALBERT HALL CONCERTSIN spite of the attractions announced at these concerts 
on the evenings devoted to the higher class of music, there 
can be no doubt that what may be termed the “ People’s 
nights” have drawn together by far the largest numbers. 
The “Irish,” ‘‘Welsh,” and even ‘‘English” Evenings 
have made a powerful appeal to the sympathies of those 
who love national music for its own sake, and the names of 
Madame Lemmens-Sherrington, the new soprano, Madlle. 
Levier, Miss Edith Wynne, Madame Patey, Miss Sterling, 
Mr. Sims Reeves, Mr. Cummings, Mr. Whitney, and many 
other artists of established reputation, offered a sufficient 
guarantee that the vocal music would be done ample justice 
to. ‘Israel in Egypt” and Bach’s St. Matthew ‘“ Passion 
Music”’ (in which Mdlle. Levier, who sang the soprano 
part, more than confirmed her former success) have shown 
the powers of the choir to much advantage. Mention 
must be made of the performance of Dr. Hans von Bilow, 
Mr. C. Hallé, Miss Agnes Zimmermann, Miss May, Miss 
Emma Barnett (who played her brother’s clever Concerto), 
Mr. Walter Bache and Mr. J. F. Barnett, all of whom have 
thoroughly supported their reputation as exponents of the 
highest style of classical music. On one of the ‘‘ English” 
nights Sir Sterndale Bennett’s Overture ‘‘ Paradise and the 
Peri,’’ Mr. G. A. Macfarren’s “‘ Festival Overture” and Mr. 
Prout’s Organ Concerto (Dr. Stainer being the exponent 
of the Organ part) were conspicuous features in the 
programme, and elicited the warmest applause. It would 
be impossible to name one half of the various compositions 
performed during the month; but we may say that the 
scheme at first announced has been conscientiously 
carried out. Mr. Barnby and the talented conductors 
who occasionally replace him, are fully deserving of the 
highest praise for their energy and good will in the cause

THE concert given by the Royal Academy of Music on 
the roth ult., at the Hanover Square Rooms, attracted a 
large number, not only of the patrons and friends of the 
Institution, but of the general public; for, apart from the 
fact of an excellent orchestra, choir, and solo vocalists, 
and instrumentalists appearing on the occasion, the circum- 
stance of its being the last performance ever to be given in 
these Rooms, was in the highest degree interesting. Under 
the able direction of Mr. Walter Macfarren, an excellent 
selection was performed, so well indeed that in the 
execution of many of the pieces, the audience almost forgot 
the fact of its being a concert of students. The pianists— 
Miss Alice Curtis, Miss Conolly, Miss Katie Steel, Miss 
Bucknall and Mr. Walter Fitton—especially distinguished 
themselves ; and Mdlle. Gabrielle Vaillant in Beethoven’s 
Romance in F, for the Violin, was most enthusiastically 
received. Mr. G. A. Macfarren’s Cantata ‘ Christmas,” 
was rendered throughout with much care and precision, the 
principal parts being admirably given by Miss Jessie Jones 
and Miss Barkley. The composer was loudly called for at 
the conclusion of the Cantata, and bowed from the Royal 
box. Praise must also be given to the other vocalists— 
Misses Marie Duval, Reimar, Bolingbroke and Nessie 
Goode, Messrs. Henry Guy and Ap Herbert—Miss Jessie 
Jones creating a marked effect in “ Hear ye, Israel,” from 
“Elijah.” The concert concluded with the National Anthem. 
Sir Sterndale Bennett, Principal of the Academy, and many 
professors and former students of the Institution were 
present

Royat AcapEmy oF Music.—The examination for the 
Westmorland Scholarship and Potter Exhibition took place

on Monday the 2rst ult., the examiners being the Pring; 
(Sir Sterndale Bennett), Mr. F. R. Cox, Signor M., Gari 
Mr. H.C. Lunn, and Mr. G. A. Macfarren. The 
were as follows:—Westmorland Scholarship, 
Charlotte Agnes Larkcom, elected; Potter Exhibition, 
Miss Alice Mary Curtis, elected

Miss Grace Linpo gave a highly successful concert a 
the Beethoven Rooms, Harley Street, on the rsth ult 
The programme was carefully drawn out, and the entire 
performance gave the greatest satisfaction. Miss Lindo 
sang the recitative and aria, ‘“‘ Non piu di fiori” (with the 
unrivalled clarinet obbligato of Mr. Lazarus), Adolphe 
Adam’s “ Cantique de Noél,’ Dr. C. G. Verrinder’s new 
ballad, ‘‘The tale he told me,” and Lachner'’s [ig 
“‘ Waldvoglein” (violoncello obbligato, Herr Schuberth), 
Miss Julia Sydney, Madame Elwood Andrea, Messrs. Noble, 
Belmont, Le Messurier, Dexter, and Trelawney Cobham 
rendered valuable service in the vocal selections, and Miss 
Josephine Lawrence, Mr. Pearce, Herr Schuberth, Mr. 
Lazarus, and Herr Oberthir contributed instrumental 
pieces. The vocal music was accompanied by Dr, 
Verrinder

A musicaL performance was given by the pupils of the 
London Society for Teaching the Blind to Read on the 4th 
ult., at the Institution, Upper Avenue Road, Regent's Park, 
The first part included a selection from the ‘ Messiah,” and 
the second part was miscellaneous. Under the able 
direction of Mr. Edwin Barnes, Professor of music at the 
Society’s School, the whole of the choral pieces were 
excellently rendered; and the solos, both vocal and 
instrumental, were worthy of much praise. The Chair was 
occupied by F. Peterson Ward, Esq




REVIEWS


745


746


BON SOIR


#112


TREBLE


——— FE 


HHL 


TTT, 


XN


C | 2 | } 


GOOD NIGHT


_O : £ 


EE 50 SW


WHATE


C. JEFFERYS. 


75


FREDERICK BRUCKMANGallery of German Composers. By Prof. Carl Jager. 
With Biographical and Critical Notices by Edward F. 
Rimbault, LL.D

Tuts volume of Portraits may be conscientiously recom- 
mended as a most valuable gift-book to those who, having 
made acquaintance with the great German composers 
through their works, are anxious to look on the features and 
expression of men who have bequeathed such inestimable 
legacies to the world. Bach, Handel, Gluck, Haydn, 
Mozart, Beethoven, Schubert, Weber, Mendelssohn, Schu- 
mann, Meyerbeer and Wagner are the twelve artists chosen 
for illustration, the portraits having been carefully executed 
from oil paintings by Professor Jiger, and photographed in 
the first style of the art. The biographical and critical 
notices, which have been supplied by Dr. Rimbault, although 
short, afford every necessary information, one advantage 
being that, unlike many such biographies which have come 
before us, the facts may be relied upon. A small, but well 
designed wood-cut forms the head-piece of each notice; 
and the gorgeous binding of the volume will make it—apart 
from the richness of its contents—a most attractive book 
for the drawing-room table. Such a work as this should 
be warmly welcomed by art lovers in this country, for in

i




RELFE BROTHERS


ORIGINAL CORRESPONDENCE


HARMONY PRIZE OF THE CHURCH CHORAL 


SOCIETY


TO THE EDITOR OF THE MUSICAL TIMES. 


FREDERICK ILIFFE


MUSICAL SETTINGS OF THE TE DEUM. 


TO THE EDITOR OF THE MUSICAL TIMES


W. J. LOWENBERG


THE MOVEABLE-DO MADE VISIBLE. 


TO THE EDITOR OF THE MUSICAL TIMES


754


TO CORRESPONDENTS


BRIEF SUMMARY OF COUNTRY NEWSfashionable, and the whole performance gave the greatest satisfaction

Currron.—On Saturday, the roth ult., Mr. Charles Hallé and 
Madame Norman-Neruda gave a pianoforte and violin Recital at the 
Victoria Rooms, forming one ofthe series of Clifton Winter Entertain- 
ments organised by Mr. James C. Daniel. The programme included 
selections from Beethoven, Rust, Heller, Ernst, Chopin, Liszt, 
Vieuxtemps, Brahms, Joachim, and Schumann. Notwithstanding the 
inclemency of the weather, there was a large audience. The two 
artists played in their usually excellent manner, although Madame 
Neruda was, we regret to hear, suffering from severe indisposition, 
and was obliged to sit down during a portion of the performance

ee

Croypon.—The annual concerts given by the Whitgift School-boys 
took place on Wednesday and Friday evenings, the 16th and 18th ult, 
when Mr. Cummings’s Fairy Ring was performed, and a selection of 
part-songs, &c. The band was efficient, and played the rather diff. 
cult accompaniments to the Cantata in excellent style. At the concl. 
sion of the concert, R. A. Heath, Esq., one of the governors of this 
admirable institution, made a very effective speech on the advan 
of giving boys a knowledge of music, and highly complimented Mr, 
Griffiths on the state of efficiency to which he had brought the 
boys. A magnificent testimonial (the gift of Mr. Heath) was they 
presented to Mr. Griffiths, consisting of a silver-gilt inkstand, mam. 
factured by Messrs. Elkington, with a suitable inscription ona 
scroll, supported by two figures of boys holding trumpets, and valued 
at about forty-five guineas. Mr. Griffiths was much affected by the 
unexpected honour, and returned thanks in a suitable speech, Mr 
Hullah was present at the concert on Friday

Devizes.--The Amateur Choral Society gave a performance 
Christmas Carols and a miscellaneous selection on Monday, the 14th 
ult., under the direction of Mr. J. T. Abraham. The most admired 
pieces were Baumer’s part-song, “ The chimes of Oberwesel,” with 
bells obbligato, Pinsuti’s ‘‘ And so shall I,” Roeckel’s “ Sweet Lisette” 
“The Wave,” duet (Guglielmo), “Scenes that are brightest,” from 
Maritana, and a chorus from Flotow's Marta. The great attraction 
of the evening was the pianoforte playing of Mr. Bambridge, the 
organist of Marlborough College. His performance of the finale from 
Beethoven's “ Waldstein” Sonata, and “ Norwegian Melodies” (W.§ 
Bambridge) created a perfect furore amongst the large audience 
filling the Town Hall. Mr. Sly and Mr. W. Price accompanied

Devonport.—A concert was given at the Mechanics’ Institute on 
Wednesday evening, the gth ult., by Mr. W. H. Hannaford, organist 
of St. John’s. The orchestra was composed of a band and chors 
numbering between sixty and seventy performers. The pieces form. 
ing the principal features of the programme were, Birch’s pastoral 
Operetta, Robin Hood, and Mendelssohn’s Motett, ‘ Hear my prayer.’ 
Mr. W. H. Hannaford conducted; Mr. W. W. Brown presided at the 
pianoforte and Mr. C. Clemens at the harmonium. Miss Triggs 
(soprano) sustained the part of Maid Marian. Mr. Donovan, 
Mr. Boolds, and Mr. Rendle were the other vocalists. Mendelssohn's 
“ Hear my prayer,” by Miss Triggs and chorus, was admirably ret 
dered. A local favourite, Miss Snell, sang “ O bid your faithful Arie 
fly” with such effect as to enlist a hearty encore. The duo for pian 
forte and harmonium, by Mr. and Master Hannaford, was encored, 
and at the close of the second performance Master Hannaford re 
ceived the ovations of the whole house. A solo on the violin, by Mr 
Pardew, and the chorus, “Let the hills resound,” brought 
evening's entertainment to a close

DurHaM.—On the 22nd ult. Mr. J. C. Whitehead, late organist 
St. Cuthbert’s Church, and assistant organist of the Cathedral, and 
who has just been appointed organist of Bury Parish Church, was, 
together with the choir of St. Cuthbert's, entertained at dinner by the 
vicar, churchwardens, and congregation. After the dinner Mr. 
head was presented with a valuable testimonial, consisting of a 
watch and chain, a gold signet ring, and an inkstand

EastTBourNE.—On Monday, the 21st inst., Mr. J. H. Deane, the 
organist of Trinity Church, gave the last of a series of eight weekly 
concerts of classical music. He has been assisted by Mr. J. Taylor 
(organist of St.. Saviour’s, Eastbourne), his brother, Mr. Edward 
Deane, of the Crystal Palace and Philharmonic Orchestras, Miss 
Roper, Miss Headland, Herr Cramer, Herr Siebenheller, Messrs. 
Cooper, &c., instrumentalists ; and Miss F. Douglas, the Misses 
and Mr. C. Roper, vocalists. Amongst other works, Haydn's 
Symphony, a portion of Beethoven's Pastoral Symphony, Septett, ft, 
have been performed. The songs have included Gounod's “Ave 
Maria,” Handel's “ Let me wander not unseen,” “ Revenge! Ti 
cries,” “ Honour and arms,” &c

EpInBurGH.—Sir Julius Benedict's St. Peter was performed on the 
30th November, in the Edinburgh Music Hall, by the Choral bas 
The vocalists were Mdlle. Enequist, Miss Marion Severn, 3% 
Bentham, and Signor Agnesi. The tone of the chorus was g¢ 
good, the parts were well balanced, and the points of Spree 
caught up with precision and vigour, and, taking all in all, the 4 
singing was a credit to the Society, and to Mr. Adam Hami — 
conductor. Mr. Carrodus and his well - disciplined followers 
thoroughly efficient, and the accompaniments and incidental sy@- 
phonies were splendidly played. Miss Severn produced 2 | pa 
impression in ‘O, thou afflicted,” which was sung with Lee = 
expression. Mdlle. Enequist gave “I mourn as a dove a it 
quisite tenderness. Mr. Bentham sang the tenor solos, feel 
Wadmore, who took his part at short notice, sang with great 
“O that my head were waters.——PRoFESSOR OAKELEY gave af ‘a 
performance on the roth ult., in the University Music Classroom




22 


755


756


DURING THE LAST MONTHgenerally effective, especially the string portion, which had the advan Second ] 
n ective, e i . - F

tage of the assistance of the Beethoven Quintette Club. Mr. F. M2@LLoY, Pe L.—The Shipwright. Song. Words

H. Torrington conducted. The third concert was given on written by F. E. Weatherley. 2s. NG 
conte imped bg and ag ee Mrs. ge was F ned —— Rose-Marie. Song. The Words written by F. E. Weatherley, » D 
vocalist, and was much applauded in her several songs. e Epwin 
Beethoven Quintette Club, consisting of Messrs. C. N. Allen and J. C. ACIRONE, C. A.—Lullaby. Song. In C and E, “op 
Mullaby (violins), H. Heind’l and W. Rietzel (violas), and Wulf Fries The Words written by Sir Walter Scott. 2s. “Penite, 
(violoncello), performed Beethoven’s Theme and Variations, Op. 104, TKINSON, FRED. C.—When all the World ‘- for each

Mendelssohn's Scherzando and Adagio, Op. 87, and Piano Concerto, ss 
Op. 25 (in conjunction with Miss L. Crowle), and several overtures Young. Song. Words by Prof. Charles Kingsley. 2s. suitable

WoopxuouseE.—On the 8th ult. a miscellaneous concert of sacred BRADFORD, JACOB. asp Riper RB . Nune 
and secular music was given in the Mechanics’ Institute, Institution dimittis (Chant Service in A). Set to the 3rd and 8th Gregorian 
Street, by the members of the choir of St. Mark’s Church, assisted by | Tones, with several Endings. 8vo., 3d. } io 
several well-known singers. The object of the entertainment, as ROSLAND JONATHAN.—Evening Service in sdditiona 
stated by the vicar (the Rev. J. S. Abbott), was the very laudable one ® Bb pee NG hy of Magnificat and Nunc dimittis, with accom dicite,” a1 
of establishing a fund for providing music for the use of the church. | 2h: ent for Or ge Pinucheate 8vo., 8d ‘ This pc 
The first part of the programme consisted of selections from the | P 8 : z baa

Oratorios of Handel, Haydn, and Beethoven, and also from Mozart's AREBROTHER, BERNARD.—Te Deum lauds. bishop of

ae Mass ; hm agg oo bg apa he of glees, pisi-cvees Be mus, in BY. 8vo., 6d. EN 
iss L. A. Buckingham, in ‘‘ But Thou didst not leave,” acquitted her- 7




& C0


28


., 64


23 28. 


8¥0


757


C™~S


THE ANGLICAN HYMN-BOOK


NEW EDITION, REVISED AND ENLARGED. 


SECOND SERIES. 


HE ANGLICAN CHORAL SERVICE BOOK, 


USELEY AND MONK’S PSALTER AND 


OULE’'S COLLECTION OF WORDS OF 


ADDITIONS TO THE 


REY. T. HELMORE’S PLAIN SONG WORKS


R. C. G. VERRINDER’S SIX KYRIES


HE PSALTER, PROPER PSALMS, HYMNS, 


JOULE'S DIRECTORIUM CHORI ANGLI- 


HE ORDER FOR THE HOLY COMMUNION


THE VILLAGE ORGANIST


758


COMPOSITIONS BY


SIR JOHN GOSS


SOCIETY FOR PROMOTING CHRISTIAN KNOWLEDGE, N


NOTICE. I


NEW AND CHEAPER EDITION


760


OCTAVO EDITIONS OF


SERVICES BY MODERN COMPOSERS


PUBLISHED BY NOVELLO, EWER AND CO


T. BEST.—A MORNING, COMMUNION


BAPTISTE CALKIN.—MORNING, COMMDU


R. JOHN B. DYKES.—A MORNING, COM


R. G. M. GARRETT.—A MORNING, COM


R. G. M. GARRETT.—A MORNING, COM


JOHN GOSS.—TE DEUM LAUDAMUS 


JOHN GOSS.—THE ORDER FOR THE 


SIR JOHN GOSS.—THANKSGIVING TE DEUM 


LFRED H. LITTLETON.—THE MORNING 


LFRED H. LITTLETON.—THE EVENING 


ALTER MACFARREN.—A SIMPLE MORN


H. MONK.—THE OFFICE OF HOLY 


GAMUEL PORTER. — A MORNING, COM


C HUBERT H. PARRY.—A MORNING, COM- 


AMUEL REAY.—A MORNING, COMMUNION


HENRY SMART. — A MORNING, COMMU


SERVICES BY MODERN 


R. J. STAINER.—A MORNING, COMMUNION 


R. CHARLES STEGGALL. — A MORNING


H. THORNE.—A MORNING, COMMUNION


B RTHOLD TOURS.—A MORNING, COM- 


HOMAS TALLIS TRIMNELL. — CHANT 


NEW VOLUMES


NOVELLO’S


PART-SONG BOOK


VOLUMES X. AND XI


A COLLECTION OF


FOUR-PART SONGS 


RL. DE PEARSALL


CONTENTS OF VOL. X


CONTENTS OF VOL. XI. 


THE REV. R. BROWN-BORTHWICK’S 


SUPPLEMENTAL HYMN & TUNE BOOK


AND SERIES OF


MODERN KYRIES


HYMN TUNES, CHANTS, ETC


SEE UNDER


D® STAINER


J _ BAPTISTE CALKIN


H. H. PARRY


D® DYKES. 


30RTH


RRISS


THE


HARMONIUM TREASURY


A SERIES;OF SELECT PIECES


ARRANGED BY


J. W. ELLIOTT


VOL. I. SACRED


VOL. II. SECULAR. 


WNDON: NOVELLO, EWER AND CO., 1, BERNERS STREET, W., AND 35, POULTRY, E.C


FUST. PUBLISHED


AMBORNE PARISH CHURCH.—WANTED 


IE AWAY! HIE AWAY! 


ORIGINAL COMPOSITIONS; 


FOR THE ORGAN


E.° SPEAS