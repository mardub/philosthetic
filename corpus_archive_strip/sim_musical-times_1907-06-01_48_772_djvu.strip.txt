


THE MUSICAL TIMES


FOUNDED IN 1844. 


PUBLISHED ON THE FIRST OF EVERY MONTH


ROYAL CHORAL SOCIETY. LONDON 


MAJESTY THE


ROYAL ACADEMY OF MUSIC, SIR EDWARD ELGARS 


TENTERDEN STREET, W


THE DREAM OF GERONTIUS


BIRMINGHAM AND MIDLAND INSTITUTE


SESSION 1906-1907


: | MANCHESTER COLLEGE OF MUSIC. 


THE ROYAL COLLEGE OF ORGANISTS


UM


THE ASSOCIATED BOARD |PHILHARMONIC SOCIETY OF SYDNEY, 


ROYAL ACADEMY OF MUSIC AND ROYAL COLLEGE OF AUSTRALIA 


UNIVERSITY


VICTORIA COLLEGE OF MUSIC, 


LONDON


INCORPORATED 1861


DURHAM


OF


GEO


A.V.C.M., L.V.C.M., F.V.C.M., 


INCORPORATED GUILD 


MUSICIANS


OF CHURCH


ASSOCIATE (A.1.G.C.M.), LICENTIATE (L.1.G.C.M.), FEL- 


1907 COMPETITIONS. 


REGISTER OF ORGAN VACANCIES. | 


CORRESPONDENCE SCHOOL 


LONDON


OF MUSIC


GRADUATED POSTAL LESSONS IN HARMONY, 


HAS YOUR LOVE


NATIONAL COLLEGE OF MUSIC. 


CHURCH “ORG: ANISTS’ SOCIETY. 


N: ATION: AL CONSERVATOIRE


MACDONALD SMITH 


SYSTEM


OF TOUCH AND TECHNIQUE


FROM BRAIN TO KEYBOARD


OF MUSIC


A.T.C.L


LIGHT ON PIANOFORTE 


PLAYIN


THE 


ROYAL COLLEGE OF ORGANISTS. 


FELLOWSHIP EXAMINATION


JULY, 1907. 


| SOLO-PLAYING TEST PIECES - 


14 


AL. EX. GUILMANT 4 


LEO


IBBS & TILLETT


YW ——— 


AND 


= MADAME 


KIRKBY LUNN. 


CL. MADAME 


™ CLARA BUTT 


AND MR. 


KENNERLEY RUMFORD 


M MR. 


WILLIAM GREEN. 


“MM


PLUNKET GREENE 


CHARLES KNOWLES


AND 


- DALTON BAKER. 


LEOPOLD GODOWSKY. TIVADAR NACHEZ. 


MONS MONS. 


JOHANNES WOLFF. HOLLMAN. 


THE


ROSE STRING QUARTET 


(OF VIENNA) 


CAPET STRING QUARTET 


(OF PARIS


ENGAGEMENTS NEGOTI ATED FOR ANY ARTISTS


IBBS & TILLETT


3129 MAYFAIR


PIANOFORTE


BY


TO GU ARANTEE 


SATISFACTORY RESULTS


IN EVERY CASE. 


MUSICAL AND CONCERT AGENTS, 


10, HANOVER SQUARE, LONDON


FECHNIQUE


CORRES PON DENCE


UCKINGHAM GATE


PROFESSIONAL NOTICES


CHANGE OF ADDRESS 


MR. SIVORI LEVEY 


| STANDARD RECITATIONS AT THE PIANO 


MISS ELLEN CHILDS 


MISS ESTELLA LINDEN 


MISS ETHEL RADBURN 


(SOPRANO). 


MISS AGNES WALKER


DRAMATIC SOPRANO


| MR. H EN RY BEAUMONT 


FESTIVAL TENOR 


FRANCIS GLYNN 


(TENOR). 


MR. GWILYM RICHARDS


TENOR


D* BORLAND’S MUSIC LECTURES


S.E


ARTHUR WALENN 


(BARITONE


ROBIN OVERLEIGH 


| (BASS) 


" A.R.A.M., —_F.R.C.O.), PIANOFORTE, HARMONY, 


XUM


XUM


I, 1907. 361


HOIR EXCHANGE, 136, FIFTH AVE., NEW 


,XPERIENCED  F.R.C.O. 


WILLIS


N.W


BUILDER’S


OR SALE. 


PEDAL 


OR SALE. — RAVENSCROFT


PSAL TER


MATEUR COMPOSERS.—SONGS, PIANO- 


362


TELEPHONE—1561 GERRARD


ESTABLISHED 1794


VALUABLE VIOLINS


NEXT SPE


VIOLINS, VIOLAS, VIOLONCELLOS, BASSI


WILL TAK


TUESDAY


AND SIMPSON'S 


CIAL SALE


OF


ON


E PLACE


JUNE 18


47


LEICESTER SQUARE, LONDON, W.C


NICHOLSON AND CO. 


ORGAN BUILDERS, 


PALACE YARD, WORCESTER. 


(ESTABLISHED 1841.) 


THE OLD FIRM


P. CONACHER & CO. 


SPRINGWOOD WORKS, 


HUDDERSFIELD


PIANO PEDALS


EVERY ORGANIST


IAL


PIANO 


NORMAN & BEARD, I , 


A VALUABLE BOOK FOR TEACHERS AND STUDENTS


TECHNIQUE


AND


EXPRESSION 


PIANOFORTE PLAYING 


FRANKLIN TAYLOR


EXTRACT FROM PREFACE


WITH NUMEROUS MUSICAL EXAMPLES FROM THE


WORKS OF THE GREAT MASTERS


TO


NEW AND ENLARGED EDITION, WITH AN APPENDIX


NOVELLO’S COLLECTION


WORDS OF ANTHEMS


REFERENCE FOR THE SELECTION OF ANTHEMS SUITABLE FOR SPECIAL DAYS, SEASONS, OR 


CONTENTS


BIOGRAPHICAL NOTICES OF COMPOSERS, WITH LIST OF THEIR ANTHEMS 


ANTHEMS SUITABLE FOR CERTAIN DAYS AND SEASONS


ANTHEMS SUITABLE TO BE SUNG WITHOUT ACCOMPANIMENT. 


ANTHEMS FOR MEN'S VOICES


ANTHEMS WITH LATIN WORDS


GENERAL INDEX


ST. PAUL’S CATHEDRAL. TRURO CATHEDRAL


WESTMINSTER ABBEY. SALISBURY CATHEDRAL. 


CANTERBURY CATHEDRAL. PETERBOROUGH CATHEDRAL


YORK MINSTER. “CARLISLE CATHEDRAL. 


WINCHESTER CATHEDRAL. *DURHAM CATHEDRAL


ST. GEORGE’S CHAPEL, WINDSOR. LINCOLN CATHEDRAL


CHAPEL ROYAL, ST. JAMES’S. LLANDAFF CATHEDRAL


BANGOR CATHEDRAL. SOUTHWELL CATHEDRAL


NORWICH CATHEDRAL. RIPON CATHEDRAL


ELY CATHEDRAL. NEWCASTLE CATHEDRAL. 


ROCHESTER CATHEDRAL. NEW COLLEGE, OXFORD


ST. ASAPH CATHEDRAL. CHRIST CHURCH, OXFORD


EXETER CATHEDRAL. ST. MARY’S CATHEDRAL, EDINBURGH, 


CHICHESTER CATHEDRAL. ETON COLLEGE


GLOUCESTER CATHEDRAL. CHRIST CHURCH, LANCASTER GATE, W. 


WORCESTER CATHEDRAL, ST. PETER’S, CRANLEY GARDENS, S.W. 


364 THE


MUSICAL


I, 1907


SCHOLARSHIPS. 


HIGHER EXAMINATIONS IN MUSIC


I SHELLEY FISHER, S 


NOVELLO’S EDITION


GIBBONS 


COMMEMORATION


WEDNESDAY, JUNE 5, 1907


EVENING SERVICE. 


WESTMINSTER ABBEY


PRICK ONE SHILLING


HUMPTY-DUMPTY 


A SHORT CANTATA FOR CHILDREN 


PRELUDE, FOUR SHORT SETTINGS OF THE OLD 


NURSERY RHYMI AND PART OF THE FAMOUS SCENE 


BETWEEN ALICE AND HUMPTY-DUMPTY 


LEWIS CARROLL


BY 


H. WALFORD DAVIES. 


THE


A CONTRIBUTION TO THE


THE PIANOFORTE 7 


AND THE ABILITY TO PLAY IT


A PIANO PLAYER


INSPECT


BEFORE BUYING


ILLUSTRATED CATALOGUE ON APPLICATI 


GIBBONS 


WESTMINSTER ABBEY 


JUNE 5


THE MUSIC TO BE PERFORMED ON THIS OCCASION


EDITED BY


IS NOW PUBLISHED COMPLETE IN ONE VOLUME


COMPLETE VOLUME, CONTAINING THE WHOLE OF 


BOSWORTH & CO., 


5, PRINCES STREET, OXFORD STREET, LONDON, W


PROGRAMME MUSIC


LAST FOUR CENTURIES


HISTORY OF 


MUSICAL EXPRESSION 


BY


FREDERICK NIECKS


133 


X11. 


395


JUNE 1, 1907


NORWICH AND ITS MUSICAL 


ASSOCIATIONS


ST. ANDREW’S HALL, NORWICH. 


FIRST SCENE


SECOND SCENE


THIRD SCENE


FOURTH SCENI


FIFTH SCENE: 


PHE EARTHQUAKE


1S1¢ 


368 THE MUSICAL TI


MES.—JUNE 1, 1907Norwich can contribute to the roll of provincial 
‘organ-makers’ : Arnald Mynhamber and John 
Ashwell, both of whom took up the Freedom of 
the City in ‘the 24 of Henry VI.’ (1446): John 
Hayne (d. 1496) ; Stephen Bretton (1598); John 
September, 1669, received 
£1 os. 6d. for repairing the cathedral organ 
‘several times’ from the previous January; Mr. 
Harbart, paid #15 in 1720 for tuning and 
repairing the cathedral organ for three years ; John 
Holmes (1764); Messrs. Lambert (1771) ; in 1801 
Mr. Crotch is voted six guineas per annum to keep 
St. Peter Mancroft organ in order; while in our 
own day the firm of Messrs. Norman & Beard is 
not unknown

To the credit of Norwich must be placed the 
issue of the first important periodical publication 
devoted to music in England, Zhe Quarterly 
Musical Magazine and Review (ten vols.), which 
first appeared in January, 1818, and continued until 
1830, the last issue containing a letter from 
Vincent Novello, dated March 8, 1830, on the 
subject of Purcell’s anthems. An obituary notice 
of Beethoven, addressed to the Editor and dated 
August 24, 1827 (vol. ix., p. 161), concludes thus

Having trespassed so much on your patience, 
Mr. Editor, I shall conclude by observing, that Haydn 
appears to me like a sparkling stream, in which the 
blue sky, the light cloud, the flower, the trembling leaf, 
and many other delightful objects of nature, are reflected 
with delicious clearness. Mozart resembles a majestic 
river, swollen by tributary streams and gliding on to 
mingle its waters with those of the ocean; while 
Beethoven seems like a mountain torrent, breaking over 
rocks and down precipices, and often rising towards 
heaven in foam and smoke and mist

I remain, Sir, your most obedient and humble servant




JAME 


GEO! 


181


SAMI


WESTMINSTER ABBEY


ORLANDO GIBBONS


503. 


XUM


THE NAVE LOOKING EAST AND SHOWING THE POSITION OF THE ORGAN UNTIL 1730


THE NSTRUMENT STOOD ON THE NORTH SIDE BEYOND THE SCREEN


IN THE ILLUSTRATION, WHICH IS FROM D


AND PROJECTED BEYOND THE PILLARS, AS CAN FAINTLY


ART'S ‘WESTMONASTERIUM ' (1725


STEPH. CRESPION 


HENRY PURCEL! 


37


372


1907


SSS


I, 1907


THE POSITION OF


THE


SHOWING


NAVE


THE


OF D. N 


179 


1754 


(44,500


THE GOTHIC ORGAN CASE (¢& 1830 WITH THE


RENAISSANCE CHOIR ORGAN CASE


FACING EAST


BENJAMIN COOKE, ORGANIST 


1762 TO 1793


FROM


HORISTERS


JOHN CROSDILL


THOMAS GREATOREN


WILLIAM BEALE. 


CHORISTER UNDER ROBERT COOKE, ORGANIST FROM


CHORISTER UNDER GEORGE EBENEZER WILLIAMS AND 


THOMAS GREATOREX, BETWEEN 1814 AND 1831. 


JAMES TURLE, ORGANIST FROM 


$31 TO 1882


WALTER MACFARREN


CHORISTERS UNDER


DISTINGUISHED LAY-VICARS


LOOKING WEST


ABBEY


THE CHOIR AND ORGAN OF WESTMINSTER


MONTEM SMITH


MINOR CANONS WHO WERE PRACTICAL MUSICIANS


PRECENTORS FROM THE YEAR ISII 


MASTERS OF THE CHORISTERS


TAYLOR


JOHN


1562 


ROBERT WHITE 1574 


JOHN GIBBS 1605 


JOHN PARSONS 1613 


RICHARD PORTMAN 1633 


JAMES TRY? 1637 


WALTER PORTER 1639 


CHRISTOPHER GIBBONS 1664 


THOMAS BLAGRAVE - 1666 


EDWARD BRADDOCK 1670


BERNARD GATES 1740


CHRISTOPHER GIBBONS : : - 1664 


STEPHEN BYNG - - - - - 167 


WILLIAM TUCKER - 1678 


CHARLES TAYLOUR - ‘ - - - 1684 


HENRY PURCELI 1685 


EDWARD BRADDOCK 160 


THOMAS VANDERMAN 1763 


THOMAS BARROW 1782


MUSIC AT THE ROYAL ACADEMYserve as a foil to the more delicate types of 
his colleagues, and one can just picture his solid 
tone and steady, unimaginative performance of 
the humdrum part which was customarily given 
to viola players in the earlier chamber music. 
The appearance and dress of the musicians 
point to the first half of the nineteenth century, 
and the reality of the picture is such that one 
insensibly wonders what composition is about 
their attention. The list of possible

this particular combination of 
instruments is very limited, so far at least as the 
great composers are concerned. Neither Haydn, 
Mozart, Beethoven, Schubert or Mendelssohn

to engage




TI XUM

or performance of chamber music by musicians of a | ‘Rheingold, and in this respect Richard Strauss in his 
more recent time than in Mr. Taylor’s picture, and | ‘Salome’ has followed the example of his illustrious 
Mr. Seymour Lucas shows, in ‘The Roundelay,’| predecessor. The idea of continuous music seems to 
that his long-continued interest in picturesque | have caught on: it is said, for instance, that the young 
musicians has not left him. Mr. George Harcourt’s | ae ee ee aoe we connects 
large painting entitled ‘At the harpsichord—my ree pappedigroas: ePnes ae le tig er cccrinagh 
; ‘ot ; : ’ | beginning to understand that applause between the 
children’ is connected with music only by the| movements of a sonata is unbecoming, and, especially 
title and by the introduction of the instrument} in some of Beethoven’s sonatas, most disturbing ; but to 
named, while Mr. Ernest Sichel makes a number} pass straight on from one sonata to another, and 
of instruments (including the ‘ tinkling cymbal’) an | possibly in the case of an unsuitable juxtaposition of 
excuse for a highly-finished piece of still life. | keys—as for instance between the * Adieux’ sonata in 
Mr. Blair Leighton’s ‘ Tristram and Isolde’ has but E flat and the following one in E minor—adding some 
little of the passion of the story, and still less of the | namaste chend ot Chest, is a Setres aanggeenen 
“eee ; Ry az “| of a design which even Wagner, with all his genius, did 
ardour of Wagner’s music. Orpheus retains his| not convincingly prove to be a wise one. 
Suggestiveness to the artist, one of the principal | : —-— 
things in the sculpture being Mr. J. M. Swan’s; Mr. Haydon Hare, organist of the Parish Church, 
fine bronze group, which is based on a motive he| Great Yarmouth, has been appointed chorus-master of 
has used before, while Mr. Thornycroft has a|the Norwich Musical Festival in succession to 
bas-relief and Mr. Meteyard a painting dealing | Dr. A. H. Mann, who has resigned that office. We

hope to give a portrait of Mr. Hare in our July issue, 
rr “7 myth, and the spouse of Orpheus is the together with a view of Great Yarmouth church and 
subjec

Che London Trio are to be congratulated not only

upon the success of another series of chamber 
concerts, but that they have played all Beethoven’s 
pianoforte trios (except the clarinet trio 
logical order during the past season

performers—Madame Amina Goodwin 
Signor Simonetti (violin), and Mr. W. E. Whitehouse 
violoncello)—have given reverent attention to the 
minutest detail of ensemble and technique due to 
these beautiful compositions. ‘The programmes have




1 21. 


00-7 


THE MUSICAL TI


THE COMPANY


FACSIMILE OF THE OPENING BARS OF °*BUNNETI


DR. EDWARD BUNNETT


IN F’ IN THE AUTOGRAPH OF THE COMPOSER


82 THE MUSICAL


TIMES.—JUNE 1, 1907


DR. EDWARD BUNNETT


365,00 


1080 


ST. PETER MANCROFT CHURCH, NORWICH


ST. PETER MANCROFT CHURCH, NORWICH


DR. ARMES


385


ORGANIST AND FIRE-ENGINE MAN. 


ROYAL COLLEGE OF ORGANISTS


ORGAN RECITALS. 


ORGANIST, CHOIRMASTER, AND CHOIR APPOINTMENTS


BOOKS RECEIVED. 


387


PHILHARMONIC SOCIETYA symphonic poem entitled ‘Cleopatra’ was the first 
number in the programme of the concert at Queen’s Hall on 
May 16. It is by Mr. G. W. Chadwick, a prominent 
American composer, of whom Mr. Louis C. Elson in his 
‘History of American Music’ declares that ‘he deserves 
laudation for avoiding the demon of cacophony which has 
run rampant in some modern music.’ If the tone-poem is a 
fair specimen of his art-work, that praise is well deserved. 
The title ‘ Cleopatra’ points in the direction of programme- 
music, but the work can be heard and judged as ‘abstract’ 
music, to use a convenient if not very consistent term. 
Mr. Chadwick, as a pupil of Jadassohn and Kheinberger, 
did not neglect the study of counterpoint, and in the 
symphonic poem under notice there are many proofs of his 
scientific knowledge. The pertormance under Dr. F. H. 
Cowen was first-rate

Mischa Elman played the Beethoven Violin concerto. 
His faultless technique has almost ceased to cause astonish- 
ment, but the breadth, nobility and feeling with which he 
interprets the music of the great master still remains 
a wonder. The vocalist was M. Felix Senius, who in 
‘Un aura amorosa’ from Mozart’s ‘Cosi fan tutte’ showed 
that he possesses a voice of beautiful quality, and that he 
knows how to use it. He met with a cordial reception and 
gave an encore. Strauss’s ‘Don Juan’ was given for the 
first time at these concerts, and the programme ended with 
Elgar’s genial ‘ Cockaigne’ overture

ROYAI




ACADEMY OF MUSIC


LONDON CHORAL SOCIETY


HANDEL SOCIETY


MR. JOSEPH HOLBROOKE’S CHAMBER CONCERT


MADAME LISA LEHMANN’S ‘GOLDEN THRESHOLD


388 THE MUSICAL T


IMES.—JUNE 1, 1907


MKS. MARY LAYTON’S CHOIR. was attested by the large audience that attended her recital 
l rve and conviction of her readings, which won for her 
ich distinction in times long past, characterized her playing 
occasion, and enthusiastic applause followed the

delicacy and power of her renderings of familiar works and | 
pieces that included Beethoven’s ‘ The Departure, Absence

ind Keturn’ sonata, and Liszt’s ‘ Soirces de Vienne




ANTHEM FOR VERSE AND CHORUS


THE H. W


GRAY CO., SOLE AGENTS FOR THE U.S.A


#4 _ = | ; 


-~_ -_ | — + — — 


¢ _— _ = 7 | | ] 


O-2 — } 


~ A. A


FULL. —_ : 


- I 1 


FUL. 


FULL


XUM


6" | | | E 


Q-8 


4 J “ 


VERSE. 


VERSE


O22


P+ ? 4 - - ——_ , —— 


ALMIGHTY GUD


WHO BY THY SON JESUS CHRIST


. VERSE 7 


O2 


9¢ 1 ' 


—_ —_


IE 


VERSE 


| | ERSE. — 


VA


VA


FULL


FULL. 


FULL


FULL. 


LMIGHTY GOD, WHO BY THY SON JESUS CHRIST


_ ——_—_


_ >-; 4 


” -_ . _——, 


FE ; | 


4 4 C 


FS 


XUM


396


BIOGRAPHICAL SKETCHES 


WITH SPECIAL


FOLLOWING


OO


THE


PORTRAITS


EIGHTY-TWO MUSICIANS—PROFESSIONAL AND AMATEI


APPEARED IN


THE


HAVI


MU


SICAL


TIMES


SINCE JULY, 1897


SIR W. STERNDALE BENNETT


LONDON


LOO


1904, 


1ISQ1. 


1905


1903. 


1597. 


1598


1904. 


1902. 


1590


THE SOUTH-EAST LONDON CHORAL UNIONjuartets and one pianoforte trio also competed The 
standard of the choral performance was high. Bentham 
Female-Voice Choir (Mr. J. E. Constantine), Heysham 
Male-Voice (Mr. S. Morphet), Lancaster Wesleyan Mixed 
Voice (Mr. A. Dowthwaite) were prize-winners in their

respective classes. The performance of Beethoven’s ()uartet 
Op. 18, No. 1) by Miss Ada Sharp’s Keighley String 
(Juartet called forth high praise from Sir Edward Elgar and 
Mr. Carl Fuchs. In the afternoon a church choir festival

Lawrence's Church. Thirteen church choirs 
took part Mr. G. H. Sutcliffe conducted, Mr. John 
Coates sang, Mr. Percy W. Smale was at the organ and the 
Rev. H. Dams, precentor of Carlisle Cathedral, chanted the 
service. In the evening a miscellaneous concert was given, 
Mr. Plunket Greene sang twenty-one songs in his 
customary style and three quartet parties sang selections 
from Mr. Ernest Walker's ‘ Five Songs




399


THE


MUSICAL


1907


401MUSIC IN MANCHESTER. 
(FROM OUR OWN CORRESPONDENT

W ts concert of April 23, the most important of our 
am wchestral associations, the Beethoven Society, 
1¢ honorary conductotship of Mr. E. Gordon Cockrell, 
lis ed the duty of turning the key on the exit door 
f season of music. The programme contained 
Scl ’s Symphony No. 2, in B flat; an Andante religioso 
by \ r Scharwenka, arranged from a pianoforte sonata ; 
rchestrated Lyrische Suite (Op. 54); and the overture 
er’s ‘ Felsenmiihle.’ The band missed excellence 
onl slight want of finish. Miss Edith Craven, a

the Royal Manchester College of Music, exhibited 
rtuoso skill in Mendelssohn’s Violin concerto, and 
Tessie Kelly was the vocalist




MUSIC IN NEWCASTLE AND DISTRICT. 


(FROM OUR OWN CORRESPONDENT.) 


MUSIC IN NORWICH AND DISTRICT. 


(FROM OUR OWN CORRESPONDENT


IN NOTTINGHAM AND DISTRICT


CORRESPONDENT


MUSI


FROM OUR OWN


25


MUSIC IN READING. April 23 and 24, by the Reading Philharmonic Society

and the Reading Orpheus combined, to celebrate the 
forty-fifth year of the existence of the former Society 
ind the twenty-fifth year of the latter. On the first 
named date Sullivan’s ‘Golden legend’ was performed 
with Miss Gleeson-White, Miss Mabel Braine, Messrs. 
William Green and Charles Knowles as soloists, and 
Dr. F. J Kead as conductor. On the following day 
Parry's ‘De profundis’ and Stanford’s ‘Elegiac ode’ 
were given, each work ymducted by its composer. 
[he solo vocalists in these works were Miss Agnes 
Nicholls and Mr. Frederic Austin, both artists singing 
with mu efinement and sympathy. Bach's motet ‘ Now 
shall the grace’ and Beethoven’s —s concerto (solo

Mr. Zacharewitsch) were also included in the programme, a




MUSIC IN SHEFFIELD AND DISTRICT. 


FROM OUR OWN RRESPON DENTWinterbottom has continued his monthly symphony cor

at Stonehouse, and in the plebiscite which supplied t 
programme of the closing concert on April 23, Beethoven’s 
* Pastoral’ Symphony stood highest in. favour. Chamber 
music has fared rather badly, and only two events have 
taken place during the period which covers this surve 
January to May. The Misses Smith played for the first 
time in Plymouth Strauss’s Sonata in E flat for pianofort« 
and violin, and that by Brahms in A (Op. too), 
Madame Minadieu was heartily appreciated in songs by 
Mozart, Brahms, Franz Ries and Stanford by those who 
remember the pleasure she continually gave when residing in 
the neighbourhood. An experiment was made | yy Mr. HH. 
Moreton to popularise chamber music by introducing into 
the series of Corporation concerts on February 16 a 
performance of string quartets (Messrs. J. Pardew, .\. Serle, 
R. Ball and C. G. Pike), including a movement from 
Haydn's ‘Emperor, and a Beethoven movement ; also 
(Mr. Moreton being at the pianoforte) a first performance in 
Plymouth of Stanford’s Pianoforte quintet

The appreciation 
of the audience was sufficiently cordial to warrant a repetition 
of the attempt




OTHER DEVONSHIRE TOWNS


CR IME 5


THE MUSICAL


TIMES


JUNE I


1907


CORNISH TOWNS


THE


MUSICAL T


BRI 1A. 


MILAN 


PARIS, 


BERI


BRIEFLY SUMMARIZED


3RACKNELL (BERKS). GRAVESEND.—Stainer’s ‘ Daughter of Jairus’ was sung 
with orchestral accompaniment at a special service in the 
parish church on May 8. The choir did efficient service 
under the direction of Mr. Howard Moss, and the solo parts 
were undertaken by Miss Marguerite Herring, Mr. J. 
Everden and Mr. Alec J. Mann. A feature of the service 
was the singing of three hymns composed by the conductor, 
and accompanied by the orchestra

HANLEY.—Mr. Herbert Sherwin’s Orchestral Society, 
combined with the Hanley Philharmonic Society, gave a 
concert in the Victoria Hall on April 25, the chief feature of 
which was S. P. Waddington’s ‘John Gilpin.’ The 
programme included three movements from Beethoven’s 
first symphony, the ‘ William Tell,’ overture, the valse from 
Tchaikovsky's ballet ‘ La belle au bois dormant,’ and the 
march and chorus from ‘Tannhiuser.’ Miss Adeline 
Johnson, Mrs. H. E. Sherwin (violin) and Mr. Harold 
Khodes (pianoforte) were the soloists, and Mr. Herbert 
Sherwin conducted

HANWORTH (MIDDLESEX).—The Choral Society gave a 
concert at the Schools on April 26, when the principal 
feature was Sir J. F. Bridge’s ‘Inchcape Rock,’ in which 
the chorus .and orchestra displayed the result of careful 
training by the conductor, Mr. H. Egbert Lane. Cowen’s 
‘ Bridal Chorus’ and Faning’s ‘ Moonlight’ also were well

Lorrus-IN-CLEVELAND.—The last concert of the season 
of the Loftus and District Musical Society was given in the 
Oddfellows’ Hall on April 24. The works chosen were 
| Van Bree’s ‘St. Cecilia’s Day’ and Dr. F. H. Cowen’s 
|‘ John Gilpin,’ both of which were admirably rendered by 
|the Society. In the miscellaneous portion of the concert 
| Miss Maggie Davies, Messrs. G. Chandos-Cradock and 
| W. Peacock were the solo vocalists. Mr. James Smith, 
| of Middlesbrough, conducted

MonmoutTu.—A successful pianoforte recital was given 
}on May 2 at the Rolls Hall by Miss Enid Payne. The 
| programme included solos by Bach, Handel, Purcell, 
Beethoven, Mendelssohn, Schumann, Brahms, Chopin, 
Grieg and Paderewski, which gave evidence of the pianist’s 
versatile talent in various styles of composition

406 THE MUSICAL TIMES.—Jvune 1




1907


MDLLE. X. G. C. J.—We cannot trace a setting by Carl Busch of 
Tennyson's ‘ The Eagle

H. R.—There is no English translation of Beethoven’s

by Lennox Browne




SPECIAL NOTICE


THE MUSICAL TIMES


SCALE OF FOR ADVERTISEMENTS


TERMS


« IKOSSINI I 


THE MUSICAL


5° SUC S: A.R.C.M., 1897-1907, ONE HUNDRED AND


1S. THE SCHOOL MUSIC REVIEW FOR JUNI


CONTAINS :=— 


A TREATISE ON _ “THE SCHOOL MUSIC REVIEW. 


COUNTERPOINT IN TWO AND THREE PARTS


FOR THE PIANOFORTE


J. S. BAC H. WILLEM COENEN


HIS WORK AND INFLUENCE ON THE} PRICE THREE SHILLINGS. 


MUSIC OF GERMANY : : 


ERMAN OF SEPH VICTOR WIDMANN 


FRIEDRICH HEGAR


PIANOFORTE & MUSICAL INSTRUMENT “ FESTAL-SONG” 


TO BE LET MALE VOICES. 


ELLIOTT, SON & BOYTON


ORIGINAL COMPOSITIONS 


FOR THE ORGAN


ALFRED R. GAUL. 


CHU RCH 


BOOK


THE PARISH 


ANTHEM


THE


‘| LOVE AND YOU 


“|O DEATH! THOU


S.S.A.A.T.T. B.B


THE TEMPEST 


| THRONE


OF M 


WHY SHOULD THY


FIVE


HERO'S REST. 


THE SURRENDER OF THE SOUL


MIXED VOICES


ART THE TRANQUIL NIGHT. 


ERCY, 


PART-SONGS


I (I.T.3.3.) 


I I ( \ I 


DRAMATIC POEMS EDWARD ELGAR 


FOR RECITATION GERMAN TRANSLATION BY JULIUS BUTHS 


WITH MUSICAL ACCOMPANIMENT ; 


YLIM


WIT


GOLDEN HARVEST SONGOF THANKSGIVING


A CANTATA 


FOR HARVEST-TIDE


ENOR AND BASS SOLI AND CHORUS


WIT IYMNS TO BE SUNG BY THE CONGREGATION


AND HYMNS SELECTEI AND WRITTEN BY 


HENRY KNIGHT. 


THE MUSIC COMPOSED BY


THOMAS ADAMS


THE RAINBOW OF PEACE 


AN EASY HARVEST-TIDE CANTATA 


A HARVEST SONG 


SEED-TIME AND HARVEST 


HARVEST CANTATA 


HARVEST-TIDE 


THE JUBILEE CANTATA 


THE GLEANER’S HARVEST 


TWELVE HYMNS FOR HARVEST 


NOW AUTUMN STREWS ON EV’RY 


PLAIN 


A CANTATA 


FOR HARVEST AND GENERAL FESTIVAL USI 


SOPRANO, TENOR, AND BASS (OR CONTRALTO) SOLI 


AND CHORUS


WITH HYMNS TO BE SUNG BY THE CONGREGATION


THE WORDS WRITTEN AND ARRANGED BY 


SHAPCOTT WENSLEY 


THE MUSIC BY


J. H. MAUNDER


BRETHREN 


ONE 


SOWING AND REAPING 


ALL OUR JOIN IN


THE LAST LOAD


THE HARVEST FEAST 


THE HARVEST DANCE 


HARVEST THANKSGIVING MARCH 


THE JOY OF HARVEST 


A HARVEST HYMN OF PRAISE 


SOWER WENT FORTH SOWING 


MAKE MELODY WITHIN YOUR 


HEARTS


THE


HE THAT SOWETH 


HEAVEN, AND 


AND SEA 


O LORD OF EARTH


THE ORIANA


COLLECTION OF


BRITISH AND FOREIGN


HENCE, STARS, YOU DAZZLE BUT THE SIGHT ( 


WITH ANGEL’S FACE AND BRIGHTNESS... 


LIGHTLY SHE TRIPPED OVER THE DALES 


ALL CREATURES NOW ARE MERRY-MINDED 


THE NYMPHS ANI) SHEPHERDS DANCED 


THUS BONNY-BOOTS


SING, SHEPHERDS ALL


... GEORGE MARSON 


RICHARD CARLTON 


\RD NICOLSON


THE FAUNS AND SATYRS TRIPPING » ) «THOMAS TOMKINS 


TRUST NOT TOO MUCH, FAIR YOUTH (5 5, ) ORLANDO GIBBONS 


O FLY NOT, LOVE = (5 4 ) ... THOMAS BATESON 


QUANDO DAL TERZO CIELO (WHEN FROM THE 


EARLY MADRIGALS


WLM


THE MUSICAL


TIMES. —JUNE


I, 1907


ANTHEMS FOR TRINITYTIDE


ALPHA AND OMEGA


I AM


COMPOSED BY


LIGHT OF THE WORLD 


CUMPOSED BY


EDWARD ELG AR


FINAL


COMPLETE LIST


THE ~LUTE” SERIES


LONDON


SERVICES


A.T.T. I 


ANTHEMS. 


A.T.B.E 


NOVELLO’S 


ANTHEMS, 


SERVICES


413


FOR


MUSIC SUITABLE FOR HOSPITAL SUNDAY


JOHN STAINER


SED BY


SHALL I DO


AND CHORUS


WHAT


TENOR


MASTER


ANTHEM


SHORT FOR SOLO


CHARLES F. BOWES


THAT


SICK


MAN 


THE


BLESSED BE THE 


PROVIDETH FOR


CUTHBERT HARRIS


WITH TUNES 


FOR USE IN SCHOOLS AND FOR CHILDREN'S FESTIVALS. 


THE MUSIC COMPOSED BY 


ARTHUR SULLIVAN, JOHN AINER, GEORGE ELVEY, 


J. STAINER, AN HERS


NATIONAL HYMNS AND TUNES FOR CHILDREN’S USE. 


HE M COMPOSED BY 


CANTATA 


HARVEST 


A JOYFUL THANKSGIVING


RLANDO GIBBONS’ 


A FLOWER SERVICE 


CHILDREN


D BY


F. A. J. HERVEY, M.A


A LITANY 


SIR JOHN STAINER


ANTHEMS BY 


EDWIN MITCHELL. 


ERNEST 


JESU, THE VERY 


POPULAR CHURCH MUSIC


H. MAUNDER


SERVICES


SERVICE VG


ORGAN 


CHURCH CANTATA


XUM


COMPOSITIONS


ANTHEMS


SERVICES. 


PART-SONGS. 


CHURCH MUSIC


TO CONDUCTORS OF 


SMALL CHORAL SOCIETIES


THE 


SPANISH JEWS TALE 


HORUS AND ORCHESTRA


THOMAS ELY, 


A » 1 


THE LORD’S PRAYER. 


NEW PART-SONG


TREE! 


S.A.T.B. 


LONGFELLOW


O HEMLOCK 


PART-SONG FOR


G. LEONARD WAINWRIGHT


SONGS FROM “THE PRINCESS


TENNYSON 


FEMALE VOICES


GUSTAV VON HOLST


S ( " I ( ) 


MUSICAL PROGRESS


PUBLISHED MONTHLY


THE


A NEW MAGAZINI


N M \ ! 


THE CONCERT-GOER 


A HANDBOOK OF THE ORCHESTRA AND 


ORCHESTRAL MUSIC 


PATERSON & SONS 


TESPER HYMN.—* PEACE WHICH PASSETI 


UNDERSTANDING. 


I . MUSI Y 


REV. G. SARSON JI. VINCENT STEANE. 


I I | 


THE CELEBRATED 


NOVELLO’S


A COLLECTION


OF


NTHEM


POPULAR


FOR 


FESTIVAL AND GENERAL USE THROUGHOUT THE YEAR


BOOK


ANTHEMS


GEN? | RK 


ONE SHILLING


ADVEN 


CHRISTMAS 


LENT 


VHITSUN


FRA


1 I 


( H G 


EACH BOOK


THE MUSICAL TIMES


SCHOOL BAND MUSIC


FIRST SELECTION. 


2. THE GOLDEN VANITY ENGLISH AIR. 


»” | MARCH OF THE MEN OF HARLECH WELSH AIR. 


{THE ARETHUSA _.... SHIELD. 


= ® |THE MINSTREL BOY IRISH AIR. 


|THE HARP THAT ONCE IN TARA’S HALLS IRISH AIR. 


LONDON : NOVELLO AND COMPANY, LIMITED. 


MUSIC FOR “MM ILITARY BAND 


P ARRANGED BY 


G ) 3 . 15 


10 6 ) 9 6 


LONDON: NOVELLO


AND COMPANY


LIMITED


INTRODUCTION


AND 


ALLEGRO 


FOR STRINGS


QUARTET AND ORCHESTRA) 


EDWARD ELGAR


DAILY TELEGRAPH 


SEVEN PIECES 


FOR THE ORGAN 


THEODORE DUBOIS


CANTILENE RELIGIEUSE | PRIERE


MARCIETTA | 6 POSTLUDE CANTIQUE 


7. MARCHE-SORTIE


SEVEN PIECES 


ORGAN


ALEXANDRE


F BY


GUILMANT


CONTEN 


. OFFERTOIRE


WEDDING MARCH 


. BERCEUSE


4. MINUETTO 


5. MARCHE TRIOMPHAL|! 


| 6. POSTLUDE


DAILY NEWS 


DAILY CHRONICLI


YORKSHIRE POS


GLOBI


ST. JAMI S GAZETTI


7. FANTAISIE SUR DEUX MELODIES ANGLAISES


ORIGINAL COMPOSITIONS 


FOR THE ORGAN


HENRY SMART


4 THREE-PART STUDY


OLD ENGLISH 


VIOLIN MUSIC 


ALFRED MOFFAT


PREFATORY NOTE


THE MUSICAL


I, 1907. 419


THE ART OF 


COUNTERPOINT 


ITS APPLICATION AS 


PRINCIPLE


AND 


A DECORATIVE


C. H. KITSON


A HANDBOOK 


EXAMINATIONS IN MUSIC 


ERNEST A. DICKS


28


P REFACE ) THE SEVENTH EDITION. 


VOICE


HE TENOR AND ITS TRAINING


STAFF


NOVELLO’S ELEMENTARY MUSIC MANUALS 


AN ELEMENTARY 


SIGHT-SINGING COURSE 


NOTATION THROUGH TONIC SOL-FA 


FOR CLASS USE 


BY 


GEORGE LANE. 


PART I 


RACT FRON 


AND COMPANY, 


VOICE CULTURE 


FOR CHILDREN 


A PRACTICAL PRIMER 


CULTIVATION AND PRESERVATION OF 


YOUNG VOICES 


EXERCISES FOR THE USE OF SCHOOLS, 


JAMES BATES


EXTRACT FROM T OM THE PREFACE


WITH CHOIRS


BY


>XERCISES FOR THE 


I SIGH FOR


TOO


MATTHEW


IVO 


LATE LOVE DIVINE, ALL LOVE


| EXCELLING | 


ARNOLD DUET + 


JOHN STAINER. 


, FOI 


JOHN


NIGHT'S SWEET COMING 


(IN TWO KEYS) 


ROSAMONI


BY


INCLUDING


YIN I IDR. INTROIT, “*] HEARD A VOICE FROM HEAVEN 


AND


ALFRED R. GAUL


J > | SONG 


THERE SITS A BIRD ON | LADY NAIRKN . 


_ THE MUSIC BY 


YONDER TREE | C. H. H. PARRY. | 


SONG 


‘ SONG 


THE MUSIC BY


ENGL ISH L Y¥ RICS | WILLEM COENEN


HUL E RT


ENTH SET 


CANTATA FOR CHORUS, SOLI, AND ORCHESTRA 


THE


MUSICAL


1907


VOCAL EXERCISES 


WIT!| MARKS OF EXPRESSION AND PHRASING BY 


ALBERTO RANDEGGER


CONCONE


AUGUSTE - PANS SERON. 


FORTY MELODIC AND PROGRESSIVE VOCAL EXERCISES 


FORTY-TWO MELODI AND PROGRESSIVI VOCAI 


FIFTY VOCALISES FOR TWO VOICES (SOPRANO ANI 


MARCO BORDOGNI


EXPRESSION AND PHRASING BY 


RANDEGGER


WITH MARKS OF 


ALBERTO


DON GIOVANNI


LE NOZZE DI FIGARO


MENDELSSOHN’S “ELIJAH.” 


SOPRANO


CONTRALTO, TENOR, BARITONE, 


SCHU THE


BRAHMS. 


MEZZO-SOPRANO


SONG DANCES


VOCAL SUITE


FOR FEMALE VOICES 


(S.S.C.) 


WITH ACCOMPANIMENT FOR THE PIANOFORTI


THE WORI WRITTEN BY 


JAMES HOGG, BARRY CORNWALL, 


AND OTHERS


WILFRED BENDALL


SCORE-READING EXERCISES 


BY


EMILY R. DAYMOND


EXAMINATION QUE STIONS


HOW TO WORK THEM 


CUTHBERT HARRIS


PREFACE


AND MANTLE SLEEVE 


CONVENIENT AND INVISIBLI 


MPANY


COAT MUSIC CASE


BY


SECTION A.—TECHNICAL PRACTICE. IN SIX BOOKS


SECTION B.—STUDIES. IN SIX BOOKS. 


PIECES


NOVELLO'S 


ALBUMS FOR PIANOFORTE AND


STRINGED INSTRUMENTS


AND VIOLONCELLO


TWO VIOLINS, VIOLA


15


19


FREDERIC H. 


MACFARREN, W


COWEN, FREDERIC H. 


LYRIC PIECES FOR 


, VIOLIN AND PIANOFORTE 


VIOLIN AND PIANOFORTE COMPOSED BY


JOSEPH HOLBROOKE. 1 P 


BUNTE BLATTER COLLECTED FROM TRADITIONAL _ 


COMPOSED BY WITH PIANOFORTE ACCOMPANIMENT §& ; 


HANS SITT. BY ‘ \ 


SIX


MORCEAUX DE SALON PRICE TWO SHILLINGS EACH NET


SET I


EDWARD GERMAN, | A IIISTORY OF MORRIS DANCING — 


. TH A DESCRIPTION OF 


=<LEVEN DANCES II. 


CECIL J. SHARP 15. 


NNN RD DD


THE MUSICAL


ORIGINAL 


ORGAN 


EDWIN H. LEMARE


CONTEMPLATION 


CHANSON D'ETE 


CAPRICE ORIENTALI


CANTIQUE D'AMOUR 


FANTAISIE FUGUI 


MADRIGAI 


RECITAL SERIES 


EDWIN H. LEMARE


COMPOSITIONS


NNWNNNNDN Y


ND


NNNWN 


14


6} 17


ORGAN | 


TRANSCRIPTIONS


TSCHAIKOWSKY 


> CORONATION MARC H TSCHAIKOWSKY 


_BE ETHOVEN


MENDELSSOHN 


ORGAN 


SCHUMANN


JOHN E


A. C. MACKENZIE 


ME NDELSSOHN


UNFINISHED


SCHUBERI


AR CUISCHUBERT

Arranged by Joun E. West 
ro. PRELUDE ro PART II, (“Tue 
‘EDW ARD ELGAR 
Arranged by G. R. SINCLAIR 
1. FINALE ¢ 1SYMPHONY N BEETHOVEN 
Arranged by A. B. PLAN1 a 
ADORAMUS TI , HUGH BI AIk 
Arranged by Hucu Bair ~ 
. INTERMEZZO (“Tue B ; rA rOPHANES) 
C. H. H. PARRY 
Arranged by W. G. At« 
—— MARCH AND FINALE (“Tue B s" OF 
STOVHANES) C. H. H. PAKRY 
iby W. ¢ \ K w 
\NDANTE (P NOI ES ee @& ) 
Arranged by J YE. Wes J. BRAHMS 
pay roenedat (PiANor E SONATA IN I (O 
Arranged by Joun E. Wi * BRAHMS 
MODERATO anv CANZONA (Twelve Sonatas of Three 
Parts, No. V1.). Arranged by Joun PULLein

H. PURCELI




426


FOR


IN GROUPS


THE


PIANOFORTE


PROGRESSIVE STUDIES


AND THE


FINGERING


REVISED


AND SUPPLEMENTED


EDITED


ARRANGED


FRANKLIN TAYLOR


| 33- ” ” ” 3 


7; 9° ” ” 28 


12. ” ’ » 3) 42. ” ” » 3 


FIFTY-SIX BOOKS, PRICE ONE SHILLING EACH. 


FROM THE ABOVE: 


SELECTED PIANOFORTE STUDIES 


PROGRESSIVELY ARRANGED BY 


FRANKLIN TAYLOR. 


BOOKS


PRICE ONE


SHILLING


AND SIXPENCE EACH BOOK


IN TWO SETS (EIGHT


LONDON


1 M 


2. P


' THE TIMES. | WIDOR.—“* M 


DAILY TELEGRAPH. FULL SCORES 


2 DAILY NEWS


GLASGOW NEWS. VOCAL. 


XUM


UST PUBLISHED


NEW WORK FOR CHORAL SOCIETIES, ETC


THE LEAP OF KURROGLOU


BALLAD 


FOR BARITONE SOLO, CHORUS AND ORCHESTRA


JAMES R. DEAR


GUY D’HARDELOT. 


FOR YOI 


I THINK


MAUDE VALERIE WHITE


IED RO


L\LON}I


IN GOLDEN N


TERESA DEL RIEGO 


» LOVING FATHEI 


HY! DA 


VN EVES 


FLORENCE AYLWARD 


+ | ’ VINTER 


H. WALFORD DAVIES 


MI 


AMY WOODFORDE-FINDEN. 


FRANK E. TOURS. 


\ MEETING 


THER O [INI


BERNARD ROL


SILHOUETTES OF LONDON


E. J. MARGETSON


SONG OF ANDAI SIA 


DOCTOI


ALMA GOETZ 


OH BIRD O \ I 


WADDINGTON COOKE 


DREAM SHIPS 


“VISITORS 


PAUL A. RUBENS 


“THE SUMMEI! 


ERNEST NEWTON. 


PHROUGH THE FOREST 


THE MAGIC MONTH OF MAY. 


LOVE'S ECHO


AND MAY BE HAD OF ALL MUSIC SELLERS


EDWARD GERMAN. 


“THIS ENGLAND OF OURS. 


“WHEN MAIDENS GO A-MAYING.” 


“LOVE IS MEANT TO MAKE US GLAD


W. H. SQUIRE. 


LIGHTERMAN TOM. 


“THREE FOR JACK. 


“THE JOLLY SAILOR. 


“THE OLD BLACK MARE


L. DENZA. 


“LOVE IN THE VALLEY.” 


“THE ROSE ENCHANTED


HERMANN LOHR. 


MESSMATES 


*A CHAIN OF ROSES. 


LANAGAN’S LOG 


SONGS OF THE NORSELAND


FRANCO LEONI. 


LITTLE BAREFOOT. 


AUTUMN LOVE. 


*“COOLAN DHU. 


GEORGE H. CLUTSAM 


“SWEET, BE NOT PROUD.” 


VERE SMITH. 


* FAIRVLAND 


HERBERT BUNNING. 


‘REVELATION 


MY SWEETHEART. 


ROBERT CONINGSBY CLARKE. 


A BIRTHDAY SONG. 


A DEDICATION, 


“THE LINNET


LAND


FRANK LAMBERT. 


OF ALL SEPTEMBERS. 


“IN THAT HOUR. 


“YESTERDAYS | 


“WHEN LOVE BENDS LOW. 


“IN JUNI


MELBOURNE


SIR FREDERICK BRIDGE


SEATED AT THE ORGAN OF WESTMINSTER ABBEY


WV


XUM


YWLIM


NOVELLO’S PART-SONG BOOK


A COLLECTION OF 


PART-SONGS, GLEES, AND MADRIGALS


IN VOLUMES, CLOTH, GILT, 58. EACH ; OR IN SEPARATE NUMBERS


FIRST SERIES


SECOND SERIES. 


G. A. MACF ARREN. W. MACFARREN. 


O PEACEFUL NIGHT


AL


O PEACEFUL


THE WORDS WRITTEN BY W


NOVELLO’S PART-SONG BOOK


FOUR-PART SONG


THE MUSIC COMPOSED BY


EDWARD GERMAN


2S = 2 SS SS 


NIGHT


HERBERT SCOTT


SJ < <6 2


OO


O PEACEFUL NIGHT


- >» | —_|_ 


= SSS ” 


SSS ===>) = 


: NS 


9 PP —— 


) — ———$—— ——_—$— _— = 


O PEACEFUL NIGHT


62 SS = ES SS ESS 


+ - — ——_ ————, — _ - 


ZN


O PEACEFUL NIGHT


PPPP- 


PP - 


NOVELLO’S


HENRY SMART


CIRO PINSUTI


MALE VOICES. 


218


HENRY SMART


324 


325 


320


327 


328 


329 


330 


331


332 


333


FRANZ ABT


A. C. MACKENZIE. 


E. PROUT. 


J. L. HATTON


352 


353 


354


SHI