


 E MUSICAL time 


 FEBRUARY 1 , 1869 


 music number . 


 bless MERCIFUL 


 S. JAMES HALL 


 


 subscription concert 


 


 FRIDAY , FEBRUARY 5 


 HANDEL " JEPHTHA 


 MENDELSSOHN 


 LIEDER OHNE WORTE , . 


 book 1 8 


 ALBAN , HOLBORN . — boy 


 - class vocalist ONLY.—A 


 new song JOSEPH BARNBY 


 SONGS duet 


 ANTON RUBINSTEIN 


 . morning song .... 


 . evening SONG 


 ’ song EGMONT - . 


 professional notice . 


 MISS BLANCHE REEVES 


 MR . WILBYE COOPER 


 " lishe good style , moderate 


 RASS , REED , STRING , DRUM 


 EYOND ALLCOMPETITION.—T. R. WLLLIS ; 


 instrument 


 UTLER MUSICAL 


 CORNETS , SAXHORNS , DRUMS , FLUTES , CLARIO- 


 WF USICAL instrument VOLUN 


 MR . W. HUTCHINS CALLCOTT 


 MR . W. SUTCH 


 MR . J. C. BEUTHIN , 


 W. SNELL improve HARMO 


 ROGER CELEBRATED GUINEA COM 


 659 


 reakfast — EPPS COCOA . 


 660 


 NEW anthem LENT . 


 press . 


 96th psalm ) . 


 ACRED song LENT 


 SIXPENNY MUSICAL manual . 


 AUNDERS practical instruction 


 HAMILTON PATENT UNION " MUSICAL notation . 


 57 


 661 


 publish 


 C JEFFERYS , 


 57 , BERNERS STREET , LONDON , W 


 FELIX GANTIER 


 ORIGINAL 


 OPERATIC gem 


 AARPAARARMARARARARARAMAMAAD 


 picture PALESTINE . 


 3 0 


 METZLER CO . 


 MUSICAL BIJOU 


 SACRED VOCAL DU 


 9.—THIRTEEN CHRISTY minstrel ’ comic song 


 11 — popular hymn 


 oppler 


 EXETER HALL 


 MAGAZINE SACRED MUSIC . 


 . xiii , VOL . 4 , FEBRUARY , 1869 


 LONDON 


 C. JEFFERYS , 57 , BERNERS STREET , LONDONTestimonials eminent musician clergyman , capa 

 bilitie organist , choir - trainer , pianist , vocalist , composer 
 teacher . Beethoven , care Novello , Ewer Co 

 RGANIST.—A gentleman willing 




 hymn ancient modern 


 use service church 


 0 4 


 hymn ACCOMPANYING TUNES 


 ch 


 663 


 MUSICAL TIMES , 


 FEBRUARY 1 , 1869 


 musical pitch . 


 664ductor hesitate perform monumental choral | @ genius brilliant , nature pure 
 work great master | gentle , write Mendelssohn enthusiast ; 

 eighteenth , great - half reminiscence include incident 
 present century . " obvious , , | bear conspicuous , indis- 
 letter Mr. Reeves thing want | puutable proof esteem hold 
 fointroduce reform , desirability # ! ! intimately acquaint . _ , 
 silently admit year ; know | , interesting read , boy , 
 work , Mozart and| Moscheles ( scarcely thirty year age 
 Beethoven , England , approach terror | 8 ° v ° concert Berlin , listen wi 
 byall engage interpretation , breathless excitement comparatively mature 
 little doubt soon change the| c ° mp0s¢r8 e fiat major concerto . , supper 
 fetter , art artist , firm establish-|p@*ty father house concert ( , 
 ment french pitch grand point Moscheles , Hummel , Berger , Zelter , 
 gain ; compromise ; celebrity present ) " handsome boy 
 variation diapason normal again| ® jacket " stand motionless piano , whilst 
 ignore question absolute necessity Hummel Moscheles perform ; , 
 miformity ; , fix pitch , | press play , burst 
 virtually challenge decision ar- tear rush theroom . sensitive nature 
 tive eminent man French youthful pupil scarcely suffi- 
 conference , compelling continental artist ciently evident somewhat stern Zelter , 
 visit submit mere national caprice . hardly meet boy implore look 
 wge adoption diapason , , let|*8 * request play ; 
 ws hope future conductor all- wound feeling exclaim , — ' " 
 erful . custom for|¢@"th matter , boy ? 
 mstrumentalist tune , white feather , play fearlessly 
 fix pitch conductor ; | s84 concert , Gothe , Weimar ? 
 happen , end write you?—that 
 concert , wind instrument ( sharpen poltroon ? " ' tear , , 
 breath ) bring string standard ; reply uncouth rebuff . Moscheles 
 pitch orchestra elevated . somewhat surprised , 
 conductor declare normal diapason|pecoming instructor young Felix , 
 starting ; opportunity occur , earnest request mother , hear , day 
 player agree , instead memorable supper - party mention , 
 leave settle matter them- play E flat concerto delight 
 self , establish law , instrumental-| ® * concert , fire , impetuosity , 
 ist kind friend , instead bitter| ™ usical feeling finished artist . ' intellectual 
 enemy , vocalist ; conductor pitch| ining Mendelssohn materially 
 reign despotic sway , tuning , aid constant intercourse man 
 time , mean flatten , sharpening . high mark science , art , literature . 
 _ inconelusion , sincerely hope persons|w home find life love 
 interested question small points|pe#ce spring spontaneously mutual 
 difference sake unanimity . french |{# ™ ily affection , surround constant 
 iapason fix mature delibera- | ® t ™ mosphere intellect ; music , course , form 
 tion ; prove essentially prac- principal delightful meeting continually 
 tical . remember , , adopt | ' king place ; conversation , lift far 
 itwe excellent work standard dead level ordinary " ' society , " invariably reign 
 pitch , set worthy example nation | SUPTeme . Mendelssohn leave happy home , 
 remain state uncertainty father , seek valuable counsel 
 pon subject . bond brotherhood is| Cherubini , Paris , little doubt 
 strive . opening | " ie mind cultivated appre- 
 temark , sign art express are| " tion high noble , 
 intelligible alike world : let prac- debase influence world little 
 exposition sign equally agreed|2 ° effect character . journey 
 ; , confusion tongue |you28 composer Scotland , dear friend 
 place barrier man man , music lingeman , event look 
 semich bes . , gentle utmost pleasure ; 
 medium man Creator — at| consider violation sacred 
 universal language . trust , glad letter , 
 youthful enthusiasm , write Moscheles 

 ee 




 4EPEBGEEN BES 


 667ha long deservedly celebrate . 
 concert , 4th inst . , include Mendelssohn 
 music 4 Midsummer Night Dream , com- 
 poser pianoforte concerto , D minor , play 
 Madame Schumann ; Samuel Wesley fine motett , " 
 exitu Israel , " Schubert " ' Song Miriam . " 
 second concert shoit work , Mr. G. A. Macfarren , 
 " Songs Cornfield , " perform 
 time 

 Tue Annual Report Birmingham 
 Amateur Harmonic Association Society 
 highly prosperous condition ; 
 present year immediately follow Bir- 
 mingham Triennial Festival , diminu- 
 tion number member , case 
 occasion . work rehearse , 
 find Beethoven Mass D , Mendelssohn Antigone 
 Loreley , Schubert Grand Mass e flat , 
 important woik . ' financial position Associa~ 
 tion thoroughly satisfactory ; member 
 committee reason congratulate them- 
 self result indefatigable endeavour 
 promote interest Society , advance- 
 ment good rausic Birmingham 

 ay interesting lecture , 13th 
 ult . , Literary Scientific Institution , Hackney , 
 Rev. John Curwen , ' * view ot music 
 art science , suggest Tonic Sol - fa 
 method teach . " Charles Reed , Esq . , M.P. , oceu- 
 pie chair . evening satisfactory 
 exhibition readiness compose singe 
 sight disciple Sol - fa system 




 668Minuet Trio , Pianoforte . compose T. 
 Ridley Prentice 

 glad young composer work 
 upwards ; disposed view Mr. Prentice 
 modest Minuet Trio favourable light 
 form portion 
 * ' Grand Sonata " inexperienced 
 writer commence career . large work 
 kind good idea effectually bury 
 composer receive 
 meed praise ; thusa meritorious aspirant 
 fame encounter check time 
 stand great need sympathy encourage- 
 ment . Mr. Prentice hashad Beethoven mind 
 compose Minuet Trio 
 degree detract merit music . 
 movement write . ' Minuet , B flat 
 major , melodious ; passage imitation 
 composer study ina good school . 
 Trio , tonic minor , contrast 
 Minuet ; accompaniment , triplet , 
 division subject treble bass 
 instrument , excellent effect . hope 

 meet Mr. Prentice composition 
 somewhat great pretension 




 669 


 CHARLES JEFFERYSTue piece animated Polonaise , 
 d flat major , theme treat artis- 
 tically accustom majority 
 modern pianoforte composition . frequent 
 change key ; alteration harmony 
 recurrence subject excellent effect . 
 second theme , flat major , intro- 
 duce , unexpectedly , natural major , lead 
 subject tonic minor ( write C sharp 
 minor ) , composition conelude brilliant 
 passage ' original key . find good 
 piece , fur practice performance ; ama- 
 teur glad learn leading fingering 
 mark eomposer , difficulty 
 likely find . second piece , " Flower de Luce , " 
 notice review Hanover Square 
 September . elegant placid ' song with- 
 word ; " thoroughly reach player 
 train use finger expression , 
 execution . fact reprint 
 serial originally appear suffi- 
 cient proof favour receive 

 Tema con var aziont , Beethoven Septett . arrange 
 Pianoforte Frederic N. Lohr 

 tr pleasure excellent music 
 adapt household instrument ; , al- 
 faint reflection original , inculcate 
 taste good ; increase grati- 
 fication listen present 
 composer intend , arrangement 
 skilfully write ; effect instrument 
 reproduce present great difficulty 
 executant . particularly admire 4th varia- 
 tion ( b flat minor ) touch 
 clearly indicate , contrast 
 , - train player , effectively bring 
 . glad find piece form . ] 
 series similar arrangement classical work 




 EDITOR MUSICAL TIMES 


 editor MUSICAL TIMES 


 MUSICAL time 


 compose 


 bless MERCIFUL 


 anthem voice 


 SACRED MUSIC ( price - halfpence ) . 


 177 


 297 


 186 


 213 


 44 


 334 


 182 


 54 


 345 


 70 


 127 


 237 


 308 


 wa 


 373 


 379 


 384 


 23 


 249 


 | 131 


 123 


 155 


 100 


 363Barnby 

 whisp ’ agi ( Christmas 
 Carol ) . T. Cooper 
 thou art grave Beethoven 
 teach , o Lord ... Dr. Rogers 
 teach , o Lord . T. Attwood 
 teach thy way ... « .. Croce 
 te deum laudamus . . T. Cooper 

 te deum laudamus ... Dr. § . S. W 




 340 


 anthem voice 


 TENOR : 


 @ = 60 


 # sae 


 N | 4 


 pp 


 — -§_@——__6——_2—_,__02- — — _ — _ _ _ 


 bless MERCIFUL 


 bless MERCIFUL 


 2a 


 7 § -ps———-b > 


 SS 


 vr 


 qo 


 > SS 


 111 


 editor MUSICAL TIMES 


 CORRESPONDENTSPage index Vol , VILL , appear 

 continuation " Incidents Life Beethoven , ' 
 matter |x rae receive , somewhat invidious select 
 |any special commendation 

 success 




 676 


 678both oceasion . Concerts great success , 
 Lypnry.—Mr . J. A. Matthews Annual Con- 
 cert Assembly Room , Monday , 
 18th ult . vocalist Miss Clarke , Miss ' l'aylor , 
 Messrs. R A. Matthews , A. Thomas , J. A. Matthews . 
 instrumental solo , & e. , play Mr. Poole ( flute ) ; 
 Mr. Hooper ( harmonium ) , Mr. Matthews ( pianoforte ) . 
 Concert highly successful . 
 Mancurster.—Mr . Yarwood Concert , 
 Free Trade Hall , 9th ult . fully 
 fashionably attend , patron present . 
 Miss Chadwick receive vocal solo 
 ( twice encore ) , Mr. Carlos Lovatt Miss ! 
 Henderson highly successful , feature ! 
 programme - song , " gently sigh " ( word 
 music concert - giver ) , sing 
 glee party combine , utmost effect , 
 enthusiastically receive . solo 
 Herr Otto Bernhardt ( violin ) , Mr. Avison ( violon- 
 cello ; Lancashire Bell - ringer perform 
 selection , warmly applauded.——Tue an- 
 nual performance Messiah , 24th December , 
 unquestionably fine Manchester 
 year . ' principal vocalist Madame Lem- 
 mens - Sherrington , Madame Patey - Whytock , Mr. Sims| 
 Reeves , Mr. Santley . Mr. Sims Reeves sing through- | 
 Oratorio sing ; intensity } 
 patho throw recitative air de- 
 scriptive sorrow Redeemer , power | 
 energetic song , " thou shalt dash| 
 , " form lesson legitimate vocalization , | 
 heart young singer . the| 
 principal vocalist excellent voice , sing ! 
 good occasion , fourth series | 
 Concerts Manchester Vocal Society | 
 19th ult . , success . - song | 
 good effect ; Mr. W. J. Young , 
 " come let merry , " especially receive . 
 principal solo vocalist Miss Fanny Henderson , 
 Miss Tomlinson , Mr. W. Dumville , & c. , 
 utmost satisfaction music allot 

 Marztzorovau.—The concert form 
 conclude scene " Christmas half , " Marl- 
 borough College , year unusually successful . 
 programme select ; contain ) 
 glee - song , excellently render . 
 " welcome , " ( word Master College , 
 music Mr. Bambridge ) , effectively ; 
 successful teaching Mr. Bambridge amply 
 prove artistic pianoforte performance 
 pupil . great feature Concert 
 selection Beethoven Sonata Op . 2 , . 1 . 
 play true musical feeling Mr. Bam- 
 bridge . performance conclude , usual , 
 holiday song , ' Carmen Marlburiense , " sing 
 school.—a selection Christmas Carols 
 form pleasing portion entertainment 
 town , evening College Concert . 
 training choir , Mr. Bambridge ( 
 compose carol sing ) , 
 theme general admiration 

 Maryrort.—The Annual Charity Concert 
 Ball place Music Hall , Tuesday evening , 
 sth ult . , respect great success . 
 music , usual , direction Messrs. 
 Graham , assist Messrs. Scott , Buttifant , Barber 
 { Cathedral Choir ) , Mr. Casson ( Wigton ) , Mr. 
 H. Thompson ( Harrington ) . programme contain 
 song , glee , instrumental piece , 

 Roystoy.—The Amateur Musical Society hold 
 26th open meeting , 26th Dec. , Hall 
 Institute . instrumental composition 
 perform Haydn " Kinder Symphonie , " organ 
 solo conductor , Dr. Garrett , Mozart Sonata 
 E flat , pianoforte violin . vocal music 
 comprise Spohr 84th Psalm , selection Haydn 
 Mass , . 1 , - song . piece 
 render ; prove Society 
 steady progress 

 Sartspury.—Two concert lately 
 Amateur Orchestral Union , Assembly 
 Rooms , fully fashionably attend . 
 programme contain excellent instrumental work , 
 overture Lgmont , Melusine , 
 Hebrides , & e. , Beethoven symphony , ( . 8) , 
 Mendelssohn violin concerto . principal 
 piece vocal selection Weber ' ' Ocean , thou 
 mighty monster , " excellently render . solo 
 pianoforte , Mr. De Windt , harmo- 
 nium , Mrs. De Windt , admire . 
 band Union contain - know ama- 
 teur , member Wandering 
 Minstrels , Societies . Concert 
 aid fund Diocesan Choral Association 

 SHEFFIELD.—The Sheffield Choral Union 
 series performance season 
 Saturday , 26th Dec. , Music Hall . 
 Oratorio occasion Messiah . principal 
 vocalist Miss Helena Walker , Miss Phillips , Mr. 
 Robson , Mr. Briggs . solo sing ; 
 chorus precision energy . Mr. 
 W. Stubbs ably conduct , Mr. Phillips pre- 
 organ . concert great success . 
 — Tue St. Stephen annual gathering hold 
 Temperance Hall , 29th December . choir 
 assist Miss Harrison , Mrs. House , Messrs. Kay , 
 Hattersley , Styring . selection song glee 
 effectively ; lecture , Rev. J. 
 Burbidge , exceedingly receive , 18th 
 ult . , Sheffield Amateur Harmonic Society 
 selecti- > work Dr. Sterndale Bennett , 
 complimentary concert , presence , 
 eminent composer . Woman Samaria 
 chief item programme 




 dure month , 


 KORO 


 COC AWS 


 XUM 


 681 


 OULE collection 527 chant , 57 


 T= PSALTER , PROPER psalm , hymn 


 01 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 ORDER HOLY COMMUNION , 


 ANGLICAN CHORAL SERVICE BOOK , 


 ule 


 J BAPTISTE CALKIN EASY MORNING 


 USELEY 


 MONK psalter 


 ready 


 edit 


 


 DWARD HERBERT chant te DEUM , 


 MUSIC MUSICAL INSTRUMENTS.—FEBRUARY sale . 


 important notice musical professor 


 GFF ASHWORTH new patent loop 


 authorise edition RUGBY school . 


 element MUSIC systemati 


 ive JOVIAL winter time . 


 ) hn- 


 , ELISE 


 683 


 upplemental HYMN TUNE book , 


 tune chorale 


 


 TONIC SOL - FA edition 


 new work ORGAN 


 READY , ' 


 edit WM . SPARK , MUS . DOC 


 ORGANIST TOWN HALL , & . , LEEDS 


 


 content 


 rudimentary manual exercise 


 near close . 


 TOUCH , 4 mode finger requisite 


 certain passage , order sustain sound duly 


 ending , 


 1615 . ) 65 . ALLEGRETTO PASTOR- 


 end 


 XUM 


 CED 


 L615 


 RGAN 


 [ te 


 DULY 


 tave . 


 1008 


 30 


 DASTOR- 


 XUM 


 685 


 NEW WORK ORGAN 


 W. T. BEST 


 ascend descend degree 


 ascending descend scale PAS- 


 sage GIVEN sound , order 


 acquire certainty play 


 interval 


 entire range pedal - board 


 exercise greater difficulty , 


 ascending descending de- 


 gree , scale 


 , include example 


 key 


 ree exercise use 


 arrange organ . 


 content . 


 shewing passage special 


 difficulty play 


 note 


 LONDON : NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 35 , poultry ( £ .c 


 music coming season 


 TTWOOD , T.—ENTER judgment . 


 YATTISHILL , J.—CALL remembrance . 


 EASED- 


 OLCK , OSCAR.—O LORD , long wilt THOU for- 


 ALKIN , J. BAPTISTE.—OUT deep 


 — sacrifice GOD broken spirit , 


 IEMER , P. H.—HE despise reject . 


 ARRANT , R.—CALUL REY 


 G OSS , JOHN.—COME let return unto 


 OUNOD , CHARLES.—THE seven word 


 OPKINS , E. J.—IN distress cry unto 


 RONS , HERBERT S.—SHEW thy servant light 


 IFIOURS , B.—IN THEE , o LORD , trust . 


 — T. — ponder word , o LORD . 


 \ INTER.—HEAR prayer , o LORD . 


 | ENT.—WHO COMETH EDOM ? 


 \ FACFARREN , G. AA — HOSANNA son DAVID . 


 ONK , W. H.—THE improperia , reproach . 


 RUST 


 LORD . 


 ment . 


 


 EDOM 


 DAVID 


 SALVA- 


 28 , 18 . 


 ) ACHES 


 XUM 


 687 


 LLEN , G. B.—NOW CHRIST RISEN dead . 


 ARNBY , J.—AS bear image 


 EST , W. T.—J&SUS CHRIST risen - day , ALLE- 


 LARKE , J. HAMILTON.—CHRIST risen 


 OUNOD , CHARLES.—BLESSED COMETH 


 OPKINS , J. L.—LIFT head . 


 OPKINS , E. J — seek ye living 


 LORD strength . 


 ITTMAN — LORD king . 


 EBBE.—CHRIST raise dead . 


 BREITKOPF HARTEL edition . 


 MOROAOCSCAACOHR 


 BREITKOPF HARTEL edition . 


 [ { RANZ SCHUBERT PIANOFORTE COM- 


 ST . JAMES HALL 


 ORATORIO concert 


 conductor , MR . JOSEPH BARNBY 


 FRIDAY , FEBRUARY 5 


 HANDEL JEPHTHA 


 MISS BANKS . MDLLE . DRASDIL . 


 THURSDAY , FEBRUARY 25 , WEDNESDAY , APRIL 21 


 MENDELSSOHN ELIJAH . HAYDN SREATION . 


 MDME . RUDERSDOREFF . MDLLE , DRASDIL . MADAME LEMMENS - SHERRINGTON . 


 MR . SIMS REEVES . MR . SIMS REEVES , 


 HOLY WEEK performance MENDELSSOHN LOBGESANG 


 HANDEL MESSIAH . : 


 MISS JULIA ELTON . MADAME RUDERSDORFF , MDLLE . DRASDIL 


 WEDNESDAY , JUNE 9 , 


 MENDELSSOHN ST . PAUL . 


 MADAME LEMMENS - SHERRINGTON , MDLLE . DRASDIL .