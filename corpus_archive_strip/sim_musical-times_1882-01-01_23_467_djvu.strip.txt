


1, 623 


03, O84 


103


131 346


2, 299


IQA / 


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


PUBLISHED ON THE FIRST OF EVERY MONTH


BA 


T. E. GATE HOT SE


PROFESSIONAL NOTICES. 


MISS CARINA CLELLAND. 


MISS


MISS LOUISA BOWMONT 


MR. T


MR. J. BINGLEY SHAW 


M R. 


FRANCOIS E MIL E CHOVEAUX 


DEGRI 


UI


ME


HE 


M R. 


IA] 


RA! 


. AI 


HIGH


MUSIC 


SHIPS


SCHOOL.—CHURCH OF ENGLAND 


R. AL 


4 R.A.M 


M®, CH 


RGAN 


XUM


MPANY


TRINITY COLLEGE, LONDON, | THE MUSICAL TIMES


PRACTICE OF CONDUCTING, 


EDITED BY


INCLUDING HIS CORRESPONDENCE NOVELLO, EWER AND CO.’S


BY 


EDWARD HOLMES. PIANOFORTE ALBUMS chen von Tharau

y amar 4. Marche Solennelle ... ... Ch. Gounod. 
VoLUME I. 5. Bridal March, from “ L ohengrin” .. R. Wagner. 
6. Gipsy March, from Preciosa’? <3 -«. Weber. 
6 ly W EN i bi Y SONGS 7. March (Op. 13 are. , oas .. A. Zimmermann. 
8. March, from “ Lb e Nozz edi Figaro” ... Mozart. 
FOR A 9. March, from * I idelio” ra aia .. Beethoven

Rakoczy March

MEZZO-SOPR ANO VOICE . Funeral March (‘Lied ohne Worte,” 
e ne sas No. 27) .. Mendelssohn

The English Version by NaTraLIA MACFARREN. 12. March, from “St. Polycarp” ae .. Rev. Sir Fred. Ouseley; 
ad . 13. Dead March, from “Saul” sai «. Handel. 
PRICE ONE SHILLING AND SIXPENCE. it Fasten eee” 
veraetearnls No. 8—MARCHES. 
Lona 16. Coronation March, from ‘‘ Le Prophéte” Meyerbeer. 
Praise of Tears. Huntsman, rest (Ellen’s second | 17. March, fr om “Tdomeneo’... woes Mozart. 
Knowest thou the land ? (Mignon’s song in “The Lady of the!138. “Cornelius” March ... .. Mendelssohn. 
first song in‘ Wilhelm Meister.’’) Lake.”’) 19. Turkish March (“ Ruins of Athens” a Beethoven. 
The Message of Flowers. | Thro’ the pine-wood. 20. Processional March (‘“ Onward, Christian 
Nought may’st thou ask me /| The summer waves. soldiers ’’) ... ake aa eae .. A. Sullivan, 
(Mignon’s second song in‘ Wil- | Wanderer’s Night-song. 21. Marche Cosaque. is ... H. Hofmann. 
helm Meister.’’): | Trust in Spring. 22. March (Posthumous Work). e . Schubert. 
Oh, let me dream till I awaken | The Maiden’s Lament. 23. March, from the “ Occasional Oratorio” Handel. 
(Mignon’s third song in ‘“* Wil- | To Mignon. 24. Marche Funé bre, from Sonata I., Op. 35 Chopin. 
helm Meister.’’) | The Passing-Bell. 25. Triumphal March, from “ King Ster hen” Beethoven. 
The greenwood calls (Slumber | Alinda. 26, Pilgrims’ March (Fourth Sy mphony) ... Mendelssohn, 
Song. Ave Maria (Ellen's third song in| 27, March of the Men of Harlech. 
The full-orbed moon (Romance |‘ The Lady of the Lake.’’) 28, Festal March ... oes .. J.B. Calkin. 
from “ Rosamunde,”) | The Fisherman. 29. Birthday March wi ke Schumann, 
Hallow’d night, descend. On the water. 30. La Marseillaise

London: NovELto, Ewer and Co




THE MUSICAL TIMES 


AND SINGING-CLASS CIRCULAR. 


JANUARY 1, 1882


THE MENDELSSOHN FAMILY. did storm at the icy daughters of insular aristocracy, | Rhenish town, and his playing and his memory 
D p J 
m

ee 
ni ang but there is the best evidence that he was profoundly | astonished everybody. Of. the memory Abraha 
ladig Moved by the scenery. Thus hewrites: ‘On one of/ gives an example, and it is a striking one. The 
sofas the Hebrides, August 7, 1829.—In order to make managers of the Festival desired to change one sym- 
seping YOU understand how extraordinarily the Hebrides| phony of Beethoven’s for another—the Pastoral— 
org tected me, the following came into my mind there.” | and ‘‘ when it was mentioned, he not only instantly 
os arg lnen follow twenty-one bars of music, given here, | played it from memory, but at a small trial on the 
d hi by a happy thought, in fac simile—the germ of the} eve of the rehearsal, when there was no score at 
an beautiful overture which depicts the blowing seas | hand, conducted it by heart, and sang the part of a 
d thy and sounding shores of the western isles. On| missing instrument. Ve further learn from the 
slaved ttiving at Glasgow, the two friends set out to ex- father’s letters how the son conducted himself when 
med plore Rob Roy’s country. They met with many dis- | conducting others. It seemed a miracle to Abraham 
+s of agreeables—* a cursing young Englishman, who was | that four hundred musicians should allow themselves 
uthfd something between a sportsman, a peasant, and a|to be ordered and governed by one so young. Dut 
to seq gentleman”; wretchedness; comfortless, inhospitable | Felix knew how to command and make himself

ofte solitude, and a surly people. All the same, ‘“two]|obeyed. He forbade the charivari of tuning on the 
s angcllows wandered merrily about them, laughed at} orchestra, ‘and when several of the players at




S THE


THE MASTERSINGERS OF NUREMBERG 


AN ANALYSIS OF RICHARD WAGNER'S OPERA


“TERS @ 


—)} 3.04 —_ = — 


10


THE MUSICAL TIMES


ARY I, 1882


II


WT


12


THE GREAT COMPOSERS 


SOF


14


XUM


T5


SIR FREDERICK LEIGHTON ON ART AND 


ETHICS


XUM


16that our pictures, our poetry, our music, and every. 
thing that is ours depends upon the native vigour 
and special organisation of our mental constitution 
including the still unimpressed religious sense a¢ 
well as the moral sense. Subsequent impressions, 
or even what is called cultivation, may slightly vary 
the direction of a natural bent, but will add nothing 
to its power. Out of his specialty, the artist is like 
every one else. He has his own superstitions. [If 
they coincide with other people’s, so much the better 
for him, pecuniarily. If he affects a superstition, 
the fellow-artist, at least, will find him out; for there 
is one indispensable moral element in art—truth, 
Puritanism or purism in art is invariably false. Qn 
this subject a remarkable passage of great interest 
to musicians as well as to painters occurs in Sir 
Frederick Leighton’s address. Speaking of Von 
Overbeck’s opinion that, when Raphael painted his 
famous Galatea in the Farnesina, the Lord had 
abandoned him, he says, ‘‘A further and the 
strangest development of this frame of mind, one 
with which I have myself in my youth come in con. 
tact in Germany, is that which sees in the excessive 
love of colour an almost culpable indulgence of the 
senses.” Sir Frederick Leighton politely observes 
that these views are not likely to find favour in the 
country of Reynolds and Gainsborough. But un. 
fortunately they have found too much tavour; and 
it is only recently they have disappeared, in decora. 
tive art at least, and they still prevail in the common 
jargon of criticism on the orchestration of certain 
musical composers

Sir Frederick Leighton appeals to music as the 
veductio ad absurdum of what he calls the ‘* didactic 
theory,” by which it is upheld that, because the 
moral sense is the highest attribute and distinctive 
appanage of man, its strengthening must therefore 
be man’s noblest aim, and the dignity of all human 
achievements must be according to the degree in 
which this end is primarily and professedly subserved 
by it. This theory, he says, ‘involves the dethrone- 
ment of an art closely akin in many ways to those we 
follow,” and a ‘channel of purest emotion, an art 
divine, if a divine art there be—music.” The dignity 
of music has, indeed, strange though it may seem, 
not remained unchallenged. Such heresies, how- 
ever, may be safely left to their own foolishness. It 
is given to the supreme few who occupy the solitary 
mountain-tops of fame to be able to express, without 
incurring the charge of vanity, their high conscious- 
ness of the value to the world of the gifts they 
bestow upon it. One of these few was Beethoven, 
and his proud words are there to show us in what 
esteem he at least held the power of the art on which 
he has risen to immortality: ‘He to whom my 
music reveals its whole significance is lifted up.’” 
Yet what ethical proposition, asks Sir Frederick, 
does music convey? In what does it exhort or 
teach? The principal fallacy of the theory he refers 
to, he thinks, resides in the assumption that moral 
edification can attach only to direct moral teaching: 
or that any mode of expression appealing to the 
imagination and emotions can be properly exercised 
except in the application of its own resources, and 
in conveying those emotions of which it is the special 
vehicle

Neither music nor painting, as he says, is a fitting 
vehicle for direct moral edification. An ordinary 
sermon will have more didactic efficacy than all the 
works of Angelo or Raphael, Bach or Handel. But 
Art has an awakening influence, an ethos of its own, 
a power of intensification, and a_ suggestiveness 
through association which aid those higher moods of 
contemplation that are as edifying in their way as 
direct moral teaching




TE 


TH 


>10US- 


THE PROPOSED COLLEGE FOR MUSIC


MALE-VOICE CHOIRS


XUM


20


CRYSTAL PALACE


_«€§__


ROYAL ALBERT HALL. 


AT 


XUM


SACRED HARMONIC SOCIETY


XUM


MONDAY POPULAR CONCERTSquestion—and to which she finally responded by adding a

Mazurka by the same composer. Miss Zimmermann also 
shared the honours of a well-deserved tribute of applause 
with Signor Piatti in the interpretation of Mendelssohn’s 
Sonata for pianoforte and violoncello in B flat (Op. 45), 
the first, in point of time, written by the composer for this 
combination of instruments. The fitce de résistance, both 
as regards artistic importance and length. was Beethoven’s 
septet for string and wind instruments, a masterpiece

22

which, although visited with disparaging remarks on the 
part of its composer during his later career, has attained a 
just popularity at these Concerts, the present being the 
thirty-fifth performance. The executants were MM. Hol- 
lander, Zerbini, Clinton, Wendland, Wotton, Reynolds, 
and Piatti. Miss Henrietta Beebe contributed vocal solos 
by Handel and Taubert, accompanied by Mr. Zerbini, to 
the satisfaction of the audience

The first Concert of the past month introduced a 
novelty in the first peformance of a Pianoforte Quartet 
from the pen of Mr. A. C. Mackenzie, dedicated to Mr. 
Charles Hallé. Although in the order of composition the 
quartet now under notice has probably preceded those 
more ambitious productions, viz., an orchestral Scottish 
Rhapsody entitled ‘* Burns,” and a Cantata, ‘*‘ The Bride,” 
by which this composer has recently become favourably 
known to English audiences, there can be no doubt that 
this work would of itself have been sufficient to attract the 
notice of musicians and amateurs, being constructed and 
elaborated in a thoroughly musicianlike manner, and 
introducing characteristic elements of the composer’s 
Scotch nationality which form a novel and welcome 
feature in chamber music. The latter is especially notice- 
able in the Andante with variations (in C minor) constructed 
upon a simple but graceful and melodious theme; the 
entire work consisting of a somewhat lengthy and diffuse 
Allegro in E flat, a very sprightly Scherzo in G major, 
the Andante already referred to, and a well-sustained 
Allegro vivace, in the opening key, which includes 
some very effective fugal elaboration, and worthily con- 
cludes an undoubtedly remarkable work, which, to judge 
by the cordial reception it met with on the part of the 
audience, will be added to the permanent répertoire of the 
Popular Concerts. Mr. Charles Hallé was associated with 
MM. Straus, Hollander, and Piatti, in its execution. A 
Prelude and Fugue by Mozart, and an Allegro from an 
unfinished work by Schubert, both for string quartet, 
were likewise introduced for the first time on this occasion. 
Mr. Hallé’s solo piece was Schubert’s Fantasia Sonata in G 
major( Op. 78), which he played with his accustomed lucidity 
and excellent taste. Beethoven’s variations on the once 
popular Viennese air “Ich bin der Schneider Kakadu,” 
rendered con amore by MM. Hallé, Straus, and Piatti, 
concluded a highly interesting Concert. Mr. Lloyd was 
the vocalist, and, being in capital voice, sang to perfection 
Mendelssohn's lied ‘* The Garland,” and Clay’s * I'll sing

Zerbini

the same composer. ‘The vocalist of the evening was Miss 
Carlotta Elliot, whose brilliant and sympathetic voice and 
good training rendered her delivery of Moscheles’ some. 
what laboured ‘ Frihlingslied,’”” Mendelssohn’s “ Zuleika,” 
and Macfarren’s ‘‘ Pack, clouds, away” (the latter ably 
seconded by Mr. Lazarus in the clarinet obbligato) par- 
ticularly successful. Mozart’s genial Pianoforte Trio in E 
major (No. 6) capitally played by Mdlle. Janotha, MM, 
Hollander and Piatti, brought the concert to a most satis. 
factory conclusion. Miss Elliot’s vocal solos, we should 
add, were skilfully accompanied on the pianoforte by Mr, 
Deacon

At the last of the Saturday Afternoon Concerts the pro. 
gramme was rendered special by itsinstrumental portion con. 
sisting exclusively of works by Beethoven: viz., the Quartet 
in C major (Op.59)—the third of the famous set of three dedi- 
cated to Count Rasoumowski—the Pianoforte Trio in B flat 
minor (Op. 97), and the Sonata in C sharp minor, known 
as ‘* The Moonlight.” The estimation in which these sub. 
lime masterpieces in the sphere of abstract music are held 
at this institution may be inferred from the fact that they 
were performed here on the present occasion for the 
twenty-fourth, the twenty-ninth, and the eighteenth time 
respectively. A mere record of these significant figures 
becomes, therefore, a sufficient comment; and all we have 
to add is, that the works referred to were each most 
worthily rendered, the executants in the Quartet being 
MM. Hollander, Ries, Zerbini, and Piatti, the first and 
last-mentioned artists co-operating with Mdlle. Janotha 
in the performance of the Trio, and the lady playing 
the Sonata. Mr. Santley finely declaimed Sullivan’s 
“* Thou'’rt passing hence”? and Gounod’s charming chanson 
“*Medjé,” to which he added Hatton’s “ To 
Anthea” in response to several recalls

These Concerts will be resumed on the first Monday 
evening and Saturday afternoon of the present month




ROYAL ACADEMY OF MUSICthe second and last evening Concert of the month (12th 
ult.), the executants being MM. Hollander, Ries, Zerbini, 
Lazarus, Wendland, Wotton, Reynolds, and Piatti

generally known until many years after the composer’s 
death—exhibits all the peculiar charms of Schubert’s 
artistic individuality, and would, had he produced nothing 
else, have alone sufficed to secure him the position he 
justly holds in the estimation of all amateurs as one of 
the most worthy followers in the footsteps of the great 
Beethoven. A somewhat dreamy idealism, never-failing 
imaginative powers, combined with a certain want of 
artistic self-control which produces what Schumann 
aptly describes as ‘‘ heavenly lengths”—the general cha- 
racteristics of Schubert’s chamber-music—are likewise 
to be found in a most marked degree in the work we 
speak of. Additional interest was lent to the present 
performance by the fact of two movements—an Andante 
with variations and a Minuet—appertaining to the complete 
work (but which, owing to a difficulty in procuring the full 
score, had to be omitted on fifteen previous occasions) 
having been now included. In this its complete form 
the rendering of the Ottet occupied the space of an 
hour and a quarter, but there was no sign whatever of 
flagging attention on the part of the audience, who 
thus testified to their entire agreement with the above 
dictum of Schumann. A Nocturne in C sharp minor 
and Polonaise in C minor, by Chopin, were played in 
her best style by Mdlle. Janotha, who is never hap- 
pier than when she interprets the pathetic Polish tone- 
poet; and elicited the accustomed encore, to which the 
gifted artist responded by substituting another piece by

This 
remarkable work—written in the year 1824, but not made

every respect a most meritorious composition for so young 
a writer. In memory of the late Arthur Herbert Jackson 
—one of the most promising students of the Academy—a 
Capriccio from a MS. Suite was also included in the pro- 
gramme. ‘The pianists were unusually good: Miss Amy 
Hare in a Movement from Beethoven’s Concerto in G, 
Mr. Alfred Izard in the ‘* Allegro Moderato” from Stern- 
dale Bennett’s Concerto in C minor, and Miss Cantelo in 
Walter Macfarren’s Concertstiick in E, showing both 
artistic feeling and the result of careful training. A credit- 
able performance of the first movement of Mendelssohn’s 
Violin Concerto, by Mr. Bent, also deserves mention. The 
solo vocalists were Miss Kate Hardy, Miss Ambler, Miss 
Hipwell, Miss Law, and Mr. Pounds, all of whom were 
warmly received, the chief honours, however, being gained 
by Miss Hardy, who gave the scena from the first act of 
‘* Fidelio”? with much dramatic power. Mendelssohn’s 
Psalm ‘* Not unto us” (the solo parts well sung by Miss 
Beere, Mr. Dunman, and Mr. Lucas Williams) commenced 
the Concert, which was steadily conducted by Mr. William 
Shakespeare

MR. GEAUSSENT’S CHOIR




RO


TH! 


XUM


MM, 


23


ROYAL NORMAL COLLEGE AND ACADEMY 


OF MUSIC FOR THE BLIND


XUM


RTS. in A, of 
Agnes

BRIGHTON AQUARIUM CONCE 
THE seventh of these Concerts, given on 
included in its programme Wagner's “ Rienzi” 
Ratt’s Festmarsch, and Beethoven's Symphony 
which an excellent rendering was given. Miss 
Zimmermann pleased the audience much by her 
performance of Rub instei n’s Conce rto in G, and Mr. 
Cummings obtained the only encore hitherto given to a 
vocalist at these concerts by his singing of Mr. Corder’s 
Tennyson song, ‘*O sun, that wak’nest.” 
The eighth Concert, besides presenting the Ballet music 
of Gounod’s ‘ ’ hth Symphony

ust’ and Beethoven's Ei 
brought forward in the shape of a piece entitled

very

We are happy to hear that the pecuniary result of the 
enterprise has been so far favourable as to warrant a belief 
that the concerts are now established on a permanent basis

ambitiously, with Beethoven’s * 
though played with great spirit, 
the weakness of the strings. Some

green h

MUSIC IN MANCHESTER

Ar Mr. Hallé’s Concert on November 24 the orchestral 
selection consisted of Beethoven’s Pastoral Symphony, 
Berlioz’ fine ‘* Waverley” Overture, Wagner’s * Siegfried 
Idyl’’ in E, and the Coronation March from ‘** Le Prophete.” 
Mr. Hallé played with infinite grace and neatness the

Menuetto and Gavotte from Raff's Suite in E flat, and 
3rahms’s Hungarian Dances, Nos. 4, 6, and 7. Miss




24Miss Elliot was more successful in iuidee wy Rien: ‘nil

Rubinstein. The programme on the Ist ult. comprised 
Raff’s Symphony ‘‘Im Walde,” performed for the first 
time here, the Overtures to ‘The Ruins of Athens,” by 
Beethoven, ‘‘ The Siren,’’ by Auber, and the Ballad and 
Air Slave with variations from Délibes’s Ballet ‘ Cop- 
pelia,” repeated by desire. Mr. Hallé played Mozart’s 
Concerto No. 5,in C, in his best manner, and Miss Orridge 
and Mr. F. King contributed several songs, of which 
Berlioz’s ‘‘ The Spectre of the Rose,” sung by the former, 
eserves special mention. Berlioz’ s ‘Childhood of 
Christ,” produced in England by Mr. Hallé last season, 
was given on the Sth ult. The vocalists were Miss and 
Mr. Santley, Mr. Edward Lloyd, and Mr. Hilton, all of 
whom did ample justice to the difficult and occasionally

THE MUSICAL TERS. ‘k. peaheiaaal I, 1882

MUSIC IN LEIPZIG. 
Leipzig, December 12

ungrateful music. The choruses were exceedingly well 
rendered, those for the unseen angels being especially effec- | 
tive. Onthe 15th ult. Mende Issohn’s Italian Symphony, 
Beethoven’s Festival Overture, Weber’s Overture to | 
**Oberon,” and Wagner’s March from ‘ Tannhauser,”’ 
were all familiar items. The only novelty in the pro- | 
gramme was the Aria per gli Attleti, Chaconne, and | 
Gavotte from Gluck’s ‘‘ Paride ed Elena.” The Gavotte 
has been familiarised through the medium of a pianoforte 
transcription; the other numbers, though probably un- 
known to the majority of musicians, are however not less 
interesting, being remarkable for the melodic beauty and 
rhythmical nature of their subjects, not less than the very 
happy and elaborate instrumentation. Mr. Hallé played | 
Beethoven’s Sonata in A flat, Op. 26, and Mdlle. Louise | 
Pyk sang Mozart’s ‘‘ Non mi dir,” ‘Casta Diva” from | 
* Norma,” and songs by Brahms and Moore. Two per- | 
formances of the ‘* Messiah ”’ were given under Mr. Hallé’s 
direction on the 22nd and 23rd ult. The singers on the 
former date were Madame Lemmens- Sherrington, Madame 
Patey, Mr. E tafe Lloyd, and Mr. Santley; and on the 
latter the only change in the quartet was the substitution 
of Miss Orridge for Madame P atey

At the Gentlemen’s Concert on November 28 the | 
instrumental numbers were Beethoven’s Eighth Symphony, 
the introduction to the third act, Dance of Apprentices, 
Procession and Homage to Hans Sachs from “ Die 
Meistersinger,’’ and Gounod’s Pageant March from ‘‘ La 
Reine de Saba.” The performance of the Symphony was | 
somewhat lacking in the finish and precision usually 
characteristic of the orchestra. Mdlle. Janotha gave a 
performance of Schumann’s Pianoforte Concerto in A | 
minor, which for perfection of technique and intellectual | 
grasp has probably rarely been surpassed. She also played 
in admirable style Mendelssohn’s Andante and Rondo 
Capriccioso in E, and the same master’s ‘‘ Lied ohneWorte,”’ 
No. 4. Miss Elliot and Mr. Oswald were the} 
and the former was again very successful. A | 
novelty in her selection was the fine air ‘ Suspicious 
terrors vanish,” from ‘* Guistino,” one of the neglected | 
thirty-nine Italian Operas of Handel. At the Classical | 
Chamber Concert on the 14th ult., Madame Norman. | 
Néruda and Herr Straus, who had been successively | 
announced, but were unable to appear in consequence 
of severe domestic afiliction, were replaced by Herr 
Hollander. This gentleman, Mr. Speelman, Herr Otto 
Bernhardt, and Signor Piatti gave a good, though not

Book 
vocalists

exceptionally fine, performance of Beethoven’s string | 
Quartet in B flat, Op. 18, No. 6; and with Mr. |} 
Hallé played Rheinberger’s Quintet in C, Op. 114

the latter work was exceedingly well rendered. Mr. Hallé 
and Signor Piatti gave a phenomenally fine reading of 
Mendelssohn’s sonata for piano and violoncello in D, Op. 
58, and Herr Hollander played Vieuxtemps’s ‘ Réverie”’ 
and a Spanish Dance by Sarasate. Mrs. Alfred Caldi- 
cott was the vocalist

At the Memorial Hall Concert, on the 12th ult., Quartets 
by Schumann, in A minor, Op. 41, No. 1; Beethoven, in 
C minor, No. 4, and Mendelssohn, in E flat, No. 1, were 
very well rendered by Messrs. Risegari, Speeiman, Bernhardt 
and Vieuxtemps

At Mr. De Jong’s Concert, on the 3rd ult., Miss Agnes 
Ross, Miss Hope Glenn, Mr. C. Abercrombie (who replaced 
Mr. Lloyd at short notice), and Signor Foli were the 
vocalists. The orchestral numbers included Reissiger’s 
fine Overture “ Die Felsenmihle,’ a work which deserves

absence, from 1835 to 1847—is fully described

The centenary was signalised by a representative Con- 
cert. Herr Reinecke, who received that day a new decora- 
tion from the King of Saxony, opened the proceedings 
with a ‘ Fest-Ouverture’’ written for the occasion. A 
recited Prologue followed; then a Symphony of Haydn, 
and then Mozart's Concerto for violin and viola, in which 
Dr. Joachim and Herr Engelbert Réntgen took part. 
Beethoven’s * Coriolanus’’ Overture, Mendelssohn’s Violin 
Concerto, and Schumann’s Symphony in D minor made up 
the rest of this colossal programme, the length of which 
seemed intended as a reminiscence of the ancient custom 
of the hall. It will at once be seen that Herr Joachim 
made the special feature of the evening; nor can we fail to 
note the graceful spirit which suggested his performance of 
the Concerto of the great Leipzig master who brought him, 
while still a boy, before the audience of the Gewandhaus

On the following day he presided over the second of the 
series of Chamber-music Concerts, which take place 
each winter in the Gewandhaus. The programme con- 
sisted solely of three quartets: Beethoven’s in C sharp 
minor, the others by Cherubini and Schumann. Such an

arrangement offers an exceptional opportunity for the




ALTO


LENO


XUM


A FOUR-PART SONG. 


PIANO


XUM


ON


XUM


MADRIGAL FOR FIVE VOICES


TENOR


_—= 2 === == == S25


XUM


XUM


TO SHORTEN WINTER'S SADNESS


XUM31

a 
derr Rontgen, the respected first violin of the Gewandhaus, 
in that capacity unrivalled; in chamber music, however, 
re easily allows himself to be overpowered by other 
astruments. On the other hand, even Herr Reinecke did 
sot restrain his powerful command of the piano so as to 
jlow the other instruments their due share in a work of 
ch distributed beauty as Schumann’s E flat Quintet, and 
‘ fortissimo was much more than a match for the 
soloncell o in Mendelssohn’s D major violoncello Sonata. 
The symphonies produced at the Gewandhaus on the 
ist and 8th inst. were worthy to begin the second century 
of the hall, and one can hardly imagine it possible that 
Beethoven’s Fourth Symphony and Schubert’s greatest 
work, that in C major, could be in any respect better 
performed. At the former Concert Mr. Willem of 
Amsterdam, was received as a welcome accession to the 
ranks of violinists; at the latter appeared Mr. Franz 
Rummel, who has now settled at Berlin. Hig sh 
se two artists deserve, it must be decidedly 
neither shows the signal promise which some critics ha\ 
been disposed to see in their treatment 
the piano. As regards the works they play 
coer was vigorous, but conventional and often com- 
onplace. Mr. Rummel, curiously enough, did not think 
oat while to add to the répertoire by which he has 
been already known at Berlin, and w

ACS




THE GUILDHALL SCHOOL OF MUSIC


XUM


32


THE | 


THE § 


XUM


XUMthe popular ear without sacrificing the principles of good

from, Beethoven, and

Herr




1882. 33A CommiTTee for the Testimonial Fund has 
been formed in Glasgow, where Mr. Manns’s musical 
services are much appreciated; and having requested that 
the subscription list of the fund may conti nue open till the 
close of the musical season in Scotland, the London Com- 
mittee has resolved that the list shall not be closed until 
the last day of February next

Mr. ALFRED Puysick gave three Organ Recitals at St. 
Mark’s Church, Camberwell, during the past month, viz., 
on the 2nd, gth, and 16th. The programmes inc luded 
works by Bach, Mendelssohn, Beethoven, Handel, Spohr, 
Batiste, Wely, &c., all being most ably oe

Manns




THE. 


A MC 


THE 


XUM


J. M


A MORE 


XUM


35


36


FOREIGN NOTES


SS 


XUMustelr rte Quartet (Sz 5). Pea

pody Institu r 1artet, Op. Haydn); 
Air (Mozart) ; “Kr ut er (Beethoven). nstitute 
November 1g): Strings Op. 18, No Rap- 
i ) No. 12 (Li Quartet

November String Quartet




CORRESPONDENCE


MALE-VOICE CHOIRS. 


TO THE EDITOR OF ‘THE MUSICAL TIMES.” 


TO THE EDITOR OF ‘* THE MUSICAL TIMES


J I


BAGSH


XUM


39


ORGANISTS AND CHOIRMASTERS. 


TO THE EDITOR OF ‘* THE MUSICAL TIMES


AN OLD ORGANIST A


ND CHOIRMASTER


BRIEF SUMMARY OF COUNTRY NEWS


3EDFORD. 


XUM


40


MANS 


NEW THE MUSICAL EES. i an ARY I, 1882

Leeps.—At Dr. Spark’s Free Organ Recital, in the Sent Hall, on — sts were Miss Greaves, Mis: Ay Greaves, Me Tom Smith, 
Saturday evening, the roth ult., the audience was both large and | Moss, Jos. Greaves, Percy Peplow, Fennell, and Master Jas. Brett ; 
appreciative. A well-varied programme, including one of Mozart’ S cmmanian sts, Miss S. Greaves and a Mr. J. Greaves. 
symphonies, W yas excellently rendered. Under the auspices of the XFORD.—Mr. William Carter's Can tata PI 
Leeds Amateur Orchestral Society, a Concert was given in the Albert performed by the V n in A ey 
Hall Mechanics’ Institution, on Saturday evening, the roth ult. The 1 
Society has recently secured the valuable services, as Conductor, of 
Mr. J. P. Bowling, under whom the members are now making most 
satisfactory progress. Beethoven's Symphony No. 1 was capitally 
played by the band, as were also the Overture to Mozart’ s Figaro and 
the War March of the Priests from Mendelssot Athalie. The vocal- 
jsts were Miss Woods, who sang several songs very effectively, Mr. 
Herbert Ramsden, and Mr. H. J. Coldwell. A feature of the Concert 
was the performance of two solos on the violin by Mr, Carrodus. Mr. 
Bowling gave a fantasia by Thalberg, on the pianoforte, which was 
highly appreciated——The Leeds Orchestral Society gave its first 
Concert for the season at the Church Institute, on the 16th ult. The 
band numbered nearly fifty performers, and played the Overtur 
Gazza Ladra and Les Sirénes and Haydn’ s* Military Symph 
highly creditable manner. The soloists were Mrs. Alfred } 
Miss Maggie Critchley (violin), Mr. Alfred Brov i 
Mr. J. Sydney Jones, jr un, (clarinet). Mr. J. Sydney 1

LiverrooL.—A Concert was given in the Lecture Hall attach 
St. David’s Church on Wednesday, re aagg 30, before a 
audience. The artists were Miss Hughes, Miss Dempster, 
Bryan, Mr. Binning, and Mr, Pedder pod ts); and the Misses 
Samuelson contributed some p vianoforte duets. The church che oir ar 
glee party added materially to the enjoyment of the evening ; 
panists, Miss Sumner and Mr. Foxley ; Conductor, Mr. Wm 
Parry. A song, with chorus, compos sed expres 
Mr. Parry, was effectively x by Miss H 
Concert was very successful.—Mr. James ]. 
organ in the Derby Road Presbyterian Church, Bootle (built by 
Booth and Hepworth), on Thursday evening, the Sthult. Anex 
programme was provided




XUM


42


1882Concert in the Guildhall, on the 13th ult., before a large audience, 
| The programme consisted of Bennett’s Wom an of Samari a, and a 
miscellaneous selection, including four excerp ts from Mende

music to A Midsummer Night's Dream. Bennett’s charmi wo 
was well performed, the choir singing admirably in the ch oruses, and 
the Hon. Spencer Lyttelton rendering the bass recitatives and solos in 
excellent style. In the second part Schubert’s Evl-Ki ng was wells 
by William Anstice, Esq., and the orchestra gave a capital rendering 
of the Larghetto from Beethoven’s second Sy woly ig! and the Notturno 
Midsummer Night's Dream. The Rev. E. Hall, Precentor 
ester Cathedral, officiated as Conductor. he Concert was 
highly successful

OrGAN ApPporINTMENTS.—Mr. Charles Edward Melville, Organist 
and Choirmaster to St Philip’s Church, Leeds.—Mr. Frederick Wil. 
liams, Organist and Choirmaster to Holy Trinity Church, Chelsea— 
Mr. William Henry Whitmore, Organist and Choirmaster to St 
John’s Presbyterian Church, Tottenham.—Mr. John William Oxley 
Organist and Choirmaster to the Parish Church, Galashiels, Scotland




MISS CLARA 


JIANOFORTE LESSONS 


1882


JON IOR PIANOFORTE TUNER WANTED, 


IN 


NJ O71 


ELLOW


H,, 121


XUM


GY, 


LED 


PIL. 


20. 


SD


DURING THE LAST MONTH


TOVELLO’ S TONIC SOL-FA ~ RIE S. 


ROWDER, C. F


JOVELLO, EWER AND CO.’S PIANOT ‘OR


IS


HER


XUM


NJ EW, REVISED, AND ENLARGED EDITION 


. J ; AD


CT


AN


SEGUIDILLA, |" REVERIE


ROMANCE. AUBADE 


CANON. IDYLL. 


MAZURKA. MINUET


44 


DVERTISER


PUBLISHING


BUSINESS.—A


N ELBOURNE EXHIBITION, 1881.—FIRST 


OWER” ORGAN


FOR 1882. 


ARRANGEMENT OF


BRASS REED 


£25


NE


SONG, 


ANTHE 


OPER 


PZ 


B U7 


XUM


THE MUSICAL TIMES


BRISTOL. 


MR. J. HAMILTON


6, CHESTERTON TERRACE, CITY ROAD, 


MANUFACTURER OF 


NEW MUSIC-ENGRAVING


STEAM PRINTING


EXHIBITED IN


50 PER CENT. CHEAPER. 


PATENT PAPER TYPE COMPANY, 


62, HATTON GARDEN, LONDON, E.C. 


MALEY, YOUNG & OLDKNOW, 


ORGAN BUILDERS, 


KING’S ROAD, ST. PANCRAS, N.W


VIOLIN AND BOW


MAKERS AND REPAIRERS


GEORGE WITHERS & CO. 


WHOLESALE IMPORTERS OF


MUSICAL STRINGS


A FINE COLLECTION OF ITAL IAN INSTRUMENTS. 


VIOL INISTS.—THREE 


FIRST ITAL LIAN 


THE MUSICAL TIMES 


CASES


FOR BINDING THE NUMBERS FOR THE PAST YEAR, 


VOL. XXII. 


THE CHORUSES 


ESPECIALLY FOR THE USE OF 


CHORAL SOCIETIESBACH'S PASSION (ST. MATTHEW) ... aaa “ee “a 2

BEETHOVEN'S MOUNT OF OLIVES... as eas we OG

BENNETT'S MAY QUEEN ese aes eve ae «EO




XUM


46


12, BERNERS STREET, LONDON, W. 


TO TO CONCERT- GIVERS AND EN TREPRENEURS 


GENERALLY


TO THE CLERGY. 


CHOIR-BOYS. 


MUSIC FOR BANQUETS, &. _ 


ORCHESTRAS AND CHOIRS. 


A. MOZA 


PIANOFORTE CO


STE DM: AN


ZART'S 


INCERT


OS


REVISED AND EDIT


IR


ED FOR STUDY AND CO


S. LEBER


NCERT-ROOM BY


3 4 1 6 2 0


3 6 I 5 2 0


3 4 I 6 2-0


3 6 1 8 2


MUSIC OF THE BIBLE 


INSTRUMENTS


WITH AN ACCOUNT OF THE 


DEVELOPMENT OF MODERN MUSICAL 


FROM ANCIENT TYPES, BY 


WILL PUBLISH IN MONTHLY PARTS


PRICE SEVENPE


HISTORY OF MUSIC


NCE EACH


FERDINAND PRAEGER


WITH NOTES AND ADDITIONAL CHAPTERS BY THE


ILLUSTRATED THROUGHOUT


NOW RE


PART I. ADY


PETTER, GALPIN 


CASSELL, 


LUDGATE


DRAMATIC SINGING 


PHYSIOLOGICALLY ESTIM


BY


WALTER HAYLE WALSI


CLOTH


M.D


ULL ORCHESTR: \L SCORES 


PUBLISHED BY 


NOVELLO, EWER AND CO, 


SPOHR 


GOD, THOU AKT GREAT. 


HANDEL 


HANDEL AND MOZART


HAYDN


MENDELSSOHN


SIR W. S. BENNETT 


ATED


POP


POP


SIX


SIX


TU


SIXTY


XUM


TO)... ASHDOWN & PARRY'S NEW FOREIGN PUBLICATIONS 


' 1882, 


VIOLIN


PIANOFORTE DUET. 


POPULAR I


A NEW ORGAN-LBOOK 


L'ORGANISTE PRATIQUE


FLUTE. 


SIXTY-FOUR COMIC MEI ODIES 5 


CORNET. JOHANN SEBASTIAN BACH’S 


TUTOR WITH A SELECTION OF THE MOST EDITED BY 


XUM


STAFF NOTATION MUSICAL PUBLICATIONS 


BY AS 


J. CURWEN AND SONS


YHORAL SINGER, T 


E ASY ANTHEMS F 


OR


LONDON: J. CURWEN AND SONS, 8, WARWICK LANE, E.C


XUM


THE


LTA 


MU


I 4 0 


E. 4 0 


HEINRICH HOFMANN. 


4 151 M 1, 


SONGS FOR THE LITTLE ONES 


, SIMPLE CAROLLERS ARE W B 


. P. DOUGLAS 


4 BOUCHER 


3 GRAND MASONIC MARCH 


THE READE R AND THE FLOWERS 


: } FOUR-PART SONG WITH SOLOS 


50


ISRAEL RESTORED 


AN ORATORIO 


COMPOSED BY 


CANTAB. 


RUTH


NEW SACRED CANTATA 


THE


WIDOW OF NAIN


SACRED CANTATA 


FOR SOLI VOICES AND CHORUS


LLO, 


NOVELLO’S OCTAVO


GENERAL


THE


EDITION


HISTORY


MUSIC


THE 


OF


SCIENCE & PR ACTICE OF


SIR JOHN "HAWKINS. 


NOW READY, NEW E DITIONS OF


2 SACRED MUSIC WITH ENGLISH WORDS


3» MUSIC WITH VOCAL AND ORCHESTRAL 


PARTS


4 MUSIC FOR THE PIANOFORTE


5» SECULAR VOCAL MUSIC


6 SACRED MUSIC WITH LATIN WORDS


BLUE BEARD 


A HUMOROUS CANTATA 


MUSIC COMPOSED BY 


JACK & THE BEANSTALK 


EDMUND ROGERS


BY


TO CHORAL SOCIETIES


NARCISSUS AND ECHO


COMPOSED BY 


CANT ATAS 


FC 


TWO SHILLINGS & SIXPENCE EACI


SU MME R NIGHTS


LES NUITS D’ETE) 


SIX SONGS BY THEOPHILE


GAUTIER 


BY 


HECTOR BERLIOZ 


TO CHORAL SOCIETIES


THE MUSICAL TIMES


EACH CONTAINING FORTY-EIGHT CHORAL PIECES, 


SACRED AND SECULAR


LONDON: NOVELLO, EWER AND CO


CO


CC


ROUN


ROL


STOCK


COLLEGIATE AND SCHOOL CHOIRTRAINERS & SCHOOLMASTERS


CATHEDRAL CHANT-BOOK SELECTED FROM 


O DURANTE, HANDEL, LEO, SCARLATTI, STEFFANI


AD 


SUPPLEMENTAL


BOOK OF EXERCISES


LEARNING TO SING AT SIGHT


COMPILED AND ARRANGED BY


HENRY GADSBY


M! USICAL “ODINION AND "MUSIC. TRADE


N HILLING, 


A COLLECTION OF ONE HUNDRED


XUM


THE HYMNARY


A BOOK OF CHURCH SONG


THE FOLLOWING EDITIONS ARE NOW READY


THE BRISTOL TUNE-BOOK 


) MATCH E ITE R EDITI


THE BOOK OF PSALMS (B IBLE VERSION), 


THE ANGLICAN HYMN- BOOK. 


QUFFLE {MENTAL TUNES TO POPULAR 


FOR CHORAL WEDDINGS. 


ESSED ARE ALL THEY THAT FEAR


IGH FESTIVAL COMMUNION SERVICE. 


GOD ISOUR HOPE “AND STREN


GTH 


ENTI


~-« REDUCED TO THREEPENCE EACH. 


* A. MACFARREN’S TWO-PART ANTHEMS 


ANTHE MS


> ASY 


OW TO LEARN TO PLAY THE PIANO 


HEC


HE


- 40 


OUL] 


CANI


OULI 


CAN


HE 


OBBLIGA


MAI


GTH


COM. 


LORD'S 


I, 1882 3


SECOND SERIE


PROI 


J.B


P SAL: MS


CHORI ANGLI


OULE’S 


CANUM


ANGLI- 


HE ORDER FOR THE HOLY COMMUNION. 


NOVELLO’S COLLECTION OF 


ONE HUNDRED AND SIXTY-FIVE


EDITED BY 


GEORGE C. MARTIN 


SUB-ORGANIST OF ST. PAUL’S CATHEDRAL. 


LEY


WILFORD MORGAN’S GRAND MARCHES. 


MARCIA GIUBILANTI 


MARCHE JOYEUSE 


MARCHE MILITAIRE 


MARCH IN C MINOR 


TO 0 CONDUC T ORS OF “CHOR: AL SOCIET IES. 


T° PIANISTS. OTTE 


XUM


HE. ‘BOOK. 


ELEY AND MONKS I 


JOULE’S COLLECTION OF 527 CHANTS, 57 


POINTED FOR CHANTING


THE


LONDON: NOVELLO, EWER AND CO. 


IES 


PRICE SIX SHILLINGS. I*OLIO, 200 PAGES


J. S. BACH’S 


IN ALL THE MAJOR AND MINOR KEYS 


W. T. BEST


~~ ORIGINAL


COMPOSITIONS FOR THE ORGAN 


- NEW PART-SONG. 


THE NIGHT CHIMES 


C. E. TINNEY 


TEW PART-SONG, THERE IS DE W FOR 


SOUR-P ART SONGS. 


THE MUSICAL TIMES 


REDUCED PRICE


ANGLICAN


PSALIEBREA CHANTS


SINGLE AND DOUBLE


EDITED BY THE


REV. SIR F. A. GORE OUSELEY, BART, ETC


AND


EDWIN GEORGE MONK


HARROW SCHOOL MUSIC


JOHN FARMER


ORATORIO. —“ CHRIST. “AND HIS SOLDIERS


SINGING QUADRILLES


TEW


THE


HARMONIUM TREASURY


A SERIES OF SELECT PIECES ARRANGED BY


W. ELLAOTT


HARMONIUM VOLUNTARIES


ARRANGED BY


J. W. ELLIOTT


NOVELLO, EWER AND CO


LONDON


: I 1 


DAVIS. 


O. SONGS. 


ARTHUR HENRY BROWN40. Aria, by Gluck. 1767

41. “Sanctus” and “ Hosanna,” from André’s Mass. Op. 4 
42. Last Chorus, from Beethoven's “ Mount of Olives.” 
43» “He shall feed His flock, ” from Handel’s ‘* Messiah

44. “Quoniam Tu solus,’ by Vincenzo Righini. 1788




THE KING OF THE NIGHT 


ALF 


SONG FOR BASS OR BARITO


MISTRESS MINE 


THE PILGRIMS 


NE


SUNG BY MR. FRANK BOYLI


NEW SONG


THE LOVE HE LD DEAR 


N EW


OLD 


NEPTUN 


JACK FROST. 


1 ERL- “KING


56


NOVELLO, EWER AND CO’S


LIST OF WORKS SUITABLE FOR


PRESENTS AND SCHOOL PRIZESPI. AN OFOR T E CL: ASSICS

BACH.— FORTY-EIGHT PRELUDES AND 
FUGUES Folio 
BEETHOVEN _—SON AT. AS. Ed ited by Agnes 
Zimmermann me Folio, cloth, gilt 
BEETHOVEN.—SONATAS. Edited by Agnes

Zimmermann. 8vo, cloth, gilt, 7s. 6d.; paper cover 
HANDEL.—MESSIAH. _ Arranged




MENDELSSOHN. — PIANOFORTE


WORKS 


MENDELSSOHN. — PIANOFORTE WORKS


MENDELSSOHN.—LIEDER OHNE WORTE. 


MENDELSSOHN,.—LIEDE R OHNE WORTE. 


MENDELSSOHN.—LIEDER OHNE WORTE. 


WORTE


VOC. AL ALB UMS


MENDELSSOHN.—SONGS


MENDELSSOHN.— THIRTEEN TWO-PART 


MENDELSSOHN. — THIRTEEN 


TWO-PART


CHRISTMAS CAROLS—NEW AND OLD. 


CHRISTMAS CAROLS — NE W AND OLD. 


CHRISTMAS CAROLS —NEW "AND OLD


NATIONAL NURSERY RHYMES AND SONGS. 


MENDELSSOHN. — THIRTEEN TWO-PART


RUBINSTEIN. — EIGHTEEN TWO - PART


SCHUBERT.—TWENTY SONGS


6 MOOR


CHRIST) MAS BOOKS


LITTLE SINGERS. 


SACRED SONGS FOR 


CHRISTMAS BOOK OF LITTLE SONGS


THE 


6 FOR YOUNG SINGERS


) RPI 


12 


I5 @ 


12 ‘ : 


15 


18 ( 


CO 


ONI 


XUM