


 MUSICAL time 


 MUSIC number . 


 thy mercy GI 


 ONT 


 SE 


 ICHFIELD CATHEDRAL CHOIR . — 


 JUNE 1 , 1867 


 ISS AGNES ZIMMERMANN GRAND 


 IDDLE - CLASS CHORISTER SCHOOL , 


 professional notice 


 signor J. ALFRED NOVELLO . 


 MR . C. COWDEN CLARKE , 


 MR . HENRY LAHEE 


 MR . J. C. BEUTHIN , 


 MR . JOSEPH SCATES , 


 USIC send sale return , 


 USIC engrave , print , PUB- 


 lishe good style , moderate 


 valuable useful second - hand 


 usical instrument VOLUN 


 rass , reed , string , drum 


 EYOND ALLCOMPETITION.—T. R. WILLIS 


 UTLER MUSICAL instrument 


 cornet , SAXHORNS , DRUMS , FLUTES , CLARIO . 


 W. SNELL IMPROVED HARMO 


 W. SNELL CHURCH HARMO 


 HO 


 GR 


 LOA 


 LET 


 ESS 


 LUN . | ) 


 | 


 EMY ROYAL PIANOFORTE TUTOR . — 


 CHOIR MUSICAL record 


 LARIO- 


 SICAL 


 RMO- 


 . RMO.- 


 VEST - pocket pitch - PIPE 


 HORTHAND . — PITMAN PHONO- 


 MUSIC MUSICAL instrument 


 METZLER CO . new publication 


 COMMUNION HYMN . 


 METZLER 9 


 87 , GREAT MARLBOROUGH STREET , W. 


 HARMONIUMS . 


 LEXANDRE HARMONIUMSUNING business , Musie 
 Business , dispose . apply W.S. , Messrs. 
 Novello , Ewer Co. , 69 , Dean - street , Soho , London . 
 wax TED purchase good second - hand 
 copy GOSS ORGANIST COMPANION ( com- 
 plete work . ) send price H. Curtis Card , Lewes 

 EW ORGAN arrangement 
 BEETHOVEN " HALLELUJAH CHORUS , " W. J. 
 Westbrook . Davies ’ Catalogue , 417 , Oxford - street 

 CHORAL festival . ss 
 PRECES RESPONSES , 
 LITANY , compose Tats . arrange Voices 
 JoserH BarnBy . price 4d . 
 FERIAL response , LITANY , use 
 Church 8 . Andrew , Wells - street . edit J. Barnsy . price 44 . 
 simple form INTONED SERVICE.—THE 
 prece response , LITANY , set Monotone , 
 Organ Accompaniment , specially adapt Parish Choirs , 
 JosePH BarnBy . price 4d . London : Novello , Ewer Co 




 TAN , 


 69 


 MUSICAL TIMES , 


 JUNE 1 , 1867 


 MUSIC ENGLISH CHURCH . * 


 GU 


 _ — 4 


 MAJESTY THEATRE 


 ROYAL ITALIAN OPERA 


 MR . JOSEPH BARNBY CHOIR 


 FESTT\ 


 XUMTHE MUSIOAL TIMES.—Jvnz 1 , 1867 

 gracefully write , doubtless favourite ; 
 Mr. Lahee ' * ye wood " eminently successful . 
 madrigal , W. Beale ' * come , let join roundelay , " 
 Ward " die , fond man , " Marenzio " lily white , crimson 
 rose , " Gastoldi ' * maiden fair Mantua city , " 
 attention variation tone 
 soul composition . vocalist 
 Madame Maria Vilda Mr. Sims Reeves , 
 excellent voice . Madame Vilda singing ' * Qui la voce , " " Casta 
 Diva , " * " Son vergin vezzosa , " create furore 
 audience ; encore second song powerful 
 resist . listen Mr. Reeves singing 
 Handel Recitative Air , * * deep deeply , " " " Waft 
 h angel , " hear perfect interpreta- 
 tion occasion . equally successful 
 Beethoven * " Adelaida " Kiicken " twilight darken , " 
 encore . - Miss Agnes Zimmermann , 
 fulfil prediction , place 
 accomplished pianist day , Mendelssohn 
 " Capriccio , " Op . 33 , excellent taste finished execution , 
 play Gavotte Bach , b minor , 
 Chopin Valse flat . recall platform , 
 gaveMendelssohn " volkslie " minor , retire amidst 
 enthusiastic applause . Mr. Benedict accompany principal 
 singer skill judgment consummate artist . 
 concert extremely attend ; Mr. Barnby , 
 steadiness intelligence conduct increase confidence 
 - discipline force command , warmly 
 applaud conclusion concert . successful 
 attempt hearer hopeful future 
 choir ; look forward utmost interest series 
 concert advertise season 

 MR . HENRY LESLIE CHOIR 




 PARIS EXHIBITION , 1867 


 translation . ) 


 FESTIVAL SONS CLERGY 


 GALLERY ILLUSTRATIONA concert Mr. Joseph Heming Choir 
 gth ult . , St. James Hall , programme 
 entirely devote glee , quintett , choral composi- 
 tion Sir Henry Bishop . delicacy 
 refinement choir subject toa 
 rigid training ; composition encore 
 enthusiastically . song evening ; 
 Mr. Walter Bache solo pianoforte , Messrs. J. B. 
 Chatterton Cheshire perform duet harp . 
 concert attend 

 Concert Mr. Charles J. Hargitt , 
 St. George Hall , 10th ult . , deserve record 
 attempt conscientious professor place 
 know work Beethoven english public . 
 doubt reason composition familiar 
 majority concert - goer simply account inferiority 
 nore popular work ; contain great beauty 
 unquestionable ; Mr. Hargitt deserve credit 
 public opportunity judge . " Praise 
 Music , " " Calm sea prosperous voyage " 
 rarely hear ; itis highly probable ex- 
 cellent performance work occasion , concert- 
 giver think worth revive . concert 
 respect highly interesting ; pleasure 
 attention entertainm utterly unlike 
 benefit concert constantly chronicle . 
 principal vocalist Madame Lemmens - Sherrington , Miss Rose 
 Hersee , Miss E. Cole , Madame Laura Baxter , Mr. Cummings , 
 Mr. Weiss . Mademoiselle Mehlig solo pianist , 
 orchestra complete excellent department 

 Mr. Marcellus Higgs concert 8ta 
 ult . , St. James Hall , produce operetta 
 composition , * * noble Moringer . " libretto , present 
 Mr. Higgs , gentleman sign ' * Amicus , " 
 means interesting ; scarcely fair judge 
 composer , , employ power subject 
 genial suggestive musical idea . Operetta ex- 
 ceedingly receive audience ; graceful 
 ballad encore . principal sustain Miss 
 Louisa Pyne , Madame Patey - Whytock , Mr. Patey Mr. Cummings 

 Aptommas , singing Herr Stepan , Mannheim , 
 romance Lortzing , Meyerbeer scena Roberto , 
 display bass voice admirable quality power 

 Mr. Ridley Prentice Concert 13th 
 ult . , Hanover Square Rooms , perform true 
 artistic taste good execution pianoforte work , 
 successful Beethoven Sonata appassionata , 
 Mendelssohn Sonata D , pianoforte violoncello , 
 join Signor Piatti . play Minuet 
 Trio , compose , exceedingly receive . 
 vocalist Miss Louisa Pyne , Madame Patey - Whytock , 
 Mr. Patey . Mr. Walter Macfarren conductor 

 tue annual performance Messiah 
 benefit Royal Society Musicians , place 3rd ult . , 
 St. James Hall . principal vocalist Miss Robertine 
 Henderson , Madame Sainton - Dolby , Madame Talbot Cherer , Mr. W. 
 H. Cummings , Mr. Patey , Mr. Weiss . Trumpet obbdligato Mr. 
 T. Harper . Professor Sterndale Bennett conduct 




 TREBLE 


 ALTO 


 MENDELSSOHN . 


 SS 


 TENOR 


 PA 


 SS 


 FP 


 ESI ( Z 


 pp 


 SS ; 


 @ = 104 


 - 5 | test 


 XUM 


 NEY 


 XUM 


 XUM 


 CORRESPONDENTSCuatHaM.—The Lecture Hall occupy 
 past month concert party , 
 generally successful . follow artist , 
 service ; retain respective programme , 
 viz . , Miss Grace Armytage Madame Somerville , ' " Mackney , " 
 Mr. H. C. Sanders , vocal department , Madille . Bertha 
 Brousil , violin , Madlle . Cecile Brousil , second violin piano , 
 Madame Antonette Grant Brousil , piano , Monsieur Alois Brousil , 
 violin tenor , Mr. Henry Nicholson , flute . , 
 violin solo Madlle . Bertha Brousil successful 

 CuicHEsTeR.—On Tuesday , 7th ult . , Morning 
 Concert Chamber Music Assembly Rooms , 
 Mr. Thorne , organist Cathedral ; assist 
 Messrs. Ries , Paque , Goodban , Miss Julia Elton . chief 
 point interest programme grand trio D 
 minor , Mendelssohn , Beethoven Sonata G@ , 
 Pianoforte Violin ( op , 30 ) perform MM . 
 Thorne Ries . M. Paque play Boccherini Sonata 
 violoncello , song effect . 
 room half fill , audience evidently appreci- 
 ate music perform 

 CigENCEsTER.—On Thursday , 16th ult . , 
 Cirencester Choral Society performance Haydn ( ' reation 
 Corn Hall . principal vocalist Miss C. Westbrook , 
 soprano , Mr. Green , tenor , Messrs. Brandon Ruperti , bass . 
 band select Bristol , Gloucester , Cheltenham , 
 Stroud ; Messrs. Chew Woodward leader . Mr. E. Brind 
 conduct . amateur belong Society lead 
 satisfactory way . chorus 




 


 XUM 


 GR 


 HA 


 


 2S 


 REN 


 XUM 


 81 


 INK practical ORGAN SCHOOL 


 e ’ 


 ESI 


 85 


 dure month , 


 S.A.T.B 


 } T YMN evening 


 ird collection HUNDRE D Andantino ; . 
 . 5 , Adagio ; . 6 

 ' edition . paper cover , 1 . , cloth , 
 MURLE WESTMINSTER ABBEY CHAN'I 
 BOOK , contain 189 single double chant , 
 follow author : — Alcock , Aldrich , Attwood , Ayrton , Bach , Bar- 
 row , Battishill , Beethoven , Bellamy , Bentinck , Blow , Boyce , 
 Brownsmith , Chard , Dr. Cooke , R. Cooke , Corfe , Coward , Croft , 
 Crotch , Davy , Dupuis , Dyce , Ebdon , Elvey , Farrant , Felton , Fitz- 
 herbert , Flintoft , Foster , Gibbons , Goodson , Goss , Greene , Gregory , 
 P. Hayes , W. Hayes , Henley , Hindle , Hine , Hopkins , Kelway , Kent , 
 King , Lamb , Langdon , Lawes , Lee , Lemon , Macfarren , Miller , 
 E. G. Monk , Morley , Mornington , Nares , Norris , Ouseley , H. Purcell , 
 KE . Pureell , P. Purcell , Pye , Rimbault , Robinson , Russell , Smith , 
 Soaper , Spohr , Tallis , Tucker , Turle , Turner , Turton , Walmisley , 
 Weldon , Whitfeld , Woodward . 
 London : Novello , Ewer Co 

 service 
 compose 




 J. BAPTISTE CALKIN 


 PROCESSIONAL HY MN , 


 XUM 


 OULE collection ' 415 chan't , 57 


 PSALTER , PROPER PS ALMS , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 order HOLY COMMUNION 


 ANGLICAN CHORAL SERVICE BOOK 


 USELEY MONK PSALTER 


 28 


 N EASY MORNING evening 


 MORNING SERVICE , 


 TONIC SOL - FA EDITION . 


 CONGREGATIONAL CHURCH SER 


 ORIGINAL edition 


 NEW edition 


 


 SA 


 YATE 


 SER- 


 EAUMONT , 


 TUNE 


 SHANTS 


 EV . R. HAKING anthem 


 ADVENT . EASTER . 


 LENT . trinity . 


 little child hymn 


 NEW cheap edition 


 complete work volume 


 qweet live amid mountain 


 CHORAL society singing class . 


 \ } ARCH man HARLECH . 


 EV 


 R. HAKING - song . 


 tf t voice 


 compose RICHARD KING 


 authorise edition RUGBY school , 


 SYSTEMATI 


 NOVELLO , EWER CO . 


 NEW CHEAP complete edition 


 MENDELSSOHN work 


 morning service 


 EVENING service . 


 music church service home circle 


 completion volume . publish Monthly Numbers , 2 . . number 1 60 , Volumes , handsomely bind 
 cloth , 36 . 

 , content volume . content Vol . ii . ( continue . ) 
 1 , Adagio Grand Fugue C minor * ee Mozart| 9 Tarchetto fr irst Sv , 0 
 double chorus , * * like unto thee ? " .. * Handel " — — Som Se — — ea , er oe 
 r > . 1orus , " lovely Messengers Mendelssohn 
 introduction creation « - Haydn Allegretto fi th artett ( op 59 ) Beethover 
 chorus , ' * trust " Dr. Croft Sener Sees San ee ( op 5 = 
 . ' Andante Sixth Quartett mi ve e Mozart ™ . oo " es } mp ne 7 amg 
 eee eee wee 2 " j " ii 
 rr " eng otett , esu dulcis memoria " ... eee ~ iihler 
 Ge crane Spell . Alans ej(ltemeewet ) " = Sei 
 ' nig = - . 34 , ' horus , Fat ner mercy ' oe eve ove ande 
 goal tage rom state te _ Motet tt ,   Splendente te , deus " « Mozart 
 - cee . ne cis Air Chorus , " Dal tuo stellato soglio ee Rossini 
 3 , aa ba sy eer C minor ... .. Beethoven | 35 , overture , " fall Babylon " ai aan Spohr 
 ser Cloth , Choral fugue Motett eee ese Spohr oe ore PS 
 P , gilt , 4 , Andante Quartett D minor eee ee Mozart | 9 g Gavotta , pomp pe ee maneat 
 ) ' Chorus , * * Kyrie Eleison " ... Schneider | " * os 
 , Concertante ( c major ) ... ow eos « - Handel 
 ] aot Chorus , * * round starry Throne " - » Handel Motett , " o salutaris Hostia " ; Auber 
 80F Chorus , " ' wretched Lovers " ve « + Handel | 37 , ' air Chorus , " holy ! holy ! " kae Spohr 
 ) 5 { 6 , Larghetto Symphony D oe « + » Beethoven : Chorus , " Giv H bas ‘ od 3 
 } va - 1orus , * thank unto God exe oes Spohr 
 ; Pe , ( op . ray Celestial Concerts " o ee Andante P. F. Bagatelles ' Beethoven 
 7 80 ] 1 ) " se te sulla Chorale , ' sleeper wake ! " " Mendelssohn 
 | Canzonetta Quartett . Mendelssohn Arioso , * * Lord mindful ' Mendelssohn 
 } double chorus , * * sing unto Lord " « e Handel } gg , fugue P. F. Works ‘ ii bs Bach 
 i. Aria s Sonata nie tie . — 39 . Romanza , g major ( op . 40 ) po ooo Beethoven 
 te aydn March ( posthumous Work ) ace « Mozart 
 8 , double Chorus , * Fix'd everlasting seat ie Handel Chorus , " rend sky ' , ... Handel 
 — fugue — — _ — cantata « . Reissiger|49 , March ( Scipio ) . » Handel 
 horus Motet eR ' és ae os Bach| ' " 1 - ' 
 9 , overture , ' judgment " ... pre ae Spohr . _ bow thank oi sie fp 
 March Pianoforte Duets ... ie eae Weber . . bi 
 19 , Andante QuartettinF ... os ons Haydn ntent volume iii 
 B minor chorus , * * heaven tell " ne sa Haydn : ool ps wanes sie " _ ; _ y 
 ' 11 , Wedding March .. 7 = Mendelssohn 41 . Adagio , Symphony C major , . 7 aes Haydn 
 Motett , * * deus tibi " " ne Mozart Choral Fugue , dona nobis pacem , " Mass Bminor Bach 
 12 . Andante Pianoforte Duet ... ' ..   Dussek| , , Choral Fugue , " Kyrie cy Mess tn @ majer . = 
 Adagio Finale Quartett Cc ! Spohr 42 , overture Oratorio " S Paul eee Mendelssonn 
 13 , air , " ask yon Damask Rose sweet " Handel Adagio , Pianoforte Duets e Weber 
 Choral Fugue , " Pignus Future " .. , " ne Mozart Yhoral Fugue , * * sicut locutus est , . Mag- 
 Air Chorus , " Non sdegnare " oe Gluck | nificatind ... : ove Bach 
 14 , chorus , ' Queen Summer " wd : Handel | 42 + Adagio , sy mphony e flat . . 4 . - » Haydn 
 Schiller — March os ce " Mey estier Allegro , Sonata Pianoforte 
 Sinfonia ( Semele ) pe esi ae .- Handel Violin oe " 9 Bach 
 15 , chorus , * hallelujah " ... ... Beethoven Finale second Sonata Pi anoforte Violin Baeh 
 Larghetto Ninth Quartett sie Mozart | 44 : chorus , * God thou art great , " Sacred Cantata Spohr 
 16 , Andantino Symphony , " power Larghetto 12th Grand Concerto . - Handel 
 sound " 2 Spohr Motett , * * Misericordias Domini " ... eee « . Mozart 
 Romanza Symphony " La Reine de } ejfranee ' " Haydn | £ 5 Andante , Symphony e flat a. a. Romberg 
 , military Overture ( c major ) ; Menislasshis Chorus , * soon tow'ring hope ( Joshua ) ... Handel 
 18 chorus , " die far soon " . Handel ] , , Andante , symphony e flat . no.2 . .. Haydn 
 Overture , * * Athalia ™ Bh Handel | 46 2 = Recordare Jesu pie , Requiem Mass ... Mozart 
 Motett , " Jehovah , Lord God Hos — yi Spohr Choral Fugue , " * et vitam venturi , " Mass Dmajor Righini 
 19 . chorus , " King Cesar " ... . Handel ] , , L4r8o , Fourth Quartett e flat ... Haydn 
 offertorium , * * Alma Virgo " aa = ... Hummel | 47 + " { deep . " 130th Psalm se oe Spohr 
 March , ( eg t pg ape Funeral March Pianoforte Sonata , Op . 3 35 ... Chopin 
 arch , ( Egmont ) ove ove ove » Beethoven s y 
 0 , March , ( Hercules ) ac . Handel Adagio Pianoforte Duet § onat b flat .... Mozart 
 Larghetto Clarionet Quinte tt ity . Mozart | 42 Minuet , overture ' Berenice eve .. Handel 
 Overture , Samson " ie : iv Handel ra — r — _ = 
 Minvet , Instrumental Concerto ... ‘ aa ach 
 content volume . 49 . Grand March ( op.40 ) ... pet F , Schubert 
 ° 1 , Andante Sonata ( op . 45 ) eee Mendelssohn Chi rus , " * ail pane Joshua " ose Handel 
 Funeral March ( Samson ) eve ae « Handel | 50 . ' Pastoral symphony Christmas Oratorio « .. Bach 
 _ dead — : lel - ats aie .. Handel Chorus , ' joyful , ye redeem , " sacred 
 horus , cria excelsis " ove eee Bach Cantata Festival St. John Baptist Bach 
 2 , % ovecture , oe pants op el ; eee « . Handel Adagio , sonata Violin Pianoforte 
 ndante , variation ( Septuor « ns Beethoven G minor ooo sas eee po oon Bach 
 nn % , March C major ( Idomeneo ) ... acs , Mozart ] 51 . ' Pater Noster . " Motett Chorusand Orchestra Cherubint 
 erman March Solennelle ( Op . 40 ) oe « . Schubert Sarabanda , second Violin Sonata ane Bach 
 = . oo vt ( Op . hall , eee ove Spohr Chorus ( Ground Bass ) , " Envy , eld bear 
 ouble Chorus , * * people shall hear " « » Handel Hell , " Oratorio Saul . Handel 
 Air , * * thou shalt bring ' aes Handel | 52 . allegro marziale , Pianoforte Duets ( op . 60 ) Weber 
 Fugue B major ( 48 prelude F ugue , ' . 47 ) Sach Bour ée Second Sonata Violinalone ... Bach 
 ‘ ' M : arch ( Joshua ) ove . « + Handel | 63 . Triumphal March ( Tarpeja ) oe Beethoven 
 t 28 . 25 . Coronation March ( Le Prophite ) — _ Meyerbeer Military March ( post humous Work ) Beethoven 
 Adagio P. F. Sonata ( Op . 2 oe Beethoven | 54 . prelude aud fugue F minor " Suites pour 
 dy Air , " arm , arm ye ! brave _ le Clavecin ™ eco .. Handel 
 ia cc horus , " come ' oun rceabsous ) J oe fandel Air , " honour arm , " ( Samson ) : « . Handel 
 - a. Chaconne ( d minor ) ove Bach Minuet Pianoforte Works ve 3eethe ven 
 41 , chorus , " thee let ev'ry soul subject " + » Handel ] 55 . Adagio Religioso ( Lobgesang ) ' Mendelssohn 
 Air Cc ' horus , * marv'lous work " eee Haydn Choral Fugue , ' * * let sing hallow praise 
 Andante trom P. F. Duets ( op . 2 ) ove Weber ( op . 96 ) ... ove Mendelssohn 
 Air , " * Cangio d’aspetto " . ee « . Handel Chorale , * o god'thou holy Lord " Bach 
 Andante P. F. Duets ( op . ‘ 60 ) ooo eco Weber | 56 . Adagio Fourth Symy J ‘ aa Seethoven 
 28 , chorus , " traitor desery " ene Handel Allegretto Scherzando ith Symphony Beethoven 
 - Finale , e major ( op . 52 vse seg Schumann | 57 . overture * * Esther ™ ane viete Handel 
 Pr'vices , Aagio Notturno ( op . 3 pan ve Spohr Andante fifth qui ntett . Mozart 
 % . overture , ' * Saul " eve oe eve Handel | 58 Adagio Grand Fugue C m ajor " ( 5th sonata 
 30 . air Chorus , " Placido ? 2 il Mar " wd Mozart Violin ) ont Bach 
 Andante Symphony ove Mende Issohn | 59 overture Ope era * Porus ” ans Handel 
 March Notturno ( op . 34 ) oe Spohr egretto Seventh Symphony : ethoven 
 31 , Chorus , " thou let man " ane pam Handel |60 Allegro Maestoso Serenade e ‘ flat , 
 Musette Sixth Grand Concerto , .. Handel wi ind Instruments . eo eee ane Mozart 
 Chorus , * * come , gentle Spring " ove ove Haydn Chorus , * let peace > joy " 
 Air , * sin , 0 King ° eg Handel Church Cantata Palm Sunday eve ' Bact 
 Mareh , F major ( Idomeneo ) ee ee Mozart ; 
 London : Novet.o , Ewer anp Co. , 87 , Regent Street , 69 , Dean Street , Soho , 35 , Poultry 

 XUM 




 favourite song 


 select 


 popular music ofthe olden time 


 


 long 


 J. OXENFORD 


 3allad compress , case new word write 


 1 


 5 . | 85 


 86 


 93 


 100 


 7 


 109 . 


 | 110 


 120 


 


 CHAPPELL 


 select 


 popular MUSIC OLDEN time 


 harmonize G. A. MACFARREN 


 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDON 


 WN , 


 


 XUM