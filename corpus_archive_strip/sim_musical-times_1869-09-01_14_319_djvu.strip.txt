


BMERASSERERRER EES EEE


HE MUSICAL TIMES


MUSIC IN THIS NUMBER. 


THE SPRING. 


QUTHWELL COLLEGIATE CHURCH.— 


NOVELLO’S


CATALOGUE OF 


ORGAN MUSIC


AND


SACRED MUSIC


WITH ENGLISH WORDS


NORWICH MUSICAL FESTIVAL


MENDELSSOHN’S HYMN OF PRAISE


NOVELLO’S


ORATORIOS


TO BE PERFORMED


WORCESTER MUSICAL FESTIVAL


NOVELLO’S 


OCTAVO EDITIONS 


OF THE


ORATORIOS


TO BE PERFORMED


OR 


PROFESSIONAL NOTICES


MADAME MONTSERRAT


RR


USIC SENT ON SALE OR RETURN, ON 


USIC ENGRAVED, PRINTED, AND PUB- 


UTLER’S MUSICAL  INSITRUMEN'I'S.— 


CORNETS, SAXHORNS, DRUMS, FLUTES, CLARIO- 


RASS, REED, STRING, AND DRUM AND 


HE PATENT PORTABLE METRONOME, 4 


PREG! RENEE


195


SALES OF MUSICAL PROPERTYADAMEPATEY will SING, at the NORWICH 
FESTIVAL, Mr. Gatty’s NEW SONG, “Oh, Fair Dove! 
Oh, Fond Dove

OVERTURE to FIDELIO (Beethoven). Arranged for Pedal 
Organs, by Jonn BisHor. Post free for 26 stamps

GEMS SELECTED from the WORKS of the GREAT MASTERS. 
Transcribed for the Pianoforte by Gzo. F. West. Both Sacred 
and Secular. In 72 books. 3s. each; free by post for 19 stamps. 
A catalogue of the same may be had gratis and postage free. Also, 
just issued, Fifth Edition of Mr. West’s ‘Questions on the Theory 
of Music.” Free by post fur 13 stamps. London: Published only 
by his Publishers, Messrs. Robert Cocks and Co., New Burlington- 
street




O MUSICAL SOCIETIES. — ABRAHAM'S 


REDUCED PRICES. 


MENDELSSOHN’S 


LIEDER OHNE WORTE


BOOKS 1 TO 8


ER SS EE CRETE TES


EDITED BY THE


REV. ROBERT CORBET SINGLETON, M.A


FIRST WARDEN OF ST. PETER’S COLLEGE, RADLEY, 


AND


ORGANIST AND CHOIRMASTER OF YORK MINSTER


ALWAYS IN TUNE. FOR ALL CLIMATES. 


THE BEST AND CHEAPEST DRAWING-ROOM AND CHAPEL ORGANS 


EVER OFFERED


THIRTY THOUSAND IN USE


SEVENTY-FIVE GOLD OR SILVER MEDALS


JABINET ORGANS


THE MASON & HAMLIN CA ) 


BREAVINCTON & SONS


SOLE AGENTS FOR AMERICAN ORGANS, 


309, REGENT STREET. 


XUM


IS


197


_—_


IR FREDERICK OUSELEY’S EIGHT AN- 


BLESSED IS HE WHUSE UNRIGHTEOUSNESS IS FOR- 


JUST PUBLISHED


MASS 


FOR VOICES AND ORGAN


WITH A SPECIAL GRADUALE. 


BY 


E. SILAS. 


METZLER AND CO.’S 


NEW PUBLICATIONS


66 99 


EXETER HALL. 


MAGAZINE OF SACRED MUSIC


NO, XX., VOL. 5, FOR SEPTEMBER, 1869. 


CONTENTS


UNDAY| EVENINGS AT 'THE HARMONIUM. 


SONGS AND DUETS BY


ANTON RUBINSTEIN


EW DRAWING-ROOM PIECES 


JUST PUBLISHED


EW DRAWING-ROOM PIECES 


198


N R. GEORGE VIGAY, BASS BARITONE


199


THE MUSICAL TIMES


SEPTEMBER 1, 1869


THE LONDON MUSICAL SEASON. 


YIUIM


200Handel's “ Jephtha” has proved how carefully and 
earnestly works new to the choir are prac ised; and 
the programme for next season will therefore be 
looked forward to with much interest, containing, as

it does, compositions of such importance as the 
‘¢ Passion Musik,” of Bach, and Beethoven's Mass ip 
D. Meanwhile we trust that the balance of voice 
in the choir will be carefully considered ; that the 
tenors will be strengthened, and that the ‘ diapagon 
normal” (which let us hope will be rigidly adhered 
to) will be more thoroughly under the control of the 
orchestra

Mr. Leslie’s series of Concerts contained this




201


_—_


THE STORY OF MOZART’S REQUIEM. 


HAD ANY PART AT ALL— . 


202


323


203


_—_—_


204


CRYSTAL PALACE


ACIS AND GALATEA


__


A GRA! 


205fusion, Preces, &c., were Tallis's; and the Psalmsofthe| Wy quote the following paragraph from the

day were sung to Chants by Beethoven, Goss and Pro- «Empire (Sydney paper), of’ the 16th yes “ Mr

fasor Oakeley. Croft's fine Ze Deum and Jubilate, in A,| Areren ANDERSON.—We are gratified to learn that this




SS


A FOUBR-PART SONG


+= 92


NS S 


—7._—— —————— 


RSE


BSL


SSS SSS SS SSS 


_2 


1 J 


= 7 — = SS . 


SS 


SESThe Harmonium Treasury. A Series of Select Pieces, 
Sacred and Secular ; arranged by J. W. Elliott

Yue Harmonium is now so thoroughly taking its place 
asa domestic instrument, that it becomes a matter of im- 
portance to search about for music which shall be perfectly 
within the grasp of amateurs, and sufficiently interesting 
to repay the performer for the time and attention neces- 
sary to be bestowed upon it. Here indeed is a volume 
which should be welcomed by all who love to linger over 
some of the choicest specimens of our great composers’ 
works; for extracts from the compositions of past and 
present writers have been so carefully and judiciously 
selected that the admirers of all styles of sacred music will 
be amply gratified. Mr. Elliott is already well known as 
an arranger for the Harmonium ; and a work like the one 
before us could not have been entrusted to more able 
hands, for he has the power of simplifying, without dis- 
torting, the composition he arranges ; and amateurs well 
acquainted with the beauties of the choruses of Handel 
and Mendelssohn, will find in the Harmonium Treasury 
many of them re-produced with very excellent effect. 
Amongst the contents of the volume may be mentioned 
a Fugue of Bach (from the celebrated 48), an “ Adagio 
Cantabile,” of Beethoven, several movements from the 
works of Handel, Mendelssohn, Mozart, Spohr and 
Haydn; the “ Benedictus” from Weber's Mass in G, the 
“ Kia Mater” and “Quando corpus” from Rossini’s 
“Stabat Mater.” and many other selections from the 
compositions of deceased writers; whilst amongst living 
composers whose works are extracted from, we may name

212

the deepest ferling ; and, although in these. as in most of

his works, we generally find that tinge of melancholy 
which formed part of his nature, they are so full of. lift 
and healthful vigour as to render them always acceptable 
to an audience. But Chopin’s compositions stand so 
thoronghly apart from the conventional music of the day, 
that mere plodding pianists can neither play them nor 
comprehend them: the phrases are so quaint and peculiar. 
and the various touches (although carefully indicated by 
the punctuation of the composer) require to be so tho- 
roughly under the command of the performer, that even 
his simplest Mazurkas demand earnest study before they 
can be rendered as the composer intended. ‘“ Passage 
players” (as Beethoven used contemptuously to designate 
mere mechanical executants) need not therefore trouble 
themselves with works requiring an intellectual percep- 
tion of phrasing which they have never cultivated ; but 
piani-ts who have learned to regard music as a lan 
guage in which to express poetical feeling, will find in 
the little volume before us a mine of treasure, all tie 
more welcome because so thoroughly unlike any with 
which they have previously become acquainted. The 
very best of his numerous Mazurkas are included in this 
selection—No. 5 (in G minor), 6 (in C major), 17 in C 
sharp minor), and 21 (in B major), being perhaps as 
genuine specimens of Chopin’s style as we could well 
point out to those not already conversant with these 
charming works

Rosert Cocks anp Co




XUM


TO CORRESPONDENTS


214Saispury.—The trial for a Bass Singer, in the room of 
Mr. Ingram, took place in the Cathed: al on Friday, the 28th ult., in 
the presence of the Dean, Precentor, Kev. Canun Gordon, and Mr. 
Richardson, the organist. There were six cendidates, selected 
from 22 applicants ; and, after a severe trial in solos, part-singipg, 
and singing at sight, Mr. Hilton was elected. This gentleman pos- 
sesses a very rich and powerful voice, of great compass. Mr. J. 
Corscroft, who stood second, was highly complimented

Scarporoveu.—The Fifth Meeting of the Amateur 
Orchestral Guild was held on the 4th and 5th ult., when two excel- 
lent Concerts were given in the Spa Saloon, for the benefit of two 
local charities, The very able performance of compositions which 
are severe tests even for professional orchestras, proved the high 
state of discipline already reached by this young Society, and the 
wisdom of its main rule, viz., not to admit in'o its ranks any but 
well advanced amateur executants. The following works formed 
the principal attraction inthe two programmes:—Symphony Jupiter, 
Mozart; Symphony, Power «f so.nd, spohr; Overtures. Sylvana, 
Weber; Ruins of Athens, Beethoven; Meeresstille, Mendelssohn ; 
Der Alchymist, Spohr; Merry Wives of Windsor, Nicolai; Naiads, 
Benneit; Zanetta, Auber. An Andan‘e and Rondo alla Tarantella, 
for violin and orchestra; and a ~olo de Concert, for clarionet, with 
orchestra, both by the direcior of the Guild, Dr. Haking

Sescompe, Cuesuire.—A Concert, in aid of the building 
fund of the New Mission House, took place on the 10th ult. The 
programme included Locke’s Music to Machth. and a miscel- 
laneous selection. The vocalists were amateurs, with the exception 
of Mr. Henry Haigh, of the English (pera Company, who made 
his last appearance previous to his departure for America. Mr. 
Joseph Skeaf presided at the pianoforte; and Mr. Couldwell con- 
ducted an efficient chorus




DURING THR LAST MONTH, HORNE, E. H.—Original Compositions for the 
Organ, No. 38, Overture, 2s. 
—— Do., Nos. 39 to 41, Fugue in F. Funeral March and March, 2s

EST, W. T.—Arrangements from the Scores of 
the Great Masters for the Organ. No. 75, price 23., contains :— 
Allegretto (4h Sonata, in E minor. Op. 70), Weber; Tema con 
Variazioni (Serenade for Violin and Violoncelio, Op. 8), Beethoven; 
Short Prelude and Fugue, in C major, J. L. Krebs. 
— Do., No. 76, contains :—Fugue in G minor from the Pianoforte 
Works, J. 8. Bach; and fantasia in F, originally composed 
for a Mechanical Organ

TAINER, JOHN.—Arrangements for the Organ, 
No. 3, price 28, contains:—Introduction and Allegro jrom 
Symphony in D, Haydu; and Andante moto from Piunoforte 
Sonata, Op. 122, Schubert 
ESTBROOK, W. J.—The Young Organist. A 
collection of pieces of moderate difficulty, transcribed for the 
Organ. Vol. 1, bound in whole cloth, price 10s. 6d. 
— The Young Organist, No 11. price ls. 6d., contains:—Adagio 
con expressione. Sonata, Op. 31 (Dussek), Gavotte, up. lt 
(Agnes Zimmermann). Anthem, ‘hou wilt keep him in 
perfect peace (3. S. Wesley). Diapason Movement (William 
Walond). And, Andante from a Sonata (Ignace Pleyel). 
—— Voluntaries for the Organ, No. 30, price 1s, 6d., contains:— 
Andante alla Marcia (Sonata, No. 2, Qp. 27), J. Woelfl; Adagio 
Symphony, Op. 12, L Pleyel; and March (from Op. 27), F. 
Schubert. ; - 
—— Do., No. 31, contains:—Andantino from a Quartett, Bach; 
and Allegro from a Sonata, Mozart




XUM


215


COLLBCTION OF 527 CHANTS, 57


T= PSALTER, PROPER PSALMS, HYMNS


J ULE’S DIRECTORIUM CHORI ANGLI- 


‘OULE’S DIRECTORIUM CHORI ANGLI- 


ORDER FOR THE HOLY COMMUNION


WHE ANGLICAN CHORAL SERVICE BOOK, 


USELEY AND MONK’S PSALTER AND 


A SHORT AND EASY MORNING SERVICE. 


ERIES OF MODERN KYRIES. HYMN 


PET THY MERCIFUL EARS, O LORD.” 


TO CHORAL SOCIETIES AND NATIONAL SCHOOLS. 


““\O WHEN THE MORNING SHINETH.” 


AUTHORISED EDITION FOR RUGBY SCHOOL


PLAYED BY MR. CHARLES HALLE


PREPARING FOR PUBLICATION


FOR THE


WITH VARIED ORGAN ACCOMPANIMENTS. 


EDITED BY 


AND 


TONIO SOL-FA EDITION. 


HAMILTON'S PATENT “UNION” MUSICAL NOTATION


CHARACTERS REPRESENTED. 


ACT IL


ACT II. 


TIME OF PERFORMANCE ONE HOUR AND TEN MINUTES


W. H. BIRCH, 104, LONDON STREET, READING


THE PATRIARCHS.” ‘ FAYRE PASTOREL


AN ORATORIO. A CANTATA. 


A CATHEDRAL “SING UNTO THE LORD


A FESTIVAL ANTHEM


“0 GIVE THANKS.” ZION


A PRIZE ANTHEM OF THE COLLEGE OF ORGANISTS, A FULL ANTHEM 


AN ANTHEM FOR TENOR SOLO AND CHORUS


BLESSED ARE THE 


A SECOND SET OF SIX IMPROMPTUS


FOR USE IN CHURCH AND HOME


BY


JOSEPH BARNBY


YE SERVANTS OF THE LORD 


AS NOW THE SUN'S DECLINING RAYS


ALL PRAISE TO THEE, MY GOD, THIS NIGHT. 


O JOYFUL SOUND! OG LORIOUS HOUR! 


ARISE, O LORD, AND SHINE. 


(HOLY, HOLY, HOLY, LORD GOD ALMIGHTY. 


8.\ LORD, THY GLORY FILLS THE HEAVEN. 


= THE DAY, O LORD, IS SPEN 


GREAT GOD, WHO, HID FROM MORTAL SIGHT. 


COME, MAGNIFY THE SAVIOUR'S LOVE, 


COME, GRACIOUS SPIRIT, HEAVENLY DOVE. 


WHEN SHADES OF NIGHT. 


SWEET IS THY MERCY, LORD


HIPS ALL THY MERCIES, 0 MY GOD


JESU, LOVER OF MY SOUL


BRIGHTLY GLEAMS OUR BANNER. 


PILGRIM, BEND THY FOOTSTEPS ON


O PARADISE! 


CHRIST THE LORD IS RISEN TO-DAY


24.0 COME, ALL YE FAITHFUL. 


25.1 HEARD THE VOICE OF JESUS SAY


26. WHEN I VIEW THE MOTHER HOLDING


27. {’TWAS IN THE WINTER COLD. 


28. THOU, WHOSE ALMIGHTY WORD


29. (THOU ART GONE TO THE GRAVE, 


30.~ SLEEP THY LAST SLEEP. 


31. (LORD OF OUR LIFE


32. { LEAD, KINDLY LIGHT 


33. LET OUR CHOIR NEW “ANTHEMS RAISE


35.0 HAPPY BAND OF PILGRIMS


37. (SAVIOUR ABIDE WITH US


38. WE MARCH, WE MARCH, TO VICTORY


ABIDE WITH ME


41, THE HARVEST-TIDE THANKSGIVING. 


42. { HOLY NIGHT! PEACEFUL NIGHT


ART THOU WEARY


THE DAY IS PAST AND OVE 


HEAR, O JESU, ISRAEL'S SHEPHERD, HEAR US


70 GOD THE LORD. 


51.{ LET US ALL IN CONCERT SING ALLELUIA


A SERIES OF SELECT PIECES


ARRANGED BY


J. W. ELLIOTTHaypy. Solo and Chorus. The marvellous work. (Creation) 
Mozarr. Adagio from the Fantasia in © minor. ‘ 
Exiotr, J. W. Andante Religioso

6. Novetto. O Bone Jesu. Trio for treble voices. 
MENDELSSOHN. Solo and Chorus. Sing of judgment. (Praige 
Jehovah). 
Macrarren, G. A. Love yourenemies. (Introit). 
7; 7 — Anthem. QO Lord God, Thou strength of my 
ealth. 
Macrarren, G. A. Offertoire. From the Introits. 
8. —. oem. Anthem. Stand up, and bless the Lord your 
0 
9. Menpetssouy. Trio. Lift thine eyes. (Elijah), 
NovetLo. Sancta Maria. Tenor solo, and quartett. 
10. Exxrort, J. W. Andante grazioso. 
Haypn. Adagio. 
}11, BEETHOVEN. Adagio Cantabile. 
Macrarren, G. A. O Saviour of the world. 
Holy, Holy, Holy. 
Scnum: ANN. Chorale. 
12. Goss, Jonn. Brother, thou art gone before us. 
MENDELSSOHN. Andante. (Op. 82.) 
18. Novetio. To Thee, O Lord. 
RanpeccrrR. A. A Wedding Hymn. 
Bacu. Fuga, from the 48 Preludes and Fugues. 
14. Catxin, J. B. Thou wilt keep him in perfect peace. 
MENDELSSOHN. Hearts feel that love Thee. 
Macrarren, G. A. Blessed are the pure in heart. 
15. Hrimmet Incline ad me. 
Bacu. Corale. 
MENDELSSOHN. Cast thy burden. 
Goss, Joun. O taste. and see. 
16. Gounop. Blessed is He. 
Spour. Forsake me not. 
Hauptmann. Larghetto. 
17. Strainer, Dr. Deliver me. O Lord. 
HANDEL. How excellent Thy Name. 
Hesse. Allegretto. 
18. Rossryrt. Quando corpus. 
Bierey. O Jesu mi. 
Goss, Jouy. O praise the Lord. 
19, NovetLo. Tantum ergo. 
MENDELssouN. O rest in the Lord

n




_0


LO


OCTAVO EDITION


OF


MEN DELSSOHN’S 


VOCAL WORKS


WHY RAGE FIERCELY THE HEATHEN


MY GOD, WHY, O WHY HAST THOU FOR


LORELEY... 20 


LONDON: NOVELLO, EWER AND CO., 1, BERNERS


HIRD EMITION. 


THE


WITH NEW APPENDIX


EDITED BY ROBERT BROWN-BORTHWICK


INCUMBENT OF GRANGE, /EESWICK


OPINIONS OF THE PRESS


STREET, (W.), AND 35, POULTRY, E.C


12


13


14


15


16


LCOrgan Arrangements. 
By GEORGE COOPER

VOLUME I. 
Introduction and Fugue, from an unpublished Sonata J. 8. Bach 
Adagio from the Otetto ose ooo .« Mendelssohn 
Adagio Cantabile, from a Sonata... « Haydn 
Adagio from the Quartett No. 3, Op. 29... L. Spohr 
Adagio aged from the Quintet No.12 G. Onslow 
Fugue... one ove .. Schwenke 
Adagio Cantabile. ose ove eco «. G. Onslow 
Andante from a Quintett oe = oes ewe: Andreas Romberg 
Fugue from the Temperaments tee ee Mendelssohn 
Adagio from a Sonata ove «. Beethoven 
Larghetto from the Pianoforte works «.» Hummel 
Chorus, ‘* May all the host of Heav'n” ... Handel 
Adagio from a Sonata ooo -- Beethoven 
Chorus, ‘‘Great and wonderful,” drown the

Last J udgment oe «. L. Spohr 
Larghetto from the “Vater | unser” .. F. H. Himmel 
Gloria from the MassinC ... ovo +. F. Schubert 
Andante from the “Sonata Ultima” +. Haydn 
Choral Fugue from the “ Aus tiefer Noth * Mendelssohn 
Adagio from a Sonata ‘* L’Invocation” ... Dussek 
Adagio Cantabile from Op. 50 cos ee Beethoven 
Poco Adagio from an Opera eo. Mozart 
Andante Cantabile from a Quintett... «. G. Onslow 
Fughetta Schwenke 
Andante from the Pianoforte’ works, ‘Op. At Mozart 
Amen Chorus from the “*Inclina Domine” Cherubini 
Fantasia con fuga from the Pianoforte works J. S. Bach 
Aria from an Opera... ase oon «. Mozart 
Fugue from Op. 35... «» Mendelssohn 
Der aber die Herzen, from the "“Motetts .. J.8. Bach 
I will sing of Thy great mercies, angie

St. Paul ase =e ooo ooo Mendelssohn

VOLUME II. 
Laudate Dominum _... oie +» Mozart 
Chorus, ‘‘All we like sheep,” Messiah +. Handel 
Agnus Dei, 6th Mass ... sie ee F. Morlacchi 
Chorus, ‘‘ O great is the depth, ° St. Peebes Mendelssohn 
Air “ He layeth the beams,” Ztius .. -. Handel 
Fantasia and Fugue w. A. W. Bach 
Chorus, ‘* The Nations are now the Lord’ 8

St. Paul one ook Mendelssohn 
Slow movement Sonata. Op. 10 oe Beethoven 
Movement from a Symphony ase Kalliwoda 
Larghetto First Double Quartett ... Spohr

17




21


22


24


26


27


28


30Sion) ove ose ees ooo +» Mendelssohn, 
VOLUME III

Adagio Cantabile from the Septuor +. Beethoven, 
Minuet and Trio froma Divertimento ... Mozart. 
Benedictus from the 2nd Mass oon «. Hummel. 
Preludeand Fugue... oo toe we A. W. Bach, 
Gloria from the Vocal Mass ... Spohr. 
Gute-nacht O Wiesen from the Six ‘Motetts Bach

Andante con moto, Op. 17... eve 
Fugue in double Counterpoint ‘by all the 
Intervals ove ase oe eee




CONTENTS OF VOLUME I00 
Manel’ Graduale, ‘‘Quod in Orbe,” arranged by George 
‘00

Anion Scherzoso quasi <a ey from Beethoven's fourth 
Quartett, arranged by W. T. Best

Slow movement from a Quartett, by Haydn, and Larghetto from 
a Sonata by M. Hauptmann, arranged by George Cooper

Andante from a Quintett by G. Onslow; Slow Movement from 
the fourth Quartett by Mozart; and Andante Cantabile from a 
Quintett by G. Onslow, arranged by George Cooper

Adagio from a Quartett by G. Onslow; “Salve Regina” br 
M. Hauptmann; and Adagio Cantabile from a Sonata by 
Beethoven, arranged by George Cooper

13

CONTENTS OF VOLUME II

Kyrie, Mass No.1; Adagio Cantabile from Quartett, Nu. 2 
and “ Gott deine Giite reicht so weit,” by Beethoven, arrange 
by W. Rea

Andante from Mozart's Quartett, No. 6; Adagio froma Cla‘! 
Concerto by Spohr, arranged by E. T. Chipp; and Moverwes 
from a Sonata by Beethoven, arranged by George Coupe

Adagio from a Quartett, Op. 1, by Mendelssuhn ; and Ani vais 
Maestoso by Spohr, arranged by J. Hiles

THE MUSICAL TIMES.—Serremsuer 1, 1869

naga 
VOLUME I. 
each: 1 Agnus Dei, Mass No. 1 Mozart 
re «| know that my Redeemer" "(Messiah) :. +. Handel 
‘co - o, from the Third Symphony | 
oo ~_ w= we“ Kozeluch = 18 
2 Cujus (Stabat Mater) dic Rossini 
Fac ut portem (Stabat Mater) owe +» Rossini 
Air with Variations... oa .« W. Jd. Westbrook | | 19 
3 Andante con Variazioni, from the Quartet | 
No. 4, Op. 7 Ignace Pleyel | 
Marcia Funebre, ‘Sonata, Op. 74 (4 hands) Dussek | 20 
7 Chorus, ** At last divine Cecilia came” | 
a (Alexander's Feast) <a a fe oy 
4 Adagio, Sonata 2, Op. 20 L. Kozeluch | 
Allegretto, from a Symphony in G (The 92 
Military) =... ave Haydn | 
5 anon from Trio for Two Oboes and English ' 93 
oo .. Beethoven 
Ro ny Wy a Symphony inG ove «+. Haydn 24 
. Fugue in D minor. Art of Fugue ... «- J. 8. Bach 
. 6 Andante grazioso in A, from a Trio eee Mozart 
Affettuoso, from a Concerto coe «. Dr. P. Hayes 
Minuetto, ditto oe co Dr. P. Hayes 
7 1 Fugue from Méhul's Joseph .. T. Adams 
Poco Adagio. Quartett. No. 5. Op. 9.... Haydn 25 
. 8 Beneath the Vine. - (Solomon) .. «. Handel 
Festival March... eve eve we W. Jd. Westbrook 
9 God of light. Cheres. “ Seasons) . Haydn 
Andante. Pianoforte Duets we Weber } 
Meine Lebenszeit verstreicht. Motett «. J. G. Schicht | 26 
10 Overture (Joseph) Mehul | 
Air, “* By the rushy-fringed bank” ‘(Comus) Lr. Arne } 
11 Military Symphony (1st movement) - Haydn 27 
12 Air, “‘ Rejoice greatly "| Messiah) ... «. Handel 
Fugue in DW minor. Artof Fugue ..  «. J. 38. Bach | 28 
VOLUME II, 
13 Airand Chorus “ Inflammatus” nest Mater) Rossini 
Bt The Quailcall. Grand Sonata, Op 56 ... F. Kalkbrenner 29 
4 14 Larghettocon un pocodi moto. Trio, = 65 Dussek 
Andante (Posthumous Quartett) ... Mozart 
Adagio ... - eve eco one «. dulius André 
15 Andante, Sonata, Op. 51 ‘a «. J. F. Kelz 30 
Larghetto, 5th Symphony... oun «. L. Spohr 
Chorus, Jealousy (Hercules) ... “i - Handel 31 
16 Introduction and Fugue (ilth Trio) - Dr. Boyce 
Chorus * By slow degrees,” (Belshazzar Handel

VOLUME I. 
1. Andante 
March, (Three Divertimentos (Op. 1) 
Theme varied ( 'p. 111) eve




VOLUME IIL


23


VOLUME II


222


SECULAR MUSIC (PRICE THREE-HALFPENCE EACH). 


SACRED MUSIC (PRICE THREE-HALFPENCE EACH). 4% Blessed is the people - ~ VY. Novello} 281 O Lord, how manifold are Thy wie Ger arrest J. Barnb: 
50 Blessed is he that considereth (8.8.4.23.) - Dr. Nares|3l4 O Lord my God - 8. 8. Wesley 
108 Blessed are the dead (S.A.7.B.B.) - - H.H. Pierson 276 {O Lord, my God Rev. C. Malan 
110 Blessed be he (s. solo & chorus, trebles & altos) Nenkomm|*‘” )Rend your heart, and not your garments J. Baptiste Calkin 
161 Blest are the —". ~ ~ Spohr O Lord, my God. - - Palestrina 
259 Blessing and glor: Dr. Boyce Come, Holy Ghost - Douland 
SBrightest and Sons (Sound the loud timbrel) Avison | 163 O Lord, our Governor (s. solo and choras) - Marcello 
(Teach me Thy way - ~ Croce 94 jO Lord, we trustalonein Thee - - Handel 
137 But the Lord is mindful of His own - ‘Mendelssohn (Laudate nomen Domini + - Dr. C. Tye 
198 By the waters of Babylon - - George B. Allen | 207 O Lord, Who hast taught us (Collect) - J. Marsh 
8 Call to remembrance - - - Farrant| 82 0 praise God in His holiness - ~ - J. Weldon 
92 Charity Anthem (for 3 trebles) - Dr. W. Boyce} 96 O praise the Lord ~ . + J. Weldon 
48 Charity, “ La Carita” (for 3 trebles) - G. Rossini | 168 0 praisethe Lord  - - J. Goss 
141 Christ being raised (Easter Anthem) - - §. Webbe | 264 O praise the Lord, all ye heathen - - Earl of Wilton 
194 Christ being raised (Easter Anthem) Dr. G. J. Elvey | 288 O pray for the peace of Jerusalem - - Dr. B. Rogers 
229 Christ is risen from the dead (Easter Anthem) Dr. G. J. Elvey | 301 O Risen Lord (Ascension) - J. Barnby 
169 Christ our Passover (Anthem for Easter) - . Goss | 296 O taste and see how gracious the Lord is is Arthur S. Sullivan 
54 Christmas Anthem - V. Novello | 251 Out of the deep - - - Mozart 
170 Come, Holy Ghost (s. or 7. solo and chorus) “Thomas Attwood | 257 Ponder my words, O Lord + Langdon Colborne 
935 Come unto Me, all ye that labour - J. Stafford Smith | 112 Praise the Lord, O Jerusalem - - - J. Seott 
61 Cry aloud and shout (for 5 velees) - - Dr. Croft| 46 Pray for the peace - - V. Novello 
ne of Jerusalem Dr. G. J. Elvey| 63 Praise the Lord, O my soul ( voices) - - Creyghton 
Two Hymns for Easter - J. Baptiste Calkin and J. Barnby | 209 Praise the Lord, O my soul - Dr. W. Child 
192 Deus misereatur Edward Mammatt}| 72 Praise thou the Lord (for soprano and alto) Mendelssehn 
269 Doth not wisdom cry? - R. Haking | 248 Protect us through the coming night (s.s.4.) Curschmann 
228 Drive far from us the mortal foe - - V. Novello 937 J Praised be the Lord dally - Thomas Ebdon 
89 Easter Hymn (solo, duet, trio, and quartett) V. Novello | “°" (Sun of my soul - Rev. H. L. Jenner 
58 Easter Anthem, The Lord is my strength - V. Novello 297 Rejoice, O ye people (Christmas) - Mendelssohn 
217 Enter not into judgment - Thomas Attwood {Hark, the hera'd ang: ls sing - ~ Mendelssohn 
233 Envy! eldest born of hell! (Sau) + Handel| 92 Remember, O Lord (3 trebles) - Dr. W. Boyce 
6 Forgive, blest shade - Dr. Calleott | 219 Responses to the Commandments w. T. Best & Mendelssohn 
992 { For these and all Thy mercies given - J. Lancaster | 44 See what love hath the Father - - Mendelssohn

Kyrie tleison (Nos. 1 and 2) - Mendelssohn and Weber 177 See, the morning star - - Dr. E. G. Monk 
36 Four Hymns for Christmas - Various Composers | ~‘‘ (Hark! the herald angels sing - Dr. Ions 
165 Glory to God in the highest - - - _Pergolesi | yg9 {Xing the battle sharp and glorious ” Dr. E. G. Monk 
105 Glory be to God on high - - V. Novello Jesus Christ is risen to-d: vo - Dr. Ions 
66 God, my king - . - So Bach} 54 Sing unto the Lord (Christmas Anthem) - V. Novello 
31 God save the Queen - - - V. Novello} 70 Sleepers, wake ; To God on High; To Thee, O Lord Mendelssohn 
85 Grant, O Lord (Collect) ~ - - Mozart 273 There were w hisp’ rings (Christmas Carol) . Cooper 
204 Great and marvellous are Thy works ~ Dr. Boyce | ““” {Shades of silent night dividing (Christmas Cap Samuel Gee 
202 Hallelujah! For unto us a Child is born W.H. Monk} 4 Thou art gone to the grave - Beethoven 
227 Hallelujah! Hallelujah! . V. Novello} 10 Teach me,O Lord - - - Dr. Rogers 
186 Hear, holy Power : Prayer, Masaniello) 8.8.7.7.B Auber 221 $ {Teach me, O Lord Thomas ‘aaieead 
2 Hear my prayer, O Lord - Winter 10 God, who in Thy heavy’ nly hand — - - Handel 
213 Hear the voice and prayer - J. L. Hopkins | 139 Te Deum laudamus - - J. T. Cooper 
4 Hear what God the Lord V. Novello | 143 & 144 Te Deum, in F Ww. Jackson (of Exeter) 
82&33 Hear my prayer, O God (duet and chorus) Kent} 23 Thine, O Lord, is the greatness - Kent 
48 Hymnus £ucharisticus B. Rogers| 46 There is a river - - V. Novello 
76&77 Have mercy, O Lord @. solo and chorus) ~ Mozart | 262 The Grace of God (for Ch rietmas - J. Barnby 
88 He comes, ordained of yore Jackson | 806 The Harvest-tide Thanksgiving J. Barnby 
157 Here shall soft Charity repair (a.7.B.B.) - Dr. Boyce} 58 The Lord is my strength (Easter Anthem) - V. Novello 
74 Holiest, breathe an evening blessing - W. Shore | 205 The Lord is my strength (for Easier) - W. H. Menk 
240 Holy, Holy, Holy, Lord God Almighty John Bishop} 59 The Lord descended - - - P. Hayes 
107 jHow beautiful upon the mountains - R. A. Smith | 45) {The Lord is King - - Pittman 
(The Lord loveth ~ - - V. Novello | ““* 1A Grace (Give th nanks to G od) - - V. Novello

224 How dear are Thy counsels - Dr. Croteh | 318 The Lord is my Shepherd - - G. A. Macfarren 
247 How goodly are Thy tents = - - Ouseley | 261 The Night is far spent (for Advent) - Montem Smith 
195 If ye love Me, keep My comeateets - W. H. Monk | 249 Thou knowest. Lord, the secrets of our hearts Purceil 
231 If ye love Me, keep My com mandments - Tallis |} 131 Thou visitest the earth (7. solo and chorus) - Greene 
120 I know that the Lord is great - - Ouseley | 123 To Thee, Great Lord (s. solo and chorus) - Rossini 
304 In humble faith, and holy love - Dr. Garreit | 155 Turn Thy face from my sins - - T. Attwood 
238 In the beginning (for Christmas) - - G. B. Allen | 100 Unto Thee, O Lord - + - Charles Kivy 
25 In Judah God is known (St. Paul) - Mendelssohn | #08 Veni Crestor Spirites . . Tallis 
116 Incline Thine ear to me (&. solo xnd chorus) - Himmel} 19 Vital spark Harmonized by Novello 
In Jewry is God known (for 5 voices) Dr. J. Clarke | 316 We march to victory ‘and The day ispastandover J. Barnby




THE ORGANIST’S PORTFOLIO


CHAPPELL'S VOCAL LIBRARY


PART SONGS


TO BE CONTINUED


CHAPPELL’S MUSICAL MAGAZINE


OF VOCAL AND PIANOFORTE MUSIC73 Eighteen Christy Minstrel Songs arranged as Vocal Duets. | 33 The Juvenile Vocal Album

72 Fra Diavolo for the Pianoforte. 32 Beethoven's Sonatas, Edited by Charles Hallé (No. 6

71 Nine Sets of Quadrilles, Belgravia Waltz, &c., by Dan. Godfrey. | 31 Beethoven's Sonatas. Edited by Charles Hallé (No. 5

70 Ten Songs, by Gounod. | 50 Beethoven’s Sonatas. Edited by Charles Hallé (No. 4

69 The Bohemian Girl, the Opera for the Pianoforte. | 29 Ten Contralto Songs, by Mrs. R. Arkwright, &c

68 Sixteen Operatic Songs by Verdi, Flotow, &c. | 28 Beethoven’s Sonatas. Edited by Charles Hallé (No. 3

67 La Belle Héléne, the Opera for the Pianoforte. | 27 Five Sets of Quadrilies as Duets, by Charles d’Albert, &c

61 L’Africaine, the Opera for the Pianoforte. 21 Nine Pianoforte Pieces, by Ascher and Goria

66 Twenty-five Old English Ditties. 20 Beethoven's Sonatas. Edited by Charles Hallé (No. 2

59 Faust, the Opera for the Pianoforte. 19 Favourite Airs from the Messiah, for the Pianoforie

54 Chappell's Popular Church Services. (Third Selection). 13 Twelve Popular Duets for Sopran» and Contralt» Voices

53 Chappell’s Popular Church Services. (Second Selection). | 12 Beethoven's Sonatas, Edited by Charles Halié, No. 1

62 Chappell’s Popular Church Services. (First Selection). | 11 Six Pianoforte Pieces, by Wallace




4LL THE SONGS HAVE PIANOFORTE ACCOMPANIMENTS


CHAPPELL & CO., 50, NEW BOND-STREET. LONDON