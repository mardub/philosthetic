


 MUSICAL time 


 NOVEMBER 1 , 1868 


 NEW song JOSEPH BARNBY 


 appendix HYMNS ancient 


 NEW CHRISTMAS motett GOUNOD 


 PROFESSIONAL notice 


 MR . WILBYE COOPER | 


 MR . J. C. BEUTHIN , 


 USIC send sale return , 


 N USIC engrave , print , pub- 


 lishe good style , moderate 


 rass , reed , string , drum 


 important notice musical professur 


 ) ther 


 EE ASHWORTH new PATENT loop 


 KYOND ALLCOMPE'TTLION.—T. K. WLLLIS 


 UTLER MUSICAL instrument . — 


 CORNETS , SAXHORNS , DRUMS , FLUTES , CLARIO . 


 NV USICAL LNSITKUMEN'IS FUR VOLUN . 


 EASANT HARMONIUM manufactory 


 W. SNELL improve HARMO 


 prize violinist 


 ROGER celebrate GUINEA CON 


 563 


 ready 


 edit 


 


 publish 


 


 PIANOFORTE 


 


 MENDELSSOHN ELIJAH , 


 EXETER HALL 


 MAGAZINE SACRED MUSIC 


 . x. vol , 3 , NOVEMBER 


 content . 


 37 , GREAT MARLBOROUGH STREET , LONDON , W. 


 G. ROUTLEDGE son , 


 BROADWAY , LUDGATE HILL 


 LONDON : 


 NEW ANTHEM , CH . GUUNOD . 


 J. L. HATTON . 


 LONDON 


 LONDON 


 thirty - original chant " CHOIR , " 


 NEW WORK ORGAN 


 content 


 exercisr interval 


 , include example 


 


 LONDON : NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 ALS , 


 EDAL 


 D 


 authorise edition RUGBY SCHOOL . 


 element MUSIC SYSTEMATI- 


 voluntary ORGAN . 


 ORGANISTS ’ quarterly journal , 


 E. H. THORNE 


 original composition ORGAN 


 * nocturne PIANOFORTE . 33 . 


 press . 


 44 ¢ " 


 ( 96th PSALM ) . 


 567 


 MUSICAL TIMES , 


 NOVEMBER 1 , 1868 


 musical ignorance . , recur cireum- 
 stance fully convinced 
 legislator , happen present 
 cl ssical concert , form portion andience 
 cite press represent enlightened 
 music - lover sit composition 
 quarter hour long utmost pleasure 
 delight . " delicious movement " 
 Quartets , instance , tell " 
 hear pin drop ; " end 
 burst applause . assuredly : certain 
 knowledge , beautiful 

 ment , try sleep somewhat 
 overbalance try awake ; 
 quiet , 
 profound silence ; burst joy cessation 
 sound , , course equally share 
 party . record 
 silence observe performance ; silence 
 attention ; attention 
 necessarily indicate comprehension . ' enjoy 
 high class music , absolutely necessary 
 knowledge subject previously 
 exist mind listener ; , con- 
 firmation view , feel convinced 
 - fourth audience orchestral concert 
 utterly ignorant meaning word 
 use denote kind composition per- 
 form . ' + dear fellow , " hear victim 
 , induce family toa 
 Philharmonic Concert , ' tell shall hear 
 Beethoven Symphony C minor ; 
 enjoy know Symphony 
 , C minor mean ? " , 
 patient martyr sink sofa - stall state 
 proper drowsy endurance , shall probably ' 
 inform sanguine believer universal in- 
 telligence audience , delirium 
 enjoyment beauty music . , 
 true mass public 
 highly educated art , " ballad 
 concert , " term , crowd ; 
 " royalty " song ( performance 
 entertainment expressly institute ) , 
 sell fast print ? 
 capable comprehend enjoy 
 great composition equally enjoy small ? 
 Beethoven , Claribel , Mozart , intro- 
 duce drawing - room , receive 
 equal favour ? believe . truth , 
 patronise small , advance 
 comprehension large ; work ona 
 level " royalty " ballad produce 
 art , hesitation 
 universally receive educated class 
 apathy contempt 

 let ask , , opera - house 
 mainly support devotee Art fashion ? 
 true real music - lover find 
 house representation 
 sterling work ; bulk audience com- 
 posed mere idler , ' * Opera " ' 
 evening , Rotten Row morning , 
 tosee . good society , necessary 
 rent opera - box rent hunting - box ; 
 right place right season , 
 proof knowledge requirement 
 station Providence place 
 . Art occupy real place 
 affection , exception rule ; 
 gently tolerate friend 
 fanatic , eccentricity , inconvenient , 
 perfectly harmless . , , admit 
 fact , lyrical establishment England 
 open single season 
 patronise seek music 
 sake ; proof find fine work 
 usually non - subseription night , per- 
 haps short series performance 
 reduce price , conclusion regular 
 season . ' stand fusty old Mozart " ( 




 565 


 569 


 ROYAL ITALIAN OPERA 


 CRYSTAL PALACE 


 AGRICULTURAL HALLTue concert establishment crowd 
 past month . classical night 
 |sparingly introduce , Messiah , Elijah , 
 | Creation , & c. , delight , expense 
 | . rule , , programme 
 | strangely mixed — contain music good school , 
 |of bad school , school ail — tounde , 
 presume , experience attractive 
 general audience , willing admit , 
 monetary success important result 
 Concerts , concession popular taste neces- 
 sary ; regret old Jullien time revive , 
 believe ( believe ) taste 
 class entertainment pass away . 
 Quadrille , " Fall Magdala , " " military 
 effect " tremble think 
 step direction ; especially martial spirit 
 appeal admit volunteer uniform 
 half price . " Quadrille , " descriptive 
 celebrate Riot Hyde Park , prepare , grand 
 " police effect , " admit , course , member 
 ' ¢force " reduce rate ? " Magdala " Quadrille 
 dedicate Lord Napier . " Hyde Park " 
 Quadrille dedicate Sir Richard Mayne 

 concert Lower Hall , 
 Exeter Hall , Monday evening , 26th ult .. 
 Choir St. Michael Church , Burleigh Street , assist 
 Miss Poole , Miss Marie Leaford , Miss Kate Bartlett , Miss 
 Emily Thomson ( piano ) , Mr. Wilbye Cooper , Mr. H. 
 Dimmock Hill ( clarionet ) , Miss Poole sing , ' ' speak , 
 thy servant heareth " |W. H. Weiss ) , ' ' soul 
 dark " ( T. Davenport Chatterton ) , accom- 
 panie harp Mr. J. Balsir Chatterton . Miss 
 Marie Leaford " angel , bright fair , " 
 song West , " Sweet Bird . " Miss Kate Bartlett , 
 suffer nervousness , sing song allot 
 — * come unto " ( Messiah ) , ' ' Penelope 
 task,”—with considerable effect . Mr. Wilbye Cooper 
 « heart " ( Mendelssohn ) , " long 
 wave come " ( Gabriel ) , usual ability . 
 Miss Emily Thomson play Beethoven Sonata 
 flat , Funeral March . Choir perform 
 chorus - song creditably . Mr. J. Turle 
 Lee accompany , Mr. Edward Craig conduct . 
 Concert aid Church Organ Fund 

 Friday , 9th ult . , remain 
 Rev. Ernest Hawkins inter cloister 
 Westminster Abbey , choir , able 
 direction Mr. Turle , sing appoint portion 
 service ; Goss anthem , " Brother , thou art 
 , " Dean Milman word , perform 




 570 


 571 


 xumthe key song , dominant 
 relative minor ; presume object 
 Mr. Calkin announce emphatically subject 
 early portion Oratorio , word 
 recitative judiciously note . 
 transcription chorus remarkably good ; instru- 
 mental choral effect preserve . 
 agree Mr Calkin C major , 
 begin , * ' mercy thousand fall , " usually 

 hymn , psalm , sacred cantata , 
 Mendelssohn evidence possession sublime | 
 musical genius , perfection art training , 
 associate grand 
 composer , Bach , Handel , Beethoven . 
 ( great ) work Mendelssohn , 
 Lobgesang , Lauda Sion , hear prayer , 

 somewhat fast ; reason , re- 
 gret conductor blindly follow 




 574 


 anthem voice 


 y 


 pp 


 ss 


 ff — ~~ 4 = 


 ss = 


 < — _ — = _ — — _ = 7 , — — - — ~ _ . | 


 { = SS FSS est 


 t 4 4 — — — 


 6 = S325 5S : = 


 1 $ $ — 5 OI 


 TU 


 AUP 


 EDITOR MUSICAL TIMES 


 CORRESPONDENTSAsurorpD , Kernt.—The member 
 Ashford Musical Societies , wish testify ap- 
 preciation Mr. Fuller service behalf , 
 Concert benefit Corn Exchange , 
 ist ult . Mr. Alderton Mr. Lewns , Hastings , 
 kindly service occasion . Mr. Fuller 
 conduct ; Miss Fuller Mr. Alderton 
 accompanist . Concert prove entirely successful ; 
 Corn Exchange crowd , music 
 favourably receive audience 

 Beprorp.—A highly successful Concert 
 Assembly Rooms , past month , 
 Mr. Alfred Howard . excellent little 
 orchestra , lead Mr. Toll ( Stony Stratfird ) , 
 overture effectively perform . Mr. Howard 
 pianoforte playing praise account 
 Concert receive ; piece set 
 programme ' moonlight Sonata , " 
 " Finale " ( Beethoven ) , ( com- 
 position ) . Madame Talbot Cherer , Miss Sirett , Mr. 
 A. Howard , Mr. D. Young , principal 
 vocalist ; effort appear utmost 
 satisfaction audience . accompanist 
 Mr. A. Howard , Master C. Howard , Mr. Bandey 

 BrrKENHEAD.—On Thursday evening , 15th 
 ult . , Miss Galloway ( soprano ) , pupil Madame Ruders- 
 dorff , . annual Concert Music Hall . 
 principal artist Madame Rudersdorff , Madlle , 
 Drasdil , Mr. W. H. Cummings , Mr. Orlando Christian 

 Boston , U. S.—On Wednesday , 23rd 
 September , Mr. F. H. Torrington , Montreal , 
 successful Organ Recital , Boston Music Hall . 
 programme excellently select display 
 power executant instrument ; 
 Mr. Torrington performance ( especially selection 
 chorus Jsrael Egypt , Mendelssohn 
 Sonata , . 3 ) , thoroughly appreciate , warmly 
 applaud 

 Brienton.—A interesting Pianoforte 
 Recital New Concert Hall , 30th 
 September , Mr. E. H. Thorne ( organist Chichester 
 Cathedral ) , large appreciative audience , 
 Recital commence Weber sonata d minor 
 ( Op . 49 ) , excellently play . 
 Professor Bennett " Kondo Piacevole , " Schumann 
 " Chant du Soir , " piece , 
 composer , Mr. ' Thorne good opportunity prove 
 power render composition varied 
 style ; success complete . 
 Herr Louis Ries , violinist , receive 
 utmost favour " Kreutzer " Sonata Beethoven ; 
 Thalberg De Beriot duet subject 
 Les Huguenots , join 
 Mr. Thorne . vocalist Miss Ida Thorne 
 ( sister concert - giver ) , sing solo 
 effect , receive encore 

 BromiEy , Kent.—On Tuesday evening , 
 14th ult . , Concert Town Hall , 
 member Bromley Institute Choral Society . 
 programme include Dr. Sterndale Bennett 
 Queen , Van Bree Cantata , St. Cecilia Day , 
 solo , & c. , sing Miss Fanny Hollands 
 ( soprano ) , Miss Mary ( contralto ) , Mr. John 
 Croft ( tenor ) , Mr. John Hodges ( bass ) . special 
 mention rendering , Miss Hollands 
 Miss , recitative , " Fiero incontro , " 
 duet " Lasciami ! non t’ascolto , " Jl Tancredi , 
 singing choir , particularly Sullivan part- 
 song , ' oh , hush thee , babie , " excellent ; re- 
 flecte great credit energy skill 
 conductor , Mr. Walter , R.A.M. , organist 
 Bromley Church . Mr. highly successful 
 performance Weber " Concertstiick " 
 pianoforte , accompany orchestra , com- 
 pose member Crystal Palace Band , assist 
 gentlemen amateur . Mr. Oscar Beringer ably preside 
 pianoforte ; Concert respect 
 thoroughly satisfactory . room fill 

 CurnTennam.—Mr . Brinley Richards ’ second 
 Pianoforte Recital , Montpelier Rotunda , attract 
 numerous brilliant audience . 
 taine Sonata Beethoven , gem 

 work classical master . perfor 

 Miss Annie Rannie é : 
 | Hasrrnes.—A highly successful pianoforte 

 accomplished amateur , visitor town ) ren-|   ! , y 
 dere excellent taste " Love Request , " " Bid | recital Music Hall , Madame Arabella 
 discourse , " respond encore case ; and| Goddard . Thursday , 8th ult . programme 
 Miss Tulloch ( pupil Arabella Goddard ) play | contain Mozart Sonata b flat , . 5 , selection 
 Woelffl " Ne Plus Ultra , " good execution . Mr.|from 8th book Mendelssohn Lieder ohne Worte , 
 Tucker Mr. Hardwicke vocal solo Beethoven « Sonata Pastorale , " 
 effect . | perfection execution sympathetic 

 feel fur composition long 
 Curtony Hamppren , Oxon.—The Annual know characteristic Madame Goddard 

 Ratcliff , sing piece effect . ' 
 ; Hoxpart Town , Tasmanta.—Mendelssohn 
 Cramtineton.—The new organ , build 

 amlington Church , exhibit Messrs. Nicholson 
 Son manufactory , 26th September ; 
 atge number visitor present occasion . 
 organ consist manual pedal organ , 
 15 stop . Mr. John Nicholson , organist 
 Hexham Abbey Church , honorary organist 
 eweastle Choral Union , preside instrument , 
 pay following piece : — selection , Creation , 
 aydn ; Andante , Symphony " . 1 , " Beethoven ; Cujus 
 imam , Stabat Mater , Rossini ; Concerto , " . 2 , " 
 andel ; Andante , " sonata C , " Mozart ; Chorus , " Fix’d 
 everlasting seat , " Handel . organ 
 od Mr. Leggatt , organist St. Mary Roman 
 olic Cathedral , able manipulation 
 " ee resource instrument fully bring 
 » organ voice system successfully 
 opt Herr Schultz Germany , England 
 " " ewis ; effect brilliant 
 Pression large instrument . tone 

 stemarkably fine ; diapason broad 
 




 583 


 584 


 dure month 


 564est , W. t.—arrangement score 
 great Masfers Organ 

 . 64 . price 2 . , contain , Beethoven grand March D , Op . 45 . 
 Mozart Andante , sonata C ( hand 

 . 65 , price 2s , contain Haydn Andante ( clock movement ) 
 fourth symphony D , Handel Air Chorus , " Almighty 
 Ruler sky , ’' Joshua 




 NOTES tear 


 EORGE CALKIN PATENT key - board 


 ARMONIUMS.—ENGLISH harmounium , 


 MASTER . 


 song . 


 586 


 ENRY SMART sacred vocal 


 1867 


 song . 


 BEIN 


 


 LOW 


 IOUR . 


 587 


 _ — _ 


 NOVELLO 


 collection favorite glee 


 english composer 


 


 accompaniment ( AD LIB . ) PIANOFORTE 


 revise 


 J. BARNBY 


 glee penny 


 


 music compose 


 TONIC SOL - FA edition . 


 CONGREGATIONAL CHURCH SER- 


 tune chorale 


 singe sight . 


 HAMILTON patent " union " musical notation 


 EV . R. HAKING - song . 


 ARCH ' MEN HAKLECH — 


 EE leave FALLING 


 t 


 L TUNES , 


 ist : 4 


 ave 


 jotation 


 . HAMILTON 


 song 


 R LECH 


 LLING . 


 2 559 


 MAGDALA 


 589 


 _ — _ — _ 


 ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 OULE collection 415 chant , 57 


 t= PSALTER , PROPER psalm , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 order HOLY communion 


 OULE 


 N easy morning evening 


 EV . RK . HAKING anthem 


 DWARD HERBERT CHANT te DEUM , 


 ( ) pratse LORD , YE nation ; 


 choral festival . 


 simple form intoned SEKVICE.—THE 


 music coming season 


 GAUNTLETT , H. J. THOU WILT perfect 


 GOUNOD , CHARLES . bless 1 COMETH 


 LUIHER HYMN . great GOD , . 


 MENDELSSOHN . love .. y messenger . | 


 REDFORD . REJUICE LORD 


 WISE . AWAKE , AWAKE , thy strength , O 


 HARRIS , EDMUND . HARK ! HERALD angel SING . 


 MENDELSSOHN . HARK ! HEKALD angel sing . 


 COOPER , J. T. whisp’ring 


 HANDLEY , EDWARD . carol . 94 . 


 ion . THOMAS 


 boar head CAROL , 


 BARNBY , J. GRACE GOD BRINGETH 


 BENNET T , A. ‘ 0 zion bringest good tiding . 


 watch 


 8.S.A.T T.B.B. 


 ALLE 


 SHEPHERDS 


 BES ! , W. T 


 CHAWSER C. F , F. ‘ man carry STEPHEN 


 CLARKE , J. HAMILTON . shepherd 


 CROCE , BEHOLD , bring glad tiding 


 S.A.T.B 


 GOSS , J. BEHOLD , bring guod tiding 


 591 


 HATTON , J. L. bless LORD GOD ISRAEL . 


 HOPKINS , E.J. let G9 UNT BETHLE- 


 LESLIE , HENRY . fear , bring GOOD 


 MONK W.H. HALLELUJAH ! unto child 


 PERGOLESI . glory GOD high 


 PURCELL . BEHOLD , bring glad tiding 


 WEBBE , SAMUEL . 


 ILLEARD choral advent HYMN . 


 NEW anthem CHRISTMAS . 


 


 unto child bear . 


 fu . ness time 


 content . 


 CHRISTMAS CAROLS . 


 offer dure NOVEMBER . 


 AMATEUR CHOIRS . 


 CHORAL society 


 CHAPPELL VOCAL LIBRARY - song 


 new - song 


 HENRY SMART 


 CHAPPELL cheap work 


 instrument . 


 tutor . 


 VIOLIN 


 flute . 


 CORNET - - PISTONS . 


 ENGLISH CONCERTINA . 


 GERMAN CONCERTINA 


 HARMONIUM 


 GUITAR 


 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDON 


 1 ) >