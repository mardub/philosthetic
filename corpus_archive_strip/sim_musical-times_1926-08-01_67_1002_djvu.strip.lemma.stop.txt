


 MUSICAL TIMES 


 singing - class circular 


 found 


 publish 


 AUGUST 


 ROYAL CHORAL SOCIETY . 


 ROYAL ACADEMY MUSIC . 


 YORK GATE , MARYLEBONE ROAD , LONDON , N.W.1 


 vacation COURSE teacher 


 interpretation appreciation 


 MICHAELMAS TERM 


 TEACHER TRAINING COURSE , 


 J. A. CREIGHTON , 


 A.M 


 ROYAL COLLEGE MUSIC 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. ? , 


 MAJESTY KING 


 MAJESTY QUEEN . 


 LONDON SOCIETY organist . 


 found 1913 


 1844 


 month 


 1926 


 VICTORIA EMBANKMENT , E.C.4 . 


 ASSOCIATED BOARD 


 R.A.M. R, . C.M. 


 local examination MUSIC 


 MUSICAL TI 


 96 & 95 , WIMPOLE STREET , w.1 


 TOBIAS MATTHAY 


 PIANOFORTE SCHOOL 


 complete training course teacher . 


 ROYAL 


 MANCHESTER COLLEGE MUSIC 


 GLASGOW 


 ATHENAUM SCHOOL MUSIC 


 session 1926 - 27 


 MANCHESTER SCHOOL MUSIC . 


 16 , ALBERT SQUARE 


 LONDON SCHOOL SINGING . 


 HOLIDAY lesson 


 consultation pianist 


 help need 


 | PIANOFORTE playing 


 MR . GILLETT remain LONDON dure 


 BIRMINGHAM & MIDLAND INSTITUTE 


 SCHOOL MUSIC 


 | session 1925 - 1926 . 


 NATIONAL ASSOCIATION COMPETITIVE 


 choir . 


 president 


 1,100 specimen working 


 recent success , 


 AUGUST 1926wit teen 8 : 8 | tec hnical interpretative mastery complete 

 Beethoven : 4 
 jeal j ' | impress such"musician Liszt 

 , , playec organ great deal yout | von Biilow " 7 IGsat ' write 

 instrument virtuoso . |playe . _ probably ft icouldn’t , 

 organ Beethoven day clumsy | Best rival banc . 7 — — 
 aflair , vivid recollection | Schneider , Dresden ; ' ho | pee 

 early struggle example the| Statham , capital judge hear Best 

 conversation , end life 

 deserve handsome tribute pay | 
 Beethoven . - day , organ 
 rich resource far easy handle , 
 - rate recitalist probably use mental 
 physica ! energy performer , 
 additional strain adapt 
 fresh set condition ( tonal scheme , touch , 
 acoustic , stop - position , & c. ) instrument 
 play . notorious fact , 
 fine performance , meet far } . . . merely dry fact appoint- 
 recognition pianist violinist | ments , slight attempt real 
 ual , inferior , quality . * estimate extraordinary unequalled 
 " ; wre : power executant master organ effect . 
 reason , centenary birth 
 great organist age hand , Thalberg—'a mere piano 
 country special notice , way | yirtuoso’—was accord far long 
 protest lack appreciation ) article : 
 — = concert - player . , . Best fully equal Thalberg 
 biographical fact concern Best } 1 instrument , 
 easily accessible need - round musician , knowledge 
 article . use and| _ art . 
 interest glance achievement 

 tibia consider Best attainment player , 
 Personamny . " pass , grant , keyboard 
 technique , concentrate interpre- 
 | tative , especially registration — department 
 | organ playing , performer 
 | transcriber , exercise influence . 
 concern manual technique , , 
 | , point - rate 
 : pianist . Mr. Statham relate hear 
 proverb , man teach | } ; fas Oe ree " ge 
 nim play pianoforte evening , 




 690 


 1926 


 SESS 


 1926 691inevitably skill registration affect 
 playing Bach direction 
 generally approve . edition Bach 
 - register , result apt 
 unsatisfactory , _ treatment 
 comfortably manageable . Statham 
 good play Bach , instance 
 particularly striking performance . aside 
 question registration , matter 
 widely different opinion allowable , 
 doubt playing Bach gain enormously 
 characteristic impress 
 hear play — , 
 uncanny self - possession nervous control . , 
 marvellous technique , 
 playing long work Bacha 
 memorable experience . accuracy , Statham 
 , hear play 
 time , remember hear 
 mistake 

 play Beethoven Funeral March 
 flat Sonata memory , bar 
 7rio play /vemolando 
 quarter bar instead half - bar . 
 shake head , set right bar 

 remarkable control steadiness play 
 long difficult work ascribe 
 peculiar source . Best extraordinarily slow 
 pulse — slow understand 
 subject article Zawcet late 
 Dr. Charles Hayward , attend Best medically . 
 Dr. Hayward rate Mr. Levien , 
 mislay note . Mr. Levien tell 
 Dr. Stanley Mellville ( President Royal 
 Society Medicine ) , know Best playing , 
 pianist ( having , way , 
 accompany Santley ) , suggest 
 slow pulse great deal Best 
 steadiness absence flurry ' nerve ' 
 long exacting performance 




 692 


 1926 


 H. G 


 reflection work ' 


 SCRIABIN sense motion music attain 
 skilful use vary harmony . easily assimilate 
 harmony , judiciously mix compli- 
 cat chord , sense motion reason 
 clearly mark difference . know exacily 
 fast music 
 distinguish separate harmony . 
 harmony ' invention , ' borrow 
 phrase half - witted philosopher , 
 White Knight , musical language 
 heavy _ stilted ponderous 
 Johnsonese , lengthy burst speed 
 possible . ' palpitate 
 whirl joy - molecule ' feeling 
 animation , suggest speed 
 whirl palpitate gnat noisome 
 water stagnant pond . frequent shake 
 ( puissant , radieux ) .and ejaculatory snippet 
 sound , belove Scriabin , verti- 
 cally rise spark belch forth 
 chimney traction - engine , prove 
 engine certainly , slow rate 
 cumbersome wheel progress 
 scarcely perceptible 

 people Scriabin merely 
 composer , " 4e composer . pianoforte 
 composition speak ' distinct advance 
 predecessor . ' ' think ! 
 merely pianoforte piece write 
 diflerent point view , different indi- 
 viduality , distinct advance . think 
 Bach prelude : tragic nobility 
 B flat minor ; gaiety C sharp 
 major . Scriabin work distinct advance 
 . think Beethoven sonata : * 
 sonata flat , Op . 26 ; ' appassionata , ' 
 Op . 57 : ' hammerclavier , ' Op . 106 ; 
 Scriabin work distinct advance . 
 tell Scriabin tenth Sonata 
 contribution instrumental music 
 great importance Beethoven . 
 ponder ! Schumann Carnaval , Etudes 
 Symphoniques ; Chopin Ballades , Etudes , Polo- 
 naises ; Brahms Variations rhapsody — 
 work , great , dwindle 
 insignificance Scriabin tenth Sonata . 
 tell orchestration eclipse 
 contemporary ; Schénberg , 
 Stravinsky , Strauss — master , world 
 acknowledge — handle orchestra 

 alth 
 : 
 sub 
 
 wou 
 
 find 
 Poe 
 peri 
 
 thir 
 
 wha 
 grea 
 con : 
 thos 
 1 




 1926 693 


 694 


 1926 695 


 696 


 1926 


 CHARLES WOOD 


 CHARLES WOOD 


 1926 697 


 698 


 1926 


 1926 699and proceed Mace 
 Abbé speak truth , ' music 
 produce result - day hear 
 proper condition . ' reasoning 
 need convince 
 art reach culminate point nearly 

 year ago , long Bach Beethoven 

 true , Mr. Dolmetsch bring forward 
 1925 

 summit pure music’—the String Quartets 

 Mozart Beethoven place good way 
 slope , presumably . advocacy 
 unbalanced sort good , 
 early english composer music 
 begin appreciate — reservation . 
 certain form music perfection 
 undoubtedly achieve century ago ( statement 
 imply perfection 
 reach ) . instance , perfect 
 example melody produce palmy 

 day plainsong , peak reach 




 life 


 TOLSTOY 


 music TOLSTOY 


 700 


 1926september.—the understanding music come 
 time . year ago K. A. Islavin play 
 Chopin perfectly new , 
 obscure 

 6 September.—In art chief thing some- 
 thing new — . great 
 artist distinguish . Mendelssohn 
 — music beautiful , like everybody . 
 necessary . 
 Beethoven — like , possess 
 quality . know advance 
 

 29 July , 1909.—do remember Schopenhauer 
 music ? listen music 
 remain unsaid , 
 unsatisfying . perfectly true 

 detailed Diary . 
 relate music Yasnaya Polyana 

 2 ? December , 1907.—this morning Sibor ( violinist ) , 
 Bukinik ( violoncellist ) , Goldenweizer play . 
 evening concert . play Mozart , 
 Haydn , Beethoven , Arensky . Tolstoy like Mozart , 
 Haydn . : ' particularly love 
 Haydn . modesty — artistic self - denial 
 author . ' 
 specially pleased Aiva / e , hungarian 
 melody hear . ' think Haydn croatian 

 origin , live Croatians , use 
 folk - tune . ' goldenweizer confirm . 
 listen Beethoven , Tolstoy remark 

 remain tranquil admire artist ’ per- 
 formance . ' present begin enthusiastic 
 Beethoven , : ' think 
 pretend . Mozart , Beethoven 
 begin combine inner content external 
 effect , follower external effect 
 leave 

 Arensky Trio . Sibor 

 

 admirable thing poor 
 know Beethoven poor thing . 
 public cry Beethoven 
 write 

 evening Tolstoy : 
 old , know define 




 1907 , 


 1926 


 701view Tolstoy . ,—ep1tor 

 Bradford Festival Chamber Music ( October 5 
 6 ) hold attractive scheme . 
 concert day , 11.15 3 ( hour interval ) 
 7.30 9.30 . work announce include String 
 sextet Brahms Schonberg ( ' Verklarte Nacht ' ) ; 
 Pianoforte ( quintet Elgar , Fauré , Franck ; Bax 
 quintet oboe string , harp string ; 
 Ravel Introduction Allegro harp , string quartet , 
 flute , clarinet ; String ( quartet Beethoven , 
 Franck , Dvorak , Goossens , Debussy , & c. player 
 Virtuoso String ( Quartet , William Murdoch , Gwendolen 
 Mason , Leon Goossens , Haydn Draper , Robert Murchie , 
 James Lockyer , Ambrose Gauntlett . information 
 ticket , & c. ( early application necessary ) , 
 hon . secretary , ( Jueen Hall , Bradford 

 




 catalogue early music 


 702 


 1926 


 2 ( 1 ) ) . 


 1926 703 


 704 


 1926 


 new light late tudor composer 


 XIX.—JOHN MARBECK 


 GRATTAN FLOOD 


 15 : 


 SERS 


 1543 . 


 1514 , 


 538 , 


 $ 30 


 1926 705 


 ZURICH FESTIVAL 


 ACOUSTICS CHURCH 


 CONCERT - HALL * 


 706 


 1926 


 1926 707 


 .05 x 170,000 , 


 2000 4 - 253 


 " , o05 x 170,000 


 4785 


 


 708 


 1926 


 HALLE ORCHESTRA 


 _ _ _ 


 3.c. 


 1926 709 


 note GRIEG 7 composer 
 position musical world 
 suffer popularity . 
 case Mendelssohn , extreme popularity 
 ' song word , ' ' Elijah , ' instance , 
 having blind eye musical world 
 beauty ' hebride ' Overture , chamber 
 music , good work . particularly 
 case Grieg . Grieg generally 
 regard composer ' Peer Gynt ' 
 Suite negligible trifle pianoforte 
 hear evening thousand suburban 
 drawing room , important work , 
 great real beauty , 
 considerable danger total neglect 

 Grieg suffer popularity 
 music ; suffer 
 foolish remark so- 
 critical work , remark generally 
 form blame 
 , , know 
 Grieg successful small form 
 large , short piece criticise 
 sonata symphony . 
 sensible procedure criticise Beethoven 
 Symphonies short lyric piece . 
 Parry loftily remark ' intellectual 
 process concentrated development 
 [ Grieg ] line . ' statement entirely 
 true ( knowledge ' Holberg ' 
 suite , ' autumn , ' movement Sonata 
 Op . 45 , work , easy doubt ) , 
 fair reproach composer 
 sympathy largely short ' 
 lyric formsthan symphony . 
 desirable quality musical composition 
 ' intellectual process concentrated 
 development , ' quality , 
 gift melody , harmonic originality , rhythmic 
 interest present great degree Grieg music 

 far , assume big 
 canvas great work art 
 form , assume 
 nobility grandeur utterance essential 
 quality great music ? let nobility 
 mean , indispensable quality good 
 music . Scarlatti attempt nobility , 
 search Couperin Rameau 
 easily find . man honourable 
 place composer . , turn Bach 
 french Suites , beautiful 
 write ? surely music room 
 miniature forthe symphony , blame 
 composer miniature composer 
 symphony clearly cardinal error 




 ‘ 191 


 712 


 1926 


 OQ . ~ > . _ 


 18 ) — 


 713the Spirit Music : find to|to play reproduction , praise . 
 share . Edward Dickinson . pp . 218.| Schubert convert , 
 Charles Scribner . surely lovely work . worth stack 

 euvres inédites de Beethoven . ' publish an| - rate song yearning tear 
 introduction Georges de Saint - Foix . Publica- | ( l1751 - 4 ) . 
 tion Sociétié Francaise de Musicologie.| Salisbury Singers unequal 
 Vol . 2 , 30s . London : Harold Reeves item record . 3975 . ' phrasing 

 vom Volkslied bis zur Atonalmusik . ' Franz ] short - harmonized version Ivimey 
 Landé . pp . 69 . Leipsic : Verlag von Carl|/‘Drink . ' ending draw 




 x9817 


 l 30262a 


 ( t24698c 


 59 


 6954 ' 


 6783 e 


 69 


 718 


 yossi- 


 719 


 t. 


 VIOLIN music 


 CELLO method 


 b. v 


 1926 


 church musiccontrapuntal setting word anonymous 
 writer . edit Noel Ponsonby . 
 include publisher ’ series unison 

 Holst short Festival Te Deum , ' Peace , ' 
 Parry ' War Peace , ' Ethel Smyth ' Canticle 
 Spring , ' Charles Wood ' dirge 
 veteran , ' Beethoven mass D , ' Elijah , ' 
 ' Messiah , ' London Symphony Orchestra 
 engage . Sir Herbert Brewer 
 Dr. Percy Hull share organist duty . 

 follow singer : soprano — Beatrice 
 Beaufort , Agnes Nicholls , Dorothy Silk , Elsie 
 |Suddaby ; contralto — Muriel Brunskill , Astra 

 Desmond , Olga Haley , Millicent Russell ; 
 /tenor — John Coates , Edward Roberts , Steuart 
 | Wilson ; baritone bass — Norman Allin , 
 | Howard Fry , Herbert Heyner , Robert Radford , 
 | Horace Stevens 

 regularly , midsummer war , lie 
 rumour tell 
 |*prom , ' regularly August find 
 peculiarly London institution confound 
 | prophet . present season open August 14 , 
 | concert run pretty usual line , 
 | evening allot 
 ‘ year past . programme book arrive near 
 |our press day detailed discussion , point 
 catch eye . , Beethoven 
 | symphony play chronological 
 order upside , begin . 8 end 
 } no.1 . ninth omit — odd decision , 
 surely . frequently choral 
 | finale exception 
 |truncate method . . 1 
 |omitte , interest little 
 |historic . pleasant slow movement 
 | average audience want hear - day , 
 | ninth rarely perform . 
 | point note organ solo 
 |droppe . presume economic 
 reason , question popularity 
 cause , 
 |matter regret . general 
 | musical public welcome prospect organ 
 | recognise place concert solo instrument 

 - song , edit Martin Akerman , E. T.| ( America France ) , 
 Chapman setting ' let world | reason opportunity hear 
 corner sing , ' C , S. Lang carol , ' Tres magi de | great ( eventually , , 
 gentibus , ' word translate latin text , 15th | familiar ) work Bach , play proper 
 century . apparently write school| instrument . present non - churchgoing musician 
 use . simple , dignified setting ) rarely hear save transcribe form , 
 treble man voice , mainly unison - | inevitably rob certain quality effect 
 harmony . carol , composer write organ supply . , 
 stately tune unison voice , brief refrain | renovate instrument Queen Hall 
 close verse - chorus . | jittle mere tap supply / fgioso 
 effectively - write work provide background Largos , solemn Melodies , Bach- 
 excellent organ . G. G. | Gounod concoction 




 2 = _ 


 UNACCOMPANIED anthem EVENSONG 


 c34 6 — fs 2 } 


 SSS 


 = — S SS | 3 = 


 SSS SSS 


 HAIL , GLADDENING LIGHT 


 S33 SSS SSS SS 


 L 


 SSS EE SS 


 ( SSS SS | _ — = — f — 


 FE 


 = = BSS SS . 


 — — — — SS — require totally different type hall , audience , 
 atmosphere ; think , , ordinary 
 ' Prom ' habitué drive away . 
 think objection . , 
 ' Proms , ' Symphony Concerts , 
 violin pianoforte solo ( intimate 
 character chamber music ) frequently hear , 
 suffer appreciably large hall . 
 contributor ' Feste ' point year ago 
 advocate introduction occasional 
 chamber work orchestral concert , music 
 provide far relief vocal operatic aria 
 orchestra play prominent , 
 rule superior quality . 
 fact , long symphony concert ear 
 surfeit orchestra , real need 
 short item band . . believe 
 success English Singers season 
 ' Proms ' delightful singing , 
 refreshing contrast provide . 
 orchestra gain relief , 
 course . ( way , English Singers 
 appear year programme 

 Mr. Midgley fear Promenaders 
 drive away chamber music , 
 depend choice work . 
 suggest , example , later Quartets 
 Beethoven ; advisable play com- 
 plete work , short . detach 
 movement symphony play , ought 
 little objection use separate movement 
 quartet . Léner player long 
 habit gramophone record 
 detach movement . point 
 consider . average orchestral audience 
 know little chamber music ; opportunity 
 convert , eventually increase 
 public high type music 

 matter 7.30 8 organ recital , 
 company Mr. Lorenz , especially 
 suggest organist gladly 
 work merely nominal fee . recital 
 crowd assemble slight 
 instrument player . organ solo 
 hear concert - room , 
 accord recognise place programme , 
 performer pay adequate fee 




 1926subtlety , variety , skill beat hollow | 
 countless passage composer Brahms 

 Beethoven ( especially chamber music 

 choice illustration : Sir 
 Landon doubt good 
 listener play ' blue Danube ' Waltz . 
 plenty Ronaldites find old 
 Waltz decidedly worn . good 
 excellent dance . 
 , Waltz Irving Berlin 
 Mr. Hylton hit far bad , ( 1 ) 
 poor dancing purpose ; ( 2 ) sickeningly 
 monotonous unwholesome style ; ( 3 ) 
 pianissimo attenuated couple 
 waltz drown . 
 fox - trot Mr. Hylton 
 impossible toe , , far 
 circle listener concern , 
 dead failure . nary toe stir — couple 
 doorwards fox - trot 
 hiccup way minute 




 lung man : 


 animal natural breathing 


 1926 


 729 


 complete ORGAN recitalist 


 ROYAL COLLEGE ORGANISTS 


 pass associateship , JULY , 1926 


 pass fellowship , IULY , 1926 


 FELLOWSHIP paper - work 


 G. J. BENNET ? . 


 FELLOWSHIP ORGAN - WORK 


 730 


 1926 


 ALAN GRAY 


 ASSOCIATESHIP paper - WORK 


 A. EAGLEFIELD HULL , 


 H. DAVAN WETTON 


 ASSOCIATESHIP ORGAN - WORK 


 NEW COLLEGE CHAPEL , OXFORD 


 GREGORIAN ASSOCIATION 


 record long choir service 


 DR . DAVAN WETTON 


 NEW ORGAN MELBOURNE TOWN HALL 


 1926 


 fourth , fifth , sixth 


 CINEMA ORGANIST 


 1926 733 


 position OPERA 


 KAIKHOSRU SORABJI 


 HANDEL FESTIVAL 


 fret viol 


 ORCHESTRAL notation 


 1926 


 734 


 ' genius ' WILLIAM baine correction . 


 1926 735royal academy music 

 summer term , academic 
 year , busy , addition customary 
 youtine work concert , add 
 excitement examination opera week . 
 student ’ chamber concert , June 24 , notable 
 excellent string - quartet playing movement 
 Brahms quartet minor , Op . 51 , . 2 . 
 doubt general standard chamber - music 
 performance Academy moment high , 
 particular instance ensemble 
 sedulously maintain , tone conception 
 performance exceedingly sustain 
 beginning end . good , , Beethoven Quintet 
 E flat , pianoforte wind instrument , 
 pianoforte exceptionally play . word praise 
 horn player clever management 
 somewhat unruly instrument . ex - student , Miss 
 Dorothy Howell , represent Phantasy 
 violin pianoforte . pleasantly play , 
 long . young composer , matter 
 mature year , idea 
 ' brevity soul wit.2 MS . song 
 student , Mr. Norman McLeod Steel , victim war 
 loss sight , pleasing , fact 
 betray imagination — unusual asset 
 song - writer . compact point tiny 
 song string quartet accompaniment , Miss Olive 
 Pull . intelligently sing Miss Barbara 
 Pett - Iraser 

 student dramatic class perform- 
 ance June 25 26 , ( good acting 
 quaintly - playlet , ' pass lentil 
 boil , ' especially outstanding Boy . young 
 lady play distinct fair stage , 
 diction good , know 
 . ' wonder Hat ' 
 act , interest sustain , 
 ' Queen Cornwall ' . young 
 amateur , amateur , fit play 
 tragedy , generally succeed 
 play ridiculous 




 ROYAL COLLEGE MUSIC 


 736 


 TRINITY COLLEGE MUSIC 


 ASSOCIATED BOARD 


 UNIVERSITY MANCHESTER 


 result MUS.B. EXAMINATIONS , JUNE , 1920 


 SOCIETY WOMEN MUSICIANS 


 260 


 1926 737 


 EMORY GLEE CLUB 


 B.B.C , CHAMBER concert 


 COVENT GARDEN opera 


 738 


 1926 


 RUSSIAN BALLET 


 pianist month 


 1926739 

 Mr. Solomon reduce pianoforte - playing certain 
 pellucid perfection note passion . 
 recital Wigmore Hall choose stereo- 
 type programme , begin Busoni arrangement 
 Bach Chorale , * ' Wachet auf , ' spirited 
 sparkle d major Sonata Haydn . come 
 Beethoven Sonata , Op . 31 , . 3 , Schumann 
 * Carnival , ' character brilliance lack 
 warmth abandon 

 Miss Ania Dorfmann , brilliant , use 
 exaggerated rubato destroy flow movement 
 Brahms - Handel Variations Schumann ' Carnival ' ; 
 Scarlatti Sonata Gavotte Arne 
 admirably rhythmical charm 

 Miss Jessie Hall decided personality , keen 
 sense rhythm tone - colour enable 
 spirit character play . programme 
 -Eolian Hall light . begin Bach 
 French Suite E , Schumann Etudes 
 Symphoniques , * Waldstein ’ Sonata , Prelude , Fugue , 
 Variation Franck - Bauer , end Albeniz 

 Josef Hofmann great master craft , possess 
 rare power fill audience new life 
 vigour . _ appear twice recently — time 
 violin pianoforte recital Madame Lea 
 Luboschutz , fine artist , play 
 Sonatas — Grieg F , César Franck , ' Kreutzer ' 
 Beethoven — old friend delighted 
 hear . fine style perfect ensemble 
 memorable . recital great opening 
 rarely - hear Schumann Sonata F minor 

 notable feature Miss Youra Guller 
 recital " Eolian Hall exceptionally pure , delicate 




 student OPERA 


 ROYAI . ACADEMY MUSIC 


 740 


 1926 


 ROYAL COLLEGE MUSIC 


 NATIONAL ASSOCIATION competitive 


 CHOIRS ANNUAL conference 


 SUM 


 1926 741 


 british federation musical 


 COMPETITION FESTIVALS 


 SUMMER conference LONDON , JUNE 26 


 1926 


 1926 743 


 ENGLAND WALES 


 1926 


 SCOTLAND Music province 

 BIRMINGHAM,—The complete set programme 
 symphony concert City Orchestra 
 season hand . day concert 
 alter Tuesday Thursday , 
 arrangement people outside town 
 Thursday early - closing day . concert 
 series hold Central Hall , 
 seven place usual Town Hall , 
 present undergo reconstruction . general scheme 
 series fairly enterprising , include 
 work ultra - modern school likely frighten 
 timid concert - goer . principal item 
 programme Beethoven Pianoforte Concerto . 5 , 
 e flat ( ' emperor ' ) , Mr. Harold Bauer soloist , 
 Schubert ' Fierrabras ' Overture , ' Enigma ' 
 variation . movement Holst ' planet ' 
 promise November 11 , Mr. Albert Sammons 
 play Elgar Violin Concerto b minor . 
 concert Bruno Walter conductor 
 instead Mr. Boult , , , conduct 
 programme . great c major Symphony 
 Schubert fine performance Herr Walter 

 evening . December concert , 
 Mr. Harold Samuel play Brahms rarely - hear 
 Pianoforte Concerto . 2 , b flat , 
 Triple Concerto minor , pianoforté , violin , 
 flute . Mozart Symphony . 38 , D ( ' Prague ' ) , 
 figure programme . concert include 
 novelty shape Mahler Symphony . 4 , G , 
 little music composer hear 
 Birmingham , event await special 
 interest . work require soprano soloist 
 movement , Miss Dorothy Silk 
 fill réle . Dukas ' L’Apprenti Sorcier ' include 
 programme . Mr. Johan Hock , - know 
 ’ cellist Catterall Quartet , appear sixth 
 concert , play Cello Concerto Schumann , 
 , presumably , Don Quixote 
 performance Strauss symphonic poem , 
 unknown overture Weber , ' ruler 
 spirit , ' special interest , Debussy 
 * L’Aprés - midi d’un Faune ' complete programme 
 suit musical taste . composer varying 
 style represent programme 
 concert — Brahms , Mozart , Bantock , Vaughan Williams , 
 Delius appear turn . Delius symphonic poem , 
 ' Paris , ' novelty , hear 
 interest love music composer , 
 Miss Jelly d’Aranyi soloist occasion , 
 play Mozart Violin Concerto . 4 , D , piece 
 Vaughan Williams , ' lark ascend . ' Bantock 
 ' Dante Beatrice ' Brahms ' tragic ' Overture 
 hear . chorus Festival Choral Society 
 help final concert season , 
 celebration Beethoven centenary . * Egmont ’ 
 Overture , song ' Egmont , ' big Choral 
 Symphony . Sunday concert 
 hold season West - End Cinema , 
 seating accommodation . Children concert 
 usual , series lunch - hour concert 
 arrange 

 BRADFORD.—The programme come Festival 
 chamber music ( October 5 6 ) announce , 
 programme include Brahms Sextet Clarinet 
 ( Quintet , Schénberg ' Verklarte Nacht . ' Bax Oboe 
 Harp ( uintet , Elgar Pianoforte ( uintet , Debussy 
 ( Quartet , second Violin Sonata Delius , Phantasy 
 Quartet Goossens , Ravel Introduction Allegro 
 string wind , work Mozart , Beethoven , Franck , 
 Dvorak , Fauré , principal artist 
 Virtuoso Quartet 

 BRIGHTON.—The Charlier Quartet Liége play 
 Pavilion July 2 3 , programme include 
 ' selection Old Style ' work Grétry , 
 Quartet C sharp minor Rogister , serenade 
 Joseph Jongen 




 OX 


 


 W. H 


 HA 


 1926 745 


 OPERA record ; II.—BERLIN 


 746 


 1926 


 GERMANY 


 PUCCINI ' turandot ' DRESDEN 


 school conductor 


 ADOLF WEISSMANNHOLLAND 

 Utrecht Municipal Orchestra maintain 
 standard refuse lay instrument 
 summer . week June , regular 
 conductor , Evert Cornelis , player excellent 
 Summer Music Festival , programme 
 combine happy measure classical modern . 
 Handel ' water , music , ' Beethoven * Ninth , ' Mozart 
 * Haffner ' Serenade , Mahler * Lied von der Erde , ' 
 short work composer , follow 
 appropriately Debussy Nocturnes , 
 Chausson ' Chant Funébre ' ( orchestrate d’Indy ) , 
 Honegger ' Cantique de Paques , ' song 
 Rimsky - Korsakov Moussorgsky , sing 

 Vera Janacopulos . spite excellent singing , 
 artist wear welcome 
 number . Lotte Leonard visit 
 vocalist . pleasing record dutch singer — 
 Suze Luger , Louis van Tulder , Thom Denijs — 
 respect worthy associate , orchestra 
 local branch Toonkunst play sing . 
 maintenance provincial orchestra difficult 
 problem , 
 unforeseen happen appear likely orchestra 
 Groningen , outpost need musical help 
 , resume activity 
 season . summer season Scheveningen start 
 bad weather visitor , programme 
 thoroughly good , exciting , 
 contain number item seldom hear England . 
 sensation day ’ visit Paul 
 Whiteman band , critic public 
 alike quick ' new 
 method ' ' new idea ' simply old 
 prominent advertisement actual manner 
 performance , brilliant instrumentation repertory , 
 remarkable virtuosity player , novelty 
 tone - colour win favourable comment . 
 publicly privately , , comment 
 lack newness piquancy rhythm . 
 actual novelty , ' jiidische 
 Trilogie ' Asger Hamerik , danish - american 
 composer , group song orchestral 
 accompaniment Leo Ruygrok , second ' cellist , 
 winter , second conductor Residentie Orchestra . 
 Hamerik work pleasant , richly score miniature 
 Symphony , reminiscent thematic material 
 19th - century leader orchestration chiefly 
 Wagner . long solo bassoon lightly accompany 
 form bulk middle movement , ' lament , ' 
 prove unusual highly effective . song 
 Ruygrok , sing pure tone good expression 
 Madame Stotijn - Molenaar , evidently exercise 
 writing voice orchestra composer 
 master scarcely master art 
 combine . item outstanding 
 interest perform Concertino oboe 
 orchestra striking section 
 middle * ' romance , ' beautifully play J. H. Stotijn ; 
 Strauss ' sprach Zarathustra , ' play 
 programme Wagner ' Faust ' Overture Beethoven 
 C minor Symphony ; performance Strauss 
 * Don Juan ' Tchaikovsky ' Romeo ' Juliet ’ 
 overture ; small item Alex Voormolen , Joh . 
 Wagenaar , Lekeu , & c. Georg Schnéevoigt 
 capable assistant , Ignaz Neumark , Residentie 
 Orchestra play . music 
 represent chiefly organ recital , military band 
 concert open air , high 
 standard programme selection performance , 
 . mention incidental 
 music outdoor play , write local composer , good 
 example Emile Enthoven Utrecht 
 University Lustrum Feast play ' Ichnaton , ' 
 spirit piece reflect arbitrary attempt 
 reproduce egyptian instrument effect . 
 perform Municipal Orchestra , 
 Evert Cornelis . HERBERT ANTCLIFFE 

 VIENNA 
 PHE CRITICAL STATE OPERA 




 VOLKSOPER ail 


 symphonic concert 


 1926 749 


 750 


 1926 


 content 


 720 


 MUSICAL time 


 charge advertisement 


 RI 


 BERNA 


 dure month . 


 ATHBONE , 


 FERRAR 


 1440 


 1449 


 1450 


 1451 


 JALMISLEY , T. 


 LLINGFORD , 


 GUSTAVE FERRARI 


 GUSTAVE FERRARI 


 GUSTAVE FERRARI 


 FERRARI 


 GUSTAVE FERRARI 


 GUSTAVE FERRARI 


 24 


 publish 


 W ILLERTON , 


 H. W. GRAY CO . , NEW YORK 


 special notice 


 FRIDAY , AUGUST 20 ( f1rst post 


 english lyric 


 SET MUSIC 


 C. HUBERT H. PARRY 


 set , price shilling . 


 set . 


 SECOND set . = 


 set . YO 


 FOURTH set . 


 SIXTH set . 


 SEVENTH set . = 


 eighth set . 1 


 ninth set . 


 TENTH set . — 


 ELEVENTH set . OPER : 


 twelfth set . l