


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 FOUNDED 1844 . 


 PUBLISHED MONTH 


 ROYAL CHORAL SOCIETY . WORCESTER MUSICAL FESTIVAL . 


 1914 . 


 ( ONSERVATORI M MUSIC 


 MANCHESTER SCHOOL MUSIC . COMPETITIVE FESTIVAL 


 SEAFORD PRIORY , SUSSEX . | LONDON COLLEGE MUSIC . PF 


 47 , MORTIMER STREET , REGENT STREET 


 LONDON , W. ™ 


 P.RAWN 


 “ 


 , SALES 


 MACDONALD SMITH SYSTEM 


 PIANOFORTE TOUCH TECHNIQUE 


 SIR FREDERICK BRIDGE , M.V.O 


 6000 SUCCESSFUL PUPILS 


 M. MACDONALD SMITH 


 CONTRALTO ) . 


 MR . SAMUEL MASTERS 


 ( TENOR 


 IVOR WARREN 


 TENOR . 


 ADDRESS : 


 MR . MONTAGUE BORWELL 


 ( BARITONE ) . 


 MISS 


 WINIFRED MARWOOD 


 124 , WALM LANE , CRICKLEWOOD , N.W 


 MR . GEORGE PARKER 


 MR . W. H. BREARE 


 STUDENTS PROFESSION 


 MR . LEONARD HUBBARD 


 MR . 


 CHARLESWORTH GEORGE 


 LONDON COLLEGE CHORISTERS 


 COMP OSE RS ’ MSS 


 DR . A. EAGLEFIELD HULI 


 L.R.AM . ( PAPER WORK 


 LATEST SUC SESSES 


 A.R.C.M. ( PAPER WORWR ) . 


 » SPECIAL CORRESPONDENCE 


 RUDIMENTS , HARMONY , FORM 


 F. R.C.O. SPECIALIST CORRESPONDR : 


 ISS F. 


 | N R.A , M 


 H. H. L. MII 


 R.C.O. , L.R.ALM 


 COACHING DEGR 


 TWEN 


 YEC 


 HUNI 


 HSTEIN HAL 


 WIGMORE STRI 


 CHSTEIN 


 MANAGER , 4 


 MU NRO DAVISON , F.R.C.O 


 HOLL 


 LESSONS 


 NTI RPOINT 


 ’ TEXT - BOOKS 


 JDLET 


 A.R.C.M 


 EES 


 PRED 


 LSTUI 


 ET , W 


 HALL 


 _ M : AR KS 


 YIOS , 32 , 345 39 ; 3 


 COURSE 


 SPECIALITY 


 SI CESSES : AR 


 FO 


 UR 


 ISS MARG ARET YOUNG , L.R.A.M.,A.R.C.M. 


 YL : SAINTS ’ MARGARET STREET CHOIR 


 KING COLLEGE , CAMBRIDGE . 


 OXFORD , NEW COLLEGE 


 . KNOWN CONDU CTOR & ORGANIST 


 COMPETE NT ORGANIST WISHES DEP- 


 RGANIST CONGREGATIONAL 


 SALE . — - MANUAL CHAMBER 


 RGAN PIANO MUSIC SALE.- 


 ARLINGTON MALE - VOICE CHOIR- 


 YORKS 


 VESPER HYMN . — “ THY HOLY KEEP- 


 MU SIC AL INSTRUMENTS . | — 


 SALE . AIR D : DANS SER 


 NORG = AMLIN T WO - BARTAL PEDAL VIOLIN PIANO 


 VIOLIN PIANOFORTE 


 GREAT SOLO ORGANIST CHARACTERISTI = 


 17 , ORCHARD STREET , PORTMAN SQUARE , W 


 DIES 


 STI 


 SINGING - CLASS CIRCULAR 


 JULY 1 , 1914 


 FEODOR IVANOVICH SHALIAPIN 


 437 


 435 MUSICAL 


 1914 


 440 


 MUSICAL T 


 HEART HEAD MUSIC 


 ' 1888 ) . consciousness — consciousness Lekain as/ art . 
 Lekain , Ninias , scene asa stage } remark ‘ Tchaikovsky open 

 setting , piece real life , allowed| misunderstanding . admits , 
 recognise diamond belonging the| ‘ inspiration ’ lost little heaven- 
 real actress , fictitious character , to| guided urgency , composer atones lack 
 prompt measures ensuring safety . } making use ‘ head - work . ’ 
 singer , like actor , guard ! true , , bald patches 
 emotion overpowering affect | works great masters process 
 voice , constantly watch ! kind . rule mechanically - 
 let dramatic passion distort mouth in| passages big men result 
 way spoil tone - production : | having fill traditional form traditional 
 course evening a| thana personal way . ‘ awkward moment 
 considerable portion consciousness | order sonata symphony 
 disengaged character representing , |commencement ‘ working - ’ section . 
 80asto sure taking vocal cues | Brahms generally mechanical , 
 right moment , striking awkward intervals | Beethoven . sort 
 correctly , . thought ‘ Tristan,’| writing styled ‘ head - work ’ 
 ‘ Iam Wotan , ’ dogged last| disparaging sense . remembered 
 thought ‘ tenor ; Iama bass : | artist lived , artist 
 act , sing : deploy | imagine , ‘ inspiration ’ going 
 confidently notes sure | continuously bar big work 
 fesonance ; skilfully manipuiate notes } . composition ‘ Tristan ’ 
 sure . ’ , , | necessarily work months , 
 meet apparent paradox great|years . composer 
 operatic actor convey feeling of|lay pen middle piece 
 warmth audience possessing | emotional development , 
 extraordinary degree coolness . jafter interval days weeks . 

 MUSICAL TIMES.—Juty 1 , 1914 

 setting brain work coolly soon generates technical artistic aid spheres transcription 
 needed heat . , fact , | 2nd interpretation , propaganda work — 
 actor — learn art ‘ striking | forthcoming women help , noted , 
 , ’ transporting skin given reason toadvance 
 character brief interval leaving cause art music 

 dressing room making entry stage.| _ given understand Empress 
 actors capacity | Elizabeth received suggestion favourite , 
 . ‘ reported Kean ‘ Rachel , ’ | Rasoumovsky , uncle man immortalised 
 says Mr. Archer . ‘ moment | Beethoven , award littl 
 laughing joking scenes , | Personal credit instituting national opera 
 moment , stage , raving | giving encouragement bore fruit 
 Lear writhing Phédre . ’ ~ actors|Shape Kussian musico - dramatic work , 
 induce requisite auto - suggestion Volkov ‘ ‘ Taniousha . ’ monarch 
 artificial means . ‘ Macready , as| attached little importance influence 
 Shylock , shake ladder violently | Music courtiers , believe ston 
 going scene ‘ Tubal , order up| absenting special 

 proper state white heat ’ ; | performances opera mulcted fine 
 known work proper | Mlty roubles 




 


 MUSICAL 


 44 


 444 MUSICAL TI 


 DETAILS MUSICAL COMPOSITION . 


 PRELIMINARY HINTS 


 NOTES , ETCstrong 

 exactly : esthetic sense dictates ? 
 found 
 nethod true rhythm movement 
 lear . Beethoven intended rhythm 

 fact added 
 written 4 - 4 ( 




 « 2 P1gl4 

 iene , ‘ Erf 
 , , want write notes time value | note value represent . following tabje = 
 equal quavers express : | clear : 
 ‘ vi 
 _ 2 F cong 
 ad ad oe - 
 6b « 2 ae o | e ' ome sett 
 Vo 2 = | - os fe = whic 
 — — — _ — — — 
 : Se ° coeewe > ee . 
 quavers , crotchets , ‘ 
 quavers equal unit ( @ ) . Similarly , — -- < — howe 
 = nt 
 writing 2 - 4 time crush notes ihe space © Se 
 quavers use quavers — . 
 ; — _ — _ — Q d 
 o eeeenene = * ' 
 — — — hims 
 J + ) 2 « | — eee Se , 
 } eo ? 2 2 eceeeee = eaito 
 : ee whon 
 SS = = 
 e — _ — < -—-- said , 
 Demisemiq ° eecescooe = notes 
 quavers equal unit ( @ , ) ; rhe excerpt Beethoven Choral Symphony Wore 
 want crush notes unit } quoted crucial example advantage ut 
 value , choose notes sum / derived crotchet- dotted - crotche Th 
 ( bracket figure ) exceeds time ; unit , form fitting conclusion section o 
 oul 
 Vo s 
 ys : _ alll . 9D dhe Luth 
 ifmeo adi tre ’ 
 ss a. epi 
 ¥ e- 333 Pa 2 2 sa ) = tee ? ) ee Waltl 
 6 ° — eefeees esnsisee ins 
 ; Pe Tr sheet : 
 te tunes 
 ~ — — » @ e e228 4 - _ = ed o-2 ¢ 2 ga ° ; $ - 2 ° : - — _ ; — ~ — 2a asa 
 S : ~ig 8 3:2 3 ° ( $ FF ° s 27ie =| 2 ele ? = 
 S54 sete = = { } — -eel ® 588 g ) setiaet knit 9 
 Th 
 Luthe 
 Let notated dotted - crotchet unit , composer intentions sharp hore 
 silhouetted : 
 lta ace 
 Dotted - cr eats int ur ) 
 goystttastsg os 3238 | 
 22 = } 
 s et — Sete 
 -_-_- . . 
 Stes £ 2 Luthe 
 o ° coe . 2 yo ? oe ° S ™ » 222 | 
 29 22 2 3 3 ° " |3 ° # 2 * « gl 32223 S5igetes . atest ‘ Chri 
 8 a2@ = = = = = = — = o 22 — — — = n 
 — — — eee 13th ( 
 Enchi 
 rhythm apparent : ) Av / wve di , Ge , entirely avoided , execune mprc 
 accidentals found redundant difficulties considerably reduced . melod 
 eliminated ; fast single beat bar , difficult , little doubt wi soon f 
 follow awkward execution , replaced | exceptions crotchet- dotted - crotcht ! ( 2 . 
 ) ordinary beats ; lastly necessity | unit suitable styles music . vn 
 ( le continued . ) } ths 
 o1 
 Luther 
 Reden 
 MELODIES LUTHER HYMNS f gn 

 choirbs 
 ARCHIBAL!Y W. WILSON . ~ feyt 
 d 
 sa 
 rhe chorales growth centuries . | time carried work past ° 5 
 : “ . 
 Gradually , Middle Ages , (; erman sacred | composing new hymns , w hich areion af t ated 
 verses—-the old Easter Kyiie - songs , translations of}and original . revision song * | 
 Latin hymns , metrical paraphrases parts | medieval texts came melodies , ma 
 liturgy , Christmas cradle - songs Mystery | receiving drastic treatment th Hasent 
 z . “ 7 . " > ont 
 llays -- won way services Church . | . melismata , groups notes set ( © Luther 
 ( hus German Reformation , spiritual song in| syllable , characteristic _ — print 
 popular tongue , foundation | melodies , simplified rhythm ee 
 new hymnody built . ‘ wish , ’ | defined . musical work Luther 
 wrote Luther friend , Spalatin , year 1524 , | assistance eminent musicians , Conrad yt ‘ 
 called the*As@P 




 BR . ) NEW MELODIES 


 54 ~~ ° + ) { = - _ 


 SIR ISIDORE GEORGE HENSCHEL 


 _ _ _ 


 18 , 1850 


 COMPLEAT ORGANIST . 6 ° 


 EE _ 


 7 : H 


 136 MUSICAL T 


 , 1914 


 19 ) 


 BOOKS RECEIVED . 

 Beethoven 

 Pp . 96 . ( London 




 ... T 


 SWELL - BON TUBA 


 EDITOR ‘ MUSICAL TIMES 


 MUSICAL 


 TIME 


 S.—JULY 1 , 1914 


 APOSTLES ’ CANTERBURY 


 CATHEDRAL 


 ROYAL OPERA , COVENT GARDEN . 


 FRENCH ITALIAN SEASON , 


 F. BARRETT . 


 6 , 5 , 9 , 10 , 


 = = + -!_@—6 


 S55 


 YE WALK 


 N STATUTES 


 NS 


 [ 2 1 + 


 YE 


 WALK STATUTES 


 — — - - - - = — — _ 


 ; SEES . SRP — 


 YE WALK STATUTES 


 YE WALK STATUTES 


 LE RR 


 SI8 


 YE WALK 


 STATUTES . 


 — = — = M. M 


 2 : — - 7 


 Y = | 4A chamber concert given students Academy | pwans enthusiasts 
 stthe Duke Hall 25 , Bemberg ‘ La Ballade |S¥Ch music written stage 
 - da Désespéré , ’ Miss Katherine Dyer vocalist , g | years , betray certain lack 

 fature interest . Concerted music Bach , Beethoven |of proportion , wonder 
 — — Tanéiv , Arensky played , solo works given | nes ; ry “ g~ ob ages 

 Miss Gertrude Cotter Miss Hilda Klein ( pianoforte ) , | D2V€ ver heare o cece , ee ian . 
 und Master Wolfe Wolfinsohn ( violin ) . vocalists | wishes deny extraordinary vivacity picturesqueness 
 Miss Eleanor Evans , Miss Evelyn Langston , Mr. Gerald |of Russian music , skill scored 

 ROYAL COLLEGE MUSIC . | proves music relies effect 

 _ |in great moments climax thematic treatment ( 
 programme chamber - concert given 28 | je music stage ) longest life . 
 included Beethoven String quartet F major , Op . 1353 / answers definition better 
 se Bd pore ig — en ag gee ae operas , unity , 
 Mr. 9 . © . ‘ . 3 o » SOMES ! * Prince Igor ’ possibly prove lasting value 

 Miss Eva Bagley , Miss Gladys Thomas , Miss Charlotte | works . dances ‘ Prince Igor ’ 
 ae Miss Kathleen Long ) , | familiar , proper surroundings 
 organ solo ( Mr. Ifarold E. Wylde ) . effect enhanced hundredfold , 

 production led scene enthusiasm London 
 theatre seldom witnesses . omission love 
 TRINITY COLLEGE MUSIC . interest plot slight , sufficiently 
 : interesting . M. Shaliapin appeared roles , 
 accordance sensible plan adopted | dissolute Prince Galitzky Khan Khontchak . 
 institution , students past term to|He altered appearance hardly 
 study Beethoven Mass C , June 16 a|recognisable second . M. Paul Andreev 

 performance work given College . | excellent Prince Igor , sang extremely extended 
 4 small numbers , choir sang certainty unity , | solo Act , lyrical passage 




 47 ° 


 ORFEO CATALA ( BARCELONA ) 


 ROVAL ALBERT HALL 


 LUB . 


 T 


 471 


 MUSIC CLUB 


 ORIANA MADRIGAL SOCIETY 


 QUEEN HALL ORCHESTRA 


 LONDON SYMPHONY ORCHESTRACatalani opera ‘ La Wally , ’ Overture 
 Glazounov ( . 1 ) Greek popular themes 
 features programme . , , Miss Florence 
 Macbeth singing engaged chief attention . 
 coloratura execution , usual , exceptionally brilliant 

 Herr Nikisch conducted concert June 8 , 
 programme consisted Symphonies : 
 Haydn G , Schubert B minor , Beethoven , 
 superbly played . audience occupied 
 seat house 

 M. Paderewski chiefattraction June 15 . 
 playing minor Pianoforte concerto 
 extraordinary brilliance poetry . Herr Nikisch 
 Orchestra played Elgar - welcome ‘ Enigma ’ 
 Variations , Mozart G minor Symphony , Strauss ‘ Till 
 Eulenspiegel , ’ Wagner ‘ Rienzi ’ Overture 




 SLAVONIC MUSICAmong events personal interest present 
 season interesting return 
 London concert - platform Madame Tetrazzini 
 Madame Clara Butt . famous Italian prima donna 
 appeared distinguished ‘ star ’ company 
 Albert Hal ! June 4 . opening Verdi ‘ Ah ! 
 fors e lui ’ gave instant reminder triumphs 
 country showed technique facile 
 marvellous . Eckert ‘ Echo song ’ 
 Félicien David ‘ Couplets du Mysoli ’ gave proof 
 extraordinary gifts remain undiminished 

 Madame Clara Butt , Mr. Kennerley Rumford , 
 reappearance Albert Hall June 6 
 immense audience . magnificent voice , 
 cease developing power range expression , 
 heard great advantage Verdi ‘ O don fatale , ’ 
 Beethoven ‘ Creation Hymn , ’ Herbert Hughes ‘ know 
 love , ’ numbers equally varied . Mr. Kennerley 
 Rumford chief success secured famous ‘ Largo al 
 factotum ’ * Rossini ‘ II barbiere 

 admirable Sonata - recitals given M. Paul 
 Kochanski ( violin ) Mr. Arthur Rubinstein Bechstein 
 Hall 25 June 13 . artists , high 
 rank , worked notable unity . 
 occasion played works Brahms major , 
 Szymanovski D minor , Beethoven ‘ Kreutzer . ’ 
 second heard Korngold Sonata , 
 Mr. Paul Draper introduced new interesting song - cycle , 
 * Des Hafis Liebeslieder ’ Szymanovski 

 Folk - Song ( ) uartet Monique Poole String 
 Quartet gave joint recital .Kolian Hall June 4 . 
 Dr. Walford Davies Pastorals excellently sung , 
 composer pianoforte . instrumental 
 ( Quartet heard Percy Grainger ‘ Molly shore , ’ 
 works 

 Tchaikovsky fifth Symphony Mendelssohn * Ruy 
 Blas ’ Overture works performed 
 Royal Engineers ’ Orchestra Mr. Neville Flux 
 Queen Hall qn 20 

 CHAMBEK CONCERTS . 
 trios Godard , Op . 32 , Volkmann , Op . 5 , Beethoven , 
 Op . 1 , played Miss Lena Sykes ( pianoforte ) , 
 Madame Beatrice Langley ( violin ) , Mr. Warwick Evans 
 ( violoncello ) , Bechstein Hall 28 

 programme Mr. Holbrooke concert 
 season , given Arts Centre 29 , included 
 ‘ Fantaisie ’ string quartet Richard Cleveland , 
 Alfred Hale Edward Mitchell , 
 pianoforte solos Wilfrid Kershaw . 
 heard time . Dr. Ethel Smyth Quartet 
 E minor , Dances pianoforte strings 
 Josef Holbrooke played . artists Messrs. 
 Sammons , Petrie , Tertis , Withers , Sharpe ( strings ) , 
 Mr. Kershaw Mr. Holbrooke ( pianoforte ) , Mr. 
 David Brazell ( vocalist 




 RECII 


 VOCAI ALS 


 473 


 PIANOFORTE RECITALSler Rudolph Ganz , Steinway Hall , 22 
 D major , Haya 

 Mr. Marc Meytschik , Steinway Hall , 25 — Sonata , 
 Op . 109 , Beethoven 

 Sonata 

 ebussy 

 Mr. Sydney Rosenbloom , Steinway Hall , June 3 
 ink major , Beethoven 

 Mr. Claude Pollard . Bechstein Hall , June 4 — ‘ Marée- Basse , ’ 
 ‘ La voix du Foret , ’ ‘ Prélude , ’ Zazl de Marel 

 Variations Fugue theme Handel , Arahmy 

 Mr. Walter Scott , Bechstein Hall , June 11 — Sonata 
 C major , Beethoven 

 M. de Pachmann , Queen Hall , June 13 
 Yanations , Bee‘hoven 

 Miss Fanny Davies , Zolian Hall , June 17 

 Pa es ( Op . 109 ) , Beethoven 

 Henri Gilles , Steinway Hall , June 17 — Prelude , Chorale 




 VIOLIN RECITALS . 


 RECITALSMiss Johanna Heymann ( pianoforte ) , Mr. Marcel Bonnemain 
 ( violin ) , Miss Ethel Maas ( vocalist ) , Steinway Hall , 
 June 11 — Sonata ‘ Le Tombeau , ’ Zec / azr ; Old French 
 songs 

 Miss Polyxena Fletcher ( pianoforte ) Miss Marie Motto 
 ( violin ) , Zolian Hall , June 14 — Violin sonata E flat , 
 Beethoven ; Klavierstiicke , Brahms 

 Mr. York Bowen ( pianoforte ) Mrs. Sylvia York Bowen 
 ( vocalist ) , -Kolian Hall , June 16 — Sonata B minor , 
 Lisst ; songs , York Bowen 

 474 MUSICAL TIMES.—Juty 1 , 1914 

 Scenes ‘ Lohengrin , ’ ‘ daughter regiment , ’ | attendance certainly largest excelie 
 * Faust , ’ ‘ Madama Butterfly , ’ given pupils | concerts inaugurated years ago . orchestra ¢ 
 Marylebone Operatic Dramatic School 28 , | seventy performers best heard th 
 direction Miss Florence von Etlinger . | Promenades , Mr. John Saunders leader , Mi ; 
 ‘ Matinée musicale , ’ arranged Mr. Ernest W. | Landon Ronald conductor usual . Mr. Max Mossel wy 
 Gilchrist , assistance number artists , took | director , succeeded arduous tag 
 place Molian Hall June 9 . programme included | new works introduced season , 
 Madame Liza Lehmann cycle , ‘ Parody pie . ’ | especially interested performances hs 
 Miss Marta Cunningham gave extra * Matinée musicale ’ | Sir Edward Elgar Symphonic study , ‘ Falstaff , ’ yi 
 Claridge Hotel June 11 . artists | Rachmaninov Symphony E minor , Op . 2 . lig 
 appeared Madame Merle Tillotson Alcock Mr. Bechtel | artists appeared season included mp 
 Alcock ( vocalists ) , Madame Backus - Behr ( pianoforte ) | newcomers , Miss Carrie Tubb , Ms 
 new London . Phyllis Lett , Miss Clara Evelyn , Mrs. York Bowen , Friuis 
 June 11 Miss Cicely Trask gave programme | Riess , Miss Beryl Freeman , Mile . Adele Clement , Miss } 
 * Chansons anciennes ’ Arts Centre , added ‘ Rustic Schulz , Miss Muriel Pickup , Miss Maud Delstanche , ¢ 
 sketch ’ formed old English ballads folk - songs | Mr. Brabazon Lowther . 
 scenery connected action , music arranged exception music local parks ty , 
 M. Gustave Ferrari . vocal instrumental concerts Edgbaston Botanig 
 Compositions Miss Bluebell Klean introduced | Gardens , specially arranged Mr. Oscar Pollag 
 Bechstein Hall June 15 included Pianoforte quintet | music Birmingham July August enjos ; 
 C minor , songs interpreted Miss Ada Crossley | complete rest , sazson morte . - 
 Miss Xenia Beaver . | = 
 artists appeared Professional Musicians ’ | ayers ’ 
 Débiit Society pln . .Eolian Hall June 15 , | BOURNEMOUTH . 
 Miss Mary Hessel , Miss Maud Murray , Miss Ida Agnew,| weekly Symphony Concerts 
 Miss Kathleen Joliffe , Signorina Emilia Scafidi , Mr. Thornley | little attempted behalf musical section 
 Grattan ( vocalists ) , Mr. Harry Idle ( violin ) , Signor Manlio | community . Symphony Concerts , howeve , 
 di Veroli ( pianoforte ) , Miss Maud L. Arnold } serve good purpose holding ms 
 | enthusiastic resident music - lovers , ds 
 ; . | sufficiency summer visitors ideals nx 
 thirty - fourth annual Festival Church Sunday |above musical comedy outpourings al fresco concer 
 Schools took place great success Crystal Palace | parties . programmes submitted mas 
 June 13 . choir 5,000 voices sang direction | negligible artistic standpoint . ps 
 Mr. W. Schofield , Mr. F. W. Belchamber as/ month , instance , heard attractive workss 
 organist , choral competitions proved important | Goetz Symphony F ; Flemish Dances ly 
 scheme . | Jan Blockx ; Bizet ‘ L’Arlesienne ’ Suite . 2 ; Schumam : 
 tenth annual Festival National Union School | Overture , Scherzo Finale ; Coleridge - Taylors 
 Orchestras reported School Music Review July . | ‘ Bamboula ’ African dance ; Edward German ‘ Richa 
 - second London Sunday School Festival | III . ’ Overture ; Dvordk melodious fourth Symphony ; : 
 held Crystal Palace June 17 . Mr. | meritorious Old English Suite Scott - Baker ( conduct 
 J. Wellard Matthews conducted junior choir of| composer ) ; ‘ Flying Dutchman ’ Overt 
 5,000 , Mr. William Whiteman senior choir . | ( Wagner ) ; Mozart ‘ Prague ’ Symphony ; efiectir 
 orchestra directed Mr. Wesley Hammett . Competi-| altogether original tone - poem , ‘ Woods # 
 tions children adult choirs successfully held . | April , ’ Edith Swepstone ( performance ) ; mi 
 ee oe Bh E | Massenet ‘ Les Erinnyes ’ ballet music . solos 
 -| follows : Mr. H. Wolters , Municip ! 
 ' | Orchestra ( violoncello ) , Miss Nora Read , popular loa 
 Music Provinces . | soprano , Mr. Algernon Holland ( violin ) , Miss Muriel Polisi 
 |Mann ( pianoforte ) , Mr. George Baker , Mr. Arte 
 Strugnell , Mr. Frank Foster ( vocalists ) . 
 series competitions local amateur sopmns 
 ' contraltos , tenors ( baritones basses com 
 : BIRMINGHAM . ’ . , | later ) created interest , 
 Castellano Italian English Opera Company paid | abnormal discovered , fairly satisfactory leve 
 visit Prince Wales Theatre , gave lof achievement maintained , contnlt 
 week operatic season 25 30 , producing | competition providing far best material . pris 
 Lucia di Lammermoor , ’ ‘ Rigoletto , ‘ Il 1 rovatore , ’ * li | awarded votes audience — methii 
 Barbiere di Seviglia , ’ * La Sonnambula , ’ * Maritana , ’ ‘ | invariably illustrated soundness judgment 
 Bohemian Girl . ” succeed attracting large } Mr , Thomas J. Crawford , - known London organs 
 audiences ; , houses half - . ! appointed vacant chorus - mastership t 
 orchestra poor chorus colourless . Municipal Choir . Municipal Choir affo 4 
 artists scored brilliant Swedish soprano , | lenty scope hard work , wish fort 
 Miss Dirgis , delightful coloratura singer , excellent | advance choral music Bournemouth support bs 
 baritone , Signor Vail . — ; | endeavours - heartedly . 
 Birmingham Strings Club held second Chamber | 7 
 Concert new rooms Birmingham Royal | n 
 Society Artists , gave performance BRISTOL . 
 Beethoven String quartet C minor , Op . 18 , . 4 , 
 performers Miss Christine Ratcliff , Miss Hodgkinson , | 27 , miscellaneous concert w 
 Miss Maggie Edser , Miss Brenda Sichel . vocal | vocalists Miss Hilda Eager Mr. cy 
 quartets given Mrs. W. C. Hutchings , Miss | instrumentalists Miss Helen Cavell ( violin ) , ™ S 
 England , Mr. Charles Hyde , Mr. W. C. Hutchings . | Constance Carter ( violoncello ) , Miss Dorothy Peas : 
 feature concert Miss Gertrude Fuller | ( pianoforte ) . local musicians contributed ple 
 magnificent performance Vitali ‘ Chaconne ’ violin , | received . 
 played success Town HIall opening Bristol International Exhibits 
 Midland Competition Festival , accompanied | concerts gratifying character Ut 
 organ . direction Mr. G. Herbert Riseley . afternoot 
 chief musical event June weeks’| 28 main features consisted inane 
 season Theatre Royal Promenade Concerts , June 8| compositions Bristol Symphony ere 
 June 27 . strong appeal support , | ( augmented ) , chief attraction movements 
 past years paid way . The.| Tchaikovsky ‘ Pathetic Symphony . ’ Miss Agnes N 

 violoncello 




 CORRESPONDENTS 


 _ _ _ | 


 DEVONSHIRE TOWNS 


 CORNWALLDUBLIN 

 series Sunday orchestral concerts Woodbrook 
 Concert Hall terminated 31 . programme 
 included Beethoven fourth Symphony , Wagner 
 ‘ Waldweben , ’ Tchaikovsky ‘ Rococo Variations ’ 
 violoncello ( Mr. Clyde Twelvetrees ) orchestra . Madame 
 Borel sang Handel ‘ Let bright Seraphim ’ brilliantly , 
 organ accompaniment . previous concert 
 Beethoven ‘ Eroica ’ Symphony chief feature , 
 Miss Edith Mortier vocalist 

 series concerts hall announced 
 week commencing August 3 . London Symphony 
 Orchestra engaged , conductors 
 Dr. Esposito Mr. Hamilton Harty 




 47 


 EDINBURGH 


 LIVERPOOL 


 DISTRICTManchester reputation sufiered severe shock 
 autumn breakdown Denhof opera tour , 
 29 got morning papers 
 announced loss Hallé season working 
 £ 1,648 6s . 4:7 . half defizit 
 new system payment bandsmen , balance 
 probably diminished receipts doors 
 subscribers . set - 
 experienced Hallé Society , 
 attendance annual meeting thirty 
 184 guarantors executive ; room 
 difference opinion executive 
 justified attitude passivity 
 vote satisfaction . Press 
 urged concerts sure founJation 
 ways : ( ) Ample endowment ( 
 £ 40,000 hive necessary basis liquidate 
 season deficit ): ( 4 ) duty City Council 
 replace guarantors , support Hallé 
 concerts highest type 

 choral works season include Beethoven 
 Mass D , Bach ‘ God time best , ’ Berlioz * Faust ’ 
 ‘ Messe des Morts ’ ( February 11 ) , Bantock 
 compressed version ‘ Omar Khayyam ’ ; 
 novelties promised Korngolds * Symphonietta , ’ 
 Scheinpflugs * Comedy ’ Overture , Balakirev ‘ Thamar ’ 
 Suite , Ravel ‘ Valses nobles et sentimentales , ’ Reznicek 
 Suite ‘ Donna Diana , ’ Sibelius * Scenes Historiques 

 new visiting artists include Scriabin , Misses 
 Harrison , Madame Noordewier - Reddingius , Madame 
 de Haan - Manifarges , Mlle . Ilona Durigo , M. Cortot , 
 M. Rachmaninov , Miss Isolde Menges 




 MANCHESTER 


 EE 


 OXFORD . 


 BACH FESTIVAL 


 THEby Mis 
 wind fro 

 Beethoven ‘ Egmont ’ Overture 

 MUSICAL TIMES.—Juty 1 , 1914 




 47 


 EVENTS 


 BRIEFLY SUMMARIZEDANTWERP 

 great musical Festival held Société 
 Royale de Zoologie celebration 200th anniversary 
 birth Gluck . Excerpts Gluck ‘ Alceste ’ 
 works Mozart , Beethoven , Weber , Berlioz , Peter 
 Benoit ( National composer ) heard 

 ATHENS 




 BAYREUTH 


 BETHLEHEM , PA . 


 BUDAPEST 


 COLOGNE . 


 DARMSTADT 


 DRESDEN 


 DUSSELDORF 


 EVANSTON , ILLINOIS 


 FLORENCE 


 FRANKFORT 


 GENEVA 


 HAMBURG 


 LISBON 


 MOSCOW 


 MUNICH 


 NEW YORK 


 NORFOLK , CONNECTICUT . 


 PRAGUE , 


 RIGA 


 SALZBURG 


 VENICE 


 VIENNA 


 CONTENTS . 


 461 


 1.T.B 


 SPECIAL NOTICE 


 MUSICAL TIMES . 


 SCALE TERMS ADVERTISEMENTS 


 MONTH 


 480 MUSICAL TI 


 N ORAVIAN HYMN BOOK : 


 S. SHEDLOCK . 


 RICHARD 


 R ® DHEAD 


 ~ AINT - SAENS , CAMILLE 


 A. B 


 FOSTER 


 1166 . 


 ONIC SOL - FA PUBLICATIONS : 


 2151 


 7 CHORAL 


 


 V ALTHEW , RICHARD 


 REV . H. L 


 PUBLISHED 


 H. W. GRAY CO . , NEW YORK 


 OIINSTON , E. F 


 ANNA P. 


 DIEPPE . 


 LONDON BRIDGE 


 EAST CROYDON 


 CATHEDRAL 


 SCHOOL 


 SAL ISBU CHORISTERS 


 POUNDS WALT 


 SICIANS 


 OLIDAYS.—COMPETENT ORGANS 


 . 


 ' MAN LITURGY 


 EDITED 


 ADVENT 


 LENT 


 EASTER 


 HARVEST 


 GENERA 


 HARVES 


 GENERA 


 ADVE 


 CHRISTMAS 


 MUSICAL 


 1914 


 NOVELLO ANTHEM 


 3 


 HARVE 


 GENERAL 


 CENERAI 


 ADVENT 


 CHRISTMAS 


 EASTER 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 HARVEST 


 GENERAI 


 ADVENT 


 EASTER 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 WHITSUN 


 HARVEST 


 GENERAI 


 BOOK 


 300 K 9 


 1 . GREATLY ] FERIAL RESPONSES 


 LITANY 


 E. C. B AIRSTOW . 


 _ — ' FALLS 


 HUGH BLAIR . — — - --- 


 SON 


 HARVEST FESTIVAL MUSIC 


 CANTATAS 


 SOPRANO , TENOR , BASS ( CONTRALTO ) SOLI 


 


 TENOR BASS SOLI CHORUS , ORGAN 


 TENOR BASS SOLI , CHORUS 


 CHORUS 


 


 W . “ EN ARRANGED 


 SHAPCOTT WENSLEY 


 J. H. MAUNDER 


 GOLDEN HARVEST 


 TENOR BASS SOLI CHORUS 


 SUNG 


 HENRY KNIGHT 


 THOMAS ADAMS , 


 RAIN 


 SOW PEACE 


 THOMAS . ADAMS 


 HARVEST - TIDE 


 SMALL ORCHESTRA 


 HUGH BLAIR 


 HARVEST CANTATA 


 CHORUS , SEMI - CHORUS , ORGAN 


 GEORGE GARRETT 


 SEED - TIME 


 SONG 


 HARVEST CANTATA 


 SOPRANO ( TENOR ) CONTRALTO ( 


 BARITONE ) SOLI CHORUS 


 WORDS 


 ROSE DAFFORNE BETJEMANN 


 JULIUS HARRISON 


 SOPRANO TENOR SOLI CHORUS 


 JOHN E. WEST . 


 THA 


 CHORUS ORCHESTRA 


 FREDERIC H. COWEN 


 ARVEST 


 NKSGIVING 


 GLEANER ’ HARVEST 


 ORGAN 


 FEMALE VOICES 


 


 C. H. LLOYD 


 JUBILEE CANTATA 


 SOLO VOICES , CHORUS , ORCHESTRA 


 HARVEST SONG 


 SOPRANO SOLO CHORUS 


 C. LEE WILLIAMS 


 HYMNS HARVEST 


 LET BRETHREN JOIN 


 SOWING REAPING 


 JOY HARVEST 


 HARVEST HYMN PRAISE 


 FORTH SOWING 


 SOWER WENT 


 


 MELODY 


 HEARTS 


 


 HARVES 


 O LORD HEAVEN , EARTH , 


 SEA 


 COME , YE THANKFUL PEOPLE , COME 


 LTO ( 


 


 VING 


 TH 


 OME 


 


 MUSICAL 


 IQT4 


 485 


 SERIES HARVEST ANTHE 


 MS 


 


 MUSICA 


 , 1914 


 MUSIC 


 


 NTHEMS 


 NOVELLO 


 CANTAB 


 POPULAR CH U RCH MUSIC 


 J. H. MAUNDER 


 SERVICES . 


 ORGAN , 


 CHURCH CANTATA , 


 NC 


 * O THANKS 


 * THOU VISITEST EARTH 


 FLOO SHALL WHEAT 


 JOVFUI THANKSG IVING 


 | SERVICES . BRAHMS 


 — — ® BRAHM 


 POP ULAR HARVES1 \NTHEI ! BRAHM : 


 A. J. JAMOUNEAU , 173 , VICTORIA AVENUE , HULL ! — 


 7 THILE EARTH MAINETH ’ FRANZ , 1 


 = - 3 BU . 


 ARVESTANTHEM.- 


 TIS TH 


 VOVELLO VOCAL ALBUMS 


 FRANZ . 


 TROUTBECKMETHOVEN.—TWENtTyY - S1x Sones . Vol . I. English 
 German words . English Version Rev. 
 J. TRoUTBECK . Price . 6d 

 BEETHOVEN.—SEVENTEEN SonGs . Vol . II . English 
 German words . English Version Rev. 
 J. TROUTBECK . Price . 6d 

 REETHOVEN.—TWENTY - Soncs . Vol . III . English 
 German words . English Version Rev. 
 J. TrouTBECK . Price . 6d 




 ENNETT , W. STERNDALE . 


 LONDON 


 W. 


 HOOK , JAMES . 


 JAQUES - DALCROZE 


 JAQUES - DALCROZES 


 VOCAL WORKS ENGLISH TEX 


 CELEBRATED CHILDREN 


 ACTION - SONGS 


 ACCOMPANIMENT 


 MEDIUM VOICE 


 ACCOMPANIMENT 


 SONGS 


 PIANOFORTE 


 RHYTHMIC GYMNASTICS 


 EURHYTHMICS 


 


 WITE 


 THI 


 


 OC 


 


 CHORAL 


 MUSICAL 


 EXT 


 SHOWER 


 « WORDS ADAPTED RUSSIAN MAIKOV , 


 WORDS ADAPTED RUSSIAN WORDS ADAPTED RUSSIAN 


 TH 0 ) ) C S 


 ’ CHORUS MIXED VOICES 


 WORDSWORTH 


 0OCAN YE SEW CUSHIONS ? 


 ; ( LULLABY 


 FEMALE 


 MALE 


 SIR 


 BALLADE 


 CHORUS ( T.T.B.B. ) 


 WORDS 


 CHARLES NEWTON - ROBINSON . 


 LONDON 


 SONGS 


 EDWARD ELGAR 


 GRANVILLE 


 NOVELLO 


 , 1914 


 459 


 S.AT.B 


 FOUNTAIN 


 BANTOCK . 


 MARCH CAMERON MEN 


 CHORUS MIXED VOICES . 


 MARY M. CAMPBELL 


 SPRING - ENCHANTMENT 


 - SONG MIXED VOICES . 


 HELEN F. BANTOCK 


 3 PAGEANT HUMAN LIFE 


 CHORAL SUITE 


 THOM ‘ 


 FESTIVAL SONG 


 CHORUS ( T.T.B.B. ) 


 WORDS 


 H. ORSMOND ANDERTON 


 COMPANY , LIMITED 


 PUBLISHED 


 NOVELLO HANDBOOKS MUSICIANS 


 EDITED ERNEST NEWMAN 


 CHORAL TECHNIQUE & INTERPRETATION 


 HENRY COWA RD 


 TEACHING ACCOMPANIMENTI 


 PUBLISHED 


 


 JOHN STAINER 


 WRITTEN COMPILED 


 MRS . G. T. KIMMINS 


 MUSICAL 


 MR . JOSEF 


 HOLBROOKE ' 


 CELTIC DRAMAS 


 DYLAN 


 DRURY LANE THEATRE 


 _ EVE ST . AGNES 


 CANTATA 


 CHORUS , ORCHESTRA 


 JOHN KEATS . 


 JOHN FRANCIS BARNETT 


 SOLI 


 DAILY TELEGRAPH 


 CHILDREN DON . ” MORNING POST . 


 OPERETTA . 


 ORCHESTRAL WORKS 


 CHORAL SYMPHONY 


 DRAMATIC 


 NEW SONGS . 


 SUNDAY TIMES 


 MONTHLY MUSICAL RECORD 


 GLOBE . 


 FALMOUTH 


 POEM 


 


 W. E. HENLEY . 


 R. WOODMAN 


 VOYAGE LOVE 


 SONG - CYCLE . 


 WORDS 


 HAROLD SIMPSON 


 MUSIC 


 


 N . NOVELLO MUSIC PRIMERS . PANTOMIME 


 GUIDE SOLO SINGING PLANOFORT ! SOLO 


 OSEI Y 


 CONTAINING INSTRUCTIONS SINGING 


 WELL- H. SCOTT - BAKER 


 DETAILED ANALYSIS 


 KNOWN WORKS SONGS 


 


 SCHOOLS LADIES ’ CHOIRS , 


 FR 


 PREFACE DIRECTIONS PRACTICE 


 JAMES BATES 


 ARRANGEMENT SMALL ORCHESTRA : 


 G. HENSCHEL BAGATELLE 


 CHARACTERISTIC PIECE 5 _ SONATA 


 VIOLIN PIANOFORTE 


 COMPOSED 


 


 G. F. HANDEL . 


 C. EGERTON LOWE 


 MUSICAL 


 CAMBRIDGE 


 UNIVERSITY PRESS 


 FIFTEENPENCE WEEKLY . | . 


 ST . MARTIN STREET , LONDON , W.C 


 CAMBRIDGE UNIVERSITY PRESS 


 FETTER LANE , LONDON 


 APRACTICAL GUIDE . MUSIC 


 CONTAINING 


 \UMEROUS TES T - QUESTIONS ANSWERS , ERNE ST A. DICKS 


 MUSICAL OPINION , 


 494 MUSICAL TI 


 NOVELLO 


 


 ORGAN 


 PIECES 


 ALBUMS 


 SELECTED 


 SELECTED PIECES 


 SELECTED PIECES 


 ORGAN 


 WALTER G 


 M.V.O 


 ATHENAUM 


 YORKSHIRE 


 POST 


 YOR OBSERVER 


 KSHIRE 


 BRISTOL 


 ALCOCK 


 = 2 — — — WESTERN MORNING NEWS . 


 PIECES 


 FAMILY NEWSPAPER 


 CHI 


 RCH 


 } MUSIC STUDENT . 


 PTHE BOOKSELLER 


 3 


 5 LOfficier q 

 — — — : - 
 N ORIGINAL COMPOSITIONS |NEW FORE IGN \ P U BI IC IONS . 
 S. 
 ORGAN 
 = \ PIANOFORTE MUSIC . NET . 
 1 K , ( New Series . ) AUBERT , L.—Sillages . Pianoforte Solo . 1 . Sur le Rivage . 3s - < 
 fo , s. d. net ; 2 . Socorry , 2s . net ; Dans la Nuit ie 
 Chorale Prodades C. Hubert H. Parry 3 6| BERNARD , P._Premitre Sqite oittaressne Sioa lias ae 
 O. 2 . Prelude C ; . W. Wolstenholme 1 6 BER " : 2 
 3 . Festival Prelude “ * Kin ’ feste Burg ” . W. Faulkes 1 6| BERR , J.—Op . 66 . Soir pluvieux . Pianoforte Sok 
 Royal 4 . Meditation — . W. Faulkes 1 6| — — Op . 68 . Bercetuse d'enfant . Pianoforte Solo 
 Postludium W. Faulkes 1 6/ DEBUSSY , C.—La Bo oujoux . Suite . Pianoforte 
 ” Jour Hoces J. “ Stuart Archer 1 0 DUBOIS . 1 fe “ Joujoux ; * “ Duet 4 
 . Cantiléne é R. G. Hailing 1 0 BOIS , H.—Fantasietta . Transcribed Pianoforte Duet 4 
 £ Ite Missa Est ] J. Lemmens 1 6 HUVEY , H.—Etude rationnelle des gammes et des arpeges , 
 ¢ Triumphal March Edited J. Lemmens 1 6 rythme appliqué aux gammes 
 ¢ ( - Fanfare Joun E. West J. Lemmens 1 o| JUMEL , C.—Seventeen Esquisses . Pianoforte Solo : 
 ht . Cantabile j il y pamean ; MARTIN , R. Cu.—Douze petits pitces de Virtuosite et de 
 Finale .. . Lemmens 1 ats . 1 le 
 Fantasy C. Edgar Ford « 6 ] , mye . Books & . . 
 1 Intermezzo W. Wolstenholme 1 6 Ik RKE L , J. Op . Esquisses nouvelles de la vie enfantine . 
 5 Legend Harvey Grace 1 6 Pianoforte Solo 
 6 . Meditation Alfred Hollins 1 o 
 + , Barcarolle Arthur W. Pollitt 1 o | ‘ 2 aa 
 8 , Cantique 4 Edward Elgar 1 6 ] CHAMBER MUSIC . 
 “ y Prelude Fugue | ( Ed ited Jon E. W TL . Kus © 6 DUBOIS , Tx.-—Andante Cantabile . Viola Pianoforte 
 sa sm . Epilogue W. Wolstenholme 1 6| EM MANUEL , M.—Suite sur des airs grees . Violin 
 x. Suite Ancienne - F. W. Holloway 2 6 Pianoforte 
 Original pecs nu . Fantasia Fugue G minor C. H. H. Parry 2 6| LARA , De.—Les Tr vis Masques . Fantaisie . Vis + Cello , 
 ng . Itisve 33 , Voluntary ms oe - Ww . G , Alcock 1 0 Pianoforte , Contrabass . ( Flute Clarinet ad ZZ : 
 ates Impromptu oo — + les Trois Masques , Sérénade dansée . Vi lin , Cell 
 5 Legen -G. Alcock 1 Pianoforte , Contratass . ( Flute Clarinet ad 
 % . Intermezzo J. Stuart Archer 1 o : 2 ( t os . . - ongasie ~ atiaveia 
 » . Miniatures H. M. Higgs 3 o MILHAUD , D.—s - F F r Violin Pianoforte . 
 & Toccatina nm W. G. Alcock 1 6| MOZ ART — _ e Venu Transcribed 2 ‘ Cellos . ( ard ‘ Cell 
 rity 29 . Romance flat H. Sandiford Turner 1 o ad /i , ) , Pianoforte ( Organ ) ai 
 3 . Nocturne Thomas F. Dunhill 1 + ] PICARD , i. _ Pre lude et Fugue sur un theme de Beethoven 
 ut . Festal Prelude Thomas F. Dunhill 1 6 > Violins et Viola ; 
 Caprice de Concert J. Stuart Archer 2 RAVEL , M.—Ma Mére ! Oye . Trio . Pianoforte , Violin , 
 ( continued . ) Cello . ( Contrabass Clarinet ad dié . ) 
 — sinks SAMAZEUILH , G.—Fantaisie Elégiaque Violin anc 
 sical instr London : Nove_to anp Company , Limited . Pianoforte ne ai 5 
 cea oa — — | SCHUMANN.—Abendlied Jensen “ Sehnsucht . ” Trans 
 R. WEBSTER cribed Flute Pianoforte 6 
 gan > > - SIEFERT , EF . B.—Trio . Pianoforte . Violin , ‘ Cell 
 . T > ~ ‘ F | 
 sundry . GROU NDW O RK MUSIC VIDAL , P.—Chanson printaniere . ‘ Cello Piano‘orte 
 needed II . , Key . VALUABLE EXAMINATIONS . WAGNER . Pilgrims Chorus ( Tannhiuser ) Trar ibed f 
 w I. , Suituinc ; Key , 1s . 6d . : ‘ Cellos Pianoforte 
 y clear , - = — | — — — — 
 — ePE ES Th 9ce | 
 WEBSTER ORGAN HARMONIUM MUSIC . 
 ‘ hild’c Dri - ' T — _ ' ry . | BARNES , F p ieces fe 
 Child Primer Theory Music . | BARES . & . 5.—-Op . s- _ Sue Penns Se Snge . 
 don | 1 1 ; 2 . t 
 stdin SHILLING . | FAHRMANN , H.—Op . 54 . Sonata . 1 Org 4 
 ag ate Swenty - secoxnp THOUSAND . | KARG - ELERT.—Op . 95 . Gradus ad age : Har 
 | monium . P. » t 15 Studies Bach Style 
 Q | 16 Pieces introductio Bach Style 7 
 s adm _ _ _ London : NoveELLo Company , Limited . SKAGERBERG . , | 13 Pre s. Org 2 Books 
 art RR N Ww = Book 
 BOW | iearentin 
 VOCAL MUSIC . 
 MUSIC READING LADDER BEGINNERS | : , 
 | BRAHMS.—Berce Soprano , Ac pani 
 atl e , ; QR ™ > | el f olit el lish r h , ( sermat 
 7 ERICA LOSH . } ment Violin Cello , Cnglish , French , Germa 
 " step rapid Reading . Ar 1 original simple colour | |PUBOIS , Tu.—Mass . | } xed Voice Vocal Score 
 scheme appez al children . | Organ Accompanime P 
 ‘ o conan | SCHILLINGS , MAX.—Op Jung Oluf . Musical Recita 
 ' w * rice Shillings Sixpence tion . ( Ger und English W » W Orchestra 
 — _ — — P forte mt men 
 = Lo : Novetto anp Company , Limited . 
 SCORES ORCHIIESTRKAL PARTS 
 M FRE ‘ NCH | pt BOIS . Tu.—Fantasietta . | Orchestra . Score 
 gw et : Orchestral Parts 
 MUSICAL DICTION ‘ pero 
 GLUCK.—Four Piece zed String Orehe Marck 
 ORTHOLOGIC METHOD ACQUIRING Fie Sewn : § ee Se ee oe ee 
 > _ — — Sal e ( Ar ys Sicili¢ ( mide > e 
 PERFECT PRONUNCIATION SPEAKING 
 ESPECIALLY SINGING | SAIN SAENS.—S M S 
 sabe FRENCH LANGUAGE 
 ~ = . . | NOVELLO CATALOGUES , la HARMONIU M MUSIC , — 
 4 ’ ’ : os Catalogue contains , publications Novello & 
 SPECIAL USE ENGLISH - SPEAKING PEOPLE . Co. , Ltd. , Choice Collection Foreign Publications , im- 
 ported s Novello & Co. , Ltd. ve post - free 
 application 
 ' ‘ THURWANGER — — LOS C — ALOG U ES , lt oo Ha agg 
 esa utalogue contains ¢ election - bes OTKS , 
 ntal Oe : Paris written prominent Foreign Composers . 
 xercises—@ Micier d ' Ac . adémie ; Instructor French Language Diction wo — m ly pu lished abr ad , — . 
 urces 0 New England Conservatory Music , Boston , Mass. Novell » . , Ltd. post - free application , 
 ; rag List New Foreign Publications , March , 1911 , April , 1914 
 Price Shillings . reprinted Musical Times , free applic ation . 
 _ — London : NovELto anp Company , Limited . Lonpo . NOVELLO COMPANY . Limitep 

 MUSICAL TIMES.—Juty 




 I9T4 . 495 


 FINEST ENGLISH 


 4 ~ Y | 


 CHAPPELL PIANOS 


 EXCELLED 


 CHAPPELL PLAYER - PIANOS 


 CONTAINING 


 MODERN IMPROVEMENTS EXPRESSION DEVICES . = 


 VOLE AGENTS 


 NOVELLOS MUSIC MILITARY BAND 


 SKBAR 


 PARA 


 LIMITED 


 NOVELLO COMPANY 


 LONDON 


 _ EXTRA SUPPLEMENT . 


 O LAND 


 FEAR 


 HARVEST ANTHEM ( S.A.T.B. ) PARISH CHOIRS 


 COMPOSED B 


 ) — ( GF = + — — 


 SS 


 


 


 FEAR , 0 LAND 


 = SS SSS TESS ESSE HSS 


 IG 


 FKAR , 0 LAND 


 SS 


 LL 


 FEAR , O LAND 


 _ — — — _ - — @ - — — — _ 


 FEAR , O LAND 


 ( = = = SS . SS = — — — 4 


 4 — Y 


 SS SS 


 FEAR , O LAND 


 — _ — _ — — — = : = = = 


 FEAR , 0 LAND 


 S=—= F S _ SES ES 


 FEAR , O LAND 


 PARIS OPERA HOUSE