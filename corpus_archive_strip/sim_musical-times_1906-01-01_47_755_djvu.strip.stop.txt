


 306 


 XUM 


 MUSICAL TIMES 


 FOUNDED 1844 


 PUBLISHED MONTH 


 ROYAL ALBERT HALL . LONDON 


 CHORAL 


 ' GERONTIUS . ’ 


 MENDELSSOHN 


 HYMN PRAISE . MR . FFRANGCON - DAVIES 


 MADAME SOBRINO 


 MR . FRANCIS HARFORD . SAINT - SAENS OPERA 


 -ARRY 


 ; NEW YEAR DAY CONCERT 


 UNIVERSITY DURHAM 


 PRIZE GLEE COMPETITION 


 GUILDHALL SCHOOL MUSIC . 


 KU 


 ( O S \ W 


 H. SAXE WYNDHAM BOHE 


 ROYAI GREAT 


 HUGO 


 SUCC 


 1906 


 GORLITZ , 


 IND STREET , LONDON , W. 


 BELIK SEASON , 109 


 KUBELIK TOUR 


 IIAN STRING QUARTET 


 ESS JOHN HARRISON 


 GREAT ENGLISH TENOR . 


 MISS HOPE WARWICK 


 | , LL.D MISS JOAN ASHLEY 


 } ( \.M MR . JOHN HARRISON 


 ) > NCORPORATED GUILD C TR¢ 


 MUSICIANS 


 VAL AC EMY MUSIC ROYAL COLLEGE O ! , 


 MUSIC LOCAL EXAMINATIONS MUSIC . 


 , ; LONDON 


 BOARD EXAMINATION 


 N WV GEO . A. STANTON , F.1.G.C.M. , S 


 


 


 MUSICAL 


 MORECAMBE - PROFESSIONAL NOTICES . 


 MUSICAL FESTIVAL 


 \ CATORS : ‘ HANGE ADDRESS . 


 . M " " 


 


 HERR HEINRICH DITTMAR 


 RECITATIONS PIANO . 


 — — ; MR . HENRY BEAUMONT 


 GREAT ENGLISH TENOR 


 + ( N 


 MR . C. W. FREDERICKS 


 TENOR VOCALIST 


 ( H ] } M 


 MADAME , LESLIE DALE ALEXANDER , WEBSTER 


 MISS CL ARICE LAUGHTON MR J HN BR VNI 


 XUM 


 MUSICAL 


 TENOR ) 


 BRISTOL FESTIVAI 


 MR . VIVIAN BENNETTS 


 ( BARITONE ) . 


 ; MR . JAMES COLEMAN 


 F ; ( 


 MR . HU GH SPE NC ER 


 , 1906 . 5 


 MR . ALEXANDER _ TUCKER 


 MUSICAI 


 SU NM . \N 


 


 HENRY 


 BASS 


 MR 


 MR . 


 DUTTON SOLO BOYS 


 MR . W. H. GRIFFITHS , 


 PI R LECTURER VOCAL SCIENCE 


 CONS TATION LESSONS VOICE PRODUCTION 


 PHOMAS W. BRITTON 


 N - HARDY MANCHESTER 


 EXPERT EXAM . COACH . 


 HAKP TEACHING . 


 MADAME PRETORIUS 


 \CHEI | \ 


 N H H ( H 


 INED DEGREES MUS.D MUS.I 


 ( 1 ) | LD ) 


 ( ( \I S 


 N M 


 PEVISION MUSICAL COMPOSITIONS . 


 F.R.C.O 


 PREPARING 


 M R. HERBERT F. ELLINGFORD , 


 JOICES TRAINE :D 


 COMPOSITION 


 1 MONY , COUNTERPOINT , DOUBLE ' COUNTERPOINT 


 EX 


 SG " ! 


 HOI 


 VER 


 BLY 


 T UNE 


 FORTE TI 


 XUM 


 - FIELDS , W.C 


 ORGANIS1 


 COACH , 


 BASS 


 - ENGAGEMENT 


 WANTED 


 O. ¢ HOIR E XC HANGE 


 DINHAM , 


 LATING 


 


 UPRIG HT PIANOS 


 TIOLONCELLO 


 SALE . 


 MODEI 


 SHORT GRAND 


 VIOLIN 


 | , | > -T ) SOUIES OSSCS 


 XUM 


 MUSICAL 


 JANUARY 1 


 1906 


 LEEDS PARISH CHURCH . 


 XUM 


 0 


 H 


 , S 


 D 


 LEEDS PARISH CHURCH : 


 EAST END 


 ELECTION ORGANIST 


 ORGAN , 


 PAR 


 OPENING O 


 CHURCH 


 LEEDS PARISH CHURCH 


 XUM 


 LOOKING WEST 


 TIMES O 


 BAIRSTOW 


 AY CHOIRMASTER LEEDS ARISH CHURCH 


 MUSICAL TI 


 ES.—JANUARY 1 , 1906 


 MR 


 HENRY C 


 EMBLETON 


 REQUIEM BRAHMSe felt H platfor response , , friendly ) Marie . English musician , , present 

 calls . , Hanslick , severe critics , | initial performance , person late 
 uiem [ § warmly greeted work , portion | John Farmer . largely - attended supper 
 ords , @ presented . judged be|the concert Mr. Farmer _ unfortunate 
 quote Miss book ) ‘ the| impromptu speech , belittling musical interests 
 1 ripest fruits domain sacred music,|of country , adding , Brahms 
 ding BH developed style Beethoven late|(who present ) came England perform 
 works . . . . harmonic contrapuntal art| Requiem , Englishmen , ‘ _ 
 learned Brahms school Bach , and| fellow crazy ? ’ exhibition bad taste 
 23:8 inspired living breath } drew company , Herr Lehmann , 
 present , forgotten expression of|the following spontaneous remarks way 
 touching lament , increasing annihilating | rebuke : 
 i. death - shudder . ’ Hanslick goes rebuke | venture , neverthless , word 
 ‘ half dozen gray - haired fanatics old England honour . artists met 
 school ’ hissed Brahms , stigmatizing| encouraging reception found happy home 
 ebullition bad feeling ‘ requiem ; Englishmen understand 

 rs sympathize German art German life , 
 decorum good manners Vienna * oat E sale P mage 
 . beg leave propose glass honour 

 performance complete work took 
 place Gewandhaus Concert , Leipzig , 
 February 18 , 1869 , direction § 
 Carl Reinecke 

 let turn England . regard 
 introduction country , Brahms Requiem 
 shared fate Beethoven Mass D , 
 performed /frivate 
 stupendous creation Christmas Eve , 1832 , 
 set forth Musica Times April , 1902 , 
 p. 236 . happy thought 
 late Lady Thompson ( 2ée Miss Kate Loder ) , wife 
 eminent surgeon , Sir Henry Thompson , 
 Bart . , arrange performance Kequiem 
 Brahms drawing - room 35 , Wimpole 
 Street , Marylebone . took place July ro , 
 direction Julius Stockhausen , 
 trained chorus sang baritone 
 solo . chorus assisted interesting 
 included - known names 
 Macfarren , Miss Macirone , Mrs. Ellicott 
 late Bishop Gloucester ) , Miss 
 Sophie Ferrari ( Mrs. Pagden ) sister , | 
 Miss F. J. Ferrari , Canon Duckworth , Mr. 
 William Shakespeare . Madame Regan - Sc himon | 
 sang soprano solo , English version 
 text 

 available space room _ ] 
 allow orchestra , accompaniments , 
 - hand arrangement composer , | 
 adapted played pianoforte ] 
 Lady Thompson — excellent | 
 pianist thorough musician — old Cipriani | 
 Potter , eightieth year . 
 Macfarren , audience , recording 
 performance * says lhe audience aglow 
 interest work rendering : , 
 conspicuous assembly , small 
 ’ aged musician | Potter ] , dearly 




 MOUSE 


 15871on brink eternity,+ 

 present treasure memory day 
 Potter 
 presence , memory music _ 
 musician inseparable . ’ specially interesting 
 recall fact Potter , took 
 prominent performance 
 Requiem Brahms , knew Beethoven Vienna . 
 tell Beethoven invited 
 occasion dinner rhey sitting table 
 certain dish served Beethoven 

 d m ordered seized 
 nordered dish threw servant head . 
 rhe result display Beethoven spleen 
 vas meal bread cheese , Potter 
 enioved tl company ? lo 

 turn Requiem Lady Thompson house . 
 Lady Macfarren , recalling event 
 nterval thirty - years , specially 




 TIMESSextets , Quartets , concerto 

 productions ; 
 extensive work , German Requiem , 
 known , 
 England feel , , art 
 living representative , greatest masters 
 successor , line Purcell , 
 Handel , Bach , Haydn , Mozart , Beethoven , 
 great men shone 
 blazing transcendent light , 
 extinct 

 lovers music 




 E.G. E 


 MIDDLE TEMPLE MASQUE 


 S17 


 _ 7 : AIA LTT 


 1612 


 IOI ! 


 25 


 VANDALISM HANDEL HOUSE 


 ALGERNON ASHTON . 


 RST CONCE 


 MUSICAL TIM 


 ES.—JANUARY 1 , 1906 


 IOZART SYMPHONY C ( JUPITERBy SIR GEORGE GROVE , C.B 

 sobriguet * Jupiter , bestowed 
 iis noble work late John Cramer 
 ndividual , expresses estimate forme 
 eneration calm , lofty , 
 god - like beauty held world instrumental 
 music . ‘ dethroned position 
 * Eroica ’ symphonies Beethoven 
 regular order nature 
 Jupiter dethroned — 
 Greek religion Greek art given way 
 Christianity . Jupiter head 
 Olympus , Parthenon noblest building 
 ancient world , notwithstanding Rheims 
 ithedral Westminster Abbey ; ‘ Jupiter ’ 
 Symphony greatest orchestral work 
 world preceded French Revolution . 
 ay sweetness E flat Symphony , 
 passion G minor , larger , broader , 
 grander . greatest , 
 great trilogy 
 Mozart immortalised months June , July 
 August , 1788 . 
 pardoned fondly recalling 
 e extraordinary fact masterpieces 
 ‘ h crown Mozart labours composition 
 symphonies , 47th , 48th , 49th list , 
 written period seven weeks . E flat 
 completed ( dates Mozczart 
 n autograph catalogue ) June 26 , G minor 

 

 y 25 , ‘ Jupiter ’ August 10 . mere 
 ength great works suffice 
 e fact astonishing , contents 
 ire remembered espe ally 
 t \ 
 M P 

 truly extraordinary . , memorable feat , 
 Mozart . relinquished highest form 
 orchestral composition , years 
 months elapsed date quoted 
 December 5 , 1791 , breathed 
 world , known explain . 
 hardly busy , 
 prevented /¢Aaf cause 
 creating work , great small , 
 mind . © intervals , , found 
 career composers . Beethoven , 
 producing symphonies 
 years tolerable regularity , rested 
 labours complete ninth 
 years later . Schubert 
 . symphonies belong years 
 1813 1822 ; gap 1822 
 1826 , year Azs ninth completed 
 Gastein , handed Austrian Musical 
 Society , curious freak fortune 
 manuscript time disappeared 

 rhe ‘ Jupiter ’ appears catalogue compiled 
 Dr. Ludwig Ritter von Kéchel . 551 
 complete works . autograph written oblong 
 paper , ninety - pages staves page . ” 
 rhe orchestra usually employed Mozart 
 Symphon es , clarinets trombones 




 5 - 4 14 - 4 2 - 44 - 21 4 


 MUSICAL 


 TIMES 


 JANUARY 1 , 1906 


 TE |S EPThe conclusion 

 frequent Beethoven Mendelssohn , rarely 
 met Mozart , probably owing quantity 
 wrote , rapid rate works followed 
 , allow time 
 finished . bars 
 movement originally seven ; 
 struck Mozart , replaced 
 beautiful return 
 rinal subject ( . 9 ) fascinating . 

 S 

 new feature , equally innovation 

 anticipating principal characteristic Beethoven 
 treatment Symphony , Coda 
 /7e / e concludes , learning 
 contrivance portion summarised 
 condensed , , possible , surpassed . Codi 
 - bars long . starts 
 quasi - inversion subject follows : 
 ra " = > 
 
 ev 

 worked bars . 
 begins — found body 
 movement , notwithstanding contrivances 
 employed — regular strict fugue , lasting 
 exactly thirty bars , subjects ( 
 fifth till subordinate ) brought different 
 relations closer combinations , 
 effect weld structure 
 firmly everlasting monument 
 symmetry beauty . force 
 genius wonderful man habitual 
 mastery technicalities art , 
 elaborate contrivances obtrude 
 injury poetry spirit composition , 
 brilliant , graceful , forcible 
 composer unfettered . Think 
 union invention , skill , practice , resolution 
 required imagine work 
 ; paper , 
 , state played 




 TUNE ST . MAGNUS UNFORTUNATI 


 COMPOSER , JEREMIAH CLARKE 


 _ — _ 


 € 29 


 SSS 


 34 


 ANTIGUA 


 100F 


 MUSICAL TIN 


 LES.—JANUARY , 1906 


 GLASGOW UNIVERSITY 


 16 


 LEIP DGTS 


 SLL S 


 ORGAN RECITALS 


 - SONG . 


 4 ¥ 4 


 4 


 XN J 


 7 ! 7 


 FES 


 STRAY NYMPH 


 NAR SIPS 3 


 7 4 


 SS 


 2225 


 DELIA , STRAY NYMPH 


 7 - 0 | 


 PS 


 PN 


 “ ENS 


 19 


 NN } 


 4 4 “ | 


 DELIA , STRAY NYMPH 


 NN 


 NN 


 NS STI 


 PP 


 MUSICAL 


 SYMPHONY 


 SCHI * RHENISH 


 THI 


 MANN 


 PROFESSOR 


 OLD ORGAN - PIPES 


 IMPOSITION 


 TIMES 


 


 HARD CASE , B 


 EDITOR ‘ MUSICAL 


 RICHARD STRAUSS 


 ( SPECIAI 


 SALOME ” ’ DRESDEN 


 CORRESPONDENT . ) 


 


 MUSICAL TIMES 


 MUSICAL ASSOCIATION 


 MR . CECIL SHARP FOLK - SONGS 


 PATRON FUND CONCERT 


 MUSICAL 


 SCHOOL CONCERTS 


 UPPINGHAM 


 PUBLIC 


 COLLEGE 


 CLIFTON 


 BERKHAMSTED . CRYSTAL PALACE CONCERTS 

 Saturday afternoon concerts Crystal Palace 
 comment . December 9 , Mr. Walter Hedgcock 
 conducted London Symphony Orchestra Crystal 
 Palace choir . Crisp expressive performances given 
 Sullivan ‘ Macbeth ’ overture Beethoven pianoforte 
 Concerto C minor , Miss Norah Drewett soloist ; 
 choir sang admirable precision Dvordak 
 picturesque cantata ‘ spectre bride . ” soloists 
 Miss Perceval Allen , Mr. Lloyd Chandos Mr. Thomas 
 Meux 

 December 16 London Symphony Orchestra played 
 direction Mr. Landon Ronald , 
 programme contained Tchaikovsky ‘ Pathetic ° Symphony 
 Schumann pianoforte Concerto , Miss Fanny 
 Davies soloist jay exce//ence work , 
 small , highly appreciative audience assembled . 
 novelty programme symphonic poem 
 entitled ‘ St. George , Mr. George Dorlay , , 
 judging music , disciple Richard Strauss . 
 wiil surmised warlike tendencies 
 St. George dwelt unsparing vigour 
 brass called emphasise exploits 
 Mr. Dorlay discretion great valour 
 work coldly received , 
 manifestly lively imagination 




 ‘ DREAM GERONTIUS ’ CARDIFF 


 BOOKS RECEIVED 


 46 MUSICAL 


 , 1906 


 ROYAL CHORAL SOCIETY . 


 EEN HALL ORCHESTRAMiss Nora McKay , clever young Australian violinist , 
 exceptionally favourable impression 
 sensitiveness brilliancy playing Bechstein Hall 

 December 1 . programme included   Corellis 
 ‘ Variations Sérieuses , ’ Max Bruch ‘ Scotch ” Fantasia , 
 Bach Sonata G minor , Beethoven Romance G. 
 Miss McKay interpretations testified artistic intuition n 
 reliable technique 

 Miss Neili Fraser sang finish refinement 
 recital , Bechstein Hall , December 1 contralt 
 voice produced manifestly dominated musical 




 RICHMOND 


 MUSIC VIENNA . 


 MUSICAL 


 1906 


 MANDYCZE 


 SKI 


 MUSIC BIRMINGHAM 


 1S05 


 MUSIC BRISTOL 


 OU 


 DISTRICT . 


 CORRESPONDENT 


 


 MUSIC DUBLIN . 


 ( CORRESPONDENT 


 MUSIC EDINBURGHAt Mr. Chollet chamber concerts , given 
 Freemasons ’ Hall November 28 , performed Brahms 
 Sonata pianoforte violin major , played 
 Mr. Arthur W. Dace Mr. Chollet , Miss Young 
 Scott sang artistically 

 concert fully justified expectation 
 Edinburgh String ( Juartet-—Messrs . Colin Mckenzie , 
 J. H. Hartley , R. de la , D. Millar - Craig — 
 experienced - equipped musicians ; 
 readings quartets Haydn , Beethoven 
 reached high level merit 

 Empire Theatre filled 
 December 3 , orchestral concert organized 
 benefit local branch Amalgamated Union 




 MUSIC GLASGOW 


 CORRESPONDENTTchaikovsky Trio minor pianoforte , violin 
 violoncello , ensemble - nigh 
 perfect . Miss Neil Fraser , possessor contralto 
 voice wide compass , contributed songs Brahms , Strauss , 
 Schubert , accompaniments sympathetically 
 played Mr. A. Scott Jupp 

 Choral Orchestral Union concerts « 
 run successful course . classical 
 November 28 , Miss Fanny Davies gave brilliant reading 
 Saint - Saéns pianoforte Concerto . 2 G minor . 
 symphony Beethoven . 2 , novelty , * Gretchen , ’ 
 Liszt ‘ Faust ’ Symphony favourably received . 
 conducted bv Herr 

 onuunue 

 oncert 

 fourth concert December 5 
 von Hausegger , selected programme familiar 
 works . main items Beethoven ‘ Eroica ’ 
 Symphony , overtures ‘ Der Freischiitz , * Flying 
 Dutchman ’ ‘ Tannhauser ’ prelude ‘ Lohengrin . ’ 
 Wagner numbers distinguished conductor secured 
 results . novelties given fifth 
 concert December 12 , viz . , Liszt symphonic poem 
 ‘ Tasso ’ onde Serenade ( violins ) 
 Brahms . programme likewise included Elgar 
 ‘ Enigma ’ Variations , organ played 
 Mr. Berry , Tchaikovsky ‘ Nutcracker ’ suite . 
 enjoyable feature concert beautiful 
 singing Miss Antonia Dolores 
 Associated Scottish Orchestra , Choral | 
 December 19 

 best 

 MUS 

 Apostles , ’ marked great intelligence / addition Bach ‘ Chromatic Fantasia ’ ; Mr. Pep Special 
 ent . orchestra played splendidly , Mr. Berry | Grainger , gave Tchaikovsky Ist Concerto , aj Davies 
 gave organ excellent effect . solo vocalists | Mr. Egon Petri , performed Beethoven E flat Concer ind Gates 
 Misses Jenny Taggart Mabel Braine , Messrs. | Liszt paraphrase ‘ Rigoletto . ’ able 
 Harrison , Herbert Parker , Robert Burnett miscellaneous choral experiment th impressiv ' 
 Borwell , Mr. Burnett meriting special mention | Gentlemen Concerts November 28 , newh hegutiful 
 ly dramatic rendering Judas . | formed Madrigal Society voices Hall : MM gne choru 
 Bradley directed performance . \propos |Choir , Mr. R. H. Wilson , sang series olf notew 
 t concert auspices local | madrigals lighter orchestral selections . [ wilson $ 2 
 vorated Society Musicians , Dr. Keighley , | concert November 25 Brodsky Quartet th Everymar 
 chester , delivered semi - public lecture the/| programme contained Cherubini fine Quartet E fla Madame K 
 xemplified ‘ Apostles . " lecturer | Schumann pianoforte Quartet key ( Op . 47 ) , ani ‘ Song } 
 clear concise analysis copiously illus- | Beethoven Quartet F minor ( Op . 95 ) . Miss Adeh Mi ( Madame 
 t orte . Verne betrayed good gift ensemble playing iM overture c 
 uturday Popular Concerts continue attract|connection Schumann quartet . Brodsk Phi 
 crowded audiences . ‘ performances ” | ( Juartet engaged concert Athenzun ff Mendelssol 
 Bord ballad orchestra Mr. Learmont | Chess Club , playing Beethoven Quartet G ( Op . 18 , . 2 Cowen 
 Drysdale , German ‘ Welsh Rhapsody , Ambroise | Mendelssohn Quartet E flat ( Op . 12 ) , Schumann ’ [ & choir n 
 ihoma wverture ‘ Summer night . ’ ( Quartet ( Op . 41 ) . season , alt 
 : lasgow Amateur Orchestral Society concert Mr. Brand Lane subscription concert o Dodds con 
 season took place December 20 . Novelties | December 13 , artistically satisfying tha [ % Miss Agn 
 f Stanford prelude ‘ ( Edipus Rex ’ andj unaccompanied singing , Mr. Lane Philharmonic Decembet 
 orchestral accompaniment ‘ Witch | Choir , little vocal gems Elgar ‘ Weary wind B second cot 
 read Miss Agnes Bartholomew ) | West ’ Brahms ‘ Dim - lit woods . ’ 
 programme likewise included new Promenade Concerts , attraction [ % somewhat 

 

 prelude act } band drawn Hallé Orchestra , continuet Catholi 
 s Suite orchestra ( Op . 49 ) , | gather public support . instrumental selections th J Mr. 
 music ‘ Der Daemon . ’ | concert December 9 largely drawn Tchaikovsky Tyne Thea 
 l performance . | works — ‘ Casse - Noisette ” Suite , Elegy strings , ‘ cl 
 { Scherzo 4th Symphony , ‘ 181 ‘ Hiawatha 
 joverture . Beethoven Society opened eighteenth JB Society 
 season December 5 , programme contained ‘ John Gil 
 LIVERPOOL DISTRICT . ‘ ballet Prometheus , ’ Tchaikovsky Elegy Shields , « 
 strings , ‘ Hansel und Gretel ’ prelude , overture Anderton ’ : 
 ‘ Euryanthe . ’ Mr. Gordon Cockrell conducted . Mis December 
 adies ’ concert Orchestral Society , } Jessie Morris , student College Music 
 s invested unusual interest account | delighted audience spirited violin playing . Decem 
 f Mr. Sibelius , Finnish composer , Schiller - Anstalt concert , November 24 , Mr. flut 
 Symphony . 1 E minor , | Edward Isaacs contributed original pianoforte compos Arlom 

 oem * ‘ Finlandial . ’ remainder the|}tion inappropriately designated ‘ Etude Dec 
 luded Dvorak ‘ Carnaval ” overture ! Variations ’ ; variations , number , Philosoph : 
 “ march . Miss Amy Castles sang original theme , providing exercises separate details * Musical 
 | technique execution , self - contained , 
 December 5 , Miss Adela } coming close key , leaving succeeding 
 Paderewski ‘ Polish’| variation start inanother . variations / leg 
 character , expected , mos 




 R N ES NDENT 


 MI 


 1906 


 MUSIC NEWCASTLE DISTRICT . 


 ( CORRESPONDENT 


 MUSIC NORWICH DISTRICT . 


 ( CORRESPONDENT 


 NOTTINGHAM DISTRICT . 


 ( CORRESPONDENT 


 MUSICThe Grantham Philharmonic Society opened season 
 November 28 performance Handel ‘ Samson . ’ 
 solos entrusted Miss Winifred Siddons , Miss 
 Emily Owen , Mr. Harry Stubbs , Mr. E. W. Jones . 
 Mr. H. P. Dickenson directed efficient chorus 
 orchestra 

 orchestral concert Nottingham Sacred 
 Harmonic Society took place November 30 . 
 programme opened good performance 
 ‘ Oberon ’ overture , included Schumann Symphony 
 B flat , Elgar ‘ Bavarian Dances , ’ Wagner 
 ‘ Kaiser ’ March . performance decided success ; 
 band good work , great credit 
 Mr. Allen Gill , conductor . Mr. Lyell Taylor , 
 solo violinist , gave good performance Beethoven 
 ‘ Romance ’ G. Miss Lillie Wormald , capable 
 vocalist , heard best ‘ Couplets du Mysoli ’ 
 ( David ) , supported Mr. F. Warren flute 
 obbligato 

 Mr. Arthur Richards gave orchestral concert 
 season December 9 . programme included 
 Sullivan ‘ Memoriam ’ overture , Grieg suite , ‘ Sigurd 
 Jorsalfar,,5 Handel organ Concerto . 2 , Mr. 
 Herbert Richards work solo instrument last- 
 named work particularly good . Mr. Charles Keywood 
 vocalist , Mr. Eric Coates ( viola ) instrumental 
 soloist . Miss Edith Burgis led orchestra , Mr. 
 Richards conducted care precision 




 OXFORD . 


 CORRESPONDENT 


 MUSIC 


 OWNTHE MUSICAL TIMES.—January 1 , 1906 

 Town Hall , November 28 , Oxford Gleemen 
 seventy voices ) , 
 Wilsdon , gave thoroughly excellent 
 gramme remarkable interest , 
 lid Brahms ‘ Rinaldo ’ ; Schumann 
 * orchestral accompaniment , 
 1oven eighth symphony . ‘ Rinaldo , ’ told , 
 score years Christ 
 small chorus , pianoforte 
 ose days accompaniment . 
 -cially Oxford - folk , centres 
 hich attention called 
 26 . Ihe performance 
 le admirable , soloists , 
 ley , deserving praise . 
 ling , auspices 
 artet ( assistance 
 Dr. Walker _ pianist ) 
 programme opened 
 chel , 581 ) , Brahms 
 ) noforte clarinet 
 Iker Mr. Draper . 
 lin Concerto minor , 
 Beethoven ( Quartet 
 add Sunday 
 ave conti ! 
 Walker 

 SIC SHEFFIELD DISTRICT 




 MUSIC SOUTH - WEST ENGLAND 


 ( CORRESPONDENT 


 EVONSHIRE TOWN 


 CORNISH TOWNS 


 YORKSHIRE 


 CORRESPONDENT 


 MUSIC 


 


 LEEDSTwo orchestral concerts , good 

 kind , presented features familiar . 
 subscription November 29 , Dr. Richter con 
 jucted programme Beethoven Wagner 
 st enjoyable , course peculiarly 
 suited display powers utmost advantage , 
 consisted things thrice familiar 
 British public . ended ‘ Eroica ’ Symphony , 
 way celebrating centenary performance 
 Municipal Orchestra , deserves 
 Leeds public , gave Tchaikovsky programme December 
 9,when Mr. Fricker conducted performances ‘ Pathetic 
 Symphony , ‘ Capriccio Italien , ’ ‘ Casse Noisette ‘ Suiteand 
 ‘ Marche Slave , ’ reflected credit excellent 
 hestra got . great privilege 
 people Leeds able hear 
 programme , adequately rendered , prices ranging 
 2d . 1 Mr. Dillon Shallard vocalist 
 casion . Municipal concert November 25 
 chamber music , Schubert Octet Beethoven Septet 

 concert 




 OTHERThe principals Messrs. Evan Williams 
 Herbert Brown , performances characterized 
 brilliance . Bradford Permanent Orchestra , 
 November 25 , gave , help Festival Choral 
 Society , mixed programme orchestral choral music 
 British composers , Mr. Allen Gill direction . 
 Compositions Stanford ( ‘ Revenge ’ ) , Cowen , Elgar 
 German included , examples local 
 art shape Mr. J. Weston Nicholl ‘ Bavarian 
 Eclogue , ’ suite Mr. Haydn Wood 

 Huddersfield busy late , 
 said interest recent concerts 
 marked . Philharmonic Society gave popular 
 Saturday evening concerts December 2 , 
 Beethoven compositions interspersed light music . 
 Beethoven’s - violin Concerto , Mr. T. H. Clay soloist , 
 central feature concert , conducted 
 Mr. Ibeson . December 5 Glee Madrigal 
 Society , Mr. J. W. Armitage , gave concert . 
 singing chorus excellent , music 

 undistinguished , chief exception 
 H. Lloyd ‘ Rosy dawn , ’ admirably sung , 
 proved enjoyable . Lockwood District 
 Choral Society , direction Dr. A. Eaglefield 
 Hull , creditable performance Mackenzie 
 ‘ Dream Jubal , ” December 4 , Mechanics ’ 
 Institute . solo vocalisis Miss Emily Cox 
 Mr. Walter Lawley , Mr. Charles Fry appeared 
 reciter 

 Halifax Choral Society gave , November 23 , 
 refined sympathetic performance Brahms ‘ German 
 Requiem , ’ Mr. F. de G. English . chorus sang 
 difficult music marked intelligence praiseworthy 
 accuracy , solos sung artistically 
 Miss Evangeline Florence Mr. Frederic Austin . 
 December 6 Orchestral Society , 
 Mr. J. B. Summerscales , played new suite , ‘ Idylls 
 Spring , ’ Mr. F. Davidson , local composer . Beethoven 
 7th Symphony principal feature programme . 
 date Cleckheaton Philharmonic Society 
 gave ‘ Elijah , ’ Mr. W. H. Wright conducting satisfactory 
 performance . Molique ‘ Abraham ’ heard 
 , Pudsey Choral Union gave , 
 time , November 27 , Mr. H. H. Pickard . 
 portions Coleridge - Taylor ‘ Hiawatha ’ 
 sung admirable spirit verve Morley 
 Choral Society , Mr. Fricker conductorship , 
 December 7 , occasion Schumann piano- 
 forte Concerto played refinement sympathy 
 Mr. Herbert Johnson , gifted young pianist town 
 Wakefield chamber concerts , December 19 , 
 Miss Agnes Nicholls gave delightful vocal recital , 
 aid Mr. S. Liddle accompanist 

 York Symphony Orchestra , 
 Mr. Noble guidance notable advance , 
 attempted exacting work Beethoven 7th 
 Symphony November 23 , got task 
 credit . York Musical Society chorus aftorded 
 wholesome example societies submitting t 
 weeding - process . assistance Mr. Fricker , 
 Leeds , efficient members Leen eliminated , 
 judge f purged chorus 
 Coleridge -Taylor ‘ Hiawatha wedding 

 

 Hull Vocal Society November 21 , 

 coupled Elgar ‘ Black Knight . ” Dr. G. H. 
 December 1 , Hull 
 Society , Mr. J. W. Hudson conducts , 
 programme , including Beethoven 
 Mr. Patman clever ‘ Cinderella 

 




 1S 


 \THENS 


 BERLIN . 2nd 

 works Beethoven 

 armonic 
 H. Cowen , 
 rk 
 Dr. Edward 
 ingari 
 Philharmonic Choir , 
 choir - trainer , Prof. Siegfried 
 considered finest performance 
 | 
 : virtually 
 seeing Prof. 
 training stupendous 
 Royal Opera Choir , 
 ted Berlioz ‘ Childhood 
 Olives , ” works 
 Germany . — 
 Schumann , gave 
 ‘ Funeral song ’ 

 chorus , strings brass ( Op . 13 ) , ‘ song fates 

 Nenie ’ ‘ Requiem . ’ fourth Philharmonic 
 Concert , Arthur Nikisch direction , devoted 
 Beethoven — ‘ Fidelio ” ‘ Leonore ’ ( , 3 
 overtures , C minor Symphony , G majo , 
 pianoforte Concerto , solo - named , 
 superbly played Eugen d’Albert , completed scheme 

 new opera - house , Die Komische Oper , opened 
 November 17 . implies , building js 4 
 devoted light , necessarily comic , opera 
 director Herr Hans Gregor , Offenbach ’ 
 * Contes d’Hoffimann ’ work cater 
 Berliners ’ taste ‘ light ’ music 




 BIELEFELD 


 BREMEN 


 BRUSSELS 


 COLOGNE 


 CREFELD 


 DESSAU 


 MUSICAL 


 , 1906 . 55LEIPZIG 

 Max Reger gave concert entirely 
 ms November 19 , great sets 
 ff Variations — pianoforte solo theme Bach 
 Op . 81 ) , pianofortes , hands , 
 theme Beethoven ( Op . 86 ) — Pieces 
 Pittoresques pianoforte duet ( Op . 34 ) , number 
 songs , greatly appreciated friendly audience . 
 _ — Max Pauer played Brahms pianoforte 
 sonatas recital day following 
 rder , viz . , F sharp minor ( Op . 2 ) , F minor ( Op . 5 ) , 
 C major ( Op . 1 ) . 
 Brahms cult Germany , programme 
 Herr Pauer satisfied ravenous 
 appetite great master music 

 hieriot , assistance Winderstein Orchestra , 
 
 new symphony ( . 3 , C major ) , overture 
 ‘ Dionysia , ’ concerto pianofortes orchestra 
 Op . 77 ) performed.——At seventh 
 concert , November 30 , Arthur Nikisch conducted Elgar 
 ‘ Enigma ’ Variations , means brilliant 
 performance helped work emphatic success . — — 
 December 3 - act opera , founded Tennyson 




 MILAN . 


 PARISdies tl 
 
 M. Colonne 

 Herr Burgstaller sang Beethoven song - cycle , ‘ 

 ntfernte Geliebte .. ———At Lamoureux concert 
 November 26 , M. Chevillard conducted Balakirev symphonic 
 poem ‘ Russia , ” succeeding concert ( December 3 ) 
 introduced new symphonic poem , ‘ Quasimodo , ’ 




 PRAGUE 


 WIESBADEN 


 BRIEFLY SUMMARIZED 


 MUSICAL TIM 


 CHELMSFORD 


 1 ‘ 1 


 ES.—JANUARY 1 , 1906when programme included Mendelssohn ‘ Italian 
 Symphony , Avdante Schubert Symphony C 

 Beethoven pianoforte Concerto C minor , whic 

 Dr. 
 performances 




 ST . 


 SEVENO 


 SLOUGH 


 SUTTON 


 WALSAI 


 


 R. FORGANO PLENO.—‘The correct way 
 obtaining post staff musical 
 papers — preferably weekly — writer critiques 
 notices concerts , recitals , & c. , ” , regret 
 unknown . events ¢z#correct 
 send specimens work Editors papers 
 , weekly daily 

 J. C. B.—(1 ) sonatas Domenico Scarlatti 
 refer included volume sonatas 
 issued Messrs. Breitkopf & Haertel , . 20 , 
 fingered editions sonatas , edited 
 respectively Tausig , Longo , Czerny , Biilow , Méréan . 
 ( 2 ) funeral march Beethoven 

 set 

 kK. F. C. C.—As niece , 
 appear learned music , advisable 
 attain elementary knowledge art privat 
 lessons thinks studying ‘ especially singing , ’ 
 ata foreign conservatoire 

 STrETToO.—A _ supplement portrait Beethoven 
 issued Tue Musical . TIMEs January , 1901 , 
 ( bust Schaller ) appeared special Beethoven 
 number December , 1892 . portrait Bach 
 included series 

 B. P.—Full particulars Mark Hambourg prizes 
 composition obtained donor 
 2 , Clifton Gardens , Maida Vale , London . Thanks 
 ‘ piece music ’ entitled * sea . ” borne 
 high tide success left high dry 




 


 MUSICAI 


 1906 


 CONT ENTS 


 DAVEY 


 MONTH . 


 ® E RMAN , R. F. MARTIN 


 REWER , A. HERBERT 


 ETHERSTON , REV . SIR G. R. 


 , 


 SPECIAL NOTICE . 


 VOT LATER 


 1SSUE 


 AC PHERSON , 


 JANUARY 23 . 2 


 OFMANN , H 


 UNIOR VIOLINIST 


 EAS 


 MUR 


 AKF , 


 ARR 


 = IR , 


 R ' SST 


 FLETCH = 


 CHOO 


 GOTTLIEB 


 BUFFEY . 


 CCHOO 


 GELLA 


 * IMPS\ 


 TOCK 


 PARR ' 


 ! 


 MUSICAL 


 1900 


 ARRY , C. HUBERT H. 


 CHOOL MUSIC REVIEW , Unison Song 

 Joun E. WEs1 
 Creation Hymn . Arranged 
 S.S.A. ( accompanied ) BEETHOVEN 
 Christmas Tree . Unison 
 Song PETER CORNELIUS 

 656 




 PETER CORNELIUS 


 PETER CORNELIUS 


 PETER CORNELIUS 


 PETER CORNELIUS 


 PETER CORNELIUS 


 CIM SON , F. J.- 


 PARRY , C. HUBERT 


 MONTH . 


 ONIC SOL - FA _ SERIES . 


 1\ TILBYE , JOHN — — — 


 SCHOOL MUS SIC REVIEW 


 SCHOOL MUSIC REVIEW JANUARY 


 1475 


 MUSIC REVIEW 


 SCHOOL 


 MISS ETHEL RADBURN 


 SOPRANO 


 CHAMBER MUSIC 


 MATEURS 


 MUSICAL 


 00 


 1906 


 V . F 


 BEARD 


 PIANOFORTES 


 NORMAN 


 ORGAN PEDALS 


 NEW STOOLS 


 OLD FIRM . 


 P. CONACHER & CO . , 


 SPRINGWOOD WORKS 


 HUDDERSFIELD 


 NICHOL SON YD CO . , 


 ORGAN BUILDERS , 


 PALACE YARD , WORCESTER . 


 ( ESTABLISHED 1841 


 NSCRIPTIONS ORGAN 


 EDWIN H. LEMARI 


 BARCAROLE 


 HEINRICH HOFMANN 


 C 


 MARCH 


 HEINRICH HOFMANN 


 NEW ORGAN COMPOSITION 


 INTRODUCTION , 


 


 ADESTE FIDELES ” 


 ORGAN 


 ARTHUR S. HOLLOWAY 


 MUS.D. , OXON . ) . 


 NEW CONCERT SONGS 


 DR . ARTHUR S. HOLLOWAY 


 PIANOFORTE PIECE 


 NEW 


 DR . ARTHUR S. HOLLOWAY 


 LOVE APPEAL 


 NOVELLO SCHOOL SONGS . 


 NURSERY RHYMES 


 JOHANNES BRAHMS 


 CONTENTS 


 SEVEN 


 FOU R - SONGS 


 ( MARIENLIEDER 


 ED 


 HARI 


 MUSIC LENT 


 DICITE 


 * T HI PASSION ‘ HRIST . G. _ HANDEL . 


 S \ , -ETTMAN , ED 


 NL ED 


 MSE ! : MEI , DEUS . 


 ISERERE MEI , DEUS 


 E.G 


 


 


 REV . E. MONRO 


 J. STAINER , MYLES B. FOSTER , 


 REPROACHES 


 NOVELLO ANTHEMS LENTH OL 


 TW 


 INTE 


 SHI 


 SACRED CANTATA : 


 SACRED PASSION HOLY REDEEMER 


 SAVIOUR LIFE EARTH REV . - J SPARROW . SIMPSON , M.A. 


 SOLO VOICES ( TENOR BARITONE ) 


 CHORUS 


 INTERSPERSED HYMNS 


 I. STAINER 


 APPRO ! IATE HYMD 


 W. MAURICE AD . \MS 


 ERSE 


 WORDS SELECTED WRITTEN 


 SH : \P COTT WENSLEY THOMAS ADAMS 


 NEARER , GOD , THEE 


 , 4 CHURCH CANTATA 


 MUSIC 


 PI 


 RDS WRITTEN COMPILE 


 SI 


 1904 . 


 


 LEE \\ IL L L. AMS 


 BIRMINGHAM POST = ARTHUR E. DYER 


 1904 . REV . E. V. 


 PEACE 


 BIDE 


 GOD 


 GOD 


 TH 


 JESUS 


 PRAIS 


 LENT GOOD FRIDAY . 


 ’ 


 COME 


 JESU , P 


 SING Y ! 


 SP 


 WRES 


 NOVELLO EDITION 


 BACH S CHURCH CANTATAS 


 4 STRONGHOLD SURE 


 NOUGHT SOUNDNESS ALI 


 


 SHILLING 


 : BACH MOTETS . 


 


 NOVELLO , EWER CO 


 NEW YORK 


 HANES Y G ; ROE S 


 CANON WILLIAMS ( CAERFYRDDIN ) 


 , ; E FLAT 


 BLESSED THE\ — — 


 MOURN 


 MOTET 


 CHORUS ORGAN 


 PARIS H CHOIR 


 COMMU NION SE RVICE 


 — L MILLER . FRCO . HOLY COMMUNION 


 PUBLISHED 


 


 CONSISTING OTF 


 SETTINGS OFFICE 


 CHURCH MUSIC 


 BENEDICTUS QUI VENIT , AGNUS DEI 


 INCLUDING THI 


 PRICE SHILLING SIXPENCI 


 REVIEWED 


 


 CHORUS 


 T\R ~ ~ ATRAT 4 Y ) ~ T 


 SHORT SACRED CANTATA 


 USE PLACES WORSHIP . 


 MUSIC 


 CHREE F. W. PRIEST , A.R.C.O. 


 LITANY ‘ RESPONSES 


 - SONGS 


 MANUAL PLAINSONG 


 DIVINE SERVICE 


 NEW EDITION 


 


 PRE RED 


 SERVICES H. 


 XTREMELY SUITABLE PERFORMANCE THI BODY 


 SEASON CHRISTMAS . ( ES IST NICHTS GESUNDES MEINEM LEIBE ) 


 CANTATA 


 OLI , CHORUS , ORCHESTRA 


 


 PRINCE PEACE 7 


 ORCHESTRA 


 WORDS SELECTED HOLY SCRIPTURE 


 J. S. BACH 


 W 5 


 CARDS 


 POST 


 MUSICAI 


 READY 


 COUNCIL SCHOOL HYMN BOOK 


 COLLECTION HYMNS , PRAYERS , 


 USE COUNCIL SCHOOLS 


 D MEET PARTICULAR NEEDS NEW EDUCATION 


 AUTHORITIES ESTABLISHED ACT 1902 


 PRICES 


 NOTE MUSIC EDITION 


 PRESS NOTICES 


 RISTIAN WORI 


 BOOKSELLER 


 HOOLMISTRESS 


 MES MIRROR 


 SCOTSMAN 


 MORNING NEWS 


 NOVELLO'S. 


 CHORISTER SERIES 


 CHURCH MUSIC 


 VERSICLES RESPONSES 


 ESSED DWELL THY HOUSE 


 KYRIE CREED 


 BENEDICTUS AGNUS DE 


 LO ! HILLS HELP DESCENDS 


 HEAR PRAYER , O LORD 


 OQ PRAISE LORD 


 O LORD , THOU HAST SEARCHED 


 GOD SAVE KING 


 TEARS SOWETH 


 LORD SHEPHERD . SMART 


 BABYLON WATERS H. SMAR1 


 


 NOVELLO , EWER & CO . , NEW YVORK 


 PUBLISHED 


 ODE NORTH - EAST 


 WIND 


 SET CHORUS ORCHESTRA 


 FREDERIC CLIFFE 


 MORNING LEADER . 


 FRIELD INDEPENDENT 


 ORIANA 


 COLLECTION EARLY MADRIGALS 


 BRITISH FOREIGN 


 ANGEL FACE BRIGHTNESS ... » ) « + » DANIEL NORCOME 


 ARISE , AWAKE , SILLY SHEPHERDS _ ” ... THOMAS MORLEY 


 


 NOVELLO , EWER CO . , NEW YORK 


 COMPOSITIONS 


 MEE PATTISON 


 HUMOROUS - SONGS 


 DESCRIPTIVE CHORUSES 


 


 WRECK HESPERUS 


 POEM 


 LONGFELLOW 


 USIC CHORUS ORCHESTR 


 NARCISSUS ECHO 


 CANTATA CHORUS , SOLI , ORCHESTRA 


 COMPOSED 


 EDWIN C. . 


 - SONGS 


 MEN VOICES 


 


 LAURENT DE RILLE . 


 PUBLISHED . 


 JESU , HOPE HEAVEN 


 CONTRALTO BASS 


 


 RUSSIAN SONGS 


 ROSA NEWMARCH . 


 SEVEN PIECES SEVEN : PIECES 


 ORGAN 


 ORGAN 


 THEODORE DUBOIS 


 1 ED 


 ALEXANDRE GUILMANT 


 6 . POSTLUDE CANTIQUE > BERCEUSE . 6 . POSTLUDE . 


 MARCHE - SORTIE . 7 . FANTAISIE SUR DEUX MELODIES ANGLAISES 


 VILLAGE ORGANIST 


 SECOND SONATA \ SERIES PIECES 


 ORGAN CHURCH GENERAL USE 


 M EDWIN H. LEMARI 


 E 


 ALOYS CLAUSSMANN . F. CUNNINGHAM WOODS 


 OLD ENGLISH 


 ORGAN MUSIC 


 JOHN E. WEST 


 PREFATORY NOTE . 


 SEVENTEENTH CENTURY PIECES . 


 


 PIED PIPER HAMELIN 


 TENOR BASS SOLI , CHORUS ORCHESTRA 


 C. H. H. PARRY 


 PRICE SHILLINGS 


 TIMES MORNING POST 


 MANCHESTER GUARDIAN 


 MORNIN 


 


 NOVELLO , EWER CO . , NEW YORK 


 COMPOSITIONS 


 SAMUEL SEBASTIAN WESLEY 


 NUN 


 BENEDICITE , OMNIA OPERA 


 ORGAN MUSIC . 


 MINIATURE SCORE 


 RICHARD WAGNERS 


 TRISTAN ISOLDE 


 LYRIC DRAMA ACTS . 


 NS SPECIAL BINDINGS , PRICE APPLICATION 


 W. A. MOZART 


 KOCHELS THEMATIC LIST 


 COMPLETE WORKS MOZART 


 EDITION , ENTIRELY REVISED BROUGHT DATE 


 PAUL GRAF WALDERSEE 


 BERTHOLD LITZMANN 


 CLARA SCHUMANN 


 SECOND VOLUME 


 IS4O0—10550 . 


 O PORTRAITS 417 PAGES GERMAN 


 BREITKOPF & HARTEL , 


 54 , GREAT MARLBOROUGH ST . , REGENT ST . , LONDON , W. 


 LE AGENTS CELEBRATED 


 PAUL WERNER DRESDEN PIANOFORTES 


 LENT H : 


 STERND 


 HALF - TE