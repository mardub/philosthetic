


 PUBLISHED 


 ROYAL CHORAL SOCIETY . 


 ROYAL ALBERT HALL . 


 PROSPECTUS . 


 MENDELSSOHN . “ ELIJAH 


 ROYAL ACADEMY MUSIC , 


 TENTERDEN STREET , W 


 ROYAL COLLEGE ORGANISTS 


 SS 


 MUSICAL TIMES 


 FOUNDED 1844 . 


 MONTH 


 OCTOBER 1 , 1909 


 QUEEN IALL . 


 | PROMENADE CONCERTS 


 ( FIFTEENTH SEASON ) . 


 QUEEN HALL ORCHESTRA . 


 ROBERT NEWMAN 


 HERR MORIZ 


 ROSENTHAL PIANOFORTE RECITAL 


 PAPILLONS \ 


 YSAYE ORCHESTRAL CONCERT 


 CONCERT ‘ 


 QUEEN HALL ORCHESTRA 


 YSAYE VIOLIN RECITALS 


 ROBERT 


 QUEEN HALL ORCHESTRA 


 SYMPHONY CONCERTS 


 QUEEN HALL ORCHESTRA 


 SUBSCRIPTIONS 


 ROYAL LONDON BACH SOCIETY . 


 SESSION 1909 - 1910 . 


 INCORPORATED GUILD CHURCH 


 TOBIAS MATTHAY PIANOFORTE SCHOOL . ASSOCIATE ( A.1.G.C.M. ) , LICENTIATE ( L.1.G.C.M. ) , FEL- 


 COMPETITIONS 1909 


 CITY LONDON COLLEGE 


 WHITE STREET , MOORFIELDS , E.C 


 VICTORIA COLLEGE MUSIC , 


 | LONDON 


 INCORPORATED 1891 . 


 SOU ’ 


 WEDNI 


 PHIL ! 


 PHYL 


 MESSIEURS 


 MILL . 


 JOSE 


 WEDNESD 


 THURSDA ' 


 SATURDA 


 XUM 


 1909 . 623 


 TRIENNIAL MUSICAL 


 FESTIVAL 


 SOUTHPORT 


 WEDNESDAY , 


 OCTOBER 13 


 PHILLIPS , EDNA THORNTON , GRAINGER KERR , 


 PHYLLIS LETT , LILY WHITESIDE . 


 MILLAR , ROBERT RADFORD , HERBERT BROWN , 


 MESDAMES AGN 


 HALLE ORCHESTRA 


 SIR C. HUBERT H 


 ORGANIST 


 THURSDAY 


 ‘ VISION CLEOPATRA . ” 


 FOLK SONGS . ” 


 “ REST . ” 


 PENSIEROSO 


 


 LOVERS 


 MUSIC 


 ALEXANDRA PALACE CHORAL 


 ORCHESTRAL SOCIETY 


 ALLEN GILL 


 LIST INTENDED PERFORMANCES 


 SEASON 1909 - 10 


 1909 . 


 1910 . 


 NATIONAL CONSERVATOIRE 


 SPECIAL FEATURE 


 BUSINESS 


 W. E. HILL & SONS 


 


 REPAIRING VIOLINS , VIOLAS , 


 VIOLONCELLOS , BOWS 


 HIGHEST HONOURS VIOLIN , BOW , CASE- 


 MAKING 


 W. E. HILL & SONS 


 SOLE VIOLIN BOW MAKERS H.M. KING . 


 140 , NEW BOND STREET , LONDON , W 


 1909 


 PROFESSIONAL NOTICES 


 MISS KATE CHERRY , 


 MADAME 


 HANNAH JONES , A.R. 


 MISS ANNIE M AP PLEBECK 


 MR . GORDON HELLER 


 MR . CHARLES H 


 LOFTHOUSE 


 MISS ESTE LLA LINDEN 


 ( YORKSHIRE SOPRANO 


 MISS ETHEL RADBURN 


 ( SOPRANO ) 


 MISS DASIE E. AVIS 


 FR : ANCIS G LYNN 


 TENOR ) 


 ORATORIOS 


 MR . SAM UEL MASTERS 


 613 P.O. 


 ERNEST PENFOLD 


 MR 


 HENRY PLEVY 


 MR . 


 ( TENOR 


 MR 


 ARCHDEACON 


 BARITONE ) . 


 CHANGE | ADDRESS 


 MONTAGUE BORWELL 


 BARITONE ) 


 MISS 


 WINIFRED MARWOOD 


 BRONDESBURY PARK , N.W 


 MR 


 BOARSLAND 


 RBERT PARKER 


 MR 


 MEISTERSINGER 


 LICHFIELD . 


 PARK GARDENS , TU 


 MR . CUTHBERT ALLAN 


 WILLIAM BURT 


 MR . 


 ( BASS - BARITONE 


 BROCKWELI 


 ROBIN OVERLEIGH 


 BASS - BARITONE 


 MR . EATON COOTER 


 MISS ELLEN CHILDS 


 ( CHROMATIC HARPIST ) . 


 MISS MARIAN JAY 


 SOLO VIOL INIST 


 LRA . M 


 D EV 


 LATEST 


 JOCA 


 DUNCAD 


 L.R 


 TOIC 


 SID 


 FROST ( 


 CORRI 


 POINT , 


 ANOQ ) 


 AR 


 SW 


 OYS 


 XUM 


 , 1909 . 625 


 EEE ~ - _ = 9 - ~ 


 COMPOSERS ’ MSS . 


 L.R. A.M. ( PIANOFORTE 


 LATEST SUCCESSES : — 


 L.R.A.M. EXAMINATION , 1 


 . GEORGE R. CEILEY B+ . LESSONS 


 - “ SECRETS SONG SPEECH 


 NY 


 EDMONDSTOUNE 


 N USICAL COMPOSITION . 


 R. CUTHBERT 


 ATEST SUCCESSES 


 JULY , 1 


 YEARS ’ SUCCESSES : 5 MUS . DOC . ; 24 MUS . BAC 


 20 17 


 S ORGAN LESSONS 


 DR . LEWIS ’ TEXT - BOOKS : 


 


 LONDON COLLEGE MUSIC 


 GREAT MARLBOROUGH STREET , LONDON , W. 


 MUSICAL EDUC ATION EXAMINATIONS 


 ASSOCIATES ( A.L.C.M. ) . “ _ — 


 TEACHERS DIPLOMA . 


 MUSICAL 


 , 1909 . 627 


 LONDON 


 INTEREST [ 


 INTERES 


 EHREMAYER SYSTEM 


 PIANOFORTE SIGHT - PLAYING 


 L. M. EHREMAYER , 27 


 KUM 


 COLLEGE 


 DIPLOMAS THEORETICAL 


 CHANCERY 


 MUSIC 


 MUSIC 


 F.R.A.M. ; S 


 ORCHESTRA , OPERA CLASS , STRING 


 PIANISTS 


 EXTRACTS PUPILS ’ REPORTS : 


 DRESSES 


 LANE , W.C 


 BUYING PIANO PLAYER 


 EDWARD ELGAR . DAILY TECHNICAL STUDIES 


 SCORE BOSWORTH & CO . , 


 GE : : 


 _ ST . ALBANS CATHEDRAL 


 ST 


 ALBANS CATHEDRAL SOUTH WEST . 


 


 1909 


 ILLUMINATION COTTON MS . , NERO , 7 , 


 WALLINGFORD SCREEN . 


 1909 


 ST . ALBANS 


 ROBERT 


 ABBEY CHURC 


 I6TH CENTURY 


 AGNES WIFE 


 FAYRFAX , 


 H , 


 ORGANIST 


 7 PEDESTAL SHRINE ST . ALBAN , WATCHING LOFT 


 634 


 SAXON BALUSTER SHAFTS SOUTH TRANSEPT . 


 _ — _ 


 302 , 


 XUM 


 MUSICAL TIMI 


 NAVE , LOOKING EAST 


 636 


 1909 


 UU 


 FATHER SMITH ORGAN , 


 1828 . 


 MORNING SERVICE II . 


 6 . 14 . 9| 


 £ 2 . 12 . 6 


 AFTERNOON 3 O'CLOCK 


 WOODEN FIGURE MENDICANT . 


 16 


 PEDAL ORGAN 


 ACCESSORIES . 


 NORMAN ARCHES 


 TOWER . 


 638 


 ABBEY 


 GATEWAY . 


 1833 


 1516 


 32 . ) 


 NORMAN DOORWAY SOUTH TRANSEPT . 


 1858 - 1880 . : : 


 MR . W. L 


 LUTTMAN SEATED ORGAN ST . ALBANS CATHEDRAL . 


 XUM 


 HATTON CHARACTERISTIC ATTITUDE 


 642From inferred 
 Hatton compositions come notice 
 Society British Musicians , met 
 approval experienced musician 
 ‘ dear old Attwood 

 Hatton instrumentally inclined 
 productive period career evidenced 
 collection old programmes belonged 
 late Mr. H. J. B. Dando , 
 possession present writer . 
 programmes , printed pink card , concert , 
 presumably private , given ‘ Thursday 
 evening , 18th April . ’ Unfortunately yearnor 
 place given ; selection — included 
 Beethoven second symphony string quartet 
 , ‘ Zauberfléte ’ overture — contained 
 ballad John Barnett opera ‘ Farinelli , ’ 
 produced early 1839 , year probably 
 concert given . events 
 programme contains Quintett pianoforte , 
 violin , tenor , violoncello , double bass , 
 Hatton , mentioned 
 biographers . Efforts trace place giver 
 concert far failed 

 find composer Drury Lane 
 Theatre . July , 1842 , engaged 
 Macready chorus - master salary 
 guineas week . season opened October 1 . 
 February 23 , 1843 , new operetta act 
 produced entitled ‘ Queen Thames , , 
 Anglers . ’ composer 
 withheld representation ( March 6 ) , 
 chorus - master ( /.e . , Hatton ) received 
 publicity . successful piece 
 ran , intervals , nights , 
 probably longer career incident 
 presently mentioned . songs 
 ‘ Alas ! unwed - ’ ‘ Blind man 
 buff , ’ madrigal , ‘ merry bridal bells , ’ 
 described ‘ good deal better 




 1909 . 643Mr . J ] . L. Hatton returned London Saturday 
 [ April 6 , 1844 ] Vienna , super- 
 intending representation opera , 
 composition , highly successful ; Mdlle . Lutzer 
 Herr Staudigl sustained principal characters , 
 took warmest interest production 
 countryman 

 Philharmonic concert 27 , 1844 , 
 duet , ‘ Stung horror , ’ ‘ Pascal Bruno , ’ 
 sung Miss Rainforth Herr Staudigl . 
 concert conducted Mendelssohn , 
 Wedding march performed time 
 country , Joachim , aged fourteen , astonished 
 everybody wonderful interpretation 
 Beethoven Violin concerto . words 
 duet doggerel ‘ 
 Revenge . ’ began 

 Pascal . Stung horror , shame , anguish 




 JACK HATTON 


 MR . CHARLES LYALL , 


 KIND PERMISSION 


 CARICATURE SKETCH REPRODUCED 


 644 


 1909Two sets , making eighteen 
 songs , followed ‘ order ’ Mr. Oliphant 
 ‘ exclusive property , ’ 
 ‘ Simon cellarer , ’ , said , 
 remunerated composer £ 10 note 

 excellent pianist , Hatton successful 
 appearances Melodists ’ Club London , 
 member , Hereford 
 Musical Festival 1846 . occasion , 
 miscellaneous concerts 
 Shire Hall , played pianoforte 
 Beethoven early Quintet E flat pianoforte 
 wind instruments , favoured audience 
 ‘ Le Savoyard , ’ described ‘ humorous 
 chansonette composition sung 
 accompanied . ’ second concert 
 played solo Mozart Pianoforte 
 sania D minor . ‘ works 
 immortal master ’ ( said Zhe Times ) , ‘ 
 enriched repertory piano greater 
 extent composer , 
 universal Beethoven , neglected 
 performers , Mr. J. L. Hatton deserves 
 credit endeavouring bring notice . ’ 
 Hatton ‘ Czapek ’ * Chapel ’ 
 ‘ shepherd winter song’—were sung 
 festival . Concerning compositions , 
 Times critic — internal evidence , Mr. J. W. 
 Davison — said 

 songs 

 Handel , Mozart , 
 sentimental 

 Beethoven , & c. 
 

 sang songs comic , | 
 levied contributions | 
 nations compositions , kept audience 
 alternately moved delight excited laughter . | 
 Mr. Hatton modest obtrude | 
 works visitors . ‘ Adventures | 
 Robinson Crusoe 




 , 1909 . 645 


 646 


 1909f | characteristics happily caught 
 o > — = f — . . r . . Bw 
 5 — ag — = = F |excellent caricature Mr. Charles Lyall , 
 - - kindly allowed reproduce . 
 — — § o- Hatton nautical proclivities musically reflected 
 e—~—_—s — = - . « @ » ? oe 4 ; 
 ne ES SS ‘ Songs sailors . ’ Freemason 

 Beethoven . like sound 5ths — look 

 é ores 
 = = s 




 SS 


 DEAR | Wednesday , 26th August , 1846 , conducted | soldier diplomat , ’ ‘ Dictionary 
 ‘ Elijah , ’ 25th ° ) , performance | National Biography ’ states . succeeded 
 Friday ( 28th ) , long chat 

 S 3 sh 5 j 45 > 
 subject 3 quartetts spoke writing , - — title , , = — ahager oo 1822 
 parted time , alas ! spend days e formation - = e ~ woyal é nea . Music , 
 evenings came England . took tive interest 
 visits delight advantage leading remainder life . highly esteemed 
 chamber music guidance Mr. Alsager Berlin — resident minister 1541 
 house Queen Square , Bloomsbury , | 1851 — news death reached 
 occasions , , played tenor parts | capital , principal military bands assembled 
 chamber works . played presence Prince Regent distinguished 
 juite , stop , ‘ , want| company , performed Beethoven Funeral march , 
 ; , laugh , play / q favourite piece Earl . voluminous 
 notes , _ gene r ar [ | composer , output included seven Italian operas , 
 way Teed hardly tll T ad bette |chree cantatas , masses , cathedral services , anthems 
 tenor playings called hymns , madrigals , ‘ vocal MUSIC , 
 ‘ colleague . ’ | printed manuscript . 
 attempts arrange night | forgotten Richard Fitzwilliam , seventh 
 Quartett concerts [ Crosby Hall ] , success . | \ iscount ( 1745- 191 6 ) , founded Fitzwilliam Museum . 
 arrived 1844 fixed concert . | Musical Times March April , 1903 , 
 glad tidings announced subscribers| contained illustrated article important 
 verily besieged tickets , having space | treasure - house . 
 concert - room , landing staircase | 
 crowded listeners . gave magnificent impromptu ; _ _ , : : 
 performance forgotten who| _ peer survey obtained world - wide 
 fortunate hear it.—Yours truly , fame father great Duke Wellington . 
 J. H. B. Danpo . music , Garrett Colley Wellesley ( Wesley ’ , 
 | Earl Mornington ( 1735 - 1781 ) , widely favourably 
 |known composer ‘ cool grot 
 mossy cell ’ ( prize glee ) , excellent double 
 chants . musical sensibilities Lordship 
 displayed aremarkable manner whilst nurse 
 co “ sgh . arms , long speak . elected 
 ey peg ‘ aan mw 4 — s | professor music University Dublin 

 1764 ) , conferred degree 
 encore extemporised earlier numbers | doctor music 




 648 MUSICAL TIM 


 1717 : 


 , 1909 . 649 


 1717Rome , 1630 

 Turning Department Manuscripts , 
 satisfactory learn regard Catalogue 
 Music ‘ revision descriptions vol . iii . 
 Instrumental Music ) completed , 
 text printed . ’ early issue book 
 reference expected . Foremost 
 acquisitions bequest Miss Harriet 
 C. Plowden Beethoven Sonata violin 
 pianoforte G ( Op . 30 , . 3 ) , String 
 quartets Mozart described / usica/ 
 Times September , 1907 . Owing question 
 doubts ownership treasures delivered 
 Museum November . acquisi- 
 tions described 

 Messe Solennelle ( Ste . Cécile ) , score , Charles 
 Gounod ; 1852 . Autograph 




 , 1909 


 DARWALI 


 4A 


 + = = SS SS 


 FACSIMILE ( SLIGHTLY REDUCED ) ‘ DARWALL 148TH ’ AUTOGRAPH COMPOSER . 


 1800 


 425 SS S52 SSE = 


 2 = = SSS ET 


 RECITALS SIR WALTER PARRATT 


 FS 


 _ — _ — — _ 


 ENTS 


 OLD NEWCASTLE FESTIVALS 


 654 


 1909place , increasing accommodation . ’ Sir George 
 Smart , conducted , received Chevalier 
 fee £ 157 105 . Miss Stephens ( ‘ Kitty ’ Stephens 
 John Braham paid £ 189 , Mr 
 Henshaw , organist Durham Cathedral , received 
 £ 12 assisting Mr. Thompson , organist 
 church - festival ; gentleman 
 gave fee £ 10 charities 

 regard music performed festival 
 1824 , marked improvement shown pro- 
 grammes evening concerts . Symphonies 
 Haydn , Mozart ( Jupiter ) , Beethoven ( key stated 
 played , following overtures : ‘ Die 
 Zauberfléte , ‘ Der Freischiitz , ’ ‘ Anacreon . ’ 
 lighter fare programmes included ‘ Charlie 
 darling’—which ‘ gave great offence , 
 substituted Chevalier song 
 consulting committee — ‘ Scots wha hae ’ 
 sung Braham ) ‘ Fantasia Mandolin ; 
 composed performed Signor Vimercati , 
 ‘ curious astonishing . ’ 
 complete oratorio given church 
 ‘ Messiah ’ ; selections ‘ Israel 
 Egypt , ’ ‘ Judas Maccabieus , ’ ‘ Creation , ’ ‘ Seasons ; 
 ‘ Requiem Mass ’ ( Mozart ) , & c. ‘ Grand chorus 
 fugue ’ Mozart ‘ arranged orchestra , 
 Sir George Smart , ’ words ‘ O heavenly Lord ! 
 Almighty amidst mightiest . ’ Madame 
 Catalani , occasion , condescended allow 
 Mr. Braham sing opening solos ‘ Messiah , 
 retained ‘ despised , ’ transposed 
 key G ! Furthermore , ‘ accommodate Madame 
 Catalani ’ , ‘ Lord shall reign ’ ( ‘ Israel Egypt ’ 
 transposed B flat 

 festival held Newcastle September , 
 7842 . previous occasions occupied days , 
 Sir George Smart conducted . sacred 
 performances , given church , largely 
 inevitable ( days ) ‘ selection ’ nature — e.g , 
 ‘ Israel Egypt , ’ ‘ Mount Olives , ’ ‘ Creation , ’ c. 
 ‘ Messiah ’ performed entire , Kossini 
 ‘ Stabat Mater , ’ comparatively new England . 
 ‘ Stabat went new English 
 words , selected adapted , 
 principally church liturgy ’ : records 
 Musical \World long notice festival . 
 programmes evening concerts 
 commend . symphonies performed 
 Haydn E flat , Mozart E flat , Beethoven 
 C minor . overtures played — Die Zauber- 
 fléte , Fidelio , Euryanthe , Der Berggeist , Mid- 
 summer Night Dream , addition violin 
 violoncello concerto , septets found places 
 scheme , Neukomm Beethoven . 
 ‘ grand fancy dress ball ’ set seal festival 
 1842 

 Sir George Smart , punctilious punctual , 
 annotated copies word - books , gives 
 amusing sidelights festival . records 
 * Merry - faced Lindley [ violoncellist ] -took 
 accustomed pinch snuff amid cheers 
 audience . ’ Concerning Duke Cambridge , 




 AL 


 - SONG ( UNACCOMPANIED 


 TITLE BREEZE 


 EE 4 


 BREEZE 


 — > , — — — 2 - 


 


 BREEZE 


 P LL 


 NI 


 . 4 - 2 


 @ ‘ \ SS 


 7 _ — _ — " " _ 


 NT 


 2 ) ’ | 7 | — — > 


 661 


 , 1909 


 - SONGS 


 MUSIC 


 PIANOFORTE 


 1909 


 ONGS . 


 BOOKS RECEIVED 


 DIFFICULTIES MODERN 


 CHORAL MUSIC 


 SINGING 


 EDITOR ‘ 


 MUSICAL TIMES 


 HEREFORD MUSICAL FESTIVAL 


 664 MUSICAL 


 1909 


 WALKUR 


 MUSICAL TIM 


 SIEGLINDE SIEGMU > : 


 PROMENADE CONCERTSMiss Agnes Nicholls Mr. John Coates 

 Mass D Beethoven , - work frequently 

 morning . trying choral writing strained powers | 
 choir somewhat , singers } 
 commended courage , result | 
 successful securing effect Beethoven desired causing | 
 work awaken religious feelings singers 
 asin hearers . Dr. Sinclair interpretation tempi 
 liberal , dragged . solo quartet , consisting | 
 Miss Perceval Allen , Madame Ada Crossley , Mr. Gervase | 
 Elwes , Mr. Robert Radford , chosen | 
 performance weak spots festival . | 
 artistic atmosphere restored second | 
 magnificent interpretation Sir Edward Elgar symphony | 
 baton composer . best | 
 performances given , work , heard | 
 cathedral time , exercised new appeal . 
 Motet Bach , ‘ afraid , ’ double choir , | 
 attacked , served reveal deficiency choir 
 important quality incisiveness 

 _ evening programme consisted | 
 Sir Hubert Parry oratorio ‘ Job , ’ heard | 
 meeting seventeen years ago . work well| 
 entitled consideration present day . choral | 
 writing best Sir Hubert Parry , | 
 originality modernity | 
 long monologue Job , delivered | 
 eflect Mr. Frederic Austin . Mr. Walter Hyde | 
 good impression music Satan , Master 
 Tidmarsh gave music Shepherd , | 
 case articulation words clear . 




 


 1909 


 COMING SEASON 


 MUSIC BIRMINGHAM . 


 ( CORRESPONDENTIn forecasting season events , place honour 
 assigned Birmingham Festival Choral 
 Society ( conductor , Dr. Sinclair ) , intend 
 subscription concerts addition Christmas perform- 
 ance ‘ Messiah . ” works given include 
 ‘ Creation , ’ ‘ Samson Delilah , ’ ‘ Hiawatha ’ 
 cycle 

 Midland Musical Society , conducted Mr. A. J. 
 Cotton , announce Cowen ‘ Sleeping beauty , ’ ‘ Messiah , ’ 
 ‘ Hiawatha ’ cycle , Dvordk ‘ Stabat Mater , ’ Beethoven 
 Mass C 

 Birmingham Choral Orchestral Association 
 , Mr. Joseph H. Adams direction , German 
 ‘ Merrie England , ’ Brahms ‘ Song Destiny , ’ Schubert 
 ‘ Song Miriam , ’ Elgar suite ‘ Bavarian 
 Highlands , ’ Handel selection 




 MUSICAL TIN 


 MUSIC BRISTOL 


 CORRESPONDENThe Sine Nomine Choral Society , conductor Mr. R. 
 Simmons , practising Gade ‘ Crusaders 

 Bath 
 Heymann , resumed September 25 . 
 Symphony Concerts performance 
 Beethoven , Brahms Tchaikovsky 
 chronological order 

 MUSIC GLASGOW 




 CORRESPONDENTcathedral . addition cathedral organist , 
 performers Dr. Basil Harwood Mr. C. H. Moody . 
 following forecast shows , coming 
 promises interest . Choral 
 Orchestral Union scheme includes - concerts . 
 Dr. Cowen conductor - - chief Scottish 
 Orchestra , conductors Dr. Richter 
 Messrs. Wassili Safonoff Henri Verbrugghen . 
 choral works performed ‘ Messiah , ’ ‘ Acis 
 Galatea , ’ ‘ St. Paul ’ ( I. ) , Cliffe ‘ Ode 

 North - east wind , ’ Bach Mass B minor Beethoven 

 , - named , 
 Pollokshields 

 
 important step adopting French pitch , coming 

 Nl 
 | Teachers ’ Choral Society ( Mr. Alec Steven , conductor ) 
 MacCunn ‘ Lord Ullin Daughter , ’ Mendelssohn 
 ‘ Hear prayer , ’ miscellaneous pieces . 
 Athenzum School Music Operatic Choral Societies , 
 joined Mr. Henri Verbrugghen direction , 
 announce formidable programme , viz . , ‘ Cavalleria 
 Rusticana , ’ ‘ Lohengrin , ’ ‘ Les noces de Jeanette , ’ 
 Mendelssohn ‘ Hymn Praise , ’ Beethoven ‘ Kuins 
 Athens . ’ 
 | University Choral Society , conducted Mr. A. 
 M. Henderson , perform Dunbhill ‘ Tubal Cain , ’ 
 Somervell ‘ Earl Haldan daughter , ’ Grieg ‘ Kecognition 
 land , ’ selection madrigals - songs . 
 concert Greenock Choral Union ( Mr. W. T. 
 Hoeck , conductor ) prepare parts 1 2 Coleridge- 
 Taylor ‘ Hiawatha , ’ Dumbarton Choral Union ( Mr. 
 |E Owston , conductor ) concert - performance 
 | * Maritana ’ ‘ Bohemian girl 

 MUSIC LIVERPOOL 




 CORRI INDENThe Methodist Choral Union , conductor Mr. Percival H. 
 Ingram , announces concerts Handel ‘ Samson ’ 
 performed Bootle , ‘ Messiah ’ 
 ‘ Creation ’ Philharmonic Hall 

 Chamber music represented Schiever 
 classical chamber concerts Mr. Rawdon Briggs string 
 quartet , Mr. Egon Petri play Beethoven 
 recitals pianoforte sonatas master 

 Mr. Donald Francis Tovey initiate discourses 
 connection Music Lectures Association 
 University , October 5 




 668 


 1909 


 MUSIC SHEFFIELD DISTRICT . 


 ( CORRESPONDENT 


 DISTRICT 


 ENT 


 MUSIC NEWCASTLE 


 CORRESPONI 


 MUSIC NOTTINGHAM DISTRICT 


 PONDENT 


 CORRIof works performed , audiences receive valuable 

 assistance excellent analytical programmes prepared 
 Mr. A. Corbett - Smith . addition masterpieces 
 music — e.g. , Beethoven Mozart symphonies — 
 works British composers receive attention , names 
 Elgar , Sterndale Bennett , Sullivan , Stanford Edward 
 German included past programmes . prospectus 
 forthcoming season comprises Elgar Variations , 
 Zantock ‘ Helena ’ variations , German ‘ Seasons ’ suite , 
 Parry Symphonic variations . Special programmes 
 provided time time illustrating particular 
 composer period musical history . cordially wish 
 success enterprise , trust excellent 
 example induce municipal authorities ‘ anc 
 likewise 

 works gi ' 
 ! 
 
 rforma ! 
 Biegfried 
 ‘ Ring , ’ | 
 particular 
 solo s 
 high 
 Lohengri 
 ( Kundry 
 gooc 
 Dr. Hs 
 desk 




 MUSICAL TIM 


 BAYREUTH 


 BERLIN 


 BIELEFELD 


 CASSEL 


 COPENHAGEN 


 LEIPSICMUNICH 

 festival performances Mozart operas given 
 Residenztheater included ‘ Le nozze di Figaro , ’ ‘ Die 
 Entfiihrung aus dem Serail , ’ ‘ Don Giovanni , ’ ‘ Cosi fan 
 tutte , ’ skilfully conducted Herr 
 Felix Mottl , Mozart performances followed 
 ‘ Wagnerfestspiele ’ Prinzregententheater , 
 following operas given : ‘ Tannhaiiser , ’ ‘ Die 
 Meistersinger , ’ ‘ Tristan und Isolde , ’ ‘ Ring des 
 Nibelungen , ’ respective batons Messrs. 
 Mottl , Rohr , Franz Fischer . Konzertverein , 
 conductorship Ferdinand Léwe , gave series 
 symphony concerts devoted works Beet- 
 hoven , Anton Bruckner , Brahms . programmes 
 comprised Beethoven symphonies , 

 performed 




 OSTEND 


 GAMBA , result labours 

 Thieriot ) ; Danses pseudo - classiques en forme de quadrille | F aA ’ 
 d’aprés des themes de J. S. Bach ( Delaborde ) ; Gaudeamus| CAPE Town.—The Philharmonic Society gave second 
 igitur ( Liszt ) ; O du lieber Augustin ( Max Reger ) ; | concert season City Hall August 18 . 
 S ’ kommt Vogel ( Ochs E. Scherz ) . | programme comprised Beethoven ‘ Egmont ’ overture 

 CH R , ‘ cial | Mendelssohn ‘ Scotch ’ symphony , Handel ‘ Largo 




 P. L 


 SCAI 


 671 


 CONTENTS 


 MONTH 


 MUSICAL TIMES 


 SCALE TERMS ADVERTISEMENTS 


 | ° . . 99 ° > . 


 672 


 197 ” ’ ~ : — — 


 


 1909 


 ‘ SCHOOL 


 NUAL SUBSCRIPTION , INCLUDING POSTAGE , 25 


 IST MONTH 


 MUSIC REVIEW OCTOBER 


 CONTAINS 


 SCHOOL 


 


 SCHOOL 


 MUSIC REVIEW 


 MUSIC REVIEW 


 CONTAINS FOLLOWING MUSIC 


 CHOR . AL ORCHESTR : AL SOCIETIES 


 MR . 


 RECITALS 


 DRE 


 CHARLES 


 MACKENZIE * 


 MENDELSSOHN 


 MENDELSSOHN 


 ( ¢ P ( 


 


 * MIDSUMMER NIGHT 


 SCHUMANN ‘ ‘ MANFRE 


 ROBERT 


 EK . WEST * * KING 


 MACKENZIE * * DREAM 


 DRE 


 EUGE 


 FRY 


 MUSIC 


 ATHALIE 


 NE 


 BRIXTON ORGAN 


 BRIXTON INDEPENDENT CHURCH , S.W 


 MONDAY , OCTOBER MONDAY , NOVEMBER 


 NDAY , OCTOBER MONDAY , NOVEMBER 


 MONDAY , DECEMBER 


 MISS 


 ( SOPRANO 


 NE LU { ’ 


 PRACTICE 


 RGAN 


 E DINA THRAVES 


 GAVOTTE G 


 ARRANGEMENT SMALL ORCHESTRA 


 SCHUMANN CENTENARY 


 N.W 


 SICILY 


 JUBAL 


 


 673 


 


 BYKON 


 MANFRED 


 | ORCHESTRA CHORI 


 MR . CHARLES FRY\ 


 674 MUSICAL 


 1909 


 F.R.C.O 


 D * ® H. H. L. MIDDLETON 


 C.0 . , L.R.A.M. , A.R.C.MN 


 DEGREES . L.R.A.M. 18 


 97 - 1908 , NINETY 


 COACHING 


 A.R.A.M. , F.R.C.O. ) , PIANOFORTE , HARMONY 


 COUNTERPOINT , COMPOSITION , ORCHESTRATION 


 ... JOSEP H SPAWFORTH , A.R.A.M. ( 67 , 


 TRALIN ING VOCAL & MUSICAL ACADEMY , W. 


 M * , PERCY WOOD , 


 TU DIO , OXF ORD CIRCUS.- 


 MARBLE ARCH VOCAL STUDIO , 


 WEST - END CONCERTS 


 PUPILSpupus f - 1 
 Vol teste F advi free . Wi ite appointment 

 M D 
 M Ust ' STUDIOS , Teaching Practice . 
 1 f ' apply , 7 ( Beethoven House , Georg 

 H House , H Street , Sutt Surre 




 MUSIC COPYING AGENCY . 


 INTRODUCTION SUCCESSFU 


 REGEN1 ST 


 SCHOOL 


 POSTAL 


 ORRESPONDENCE 


 LONDON . — GRADUATED 


 MUSIC , 


 LESSONS 


 HARMONY , COUNTERPOINT , THEORY MUSIC . FORM 


 . 


 CHURCH ORATORY , | L ONDON 


 CHOIR SCHOOL 


 SENOR WANTED 


 28 L 0 


 AC 


 A. MADELEY 


 USIC 


 INS 


 , FORM 


 R.C.0 


 JON 


 OL 


 , 1909 . 67 


 ORCA 


 HU RCH 


 H.M. - 


 ) T > 


 PIANO PEDALS . 


 BEST DC » -HEAP EST 


 WORKMANSHIP GUARANTEED 


 EXCE PTIONAL 


 AX 


 SONGS , 


 O COMPOSERS.—CREWSHER & CO 


 OLD FIRM 


 


 P. CONACHER & CO . 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD 


 GOLD MEDALS 


 NICHOLSON CO . 


 ORGAN BUILDERS , 


 PALACE YARD , WORCESTER . 


 ( ESTABLISHED 1841 . ) 


 TAINER & BELL PUBLICATIONS . 


 MODERN CHU 


 ORGAN MU SIC . 


 SONGS 


 ORGAN 


 AUTUMN 


 SOUVENIR 


 JAMES LYON 


 MUSICAL 


 1909 


 NOVELLO 


 LW POPULAR SONGS 


 PUBLISHED KEYS SUIT VOICES SPECIFIED 


 PRICE TIWO SHILLINGS 


 ROLLING RIO 


 REMEMBRANCE REGRET 


 SONGS FRIENDSHIP 


 LEWYS JAMES 


 DEAR HEART . 


 ‘ THOU ROSE 


 AFFINITY 


 - DREAM 


 HEART 


 LOVER LASS 


 LONDON 


 


 HNSON 


 ELBEY 


 


 AUSTIN 


 NET . 


 ELEANORE 


 WELCOME 


 FISHERS 


 FAR HEAVENLY HOME . 


 MARGETSON 


 HOME THOUGHTS ABROAD 


 DAYS CHRISTMAS . 


 


 1909 . 677 


 ANTHEMS ADVENT 


 E. H 


 


 HYMNS 


 LOWLY GUISE 


 BLESSED 


 BL 


 WA TCH YE , PRAY YE 


 ( WACHET , BETET ) 


 CANTATA 


 CHORUS , ORCHESTRA 


 COMPOSED 


 J. S. BACH 


 SOLI 


 ENGLISH VERSION 


 ADVENT HYMN 


 THY KING APPEARETH . ” 


 SOPRANO SOLO CHORUS , ORCHESTRAL 


 ACCOMPANIMENT 


 COMPOSED 


 R. SCHUMANN 


 TRANSLATED GERMAN FRIEDRICH RUCKERT 


 J. TROUTBECK , D.D 


 


 WATCH 


 CANTATA ADVENT 


 SOPRANO SOLO CHORUS 


 


 SUNG CONGREGATION 


 MUSIC COMPOSED 


 HUGH BLAIR 


 ADVENTS 


 CHURCH CANTATA 


 WORDS SELECTED WRITTEN 


 COMPOSED 


 GARRETT 


 GEORGE M.A 


 TRU 


 ZION 


 YTATA ADVENT 


 COMPOSED 


 WARWICK JORDAN 


 OW YE MPET 


 


 HARVES 


 STMAS ANTHEMS 


 EFFECTIVE ANTHEM . 


 COM 


 EDWARD B 


 POSITIONS 


 SIXTEENTH 


 STAFF NOTATION 


 VIRGINS 


 SACRED 


 THOUSAND 


 SERVICES 


 CANTATA 


 VEST FR \ MUSIC . . 


 POPULAR 


 NM 


 CHURCH MUSIC 


 AUNDER 


 JOAN ARC 


 HISTORICAL CANTATA 


 SOLO VOICES , CHORUS 


 ORCHESTRA 


 MUSIC 


 CHORUS 


 


 ANTHEMS 


 NEW ANTHEMS 


 MUSICAI 


 LIGH 


 ANCIENT MODERN 


 OURNAL 


 EVENING HYM 


 SUITABLE SMALI 


 JESUS ! THY BOUNDLESS 


 CHORAL SOCIETIES 


 LOVE 


 SACRED CANTATA 


 SOLI ( SEMI - CHORUS ) 


 ORGAN 


 BASIL HARWOOD 


 EASY MUS 


 TOL 


 ROBERT C 


 SETTING 1 


 MMUNTON 


 GNUS DEI 


 DAVIES 


 WALFORD 


 ORCHESTRA 


 MANCHESTER GUARDIAN 


 HEREFORD FESTIVAL , 


 19009 


 NOBLE NUMBERS 


 ROBERT HERRICK 


 ER 


 BERT , DONNE 


 WRITE 


 VIOLONCELLO 


 DAVIES 


 DAILY TELEGRAPI 


 NG POST 


 POST 


 T 1 


 SS 


 , SONG 


 RUS 


 GUIDO 


 EDWARD 


 CAVALC 


 HEREFORD 


 SEPTEMBER 


 FESTIN 


 1909 


 NACCOMPANIED ) 


 WORDS 


 ANTI ( 1250 


 ELGAR 


 AL 


 


 PARTS 


 1301 


 RD FESTIVAL , SEPT . 1099 


 OLD ENGLISH SUITE 


 GRANVILLE 


 ARRANGED SMALL ORCHESTRA 


 BANTOCK 


 TIMES . 


 ATHEN - EUM BIRMINGHAM DAILY GAZETTE EXPRESS 


 LAZARUS MIDNIGHT 


 EASTER CANTATA SYMPHONIC POEM CHORUS 


 ORCHESTRA 


 SOLI , CHORUS ORCHESTRA 


 EN ‘ SH VERSION 


 W. G. ROT HERY EDWARD CARPENTER 


 FR : ANZ SCHUBERT . RUTLAND BOUGHTON 


 MUSIC 


 RUTLAND BOUGHTON 


 BAR 


 BEN 


 BEN 


 CAL 


 LEM 


 BOOTH , J 


 LESLIE 


 LESL 


 MACI 


 


 THRE 


 LCOTT , J. W. 


 1909 


 22 


 D4 


 115 


 REDUCED PRICES 


 MORLEY , T 


 PARRY , J 


 03 


 73 


 PIERSON , H. HUGH — 


 » » 200 


 » 236 


 239 


 237 


 ” 155 


 ZIMMERMANN 


 LONDON 


 NOVELLO 


 COMPANY 


 LIMITED 


 AD 


 BEYC 


 \ SELECTED LIST \ SHORT SELECTED LIST 


 ORATORIOS CHURCH CANTATAS POPULAR ANTHEMS SEASONS . 


 4 SHORT . SELECTED LIST ¢ 


 MODERN - SONGS MADRIGALS 


 MIXED VOICES 


 ANDROMEDA MASQUE COMUS 


 ANNUNCIATION 


 ODE NIGHTINGALE SKELETON ARMOUR 


 VOICES PEACE FAKENHAM GHOST 


 C. HUBERT H. PARRY . B LUARD SELBY 


 IMPORTANT ORGANISTS 


 NEW ORGAN WORKS 


 POSTLUDES 


 2 , WICKHAM ROAD , BROCKLEY , LONDON , S.E 


 1 J P ‘ 7 . 


 LD ) ' D " 


 P : P : 


 N P 


 N G 


 


 1909 


 HYMN - TUNE 


 ABIDE 


 KING LOVE 


 


 


 HERALD ANGELS SING 


 PEOPLE , COME 


 SHEPHERD 


 VARIATIONS 


 


 


 685 


 HARMONIUM SMALL ORGAN . 


 NEW ISSUE . 


 ORDERS REMITTANCES SENT 


 COMPOSITIONS JOHN L. HATTON 


 SACRED MUSIC . 


 | ? F < 


 SONGS DUETS 


 ] N 1 


 ( ’ W ) 


 - SONGS ( S.A.T.B 


 \ S| S : 


 ' > 


 G ) D ) 


 H 


 H S 


 H 


 LONDON 


 NOVELLO 


 SCHOOL MUSIC SERIE 


 STRAUS . \ 


 ECA 


 MEVERBEE 


 NOD . 


 MAR 


 SERVIE 


 WAGNER . - 


 HAWKES SON EDITIONS 


 NEW POPULAR 


 ORCHESTRAL PUBLICATIONS 


 SELECTIONS . | CONCERT PIECES 


 HAWKES SON , 


 DENMAN STREET , PICCADILLY CIRCUS , LONDON 


 


 NEW CATHEDRAL PSALTER 


 ORI 


 CONTAINING | 


 PSALMS DAVID . = 


 CANTICLES PROPER PSALMS CERTAIN DAYS 


 EDITED POINTED CHANTING 


 » ORI 


 . 2A 


 TY » 24 . 


 NEW CATHEDRAL PSALTER CHANTS . - 


 ‘ » 30 . 


 . , ; , » 33 . 


 » 35 


 


 ORIGINAL 


 


 ORG : \N 


 COMPOSED 


 ALFRED HOLLINS . 


 ORGAN MUSIC 


 EDITED 


 : JOHN E. WEST 


 RECENT NUMBERS . 


 29 . LARGHETTO 


 COMPOSITIONS 


 > ORIGINAL COMPOSITIONS | 


 F 


 ORGAN 


 1909 . 689 


 SCHOTT & CO 


 NEW ORGAN ALBUMS . 


 RED ALBUM 


 BLUE ALBUM 


 Ascher , Vieuxtemps , Marchant , Lefebure Wely 

 Merkel , Leybach , Beethoven , Liszt , Ernst , Bazzini , Klein , 
 Humperdinck , Nevin . 
 Votume III . 
 GREEN ORGAN ALBUM 
 FAMOUS PIECES ( OriGInaL ARRANGED ) 
 ELECTED EDITI Y 
 REGINALD GOSS - CUSTARD . 
 Price Shillings net . 
 ScuotTr & Co. , 
 157 , Regent Street , 48 , ( sreat Marlborough Street , London , W 

 RECITAL WORKS ORGAN 




 


 SHORT PIECES 


 ORGAN 


 


 SHORT MUSICAL SKETCHES 


 PIANOFORTE 


 EDWARD “ ANTAB 


 ART SINGING 


 HERBERT SIMS REEVES 


 SONGS 


 SOPRANO 


 GEORGE F. BOYLE 


 ENGLISH LYRICS 


 MARY E. COLERIDGI 


 C. H. H. PARR 


 GLORY 


 SONG 


 M. C. COMBERMERE GIBBINGS 


 WW 


 LALEST & SUCCESSFUL SONGS 


 CHORISTER 


 WIN 


 SIDNEY R. COLE 


 ROSEBUD EARLY WALK 


 MEZZO - SOPRANO 


 LITTLE BIRDIE 


 NIGHTS 


 MEZZO - SOPRANO 


 KING HEARTS 


 BARITONE 


 


 MONOTONING 


 C. S. FOSBERY , M.A. 


 DUETS 


 CONTRALTO BARITONI 


 W. G. ROTHER 


 JOHANNES BRAHMS 


 PLEADING 


 SONG 


 COMPOSED 


 EDWARD ELGAR 


 ARRANGED SMALL ORCHESTRA , 


 HARP PIANOFORTE 


 » ) CORNET CLARINET 


 P PIANOFOR 


 AID 


 EEE 


 HARMO 


 ANALY 


 COUNTI 


 DOUBLE 


 FUGUE . 


 FUGAL 


 MUSICA 


 APPLIEI 


 


 NGS 


 RANKIN TAYLOR EDITION NEW EDUCATIONAL WORKS 


 | : S. BACH COMPLETE ORGAN WORKS . . 


 | O ) 


 E BENEZER PROU P WORKS . JUI LION > TOUCH 


 E. SILAS . OVERTURE DI BALLO 


 ENTERING EXAMS . JOHN E. WEST 


 POCKET MANUAL - 


 IMPORTANT WORKS CHORAL SOCIETIES , | 


 WEDDING SHON MACLEAN 


 SCOTTISH RHAPSODY 


 ULYSSES SIRENS 


 DRAMATIC CANTATA 


 SOLI , CHORUS ORCHESTRA 


 FOREST SONG 


 SERENADE 


 CHORUS ( S.A.T.B. ) SOLO 


 LEAVES OSSIAN 


 MERRIE ENGLAND 


 CONCERT VERSION 


 PRINCESS KENSINGTON 


 CONCERT SELECTION 


 MUSIC SELLERS . _ 


 


 MISS AG ! 


 MADAME 


 MR . WAI 


 MR . 


 MICHA 


 FORTN 


 CHAME 


 CHILDR ! 


 SEATED ORGAN ST . GEORGE CHAPEL , WINDSOR HALF . ’ 


 _ TWEN 


