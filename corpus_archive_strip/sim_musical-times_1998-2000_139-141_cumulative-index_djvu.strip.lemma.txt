


 ( 1998 


 index to volume 139 ( 1998Barrett , Richard : ' tract for our time ? ' , vol.139 
 no.1864 ( Autumn 1998 ) , pp.21 - 24 . 
 Boyce - Tillman , June : ' the eye of a woman : 
 Hildegard of Bingen at 900 ' , vol.139 no.1865 
 ( Winter 1998 ) , pp.31 - 36 

 Brindle , Reginald Smith : ' Beethoven 's primitive 
 cell structure ' , vol.139 no.1865 ( Winter 1998 ) , 
 pp . 18 - 24 

 Brindle , Reginald Smith : ' memory hither come ? ' 
 [ guest editorial ] , vol.139 no.1863 ( Summer 
 1998 ) , pp.2 - 3 




 the MUSICAL TIMES / WINTER 2001 in the work : a study in performance practice , 
 review by Susan Bradshaw , vol.139 no.1864 
 ( Autumn 1998 ) , pp.62 - 63 

 Beethoven , Ludwig van , e . Brandenburg : 
 Briefwechsel : Gesamtausgabe , review by 
 William Drabkin , vol.139 no.1864 ( Autumn 
 1998 ) , pp.39 - 45 

 Blake , Andrew : the land without music : music , 
 culture & society in twentieth - century Britain , 
 review by Christopher Fox , vol.139 no.1865 
 ( Winter 1998 ) , pp.46 - 52 




 ( 1998 


 index to volume 139 ( 1998Barrett , Richard : ' tract for our time ? ' , vol.139 
 no.1864 ( Autumn 1998 ) , pp.21 - 24 . 
 Boyce - Tillman , June : ' the eye of a woman : 
 Hildegard of Bingen at 900 ' , vol.139 no.1865 
 ( Winter 1998 ) , pp.31 - 36 

 Brindle , Reginald Smith : ' Beethoven 's primitive 
 cell structure ' , vol.139 no.1865 ( Winter 1998 ) , 
 pp . 18 - 24 

 Brindle , Reginald Smith : ' memory hither come ? ' 
 [ guest editorial ] , vol.139 no.1863 ( Summer 
 1998 ) , pp.2 - 3 




 the MUSICAL TIMES / WINTER 2001 in the work : a study in performance practice , 
 review by Susan Bradshaw , vol.139 no.1864 
 ( Autumn 1998 ) , pp.62 - 63 

 Beethoven , Ludwig van , e . Brandenburg : 
 Briefwechsel : Gesamtausgabe , review by 
 William Drabkin , vol.139 no.1864 ( Autumn 
 1998 ) , pp.39 - 45 

 Blake , Andrew : the land without music : music , 
 culture & society in twentieth - century Britain , 
 review by Christopher Fox , vol.139 no.1865 
 ( Winter 1998 ) , pp.46 - 52 

 1998 ) , pp.68 - 69 

 Brandenburg , Sieghard , ed . : Ludwig van 
 Beethoven : Briefwechsel : Gesamtausgabe , 
 review by William Drabkin , vol.139 no.1864 
 ( Autumn 1998 ) , pp.39 - 45 

 Brougham , Henrietta , Fox , Christopher & Pace , 
 lan , edd . : uncommon ground : the music of 
 Michael Finnissy , review by Wilfrid Mellers , 
 vol.139 no.1865 ( Winter 1998 ) , pp.60 - 62 . 
 Burrows , Donald , e . : the Cambridge companion 
 to Handel , review by Richard Drakeford , 
 vol.139 no.1862 ( April 1998 ) , pp.34 - 35 

 Cooke , Mervyn : Britten and the Far East , 
 review by Arnold Whittall , vol.139 no.1865 
 ( Winter 1998 ) , pp.62 - 64 

 David , Hans T. & Mendel , Arthur , edd . : the 
 new Bach reader : a life of Johann Sebastian Bach 
 in letter and document , review by Yo Tomita , 
 vol.139 no.1865 ( Winter 1998 ) , pp.66 - 68 . 
 DeNora , Tia : Beethoven and the construction of 
 genius : musical politic in Vienna , 1792 - 1803 , 
 review by Robert Anderson , vol.139 no.1864 
 ( Autumn 1998 ) , pp.60 - 61 

 Durr , Alfred & Kobayashi , Yoshitake , edd . : 
 Bach - Werke - Verzeichnis : kleine Ausgabe ( BWV2a ) 
 nach der von Wolfgang Schmieder vorgelegten 

 Makela , Tomi , ed . : music and nationalism in 
 twentieth - century Britain , review by 
 Christopher Fox , vol.139 no.1865 ( Winter 
 1998 ) , pp.46 - 52 

 Marx , AB , ed . Burnham : musical form in the 
 age of Beethoven : select writing on theory and 
 method , review by Andrew Thomson , vol.139 
 no.1862 ( April 1998 ) , pp.28 - 29 

 Mellers , Wilfrid , ed . Paynter : between old world 
 and new : occasional writing on music , review 
 by Arnold Whittall , vol.139 no.1862 ( April 
 1998 ) , pp.31 - 32 

 Puffett , Derrick , ed . : find the key : select 
 writing of Alexander Goehr , review by Andrew 
 Thomson , vol.139 no.1860 ( February 1998 ) , 
 pp.23 - 25 

 Rosen , Charles : the classical style : Haydn , 
 Mozart , Beethoven , review by Ivan Hewett , 
 vol.139 no.1864 ( Autumn 1998 ) , pp.50 - 52 . 
 Rosen , Charles : the frontier of meaning : three 
 informal lecture on music , review by Ivan 
 Hewett , vol.139 no.1864 ( Autumn 1998 ) , 
 pp.50 - 52 

 the MUSICAL TIMES / WINTER 2001 




 ( 1998 


 ( 1998Simeone , Nigel , Tyrrell , John G Nemcova , 
 Alena : Jandcek ’s work : a catalogue of the music 
 and writing of Leos Jandcek , review by Arnold 
 Whittall , vol.139 no.1863 ( Summer 1998 ) , 
 pp.37 - 60 

 Stowell , Robin : Beethoven : Violin Concerto , 
 review by Robert Anderson , vol.139 no.1864 
 ( Autumn 1998 ) , pp.60 - 61 

 lawaststjerna , Erik : Sibelius volume iii : 
 1914 - 1957 , review by Robert Anderson 
 vol.139 no.1861 ( March 1998 ) , pp.30 - 31 . 
 Tommasini , Anthony : Virgil Thomson : composer 
 on the aisle , review by Wilfrid Mellers , vol.139 
 no.1864 ( Autumn 1998 ) , pp.53 - 57 




 THE MUSICAL TIMES / WINTER 2001 


 d.0l 


 s06 


 THE MUSICAL TIMES / WINTER 2001 


 ( 1998 


 ( 1999 


 en } d ) 49 ro MAZ0 ) movie OMG R IID 


 THE MUSICAL TIMES / WINTER 2001Jenkins , Garry & d’Antal , Stephen : Kiri : her 
 unsung story , review by Erica Jeal , vol.140 
 no.1869 ( Winter 1999 ) , p.79 

 Jones , David Wyn : the life of Beethoven , 
 review by William Drabkin , vol.140 no.1867 
 ( Summer 1999 ) , pp.69 - 70 

 Kennedy , Michael : Richard Strauss : man , music , 
 enigma , review by James Morwood , vol.140 
 no.1866 ( Spring 1999 ) , pp.60 - 61 

 Nicholls , David , e . : the Cambridge history of 
 american music , review by Christopher Fox , 
 vol.140 no.1868 ( Autumn 1999 ) , pp.57 - 62 . 
 Osborne , Richard : Herbert von Karajan : a life 
 in music , review by James Morwood , vol.140 
 no.1867 ( Summer 1999 ) , pp.71—73 

 Plantinga , Leon : Beethoven ’ concerto : history , 
 style , performance , review by William Drabkin , 
 vol.140 no.1868 ( Autumn 1999 ) , pp.71 - 72 . 
 Potter , John : vocal authority : singing style and 

 index 
 vol . 140 
 ( 1999 




 the MUSICAL TIMES / WINTER 2001index 
 vol.140 
 ( 1999 

 review by Richard Drakeford , vol.140 no.1869 
 ( Winter 1999 ) , pp.62 - 64 . 
 Simeone , Nigel , e . : " Bien cher Félix " : letter 
 from Olivier Messiaen and Yvonne Loriod to Felix 
 Aprahamian , review by Andrew Thomson , 
 vol.140 no.1868 ( Autumn 1999 ) , pp.69 - 70 . 
 Simeone , Nigel : Olivier Messiaen : a bibliographical 
 catalogue of Messiaen ’ work , review by 
 Andrew Thomson , vol.140 no.1868 ( Autumn 
 1999 ) , pp.69 - 70 . 
 Sipe , Thomas : Beethoven : Eroica symphony , 
 review by William Drabkin , vol.140 no.1867 
 ( Summer 1999 ) , pp.69 - 70 . 
 Smaczny , Jan : Dvorak : Cello Concerto , review 
 by Richard Drakeford , vol.140 no.1869 ( Winter 
 1999 ) , pp.62 - 64 . 
 Steane , John : Singers of the century volume 2 , 
 review by Michael Tanner , vol.140 no.1869 
 ( Winter 1999 ) , pp.78 - 79 . 
 Sutcliffe , W. Dean : Haydn study , review by 
 Ivan Hewett , vol.140 no.1868 ( Autumn 1999 ) , 
 pp.63 - 65 
 Thistlethwaite , Nicholas @ Webber , Geoffrey , 
 edd . : the Cambridge companion to the organ , 
 review by Ann Bond , vol.140 no.1867 
 ( Summer 1999 ) , p.77 . 
 Thomas , Wyndham , ed . : composition , 
 performance , reception : study in the creative 
 process in music , review by Anthony Gritten , 
 vol.140 no.1867 ( Summer 1999 ) , pp.68 - 69 . 
 Tyrrell , John , ed . : my life with Jandcek : the 
 memoir of Zdenka Jandckova , review by 
 Diana Burrell , vol.140 no.1866 ( Spring 1999 ) , 
 92 - 54 
 music review 
 Berlioz , Hector : Benvenuto Cellini , review by 
 Julian Rushton , vol.140 no.1866 ( Spring 1999 ) , 
 pp.30 - 5 1 ] 
 Berlioz , Hector : Choral Music with Orchestra 2 
 review by Julian Rushton , vol.140 no.1866 
 ( Spring 1999 ) , pp.50 - 51 

 Berlioz , Hector : Messe solenelle , review by 
 Julian Rushton , vol.140 no.1866 ( Spring 1999 ) , 
 pp.30 - 51 




 THE MUSICAL TIMES WINTER : 2001 


 index to volume 141 ( 2000 


 the MUSICAL TIMES / WINTER 2001 


 ( 1999 


 ( 2000 


 ( 2000 


 THE MUSICAL TIMES / WINTER 2001nature , review by David Allenby , vol.141 
 no.1872 ( Autumn 2000 ) , pp.66 - 68 

 Jones , Timothy : Beethoven : the ' moonlight ' and 
 other sonata , op.27 and op.31 , review by 
 William Drabkin , vol.141 no.1871 ( Summer 
 2000 ) , pp.77 - 78 

 Kurtzman , Jeffrey : the Monteverdi Vespers of 
 1610 : music , context , performance , review by 
 Peter Holman , vol.141 no.1872 ( Autumn 2000 ) , 
 pp.32 - 57 




 ( 2000 


 THE MUSICAL TIMES / WINTER 2001 


 ( 2000 


 classified advertisement 


 for sale 


 vacancy 


 AUB 


 OR 


 USA 


 THE MUSICAL TIMES / WINTER 2001