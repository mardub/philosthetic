


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 PUBLISHED MONTH 


 CHARLES HALLE “ PRACTICAL PIANOFORTE SCHOOL 


 CHARLES HALLE 


 EDITION NEW POPULAR WORKS 


 CAREFULLY FINGERED REVISED . 


 RAFF , JOACHIM 


 HAYDN , JOSEPH . RUBINSTEIN , ANTON . 


 LISZT , FRANZ . WAGNER , RICHARD . 


 NKHWNHNW HWWPW 


 SNHHWHO WL 


 WENHNDNOAWAHHUN 


 FORSYTH BROTHERS , 2724 , REGENT CIRCUS , OXFORD STREET , LONDON , 


 CROSS STREET , SOUTH KING STREET , MANCHESTER 


 PROFESSIONAL NOTICES 


 ORCESTER CATHEDRAL.—PRECENTOR 


 ING COLLEGE , CAMBRIDGE — 


 ALBERT E. BISHOP . 


 £ 110 


 I. T 


 40 


 TRINITY COLLEGE , LONDON 


 WEYMOUTH STREET , PORTLAND PLACE 


 DEPARTMENT EVENING CLASSES 


 TRINITY COLLEGE , LONDON 


 HIGHER EXAMINATIONS WOMEN 


 TRINITY COLLEGE , LONDON 


 RESIDENT STUDENTS 


 COLLEGE , LONDON . 


 HONORARY MEMBERS 


 TT RINTTY COLLEGE , LONDON 


 TRINITY 


 £ 5 PRIZE . SENIOR DIVISION . 


 3 PRIZE . JUNIOR DIVISION . 


 O AMATEUR COMPOSERS.—MSS . REVISED 


 O COMPOSERS WISHING PUBLISH 


 USIC ENGRAVED , PRINTED , PUB- 


 51 


 EMINENT ARTISTS , 


 ESTABLISHED , APRIL 1866 . 


 ENGLISH GLEE UNION . 


 ASSISTED 


 YORKSHIRE CONCERT PARTY . — 


 ADAME TREBELLI CONCERT TOUR . — 


 AST LONDON ORGAN WORKS 


 GTUTIGART HARMONIUM COMPANY . — 


 EAN CHEAP MUSICAL INSTRUMENTS . — 


 RUSSELL MUSICAL INSTRUMENTS . ; 


 GREAT SALE MUSICAL PROPERTY 


 D’ALMAINE PIANOS 


 485 ) 


 HALF - PRICE 


 CONSEQUENCE DEATH LATE 


 PROPRIETOR 


 SPLENDID STOCK 


 OFFERED 


 15 17 GUINEAS 


 20 25 GUINEAS 


 DOUBLE CHECK ACTIONS HALF - PRICE 


 SEVEN YEARS WARRANTY 


 VIEW 


 5 , FINSBURY PAVEMENT 


 MOORGATE STATION . 


 LONDON AGENTS 


 MESSRS . MOUTRIE SON , 


 PIANOFORTE SALOON 


 55 , BAKER STREET , LONDON , W 


 TESTIMONIAL . 


 HIGHEST CLASS EXCELLENCE 


 MILLEREAU BAND INSTRUMENTS 


 ARTISTS 


 MILLEREAU FRENCH VOCAL HORN C. 


 ORGAN - TONED HARMONIUMS . 


 W. HATTERSLEY & CO . 


 CELEBRATED 


 ORGAN - TONED HARMONIUMS 


 UNEQUALLED MAKERS 


 QUALITY TONE , DURABILITY , RAPIDITY TOUCH , 


 PREDOMINANCE TREBLE BASS , 


 PERFECT ACTION , 


 BRILLIANCY SOLO STOPS , NEWLY INVENTED 


 DOUBLE ACTION , SOLID IMPROVED SOUND - BOARD , 


 DOUBLE EFFECTIVE SWELLS 


 W. HATTERSLEY & CO . , 


 W. P. M. SLATER . 


 E. & W. SNELL 


 IMPROVED ENGLISH HARMONIUMS 


 * TOWN , LONDON , N.W 


 CENTENNIAL EXHIBITION 


 NOTICE . 


 NEW YORK , 


 GENERAL EXCELLENCE 


 STANDARD ” 


 AMERICAN ORGANS 


 PRICES , 12 125 GUINEAS 


 BARNETT SAMUEL & SON , 


 31 , HOUNDSDITCH , E.C. , & 32 , WORSHIP STREET , 


 FINSBURY SQUARE , 


 MUSIC STRINGS — WHOLESALE RETAIL . 


 J. P. GUIVIER CO 


 MANUFACTURERS IMPORTERS 


 KINDS MUSIC STRINGS 


 MUSICAL INSTRUMENTS 


 25 , SHERWOOD STREET , REGENT CIRCUS , PICCADILLY . 


 LOUCESTER MUSICAL FESTIVAL , 


 CATHEDRAL , 


 EEDS MUSICAL FESTIVAL , 1877 , TOWN 


 HALL , LEEDS . 


 PRINCIPAL VOCALISTS : — 


 MDLLE . ALBANI , 


 MDME . EDITH WYNNE , MRS . OSGOOD , 


 MDME . PATEY , 


 MDLLE . REDEKER , MRS . UDIE - BOLINGBROKE , 


 MR . EDWARD LLOYD , MR . WM . SHAKESPEARE , 


 MR . SANTLEY , BAND CHORUS 400 PERFORMERS . 
 Organist : Ui . SPARK . Chorus Master : MR . BROUGHTON . 
 Ourtiine ProuraMMes.—WeEpDNESDAY : Elijah . Evening : 

 - King ( new Cantata ) , Walter Austin , Miscellaneous Selec- 
 tion . THurspay : Mendelssohn Walpurgis Nacht , Beethoven 
 Symphony ( . 8) , Miscellaneous . Evening : Solomon , Fripay : 
 h , G. A. Macfarren ( written Festival ) . Evening : 
 aff Symphony G minor , Miscellaneous . SAaturRDAy : Bach 
 Magnificat D , Mozart Requiem , Beethoven Mount Olives . 
 SEATS GALLERY TICKETS ( Reserved ) 
 FEsTIVAL Offices . 
 Serial Ticket seven performances ( transferable ) ... £ 5 os . 
 Single Ticket , MOrning ose ceo oes vee cone ore vee £ E TS 

 itto CE RIE ae FESS pean eile ey ae 
 SECOND RESERVED SEAT TICKETS Hopkinson 
 Brotuers Co. , Commercial Street . 
 RINNE ocr ooe ” ncn econ ) aes Sas > 
 nnn HS Bac ’ Lobel ve : Seek tere ols ) tng » - aan es 
 FESTIVAL PROGRAMMES gratis 
 FesTIvAL OFFices , nog bg iol 
 OHN WM . ATKINSON , 
 PRED . R- SPARK , } Hon , Secs . 
 Festival Offices , Great George Street ( Town Hall ) , Leeds 




 ROYAL ALBERT HALL CHORAL 


 SOCIETY 


 GROSVENOR CHORAL SOCIETY . 


 MANCHESTER GENTLEMEN GLEE 


 MUSIC HOUSE 


 MUSICAL TIMES 


 SCALE TERMS ADVERTISEMENTS 


 MUSICAL TIMES 


 UNITED STATES , CANADA , 


 AUSTRALIA 


 NOVELLO & CO . , 1 , BERNERS STREET . W 


 MESSRS 


 NOVELLO , EWER CO 


 MESSRS . DITSON & CO 


 ORDERS STATE 


 NOVELLO EDITIONS 


 STUDENT TEXT - BOOK 


 


 SCIENCE MUSIC 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 SEPTEMBER 1 , 1877 


 MUSICIANS MASTERS . 


 GREAT COMPOSERS , SKETCHED 


 . DUSSEK PIANOFORTE SONATAS . 
 EBENEZER Prout , B.A 

 Ir requent subject complaint 
 pianoforte teachers conscientious 
 selection music pupils find 
 difficult good classical pieces moderate 
 technical difficulty . player fairly advanced 
 worse useless Beethoven Sonatas , 
 teaching Mozart . 
 curious thing teachers frequently 
 ignore altogether — un- 
 acquainted — pianoforte works Haydn , 
 Clementi , Dussek . object directing 
 , attention perfect musical beauty , 
 propose remarks Sonatas 
 - named composers 

 Comparatively , educated musicians , 
 know Dussek developing 
 technical resources piano . contemporary 
 Mozart , great pianist , composed 
 Sonatas , Concertos piano 
 orchestra , Quintett , Quartett , Trio piano 
 strings , literally countless number small 
 pieces , Variations , Rondos , & c. thisenormous mass 

 424 MUSICAL TIMES.—SeEptemsper 1 , 1877 

 recalls Beethoven earlier style bold- 
 ness harmonies ; long Adagio patetico 

 grace suavity Mozart . 
 florid ornaments furnished 
 hint Hummel . Finale preceded 
 short Intermezzo , fact 
 Prelude . spirit tune- 
 ful , said movement equal 
 rest Sonata . chief theme 
 trivial , tinge vulgarity ; 
 forms principal material subsequent 
 developments , music rises high level 




 ENGLISH MUSICAL PRIVILEGES . 


 426 


 GEORGE TOWNSHEND SMITH 


 SALE DR . RIMBAULT LIBRARYfided , acted judges . 
 instrumental choral performances great ex- 
 cellence . competition close , resulted m 
 award following band prizes : 1 . Strand Union 
 School , Edmonton ; 2 . St. Pancras School , Leavesden ; 
 3 . West London District School , Ashford ; 4 . St. Mary 
 Orphanage , North Hyde , Hounslow ; 5 . Exmouth Train- 
 ing Ship ; 6 . Milton Schools , Portsmouth . singing 
 prizes given best competing choirs 
 follows : 1 . St. Mary Orphanage ; 2 . Milton School , 
 Portsmouth ; 3 . South Metropolitan Schools , Sutton . 
 presenting prizes band choir masters Sir F. 
 Fitzwygram expressed gratification success 
 competition , intention similar prizes 
 competition 

 Royal Society Musicians received 
 interesting gifts add choice souvenirs great 
 departed possession . proof 
 engraving portrait late Sir W. Sterndale 
 Bennett , signed artist engraver , Millais 
 Barlow . fine work art presented 
 Society , framed suitable inscription , 
 late eminent musician daughter , Mrs. Case . 
 gifts Mr. Charles Neate ( son pianist 
 death noticed MusIcaL 
 Times ) , valuable memorials 
 Neate master Beethoven . consist 
 engraved portrait Beethoven ( date 1814 ) , 
 words recording gift signed mighty master , 
 accidentally let fall large blot ink paper 
 desirous cancelling presentation sub- 
 stituting ; Neate demurred , saying “ 
 blot Beethoven superior page 
 material inferior author . ” portrait Mr. 
 Neate added letter Beethoven father , 
 French , Beethoven says , Neate decide 
 having benefit concert , services avail- 
 able way desire . Royal Society 
 Musicians possesses excellent portraits 
 musical treasures , carefully preserved 
 large meeting - room , Lisle Street , Leicester Square ; 
 pleasant find Charity forgotten 
 relatives long life 
 members warm friends deserving 
 Institution , distributes funds 
 relieving wants aged distressed musicians 

 Dean Chapter St. Paul having kindly 
 agreed sanction special Service . working 
 classes Cathedral , consultation Mr. Robert 
 Alderson Turner , Hon . Secretary Gregorian 
 Association , determined Festival Service 
 given repeated oth ult . , 
 working classes especially invited attend . 
 choir , numbering 1,000 voices , including nearly roo 




 430 


 REVIEWS 


 - SONG . 


 4 — ! ! 4 


 PIANO . | 


 LZ J 


 1 -2 


 TT § 


 TR 


 SSS SS SS 


 _ _ { } . +9494 — 4 


 BEAR 


 = SS | 


 — _ — _ 


 437 


 FOREIGN NOTESVocal performances especial interest given 
 Association known “ Renner Madrigal Quartett ” 
 connection hundredth anniversary foun- 
 dation Germanic Museum Nirnberg . 
 programme comprise number Madrigals 
 time ‘ Meistersinger , ’ compositions 
 similar character Senfl ( 1520 ) , Orlando di Lasso ( 1520- 
 94 ) , Thomas Tallis ( 1585 ) , Thomas Morley ( 1600 ) , 
 rarely , , heard days 
 antiquarian research 

 According Neue Zeitschrift fiir Musik number 
 autograph letters , & c. , eminent composers , 
 possession wealthy amateur , passed 
 hands Dr. Joseph Miiller ( late editor Alige- 
 meine Musik - Zeitung ) . ‘ importance collection 
 greatly enhanced fact contents 
 hitherto entirely unknown , jealously guarded 
 previous possessor , , , testamentary 
 direction , interdicted publication number years . 
 letters thirty - seven number , viz . J. S. Bach ( 3 ) , 
 C. Ph . E. Bach ( 2 ) , Beethoven ( 4 ) , Couperin ( 2 ) , Gluck ( 4 ) , 
 Grétry ( 2 ) , Handel ( 7 ) , Haydn ( 3 ) , Di Lasso ( 1 ) , Lully ( 3 ) , 
 Morley ( 1 ) , Mozart ( 4 ) , Heinrich Schitz ( 1 ) , nearly 
 documents considerable importance 
 student art - history . letters , 
 collection contains unpublished compositions Bach , 
 Couperin , Handel ( complete opera ) , Haydn , Lully , 
 Schitz 

 January town Hamburg celebrate 
 hundredth anniversary representation 
 stage original German Opera . work per- 
 formed called ‘ ‘ Adam und Eva , oder der erschaffene , 
 gefallene , und aufgerichtete Mensch ” ( ‘ * ‘ Adam Eve , 
 man created , fallen , anon raised ” ) ; text 
 written laureate poet Richter , music Johann 
 Theile , quondam Capellmeister great Hansa 
 Town 




 CORRESPONDENCE 


 MUSIC UNITED STATES COLOMBIA . 


 EDITOR ‘ ‘ MUSICAL TIMES 


 


 “ STICKER ACTION ” PIANOFORTES . 


 EDITOR “ MUSICAL TIMES 


 MUSICSELLER THIRTY YEARS ’ STANDING 


 BARKER FUND . 


 EDITOR ‘ ‘ MUSICAL TIMES . ” 


 HENRY SMART 


 HARMONIOUS BLACKSMITH . ” 


 EDITOR “ MUSICAL TIMES 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWSHEPTONSTALL , NEAR Hattrax.—On Tuesday , 31st July , thesacred 
 Cantata , Jonah , performed Parish Church Halifax 
 Parish Church Choir , assisted members choir Heptonstall . 
 Rev. F. Pigou , M.A. , Vicar Halifax , gave appropriate 
 address Church Music . principal parts Cantata 
 taken Mrs. Whitehead ( soprano ) , Miss Empsall ( contralto ) , Mr. 
 Verney Binns ( tenor ) , Mr. Morton ( bass ) . Dr. Roberts , 
 composer work , presided organ , Cantata 
 efficiently rendered , produced favourable impression 

 LEVERINGTON , WisBBCH.—On rst ult . anew Organ stops , 
 built Conacher Co. , Huddersfield , opened Parish 
 Church . Mr. Arthur C. Thacker ( Organist Thorney Abbey ) presided 
 instrument . Choral Service 11.30 , 
 ably rendered choir S. Augustine , Rector , 
 Rev. E. J. Littlewood . sermon preached Lord Bishop 
 Winchester . o’clock Mr. Thacker displayed accustomed 
 efficiency Organ Recital works Handel , Beethoven , 
 Mendelssohn , Batiste , & c. , church crowded . 
 organ admirably constructed , tone fine rich 

 Marcu.—On rst ult . successful Festival Parochial Choirs 
 celebrated Church St. John , able direction 
 Organist , Mr. Charles Greenwood . following Choirs took 
 service , viz . St. John , March ; St. Mary , March ; Holy 
 Trinity , Coates ; Coldham ; Welney ; total number voices , 104 . 
 interior church tastefully decorated , congre- 
 gation numerous . service commenced processional 
 hymn , sung surpliced choirs , ‘ ‘ Hark , sound holy voices . ” 
 Anthem taken Psalm cl . , “ Praise God 
 holiness . ” Hymns 157 , 323 , 335 , concluding Reces- 
 sional ( 197 , Ancient Modern ) , grand old tune , “ St. 
 Ann . ” Ely Confession ( usual St. John ) sung , 
 Tallis Responses . Canticles sung service 
 Dr. Wesley , special Psalms Anglican chants . 
 eloquent appropriate extempore sermon preached 
 Rev. Canon Bulstrode , Ely Cathedral , founded text “ Seek 
 ye kingdom God , righteousness . ” offertory 
 liberally responded . proceeds devoted 
 “ Choral Fund 




 OBITUARY 


 MONTH . 


 CONTENTS 


 PAGE 


 RGAN METAL PIPES , ORGAN PIPES 


 T ? TRADE PROFESSION.—TWO 


 NEW NATIONAL - SONGS . — 


 HARVEST ANTHEMS 


 TAINER , ' — SHALL DWELL LAND . 


 ALLCOTT , W. H.—THOU VISITEST EARTH 


 ACFARREN , G. A.—GOD SAID , BEHOLD , GIVEN 


 ATTISON , T. M — O PLENTIFUL THY GOOD- 


 AYLOR , W.—THE EYES WAIT THEE . 


 RIDGE , J. FREDERICK.—GIVE UNTO LORD 


 HYMNS TUNES 


 


 HARVEST 


 SELECTED 


 HYMNARY . 


 PRICE PENNY 


 O LORD , HEAVEN THY POWER 


 GERMAN 


 ALBERT LOWE 


 NEW HARVEST ANTHEM 


 EARTH LORD , ” 


 ALBERT LOWE 


 HARVEST CAROL 


 “ HOLY SEED - TIME 


 EW HARVEST ANTHEM.—*O 


 “ O SING UNTO LORD THANKSGIVING . ” 


 NEW ANTHEM HARVEST . 


 EARTH REMAINETH 


 HARVEST . 


 THOU VISITEST EARTH 


 GOD LORD 


 LORD 


 COMPOSED 


 CHARLES JOSEPH FROST 


 ARVEST ANTHEM . — ‘ 1 HOU CROWNEST 


 WORSHIP LORD BEAUTY 


 HARVEST 


 KYRIES . 


 GLORIAS ( GOSPEL ) . 


 GRATIAS ( GOSPEL 


 REDUCED PRICE . 


 ANGLICAN 


 PSADLTE R : CHA N - F 


 SINGLE DOUBLE 


 EDITED 


 REV . SIR F. A. GORE OUSELEY , BART . , ETC 


 


 EDWIN GEORGE MONK 


 NOVELLO OCTAVO EDITION 


 PAPER BOARDS 


 PAPER BOARDS , CLOTH BACKS GILT LETTERING , 


 HANDEL MESSIAH - - - - - - 2 6 


 ” JUDAS MACCABZUS - - - * 2 6 


 ” ST . PAUL - - - - 2 6 


 HAYDN CREATION - - - - - - 3 , 6 


 LONDON : NOVELLO , EWER CO 


 J OS * NEW SONGS CONTRALTO VOICE . 


 ORATORIO . 


 4 - 7 


 447 


 EDITED DR . STAINER 


 READY , PRICE SHILLINGS 


 PIANOFORTE 


 ERNST PAUER 


 PRINCIPAL PROFESSOR PIANOFORTE 


 NATIONAL TRAINING SCHOOL MUSIC 


 CONTENTS 


 SN DAB YW NS 


 CATHEDRAL PSALTER 


 CHANTS 


 EDITED 


 S. FLOOD JONES , M.A. , J. TROUTBECK , M.A. , 


 PRECENTOR WESTMINSTER ; MINOR CANON WESTMINSTER ; 


 ORGANIST WESTMINSTER ; ORGANIST ST . PAUL ; 


 


 JOSEPH BARNBY 


 PRECENTOR ETON 


 LONDON : NOVELLO , EWER CO 


 CATHEDRAL PSALTER 


 POINTED CHANTING 


 PUBLISHED SANCTION 


 REV . DEAN ST . PAUL 


 


 REV . DEAN WESTMINSTER 


 OON WH 


 LONDON : NOVELLO , EWER CO . , 


 1 , BERNERS STREET ( W. ) , 80 & 81 , QUEEN STREET ( E.C 


 449 


 HYMNARY 


 BOOK CHURCH SONG 


 FOLLOWING EDITIONS READY : — P 


 NEW HYMNAL . 


 TONIC SOL - FA EDITION 


 TONIC SOL - FA EDITION 


 7 BURNLEY TUNE - BOOK.—NOTICE 


 MISSIONARY MANUAL TUNE - BOOK . 


 MISSIONARY MAN UAL ‘ HYMNS PRAYERS . 


 REV . R. BROWN - BORTHWICK 


 ENTWORTH PHILLIPSON * GUIDE 


 YOUNG PIANOFORTE TEACHERS STUDENTS . ” 


 AKER PIANOFORTE TUTOR 


 NEW METHOD . 


 USIC NEW CODE . — ‘ “ 


 450 


 SYDNEY SMITH 


 NEW PIECES 


 SELECT 


 PIANOFORTE PIECES 


 STEPHEN HELLER 


 AUBADE 


 UNE PETITE FEUILLE ... .. .. .. 3 0 


 LONDON : ASHDOWN & PARRY 


 HANOVER SQUARE 


 - SONGS 


 


 R. L. DE PEARSALL 


 CONTENTS VOL . X 


 CONTENTS VOL . XI 


 


 R. L. DE PEARSALL 


 COLLEGIATE SERIES 


 DR . S. S. WESLEY 


 BERTHOLD TOURS 


 DULCIANA 


 SCHUMANN . 


 J. P. KNIGHT 


 J. L. ROECKEL 


 G. B. ARNOLD , MUS . DOC . 


 C. E. HEY 


 ERNEST LINDE . 


 C. JEFFERYS 


 67 , BERNERS STREET 


 NEW PUBLICATIONS 


 MELODIOUS MELODIES 


 ARRANGED HARMONIUMThe subjects selected Works 

 Auber . Haydn . Rossini . 
 Batiste . Hiller . Schubert . 
 Beethoven . Méhul . Schumann . 
 Boccherini . Mendelssohn . Spohr . 
 Chopin . Meyerbeer . Verdi . 
 Donizetti . Mozart . Wagner . 
 Handel . Rink . & e 

 NEW NUMBERS 




 HOUSEHOLD MELODIES 


 PIANOFORTE 


 VE LIVED LOV ED 


 14 . 


 15 . WINGS 


 16 . O ERIN , COUNTRY 


 17 . OH , BLOOM 


 18 . COME , LET HAPPY 


 LONDON : 


 ECOND SERIES 


 ANGLICAN CHORAL SERVICE BOOK . 


 USELEY MONK PSALTER 


 OULE COLLECTION WORDS 


 OULE COLLECTION 527 CHANTS , 57 


 PSALTER , PROPER PSALMS , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI 


 ORDER HOLY COMMUNION 


 “ UNTO , O LORD . ” 


 ANTHEM 


 OFFERTORY SENTENCES 


 ( COMPLETE ) 


 SET MUSIC 


 CHARLES JOSEPH FROST 


 QC)FFERTORY SENTENCES COMPLETE ( 20 


 OFFERTORY SENTENCES 


 APPENDIX FESTAL USE , 


 SET MUSIC 


 JOSEPH BARNBY . 


 Y SOUL TRULY WAITETH 


 DR . SPARK NEW ANTHEM 


 , ISRAEL 


 HOSO DWELLETH 


 ARVEST ANTHEM . — “ PRAISED 


 LORD DAILY . ” 


 ING MERRILY UNTO GOD 


 AROLS USE CHURCH 


 CAPTIVITY . — * WAIT 


 MANUAL SINGING 


 USE CHOIR TRAINERS & SCHOOLMASTERS . 


 - NINTH EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT - SINGING MANUAL 


 COLLEGIATE VOCAL TUTOR 


 COLLEGIATE ORGAN TUTOR . 


 EDITED FREDERIC ARCHER . 


 COLLEGIATE PIANOFORTE 


 TUTOR 


 CONSECUTIVE FIFTHS 


 OCTAVES COUNTERPOINT 


 ESSAY , R. L. DE PEARSALL 


 CATHEDRAL CHANT - BOOK 


 NEW BALLADS 


 BERTHOLD TOURS 


 R. L. DE PEARSALL 


 KILL’D COCK ROBIN ? ... — « .. 38 


 NEW EDITION 


 DR . BENNETT GILBERT 


 SCHOOL HARMONY 


 TRUTH EASE MUSIC . SHARP FLAT 


 KEYS INSTRUMENTALIST VOCALIST 


 SEQUENTIAL SYSTEM MUSICAL 


 NOTATION 


 454 


 THEORY MUSIC . WILLIAM J. YOUNG ’ 


 O OUNG 


 LOUISA GIBSON . POPULAR - SONGS 


 SECOND BOOK 


 POEME DE PAUL POIRSON ET LOUIS GALLET . 


 STREET , MIDDLESEX HOSPITAL , LONDON , W 


 POPULAR - SONGS GABRIEL 


 POET DAUGHTER 


 - SONG 


 WORDS ROBERT BURNS 


 SET MUSIC S.A.T.B. 


 LFRED R. GAUL CHORAL SONGS , 


 OPINIONS PRESS 


 CHORAL SONGS SCHOOL & HOME 


 - ORIGINAL SONGS 


 


 , , VOICES . 


 SELECT COMPOSITIONS 


 


 GREAT MASTERS , 


 ARRANGED ORGAN 


 A. H. BROWN 


 ANDANTE INA ORGAN 


 SAMUEL SEBASTIAN WESLEY 


 N AIR HOLSWORTHY CHURCH 


 SMALLWOOD 


 CLASSICAL & SECULAR EXTRACTS 


 


 PIANOFORTE . 


 NOVELLO , EWER CO . COMPLETE 


 UNIFORM EDITION 


 MENDELSSOHN 


 THIRTEEN - SONGS 


 NOVELLO 


 TONIC SOL - FA SERIES 


 TRANSLATED EDITED 


 HEAR PRAYER ...   .. — ... MENDELSSOHN 0 3 


 ORCHESTRAL SCORES 


 GUINEA 


 HANDEL MESSIAH 


 HANDEL ISRAEL EGYPT 


 HAYDN CREATION 


 LONDON : NOVELLO , EWER CO 


 CHALLEN SONS 


 GOLD MEDAL PIANOS 


 20 , OXFORD STREET , LONDON 


 ESTABLISHED 1804 . 


 | PRIZE MEDAL , LONDON , 1862 . | | GOLD MEDAL , SOUTH AFRICA , 1877 


 CHALLEN & SON , 20 , OXFORD STREET . 


 PRESS 


 CHRISTIAN PILGRIM 


 PILGRIM PROGRESS 


 SACRED CANTATA 


 SET MUSIC 


 WILFORD MORGAN 


 18 , SURREY STREET , LONDON , W.C