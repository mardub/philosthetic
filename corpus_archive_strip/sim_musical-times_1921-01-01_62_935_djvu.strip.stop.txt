


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 FOUNDED 


 PUBLISHED 


 ROYAL CHORAL SOCIETY 


 C.V.O. , N 


 SATURDAY , JANUARY 1 


 ESSIAH .- - 


 MISS RUTH VINCENT . 


 MISS PHYLLIS L&TT . 


 MR . BEN DAVIES . 


 ROBERT RADFORD 


 HANDEI 


 MR 


 SATURDAY , FEBRUARY 5 , ! P.M 


 SAMSON DELILAH 


 MADAME KIRKBY LUNN 


 MR . FRANK MULLINGS . 


 MR . EDWARD HALLAND . 


 MR . EDMUND BURKE 


 SAINT - SAENS 


 ROYAL ACADEMY MUSIC . 


 YORK GATE , MARYLEBONE ROAD , LONDON , N.W.1 


 ROYAL COLLEGE MUSIC . 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. 7 . 


 MAJESTY KING . 


 921 . | 


 ROYAL COLLEGE M 


 USIC PATRON FUND | 


 JANUARY 1 


 1844 


 MONTH 


 ASSOCIATED BOARD 


 R.A.M. R.C.M. 


 LOCAL EXAMINATIONS MUSIC 


 21 


 19 


 GUILDHALL SCHOOL MUSIC 


 ESTABLISHED MANAGED CORPORATION LONDON . 


 VICTORIA EMBANKMENT , E.C. 4 . 


 PRINCIPAL ° LANDON RONALD . 


 ROYAL COLLEGE ORGANISTS 


 ROYAL 


 MANCHESTER COLLEGE MUSIC 


 18 , 


 MUSICAL 


 BIRMINGHAM & MIDLAND INSTITUTE 


 SCHOOL MUSIC . 


 SESSION 1920 - 1921 


 MANCHESTER SCHOOL MUSIC . 


 FOUNDED 1892 . 


 UNIVERSITY DURHAM 


 LONDON COLLEGE CHORISTERS . 


 BLOMFIELD CRE 


 VIOLINISTS 


 LTD 


 MARTIN LANE , LONDON , W.C 


 EXAMINATIONS 


 COLLEGE 


 VIOLA 


 » ELE 


 JACKSON 


 TE CHNIQUER 


 W.1 


 MUSIC SECT ION . 


 W.C.1 


 Y. M.C.A. , 


 COMPOSITIONS 3V SE D. 


 WRITTEN SONGS 


 1S1 


 MUSICAL 


 BRIGGS 


 LONDON COLLEGE MUSIC . 


 GREAT MARLBOROUGH STREET , LONDON , W 


 EXAMINATIONS — LOCAL HIGHER 


 A. GAMBIER HOLMES , 


 MUSIC 


 VICTORIA COLLEGE 


 LONDON . 


 INCORPORATED 1891 . 


 158 


 INCORPORATED GUILD CHURCH 


 MUSICIANS 


 VENERABLE " ARG HDE ACON 


 MIDDLESEX 


 F.R.A.M 


 TION 


 1921 


 INDUC 


 ASSOCIATE ( A.1.G.C.M. ) , LICENTIATE ( L.1.G.C.M ) ) , 


 COMPETITIONS , 1921 . 


 BRONZE MEDALS 


 F.R.A.M 


 F.1.G.CM 


 ) . , A.R.C.M 


 URCH 


 , D.D 


 LESEX 


 A.M 


 NDUCTION 


 [ L.1.G.C.M , ) , 


 AOLIAN HALL . MR . CHARLES TREE 


 


 NUSICAL ( COMPETITION ) FESTIVAL . — BRITISH MUSIC SOCIETY 


 40H I4TH . ( NATIONAL INTERNATIONAL 


 CONCERT DIRECTION . £ 15,000 WANTED 


 ARTISTS ORATORIO BALLAD FOSTERING ENCOURAGEMENT 


 CONCERTS . BRITISH MUSIC HOME ABROAD 


 


 GENERAL SECRETARY 


 HOOL M 25 LL S B N. NEWCASTLE - - LYMI 


 4 MUSICAL 


 LONDON , W.C , . 1 


 MATRIC 


 COMBINED 


 A.R.C.O 


 A.R.C.M. 


 B.A 


 RCO 


 SAVE 


 M.C.A 


 MONEY 


 ROAD 


 


 TIME 


 PHI 


 ILKESTON NOTTINGHAM 


 201 


 CORRESPONDENC CE 


 TUITION 


 NORMAN SPRANKLIN 


 DIPLOMA PAPER WORK . 


 L.R.A.M 


 COURSES 


 APPRECIATION , 


 KNOWLEDGE 


 MUSICAL 


 GENERAL MUSICAL 


 SINGLE SUBJECTS 


 LINDEN ROAD , 


 BRISTOL 


 3033 


 48 , 


 REDLAND 


 CITY CAPE 


 TOWN 


 YICIPAL ORCHESTRA . 


 MUSICAL DIRECTOR 


 CONDUCTOR 


 CORPORATION 


 MUN 


 APPOINTMENT 


 COUNTY BOROUGH SOUTHPORT 


 EDUCATION COMMITTEE . 


 


 MUSICAL 


 1921 


 PROFESSIONAL NOTICES 


 MOUNTFORD SCOTT ( TENOR ) . 


 R. J 


 MR . 


 A.R. 


 F.R.C.O. , 


 ( COMPLETE TRAINING CORRESPONDENCE 


 MISS LYDIA JOHN 


 A.R.A.M. CONTRALTO . 


 MR . SAMU EL MASTE 


 TENOR 


 RALPH WILLIAMS ( LR 


 RS 


 A.M 


 WILLIAM BARRAND 


 OSWALD PEPPERCORN 


 HERBERT TRACEY 


 BASS ) . 


 LEDGE 


 MRS . J. 


 PIANIST , A.R.M.C.M 


 R. MAITLAND 


 DOUGLAS STEVENS 


 ( TEACHER ) ELOCUTION 


 NEW OXFORD STREET , 


 5 W.C. 1 . 


 R 


 L.R.A.M 


 MR . W. 


 ITED , 


 H. BREARE 


 STUDENTS PROFESSION 


 MR . AMBROSE COVIELLO 


 | COMPOSERS ’ MSS 


 REVISED PREPARED PRINTING 


 COURSE ENSURING THOROUGH UNDERSTANDING PRACTICAL 


 EFFICIENCY 


 G. HUBI- NE AV 4 OMBE 


 ( LYRIC AUTHOR ) . 


 MUSICAL COMPOSITIONS 


 EVISION 


 PECIAL CORRESPONDENCE COURSES : — 


 F.R.C.O. SPECIALIST CORRESPONDENCE 


 MUSICAL 


 DR . LEWIS ’ TEXT - BOOKS 


 N ISS F. HELENA MARKS 


 TORMAN & BEARD P NEUMATIC ACT 


 A.R.C .M. B 


 ISS MARGARET YOUNG , 


 ELEMENTS HARMOM 


 W ORE E . \THEDRA 


 SING \NTE 


 H ORGANS 


 PIANIST 


 ENGLISH 


 FOLK - SONGS 


 COLLECTED ARRANGED PIANOFORTE ACCOMPANIMENT 


 


 CECIL J. SHARP 


 SELECTED EDITION 


 SONGS BALLADS . 


 ( VOLUMES I. II 


 PREFACE . 


 TRINITY COLLEGE MUSIC 


 RT . HON . EARL SHAFTESBURY , K.P 


 SUCCESSFUL PI 


 _ , . 


 TUSK 


 GRADED VIOLIN 


 CYRIL SCOTT | 


 3 . SCHERZO . 4 . ELEGY . OLD FRENCH 


 5 . RONDO RETROSPECTIVO . © HARPSICHORD PIECES 


 AKRANGEL 


 PIANOFORTE SOLO 


 PARTS , TWELVI 


 


 ELKIN & CO . LTD . ALFRED MOFFAT 


 4ND SINGING - CLASS CIRCULAR 


 JANUARY 1 


 IQ21 


 PERFORMING FEES 


 ITALIAN COMPOSERS 


 INTRODUCTION 


 


 1789 - 1870 


 II 


 CONCERT - ROOM SONG 


 BACH 


 S70 


 ORGAN WORKS 


 IV . — 


 


 HARVEY GRACE 


 LATER WEIMAR PRELUDES 


 DES 


 1921 13 


 55 


 SS 


 14 


 15 


 16 


 NEW LIGHT EARLY TUDOR 


 COMPOSERS 


 XIV.—RICHARD BRAMSTON 


 15 


 VILL 


 * ‘ BEGGAR 


 


 OPERA ’ 


 FRANK KIDSON 


 J . : : . > SYDNEY GREW 


 XUM 


 20 


 1921 


 CRITICS 


 MOULT 


 CONCERTS 


 1921 21 


 


 SONY 2 4 


 MUSICAL TIM 


 ES — JANUARY 


 1921 2suggest 

 haste add 
 written Jaily News given pleasant 
 shock . Asa result , doubt , prodding 
 excellent music critic , celebrated 150th 
 anniversary Beethoven birth giving 
 leading article composer , , 
 page , symposium present position 
 musical influence 

 reader know fairly recent book 
 giving specifications particulars 
 principal organs British Isles — 
 style old Hopkins Rimbault ? 
 correspondent anxious obtain book . | 
 view recent developments organ - building , 
 awork kind , 
 

 grammes — Lieder German language , 
 rule London programmes 

 remember time 
 Mr. Fanning visit attempt sing German 
 songs original language caused ‘ certain 
 liveliness ’ West End concert - hall . ‘ 
 groups ’ ‘ programmes ’ songs 
 rule , months Mr. Fanning 
 visit . bursting celebrate 
 Beethoven centenary hearing favourite 
 * Adelaide ’ tongue 
 born ; far singer ready 

 sung 

 onslaught , 

 * Walzermasken , ’ , defied conquest 
 years daily battlement , nearly 
 years claim maturity pleasure 
 performance . fact worked 
 Beethoven Op . 2 , . 3 , time recently , 
 memorized polished week , achieved 
 * Pathétique ’ days . gives 
 ratio 

 ‘ Satire , ’ ? spirit satire 
 * Karikatur , ’ assail pedantry 
 beautiful music . ‘ Oh , , 
 course , ’ . fast , ; look 
 score handled 
 instead sepulchre . old 
 Albrechtsberger rise hear 
 doubtless drop dead 




 MARY GARDEN 


 


 SPLENDOUR 


 REVEALED 


 RUTH MILLER 


 192 


 PRIZE COMPETITIONS 


 1921 


 SONGS 


 


 1921 


 PIANOFORTE 


 ORGAN 


 XUMNovember Woods ’ dates years later 
 ‘ Garden Fand , ’ shows composer 
 reached stage development . 
 closely knit , lucid , trace 
 effort . inevitably right . 
 contrasts effective , 
 come . 
 programme psychological , dealing inner 
 experiences share . 
 great mastery orchestral colour , highly 
 individual . middle section , happy 
 memories contrast gloom November 
 wood , composer reaches high level imagina- 
 tion inspiration , bigness 
 music achieve 

 Mr. Hamilton Harty conducted sympathy 
 enthusiasm , Philharmonic Orchestra played 
 finely , Excellent , , playing 
 Mr. Harty skilful discreet arrangement 
 Handel ‘ Water Music ’ Tchaikovsky fifth 
 Symphony . December 16 
 fiftieth anniversary Beethoven birth , 
 ‘ Coriolan ’ Overture included programme 

 generous tribute Beethoven 

 rest programme devoted un- 
 accompanied singing Philharmonic Choir , 
 Mr. Kennedy Scott . Choir great 
 strides year . quality tone , cleanness 
 attack , variety expression difference 
 remarkable . singing music Sweelinck , 
 Pretorius , Calvisius , Orlando Gibbons 
 human expressive , clearness part- 
 singing admirable . Equally good 
 singing Dr. Vaughan Williams ’ arrangements 
 English Folk - Songs . exceedingly 
 clever , times composer allow 
 skill making choral effect obscure 
 simple beauty tunes . Wassail Song , 
 humorous words music , happiest 
 , repeated 




 CHAMBER MUSICPIANISTS VIOLINISTS 

 sphere pianoforte playing outstanding 
 events Chopin , Schumann , Liszt | 
 recitals M. Cortdét , regrettable feature , | 
 Wigmore Hall too| 
 small audiences . new 
 advantage said remarkable playing , | 
 need added said | 
 Mr. Lamond Beethoven recitals . Mr. | 
 Hoffmann popularity speedily growing , is| 
 satisfactory know returns in| 
 summer heard orchestra . Mr. | 
 Rubinstein continued gain favour , | 
 playing sonatas M. Kochanski showed 
 deeper musical qualities required 
 brilliant soloist . interesting 
 pianoforte recitals music 
 pianofortes given December 4 Miss Irene 
 Scharrer Miss Myra Hess , unanimity 
 style remarkable . played things 
 arrangement ‘ Aprés - midi d’un Faune , ’ an| 
 interesting experiment , altogether successful | 
 spite excellence performance . AsIwasat 
 Kingsway Hall moment , rely 
 authority trustworthy friend . Miss Winifred 
 Christie earned high place 
 pianists United States . given 
 orchestral concert recital , - balanced 
 lucid style finished technique 
 wide appeal . room - 
 musicianly playing 

 violinists given recitals , 
 interesting , , M. 
 Kochanski , sterling musical qualities 
 absence sensational methods deserve high 
 praise . new pieces Szymanowsky , 
 played , charm . Mr. Louis Godowsky 
 young violinist great promise times | 
 mentioned . performance Elgar | 
 Concerto recital ‘ best thing | 
 far . played graceful } 
 effective little pieces 




 SINGERS SINGING 


 NOTES JANUARY 


 MARCEL DUPRE CONCERT 


 30 


 CARL ROSA SEASON NEW WORK 


 REPERTOIRE 


 POPULAR 


 W 1 


 VERSATILE COMPANY 


 NEW ADDITIONS 


 ALTO 


 ENOR 


 ANTHEM GENERAL USE 


 SS — — — — — = = 


 ROSEATE HUES EARLY DAWN 


 SSS = SS 


 > * _ — — _ — = — _ — _ — — _ 


 SSS = = = = 22S ES SE 


 2 — - _ L _ _ _ _ — — — — _ — 


 ANS = SEN 


 ROSEATE HUES EARLY DAWN 


 — — — 22124 


 4 — 5 


 9 - 2 


 = = SS SS 


 _ SS SS = SS SS ES LES TS 


 EAL SA 2 


 


 = LS = — — 


 $ $ — — — — — — — — — — — — — — , - — — — _ — — — — _ _ _ _ — . 


 ROSEATE HUES 


 EARLY DAWN 


 = EEG 


 — _ = ; ~ | — — = - { — - 7 


 XUM 


 ROSEATE HUES EARLY DAWN 


 KING 


 = — — — — . — — # # - — _ } , _ 


 XUM 


 1921 


 H.M 


 41 


 RHOW 


 TOLLESaint - Saens 

 Guilmant D minor Sonata ; 
 Beethoven uta ; Introduction , 
 Variations , C. H. Llovd ; Fantasia E flat 

 Son 




 ‘ CHURCH HEATED 


 ORGAN RECITALS 


 APPOINTMENTS 


 MUSICAL 


 MR 


 ERNEST NEWMAN ‘ PIANO 


 NSHIVELY 


 


 45but apart altogether , great charm 
 Mr. Button book lies youthful exuberance 
 throws overboard authority altogether . 
 appeals authority , reason , system , logic . 
 ( Lhe title work proclaims . ) fall , 
 , , authority knock 
 case . , leaves open 
 obvious retort Mr. Button Sir Edward Elgar 

 le , , rate , Bach , Beethoven , 

 , 
 ' — , Xc . , \. R 




 10 


 VINCENT D’INDY VIEW ITARMON\ 


 LETTERS GREAT COMPOSERS 


 PERMANENT OPERA ENGLISII 


 LAUDI 


 REV 


 ORGAN LOLINGEN , GERMAN 


 PRICE CLASSIC FIDDLES 


 1420 


 16960 


 CORNET WANTED 


 10 


 14920 


 C. SANFORD TERRY 


 D * MAINZER MUSIC BOOK 


 YOUNG . 


 PREFACE 


 [ ) * MAINZER MELODIES CHIL- 


 FAVORITE SONGS SEASON , 


 7E WELCOME THEE THY NATIVE 


 ROYAL ACADEMY MUSICunl Elgar . 
 Mr 

 Beethoven Pianoforte Concerto C minor , played 
 Miss Betty Humby , /%va / e Rachmaninov Piano- 
 forte Concerto , played Miss Elsie Betts , 
 showed pianists considerable promise . 
 interesting songs Sylvia Carmine ( student ) , Saint- 
 Saéns ’ Ballad ‘ Drummer Betrothed , ’ Mendels- 
 sohn scena * ‘ Infelice , ’ completed programme 

 local professional musicians , 
 Burnett ( pianoforte 




 MUSICAL T 


 IMES — JANUARY 


 1921 


 MUSICIANS 


 CORE 


 PON DENTS 


 ABERDEEN 


 R. J 


 BATH 


 BELFAST 


 BIRMINGHAM 


 49 


 BOURNEMOUTH 


 BRISTOI 


 20 , 


 CORNWALL 


 COVENTRY DISTRICTDARLINGTON DISTRICT 

 music flourishing , 
 oncerts Polum Hall School . , 
 28 , ‘ cello pianoforte recital 
 Misses Hetty Ethel Page , Miss Elsie Chambers . 
 principal works Sonatas Saint - Saens 
 Sammartini . November 2 London Philharmonic 
 Quartet played Beethoven 
 ( Quartet , * Puck 

 Chamber 
 




 DEVON 


 MUSICAL 


 EDINBURGH 


 11SOOvertur 
 time writing , interesting concert 
 series 

 November 17 Royal Choral 
 performance Mendelssohn ‘ Hymn 
 ‘ Fallen , ’ Beethoven Choral Fantasia ( cal 
 pianist , Mr. Ramsay ind Gustav Hlolst setting 
 Psalm 86 Psalm 147 . named numbers , 
 given time Edinburgh , Psalm 86 
 ex eedingly 
 untal treatment cf second example inter 
 esting , general effect uplifting , 
 soloists Miss Dorothy Silk , Mrs. John Walker , 
 Mr. Arthur Jordan 

 evening Mr. Appleyard gave pianoforte 
 pupil Leschetitzky , great credit 

 November 25 , Miss Marjorie Greenfield ( vocalist 
 Miss Dorothy Chalmers ( violin ) gave 
 interesting chamber concert . Miss Chalmers , accompanied 
 hy Miss Isobel Gray , gave fine reading Brahms ’ 
 ) minor Sonata , Op , 108 , Miss Greenfield covered 
 wide range vocal art 

 Mr. John Petrie Dunn , assistant - lecturer Prof. Tovey 
 Edinburgh University , accomplished pianist . 
 December 7 — co - operation Mr. Watt Jupp ( violin 
 Mr. Bernard gave fine 
 programme . Trios Beethoven Op . 70 , . 1 , 
 Variations , Op , 121 

 December 9 , M. r - Grondahl gave pianoforte 
 recital , strengthened impression 
 orchestral concert referred 




 GLASGOW 


 _ — _ HASTINGS 

 laudable ambition Hastings Corporation 
 place town rank pleasure resorts 
 musically d — rapidly materialising . decided 
 step direction November 18 , 
 Mr. Julian Clifford secured provincial performance 
 Montague Phillips ’ new Pianoforte Concerto — 
 smaller sensations recent * Proms . ’ 
 impression , warmly received . 
 instant appeal undeniable . Planned grandiose 
 scale , bristles difficulties , capable 
 exponent Mr. William James desired ; 
 orchestra , practically ‘ read ’ work , en ered | 
 intricacies main , 
 composer safe guidance . concert offered 
 ‘ Unfinished ’ stirring account Rimsky- 
 Korsakov ‘ Caprice Espagfiole . ” Mr. W. Lf . Reed played | 
 Beethoven Violin Concerto true disciple | 
 Joachim , essential qualities 
 Beethoven player . Moszkowski Pianoforte Concerto , 
 played Miss Helen Guest facile execution , 
 best pedalling , heard time . | 
 Miss Lena Kontorovitch alternately passionate | 
 tender Saint - Saens ’ Violin Concerto , found 

 openings exercise surprisingly intense 
 temperament 




 KENTin aid West Kent General Hospital 

 Beethoven fifth 

 Overture , 




 24 , 


 1 1 1 , 


 LIVERPOOLworthily sustained reputation younger natiy 
 school . Balfour Gardiner choral tone - poem 
 * April , ’ chorus orchestra , provided delightfy 
 minutes performance ally conducted chorys . 
 master , Dr. A. W. Pollitt . subject poem ly 
 Edward Carpenter , ecstatic apostrophe th 
 spirit Spring , ‘ April , month Nymphs , Faun , 
 Cupids . ’ difficult conceive musi 
 keeping poetic fancy imagery th 
 lines . chorus - makes instrumental demands 
 singers , courageously surmounted , counsels 
 perfection apart , hoped hearing 
 accorded extremely clever effective work 

 performance Beethoven C minor Symphony 
 Philharmonic concert November 30 gave immensg 
 satisfaction number subscribers oider 

 

 school 
 consider heard sufficient music 
 ultra - modern type long time . 
 performance , conducted M. Bronislaw Szulc , 
 Warsaw , satisfactory 

 Elgar ‘ Polonia ’ lost interest 
 power appreciative hands M. Szulc , failed 
 preduce specially new symphonic poem , 
 ‘ La Steppe , ’ late Siegmund Noskovski , 
 Warsaw musician mark ( teacher Rozycki , 
 symphonic poem ‘ Anhelli ’ omitted owing lack 
 time ) . vocalist Mr. Frank Mullings , 
 apology ground hoarseness . 
 acceptably heard songs Wagner , 
 especially Hugo Wolt ‘ Secrecy . ’ version 
 Schubert ‘ Erl King ’ equally . M. Bronislaw 
 Szulc , pupil Noskovski Nikisch , comes 
 good musical stock . father years 
 Warsaw Conservatorium , Paderewski 
 professor . sons musicians , notable family 
 record performance given Beethoven Septet 
 Szule fer 

 evident great pains taken 
 preparation Berlioz * Faust , ’ performance 
 given Welsh Choral Union November 20 refi 
 credit able conductor , Mr. Hopkin Evans , 
 superb choral material . years elapsed 
 Union previous performance great work , 
 surpassed present 
 find old spirit , 
 enthusiasm remain . old ideals 




 MANCHESTER DISTRICTThe Co - operative Wholesale Society Choir bids fair soon 
 prominent male - voice choir , 
 ought consign dust - heap things 
 sung occasion . Miss Desmond Mr. Albert 
 Sammons deserve mention beauty 
 distinction executive ability , highet 
 standard taste shown selections 

 Pride place awarded Edith Robinson 
 Quartet work past month Beethoven 
 celebration performances ( Quartets chronological 
 order , Dr. Brodsky Mr. R. J. Forbes follow 
 January Pianoforte Sonatas , oficial Hallé 
 commemoration concert given day notes 
 press 

 young trios — Miss Midgeley Messrs. Hatton 

 K. Moorhouse — determination ambition 
 place city chamber - music 
 Nearly trios quartets sprung 
 years owe early 
 training ensemble musical inspiration Dr. Brodsky , 
 , - years ago , founded quartet bearing 
 . November 30 anniversary recognized 
 Royal Manchester College Music separate 
 presentations ( ) College , ( 4 ) staff , ( c ) 
 replying , Dr. Brodsky commented 
 inadequacy present equipment meet great 
 sudden influx students . met immediately , 
 finest reward ungrudging work 
 years provide necessity 

 transfer Brodsky concerts Monday mid - day 
 Houldsworth Hall attended bad 
 luck way fog hindrances free move- 
 ment enable fair estimate situation . 
 series included Elgar ( Quartet , 
 secon way amplifying Beethoven celebrations , 
 Robinson Quartet taken initiative 

 MUSICAL 




 NEWCASTLE TYNEd work efficiently , 
 

 estra ¢ 
 horn passag : lid 
 | th 
 * Progress 
 mmbated 
 said supersede 
 wy 
 Mozart 
 1 Beethoven 
 t Mozcart . 
 Glasgow University , gave 

 ture 




 NOTTINGHAM DISTRICT 


 16 


 TIMES — JANUARY 1encouraging second chambet 

 Quartet . Beethoven ( Quartet 
 succeeded Joseph Speaight * Shakespearia 
 Pieces ’ string quartet : ( @ ) ‘ Lonely Shepheri , 
 5 ) * ( ueen Mab sleeps , ’ ( c ) * Puck . ’ Finally came Brahny 
 gorgeous ( Quartet , Op . minor ( pianoforte , 
 violin , viola , violoncello ) , performers wi 
 entrancing br.lliance imagination . regret tha 
 felt havirg opportunity hearing XN 

 25 , G 




 G. W. 


 RING 


 


 1921 7 


 OXFORDPachmann came , ‘ good - bye 

 October 15 ; played delightfully . 
 frst series Subscription Concerts took place 
 ( October 22 Town Hall , Sir Hugh Allen 
 conducted members London 
 Symphony Orchestra , giving enjoyable concert . 
 programme included Beethoven Pianoforte Concerto 
 inG , Miss Myra Hess playing solo , beautifully 

 October 25 Hambourg gave excellent recital 
 ‘ Hall , showing absolutely home 
 Beethoven , gave Ravel ‘ Jeux d’eau ’ 
 Debussy ‘ Toccata 

 November 4 came second Subscription Concert 
 building , Bohemian Czech Quartet gave 
 4 fine programme consisting mainly advanced works , 
 notable Smetana Quartet , ‘ Aus 
 meinem Leben . ’ mistake gentlemen 
 altered arrangement printed 
 programme notice eleventh hour , 
 causing disappointment musical 
 audience gone trouble procure scores 




 SOUTH WALESlooked maintain permanent Welsh National 
 Orchestra 

 Cardiff Chamber Music Society held 
 concert season hall High School Girls , 
 evening December I. Miss Jelly d ’ Aranyi ( violin ) 
 Mrs. Ethel Hobday ( pianoforte ) joint 
 exponents Sonatas César Franck , Dohnanyi 
 ( Op . 21 ) , Beethoven ( Op . 30 , , 2 

 December 2 , Albert Hall , Swansea , crowded , 
 occasion appearance M. Alfred Cortét 
 Miss Vera Horton Swansea Subscription 
 Concerts . M. Cortét playing Chopin - 
 Preludes , , memorable experience 




 YORKSHIRE 


 LEEDS 


 58 


 19 


 SHEFFIELD 


 _ _ 


 YORKSHIRE TOWNS 


 


 VIOLS ENGLAND 


 192 ! 59AMSTERDAM 

 Beethoven Festival said 
 great success . time writing 
 concert Cue , devoted master 
 ninth Symphony . work management 
 Festival deserving highest praise . 
 arrangement programmes fourteen 
 chamber music concerts exemplary , 
 doubted better survey Beethoven creations 
 Pianoforte Sonatas 
 found represented , 
 come denomination chamber music proper . 
 late years opportunities 

 circumstance entered Lord Chamberlain Records | heard , 

 works 

 real 
 Beethoven refusal 
 unrelenting self - criticism . 
 wished hear 
 Op . 29 , Septuor . 
 concerts , , lay fact 

 hav 

 mins 

 slightly perceptible . 
 ot 
 especially characteristic ‘ Beethoven 
 contrary popular opinion , 
 innovation 
 ertainly cheraux di 
 Mannheim orchestra , Mozart , 
 ut splendid band , testified 
 * Nozze di 
 Budapest ( Quartet went 
 underlining 
 rheir performance based 
 respect 

 dynamic 

 regarded liberty . 
 orchestral concerts failed 
 , 
 pardonably dull situation saved 
 MM . Egon Petri Leonid 

 Ilona Durigo . 
 Mengelberg 
 Beethoven . Seldom 
 unbounded 
 o magic 
 orc } splendid 
 M. Zimmermann surpassed 
 nances solo Violin Concerto 

 S ywe 
 secured pl 
 p nti n ‘ 1 rate b 
 matter regret 




 W. HARMANS 


 PARIS 


 REVIVALS 


 INDIAN OTELLO 


 GEORGI 


 ROME 


 LOHENGRIN 


 * AME ITALY 


 MUSICAL 


 TIMES 


 61 


 3 — TANUARY 


 PLAIN SPEAKING 


 AUGUSTEO Le festia de Varaignée ’ .. 
 ‘ Tristan Isolda ’ ( P relu le , death 
 Isulda ) 

 extremely regrettable ond annoying incident threatened 
 ruin second concert . reason best known 
 , employees electric - light station 
 felt agerieved , , diabolical malice , arranged 
 strike critical hour Sunday afternoon 
 public spectacles swing . plan succeeded 
 ouly , 5 p.m. electric light failed 
 parts city . Augusteum , Beethoven seventh 
 Symphony reached movement , naturally 

 strings 

 erected 

 
 Manfredini 
 Beethoven 
 Sinigaglia 

 Roussel 




 LEONARD PEYTON , 


 VIENNAVery little requires comment occurred 
 notes . performances ‘ Parsifal ’ given 
 State Opera October 31 November 1 , 
 performances 
 principally splendid work Frau . 
 guest LBerlin State Opera , 
 stay artist appeared 
 ‘ Ring ’ ( Briinnhilda ) ‘ Fidelio ’ ( Leonora ) . 
 guest note Cornelius Bronsgreest , als » 
 Berlin , sustained success parts Rigoletto 
 Papageno . heen heard ‘ Madame 
 Butterfly , ’ ‘ Carmen , ’ ‘ Huguenots . ’ 
 November 29 , performances given Korngold 
 operas , ‘ Ring Tolykrates ’ ‘ Violante , ’ 
 direction composer 

 promised Beethoven Festival second 
 week December . scheme comprises number 
 concerts performance * Fidelio ’ State Opera . 
 principal concert held December 12 
 Belvedere Palace style time Beethoven 
 living composing Vienna , 
 { Dr. Schalk , Opera 

 Puccini - Act pieces apparently 
 , performances given week . 
 S. WINNEY 




 H. W. GRAY CO . , NEW YORK . 


 YORK . 


 VICE 


 


 1921 


 TRANSCRIPTIONS PIANO HARPSICHORD 


 PIECES ) . 


 _ COURANTI 


 ‘ LE BAVOLET FLOTTAN1 


 ; , GIGU ! 


 6 SARABANDE 


 3 LA BERSAN 


 T. HILTON -TURVEY 


 SONG ALBUM 


 MAY- DAY LILT 


 STARS SEA 


 LOVE 


 SLEEPY - BYE SONG 


 CHEEK LIKE TINTED 


 ROSE 


 JUNE 


 LONG LANE TURNS 


 LAMIA 


 SYMPHONIC POEM 


 ORCHES?7 RA 


 DOROTHY HOWELL 


 


 MUSICAL 


 CANTATAS LENT . _ 


 NIGHT BETHANY 


 WORDS WRITTEN COMPILED 


 JOSEPH BENNETT 


 LEE WILLIAMS 


 


 GARDEN 


 TENOR 


 SOPRANO 


 BARITONE SOLI 


 CHORUS 


 WORDS WRITTEN SELE(C 


 VIOLET 


 ECTED 


 CRAIGIE 


 MUSIC 


 HALKETT 


 MPOSED 


 FERRIS TOZER . 


 CRUCIFIXION 


 MEDITATION 


 HOLY REDEEMER | 


 WO ! SELE WRITTEN 


 W. J 


 SP ARROW SIMPSON , M.A 


 SET 


 ST 


 SACRED PASSION 


 TED 


 


 AINER 


 DARKEST HOUR 


 TENOR ‘ BARITONE SOLI 


 CHORUS 


 SUNG ‘ CONGREGATION 


 SELECTED , 


 HAROLD M 


 SOPRAN 


 HYMNS 


 W 


 USIC COMPOSED , 


 MOORE 


 CROSS CHRIST 


 HOLY SCRIPTURES , INTERSPERSED 


 PRIATE HYMNS , 


 W. MAURICE ADAMS 


 APOSED 


 


 THOMAS ADAMS . 


 3S 


 LONDON 


 NOVELLO COMPANY 


 DESERT 


 OLIVET 


 


 GETHSEMANE 


 WORDS WRITTEN COMPILED 


 JOSEPH BENNETT 


 MUSIC COMPOSED 


 C. LEE WILLIAMS 


 STORY C ALVARY 


 TENOR BASS SOLI CHORUS 


 WORDS SELECTED WRITTEN 


 ROSE DAFFORNE BETJEMANN 


 MUSIC COMPOSED 


 


 EASY CANTATA 


 E. V 


 


 HALL , M.A 


 


 DOLOROSA 


 DEVOTION 


 BARITONE SOLO CHORUS 


 WORDS DERIVED MAINLY ANCIENT SOURCES 


 MUSIC COMPOSED 


 CUTHBERT NUN 


 CALVARY 


 


 TENOR BARITONE SOLI CHORUS 


 INTERSPERSED HYMNS SUNG 


 1E CONGREGATION 


 WORDS SELECTED WRITTEN DY 


 SHAPCOTT WENSLEY 


 MUSIC 


 MAUNDER 


 J. H. MAUNDER 


 FOOT CROSS 


 SOLI , CHORUS ORCHESTRA 


 COMPOSED 


 ANTON DVORAK 


 LIMITED 


 MUSICAL 


 


 VARY 


 ‘ HORUS 


 MANN 


 4S. 


 


 


 SOURCES 


 LRY 


 ORUS 


 UNG 


 CROSS 


 ROWE 


 1921 


 MUSIC LENT 


 PASSION LORD . 


 # PASSION CHRIST . G. F. HANDEL . 


 R VOICES ORGA 


 WORDS 


 REV . E. MONRO 


 SET MUSIC 


 REPROACHES 


 SET MUSIC 


 STORY CROSS OFFICE 


 BENEDICITE 


 SET MUSIC FOLLOWING COMPOSERS 


 NBY , 


 TENERR 


 


 DIRECTIONS SINGING 


 “ PASSION 


 USE CHURCH ENGLAND 


 EDITED PY 


 CYRIL MILLER 


 66 


 MUSIC LENT . EASTER 


 SEASONS 


 


 SERVICES . 


 STORY CROSS 


 ANTHEMS . 


 LEAD THY TRUTH 


 LORD SHEPHERD 


 PENITENCE 


 PEACE 


 J. H. MAUNDER 


 1 - 95 


 SEASON SUCCESS 


 CREATION PRAISE . ” 


 OLN 


 


 “ CHRIST CONQUEROR . ” 


 HUNTING SONG 


 COMPOSITIONS 


 JOHN GERRARD WILLIAMS 


 PIANOFORTE SOLO 


 SONGS . 


 - SONGS . 


 FAIR , SWEET , CRUEI ( S.A. T.B. ) 


 ( S.A. T.B. ) 


 SLEEPS . ( S.A.1T.B 


 VOL . IV 


 NOVELLO CLASSICAL 


 THIRTY SONGS 


 


 COMPOSERS 


 NOTATIONS 


 SONGS 


 


 “ SHEPHERD MEN 


 CANTATA JOHN S. WITTY . 


 


 SCENE CALVARY ” 


 FINE PRODUCTION 


 SUN SOUL 


 GENTLE SHEPHERD 


 DIVINE LOVE 


 LORD SHEPHERD 


 LEAD , KINDLY LIGHT 


 NAZARENE 


 PROMISE PEACE 


 JOSEPH ADAMS MUSIC PUBLISHING CO 


 STABAT MATER 


 GIOVANNI BATTISTA PERGOLESI 


 


 ENGLISH VERSION 


 


 4 ( WACHET , BETET ) 


 + -ANTATA 


 ; SOLI , CHORUS , ORCHESTRA 


 J. S. BACH 


 AVELING 


 SONGS 


 SONGS 


 67 


 CHRIST LAY DEATH 


 DARK PRISON 


 CHRIST LAG TODESBANDEN 


 EASTER CANTATA 


 SOLI , CHORUS , ORCHESTRA 


 COMPOSED 


 J. S. BACH 


 


 PASSION CHRIST 


 SET MUSIC 


 EDITED 


 EBENEZER PROUT 


 ABRIDGED CHURCH USE TH+ . 


 REI . JAMES BADEN POWELL 


 UNISON SONGS . 


 PIANOFORTE MUSIC . 


 CHORAL RH APSODIES . 


 CHURCH MUSIC . 


 BERNERS STREET , LONDON , W. 1 


 VOL . 


 


 EMILE JAQUES - DALCROZE 


 PASTORALS 


 “ COME , MARY . ” 


 “ SHEPHERD 


 PIPES FORSAKE ? ” 


 NORMAN GALE , 


 


 BRISTOW 


 ERNEST FARRAR 


 KEYS 


 ORGAN 


 PRINCIPLES 


 O3 


 PRACTICE 


 


 HERBERT F. ELLINGFORD 


 HIAWATHA WEDDING - FEAST 


 SELECTION 


 ORGAN 


 S. COLERIDGE - TAYLOR . 


 ARRANGED 


 HUGH 


 DAPTED 


 BLAIR 


 HUBERT 


 PARRY 


 ( SET 12 


 JOHANN 


 BACH 


 SEBASTIAN 


 BACH 


 CATHEDRAL ACRIBELLE GENUINE 


 SUPER - QUALITY SILK STRING 


 ET 


 T H E ” D ] S C 


 REG 


 ORGAN BLOWER 


 SOLE MAKERS “ DISCUS ’ BLOWER 


 VIOLIN VIOLONCELLO WORKS 


 WORKS OLD MASTERS 


 _ _ 


 PRICE EDITIONS 


 PUBLISHED 


 91 — NOVELLO MUSIC PRIMERS 


 SYSTEM 


 MUSICAL NOTATION 


 H. ELLIOT BUTTON 


 SIR EDWARD ELGAR 


 EXTRACT INTRODUCTION . 


 WORKS IGOR STRAVINSKY 


 PUBLISHED 


 J. & W. CHESTER , LTD 


 PRIBAOUTKI ( CHANSONS PLAISANTES 


 QUATRE - CHANTS RUSSES 


 BALLET SONGS ACU . ARRANGED