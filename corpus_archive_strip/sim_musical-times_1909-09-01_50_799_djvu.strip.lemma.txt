


 1909 


 XUM 


 the MUSICAL TIMES 


 found in 1844 . 


 publish on the first of every month 


 W , 7 


 4 c ° ) FASC 


 ROYAL CHORAL SOCIETY . QUEEN ’S HALL . 


 fifteenth season ) . 


 TUESDAY morning . — " ELIJAH . ' 


 the ROY AL " COLLEGE of MUSIC , ' ; 


 WEDNESDAY evening . — 


 USIC . THURSDAY evening.- 


 1909 


 ROYAL 


 MANCHESTER COLLEGE of MUSIC . 


 BIRMINGHAM and MIDLAND INSTITUTE . 


 SCHOOL of MUSIC 


 GRANVILLE BANTOCK . 


 SESSION 1909 - 1910 


 COLLEGE of MUSIC 


 AND 


 NORTHERN CENTRE of the 


 TOBIAS MATTHAY PIANOFORTE SCHOOL 


 HUDDERSFIELD 


 ROYAL CONSERVATORY for MUSIC , 


 STUTTGART . 


 instruction in every BRANCH of MUSIC . 


 the 


 SOUTH - WESTERN POLYTECHNIC 


 INSTITUTE , 


 CHELSEA , S.W 


 PHYSICAL TRAINING COLLEGE 


 CITY of LONDON COLLEGE , 


 WHITE STREET , MOORFIELDS , E.C 


 FUND . 


 for the encouragement of british 


 composer and executive artist 


 WATFORD SCHOOL of MUSIC 


 PRINCIPAI HOWARD - JONES 


 the 


 DELLE SEDIE SCHOOL of 


 SINGING , L 


 LONDON COLLEGE of MUSIC 


 GREAT MARLBOROUGH STREET , LONDON , W 


 TEACHING department 


 SECRETS of SONG and SPEECH " 


 ON ’S 


 JF 


 IMON 


 VICTORIA COLLEGE of MUSIC , 


 LONDON . 


 incorporate 1801 . 


 INCORPORATED guild of CHURCH 


 musician 


 ASSOCIATE ( A.1.G.C.M. ) , LICENTIATE ( L.1.G.C.M. ) , FEL- 


 competition for 1909 


 register of ORGAN vacancy . 


 LONDON COLLEGE of MUSIC 


 GREAT MARLBOROUGH STREET , LONDON , W 


 LOCAL and HIGHER examination 


 NATIONAL CONSERVATOIRE 


 OCTOBER 20 , 21 and 22 , 1909 


 chorus of 350 voice . 


 the LONDON SYMPHONY ORCHESTRA , 


 WEDNESDAY 


 THURSDAY 


 FRIDAY 


 SOUTHPORT TRIENNIAL MUSICAL 


 FESTIVAL 


 PHILLIPS , EDNA THORNTON , GRAINGER KERR , 


 PHYLLIS LETT . 


 MILLAR , ROBERT RADFORD , HERBERT BROWN , 


 HALLE ORCHESTRA 


 THURSDAY 


 " the vision of CLEOPATRA 


 ON two FOLK song . 


 1909 . 559 


 THI first annual | 


 ‘ ESTIV AL SERVICE | W. E. HILL & SONS 


 HEI 


 HOLY TRINITY CHURCH , | repairing of violin , VIOLAS 


 MARYLEBONE ROAD , N.W 


 AN 


 of the BUSINESS of 


 IN UNISON — 


 the 


 SYSTEM MUSIC COPYING agency 


 every organist 


 NG 


 the MUSICAL TIMES.—SEPTEMBER 1 , 1909 . 563 


 PROFESSIONAL notice 


 MISS FLORENCE VERNON 


 ( CONTRALTO ) 


 MISS ESTELLA LINDEN 


 ( YORKSHIRE SOPRANO ) . | 


 MISS ETHEL RADBU RN 


 ( SOPRANO ) | 


 MISS DASIE E. AVIS 


 ALL STANDARD work and oratorio . 


 FRANCIS G LYNN 


 MR . SAMUEL MASTERS 


 ( TENOR 


 please note change of address 


 MR . ERNEST PENFOLD 


 change of address 


 MR . MONTAGUE BORWELL| 


 ( BARITONE ) 


 WINIFRED MARWOOD 


 * ' BOARSLAND , " BRONDESBURY PARK , N.W 


 MR . WILLIAM BURT 


 ( BARITONE ) . 


 MR . HENRY 


 ( TENOR ) . 


 MR 


 ARCHDEACON 


 MR . CUTHBERT ALLAN 


 ROBIN OVERLEIGH 


 ( BASS - BARITONE 


 PLEVY 


 MR . EATON COOTER 


 BASS ) . 


 MR . JAMES COLEMAN 


 BASS ) 


 MR . ALEXANDER TUCKER 


 BASS ) . 


 MISS ELLEN CHILDS 


 ( CHROMATIC HARPIST ) . 


 MR . MUNRO DAVISON 


 composer ’ MSS . 


 the 


 A.R.A.M. , F.R.C.O. ) , PIANOFORTE , HARMONY , 


 CS 


 1908 ; 


 R.C.O 


 the MUSICAL TIMES.—SEPTEMBER 1 , 1909 


 563 


 LONDON . — graduate postal lesson in 


 HARMONY , COUNTERPOINT , THEORY of MUSIC , FORM 


 ISS GRACE IVORSON , A.R.A.M. , A.R.C.M. | 


 ALWAYS 


 HE ARUNDEL MALE - VOICE choir — 


 important to MUSIC teacher 


 £ 14 


 564 the MUSICAL 


 1909 


 HIGHER examination 


 licentiate in MUSIC 


 matriculation . 


 F.R.C.O. , J 


 licentiate 


 associate 


 HIGHER certificate . 


 SYMPHONY 


 for full orchestra 


 EDWARD ELGAR 


 FULL score 


 three GUINEAS net . 


 ARRANGEMENT FO ! PLANOFORTE 


 SIGFRID KARG - ELERT 


 the pianoforte 


 and the ability to play it 


 before buy a piano player 


 ILLUSTRATED catalogue on application , 


 BOSWORTH edition 


 reduction in price 


 ISCAR BERINGER ’S 


 for the PIANOFORTE 


 edition without SC . AL E S 


 scale only ; ' 


 BERINGER ’S 


 ANOFORTE TUTOR 


 BOSWORTH & CO . , 


 5 , PRINCES STREET , OXFORD STREET , LONDON , W 


 the ROYAL COLLEGE of ORG : ANISTS 


 HE ROYAL COLLEGE of ORGANISTS . — 


 F EL L OWSHIP . 


 the MUSICAL TIMES.—SEPTEMBER 1 , 1909 


 565 


 SEPTEMBER 1 , 1909 


 GRANTHAM , and its stately 


 parish CHURCH 


 JOHN EVELYN 


 GRANTHAM GRAMMAR SCHOOL , where SIR ISAAC NEWTON be educate 


 SIR 


 NEWTON 


 ISAAC 


 54 59 ) . 


 568 the MUSICAL time 


 OVER the 


 LIBRARY 


 the chain 


 SILC 


 SOUTH PORCH of the CHURCH 


 M. F 


 KING WEST 


 570 ° the 


 1909 


 the CRYPT , show the ancient stone ALTAR 


 and an old chest . 


 1640 


 xum 


 1909 . 571 


 A fine cluster of column AT 


 the WEST END of the CHURCH 


 GRANTHAM , LINCOLNSHIRE , AUGUST the IST . , 1745 


 573 


 exhibition 


 I , 1909 . 575 


 25 


 RICHARD WAGNER 


 the KING ’S MUSICK 


 576 


 1677 ) 


 the 


 MUSICAL TIMES . — SEPTEMBER 


 I , 1909 . 577 


 GREEK MUSIC and the 


 of HELLAS 


 FOLK - SONGS 


 the MUSICAL TIM 


 578 


 ES.—SEPTEMBER i , 1909 


 MR . RAYMOND DUNCAN . 


 XUM 


 the MUSICAL TIME 


 S.—SEPTEMBER i , 1909 . 579 


 MRS . PENELOPE DUNCAN 


 580 


 F.S.A 


 12.30 p. 


 1 1900 


 23:8 


 the 


 MUSICAL TIMES.—SEPTEMBER 1 , 1909 . 581 


 582 the MUSICAL TIM 


 ES.—SEPTEMBER i , 1909 


 HEREFORD FESTIVAL novelty 


 DR . WALFORD DAVIES ’s ‘ noble numberschoral . Elijah ( Mendelssohn ) ; 
 ( Handel ) ; Midnight ( Ausland Boughton ) ; dream of| 
 Gerontius ( Z / gar ) ; the Spirit also helpeth we ( Sach ) ; | 
 Stabat Mater ( Dvordé ) ; Omar Khayyam , part ii . and iii . | 
 ( Bantock ) ; Mass in C , no . 4 ( Cherudini ) ; Song of Destiny | 
 ( Brahms ) ; and Faust ( Berlioz 

 ORCHESTRAL . symphony : Jupiter ( A / ozart ) ; Eroica 
 ( Beethoven ) ; Unfinished ( Schudert ) ; in a flat ( # /ear ) . 
 overture : Der Freischiitz ( Weber ) ; Leonora , no . 3 
 ( Beethoven ) ; Die Meistersinger ( Wagner ) . suite ( Bach ) . 
 symphonic - poem : Francesca di Rimini ( 7chathovsky ) and | 
 Till Eulenspiegel ( S¢rauss ) . symphonic variation ( /arry 

 ti hard to find God , but to comprehend 
 he , as he be , be labour without end 




 the 


 I , 1909 . 583 


 old english SUITE , for SMALL ORCHESTRA , 


 BY PROFESSOR GRANVILLE BANTOCK 


 534 


 1909 


 distinguished canadian ORGANIST : 


 DR . ALBERT HAM 


 1883 . 


 1909 . 585 


 ST . JAMES ’S CATHEDRAL , TORONTO 


 586 


 PRESIDENT of 


 OF ORGANISTS 


 THI 


 ROYAL 


 NEW 


 the college 


 excellent advice to organist 


 MR . ALFRED HOLLINS 


 AN OLD CITY ORGAN MODERNISED 


 the retirement of MR J. W. ELLIOTT 


 587 


 the NEW ORGAN for SANDRINGHAM CHURCH . | 


 programme 


 specification of the ORGAN . 


 the LAY - CLERKS of GLOUCESTER CATHEDRAL 


 1909 


 A chorister ’ FESTIVAL service 


 the DOYEN of LADY ORGANISTS 


 1909 


 tow . 


 1909 


 ALEXANDER ’s feast 


 F. G. EDr . Coerne divide his subject into three main division : 
 ( 1 ) preliminary ( composer prior to Bach and Handel ) ; 
 ( 2 ) the Classic era ; and ( 3 ) romanticism . each of these 
 section be provide with a summary , and there be an index of 
 name . eighty - five page , nearly a third of the whole , 
 consist of an appendix of musical illustration , in score , from 
 the work of Monteverde , Alessandro Scarlatti , Haydn 

 Mozart , Beethoven , Weber , Berlioz , Mendelssohn , Wagner , 
 Saint - Saéns , Tchaikovsky , Dvordk and Richard Strauss . 
 the absence from these illustration of an american or english 
 composer as a contributor to the evolution of the modern 
 orchestra be rather remarkable , the more especially as the 
 author say : ' the american composer be to - day ready to 
 enter the list against the entire world 

 it be interesting to learn what Dr. Coerne have to say about 
 english orchestrator — this be a word he use — in the 
 four page devote to our countryman . his ' sturdy list , ' 
 as he call it , consist of Balfe , Macfarren , Bennett , Barnby , 
 Sullivan , Bridge , Mackenzie , Parry , Cowen , and Stanford . 
 Elgar be refer to later , partly in two foot - note , and 
 Coleridge - Taylor share the same fate as Debussy in be 
 consign to foot - note typ2 . why Balfe be mention be not 
 very obvious , as we be tell that ' neither the substance- 
 matter nor the instrumentation of his work merit the 
 distinction of having further the cause of indigenous 
 english music . ' and then be the work of Macfarren 
 ' among the most important contribution to the literature of 
 english music ' ? the coupling of Barnby ’s name with two 
 concert performance of ' Parsifal ' have nothing whatever to 
 do with his share , whatever that may have be , in the 
 evolution of orchestration . Sullivan be dismiss in a 
 paragraph of twenty - two line 




 590 the 


 1909 


 book receive 


 ALTO 


 TENO 


 anthem for EVENSONG 


 09 


 save US , o LORD , while wake 


 » _ ' ~~ 7 


 : ! SM 4 4 


 2 | p 


 S a 4 ¢ a 7 


 - xn 


 7 , 4 


 } . — _ — pp 


 ° — _ 4 + 


 6 | SSS 


 2 | — z i { 


 save US , o LORD , WHILE wake 


 = 7 ] " 7 + ; - 


 pp 


 : f 3 = ; = 


 " | pp =] - pp 


 6 : = ss 


 4 p 


 SAVE U 


 O LORD , WHILE wake 


 — eee 


 596 the 


 1909 


 MR . MACLEAN ’S opera ' maitre SEILER 


 promenade concert 


 NATIONAL CO - operative FESTIVAL 


 the MUSICAL TIMES — SEPTEMBER 1 


 597 


 1909 


 CAPE TOWN MUSICAL FESTIVAL 


 from a correspondent 


 BERLIN 


 BERNE 


 BOSTON 


 JOURIEFF ( RUSSIA 


 MUNICH 


 NAPLES 


 OSTEND 


 ROME 


 SCHEVENINGEN 


 SON DERSHAUSEN , 


 WILDUNGEN 


 598 


 I , 1909cathedral use ' be nothing more nor less than the method of | 
 perform divine service in cathedral this include 
 intoning , antiphonal singing of the psalm , the due observance 
 of the verse part in service and anthem , and — may we 
 add ? — a _ devotional organ accompaniment free from 
 secular suggestiveness 

 A STupENT.—(1 ) with all due respect to the distinguished 
 german professor you mention , the Cotta edition of 
 Beethoven ’s Pianoforte sonata in D ( op . 10 , no . 3 ) be quite 
 correct in regard to indicate the manner of perform the 
 grace note in that work . moreover , soeminent an authority 
 as Mr. Dannreuther confirm the direction therein give by 
 say ' appoggiature , short ' ( see Dannreuther ’s primer of 
 * musical ornamentation , ' Part ii . , p. 113 ) . ( 2 ) the c flatin 
 bar 37 ( reckoning without repeat ) in no . 15 of Schumann ’s 
 ' Die Davidsbiindler ' be an enharmonic change of the 
 precede b natural , therefore the trill note ( b natural and 
 C ) remain the same in spite of the notational change 

 Perys — ( 1 ) the song ' beauty retire , ' by Mr. Pepys , be 
 print in Sir Frederick Bridge ’s ' Samuel Pepys : a lover 
 of musicke , ' publish by Messrs. Smith , Elder , & Co. 
 ( 2 ) the legend of St. Francis of Paula ( a.p . 1508 ) 
 walk upon the water , that prompt Liszt to write his 
 pianoforte piece thereupon , be that the saint be float 




 110 


 the MUSICAL TIMES.—SEPTEMBER 1 , 1909 . 599 


 dure the last month . 


 CHORISTER serie 


 NEW edition 


 pf ett SEE 


 EW 


 the 


 MUSICAL TIMES.—SEPTEMBER 1 


 reduce price 


 CALLCOTT , J. W. 


 NOVELI.O 


 LONDON 


 AND COMPANY 


 LIMITE 


 the 


 PIANO PEDALS . for the 


 BES and ( heapest . | compose by 4 : 


 the OLD FIRM . 12 » 


 P. CONACHER & CO ™ # 


 > ’ 15 . 


 HUDDERSFIELD 


 two GOLD MEDALS ORGAN MUSIC : 9 


 NICHOLSON and CO . edit by EB 


 ORGAN BUILDERS , JOHN E. WEST . 23 


 PALACE YARD , WORCESTER . 24 . FI 


 > J 8 8 


 n 


 progressive study 


 for the pianoforte 


 edit , arrange in group , and the fingering revise and supplement 


 FRANKLIN TAYLOR 


 fifty - six book , price one shill each . 


 from the above : 


 select PIANOFORTE study 


 FRANKLIN TAYLOR . 


 in two set ( eight book ) , price one shilling and sixpence each book 


 completion of 


 BY 


 SECTION a.—technical practice . in six book 


 SECTION b.—studie . in six book 


 cloth binding . 


 piece . 


 22 og 


 the 


 605 


 for 


 complete 


 anthem 


 list 


 HARVEST anthem 


 IT 


 give 


 while 


 HERBERT 


 UNTO 


 the vineyard of 


 COME 


 LONDON 


 be a good 


 thing 


 THANKS 


 compose by 


 to GIVE 


 THOMAS ADAMS 


 the 


 EAR 


 YE 


 compose by 


 EARTH REMAINETH 


 compose by 


 recently publish 


 THEE 


 JOHN 


 HERBERT 


 WILL 


 ARTHUR W 


 YE 


 BRU 


 O BE 


 JOHN E 


 JOYFUL 


 SEEDTIME and HARVEST 


 0 GOD 


 THANKS 


 compose by 


 compose by 


 MAGNIFY 


 compose by 


 CE 


 IN 


 se by 


 ALCOCK . 


 THE 


 WEST . 


 HEAVENS 


 WAREING 


 DO WE 


 WEST 


 WAREING 


 THEE 


 marchant 


 thankful PEOPLE , COME 


 STEANE 


 GOD 


 LORD 


 GIVE 


 606 


 1909 


 NOVELLO 


 W. H. 


 the 


 ute " SE rie of HARVEST anthem 


 1909 . 607 


 A GOLDEN HARVEST 


 A CANTATA 


 7 : HENRY KNIGHT 


 THOMAS ADAMS 


 A HARVEST SONG 


 HARVEST CANTATA 


 4 4 


 the JUBILEE CANTATA 


 A SONG of thanksgiving 


 the GLEANER ’S HARVEST 


 NOVELLO 


 LONDON 


 SONG of thanksgiving 


 A CANTATA 


 FOR harvest and general festival use 


 FOR 


 SOPRANO , TENOR , and BASS ( or CONTRALTO ) SOLI 


 AND CHORUS 


 with hymn to be sing by the congregation 


 the word write and arrange by 


 SHAPCOTT WENSLEY 


 J. H. MAUNDER 


 TWELVE hymn for HARVEST 


 STREWS on EV’RY 


 PLAIN 


 let all our brethren join in 


 one 


 sowing and reaping 


 MARCH 


 now AUTUMN 


 HARVEST thanksgiving 


 the joy of HARVEST 


 a HARVEST HYMN of PRAISE 


 the sower go forth sowing 


 MAKE MELODY within your 


 heart 


 he that SOWETH 


 O LORD of HEAVEN , and EARTH , 


 | and SEA 


 the 


 MUSICAL 


 EDWARD BUNNE TT 


 service 


 popular C 


 HARVEST anthem . 


 service , 


 ORGAN , 


 CHURCH CANTATA 


 CO ) mposition 


 D. CANTAB 


 HARVEST FESTIVAL MUSIC , 


 PIANOFORTE 


 ORGAN . 


 1231 


 1/6 


 1/6 


 the 


 CHU RCH MUSIC 


 H. MAUNDER 


 HARVEST . FESTIVAL 


 | book 


 contain TALLIS 's prece and response , the 


 canticle and special psalm 


 point for chanting and SET to new and appropriate 


 chant by 


 SIR J. BARNBY , MYLES B. FOSTER , 


 SIR A. C. MACKEN YZIE , SIR J. STAINER 


 and other 


 TOGETHER with 


 four new HYMN tune 


 | compose expressly by 


 SIR J. BARNBY , SIR J. STAINER , 


 AND 


 JOHN E. WEST . 


 ORGAN MUSIC 


 HARVEST festival 


 the village ORGA 


 NNIN \GHAM WOODS 


 NIST 


 F. CU } 


 most successful harvest CANTATAS 


 LORD 


 YD 


 two harvest 


 BY 


 SONG 


 TURNER 


 festal 


 EDMUND 


 L.A , 


 : the ORGAN RECITALIS 


 new and popular basil HARWOOD 


 LHE TEN virgin 


 A sacre cantata for four solo voice , CHORUS and orchestra 


 BY 


 ALFRED R. GAUL 


 4 4 


 HI 


 1909 . 611 


 novelty 


 NOBLE number 


 BY 


 ROBERT HERRICK 


 ETHER with contemi rary number by 


 GEORGE HERBERT , DONNE 


 and an anonymous writer 


 select and SET to music 


 for SOLO voice , CHORUS , VIOLONCELLO and 


 ORCHESTRA 


 H. WALFORD DAVIES 


 GO , song of MINE 


 the word by 


 GUIDO CAVALCANTI ( 12560 


 1301 ) 


 [ translate 


 the music by 


 EDWARD ELGAR . 


 LAZARUS 


 AN EASTER CANTATA 


 FOR SOLI , CHORUS and ORCHESTRA 


 W. G. ROTHERY 


 FRANZ " SCHUBERT . 


 WELCOME , SWEET PLEASURE 


 AND 


 A SHEPHERDS DANCE 


 FROM " in SPRINGTIME 


 A. HERBERT BREWER 


 OLD english suite 


 arrange for SMALL ORCHESTRA 


 GRANVILLE BANTOCK 


 WILSON MANHIRE 


 musical form 


 model answer to 


 question on " touch 


 hint to candidate 


 J. H. LARWAY , 


 14 , WELLS STREET , OXFORD STREET 


 popular song 


 with SOL - FA include 


 LONDON , W 


 T. RICHARDSON 


 the BONNIE AIRLIE 


 ( A. STELLA 


 BRAES OQ 


 the STRATHEARN COLLECTION 


 of part - song . 


 PATERSON and SONS 


 EDINBURGH : LONDON : 


 NOVELLO ' ’S 


 OCTAVO edition of part - song 


 select list of 


 part - song publish within the last twelve month 


 PART - song for mix voice 


 part - song for male voice 


 PART - song for FEMALE voice 


 OR 


 100 


 BEY 


 the MUSICAL TIMES.—SEPTEMBER 1 , 1909 


 size 


 a select list of 


 oratorio and church cantata 


 a classified list of 


 ( sacre . ) 


 A classified list of 


 100 popular cantata ( secular ) . 


 4 any the a e CATALOGUES CA ? 


 BON - BON SUITE 


 ANDROMEDA 


 CYRIL B. ROOTHAM . 


 FAUST 


 WALKER 


 ERNEST 


 beyond these voice there 


 C. HUBERT H. PARRY 


 IS PEACE 


 LONDON 


 SHORT select list of 


 popular anthem for all season 


 A short select list of 


 popular service . 


 4 short select list of 


 popular ORGAN music . 


 \ select list oi ! 


 popular part - song for mix voice 


 three half - pence for each number . 


 select list 01 


 part - song and madrigal 


 for mix voice 


 modern 


 ode on time 


 H. WALI DAVIES 


 ORD 


 the MASQUE of COMUS 


 J. F. BRIDGE , 


 annunciation 


 ALEX . M. MACLEAN 


 THE 


 IN ARMOUR 


 the SKELETON 


 the FAKENHAM GHOST 


 LUARD - SELBY 


 614 


 I , 1909 


 just publish 


 54 


 AN 


 ILLUSTRATED CATALOGUE 


 OF 


 MUSIC LOAN 


 IIELD UNDER the 


 HIS MAJESTY 


 HER MAJESTY THE 


 AND 


 the 


 EXHIBITION 


 PATRONAGE OF 


 the KING 


 QUEEN 


 BY 


 WORSHIPFUL company of 


 MUSICIANS 


 AT 


 FISHMONGERS ' HALL 


 LONDON 


 NOVELLO and COMPANY , LIMITED 


 TECHNIQUE 


 AND 


 expression 


 PIANOFORTE PLAYING 


 BY 


 FRANKLIN TAYLOR 


 extract from PREFACE 


 with numerous musical example from the 


 work of the GREAT master 


 SIX octave study 


 FOR the PIANOFORTE 


 WILLEM COENEN 


 ARRANGEM 


 AR 


 STRING 


 ARRA 


 VIOLA 


 VIOLIN 


 VIOLOD 


 PIANO 


 ORGAN 


 STRIN 


 WIND 


 the MUSICAL TIMES.—SEPTEMBER i. 1909 . 615 


 ON the cliff of AUTUMN 


 prelude to ACT II . of JAMES LYON . 


 " the wrecker 


 EDWARD ELGAR CANTILENE ROMANESQUE 


 2 HARP or PIANOFORTE . I D 


 ’ he NRY I. E DW ard . 


 GAVOTTE IN G 


 arrangement for SMALL ORCHESTRA 


 compose by 


 S | NEW ORGAN| MUSIC — 


 ALLEGRETTO | PRELUDE 


 IN C SHARP MINOR 


 the MUSICAL TIMES 


 SEPTEMBER I , 1909 


 late 


 A ROSEBUD BY my 


 the art of SINGING 


 HERBERT SIMS REEVES . 


 MONOTONING 


 CHORISTER ’ S aid to 


 BY 


 C.S. FOSBERY , M.A. 


 SIDNEY R. COLE ’S 


 & most successful 


 early 


 FOR MEZZO - SOPRANO 


 two song 


 what do LITTLE BIRDIE s 


 the night 


 for MEZZO - SOPRANO 


 the KING of HEARTS 


 FOR BARITONE 


 song from the 


 HIGHWAY 


 compose by 


 ERNEST AUSTIN 


 SONGS | 


 WALK 


 1900 


 four duet 


 for CONTRALTO and BARITON ! 


 the english version by 


 W. G. ROTHERY 


 the music by 


 JOHANNES BRAHMS 


 HO - HO , of the GOLDEX 


 | BELT 


 ONE of the " NINE STORIES of CHINA 


 a short , humorous CANTATA 


 for CHILDREN ’s voice 


 word by 


 JOHN GODFREY SAXE 


 MUSI by 


 HERBERT W. WAREING 


 song 


 | with instrumental accompaniment 


 | E. M. SMYTH 


 ENGLISH and FRENCH WORDS 


 ODELETTE 


 the dance . 


 4 . AN : ACREON TIC ODE 


 the 


 CH 


 the 


 NEW CATHEDRAL PSALTER 


 the psalm of DAVID 


 TOGETHER WITH 


 the canticle and proper psalm for certain day 


 edit and point for chanting 


 408 new CATHEDRAL psalter chant . 


 PIANOFORTE . 


 INTERMEDIATE 


 EXAMINATIONS 


 LOWER division 


 examination 


 LOCAL CENTRE.—INTERMEDIATE grade 


 CONTRALTO . 


 TENOR 


 BARITONE , 


 BASS . 


 LOCAL CENTRE.—ADVANCED GRADE 


 SOPRANO . 


 MEZZO - SOPRANO . 


 CONTRALTO 


 BARITONE , 


 BASS 


 SCHOOL EXAMINATIONS.—PRIMARY . 


 SCHOOL EXAMINATIONS.—ELEMENTARY . 


 SOPPANO . 


 CONTRALTO . 


 ( B. & H. , 907 ) 


 BARITONE , 


 SS . 


 SCHOOL EXAMINATIONS.—HIGHER division . 


 MEZZO - SOPRANO . 


 CONTRALTO 


 TENOR . 


 BARITONE 


 BASS . 


 class singing EXAMINATION 


 LOC . 


 STAINE 


 PIE 


 PIANOFORTE MUSIC 


 the 


 school EXAMINATIONS.—HIGHER division . | 


 CHAMBER MUSIC 


 . | J , 7 , TC ~ 


 VIOLONCELLO . _ 


 LOCAL CENTRE.—INTERMEDIATE grade . | VOCAL MUSIC , 


 NOVELLO 's | 


 new and popular song 


 publish in key to suit the voice specify below 


 price TIO shilling each , net 


 roll down to RIO 


 remembrance and regret 


 pleading 


 ELEANORE 


 song of FRIENDSHIP . to welcome you . 


 MY heart A - dream . home thought from abroad . 


 IT be a lover and his lass the twelve day of CHRISTMAS . 


 FI 


 MICI 


 FOR ' 


 CHILL 


 the