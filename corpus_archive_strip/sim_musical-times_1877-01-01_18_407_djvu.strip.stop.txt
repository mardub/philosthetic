


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 PUBLISHED MONTH 


 FORSYTH BROTHERS ’ NEW PUBLICATIONS 


 NEW COMPOSITIONS PIANOFORTE 


 LATEST COMPOSITIONS PIANOFORTE 


 NEW COMPOSITIONS PIANOFORTE 


 NEW PIANOFORTE MUSIC 


 GEORGE NEVILLE . ¥ | J. RANDOLPH 


 DER FREISCHUTZ . 


 N JUAN . 


 UALE 


 FREDERICK UNGER 


 ASIE 


 COTSFORD DICK . ROMANCE 


 W. WALKER 


 J. WRIGLEY . IDYLL 


 OYOUS SPRING ... 


 NEW ORGAN ARRANGEMENTS . 


 NEW POPULAR SONGS 


 ASOR DOOLF 


 AUSER 


 UN BALLO MASCHERA . 


 WATER CARRIER . 


 WILLIAM TELL . 


 ZAMPA 


 CROSS STREET , SOUTH KING STREET , MANCHESTER 


 PROFESSIONAL NOTICES 


 CHORAL SOCIETIES . 


 RINITY COLLEGE , LONDON.—EVENING 


 SOUTH LONDON MUSICAL TRAINING COLLEGE . — 


 USICAL EXAMINATIONS.—CANDIDATES 


 ING 


 GE . — 


 TES 


 SONS 


 IANOFORTE , ORGAN , HARMONIUM , SING 


 ORKSHIRE ( ST . CECILIA ) CONCERT 


 ESTABLISHED APRIL 1866 . 


 ENGLISH GLEE UNION . 


 ASSISTED 


 HURCH CHOIR.—WANTED , LEADING 


 “ LIVERPOOL QUARTETTE , ” 


 PARTIES FOLLOWING EMINENT 


 CENTENNIAL . EXHIBITION 


 NOTICE 


 NEW YORK 


 STANDARD ” 


 AMERICAN ORGANS 


 PRICES , 12 125 GUINEAS 


 BARNETT SAMUEL & SON 


 31 , HOUNDSDITCH , E.C. , & 32 , WORSHIP STREET , 


 FINSBURY SQUARE , 


 AST LONDON ORGAN WORKS 


 TUTTGART HARMONIUM COMPANY . — 


 & W. SNELL IMPROVED HARMONIUMS , 


 VERTICAL IRON FRAME PIANOFORTES 


 GTBONG SINGLE CASTING 


 NEW - YEAR PRESENTS 


 LONDON AGENTS , 


 PIANOFORTE SALOON , 


 55 , BAKER STREET , LONDON , W 


 TESTIMONIAL . 


 RUSSELL MUSICAL INSTRUMENTS . 


 EAN CHEAP MUSICAL INSTRUMENTS . 


 FLAVELL 


 QUARTERLY SALE MUSICAL PROPERTY . 


 USIC ENGRAVED , PRINTED , PUB- 


 ANGLICAN HYMN - BOOK 


 NEW EDITION , REVISED ENLARGED 


 TONIC SOL - FA EDITION 


 TONIC SOL - FA EDITION 


 REDUCED 


 PRICES 


 OUSELEY MONK 


 POINTED 


 PSALTER 


 CAROLS USE CHURCH CHRISTMAS 


 EPIPHANY 


 CONGREGATIONAL PSALMIST 


 PUBLISHED . 


 COLLECTION EIGHTEEN 


 THIRTY - 


 RESPONSES COMMANDMENTS 


 MANUAL SINGING 


 USE CHOIR TRAINERS & SCHOOLMASTERS , 


 - - EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT - SINGING MANUAL 


 APPENDIX 


 ( COMPANION WORK 


 COLLEGIATE VOCAL TUTOR 


 CATHEDRAL CHANT BOOK 


 NOTICE 


 MUSICAL WORLD 


 NEW PIANOFORTE SOLO , H. C. LUNN 


 NEW VOCAL MUSIC , C. A. MACIRONE . 


 MUSIC . 


 NEW EDITION 


 DR . BENNETT GILBERT POPULAR WORK 


 SCHOOL HARMONY 


 SUBJECT SPECIAL EXERCISES . 


 DEDICATED , PERMISSION , 


 AKER PIANOFORTE TUTOR . price 2s . 5s.each . series contains cele 

 brated beautiful works Handel , Bach , Haydn , Mozart , Beethoven 

 Weber , Steibelt , Dussek , Hummel , Schumann , Mendelssohn , Chopin 




 O CHORAL SOCIETIES , PENNY READINGS , 


 Y LADDIE FAR AWAY . NEW SONG : 


 T. ANNE , SOHO.—SPECIAL SERVICES 


 MUSICAL TIMES 


 SCALE TERMS ADVERTISEMENTS 


 HELMHOLTZ TONE SCIENTIFIC 


 LECTURES 


 MUSIC PRIMER SCHOOLS 


 VOL . II . OCTAVO EDITION 


 W. STERNDALE BENNETT PIANOFORTE WORKS 


 PUBLISHED 


 EIGHTEEN - SONGS 


 COMPOSED 


 ANTON RUBINSTEIN . 


 ENGLISH VERSION NATALIA MACFARREN . 


 CONTENTS : 


 FOURTEEN SONGS 


 POEMS ROBERT BURNS 


 CONTENTS . 


 38 . 


 3S 


 G. F. 


 CONTENTS . 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 JANUARY 1 , 1877 


 READERS 


 H. C. L 


 PURCELL 


 HAYDN MEMORIAL . recollect invited hear pianist , 
 pet amateur musical circle 

 perform Beethoven Sonatas ‘ 

 alterations additions listeners scarcely 
 recognise works . ” course engaged , 
 convey readers 
 slightest notion effect produced ambj- 
 tious executant . regimental band . 
 masters endeavouring pursue system 
 majority compositions selected 
 , ceftain 
 versions National Anthem use , 
 doubt agreeable heard separately , pro- 
 duce means pleasant sensation played 




 | 


 LATE HENRY PHILLIPS 


 EDWARD LAND 


 G. F. ANDERSONCRYSTAL PALACE 

 SuLtivan Cantata ‘ ‘ Shore Sea ” occupied 
 greater portion Saturday concert 2nd ult . 
 work composed opening Inter- 
 national Exhibition 1871 , produced 
 occasion Albert Hall . consists numbers , 
 laid soprano baritone solos , chorus 
 orchestra . containing clever writing , 
 , , composer best works , fact 
 probably accounted having 
 written special occasion . known piéces 
 de circonstance seldom favourable specimens 
 authors . movements Mr. Sullivan 
 Cantata best numbers . performance 
 Palace exceedingly good : solos 
 given Madame Lemmens - Sherrington Mr. Wadmore , 
 chorus showed excellence 
 marked singing present season . Meyer- 
 beer interesting overture “ Struensee ” opened , 
 Beethoven great “ Leonore ” overture concluded , con- 
 cert , alsoincluded Bach Chaconne Ernst ‘ ‘ Hun- 
 garian Airs , ’ played Herr Wilhelmj , réfertoire 
 exceedingly limited . Mr. Wadmore sang 
 Romance Mercadante , Madame Sherrington con- 
 tributed Cavatina Rossini 

 instrumental concert oth 
 exceedingly good . orchestral works given 
 Schumann Overture ‘ ‘ Genoveva , ” Bennett Symphony 
 G minor , Liszt ‘ Symphonic Poem , ” ‘ “ ‘ Mazeppa . ” 
 works familiar need 
 comment ; Liszt ‘ ‘ Mazeppa ” given occasion 
 time England — , original shape , 
 having recently heard duet pianos 
 Mr. Walter Bache recital . work illustration 
 Victor Hugo poem , subject , needless 
 , treated Lord Byron . Like 
 Liszt compositions , music requires careful study 
 repeated hearing appreciation ; 
 said present parts sound obscure 
 vague , evidence unmistakable power . 
 performance wonderful pieces 
 playing heard Crystal Palace . music 
 extremely complex enormously difficult , 
 rendered finish precision 
 surpassed . afternoon Miss Anna Mehlig 
 gave excellent performance Hiller Pianoforte Con- 
 certo F sharp minor . vocalists Miss Ida 
 Corani Mr. F. H. Celli . exception 
 Wolfram song ‘ * Tannhauser , ” said 
 vocal music worthy concert 

 18 MUSICAL TIMES.—January , 1877 

 Beethoven birth , programme entirely selected 
 works , fault found 
 far toolong . commenced Overture 
 * * Prometheus , ” illustrating composer earliest | 
 manner . succeeded Cavatina chorus | 
 ‘ ‘ shall sorrow grieve , ” “ Praise | 
 Music , ” solo sung Madame 
 Blanche Cole . Madame Arabella Goddard played | 
 brilliant finished style Concerto E flat ; | 
 songs Mr. Edward Lloyd ( ‘ ‘ Adelaida ” ) | 
 Madame Antoinette Sterling ( ‘ ‘ Wonne der Wehmuth ” 
 ‘ ‘ Neue Liebe , neues Leben ” ’ ) came Choral Sym- 
 phony ! precede work lengthy selection 
 way mistake . hour music , hearers 
 fresh enjoy thoroughly appreciate 
 fully elaborate masterpiece . performance 
 high excellence . solo quartet consisted 
 artists named , addition Mr. 
 H. A. Pope ; chorus distinguished 
 trying music allotted precision spirit , 
 rendering instrumental movements 
 treat highest order 

 concerts resumed 3rd February — 
 anniversary Mendelssohn birthday — pro- 
 gramme selected entirely works 
 composer 




 GADSBY “ ALCESTIS 


 ROYAL ALBERT HALL CHORAL SOCIETY 


 ST . ANNE , SOHO 


 ROYAL ACADEMY MUSIC 


 BOROUGH HACKNEY CHORAL ASSOCIATIONIr strange journey far 

 east Shoreditch Town Hall hear Schubert Mass 
 F time London ; members 
 Association engaged Mr. Ebenezer Prout Con- 
 ductor , scarcely expect content 
 remain passive agent hands ; unex- 
 ampled success concert present season , 
 27th November , fully proved counsels 
 right direction . excellent rendering 
 work scarcely familiar 
 choral body reflected highest credit , 
 Conductor , laboured hard 
 task preparation , singers , , 
 presume amateurs , gave ample evidence 
 power grapple difficul- 
 ties interesting task . religious fervour 
 beautiful ‘ ‘ Kyrie ’ delivered produced 
 marked effect auditors ; ‘ “ Gloria ” 
 movements , including Trio ‘ ‘ Gratias agimus , ” 
 went faultlessly , fugue ‘ Cum 
 | sancto spiritu , ” severely taxed capabilities 
 |choir , especially worthy commendation . 
 “ Credo , ” lovely setting text , appropriately 
 | subdued accompaniment , sung impres- 
 |sively , “ Sanctus ” ( mind 
 | weakest portion Mass ) given utmost 
 care , executants resolved 
 reverence work bestowing equal attention 
 . “ Benedictus , ” canon 
 | sopranos tenors , remarkable variety 
 | accompaniment , went , warmly 
 | applauded , particularly section audience 
 | unaccustomed listen elaborate combinations 
 voices instruments . ‘ “ Agnus Dei , ” charm- 
 ingly peaceful movement , leading ‘ “ * Dona nobis ” — 
 , like Beethoven Mass C , subject 
 ‘ “ ‘ Kyrie ” returned — concluded work , 
 listened extreme delight audience filling 
 Hall . praise solo 
 singers — Miss Marie Duval , Miss Geddes , Miss Pauline 
 Featherby , Messrs. H. Guy , Goodwood , Thurley Beale — 
 Miss Duval especially distinguishing 
 soprano solos composition abounds . 
 orchestra extremely good ; conducting Mr. 
 Prout , whilst devoid superabundant energy , 
 calm dignified inspire person concerned 
 interpretation Mass fullest confidence . 
 space record miscellaneous second 
 composed classical materials induce 
 belief new Conductor resolved indoctrinate 
 placed confidence 
 true principles art 

 Tue Monday Saturday Popular Concerts 
 continued attract past month numerous 
 appreciative audiences . 
 little absolute novelty programmes ; works 
 established favour frequenters 
 concerts rendered perfection . 
 pianists mention Mdlle . Mehlig , Miss Agnes 
 Zimmermann , Mr. Charles Hallé ; Madame 

 G. TOWNSHEND SMITH , 
 “ Conductor Hon . Sec . Hereford Festivals 

 TueE Schubert Society , conductorship Herr 
 Schuberth , gave Eleventh Soirée Musicale 13th 
 ult . , Beethoven Rooms . prominent 
 long list vocalists appeared mention 
 names Madame L. Gage , Madame Rosetti , Madame 
 Schuberth , Miss Alison Leigh , Mr. Bishenden . Solos 
 pianoforte given Miss McCarthy Miss 
 Albrecht . programme long varied . 
 Madame Gage received encore rendering 
 “ La Stella , ” Mililotti , Miss Alison Leigh displayed 
 promising contralto voice . accompaniments 
 played Mr. Samson Herr Schuberth 

 Christmas General Meeting Royal Society 
 Musicians , Mr. W. H. Cummings elected Honorary 
 Treasurer , place late Mr. G. F. Anderson 

 Mr. W. S. HoyTe gave concert Assembly 
 Rooms , St. John Wood , 5th ult . , 
 numerously attended . pianoforte solos — 
 comprised Sterndale Bennett Sonata ‘ ‘ Maid 
 Orleans , ’’ Fantasia Liszt , group minor com- 
 positions Chopin , Henselt , Silas , & c.—Mr . Hoyte 
 warmly applauded , times recalled plat- 
 form . programme included movements 
 Beethoven Trio ( Op . 97 ) , Mendelssohn Trio 
 C minor , concert - giver ably assisted 
 Herr Wiener ( violin ) Herr Daubert ( violoncello ) . 
 vocalists Madame Alice Barth , Miss Marion Severn , 
 Mr. Stedman , highly successful ; 
 Mr. Stedman singing new song Berthold Tours , 
 | crown thee Queen , ” especially admired , 
 Henry Leslie graceful Trio ‘ ‘ Memory ” eliciting well- 
 deserved marks approbation 

 Advent Sunday , special musical services held 
 Christ Church , Mayfair . morning Dr. Stainer 
 Anthem ‘ Hosanna highest ” _ effectively 
 rendered . evening service usual Choir 
 Church augmented Messrs. Barrett , De Lacey , 
 Thornton , Moss , St. Paul Cathedral , kindly 
 volunteered services . following music sung : 
 Canticles Parry Service D , Anthems , 
 viz . , * * ? ’’ ? Dr. Arnold , ‘ ‘ O Saviour 
 world , ” Sir John Goss . Tallis Responses . 
 sermon morning preached Vicar , 
 evening Rev. Cosmo R. Gordon , D.D. , 
 Incumbent Grosvenor Chapel . musical portion ot 
 services direction Mr. R. Stokoe , 
 presided organ 

 annual concert benefit Orphanage 
 H.M. Customs givenin St. James Hall 7th ult . , 
 direction Mr. W. Phillips . success , 
 pecuniarily artistically , desired . 
 Mdme . Edith Wynne , Miss Mary Davies , Mdlle . Enriquez , 
 Miss Bolingbroke , Mr. W. H. Cummings , Mr. H. Guy , 
 Signor Caravoglia , Mr. Maybrick vocalists , 
 Miss McManus , pupil Sir Julius Benedict , 
 pianist . Sir Julius Benedict Mr. Fountain Meen 
 conductors 

 musIcAL performance given pupils 
 London Society Teaching Blind Upper Avenue 
 Road , Regent Park , 18th ult . , conducted Mr. 
 Edwin Barnes , Professor Music Society Schools . 
 excellent selection - songs rendered , 
 organ pianoforte solos works Bach , 
 Beethoven , Handel , Mozart played pupils 
 manner reflected highest credit 
 instructor 

 Tue South Norwood Musical Society gave second 
 concert season 18th ult . , performed 
 Mendelssohn ‘ ‘ hart pants ” ? com- 
 poser ‘ “ ‘ Hear prayer , ” selection 
 secular music . Miss Jessie Royd sang principal solos 
 taste effect , Miss Bawtree contributed 
 new Christmas song Conductor won 
 unanimous encore . chorus - singing good , 
 especially “ ‘ Hear prayer . ” Mr. W. J. Westbrook , 
 Mus . Bac . , Cantab . , conducted 




 ANTHEM PARTS , TENOR SOLO . 


 TENOR 


 — 108 . 


 4 1 


 _ SS SS 


 SES ES 


 SR 


 EFF F 


 | | } 7 1 


 PET = F EE : 


 LIBRARY 


 NASHVILLE , TEMNGSS65 


 7 - 1 - 


 2 - 2 


 4 4 


 ES SSS = = S22 


 REVIEWS 


 SCHLESINGER , BERLIN 


 30 


 MUSICAL TIM 


 LAMBORN COCK 


 FOREIGN NOTESan early production genius : Opeketta entitled 
 “ Les Tregueurs , ” performance Wis evidently 
 appreciated audience 

 @oors Coisevdateive having reopened 
 begineng monthy admit public annual 
 performances high - class music , concert season 
 1876 - 77 said definitely commenced Paris . 
 President Republic present 
 concert , inaugurated “ Heroica ” ’ 
 Beethoven . time , performances , chiefly 
 classical music , Concerts Populaires continuing 
 excellent work interests true art . 
 curious fact , , works Gluck , 
 Mozart , Beethoven , Weber , invariably meet 
 fullest appreciation audience concerts , 
 modern German school , present , 
 poor chance success . lately heard 
 noisy demonstrations dissent performance 
 Wagnerian music produced institution . 
 works Joachim Raff , , equally powerless 
 attracting attention French amateurs , spite 
 determined perseverance conductor , M. Pas- 
 deloup , — French equivalent 
 German “ Wolfgang”—is , consequence , 
 suspected German origin ! , according Le 
 Ménestrel , Raff charming Symphony “ Forest , ” 
 repeatedly performed Populaires , 
 little progress favour public . 
 hand , pamphlet entitled ‘ Richard Wagner 
 et les Parisiens , ” recently published French capital , 
 eagerly read . need hardly added 
 tendency favourable German operatic 
 reformer ; M. Pasdeloup escape smart 
 attacks directed unpatriotic impartiality 
 having produced works representative modern 
 Germany concerts . Setting aside artistic merits 
 demerits question , certainly somewhat 
 paradoxical , device ‘ ‘ Popular Concerts , ” 
 music forced Parisian people 
 happens peculiarly unpopular 

 manuscript Mass M. Gounod performed 
 Church St. Eustache St. Cecilia day , 
 direction composer . According Revue de la 
 Musique , new work composer ‘ “ Faust ” 
 sustain , increase , reputation . ‘ * 
 unproductivity , ” paper asks , ‘ 
 marked great composer career late ? 
 genius longer responds readily 
 wo nt ? ” conclusion 
 signs momentary fatigue , long 
 favourite national composer , renewed energy , 
 resume progress art . said M. 
 Gounod agreed write Opera forthcoming 
 International Exhibition Paris , libretto 
 pen M. Sardou 




 CORRESPONDENCE 


 CERTAIN DISCREPANCIES 


 EDITIONS MENDELSSOHN “ LIEDER . ” 


 EDITOR “ MUSICAL TIMES 


 34 


 OLD EDITIONS 


 NEW EDITIONS 


 MUSIC CENTENNIAL EXHIBITION . 


 EDITOR ‘ ‘ MUSICAL TIMES 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWSAmeERSHAM.—A Concert given Town Hall Thursday 
 r4th ult . , auspices Literary Club . Mr. Stone , jun . , 
 Chesham , sang “ Love request ” “ Thorn ” 
 effect , Miss Mead gave equal success “ fisher maiden ” ( 
 songs encored ) , Miss Jarvis sang “ winter night ” 
 “ o’clock morning ” good taste . pianoforte solo 
 Mrs. Cheese gave satisfaction . Concert decided 
 success 

 BiIRMINGHAM.—The Festival Choral Society gave Concert 
 series Friday evening 8th ult . , glees , & c. , 
 sung , including new - song Mr. A. R. Gaul , entitled ‘ 
 better land . ” vocalists Mdme . Trebelli , Signor Dorini , Mr. 
 Edward Lloyd , Herr Behrens.——The Messrs. Harrison gave 
 second subscription Concert Wednesday 13th ult . , Charles 
 Hallé band attraction evening . selection per- 
 formed included Beethoven “ Leonora , ” . 3 , Haydn Symphony 
 G Major , Mendelssohn Overture Midsummer Night 
 Dream . Mdme . Edith Wynne Signor Foli vocalists , 
 Mdme . Néruda solo violinist , Mr. Charles Hallé solo pianist . — 
 Carl Rosa Opera Company Theatre Royal week 
 commencing 4th ult 

 Bisuop AucKLAND.—The Auckland Musical Society gave 
 Concert season Town Hall 27th November . 
 programme consisted selection Handel Messiah , 
 “ Triumphal March , ” Madame Dolby St. Dorothea , Mac- 
 farren - day . artists engaged Madame Edith Wynne , 
 Mdlle . Bertha Brousil , Mons . Adolphe Brousil , & c. performance 
 great success , reflected highest credit Mr. Kilburn , 
 conductor 

 Dumrriges.—The Philharmonic Society gave successful perform . 
 ance Handel Samson evening 15th ult . select 
 orchestra Edinburgh , led Mr. Hope - Dambmann , ably sup- 
 ported vocalists efforts . choruses went excellently , 
 voices fresh balanced . Members Society 
 rendered solos great success . Mr. J. G. Pearson , organist 
 Greyfriars , presided harmonium , performance 
 reflected great credit Conductor , Mr. J. G. Gooden , organist 
 St. John Church 

 EpinsurGH.—The organ performances given periodically 
 University session Sir Herbert Oakeley , Class - room 
 Park Place , began tbe 7th ult . selection pieces included 
 works great masters , eminent 
 composers , gave additional interest programme , 
 novelty imparted portion . performance 
 highly satisfactory.——The sixth Concert season Choral 
 Union took place 18th ult . band , numbering - , 
 led Mr. Carrodus , conducted Mr. Adam Hamilton . Mr , 
 Carrodus appeared capacity solo violinist , 
 singer Miss Enriquez . programme included Mendelssohn 
 Scotch Symphony ; overtures ‘ * Leonora , ” . 3 ( Beethoven ) , 
 “ Ruler Spirits ” ( Weber ) ; Andante Minuet Professor 
 Oakeley , conducted composer ; brilliantly instrumented 
 introduction act Lohengrin , highly appre- 
 ciated encored . performance overture Leonora 
 meritorious , band entering spirit greatest 
 overtures 

 ExETER.—On rst ult . Miss Godolphin gave second Concert 
 Royal Public Rooms , assisted Miss Marian Lynton , Mr. 
 Chaplin Henry , Mr. Turle Lee , , playing pianoforte 
 solos , accompanied songs . — ~ members Madrigal 
 Society gave accustomed winter Concert Royal Public 
 Rooms 14th ult . programme selected , in- 
 cluded Mendelssohn “ Lake , ” favourite madrigals , 

 FARNHAM , SuRREY.—The fifth Concert Farnham Musical 
 Society given 12th ult . , programme sacred 
 secular music performed , marked improvement part- 
 singing evident . solo vocalists Misses Harris 
 M. J. Nash , Mrs. Scammell , Messrs. Sydenham , West , Kingham , 
 Hawker . new song , composed sung conductor , en- 
 titled ‘ Expected Ship , ” favourably received . 
 pianoforte playing Misses Rennie ( pupils Mr. Sydenham ) 
 praiseworthy . accompaniments ably played 
 Miss Sidebotham , Miss C. Julius , Miss Wells , Mr. Sydenham 

 GorLeston.—Herr Louis Loffler gave pianoforte recital 
 lecture great musical composers St. Andrew Hall , 
 7th ult . , highly - appreciative audience . illustrations 
 — including “ Sonata Pathétique ” ‘ Moonlight Sonata ” 
 Beethoven , “ Harmonious Blacksmith ” Handel , * Home , 
 sweet home ” Thalberg — admirably calculated display 
 styles composers , versatility 
 pianist powers ; efforts rewarded warm well- 
 deserved applause . proceeds concert devoted 
 St. Andrew Church Organ Fund 

 GRAVESEND.—On 13th ult . , annual Concert aid 
 funds Customs ’ Orphanage given Assembly Rooms , 
 direction Mr. W. Phillips . artists comprised Miss 
 Margaret Hancock , Mdme . Ashton , Mr. Albert Hubbard , Mr. 
 Mrs , Ernest Baynes , Mr. H. P. Matthews , Mr. H. Ashton , Mr. Fan , 
 Mr. Carpenter , Mr. Fountain Meen . concerted music 
 contributed Messrs. Ashton , Cozens , Meen , Hubbard ( 
 English Glee Union ) , supplemented Mdme . Ashton soloist . Miss 
 Turner , R.A.M. , played Sonata Mozart ; Mr. Fountain Meen 
 accompanied . ne 




 NAN ’ Newport , SALtop.—On Wednesday evening 6th ult . , mem- 
 bers Choral Society town gave Concert 
 season . programme included selection 
 judas Maccabeus , choruses rendered great 
 spirit precision . songs duets taken members 
 Society . band played Overture Saul 
 March St. Polycarp ( Sir F. A. G. Ouseley ) . second 
 concert miscellaneous . band chorus numbered 
 performers . Mr. Smart , organist Parish Church , conducted 

 Norwicu.—An evening Concert , continuation series 
 successfully inaugurated Mr. — Darken , given St. 
 Andrew Hall rst ult . Concert rendered additionally 
 attractive appearance , time Norwich 
 audience , Miss Agnes Zimmermann , pianist , Mdlle . Corani , 
 soprano vocalist . Beethoven Trio C minor finished 
 performance evening , executants Miss Agnes Zimmer- 
 mann , Madame Norman - Néruda , Signor Piatti , 
 highly appreciated respective solos . Mdlle . Corani gave 
 manifestations careful training , suffering 
 severe cold . Miss Enriquez , anestablished favourite 
 Norwich , sang Gluck air , “ Che fard , ” accustomed sweet 

 PertH.—On the7th ult . members Euterpeon Society gave 
 Concert season City Hall , supplemented 
 large efficient orchestra , led Mr. Carrodus , conducted 
 Dr. Sullivan . programme consisted Spohr 
 God , Thou art great , Gade ' Eri King Daughter , 
 given great precision . solos , usual , 
 rendered members Society , exception bari- 
 tone solo Erl King Daughter sung Mr. 
 Rudolf Hempel . second chiefly orchestral . Mozart 
 “ Non piu Andrai ” rendered Mr. Hempel spirited 
 manner secure encore . Mr. Carrodus performed move- 
 ments Mendelssohn Violin Concerto Fantasia Scotch 
 airs . playing orchestra remarkable precision 
 accuracy . Mrs. Hempel Miss Steele deserve highest praise 
 labours drilling chorus 

 RamsGATE.—A Concert , aid funds Ramsgate 
 St. Lawrence Royal Dispensary , given St. James Hall , 
 18th ult . , Ramsgate Amateur Choral Society . programme 
 consisted soles choruses Creation , miscellaneous 
 selection solos , glees , - songs . principal parts 
 sustained Mrs. Rogers , Mr. J. A. Birch , Mr. A. Moulding , Mr. 
 . Higgins . Mr. J. B. Lott , Mus . Bac . , Deputy Organist Canter- 
 ury Cathedral , presided harmonium , Mr. R. Walker 
 pianoforte . Mr. J. A. Birch Mr. T. Duckett Conductors 

 RicHMOND , Yorks.—Mr . James H. Rooks , organist Parish 
 Church , gave annual Concert Assembly Rooms Monday 
 evening 4th ult . , crowded audience . programme 
 arranged , carried following ladies gentle- 
 men : Vocalists — Miss Clarke , Miss L. Sanderson , Mrs. E. D. Swar- 
 breck , Miss J. Young , Alderman J. G. Croft ( ex - Mayor ) , Mr. C. G. 
 Croft , M.A .. Mr. W. H. Emsley , Herr Gruber , Rev. C. T. Hales , M.A. , 
 Mr. H. C. Priestman , Mr. E. D. Swarbreck , Mr. C. G. Tate , J.P. , 
 Rev. J. S. Warman , M.A , Instrumentalists — Pianoforte , Lady 
 Lawson , Miss Bennett , Miss Sanderson , Miss Louie Young ; 
 Violins , Colonel Bradley Herr Otto Deuk ; Violoncello , Sir John 
 Lawson , Bart . ; Harp , Miss Croft ; Harmoniums , Mr. Pulman 
 Mr. Rooks ; American Organ , Mr. Rooks . programme included 
 Overtures Guillaume Tell Zampa , Trio Piano , Violin , 
 Cello Beethoven , vocal selections Don Giovanni , Il 
 Flauto Magico , Faust , Lurline 

 RocHEsTER.—On Monday evening 11th ult.the members 
 Choral Society gave fine performance Handel Oratorio 
 Messiah new Corn Exchange . Eminent artists , vocal in- 
 strumental , engaged , solo vocalists Mrs. Osgood , 
 Madame Patey , Mr. Vernon Rigby , Mr. R.Hilton . orchestra 
 led Mr. J. T. Willy , Trumpet Obbligato played Mr. 
 T. Harper , Rev. W. H. Nutter conducted 




 38 


 MONTH . 


 MR . CHARLES FRY 


 ADIES ’ SCHOOL , LANGHAM HOUSE 


 RGAN , PIANOFORTE , HARMONIUM , HAR : 


 ANNE CHURCH , WANDSWORTH 


 1RY 


 39 


 SIMPSON CO . 


 IMPROVED HARMONIUMS 


 PIANOFORTES HALF PRICE 


 SHEET MUSIC - IOTH 


 780 PIANOFORTES 


 KEYED INSTRUMENTS ; 


 40,000 PIECES " HIGH - CLASS MUSIC 


 VIEW , 5 , PAVEMENT , FINSBURY , 


 OM MUNION SERVICE ALEXANDER S. 


 NORTH COATES SUPPLEMENTAL 


 ETZLER & CO."S CHRISTMAS ANTHEMS 


 BLESSED COMETH » 


 O CHORAL ’ SOCIETIES . — CHRISTMAS 


 LFRED ALLEN “ GAVOTTE FESTIVALE 


 R. DEARLE NEW ANDANTE PASTORALE 


 VILLAGE ORGANIST 


 CLASSICAL ORGANIST . 


 EIGHTY 


 MUSICAL SENTENCES 


 


 OPINIONS PRESS 


 COMPLETE EDITION . 


 MENDELSSOHN LIEDER OHNE 


 WORTEFolio , handsomely bound cloth gilt obd ' 2 G Hidgs 
 Octavo , FH 4 shay Pies vo 
 PM el RC er er PMT IE » 
 ave Editions containing 7th 
 8th Books 

 BEETHOVEN SONATAS 

 Edited Fingered AGNES ZIMMERMANN . 
 Folio , elegantly bound , gilt edges ... ... 21s . od . 
 Octavo < < veo coe , 78 . Gad 




 MOZART SONATAS 


 LONDON : NOVELLO , EWER CO . 


 NEW ORATORIO CHORAL SOCIETIES , ETC 


 PILGRIM PROGRESS 


 ARRANGED COMPOSED 


 VIOLIN : 


 FAMOUS MAKERS IMITATORS . 


 WILLIAM J. YOUNG 


 OCTAVO . 


 OPINIONS PRESS 


 OU 


 RI 


 41 


 SECOND SERIES 


 ANGLICAN CHORAL SERVICE BOOK . 


 USELEY MONK PSALTER 


 E.G. 


 OULE COLLECTION WORDS 


 OULE COLLECTION 527 CHANTS , 57 


 PSALTER , PROPER PSALMS , HYMNS , 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI 


 ORDER HOLY COMMUNION . 


 PRICE SHILLINGS . 


 MORNING , COMMUNION , EVENING 


 LAIN - SONG EVENING SERVICE.—MAGNI- 


 MUSICAL PRESENTS 


 PITMAN MUSICAL LIBRARY 


 HANDSOMELY BOUND VOLUMES 


 MELODIA DIVINAor , SACRED COMPANION PIANOFORTE 
 HARMONIUM 

 comprehensive Collection Psalms Hymns , short 
 Anthems , adaptations Handel , Haydn , Mozart , Beethoven , 
 Mendelssohn , original compositions . 
 principal Religious Periodicals , Magazines , & c. , reviewed 
 “ Melodia Divina ” favourably , work great request 
 Gift - book Sacred Music . elegant appearance sterling 
 worth collection Family Sacred Music render appro- 
 priate Birth - day Gift - book Wedding Present 

 WARD TRIOS AMATEURS 




 FAWCETT HARMONIUM TUTOR 


 CHEAP MUSIC 


 TRIOS LADIES ’ VOICES 


 ARRANGED T. CRAMPTON . 


 PARADISE . 


 J. FAWCETT POPULAR ORATORIO 


 F , PITMAN , 20 , PATERNOSTER ROW , LONDON 


 SHILLING WORTH MUSIC . 


 MUSICAL BUDGET 


 LFRED R. GAUL CHORAL SONGS , 


 VOCAL ALBUM MUSIC 


 EDITED CHIEFLY 


 PROFESSOR G. A. MACFARREN 


 ORDER PROFESSOR MACFARREN EDITIONS 


 CHORAL SONGS SCHOOL & HOME 


 - ORIGINAL SONGS 


 


 , , VOICES 


 - SONGS CHRISTMAS 


 DEATH OLD RING , WILD BELLS . 


 YEAR , SONG SEASON . 


 SKATER SONG 


 HOLLY . 


 MERRY , . 


 R. L. PEARSALL 


 COLLEGIATE SERIES 


 DR . S. S. WESLEY 


 BERTHOLD TOURS 


 DULCIANA . 


 SCHUMANN . 


 J. P. KNIGHT . 


 NEW BALLADS . 


 BERTHOLD TOURS 


 R. L. PEARSALL . 


 CONSECUTIVE FIFTHS OCTAVES 


 COUNTERPOINT 


 ESSAY , R. L. PEARSALL 


 C. JEFFERYS , 67 , BERNERS ST 


 COLLECTION 


 - SONGS , GLEES , 


 EDITED 


 MICHAEL WATSON 


 MUSICAL UNMUSICAL CAREER 


 ADAGIO , CHROMATIC , MODERATO } ; CONCLUDING 


 CODA 


 MUSICAL DEGREES , 


 POSITIVE , COMPARATIVE , SUPERLATIVE , 


 WARBLED ( CROWED ) MUSICAL BIRD 


 


 CONTENTS : 


 CHAP . CHAP . 


 EXPLANATION 


 BEAUT 


 TS 


 _ _ 


 SERVICE PRAISE CHRISTIAN WORSHIP 


 GREATLY FACILITATED 


 CHRISTIAN PSALMIST 


 COLLECTION 


 OPINIONS PRESS 


 MUSIC MILLION 


 WORKS PHILIP PHILLIPS 


 NINETY - FIFTH THOUSAND 


 AMERICAN SACRED SONGSTER 


 OLD NOTATION TONIC SOL - FA . 


 - SEVENTH THOUSAND 


 SONG LIFE 


 SUNDAY SCHOOLS 


 - SECOND THOUSAND 


 SINGING ANNUAL 


 SUNDAY SCHOOLS . 


 FIFTEENTH THOUSAND 


 SONG MINISTRY 


 


 THIRTIETH THOUSAND 


 AMERICAN SACRED SONGSTER 


 WORDS EDITION 


 SIXTIETH THOUSAND 


 SONG LIFE 


 SUNDAY SCHOOLS 


 FOURTH THOUSAND 


 VOICE SONG 


 MR . PHILLIPS ’ NEW WORK . 


 CONTAINING SONGS , 


 FIFTEENTH THOUSAND 


 SONG MINISTRY 


 2 . 


 BEAUTIFULLY ILLUSTRATED CUTS ORNAMENTAL 


 BORDERS 


 SUNDAY SCHOOL UNION 


 56 , OLD BAILEY , LONDON , E.C 


 NOVELLO OCTAVO CHORUSES 


 BACH CHRISTMAS ORATORIO 


 BARNBY REBEKAH 


 BENEDICT ST . PETER19 \ swallow death # 4 
 620 Fearthou ... 2 
 621 SinguntotheLord ... 2 

 BEETHOVEN MASS D. 
 344 Kyrieeleison ... ooo eos oe 
 5583 Gloria excelsis si oan pre 
 554 Credo ps ono 
 555 Sanctus Benedictus pve se 
 550 Agnus Dei sé ove ooo oe 8§ 
 BEETHOVEN 
 RUINS ATHENS . 
 366 Daughter high - thronéd Jove 1 } 
 367 thou didst frown oS oe 
 368 Twine ye garlands ove oe § 
 369 Susceptible hearts ... es oo 2 
 370 Deign , great Apollo ... wes 3 
 371 Hail , mighty master , hail ... 3 
 CHERUBINI REQUIEM . 
 $ 31 Introit — Requiem s#xternam — 
 unto pure heart 
 Graduale — Requiem eternam ae 
 557 unto humble 

 CHERUBINI REQUIEM.—Coniinued 




 GADE ERL KING DAUGHTER . 


 GADE ZION . 


 GADE CRUSADERS 


 S.A ) 3 


 GRAUN PASSION 


 GOUNOD MESSE SOLENNELLE 


 GOUNOD COMMUNION . 


 [ MESSE SOLENNELLE . ] 


 HAYDN PASSION . 


 HILLER NALA DAMAYANTI 


 ( S.A. ) 1 


 HILLER SONG VICTORY 


 HUMMEL 


 HUMMEL MASS D. 


 HUMMEL 


 COMMUNION SERVICE D. 


 MENDELSSOHN 


 MIDSUMMER NIGHT DREAM . 


 MENDELSSOHN 95TH PSALM . 


 MENDELSSOHN LAUDA SION . 


 [ PRAISE 


 MENDELSSOHN ATHALIE . 


 PR OON 


 WF AGDN 


 MACFARREN - DAY 


 SCHUBERT MASS C. , 


 SCHUBERT 


 COMMUNION SERVICE C , 


 SCHUBERT MASS G. 


 SCHUBERT 


 COMMUNION SERVICE G. 


 SCHUBERT MASS F. 


 SCHUBERT 


 COMMUNION SERVICE F. 


 SCHUBERT 


 COMMUNION SERVICE BP . 


 SCHUBERT 


 WOWNAQDN 


 SCHUMANN 


 PILGRIMAGE ROSE 


 SCHUMANN FAUST 


 SCHUMANN MANFRED 


 SCHUMANN 


 PARADISE PERI 


 NDNHHDVHWOHDN 


 SMART 


 BRIDE DUNKERRON . 


 RHNWH 


 SPOHR FALL BABYLON 


 424 


 SPOHR CALVARY 


 SPOHR CHRISTIAN PRAYER 


 A. SULLIVAN 


 VAN BREE ' 


 ST . CECILIA DAY 


 542 


 543 


 544 


 545 


 546 


 547 


 548 


 549 


 HANDEL 


 399 


 410 


 408 


 402 


 398 


 4 NNH DD 


 ORGANOPHONE 


 [ 27 


 PRICES 


 SETS VIBRATORS , 2 KNEE ACTIONS , 23 REGISTERS — _ 


 857 , OXFORD STREET , LONDON