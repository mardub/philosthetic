


HE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR


FOUNDED IN 


PUBLISHED ON THE FIRST OF EVERY


ROYAL CHORAL SOCIETY


C.V.O., N


SATURDAY, JANUARY 1


ESSIAH .- - 


MISS RUTH VINCENT. 


MISS PHYLLIS L&TT. 


MR. BEN DAVIES. 


ROBERT RADFORD


HANDEI


MR


SATURDAY, FEBRUARY 5, A! P.M


SAMSON AND DELILAH 


MADAME KIRKBY LUNN 


MR. FRANK MULLINGS. 


MR. EDWARD HALLAND. 


MR. EDMUND BURKE


SAINT-SAENS


ROYAL ACADEMY OF MUSIC. 


YORK GATE, MARYLEBONE ROAD, LONDON, N.W.1


THE ROYAL COLLEGE OF MUSIC. 


PRINCE CONSORT ROAD, SOUTH KENSINGTON, S.W. 7. 


HIS MAJESTY THE KING. 


921. | 


THE ROYAL COLLEGE OF M


USIC PATRON'S FUND | 


JANUARY 1


1844 


MONTH


THE ASSOCIATED BOARD 


OF THE R.A.M. AND THE R.C.M. 


FOR LOCAL EXAMINATIONS IN MUSIC


21


19


GUILDHALL SCHOOL OF MUSIC


ESTABLISHED AND MANAGED BY THE CORPORATION OF LONDON. 


VICTORIA EMBANKMENT, E.C. 4. 


PRINCIPAL ° LANDON RONALD. 


THE ROYAL COLLEGE OF ORGANISTS


ROYAL 


MANCHESTER COLLEGE OF MUSIC


18, 


THE MUSICAL


BIRMINGHAM & MIDLAND INSTITUTE


SCHOOL OF MUSIC. 


SESSION 1920-1921


MANCHESTER SCHOOL OF MUSIC. 


FOUNDED 1892. 


UNIVERSITY OF DURHAM


THE LONDON COLLEGE FOR CHORISTERS. 


BLOMFIELD CRE 


OF VIOLINISTS 


LTD 


MARTIN'S LANE, LONDON, W.C 


EXAMINATIONS


COLLEGE 


VIOLA


»ELE


JACKSON


THE TE CHNIQUER


W.1


MUSIC SECT ION. 


W.C.1


Y. M.C.A., 


COMPOSITIONS RE 3V SE D. 


WRITTEN TO SONGS 


1S1


MUSICAL


BRIGGS


LONDON COLLEGE OF MUSIC. 


GREAT MARLBOROUGH STREET, LONDON, W


EXAMINATIONS—LOCAL AND HIGHER


A. GAMBIER HOLMES, 


MUSIC


VICTORIA COLLEGE OF 


LONDON. 


INCORPORATED 1891. 


158


INCORPORATED GUILD OF CHURCH 


MUSICIANS 


THE VENERABLE "ARG HDE ACON OF 


MIDDLESEX 


F.R.A.M


TION 


1921


INDUC 


ASSOCIATE (A.1.G.C.M.), LICENTIATE (L.1.G.C.M)), 


COMPETITIONS, 1921. 


BRONZE MEDALS


F.R.A.M


F.1.G.CM 


)., A.R.C.M


URCH


ON, D.D


LESEX 


A.M


NDUCTION 


[L.1.G.C.M,), 


AOLIAN HALL. MR. CHARLES TREE


THE 


NUSICAL (COMPETITION) FESTIVAL. — BRITISH MUSIC SOCIETY


MAY 40H TO TO I4TH. (NATIONAL AND INTERNATIONAL


CONCERT DIRECTION. £15,000 WANTED


ARTISTS FOR ORATORIO AND BALLAD FOR THE FOSTERING AND ENCOURAGEMENT OF 


CONCERTS. BRITISH MUSIC AT HOME AND ABROAD


OF 


THE GENERAL SECRETARY


HOOL OF M 25 LL S B N. NEWCASTLE-UNDER-LYMI 


4 THE MUSICAL


LONDON, W.C,. 1


MATRIC


COMBINED


A.R.C.O 


A.R.C.M. 


B.A


RCO


SAVE


M.C.A


AND MONEY


ROAD


AND


TIME


PHI


ILKESTON NOTTINGHAM


201


CORRESPONDENC CE 


TUITION


NORMAN SPRANKLIN


DIPLOMA PAPER WORK. 


L.R.A.M


COURSES ALSO IN 


APPRECIATION, 


KNOWLEDGE


MUSICAL 


GENERAL MUSICAL 


SINGLE SUBJECTS 


LINDEN ROAD, 


BRISTOL


3033


48, 


REDLAND


OF THE CITY OF CAPE 


TOWN


YICIPAL ORCHESTRA. 


OF MUSICAL DIRECTOR 


CONDUCTOR


CORPORATION


MUN


APPOINTMENT AND


COUNTY BOROUGH OF SOUTHPORT 


EDUCATION COMMITTEE. 


THE


MUSICAL


I 1921


PROFESSIONAL NOTICES


MOUNTFORD SCOTT (TENOR). 


R. J


MR. 


A.R. 


F.R.C.O., 


(COMPLETE TRAINING BY CORRESPONDENCE


MISS LYDIA JOHN


A.R.A.M. CONTRALTO. 


MR. SAMU EL MASTE


TENOR 


RALPH WILLIAMS (LR


RS


A.M


WILLIAM BARRAND


OSWALD PEPPERCORN


HERBERT TRACEY


BASS). 


LEDGE


MRS. J. 


PIANIST, A.R.M.C.M 


R. MAITLAND


DOUGLAS STEVENS 


(TEACHER) ELOCUTION 


NEW OXFORD STREET, 


5 W.C. 1. 


R AND


L.R.A.M


MR. W. 


ITED, 


H. BREARE 


TO STUDENTS AND THE PROFESSION


MR. AMBROSE COVIELLO


| COMPOSERS’ MSS


REVISED AND PREPARED FOR PRINTING


COURSE ENSURING THOROUGH UNDERSTANDING AND PRACTICAL 


EFFICIENCY


G. HUBI- NE AV 4 OMBE 


(LYRIC AUTHOR). 


MUSICAL COMPOSITIONS


EVISION OF 


PECIAL CORRESPONDENCE COURSES :— 


F.R.C.O. SPECIALIST IN CORRESPONDENCE 


THE MUSICAL


DR. LEWIS’ TEXT-BOOKS


N ISS F. HELENA MARKS 


TORMAN & BEARD’S P NEUMATIC ACT


A.R.C .M. B


ISS MARGARET YOUNG, 


THE ELEMENTS OF HARMOM


W ORE E . \THEDRA 


NOR SING \NTE 


H ORGANS


PIANIST


ENGLISH


FOLK-SONGS


COLLECTED AND ARRANGED WITH PIANOFORTE ACCOMPANIMENT


BY


CECIL J. SHARP


SELECTED EDITION


SONGS AND BALLADS. 


(VOLUMES I. AND II


PREFACE. 


TRINITY COLLEGE OF MUSIC


THE RT. HON. THE EARL OF SHAFTESBURY, K.P 


SUCCESSFUL PI


_ , . IN 


TUSK 


GRADED VIOLIN 


CYRIL SCOTT | 


3. SCHERZO. 4. ELEGY. FOUR OLD FRENCH 


5. RONDO RETROSPECTIVO. © HARPSICHORD PIECES


AKRANGEL 


PIANOFORTE SOLO


PARTS ONLY, TWELVI


BY


ELKIN & CO. LTD. ALFRED MOFFAT


4ND SINGING-CLASS CIRCULAR


JANUARY 1


IQ21


PERFORMING FEES


ITALIAN COMPOSERS OF 


INTRODUCTION


SOME


1789-1870 


II


THE CONCERT-ROOM SONG


BACH


S70


THE ORGAN WORKS OF


IV. —THE 


AND


HARVEY GRACE


LATER WEIMAR PRELUDES


DES


1921 13


55


SS


14


15


16


NEW LIGHT ON EARLY TUDOR 


COMPOSERS 


XIV.—RICHARD BRAMSTON


15


VILL


THE *‘BEGGAR’S


BY


OPERA’ 


FRANK KIDSON


J . : : . > SYDNEY GREW 


XUM


20


1921


CRITICS 


MOULT


CONCERTS AND 


1921 21


SO


A SONY 2 4 


THE MUSICAL TIM


ES—JANUARY


1921 2suggest

I make haste to add that since the above was 
written the Jaily News has given us a pleasant 
shock. Asa result, no doubt, of due prodding by 
its excellent music critic, it celebrated the 150th 
anniversary of Beethoven’s birth by giving us a 
leading article on the composer, and, on the same 
page, a symposium on his present position as a 
musical influence

Does any reader know of a fairly recent book 
giving the specifications and other particulars of 
the principal organs in the British Isles—something 
after the style of the old Hopkins and Rimbault? 
A correspondent is anxious to obtain such a book. | 
In view of recent developments in organ-building, 
awork of the kind is due, if not already in 
being

grammes—of Lieder in the German language, are the 
rule on many London programmes

I seem to remember that at the time of 
Mr. Fanning’s visit an attempt to sing some German 
songs in the original language caused ‘a certain 
liveliness’ at a West End concert-hall. And ‘two 
groups’ and ‘ whole programmes’ of such songs are 
not the rule even now, six months since Mr. Fanning’s 
visit. I myself have been bursting to celebrate 
the Beethoven centenary by hearing my favourite 
* Adelaide’ in her own tongue wherein she 
born ; far no singer seems ready

sung

onslaught, down

The * Walzermasken,’ however, defied conquest even 
after two years of daily battlement, and it is now nearly 
four years before I can claim maturity and pleasure in 
performance. Put against this the fact that I worked 
at Beethoven’s Op. 2, No. 3, for the first time recently, 
memorized and polished it in a week, and achieved the 
same with * Pathétique’ in two days. That gives you 
just the ratio

And now ‘Satire,’ of what? The spirit of satire and 
in the next * Karikatur,’ assail pedantry and everything 
and yet make beautiful music. ‘Oh, well, of 
course,’ you may say. Not so fast, however; look at 
the score and see what is there and how it is handled to 
make it what it is instead of a sepulchre. If old 
Albrechtsberger might rise to hear them he would 
doubtless drop dead again




IS MARY GARDEN 


AGAIN


SPLENDOUR THAT 


REVEALED


BY RUTH MILLER 


192


PRIZE COMPETITIONS


1921


SONGS


THE


1921


PIANOFORTE


ORGAN


XUMNovember Woods ’ dates about five years later than 
‘The Garden of Fand,’ and shows the composer to 
have reached a further stage in his development. 
It is more closely knit, more lucid, and has less trace 
of effort. It seems to be more inevitably right. 
contrasts are more effective, 
because they seem to come from within. The 
programme is a psychological one, dealing with inner 
experiences which all of us may share. There is 
great mastery of orchestral colour, and it is highly 
individual. In the middle section, where happy 
memories contrast with the gloom of the November 
wood, the composer reaches a high level of imagina- 
tion and inspiration, and there is a bigness in the 
music which he does not always achieve

Mr. Hamilton Harty conducted with sympathy and 
enthusiasm, and the Philharmonic Orchestra played 
very finely, Excellent, too, was its playing of 
Mr. Harty’s own skilful and discreet arrangement of 
Handel’s ‘Water Music’ and of Tchaikovsky’s fifth 
Symphony. December 16 was the hundred and 
fiftieth anniversary of Beethoven’s birth, so the 
‘Coriolan’ Overture was included in the programme

a not too generous tribute to Beethoven

The rest of the programme was devoted to un- 
accompanied singing by the Philharmonic Choir, 
under Mr. Kennedy Scott. The Choir has made great 
strides since last year. In quality of tone, cleanness 
of attack, and variety of expression the difference is 
remarkable. The singing of the music of Sweelinck, 
Pretorius, Calvisius, and Orlando Gibbons was very 
human and expressive, and the clearness of the part- 
singing was admirable. Equally good was the 
singing of Dr. Vaughan Williams’ five arrangements 
of English Folk-Songs. They are exceedingly 
clever, and only a few times does the composer allow 
his skill in the making of choral effect to obscure 
the simple beauty of the tunes. The Wassail Song, 
humorous both in words and music, is the happiest 
of all, and it had to be repeated




CHAMBER MUSICPIANISTS AND VIOLINISTS

In the sphere of pianoforte playing the outstanding 
events have been the Chopin, Schumann, and Liszt | 
recitals of M. Cortdét, the only regrettable feature, | 
about which is that Wigmore Hall is really too| 
small for the audiences. Nothing new can with 
advantage be said about his remarkable playing, nor | 
need anything be added to what has been said so | 
often about Mr. Lamond’s Beethoven recitals. Mr. | 
Hoffmann’s popularity is speedily growing, and it is| 
satisfactory to know that when he returns to us in| 
the summer he will be heard with orchestra. Mr. | 
Rubinstein has continued to gain favour, and his | 
playing of sonatas with M. Kochanski showed that 
he has deeper musical qualities than those required 
of a brilliant soloist. One of the most interesting 
pianoforte recitals has been that of music for two 
pianofortes given on December 4 by Miss Irene 
Scharrer and Miss Myra Hess, whose unanimity of 
style is remarkable. They played among other things 
an arrangement of ‘Aprés-midi d’un Faune,’ an| 
interesting experiment, not altogether successful in | 
spite of the excellence of the performance. AsIwasat 
Kingsway Hall at that moment, I have to rely on 
the authority of a trustworthy friend. Miss Winifred 
Christie has earned a high place for herself among 
pianists in the United States. She has given an 
orchestral concert and a recital, and her well-balanced 
lucid style and finished technique should make a 
wide appeal. There should always be room for-such 
musicianly playing

Among many violinists who have given recitals, 
the most interesting, perhaps, has been M. 
Kochanski, whose sterling musical qualities and 
absence of sensational methods deserve very high 
praise. The new pieces of Szymanowsky, which he 
played, are full of charm. Mr. Louis Godowsky is a 
young violinist whose great promise has many times | 
been mentioned. His performance of the Elgar | 
Concerto at his own recital is the ‘best thing he has | 
done so far. He also played some graceful and} 
effective little pieces of his own




SINGERS AND SINGING


SOME NOTES FOR JANUARY


MARCEL DUPRE’S CONCERT


30


THE CARL ROSA SEASON A NEW WORK 


REPERTOIRE


A POPULAR


W 1


A VERSATILE COMPANY


THE NEW ADDITIONS


ALTO


ENOR


ANTHEM FOR GENERAL USE


SS ————— == 


THE ROSEATE HUES OF EARLY DAWN


SSS = SS 


> * _— —_ — = —_ —_— — _ 


SSS == == 22S ES SE


2 — - _L____ ————_— 


ANS = SEN


THE ROSEATE HUES OF EARLY DAWN


——— 22124 


4— 5 


9-2


== SS SS 


_ SS SS = SS SS ES LES TS 


EAL SA 2


CAN


= LS = —— 


$$ — ————————— — ———, -—— —_— ———____—. 


THE ROSEATE HUES OF


EARLY DAWN


= EEG 


—_ = ; ~ | —— = -{ — - 7 


XUM


THE ROSEATE HUES OF EARLY DAWN


KING


= ———— . —— ##-—_}, _ 


XUM


1921


H.M 


41


RHOW


TOLLESaint-Saens

from Guilmant’s D minor Sonata ; 
Beethoven’s uta in A; Introduction, 
Variations, C. H. Llovd; Fantasia in E flat

Son




‘THE CHURCH WILL BE HEATED


ORGAN RECITALS 


APPOINTMENTS


THE MUSICAL


MR


ERNEST NEWMAN ON ‘THE PIANO


NSHIVELY


THE


45but apart altogether from this, the great charm of 
Mr. Button’s book lies in the youthful exuberance with 
which he throws overboard all authority altogether. He 
appeals not to authority, but to reason, system, and logic. 
(Lhe very title of his work proclaims this.) To fall back, 
then, after all, on authority is rather to knock the bottom 
out of his case. Moreover, it leaves it open to me to make 
the obvious retort that if Mr. Button has Sir Edward Elgar

le, I, at any rate, Bach, Beethoven, and the

on his side, have 
'—Yours, Xc., \. R




10


VINCENT D’INDY’S VIEW OF ITARMON\ 


LETTERS OF GREAT COMPOSERS


PERMANENT OPERA IN ENGLISII 


LAUDI


REV


ORGAN AT LOLINGEN, GERMAN


THE PRICE OF CLASSIC FIDDLES 


1420


16960


A CORNET WANTED


10


14920


C. SANFORD TERRY


D* MAINZER’S MUSIC BOOK FOR THE 


YOUNG. 


PREFACE


[)* MAINZER’S FIFTY MELODIES FOR CHIL- 


THE THREE FAVORITE SONGS OF THE SEASON, 


7E WELCOME THEE BACK TO THY NATIVE 


THE ROYAL ACADEMY OF MUSICunl Elgar. 
which Mr

from Beethoven’s Pianoforte Concerto in C minor, played by 
Miss Betty Humby, and the /%va/e of Rachmaninov’s Piano- 
forte Concerto, played by Miss Elsie Betts, both of whom 
showed themselves to be pianists of very considerable promise. 
Three interesting songs by Sylvia Carmine (student), Saint- 
Saéns’ Ballad ‘ The Drummer’s Betrothed,’ and Mendels- 
sohn’s scena *‘ Infelice,’ completed the programme

local professional musicians, 
Burnett (pianoforte




THE MUSICAL T


IMES—JANUARY


1921


MUSICIANS 


OUR OWN CORE


PON DENTS


ABERDEEN 


R. J


BATH


BELFAST 


BIRMINGHAM


49


BOURNEMOUTH


BRISTOI 


20, 


CORNWALL


COVENTRY AND DISTRICTDARLINGTON AND DISTRICT

music is flourishing, and we have already 
oncerts at Polum Hall School. The first, on 
28, was a ‘cello and pianoforte recital by the 
Misses Hetty and Ethel Page, with Miss Elsie Chambers. 
The principal works were Sonatas by Saint-Saens and 
Sammartini. On November 2 London Philharmonic 
Quartet played Beethoven in I 
(Quartet, and * Puck

Chamber 
had two




DEVON


THE MUSICAL


EDINBURGH


11SOOvertur 
at the time of writing, most interesting concert 
series

On November 17 the Royal Choral 
performance of Mendelssohn’s ‘Hymn of 
‘For the Fallen,’ Beethoven’s Choral Fantasia (with cal 
pianist, Mr. Ramsay ind Gustav Hlolst’s setting 
of Psalm 86 and Psalm 147. Of these last named numbers, 
which were given for the first time at Edinburgh, Psalm 86 
The ex eedingly 
untal treatment cf the second example was inter 
esting, but the general effect was not so uplifting, The 
soloists were Miss Dorothy Silk, Mrs. John Walker, and 
Mr. Arthur Jordan

On the same evening Mr. Appleyard gave a pianoforte 
A pupil of Leschetitzky, he does great credit to his

On November 25, Miss Marjorie Greenfield (vocalist 
Miss Dorothy Chalmers (violin) gave a very 
interesting chamber concert. Miss Chalmers, accompanied 
hy Miss Isobel Gray, gave a fine reading of Brahms’ 
I) minor Sonata, Op, 108, and Miss Greenfield covered a 
wide range of vocal art

Mr. John Petrie Dunn, assistant-lecturer with Prof. Tovey 
at Edinburgh University, is an accomplished pianist. On 
December 7—in co-operation with Mr. Watt Jupp (violin 
Mr. Bernard gave a fine 
programme. The Trios were Beethoven's Op. 70, No. 1, 
and Variations, Op, 121

On December 9, M. r-Grondahl gave a pianoforte 
recital, and strengthened the impression he made at the 
orchestral concert already referred to




GLASGOW


_—_HASTINGS

The laudable ambition of the Hastings Corporation to 
place the town in the front rank of pleasure resorts 
musically a id otherwise—is rapidly materialising. A decided 
step in this direction was made on November 18, when 
Mr. Julian Clifford secured the first provincial performance 
of Montague Phillips’ new Pianoforte Concerto—one of the 
smaller sensations of the recent * Proms.’ Whatever the 
impression it made then, it was most warmly received here. 
for its instant appeal is undeniable. Planned on a grandiose 
scale, it bristles with difficulties, of which no more capable 
exponent than Mr. William James could be desired ; while 
the orchestra, which practically ‘read’ the work, en ered | 
into its intricacies with might and main, under the 
composer’s safe guidance. The same concert offered the 
‘Unfinished ’ and a really stirring account of Rimsky- 
Korsakov’s ‘Caprice Espagfiole.” Mr. W. Lf. Reed played | 
Beethoven’s Violin Concerto as only a true disciple of | 
Joachim could, for he has all the essential qualities of a 
Beethoven player. Moszkowski’s Pianoforte Concerto, 
played by Miss Helen Guest with facile execution, but with 
not the best pedalling, was heard here for the first time. | 
Miss Lena Kontorovitch was alternately passionate and | 
tender in Saint-Saens’ Violin Concerto, where she found

many openings for the exercise of her surprisingly intense 
temperament




KENTin aid of the West Kent General Hospital

i of Beethoven's fifth

Overture, and the




24, 


1 1 1 , 


LIVERPOOLworthily sustained the reputation of our younger natiy 
school. This was Balfour Gardiner’s choral tone-poem 
* April,’ for chorus and orchestra, which provided a delightfy 
ten minutes in a performance ally conducted by the chorys. 
master, Dr. A. W. Pollitt. The subject is a poem ly 
Edward Carpenter, and is an ecstatic apostrophe to th 
spirit of Spring, and to ‘ April, month of Nymphs, Faun, 
and Cupids.’ It would be difficult indeed to conceive musi 
more in keeping with the poetic fancy and imagery of th 
lines. The chorus-part makes instrumental demands on the 
singers, which were courageously surmounted, counsels of 
perfection apart, and it is hoped that a further hearing 
be accorded to this extremely clever and effective work

The performance of Beethoven’s C minor Symphony at 
the Philharmonic concert on November 30 gave immensg 
satisfaction to a number of subscribers of the oider

may

school 
who consider they have heard sufficient of the music of the 
ultra-modern type to last them for a long time. The 
performance, which was conducted by M. Bronislaw Szulc, 
of Warsaw, was very satisfactory

Elgar’s ‘Polonia’ also lost nothing of its interest and 
power in the appreciative hands of M. Szulc, but he failed 
to preduce anything specially new in the symphonic poem, 
‘La Steppe,’ by the late Siegmund Noskovski, another 
Warsaw musician of mark (the teacher of Rozycki, whose 
symphonic poem ‘ Anhelli’ had to be omitted owing to lack 
of time). The vocalist was Mr. Frank Mullings, for whom 
an apology was made on the ground of hoarseness. All the 
same he was acceptably heard in songs by Wagner, and 
especially in Hugo Wolt’s ‘Secrecy.’ His version of 
Schubert’s ‘ Erl King’ did not equally please. M. Bronislaw 
Szulc, a pupil of Noskovski and Nikisch, comes 
good musical stock. His father was for forty years at the 
Warsaw Conservatorium, where he Paderewski’s 
professor. All his sons are musicians, and a notable family 
record is the performance given of Beethoven’s Septet by 
Szule fer

It was evident that great pains had been taken with 
preparation of Berlioz’s * Faust,’ of which the performance 
given by the Welsh Choral Union on November 20 refi 
credit on its able conductor, Mr. Hopkin Evans, and upon 
his superb choral material. Ten years have elapsed since 
the Union’s previous performance of this great work, and if 
indeed this was not surpassed on the present 
find that the old spirit, 
enthusiasm remain. with the old ideals




MANCHESTER AND DISTRICTThe Co-operative Wholesale Society's Choir bids fair soon 
to become our most prominent male-voice choir, and as such 
it ought to consign to the dust-heap some of the things 
sung on this occasion. Miss Desmond and Mr. Albert 
Sammons deserve mention not alone for the beauty and 
distinction of their executive ability, but for the highet 
standard of taste shown in their selections

Pride of place must be awarded to the Edith Robinson 
Quartet for its work in the past month in the Beethoven 
celebration performances of all the (Quartets in chronological 
order, Dr. Brodsky and Mr. R. J. Forbes are to follow in 
January with the Pianoforte Sonatas, and the oficial Hallé 
commemoration concert will be given on the day these notes 
go to press

Two young trios—Miss Midgeley and Messrs. Hatton and

K. Moorhouse—show both determination and ambition to 
make for themselves a place in the city’s chamber-music 
Nearly all the trios and quartets which have sprung 
up here in the last ten or fifteen years owe their early 
training in ensemble and musical inspiration to Dr. Brodsky, 
who, twenty-five years ago, founded the quartet bearing his 
name. On November 30 his anniversary was recognized at 
the Royal Manchester College of Music in separate 
presentations from (a) the College, (4) the staff, (c) the 
In replying, Dr. Brodsky commented on the 
inadequacy of present equipment to meet the great and 
sudden influx of students. It cannot be met immediately, 
but the finest reward for his ungrudging work of the last 
twenty years would be to provide for this necessity

The transfer of the Brodsky concerts to Monday mid-day 
at Houldsworth Hall has been attended by too much bad 
luck in the way of fog and other hindrances to free move- 
ment to enable any fair estimate of the situation to be made. 
The first of the series included the Elgar (Quartet, and the 
secon was by way of amplifying the Beethoven celebrations, 
in which the Robinson Quartet had taken the initiative

THE MUSICAL




NEWCASTLE ON TYNEd did its work efficiently, though 
always quite

estra ¢ 
horn passag the : lid not 
| before th 
on * Progress 
mmbated the 
said to supersede 
wy had to give 
A Mozart 
1 Beethoven 
t Mozcart. 
Glasgow University, gave

ture




NOTTINGHAM AND DISTRICT 


16


TIMES—JANUARY 1encouraging second chambet

Quartet. Beethoven’s (Quartet in I 
was succeeded by Joseph Speaight’s three *Shakespearia 
Pieces’ for string quartet: (@) ‘The Lonely Shepheri, 
5) *(ueen Mab sleeps,’ (c) * Puck.’ Finally came Brahny 
gorgeous (Quartet, Op. i minor (for pianoforte, 
violin, viola, and violoncello), by all performers wi 
entrancing br.lliance and imagination. The only regret tha 
was felt was in havirg no opportunity for hearing XN

25, in G




G. W. 


RING


THE


1921 7


OXFORDPachmann came to see us, and again to say ‘good-bye

October 15; he played as delightfully as ever. The 
frst of a series of eight Subscription Concerts took place on 
(October 22 at the Town Hall, when Sir Hugh Allen 
conducted some forty or fifty members of the London 
Symphony Orchestra, giving a most enjoyable concert. 
The programme included Beethoven's Pianoforte Concerto 
inG, Miss Myra Hess playing the solo part, beautifully

On October 25 Hambourg gave an excellent recital at the 
‘own Hall, and as well as showing how absolutely at home 
he is with Beethoven, gave also Ravel’s ‘Jeux d’eau’ and 
Debussy’s ‘ Toccata

On November 4 came the second Subscription Concert in 
the same building, when the Bohemian Czech Quartet gave 
4 fine programme consisting mainly of advanced works, 
the most notable perhaps being Smetana’s Quartet, ‘ Aus 
meinem Leben.’ It was a mistake for these gentlemen to 
have so much altered the arrangement of the printed 
programme without notice after the eleventh hour, thus 
causing disappointment to the more musical part of the 
audience who had gone to some trouble to procure the scores




SOUTH WALESlooked for to maintain a permanent Welsh National 
Orchestra

The Cardiff Chamber Music Society held its third 
concert of the season at the hall of the High School for Girls, 
on the evening of December I. Miss Jelly d’ Aranyi (violin) 
and Mrs. Ethel Hobday (pianoforte) were the joint 
exponents of the Sonatas of César Franck, Dohnanyi 
(Op. 21), and Beethoven (Op. 30, No, 2

On December 2, the Albert Hall, Swansea, was crowded, 
the occasion being the appearance of M. Alfred Cortét and 
Miss Vera Horton at the last of the Swansea Subscription 
Concerts. M. Cortét’s playing of Chopin’s twenty-four 
Preludes was, as always, a memorable experience




YORKSHIRE 


LEEDS


58 THE


19


SHEFFIELD


__


OTHER YORKSHIRE TOWNS


THE


THE VIOLS IN ENGLAND


192! 59AMSTERDAM

The Beethoven Festival may be said to have been on the 
whole a great success. At the time of writing there is only 
one more concert Cue, which will be devoted to the master’s 
ninth Symphony. The work done by the management of 
the Festival is deserving of nothing but the highest praise. 
Indeed the arrangement of the programmes of the fourteen 
chamber music concerts was almost exemplary, and it may 
be doubted whether a better survey of Beethoven’s creations 
If the Pianoforte Sonatas were 
not found to be represented, this was because they cannot 
really come under the denomination of chamber music proper. 
Moreover of late years we have had various opportunities

circumstance was entered in the Lord Chamberlain’s Records | on which the whole of them have been heard, so that this

The only two works one

real 
Beethoven's refusal to 
of his unrelenting self-criticism. 
wished to hear along with the others were the 
Op. 29, and the Septuor. A 
concerts, however, lay in the fact that

hav

mins

was only slightly perceptible. 
the ot 
we especially the characteristic ‘ Beethoven 
contrary to popular opinion, this 
an innovation of 
ertainly one of the cheraux di 
Mannheim orchestra, Mozart, 
ut splendid band, has testified to this 
of it himself in his * Nozze di 
The Budapest (Quartet went 
in the underlining of this 
rheir performance seemed to be based on 
If in this respect they

dynamic

regarded as a liberty. It 
five orchestral concerts failed 
Indeed, they would have 
pardonably dull if the situation had not been saved 
MM. Egon Petri and Leonid

Ilona Durigo. It was 
Mengelberg that 
Beethoven. Seldom 
unbounded and 
on this o As if by magic 
orc} to have splendid 
i i while M. Zimmermann surpassed all his 
nances of the solo part in the Violin Concerto

S ywe 
they secured by pl 
p nti n ‘ 1 rate b 
matter for regret that the first




W. HARMANS


PARIS 


REVIVALS


AN INDIAN OTELLO


GEORGI


ROME


LOHENGRIN


HOW * AME TO ITALY


THE MUSICAL


TIMES


61


3—TANUARY


PLAIN SPEAKING


THE AUGUSTEO Le festia de Varaignée’ .. 
‘Tristan and Isolda’ (P relu le, and death 
of Isulda) see

An extremely regrettable ond annoying incident threatened 
to ruin this second concert. For some reason best known 
to themselves, the employees of the electric-light station 
felt agerieved, and, with diabolical malice, arranged to 
strike at the critical hour on Sunday afternoon when all the 
public spectacles were in full swing. Their plan succeeded 
ouly too well, and at 5 p.m. the electric light failed in all 
parts of the city. In the Augusteum, Beethoven’s seventh 
Symphony had reached the last movement, and naturally

for strings

erected to

for 
Manfredini 
Beethoven 
Sinigaglia

Roussel




LEONARD PEYTON, 


VIENNAVery little that requires comment has occurred here since 
my last notes. Two performances of ‘ Parsifal’ were given 
at the State Opera on October 31 and November 1, with 
These performances were 
principally for the splendid work of Frau. 
guest from the LBerlin State Opera, as 
During her stay here this artist also appeared 
in the ‘Ring’ (Briinnhilda) and ‘Fidelio’ (Leonora). 
Another guest of note has been Cornelius Bronsgreest, als» 
of Berlin, who sustained with success the parts of Rigoletto 
and Papageno. He has also heen heard in ‘Madame 
Butterfly,’ ‘Carmen,’ and ‘The Huguenots.’ On 
November 29, performances were given of Korngold’s 
two operas, ‘The Ring of Tolykrates’ and ‘ Violante,’ 
under the direction of the composer

We promised a Beethoven Festival for the second 
week of December. The scheme comprises a number of 
concerts and a performance of * Fidelio’ at the State Opera. 
The principal concert will be held on December 12 in the 
Belvedere Palace in the style of the time when Beethoven 
was living and composing at Vienna, and will be under the 
{ Dr. Schalk, of the Opera

Puccini’s three one-Act pieces are apparently a 
here, and performances are being given once every week. 
S. WINNEY




THE H. W. GRAY CO., NEW YORK. 


YORK. 


VICE


THE


1921


TRANSCRIPTIONS FOR PIANO OF HARPSICHORD 


PIECES). 


_ COURANTI 


‘ LE BAVOLET FLOTTAN1 


;, GIGU! 


6 SARABANDE


3 LA BERSAN


T. HILTON -TURVEY


FIRST SONG ALBUM


A MAY- DAY LILT


STARS ON THE SEA


FOR LOVE AND YOU


SLEEPY - BYE SONG


HER CHEEK IS LIKE THE TINTED 


ROSE


IF JUNE WERE MINE


WHEN THE LONG LANE TURNS


LAMIA


SYMPHONIC POEM 


FULL ORCHES?7 RA


DOROTHY HOWELL


THE


MUSICAL


CANTATAS FOR LENT._


LAST NIGHT AT BETHANY


WORDS WRITTEN AND COMPILED BY 


JOSEPH BENNETT


LEE WILLIAMS


IN THE


GARDEN


TENOR


SOPRANO


AND BARITONE SOLI 


AND CHORUS


THE WORDS WRITTEN AND SELE(C


VIOLET


ECTED 


CRAIGIE


THE MUSIC


HALKETT


MPOSED BY


FERRIS TOZER. 


THE CRUCIFIXION 


A MEDITATION


HOLY REDEEMER | 


THE WO! SELE WRITTEN BY 


W. J


SP ARROW SIMPSON, M.A


SET


ST


SACRED PASSION


TED


AND


AINER


THE DARKEST HOUR 


TENOR ‘AND BARITONE SOLI


AND CHORUS 


BE SUNG ‘BY THE CONGREGATION 


SELECTED, AND


HAROLD M


SOPRAN


HYMNS TO


THE W


USIC COMPOSED, BY


MOORE


THE CROSS OF CHRIST


THE HOLY SCRIPTURES, INTERSPERSED 


PRIATE HYMNS, BY 


W. MAURICE ADAMS


APOSED BY


WITH A


THOMAS ADAMS. 


3S


LONDON


NOVELLO AND COMPANY


DESERT AND IN THE


OLIVET


AT THE


GETHSEMANE 


THE WORDS WRITTEN AND COMPILED BY 


JOSEPH BENNETT 


THE MUSIC COMPOSED BY


C. LEE WILLIAMS


THE STORY OF C ALVARY 


FOR TENOR AND BASS SOLI AND CHORUS


THE WORDS SELECTED AND WRITTEN BY 


ROSE DAFFORNE BETJEMANN


THE MUSIC COMPOSED BY 


IS IT NOTHING TO YOU 


AN EASY CANTATA 


E. V


BY


HALL, M.A


VIA


DOLOROSA 


A DEVOTION


FOR BARITONE SOLO AND CHORUS 


THE WORDS DERIVED MAINLY FROM ANCIENT SOURCES 


THE MUSIC COMPOSED BY


CUTHBERT NUN 


TO CALVARY


FOR


TENOR AND BARITONE SOLI AND CHORUS 


INTERSPERSED WITH HYMNS TO BE SUNG BY 


1E CONGREGATION


THE WORDS SELECTED AND WRITTEN DY 


SHAPCOTT WENSLEY


THE MUSIC BY 


MAUNDER


J. H. MAUNDER


FOOT OF THE CROSS


FOR SOLI, CHORUS AND ORCHESTRA


COMPOSED BY


ANTON DVORAK


LIMITED


MUSICAL


THE


VARY 


‘HORUS


MANN


4S. 


YOU


US 


SOURCES


LRY


ORUS 


UNG BY


CROSS


ROWE


1921


MUSIC FOR LENT


HE PASSION OF OUR LORD. 


#THE PASSION OF CHRIST. G. F. HANDEL. 


R VOICES AND ORGA


THE WORDS BY 


THE REV. E. MONRO 


SET TO MUSIC BY


THE REPROACHES


SET TO MUSIC BY


THE STORY OF THE CROSS THE OFFICE OF


THE BENEDICITE


SET TO MUSIC BY THE FOLLOWING COMPOSERS


NBY, 


TENERR


AND


DIRECTIONS FOR SINGING 


“THE PASSION


FOR USE IN THE CHURCH OF ENGLAND


EDITED PY


CYRIL MILLER


66


MUSIC FOR LENT. EASTER


AND OTHER SEASONS


BY 


SERVICES. 


THE STORY OF THE CROSS


ANTHEMS. 


LEAD ME IN THY TRUTH 


THE LORD IS MY SHEPHERD 


PENITENCE 


PEACE


J. H. MAUNDER


1-95


THE SEASON’S SUCCESS


CREATION’S PRAISE.” 


OLN 


ALSO 


“CHRIST THE CONQUEROR.” 


HUNTING SONG


COMPOSITIONS


JOHN GERRARD WILLIAMS


PIANOFORTE SOLO


SONGS. 


PART-SONGS. 


FAIR, SWEET, CRUEI (S.A. T.B. ) 


(S.A. T.B. ) 


THREE SLEEPS. (S.A.1T.B


VOL. IV


NOVELLO’S CLASSICAL 


THIRTY SONGS


BY 


VARIOUS COMPOSERS 


NOTATIONS


SONGS


BOTH


“SHEPHERD OF MEN 


CANTATA BY JOHN S. WITTY. 


ALSO


THE SCENE ON CALVARY” 


ANOTHER FINE PRODUCTION


SUN OF MY SOUL


THE GENTLE SHEPHERD


DIVINE LOVE


THE LORD IS MY SHEPHERD


LEAD, KINDLY LIGHT


THE NAZARENE


THE PROMISE OF PEACE


JOSEPH ADAMS MUSIC PUBLISHING CO


STABAT MATER


GIOVANNI BATTISTA PERGOLESI


FOR


ENGLISH VERSION 


BY 


4 (WACHET, BETET) 


+ -ANTATA 


; FOR SOLI, CHORUS, AND ORCHESTRA 


J. S. BACH


AVELING


SONGS


SONGS


67


CHRIST LAY IN DEATH'S 


DARK PRISON


CHRIST LAG IN TODESBANDEN


AN EASTER CANTATA


FOR SOLI, CHORUS, AND ORCHESTRA


COMPOSED BY


J. S. BACH


THE


PASSION OF CHRIST


SET TO MUSIC BY


EDITED BY


EBENEZER PROUT


ABRIDGED FOR CHURCH USE BY TH+. 


REI. JAMES BADEN POWELL


UNISON SONGS. 


PIANOFORTE MUSIC. 


CHORAL RH APSODIES. 


CHURCH MUSIC. 


BERNERS STREET, LONDON, W. 1


VOL. I


BY


EMILE JAQUES-DALCROZE


TWO PASTORALS 


“COME YOU, MARY.” 


“WHO WOULD SHEPHERD


PIPES FORSAKE ?” 


NORMAN GALE, 


BY


BRISTOW


ERNEST FARRAR


TWO KEYS


THE ORGAN


ITS PRINCIPLES


O3


AND PRACTICE


PART I


HERBERT F. ELLINGFORD


HIAWATHA’S WEDDING-FEAST 


A SELECTION 


ORGAN 


S. COLERIDGE-TAYLOR. 


ARRANGED AND


HUGH


DAPTED BY 


BLAIR


HUBERT


PARRY


(SET 12


JOHANN


BACH


SEBASTIAN


BACH


OUR CATHEDRAL ACRIBELLE IS A GENUINE 


SUPER - QUALITY SILK STRING


ET


T H E ” D ] S C 


REG


ORGAN BLOWER


SOLE MAKERS OF THE “DISCUS’ BLOWER 


VIOLIN AND VIOLONCELLO WORKS


WORKS OF THE OLD MASTERS


__ 


THE PRICE OF BOTH EDITIONS IS THE SAME


JUST PUBLISHED


91—NOVELLO’S MUSIC PRIMERS


SYSTEM 


MUSICAL NOTATION 


H. ELLIOT BUTTON


SIR EDWARD ELGAR


EXTRACT FROM INTRODUCTION. 


WORKS BY IGOR STRAVINSKY 


PUBLISHED BY 


J. & W. CHESTER, LTD


PRIBAOUTKI (CHANSONS PLAISANTES


QUATRE-CHANTS RUSSES


BALLET WITH SONGS IN ONE ACU. ARRANGED FOR 