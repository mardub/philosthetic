


 XUM 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 PUBLISHED 


 SOCIETY 


 ELEVENTH SEASON 


 URGH 


 SECOND CONCERT , WEDNESDAY , NOVEMBER 23 


 BAND CHORUS 1.000 PERFORMERS . 


 10 


 UILDHALL SCHOOL 


 HURCH SOLO - SIN IGING , INTONING . , 


 CHORISTERS . 


 


 NOVEMBE 


 MONTH 


 CHOIR 


 JOL UNTEE R 


 - CLASS SOLO TENOR 


 546 


 PROFESS 


 ONAL NOTICES 


 MISS MARIE 


 MRS . 


 MISS HEL D N SWIFT 


 MADAME L OU ISE 


 MISS LOUISA BOWMONT 


 MR . THO ) LAS OLDROYD 


 MR . HERBERT PARRATT 


 MR . JOHN JAS . SIMPSON 


 MR . J. BINGLEY SHAW 


 N USIC SCH OOL.—CHURCH ENGLAND RD , ¢ : E.C .. 3 


 548 


 TRINITY COLLEGE , LONDON 


 HIGHER EXAMINATIONS MUSIC . 


 CLASSES LECTURES . 


 CHAMBER MUSIC COMPETITION , 1881 


 PRIZE ESSAY MUSICAL SU BJECT , 1881 . 


 SACRED MUSIC COMPETITION , 1881 . 


 RINITY COLLEGE CALENDAR 


 


 ISSI 


 NOVELLO , EWER & CO . MUSIC PRIMERS 


 J. FREDERICK BRIDGE 


 SHILLINGS . 


 PAPER BOARDS , SHILLINGS SIXPENCE , 


 ORCHESTRAL SCORES 


 PUBLISHED 


 NOVELLO , EWER CO 


 SPOHR 


 HANDEL 


 HANDEL MOZART 


 


 HAY DN- 


 “ MENDELSSOHN 


 SIR W. S. BENNETT 


 MUSICAL TIMES 


 PUBLISHED MONTH ) 


 SCALE TERMS ADVERTISEMENTS 


 R ! MUSICAL DIRECTORY , 18$82 


 SEVES 


 | IVES ’ MUSICAL DIRECTORY , 1382 . — 


 ST ANDARD ” 


 SUMMER NIGHTS 


 ( LES NUITS D’ETE 


 SONGS THEOPHILE GAUTIER 


 HECTOR BERLIOZ 


 N USICAL DIRECTORY , 1882 ( REEVES ’ ) . — 


 82 


 XUM 


 


 549 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 LISZT SEVENTIETH BIRTHDAY , 


 OCTOBER 22for length time maintain vital power intact 
 | aid daughter , Music , turn 
 foster - mother , events 
 called capacity time 

 infinitely , , regretted 
 , predominant influence party , 
 increased immoderately glittering 
 pomp Church , disdaining admit 
 theatrical elements , — i.e. , 
 theatrical superficial excrescences — found 
 way music service . 
 Fesuitical style recognisable music ; 
 taste formed eternally true 
 classical art discern Beethoven 
 Grand Mass , Mozart Requiem , fact 
 seventeenth century opera in- 
 vaded Church , strangely fastidious 
 impersonifications saints time like- 
 wise reflected character Church music . 
 case Germany 
 countries inhabited Latin races , 
 known visited Italy 
 hear latest operatic airs resounding 
 organ majestic dome St. Peter 

 550 MUSICAL TIMES.—Novemper 1 , 1881 




 CRITICAL EXCURSIONS . 


 XUMstringing unconnected themes , phrases , 
 rhythms , flourishes ( I‘loskeln ) , taker 

 singly uninteresting crude , joined 
 soi - disant impression nun- | 
 chalant musical improvising , nay delirious raving ; 
 listening ‘ Humoreske ’ 
 lips question , meaning 
 , ‘ humour ’ ? 
 seriously striving musician , unfortunately began 
 compose Beethoven ceased , 
 induced dim feeling necessity 
 going far possible way all- 
 crushing symphonist paths , feeling , 
 help secing , lead 
 attempts — weak beginnings . 
 smaller compositions , 
 numbers ‘ Carnaval , ’ attain perfectly 
 adequate musical expression respective 
 ‘ programmatical ’ superscriptions . larger 
 works , sonata form abandoned , 
 look vain comprehensive programme 
 corresponding proportionately size , 
 , indication head 
 poetico - artistic subject treated 
 . Nay , abandonment sonata 
 form , classical fundamental form , 
 undertaken consequence self - conscious artistic- 
 ally thoughtful reflection hard - pressed musician ; 
 appear act nebulously 
 indefinite , blindly groping caprice , 
 went sonata symphonic form soon 
 Mendelssohn , respect certainly 
 far superior Schumann , courageously set 
 example 

 Let pause moment consider 




 1831 


 S12 


 TI 


 552earnestness energy , dis- 
 posal intelligence skill , hardly created | 
 themes pieces music , works | 
 art 

 begin crowning assertion 
 heap preposterous statements ; 
 insignificant seed - corns grandest | 
 famous chefs - dceuvre sprung ! | 
 notion “ ideas fit 
 existing forms , forms discovered , ” 
 conceived endowed reason 
 problem recommend solution | 
 ingenious reader . , help asking : 
 Supposing Schumann able produce 
 fancies ( Einfille ) , wiser 
 accept spurn ? com- 
 poser aphoristic thoughts - lined sketches , 
 loosely fantastically strung 
 , raison d’étre reflections , 
 maxims , epigrams , caractéres , Xenicn , pensieri , & C. , 
 La Rochefoucauld , La Bruyére , Pascal , 
 Chamfort , Vauvenargues , Goethe , Schiller , Leopardi , 
 enriched literature ? ‘ sure , 
 inferior kind art , minor 
 branch art , compared works 
 developed reasoning , - membered , artisti- 
 cally constructed organisms . , flashes 
 intellect , iridescences fancy , 
 throbs heart , furtive peeps human 
 life character , far worthless 
 despicable . , type 
 beauty , type 
 highest degree perfectness admissible ? 
 narrow - minded narrow - hearted partisan 
 Joseph Rubinstein expected 
 sympathies . 
 Schumann , , exception , composers 
 appeared world time . 
 Brahms , Raff , Goldmark pelted , 
 utmost mud dirt gamin | 
 critics . Haydn , Mozart , Beethoven , Mendels- | 
 sohn treated somewhat respect : 
 , , cards plaved|a 
 good effect - mentioned smaller | 
 ones , swept table soon | 
 ace trumps , Wagner , turns . 

 Beethoven merely pedestal 1 th 

 master masters rises majesty sublimity . | 
 listening Joseph Rubinstein strictures shall | 
 mind rejects purely 
 instrumental music , absolute programme 
 music , advises world long 




 XUM 


 553above - quoted weak trivial Italian song 

 , weak trivial . 
 note , 2 minor instead major interval , 
 setting words * * Ingemisco tanquam reus ’ 
 Mozart * * Requiem ” added 
 example kind . rosalias occur com- 
 paratively rarely form . Oftenest 
 repetition , frequently sequence 
 lower pitch modeis changed . Handel * * 
 beautiful feet ” ( bars 7 8) 
 “ Messiah ” furnishes example phrase repeated 
 , tone higher , mode ; Beet- 
 hoven overture ‘ ‘ Coriolan , ’ Op . 62 ( bars 15 , 
 & c. ) , passage repeated , tone lower , 
 mode ; composer * ‘ Sonata 
 Appassionata , ” ’ Op . 57 ( beginning 
 movement ) , passage repeated , tone lower , 
 different mode . 
 remind Beethoven notable com- 
 posers effective use rosalias . 
 impressive weird 
 mighty upheaval Heroic 

 position takes place C minor C sharp minor ! 
 patent student master 
 repetition carried farther 
 composer . persistence 
 fasten melodic rhythmic motive 
 monotonous best learned 
 symphonies — mention especially move- 
 ments C minor Pastoral , 
 scherzo Ninth Symphony . Heroic 
 Symphony chief phrase subject 
 appears place successively ( Joseph Rubinstein 
 rosaliter ) E flat , F , D flat , B flat , 
 E flat ( key times ) . 

 Beethoven confine transpositions ; 

 introduces freely wonderful effect repetitions 
 pitch . C minor Symphony ( bars 
 63 , & c. ) - bar phrase occurs times im- 
 mediate succession 

 place question force expres- 
 | siveness iteration transposition clear light 
 | | better illustrate examples drawn 
 | trom master highly esteemed conservative 
 } radical , recognised 
 ; mere composer , contriver 
 | euphonious combinations sounds — short , 

 true tone - poet . , , follows additional 
 number rosalias ( taking word sense 
 | comprehensive defini- 
 [ tions quoted ) literal repetitions 
 pitch culled works Beethoven 
 written different periods life 
 representative styles 

 Sonata , Op . 2 , . 1.—The phrase 
 } second subject movement com- 
 | pletely partially repeated . working- 
 |out section phrase appears addition 
 |same number repetitions twice tone higher , 
 |after melody taken bass . 
 } second half subject contains likewise repeti- 
 [ tion - bar phrase , codas 




 554and abusive appellation ) Scherzo 
 sonata opens mind beginni 
 Andante favori 

 Speaking lesser pieces Beethoven piano- 
 forte works , reader attention | 
 certain rosalias Bagatelles ( Op.33 ) . 

 nw 
 




 JJ Heroic Symphony ( movement , bars 
 134 , Xc . ) - bar phrase appears 
 E minor transposed minor . 
 passage occurs fF E flat minor . | 
 help enraptured ethereally | 
 lovely rosalia ? think possible 
 born woman insensible | 
 proof charm playful rosalia | 
 beginning Allegretto scherzando Eighth | 
 Symphony ( major ) . transposition 
 D mysterious opening passage Ninth 
 symphony , effect power descrip- 
 tion : justice great 
 choice accumulation adjectives 

 pointed theorists 
 musicians generally variance 
 rosalia . , 
 accepting comprehensive defini- 
 tions , shail escape 
 brought face face puzzling question . Tor 
 instance , second half | 
 bars Mozart G minor Symphony rosalia , | 
 notwithstanding modifications intervals | 
 melody total change harmonies ? 
 , thename lose opprobriousness ; 
 opprobriousness based monotony 
 thing called , monotony exist 
 variety . defence rosalias holds 
 good case repetitions melody | 
 pitch , different accompaniment | 
 slightly altered cadence ( Beethoven string 
 quartet , Op . 127 — beginning Allegro ) . Nay 
 mere change cadence 
 produce desirable variety . 
 common procedures composers begin second | 
 clause musical sentence like , giving | 
 end different turn . addition | 
 examples point found notes | 
 Beethoven works , instead thousands 
 let following suffice . Beethoven | 
 Sonata , Op . 10 , . 3 , movement , subject ; 
 Sonata , Op . 22 , movement , subject ; | 
 Symphony major , subjects Vivace 

 romanticism 




 CLEMENT MAROT HUGUENOT 


 PSALTER . 


 VI . 


 XUM 


 PSEAV . CXXXIV 


 PSEAV . XXXVI 


 PSEAV . XLII 


 IQ 


 PSEAV . XXV 


 MUSICAL TIMES . — NOVEMBER , 1881 


 ARAL ASO ER 


 XUM 


 MUSICAL TIMES 


 NOVEMBER , I88I 


 25 . “ 1539 


 1542—1549 


 ( 1539 


 ANNI 


 2542 ~~ 37 


 20 . 2551 . 55 . 1562 . 


 21 . 1551 . 56 . | 1562 . 


 23 . 1543 . 59 . 2562 . 


 100 


 101 . 


 102 . 


 103 


 104 


 1543 


 215512 


 1562 


 1562 


 1543 


 1562 


 1562 


 1562 


 1562 


 1562 


 1543 . — 1551 


 1551 


 1562 . 


 1562 


 1562 


 1562 


 1543 


 1551 


 1562 


 105.| 1562 . 


 106 . | 1562 


 107 


 1543 


 1562 


 100 


 110 


 111 


 112 . 


 L135 


 150 


 ( 1551 


 1562 


 1542 


 1551 


 1562 


 1562 


 1562 . 


 1562 . 


 1562 . 


 1562 . 


 1562 . 


 1562 


 154 ? -- 1549 


 GREAT COMPOSERS , SKETCHED 


 . 


 XUM 


 560 


 17His passion Gluck survived 
 lapse vears . 
 12 , 1856 ) says 

 , shall forget artistic 
 instinct , hesitation , recognised 
 adored transport , , new genius . 
 Yes , yes , depend , men half- 
 feeling half - science , 
 heart single brain lobe , , 
 great superior gods art , Beethoven 
 Gluck . ‘ reigns infinitude thought , 
 infinitude passion ; , 
 strong second , never- 
 theless 
 Jupiters single divinity , 
 admiration worship ought absorbed 

 23 , 1856 , find master writing 
 friend Morel , entreating good offices Louis 
 Berlioz , desired leave imperial navy 
 enter merchant service . letter contains 
 reference state health , 
 beginning end 




 XUM 


 XUM 


 


 561 


 239 1856 


 1863 


 TY 


 XUM 


 SCHUBERT SYMPHONY 


 563 


 1828 


 1828 


 1037 


 XUM 


 XUM565 

 s ‘ Lauda Sion ” ( E natin , frag- 
 ments “ Christus , ” music “ Athalie , ” | 
 Haydn “ Seasons , ” Spohr ‘ Calvary , ’ Beethoven 
 Mass D “ Mount Olives ” ’ ( original 
 libretto ) , Bach “ St. Matthew Passion , ” Dr. Crotch ’ s 
 ‘ ‘ Palestine , ’ Benedict * Legend St. Cecilia , ” 
 Costa * “ Eli ’ ? ‘ ‘ Naaman , ” Macfarren ‘ John 
 Baptist , ” Rossini ‘ * Moses Egypt . ” 
 scarcely expected Institation 
 perfectly organise -d Sacred 
 poe | content merely appealing : 
 1e publ ic support series concerts yea 
 preach cause high - class sacred music out- 
 , inside , walls Exeter Hall 
 anweh duty , triennial Festival honour ot 
 Handel legitimate offspring self- 
 imposes task . Gradually , , idea 
 developed ; centenary composer 
 death ( 185 g ) , performance works 
 contemplated ( chiefly suggestion 
 indefatigable ‘ Treasurer , Mr. R. K. bi wh ) , pre- 
 liminary Festival held years , 
 established fact , powwestal aid 
 Sacred Harmonic Society , experienced 

 Mendelssohn 




 566 MUSICAL TIM 


 NORWICH MUSICAL FESTIVAL . 


 SPECIAL CORR!SPONDENT 


 XUM 


 XUM 


 567 


 - , PDE } 


 EE . 


 568 


 HUDDERSFIELD MUSICAL FESTIVAL . hare creditable manner . trio , * Lift thine eyes 
 bey Madame Albani , Miss Davies , oe Madame Pater , 
 created marked effect . accompaniments band 
 organ finely played 

 evening miscellaneous Concert given . 
 chief features magnificent playing band 
 Overture ‘ “ ‘ Oberon , ” Beethoven ’ s Symphony 
 major , Overture ‘ ‘ Tannhauser , ” Valse lente 

 Wil 




 XUM 


 XUM 


 LUX 


 569 


 CRYSTAL PALACE 


 57 


 RICHTER CONCERTSing thing young composer . claim 
 score juvenile authorship , uncom- 
 promising pretensions rank chief kind ; 
 largely developed , ambitious style character , 
 rigidly observant classic form , redundant matter , 
 Redundancy , point fact , certain 
 reckless daring , principal failings . movement 
 contains , episodes expanded till 
 co - ordinate principal themes ; harmonic 
 wealth profuse melodic . exuberance 
 surprises , certain extent gratifies ; Concerto 
 loses point clearness , order , con- 
 ciseness prevent saying word 
 necessary logical dialectic complete . 
 ness . expected , music , 
 things , ‘ youth served . ” , , 
 recognise mastery resources art — 
 resources alike fancy expression — 
 rarely met young . details 
 work purposely refrain going . heard 
 description judgment war- 
 ranted ; little said Mr. D’Albert success 
 fact desire hear strong . Concerto 
 admirably played regards orches stra , solo 
 gained hands stronger performer 
 Mr. D’Albert , e : cecuted better 
 instrument chosen occasion . com- 
 poser loudly applauded movement , 
 times recalled close , amid genuine excite- 
 ment 

 Beethoven Choral Symphony ended Concert , 
 generally speaking , rendered better style 
 occasion , chorus thoroughly 
 work singing faultlessly . solos , intrusted 
 artists named , heard greater 
 advantage . Saturday concert included programme 
 3eethoven “ Eroica ’ ? Symphony selection 
 works Richard Wagner 

 




 BRIGHTON AQUARIUM CONCERTS . 


 XUM 


 XUM 


 571First Symphony , Massenet 
 ; added “ ‘ arrange- 
 ment ” strings little piece entitled * * Traumerei ” ’ 
 Schumann ° « s Kinderscenen ” — tho ugh pretty 
 trifle presented save composer 
 wrote hard discover . Familiar works 
 , need touch char acter per- 
 formance Mr. Corder small - selected orchestra , 
 E piece played great care , style 
 spo ! se skill executants 
 ability conductor . Pract , 
 ide limited number instrume : nts . ote 
 aiteentoesn conservatory turned place fron 

 certo , Beethoven 
 Scenes Pittoresques 

 d good concert - room , ¢€ net . n 
 iinutive orchestra { s pretty nearly 

 o 

 concerto ably executed Mr. Oscar B eringer , 
 wh sympathies aid executive ability overcoming 
 difficulties modern music . Vith Beethoven 

 sentative noblest — oi 




 LYCEUM THEATRE 


 MUSIC MANCHESTERal Hall sth ult . , executants bei ng Messrs 

 Speelman , Bernhardt , ae 
 programme consisted Quartets Mozart B flat , 
 Raff D minor , Mendels nin E minor . Raff 
 fine unequal work d . 
 performance throt ighout m « st admirable . Mr. 
 Hallé gave Cc incert season 27th ult . 
 programme included Mozart Symphony ( . 1 ) 
 D , Brahms Academical Festival Overture , Beethoven 
 Pianoforte Concerto C , Schumann * Phantasie- 
 stiicke . ” vocalist Madame Schuch Proska , 
 Dresden.——During season , addition choral 
 works announced month , following instrumental 
 novelties probably introduced : Brahms Tragic 
 Overture , Berlioz ’ * * Romeo Juliet , ’ Symphony 
 Overture ‘ * Waverley , ’ ” Mozart Ballet - music ‘ Ido- 
 meneo , ” Gluck Ballet - music “ Paris Helena , ” 
 Raff Symphony , “ m Walde , ” ’ Dvorak Danses Slaves , ” 
 Cowen Overture ‘ “ ‘ Niagara , ” Rubinstein Second 
 Concerto 

 




 572MUSIC LEIPZIG . 
 Leipzig , October 21 

 musical associations hold highest 
 place Leipzig , lesser — Euterpe Concerts 
 — begun ; famous series 
 performances place weekly Gewand- 
 haus given . great boast 
 Gewandhaus directors orchestra reserved 
 strictly single end , hardly members 
 suffered degrade artistic training occasional 
 engagements lighter kind . Res severa est verum gau- 
 dium inscription cornice hall , 
 gives note character performances . 
 Gewandhaus considered sort temple , whereof 
 ministering priests things kept pure , 
 , unfortunately , initiated equally 
 select . , owing smallness building 
 renown concerts , absolute im- 
 possibility new comers sure place ; 
 hardly aseat subscribed years , 
 vacancy probably patient 
 applicants . Consequently rehearsals , place 
 early hour morning , crowded 
 students Conservatorium unlucky 
 multitude concerts closed . 
 Distinguished Euterpe management , inclines 
 works Liszt , aim Herr Reinecke 
 preserve Gewandhaus Concerts definitely 
 classical character . performances hitherto 
 given included Haydn Oxford Symphony , Beet- 
 hoven Pastoral Symphony , Second ( D major ) 
 Symphony Brahms — work , repeated hearing , 
 assures author right held 
 successor Beethoven massive strength , 
 mastery orchestral body , creation 
 solid manipulation inspired melody . Herr Reinecke 
 , , sought prompt acknowledg- 
 ing worth new compositions stamp 
 artistic workmanship . Accordingly , classical 

 deviated high character producing 
 English glees . popular estimation weekly perfor . 
 mances Bach church , St. Thomas , retain 
 position ; excel purity precision 
 , instance , wonderful motett , ‘ Spirit 
 helpeth infirmities ” ( ‘ Der Geist hilft unsrer 
 Schwachheit auf ’’ ) , rendered Saturday . 
 choir management learned editor 
 Bach - Gesellschaft , Wilhelm Rust 




 XUM 


 XUM 


 O1 


 9—11 


 OICE 


 2102 - 6 9\5 


 NOVELLO , EWER 


 TENOR 


 ANTHEM YOR 


 SOLO CHORUS 


 TENOR 


 CO 


 CHRISTMAS 


 SOLO — — 


 23 


 — - EE 


 ED 


 XUM 


 CUM 


 XUM 


 227 


 XUM 


 9 - 4 


 XUM 


 DROP , YE UEAVENS , 


 8/788 8 


 XN ! 


 =] PRE SES 


 580 MUSICAL TIM 


 METZLER & COV 


 LIST 


 MUSIC 


 CHRISTMAS 


 READY 


 2 . EARLY MORNING . 


 3 . LOWLY BETHLEHEM MANGER . 


 4 . SLOWLY FALL SNOW - FLAKES 


 ETHLEHEM . 


 CHRISTMAS HYMN . 


 SHEPHERDS NIGHT - WATCH KEEPING 


 TARK ! 


 H ( MENDELSSOHN 


 CHRISTMAS CAROL , ‘ SING . JOY- 


 ITTLE SONGS LITTLE VOICES . 


 OLY 


 HERALD ANGELS SING 


 RICH 


 OFFE RINGS , RARE . 


 BUNCH COW SL IP “ 3 


 RIENDSHIP 


 QUEENIE 


 THOLD TOURS ’ NEW ARRANGEMEN TS 


 » 2 . REVE CHARMANT « GASTON DE LILLE 4 0 


 METZLER 


 CO 


 35 , 36 , 37 , 38 , GREAT MARLBOROUGH STREET , LONDON , W 


 XUM 


 XUM 


 581 


 XUM 


 XUM 


 583vice held St. Mark Church , Camberwell , 

 Festival Harvest Thanksgiving . ordinary 
 choir augmented gentlemen neigh- 
 bouring choirs . Magnificat Nunc dimittis 
 sung Banks setting E flat , anthem 
 Dr. Stainer ‘ * Ye shall dwell land , ’ 
 blessing Sullivan Te Deum D sung , choir 
 grouped round altar . Mr. Alfred Physick , 
 organist choirmaster church , presided 
 organ , played concluding voluntary Beethoven 
 “ Hallelujah 

 Harvest Home Concert given Thursday , 
 73th ult . , Claremont Chapel Schoolroom , 
 members friends King Cross Senior Band 
 Hope . Gounod * Ave Maria ” chief item 
 programme . Songs given Misses Ethel 
 Harwood , F. Davies , M. Tensh , Messrs. H. T. Probert , 
 A. Probert W. H. Mason , varied pianoforte 
 solo Miss Willcocks ( accompanied 
 evening ) , duet piano harmonium , violin solos , 
 selections orchestra , received 
 numerous appreciative audience 




 584A series Concerts given Kilburn 
 Musical Association , direction Herr Adolph 
 Gollmick , Town Hall , Kilburn , coming 
 season ; , Wednesday , December 14 , 
 devoted Handel “ ‘ Messiah 

 Mr. WaLter Bacur Pianoforte Recital takes place 
 St. James Hall - day . programme selected 
 works Beethoven Liszt , includes 
 master ‘ ‘ Mephisto - Walzer . ” Mr. Bache 
 Orchestral Concert announced February March , 
 1882 , programme consisting Liszt ‘ Goethe . 
 Marsch , ” “ ‘ Mephisto - Walzer ” ’ ‘ “ Faust ’ Symphony 

 Concert given roth ult . Mission 
 Room St. Saviour , Fitzroy Square . Solos sung 
 Miss Filmore , Mrs. Frisby , Mr. Suter , Mr. W. ) , 
 McLaren . Miss Nellie McEwen contributed 
 songs effect . glees rendered 
 members choir St. Edmund King 
 Martyr , Lombard Street 




 XUM 


 XUM 


 585 


 REVIEWS 


 MIphous Canon ” particularly happy ; final 

 chapter , headed ‘ ‘ Hints Student , ” ’ contains 
 valuable rules construction species com- 
 position . impossible speak highly judi- 
 cious manner examples selected 
 work . needless 
 taken works John 
 Sebastian Bach ; extracts com- 
 posers , case aptly enforce special 
 point , deepest interest , 
 effect strongly drawing attention 
 pupil compositions quoted . 
 especially mention Beethoven Sonata , 
 Op . 28 ‘ popularly known ‘ Sonata Pastorale ” ) , 
 passed mere “ finger pianists ” 
 thought passage fine 
 example Double Counterpoint . canons 

 lfil 




 586We approach study “ ‘ Nirwana ” re- 
 spect composition eminent 

 music help pupils sing 
 pianoforte service ; transcriptions good 
 songs , — provided degenerate 
 finger display — welcomed , 
 useful practice enforce , 
 young instrumentalists acquainted standard 
 vocal works . set Haydn 
 ‘ ‘ Mermaid Song , ’’ Mozart ‘ ‘ Violet , ’’ Beethoven 
 “ Mignon Song ” ( ‘ Knowest thou land ? ” ’ ) . 
 , course , lend equally 
 “ arrangement ” instrument ; Herr Eisoldt 
 acquitted task credit . need 

 scarcely said pleasing accompaniment 

 Mermaid Song , ” apart melodious character 
 theme , render popular number 
 , Mozart beautiful vocal gem attract 
 young players , sufficient variety Beethoven 
 - known song interest hanker 
 ‘ pretty ’ music . little fingering marked , 
 ‘ ‘ Mermaid Song 

 Pianoforte . John 




 XUM 


 587 


 FOREIGN NOTES . 588 

 reference acontroversy recently set foot 
 musical world , Leipzig Signale remarks ironically : 
 “ “ Mozart Requiem said dis- 
 covered ; search symphony , Franz 
 Schubert , unknown . asked , 
 tenth symphony second oratorio 
 Beethoven ? symphony , known , 
 bespoken paid advance London Philhar- 
 monic Society ( Beethoven sent Ninth inits stead , 
 performance Vienna ) ; oratorio 
 asked Gesellschaft der Musikfreunde Vienna , 
 likewise paid : ergo , 
 works ? — R.S.V.P 

 read Le Ménestre ! : 
 Pére Dupin , 
 authors ‘ ninety - years age ’ , 
 comic opera , entitled ‘ Ploch le Soldat , 
 rietti write music 




 MUSICAL TIMES 


 NOVEMBER , 1881net ) ; Kapsodie Hon 

 Beethoven ) ; Suite d’Orchestre ( Ma 
 ration Nouve 

 Ss . Overture , “ Oberon ” ( Weber 

 Leipzig.—Gewandh : : Fest- Ou iverture ( Volk 

 mann ) ; Prelude Fugue , E minor ( taokn } 1 ) ; Pianoforte Con- 
 certo , . 2 ( Sc arwenka ) ; ordanza ” ( Liszt ) ; Pastoral Sym- 
 phony ( Beethoven ) ; Vocal soi ( W sac Kirchner , ae 
 Schumann ) . Ger — idhaus Concert ( Octot 13 ): Overtu - 
 veva ” ( Schumann ) ; Violin Con ( Gad “ og 

 Handel ) ; “ enced , . 2(Brahr cal 




 CORRESPONDENCE 


 MALE VOICE CHOIRS . 


 EDITOR * * MUSICAL TIMES 


 XUM 


 XUM 


 589 


 COLLEGE ORGANISTS.—CONFERENCE 


 ORGAN CONSTRUCTION 


 EDITOR ‘ ‘ MUSICAL TIMES . ” 


 DULCIANA 


 EDITOR “ MUSICAL TIMES . ” 


 754 


 BENEDICT “ LIFE WEBER . 


 ‘ MUSICAL TIMES 


 EDITOR 


 FRIEDRICH WIL ! JAHNS , 


 MT 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWS 


 HTON - UNDE 


 599Bourne , Linco NSHI Violin Pianoforte Recital wa 

 S 
 — Rooms , Angel Hotel , Messrs. 
 Mr. F. Cundy , vocalist . pro- 
 > works Beethoven , Schubert , Mozart , 
 e Recital attended highly 

 given 15th ult . 
 Bertolle Lewis , assis 
 gramme selected 
 Schumann , Gounod , & c. 
 successful 

 nin Colston Hall , largely 
 n principal items programme 
 ( . 5 ) C minor , W eber Overture 
 Euryanthe , Rossini Siege Covint h , W 
 Overture Tanuhduser . yd ‘ ‘ Funeral March Marionette , ” 
 Boccherini Minuet major muted strings , Meyerbeer ’ s 
 Coronation March ( Le Pop included pro- 
 gramme . vocalists w ‘ ere Madame Evans - Warwick , Mr. C , 
 Fredericks , Mr. Waite led band , Mr. Riseley conducted . 
 — Harvest Thanksgiving Service held St. Nicholas 
 Church September 29 , usual choir church 
 augmented choirs Christ Church , Broad Street , St , 
 Peter , Peter Street , numbering altogether voices . service 
 Goss ( unison ) , anthem , “ Behold given 
 herb bearing seed , ” specially written occasion Mr , 
 William Fear Dyer , ; ganist Choirmaster St. Nicholas , 
 words selected var ious passages Scripture Rev , Ti 
 G. Alford , M.A. , Vicar . solos sung members St , 
 Nicholas ’ choir . se rmon , preached Rev , 
 Ambrose M. Foster , Wilton , near Taunton , Dr. Stainer anthem 
 “ O clap hands ” sung , quartet taken members 
 Christ Church . Mr. Brookes , Organist Choirmaster St. 
 Peter lay - vicar Cathedral , conducted service . 
 offertory Church Missionary Society . — Union twenty- 
 Congregational Baptist Choirs city recently 

 series giv 
 patronised . occasi 
 Beethoven Symphon 

 agner 




 XUM 


 XUM 


 TT 


 3 ( £ 1 


 NEWCASTLE - - T } 


 592 


 DEATHS 


 EW , REVISED , ENLARGED EDITION 


 EW , REVISED , ENLARGED EDITION 


 MONTH . 


 RISTOL TUNE- BOOK 


 SUPPLEMENT 


 JOPHAM 


 ARTER , GEORGE 


 TOVELLO OCT AVO 


 OVELLO - SONG BOOK . 


 TOVELLO TONI IC SOL - FA SERIES . 


 YEDUCED PRICE : — 


 MATTHEWS , REV . T. R. — 


 RGAN 


 XUM 


 593 


 STAINES TOWN HALI 


 AE 


 ORGANIST 


 YOUNG 


 E - ENGAGEMENT 


 XUM 


 DIANOFORTE TUNER WANTED . 


 - CLASS 


 LUNER . — SITUATION 


 PIANOFORTE 


 DI ANOFORTE 


 EW 


 594 


 MUSICAL TIMES 


 NOVEMBER , 1881 


 RGAN MANUFACTORY 


 NV ESSRS . KELLY & CO 


 WEST - ENI 


 TH 


 AGENCY 


 UTHORS ’ WORKS CORRECTED , REVISED , 


 N ELBOURNE EXHIBITION , 1881.—FIRST 


 SOWER ” ORG 


 PIANOFORTE MANUFACTURERS 


 OTS 


 XUM 


 LY 


 RUDALL , CARTE & CO 


 ORCHESTRAL MILITARY 


 FLUTES . 


 OLD SYSTEM . IMPROVED OLD SYSTEM , RADCLIFF 


 MODEL . CARTE SYSTEM . 


 DITTO , ROCKSTRO MODEL 


 CLARINETS . 


 ORDINARY , BOEHM , CARTE PATENT 


 OBOES . 


 ORDINARY , BOEHM , BARRET SYSTEM . 


 BASSOONS , CORNETS , 


 HARPER MODEL TRUMPETS . 


 RAOUX MODEL FRENCH HORNS . 


 TENOR HORNS , BARITONES , EUPHONIUMS , 


 RUDALL , CARTE & CO . , 


 LONDON , W 


 BRISTOL 


 MR . J. HAMILTON 


 6 , CHESTERTON TERRACE , CITY ROAD 


 MANUFACTURER 


 ORGAN PEDALS PIANOFORTES 


 RUMMENS 


 XUM 


 VIOLIN BOW 


 MAKERS REPAIRERS 


 GEORGE WITHERS & CO . 


 WHOLESALE IMPORTERS 


 MUSICAL STRINGS 


 FINE COLLECTION ITALIAN INSTRUMENTS 


 ST . MARTIN LANE , LONDON 


 LER MUSICAL INSTRUMENTS 


 MALEY , YOUNG & OLDKNOW , 


 | ORGAN BUILDERS , 


 | KING ROAD , ST . PANCRAS , N.W 


 95 


 JAMES CONACHER SONS , 


 ORGAN BUILDERS , 


 BATH BUILDINGS , HUDDERSFIELD 


 NEW MUSIC- ENGRAV ING 


 STEAM PRINTING 


 EXHIBITED 


 50 CENT . CHEAPER . 


 HATTON GARDEN , LONDON 


 RGAN , 


 596 


 12 , BERNERS STREET , LONDON , W 


 CONCERT . GIVERS ENTREPRENEURS 


 GENERALLY . 


 CLERGY . 


 _ _ _ _ _ _ CHOIR - BOYS . 


 ORCHESTRAS CHOIRS . 


 21 


 NEW FOREIGN PUBLIC : \TIONS 


 MENDEL SSOHN . 


 LONDON : NOVELLO , EWER CO 


 


 XUM 


 597 


 KING CARNIVAL 


 GABRIEL DAVIS 


 


 RIVER 


 GABRIEL DAVIS 


 ELSIE ( FLOWER GIRL SONG ) 


 ZINGARA 


 QUEEN 


 MUSIC COMPOSED 


 GABRIEL DAVIS . 


 NEW SONG . 


 COMING YEAR 


 OPINIONS PRESS 


 NEW PATRIOTIC - SONG . 


 KING NIGHT 


 SONGS COUNTRY LIFE 


 COMPOSED 


 STEPHEN S. STRATTON 


 STANDARD 


 PUBLICATIONS 


 STANDARD 


 VOCAL MUSIC 


 SINGER LIBRAR 


 UNEQUALLED COLLECTION VOCAL CONCERTED 


 SECULAR , 


 COLLECTION 


 KNOWN VOCAL COMPOSITIONS 


 RECENT WORKS MACFARREN , 


 S.A.T.B. ) AD LIB . PIANOFORTE ACC 


 MARY SMITH , IGNACE GIBSONE , SIR W. S. BENNETT , 


 WALTER MACFARREN , CIRO PINSUTI , W. H.CUM MINGS , 


 HATTON , OLIVERIA L. PRESCOTT , CARULLI , 


 BELLINI , BENEDICT , BISHOP , BARNETT , BARNBY , 


 CIMAROSA , CAMPANA , DONIZETTI , FLOTOW , HATTON , 


 HILLER , MENDELSSOHN , MACFARREN , MEYERBEER , 


 MOZART , MERCADANTE , PINSUTI , ROSSINI , SPOHR , 


 S SENT POST - FREE 


 WORLD 


 CATALOGUES 


 


 ASHDOWN PARRY , 


 HANOVER SQUARE 


 LONDON 


 598 MUSICAL TIM 


 ANTHEMS — ADVENT 


 HYMNS TUNES 


 


 ADVENT 


 SELECTED “ HYMNARY 


 ADVENT 


 BENEDICITE 


 SET MUSIC FOLLOWING 


 MODERN COMPOSERS 


 SCHUMANN ADVENT LIED 


 LOWLY GUISE T HY 


 KING APPEARETH 


 TRANSLATED GERMAN FRIEDRICH RUCKERT 


 SOPRANO SOLO CHORUS , 


 ORCHESTRAL ACCOMPANIMENT 


 ADVENT HYMN . _ 


 O COME , O COME , EMMANUE 


 S. VINNING 


 MUSIC 


 ADVENT CHRISTMAS 


 


 CHRISTMAS CAROLS 


 THIRTY CHRISTMAS CAROLS 


 ( ANCIENT MODERN ) 


 CHRISTMAS CAROLS 


 LL HAIL MORN CHRIST 


 _ CHURCH SE ASONS . 


 ~ MU 


 ISIC FOR- CHRIST ) MAS 


 HYMN CONCL USION EVEN- 


 ALBERT LOWE 


 CHRISTMAS ANTHEM , “ HOSANNA ! ” 


 EASY ANTHEM CHRISTM : . 


 2LESSED COMETH . 


 XUM 


 


 CHRI 


 USICAL 


 STMAS 


 ANTHEMS 


 HYMNS T 


 CHRIS TMAS 


 SELECTEI ROM “ HYMNARY 


 PRICE 


 ONT 


 CHRISTMAS CAROLS 


 WORDS EDITED 7 


 HENRY RAMSDEN BR 


 REV . 


 


 JOHN STAD 


 MLEY . } 


 LEY , MAA 


 


 CHRISTMAS ORATORIO 


 JOHN SEBASTIAN BACH 


 ENGLISH TRANSLATION ADAPTATION 


 REV . J. TROUTBECK , M.A. 


 XUM 


 CH RISTMAS CAROLS 


 RANGED 


 MEN VO ICES 


 SET SIC 


 GEORGE 


 PRICY 


 “ 4D _ TINE 


 CAROLS CHRISTMAS - TIDE 


 ARTHUR SU LLIVAN 


 SONG 


 CHRISTMAS BELLS 


 SEA 


 CHRISTMAS BOOK 


 


 


 J. F. LORRAINE 


 CHRISTMAS EVE 


 SHORT CANTATA 


 ALTO SOLO , CHORUS , ORCHESTRA 


 COMPOSED 


 NIELS W. GADE . 


 BIBLICAL PASTORAL 


 MUSIC COMPOSED EY 


 HENRY LESLIE . 


 MENDEL SSOHN 


 OLIVER KING 


 SEGUIDILLA , REVERIE , EDVARD GRIEG , 


 ROMANCE . AUBADE . ADOLPHE HENSELT , 


 CANON . IDY LL , STEPHEN HELLER , 


 MAZURKA . | MINUET . FRANZ LISZT 


 SELECT COMPOSITIONS 


 


 PIANOFORTE 


 EDITED 


 CARLI ZOELLER . 


 BOOK 


 BOOK II 


 


 PIANOFORTE 


 ANTHEMS 


 


 WO ) LAN SAMARI 


 SACRED CANTATA 


 COMPOSED 


 WILLIAM STERNDALE BENNETT 


 NEW REVISED EDITION 


 SUITE E MAJOR 


 ORGAN 


 EMILE BERNARD . 


 WNH 


 CHERUBINIS MASSES 


 SCORE , 


 CELEBRATED NOCTURNES : 


 FAVOURITES 


 COLLECTION POPULAR MELODIES 


 RITT ER ALBUM 


 ORGAN 


 COLLECTION - ORIGINAL PIECES 


 COMPOSED HONOUR 


 A. G. RITTER , 


 


 EMINENT GERMAN COMPOSERS ORGAN MUSIC 


 EDITED 


 RUDOLPH PALME . 


 READY , NEW EDITIONS 


 2 SACRED MUSIC ENGLISH WORDS 


 3 . MUSIC VOCAL ORCHESTRAL 


 PARTS 


 4 . MUSIC PIANOFORTE 


 5 . SECULAR VOCAL MUSIC 


 6 SACRED MUSIC LATIN WORDS 


 XUM 


 EIGHTY - EDITION . 


 PRICE SHILLING , ENLARGED 


 COLLEGIATE SCHOOL 


 SIGHT - SINGING MANUAL 


 COLLEGIATE ORGAN TUTOR 


 THIRTEENTH EDITION . 


 CATHEDRAL CHANT - BOOK 


 BOOKS 2 , 3 , & 4 , 


 COLLEGIATE SCHOOL 


 SINGING MANUALS 


 CHORUSES , GLEES 


 SUPPLEMENTAL 


 BOOK EXERCISES 


 USE 


 LEARNING SING SIGHT } 


 HENRY GADSBY 


 O PIANOFORTE STUDENTS , TE ACHE RS , 


 XUM 


 REDUCED PRICE , SHILLING 


 MANUAL SINGING 


 USE 


 CHOIRTRAINERS & SCHOOLMASTERS 


 RICHARD MANN 


 NEW REVISED EDITION , ADDITIONS , 


 COLLE CTION 


 - SOLFEGGI 


 SELECTED 


 DURANTE , HANDEL , LEO , SCARLATTI , STEFFANI , 


 JAMES HIGGS 


 SHILLING . 


 NV USIC NEW CODE . “ 


 SINGERS , TE ACHERS , PU BLI IC 


 SINGING LESSON 


 ORCHESTRA TH E CHOIR . 


 602 


 HYMNARY 


 BOOK CHURCH SONG 


 FOLLOWING EDITIONS READY 


 LONDON : NOVELLO , EWER CO . 


 BRISTOL TUNE - BOOK 


 SUPPLEMENT 1881 , CONTAINING 37 TUNES , 64 . 


 MATCH EDITION . 


 ‘ 49 


 BOOK PSALMS ( BIBLE VERSION ) , 


 ANGLICAN HYMN - BOOK . _ 


 TE 


 V.W 


 TE DEUM LAUDAMUS ( F ) 


 JUBILATE DEO ( F 


 SEVERN 


 REDUCED PRICES . 


 DUCED PRICES . 


 UNION PSAL TER 


 ERRING “ METRICAL 


 3\KER . 


 “ DEEP 


 LAY PEACE 


 REDUCED THREEPENCE . 


 A. MACPARREN - ANTHEMS 


 ET HEART TROUBLED . 


 GOD HOPE . D STRENGTH 


 GEO ! 


 XUM 


 SECOND SERIE 


 USELEY MONK ’ S P SALTE R LONDON : NOVELLO , EWER CO . 


 PS : ALTER , “ PROPER . PS . AL MS , HYMNS , 


 OULE DIR SECTORIUM CHORI ANGLI.- 


 ‘ ORDER HOLY COM : UNION 


 604 


 NAZARETH 


 AD LIB 


 WORDS MUSIC 


 HENRY F. CHORLEY . CHARLES GOUNOD . 


 WORDS MELODY , 


 CHORUS , UNISON 


 CHORUS SOPRANO , CONTRALTO , TENOR BASS 


 ROOK . 


 


 ARRANGEMENTS 


 DAVIS . 


 NOTICE TRADE PROFESSION 


 NEW EDITION HENRY LESLIE ’ 


 YANTATAS AMATEURS.—HENR\ 


 WILLIAM J. YOUNG 


 XUM 


 XUM 


 605 


 ARTHUR HENKY BROWN 


 CE ORGE FREDERICK WEST SU CCESS- Volumes 

 ul s Waltzes . Books 1 3 4 0 
 Zar “ ave » Waltzes . Nos . 1to3 = » £ ¢ 
 Beethoven ’ s V “ ~ oi 
 , Duet s ¢ 0 
 Angels br sin 
 Der Lustig¢ “ 
 Ditto , Duet ... 4 0 
 Nazareth ( Gouno ei 4 0 
 Silver ! ls memory ( Dale ) oa 3 « OO 
 \ s watery nest ( Hatton ) 4 0 
 Spohr ) ; ; 3 0 
 , 12th Service ( Moz art ) o- yo wee ae 3.0 
 Lord mindful ( Mendelssohn 3 
 m. Stabat Mater . r = 4 
 Isis , 12th Service ( Mozart 3 0 
 Chinen ette de Boheme ... = 3 0 
 Gavotte B fiat ( Handel 4 0 
 IY len 3 ) 
 st “ ne ons Oo 
 Nelson V ictory ( braham 4 0 
 Harmonious Blacksm sith ( Hand 3 0 
 verdure clad ( | Haydn ) ee 3 0 
 Caller Herrin ’ 3 0 
 Freischitz ... ae 4 0 
 leil ies ena 4 
 Service ( Mozart 3 0 
 s tlock ( Handel ) 2 6 
 ( Haydn Pics : 3 oO 
 : ( Handel ) : ine aid eee 
 Cornaval de Ve enis¢ Sa 5 bas ‘ iis pee wpe 
 heavens telling ( Hi : 3 0 
 Bridal March * L , ie ae 
 post - free half- ‘ price stamps . 
 cx omplete list Mr. West compo sitions gratis post - free . 
 London : KRovertT Cocks Co » . » NEW Burlingt ston Street . 
 Edition . Price 
 OW LE ARN PLAY PIANO 
 . TauGut HIMseEcr 

 , compass fourteen pages , matter calculated 
 produce better results half - - dozen years ’ teaching . ” 
 ART PLAYING SIGHT . Oxe 
 TauGutT Himsetr . Price rs . 
 “ author believes system infallible ; fair trial , 
 believe . ” 
 OW FORM TRAIN VILLAGE 
 CHOIR . OrGANISING CHOIRMASTER . Price . 
 HARMONIUM , PLAY . 
 Price ts 




 MEN MAN LIFE BOAT ” ; “ LADY . ” 


 VOICE SPRING . 


 SPECIAL TERMS CHORAL SOCIETIES . 


 VICTORIES JUDAH 


 LADYE BARBARA GAVOTTE 


 GABRIEL DAVIS 


 LA 


 NEW SONG BASS BARITONE . 


 O MISTRESS 


 NEW SACRED SONG 


 PILGRIMS 


 SUNG MR . FRANK BOYLE , 


 NEW SONG 


 LOVE HELD DEAR 


 ENFORD , 


 NORFOLK NORWICH MUSICAL FESTIVAL , 


 


 SUN - WORSHIPPERS 


 CHORAL ODE . 


 CHARLES NEWTON SCOTT 


 A. GORING THOMAS 


 PRICE SHILLING SIXPENCE , 


 REDUCED PRICES 


 OUSELEY MONRK 


 POINTED PSALTER 


 HARROW SCHOOL MUSIC 


 


 JOHN FARMER 


 ORATORIO . — “ CHRIST SOLDIERS 


 SINGING QUADRILLES . 


 NARCISSUS ECHO MUSICAL TIMES 


 CONTAINING - CHORAL PIECES , 


 SACRED SECULAR 


 XUM 


 PERFORMED GREAT SUCCESS WORCESTER FESTIVAL 


 BRIDE 


 CANTATA 


 TRANSLATED GERMAN R. HAMER 


 SET MUSIC 


 A. C. MACKENZIE 


 PRICE SHILLING SIXPENCE 


 LONDON : NOVELLO , EWER CO 


 INSTRUME NTS MUSIC 


 TOY SYMPHONIES , & C 


 SET INSTRUMENTS 


 GUINEA HALF 


 INSTRUMENTS SEPAI 2 . T E LY . 


 J. ANDRE . NET . T. 


 S HAYDN . A. ROMBERG . 


 _ MOZART . M. WALLENSTEIN . 


 XUM 


 NOVELLO , EWER CO 


 LIST WORKS SUITABLE 


 PRESENTS SCHOOL PRIZESPIANOFORTE CLASSICS . 
 BACH . — - PRELUDES s. d.|MENDELSSOHN.—LIEDER OHNE WORTE 

 FUGUES . Folio 6 0 ; Cheap edition . Containing Books 7 8 . 
 BEETHOVEN.—SONATAS . New complete | Svo , 102 pages , Cloth , gilt , 4s . 6d . ; paper cover 
 edition . Edited Agnes Zimmermann . MENDELSSOHN.—OVERTURES ( Soro ' . 
 Folio , cloth , gilt 21 . 0 } complete edition ea Folio , cloth , gilt 
 BEETHOVEN.—SONATAS . New complete MENDELSSOHN.—OVERTURES ( Durr ) . 
 edition . Edited Agnes Zimmermann . complete edition ene Folio , cloth , gilt 
 8vo , cloth , gilt , 7s . 6d . ; paper cover 5 0 MENDEI SSOHN.—SYMPHONIES.S pa 
 HANDEL . — * MESSIAH . ” Arranged Piano- ee yop ve 
 Rene : Sindee Maetieald Pours ~ oa . 3 complete edition os olio , cloth , gilt 
 ; 2NDELSSOHN.—SYMPHONIES ( Dver ) . 
 MENDELSSOHN . — PIANOFORTE WORKS ee 
 ( including Lieder ohne Worte ) . entirely : y Res Aeon aad ilebaiee 
 new carefully revised edition . Folio , 518 MENDELSSOHN . — * ELIJAH . ” — Arranged 
 pages . Handsomely bound .. Cloth , gilt 21 0 ! Pianoforte Solo Berthold Tours 
 MENDELSSOHN . — PIANOFORTE WORKS MOZART.—SONATAS . New complete edi- 
 ( including Lieder ohne Worte ) . entirely tion . Edited Agnes Zimmermann . 
 new carefully revised edition . 8vo , 518 pages Folio , cloth , gilt 
 Cloth , gilt , ros . 6d . ; paper cover 7 6 ! y¥QZART.—SONATAS . New complete edi- 
 MENDELSSOHN.—LIEDER OHNE WORTE . | tion . Edited Agnes Zimmermann . 
 complete edition . Containing Books 7 | vo , cloth , gilt , 5s . ; paper cover 

 8 . Elegantly bound . Folio , 145 pages ( Seek Noa ees ee gee oe etd 
 Portrait Composer ) .. .. a2 o| PLANO ! Onre ALEL MS.—Edited . Berthold 
 MENDELSSOHN LIEDE > OHNE WORTE | Tours . Vol . I. , Bach ; Vol . II . , Handel . Cloth , 
 MENDELSSOHN.—LIEDER OHNE WORTE . | PIANOFORTE ALBUMS.—Edited Berthold 
 complete edition . Containing Books ae elas ‘ é 2 Ses pe eae 
 Tours . Nos.1 , 2 , 3 , Compositions Bach 




 VOCAL ALBUMS 


 CHRISTMAS BOOKS 


 CHRISTMAS CAROLS — NEW _ OLD . SACRED SONGS LITTLE SINGERS . 


 CHRISTMAS CAROLS — NEW OULD 


 XUM