


 the MUSICAL TIMESFEBRUARY 1 , 1871 

 in paper cover , price 2 . 6d . , scarlet cloth , gilt , 4 . 
 OVELLO ’S OCTAVO edition of OPERAS . 
 vol . 1 . BEETHOVEN ’S FIDELIO , with the german and 
 english word , correct accord to the original score , and the 
 english word translate oy NataLia MACFARREN 

 in paper cover , price 2 , 6d . , scarlet cloth , gilt . 4s 




 . SUNG by HERR STOCKHAUSEN . 


 SUNG by MR . SANTLEY 


 66 


 SUNG by MISS JULIA ELTON 


 he RICHES of CHAUCER 


 one can fly my law supreme 


 8s. JAMES ’S HALL 


 the 


 third season , 1831 . 


 the first subscription concert 


 will be give on 


 WEDNESDAY , FEBRUARY 


 BACH ' ’s SACRED ORATORIO 


 the : PASSILO RH 


 MADAME RUDERSDORFYF , 


 MADAME PATEY , 


 MR . W. H. CUMMINGS , 


 HERR STOCKHAUSEN 


 15 


 ~ § , JAMES ’s HALL 


 the 


 the second SUBSCRIPTION concert 


 will be give on 


 WEDNESDAY , MARCH , 1 


 MENDELSSOHN ’S ELIJAH 


 MADAME RUDERSDORFF , 


 MADAME PATEY , 


 MR . SIMS REEVES , 


 MR . RAYNHAM , 


 MR . SANTLEY 


 PROFESSIONAL notice , 


 H. T. LEFTWICH 


 EYOND ALLCOMPETITION.—T.R 


 WAUSICAL instrument for VOLUN . 


 RASS , REED , string , and DRUM and 


 armonium ! harmonium ! 


 W. HATTERSLEY , 


 HARMONIUM MANUFACTURER , 


 95 , MEADOW - STREET , SHEFFIELD . 


 G RATEFUL — comforting . _ 


 3 | ECO FEC IC | BS 


 ER 


 771 


 _ — _ 


 HE COLLEGE of organist ’ manual 


 eighth edition . 


 sight - singing manual 


 second edition 


 SINCING CARD 


 authorise musical text - book at RUGBY SCHOOL , 


 HE element of MUSIC systemat ] . 


 IRANA ’s new song . 


 ake back THOSE beauteous 


 CHUMANN ’s FROHLICHER LANDMANN , 


 the MESSIAH 


 additional INSTRUMENTATION of MOZART , 


 ACT II . 


 time of PERFORMANCE one hour and ten minute 


 W. H. BIRCH , 104 , LONDON STREET , read 


 C. JEFFERYS 


 57 , BERNERS STREET 


 FELIX GANTIER ’s operatic GEMSCATHEDRAL gem , by LOUIS DUPUIS . 
 NANTES.—Fantasia , on subject from Mozart 's 12th Mass 
 WURMS.—Fantasia , on subject from Weber 's Mass in G 
 ROTTERDAM — Fantasia , on subject from Haydn 's 3rd 

 Imperial ) Mass . wie aaa on aes net 
 MALAGA.—Fantasia , on subject from Beethoven 's Mass 

 in oo ae eee eee eee eee eee one 
 ROUEN . — Fantasia , on subject from Gounod ’s Messe 
 Solennelle eee eco eco ses ose oe eee 
 CAEN.—Fantasia , on subject from Rossini ’s Stabat Mater 
 MAYENCE.—Fantasia , on subject from Haydn ’s Ist 
 Mass , b flat ... eee pom eee aoa sce eco 
 COLOGNE.—Fantasia , on subject from Mozart 's Ist 
 Mass , in C aa a wid _ age one ono 
 9 . PARIS.—Fantasia , on subject from Mendelssohn ’s Hymn 
 of Praise eee aoe ese eee pom eee " se 
 10 . LONDON.—Fantasia , on subject from Handel ’s Messiah 
 11 . NORWICH.-—Fantasia , on subject from Haydn 's Creation 
 12 . CANTERBURY.—Fantasia , on subject from Spohr ’s as 
 pants the Hart ... eee aes ote eee ee wo 8 @ 
 beautifully illustrate in Colours , with view of celebrate cathedral . 
 " these piece , like those by Felix Gantier , review some time 
 ago in the Musical Times , be especially intend for young player , 
 and will , we think , be find equally acceptable to teacher and 
 pupil . they be write in the form of little Fantasias , each com- 
 mence with a short introduction . the illustration , print in 
 colour , be exceedingly beautiful . ”—Musical Times , Feb. , 1870 




 WILLIE PAPE . 


 HE LONDON VOCAL QUARTETTE 


 R. J. F. N 


 = : . = MISS FANNY DANIELSON 


 MR . T. PEARSON 


 assist by 


 R. LANSDOWNE COTTELL ’s concert 


 v — _ 


 the MUSICAL TIMES , 


 FEBRUARY 1 , 1871 


 CLIFFORD ’s ANTHEM - BOOK . 


 A RETROSPECTIVE review 


 776 


 _ _ _ _ 


 777 


 LL 


 778 


 CRYSTAL PALACE 


 _ _ _ _ 


 ITALIAN OPERA BUFFA 


 _ — _ 80 the MUSICAL TIMES.—Fexzrvary 1 , 1871 

 family use , and give rise to the choral society which } Mr. P. ¥ . Van Noorpen give a Soirée Musicale 
 are be daily multiply throughout the land , Specimens| at the Beethoven Rooms , on Wednesday , the 18th ult , 
 be give from the writing of live composer , and | assist by Misses Patti Laverne , Butler , Dunsford , Evens 
 argument deduce thence that the musicianship of ' and Madame Eldon,—Messrs , Lawson , Greene , Bedf 
 England be in course of regeneration , and that we may Wood , Wright and Bolton , Miss Patti Laverne 
 look to the past as a mirror of time to come . the rendering of Rossini ’s " Una Voce " be highly satig 
 illustration be receive with the most enthusiastic factory ; and the trio , " the Labourer ’s evening song " 
 applause , and several be encore , include the original| be also effectively sing by Miss Laverne , Poe 
 set of " where the bee suck , " by Robert Johnson , |Kjdon and Mr. Bolton , two part - song be well give 
 " o come ye from Newcastle , " a ditty of the 16th century | by Mr. Van Noorden ’s pupil 

 both excellently sing by Miss Harmon ) , and Bishop ’s ' 
 well know glee , " Mynheer Vandunck . " ' the roomwas| we be inform that ' " Rebekah , " by Mr , 
 densely crowd on each evening . Joseph Barnby , be now be rehearse by the Harlesdon 

 encore ) , Herr Stepan , and Mr. D. Ryan , all of which 
 be much appreciate , Handel ’s ' * where e’er you walk " 
 « Semele " ) , be well render by Madame Gilbert , Miss 

 therine Poyntz sing two song very effectively , and 
 Mr. Stedman , a tenor with an agreeable voice , sing a 
 ballad by Madame Gilbert , « from Maytime to December , " 
 and also give , with much expression , Balfe ’s song , ' « we 
 may be happy yet . " Mr. Holmes play with refined 
 taste Beethoven ’s Andante in F , aud join Mr. Alfred 
 Gilbert in an admirable duet for two piano , by Mr. Cipriani 
 Potter . ' the evening be bring to a close by the 
 overture to ' ' Masaniello , " arrange for two piano ( eight 
 hand ) , and remarkably well play by the four gentleman 
 before mention 

 by a circular , which have be forward to we , 
 we perceive that Mr. Ridley Prentice be endeavour to 
 establish classical chamber concert at the Eyre Arms 
 Assembly Rooms , St. John ’s Wood , similar in character 
 to those which he have successfully carry on for two 
 season at Brixton . when we say that , should he meet 
 with sufficient support , he will associate with himself in 
 the performance such artist as Mr. Henry Blagrove , Mr. 
 Henry Holmes , Herr Straus , Signor Piatti , & c. , there can 
 be no doubt of the general excellence of the concert ; 
 and all who desire the progress of the high classical 
 music will join we in wish Mr. Prentice every success 
 in his enterprise 

 this cycle of song be full of conceit ; and although , 
 as may be expect , we have here and there the true 
 touch of the artist , these touch be few and far between , 
 and whenever one appear , it be instantly quit , as if the 
 poet have condescend to write for a child , and be afraid 
 of soar above his comprehension . the story , if story 
 it may be call , be simple enough . a lover , one windy 
 morning , be on the hill , and anxiously look at the win- 
 dow of his sweetheart . approach her cottage , he find 
 she go ; and if our reader can extract any meaning from 
 the line " down in the south be a flash and a groan , she be 
 there ! she be there ! " they may discover where she have 
 goneto . then we find that the frost have come , and " fuel 
 be dear . " spring return , and the lover having propose , 
 by letter , to his mistress , after a weary time of waiting , 
 receive a reply . he be accept ; and the lady be re- 
 quest to name the day , the poem conclude with a song 
 | descriptive of the joy of the bridal morning . our reader 
 |may naturally ask what the Wrens have to do with all 
 |this : we can not say . the lady be call the " Queen of 
 the Wrens , " and the lover long for the time when they 
 shall " have a nest together , " so that we presume this may 
 be pass as one of the " conceit " which we have already 
 mention . we do not know whether Mr. Sullivan in 
 compose these lyric have discover more real poetry in 
 they than we have ; or whether , in earnest love of his art , 
 he have resolve to shame his partner ; but certainly he have 
 throw a heart into his task which have produce excellent 
 result . all the song be remarkable for artistic finish , 
 and in many of they we discover a continuity of idea 
 which be highly commendable on account of the positive 

 by Mr. Joseph Barnby , be promise . r. Benedict ’s 
 Oratorio , ' St. Peter , " and Dr. Ferdinand Hiller ’s « Nala 
 and Damayanti " ( the latter of which will be hear for the 
 first time in London ) be also to be give ; and Bach ’s 
 " Passion " music and Beethoven ’s Mass in D , both of 
 which create such an effect at these concert last season , 
 will be repeat . during the series of six subscription 
 concert other important work will also be perform . 
 the vocalist engage be Madame Lemmens - Sherrington , 
 Miss Edith Wynne , Madame Rudersdorff , Madame Patey , 
 Miss Julia Elton . Mr. Sims Reeves , Mr. Cummings , Mr. 
 Raynham , Mr. Santley , Herr Carl Stepan , Mr. J. Thurley 
 Beale , and Herr Stockhausen . it be needless to say that 
 the chorus will consist of Mr. Joseph Barnuby ’s ch«ir , and 
 that all the performance will be conduct by Mr. 
 Barnby , with the exception of the work of M. Gounod 
 and Dr. Hiller , which will be conduct by the composer . 
 as usual , Mr. F. A. W. Docker will be organist , a post 
 which he have so efficiently fill since the establishment 
 of the choir . the first concert takes place at St. James ’s 
 Hall , on the 15th inst . , when Bach ’s ' « Passion " music 
 will be give 

 Rebielws 

 the Musical Directory , Annual and Almanack for 1871 

 ar length we be enable conscientiously to recommend 
 this work as a reliable book of reference : all the date 
 * yave be carefully correct , and everywhere we see the 
 result of that careful supervision which we have a right to 
 expect in a volume the chief merit of which should be its 
 unerring accuracy . ' Che " remark on the past year " 
 give a faithful record of the principal musical event ; but 
 as in one part of the article it be expressly say that 
 opinion be purposely not advance , we think it a 
 pity the writer should affirm that Herr Wachtel , the 
 vocalist , have ' never be properly estimate here . " on 
 the contrary , we believe , it be because he have be properly 
 estimate , that his sudden secession from his position at 
 the Royal Italian Opera during the last season cause so 
 little regret that the public do not even care to read his 
 letter explain the cause of his absence . Mr. John 
 Towers , who have on a former occasion contribute the 
 result of his musical research to this work , give we in 
 the present number an interesting paper on Beethoven , and 

 a 




 838 


 2 eam bp rer ee p 


 anthem for TENOR SOLO , QUARTETT , and CHORUS . 


 the word adapt to the . music of 


 VINCENT NOVELLO . 


 s 


 call to remembrance 


 : + t t j + 


 SSS 


 ss 


 call to remembrance 


 4h - 


 = SS 3 e 


 SSS 


 NOVELLO ’S 


 OCTAVO edition of 


 OPERASThe price of each Opera , sew in paper cover , will be 2 . 6d . ; or , bind in scarlet 
 cloth , 4 

 now ready , 
 BEETHOVEN ’S " FIDELIO , " with german and english word ; 
 AUBER ’s " FRA DIAVOLO , " with french and english word ; 
 MOZART ’s " DON GIOVANNI , " with italian and english word ; and the music of all 
 the Recitatives . 
 BELLINI ’s " NORMA , " with italian and english word , will be issue on March 1 

 the following opera be also in course of preparation 




 VERDI ’s RIGOLETTO , ROSSINI ’s BARBIERE . 


 »   TROVATORE , BELLIN ’s SONNAMBULA , 


 DONIZETTV ’s LUCIA , MOZART ’s NOZZE DI FIGARO , 


 789 


 790rally adopt . for freshness and vigour of style this 
 Anthem , or , rather , this grand Cantata , be most remarkable . 
 let those who think they know Handel ’s conventional 
 modulation and periodic tread study this work to their 
 refreshment 

 the tenth anthem , " let God arise , " consist of five 
 movement . the fine effect which arise from the treat- 
 ment of the close of number 1 and the entry of number 2 , 
 will suggest to many a similar effect in Bach ’s Passion- 
 music . the chorus of this work be in five part , the 
 alto be divide into first and second . but the first 
 alto part , as far as compass be concern , may well be 
 term a second treble part ; no difficulty in performance 
 need therefore be anticipate from this somewhat unusual | 
 arrangement of the voice part . the striking syneopation | 
 of triple time , so common in Beethoven ’s work , be not 
 often find in those of Handel , yet a fine example occur 
 at the close of number 2 , to the word ' ' fly before 
 Him . " the mind be irresistibly impel to reckon on a 
 triple accent , and be therefore strangely impress with 
 what be , except for its position , an ordinary phrase in 
 common time . a solo , apparently for a bass voice , but 
 much more suitable to a baritone , next follow , to+ ' like 
 as the smoke vanish . " at the word , " like as wax 
 melteth , " the alto take up the solo , the style of the music 
 be continuous . a short but exceedingly beautiful 
 duet for alto and bass precede the last chorua , " bless 
 be God . hallelujah , " the theme of which , announce 

 over a flow bass . be nothing more nor less than the 
 eanto fermo , ' I will sing unto the Lord , " in Jsrael in 
 Egypt ; but the treatment be of course entirely different 
 from that which it receive in the Oratorio bring out 




 791 


 to the EDITOR of the MUSICAL TIMES 


 to the editor of the MUSICAL TIMES , 


 H. M.A 


 TO COBRESPON : DENTS 


 792Doveuas , Iste or May.—On the 11th ult . a large 
 audience bleed for the opening concert of the Douglas Singing 
 Class , under the able direction and instruction of Miss Wood . 
 several part - song be give with much success , amongst the 
 most effective of which we must mention ' the dawn , " by G. F , 
 Webb ; Miss Wood 's clever ' ' Serenade " ( compose expressly for 
 the Douglas Singing Class ) , and W. Macfarren ’s ' ' you steal my 
 love . " Pinsuti ' ’s duet , ' ' out in the sunshine , " be well sing by 
 Mrs. Spittall and Miss Wood ; and Mrs. Spittall , in response to an 
 encore for Sullivan ’s song , " the mother ’s dream , " give Miss 
 Wood 's ' across the sea , " which be loudly and deservedly 
 applaud . a decided feature in the concert be the part - song , 
 ' Autumn wind , " the solo of which be so effectively give by 
 Miss Wood as to elicit an enthusiastic encore . Miss Hutchinson 's 
 performance of Pauer 's " cascade " and Thalberg ’s ' ' home , sweet 
 home , " on the pianoforte , also deserve especial commendation . the 
 concert be attend by his Excellency the Lieutenant - Governor 
 and Mrs , Loch 

 DunpEE.—On the 22nd December a very excellent per- 
 formance of Handel 's Messiah be give in the Kinnaird Hall , the 
 principal vocalist be Madame Rudersdorff , Miss Alice Fairman , 
 Mr. Arthur Byron , and Mr. Orlando Christian , all of whom be 
 highly effective , Miss Fairman ( who be a stranger in Dundee ) mak- 
 e a most favourable impression in the alto solo . the chorus 
 be sing with commendable : precision throughout , especially 
 ' and the glory , " ' for unto we , " and the " hallelujah . " Mr. 8S , C. 
 Hirst preside at the organ , and Mr. Henry Nagel conduct . — 
 Tue first of a series of orchestral concert take place on the 23rd 
 December . the vocalist be Madame Rudersdorff and Mr. Arthur 
 Byron , and the solo instrumentalist Herr Louis Rothfeldt ( piano- 
 forte ) and Mr. C. Hamilton ( violoncello ) . Madame Rudersdorff 
 obtain an encore in a spanish song , and Mr. Byron make a 
 decide effect in the song from Joseph Barnby ’s Rebekah , * ' the 
 soft southern breeze . " the orchestral piece be Beethoven 's Pas- 
 toral Symphony , the same composer 's Overture to Fidelio , Nicolai 's 
 overture to ' ' the Merry wife of Windsor , " and Weber ’s " jubilee " 
 overture . it be much to be regret that the second of these con- 
 cert , which be give on the 24th December , from circumstance 
 beyond the control of the promoter of the series , be likely to be 
 the last 

 Eprxsurcu.—Professor Oakeley ’s second lecture on the 
 subject of vocal music , consider from a practical point of view , 
 be giver , on the 10th ult . , in the Music Class - room , Park Place , 
 with choral illustration , to student and other interested in the 
 subject , include some of the member of the Edinburgh Choral 
 Union 

 Epmontron.—The eighth half - yearly private rehearsal 
 of the Edmonton Vocal Association , take place on Monday Evening , 
 the 9th ult . , before alarge audience . the first part be devote to 
 Spohr ’s sacred Cantata ' ' the Christian ’s Prayer , " the soli part be- 
 e ably sustain by member of the choir . the chorus be 
 sing with much precision and refinement . the second part con- 
 siste of glee , part - song , & c. , amongst which may be mention 
 Mendelssohn ’s ' ' farewell to the Forest , " and ' ' May Song ; " Hatton 's 
 " Fairy Whispers,”and ' the blue Bird ; " Leslie 's * Troubadour , " & c. 
 Misses Denham and Friedel gain a well merit encore ina duet , 
 and Mr. Selwyn Jay inasolo . the accompaniment be efficiently 
 play by Mr. J. Harman Judd , and the whole performance , under 
 the able conductorship of Mr. Joseph Kale , be highly successful 

 Exern.—Herr Liffler give a concert in the Assembly 
 Rooms on Wednesday , the 11th ult . in the first part of the pro- 
 gramme he perform in excellent style a selection from ' " * Zauber- 
 fiéte , " ' * Andante and Rondo Capriccioso " ( Mendelssohn ) , and " ' Le 
 Chant des Naiades " ( Ascher ) . the principal piece in the second 
 part be ' Sonate Pathetique " ( Beethoven ) and ' Fantasie im- 
 promptu " an : ! " Grand Polonaise , " in A , by Chopin . the programme 
 be intersperse with song , among which be ' the Watch on 
 the Rhine , " ' the Marseillaise , " ' ' down among the dead man , 
 " the good Rhine wine , " & c. Herr Liffler make some interesting 
 remark on the composer whose music he perform , and the 
 entertainment be in every respect exceedingly attractive 

 EvesHam.—Mr . Charles Lunn ’s lecture on the " Philo- 
 sophy of Song " be deliver at the Institute on Friday , the 13th 
 ult . , to an audience of nearly a hundred people . in his introductory 
 remark Mr. Lunn dwell on the practice of music as an art , and a8 
 one of the fine art , compare it with , and place it above , the 




 793Leeps.—The concert on the 21st ult . be a decide 
 success . the Town Hall be throng in every part , and the grati- 
 fication of the audience with the performance be evidently most 
 genuine . the first part consist of a portion of the Messiah , 
 which be render in a very creditable manner . Miss Winder , 
 Miss Newell , Miss Anyon , Miss Kennedy , and Messrs. Longbottom , 
 Hartley , White , and Briggs , be the soloist ; and the chorus 
 be give by member of the Leeds Madrigal and Motet Society , 
 assist by the string band of the Fifth Dragoon Guards . " for 
 unto we , " the Pastoral Symphony , and the " hallelujah " chorus 
 be loudly applaud ; and a portion of the first - name be re- 
 peate . the second part of the programme consist of a Shake- 
 spearian overture , arrange by Dr. Spark , and seyeral miscellaneous 
 piece . the Madrigal Society , train by Dr. Spark , sing splen- 
 didly throughout the concert , the voice be admirably balance , 
 and well under the director ’s baton . the most perfect performance 
 of the evening be that of two new part - song by Henry Smart — 
 " oh wake , love , " and ' ' Crocuses and Snowdrops . " well choral 
 singing than this have rarely be hear within the Town Hall . 
 the whole performance , solo and choral , reflect the utmost credit 
 on the Madrigal Society 

 Leicester — a concert take place , on the 28th Decem- 
 ber , in the Freemasons ’ Hall , in aid of the organ fund of St. 
 Andrew 's Church , at which Mr. Alfred Gilbert , R.A.M. , -and 
 Madame Gilbert , R.A.M. , kindly give their service . the per- 
 formance include the ' ' Gratias agimus , " from Rossini ’s Mass , 
 ( well render by Miss Bromley , Mr. T. H. Belcher , and Mr. Arthur 
 Sankey . ) Handel ’s ' ' farewell ye limpid stream " and Mozart 's 
 * dove sono , " ( expressively give by Madame Gilbert . ) ' Cujus 
 animam " ( Rossini ) and ' " O del mio core " ( Gluck ) , by Mr. Edward 
 Sankey ; ' ' if with all your heart " and " the Garland " ( Men- 
 delssohn ) , by Mr. T. H. Belcher ; " Nazareth " ( Gounod ) , and * si 
 trai ceppi " ( Handel ) , by Mr. Arthur Sankey ; and ' tear such as 
 tender father shed " ( Handel ) , and " ' King Christmas " ( Hatton ) , by 
 Mr. Charles Sankey ( encore ) . Mr. Gilbert perform with mach 
 effect the ' ' moonlight sonata " ( Beethoven ) , and be deservedly 
 encore in Weber 's ' * ' Concert Stiick 

 LiskEARD , Cornwatu.-—A Choral Society have recently 
 be form in this town , of which Mr. F. N. Lohr have be 
 appoint the conductor 

 Liverpoot.—The second of the series on the plan of 
 the London ' Monday Popular Concerts , " take place in the Phil- 
 harmonic Hall , on Wednesday , the 11th ult . , the executant be 
 first violin , Herr Straus ; second violin , Herr L. Ries ; viola , Mr. 
 Zerbini ; violoncello , Signor Piatti ; solo pianoforte , Madame 
 Arabella Goddard ; vocalist , Herr Jules Stockhausen ; accompanist , 
 Mr. Zerbini . the programme include quartet in A minor , no . 1 , 
 Op . 29 , for string ( Schubert ) ; recit . and air ( Handel ) from Susanna , 
 " frost nip the flower " and ' on fair Euphrates ’ verdant side " ; solo 
 pianoforte ( Beethoven ) , in a flat major , op . 26 ; Trioin C minor , 
 no . 2 . op . € 6 , for pianoforte , violin , and viol llo ( Mendelssohn ) 
 song , ' " ' Kennst du das Land " ( Beethoven ) and ' " Widmung " 
 ( * du meine seele " ) ( Schumann ) . this excellent concert conclude 
 with Beethoven ’s Quartett in A , Op . 18 , exquisitely play by 
 Herren Straus and L. Ries , M. Zerbini , and Signor Piatti . the 
 room be fill with a thoroughly attentive audience 

 Mancuester.—The Manchester Vocal Society give a 
 subscription Concert in the Concert Hall on the 27th Dec. the 
 programme comprise composition by R. J. Stevens , Mendelssohn , 
 H. Smart , R. 8 . Pearsall , Bishop , C. Voss , Ciro Pinsuti , Gounod , 
 & c. the principal be ber of the Society , and Mr. H. 
 Wilson conduct and accompany as usua].——the programme of 
 the Gentlemen ’s Concerts , at the Concert Hall , on the 28th Dec 

 794 the MUSICAL TIMES.—Frprvary 1 , 1871 

 include composition by Haydn , Handel , Cimarosa , Mendelssohn , 
 Mozart , Spohr , Rossini , Bach , Schumann , Schubert , Gounod , & c. 
 the principal be Madame Norman - Neruda , Miss Edith Wynne 
 and Herr Stockhausen , — -Mr . Hallé ’s 10th Concert be give on 
 the 29th Dec. principal Mr. Santley and Mr. Hallé . the novelty 
 be Gade ’s Overture ' ' Ossian " and Entr’acte from " Rosamunde " 
 in G. the programme also include Beethoven 's Pastoral 
 Symphony.——The 11th concert take place on the 5th ult , Prin- 
 cipal Madame Viardot Garcia and Mr. Hallé ; the novelty be 
 the Ballet Music to Camacho ’s Wedding , by Mendelssohn . the 
 programme include Beethoven 's Grand Symphony in A , no . 7 . 
 — — Mr. Hallé ’s 12th concert take place on the 12th ult . Mendels- 
 delssohn ’s Hymn of praise , and Rossini ’s Stabat Mater , 
 be give , with Miss Arabella Smythe , Madame Patey , Mr 
 W. H. Cummings and Mr. Santley as _ principals.——On 
 Friday evening , the 18th ult , at Owen 's College , Mr. J. F. 
 Bridge , Mus . Bac . , and organist at the Cathedral , give the first 
 of a series of ten Lectures on Harmony and Composition.——A 
 cLAssicaL Chamber Concert be give in the Concert Hall , on the 
 17th ult . , with Messrs. Straus , Jacoby , Baetens , Paque , and E. Hecht 
 a principal . the programme , all instrumental , include compo- 
 sition by Beethoven , Schubert , Boccherini , and Haydn.——Mr . 
 Ha xx ’s 13th concert be give on the 19th ult . , at the Free Trade 
 Hall , the principal be Madame Vanzini , Madame Norman- 
 Neruda , Signor Foli , and Mr. Hallé . the novelty be ' ' Concert 
 Overture in D " ( Schubert ) and " six variation on an original 
 theme iv F , " Op . 34 ( Beethoven ) . the programme include com- 
 position by Spohr , Gounod , Wagner , Mozart , Dussek , Loder 
 Auber , and Beethoven ’s Grand Symphony in F , no . 8.——Messrs 
 ForsytH Brotuers ’ annual concert take place in the Free Trade 
 Hall , on the 2lst ult . the principal be Mesdiles . Titiens , 
 Sinico , Rosa Hannenberg , Louisa Jansen , Signori Foli , Ciampi , 
 and Vizzani . Signor Bevignani act as conductor and accompa- 
 nist . the programme be well choose , and the various artist 
 meet with a good reception from a large audience.——J / udas Mac- 
 cabeus be give at Mr , Hallé ’s 14th concert , on the 26th ult , 
 with Madile . Titiens , Miss Galloway , Miss Alice Fairman , Mr. 
 Neilson Varley , and Mr. Lewis Thomas as principal . Mr. Hallé 
 conduct , and Mr. Henry Walker preside at the organ 

 OxrorpD. — the member of the Oxford Orpheus Society 
 recently present their honorary conductor ( Dr. Stainer ) with a 
 testimonial , asa mark of their esteem , and as a trifling acknowledg- 
 ment of his valuable service as the honorary conductor of the 
 Society since its establishment some seven year ago . the testi- 
 monial consist of a chaste silver inkstand , mount on a polished 
 stand of black bog oak : on two circular plate of silver , one on 
 either side of the ink - vase , the follow inscription be engrave : 
 * present to John Stainer , Esq . , Mus . Doc . , M.A. , by the member 
 of the Oxford Orpheus Society . " the testimonial be present 
 by the President ( Mr. James Jenkin ) in the presence of several of 

 Swansea.—The Swansea Harmonic Society give its 
 third annual concert at the Music Hall , on Thursday evening , the 
 29th December , when Handel 's Oratorio , the Messiah , be ren- 
 dere with much success under the conductorship of M. Jules 
 Allard . the principal vocalist be — soprano , Miss Annie Ed- 
 munds ; contralto , Miss Marion Severn ; tenor , Mr. Edward Lloyd ; 
 and bass , Mr. Lewis Thomas , all of whom acquit themselves 
 most satisfactorily . the chorus be sing with great precision , 
 and reflect much credit upon the conductor . the Neath Har- 
 monic Society render efficient aid in the performance . both 
 Societies be vigorously rehearse Mendelssohn 's Zlijah with a 
 view to place that Oratorio before the public in March or April 
 next , at Swansea and Neath 

 Torquay.—Mr . William Vinning ’s second Recital of 
 pianoforte music for the present season take place , on Saturday , the 
 14th ult . the same young lady who make so favourable an 
 |impression at the first Recital , perform the primo part of Beet- 
 |hoven ’s Septett , arrange as a pianoforte duet , with much effect . 
 | Mr. Vinning ’s solo include Mendelssohn 's 4th Book of " Lieder 
 ohne Worte , " Beethoven 's Sonata in a flat ( with the funeral 

 the member . it be very handsomely execute by Mr. Osmond , | March ) , Book 2 of Stephen Heller 's ' ' nuits blanche " and Woelfl ’s 

 the MUSICAL TIMES.—Fesrvary 1 , 1871 . 795 

 essrs . A. Rowland , Levason , Read , and Guest , and very warmly 
 receive . Mr. Whitehead Smith , in the sonata , " Pathetique , " of 
 Beethoven , and the two song without word , by Mendelssohn , 
 number 2and 3 of Book 4 , exhibit much intelligence and facile 
 execution . a trio for pianoforte and string , play by Messrs. 
 Whitehead Smith , Levason , and Guest , be much admire ; and a 
 Grand Duo for pianoforte and double - bass , by Messrs. C. J. Read 
 and A. Rowland ( the composition of the latter gentleman ) be so 
 well play as to be re - demand . the vocalist be Miss Sofia 
 Vinta , who produce a highly favourable impression in all her 
 song , and receive enthusiastic encore for Spohr 's " ' rose softly 
 bloom , " afid a waltz airby Arditi . a portion of Mozart 's 
 Quintet in C minor , admirably play by Messrs. Rowland , Levason , 
 Read , W. Smith , and Guest , bring this excellent concert to a 

 close 

 Organ AppointmMeNts.—Mr . Alfred H. Holt , to Sale Congrega- 
 tional Church , Cheshire —— Mr. Capel Hullett , to St. Thomas 's 
 Church , Regent Street ( Archbishop Tenison ’s Chapel ) . Mr. 
 William Henry Lee Davies , to Craven Hill Congregational Church , 
 Bayswater 

 MUSICAL copyright of MESSRS . CRAMER and CO . , 
 ' of 201 , REGENT - STREET . 
 ESSRS . PUTTICK and SIMPSON will sell by 
 auction at their House , 47 , Leicester - square , W.C. , on 
 Monday , March 27 , and several follow day ( Sundays except ) , 
 the entire , extensive , and important stock of musical copy- 
 right and engrave plate of Messrs. CRAMER and 
 Co. , of 201 , Regent - street , form the large assemblage of this 
 class of property ever offer for sale , and comprise the Popular 
 Operas of Batre , WALLACE , and other composer , the entire series 
 of BEETHOVEN ’s work , edit by MoscHELEs and Lixpsay Soper , 
 avery large number of english Songs and Pianoforte Music , by 
 the most popular writer of the day ; Instrumental Music and 
 Arrangements , & c 

 dure the last month 

 hage nal SIEGFRIED.—L’Allégresse . Morceau 
 de Salon , pour Piano . ( op . 50 . ) 1s . 6d 

 IMMERMANN , AGNES.—Allegretto alla polacca 
 ‘ 4 from Beethoven 's Serenade , Op . 8 , for Violin , Viola , and 
 Violoncello , arrange for the Pianoforte . 1s . 6d . 
 — — Scherzo , from Beethoven ’s trio for Violin , Viola , and Violon- 
 cello , op . 9 . no.1 , arrange for Pianoforte . 1s . 6d . " 
 — menuetto from Beethoven ’s trio for Violin , Viola , and Violon- 
 cello , op . 9 . no . 2 , arrange for Pianoforte . 1s . 6d . 
 — no . 4 of Robert Schumann ’s sketch for the Pedal Pianoforte , 
 arrange for Pianoforte . 1s . 6d 

 EST , W. t.—arrangement from the Scores of 
 the Great Masters , for the Organ . no . 79 , price 2 . , contain 
 Chorus , Hallelujah , Occasional Oratorio , Handel ; Minuetto , Not- 
 turno for Wind instrument , Op . 34 , Spohr ; Polacca from the same . 
 — no . 80 . price 2 . , contain Overture , Jubilee , Weber 




 complete SERVIC ! of SONG for church or home . 


 TO CHORAL SOCIETIES and NATIONAL SCHOOLS . 


 % " " \o when the morning SHINETH . " 


 ARK ! HARK ! the ORGAN LOUDLY 


 DR . 8S. 8 . WESLEY 's NEW service . 


 AAA 


 edit by the 


 REV . ROBERT CORBET SINGLETON , M.A. , 


 FIRST WARDEN of ST . PETER ’s COLLEGE , RADLEY , 


 AND 


 ORGANIST and CHOIRMASTER of YORK MINSTER 


 HE ANGLICAN CHORAL SERVICE BOOK , 


 USELEY and MONK ’S PSALTER and 


 OULE ’s collection of 527 chant , 57 


 OULE ’S DIRECTORIUM CHORI ANGLI- 


 OULE ’ 's DIRECTORIUM CHORI ANGLI- 


 HE order for the HOLY COMMUNION , 


 now ready , 


 HE CONGREGATIONAL PSALMIST : 


 tune and chjrale . 


 NOVELLO ’S OCTAVO 


 edition of anthem 


 by various composer , 


 for particular season and 


 for general use , 


 edit by the REV . WALTER HOOK , 14 , 


 rector of LAVINGTON 


 will greatly rejoice in the LORD . 


 CH 


 e 


 HANDEL ’s 


 CHANDOS anthem 


 the 


 edit by DR . WILLIAM SPARK 


 NEW VOLUME . 


 for the ORGAN 


 arrange by JOHN HILES . 


 BOOK xix 


 BOOK xxi 


 BOOK xxii . 


 BOOK xxiii . 


 BOOK xxiv 


 BOOK xxv . 


 BOOK XXVI 


 BOOK XXVIL 


 SN arie 


 a new work for the ORGAN 


 CHAPPELL ’s ORGAN JOURNAL 


 consisting of favourite movement select from the 


 BY the 


 MOST eminent organist 


 CHAPPELL & C0 . ’s cheap publication 


 for various instrument . 


 CHAPPELL ’s instruction book 


 VIOLIN 


 FLUTE 


 CORNET . 


 ENGLISH CONCERTINA . 


 GERMAN CONCERTINA 


 PIANOFORTE . 


 CLARIONET . 


 HARMONIUM . 


 SINGING 


 DRUM and FD 


 VIOLONCELLO . 


 BANJO . 


 TROMBONE 


 HARMONY 


 GUITAR 


 SAX - HORN . 


 SERAPHINE ANGELICA 


 VIOLIN . 


 FLUTE 


 CORNET - A - PISTON 


 GERMAN CONCERTINA 


 ENGLISH CONCERTINA . 


 HARMONIUM 


 recommend by all the professor 


 CHAPPELL & CO . , 50 , NEW BOND STREET , LONDON 


 XUM