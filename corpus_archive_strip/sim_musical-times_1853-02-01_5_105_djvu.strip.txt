


1E MUSICAL TIMES


FOR THE MUSIC CONTAINED IN THE 103 NUMBERS 


SEE PAGE 122


ADVERTISEMENTS


NEW ORGAN MUSIChes Preludial Pieces for the Organ, 
in various styles ; intended as Introductory Voluntaries. 
Composed by Epwarp J. Horxins, Organist of the Temple 
Church. Addison and Hollier, 210, Regent-street, Also, 
Vol. 1, of Select Organ Movements—Vol. 2 in the Press

peer V. Home Music, or Con- 
gregational and Choristers’ Psalm and Hymn Book, 
containing selections from Handel, Haydn, Mozart, Beethoven, 
Winter, Paesiello, Himmel, Herold, Webbe, and Novello. 
Whole scarlet cloth, 6s

Price 7s. 6d. - > 
London: J. A. Novello, 69, Dean-street, Soho, & 24, Poultry




130


THE MUSICAL TIMES


SCOTTISH MUSIC


OF THE DANCE MUSIC OF 


SCOTLAND


A NEW EDITION


IMPORTANT MUSICAL WORKS 


OOSEY’S STANDARD OPERASFingering Ms “2 o—* Canti Popolari Toscana, 6 numbers, 3s, each

Ruth and Naomi” 3 o| FOUR NEW PORTRAITS of MENDELSSOHN, 
“ How beautiful,” *¢ Farewell Scotia,” and all the pieces BEETHOVEN, MOZART, and HANDEL, beautifully drawn 
originally published j in Mainzer’s Musical Times, at on stone by the most distinguished German and English Artists. 
per doz. - - - - 1 oOn fine boards, (size 25 in. by 20 in.) price 6s. each. These

Novello; Simpkin, Marshall, & Co.; London. 
Hime, & Co., Manchester. Menzies, Edinburgh




BLUED-STEEL SOSTENENTE TUNING FORKS, 


STANDARD ELEMENTARY WORKS. 


VACCAI’S PRACTICAL


CRESCENTINI, ZINGARELLI, CATRUSO, PAN- 


XUM


XUM


THE MUSICAL TIMES


131


THE MUSICAL TIMES, 


MOZART’S MASSES. 


SS 


132


THE MUSICAL TIMES


= 4 J. 


XUM


THE MUSICAL TIMES. 133No. 7 brings us, as it were, to a new era of the 
Masses. It is the first in which Mozart unites the 
four voices to a complete though small wind or- 
chestra. The score before us, in the hand-writing 
of the indefatigable Mr. Novello, made by him in 
the year 1819 from separate parts, is for two 
violins, tenor and bass, clarinetti, fagotti, and 
corni. We remember from the first publication 
of this work (which we think anticipated some of 
the earlier numbers) having a special delight in 
it, from the novelty and beauty of the style

There is a general elevation pervading it, with 
here and there passages of lofty genius. The in- 
troductory symphony is full of promise, and 
brings to mind a middle phrase in the slow move- 
ment of Beethoven’s ‘Sonata Drammatica,’ in

D minor. These four bars introduce the voices 
tutti piano— 
Pima Andante. -™ 
ae - 
Ss == 
| P| i 
Viol. p Pe oe we | 
e Bassi ! a. 
POPs ea ee eS 
‘ ——_—_— a

The corni go with the basses and tenors, the 
clarionets in octaves above the violins, the bas- 
soons are silent; the musician may thus imagine 
the score. The character of this beautiful open- 
ing is sustained throughout a Kyrie conspicuous 
for elegance in the solo parts and grandeur in the 
chorus ; and the Mass gains immensely in interest 
over others through the careful finishing of the 
second violin, tenor, and wind parts. Mozart has 
scarcely ever excelled the vigour and fire of the 
Gloria. Something extraordinary is always ex- 
pected from him at ‘ Qui tollis.” Here we have 
an unison accompanied by an orchestral phrase 
which anticipates Beethoven

7 Allegro. 
Vio. 1 &2. ——— 
Soprano. =. 
Alto. foxe) 
Tenor, = 
Bass. Sa 
Qui i p 
Viol. <p ao 
ON \ cee re ee ee ee 
e Bassi. 2 i — SS OooT—E oa

ma

Chromatic accompaniment such as this, to 
holding notes in unison, is peculiarly character- 
istic of Beethoven’s energetic orchestral style. 
It is pleasant to recognize the kindred genius of 
the two masters in such passages—one of which, 
casually heard, may have influenced the whole 
career of music. The ‘ Et incarnatus,’ 4dagio, 
in E flat, is a quartett for voices accompanied by 
wind instruments, in which the clarionets display 
their beautiful low tones. Mozart excels himself 
in this solemn and exquisite movement—the mo- 
dulations, cadences, and orchestral effects of 
which are so great, as to render praise impertinent 
and superfluous. Continuing throughout the 
Sanctus, the Mass maintains its interest and ori- 
ginality. The Benedictus, a vocal quartett in 
E flat, interspersed with chorus, is accompanied 
with the violins con sordini clarionets and bas- 
soons; the horns are silent. This movement is 
of extreme simplicity ; the voices move in notes 
of equal length, and principally in minims ; but

the unadorned melody makes its effect. The 
Agnus Dei is written in the triple measure of the 
Kyrie, to introduce it with new scoring in the 
wind instrument parts, and a new and most 
graceful termination




MUSICAL OBITUARY


134 THE MUSICAL TIMES


, - VERNON


THOMAS KELWAY


XUM


HORU: 


GLORY BE TO GOD ON HIGH


= == = SS = SS = ES 


L ~ 4


XUM


GLORY BE TO GOD ON HIGH. 


=S3 === = = 


| —— 3 — F——FFE 


GLORY BE TO GOD ON HIGH


QL 


WW


TR 


XUM


XUM


THE MUSICAL TIMES


139


SERVICES. 


ANTHEMS, Novello’s Cheap Music.—To Mr. Alfred Novello, 
the eminent music publisher, of Dean-street, Soho, the 
public are under considerable obligation. The enter- 
prise which tempted him some years ago to meet the 
economical spirit of the age by issuing a series of 
cheap reprints, has been followed by the diffusion of 
many of the best fruits of art in quarters where they 
could never have penetrated, had not the possibilities 
of the pocket been taken into consideration. The vast 
stores of ecclesiastical music possessed by the Pro- 
testant and Catholic Churches have hitherto been 
sealed to the many, but Mr. Novello has placed them 
within ordinary reach, and works which formerly were 
either costly in themselves, or expressed in the antique 
forms of the period in which they were written, have 
been lowered in price and edited with a, view to general 
intelligibility. To the sagacity thus manifested we 
have more than once borne testimony, when noticing 
the octavo editions of the oratorios issued by this dili- 
gent and prolific house, These publications appear

with periodical regularity, and the series now com- 
prises several of the greatest masterpieces which 
genius has bequeathed to the world. In the course of 
a few months this compact and portable edition will 
embrace the whole of Handel’s oratorios. It already 
includes the Messiah, Samson, Judas Maccabeus, 
Jephtha, Israel in Egypt, Joshua, the Dettingen Te 
Deum, and Solomon, while the three cantatas, Acis 
and Galatea, Alexander’s Feast, and the Ode to St. 
Cecilia’s Day, form another volume. The immediate 
speciality of these reprints is the extreme portability 
of the size in which they are published. None of the 
volumes which we have mentioned exceed half an 
inch in thickness, and they may be held in the hand 
as easily as an ordinary magazine. To secure this 
convenience as to size, typography of an unusuall 
small character was necessary, and the books in this 
respect are models of mechanical beauty. The musical 
type, though minute, is remarkable for its clearness, 
and neither perplexes nor distresses the eye. Printed 
in precisely the same attractive and comprehensive 
form, we also have Haydn’s Creation, and Mendels- 
sohn’s St. Paul, Lobgesang, and As the Hart pants, 
these last three masterpieces being brought into two 
neat volumes; while another volume is devoted to the 
Twelfth Mass of Mozart, the Third Mass of Haydn, 
and Beethoven’s Mass in C. Another series is the 
Glee Hive, a collection of popular glees, &c., published 
originally in weekly numbers, but now forming three 
light and portable books, delicately encased in crimson 
and gold. This selection, which contains 83 compo- 
sitions, is confined to the chefs d’euvre of Stevens, 
Callcott, Spofforth, Webbe, Wilbye, &c.—those glees 
and madrigals, in fact, the best known to the amateur 
glee singer. ‘It is hoped,” observes Mr. Novello 
in the preface, ‘that many a summer ramble and 
pic-nic will be made most musical by the presence of 
the Glee Hive well used. It should be remembered,” 
he continues, “that the effect on these occasions is 
better produced by some familiar favourite, cou- 
rageously trolled forth by those who feel at ease by 
frequent rehearsal, than by some less well-executed 
novelty.” The plan of the Hive is thus indicated, 
but we can but hope that at some future time it will 
extend its gatherings, and bring within the same 
narrow and economical compass other compositions 
whose merits at present are known only to the pro- 
fession. We cannot close this brief notice without 
again applauding Mr. Novello for the enterprise 
which he has exhibited in these cheap publications. 
The little volumes of which we have given a hasty 
record contain many of the imperishable treasures of 
genius; and thousands can now possess works for 
which they formerly yearned, and become familiar- 
ized by the fireside with the sublimest intellectualities 
of art. To facilitate the acquirement of these delights 
has been the aim of the publisher, and he has his 
reward in watching the growth of numberless public 
and private musical societies, and the cultivation of a 
purer and more rational taste. Music, in its better 
meaning, has made — strides in this country within 
the last few years, and every encouragement which is 
given to it by liberality of publication, and particularly 
by such boons as the reproduction, in a convenient 
and accessible form, of the great efforts of genius, 
challenges the gratitude of all who recognize the social 
ameliorations involved in the pursuit of a gentle and 
agreeable art.—Morning Herald, Jan. 7th, 1853

140




THE MUSICAL TIMES


TO CORRESPONDENTSif postage free, but our space might not always permit of

our using them. There are lives in English of Handel, 
Mozart, Beethoven, and Haydn. Novello’s Part-Song 
Book is completed in twelve numbers, printed in score 
and in parts

W. K.—You will see that Pierson’s oratorio, Jerusalem, can 
be heard in London on the 7th of April, the Harmonic 
Union having announced their intention of giving it at 
that time

Brief Chronicle of the last Month

Harmonic Unton.—The second concert of this new 
association was given on the 20th, at Exeter Hall. The 
programme included Beethoven’s Ruins of Athens, the 
Walpurgis Night, and a general selection. The spirit and 
ability exercised by Mr. Benedict, the conductor, appear 
to be now appreciated, and the successful. issue of this 
concert must be gratifying to all concerned. A new 
trombonist, Herr Nabich, made his appearance—he is a

layer of extraordinary capacity, and will become “a 
ion” as a soloist, during the concert season. A young 
pianist, Mr. W. Mason, created a favourable impression 
by the neatness of his performance of Weber’s Concert 
Stiick The band was in first-rate condition under its ad- 
mirable conductor. A melancholy event (towhich we have 
alluded more specially in another part of our paper) at the 
rehearsal in the morning, threw a gloom over the mem- 
bers of the orchestra,—the sudden death of Mr. Harper, to 
whose memory the “ Dead March” in Saul was performed 
in the evening. The directors of this society have de- 
cided to produce Mr. Pierson’s oratorio, Jerusalem, (which 
was so successful at Norwich,) on the 7th of April—the 
intermediate time will be devoted to studying the choruses, 
and by that time Madame Clara Novello will have re- 
turned to England, to whom an engagement has been 
offered. This oratorio will have the care and preparation 
bestowed upon it which its great merits demand. Our 
readers will call to mind the extract, “Blessed are the 
dead,” presented in our 103rd Number

Hotywetit. — The Philharmonic Society established 
here gave a performance at the end of December, under 
the direction of Mr. Jabez Jones. The first part was a 
selection from the works of Handel and Haydn, the 
second was of a miscellaneous character. Several pieces 
were encored; and the room was densely crowded

BurnsaLu.—A concert of sacred music was given on 
the 25th of December, in the Church School Room, by 
the pupils of Mr. William Green, of Greenhowhill, as- 
sisted by several amateurs of the surrounding neighbour- 
hood. 'The concert consisted of selections of sacred music 
from Handel, Haydn, Beethoven, and from Jackson's 
Deliverance. Mr. Thomas Thorpe was the conductor

Brieuton.—The Rev. Thomas Helmore gave an in- 
teresting lecture on ‘‘ The Music of the Church” early 
in December, at the Pavilion. Several specimens of 
church music were performed, before a numerous auditory




XUM


THE MUSICAL TIMES. 141


142 THE MUSICAL TIMES


SIR JOHN HAWKINS’S 


GENERAL HISTORY


THE SCIENCE AND PRACTICE OF MUSIC


XUM


XUM


THE MUSICAL TIMES. , 143


SACRED. 


PSALMODY


SECULAR


PART THE SECOND


PART THE THIRD


144


THE MUSICAL TIMES


DURING THE LAST MONTH, 


(.A.7.B.) 2 3/0 BS} >2 “0 