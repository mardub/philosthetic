


 — — — _ _ _ _ _ 


 BASS 


 E.C 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 PUBLISHED MONTH 


 LEEDS 


 — TRIENNIAL MUSICAL FESTIVAL 


 ASSOCIATION 


 SEASON , 1889 - 99 


 SHOREDITCH TOWN HALL , 


 CALLIRHOE 


 BAND CHORUS 230 PERFORMERS 


 WANDSWORTH PHILHARMONIC 


 SOCIETY 


 SEASON 1859 - 90 


 BAND CHORUS 150 PERFORMERS 


 VWIIM 


 SEPTEMBER 1 , 1889 


 | NOVELLO ORATORIO CONCERTS . 


 CHOIR . 


 FINSBURY CHORAL ASSOCIATION _ 


 METROPOLITAN COLLEGE MUSIC 


 R.A.M. CLUB 


 COLLEGE ORGANISTS 


 GUILD ORGANISTS 


 89 , CHANCERY LANE , LONDON , W.C 


 XUM 


 XUM 


 516 


 ORRESPONDENCE COLLEGE MUSIC , 


 IANO , HARMONY , COUNTERPOINT . 


 TANTED , ALTO TENOR , 


 LTO WANTED , 


 JOCALISTS , SOLO INSTRUMEN TALISTS , 


 12 


 BASS VOICES , _ 


 GENTLEMAN ( 16 


 XUM 


 STER | 


 1889 . 517 


 PIANOFORTE 


 B USINESS . 


 ASSISTANT 


 MENT 


 JANTED , 


 24 


 “ ORG 


 O RGANS SALE 


 HIRE 


 RGANS 


 MU SIC AL SOCIETIES PROFESSION . 


 * ECOND - HAND STEINWAY CONCERT 


 COLLIN -MEZIN PARIS . 


 MUSICAL 


 T ° COMPOSERS.—MARRIOTT & WILLIAMS 


 T R 


 1889 


 UTHORS ' WORKS CORRECTED , REVISED 


 INV 3 


 MILITARY , BRASS , FIFE DRUM 


 BAND MUSIC . 


 “ BRASS BAND NEWS 


 D'ALMAINE 


 JAMES CONACHER SONS , 


 ORGAN BUILDERS , 


 BATH BUILDINGS , HUDDERSFIELD 


 NICHOLSON CO . , 


 ORGAN BUILDERS , 


 PALACE YARD , WORCESTER 


 ESTABLISHED 1541 . ) 


 ISED 


 SALES 


 XUM 


 MUSICAL TIMES . 


 NEWLY REVISED ILLUSTRATE ! TALOGI EE . 


 OLD FIRM . 


 CHURCH CHOIR GUILD , LONDON 


 WELLINGTON STREET , STRAND , W.C 


 PATRONS : 


 TRINI TY COLL EGE LON \DON . PU BLISHED DAY 


 


 SHORT CHURCH CANTATA 


 CLASSES LECTURES . WORDS WRITTEN COMPILED 


 POEM WRITTEN 


 MRS . HEMANS 


 SHILLING BOOK 


 NEW CAREFULLY REVISED EDITION ORGAN AR R ANGE MENTS 


 FRC 


 SCHUMANN MENDE LSSOHN 


 ORIGINAL COMPOSITIONS | ELIJAH 


 300 : 


 COMPOSED LY 


 


 SELECTED SCHEM ELL COLLE CTION , | CHORUS , SEMI - CHORUS , ORGAN 


 ARRANGED VOICE PIANOFORTE | WORDS SELECTED WRITTEN 


 ACCOMPANIMENT , J. FRANCIS WALLER , LL.D. 


 XUM 


 TIVAL 


 VAL , 


 


 YUM 


 MUSICAL TIMES 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 SEPTEMBER 1 , 1889 


 ANGLO - CANADIAN COPYRIGHT . 


 STRATE OP 


 522 


 PIR 


 XUM 


 1868 , 


 IS65 


 YUM 


 MUSICAL TIMES 


 524 


 XUM 


 LADIES ’ SURPLICED CHOIRS 


 EP 


 SE 


 XUM 


 MUSICAL TIME 


 S.—SEPTEMBER , 1889 . 527 


 POE AR APSS 


 528 


 VALUE SECLUSION . enrich intellect , solitude true 

 school genius . truth saying 
 universal application , 
 province hold good greater force 
 musical composition . look 
 maiter @ priori @ posteriori stand- 
 point , shall find results enquiries 
 conform Gibbonian maxim . , 
 , imagined , recommending 
 composers , especially native 
 pay attention salutary precept , 
 engaged task , French , 
 staving open door . accused 
 preaching self - evident truism ; 
 reminder needed , firmly believe , 
 justification demanded offering advice — 
 thing genius given accepting . 
 said outset , whichever way look 
 matter , appropriateness Gibbon dictum 
 music evident . view apart 
 details , obvious com- 
 poser — , , entirely selt - sufficing 
 personage — work best , desert 
 island lighthouse , free 
 interruptions society roar jangle 
 town life . original incentive composition 
 engaged nature 
 humanity — landscape ladylove — contend 
 sounds subject - matter trans- 
 lates likely beautiful appro- 
 priate composer undisturbed 
 . ‘ intrusion mundane noises 
 good deal , , ; 
 ear composer hears mental 
 iunecy ear , instances musicians 
 — Mozcart , instance — write 
 taken shape brains sorts dis- 
 cordant noises going . 
 silence great boon composer , 
 solitude greater . suppose 
 soughing breeze roaring waves 
 seriously interfere flow 
 composer inspiration — reverse ; 
 rattling cabs , unseasonable caterwauling 
 cats , crash incoming coals , long- 
 drawn blasts street cornet - player , hardly 
 argued stimulate sooth imagination . 
 , turn general abstract 
 speculation value solitude concrete 
 examples , shall find practice 
 eminent composers entirely bears . 
 conditions Haydn surroundings undoubtedly 
 secure leisure quiet great 
 years life . Mozart case peculiar . 
 power abstraction remarkable 
 exempt necessity isolation . 
 read special predilection sitting 
 late night play , found brain worked 
 better quicker open air . isolation 
 chief features Beethoven life . 
 urged necessity , 
 right ground regard virtue . , 
 far creative faculties concerned , 
 doubt solitude essential 
 free play . devoted nature country , 
 inspirations come readily 
 solitary rambles summer , 
 spent country . Sir James Crichton 

 Browne , address recently read British 
 Medical Congress , stated opinion 

 MUSICAL TIMES.—Sepremser 1 , 188 g 

 Beethoven summer creative time . 
 winter , told friend , little writing 
 scoring composed summer . Beet . 
 hoven isolation , , materially enhanced 
 physical infirmities . cuts man 
 fellows deafness . 
 wanting urge great composer 
 | calamity altogether unmixed evil , 
 | Mr. Finck , essay * * Composers work , ” 
 |points musicians respect 
 } fortunate painters . “ Titian lost eye . 
 isight painted picture , 
 | Beethoven , losing principal sense , 
 continued compose better . Mr , 
 | Thayer thinks , purely artistic view , 
 | Becthoven deafness advantage 
 [ ; compelled concentrate 
 | thoughts symphonies head , undisturbed 
 harsh noises external world . ” Weber 
 | Wagener greatly addicted solitary walks . 
 | Mendelssohn sociable men , 
 j finest work outcome 
 | direct communing Nature revealed 
 | forests floods caves . ‘ long 
 ‘ lonely rambles Italian hills Berlioz 
 | describes charm Memoirs 
 |fertile musical thoughts . come 
 | present time , shall find leading composers 
 jof day constituted like predecessors , 
 | Verdi leads life country gentleman , rarely 
 jemerging solitude entertaining 
 | visitors . Admirers Grieg remember loves 
 |to compose little cottage built 
 | secluded spot bank 
 imost beautiful fjords Norway . , finally , 
 | Brahms need solitude quiet country 
 |is imperative engaged creative 
 | work . stories told ingenious 
 devices endeavours secure immunity 
 noise interruption 

 theorises matter , regards 
 point view history , results , 
 ‘ seen , . naturally 
 expect great creative minds enamoured 
 isolation , generally found 
 . Let , , supposed aim 
 urge student composition 
 wilderness hope divine afflatus 
 fall . Solitude create 
 genius society destroy . 
 |fact , ability intellectual retreat , 
 las , shut oneself fellows 
 mark self - sufficing independent nature 
 given possess . average 
 man gregarious , likes company kind 
 work play . able live 
 test originality strength purpose . 
 great people frequent places amusement 
 reason assigned Dr. Johnson — 
 afraid sit home think . 
 bored minds ill - stored 
 subjects reflection , depressed intrusion 
 harassing worrying thoughts . Happy , 
 case , creative 
 instinct fall , indulgence 
 brings keenest intellectual pleasure 
 mankind capable . self - absorption 
 original musical composer , work , 
 compared great mathematician engaged 
 transcendental problems . , , 
 quote sensible remark Mr. Finck , “ 
 literature blame writer , expression 
 goes , ‘ evolves facts inner consciousness , ’ 
 music proceeding evidence highest 




 SSOTS , 


 ‘ , 


 YUM 


 MUSICAL CLEFS ABOLITION 


 53 


 CNL 


 XUM 


 XUM 


 MUSICAL TIMES 


 SEPTEMBER , 1889 . 531 


 HIT 


 | \ 


 E ! 


 YIIM 


 MUSICAL TIMESSEPTEMBER , 188 g. 533 

 indicate difference written | ( Beethoven gives note 

 produced sounds instruments , transposing , | sing 

 instead S 

 expected accept 
 needed improvement . ‘ gives score 
 bar concluding Prestissimo Beethoven 
 “ Choral Symphony 

 , naively observes , “ exceptionally 
 complicated . ” “ Present Method ” “ Pro- 
 posed Method ” printed ; new 
 treatment like commented cn ; 
 notes , voices orchestral 
 instruments , G clef , respective octaves 
 indicated figures underneath . looks odd , 
 anomalous , note tenor placed 




 534 


 MUSICAL TIMES 


 SEPTEMBER , 1889 


 GLOUCESTER MUSICAL FESTIVAL 


 XUM 


 XUM 


 535 


 CASTLE 


 HYDE 


 XUM 


 


 XUM 


 537 


 FACTS , RUMOURS , REMARKS 


 538A stiLv astonishing peculiar 
  jathotniation given answers History 
 |questions . ‘ set forth , ‘ 
 | list English composers chronological order 
 | living year 1820 . Mention 
 | work . ’ question allowed latitude , 
 |in hope bring knowledge 
 |the names works native musicians 
 |the second decade present century . 
 | answers proved scope allowed con- 
 sidered sufficient . statements 
 showed comprehensive patriotic grasp 
 subject 

 names Beethoven , Grann , Clementi , 
 Andreas Romberg , Gade , Schubert , Mozart , Wagner , 
 Stephen Heller , Bezet , Liszt ( spelt Lizst , 
 Lizts ) , Brahm , Spohr , Anthon Rubistein , Hadyn , 
 Piccini , Glick ( sic ) , Weber , Chopin , Hérold , Monte 
 Verde , Mendelssohn , Lully , Gounod , Ambrose 
 Thomas , Carissimi , Rameau , Donizetti , Barnby , 
 called Branbey , Dr. Bridges , meritorious 
 organist , Cowen , remarkable composers 
 Cerney , Mechencie , Divoke , Cotch , Dr. Harn , Percel , 
 Juskin du pres , Mucann , composer “ Calirrhoe , ” 
 Summicheal Costa , given 
 Ienglish composers 

 Somer works attributed native musicians 
 |are worthy mention . Bull wrote ‘ Rule , Brittania ” ; 
 Arne wrote ‘ Able ’ ; Barnbey , ‘ Hezekia ’ ; Chopin , 
 ‘ Les deux journées ’ ; Balfe wrote ‘ La Sonnanbula ’ 
 } ‘ Boheimain Girl ’ ; Handel wrote ‘ Idomeneo ’ ; 
 Liszt wrote ‘ Regatta Venezuila ’ ; wrote ‘ Fuguis ’ 
 ‘ Dido Inias ’ ; Bennet , ‘ Barchoral ’ ; Arne 
 wrote anthems church music , Goss wrote masses , 
 Beethoven wrote waltz . ‘ facts ’ 
 derived inner consciousness writers , 
 /some traced ‘ meritorious text - books 

 Thr answers question , ‘ Rossini ? 
 influence exercise art music 
 time ? ’ brought light curious 
 interesting intelligence . nationality . 
 | ‘ German birth , born Pesaro 
 Italy ’ ; ‘ born 1670 died 1826 ’ ; 
 ‘ Frenchman , ’ ‘ noted writer French , ’ 
 place nativity ‘ Pizzarro Genoa ’ ; 
 ‘ Italian , people feel drunk 
 |sparke richness melody ’ ; composed 
 ‘ Oberon , ’ ‘ Don Giovanni , ’ ‘ Der Frieschutz , ’ 
 |‘Stabet Matar 




 1OUS . 


 20 ) " 


 USIC 


 VIIM 


 539 


 540 


 100 


 VIIM 


 SSTS . 


 ROYAL 


 ACADEMY MUSIC 


 500 


 BAYREUTH FESTIVAL 


 PLAYS 


 542 


 MUSIC ITALYand marvellous finish pianissimi , 
 Italy altogether unknown , Cologne Union 

 Albert Hall London , ofan adequate |to despise mesza - forte 
 size performances Beethoven Choral | appropriate use ought . 

 Symphony , programme Cologne Choral Union , jon hand , perfect intonation , 




 S01 


 130 


 POO ! 


 MRS . CHARLES BRIDGEMAN 


 62 E = 


 — _ — _ 


 BET 5 CTT EI 


 4 9 - 6 4 


 XUM 


 DREN 2 PS 


 TT 


 4 ~ — ~~—+ _ 


 1889 , 


 XUM 


 548 . MUSICAL TIMESSEPTEMBER , 188 g 

 identity assumed names . ‘ “ Series } Current reports indicate busy musical season . 
 Household Songs , ” ‘ ‘ Summer Holiday , ’ | abouts . Glasgow Choral Union scheme 
 “ Golden Band ” series attained enormous sale | wrought detail , taken 
 United States . | Mr. August Manns return scene artistic 
 M. Vastin , died age ninety- | achievements , band include best 
 , 5th ult . , Saint Julien - sur - Sarthe , strange | talent induced migrate north Tweed 
 present generation . Thirty years ago retired / winter months . orchestral programmes 
 active life , serving professor Conservatoire | , course , referred , following choral 
 thirty years . officiated violoncello soloist | works found scheme : — ‘ Messiah , ” 
 opera 1814 — months Waterloo fought . | Beethoven Mass D , MacCunn cantata ‘ Lay 
 ‘ regret announce |}the Minstrel , ’ ‘ * Cameronian Dream . ” 
 death Mr. Roperr Aucustus Arkins , Organist |The - named work , pen young 
 Cathedral St. Asaph . born October 2 , 1811 , | Greenock composer , commissioned Messrs , 
 appointed post held credit | Paterson , Edinburgh . chorus , baritone solo , 
 year 1834 . died 3rd ult . |orchestra . choral ballad shape new setting 
 “ Ye Mariners England ” brought 

 Choral Union . composer , Mr. Duncan , regarded 




 CORRESPONDENT 


 CORRESPONDENT 


 XUM 


 MUSICAL TIME 


 MUSIC ENGLAND 


 WEST 


 XUM 


 55 


 |C. T 


 MR 


 TH 


 TH 


 XUM 


 MUSICAL 


 TIMES 


 55 


 REVIEWS 


 NIU 


 552 


 1889 


 1SS8 , 


 GF * ¢ 


 XUM 


 553 


 XUM 


 554The Service Holy Matrimony , Westminster 
 Abbey . { | Novello , Ewer Co 

 Tuis includes single chant , adapted Beethoven 
 Turle , Psalm , arrangement Tallis Ferial 
 use responses ; hymn , “ Father Life , ’’ 
 Rev. S. Flood Jones , set tune Turle ; Dr. 
 sridge simple effective Anthem , ‘ ‘ blessing 

 
 
 




 FOREIGN NOTES . Hamburg , trom gth 13th inst . , direction 

 Dr. Hans von Bulow , representative 
 German character , including works Philipp Emanuel 
 Bach ( Symphony F major ) , Haydn , Mozart , Beethoven , 
 Mendelssohn , Meyerbeer ( Overture , written inau- 
 guration London Exhibition 1862 ) , Wagner , 
 Brahms , Johann Strauss ( valses , ‘ * Volksanger ” 
 ‘ “ Phénix - Schwingen ’’ ) . interesting scheme 
 set forth , popularity Conductor , combined 
 - known hospitality Hamburgers , fail 

 attract cosmopolitan audience music - lovers 

 composed ‘ Czar und Zimmermann , ” 
 opera fame chiefly rests 

 series public lectures musical subjects 
 delivered Berlin University approaching 
 winter term , including Professor Bellermann discourse 
 ‘ * Music Ancient Greece ” Professor Spitta 
 ‘ Chamber Music Death Beethoven , ” 
 widely divergent subjects , handled , , 
 competent authorities 

 novelties produced new 
 season Royal Opera Berlin , Ponchielli 
 ‘ * Gioconda ” Heinrich Hofmann charming opera 
 “ Aennchen von Tharau 




 XUM 


 555M. Ambroise Thomas opera “ * Hamlet ’ ? produced 
 fiftieth time month Imperial Opera 
 Vienna , institution Wagner ‘ * Lohen- 
 grin ” met 250th performance 

 forthcoming performances classical plays 
 Paris Odéon Theatre include Shakespeare ‘ * Merchant 
 Venice , ’ music M. Gabriel Fauré ; ‘ “ * Twelfth 
 Night , ” music M. Widor ; ‘ * Ado 
 , ” music M. Benjamin Godard ; 
 Goethe ‘ ‘ Egmont , ” ’ Beethoven music . M.Lamou- 
 teux Conductor 

 violins belonging late eminent violinist , M. 
 Alard , sold Paris-—viz . , Stradivarius 
 £ 2,000 , Stainer £ 260 




 CORRESPONDENCE 


 LADIES ’ SURPLICED CHOIRS . 


 EDITOR “ MUSICAL TIMES 


 556 


 EDITOR “ ‘ MUSICAL TIMES 


 EDITOR ‘ MUSICAL TIMES . ” 


 MUSICAL DIPLOMAS . 


 EDITOR ‘ * MUSICAL TIMES 


 NOTATION 


 EDITOR ‘ MUSICAL TIMES . ” 


 SIR 


 XUM 


 STON 


 LU SICAL 


 557practised - - days , rightly . best 

 different manuals 
 
 scales played absolutely legato . “ Es geniizt 
 wenn aufsteigender Scala beim Uebersetzen der obere , 
 absteigender Scala der untere Ton des Doppelgritts 
 gebunden wird ” ? ( Lebert Stark ) . best 
 auricular illusion . goa step , 
 allow use thumb consecutive keys 
 executing double thirds ? actual practice 
 wide field 
 scientific discussion . Previous Chopin , double thirds 
 sixths written Toccatas 
 Studies . Evenin Beethoven Sonatas mind 
 |no extended scale passage kind 

 moot point fingering chromatic 




 PIANOFORTE FINGERING : PLEA | 


 GREATER UNIFORMITY . 


 EDITOR ‘ * ‘ MUSICAL TIMES 


 AUTHORS ’ NAMES PROGRAMMES 


 EDITOR ‘ * MUSICAL TIMES 


 GEORGE F. SHARP 


 MUSICAL SIGNS . 


 EDITOR ‘ * MUSICAL TIMES 


 538 


 2 , 1889 


 P/ ATE NT INTE LL IGE } NCE 


 CORRESPONDENTSis taken Larg 

 Y D Beethoven 

 exon — Tenor parts Rossini 




 BRIEF SUMMARY COUNTRY NEWS 


 MISS 


 XUM 


 MONTH . _ | : CONTENTS . 


 560 


 AMES BLAMPHIN GRAND HARP RECITAL 


 LIVERPOOL CONCERT PARTY 


 70 . MU SICAL STUDENTS 


 S.W 


 URPLUS 


 RGANIST WANTED 


 JANTED , 


 R.S.O 


 VW 


 CHOIRMASTER 


 150 


 DAILY STUDY 


 JIANOFORTE STUDENT 


 THOU CROWNEST YEAR 


 THOUSAND ) , 


 30 


 XUM 


 . MO- 


 SEMI 


 ENT 


 UDY 


 AR 


 GER 


 IRTE , 


 ARNIM , 


 UTHOR 


 CONDUCTED | 12 , BERNERS STREET , LONDON , W 


 LONDON PROVINCIAL 


 MARIE TITIENS 


 PIANOFORTE . 


 \SLEY 


 NEW EDITION.—NOW READY 


 GLEE PARTY 


 ORCHESTRA 


 WESTMIN \STER ABBE Y QUINTET 


 SINGING , ORGAN , PIANOFORTE LESSONS . | DEDICATED EXPRESS PERMISSION 


 MR . JOSEP CANTOR 


 “ GEMS OPERAS 


 CONCERT COMPANY 


 ADDITIONS SON 


 A. C. MACKENZIE 


 BOOKS . 


 YUM 


 SERVICE _ | CHURCH MUSIC _ 


 J. FREDERICK BRIDGE , . , SECUL AR VOCAL . 


 ‘ AY , WATCHMAN , NIGHT ? 


 SAITH LORD , KING ISRAEL 


 OFFICE HOLY COMMUNION . PARTS . 


 RAL 


 563 


 HARVEST ANTHEMS & HYMNS , 


 HYMNS TUNES 


 HARVEST 


 SELECTED “ 


 HYMNARY . ” ’ 


 PRICE PENNY 


 LO ! SUMMER COMES 


 HARVEST GENERAL USE 


 HARV EST 


 | THANKSGIVING MARCH 


 ‘ J 


 BAPTISTE 


 ANTHE 


 CALKIN 


 XUM 


 ITHE GLEANERS ’ HARVEST 


 CANTATA FEMALE VOI 


 JUBILEE CANTATA 


 SOLO VOICES , CHORUS , ORCHESTRA 


 C. M. VON WEBER . 


 SOW ING D REAPING 


 HARVEST CAROL . 


 J. MAUDE 


 HARVEST 


 . A. S¥D!I 


 CRAMENT 


 NTHEMS 


 TAM 


 SING UNTO TH 


 ANTH HAR 


 EARTH 


 ALBERT LOW 


 ALBERT LOWE HARVEST CARO ! 


 NEW HARVEST ANTHEMS 


 B 1\ LARI 


 THI [ 


 564 


 HARVEST MUSIC 


 H ARVEST FESTIVALS 


 MAN W OUL D PRAISE 


 NEW HARVEST CAROL . 


 MUSIC 


 HENRY T. TILTMAN . , 


 LORD 


 NEW HARV EST M , 


 SING UNTO LORD THANKS- 


 ANTHEM 


 LORD HATH PREPARED 


 COMPOSED 


 FE 


 RUTH 


 HARVEST PASTORAL 


 WORDS 


 EDWARD OXENFORD 


 MUSIC 


 ALFRED R. GAUL 


 SUITABLE 


 THANKSGIVING SERVICES 


 PERFORMED TOWN HALL , BIRMINGHAM , 


 OCCASIONS . 


 H ARVE ST FESTIVALS 


 MAGNIFICAT NUNC DIMITTIS 


 


 LEWIS ELWYN LEWIS . 


 A. MACFARRE N CANTATE DOMINO 


 XUM 


 


 OUSAND 


 HAM 


 TIS 


 MINO 


 MLW 


 565 


 KING ARTHUR . 


 DRAMATIC CANTATA | 


 WORDS WRITTEN 


 JAMES SMIETON , 


 MUSIC COMPOSED 


 JOHN SMIETON 


 M.A 


 SCOTSMAN 


 SCOTTISH LEADER 


 DUNDEE ADVERTISER 


 DUNDEE COURIER 


 GREAT SUCCESS NEW CANTATA . 


 BONNIE KILMENY 


 CANTATA 


 SOLI , CHORUS , ORCHESTRA 


 MUSIC 


 SAMUEL 


 CANTATA CHOIR CONGREGATION 


 


 LANGDON COLBORNE 


 XUM 


 


 


 CAPTIVES BABYLON 


 SUITABLE CONCERT ROOM CHURCH . ) 


 COMPOSED 


 SCHOOL CANTATAS SONGS 


 SES 


 ) MINUTES ’ E 


 USE SINGING - CLASSES SCHOOLS 


 XNERCISES 


 COMPILED ARRANGED 


 FLORENCE A. MARSHALL . 


 VOLUNTARIES CELEBRATED PIECES 


 HAKMONIUA 


 KING EEA L ; LYR 


 XUM 


 — _ _ 


 ; NOVELLO 


 | PIANOFORT 


 UBLISHED 


 ZSWER CO.W 


 ALBUMS 


 _ — NOVELLO , EWER CO . 


 NOVELLO , EWER CO . 


 ED 


 MERRY MONTHS 7 


 COMPOSED 


 XUM 


 BERTHOLD TOURS 


 ALBUMS 


 LE LIADOFF 


 NATOLE LIADOFF 


 LIADOFF 


 568 


 MATCH EDITION 


 BOOK PSALMS ( BIBLE VERSION ) , 


 ANGLICAN HYMN - BOOK 


 HOME HYMN - BOOK 


 POCKET EDITION 


 BRISTOL TUNE - BOOK 


 PERFORMED LEEDS FESTIVAL 


 REQUIEM 


 


 SOLO , CHORUS , ORCHESTRA 


 


 JOHANNES BRAHMS . 


 MALE VOICE CHOIR.—THE NATIONAL 


 ANTHEM , 


 USELEY MONK 


 | PSAL TER 


 ANGLICAN CHORAL SERVICE BOOK 


 7 . ANGLIC : CHANT - BOOK . 


 OULE COLLECTION 5 


 PSALTER , PROPER PS . ALMS , HYMNS 


 OULE DIRE CLORIU M CHORI- ANGLI . 


 OULE DIRECTORIUM CHORI ANGLI 


 ORDER HOLY COMMU NION 


 READY 


 


 MENDELSSOHN 


 ELIJAH 


 PRICE SHILLING 


 


 OCTAVO EDITION 


 LONDON & NEW YORK : NOVELLO , EWER & CO 


 POIN 


 LOD 


 XUM 


 SECONDE SERIES 


 


 CANTICLES HYMNS 


 CHURCH , 


 POINTED CHANTING , SET APPROPRIATE ANGLICAN CHANTS , SINGLE DOUBLE 


 


 RESPONSES COMMANDMENTS 


 REV . SIR F. A. GORE OUSELEY , BART . , M.A 


 EDWIN GEORGE MONK 


 5S 


 LONDON NEW YORK : NOVELLO , EWER CO 


 MONTHS 


 VOCAL DUETS SCHOOL USE 


 MUSIC 


 EXEMPLIFIED RECORDS HOUSE T 


 


 PORTRAITS TH R EE SACRED SONGS 


 VINCENT NOVELLO , J. ALFRED NOVELLO , VOICE PIANOFORTE 


 


 HENRY LITTLETON . 


 COMPOSED 


 GERARD F. COBB 


 16 . 


 _ — — PRICE SHILLINGS NET 


 OPINIONS LONDON PROVINCIAL PRESS 


 


 A. R. GAUL CANTATA 


 JOAN ARC 


 


 REPEREE 


 ~BESS 


 TEWKESBURY REGISTER . 


 NEWCASTLE - - TYNE DAILY CHRONICLE 


 CRYSTAL PAL ACE REPORTER . BIRMINGHAM DAILY POST . ’ 4 


 » 6 


 LONDON NEW YORK : NOVELLO , EWER CO 


 1889 


 POPULAR PUBLICATIONS 


 VESPER VOLUNTARIES 


 ORGAN , AMERICAN ORGAN , HARMONIUM 


 VOL . I. 


 » 3 ” II W. 1 € 


 VOL . II 


 VOL ... 


 12 


 VOL . IV . 


 BR 


 


 22 . F 


 23 . F 


 


 TATA 


 BANJO TUTOR 


 GUITAR TUTOR . 


 CORNET JT 


 HAKMONIUM 


 


 1F ROBINS 


 ORSBORN TUCKWOOD 


 CHRISTIAN PILGRIM 


 SACRED CANTATA 


 PREFACE 


 


 ¥ 22 , 


 [ WO - SONGS 


 ARRANGED LAD ! VOICES , USE 


 SONGS FAIRIES . B ; 


 COLLEGE SERIES . 


 ) 5 : CANTERBURY SERIES TRIOS . 


 ORSBORN TUCKWOOD 


 64 , BERNERS STREET 


 LONDON , W 


 STANDARD > 


 SERVICES ANTHEMS 


 J. WESTBROOK 


 PUBLISHING COMPANY 


 EASY HARV EST MU SIC 


 HARVEST HYMNS 


 WORDS 


 REV . S. CHILDS 


 MUSIC 


 DEAN ALFORD , MR . LANGRAN , 


 A. H. BROWN 


 CLARKE 


 DR . DYKES 


 II 


 15 


 ORIGINAL 


 ORIGINAL 


 MESSIAH 


 M ACE ARREN 


 SIR GEORGE 


 CREATION 


 PERFORMING EDITION , 2 


 SIR GEORGE MACFARREN . 


 GEORGE C. MARTIN . 


 ORIGINAL 


 DOLA 


 DRAE 


 FRES ' 


 FUCH 


 GIGOL 


 HAND 


 MORLEI 


 PETER 


 RAVIN , 


 WAGNI 


 LONMEW FOREIGN PUBLICATIONS . | REVIEW 

 BEETHOVEN.—Five pieces ( Marches , Polonaise , | 
 Ecossaise ) , Military Band . Arranged Pia noforte 

 5.4 Duet , Accompaniment Big Drum , Drum , | yy . Y ’ r ‘ 
 ¢ Triangle , Cymbals , C , Burchard . ee 9 0 N k \\ \ Ok } \| [ \ | ( \ ] 
 2 % _ — Pianoforte Duet , Accompanime nt 6 - ew \ n J y 
 BEHR , F.—Frihlings Boten . ‘ easy little pieces | : 
 Io Pianoforte : — .yR ‘ y 
 ¢ Bookl . .. xe Ss ‘ 9 aus § OC ! \ 4 ) 
 ¢ Book II . .. ‘ “ - ea v6 ee : ae CON ‘ 42ib 
 ; BEMBERG , H. — ‘ Joy anal Song , English words . | ee 
 “ | High low voice : eh -- met 2 0 ] SOREAISINS 
 
 ey ; BLES , A.—Andante Polka Maz aia . “ Cornet Solo , PROGRAMMES NOTEWORTHY OCCURRENCES 
 ee , Pianoforte Accompaniment .. - - » net 2 6 ‘ Sat eaeney 
 DANBE , J.—Six Salon Pieces Violin Pian 1oforte : NUMEROUS CRITICISMS , 
 No.1 . Second Andante appassionati “ ec a+ § ; 
 aa » 2 Rondo caractéristique ae ie ax . 1 0 tY 
 +4 » 3 Jeanne Valse .. ee Pe er ~ > ~ - 
 . f , 4 Gavotte de Lulli ae ae wa oe oO H. k. KREHBIEL . 
 5 . Menuet ( style ancien ) 33 , oe 
 6 . Hymne ( mélodie russe ) , w ith ‘ é Lien ae 
 ; ; oe ee ee . é ii oe 
 DOLMETSCH , A.—Rondeau . Sar Vi iolin Pi : unoforte .. 4 0 Vottme I. , 1885—1886 | Vora FE , 2 97 — 1965 
 — Sérenade . ie Violin Pianoforte .. P 3 ¢ = Il .. 1886—1S87 . IV .. 1&8 rSS8o . 
 — “ La Malinconia . ” Melody Viola Violoncello Ch _ 
 Pianoforte ee ey es . — _ — _ — _ — 
 — “ risen . ” Sacred Song f Voice Pianoforte , NEW YORK & LONDON : NOVELLO . EWER CO 
 Violin Organ adlib . .. ae = os Met 3 4 oi eee “ 
 DRAESEKE , F.—Serenade . small Cuchaates , Op . 49 : 
 score .. ae = = eo “ SER TE 4 PRESS NOTICES 
 s art ai ne » 24 & l. , hase et J 
 ; FRESCOBALDI , H.—Collectio Musicus Organicw . Selected eee 
 Organ compositions . E dited X. Haber ! net I4 0 : oo : ee : ay ? 
 2 FUCHS , R.—Waltzes . Pianoforte Duet . Op.25 . Arranged lez nd ; ican , 
 Orchestra , R. eakstyctih ? nelis f unimp ] rad literary 
 Book I. Score .. ae 34 oe net S Oj cty ry > \ ed h critics 
 ‘ Orchestral parts > « | | : 
 Book II . Score .. 
 xe Orchestral parts ‘ ae 
 GIGOUT , EUGENE.—Sui de trois Morceaux ‘ Marche 
 : rustique , Lied , Marche de fete ) Organ ae net 4 
 ot — Meéditation . Violin Orchestra : 
 rag score 
 Orchestral parts 

 LONDON & NEW YORK : NOVELLO , EWER CO . noblest arts ork Tis 
 s : “ opportune occasion , possible , 
 ; eA 

 READY . deal Mr. Krehbiel criticisms . Dissenting 
 ” 0 certain matters , unqt lestionz ably product remarkable 
 BOOK XII independence insight . criticism Mad : ime Patti Carmen 
 2 acer positively refreshing , remarks ( pp . 201—204 ) Beethoven 

 d TATCN belief metronome , connection Herr Seidl interpretation 
 Sal \V OLU N RI ES symphonies , conservative antagonism mere law- 
 - lessness compatible itense faith advanced 




 GEORGE CALKIN 


 XUM 


 JUD 


 ORATORIO 


 C. HUBERT 


 


 DAILY TELEGRAPH 


 TIMES . 


 STANDARD 


 MORNING POST 


 LONDON & NEW YORK : NOVELLO , EWER CO 


 ITH 


 H. PARRY 


 DAILY NEWS 


 DAILY CHRONICLE 


 ATHENUM 


 GUARDIAN 


 DREAM JUBAL 


 POEM MUSIC 


 SOLI , CHORUS , ORCHESTRA 


 575 


 ACCOMPANIED RECITATION 


 WRITTEN 


 JOSEPH BENNETT 


 MI 


 A. C. MACKENZIE 


 VOCAL SCORE 


 DAILY TELEGRAPH 


 STANDARD 


 DAILY NEWS 


 MORNING POST 


 DAILY CHRONICLE 


 LONDON & NEW YORK : NOVELLO , EWER CO 


 ATHENEUM 


 WORLD . 


 VANITY FAIR 


 SUNDAY TIMES . 


 LIVERPOOL COURIER . 


 LIVERPOOL MERCURY 


 LIVERPOOL DAILY POST . 


 579 


 JTHN NOFOKTES 


 BLUTHAERK PIAN 


 GRAND UPRIGHT 


 PERFECT PIANOFORTES 


 WORLD 


 


 * TYPICAL ILLUSTRATION 


 BLUTHNER HOUSE 


 7 , 9 , 11 , WIGMORE STREET , CAVENDISH SQUARE , 


 LONDON , W 


 FI 


 HENSCI 


 SE 


 THI 


 YRY ' 


 THURSDA