


 MUSICAL time 


 singing - class circular . 


 publish month 


 AUGUST 


 , 1875 


 SIR JOHN GOSS EXHIBITION 


 COUNCIL COLLEGE 


 ORFOLK NORWICH MUSICAL 


 TI 


 FES VAL , 


 CONCORDIA 


 annual subscription , include postage , 


 NINETEEN SHILLINGS SIXPENCE 


 office — LONDON : 1 , BERNERS STREET , W 


 H.E 


 PROFESSIONAL notice 


 MR . J. TILLEARD 


 USIC engrave , print , PUB 


 NEW PEDAL attachment 


 pianoforte 


 testimonial . 


 JESSE MINNS , F.C.O. , 


 AST LONDON ORGAN WORKS , 


 , RUSSELL MUSICAL instrument . : 


 O CHORAL SOCIETIES , CATHEDRAL 


 STANDARD AMERICAN ORGANS 


 manufacture 


 PELOUBET , PELTON & CO . , NEW YORK 


 EXPRESSION 


 BARNETT SAMUEL & SON , 


 WHOLESALE MUSICAL INSTRUMENT WAREHOUSE , 


 HOUNDSDITCH , LONDON 


 O PIANOFORTE DEALERS , AUCTIONEERS , 


 PEDAL practice . — PEDAL REED 


 RGAN , PIANOFORTE , HARMONIUM , HAR- 


 OTICE REMOVAL.—THOMAS S&S. JONES , 


 second 


 serie 


 


 CANTICLES 


 


 point chanting , SET 


 HYMNS 


 CHURCH 


 appropriate anglican chant 


 SINGLE DOUBLE 


 


 edit 


 


 REV . SIR F. A. G. OUSELEY , BART . , ETC 


 


 EDWIN GEORGE MONK 


 TONIC SOL - FA edition . 


 manual singing 


 use choir trainer & schoolmaster . 


 thirty - edition . 


 COLLEGIATE SCHOOL 


 SIGHT singe manual 


 appendix 


 ( companion work 


 collegiate VOCAL TUTOR 


 CATHEDRAL chant book 


 second edition DR . BENNETT GILBERT 


 SCHOOL harmony 


 HARVEST anthem 


 ALLCOTT , W. H.—THOU visitest earth 


 ACFARREN , G. A.—GOD , BEHOLD , 


 AYLOR , W.—THE eye wait THEE . 


 RIDGE , J. FREDERICK.—GIVE unto LORD 


 EETON , HAYDN.—THE eye wait 


 hymn tune 


 HARVEST 


 select 


 HYMNARY 


 PRICE PENNY 


 PRAISE , o praise heavenly ) , 


 » earth 


 C. JEFFERYS , 67 , BERNERS ST 


 JEFFERYS MUSICAL journal 


 melodious MELODIESArranged organ harmonium JoHN OWEN . 
 shilling , net . 
 

 Andante ( Heller ) , Lied ( Mendelssohn ) , Schlummerlied ( Schumann ) , 
 March ( Schumann ) , Andante ( Oesten ) , Andante Moderato(Schumann ) , 
 Andante Cantabile ( Beethoven ) , Allegretto ( Mendelssohn ) , Traiimerei 
 ( Schumann ) , Andante ( Beethoven ) , Wedding March ( Mendelssohn 

 ii 

 angel bright fair ( Handel ) , Lied ( Mendelssohn ) , Andante 
 ( Russell ) , Adagio Cantabile ( Beethoven ) , Cujus Animam ( Rossini ) , 
 Faith ( Glover ) , Hope ( Glover ) , Charity ( Glover ) , Sanctus ( Bartniasky ) , 
 Caprice ( Blumenthal ) , Gloria , twelfth Mass ( Mozart 

 III.—OPERATIC . ) 
 ( press . ) 
 iv.—voluntarie 




 song child . 


 content 


 LONDON : C. JEFFERYS , 67 , BERNERS STREET , W 


 RESIDENT MUSIC master require 


 IANO - tuning.—employment want , 


 MUSICAL time 


 singing - class circular . 


 AUGUST 1 , 1875 


 ESTHER : ORATORIO G. F. HANDEL . * 


 HANDEL organist church 


 year 1718 1721 , 


 compose oratorio ESTHER 


 organ 


 BD 


 CRYSTAL PALACE 


 ROYAL ITALIAN OPERA 


 170 


 MAJESTY OPERAPHILHARMONIC society 

 tue feature final concert season 
 5th ult . , " Idyll " memory late Sir 
 Sterndale Bennett , compose fellow - student 
 warm friend , Dr. G. A. Macfarren . good feeling 
 prompt graceful tribute sufficiently 
 appreciate artistic worth composition 
 earn good opinion listener , trust , 
 , Dr. Macfarren — bow place 
 room , acknowledgment applause 
 Idyll greet — believe audience 
 desire admiration work , deep 
 heartfelt sympathy feeling composer . 
 orchestral work Haydn Symphony E 
 flat ( . 10 } Beethoven C minor , concert con- 
 clude Weber ' jubilee ’' Overture . Vieuxtemps ’ 
 Violin Concerto ( . 5 , minor ) finely play 
 Herr Wieniawski , unusual brightness 
 vocal portion programme engagement 
 Mdlle . Titiens , sing Scena , ' qual furor , " 
 Fidelio ; song , ' Glécklein m Thal , " 
 Euryanthe , Schumann Lieder 
 good style . Mr. W. G. Cusins , conductor , receive 
 ovation conclusion concert 

 ROYAL ACADEMY MUSIC 




 1872potter _ Exhibitioner . — Alice Curtis . Westmoreland 
 Scholar.—Agnes Larkcom . Sterndale Bennett scholar . — 
 Charlton T. Speer . Welsh Choral Union Scholar.—Mary 
 Davies . Parepa - Rosa Scholar.—Annie E. Butterworth 

 Mr. G. W. HamMmonn morning concert St. James 
 Hall , 8th ult . , attend crowded audience . 
 Mr. Hammond thoroughly classical reading 
 Beethoven ' ' Moonlight Sonata , " ' pianoforte 
 Sir Sterndale Bennett Chamber Trio ( Op . 26 ) , 
 Schumann Quintett e flat ( pianoforte , violin , 
 viola violoncello ) , Mendelssohn Concertante 
 variation ( pianoforte violoncello ) , prove 
 sound reliable performer . play 
 success Mozart Sonata pianoforte ( ably 
 assist Mr. W. H. Holmes ) graceful 
 piece composition , warmly ap- 
 plauded . instrumentalist Messrs. Henry 
 Holmes Mori ( violin ) , Mr. A. Burnett ( viola ) , Herr 
 Lutgen ( violoncello ) . Miss Annie Butterworth , Mr 

 O’Leary excellent song , ' ' roam Forest , " 
 highly effective , song Mr. W. H. Holmes , 
 capitally sing son , Mr. F. Holmes , 
 receive . vocal piece Miss 
 Julia Wigan , Mr. Arthur O’Leary act usual 
 skill accompanist 




 REVIEWS 


 174the MUSICAL TIMES.—Aveust 1 , 1875 

 phrygian mode , " ' ' Form 5th 
 model minor scale . " explanation book 
 exceedingly good , regret compound 
 time bar triple time 
 . triple division con- 
 found triple time unquestionably 
 misfortune , fault stu- 
 dent tell , Mr. Spencer tell , 
 bar triplet , " half 
 measure duple time half triple time . " 
 example counterpoint , think , 
 scarcely good place model tyro — 
 , example , bar page 86 , coun- 
 terpoint skip discord , c , 11th bar ex . 2 , 
 page 73 , G ’ 
 bar , counterpoint rise d — , gen- 
 erally , progression fairly good . speak 
 fugue , rule construction 
 answer ; valuable extract work 
 good composer place student guide 
 labour , author deserve praise 
 earn manner urge necessity dili- 
 gently study high model . second book , 
 treat art play pianoforte , 
 consider , early lesson remark 
 fingering particularly worthy commendation . 
 quotation composition Beethoven , Mozart , 
 Handel , & c. , , illustration author 
 observation , carefully select . 
 believe , , good transpose 
 vary passage classical work use young 
 player , protest example 45 
 ( merely mark " Beethoven " ) , opening 
 theme sonata flat ( Op . 26 ) transpose 
 semitone higher , melody bass 
 alter , bar add conclusion , subject 
 repeat octave . alteration 
 passage slight degree easy play , 
 reasonably wonder , , liber- 
 tie . conclusion , 
 book carefully print — carefully Mr. 
 Spencer , certain , thank point , 
 work , second subject alto 
 fugue D minor , page 131 , 
 wrongly note : quaver instead 
 semiquaver ; trifling error , puzzle 
 student 

 WEEKES & Co 




 TTS 


 (= SSS 5 SSS SS SSS 


 = = 2.21 ss ess : 


 LU 


 @ , DOLO . 


 LB 


 179 


 CORRESPONDENTS 


 BRIEF SUMMARY.OF COUNTRY NEWSLancinc CoL_LteGce.—The usual Concert Prize Day place 
 Tuesday , 27th ult . piece perform mention 
 Mendelssohn " hunting Song , " effectively sing , instrumental 
 piece : pianoforte duet play Organist College 
 pupil , duet violin piano play organist 
 boy . vocal piece sing Chapel Choir 
 Concert conduct Organist choir - master , 
 Mr. B. Manders 

 MAIDENHEAD.—The Philharmonic Society second concert 
 Wednesday , 2ist ult . Miss Jessie Royd , Miss Roberts , Mr. 
 Selwyn Graham , Mr. Orlando Christian principal 
 vocalist ; Herr Rosenthal , solo violin leader orchestra , 
 Herr Max Schultz , conductor . consist song , 
 Beethoven 1st Symphony , Auber Overture , Le Cheval de Bronze , 
 etc . , second Haydn Spring Summer , 
 season , creditably render . Mr. W. Goulden 
 preside pianoforte . concert successful 

 MELBOURNE , AUSTRALIA.—Bach Passion ( St. Matthew ) , 
 Philharmonic Society Good Friday night , time . 
 good available artist colony present assist 
 performance work , excellently render . 
 Mr. Summers , conductor Society , present 
 honorarium £ 50 , mark appreciation able manner 
 produce conduct work 




 dure month . 


 NOVELLO , EWER CO . complete 


 UNIFORM edition 


 MENDELSSOHN 


 thirteen - song 


 MR . BRANDON , 


 VOCALIST ( BASS ) , 


 TENTH season , 1875—76 . 


 ENGLISH GLEE UNION , 


 assist 


 NOVELLO 


 SHORT melody 


 ORGAN 


 VINCENT NOVELLO 


 ANGLICAN HYMN - BOOK 


 NEW EDITION , revise enlarge 


 ANGLICAN CHORAL SERVICE BOOK 


 USELEY MONK PSALTER 


 OULE collection word 


 PSALTER , PROPER psalm , HYMNS , 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 order HOLY communion 


 addition 


 HANT response command 


 late DR . ELVEY PSALTER . 


 late DR . MONSELL devotional poem 


 L.RRY 


 VILLAGE ORGANIST 


 CONGREGATIONAL PSALMIST 


 edit 


 ENLARGED 500 tune chorale . 


 ANDERSON SONGS ZION 


 NEW CHEAPER edition . 


 HELMHOLTZ tone . 


 ANGLICAN CHANT SERVICES 


 canticle 


 nthem W. PATTEN . — 


 " OQ WORSHIP LORD beauty HOLI- 


 t good thing thank . 


 play nearly 1,000 church . | 


 R. FOWLE standard voluntary . 


 R. FOWLE standard anthem ; 124 


 ould gain maiden heart . 


 AIL HARVEST home 


 MANFRED 


 dramatic poem , act , 


 music 


 ROBERT SCHUMANN , 


 benedictus 


 SET music follow 


 modern composer 


 set music follow 


 MODERN composer 


 SAMUEL WESLEY 


 motett double choir 


 PERGOLESI 


 spar IEA TSR 


 FEMALE voice . ) 


 JOHN HULLAH . 


 SEA 


 ready . 


 


 CATHEDRAL PSALTER 


 point chanting 


 publish sanction 


 REY . DEAN ST . PAUL 


 


 REV . DEAN WESTMINSTER . 


 hymnary 


 book church song 


 follow edition ready : — 


 notation VOCAL MUSIC 


 principle 


 substitution pitch , 


 adopt " NATIONAL method . " 


 W. W. PEARSON 


 LATIN word 


 ANDEL oratorio MESSIAH , 


 ENDELSSOHN GRAND sonata 


 song DESTINY 


 SCHICKSALSLIED ) , 


 compose 


 JOHANNES BRAHMS , 


 REV . J. TROUTBECK , M.A 


 song duet 


 compose 


 ANTON RUBINSTEIN . 


 2 . minstrel 


 THOU’RT SKE unto flower © 


 4 . morning SONG .. 


 5 . evening SONG 


 7 . song EGMONT 


 8 . SPRING SONG 


 RDNKHNRNHRKRY Y 


 trio lady ’ voice 


 compose 


 HENRY SMART 


 PWWWNHWHOW NWWW 


 - song 


 voice , 


 JACQUES BLUMENTHAL . 


 J. S. BACH 


 48 prelude fugue 


 major minor key 


 - temper CLAVICHORD , ) 


 edit collate edition 


 work , 


 W. T. BEST 


 offertory sentence 


 appendix festal use , 


 SET music 


 JOSEPH BARNBY 


 BEST arrangement 


 score great master . 


 ORGAN83 . Funeral March , ' ' death ofa hero , " Piano 

 forte Sonata , op . 26 ove aes eco -- Beethoven 

 March , b minor , op . 27 . no.1 ae . F. Schubert . 
 hunting song ' ' Waldscenen , " op . 82 R. Schumann . 
 Adagio Pianoforte Duets , op . ro ... ite Weber . 
 84 . fugue , g minor ... ose eee wee Mozart . 
 March Triomphale ... salk eco ine - Moscheles . 
 fantasia 6th quartet , e flat major « + . Haydn 

 chorus " behold wicked bendtheir bow " ( Anthem 

 Lord " ) = Handel . 
 Adagio , SymphonyinG ... 0 1.0 se wee ose Haydn . 
 94 . Scherzo , 2nd Symphony -- Beethoven . 
 Overture , Messiah . Handel . 
 95 . Andante , Sonata , major , Pianoforte ‘ na Violin , bs 
 12 , . 2 od . Beethoven . 
 chorus , ' ' way , ential ? , ' ( jubilate ) Handel . 
 chorus , " tell heathen , " ( Anthem , 
 " ocome let usse " ) ... ao ‘ ae ? Handel . 
 96 . Grand March , op . 4o , . 2 ie eee ... F. Schubert . 
 Bourrée , Pastor fido ... 2 Handel . 
 o7- Passacaille = ... seo eee .-F , Couperin . 
 Triumphal March oe " < L. Hatton . 
 Echo , Overture Feussh . ts - 
 Pianoforte ... os ot Bach . 
 98 . Andante , Bagaele , op.- 33 , . 4 -- Beethoven . 
 Andante ' ott " Mozart . 
 Fuguein minor ... se ae ove sad ae Bach 

 99 . baptismal song « - . Meyerbeer . 
 air variation , symphony | ' D ows Haydn . 
 romance Violin Orchestra , f major , op . 50 ... Beethoven 

 100.allegretto ( Gratulations Menuet ) ( posthumous work ) Beethoven . 
 allegretto , Violin Sonata major eve Handel . 
 prelude fugue " Bach " ... Bach 

 volume i. iv . , bind cloth , price £ 1 - 16s . . 
 London : Novello , Ewer Co. , 1 , Berners Street ( W 




 NOVELLO 


 original OCTAVO edition 


 OPERASEdited , correct accord Original Scores , 
 translate English , b 
 NATALIA MACFARREN BERTHOLD Tours . 
 price 2 . 6d . ; scarlet cloth , 4 . 
 ready 

 BEETHOVEN FIDELIO 
 ( german english word 

 great overture usually performed;"bee 
 piano Score publish agree original 
 score note sign phrasing 




 AUBER FRA DIAVOLO 


 MOZART DON GIOVANNI 


 BELLINI NORMA 


 VERDI IL TROVATORE 


 DONIZETTI LUCIA DI LAMMERMOOR 


 WEBER OBERON 


 ROSSINI IL BARBIERE 


 DONIZETTI LUCREZIA BORGIA 


 MOZART NOZZE DI FIGARO 


 VERDI RIGOLETTO 


 BELLINI LA SONNAMBULA 


 WEBER DER FREISCHUTZ 


 WAGNER TANNHAUSER 


 AUBER MASANIELLO 


 BELLINI PURITANI 


 WAGNER LOHENGRIN 


 DONIZETTI LA FIGLIA DEL REGGIMENTO 


 ROSSINI GUILLAUME TELL 


 MOZART DIE ZAUBERFLOTE 


 VERDI LA TRAVIATA 


 FLOTOW MARTHA 


 VERDI ERNANI 


 MOZART IL SERAGLIO 


 GLUCK IPHIGENIA TAURIS 


 BAI 


 song , ) new song 


 composer . OCTAVO size . NOVELLO EWER CO 


 ARNBY , |.—THOU 


 OCTAVO edition 


 service modern composer 


 publish NOVELLO , EWER CO 


 LFRED H. LITTLETON.—THE morning 


 T. BEST.—A morning , communion 


 BAPTISTE CALKIN.—MORNING , COMMU 


 R. JOHN B. DYKES.—A morning , com 


 R. G. M. GARRETT.—A morning , com 


 R. G. M. GARRETT.—A morning , com- 


 IR JOHN GOSS.—THE order 


 LFRED H. LITTLETON.—THE evening 


 ALTER MACFARREN.—A simple morn 


 H. MONK.—THE office HOLY 


 HUBERT H. PARRY.—A morning , com- 


 ENRY SMART.—A morning , commu- 


 service MODERN 


 R. J. STAINER.—A morning , communion 


 R. CHARLES STEGGALL . — morning 


 s78 R. P. STEWART.—A morning , com- 


 H. THORNE.—A morning , communion 


 ERTHOLD TOURS.—A morning , com- 


 HOMAS TALLIS TRIMNELL . — CHANT 


 R.S. S. WESLEY.—A short cathe- 


 NEW edition ( OCTAVO ) 


 ORATORIOS , CANTATAS , 


 BACH.—O light EVERLASTING 


 FLAT 5 - 6 


 LONDON : NOVELLO , EWER CO 


 NANDADWDTOADAOS O 


 second serie 


 collection - song madrigal 


 publish volume , OCTAVO size , elegantly bound 


 MODERN composer 


 separate number , follow 


 VOLUME i. contain 


 song 


 SIR J. BENEDICT . 


 song HENRY SMART 


 SEVEN SHAKSPERE song 


 G. A. MACFARREN 


 song J. L. HATTON . 


 VOLUME ii . contain — 


 song BYG.A.MACFARREN 


 song C. A. MACIRONE . 


 song HENRY LESLIE 


 madrigal 


 composer . 


 VOLUME iii . contain — 


 song HENRY HILES 


 song 


 FRANCESCO BERGER . 


 song 


 J. BAPTISTE CALKIN 


 S.A.T.B 


 song J. BARNBY . 


 song G.A.MACFARREN . 


 VOLUME IV . contain — 


 song A. ZIMMERMANN 


 14 


 shakspere song 


 G. A. MACFARREN 


 song HENRY LESLIE 


 song HENRY SMART 


 song SAMUEL REAY . 


 VOLUME V. CONTAINS 


 song A. S. SULLIVAN . 


 song W. MACFARREN 


 SD 


 168 


 169 


 170 


 171 


 172 


 173 


 song J. LEMMENS 


 song HENRY SMART 


 song CIRO PINSUTI 


 VOLUME VI . contain — 


 thirty - song 


 J. L. HATTON 


 VOLUME vii . contain 


 - song 


 J. L. HATTON . 


 man voice 


 VOLUME viii . contain 


 - song 


 HENRY SMART 


 VOLUME ix . contain 


 - song 


 WALTER MACFARREN 


 VOLUME x. contain — 


 song madrigal 


 R. L. DE PEARSALL . 


 VOLUME xi . contain — 


 song madrigal 


 R. L. DE PEARSALL 


 song author 


 LONDON : NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C. ) 


 NEW YORK : J. L , PETERS , 843 , BROADWAY 


 CHAPPELL & CO . , 50 , NEW BOND STREET , SOLE AGENTS 


 CHAPPELL & CO . , 504 , NEW BOND STREET , SOLE agent 


 new work amateur organist 


 new offertory sentence 


 voice , organ accompaniment . 


 new work singing class 


 CHAPPELL PENNY OPERATIC - song 


 CHAPPELL & CO . , 50 , NEW BOND STREET , LONDON