


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


PUBLISHED ON THE FIRST OF EVERY MONTH


CONCORDIA


ANNUAL SUBSCRIPTION, INCLUDING POSTAGE, NINETEEN SHILLINGS & SIXPENCE


OFFICE—LONDON: 1, BERNERS STREET, W


PROFESSIONAL NOTICES


TEAM PRINTING.—FITZSIMMONS & SONS 


RUSSELL’S MUSICAL INSTRUMENTS


AST LONDON ORGAN WORKS


MR. J. TILLEARD


USIC ENGRAVED, PRINTED, AND PUB- 


HARMONIUMS


HE CLARABELLAS.—IMPROVED GUT


UARTERLY SALE OF MUSICAL PROPERTY. 


ESTABLISHED 27 YEARS. 


OMPOSERS’ MSS. CORRECTED, REVISED, 


RGAN, PIANOFORTE, HARMONIUM, HAR


INGING, INSTRUCTION IN ORATORIOS


CHORUSES FOR LADIES’ VOICES


ANGLICAN


POA LT ER


CHANTS


SINGLE AND DOUBLE


EDITED BY THE


AND


EDWIN GEORGE MONK


TONIC SOL-FA EDITION. 


A MANUAL OF SINGING


FOR THE USE OF CHOIR TRAINERS & SCHOOLMASTERS 


THIRTY-SECOND EDITION. 


THE COLLEGIATE AND SCHOOL


SIGHT SINGING MANUAL


APPENDIX


(COMPANION WORK TO THE ABOVE


COLLEGIATE VOCAL TUTOR


CATHEDRAL CHANT BOOK


SECOND EDITION OF DR. BENNETT GILBERT'S


SCHOOL HARMONY


MR. LESLIE’S ELEMENTARY MUSIC


THE


NEW QUARTERLY MAGAZINE


CONTENTS


1. ALLOTMENT GARDENS. 


JEFFERIES


2. THE RELIGIOUS ELEMENT IN CHAU- 


5. RICHARD WAGNER AND HIS “RING OF 


7. THE MORAL ASPECTS OF VIVISECTION. 


8. THOMAS LOVE PEACOCK


NOVELLO, EWER AND CO.'S ONLY COMPLETE AND 


UNIFORM EDITION OF


MENDELSSOHN’S


THIRTEEN TWO-PART SONGS


C. JEFFERYS, 67, BERNERS ST


JEFFERYS’S MUSICAL JOURNAL


MELODIOUS MELODIESPART I

Andante (Heller), Lied (Mendelssohn), Schlummerlied (Schumann), 
March (Schumann), Andante (Oesten), Andante Moderato(Schumann), 
Andante Cantabile (Beethoven), Allegretto (Mendelssohn), Traiimerei 
(Schumann), Andante (Beethoven), Wedding March (Mendelssohn

PART II

Angels ever bright and fair (Handel), Lied (Mendelssohn), Andante 
(Russell), Adagio Cantabile (Beethoven), Cujus Animam (Rossini), 
Faith (Glover), Hope (Glover), Charity (Glover), Sanctus (Bartniasky), 
Caprice (Blumenthal), Gloria, from Twelfth Mass (Mozart

To be continued




SONGS FOR CHILDREN


CONTENTS


THE CHORAL SOCIETY


LONDON: C. JEFFERYS, 67, BERNERS STREET, W


ICHFIELD CATHEDRAL CHOIR


T. ANNE’S CHURCH, WANDSWORTH. — 


PROFESSIONAL PIANOFORTE TUNER


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


JUNE 1, 1875


THE PICTORIAL POWER OF MUSIC. thus the thesis of a Symphony by Spohr, and the

appropriation of natural sounds to art use by 
Beethoven. Then there is the Capriccio by Bach 
for the harpsichord, written in 1704, representing the 
dissuasion of a beloved brother from a hazardous 
journey, his resistance of the same, and his de

parture, and concluding with a fugue on the notes 
of the postboy’s horn; and what can be more 
modern in purpose than this, more descriptive, or 
more programmatical? ‘There are the same com- 
poser’s Pastoral Symphony in his Christmas Oratorio, 
and Handel’s piece of the same name in his Messiah, 
both figuring the shepherds at watch in the bright 
starlight on the night of the Nativity. There is the 
Symphony in Samson, to represent the falling of the 
building overthrown by the miraculous strength of 
the blind hero—almost the single instance of Handel’s 
use of chromatic passing notes—and that in Bel- 
shazzar, which is quaintly, but characteristically 
marked “ Allegro postillions,” to accompany the 
supposed hurried entrance of the throng of wise 
men summoned to interpret the prophetic writing 
on the wall; and need one look in the works 
of any age for a truer image of the feeling prevalent 
among the dramatic persons than the Dead March 
in Saul or the Jubilant March in Judas Maccabeus ? 
Haydn’s representation of the earthquake in his 
Seven Last Words, and of chaos in his Creation, 
bring the art of descriptive instrumental music a 
generation nearer to our own age. Even Beethoven, 
the accredited inventor of the practice, was fore- 
stailed by a few years in his design of the Sinfonia 
Pastorale, the earlier conception of pourtraying 
pastcral life in orchestral music being due to one 
Knecht, an obscure composer, who wrought in a 
place, little less unknown, called Biberach. Glorious 
specimens of descriptive music are Beethoven’s 
Overtures, all four of them, to Leonore, that to 
Coriolan, and that to Egmont; and his Sonata repre- 
senting a farewell, absence, and a return, belongs to 
the same category. Rossini worked with the brush 
of a scene-painter in his Overture to Guillaume Tell 
and in the representations of a storm that divide the 
action of the second act in the Barbiere, Matilda 
di Shabran, and other of his comic Operas. Spohr 
evinced his descriptive power in his Symphonies, the 
Consecration of Sound, the Contention between 
Earthliness and Godliness in the soul of man, and 
the Seasons, and in his Overture to Azor and Zemira, 
representing the storm, the wreck of the merchant’s 
vessel, and his magical rescue. Weber’s Concert 
Stiick tells a complete story of anxiety in absence, 
a knight’s return, his true love’s eager rush to meet 
him, and their rapture in each other’s embrace. The 
success of Mendelssohn has been equal to that of 
the best of them in his Overtures, and in his 
Reformation Symphony and in those two orchestral 
works recording his impressions in Scotland and in 
Italy. The tone-pictures by Sterndale Bennett, 
namely, the Naiades, the Wood-Nymphs, and Paradise 
and the Peri, are each a masterpiece. Latest, 
if not last, the Abbé Liszt and Herr Raff are writing 
symphonic poems and Symphonies without the 
assumption of poetry, purposing to paint in tones 
the pictures announced in the titles they choose

Thus much for the pictorial in music for instru- 
ments. It is the admitted province of compositions 
for voices, from the close of the sixteenth century at 
least, to express the words to which they are set. 
Many and many an author has been uncontent to 
limit this word, express, to the sense of declaim, and 
has found means to illustrate his text by figures of his 
own imagining, fully as metaphorical as anything 
that graces the verse of a poet. Think of the weighty 
wall of waters and the rippling of the harmless waves 
against it, in the chorus “ He led them through the 
deep,” the sense of substance as in our London fogs 
in “ He sent a thick darkness,” the oppression as of

The questionless perspicuity of these vocal instances 
of which the words indicate the purpose, proves that 
instrumental music may be equally full of meaning 
though this have no external indication

Descriptive instrumental music has its worst side 
outwards in the so-called “ Battle Pieces” and other 
things of the class, that were more in vogue from 
fifty to a hundred years ago than they are now, 
beginning with Kotswara’s Battle of Prague, that was 
erewhile as certainly to be found in a ladies’ boarding 
school as a back board or a French “ mark;” inclu- 
ding Dussek’s Sorrows of Marie Antoinette that ends 
with a glissando descent from the top to the bottom 
of the pianoforte to picture the fall of the guillotine; 
and not omitting the work of Beethoven himself that 
was designed, not merely to commemorate, but to 
describe Wellington’s success at Vittoria. In these 
and such as these, the description is of prior considera- 
tion to the music, and, to prevept the possibility of 
any portion of this being misunderstood, the staves 
are interlined, in most cases, with indications as 
evident as ‘‘ The cries of the wounded,” ‘the agony 
of the royal lady when her infant son is torn from her 
arms,” and so forth. To another species of delinea- 
tion belongs the notable piece by Cesti, wherein the 
purpose to represent a rainbow is effected by the 
successive entry of all the instruments in the score, 
beginning with that on the lowest staff and ending 
with that on the highest, each for a single note, 
followed by the reverse of the succession, with which 
image if the eye be satisfied, it is possible the ear 
may not. All these compositions, from the meanest 
upwards, address but a low order of intelligence, and 
their littleness gives licence to many effective sallies 
against the pretence to address the sight through the 
hearing or to set forth visible objects by means of 
sounds

Indisputably, all musical images are vague, and 
are susceptible of various interpretation by different 
hearers. It is more than forty years ago that a




TA


105It is now, lastly and chiefly, to consider what is 
described, or pictured, or, to refer to the phrase of 
the day, prographed in music. A music lover, after

this kind, once said that he liked ‘‘that portion in 
Beethoven’s Pastoral Symphony which represented 
the windmills ”’—but this was not the artist’s aim, 
nor isit hisend. Neither are the jagged rocks or the 
dashing waters of the Hebrides exhibited in Mendels- 
sohn’s Isles of Fingal, nor the forms and groupings of

the insects in Handel’s “ He sent all manner of flies,” 
nor the skeletons of the deer and the hunters in 
Weber’s Wild Chase in the Air, nor the wretch who 
learned to pray from the innocence of the child in 
Bennett’s Paradise and the Peri. Most epigram- 
matically and most completely was the whole pur- 
pose of this class of music set forth in Beethoven’s 
announcement of the great work which stands fore- 
most in everybody’s thought of the matter—‘* Mehr 
Ausdruck der Empfindung als Mahlerei,” (More the 
expression of feeling than a painting). There is 
and there can be no pretence in any work of the 
kind to show more or less than what would be 
the artist’s impressions under the circumstances 
supposed, and this is shown in his own language, 
which happily is one without a glossary, that de- 
pends for its free translation on the perceptivity of 
the hearer. Who has basked in the sunshine 
and felt the kiss of the soft breezes in the open 
country, who has mused beside a running stream and 
noted the rippling of the current and the rustling of 
the leaves and the chirping of the birds, who has 
watched villagers at their merry-making, who has 
witnessed the rise and climax and culmination of a 
storm, and who has seen the returning peace of 
nature with the outburst of gratitude that flows from 
all animate and even inanimate existences—he has 
in his own breast the key to Beethoven’s imaginings, 
and he may unlock the magical casket and be at one 
with all its images. Already has been suggested 
that every work of art is a subjective picture, a con- 
fession of the joys or sorrows of him who produces 
it. In this respect, an unentitled piece of music 
somewhat resembles a landscape or a pastoral poem, 
which possesses a personal character, in so far as it 
may express the feelings of the author, beyond the 
positive facts of which it is a statement, namely, that 
there is a tree tothe right hand, or a brook to the 
left, or a hill in the distance, or the warm glow of a 
summer heaven pervading the whole. To-day, one 
may regard these objects with grateful devotion; 
to-morrow, with regret for the companion with whom 
he once viewed them; again, in the overflow of 
animal spirits springing from health or from some 
fortune apart from the scene; at another time, in the 
indifference of a purposeless hour; anon, in the 
despair of frustrated endeavour. Whatever the mood, 
this will bespeak itself in the description, and, 
whether in lines, in words, or in notes, the indi- 
viduality of the artist will be evident in his work, and 
the program will be traceable, even though it may 
not have been prescribed

These remarks are the wild growth of a fertile 
theme. With cultivation it would yield a rich 
harvest to the thinker, but even these random 
words may indicate that there is store of fruit for 
the gathering




THE BENEDICT TESTIMONIALCRYSTAL PALACE

WE have often wondered why the classical character of 
the winter concerts at this establishment should be aban- 
doned in the summer, and music-lovers be compelled to 
seek for good works at other places, or to listen to operatic 
scraps in an atmosphere where they have been taught to 
expect only the highest specimens of the art. At last we 
are enabled to record that the seasons of the year are no 
longer to influence the nature of the music. The pro- 
gramme of the first summer concert, on the 15th ult., could 
scarcely perhaps be at all distinguished from one of the 
series just terminated; and when we say that on the 
following Saturday the selection included Beethoven’s 
Choral Symphony, it may be concluded that these entertain- 
ments can be fairly accepted as models of those which are 
to follow. All the instrumental works have been, as usual, 
finely rendered, under the able conductorship of Mr. Manns; 
and the vocalists have comprised most of the leading 
favourites of our concert-roooms

HER MAJESTY’S OPERA




ROYAL ITALIAN OPERA


ROYAL ALBERT HALLPHILHARMONIC SOCIETY

HERR WILHELM)’s performance of the first Allegro and 
Andante of Rubinstein’s Violin Concerto in G, at the third 
concert, was in every respect admirable. The composition, 
too, is highly interesting, the Allegro, especially, being a 
finely written movement and containing some most effective 
conversational passages between the solo instrument and 
the orchestra. The principal orchestral piece—Schumann’s 
Symphony in B flat, No. 4—was a welcome item in the 
programme, and was on the whole extremely well played. 
At the fourth concert, on the roth ult., a very good per- 
formance of Beethoven’s Choral Symphony was given, the 
principal vocal parts being carefully rendered by Madame 
Blanche Cole, Miss Enriquez, Mr. Henry Guy and Mr. 
Wadmore. A new pianist, Herr Lodovico Breitner, dis- 
played much power and facile execution; but we should 
like to hear him in more exacting music than the showy 
Concerto in E flat, of Liszt, before pronouncing any 
judgment upon his artistic qualifications. A feature in the 
selection was Schumann’s charmingly fresh and tuneful 
chorus, ‘Gipsy life,” which was capitally sung, and 
pleased so much as to elicit an enthusiastic encore. At 
both the concerts noticed Mr. W. G. Cusins, as usual, 
conducted

VERDI’S REQUIEM




TRIO FROM MENDELSSOHN’S « ELIJAH


HI


T 7 


L _. 


(CHE =| | = + => E 


NP 


CYCS


(22


| 4 1 


_—__™ 


[S44 | = —_— = 


THE ONLY COMPLETE EDITION OF


MENDELSSOHN’S SONGS


ENGLISH AND GERMAN WORDS.) 


EDITED AND IN GREATER PART TRANSLATED BY 


NATALIA MACFARREN


CONTENTS


FLOWERS 5 | 


LONDON: NOVELLO, EWER AND CO., 1, BERNERS STREET (W.), AND 35, POULTRY (EC


II5Mr. H. WavMsLey LITTLE gave a concert in aid of the 
organ fund for Christ Church, Woburn-square, at Store- 
street, on the 3rd ult., assisted by the Misses E. Blair, J. 
King, A. E. Bolingbroke, Messrs. W. A. Howells and A. F. 
Jarratt, vocalists; Miss C. Aptommas, harp, and Miss E. 
Edridge and Mr. G. F. Smith, pianoforte. The concert- 
giver was highly successful in his pianoforte solo, Chopin’s 
second Scherzo, Op. 31, and in a duet with Mr. Smith, 
‘“‘ Allegro Brillante” (Mendelssohn). A very elegant song 
by Mr. Little, ‘‘ The fountains mingle with the river,” was 
charmingly sung by Mr. Howells, Mr. Jarratt gave a good 
rendering of ‘“‘The young mountaineer” (Randegger), 
and the vocal solos by the ladies were received with en- 
thusiastic applause, many of them being encored. Some 
part-songs, under the conductorship of Mr. Little, were 
sung by a select choir

AN amateur concert was given at the St. Mark’s Schools, 
Regent’s-park, on Wednesday evening the 5th ult. Mr. 
George Calkin conducted, and the Rev. E. D. Galloway 
accompanied the solos and choruses. The programme 
commenced with Mendelssohn’s ‘‘ Lauda Sion,” which was 
rendered in a very effective manner by the St. Mark’s 
Choral Class; the solo “ Lord, at all times”? being well 
given by Miss Fogarty, and encored. Miss Steele, Miss 
Johnstone, Miss Janet King, Miss Goodall, Mr. G. Calkin, 
Mr. Conrad King, and several others contributed to the 
vocal music. Miss Grassman played one of Beethoven’s 
Sonatas on one of Messrs. J. and J. Hopkinson’s pianos 
(kindly lent by them for the occasion), and Miss May a 
movement by Hummel. Much praise is due to Mr. Calkin 
for the training of the chorus. Mr. Galloway accompanied

THE annual performance of the ‘“‘ Messiah” in aid of the 
funds of the Royal Society of Musicians, took place at St. 
James’s Hall on the 7th ult. Thesolo vocalists were Miss 
Edith Wynne, Mesdames Blanche Cole, Osborne Williams 
and Patey, and Messrs. W. H. Cummings, J. Williams, 
Stanley Smith, and Lewis Thomas. Mr. Willy led, Mr. 
E, J. Hopkins presided at the organ, and Mr. W. G. Cusins 
conducted

Tue Chamber Concerts given by Mr. Alfred Gilbert and 
Madame Gilbert at the Gallery of the Society of British 
Artists, are in every respect entitled to the highest praise. 
The programmes contain an excellent selection of classical 
music, both vocal and instrumental, and are so arranged as 
to afford opportunity at the termination of each concert for 
the inspection of the Gallery of pictures

Miss AGNES ZIMMERMANN gave an evening concert at 
St. James’s Hall on the 2gth April, which was attended by 
an audience evidently prepared to appreciate a programme 
framed rather for the select few than the many. The 
concerted pieces were Beethoven’s Sonata in A major, for 
pianoforte and violoncello (Op. 69), a Sonata in A, for 
pianoforte and violin, the composition of the concert-giver, 
and Schubert’s Trio in B flat, for pianoforte, violin and 
violoncello (Op. 99). The refined and intellectual per- 
formance of Miss Zimmermann was the theme of general 
admiration in all these pieces; and as she had the co- 
operation of Herr Straus (violin) and Herr Daubert (violon- 
cello) it is needless to say that the general rendering of 
each work left nothing to be desired. Of Miss Zimmer- 
mann’s Sonata we recorded our highly favourable opinion 
when played by the composer and Madame Norman- 
Neruda last year at the Hanover-square Rooms; and we 
have now only to say that it was received by the audience 
with equal marks of favour. Schumann’s ‘“ Etudes Sym- 
phoniques,” dedicated by the composer ‘to his friend 
W. S. Bennett,” were excellently played by Miss Zimmer- 
mann; and, although rather ‘‘severe” for a concert- 
room, were warmly applauded. The vocalist was Madame 
Lemmens-Sherrington, who was enthusiastically encored 
in a graceful song by Miss Zimmermann, ‘Only a year 
ago, love,’ and also sang with much success two songs by 
Rubinstein

On Monday evening, the roth ult., Miss Florence 
Wydford gave her annuai concert at the Horns, Kennington. 
The artists engaged were the Misses Agnes Drummond, 
Josephine Pulham, Marian Haigh, Florence Wydford, and 
Messrs. Henry Guy, Theodore Distin, E. Stone, and 
Thurley Beale. Miss Adelaide Pulham, Miss Fanny 
Henman, and Mr. George F. Smith, R.A.M., presided at 
the pianoforte, and Mr. C. P. Mann conducted. The 
programme comprised many well-known songs, several of 
which were encored. A pianoforte and concertina duet by 
Mr. C. P. Mann and Mr. E. Stone, on “‘ Scotch Airs,” was 
loudly applauded




REVIEWS


BD Musical Composers and their Works. For the use of 
schools and students in music. By Sarah Tytler

Books like these do good, especially when mere facts 
are related, and readers are allowed to form their own 
opinions, instead of having them forced upon them by one 
who, having perhaps much talent for collecting materials, 
may have but little power in pronouncing a correct judg- 
ment upon them. Miss Tytler’s work has at least the 
merit of being carefully put together; the most reliable 
information has been sought for respecting the career of 
each composer, and students who begin by reading the 
lives of those who have shed such lustre upon the art, will 
probably end by playing and endeavouring to understand 
the undying compositions which they have bequeathed to 
us. The authoress need not have apologised in her Pre- 
face for devoting so much space to the ‘sketches of Mozart, 
Beethoven, Mendelssohn, and Moscheles,” for one of the 
great objects in a book of this kind is to interest readers ; 
and all young persons like to linger over the biographies of

those whose names they are constantly hearing. We are 
especially pleased with the manner in which the lives of 
Mozart and Mendelssohn are treated, and quite agree with 
our authoress that, apart from the right which Moscheles, 
by virtue of his talent, has to a high place in the history 
of music, his intimate connection with the first composers, 
makes the record of his career highly valuable. Strange 
however is it that in a book so well digested, where space 
does not allow more than a short catalogue of some of the 
minor composers who have contributed to preserve, trans- 
mit, and make popular in their own way, the gentle science 
in England, the only names mentioned should be “‘ Chappell, 
Brinley Richards, D’Albert, and the brothers Godfrey




WILLIAM CZERNY120 THE MUSICAL TIMES.—June 1, 1875

the consecutive 5ths from the 2nd to the 3rd and the roth 
to the 11th bars of the episode in F do not add to the 
effect. The remaining pieces present very similar features 
for blame or praise, and it is unnecessary to speak of them 
more in detail. The adaptations are selected from the 
works of Bach, Handel, Pergolesi, Haydn, Mozart and 
Beethoven. In No 2 we find a movement of Pergolesi, 
but we do not think he could have been guilty of the 8th 
between extreme parts and the 5th in third and fourth lines 
of page 11. Whether the progressions appear in the 
original or not we must blame the editor’s powers either of 
adaptation or selection. Apart, however, from the musical 
merit of the series, it seems to us inexpedient to write for 
the organ on two staves, except in cases where, as in the 
St. Ann’s Fugue of Bach, the pedal part is omitted for 
a while, or when only three or four parts move at once. 
Some confusion arises in the pieces under consideration 
from the crowded appearance of the two staves, although 
what seems a difficulty to us may very likely be a matter 
3 ane to practical and experienced organists like Mr. 
ilner

ORIGINAL CORRESPONDENCE




MEDDLING WITH THE OLD MASTERS. 


TO THE EDITOR OF THE MUSICAL TIMESZ—@—I

are correct and stand so in Beethoven’s revised copy. Of 
the two versions of the passage quoted from the Sonata, 
Op. 81, the second is the original

From a note in Thayer’s Chronologisches Verzeichniss 
(Berlin 1865), it appears that the alteration mentioned by 
“« Allegro” was made first by a Viennese publisher, Mollo, 
in Beethoven’s life-time. There isno reason whatever for 
supposing that Beethoven authorized the change

Yours truly, 
Malvern, May 24, 1875. An OLD SUBSCRIBER




TO CORRESPONDENTS


BRIEF SUMMARY OF COUNTRY NEWS


DURING THE LAST MONTH. 


A SONG OF DESTINY


SCHICKSALSLIED), 


OMPOSED BY


JOHANNES BRAHMS, 


REV. J. TROUTBECK, M.A


THE ANGLICAN HYMN-BOOK. 


NEW EDITION, REVISED AND ENLARGED. 


SECOND SERIES


HE ANGLICAN CHORAL SERVICE BOOK, 


ADDITIONS TO THE 


EV. T. HELMORE’S PLAIN SONG WORKS. 


USELEY AND MONK’S PSALTER AND


OULE’S COLLECTION OF WORDS OF 


HE PSALTER, PROPER PSALMS, HYMNS, 


OULE’S DIRECTORIUM CHORI ANGLI- 


OULE’S DIRECTORIUM CHORI ANGLI- 


HE ORDER FOR THE HOLY COMMUNION. 


WORSHIP THE LORD IN THE BEAUTY 


ORTY MOODY AND SANKEY’S REVIVAL


NOW READY. 


CATHEDRAL PSALTER


POINTED FOR CHANTING


PUBLISHED WITH THE SANCTION OF


THE VERY REV. THE DEAN OF ST. PAUL'S


AND


THE VERY REV. THE DEAN OF WESTMINSTER. 


THE HYMNARY 


A BOOK OF CHURCH SONG


THE FOLLOWING EDITIONS ARE NOW READY :— 


THE VILLAGE ORGANIST


THE CONGREGATIONAL PSALMIST


EDITED BY 


ENLARGED TO 500 TUNES AND CHORALES. 


NEW AND CHEAPER EDITION. 


C°% JESU, SALUS IN TE SPERANTIUM


HYMNS FOR THE CHURCH OF ENGLAND, 


WITH PROPER TUNES


ALREADY PLAYED IN NEARLY 1,000 CHURCHES. 


USIC, A RUDIMENTARY AND PRACTICAL 


HE PIANOFORTE, THE RUDIMENTS OF 


SIN |W SS ISRO DRAD SS


ND


NEW AND COMPLETE EDITION


MENDELSSOHN’S


MUSIC TO 


SHAKSPEARE’S


PRICE ONE SHILLING


NEW SONGS


— THE BELLS OF ST. ETHELRED


THE ROCK OF AGES 


EMMENS, J.—THE WREN’S NEST. 


LEMMENS-SHERRINGTON.) 


FF SP SF SHE SF SH HH HS


— ROSE MA


INSUTI, CIRO. —BEFORE THE FIGHT . 


G45, E.—POLLY VANDERDECKEN


NOVELLO’S 


ORIGINAL OCTAVO EDITION OF


OPERASEdited, Corrected according to the Original Scores, and 
Translated into English, by 
NATALIA MACFARREN and BERTHOLD Tours. 
Price 2s. 6d. each ; or in scarlet cloth, 4s. 
NOW READY

BEETHOVEN’S FIDELIO 
(With German and English words

With the two great Overtures as usually performed; being the only 
Piano Score that has been published agreeing with the Original 
Score as to the notes and signs for phrasing




AUBER’S FRA DIAVOLO 


MOZART’S DON GIOVANNI 


BELLINI’S NORMA


VERDI'S IL TROVATORE 


DONIZETTI’S LUCIA DI LAMMERMOOR


WEBER’S OBERON


ROSSINI’S IL BARBIERE 


DONIZETTI’S LUCREZIA BORGIA 


MOZART’S NOZZE DI FIGARO 


VERDI’S RIGOLETTO


BELLINI’S LA SONNAMBULA


WEBER'S DER FREISCHUTZ 


WAGNER’S TANNHAUSER 


AUBER’S MASANIELLO 


BELLINI’S I PURITANI 


WAGNER’S LOHENGRIN 


DONIZETTI’S LA FIGLIA DEL REGGIMENTO 


ROSSINI’S GUILLAUME TELL 


MOZART’S DIE ZAUBERFLOTE 


VERDI’S LA TRAVIATA 


FLOTOW’S MARTHA 


VERDI’S ERNANI 


MOZART’S IL SERAGLIO 


GLUCK’S IPHIGENIE IN TAURIS


126


A COLLECTION OF


BY THE MOST EMINENT GERMAN COMPOSERS, 


WITH ENGLISH WORDS. 


BOOK I. 


BOOK II. 


BOOK III. 


BOOK IV. 


BOOK V. 


BOOK VI


BOOK VII


BOOK VIII. 


BOOK X


BOOK XI. 71 Hilarity on 
72 The Recompense ove 
73 The Request ... eee 
74 The Wood Nymph

Beethoven 
Annacker 
Naumann 
Baur 
Pohlentz 
H. Werner 
Zelter

De Call




BOOK XIII, 


BOOK XIV


BOOK XV


BOOK XVI


BOOK XVII. 


BOOK XVIII. 


BOOK XIX


BOOK XX


BOOK XXI


BOOK XXII


BOOK XXIII


BOOK XXIV. 


BOOK XXV. 


BOOK XXVI. 


BOOK XXVII. 


BOOK XXVIII


BOOK XXIX. 


BOOK XXX. 


BOOK XXXI. 


BOOK XXXII, 


BOOK XXXIII. 


BOOK XXXIV. 


BOOK XXXV. 


BOOK XXXVI. 


BOOK XXXVII. BOOK XXXVIII

6*Thy Goodness spreads... Beethoven

7*God is my song eee do. 
8*I love my God ooo 
9*Swiftly fades my life ... 
10*The Heavens proclaim 
Him eve oe 
11*God my help ... eco 
12*Look up to God one 
13*Prayer eee eee




BOOK XL. 


BOOK XLII. 


BOOK XLII. 


LONDON: NOVELLO, EWER AND CO., 1, BERNERS STREET, W


CHAPPELL & CO., 504, NEW BOND STREET, SOLE AGENTS


CHAPPELL & CO., 50, NEW BOND STREET, SOLE AGENTS


A NEW WORK FOR AMATEUR ORGANISTS


TWENTY NEW OFFERTORY SENTENCES


FOR FOUR VOICES, WITH ORGAN ACCOMPANIMENT. 


NEW WORK FOR SINGING CLASSES


CHAPPELL’S PENNY OPERATIC PART-SONGS


CHAPPELL & CO., 5, NEW BOND STREET, LONDON