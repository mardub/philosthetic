


 publish 


 MUSICAL time 


 f SINGING - class circular 


 SEPTEMBER 1 


 month 


 1887 


 ROYAL IRISH ACADEMY MUSIC 


 , WESTLAND ROW , DUBLIN , 


 VICTORIA . MBANKMENT , E.C. 


 SCHOOL 


 ORATORIO RECITATIVE , 


 BURLINGTON HALL , BURLINGTON ST . , REGENT ST 


 GU ILD ORGAN ISTS , 


 35 , WELLINGTON STREET , STRAND , W.C 


 ; BIRMINGHAM SCHOOL SINGING 


 annual CONSTABULARY CONC 


 NEWCASTLE - - TYNE , 


 ERT 


 TOVELLO oratorio concert 


 BOROUGH HACKNEY CHORAL | 


 association 


 EGYPT CAME 


 B.A 


 RED CROSS 


 ISRAEL 


 HENRY 


 . CHARLES FRY — _ — — — 


 chant VENIC 


 recital 


 MER 


 LRINTTY 


 party 


 MUSICAL TIMES 


 SEPTEMBER , 1887professional notice . 
 MISS MARIAN B ( soprano ) . 
 Concerts , Oratorios , & c. , address , Mr. Brook Sampson , Mus . Bac 

 Beethoven House mpton 

 MISS ADA BECK ( soprano ) 
 ( pupil late Mr. Welch ) . 
 Oratorios , Concerts , & dress , Brompton Cottage , New 
 Brompton , cha utham 




 MADAME CARRIE 


 MADAME MINNIE GWYNN 


 MADAME CL ARA W ESTSopra ) , 
 MISS LOTTIE WEST orem 

 Beethoven Villa , King Edward Road , Hackne 

 MISS MARY WILLIS ( Mezzo - Soprano Contralte 
 ( pupil late Madame Sainton - Dolby , Assistant Professor 
 = ademy ; Professor Hyde Park Academy Music ) . 
 Oratorios , Concerts , & c. , address , 9 , Rochester Terrace , ' 
 Camden Road , _ N. WwW 




 D AKIN , R.A.M 


 MISS MARY E 


 MISS HELEN 


 MR 


 MR . S. BOYCE 


 MR . ARTHUR FOX , A.R.A.M. 


 MR 


 MR . HENRY BAILEY 


 72 


 515 


 MR . T. E 


 MR . EDWARD MILLS 


 MS SS LEILA BAIRSTOW 


 VV ISS FANNY 


 R. BROUGHTON B LACK ( B 


 MBLER 


 516 


 MR . FRANK 


 ( BASS 


 ELIJAH ... « .. MENDELSSOHN 


 ... ROSSINI 


 PURCELL 


 JUBILEE CANTATA . WEBER . 


 QUEEN STERNDALE BENNETT . 


 MARTYR ANTIOCH SULLIVAN , 


 ANCIENT MARINER « .. BARNETT . 


 SLEEPING BEAUTY 


 DAUGHTER JAIRUS 


 + » STAINER 


 BIRMINGHAM CONCERT PARTY 


 MISS CLARA SURGEY , 


 MISS EMILIE HARRIS , 


 MR . R. LLOYD JAMES 


 


 MR . GEORGE HARRISS 


 NM USIC SCHOOL.—CHURCH ENGLAND 


 260 


 


 1 . M. , 


 S. 20 


 AR 


 517 


 IANO , HARMONY 


 COUNTERPOINT 


 XUM 


 DIANOFORTE REP 


 28 


 IC W EHOUSE . G 


 USIC B USINI 


 JSINESS 


 ATOR 


 NER 


 AGE 


 AGE . 


 M R 


 1875.—¢ 


 CH . J. B. COLLIN - MEZIN , 


 VIOLIN , - OLONCELLO , LOW MAKER , 


 ARTISTIC HOUSE 


 ANC AL CHURCH 


 COLLEGE , LONDON 


 EARL O 


 ABERDEEN 


 SESSION 1887 - 8 


 CLASSES lecture 


 IAS TERM COMMENCES SEPTEMBER 26 . 


 READY 


 PRESSLY WORCESTER MUSICAL 


 FESTIVAL , 1887 


 


 JOSEPH BENNETT 


 compose 


 COWEN 


 MUSIC 


 FRED 


 LONDON : NOVELLO , EWER CO 


 ADMIRABLY SUITED 


 DRIV INGORGAN BELLOWS 


 < che 


 follow CHU ! 


 use 


 cathedral 


 MAN 


 STE R 


 LE READY 


 NGING 


 ATTCT 


 KROAUSEN 


 LOWE . 


 PRICE SHILLINGS 


 LONDON : NOVELLO , EWR CO 


 NOVELLO , EWER CO . 


 service 


 SACKE ED SONG 


 REV . DR . TROUTBECK 


 PRICE SIXPENCE 


 ditor . 


 a. G 


 U2 


 singing - class CIRCULAR 


 IZ 8 


 22 


 1887 


 prodigy 


 0 5 


 


 523 


 1724 , 


 


 XUM 


 524 


 great composer 


 XUM 


 XUM 


 OTR 


 35 


 forgotten book 


 528 


 ALC 


 53the MUSICAL TIMES.—Sepremser 1 , 1887 

 evoke listen music . 
 work perform Festival chiefly 
 Handel . selection Mozart , Haydn , 
 Beethoven , great admirer 
 , Handel prefer- 
 ence ' ' occasion 
 place . " Beethoven , frankly 
 confess slightly acquaint 
 work . exertion soloist 
 main fail , allotment 
 open grave question . director 
 meet obstacle lack 
 ' ' prima donna assoluta . " ' Malibran 
 Pasta available , Catalani retire . 
 decidedly prominent singer , 
 author opinion , Braham , , 
 advanced year , possession 
 power , fill Abbey ease , fault 
 find taste judgment 
 performance . woman , consider 
 Caradori incontestably good . ' * 
 sing usual excellence , . . . heard 
 ... universal satisfaction , em- 
 ploye little , song 
 allot course concert 

 second order merit place Miss Stephens 

 new Academy Music , 
 Miss Clara Novello , " young girl 

 clear , good voice , " 
 man favourite , like 
 Vaughan Bellamy , know per- 
 , Sapio , fine tenor , Seguin Phillips , 
 good bass , deserve notice , 
 morc allot . " numerically 
 concert certainly provide vocal- 
 ist , " , insist , number amend 
 want excellence . principal 
 , exclusive opera singer — Grisi , Rubini , 
 Ivanhoff , Zuchelli , Tamburini . Tam- 
 burini . Grisi break rehearsal — 
 unlike , add , generation Italians . 
 sing Handel perfection ! ' entirely 
 bad modern opera music unfit singer every- 
 thing good . " day performance open 
 Coronation Anthem ' Zadok Priest , " 
 follow Haydn " creation , " , 
 single number high praise , 
 pronounce languid insipid . 
 * verdure clothe , " sing Caradori , 
 feature concert , ' ' heaven 
 tell " produce great impression . 
 day performance selection 
 * * Samson , " Braham distinguish , 
 second day open extremely miscel- 
 laneous selection , consist fragment 
 work Haydn , Mozart , Beethoven . 
 " mangiing system " Lord Mount- 
 Edgcumbe inveigh severity , 
 impossible conceive great patchwork 
 Concert . 
 consist wholly Handel " Israel Egypt , " 
 good deal interpolation word 
 music , write 

 far good . 

 

 Emperor , " ' soft pleasing melody , " 
 rest occupy 
 Handel " Judas Maccabeus . " connection 
 cello accompaniment , play Lindley 
 air " O Liberty , " Lord Mount - Edgcumbe offer 
 interesting pertinent remark abuse 
 cadenzas ad libitum . ' wish 
 ( Lindley ) finish symphony longa 
 cadence , accordance air . 
 , way , remark instrumental , 
 vocal performer apt indulge 
 bad habit tediously spin cadence 
 unreasonable length . , especially , 
 great fault . cadenzas 
 ad libitum invariably close song , 
 positive rule confine , shake include , 
 breath . acadence fiatt Italians 
 consider undisputed mark bad singer . 
 count respiration 
 cadence , deep shake , 
 disjoin passage 
 natural expected close . effect 
 indescribably bad . compress extem- 
 poraneous effusion short compass 
 sustain husband breath effect , 
 require skill lengthen desultory 
 wandering hear . " 
 second concert miscellaneous , 
 begin Motett Mozart " sort 
 | latin verse doggerel rhyme common roman 
 | catholic service , offensive english 
 ‘ classical ear . " Lord Mount - Edgcumbe comment 
 ! use boy chorister Pergolesi 
 | * * Gloria excelsis " following term , 
 | worth quote : ' * boy sing 
 ; clear bell - like tone 
 ' charming effect cathedral service , 
 ' sufficiently form bring 
 ‘ forward principal occasion like . 
 | exhibition like schoolboy recite 
 ' lesson public , thing 
 ' select partial audience . " Braham 
 performance ' " deep deep " 
 | * * waft , angel , " pronounce per- 
 fect , , , good performance 
 Festival . follow double chorus Leo , 
 air litany Mozart , Latinity 
 sore trial Lord Mount - Edgcumbe . 
 piece , remark , Latin , crab 
 , admit simple rendering — 
 transubsiantiation . ' remainder 
 selection Beethoven ' Mount 
 Olives , " ' beautiful . " 
 consist ot anthem Purcell 
 Pergolesi , air Handel " Solomon " 
 " Joshua , " Sestet Haydn service , 
 Clara Novello distinguish _ , 
 quartet Himmel , lead Tamburini , 
 prominent practically re- 
 solve solo . fourth con- 
 cert consist solely " Messiah , " error 
 complain — little use good 
 singer — conspicuous 
 precede day . Braham , Caradori , Miss 
 Stephens air apiece , bass song 
 divide singer , Seguin , good , 
 . curious tell 
 owe lack good counter - tenor , ' * 
 despise " , faute de mieux , assign female 
 contralto , " shall feed flock . " 
 Lord Mount - Edgcumbe add ' 
 occasion execute perfection 
 Rubineili . " sum judgment light 
 recollection early festival , 

 Handel . day begin Haydn " hymn | instrumental nearly 




 XUM 


 MUSICAL TIMES 


 TEMBER 


 1887 


 531 


 musical portrait . 


 III.—THE PRIMA DONNA 


 532 


 IV.—THE PRIMA DONNA HUSBAND 


 J EEE ES OEE 


 XUM 


 MUSICAL TIME 


 MUSIC EMBRYO 


 S.—SEPTEMBER , 1887 . 533 


 534 


 xum 


 xum 


 536 


 DR . MACKENZIE JUBILEE ODE AUSTRALIA 


 25 


 b 


 


 xum 


 537 


 WELSH NATIONAL EISTEDDFOD 


 538 


 MUSIC SOUTH WALES . 


 ( correspondent 


 xum 


 1887 . 539 


 obituarycert limited scale . 1550 , 
 | Pasdeloup conduct monster concert , organise 
 ! german resident Paris , hold 
 lcovered arena Champs Elysces commemo- 
 lration centenary birth Schiller , 
 lthat idea occur 

 lestablishe concert classical music similarly 
 spacious locality . , 1861 , enterprising con- 
 ductor , support influential patronage , inaugurate 
 | scheme ' classical concert ' popular price 
 Cirque Napoléon . success truly surprising , 
 | admirable concert mainly 
 low french audience familiarise 
 work Mozart , Beethoven , Weber , Schubert , 
 classical school , hitherto accessible 
 academical wail Conservatoire . 
 lis M. Pasdeloup undying merit , rival institu- 
 | tion spring course time french capital , 
 { eventually outbid original institution 
 | matter popularity , founder Concer Populaires 
 secure honourable place annal 
 |of french musical history . non - success 
 artistic undertaking end career 
 way eliminate 

 

 Tue general Committee Norfolk Norwich 
 Musical Festival hold meeting Norwich Guildhall , 
 Saturday , 13thult . , receive report sub- 
 committee respect arrangement ap- 
 proaching Festival . report , read Mr. C. R. Gilman , 
 honorary secretary , state arrangement 
 hold Festival 11th , 12th , 13th , 
 | 14th October , principal vocalist engage 
 lare Madame Albani , Miss Liza Lehmann , Miss Annie 
 | Marriott , Miss Hilda Wilson , Miss Lena Little ; Mr. 
 | Edward Lloyd , Mr. Charles Wade , Mr. Barton McGuckin , 
 | Mr. Santley , Mr. Alec Marsh , Mr. Brockbank , Mr. 
 | Barrington Foote . Dr. Bunnett organist , Dr. 
 Hill chorus- master , Mr. Alberto Randegger 
 ji conductor . programme follow : — 
 | Tuesday evening , October 11 , ' Jubilee Ode " ( A. C. 
 | Mackenzie ) ; Psalm xix . , ' " heaven declare " ' 
 | ( Camille Saint - Saéns ) ; ' hymn Praise " ( Mendels- 
 } sohn ) . Wednesday morning , October 12 , ' Garden 
 Olivet * ' ( Bottesini ) , devotional Oratorio compose 
 expressly Festival , conduct com- 
 poser ; ' * Stabat Mater ' ? ( A. Dvorak ) . Wednesday 
 ing , October 12 , miscellaneous concert . Thursday 
 morning , October 13 , Oratorio " Isaiah " ( Luigi Mancinelli ) , 
 compose expressly Festival , conduct 
 composer ; ' ' Fourth Mass " ( Cherubini ) . Thursday even- 
 ing , October 13 , symphony F minor , " Irish " 
 ( C. V. Stanford ) , conduct composer ; Cantata , 
 " * golden Legend " ( Sir Arthur Sullivan ) , conduct 
 composer . Friday morning , October 14 , Oratorio , ' * 
 Messiah " ( Handel ) , Friday evening , October 14 , dramatic 
 legend , ' ' Faust ' ( H. Berlioz 

 tue second Examination Degree Bachelor 
 Music University Oxford commence 
 Tuesday , October 18 , 10 a.m. , Schools . addi- 
 tion usual subject , require critical 
 knowledge score Beethoven * Fidelio " ( in- 
 clude e major overture / y ) Mozart Symphony 
 E flat . candidate require bring score 
 . examination Degree Doctor Music 
 commence time place . 
 examination occupy 
 day . candidate exercise approve , 
 propose offer 
 examination , require Mr. 
 George Parker , Clerk Schools , 
 October 1 , pay statutable fee £ 2 , exhibit 
 " * testamur " having pass previous examina 

 




 540enlargement church year ago . 2oth 
 inst.—the Feast Dedication church — Bennett 
 " Woman Samaria " sing 

 Sunday , July 17 , interesting Centenary Ser- 
 vice hold Church St. john Baptist , 
 St. John Square , Clerkenwell , year 
 | ago Rev. John Wesley , Rev. W. Romaine , 
 Rey . Rowland Hill preach pulpit use 
 | behalf Finsbury Dispensary , found 1780 . 
 sermon offertory aid object , 
 | patronage Lord Mayor Sheriffs . 
 morning preacher Rev. Philip Bainbrigge , M.A , , 
 Rector St. Thomas ’ s , Regent Street . evening 
 preacher Rev. Robert Maguire , D.D. , Rector 
 | St. Olave , Southwark . pga ' direction Rector 
 lof St. John ’ s , Rev. . Dawson , M.A. , careful 
 | complete arrangement , hymn 
 service Wesley , particular desire 
 Svan morning ' * o thousand 
 tongue sing , " evening , " * ye servant 
 lof G God , Master proclaim . " end book 
 | musical Services , Beethoven Haydn , interesting 
 | biographical notice Rev. John Wesley , 
 Rev. W. Romaine , ' Rev. Rowland Hill , , 
 R. Moreland , Honorary Secretary . 
 | view ancient beautiful crypt St. John , , 
 upwards 700 year old , excellent pre- 
 servation . copy interesting book 
 obtain apply Honorary Secretary , 
 Finsbury Dispensary , Brewer Street , Goswell Road , E.C 

 Tue Borough Hackney Choral Association begin 
 season 1837 - 88 , October 31 , Shoreditch Town 
 conductor , Mr. Ebenezer Prout , B.A. 




 1888 


 XUM 


 - song 


 EE 


 xum 


 M79 


 OH art THOU dream 


 SS 


 WA 


 XN p 


 5 ? ) SE 


 2 — — — 6 f 


 oh ART THOU DREA 


 MING 


 EL SEED 


 — — — ~ — 4 


 hl 


 TTI 


 5 - 47 


 review . 


 xumsong , word german writer . com- 
 position , rule , largely developed 
 op . 4 , amateur reckon 
 conspicuously valuable . . 1 , ' secret , " 
 suggest Schubert manifest fashion , partake largely 

 ea4 
 Emperor , order performance | composer lyrical beauty , occasion , frank 3 
 Tuileries . " stage erect salon de Diane , | geniality . favourite . . 2 , ' 
 Offenbach conduct orchestra brilliant | gentle Touch , ' abound pathos deepen [ > 
 audience blaze diamond . majesty | solemnity , , musically , altogether beautiful . no.3 , ff 
 row , near Count Bacciochi , | ' passage Bird , " rank . 1 fulness J = 
 Grand Chamberlain , arm wand . Pradeau | development , remain example pre- 
 Berthelier [ representative ' blind " | , | sent feature , technical expressive , lift far oo 
 think — impose audience , feel thousand con- | common order . final set song ( op . 
 fuse apprehension awaken heart . | 19 ) , word german author , present : 
 find judge critical | nee eked indicate , ter proof _ 
 general rehearsal ? effect long burst | composer genius undergo continuous rapid oa 
 laughter sovereign ? famous duct | development . far detail , 
 barely finish , terrified gesture , Pradeau | heartily recommend book amateur learn . ' 
 point companion M. Bacciochi , signal | appreciate german song . find abundance vs 
 vehemently wave arm . | matter dwell delight . oo 
 doubt . sinister presage 
 realise . Empress evidently ane piece | Notes Notions Music . N. Kilburn , Mus . Bac . , 
 stop ; , arm , trombone , |Cantab . [ James Burns . ] 
 guitar , Pradeau Berthelier leave Tue author , preface , tell note 7 
 Stage , profound amazement present . | « outcome practical experience , th 
 Chamberlain , dismay , rush , find | idea mere student . " article , note , inform hor 
 * * Salle des Maréchaux , _ breathless ' commonly believe genius 
 emotion , language variegate Italian , informed original , " parallel passage different lie 
 eed alarm composer musical note ' kind 
 — kant bisser ( 1 . ) signal snl encore ) . common fund composer help | 
 M. Martinet amusing picture later | fount inspiration fail . " 
 chapter F riday — . es Offenbach thing state lofty oracular form , ma 
 house , sort musical absurdity new discovery , , 
 enact Bizet , Gustave Doré , Délibes , friend . author condescend enter scene , 
 read , occasion , ' Farmyard Symphony world know . musician ( 
 perform , - composer create | extremely grateful author kindness place ( oc 
 furore rendering ' — little dog | hefore new fact Beethoven deaf , , 
 foot tread feature Macfarren blind , Mozart great won- 
 pone : character especially command respect | erful genius , Mendelssohn great faculty gra 
 sok poe xa bes page de " | ines extemporise piano , Wagner use rt 
 vol nee igs @ Geath coulc | advanced musical mean . fact 
 ate , rlume preiced py characterise poa uste print bf . 
 f ‘ titious Itali tisk ' ell 5 q ‘ 4 , read chapter " musical vi 
 ERY SUPCER Ee StARN , 16 es SePTOORENS . culture , " feel glow pride tell 8 
 eighteen Songs . pianoforte accompaniment . : ts bak aber Sugiand py ve y 
 compose Hermann Goetz ( Op . 4 , 12 , and1 g ) . COMPOS UOnSias Messiah , Creation , ' E ti Ma 
 english version Rev. Dr. Troutbeck . continue hold peg place ae — 4 ee 
 ( Novello , Ewer Co.| tion people " ? " brave ’ ord bridge , | 
 brave : ' bear mind alr 
 pen Hermann Goetz — gifted } music end oratorio . ' 
 die soon fame art — come | literary man , considerable position world 

 xum 




 xumthe matter pupil ; professor 
 | think ; remember 

 amateur purchase Albums , play content 
 communication teacher . objection 
 apply considerable force 
 extract " ' Andante F — Beethoven , " ( apart 
 convey idea commence lengthy 
 piece ) distinctive titlk — save musician , 
 accustom hear - — likely con- 
 found Andante F composer . 
 little piece , , complete them- 
 self , abstractedly excellent theme linger 
 

 Treasury . compile edit Joseph B. Mead . 
 { publish Trustees 




 10 . J 


 XUM 


 MUSICAL time 


 foreign note 


 552Byron ' Manfred ' translate Hungarian , 
 shortly produce , Schumann music , 
 opera house Buda - Pesth 

 German Opera New York open door 
 November 2 , performance ' Tristan und Isolde . " 
 work obtain hearing 
 season ( extend sixteen week ) instance 
 " Der Ring des Nibelungen " ( exception 
 " Rheingold " ) , ' Tannhauser , " ' ' Die Meistersinger , " 
 " Rienzi ' ? ( Wagner ) , ' Fidelio " ( Beethoven ) , * * Euryanthe 

 Weber ) , ' Fernando Cortez ’' ( Spontini ) , ' Merlin " 
 { Goldmark ) , ' " Aida " ( verdi)—assuredly worthy 
 programme . Mesdames Lilli Lehmann , Krauss , 




 CORRESPONDENCE 


 DENMAN LORD PRAYER , 


 editor " MUSICAL TIMES 


 C. OLDERSHAW . 


 DOUBTFUL DEGREES . TREMAIN concerto 


 editor ' * MUSICAL TIMES . " editor ' * MUSICAL TIMES 


 editor ' * MUSICAL TIMES 


 FEA PNT TTR NI ttpott 

 article , think , information ) Albrechtsberger | Beethoven master ) . 
 Society Science , Letters , Art.—Yours faithfully , publish Forberg , second Siegel , 
 A. E. Rype - Rocers . | Leipzig , Pohle Hamburg , 
 3elper , August 19 , 1887 . | procure Messrs. Augener London . 
 , Sir , obediently , 
 — | Alby Hill House , R. H. Lecce 

 ae | Hanworth , Norfolk . 
 editor " * MUSICAL TIMES , 
 Sir,—I favour ( organist , aoe 
 imagine ) invitation join Society Science , | CELLO . 
 letter , Art , Fellowship offer , | editor " MUSICAL TIMES . " 
 chief condition , far remember , payment ] ; . 
 fee reference professional . unfortunately | _ s!8,—may suggest correspondent , addi- 
 destroy circular receipt , | tion solo trio , beautiful 
 follow , appear Kentish Express of| form composition — , song , violoncello 
 July 30 , 1887 , Society require examina- | obbligato , * * Waterlily , " Horsley ; 
 tion : — Mr. Stroud , Master St. Michael National | Guiding Star , Fesca , publish Augener . 1S 
 School , pass examination Science Arts , charming song " * ’ Bower , " Slatter , 
 include examination Harmony Counter- publish Weekes . ' good , calculated 
 point , Society Science , Letters , Art , London . | procure pleasure hearer player 




 ORIGIN musical phrase 


 editor * MUSICAL TIMES 


 MR . JOSEPH KERFOOT 


 J. MAKEHAM 


 554 


 correspondent 


 brief summary country NEWS 


 30of 

 BuRTON - - TRENT.—The new organ , construct A. Kirkland , 
 Holloway , New Street Baptist Chz ne l , ope ne Tuesday , 
 231d ult . , introductory service . rrand Organ Recital 
 evening Mr. E. H. Turpin , perform piece 
 Molique , Beethoven , Handel , Bach , Morandi , Guilmant , Capoccl , | 
 Spohr , Weber , composer 

 gratification audience . 
 Cnicaco.—A song recital excerpt Rossini , Mozart , Schu 
 bert , Taubert , Raff , Jensen , Jiingst , Warren , Proch , Sullivan , 
 M 3enie M. Baldwin , soprano , pupil Mrs. E. | y 
 Wednescay evening , July 27 , 1887 , o’clock , pia 
 wareroom Messrs. Lyon Healy . Mr. Clarence Eddy play 
 accompaniment . recital attend complet tely 
 successful , Miss Baldwin voice singing greatly acmire 




 LOTR ! 


 YUM 


 XUM 


 556 


 ew , revise , enlarge edition 


 UNING BUSIN 


 CHURCH ORGANIST 


 collection piece use dure 


 DIVINE service 


 1887 


 songscompose 

 BEETHOVEN 

 Dr. TROUTBECK 




 L. VAN 


 VOLUME I. 


 content , 


 floweret 


 album italian song 


 - song SET italian word , 


 accompaniment PIANOFORTE 


 compose 


 ANGELO MARIANI 


 NEW edition , carefully revise . 


 MENDELSSOHN overture 


 NEW edition , carefully revise 


 MENDELSSOHN symphony 


 ord 


 e 


 557 


 VIOLIN BOW 


 maker repairer , 


 GEORGE WITHERS & CO . 


 WHOLESALE importer 


 musical string 


 fine collection italian instrument 


 51 , ST . MARTIN LANE , LONDON 


 MESSRS . BEARE SON 


 manufacturer importer 


 description 


 musical instrument , 


 34 , RATHBONE PLACE , LONDON , W 


 MEKCHANTS , shipper , trade supply 


 newly revise illustrate catalogue 


 _ JAMES CONACHER son 


 ORGAN BUILDERS , 


 BATH BUILDINGS , HUDDERSFIELD . 


 EUSTACE INGRAM 


 ORGAN BUILDER , 


 BURNARD PLACE , HOLLOWAY , LONDON , N 


 P. CONACHER & CO . 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD . 


 establish 1854 . 


 specification free APPLICA TION 


 NICHOLSON CO . , 


 ORGAN BUILDERS , 


 PALACE YARD , WORCESTER . 


 ( establish 1831 . ) 


 CHOIRMASTERS & MUSICAL SOCIETIES 


 558 


 MUSICAL student profession 


 NEW PATENT TOUCH regulator 


 invent 


 PRICE guinea 


 rummen ' ’ 


 ORGAN PEDAL attachment 


 pianoforte 


 home practice ORGAN MUSIC 


 ready u length 


 se , 


 time , 


 47 , CHURCH ROAD , BRIXTON , LONDON , S.W 


 NOVELLO , EWER & CO . 


 primer 


 musical biography 


 JOSEPH BENNETT 


 READY 


 | HECTOR BERLIOZ 


 FREDERIC CHOPIN 


 ROSSINI 


 MEYERBEER 


 CHERUBINI 


 PROSPECTUS 


 LONDON : NOVELLO , EWER CO 


 E.C 


 ER 


 XUM 


 HARVEST . ANTHE MS 


 hymn 


 HARVEST 


 select " hymnary 


 PRICE penny 


 TUNES 


 thanksgiving 


 organ 


 


 J. BAPTISTE CALKIN . 


 


 JUBILEE CANTATA 


 


 ( HARVEST CANTATA ) 


 SOLO voice , chorus , orchestra 


 compose 


 C. M. VON WEBER . 


 ARCH 


 BARN ARVEST 


 1887 . 559 


 | PRAISE LORD , 0 JERUSALEM 


 anthem harvest thanksgiving 


 special service 


 


 REV . E. V. HALL . 


 NEW HARVEST nthem . 


 | earth REMAINETH 


 W. H. HOLLOWAY 


 tuneful difficult . 


 MALLWOOD ( WM . ) fest 


 anthem 


 pr aise LORD DAILY . 


 4 . magnify THEE , o GOD , 


 LBE " LOWE HARVEST anthem 


 ADE RT LOWE HARVEST ROL , 


 NEW 


 HARVEST 


 M 


 IC 


 HARVEST anthem 


 560 


 VOCAL score . old notation . TWELFTH THOUSAND 


 RUTH 


 HARVEST PASTORAL 


 word 


 EDWARD OXENFORD 


 music 


 ALFRED R. GAUL 


 suitable 


 thanksgiving service 


 HARVEST anthem 


 compose 


 E. A. SYDENHAM . 


 O thank unto LORD 


 NEW work HARVEST festival 


 compose 


 COME , ye thankful PEOPLE , come . 


 COME , ye th ANKF UL PEOPLE , come . 


 HARVEST anthem 


 O thank unto LORD 


 


 perform TOWN HALL , BIRMINGHAM , 


 occasion 


 SEED time HARVEST 


 HARVEST carol 


 compose 


 HERBERT TOWNSEND . 


 OQ man praise LORD 


 ( anthem HARVEST festival ) 


 


 DR . JOSEPH C. BRIDGE , M.A 


 HARVEST truly great 


 short , anthem HARVEST , EMBER day , 


 compose 


 W. W. PEARSON . 


 HARVEST MUSIC . | 


 HARVEST CANTATA \ 


 glad 


 HARVEST HYMN 


 NEW HARVEST ANTHEM 


 eye wait THEE 


 NEW HARVEST ANTHE M. 


 NEW anthem HARVEST . 


 raise LORD , o JERUSALEM . 


 rd 


 IBER , 1887 . 


 work 


 JOHN FARME 


 HARROW SCHOOL 


 HARROW GLEE BOOK 


 CHRIST soldier . 


 REQUIEM 


 FROG WOU - WOOING 


 t \¢ 4 t « 


 561 


 SHOOL 


 O 6 


 LONDON : NOVELLO , EWER CO 


 7 9 


 2 8 


 EWER CO , 


 TE 


 NOVELLO , 


 ALBUMS VIOLIN PIANOFOR 


 SOPRANO 


 COMPO 


 


 compose 


 


 transcribe 


 1857 


 SSS VE VOCAL duet 


 CONTRALTO 


 se 


 CIRO PINSUTI . 


 K II 


 BERTHOLD TOURS 


 SOPRANO 


 THIRTY FREDERIC 


 melody 


 compose 


 BERTHOLD TOURS . 


 GOUNOD " MORS ET VITA " NYLES B. 


 BATTISON HAYNES . MENDEL 


 7 THIRTEEN 


 IPPOLITO RAGGHIANTI 


 MENDEL 


 * P IECES shilling 


 VOVELLO 


 F 


 lady ’ 


 thirteen 


 S ] duet S 


 CONTRAL 


 


 H. COWEN 


 _ — 2 duet 


 transcription SOPRANO CONTRALTO 


 


 MENDELSSOHN " ELIJAH " OLIVER KING . 


 song 


 boy ’ voice 


 FOSTER 


 LO , EWER 


 SSOHN 


 NOVELLO FOLIO EDITION 


 SSOHN 


 ) -PART song 


 SIXPENCE 


 ALB UMS 


 48 


 GERMAN SONG 


 select , word translate ENGLISH , 


 


 FRANCIS HUEFFER 


 SIXPENCE 


 PRICE shilling 


 song 


 ROBERT FRANZ 


 PE ANZ 


 LI 


 TWE S 


 ANTON RUBINSTEIN 


 - seven 


 JOHANNES BRAH 


 t 


 LONDON : NOVELLO , EWE 


 XUM 


 563 


 SCHUBERT song 


 select 


 NATALIA MACFARREN . 


 PRICE shilling SIXPENCE 


 song 


 MEZZO - SOPRANO 


 content , 


 SONGS 


 CONTRALTO 


 content , 


 ENTY 


 


 — TWE n TY song 


 SOPRANO TENOR 


 content . 


 ( SWAN SONGS ) 


 LONDON : NOVELLO , EWER CO 


 READY . 


 » BIRMINGHAM FESTIVAL CHORAL SOCIETY 


 JOAN ARC 


 MAID ORLEANS " 


 HISTORICAL CANTATA 


 verse FREDERICK ENOCH 


 LONDON & NEW YORK : NOVELLO , EWER CO 


 MOLW ( 


 CENWC 


 0 PRA\ 


 0 ARGI 


 YW DY 


 XUM 


 


 BENEDICITE , 


 , WATCHMAN , " night 


 spirit long THEE . 


 FOE , deep . 


 CENWCH GAN O FAWL . J. STAINER . 


 0 ARGLWYDD , MOR LIOSOG ) 


 CHURCH MU 


 MUSIC 


 


 DR . FRANK BATES . 


 XUM 


 READY 


 LORD REIGNETH 


 | ( PSALM XCHI . ) 


 | ROBERT PARKER PAINE . 


 | PRICE SHILLING 


 school cant song 


 captive BABYLO 


 ORATORIO 


 compose 


 LONDON : HART & CO . , 22 , PATERNOSTER ROW , E.C 


 month | 


 CHORAL society . song 


 


 3 4 4 | 


 POIN 


 XUM 


 02 


 02 


 02 


 02 


 02 


 02 


 DH H HN 


 second serie 


 


 CANTICLES 


 


 HYMNS 


 CHURCH 


 point chanting , SET appropriate anglican chant , SINGLE DOUBLE 


 


 response commandment 


 edit 


 REV . SIR F. A. GORE 


 OUSELEY , BART . , M.A 


 


 EDWIN GEORGE MONK 


 MORNING & E VENI \G service 


 office HOLY ' COMMUNION 


 HARVEY LOHR . 


 office HOLY COMMUNION F. 


 F. H. CHEETHAM 


 PEAIN SETTING 


 


 office HOLY COMMUNION 


 BENEDICTUS AGNUS ‘ DEL , general use . 


 XUM 


 psalm 


 bible version 


 point chanting 


 REV . DR . TROUTB ECK 


 CHRISTMAS ANTHEM 


 EMMA MUNDELLA . 


 anthem treble & ALTO voice 


 EMMA MU NDELLA 


 EMMA MUNDELL 


 DR . STAINER . 


 568 


 BRISTOL TUNE - BOOK 


 match edition 


 BOOK PSALMS ( BIBLE version ) , 


 ANGLICAN hymn - BOOK 


 G A. MACFARREN CANTATE DOMINO , 


 reduce threepence 


 A. MACFARREN - anthem | 


 NEW anthem , VIOLIN OBBLIGATO . , 


 teach thy way , o LORD 


 festival setting 


 CANTATE DOMINO & DEUS MISEREATUR 


 compose 


 JOHN J. BAILEY 


 MAGNIFICAT & NUNC DIMITTIS 


 ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSAI 


 MONK PSALTE R 


 OULE collection 


 527 chant , > 


 PSALTER , PROPER " PS AL MS , HY MINS 


 OULE DIRECTORIU M _ CHORI ANGLI- 


 CHURCH M 


 MUSIC 


 - song 


 V 


 th 


 569 


 MERRY JUNE 


 300 K 


 ER 


 2 


 , 5 


 NGLI 


 NITION , 


 NETS , 


 GAN : 


 TRIOS FE 


 CRYSTAL PALACE 


 TTIND 


 VOCAL POLKA 


 MERRY JUNE " 


 ‘ ( OCAL POLKA 


 MERRY 


 JUNE 


 song 


 - poem ROBERT BURNS 


 


 GEORGE J. BENNETT . 


 ALE 


 voice 


 NOVE ! 


 ORG : MU SIC 


 SHORT EASY piece ' 


 ompose 


 EI SUNNETT 


 LARGO E FLAT 


 CAVATINA 


 ( F ) | 


 1 PIANOFORTE ACCOMPANIMENT | 


 compose 


 FRANCESCO BERGER 


 WERA 


 SUNG MADAME ALBAN 


 CROWN MONARCH 


 precious 


 R SOPRANO JUBILEE 


 compose 


 SOLO FO ODE 


 A. © . MACKENZIE . 


 EMPIRE FLAG 


 ORUS 


 compose 


 A. C. MACKENZIE . 


 | plight vo\ 


 duet SOPRANO TENOR 


 MUSIC COMPOS ! 


 D 


 SONG , harmonium ¢ 1an accompaniment 


 th 


 H. W. LONG PEL LOW . 


 ARS SON . 


 selection " JUBILEE 


 publish B 


 W. W. PE 


 LYRIC " 


 


 


 SIR HERBERT OAKELEY , 


 ‘ organist VIL LAGE 


 JOHN GILPIN 


 GEORGE FOX . 


 MASS 


 TH ! 


 memory JOAN ARC 


 CHARLES GOUNOD 


 HARMONY & THOROUGH - BASS 


 use class 


 aring 


 nation 


 


 new humorous - song 


 


 MERRY dwarf 


 write 


 EDWARD OXENFORD . 


 compose 


 A. C. MACKENZIE . 


 festival setting 


 


 magnificat NUNC DIMITTIS 


 accompaniment ORGAN ORCHESTRA . 


 domestic life 


 duet pianoforte 


 J. MOSCHELI 


 PUBL D 


 JUVENILE - ALBUM 


 characteristic piece 


 ( hand 


 BERTHOLD tour 


 NOVELLO , e } R CO . 


 album violin pianoforte 


 HUNGARIAN dance 


 SUIT 


 transcribe 


 STEGPRIED JACOBY . 


 CHOK . AL SOCIETI ES 


 able OPE [ ning concert 


 ENSU ENG SEASON 


 COMPO D 


 FESTIV AL CONCERT 


 CRYSTAL PALACE 


 JUNE 22 , 1887 


 write 


 JOSEPH BENNETT 


 music compose 


 A. CG . MACKENZIE 


 PRICE shilling SIXPENCE 


 LONDON : NOVELLO , EWER CO 


 WA 


 CONCONE 


 lesson 


 NEW edition 


 expression & phrasing 


 ALBERTO RANDEGGER 


 NEW FOREIGN PUBLICATIONS1887 

 BEETHOVEN.—Funeral March Sonata , Op . 26 . lesan 
 arrange Pianoforte Duet S. Jadassohn 

 CHOP 
 Pia 




 32 


 HAUSER 


 HOFS 


 HUL L 


 ADA 


 KAAN 


 MATYS 


 ALBERTO RANDEGGER 


 LONDON : NOVELLO , EWER CO 


 MOSZ 


 ROSE 


 NH . AIN , ] 


 SCHARWENKA , 


 STORCH , E\ 


 THUI 


 LONDON : NOVELLO , EWER CO 


 571 


 NOVELLO , EWER & COU 


 MUSIC primer 


 ( continue 


 SAINT LUDMILA 


 ORATORIO 


 JAROSLAV VRCHLICKY . 


 ANTONIN DVORAK . 


 


 SPECTRE BRIDE 


 DRAMATIC CANTATA 


 write 


 K. J. ERBEN , 


 C compose 


 ANTONIN DVORAK 


 PAT RIOTIC HY ) MN 


 BOHEMIAN poem " HEIRS 


 WHITE MOUNTAIN " 


 write 


 VITESLAV HALEK . 


 ANTONIN DVORAK 


 SIC compose 


 STABAT MATER 


 SOLI , CHORUS , ORCHESTRA 


 pose 


 ANTONIN DVORAK 


 LONDON : NOV 


 CHORAL society 


 ELLO , EWER CO 


 , 1887 


 story SAYID 


 DRAMATIC CANTATA SOLO voice , 


 CHORUS , ORCHESTRA 


 LIBRETTO write 


 JOSE PH BENNETT 


 D 


 MACKENZIE 


 33 


 ROSE ‘ SH . ARC YN 


 DRAMATIC CANTATA , found 


 IC compose 


 A. ¢ [ AC KENZIE 


 JUBILEE ODE 


 compose 1 


 JUNE 22 , 1837 


 write 


 JOSEPH BENNET " . 


 A.C. MM . \C KENZIE . 


 Y BRIDE 


 CANTATA 


 translate GERMAN R. HAMERLING 


 compose 


 A. C. MACKENZIE 


 574 


 NOVELLO collection 


 FEM AL E VOICES . 


 recent NU } MBE RS 


 ORPHEUS 


 CENT NUN IBERS 


 IVELLO 


 PAR T - SONG book 


 recent numi 


 INT 


 NUM 


 . | 290 . B 3 


 3er 


 RQ 


 XUM 


 M 


 FTHE ORGAN 


 \K JOHN SEBASTIAN 


 J. F. BRIDGE 


 sonata 


 sonata 


 PEDAL 


 _ _ _ 


 ORGAN 


 compose 


 GEORGE CALKIN 


 VORE 


 3ACH 


 SICAL time 


 SOFT voluntary 


 RIGINAL 


 ENEL 


 OTT O DI 


 12 . " " " » SS 


 13 . " " " " 3 


 14 


 15 


 12 


 2 . | ? - 2 6 


 ITT 


 NYY ) TVCNC 


 " ORIGIN . AL 


 TIMES 


 MU SICAL 


 L COMPO 


 1887 


 ITIONS 


 LONDON : NOVELLO , EWER CO .   FACUL ? ; 


 XUM