


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


FOUNDED IN 1844. 


PUBLISHED ON THE FIRST OF EVERY MONTH


ROYAL CHORAL SOCIETY. THE ROYAL COLLEGE OF ORGANISTS


WEEKLY ORCHESTRAL PRACTICES ARE CONDUCTED 


EEDS TRIENNIAL MUSICAL FESTIVAL. HOLIAN HALL, NEW BOND STREET, W. 


SIR EDWARD ELGAR, O.M


SCHOLARSHIP


426 THE


I, 1913


VICTORIA COLLEGE 


LONDON. 


INCORPORATED 1801


OF MUSIC


A.R.C.M., I


INCORPORATED GUILD OF CHURCH 


MUSICIANS


ASSOCIATE (A.1.G.C.M.), LICENTIATE (L.LG.C.M.), FEL- 


1.G.C.M


COMPETITIONS FOR 1913


TWOPENCE


REGISTER OF ORGAN VACANCIES. 


MR. W. H. BREARE


A FINE MODERN VIOLIN, 


| —__—_—— ‘ 


SENT


OS


‘SOLD BY W. E. HILL & SONS


AT THEIR PLACE OF BUSINESS, 


140, NEW BOND STREET


LONDON, W


FIRM


OLD


THE


P. CONACHER & CO. 


PIANO PEDALS


EVERY ORGANIST


NORMAN & BEARD


PNEUMATIC PEDAL ATTACHMENT - 


P % » 145- 


PROFESSIONAL NOTICES. A. HUNSTON TAYLOR


MRLAR THUR GIDDINS


MR. FRANCIS GLYNN


MR. SAM U EL MASTERS


MR. MONTAGUE BORWELL


BARITONE


WINIFRED MARWOOD


124, WALM LANE, C RIC KLEWOOD, N.W


MR. ANDE -RSON NICOL


1003-14


, <_LES TREE 


MR. CHARLES +E. 


COMPOSER AND VOCALIST


TERMS MODERATE


THE LONDON COLLEGE FOR CHORISTERS. 


COMPOSERS’ MSS. 


DR. A. EAGLEFIELD HULL


L.R.A.M. RV


PAPER WORKR


VIG GO KIHL


SOLO PIANIST). 


LATEST SUCCESSES


428


I, 1913


DR. LEWIS’ TEXT-BOOKS


OMPLETE MU SIC. AL EDU CATION


A.M


NASAL VE. 


A.R.C.M.—ARTHUR MANGELSDORFF


COACHING FOR DEGREES I » 1897-1913, ON


HUNDRED AND TWENTY-SEVEN SUCCESSES ; A RGM


1897-1913, THREE HUNDRED ARD THIRTY-FOUR SUC


M*.: S( ‘OTT: BAKE R, AR A.M., L.R.A.M. 


HE


LICHFIELD C ATHE DRAL


ALL


CHOIR


SAINTS’, MARGARET STREET, 


CHOIR SCHOOL


MICHAEL’S COLLEGE, 


WELLS


TENBURY


ST


DUKE OF NEWC AS’ PL 4E'S CHOIR SC HOOL


GEORGE’ S CHOIR SCHOOL


ONTRALTO W AN’ T ED, 


PARIS


A.R.C.M., 7, R


RG ANIST AND CHOIRM.: ASTER WANTEL


VALUABLE ORGAN SCHOL ARSHIP 


BARNES OPERATIC AND DRAMATIC 


SOCIETY


AND INSTR


S.W


ANTEL


1913. 429


TOR SALE. - 


BUYS BROADWOOD GRAND PIANO


WHETMARSH VIOL IN, 


F BOUND


MUSICAL


OR SALE


TIMES


ONE ACCOUNT FOR ALL PUBLISHERS


MURDOCH'S CATALOGUES NOW READY 


1 VIOLIN MUSIC. >», EDUCATIONAL MUSIC


MURDOCH, MURDOCH & CO


PIANO, ORGAN, AND MUSIC STORES


461 AND 463, OXFORD STREET, W. 


ESTABLISHED 1750


GRAY & DAVISON, 


ORGAN BUILDERS, 


PRATT STREET, N.W. 


48, SOUTHFIELD ROAD, OXFORD


8, HOLT ROAD, LIVERPOOL


12


PIANO PEDALS


AUF WIEDERSE H EN 


COMPOSED KV 


A. HERBERT BREWER


SMALL ORCHESTRA


PROGRESSIVE STUDIES


FOR THE PIANOFORTE


EDITED, ARRANGED IN GROUPS, AND THE FINGERING REVISED AND SUPPLEME NTE


FRANKLIN TAYLOR


FIFTY-SIX BOOKS, PRICE ONE SHILLING EACH. 


FROM THE ABOVE


SELECTED PIANOFORTE STUDIES


FRANKLIN TAYLOR. 


IN TWO SETS (EIGHT BOOKS), PRICE ONE SHILLING AND SIXPENCE EACH BOOK


E NTED


INGERS


INDE 


LAYING


BOOK


1913


431


BRITISH


OLYMPIA, W


SEPTEMBER 6


ALL-BRITISH COMPETITIONS


IN VOCAL AND


IO13 


MUSIC EXHIBITION


INSTRUMENTAL


PRIZES


PRIZES TO THE


CLAUDE P. LANDI


CLASSES, INCLUDING


V ALUI E


L.R.A.M


OF


E.C


NOVELLO’S 


MUSIC FOR MILITARY BAND


LONDON


GEORGE


NOV EL L O AND COMPANY


LIMITED


MUSIC


4M ONNNY NPR ONNNS


TO THE VALUE OF OVER £1,000


TWENTY-NINE


OVER 41,000


432 THE MUSICAL


ARNE


AND


BRITANNIA


DR


RULE


WILLIAM HAYMAN CUMMINGS


I ‘ V 


ELECTRIC PASSENGER LIFT. 


BROADWOOD © 


| PLAYER-PIANOS


GRANDS AND UPRIGHTS. 


ILLUSTRATED CATALOGUE ON


APPLICATION


CONDUIT STREET, LONDON, W


BOS SWORTH & COS 


| NEW STUDIOS


JOHN


HOT-WATER RADIATORS 


AND ELECTRIC LIGHT


MINUTES W


WITHIN THREE ALK OF PICCADILLY


CIRCUS OR OXFORD CIRCUS TUBE STATIONS


I GOD IS OUR HOPE


ANTHEM 


FOR DOUBLE CHOIR, BASS SOLO, AND ORCHESIRA 


C H. H. PARRY


KET


ESLRA


433


AND SINGING-CLASS CIRCULAR


1913


PS


ACS 


437


TY 


438


THE MUSICAL 1


COW 


THE 


THE APPRAISEMENT OF 


MUSIC


PROGRAMME439

hand, a fact on which theory does not insist 
sufficiently is that the pattern, length, and tone of 
any motive predetermines its working-out. The 
motive of Beethoven’s fifth Symphony calls, inside 
the general pattern of a classical first movement, 
for other methods of working out than those 
which suit the motive of Beethoven’s sixth (as to 
modern music, an illuminative comparison may be 
made between the first sections of Borodin’s 
second and_ third—unfinished—Symphonies re- 
spectively). Creative faculty is displayed alike in 
inventing a theme and in working it out, possibly 
even more in the working-out. An _ inferior 
composer will expose himself as thoroughly through 
his treatment of a ‘pure, abstract’ motive as in a 
piece of ‘ materially descriptive’ music, despite the 
semblance of style that he may achieve by 
closely adhering to standard rules

The limitations of purely descriptive cr imitative 
music are obvious as soon as one overlooks the 
part played in it—and freely played—by creative 
imagination ; but that is begging the question, and 
it still remains to demonstrate that, for instance, 
M. Ravel’s ‘Jeux leau,’ although founded on 
nothing but imitative motives, do not afford as 
elaborate a display of purely musical imagination 
as one can wish for

44¢ THE MUSICAL TIMES.—JuLy 1, 1913

called conventional association, to show that the! be no emotional interest in the fact that the entries 
hero has not left the ground, borrowing its}in seconds aim at reproducing ‘the fashion of 
justification from ‘an analogy founded on a third} timorous singers.’ But for all that, the effect js 
term.’ | excellent : and had Kuhnau attempted straight. 
But in ‘pure’ music a variation or any part of | forwardly to express Gideon’s anguish, one may 
a work may as well as not be under- or overlaid | question whether he would have achieved better 
by a holding note. In the classical fugue, indeed, | than the harsh, tremulous seconds—a most daring 
the holding note appears—in view of a merely|effect at that time, and one that has long ag 
abstract musical effect—to assert the return to the | passed into the sphere of pure music. 
original tonality and emphasise the conclusion. Again we find all separate questions we have 
Again, in M. Ravel's ‘ Le Gibet ’—which is poetic | deal with ‘crossing and recrossing into each other 
music, but neither descriptive nor programmatic— | territory.’ But keeping them apart has at least 
a holding note obtains from beginning to end, its|enabled us to study each point in turn and to 
obvious purpose being to produce an effect of | suppress many causes of ambiguity. 
harping dreariness and obsession. So much has been written on the one remaining 
Very few such comparisons will suffice to show | point (that of the actual programme controlling 
the student that when he is told that materially | ‘the order in which phrases appear and the way in 
descriptive music is music of the lowest sort, he is| which they are played off against each other) 
asked blindly to endorse some dogmatic principle, | from Wagner’s ‘Letter on the tone-poems of 
some postulate which no amount of quibbling has| Franz lI.iszt’? to Mr. Newman’s essay on 
reduced as yet; and that if a closer study of the | ‘ Programme-music,’ that a few words of summing. 
matter proves anything, the proofs adduced are in| up may well suffice. 
favour of the principle of poetic music. Briefly, then, as imitative, descriptive and 
The stimulus afforded by suggestions, material | other poetic or dramatic elements suggest motives 
or immaterial, has intervened in more cases than| that may be more pregnant and more novel than 
we are aware of. For instance, we are told that| merely formal, abstract motives, so does a 
Beethoven imagined the motive of the Scherzo of | programme suggest new plastic forms. The more 
the ninth Symphony whilst watching from afar the | gifted the composer, the more satisfactory the 
lamp-lights of Vienna flare up one by one through|forms that he derives from his programmes 
the mist; and be the anecdote true or not, we} These forms may stand in close relationship wih 
find as little reason for accepting it as for|the ‘regular’ forms—a combination of two int 
rejecting it. lone, or the use of one with certain deviation 
If it is true, the one instance suffices to show being no uncommon occurrence—or have nothing 
how useful a material suggestion in the hands of | to do with any acknowledged type. 
a composer of genius may become, even to the | Now the perennity of any law of constructions 
ends of ‘pure’ music. But one should likewise | as great a superstition as any of the other dogmat 
note that, contrary to a current belief, the more | principles against which our student has been 
abstract data, even if ‘loftier,’ are not the most | more than once cautioned. Among the ma 
useful to the composer of poetic music. Many | abstract, general definitions of artistic beauty that 
are inclined to think that a high-sounding philo-| have been offered, none seems more to the pot! 
sophic or symbolic scheme must suit the purposes | than Diderot’s. ‘The sense of Beauty, the Fren 
of musical art better than the ripple of a brook or | philosopher said, originates in the notion of relation 
(exceptis excipiendis) the lighting of rows of lamps. | ship—a work of art being an independent whole. 
And this leads again to the everlasting confusion | that has a purport of its own, and whose several 
between mind and feeling. Music expresses | parts stand in obvious and satisfactory connectio. 
emotion through the mediums of sound and of |and artistic pleasure consisting of a perfect appre 
rhythm. Any of the material starting-points of | hension of those connections and of their purport. 
imitative or descriptive music suggest musical| A predetermined scheme like that of the classic 
elements that are good in proportion as they are | symphony, therefore, is a great help to the heare. 
themselves intrinsically beautiful and apt to convey | On the other hand, it does not contain the suff 
emotion. But an abstract notion in itself suggests | total of possible relationships, nor preclude other 
neither sound nor rhythm ; and the more abstract | satisfactory schemes. Artistic education consis’ 
it is, the less emotional. It does not properly|in acquiring the capacity of apprehending © 
appeal, therefore, as a stimulus to the composer’s| relationship between parts of a work so as t0 
imagination, but only in indirect and less profitable | understand the message conveyed by that work 
wise. ‘The holding-note in Dr. Strauss’s ‘Don|Why do so many people blindly adhere to the 
(Juixote’ is a mere freak, as puerile as the material ltenets of the past? Simply because they a 
imitation of the bleating of sheep in another|incapable of doing that much, exactly as mat! 
section of the same work. But it still remains | writers of music are incapable of going beyond the 
permitted to judge either from the musical point | routine of fixed methods and preordained schemes 
of view pure and simple. | Diderot’s definition helps to explain why — 
Another case in point is afforded by ‘Gideon’s| art judges believe pure music to stand on a highe 
doubts’ in Kuhnau’s Bible Sonata.* There can|level than poetic music. But after having

ially 
|how absolutely independent even — a 
S.e Musical Times, May, 1913, ‘ The Problem of Discord. | descriptive or strictly programme-music remas




I I


44


THE REVIVAL OF ‘BENVENUTO CELLINI 


IN PARIS. 


22, 1910


1913. 445


446 THE


I, 1913


GEORGE HOLMES. 


448


THE ORGANS OF CHARTRES CATHEDRAL 


1913


MUSICAL


FHE ORGAN AT CHARTRES CATHEDRAL


THI


OREM IIE DORE UOC EY AP


450


I, 1913


14. 


ORGAN RECITALS


APPOINTMENTS


NOTES ON ORGANS AT WINDSOR CASTLE.’ 


BOOKS RECEIVED


THE IMPERIAL CHOIR AT GHENT


ROYAL OPERA, COVENT GARDEN


ARIADNE IN NAXOS


SD


HARVEST ANTHEM


30 ID 


SS == = 


- —————_=_=_=_== — — * 


| 4 7 I ,  — — 


———— 2S SSE : = 


GH = 


AND GOD SAID, WHILE THE EARTH REMAINETH


SS SSS! SS SS SSE SS SS: 


EE - —_|__«: | EEE


OS LT TOTES I A


WIN


CEN SA CERN LL


DB. 


S . 1 & 


\ — 1-5 #8 


_-_


465


1913


SUOMEN


LAULU


THE FINNISH CHOIR. 


SUOMEN


LAULU


ECESSOIS. 


THE SAINT-SAENS CELEBRATION


CHOIR CONCERT


4 GERMAN


ONDON SYMPHONY OR


HESTRA


20flattery of the music. It was broad, majestic, full of 
proud spirit, and of course a fine specimen of technical 
power. Weber's ‘Der Freischiitz’ Overture and

Beethoven’s fifth Symphony completed the programme

At the concert on June 2, Herr Mengelberg gave us his 
of Tchaikovsky’s fifth Symphony, in which he 
employed all his individuality, his sense of climax, and his 
art of bringing out musical design. The ably-wzitten 
Pianoforte concerto by Mr. Haydn Wood (produced at a 
recent Patron’s Fund Concert) was played with skill and 
enthusiasm by Miss Tina Lerner. The ‘ Meistersinger’ 
Overture and a Concerto Grosso of Handel added further to 
the enjoyment of the evening

On June 9 we had the privilege of one of those 
choral incursions from Yorkshire that periodically 
remind us of our deficiencies in the South. The Leeds

Philharmonic Chorus co-operated with the Orchestra in the 
performance of Elgar’s ‘The Music- Makers’ and Beethoven’s 
Choral Symphony. The singing: in the cantata was highly 
pointed and shaded, and in this formed a suitable choral

background to the high artistry of Miss Muriel Foster as the

and Mr. Thorpe Bates

M. Paderewski was doubtless the chiet attraction on 
June 16, when the hall was practically full, but his 
performance of Beethoven’s ‘ Emperor’ Concerto was not 
ideal. It lacked spontaneity and charm, and it was 
dominated by the personality of the exponent at the expense 
of the music. The concert was made memorable, however, 
by the splendid interpretation of the ‘ Eroica’ Symphony, 
given under Herr Nikisch’s direction

THE MUSICAL TIMES.—Juty




1913


CHAMBER CONCERTSTHE MUSICAL TIMES.—JuLy 1, 1913. 469

Another of their unequalled Trio concerts was given by | of Brahms in A and the ‘ Kreutzer’ of Beethoven. The 
MM. Thibaud, Casals, and Bauer at Queen’s Hall on May | songs included Lieder and Mr. Harty’s ‘ By the bivouac’s 
2. The programme consisted of the Trios of Brahms in| fitful flame’ (Whitman). As before, everything was

C minor (Op. 101), Saint-Saéns in F major (Op. 18), and | admirably performed. 
Tchaikovsky in A minor. eure 
Sees An interesting concert of the Beecham Orchestra at




STUDENT CONCERTS


COUSSY


THE MUSICAL


47


TIMES


[IANOFORTE RECITALS. | 


I, 1913Misses Elsa and Cecilia Satz, Bechstein Hall, June + 
Sonata in D for two pianofortes, J/ozart

Mr. Louis Edger, Steinway Hall, June 4—Sonata jp 
A major, Beethoven

Mr. Eugen d’Albert, (Queen’s Hall, June 5—Sonay

appassionata, Beethoven. 
Miss Katherine Goodson, Bechstein Hall, 
in A flat, Op. 110, Beethoven

June 5—Sonan

Mr. Arthur Rubinstein, Bechstein Hall, June 6- 
* Carneval,’ Schumann. 
Herr Ernst von Dohnanyi, AZolian Hall, June 7—Sonau

Op. 111, Beethoven

Miss Kate Friskin, .Zolian Hall, June 11—‘ Davidsbindler, 
Schumann

Miss Isolde Menges (violinist), (Queen's 
Sonata in D minor, Brahms

Miss Beatrice Harrison (violoncellist) and Mr. Enget 
d’Albert (pianist), Bechstein Hall, June 6—Sonatas 
Beethoven (A major), Brahms (E minor), and Saznt-Sam 
(C minor

Mr. Mischa Elman 
Concerto in F sharp minor




OTHER RECITALS AND CONCERTS. 


00; pia anist- 
Bechste

Mr. David Levine (pianist)—Sonata No. 3, in C, 
Beethoven; Miss Marguerite Le Mans _ (vocalist)— 
‘Chansons de Miarka,’ 4/7. 4. George ; Mr. Philip Levine 
(violinist) —Sonata in A, Brahms ; Molian Hall, May 27

Madame Sobrino (vocalist)—‘ Kling,’ Strauss ; Senor 
Sobrino (pianist)—Sonata in G minor, Schumann ; 
Bechstein Hall, May 28




BY OUR OWN CORRESPONDENTS


BIRMINGHAMMiss Myra Jerningham (pianist) and Miss Marjorie Hayward 
(violinist), Aolian Hall, June 17—Sonata in G major, 
Nicholas Gatty

Mr. Bronislaw Huberman, Queen’s Hall, June 17—Sonata 
in A major, Beethoven

Mr. David and Madame Clara Mannes (violinist and pianist), 
Bechstein Hall, June 17—Sonata in A, César Franck




BOURNEMOUTH


472 THE MUSICAL


I, 1913


DEVON AND CORNWALL. 


ONSHIKE TOWNS. 


CORNWALL. 


THE MUSICAL T


DUBLIN. orchestra in memory of the late Lord Ashbourne (formerly 
Lord Chancellor of Ireland and a patron of the Society). 
Madame Borel, Mr. William Lewin, and Mr. T. W. Hall 
were the soloists. Dr. Charles Marchant conducted

The Sunday Orchestral Concerts at Woodbrook came to an 
end for the present season with the concert on June 1, when 
the programme included Beethoven’s seventh Symphony and 
Dr. Esposito’s sé cond Irish Rhapsody for violin and orchestra 
(with Signor Simonetti as soloist). Mr. Arthur MacCallum 
was the vocalist. On May 25 the chiefitem on the programme 
was Schumann’s ‘Andante and Variations’ for two 
pianofortes, beautifully played by Miss Fanny Davies and 
Dr. Esposito. Miss Davies also played a group of short 
pieces by Scarlatti, Brahms, and Liszt. 
was that of Haydn, in B flat

LIVERPOOL. 
To the names of next season’s ‘ 
whom definite arrangements have been made by the 
Philharmonic Society, are to be added Mr. Max Fiedler and 
Mr. Hamilton Harty. Elgar’s ‘Caractacus’ has also been 
selected for performance as well as the ‘ Messiah.’ Sir 
Frederic Cowen will conduct both works, which will have 
the advantage of being chorally prepared by Mr. Harry 
Evans. Composed for the Leeds Festival of 1898, it is 
strange that so long an interval should elapse before the 
first performance of Elgar’s ‘ Caractacus’ at Liverpool. 
In this belated recognition of the merits of an interesting 
and attractive work, the Philharmonic Society have made a 
wise choice, for ‘ Caractacus’ is a quasi-novelty which will 
at once commend itself and subsequently occasion no 
acrimonious discussion in the daily press. One of the 
choral novelties selected to be produced at the ensuing 
Leeds Festival in October, Mr. Hamilton Harty’s 
‘The Mystic Trumpeter




473


1913


MANCHESTER AND DISTRICTThe report of the Hallé Society on June 2 does not make 
| pleasant reading. The loss on twenty Manchester concerts 
| was £400, and twenty concerts in various other centres 
showed a deficit of £316; but this debit balance had been 
| reduced to £5 necessitating a call of £3 per guarantor. 
| The appeal to the public for special funds and to the 
guarantors to pay up the balance of their guarantee had 
|together resulted in fifty-four persons promising at once, 
or in instalments, an aggregate of £1,952. Of this, £1,340 
| has been invested, yielding £55 per annum, not enough to

pay for one extra rehearsal. The net proceeds of the 
Annual Pension Fund Concert were £161. For the coming 
winter, arrangements have been completed whereby the bulk 
| of the players are engaged on weekly terms instead of pei 
engagement, provision being made for fifty concerts and 
twenty extra rehearsals in addition to the customary one on 
the day of the concert. 
| Arrangements have been made for another Scotch tour next 
spring ; so much were the executive impressed with the 
character of the Edinburgh and Glasgow press criticisms of 
the Beethoven Festival Concerts in Edinburgh last March, 
that they have taken the unusual course of printing these in 
brochure form and sending a copy to all subscribers! Can 
it be that they think Balling and his men are not appreciated 
at their true worth in Manchester, and that we must have it 
knocked into our heads that Edinburgh and Glasgow critics 
The executive's action 
would have been more comprehensible had there been 
anything of a distinctive character about these critiques

As to next season, Mr. E. J. Broadfield announced a 
repetition of the ‘ Parsifal’ evening, Verdi’s ‘ Requiem’ in 
honour of his centenary (a work not heard in Manchester 
for the past twenty-five years), Brahms’s * Schicksalslied,’ 
and Walford Davies’s ‘ Song of St. Francis’ (his setting of




72474 THE MUSICAL TIMES.—JvuLy 1, 1913

desirous of singing ‘ Atalanta in Calydon’ again, but nothing | Orpheus Choir there exists a bond of real fellowship T 
definite is known on this head; one wonders whether | born of a recognition of the Choir’s supreme artistic qualities P 
Bantock’s new choral work in four or five movements, called | and celebrations of German national occasions usually fing DI 
* The Vanity of Vanities’ will be considered as a ‘ possible’ ; | the Choir singing for the Kaiser and Consul with heart and $e 
on February 14 Mr. Harry Evans is to do it in Liverpool. voic sage time in German !). On his return from Frankfurt at 
The orchestral novelties already arranged include Max|in May, Captain Schlagintweit brought back Heégar’s new m 
Reger’s ‘Concerto in the ancient style’; Sinigaglia’s | work ‘ 1813 ’ (commemorative of the Napoleonic debacle of w 
‘La Baruffe Chiozzotte’; Strauss’s ‘Aus Italien’ and|a hundred years ago) for the Orpheus men to study. The at 
Holbrooke’s ‘Queen Mab’ Scherzo (both legacies from last | festivities on June 13 were inaugurated by Mr. Nesbitrs ar 
season) ; Rachmaninoft’s Pianoforte concerto, “ be played by | men singing Adolf Frey’s rousing lines, and one wondered sh 
the composer ; Bantock’s ‘ Helena F. B.’ Variations, and a| how they would have fared with this piece in the huge ne 
Bruckner Symphony. Besides Rachmaninoff, we are to | competition at Frankfurt on May 4. After the banquet they an 
hear Siloti, Cortot, and Irene Scharrer, Isolde Menges, | also sang in German Strauss’s ‘Liebe’ and ‘Brauttanz in 
Brodsky, and numerous vocalists. }and Brahms’s ‘Wiegenlied’; later in the evening the 5 we 
In recent years various attempts have been made, without | Consul’s daughter and Felix Fleischer (from Bremen, bat bu 
success, to bring home to the executive the necessity of | now with Carl Rosa) repeated Wolf Ferrari’s ‘ Susannens sai 
introducing fresh blood into that body. The executive is} Geheimniss,’ produced by them here on the occasion o Ey 
elected by, and from among, the guarantors of £ 100 (who may | the Kaiser’s Birthday Banquet in 1912. Dr 
not in every case be subscribers) instead of from the main body a ———_ f int 
of subscribers ; as a consequence the executive can only be | senkeaieiieaiis im . . — 
drawn from a restricted - a08g and conceivably the ablest | NEWCASTLE AND DISTRICT. ‘ = 
persons for such duties may find themselves disqualified} The only event of importance during the month has bee aa 
by inability to assume guaranteeship. Until this year the recital of modern song given by Mr. Frank Maullings, a 
a deaf ear has been turned to all such requests. | the June meeting of the Northern Section of the LSM 
By June 2 the executive admitted that the idea was Four groups of songs were chosen from the works of Hug 
receiving consideration; pressed for a less vague reply | Wolf, Richard Strauss, Granville Bantock (‘ Ferishtah’s 
the chairman hinted that such a course would almost | fancies’) and Roger Quilter (Shakespeare songs) respectively , 
certainly be taken ‘at the next meeting’ (presumably in| Mr. Mullings sang with rare ability and insight, and M. § 
June, 1914). Bishop Welldon, who has been most persistent | W. G. Whittaker accompanied. Mr. T. Henderson, hon we 
on this point of more democratic — on receiving the | secretary of the Northern Section contributed explanator 
reply just indicated promptly suggested the names of | notes on the songs, devoting particular attention to those he 
Dr. J. Kendrick Pyne and Mr. S. El. Nicholson (past and | Prof. Bantock. c 
present organists at the Cathedral here), which produced | ——-— -- Mu 
the rejoinder that neither were guarantors, and even if they OXFORD. Chi 
had been there were enough specialists already on the | ’ , 
executive. | On May 3, a delightful pianoforte recital was given in the pee 
To an outside observer it would seem that the most Assembly, Room of the Town Hall by Mr. Harold Bauer cree 
pressing need is the choice of one or two men of the principal items being Bach’s ‘ Italian’ Concerto and bow 
acknowledged business capacity, who to such qualities unite oo Sonata in F major, Op. 78. ; th 2 
keen musicianship, a wide outlook on the world of music, On May 10, in the Town Hall, a complimentary concert ‘7 
sound ideas, catholicity of taste, and whose artistic judgment | was given ies the benefit of Mrs. Sunman (widow of the oe 
can be relied upon by the conductor as hisown. Men of | late Mr. Henry Sunman, for many years a member of the Mr 
this type have always been found in movements achieving | Cathedral choir), at which nearly £60 was realised. 2 
great ends. Liverpool formerly possessed such men in the combined lay-clerks of the various Colleges are to & H 
late Alfred E. Rodewald and Mr. J. Dudley Johnston (now | sincerely congratulated on an excellent concert. two 
removed to London). Men like these are frequently better The first * Eights-week ’ concert was given by Balliol on of 
posted on current musical matters than _ professional May 18, when amongst other items the Ackroyd Sinng Mr. 
musicians, simply because their passion for music leads them | (Juartet gave excellent interpretations of Smetana’s (Quartet cons 
totake a more live interest in things—they are amateurs in|in E minor, and Brahms’s Quintet in F minor, Op. } H 
the fundamental sense of the word ; and once you are sure of | Two days later a very interesting concert was a : on J 
their unflinching fidelity to root principles, a couple of | Exeter, the chief item being Stanford's * Revenge, whic Sulli 
persons of this type on an executive are worth half-a-dozen | was ably c nducted by the organ scholar, Mr. HH. S. Price, The 
professional musicians. | while songs and part-songs were given in the miscellaneous eight 
A writer in the J/anchester Courier recently said, with | part. derfc 
truth: ‘A striking act of supreme intelligence might| On May 21 came the Keble concert, at which more tha cred 
cancel the past in an instant. Courage, a high sense of |a thousand people enjoyed themselves. A full orchestra was sale 
adventure, willingness to face a strenuous period of criticism, provided, and gave a good account of the Prelude to the Mrs, 
mostly adverse and mostly from a generation out of touch | third Act of ‘Die Meistersinger,’ Berlioz’s ‘* Hungaman Mr. | 
with the flow of things musical—these are needed in order to | March,’ Grieg’s ‘Peer Gynt’ Suite (Op. 46), and Schubert’ Bowe 
tackle the problem of how to fill empty seats without abating | ‘ Rosamunde ’ Overture, Op. 26. The rem: rining items wer. Mr. 
artistic ideals.’ |songs and part-songs given under the conductorship @ show 
On Saturday, June 21, ten or twelve hundred choristers| Mr. D. G. A. Fox (organ scholar) and Mr. r. F ot qr 
assembled under Mr. Nicholson at the Cathedral for the | (Jueen’s followed the next day with ane interest Xccon 
Diocesan Festival, their large numbers leaving little room | programme, consisting of two sets of songs in ear yor succe. 
for the congregation. Wesley’s ‘Ascribe unto the Lord,’ | nicely sung by Fraulein Diestel, a few part-songs, and tw and § 
Brahms’s ‘ Requiem’ chorus ‘ How lovely are Thy dwellings,’ instrumental Trios for flute, violin, and piano forte, tt 
and Bach’s ‘Come, ye thankful’ (from the ‘Christmas | Kuhlau and César Cui. . 
Oratorio’), with Smart’s ‘ Magnificat,’ constituted the The chief concert of the term, given under the auspices © Mr. 
Festival work. the Musical Club, took place on May 28, in the Town Hall. om 
The girl-operatives of Miss Say Ashworth’s Ancoats} when Dr. Allen directed a programme of eat wr Mar | 
Institute Choir leave Manchester on August 4 en route | excellence. It consisted of Beethoven's seventh Symp phony; and 
(vid Folkestone and Boulogne) for Zurich, where, in the] Elgar’s ‘ Enigma’ Variations, the ‘Tristan’ Prelude ant trio, 
Tonhalle, in conjunction with the orchestra, two concerts are | Liebestod, César Franck’s poem for pianoforte am the 
to be given by them on August 6 and 7 ; thence they proceed | orchestra, * Les Djji om with Mr. Egon Petri as _ Mn 
to Lucerne, with appearances at the Kursaal. Here they are} and Mr. Balfour Gardiner’s ‘ Shepherd Fennel’s ¢ — (Violin 
to remain several days to have, as their generous conductor | which so pleased the audience that the last portion ™ the 
hopes, ‘ the best time they ever had in their lives.’ | repeated by way of encore. Music the ‘i 
The Manchester celebration of the twenty-fifth On June 10, Sir Walter Parratt, the Professor of _~ pees. 
anniversary of the Kaiser’s accession to the throne | gave his usual terminal lecture in the Sheldonian — and w 
took place on June 13 in a befitting manner. Between before a large audience, the subject being * The full orches of the

beginning to end




PB


475


BRIEFLY SUMMARIZED


THE


MUSICAL TIME


3.—JULY I, 1913AMSTERDAM

The twenty-fifth anniversary of the foundation of the 
Concertgebouw Orchestra was celebrated with a three days’ 
musical festival under Herr Mengelberg’s direction. The 
programmes included Mahler’s ‘Das Lied von der Erde’ 
and eighth Symphony, and works of Beethoven

ANTWERP. 
Peter Benoit’s choral works ‘Noél’ and ‘Alma 
Redemptoris,’ a Concert-overture by E. Wambach, an 
‘Elegie’ by Sokolow and a ‘Hlamlet’ Overture by




BASEL


BAYREUTH


LIN


BRESLAU


BUENOS AIRES. 


BORDEAUN


CHICAGO NORTH SHORE FESTIVAL


COPENHAGEN


1913


DORTMUND


DRESDEN


FRANKFURT


JENA


KONIGSBERGLEIPSIC

The Wagner celebrations have been most elaborate here 
in the master’s native town. Performances were given at 
|the Municipal Theatre, of operas from ‘Rienzi’ to 
| ‘Gotterdimmerung.” On May 22, at 10-30 a.m., the 
foundation-stone of the monument by Max Klinger was laid. 
| This was followed at 12 o’clock with a festival concert at 
| the Gewandhaus, when Beethoven’s ninth Symphony was 
|given under the direction of Prof. Arthur Nikisch. In 
| the evening a festival performance of ‘Die Meistersinger’ 
| took place at the Municipal Theatre——On May 23 an 
| extensive Wagner exhibition was opened, and a concert was 
given at the Alberthalle on May 24, when ‘ Das Liebesmahl 
| der Apostel’ was performed

LILLE




LINZ


MANNHEIM


MAYENCE


MILAN


MONTREAL


NURNBERG


PARIS


REGENSBU RG


ROME


SALZBURG


ST. PETERSBURG, 


STRASBURG


STUTTGART


VEVEY


VIENNA


THE IMPERIAL CHOIR AT THE CRYSTAL PALACE, 


THE MUSICAL


TIMES


479


VATERLAND.- 


OPERA. 


DURING 


THE LAST MONTH


LIMITED. 


I ROOME


CONTENTS


454


465


A. B. , > DE TT 


RY


ANTHEMS


FOR


TRINITYTIDE


COMPLETE LIST


G. C. 


THE “LUTE” SERIES 


ANTHEM


NOVELLO’S 


AMER EMS


HYMNS AND TUNES


SET II 


EVENING CANTICLES. 


BYRD


RAIN COMETH DOWN


AS THE


A MANUAL OF PLAINSONG


FOR DIVINE SERVICE 


CONTAINING


THE CANTICLES NOTED 


THE PSALTER NOTED


TOGETHER WITH THE LITANY AND RESPONSES 


A NEW EDITION 


PREPARED BY 


UNDER THE GENERAL SUPERINTENDENCE OF


JOHN STAINER


EXTRACT FROM INTRODUCTION


PREFACE


SH HHNMINL


THE


CANTIONES


MUSICAL SETTINGS OF THE 


LITURGY


EDITED BY


SAMUEL GREGORY OUI


ROMAN


11


5 SALVE, REGINA 


SACRE


O BE JOYFUL


Q THAT MEN WOULD PRAISE


1913


483


NEW


HARVEST ANTHEMS


AND GOD SAID 


COMPOSED BY 


CUTHBERT HARRIS


WILL CAUSE THE SHOWER 


E. W. NAYLOR


YE SHALL GO OUT WITH JOY 


| WILL GREATLY REJOICE 


COMPOSED BY


E. C. BAIRSTOW


RECENTLY PUBLISHED. 


A SONG OF 


CUTHBERT HARRIS


SING PRAISE


THE ETERNAL GOD 


JOH NE. WEST


IN THE LORD


COMPOSED BY


EBENEZER PROUT


THING TO GIVE 


THANKS


THOMAS ADAMS


LORD


COMPOSED BY 


HUGH BLAIR


484


NOVELL O'S


1913


C. M


THE


140 


152


SE RIES OF HARVEST ANTHEMS


FRBRRRRES


ARR


PRR ER


FO


30. 


BREE


HARVEST FESTIVAL M USIC


CANTATAS. 


SONG OF THANKSGIVING HARVEST CANTATA


FOR SOPRANO, TENOR, AND BASS (OR CONTRALTO) SOLI FOR SOPRANO (OR TENOR) AND CONTRALTO (OR 


AND CHORUS BARITONE) SOLI AND CHORUS 


THE MUSIC BY


SHAPCOTT WENSLEY JUI IUS HARRISON 


~ : FOR SOPRANO AND TENOR SOLI AND CHORUS 


FOR TENOR AND BASS SOLI AND CHORUS JOHN E. WEST. 


HENRY KNIGHT


THE MUSIC COMPOSED BY A SONG OF THANKSGIVING


THOMAS ADAMS, FOR CHORUS AND ORCHESTRA 


FOR TENOR AND BASS SOLI, CHORUS, AND ORGAN TH E GLEANER’S R’ S HARVEST 


SMALL ORCHESTRA FOR SOLO VOICES, CHORUS, AND ORCHESTRA


BY 


BY 


J > M. V WEBER. 


HUGH BLAIR. C. M. VON 


A HARVEST SONG


FOR SOPRANO SOLO AND CHORUS


HARVEST CANTATA


FOR CHORUS, SEMI-CHORUS, AND ORGAN


BY BY 


GEORGE GARRETT. C. LEE WILLIAMS. 


TWELVE HYMNS FOR HARVEST THE SOWER WENT FORTH SOWING 


LET ALL OUR BRETHREN IN IN ONE 


THE JOY OF HARVEST AND SEA 


A HARVEST HYMN OF PRAISE COME, YE THANKFUL PEOPLE, COME 


486 THE MUSICAL


IQI3


ANTHEMS


ORGAN. 


SIX


COMPOSITIONS. 


OFFICE OF HOLY COMMU NIONIN G


FOR MEN'S VOICES. 


A SECOND SET OF 12 SHORT PIECES FOR THE ORGAN


HARVEST F ESTIVAL


BOOK


CONTAINING TALLIS’'S PRECES AND RESPONSES, THE 


CANTICLES AND SPECIAL PSALMS


POINTED FOR CHANTING AND SET TO NEW AND API


CHANTS BY 


BARNBY, MYLES B. 


MACKENZIE


AND


ROPRIATE


SIR J. 


SIR A


FOSTER, 


SIR J. STAINER


GETHER WIT


FOUR NEW HYMN TUNES


ED EXPRESSLY


SIR J. BARNBY, SIR J. STAINER


JOHN E. WEST


SIXPENCE I


PRICE


OPULAR CHURCH MUSIC


H. MAUNDER


SERVICES. 


ORGAN. 


CHURCH CANTATA. 


MASSAGES AS O 


1ATY 14, 1905


THE MUSICAL


1913


487


SIMPER’S 


NEW ANTHEMS


SING PRAISE TO O


CALEB 


HARVEST


1913 .. <= ‘ ~ 2 


THE EARTH


POPULAR CHURCH SERVICES


EDWYN A. CLARE'S 


VERY POPULAR HARVEST ANTHEMS


A NEW EVENING SERVICE IN G. 


NOEL


A CHRISTMAS PIECE 


FOR PIANO SOLO


HYMNS ANCIENT AND MODERN


OLD EDITION


LIBERAL GRANTS OF 


ANCIENT


VARIED HARMONIES 


ACC( YMPANIMENT (AND VOICE AD LIBITUM


OF CERTAIN TUNES IN HYMNS ANCIENT 


1889, NEW EDITION, 1904


EITHER EDITION 


FOR ORGAN


MODERN


ORGAN MUSIC


HARVEST FESTIVALS 


VILLAGE ORGANIST


YINGHAM WOODS


THE


F. CUNN


RAINBOW


OUR MUSIC READING LADDER FOR BEGINNERS


JUST PUBLISHED. 


THE BARLESS PSALTER 


POINTED FOR USE WITH ANGLICAN CHANTS


AN EASY BOOK FOR CHOIR AND CONGREGATION R


CONTAINING


THE PSALMS OF DAVID, ( 


THE CANTICLES AND PROPER PSALMS 


THE ATHANASIAN CREED


TOGETHER WITH


SHORT NOTES ON THE TEXT & A FULL EXPLANATORY PREFACE 


EDITED BY 


WALTER MARSHALL, M.A., 


AND 


SEYMOUR PILE, M.A., F.R.C.O., 


THE NEW CATHEDRAL PSALTER


CONTAINING


THE PSALMS OF DAVID 


PSALTER AND CHANTS COMBINED. 


MS


PRAGA AAANS


FANTASIA EROICA


489


BY


FR. KUHMSTEDT


EDITED BY


ARTHUR BOYSE


F.R.C.O


AND COMPANY, LIMITED


SHORT PRELUDES


FOR THE


ORGAN


TRANSCRIPTIONS 


A. HERBE RT BREWER


DWARD ELGAR 


3. CHANSON DE NUIT .. .. EDWARD ELGAR 


4. CHANSON DE MATIN .. - EDWARD ELGAR 


7. CANTIQUE D'AMOUR


H. LLOYD


EDWARD ELGAR 


THEO. WENDT


0 {WE MARIA\\ |... ADOLPH HENSELT


CORONATION MARCH


AGNER 


SOUVE NIR DE PRINTEMPS JOSEPH HOLBROOKE 


AUF WIEDERSEHEN .. = BREWER 


WAGNER 


WAGNER


EDWARD ELGAR 


ORGAN


2 0 


20 


20 


20


20


TRANSCRIPTIONS


GEORGE J. BENNETT


TSCHAIKOWSKY


TSCHAIKOWSKYMINUET.—{Sonata In E Frat). ‘ia 31, iii

BEETHOVEN 
A. C. MACKENZIE

8. PRELUDE.—(“ Cotomsa




MENDELSSOHN


SLOW MOVEMENTS 


FROM 


SONATAS 


FOR THE ORGAN 


F. MENDELSSOHN-BARTHOLDY. 


ORIGINAL


THE MUSICAL


COMPOSITIONS


FOR THE


ORGAN


NOVELLO’S 


JMS FOR THE ORGAN 


1913


ORIGINAL COMPOSITIONS


FOR THE


ORGAN


COMPOSED BY


ALFRED HOLLINS


ORIGINAL COMPOSITIONS


FOR THE


ORGAN


COMPOSED BY


W. WOLSTENHOLME


COMPOS ITIONS FOR THE ORGA


SIGFRID KARG-ELERT


EE


SOLE


COMPOSED BY


H. WALFORD DAVIES


FULL ORCHESTRA. 


STRINGS AND ORGAN. 


ORGAN


PIANOFORTE SOLO


VIOLIN AND PIANOFORTE 


VIOLONCELLO AND PIANOFORTE


MN MELODY


AAA


THE


SIX


NS


NS


RAR


THE MUSICAL TI


491


COUNTRY DANCE TUNES


COLLECTED AND ARRANGED FOR PIANOFORTE BY


CECIL J. SHARP


TRY DANCE BOOK 


THE COUN NCE


EDITED BY 


CECIL J. SHARP


SIX SHORT EASY PIECES 


FOR THE PIANOFORTE


COMPOSED BY


CLEMENT M. SPURLING. 


INTENTS : 


_—__


PETITE VALSE


J. HOLLMAN 


ARRANGED FOR PIANOFORTE SOLO BY 


JOSE VARGAS-NUNEZ. 


INVOCATION 


FOR 


ORCHESTRA 


COMPOSED BY 


A. C. MACKENZIE


ARRANGEMENT FOR 


VIOLIN AND PIANOFORTE, 


BY THE COMPOSER


FOR VIOLIN AND PIANOFORTE. 


BY


C. H. LLOYD


CONTENTS : 


SIX EASY PIECES


FOUR CHARACTERISTIC PIECES


FOR VIOLIN AND PIANOFORTE. 


COMPOSED BY


C. H. LLOYD. 


LA SAVANNAH 


AIR DE BALLET 


FOR ORCHESTRA. 


COMPOSED BY 


A. C. MACKENZIE


10


2000 0


492


NOVELLOS VOCAL ALBUMSEnglish Version by » Rev. J. Trovu SCK. we 2 | ‘ 
yy ee Sy Oe a - TRCTIER. Tee (1813-1865). Edited by W. A. BARRETT. Price x

BEETHOVEN.—Twenty-Six Soncs. Vol. I. English MOZART, W.A.—NINETEEN SonGs. English and Ge 
and German words. The English Version by the Rev. words. The English version by the Rev, J. Trou Tan} 
J. TROUTBECK. Price ts. 6d. D.D. Price 1s. 6d

BEETHOVEN.—SEVENTEEN Soncs. Vol. II. English 
and German words. The English Version by the Rev. 
J. TROUTBECK. Price ts. 6d

BEETHOVEN.—TweEnty-Two Soncs. Vol. III. English 
and German words. The English Version by the Kev. 
J. TROUTBECK. Price 1s. 6d

BENNETT, W. STERNDALE




W. 4


OF THE MILL” 


ROTHERY. 


BRAHMS, J


FRANZ, ROBERT 


FRANZ, ROBERT


SCHUMANN, R.— 


THE 


LONDON


NOVELLO AND COMPANY


LIMITED


AS . SONGS 


& HAMILTON HARTY. 


NDEGGE


ALBER


\CFARRE


-NDORI


CF ARK


LOVE IN THE MEADOWS 


E SONGSTERS’ 


NOVELLO’S- 


CLASSICAL SONGS


CONTENTS. 


TENOR SONGS ®¥ S


NEW EDITION (1912


IN MUSIC 


600 QUESTIONS WITH ANSWERS 


ERNEST A. DICKS


PREFACE TO THE NINTH EDITION


_ E. A. D


MELODIOUS TECHNIQUI 


FOR THE PIANOFORTE


J. A. O'NEILL


HAROLD OAKLEY


COMPASS


HANDBOOK OF EXAMINATIONS


RECENTLY ADDED TO


NOVELLO’S OCTAVO EDITION


KUBLA KHAN 


A RHAPSODY


FOR SOLO, CHORUS, AND ORCHESTRA 


THE WORDS BY 


COLERIDGE


THE MUSIC BY


S. COLERIDGE-TAYLOR


NATIONAL 


NURSERY RHYMES


WITH SIXTY-FIVE ILLUSTRATIONS


ENGRAVED BY THE 


BROTHERS DALZIEL, 


THE MUSIC BY 


J. W. ELLIOTT


WIT


RA


THE


MUSICAL TIMES


1913


495


A VALUABLE BOOK FOR TEACHERS AND STUDENTS


TECHNIQUE


PIANOFORTE


MUSIC


NEW FOREIGN PUBLICATIONS


NET. 


490


THE MUSICAL


NOVELLO'S OCTAVO EDITION


RECENTLY


SELECTED LISTS OF 


A.C


A.C


VOICES


FE MALE


VOICE


MALE


PUBLISHED 


59


PART- 


PART-SONGS


SONGS


F. L 


A.C


B.B


E. W. NAYLOR


NOVELLO’S OCTAVO ANTHEMS


COMPOSED BY


I WILL CAUSE THE SHOWER TO COME 


DOWN 


_ — 7 — —} 


—_—_}_-_» — * —“ = 


_ | SPS SR


I WILL CAUSE THE SHOWER TO COME DOWN. 


SS 


2 1 _= - {— = L = L 


M L | | = 


I WILL CAUSE THE SHOWER T0 COME DOWN


AERA T YER ET


_—_—_ 


SSS


LAN


10W


10W


UH


I WILL CAUSE THE SHOWER


TO COME DOWN


_— I — L


1S $0? 


I WILL CAUSE THE SHOWER T0 COME DOWN


= — 3 SS 


—S SS ESS SSS LSS = | —-_9-——_9— 7 


WW SHUT


LH


WA WA


L L 


I WILL CAUSE THE SHOWER TO COME DOWN


— ——_—__—___ SS | ——— —_ 


I WILL CAUSE THE SHOWER TO COME DOWN


1 WILL CAUSE


THE SHOWER TO COME DOWN


= =.= == SS


PRINC


GEORGE FREDERICK HANDEL


FROM THE PORTRAIT BY MERCIER


THE RIGHT HON. THE EARL OF MALMESBURY