


THE MUSICAL TIMES


SEPTEMBER 1, 1868


MUSIC IN THIS NUMBER


ONCE UPON MY CHEEK. 


GLEE 


HOW FADES THE LIGHT 26 


IMPORTANT SALE OF


DURING THE MONTHS OF SEPTEMBER AND OCTOBER


GRICULTURAL HALL.—TUESDAY POPU


PROFESSIONAL NOTICES


MR. WILBYE COOPER 


MR. J. C. BEUTHIN


57, BERNERS-STREET, W. 


USIC SENT ON SALE OR RETURN, ON 


USIC ENGRAVED, PRINTED, AND PUB


LISHED IN THE BEST STYLE, AND ON MODERATE


RASS, REED, STRING, AND DRUM AND 


USICAL INSTRUMENTS FOR VOLUN- 


EYOND ALLCOMPETITION.—T. R. WILLIS, 


INSTRUMENTS


UTLER’S MUSICAL


CORNETS, SAXHORNS, DRUMS, FLUTES, CLARIO- 


AND W. SNELL’S IMPROVED HARMO


141


EASANT’S HARMONIUM MANUFACTORY 


499


HE ELEMENTS “OF MUSIC SYSTEMATI- 


NEW ELEMENTARY WOR 


HE ORGANISTY’ QUARTERLY JOURNAL 


HORAL HARMONY


ARVEST SONG (CONGREVE'’S


EV. R. HAKING’S PART-SONGS. 


EXETER HALL


NEW MAGAZINE OF SACRED MUSIC. 


NO, VIII. FOR SEPTEMBER


CONTENTS


37, GREAT MARLBOROUGH STREET, LONDON, W. 


G ROUTLEDGE AND SONS, 


BROADWAY, LUDGATE HILL


METZLER AND CO, 


37, GREAT MARLBOROUGH STREET, LONDON, W


LONDON: 


NEW ANTHEM, BY CH. GUUNOD. 


LONDON


500


ENRY SMART'S SIX SACRED VOCAL


ENRY


SMART’S TRIOS FOR FEMALE 


VOICES


DWARD HECHT’S NEW SONGS


PEN ME THE GATES OF RIGHTEOUS. 


PRAISE THE LORD ALL’ YE PEOPLE, 


501The separate numbers are sold at half-price

We have here a series of characteristic pieces, ‘ Dedicated to hi 
.grandchildren,’ by a veteran composer, whose name will always be 
honourably associated with the history of pianoforte music and the 
development of the pianist’s art. It is now nearly half-a century 
since Mr. Moscheles settled in London; and by his various per- 
formances, especially of the pianoforte works of Beethoven, led the 
way towards that general appreciation of what is great in the art 
which is still happily progressing. Many of Mr. Moscheles’ com- 
positions, especially his Concertos and his Studies will, for their 
own merits, as well as for their influence on the development of the 
mechanism of pianoforte playing, associate their composer’s name 
with that of Cramer, Hummel, and Kalkbrenner in the history of 
the art. Since Mr. Moscheles’ removal to Leipzig, as Principal of 
the Conservatoire there, his activity has been as unremitting as in 
his earlier days; and we have here the most recent results of 
thought and skill as fresh as those which distinguished their author's 
career when among us. The duets now referred to are worthy to 
rank with those charming pieces—the ‘ Zwilf Clavierstiicke fiir 
grosse und Kleine kinder’ of Robert Schumann. In both instances 
distinctive title is given to each piece, pointing to the charac- 
teristic which it is intended to illustrate. No. 1, ‘ Brother and 
Sister,’ has the tranquil flow of pure feeling. No. 2, ‘ Affection,’ 
although less developed, is full of calm expression. No. 3, ‘Alterea- 
tion,’ an interruption to this equanimity, is a capital musical 
illustration of such occasional wrangles as will sometimes occur, 
‘in the best regulated families.’ This piece is worthy to pair with 
its author’s study, ‘Contradiction,’ No. 3, in his Op. 95. No. 4, 
‘Grandfather’s Dance,’ is full of quaint character, with a certain 
formal yet genial grace, such as one might suppose a venerable 
gentleman to exhibit when disporting with his grandchildren at 
some juvenile festivity. No. 5, ‘Elegy,’ is a beautiful piece of 
cantabile writing in which the secondo part is of equal importance 
with the primo. No. 6, ‘A Fugal Waltz,’ is an admirable example 
of the power of a master to turn science to the most popular uses. 
As a Faraday could make the wonders of chemistry intelligible to 
a young audience, we have here the learned form of the fugue 
popularised in what is generally considered as a frivolous form— 
that of the waltz. The subject of a capital dance piece, in three- 
four time, is changed to six-eight, and converted into a fugue, with 
great vigour and variety of treatment; reverting, as a close, to the 
waltz tempo and style. No. 7, ‘The Harper's Ballad,’ just such a 
strain, with a mixed rhythm, as one might imagine to proceed from 
a bard of old, has much quaint beauty. No. 8, ‘Grandmother 
‘at her spinning-wheel,’ is a simple melody, accompanied by a mean

2 
6 9 Soldier’sLife . . 8 
0 10 Serenade . ar ae 
6| 11 Quickstep ° 8 
0; 12 Canonalla Tarantella 4




HE CONGREGATIONAL CHUKCH SER- 


TONIO SOL-FA EDITION. 


THE SIMPLEST FORM OF INTONED SERVICE.—THE 


OVELLO’S POCKET EDITIONS OF THE 


ORATORIOS 


503


THE MUSICAL TIMES, 


SEPTEMBER 1, 1868


THE LONDON MUSICAL SEASON. so many years passed for chorus-singing in this

3) 3 y - J 
country. The number of benefit Concerts anc INCIDENTS IN THE LIFE OF BEETHOVEN

Pianoforte ‘“ Recitals” appears steadily on the in- 
crease ; but with the exception of those of Madame 
Arabella Goddard, Herr Pauer, M. Rubinstein, and 
Mr. Charles Hallé, scarcely any have had any direct

By R. M. HAYLEY. 
(Continued from p. 474.) 
His pupil Ries gives an interesting anecdote of the

bearing upon the art. The playing of M. Rubinstein di : : nap 
ae tar Ae sregard which Beethoven sometimes exhibited for 
has caused a diversity of opinion amongst critics siliae® 0s Ginlehmeane day,” he says, “taking a walk

which may be fully accounted for by the inequality

before him, we are told that it is “ genius.” But

effect. Beethoven did not recollect the passage, and

we are exacting enough to require that, however inalated that I: wen mistaken fn-caving thet these 
great the “ genius” of a pianist, it is pag sw bs were fifths in it. As he usually cutted wniipenes 
he should Play in time; and that sa ee — in his pocket, 1 asked him for a piece, and wrote 
interpretation of a work is incompatible with down the passage in four parts. Beethoven, seeing 
spasmodic bursts of ill regulated enthusiasm. Welt 1 was right, said, ‘Well, who has prohibited 
do not deny the exceptional qualifications of M.| 114 ‘use of fifths like these?’ I was at a loss how 
Rubinstein, but we deny his power to control them. to take the question. He repeated it several times

The Handel Festival has been a great success ; but/ till at last, 1 answered in a tone of surprise, ‘ Bless 
as we have already said in our comments upon the/me! they are forbidden by the very fundamental 
performance in these pages, with few exceptions, it|rules of harmony!’ He still insisted on knowing 
was merely a repetition of the former Festivals.|by whom they were so forbidden, when I replied, 
At the Crystal Palace the Concerts, under the|‘By Marpurg Kirnberger, and every theorist who 
direction of Mr. Manns, have fully sustained the}has ever written on the subject.’ ‘ Well,’ cried 
reputation so worthily gained for this establishment] Beethoven, ‘they may have forbidden them, but J 
by the zealous couductor and his well trained orches-| allow them

506

THE MUSICAL TIMES.—Sepremeer 1, 1868

In the instruction of his pupil, Beethoven showed 
a care and patience that was the more remarkable, 
when we consider his hasty and impetuous disposition. 
Ries always attributed this to a long-cherished 
affection for his father, and this circumstance gives 
to Beethoven’s character an amiable aspect It 
is undoubtedly true, that in the lessons he gave bis 
friend, he exhibited a mildness and forbearance that 
was quite unusual, often making him repeat difficult 
passages, and even whole movements, ten times over. 
“In the variations in F major,” says Ries, “I had 
to repeat the last part of the adagio seventeen times, 
Beethoven not being satisfied with a little cadenza, 
which I thought I played very well; this lesson lasted 
two hours.” His anecdotes, respecting Beethoven's 
method of teaching, are very interesting. ‘ When 
I played the notes of a passage imperfectly,” he says, 
“or touched too slightly notes which ought to have 
been well marked, Beethoven seldom found fault 
with me; but if I failed in expression, and did not 
observe the crescendos, and other shades of meaning 
which give a piece its character, he used to get 
angry. The former fault he would say, might be 
the effect of accident; but the latter argued a want 
of knowledge, sentiment, and attention. He him- 
self had once the misfortune to strike a wrong note 
even when playing in public

Notwithstanding the reputation which Beethoven 
had now acquired, and the estimation in which he 
was held by other professors of his art, he never 
showed any symptom of vanity or self-conceit. 
Experience had taught him that persons in general 
judge of any work by the character of its author; 
for the capacity of forming a correct opinion in mat- 
ters of taste is about as rare as the power of excel- 
ling and rising above mediocrity. Beethoven, who 
knew well his own powers, was annoyed, that the 
art in which he had attained such eminence should 
be slighted in any degree, and, caring little for the 
opinion of ignorant persons, would never subject 
himself to any neglect, if he could possibly avoid it. 
At a musical party, at the house of Count Browne

aside and told him that he had only been amusing 
himself at the old lady’s expense. Beethoven took 
the joke in good part; but the embarassment of his 
pupil increased when he was called upon to repeat 
the march. The presence of his master so discon- 
certed him, that the effect of the performance was 
utterly marred. Beethoven, however, was over- 
whelmed with encomiums, which he received with 
a distracted air and apparent annoyance. ‘ Such,” 
—he afterwards said to his young friend—“ such are 
your great critics, who venture to pass a correct and 
decisive judgment on all the music they hear. Only 
tell them that it is the production of their favourite, 
and, be what it may, they are immediately in 
raptures with it

As Beethoven advanced in years, the infirmity of 
deafness, to which he had been long subject, in- 
creased to such an extent that it was quite painful 
to his friends to see how greatly the deprivation of 
hearing affected him. At times he would become 
much better in this respect; but the evil returned 
with redoubled force, and made him more than 
usually irritable. His most intimate friends would 
converse with him in a loud tone, so that he might 
not be so sensible of the affliction under which he 
laboured ; and it occasioned him so much vexation 
to be obliged to allude to it, that whenever anything 
was uttered in his presence which he did not com- 
prehend, he was in the habit of attributing it to 
an absence of mind, to which he said he had been 
always subject

The utter wretchedness to which Beethoven was 
reduced by his increasing deafness, is thus described 
in a letter from his friend Stephen von Brenning, 
dated 13th November, 1806, and addressed to Dr. 
Wégeler, at Coblenz. ‘‘ You can have no idea of 
the indescribable, I might say, terrible effect pro- 
duced on Beethoven by his deafness. Imagine to 
yourself, how dreadful to a man of his strong and 
deep feelings, must be the thought of such a cala- 
mity. Add to this the reserve, the distrust, even of

his best friends, and the indecision that accompany

at Baden, near Vienna, where there was a large it; except now and then, when the genuine feelings 
assemblage of people of fashion, Beethoven and Ries | are expressed without disguise, intercourse with him

were to play a duet on the pianoforte. ‘They had

is for the most part a painful effort, for you must

scarcely begun when a young nobleman broke the not be off your guard for a single moment. From 
general silence by talking to a lady who sat next to May, till the beginning of this month, we have lived 
him. Beethoven tried by significant gestures, to stop in the same house. He had scarcely taken up his 
this conversation ; but finding himself unnoticed, he ahode with me when he was attacked by a violent 
started up, pushed Ries’ hands off the keys and said and dangerous illness, which at last took the form of

loudly, ‘Fir solche schweine spiel’ich nicht.” | an intermittent fever

confusion caused by this outbreak may be imagined. | He is now better

Anybody, but Beethoven, would have been turned 
out of the room; but he was a privileged man, and 
his rudeness was not only passed over, but he was 
requested to re-seat himself at the instrument, which 
he obstinately refused to do. Ries was then asked 
to play a Sonata, but Beethoven forbade him to 
touch a note ; and the scholar, fearful of the conse- 
quences of offending his master, durst not disobey 
his prohibition

On another occasion, at the same house, he exhi- 
bited greater control over his feelings. An old 
Countess, who had heard Ries play a march of his 
master’s on the previous evening, alluded to it in 
the presence of company; but Ries who knew Beet- 
hoven’s antipathy to the lady, began to feel a 
little embarassed, fearing that he himself might be 
called upon to play it. He therefore took Beethoven

Anxiety and attendance on 
|his sick-bed have somewhat injured my own health. 
The defect in his hearing rendered him very sus- 
picious and prone to take offence, as we have before 
remarked. He became remarkably credulous of 
calumnies invented against his dearest friends, and 
although after a full explanation, he was always 
ready to make amends for a wrong impression he 
had entertained respecting them, they occasionally 
incurred his displeasure, and remained for some time 
exposed to unmistakable proofs of his abated friend- 
ship, without being able to divine the cause. Where 
these feelings of distrust were not awakened he was 
unalterably true. In all their difficulties they could 
reckon upon his sympathy and assistance. This 
amiable trait of his character is well illustrated by 
his generous intervention on behalf of his friend and 
disciple Ferdinand Ries. 
Soon after the occupation of Vienna by the French




FRTHE MUSICAL TIMES.—Srpremssrr 1, 1868, 507

army in 1805, Ries, who was born on the left bank 
of the Rhine, was summoned, in accordance with the 
French law, as liable to be drawn in the conscription. 
On hearing this, Beethoven immediately wrote a 
letter to the Princess von Lichterstein, which, to his 
great annoyance, was never delivered. The purport 
of it was as follows :—‘‘ Forgive me, Most Serene 
Princess, if the bearer of this letter should occasion

ou an unpleasant surprise. My poor pupil, Ries, is 
called upon in this unhappy war, to shoulder the mus- 
ket, and will have to leave this place as an alien, in 
the course of a few days. He is utterly destitute of 
means, and has a long journey before him, and having 
no time to give a concert which might have helped 
him, is compelled to have recourse to the benevolence 
of the generous and charitable. I recommend him to 
you, and feel sure that you will forgive the step I am 
thus bold enough to take. Nothing but extreme neces- 
sity will bring a man of independent mind to adopt 
such means.” Even with this very friend, in whom he 
took so warm an interest, a rupture ensued which 
might have been fatal to their friendship. Beet- 
hoven was invited by Jerome, King of Westphalia, 
to take the superintendence of the choir at his chapel, 
at Cassel. Placed as Beethoven then was, in rather 
reduced circumstances, owing to the pressure of 
the war, such a situation which promised to secure to 
him a fixed and certain income, must have been 
very desirable. ‘The salary was 600 ducats, together 
with equipage, &c., free of expense. All prelimi- 
naries were arranged, and the contract was merely 
awaiting Beethoven’s signature, when the Archduke 
Rudolph and other persons of distinction, alarmed 
at the prospect of losing so illustrious a composer, 
resolved to settle on him a pension for life on the 
express condition that he should continue to reside 
within the Imperial dominions. On this being 
noised abroad, Ries one day was asked whether, 
seeing it was now certain that Beethoven would not 
accept the situation, he, as his pupil, would be con- 
tent to go to Cassel in his room upon a smaller 
salary? He immediately repaired to Beethoven's 
residence, in order to gain information, and ask for 
his advice under the circumstances. What was his 
surprise when he found that he was refused all 
access to him, and for the space of three weeks, 
even the letters which he addressed to him, remained 
unanswered. At last he met Beethoven at a ball, 
and accosted him, stating his reasons for the inter- 
view which he had sought. ‘Do you think,” said 
Beethoven, in a sarcastic tone, ‘‘that you could by 
any possibility fill a place that has been offered to 
me?” They then parted, and Beethoven remained 
cold and distant the whole of the evening. The next 
morning, Ries made another attempt at a reconcilia- 
tion, and calling on Beethoven was informed, by the 
servant, that he was not at home, although he was 
distinctly heard in his apartment playing and sing- 
ing. Ries announced his determination to enter the 
house without further ceremony, but was repulsed 
from the door. Enraged at such treatment, he 
seized the servant and threw him on the ground, 
where he was found by Beethoven himself, who 
rushed out of his room to ascertain the cause of the 
uproar. Ries now loaded Beethoven with reproaches 
to such an extent, that the latter was unable to 
utter a word and remained rooted to the spot, 
totally powerless from terror and amazement. ‘The 
er was now afforded to Ries of fully ex- 
plaining matters, and the result was that Beethoven

I was told that you were trying to get the situation be- 
hind my back.” Ries assured him that this was so far 
from being the case that he had not even returned an 
answer to the proposal that had been actually made to 
him. Seeing how the affairstood, Beethoven now made 
ample apologies for his conduct, and exerted himself 
to the utmost in order to secure the situation for his 
pupil, but without success. It was now too late. 
It would have been highly advantageous to Ries 
had he been able to realize the project which Beet- 
hoven had suggested of their making a professional 
tour together. It had been arranged that Ries 
should play Beethoven's pianoforte pieces and other 
compositions, and that Beethoven should lead, per- 
forming fantasias only. The fantasias of Beethoven, 
when he was in the vein, or excited, were truly won- 
derful, and few performers have attained the same 
eminence in this branch of the art. The fertility of 
his invention, the inexhaustible variety which he 
shewed in the treatment of a subject, and his power 
of overcoming every difficulty which presented itself 
were alike astonishing. It was remarkable, too, 
how the inspiration of the moment rendered him 
insensible to external impressions. ‘On one occa- 
sion,” Ries used afterwards to relate, ‘‘ I was sitting 
at the piano, and Beethoven near me, conversing 
about themes for fugues. Whilst I was playing the 
first fugue from Graun’s “‘ Death of Jesus,” Beet- 
hoven commenced to play it after me with the left 
hand, and then with both hands, and thus continued 
without the slightest interruption for a full half 
hour. It was to me inconceivable how he could 
hold out so long and in so inconvenient a position.” 
Without overvaluing himself, Beethoven knew his 
own merit as a composer; and when the celebrated 
Clementi came to Vienna, he lent a willing ear to 
the suggestion of a friend, that the Italian should 
be allowed to pay the first visit. The consequence 
was that they never knew each other except by 
sight. It often happened that Clementi, with his 
pupil Klengal, and Beethoven with Ries, sat toge- 
ther at the same table without the interchange of a 
single word. This restraint was still less agreeable 
to the pupils; but Ries was more disconcerted by an 
unlucky incident which provoked in a high degree 
that irascibility which has been more than once men- 
tioned as a peculiarity in Beethoven’s character. 
Beethoven having finished a beautiful movement 
for the pianoforte, played it to Ries, who was so 
much delighted with it, that he asked to hear it a 
second time. On his way home, Ries, passing the 
house of Prince Lichnowsky, wentin, in ordertoinform 
the Prince of the production of this new master- 
piece. Remembering a considerable part of it, he 
played it to the Prince, who, being a good musician, 
and having an excellent memory, was enabled to 
retain several passages in it. Wishing to confound 
the author, the Prince called the next morning upor 
Beethoven, and told him that he had just composed 
an andante, of which he desired his opinion. In 
spite of Beethoven's distinct declaration that he did 
not wish to hear it, the Prince sat down at the 
pianoforte and began to play. He had not pro- 
ceeded far when Beethoven recognised his own com- 
position, and his surprise speedily gave way to 
anger, when it occurred to him that nobody but Ries 
could have communicated this unpublished piece. 
He swore that he would never again play in the pre- 
sence of his pupil, and he kept his word; for an 
subsequent occasions he always required him to 
leave the room before he would commence playing

calmly replied. ‘I was not aware of this before




508THE MUSICAL TIMES.—Srprremper 1, 1868

One day, at a breakfast given by Prince Lich- 
nowsky to a select number of friends, amongst whom 
were Beethoven and Ries, it was proposed to adjourn 
to Beethoven’s residence to hear some portions of an 
opera he had just composed, but which had not yet 
been performed in public. Beethoven accordingly 
sat down to the instrument, but, before beginning to 
play, peremptorily demanded that Ries should leave 
the room. Ries obeyed with tears in his eyes, but 
the Prince, justly displeased at such treatment, of 
which he himself had been the cause, expressed 
himself warmly on the subject. A sharp altercation 
ensued, and the matter ended by Beethoven rising 
from the pianoforte and refusing to play to the com- 
pany, who separated much disappointed. It is

ainful to add that Beethoven’s obduracy remained 
inflexible, and that from this time, though in other 
respects they continued on a friendly footing, Ries 
never heard him play a note

Independence was Beethoven’s swmmum bonum, 
and his great anxiety to exhibit it on all occasions 
led him to reject many kind offices which others 
would have accepted with every mark of gratitude. 
For a short time he was a guest in the house of his 
friend, Prince Lichnowsky, who provided an apart- 
ment for him, and gave him the use of his table; 
and, knowing Beethoven’s jealousy of any thing like 
the appearance of neglect, the Prince gave orders to 
his servants, whenever they heard his own bell and 
that of Beethoven’s ring at the same time, invariably 
to attend to Beethoven’s first. No sooner, however, 
did Beethoven discover that such an order had been 
given than he immediately engaged a servant at his 
own expense, though at that time he could but ill 
afford it

Beethoven was by no means so attentive to the 
rules of politeness in his own conduct towards 
others, as he was in exacting a punctilious deference 
towards himself. One evening, at a soirée given at the 
house of an old Russian Countess, Prince Louis 
Ferdinand, of Prussia, one of the most distinguished 
amateur musicians of the time, and an enthusiastic 
admirer of Beethoven, was present. When the con- 
cert was concluded, the company retired to supper 
in an adjoining apartment. Beethoven followed the 
guests, but finding that there was no room for him 
at the table, the covers having been laid only for the 
noblesse of the party, gave vent to his feelings of 
mortification in no unequivocal terms, and abruptly 
left. A few days afterwards, Prince Louis Ferdi- 
nand gave a grand entertainment, to which Beet- 
hoven was also invited. The old Countess, too, was 
pam. but matters now took a different turn

hen the company sat down to table, Beethoven 
found himself placed between the Prince and the 
Countess, an arrangement by which he was com- 
pletely pacified, and he never ceased afterwards to 
make use of every opportunity of alluding to this 
mark of distinction amongst his friends. He was, 
in fact, more gratified by any mark of honour and 
condescension which he received from the great 
than might have been expected from the general tone 
of his character. When the King of Prussia sent him 
a gold snuff-box, he was very fond of shewing it, 
and always remarked that it was not an ordinary 
box, but such as it was usual to give only to 
Ambassadors

As a general rule, trivial matters were passed un- 
noticed by the great musician. His mind was so 
much engrossed by his professional studies, that it

was no affectation on his part to despise many things 
to which others attach importance. It was chiefly 
when deeply engaged in some great composition that 
his fits of absence and abstraction were most re- 
markable. At such times his household affairs, not- 
withstanding their great simplicity, were entirely 
neglected. He had dedicated a work to Count 
Browne, and that nobleman, as a mark of gratitude, 
presented him with a fine saddle horse. Beethoven 
at first showed a great partiality for his new acqui- 
sition, and for a time rode out every day ; but his 
musical occupations soon put the horse entirely out 
of his head, and he forgot even to make arrange- 
ments for its keep. His servant, however, turned 
his master’s forgetfulness to a good account by letting 
out the animal at so much a-day, and pocketing the 
hire. Beethoven knew nothing of the matter till he 
was surprised, at length, by the appearance of a. 
long bill for corn and hay, which he was obliged to 
pay. He now considered it high time to get rid of 
the horse; but we are not told that he got rid like- 
wise of the trusty domestic, who had mulcted him 
to such an extent for his own advantage

Beethoven's absence of mind was very frequently 
exhibited. When the charms of nature, which 
he always loved, allured him to a distance from his 
home, he often forgot, to the serious annoyance of 
his careful housekeeper, that his meals would be 
awaiting his return at the usual hour, and thus all 
her pains and anxiety to provide a repast likely to 
please him, were entirely thrown away. It was not 
always, however, that he was so oblivious of the wants 
of nature. ‘Great men,” (says Schindler) “as 
well as their inferiors, are subject to certain natural 
wants, such as eating and drinking. Some of Beet- 
hoven’s peculiarities in these matters deserve to be 
ranked amongst the curiosities of house-keeping. 
For his breakfast he usually took coffee, which he 
frequently prepared himself, for in this beverage he 
had an Oriental fastidiousness of taste. He allowed 
sixty beans for each cup; and lest his measure should 
mislead him to the amount of a bean or two, he 
made it a rule to count over the sixty for each cup, 
especially when he had visitors. He performed this 
task with as much care as others of greater import- 
ance. Among his favourite dishes was bread-soup, 
made in the manner of pap, in which he indulged’ 
every Thursday. To compose this, ten eggs were 
set before him, which he tried before mixing them 
with the other ingredients; and if it unfortunately 
happened that any of them were musty, a grand 
scene ensued. The offending cook was summoned 
to his presence by a tremendous ejaculation; she, 
however, well knowing what might occur, took care: 
cautiously to stand on the threshold of the door, 
prepared to make a precipitate retreat; but the 
moment she made her appearance the attack com- 
menced, and the broken eggs, like bombs from well- 
directed batteries, flew about her ears, their yellow 
and white contents, covering her with viscous 
streams

To be continued




ROYAL*ACADEMY OF MUSICTHE MUSICAL TIMES.—Serpremser 1, 1868. 509

interesting to those who, having followed us in our 
remarks upon the past and present position of the 
Academy, are desirous of receiving 4 practical proof of 
the talent contained within its walls. Foremost amongst 
the compositions of the students, we must mention the 
first movement of a Symphony by Mr. Alwyn, which is 
not only based upon the enduring models bequeathed to 
us by the greatest writers, but instinct with a true per- 
ception of orchestral colouring. A Capriccio, with full 
instrumental accompaniments, the composition of Mr. W. 
Shakspeare (who most ably performed his own work), is 
alse full of good writing, and shows that the young com- 
poser’s talents ave been directed in the right school. A 
vocal trio by Mr. Jackson, is excellently written for the 
voices; and a very pleasing part-song, by Miss Dowling, 
proved that the male students have not absorbed the 
whole of the creative musical talent in the Institution. 
The pianoforte playing was uniformly good. Mr. Alwyn 
performed the first movement of Ferdinand Hiller’s 
Concerto, in F sharp minor, with a decision and real 
musical feeling scarcely to be expected from one so young 
—Miss Buer (of whom we have already made favourable 
mention at Mr. Walter Macfarren’s Recitals) gave the 
first movement of Beethoven’s Concerto in E flat, with 
true perception of the meaning of the composer—Miss 
Vokins played Mendelssohn’s I’antasia in F sharp minor, 
with judgment and earnest appreciation of the work ; and 
Mr. Kemp, in Bach’s “ Chromatic Fantasia,” exhibited a 
well trained finger, and an intimate knowledge of the 
severest school of pianoforte playing. Several vocal pieces 
were sung with much effect by Misses De Chastelaine, R. 
Jewell, Home, A. Lohman, Gardner, Severn, Christian, 
Lanham, &c. A very commendable performance ot a 
portion.of Mendelssohn’s music to Athalie was also given, 
the solo parts being ably sustained by Misses Ryall, R. 
Jewell, Greenaway, Christian, Lohman, A. Lohman, 
Lanham, De Chastelaine, and Home. The concert was 
ably conducted by Mr. Otto Goldschmidt. At theconclusion 
of the performance the prizes were distributed by Lady 
Thompson (well known as a former pupil of the Academy 
as Miss Kate Loder). The silver medals were awarded 
to Miss R. Jewell, and Mr. W. Shalaspeare; and bronze 
medals to Misses Dowling and Lohman, and Mr. Alwyn— 
other prizes being assigned to Misses Scates, Vokins, 
Severn, De Chastelaine, Buer, Ryall, Lanham, and Tovey ; 
Messrs. Townsend, J. Jackson, Cover, Richards, and 
Kemp. ‘The following received honourable mention: 
Misses Cullenford, Field, Gardner, L. Gardner, Waite, 
Christian, Watson, Home, Greenaway, Goode, and 
Sharpe; Messrs. Randall, Heywood, Cook, Beardwell, and 
Pettitt

GENOA




510


WILLIAM STERNDALE BENNETTWe have been requested by a correspondent 
to insert the following notice of a Concert, given by the 
pupils of Mr. Cottam :—“‘ The members of the St. George’s 
Choral Society gave their first Public Concert on the 
evening of the 11th ult., in the National School Rooms, 
Borough Road, when, notwithstanding the state of the 
weather, the place was literally crowded; great interest 
being taken in Mr. Cottam’s new method of instruction. 
The vigour and precision displayed in the choruses, gave 
the greatest satisfaction; but it was in solo singing that 
the effects of the teaching were most perceptible, the florid 
passages being given with the utmost ease and confidence, 
while the style and finish left nothing to be desired. In 
consequence of such a decided success, it is the intention of 
of the Society to give a series of Concerts during the 
winter

THE arrangements for the Gloucester Musical 
Festival, which commences on the 8th inst., are, we are glad 
to hear, proceeding most satisfactorily; the number of 
Stewards obtained (chiefly through the influence of the in- 
valuable Secretary, Mr. J. H. Brown) amply ensuring an ex- 
cellent guarantee fund. The principal vocalists engaged 
are Madlle. Tietjens, Madlle. Liebhart, Miss Edith Wynne, 
Madame Sainton-Dolby, Madlle. Zandrina, Madlle. 
Drasdil, Mr. Sims Reeves, Mr. Vernon Rigby, Mr. Lewis 
Thomas, and Mr. Santley. The performances in the 
Cathedral will be, on the first day 7’he Creation (first part), 
S. Wesley’s 1lith Psalm, Confitebor tibi, Mendelssohn's 
Psalm, “ As the hart pants,” and Beethoven’s “ Service 
in C:” on the second day Mendelssohn’s Flijah: on the 
third day Mendelssohn’s ‘“ Hymn of Praise,” with selections 
from the works of Spohr, Handel’s Samson, and Herr 
Schachner’s Jsrael’s return from Babylon: and on the 
fourth day the Messiah. A feature in the evening Concerts, 
at the Shire Hall, will be the performance of selections 
from Der Freischiitz and Don Giovanni. Mendelssohn 
will, we understand, be well represented; the First 
Walpurgis Night, the Finale to Lorely, the Reformation 
Symphony, and the Overture to the “Isles of Fingal,” 
being included in the programmes. We refrain from any 
comment upon the published scheme of the performances, 
as a detailed notice of the Festival will appear in our next 
number

Rebielos




GLEE. 


— SS SSS SES SS EES 


I T —— =F 


XUM


R |. I


———— — SS 


515


516


517


518


519


_——K


TO THE EDITOR OF THE MUSICAL TIMES


10 THE EDITOR OF THE MUSICAL TIMES


520


XUM


521


522


DURING THE LAST MONTH


HE PLA


E DEUN 


523


ANGLICAN CHORAL SERVICE BOOK, 


USELEY AND MONK’S PSALTER AND 


QULE’S COLLECTION OF 415 CHANTS, 57 


3. (HE PSALTER, PROPER PSALMS, HYMNS


{JOULE’S DIRECTORIUM CHORI ANGLI- 


ORDER FOR THE HOLY COMMUNION, 


EV. R. HAKING’S ANTHEMS 


BAPTISTE CALKIN’S EASY MORNING 


DWARD HERBERT'S CHANT TE DEUM, 


ENDELSSOHN’S SIX CATHEDRAL


524


FOR THE 


THE MUSIC COMPOSED BY


ORGAN, BY W. T. BEST


COMPLETION OF COLLIN’S “CHURCH ORGANIST," 5 


THREE BOOKS. 4


NEW WORK FOR CHURCH ORGANISTS. 


OSEPH BARNBY’S ORGAN MUSIC.— 


HANDEL’S CHORUSKS


ANIST,” 1


FOR THE ORGAN, 


W. T. BEST


CONTENTS OF VOLUME I. Ducis

Beethoven 
ooo Spohr 
ee Mozart 
.. Schneider

Handel 
Handel

Beethoven 
Handel

ose Kullak 
Mendelssohn

Handel

Beethoven 
Mozart

Romanza from the Sy: mphony “ La Reine de France” Haydn




CONTENTS OF VOLUME II. 


25


28


CONTENTS OF VOLUME IIIAdagio, from the Symphony in C major, No.7 ww 
Choral Fugue, ** Dona nobis pacem,”’ Mass in B minor

Choral Fugue, ** Kyrie Eleison,” Mass in G major... 67 to 1: 
Spohr | 42. Overture to the Oratorio “St. Paul” fe Mendelf 133 to 1 
Adagio, from the Pianoforte Duets ) 
Mendelssohn Choral Fugue, “Sicut locutus est,” from “the Mag- . 
«. Handel nificatinD ... oo MS to i 
«. Handel Adagio, from the Sy mphony inE fi: at. No. ‘4 1 
oo Spohr Allegro, from the Third Sonata for Pianoforte and Fiat 
+e —_ Handel Finale to the Second Sonata for Pianoforte and Violin 160 to 17 
. Hummel | 44. Chorus, “ God thou art great,” from the Sacred ors § d 
+e Beethoven Larghetto from the 12th Grand Concerto oo =H ms 
e. Handel Motett, “* Misericordias Domini” ... ove oa = to 18 
es» Mozart] 45. Andante, from the Symphony in E flat ue A. Ro $2.—The 
Handel Chorus, “ How soon our tow'ring hopes” (sete » Hy 1838—Var 
Andante, from the Symphony in rE flat, No.2 «| 8 
Quartett, ‘* Recordare Jesu pie,” Requiem Mass ons 14 
Mendelssohn Choral Fugue, “* Et vitam venturi,” Mass in D major ii 193 to 19 
Handel Largo, from the Fourth Quartett in E flat... ” and ] 
+ Handel] 47. ‘* Out of the deep.” The 130th Psalm ne | 
eve Bach Funeral March from the Pianoforte Sonata, Op. 3... @ TTENRY 
° Handel Adagio from the Pianoforte Duet Sonata in B flat... # i TIONS 
Beethoven i Minuet, from the Overture to “ Berenice” .. on No.1 Ch 
« ‘Mozart Minuet, from the Serenade inC minor... oe ‘oT . 
ee Schubert Minuet, from an Instrumental Concerto... oe 3 Gre 
ais Spohr} 49. Grand March (Op.40) ... ae phi Fr. aa 
sw. Handel Chorus, “ Hail, mighty Joshua” ow FB 
+ Handel! 50. Pastoral Sy mphony from the Christmas Oratorio - I BAPT 
Chorus, “Be joyful, ye redeemed,” from a Sacred « POSIT] 
«. _ Handel Cantata for the Festival of St. John the Baptist Mf, 1 Andante 
Meyerbeer Adagio, from a Sonata for Violin and Pianoforte in i 2 Hom 
Beethoven G minor os 3 Marche’ 
Handel | 51- ‘* Pater Noster,” Motett for Chorus and Orchestra MT 4 Andante 
a Sarabanda, from the Second Violin Sonata os Hymn 
nee Bach Chorus (on a Ground Bass), “Envy, eldest born of 
ew. Handel Hell,” from the Oratorio of Saul . 
«. Haydn] 52. Allegro marziale, from the Pianoforte Duets (Op. 60

Bourée from the Second Sonata for Violinalone | j

Religioso ( Lobgesang ) Mendelssohn 
4] Put, “Let us sing His hallow'd praises

Op. 96) : ove Mendelssohn 
Chorale, “O God, “thon holy Lord" ” an Bach 
_ Adagio from the Fourth Symphony Beethoven

Allegretto Scherzando from the Eighth Symp hony Beethoven 
1, Overture “Esther ” Handel 
Andante from the Fifth Quintett Mozart 
3, Adagio and Grand Fugue in Cc major (th Sonata forV me Bach 
ho, Overture to the Opera a? tom” «. Handel 
Allegretto from the Seventh Symphony

Beethov en




» M@PINKS PRACTICAL ORGAN "SCHOOL, 


THE


BY


CONTENTS OF PART I


RUDIMENTARY MANUAL EXERCISES. 


NEAR THE CLOSE


CERTAIN PASSAGES, IN ORDER TO SUSTAIN THE SOUND DULY


ENDINGS. 


(1615.) 65. ALLEGRETTO PASTOR- 


END OF THE FIRST PART


_—_


JUST PUBLISHED


THE ORGANIST’S PORTFOLIO


A SELECTION OF OPENING, MIDDLE, AND CONCLUDING VOLUNTARIES 


BY


CELEBRATED COMPOSERS, ANCIENT AND MODERN


ADAPTED CHIEFLY FOR


EDWARD F. RIMBAULT


CHAPPELL AND CO, 49 AND 50, NEW BOND STREET, LONDON, W


CHAPPELL’S CHEAP WORKS 


FOR VARIOUS INSTRUMENTS. 


TUTORS. 


VIOLIN. 


FLUTE. 


CORNET-A-PISTONS


ENGLISH CONCERTINA. 


GERMAN CONCERTINA


HARMONIUM. 


GUITAR. 


CHAPPELL & CO., 50, NEW BOND-STREET, LONDON