


 HE MUSICAL TIMES 


 AND singing - class circular 


 publish on the first of every month 


 SEPTEMBER 1 , 1876 


 BRISTOL MUSICAL FESTIVAL 


 COLSTON HALL , 


 » ALBANI , » W.H. CUMMINGS , 


 BRIXTON CHORAL SOCIETY , 


 ANGELL TOWN INSTITUTION , BRIXTON 


 ING ’s COLLEGE , CAMBRIDGE . — 


 SOPRANO . — re - engagement require » 


 REENWICH PARISH CHU RCH.—WANTED , 


 INCOLN CATHEDRAL.—THREE boy 


 JOHN FRANCIS BARNETT ’s oratorio , 


 PROFESSIONAL . notice 


 USIC engrave , print , and PUB 


 verical IRON frame pianoforte 


 GTRUNG upon a single casting 


 ORGAN - tone harmonium 


 HATTERSLEY & CO.”"S celebrate 


 ORGAN - tone drawing - room model 


 HATTERSLEY : & CO . 'S 


 book of design and above 200 testimonial 


 * show room : — 


 RUSSELL 's MUSICAL instrument . : 


 EAN ’s CHEAP MUSICAL instrument . 


 nt . 


 579 


 STANDARD AMERICAN ORGANS 


 manufacture by 


 PELOUBET , PELTON & CO . , NEW YORK 


 METAL EQUITONE flute , with KEYS 


 CYLINDER flageolet , with KEYS 


 LONDON agent , 


 PIANOFORTE SALOON , 


 55 , BAKER STREET , LONDON , W 


 TESTIMONIAL . 


 E & W. SNELL ’s improve harmonium , 


 AST LONDON ORGAN WORKS 


 FLAVELL 


 ANGLICAN 


 PSAL TER CHAN TS 


 SINGLE and DOUBLE 


 edit by the 


 AND 


 EDWIN GEORGE MONK 


 HE BRISTOL TUNE - BOOK , 


 TONIC SOL - FA EDITION 


 A MANUAL of SINGING 


 for the use of CHOIR trainer & schoolmaster , 


 FORTIETH EDITION . 


 the COLLEGIATE and school 


 sight - singing manual 


 appendix 


 ( companion work to the above 


 _ COLLEGIATE VOCAL TUTOR 


 CATHEDRAL CHANT book 


 new edition of 


 DR . BENNETT GILBERT ’S popular work 


 school harmony 


 every subject have its special exercise . 


 AKER ’S PIANOFORTE TUTOR 


 YOL 


 0 . 5 


 ork 


 R. L. PEARSALL 


 collegiate serie 


 consecutive fifth and octave 


 IN COUNTERPOINT 


 AN ESSAY , by R. L. PEARSALL . 


 NEW CONTRALTO SONG . 


 PE AC E 


 WILLIAM J. YOUNG ’S 


 OCTAVO . 


 the REY . R. BROWN - BORTHWICK ’S 


 serie of 


 MODERN kyrie 


 see under 


 lesse be the dead . " a 


 TRINITY COLLEGE , LONDON 


 institute 1872 ; incorporate by SPECIAL 


 CHARTER under act of parliament , 1875 


 department of evening class 


 HARMONY 


 oyal LEAMINGTON SPA . — ORGANIST 


 ANSTEAD CONGREGATIONAL CHURCH 


 LBANY - STREET CONGREGATIONAL offer in return . address Choirmaster , 7 , Eardley - crescent , West 
 Brompton , S.W 

 dvertiser desire an engagement as 
 organist or choirmaster in or near London . address 
 Beethoven , Post - office , Goldhawk - road , shepherd’s - bush , W 

 GENTLEMAN ( Assoc . Mus . T.C.L. ) be desirous 




 RGANIST and CHOIRMASTER.—A gen- 


 the MUSICAL time 


 and SINGING - class circular . 


 SEPTEMBER 1 , 1876 


 the NIBELUNGEN TRILOGY at BAYREUTH 


 by JOSEPH BENNETT 


 584 


 dose 


 585 


 58 8 


 the MUSICIANS ’ company 


 59 


 ALEXANDRA PALACEance of the " Adagio " ' and " Scherzo " from a pianoforte 
 Concerto by Litolff , and some showy F antasia . the 
 concert have be fully attend , in spite of the proverbial 
 ' thinness " of the metropolis at this time of the year 

 the English Opera Season at the Lyceum Theatre , 
 under the direction of Mr. Carl Rosa , will commence on 
 Monday , the 11th inst . , the work select for the opening 
 night be Cherubini ’s " water Carrier , " the production of 
 which at the end of last season be so heartily welcome 
 by all music lover . Sir J. Benedict ’s " Lily of Killarney , " 
 with alteration and addition specially write for this 
 company , will be perform ; and for novelty we be 
 promise " Joconde , " ' by Nicolo Isouard ; ' " Giralda , " ' by 
 Adolphe Adam ( both for the first time in England ) ; a new 
 opera , " Pauline , " by F. H. Cowen , write expressly for 
 this company ; Beethoven ’ we Fidelio " ( as originally com- 
 pose , without recitative ) ; and Wagner ’s " fly Dutch- 
 man , " ' for the first time in English . the company will 
 include , among other , Mdlle . Ida Corani , Miss G. War- 
 wick , Miss Cora Stuart , Miss Gaylord , Miss oe 
 Yorke , Miss Lucy Franklein , Mr. H. Nordblom , Mr. F. 
 Packard , Mr. C. Lyall , Mr. F. H. Celli , Mr. A. Cook , ve 
 Mr. Santley . the orchestra will be lead by Mr. Carrodus 

 AN article recently publish in the Standard draw 
 attention to the good work now progress in Italy , under 
 the earnest direction of Signor Giulio Roberti . all music 
 lover who remember the state of the art in Italy some 
 time back must have be strike with the inferiority of 
 modern performance , both sacred and secular , the ser- 
 vice at St. Peter ’s , even the singing of the nun at the 
 once celebrate Trinita dei Monti be far from satis- 
 factory ; and at the opera house the ' * Spectacle " and the 
 ' * * Ballo " be now the principal attraction . in the hope of 
 remedy this state of thing , Signor Roberti , who have 
 already make his name in England by the publication 
 of a Mass and several piece " of chamber music — ob- 
 taine permission to found a school of choral singing 
 at Florence , the success of which have be remarkable . 
 since then he have be charge with the establish- 
 ment of class of music in all the municipal schgol , 
 and with the yet more important organization of a normal 
 school for master and mistress . although it must be 
 some time before great result can be achieve , the move- 
 ment be steadily advance ; and we trust that the effort 
 of Signor Roberti in initiate so important a reform may 
 be fully and extensively recognize 




 TREBLE . 


 — < — = — _ — _ _ 


 — — _ — — _ 


 42)9 ) . = LL 


 — _ — _ 


 A 5 


 SPI 


 — = — _ _ 


 NR 


 KG 


 ‘ 3 = 4 { s| 4 =| 


 23 


 59f 


 review 


 B.A 


 TO correspondent 


 BRIEF summary of COUNTRY NEWSTHE MUSICAL TIMES.—SeEpremser 1 , 1876 

 Lincotn.—At the opening of the organ at the " John Hannah " 
 Wesleyan chapel , on the 2nd ult . , a recital be give by Dr. Spark , 
 of Leeds , when a tasteful and judicious variety of music be select 
 from the work of Handel , Rossini , Beethoven , Haydn , Guilmant , and 
 Sebastian Bach . by particular desire , Dr. Spark introduce the music 
 of the pastoral symphony from the Messiah , and conclude with the 
 hallelujah chorus . Dr. Spark be evidently an enthusiast in his pro- 
 fession , and do not limit his performance to the afternoon . he give 
 some voluntary during the morning and evening service , and wind 
 up at night by play the Grand March in Sir Michael Costa ’s 
 Oratorio Eli . the new organ be a magnificent instrument , and 
 form a fine finish to the chapel , which be itself an example of simple 
 elegance in the formation and fitting up of a place of worship 

 MACCLESFIELD.—The new organ at King Edward - street chapel be 
 open on Sunday , the 6th ult . , by W. H. Ford , jun . , organist of the 
 chapel . the follow be the specification of the instrument , which 
 have be erect by Mr. John Bellamy , of Hanley . great organ , C C 
 to A ( 58 note ): Cremona , 8 ft . ; open diapason , 8 ft . ; stop diapason bass , 
 8 ft . ; dulciana , 8 ft . ; principal , 4 ft . ; hohl flute , 4 ft . ; fifteenth , 2 ft . ; 
 swell organ , c c to a(58 note ) ; hautboy , 8 ft . ; double diapason , 16 ft . ; 
 open diapason , 8 ft . ; lieblich gedacht , 8 ft . ; gemshorn , 4 ft . ; piccolo , 
 2 ft . pedal organ , c c c to f ( 30 note ) ; grand Bourdon , 16 ft . ac- 
 cessorie : tremulant to swell , swell to great , pedal to swell , pedal 
 to great , two composition pedal . the manual be upon a new prin- 
 ciple by Mr. Bellamy , and be extend to full modern compass . the 
 pedal be radiate , and the general quality of tone excellent , the solo 
 stop be especially admire 

 Pret , Istt or Man.—A Choral Festival be hold in the ruin of 
 St. German 's Cathedral , on the 17th ult . , in which fifteen choir ( 200 
 voice ) take part . the Magnificat and Nunc dimittis be sing to 
 chant , and the anthem be " o Lord , how manifold " ( Barnby ) . the 
 rendering of the various part of the service be very creditable to the 
 choir engage , but the accompaniment of a harmonium be insufficient 
 to sustain so large a body of voice . the sermon be preach by the 
 Bishop of Lichfield , and the Rev. F. Moore act as conductor . the 
 congregation number 2,000 , and the collection realize about £ 50 

 Sanpown , I.W.—An excellent concert be give on the 3rd 
 ult . , in the Town Hall , by Mr. Augustus Aylward , of Ryde . the 
 artist be Miss Amy Aylward , R.A.M. , Miss Leila Aylward , R.A.M. , 
 Miss Gertrude Aylward ( Stuttgart Conservatoire ) , Mr. W. H. Aylward , 
 R.A.M. , Mr. T. E. Aylward ( organist , Cathedral , Llandaff ) , Mr. W. P. 
 Aylward , Mr. A. Aylward , Mr. F. H. Simms ( organist , All Saints , 
 Ryde ) , and Mr. A. Burnett ( Royal Italian Opera ) . the programme 
 be select from the composition of Beethoven , Handel , Schubert , 
 Gounod , Donizetti , & c. , and include Beethoven ’s G major Trio , 
 Op . 1 , no . 2 , Schubert 's " La jeune religieuse , " arrange for piano , 
 violin , cello , and organ , Handel 's sonata in a for piano and violin , & c 

 r. W. H. Aylward be highly ful in his vi llo solo , as 
 be also Miss Amy Aylward in the song " o bid your faithful Ariel 
 fly ' ( Arne ) , with orchestral accompaniment . Mr. Burnett be the 
 solo violinist 




 notice to PIANOFORTE dealer and professor . 


 dure the last month 


 MR . CHARLES FRY 


 E FREDERICK WROE , decease . — 


 business for sale . 


 O PIANOFORTE and MUSIC - SELLERS 


 O AMATEUR CHORAL SOCIETIES . — 


 OUTH LONDON MUSICAL TRAINING 


 CONCERT AGENCY.—COPYRIGHT 


 HE YORKSHIRE CONCERT PARTY . — 


 HE YORKSHIRE ( ST . CECILIA ) CONCERT 


 establish APRIL 1866 . 


 HE ENGLISH GLEE UNION . 


 assist by 


 ADIES ’ SCHOOL , LANGHAM HOUSE 


 INEY 


 OUGH 


 2RT 


 ON 


 R. H. WALMSLEY LITTLE ,   F.C.O. , 


 RGAN , HARMONIUM , PIANO , SINGING 


 OPINIONS of the PRESS 


 HE MUSICAL DIRECTORY , 1877 


 new story by SABILLA NOVELLO 


 just publish , 


 BLUEBEARD ’S WIDOW and her 


 sister ANNE 


 BY 


 SABILLA NOVELLO . 


 with illustration by the authoress 


 dedicate , by permission , to 


 604 


 the anglican HYMN - book 


 NEW edition , revise and enlarge 


 USELEY and MONK ’S PSALTER and 


 OULE ’s collection of word of 


 HE PSALTER , PROPER psalm , HYMNS 


 OULE ’S DIRECTORIUM CHORI ANGLI- 


 OULE ’s DIRECTORIUM CHORI ANGLI 


 HE order for 


 THE HOLY communion . 


 NEW FESTIVAL ANTHEM . PRICE SIXPENCE 


 when the LORD turn again 


 the captivity of SION 


 PSALM CXXVI 


 FESTIVAL anthem 


 consisting of 


 open and conclude chorus , and 


 solo for TENOR and SOPRANO . 


 compose by 


 CHARLES JOSEPH FROST 


 U.FRED 


 OUR 


 NNAIS 


 RIST- 


 SE 


 GAIN 


 the REV . THOMAS HELMORE ’S 


 manual of plain song 


 now publish 


 harmony of the brief directory 


 the village organist 


 taste and see how gracious the 


 IX movement from MENDELSSOHN ’s 


 proposal for publishing by subscription 


 SIX cantata 


 BY CARISSIMI , 


 edit ( with pianoforte accompaniment ) by 


 RIDLEY prentice 


 eight organ piece 


 ORIGINAL and select 


 BY 


 ALTER MACFARREN ’S new " suite 


 DE piece . " 


 66 INSTER window . " CIRO PINSUTI ’S 


 TO CHORAL society . 


 NEW HARVEST ANTHEM . 


 compose by 


 HENRY SMART 


 ALBERT LOWE ’S 


 HARVEST carol . 


 " HOLY I$ the seed - time 


 NOVELLO , EWER and CO 


 TWOPENCE . 


 « BARNBY 


 EASY anthem for HARVEST festival . 


 specially adapt for HARVEST festival . 


 ARVEST anthem . 


 EW and CHEAPER edition . 


 WAGNER ’S OPERAS , 


 LOHENGRIN 


 AND 


 TANNHAUSER 


 twelve hymn with tune 


 for 


 HARVEST 


 select from 


 the HYMNARY 


 PRICE one PENNY 


 time - 


 PRAISE , o praise our heavenly G 


 KING - ERMAN 


 HARVEST anthem 


 MART , HENRY.—THE LORD HATH do GREAT 


 ALLCOTT , W. H.—THOU VISITEST the earth and 


 ACFARREN , G. A.—GOD say , BEHOLD , I have give 


 ATTISON , T. M@—O how PLENTIFUL be thy GOOD- 


 AYLOR , W.—THE eye of all wait upon THEE . 


 RIDGE , J. FREDERICK.—GIVE unto the LORD the 


 EETON , HAYDN.—THE eye of all wait upon 


 LEY 


 EAT 


 NTO 


 ALL 


 the 


 RTH . 


 IRLEY 


 OOD- 


 ‘ HEE . 


 ) the 


 - ALL 


 UPON 


 BY 


 COMMITTEE . 


 NOVELLO , EWER and CO . ’S only complete and 


 UNIFORM edition of 


 MENDELSSOHN ’s 


 thirteen two - part song 


 just publish . 


 compose by 


 NIELS W. GADE 


 the crusader 


 compose by 


 NIELS W. GADE . 


 the HOLY SUPPER of the 


 APOSTLES 


 compose by 


 RICHARD WAGNER 


 PIANO 


 QPP HAD E HH SPRAY 


 PERGOLESI ’S 


 SBTABAT MATES 


 FEMALE voice . ) 


 JOHN HULLAH 


 NOVELLO ’S 


 TONIC SOL- FA serie 


 the follow number be now ready 


 the hymnary ( tune only ) . 


 SPECIMEN page . ) 


 forward ! be our watchword 


 ONWARD , CHRISTIAN SOLDIERS 


 ARTHUR S. SULLIVAN .