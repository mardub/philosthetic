


 publish 


 MUSICAL TIMES 


 singing - class circular . 


 FOUNDE 


 D 


 irst 


 1844 . 


 month 


 ROYAL CHORAL SOCIETY . ROYAL 


 ~ MANCHESTER COLLEGE MUSIC . 


 F. W. RENAUT , 


 ROYAL COLLEGE MUSIC , 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. 


 FRANK POWNALL 


 GUILDHALL SCHOOL MUSIC . 


 JOHN CARPENTER ST . , VICTORIA EMBANKMENT , E.C. 


 LANDON RONALD 


 WEEKLY orchestral 


 practice conduct 


 organist 


 ROYAL COLLEGE 


 


 MIDLAND 


 MUSICAL COMPETITION FESTIVAL , 


 BIRMINGHAM , 


 20 24 , 1913 


 7,500 competitor , 170 choir , 11 orchestra 


 HAN 


 PRIESTLEY 


 DAY 


 BRASS BAND contest 


 BLACKPOOL - 


 COMPETITIVE 


 great festival 


 OCTOBEL . 13 - 18 , 1913 . 


 SYLLABUS ; ready . 


 


 OLIAN HALL , NEW BOND STREET , W. 


 UNIVERSITY DURHAM . _ 


 . AMINA GOODWIN . 


 SCHOLARSHIP , 


 290 


 VICTORIA COLLEGE MUSIC , 


 LONDON . 


 incorporate 1861 


 incorporate guild CHURCH 


 MUSICI 


 associate ( A.1.G.C.M. ) , licentiate ( L.1.G.C.M. ) , FEL- 


 competition 1913 . 


 register ORGAN vacancy . 


 MR . W. H. BREARE 


 student profession 


 TWOPENCE 


 ROY AL IRISH ~ ACADEMY MUSIC 


 WESTLAND ROW , DUBLIN 


 CG 


 GRAHAME - HARVEY 


 1913 


 LM . EHREMAYER 


 27 , CHANCERY LANE , LONDON , W.C 


 FINE MODERN violin 


 SOLD W. E. HILL & son 


 place business 


 140 , NEW BOND STREET 


 LONDON , W 


 PROFESSIONAL notice . 


 MISS ISABEL CLEAR ( CONTRALTO 


 MR 


 HERBERT TEALE 


 ( DRAMATIC TENOR 


 MISS LYDIA JOHN ( A.R.A.M 


 CONTRALTO 


 MR . FRANCIS GLYNN 


 MR . SAMUEL . MASTERS 


 WINIFRED MARWOOD 


 124 , WALM LANE , CRICKLEWOOD , N.W 


 MR . CHARLES KNOWLES 


 HERBERT MARKS ( LRA ™ M 


 BARITONE ) . 


 BARITONE 


 V IGGO KIHL 


 SOLO PIANIST 


 MR . JAMES RIC HARDSON 


 ' LONDON COLLEGE chorister 


 SOLO VIOLONCEI 


 EADING 


 DENHOF OPERA SEASON , 1913 - 14 


 composer ’ MSS . 


 DR . A. EAGLEFIELD HULL 


 evision MUSICAL composition 


 L.R.A.M. ( paper work 


 late success 


 F.R.C.O. 53 


 48 , 


 292 


 SCHOLAR . 


 HORAL 


 F.R.C.0 , 


 , VACANC 


 MUSICAL 


 


 , 1913 


 COUNTY LONDON 


 LAURENCE GOMME , 


 ORGANS 


 293 


 account publisher 


 MURDOCH SALON " ' e ready 


 1 . VIOLIN MUSIC . . EDUCATIONAL MUSIC . 


 MURDOCH , MURDOCH & CO 


 PIANO , ORGAN , MUSIC store 


 461 463 , OXFORD STREET , W. 


 PIANO PEDALS . 


 organist 


 NORMAN & BEARD 


 PNEUMATIC PEDAL attachment 


 P IANO 


 RECE ) NOVELLO OCTAVO edition . 


 K U BL KHAN 


 RHAPSODY 


 SOLO , CHORUS , ORCHESTRA 


 word 


 COLERIDGE . 


 music 


 S. COLE RIDG E - TAYLOR . 


 


 1913 


 messr 


 


 


 hold 


 HALF- - early sale 


 include 


 VIOLONCELLO 


 47 , LEICESTER SQUARE 


 valuable V iolin 


 PUTTICK & SIMPSON 


 HIGH - class VIOLINS , VIOLON ( CELLOS , BO\ 


 week JUNE 


 BROKEN MELODY , " 


 LON 


 DON , W.C 


 FIRM 


 CONACHER 


 OLD 


 HUDDERSFIELD . 


 gold medal 


 SPRINGWOOD WORKS 


 CO 


 RAINBOW 


 music reading ladder beginner 


 MALKIN PATENT PEDAL C 


 NEWCASTLE - - LYME 


 CALEB SIMPER 


 KILBIRNIE 


 BARNSTAPLE 


 establish 1750 . : 


 GRAY & DAVISON , 


 ORGAN BUILDERS , CALEB SIMPER 


 NEW anniversary selection . 


 N , WC 6 . 


 nner 


 19 , 


 — _ — II 


 ion . 


 , 1913 


 295 


 NOVELLO 


 string instrument 


 violin , VIOLA , VIOLONCELLO 


 17 


 18 


 19 


 20 


 27 


 2i 


 23 


 24 


 25 


 26 


 28 


 30 


 31 


 LEMARE , E 


 album PIANOFORTE 


 246 


 institute 1872 


 1913 


 BROADWOOD 


 player - piano 


 grand upright 


 illustrate catalogue application . 


 - illustration 


 m , » ) 


 p d > . | ; 


 3 s 5 m ~ 1 . . 


 QS 


 RKET 


 ion 


 eet 


 ESTRA 


 SINGING - class circular 


 1 , 1913 


 centenary BIRTH 


 RICHARD WAGNER 


 


 " 1954 


 WAGNER PROSE 


 work 


 ERNEST NEWMAN298 MUSICAL TIMES.—May 1 , 1913 

 
 ' Wibelungen . ' ideal selection begin | phraseology great treatise thirty year 
 ' Autobiographical sketch ' ( 1842 ) , work | later Beethoven — notably passage 
 early essay ' overture ' ( 1841 ) , | contend ' Leonora ' . 3 overture 
 ' art revolution ' ( 1849 ) , ' art - work / concise drama thay 
 future ' ( 1850 ) , ' communication Friends’| rest opera . 
 ( 1851 ) , article ' Liszt symphonic poem ' } ' autobiographical sketch ' 
 ( 1857 ) , ' Zukunftsmusik ' ( 1861 ) , _ the/| write leave Paris 1842 settle Dresden 
 ' destiny Opera ' ( 1871 ) , writing the| write practically prose till 1848 ; 
 years—‘actor Singers ' ( 1872 ) , ' on| brain mature musically th : 
 poetry composition ' ( 1879 ) , ' operatic } completion ' Lohengrin ' 1848 th 
 poetry composition ' ( 1879 ) , ' the|commencement ' Rhinegold ' 18 ; 
 application music drama ' ( 1879 ) . | actually write musical work dunr 
 work continuous picture his|/all time , 1842 1848 , thougi ’ 
 development consciousness as}he publish virtually prose , idea 
 dramatist musician . muscial zesthetic ripen , | ; 
 ' Beethoven ' ( 1870 ) its| official duty Dresden , 
 form expansion Schopenhauerian| composition ' Tannhiiuser ' anj 
 theory nature music : } ' Lohengrin , ' leave little time theoretical 
 penetrating interesting judgment | work ; 1848 evidently come th 
 certain contemporary in| great crisis growth . conceive 
 curtail version article Auber , | idea opera subject ' Siegfried 
 Spontini , . selection be| death , ' , unconsciously fir 
 complete ' explanatory | turn old world open 
 programme ' work of|and opera house . political activity durin 
 , fascinating treatise ' conducting ' ; | ferment 1848 1849 probably onj 
 altogether , imagine , ordinary | - product general restlessness hung : 
 student Wagner need know . |of mind time . muse 
 pity tnat Wagner prose style alter so| paper away , , si 
 bad grow old . * inhisyoung| year , Dresden 
 day write ina manner win admiration | swiss solitude try bearing 
 exact critic like Heine Berlioz — lightly , | general music particular . seethin ; 
 brightly , vein humour run } brain little comfort : ani 
 argument . early work is|the period 1848 1852 | 
 interesting confirm | work seizable form thousand ide 
 picture ' autobiographical | obsess 1842 , dai 
 Sketch , ' ' communication friend , ' and| clamorous expression . th 
 ' Mein Leben , ' eager young spirit } thoughtless old charge bh 
 unconscious definite goal life art , | work theory write opera ! 
 hover uncertainly musical taste ! demonstrate . truth ' Tristan ' 
 Beethoven , Weber , french _ italian|the ' Ring ' ' Parsifal ' fromhs 
 school . ' journalistic work Paris| young manhood , oak acon 
 merely good pot - boiling:| Wagner man assimilate 
 essay period specially worth | thing life alter fibre nate 
 preserving,—the delicious article Paris| personality , deflect inch thet 
 performance ' Der Freischiitz,’+ really| primal bias . nature grow accretion 
 admirable banter Paris Opéra , ! outside profoun¢ 
 article ' overture , ' in| modify original tissue , final man ist 
 embryo — 1841 — idea } balance strike 
 relation poetry music Wagner } come outside . othe , 
 work copious detail in| outside influence alter jot 
 ' Opera drama , ' work . is| original nature . Wagner : ® 
 luminous article , | predestine certain work , | 
 student musical esthetic } music prose simply s 
 fail read ; Wagner slowly | discovery , - grow oak 1 © 
 feel way — ' Flying Dutchman ' | acorn discovery . 
 complete — free emotional form } diagnosis explain unshakead 
 mature work . striking insight | confidence , intolerance contr ) 
 soul Beethoven music | opinion , inability grant possible existence 
 Wagner later writing impressive ; | exist hit 
 idea youthful essay have|and fact find 
 reproduce substance in| compass steer end > 
 = rin late year . -| musical writing yer 
 842 , , | life , lucid style = 
 article , the| concise argument thin 

 7a 




 _ _ _ 


 , i91t3 . 299he wholly absorb music - drama : 
 channel life - force 
 find adequate outlet ; 
 great need soul artist logic , 
 coherence , consistency , problem rack 

 build musical drama 
 profoundly human quality thoroughly 
 organic tissue . extremely interesting watch 
 ideal come realization stage 
 creation prose work . pursuit 
 vision range , tortuous page 
 ' opera Drama , ' heaven 
 earth beneath water 
 earth . rid huge book superfluous 
 matter arrive simple 
 proposition music - drama union drama 
 music , 
 , drama 
 worthy capable , 
 music live organism , 
 arbitrary collection badly - joint limb 
 flow blood . artist 
 drive create , use 
 thinker merely purpose 
 clarification . ' communication friend ' 
 simply long passionate statement 
 need organic substance form music- 
 drama , mean think 
 attain . unconscious 
 musician guide 
 control factor . 
 clearly wasso . witha 
 slight change original - 
 composer stamp Beethoven , 
 content work limit purely 
 orchestral form . 3ut musical sense 
 definite poetic turn Beethoven , 
 need definite poetic basis weave 
 symphony voice orchestra . music 
 meant little speak 
 directly humanity humanity . theme 
 invent mere invention sake , 
 work mere sake work - : 
 spring expression 
 overwhelming human need blind 
 vision , answer change 
 change life man mood 
 paint . inevitableness idea 
 form admire Beethoven 
 miss Brahms . inability 
 compromise matter 
 contemptuously sweep existence 
 music day . precisely 
 broadening beethovenian spirit design , 
 making instrument capable 
 express emotion mankind feel , 
 open enormous possibility 
 music . hardly tenth 
 new possibility realise 
 death . formalist , academic , 
 conservatoire us,—all crowd 
 little timid soul 
 ruin safe sheep - track ; 
 big man build Wagner 
 vast enduring edifice erect 
 work Beethoven . 
 failure blame : simply mean 
 year familiarity music 
 theory , world large 
 assimilate essence teaching . 

 MUSICAL t 




 30 


 problem DISCORDBerlioz find intolerably harsh discord 
 Wagner ' Tristan ' ; score work pre- 
 served bear contemptuous angry pencil 
 annotation proof sincerity . ream 
 paper nowadays blacken attempt 
 decide M. Debussy favourite harmony 
 beautiful unbearable . 
 time discover score music - lover prepare 
 declare M. Arnold Schénberg discord 
 extremely painful . let waste time 
 try ascertain case 
 matter eventually settle : 
 question pertain problem 
 discord come mind practical 
 esthetician include horoscope 

 zsthetical appeal discord use 
 definite dramatic purpose ( beginning 
 final section Beethoven Ninth Symphony , 
 climax Venusberg section ' Tannhauser 
 overture , Liszt ' Dante ' Symphony 
 afford striking familiar case point ) 
 comment . let notice , , 
 write dramatic music tone - poem — , 
 generally speak , intent express 
 intense definite feeling painful kind — 
 composer lead use , zvvenf daring 
 discord belonging usual idiom 
 ' pure ' music period . remark 1 
 special use present , 

 MUSICAL TIMES.—May 1 , 1913 . 301 




 1913 


 ERICH J 


 WOLFF song . 


 NEWMAN 


 088 , 


 i9i3- 3 ° 93 


 TQ 


 304 MUSICAL TSt . George Chapel Royal , separatec 

 important competition announce th 
 Kaiserliche Kénigliche Gesellschaft der Musikfreund 
 open composer nationality , g 
 | time student Vienna Conservatoip 
 K.K. Akademie fiir Musik und darstellende Kung 
 hold commemorate eightieth anniversary¢ 
 performance Beethoven ninth Symphony 
 | Kyrie , Credo , Agnus Dei , Dona nojjs 
 |pacem ' Missa solemnis . ' prize 29 
 | kronen offer good composition submitte , 
 form opera , orator 
 cantata , symphony , sonata , mor 
 instrument . ms . receive March » 
 date Beethoven death ) , December 
 | ( date birth ) year . work map 
 | submit competitor , usual fom 
 |of motto seal envelope adopt 
 Messrs. Robert Fuchs , Herrmann Gradener , Richard 
 | Heuberger , Robert Hirschfeld , Eduard Kremse , 
 | Mandyczewski , Franz Schalk consente 
 | act adjudicator 

 




 note ORGANS WINDSOR 


 CASTLE 


 SOR 


 ( pler 


 . coupler 


 accessory . 


 CHARLES R 


 R.W 


 WINDEBANK 


 306 


 BERNARD SMITH 


 167 3 . 


 LATIMER . 


 J. WILLIAMSON 


 


 DANBY . 


 107 4 


 12 100 


 " 52 16 


 3 ° 97 


 1913 


 ORGAN WALTON - - THAMES 


 


 


 308 


 specification 


 ANDREW FREEMAN 


 HUGO VON HOFMANNSTHAL . MUSICAL TIMES.—May 1 , 1913 

 poor music . doubt past 
 musician far easily pleased 
 book offer . frequently , course , good 
 musician find great difficulty obtain text 
 suitable purpose . importance 
 good libretto recognise , 
 fact arise past experience . 
 * Euryanthe , ' ' Alfonso Estrella ' ' Rosamunde ' 
 suffer libretto find interesting 
 difficult . obvious , 
 position librettist important , 
 different composer , course , view matter 
 different way . Beethoven fastidious 
 subject sentiment actual 

 form . prolific ubiquitous Scribe 




 REFORM CHURCH MUSIC 


 17 


 , 1913 . 311 


 s 


 W. G. 


 


 312 


 1913 


 3 5 


 


 13 


 1913 . 3 


 recital . 


 ORGANIST CHOIRMASTER appointment 


 MUSICAL t 


 , 1913 . 315 


 book receive , 


 ' PASSION ' CHORALE 


 editor ' MUSICAL TIMES . ' 


 


 316 


 ARTHUR T. FROGGATT 


 — 12 101 editor ' MUSICAL TIMES . ' 


 SE 


 _ — _ — _ 


 FRANCIS 


 KORBAY . 


 DVORAK ' STABAT MATER 


 MI 


 00 


 MR . BALFOUR GARDINER CON ERTS , QUEEN HALL 


 ROYAL CHORAL SOCIETY 


 LONDON CHORAL SOCIETYQUEEN HALL SYMPHONY CONCERTS 

 noteworthy concert series — 
 series — place recently , 
 Queen Hall Orchestra April 5 , choir 
 Birmingham Festival come London 
 performance Beethoven Choral Symphony work 
 Bach . choir good Birmingham 
 produce , reputation precede London 
 fully sustain visit . brilliant per- 
 formance finale Symphony seldom 
 hear . Bach unaccompanied motet ' afraid , ' 
 singer tone - power , exhilarating attack , 
 buoyancy , earn high praise 
 Birmingham Festival Festival 
 Incorporated Society Musicians . choir 
 hear Bach ' God time good , ' orchestral 
 accompaniment adapt Mr. Van der Stucken . 
 solo symphony sing Miss Carrie Tubb , 
 Miss Gwladys Roberts , Mr. Gwynne Davies , Mr. 
 Herbert Heyner , manner indicate special 
 preparation . Sir Henry Wood conduct 
 mastery , occasion tribute 
 ability Mr. R. H. Wilson , chorus - master 

 conclude concert season , April 
 19 , M. Godowsky form centre attraction . 
 reading Brahms B flat pianoforte concerto 
 great quality brilliance intellectuality 
 expect . playing fascinating 
 certainty authority , equally fascinating 
 observe subtle skill Sir Henry Wood 
 orchestra accompany . Symphony occasion 
 Beethoven seventh , interpret manner 
 - weigh interesting . dance 
 Beethoven , arrange Herr Steinbach , Strauss 
 ' Till Eulenspiegel , ' complete programme 

 annual Endowment Fund concert place 
 April 12 large audience . Lady Speyer Mr. 
 Mark Hambourg soloist , purely orchestral 
 number programme choose Wagner 




 NEW SYMPHONY ORCHESTRA 


 320 


 , 1913 


 AMATEUR ORCHESTRASThe Stock Exchange Orchestral Society , conduct 
 Mr. George Kitchin , concert Queen Hall 
 April 10 . excellent performance Humperdinck 
 ' Hansel und Gretel ' Overture , Haydn ' military ' Sym- 
 phony G , Berlioz hungarian march , abundant 
 pleasure large audience . male - voice choir , 
 Mr. Frank Idle , responsible good glee - singing . 
 soloist occasion Miss Phyllis Lett 

 April 17 , Mr. Joseph Ivimey ably conduct concert 
 Strolling Players ’ Orchestral Society Queen Hall , 
 provide , good thing , attractive 
 extremely efficient performance Beethoven eighth 
 Symphony . solo music provide Miss Irene Scharrer 
 ( pianist ) Mr. George Baker ( vocalist 

 - sixth concert North London Orchestral 
 Society place Queen Hall April 18 , 
 inspiring careful guidance Mr. Lennox Clayton 
 decide success . feature evening 
 performance Dvorak ' New world ' Symphony . Bach 
 bass cantata ' Ich den Kreuzstab ' sing 
 Mr. Robert Maitland Twickenham Philharmonic 
 Society . Miss Winifred Christie ( pianist ) 
 soloist 




 recital 


 CHAMBER CONCERTS 


 SE 


 ANTHEM , " LORD , THOU ART GOD 


 100 


 — — $ — — — — — S TW 


 SSS 


 SS 


 S — _ — — ~- = 


 = f _ = 


 4 — — — — e 


 SSS _ SS 


 LAN 


 LORD GOD 


 


 


 LORD GOD 


 HH 


 HAN 


 HH 


 , 


 . J P 


 1 , 1913 


 ( correspondent . ) 


 BELFAST 


 AV 


 , 1913 . 329 


 BIRMINGHAMAskey , organist Mr. C. W. Perkins 

 Birmingham Chamber Concerts Society hold 
 musical function season ( Queen College 
 March 18 , executant Catterall 
 String Quartet . perfect unanimity artistic expression 
 characterize playing Beethoven string quartet 
 e flat , Op . 130 , composer String quintet 
 c major , Op . 29 , Mr. C. A. Butler occupy 
 lesk second viola 

 customary terminal concert provide orchestra 
 Midland Institute School Music 
 lage Lecture Theatre March 17 , able 
 direction Professor Granville Bantock . 
 delightful listen Haydn fourth ' Salomon ' 
 ( know ' Paukenwirbel ' ) Symphony , 
 performance distinct merit . interest 
 short Concerto G minor , . 1 , string , 
 Charles Avison ( 1710 - 70 ) , score Professor 
 Bantock buy second - hand bookshop Charing Cross 
 Road , shilling . solidity variety 
 structure , work decided merit 
 glad hear , interpretation wonderfully clear 
 accent , phrasing , rhythm , sonorous tone . 
 Ippolitov Ivanoff ' armenian rhapsody ' _ national 
 theme , Op . 78 , score orchestra , important 
 violin solo , excellently play Miss Zoé 
 M. Wadely , leader orchestra , prove 
 novelty , picturesque performance realise 
 orchestra . student School , William Fenney , figure 
 composer conductor ' Pastorale ' orchestra , 
 considerable inventive power , talent 
 achestral colouring . vocal interlude Bach 
 intata soprano solo orchestra , ' non sa che sia 
 dolore , ' score string , flute , ' cembalo , probably 
 wntten Leipsic 1723 vocalist 
 Miss Eva Badger 

 f Mascagni ' Cavalleria Rusticana ' Town Hall 
 April 5 , occasion series 
 oe provide season musical organization . 
 choir sing remarkably , orchestra 
 good form 

 Birmingham Symphony Orchestra special 
 concert Town Hall April 6 , Mr. Julian 
 Clifford , conductor , 
 Orchestra , arrange excellent programme , include 
 Beethoven overture ' Leonore ' . 3 , Glazounoff 
 symphony . 6 . special interest 
 ‘ pearance England russian pianist Miss Maria 

 vinskaya . splendid performance Liszt e flat 




 BOURNEMOUTH . week , Symphonies perform . 4 

 Beethoven , Tchaikovsky ' Pathétique , ' Dvordk ' new 
 World , ' symphony E flat Glazounoff , 
 symphony Malischewsky ( performance 
 England ) . soloist concert include Miss 
 Rita Neve ( Holbrooke poem pianoforte orchestra , 
 ' Song Gwyn ap Nudd , ' ) Miss Dorothy Bridson ( 
 performance Violin concerto Arnold Trowell , 
 conduct composer ) , Mr. Johan Wysman ( Busoni 
 orchestral arrangement Liszt ' spanish rkhapsody ' 
 pianoforte ) , Miss Jacoba Wolters , Municipal 
 Orchestra ( Debussy ' Danse Sacrée et Danse Profane ' 
 harp orchestra ) , Mr. Arnold Trowell ( Saint - Saéns 
 ’ Cello concerto . 2 ) . notice 
 certain predominance modern work , _ 
 feature extend , slightly degree , 
 small composition , interesting 
 Weber ' Oberon ' Overture , Strauss tone - poem ' Tod 
 und Verklirung , ' Good Friday music ' Parsifal ' 
 ( Wagner ) , Mendelssohn ' Ruy Blas ' Overture , Debussy 
 prelude ' L’Aprés - midi d’un Faune . ' April 3 , Mr. 
 Granville Bantock visit , conduct performance 
 ' overture greek tragedy , ' serenade 
 string entitle ' Far West 

 eclecticism , , distinguish programme 
 Monday ' Pops . , ' charge partiality 
 particular school thought uphold . 
 March 12 , programme devote Mendelssohn . 
 March Wagner . - fifth concert 
 illustrate ' Evolution Symphony . ' british music 
 subject follow week concert , 
 unrepresentative programme , eminent 
 composer orchestral work draw 
 Elgar , Mackenzie , Sullivan , item 
 hardly - class importance . Dr. Charles 
 Maclean conduct tone - poem ' Fanni’s‘Fest - tag , ' 
 Mr. Robert Chignell , addition conduct ' Romeo 
 Juliet ' prelude , sing Vaughan Williams fine song 
 ' Vagabond , ' ' bright ring word , ' 
 remain item variation Dr. H. Holloway , 
 chorus - master Bournemouth Municipal Choir . 
 example chamber - music perform concert 
 Rubinstein Sonata viola pianoforte , play 
 Messrs. Mauritz Speelman Montague Birch , member 
 orchestra , Bralhms Sonata pianoforte ’ cello 
 E minor , perform Miss Edith Leah , 
 local artist , Mr. S. Coélho , professor violoncello 
 Bournemouth School Music 




 330 musical 


 time 


 1913 


 BRISTOL DISTRICT 


 DEVON CORNWALL 


 DEVONSHIRE TOWNS 


 


 1913 . 331Curtis 

 ogramme excellently 
 alien associate band 
 pianoforte orchestra , Rimsky - Korsakoff . 
 wartet Borodine , Mozart , Grainger , play 
 Exmouth April 2 Misses Fell , 
 \gnes Ramsbotham , Dorothy Jones , Kosa Button , 
 vocal piece Mrs. Arthur Bird instrumental 
 solo . Mr. Walter Twining concert 
 Torquay Pavilion April 3 , programme consist 
 f vocal solo concerted piece . Madame Blanche 
 Marchesi Mr. Mark Hambourg visit Exeter 
 April 4 April 9 , Paignton , Mr. H. M. Walbrook 
 London ) lecture ' Robert Clara Schumann . ' 
 Miss Pine Coppin vocal recital Exeter April 9 , 
 Sidmouth , April 14 , Miss Dorothy Holden 
 nvince evidence pianistic gift . - 
 ncert Haydn String ( Quartet Torquay , April io , 
 nclude Haydn Op . 76 , . 3 , Beethoven Op . 18 , 
 . 3 . 
 Knight Bruce ( violin ) , classical programme Exeter 
 April 11 , artist prove highly gifted 

 play . Madame Miguel 




 WAGNER FESTIVAL TORQUAY 


 CORNWALL 


 DUBLIN 


 EDINBURGH 


 musicalable concert favour . similar help Rotherham singer . 

 amateur orchestral Societies — Beethoven | singing possess attribute verve , das 
 Withington — close - fifth twentieth ] colouring , Mr. Thomas _ Brameld 

 season , claim notice month . year laboriously cultivate . great range 

 serve introduce composition local musician . , - ; 
 f ness ensemble . result beautiful 

 doomed Temple , ' dramatic scena , setting 
 Mr. McConnell Wood word Dr. Hemy , choir , 
 organ , pianoforte , produce composer 
 concert Northumbrian Choir Saturday 
 Easter . Mrs. Meredith represent work 
 similar combination , remainder programme 
 fill song . April 3 , Jarrow Philharmonic 
 Society produce cantatilla , * ' Eastward , ' Mr. Alfred 
 Wall , know principal violin 
 local choral orchestral concert , asa fine soloist . 
 public recently writer , 
 young day composition appear 
 important London programme . work question 
 display musicianly skill contain interest . 
 * Hiawatha departure ' sing , rest 
 programme miscellaneous character . soloist 
 Miss Dorothy Silk , Messrs. J. Cheetham R. R. 
 ‘ larke . Mr. G. Dodds conduct usual ability 
 care , secure good choral performance . Tyne- 
 mouth , Whitley , District Choral Union conclude 
 season April 2 miscellaneous programme 
 consist Handel ' sixth Chandos Anthem , ' Brahms 
 * Nanie , ' Parry ' Blest Pair Sirens , ' Elgar ' | 
 sweet music , ' song Beethoven , Grieg , Bantock , 
 Vaughan Williams , represent 
 lovely * ' mystical Songs ' baritone solo , chorus , 
 orchestra . vocalist Miss Dorothy Silk , 
 Messrs. A. Heather H. Brown 

 enterprising operatic impresario , Mr. T. Quinlan , 
 preface colonial tour * Ring ' provincial | 
 performance , Newcastle , 
 rehearsal place Tynemouth . 
 Trilogy stage , considerable 
 interest arouse , study discussion 
 Wagner progress musical circle . Mr. N. Kilburn , 
 Friday , April i1 , , crowded interested 
 audience , revise version lecture deliver 




 1913 . 335 


 YORKSHIREMiss 

 Judson vocalist , introduce Beethoven 
 setting scottish song , original accompaniment 

 pianoforte , violin , violoncello 

 Harrogate , ' season ' begin March 20 , 

 continue till October . usual , Symphony concert 
 Wednesday afternoon , Mr. Julian Clifford bring toa 
 hear quantity interesting orchestral music . 
 Glazounoff fourth Symphony , e flat , 
 Beethoven C minor Symphony , Schubert ' unfinished , ' 
 Concertos play Miss Annie Godfrey 
 ( Saint - Saéns ' Rondo Capriccioso ' violin ) , Mr. James 

 Mr. Isidore Epstein ( Paderewski ' Polish Fantasia ' ) . 
 April 2 , Dr. Markham Lee entertaining lecture 
 ' Tchaikovsky , ' Messrs. Clifford , J. Lawson , Messeas 
 play composer ' Elegiac ' pianoforte trio way 
 illustration . visit Imperial Russian Ballet 




 BRIEFLY summarize 


 336 MUSICAL TIMES.—May 1 , 1913 

 CANTERBURY.—An exceptionally - choose programme 
 submit East Kent Orchestra April 17 , 
 capable direction Mr. Percy Godfrey . 
 Beethoven eighth Symphony , Sibelius ' valse triste , ' 
 selection ' Die Meistersinger , ' Mr. Godfrey 
 ' Vaudeville l’espiégle , ' orchestral number , 
 adequately perform orchestra - , 
 include contingent band East 

 Kent Yeomanry . interesting selection song 

 JOHANNESBURG . 
 Choral Orchestral Society March 12 

 Blodwen Hopkins soloist , attractive readi 
 Allegro moderato Beethoven ' Emperor 

 fe aal 
 performance excerpt ' Lohengrin , 

 y 
 e Piper , ' 
 rogramme 

 include delightful performance orchestra 
 Beethoven ' Egmont ' Rossini ' Gazza ladra ' overture , 
 cello soli Mr. J. Richardson , song Messrs. H 

 leservedly 
 forte 

 e Nozze ’ 
 work 
 Mr. 

 Rosenthal play Beethoven Pianoforte concerto 

 lucte 




 3 . h. 


 1913 . 337 


 MILFORD - - seawork programme 

 Beethoven ' Waldstein ' sonata . 
 St. HELENs.—Rossini * Stabat Mater ' recently 
 perform success Choral Society 

 direction Mr. T 

 March 31 , conductorship Mr. G. A. Boulter , 
 large audience . chief soloist 
 Miss Mabel Whitehouse Mr. George Foxon , 
 sing splendidly , chorus 
 admirably . Mr. Rowsby Woof ably lead orchestra , 
 play violin solo fine effect 
 miscellaneous programme follow cantata . 
 concert prove decided artistic financial success , 
 place status Society question 

 SWANSEA.—A programme unusualinterestwas present 
 Swansea District Male Choir April 21 
 Albert Hall , Swansea . special feature Mendelssohn 
 ' Antigone , ' perform assistance 
 Queen Hall Orchestra , direction Sir Henry 
 Wood . influence distinguished conductor 
 feel performance . version 
 play abridge concert performance Mr. Charles Fry , 
 recite great dramatic power , Mr. Emil 
 Leslie display voice great sympathy _ tender- 
 ness . bass solo sing Mr. Llewellyn Bowen , 
 train choir skill patience . 
 programme include Beethoven C minor Symphony . 
 choir hear great advantage chorus , 
 ' Nun Nidaros , ' Dr. Daniel Protheroe , 
 display splendid tone power . concert 
 series South Wales Musical Festival , 
 remainder describe issue 

 TorontTo.—Much fame esteem lately accrue 
 National Chorus ( conduct Dr. Albert Ham ) 
 success concert Toronto Bufialo 
 conjunction New York Symphony Orchestra . 
 fine quality tone expressive power achieve 
 choir 200 voice inspiration Dr. Ham 
 knowledge insight choral matter con- 
 spicuously interpretation Coleridge - Taylor 
 * Sea - drift , ' Max Bruch ' morning song praise , ' 
 Parry ' Blest pair Sirens , ' earn universal 
 admiration 




 8 musical 


 TIMES 


 AUGSBURG , 


 BARMEN . 


 122 


 22 


 BERLIN , 


 ALTENBURG 


 ANTWERP 


 BASEL 


 BOSTON 


 BREMEN 


 BRESLAU 


 BRUNSWICK 


 BRUSSELS 


 SUCCESS 


 


 , 1913 . 339 


 bu DA - VPEST 


 CASSEL . 


 CHICAGO 


 CAIRO 


 COLOGNE 


 DORTMUND . 


 DESSAU 


 DRESDEN 


 DUSSELDORF 


 FRANKFURT 


 GORLITZ 


 GRAZ 


 HAGEN 


 HALLE 


 HAMBURG 


 HANOVER 


 HEIDELBERG 


 KARLSRUHEKIEL 

 Beethoven rarely hear ' trauerkantate ' sing 
 Philharmonischer Chor ( conductor , Dr. Mayer - Reinach 

 Brahms ' Niinie , ' cantata Julius Weismann , 
 Bach cantata ' Weichet nur betriibte Schatten , ' 
 submit Kieler Gesangverein 




 KONIGSBERG 


 LEGHORN 


 LEIPSIC 


 MUSICAL T 


 340 


 LIEGE 


 LYONS 


 MADRID 


 MAG DEBU RG 


 MANNHEIM 


 MAYENCE 


 MONTE CARLO 


 MULHAUSEN . 


 MUNICH 


 NEW YORK 


 MARSEILLES . MEININGEN 

 musical Festival place April 1 - 3 . 
 | orchestral chamber - music concert , 
 work Bach , Mozart , Beethoven , Brahms , 
 | Bruckner , Max Reger ( conduct Festival ) 
 | represent programme follow : choral 
 work , ' Die Nonnen , ' ' Concerto olden style , ' 
 | variation fugue theme Johann Adam THliller , 
 | string quartet e flat ( op . 109 ) , Sonata pianoforte 
 j}and clarinet ( op . 86 ) , variation fugue 
 | theme Beethoven pianoforte 

 MILAN 




 NANTES 


 NICE 


 NORDHAUSEN 


 NURNBERG 


 PARIS 


 PRAGUE , 


 PRESSBURG 


 ROME 


 MUSICAL 


 1913 . 341 


 ROUEN . 


 SAARBRUCKEN 


 ST . PETERSBURG 


 TURIN 


 VIENNA 


 WARSAW , 


 CRAYON . 


 MUSICAL time 


 


 scale term advertisement . 


 342 MUSICAL TIM 


 dure 


 month . | 


 OVELLO & CO . , LIMITED 


 dure month 


 AUGHT 


 YARRY , C. H. H.—**G 


 publish 


 H. GRAY CO . , NEW YOR 


 ROCKWAY , 


 JOOD , D. D. 


 UGHT , 


 remarkable work 


 


 MUSIC education 


 WRITTEN B Y F " FAMOU S expert 


 PADEREWSKI , 


 MARK HAMBOURG , 


 CARUSO , 


 CLARA BUTT , 


 EDWIN H. LEMARE , 


 MADAME MARCHESI , 


 JOHN DUNN 


 value teacher 


 FREE inquiry . 


 remarkable 


 work 


 music lover 


 story work 


 great COMPOSERSThe original score reproduce 

 work greatly enrich inclusion 
 example facsimile manuscript great composer 
 immortal work . find example 
 original score Handel ' * Messiah , " Bach Prelude 
 massive choral , Beethoven tender Sonatas , 
 Chopin Prelude D , weird melody Dvorak , 
 Haydn original manuscript austrian hymn , 
 Gounod ' * ' Romeo Juliet , " " Mendelssohn Melodies 
 leave hand great composer , host 
 ; short , mass rare 
 hitherto unpublished manuscript bring 
 compass single work . 
 include facsimile music Auber , Bach , Balfe , 
 Beethoven , Bellini , Bizet , Boieldieu , Brahms , Bruch , 
 Cherubini , Donizetti , Dvorak , Franz , Gade , Gluck , Gounod , 
 Grétry , Grieg , Halévy , Handel , Haydn , Herold , Liszt , 
 Marschner , Massenet , Mendelssohn , Meyerbeer , Mozart , 
 Pergolesi , Purcell , Raff , Rameau , Rheinberger , Rossini , 
 Rubinstein , Saint - Saéns , Scarlatti , Schubert , Schumann , 
 Sgambati , Spohr , Spontini , Strauss , Sullivan , Thomas , 
 Tschaikowsky , Verdi , Wagner , Weber , Xc 

 criticism 




 FREE INOQUTRY . 


 content 


 music : 


 special notice 


 THURSDAY , 22 


 IE . B 


 1913 


 ASSOCIATE examination 


 fellowship examination 


 STUDY 


 LESCHETIZKY method 


 PIANOFORTE- playing 


 pianist 


 


 great 


 DEVELOP 


 


 APPLEYARD , 


 STS . 


 RCO 


 


 


 anthem 


 trinitytide 


 " lute 


 complete list 


 serie 


 NOVELLO 


 hymn 


 tune 


 J. STAINER 


 A. CHALLINOR 


 J. STAINER 


 ALBERTO RANDEGGER 


 ALFRED MOFFAT 


 J. STAINER 


 . .. J. STAINER 


 A. SULLIVAN 


 F , . WESTLAKE 


 J. STAINER 


 J. STAINER 


 EVENING CANTICLES 


 NUNC DIMITTIS 


 ANTHEM 


 WILLIAM BYRD 


 RAIN COMETH , " 


 MANUAL PLAINSONG 


 canticle note 


 psalter note 


 GREGORIAN tone 


 litany response 


 new edition 


 prepare 


 GENERAL SUPERINTENDENCE 


 JOHN STAINER 


 extract introduction 


 PREFACE 


 G2 


 MUSICAL TIMES 


 , 1913 


 347 


 composition 


 


 ORGAN MUSIC . 


 POPULAR CHURCH MUSIC 


 H. MAUNDER 


 service , 


 ORGAN . 


 CHURCH CANTATA , 


 ORGANS TUNING 


 


 song 


 vocal composition 


 word MACKAY 1001 gem 


 SIXPENCE 


 action 


 month 


 vocal duet . 


 SONGS 


 " bird 


 TRIOS 


 vocal 


 legend 


 WOOD 


 JUVENILE OPERETTA UNISON singing 


 ELFIN HILI 


 OPERETTA UNISON singing . 


 toiler DEEP 


 CANTATA FEMALE voice 


 SEVEN 


 


 dedicate ( permission ) 


 right HON . EARL MEATH , P.C. , K.P 


 hymn EMPIRE day 


 short form service use 


 school church 


 price threepence . 


 content 


 author . COM POSER , 


 form service . NATIONAL ANTHEM 


 LONDON : NOVELLO COMPANY , limited 


 hymn EMPIRE day . 


 RECESSIONAL 


 GOD father , know old 


 UNISON SONG , action GEORGE C. MARTIN . 


 ALFRED R. GAUL . UNTO THEE 


 word 


 1913 


 


 POINTED 


 


 publish 


 BARLESS PSALTER 


 use 


 ANGLICAN 


 chant 


 EASY book choir congregation 


 containing 


 


 


 psalm 


 DAVID 


 canticle proper psalm 


 


 SHORT note 


 SEYMOUR 


 LONDON : NOVELLO 


 ATHANASIAN 


 


 text & explanatory preface 


 edit 


 WALTER MARSHALL 


 LATE 


 SHORT PRELUDES 


 


 ORGAN 


 SE 


 


 PILE , M.A 


 


 COMPANY 


 NEW 


 CREED 


 F.R.C.O 


 LIMITED 


 REVISED edition 


 ORGAN 


 JOHN STAINER 


 edit JOHN E. WEST 


 349 


 publish 


 month 


 sketch PIANOFORTE 


 


 FREDERIC H. COWEN 


 content 


 issue net book . | NEW EDITION ( 19:2 


 TECHNIQUE HANDBOOK EXAMINATIONS 


 


 PIANOFORTE playing | ERNEST ' A. DICKS 


 | — _ — _ — ' 


 


 NOVELLO OCTAVO edition - song 


 select list recently publish - song 


 mix voice ( 5 


 FEMALE voice 


 male . voice ( TTEBB 


 NEW song song 


 = — drover . 


 RACHRAY MAN 


 STRANGER grave . 


 CLARENCE S. HILL . 1 . 


 V. W. PEARSON 


 DESCRIPTIVE chorus . 11 


 heart . ' > 47 


 ment 


 i9t3 . 353 


 new serie 


 SACRED song 


 edit , 


 mark expression phrasing , 


 ALBERTO RANDEGGER 


 PRICE shilling 


 net book 


 set 


 SOPRANO 


 CONTRALTO 


 TENOR 


 BASS 


 SOPRANO 


 CONTRALTO4 glory God nature ( creation hymn 

 Beethoven 
 § . fac ut portem ( " Stabat Mater " ) .. G. Rossini 
 § . morning Prayer ( * * Eli " ) M. Costa 

 NOVELLO COMPANY , LiMiTED 




 second set 


 TENOR1 . , wait thou leisure 
 ( * * 1f thou sufferest " ) J. S. Bach 
 2 . daughter Jerusalem ( ' ' St. Peter " ) J. Benedict 
 3 . sun ( " ' Samson " ) ... Handel 

 4 . o come , let worship ( " ' Psalm xcv . " ) 
 F. Mendelssohn - Bartholdy 
 5 . twilight gently fall ( Ave Maria ) ... J. Raft 
 6 . song penitence ( busslie ) Beethoven 

 BASS 

 F. Schubert 

 Beethoven 

 5 . consume ( * ' St. Paul 




 , 1913 


 MICE council 


 CANTATA child 


 SHAPCOTT WENSLEY 


 singing leave 


 POEM 


 


 JAMES RUSSELL LOWELL . 


 


 GEORGE RATHBONE 


 NOVELLO | 


 classical song 


 content 


 invocation 


 ORCHESTRA 


 compose 


 MACKENZIE . 


 A. G 


 arrangement 


 VIOLIN PIANOFORTE , 


 composer 


 EASY piece 


 PIANOFORTE 


 


 VIOLIN 


 C. H. LLOYD 


 characteristic PIECE 


 VIOLIN PIANOFORTE 


 melodious technique 


 compose 


 J. A. O'NEILL 


 355 


 original composition 


 


 ORGAN . 


 AAA 


 NOVELLO 


 album 


 N select 


 ORGAN 


 piece 


 piece 


 original composition 


 


 ORGAN 


 compose 


 ALFRED HOLLINS 


 original composition 


 


 ORGAN 


 compose 


 W. WOLSTENHOLME 


 composition ORGAN 


 SIGFRID KARG - ELERT 


 CHACONNE FUGUE TRILOGY , 


 SOLEMN MELODY 


 compose 


 H. WALFORD DAVIES . 


 ORCHESTRA . ' 


 STRINGS ORGAN 


 VIOLONCELLO PIANOFORTE 


 350 


 1913 


 COUNTRY dance tune 


 collect arrange PIANOFORTE 


 CECIL J. SHARP . 


 COUNTRY dance book 


 edit 


 CECIL J. SHARP 


 SHORT EASY PIECES 


 pianoforte 


 


 CLEMENT M. SPURLING . 


 content 


 PET ITE . VALSE 


 J. HOLLMAN 


 arrange PIANOFORTE SOLO 


 JOSE VARGAS - NUNEZ 


 ALLEGRETTO 


 WOLSTENHOLME 


 " TNT 


 LA SAVANNAH 


 AIR DE BALLET 


 ORCHESTRA . 


 compose 


 A. C. MACKENZIE 


 TW O BAGATELLES 


 I. VALSETTE . 


 


 STRING ORCHESTRA . 


 compose 


 PERCY E. FLETCHER . , 


 20 


 AUF WIEDERSEHEN 


 compose 


 A. HERBERT BREWER 


 VIOLONCELLO PIANOFORTE 


 EL 


 12 , 


 14 . * 


 15 . 


 16 , 


 18 


 19 . 


 20 , 


 21 . 


 22 . 


 4 f 


 25 . ° 


 6 b 


 2 . b 


 # 4 . SI 


 2 6 


 reerbe 


 progressive study 


 pianoforte 


 edit , arrange group , fingering revise supplement 


 FRANKLIN TAYLOR 


 - book , price shilling 


 


 select pianoforte study 


 progressively arrange 


 FRANKLIN TAYLOR . 


 set ( book ) , PRICE shilling sixpence book 


 35 


 , 1913 


 english lyric 


 SET music 


 set 


 C. HUBERT H. PARRY 


 CANTIONES SACRA 


 second set . 


 set . 


 fourth set . 


 fifth set . 


 sixth set . 


 seventh set . 


 eighth set . 


 ninth set . 


 musical setting 


 ROMAN LITURGY 


 edit 


 RRRRREE 


 , 1913 


 359 


 CHOIRBOY GUIDE 


 CATHEDRAL PSALTER . 


 edit arrange 


 ERNEST N EWTON , M.A 


 PRECEPT PRACTICE 


 


 SINGING class student 


 J. A. MOONIE 


 NEW FOREIGN 


 PIANOFORTE MUSIC . 


 P.P.M. , P.C. ; 10 . ¢ 


 F. SE - ' 


 ORGAN HARMONIUM 


 BACH , J. S. 


 ELERT 


 VOCAL MUSIC . 


 MINIATURE score . 


 literature 


 publication 


 net . 


 12 ¢ 


 20 « 


 20 


 5 0 


 2 6 


 1 0 ° 


 10 © 


 2 6 


 1 6 


 3 6 


 1 6 


 1 6 


 20 


 


 AUGUST W ILHELMJ } BROWN 


 SECTION a.—technical practice . book 


 SECTION b.—studie . book 


 CLOTH binding 


 piece . 


 RRARSR 


 extra supplement . 


 NOVELLO - song 


 collection 


 139 


 140 


 a.z 


 157 


 book 


 RN 


 SPRL RRRLKRB 


 559 


 356 


 SES SRESGRRENow fie love ... G. A. Macfarren 14d 

 wind mn ! Chas , Oberthir 2d . 
 softly fallthe shade ... _ E. Silas ad . 
 love little , love long L , Wilson 2d . 
 shall tell love Wesley 3d . 
 lover lass f : Booth rad 
 Love question reply J.B. Grant ad . 
 , loathe : nelancholy(5v ) Lahee 4d 
 Song ... E , M , Hill 3d . 
 welcome dawnof ummer ‘ sday , 3d . 
 Charge Light Brigade Hecht 4d . 
 beauty mountain Goss 14 
 o sweet Mary ( 5 v. ) 4d . 
 lo ! rosy - bosom ‘ dhour | » 4d . 
 43 Rarares glow - worm 4d . 
 32 bell St. Michael Stewart 4d . 
 3 Cruiskeen Lawn ( 5 vy ) - 3d . 
 wi s circlit 3d . 
 ye mar tngland H. Pierson 1 
 Vesper Hymn ... Beethoven 2d . 
 sorrow ... Naumann 2d . 
 Swallows ... oss Pohlentz 2d . 
 Hope Faith coe Weber 2d . 
 Hark , hark , Lark ... Kiicken 3d . 
 walk dawn ... Gade 3d 

 winterday .. , A. J. Caldicott 4d . 
 homeward exe Henry Leslie 4d . 
 sea ! thec calm o ° er Marshall qd 




 2 > 22 09 


 ( second serie 


 GOSLINGS 


 humorous - song S.A.T.B 


 word write F. E. WEATHERLEY , M.A. 


 SIR FREDERICK BRIDGE , 


 C.V.O. 


 GOSLINGS 


 SS 


 — — — — = — — — = = = = = — FH 


 PE , 


 GOSLINGS 


 2S = : S 5 SSS SS SSS SS 


 GOSLINGS 


 7 } } ; 7 J 


 { ° . ' _ _ = = . 


 ZN 


 SS SS 


 4 7 


 VY 


 — _ — _ 


 _ — ¢j — = _ _ | _ — — — + — - _ + — $ $ — — — — — — . = . 


 GOSLINGS 


 VY ~/ 


 = = = = 22 2255 - 5 s = = = 


 . . 7 _ . J 


 — — — = SS 


 CIN 


 SSNS 868 


 SBN 


 GOSLINGS 


 SS SESS _ . 


 . - . . C — O 


 SANFORD TERRY 


 CONTR 


 STRONG