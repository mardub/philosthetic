


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 PUBLISHED MONTH 


 BRISTOL MUSICAL FESTIVAL , 


 OCTOBER 21 , 22 , 23 , 24 , 1873 


 PRINCIPAL VOCALISTS 


 MADAME LEMMENS - SHERRINGTON 


 MADAME PATEY - MISS ENRIQUEZ 


 MR . EDWARD : LLOYD - MR . LEWIS THOMAS 


 WORKS PERFORMED 


 CREATION , ELIJAH , MESSIAH , 


 HYMN PRAISE , STABAT MATER 


 MR . CHARLES HALLE ORCHESTRA 


 ™ | CONDUCTOR MR . CHARLES HALLE 


 OW . TICKETS PROGRAMMES SHORTLY ISSUED 


 MR . W. H. JUDE , 


 MR . J. TILLEARD , 


 USIC ENGRAVED , PRINTED , PUB- 


 RUSSELL MUSICAL INSTRUMENTS . 


 ROGER CELEBRATED VIOLIN STRINGS , R 


 FIFE 


 INGS , 


 BATES 


 _ — — _ 


 O 


 ISTS 


 199 


 AZ 


 QUARTERLY SALE MUSICAL PROPERTY . 


 HERTFORD . — FURNITURE , MUSICAL INSTRUMENTS , 


 RGAN , PIANOFORTE , HARMONIUM , HAR 


 ANGLICAN 


 PSALI EK 


 CHANTS 


 SINGLE DOUBLE 


 EDITED 


 


 


 EDWIN GEORGE MONK 


 TONIC SOL - FA EDITION . 


 READY . 


 INTERVALS COUNTERPOINT . ” 


 MANUAL SINGING 


 USE CHOIR TRAINERS & SCHOOLMASTERS , 


 - SECOND EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT SINGING MANUAL 


 ( COMPANION WORK 


 COLLEGIATE VOCAL TUTOR 


 COLLEGIATE SOLFEGGI 


 CATHEDRAL CHANT BOOK 


 CHORAL SOCIETIES . 


 \W H. BIRCH POPULAR OPERETTAS , 


 ARR 


 ARN 


 USEI 


 ALLC 


 ACF 


 = O L 


 AYE 


 ERV . 


 LARK 


 HOR ) 


 TEGG 


 EETC 


 TERS 


 OOL 


 201 


 HARVEST ANTHEMS 


 HYMNS TUNES 


 HARVEST 


 SELECTED 


 HYMNARY 


 PRICE PENNY 


 TTAS , 


 ‘ OLORD , HEAVEN THY POWER J 


 ER , 


 ( OD , FATHER , CREA- 


 T 


 10 , SUMMER COMES 


 KING . GERMAN 


 SUMMER ENDED , HARVEST O'ER 


 METCALFE 


 C. JEFFERYS , 67 , BERNERS ST 


 NEW PUBLICATIONS . 


 MICHAEL WATSON 


 DIVINE TREASURES 


 LYRIC STAGEMarch ee 3 0 
 Mlustrated Portrait eGebsilik : 
 LO UIs DUPUIS . 
 CATHEDRAL GEMS : — 
 . 16 . Lincotn.—On weniete Haydn 
 Mass aad 3 0 
 17 . Ery.—On eibbacts “ Mozart ? s ‘ ica 
 Mass a3 ate % es ‘ Sea 
 GRAND OPERATIC DUETS . 
 Arranged 
 . 1.—Lohengrin ( Wagner ) J. Rummel 5 0o 
 2.—Tannhiuser ( . ) a7 5 0 
 3.—The Crown Diamonds Wz : pa 5 0 
 4-—Galathée pA 4 0 
 5-—La Marquise ibelenaes ) 4 0 
 6.—Ernani ( Verdi ) ss ae J. Lesage 5 oO 
 7.—Il Don Giovanni ( Mozart ) ... ib 5 0 
 8.—La Gazza Ladra ( Rossini ) ... nd 5 0 
 Wiha PAPE . 
 Il Trovatore ( Verdi ) ... 4 
 Martha ( Flotow ) 4 0 
 Saladin Grand March 4 0 

 JOHN OWEN . 
 MELODIOUS MELODIES : — 
 Subjects selected Works Beethoven , 
 Mendelssohn , Schumann , Heller , & c. , arranged 
 Organ Harmonium . Price 2s . 6d . nett 

 ready . 
 SONGS CHILDREN . 
 Harmonized School purposes MIcHAEL WATSON . 
 Price Sixpence . 
 Selection consists popular Copyright subjects 




 LONDON : C. JEFFERYS , 


 BERNERS PIANOFORTE GALLERY , 


 67 , BERNERS STREET , W 


 202 


 OYAL ALBERT HALL CHORAL SOCIETY . 


 LL SAINTS ’ CHOIR SCHOOL , CLIFTON . — 


 O MUSIC TRADE.—A PIANOFORTE 


 GOODWIN 


 UNIVERSAL MUSIC LIBRARY 


 3 , DUNCAN PLACE , ) 


 LONDON , W.C 


 OT 


 LAYER 


 : NGAGE- 


 203 


 MUSICAL TIMES , 


 SINGING - CLASS CIRCULAR . 


 SEPTEMBER 1 , 1873 


 LONDON MUSICAL SEASON . 


 ARY 


 YUARE 


 RIOS , 


 ES , 


 204 


 205process attaining different things . 
 Assuredly 
 interesting , degree 
 depend method tact 
 teacher . carried 
 trouble , kind play ; acquirement 
 worthy musical knowledge 
 musical skill ‘ easy , ’ ignorant 
 misapprehension wilful misrepresentation . ” 
 observations taken heart 
 engaged popular musical instruction . 
 evident Mr. Hullah opposed Tonic 
 Sol - fa notation — ‘ “ ‘ Moveable ” 
 method altogether — examinations systems 
 conducted utmost fairness ; 
 Committee Council Education con- 
 gratulated having secured services 
 musician brings large 
 experience , zeal devotion cause 
 fail highest value 

 Daily Orchestral Performances , 
 given Royal Albert Hall connection 
 International Exhibition , , past month , 
 fully earned good wishes interested 
 advance musical art . great 
 masters amply represented performance 
 works Beethoven Eroica Mendelssohn 
 Italian Symphony , equally - known 
 compositions ; important 
 works young English composers hitherto unknown 
 produced . , 
 lacking originality design , remarkable 
 clever instrumentation , symphony Mr. Hamil- 
 ton Clarke best . sym- 
 phony Mr. Henry Gadsby , movement 
 particularly striking , calls especial 
 remark . young singers appeared , 
 Madlle . Arnim , Mr. Dalton , Mr. Melbourne 
 successful ; pianoforte pieces 
 ably performed Madlle . Le Brun , Miss 
 Marian Rock , Miss Jessie Morison ( promising young 
 pupil Mr. W. H. Holmes ) , especially . Mr. Franz 
 Rummel , clever interpretation Schumann Con- 
 certo minor calls special mention . Mr. Deich- 
 mann , absence Mr. Barnby , conducted 
 uniformly great care intelligence 

 death announced Rev. W. Mercer , Vicar 
 St. George , Sheffield , editor - known 
 Church Psalter Hymn Book 




 206 


 REVIEWS 


 AOR SW : ET TATE ATT 


 208 


 209 


 B. WILLIAMSs composer contrapuntal power great 
 advantage . subjects studied , treated 
 skill , movement forming 
 ( animated effective close duet , reflects great 

 credit composer . Overtures Auber 
 “ Zanetta , ” ’ Rossini ‘ ‘ Guillaume Tell , ’’ Weber ‘ * Der 
 Freischiitz , ” ” Beethoven ‘ ‘ Egmont , ” Rossini 
 “ Siege Corinth . ’’ ably arranged , 
 found exceedingly effective performance , 
 passages judiciously adapted 
 instruments 

 Smmpkin , MARSHALL Co 




 AR 


 210 


 = = = = = = SS SS 


 : 5 5 7 


 YT 


 T T ] PP ] : 


 TT 


 GOOD NIGHT , GOOD NIGHT , BELOVED 


 _ — _ 


 PT 


 215 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWSDupLey.—Very commendable performances masterpieces 
 Handel given different occasions Dudley Choral 
 Society , aid charitable institutions , having fully 
 established claim public favour , surprise 
 numerous fashionable audience assembled 
 Public Hall Tuesday , July 29th , listen fine Oratorio , 
 Samson . Considering short time Society engaged 
 work , execution respect satisfactory , 
 reflected credit Mr. Eyland , conductor , 
 earnestly exercised acknowledged capacity teaching . 
 overture elicited heartiest applause , accompaniments 
 thoroughly efficient . principal soprano 
 Mrs. Sutton , singing airs , ‘ ‘ Ye men Gaza ” “ Let 
 bright Seraphim , ” warmly appreciated . Miss Blower gave 
 contralto music taste , Mr. G. Mainwaring 
 highly successful bass . marked applause 
 gained Mr. Bywater , excellent voice , gave refined 
 artistic interpretation music allotted 
 character Samson . choruses rendered decision 
 intelligence 

 LIveRPOOoL.—The concert given 12th ult . Representa- 
 tive Prize Choir Philharmonic Hall , finest 
 choral performances heard Liverpool . Compositions 
 Mendelssohn , Schubert , Schumann , Beethoven , J. L. Hatton , Gibbons , 
 Sir John Goss , & c. , faultlessly sung choir , solo 
 parts excellently rendered Messrs. T. Foulkes , T Hughes , 
 Robinson . Mr. J. Sanders conducted skill , 
 warmest praise awarded Mr. W. H. Jude efficient 
 manner accompanied music pianoforte 

 MoRETONHAMPSTEAD.—An organ built erected 
 Cross Chapel , Banfield , Birmingham , considered 
 competent judges , complete beautiful 
 instrument size met England . 
 metal pipes - quarters pure tin , - quarter lead , 
 brilliant durable composition , rarely adopted 
 account expense . organ perfect respect , 
 worthy imitation , design workmanship 




 216 


 _ _ 


 MONTH 


 HEREFORD BRISTOL 


 MUSICAL FESTIVALS 


 1873 


 MESSRS . NOVELLO , EWER CO 


 & CO 


 IIRED 


 217 


 SECOND SERIES 


 ANGLICAN CHORAL SERVICE BOOK 


 USELEY MONK PSALTER 


 ADDITIONS 


 EV . T. HELMORE PLAIN SONG WORKS . 


 PSALTER , PROPER PSALMS , HYMNS , 


 ORDER HOLY COMMUNION . 


 218 


 SACRED MUSIC EDWARD LAWRANCE , 


 30 ANCIENT MODERN CHANTS , 


 A. MACFARREN - ANTHEMS 


 GOOD TEMPLARS ’ FESTIVAL SONG 


 FLOW’RY VALE 


 MADRIGAL VOICES 


 CONSTANTIUS FESTA , 


 CHEQUE BANK , LIMITED 


 COMMENCED BUSINESS OFFICES , 


 PALL MALL EAST 


 


 124 , CANNON STREET , E.C 


 ‘ TRUSTEES . 


 BANK ENGLAND . 


 WESTERN BRANCH BANK ENGLAND , 


 NATIONAL PROVINCIAL BANK ENGLAND . 


 CONSOLIDATED BANK , LIMITED . 


 NATIONAL BANK SCOTLAND . 


 ALLIANCE BANK , LIMITED 


 MANCHESTER SALFORD BANK . 


 MANCHESTER COUNTY BANK 


 219 


 READY . 


 NEW EDITION , CORRECTED FUNE 


 NOVELLO , EWER & C0 . 


 CONTAINING 


 SACRED MUSIC LATIN WORDS 


 GRATIS APPLICATION , SENT POST FREE STAMP 


 PUBLISHED , OCTAVO EDITION 


 HANDEL THEODORA 


 READY 


 HANDEL BELSHAZZAR 


 1873 


 SOUND LOUD TIMBREL 


 MELODY COMPOSED AVISON . 


 MOZART MASSES 


 HAYDN MASSES 


 PUBLISHED . 


 MESSIAH 


 FACSIMILE 


 AUTOGRAPH SCORE 


 PUBLISHED 


 RAISING LAZARUS 


 ORATORIO , 


 J. F. BARNETT 


 PUBLISHED 


 HAGAR 


 ORATORIO , 


 


 REV . SIR F. A. G. OUSELEY , BART . , M.A. , 


 NOVELLO , EWER CO . COMPLETE 


 UNIFORM EDITION 


 THIRTEEN - SONGS 


 PSYCHE : 


 DRAMATIC CANTATA 


 SOLO VOICES CHORUS . 


 MUSIC J. F. H. READ . 


 WILLIAM J. YOUNG 


 RI AVEYD 


 MARCHES 


 COMPOSERS , ARRANGED THEORGAN 

 Arranged s. d. 
 . Adam , Adolphe ... Marche Religieuse oo W.T. Best ... 2 0 
 Alexander , Alfred March swe ove 2 . 2 0 
 Beethoven + . Military , March , Posthumous W. t. Sask « 0 
 je + . Triumphal March ( Tarpeja ) os um 2 2 
 Po ee Grand March D ( Op . 45 ) ee mm 220 
 m= + Funeral March ( Op . 26 ) ” 2 0 
 + » March , ( Egmont ) .. ” 2 0 
 Best , W. T. os er 4 Festival , 
 major ... Ps « w 3 6 
 ‘ eco Wedding March ... eee ” o- 3 0 
 te « . Funeral March ... ond - aa 9 @ 
 Cherubini + » Marche Religieuse , composed 
 Coronation 
 Charles X. ae . HJ . Lincoln 1 3 
 Chopin ... oe Funeral March mceph 
 forte Sonata eee . W. T. Best ... 2 0 
 Calkin , J.B.   ... Marche Religieuse ... Se ae ae 
 * + . Festal March ss psd oi 2 0 
 Carter , William .... Processional March “ Placida " nee sow 20 
 ® George .. Grand Festival March . wal ow 8 € 
 Collin , Charles .... Marches Iat3s . ,2at2 6 

 Dussek ... « . Marcia Funébre , Sonata , 
 Op . 74 , hands W.J. Westbrook 




 MARCHES 


 


 PIANOFORTE SOLO 


 SSRBV BREA 


 30 


 40 


 GERMAN VOLKSLIEDER ALBUM 


 SONGS , 


 JOHN OXENFORD 


 MPR NONDRHN DON HHO NDNONYNHONNHNHRNNHN HHH HHH RDDRRDD 


 NOVELLO , EWER & CO.’"S COLLECTION 


 SCHUMANN SONGS 


 ENGLISH GERMAN WORDS 


 NHHNNHNNNHNWHHNDHWNHNHNDNNNHWHNDHNKNDKDW YD 


 CAMOOANTDCCOADPOCSCOCOADOCONDOOAAGO 


 NEW NUMBERS . 


 BEST ARRANGEMENTS 


 SCORES GREAT MASTERS 


 ORGANPrice 2s . 

 . 
 81 . March ‘ ‘ Ode St. Cecilia Day ” ... andel , 
 Prelude Fugue ( E minor ) Scaiins Works Ntendelssonn 
 82 . Chacone ( F major ) andel . 
 Andante con variazioni Pianoforte Duets , Op . 3 . Weber . 
 Romanza ditto ditto Weber . 
 Conquering Hero comes .. cco Handel . 
 83 . Funeral March “ death hero , ” Piano- 
 forte Sonata , Op . 26 poy eco pee - Beethoven , 
 March , B minor , Op . 27 . No.1 ... ey Schubert . 
 Hunting Song ‘ “ ‘ Waldscenen , ” Op . 82 R. Schumann , 
 Adagio Pianoforte Duets , Op . 10 ... ove ° Weber : 
 84 . Fugue , G minor .. ove ove ace ° Mozart . 
 March Triomphale ... Moscheles . 
 Fantasia 6th Quartet , E flat major wee Haydm 
 85 . Allegretto , variations finale yo : pe 6th 
 Quartet , E flat major .. ooo Haydr . 
 Allegretto ( G minor ) , Op . 14 eee C. Mayer . 
 Chorus , “ goa é il cielo , ” “ Alcina Handel . 
 86 . Allegretto , Haydn 

 rom 2nd Quarter , D minor 
 flat major , Piano Duets , ‘ Op . 3 Weber . 
 uartett , D major ... eee 

 Andante cantabile 
 Largo cantabile , 5th 

 87 , Overture ( D major ) , Pp . 15 Sp 
 88 . Prelude Air , variations ‘ Suites de Pieces Handel . 
 Andante ( G major ) Rosamunde ... . - . Schubert . 
 Serenade eco . Speahee Heller : 
 89 . Andante , T2th Sy mphony eco Mozart . 
 Prelude Fugue ie sea ore pe oe Bach . 
 Finale notturno , Op . 3 Spohr . 
 . Chorus , “ Thou shalt faabe hear joy ‘ gladness ” 
 ( r51st Psalm ) ose Handel . 
 Evening music , Allegretto scherzoso ( Op . 99 ) R. Schumann . 
 gt . Chaconne , “ La Favorite ” ... eco Couperin . 
 Triumphal March “ * Vom Fels Zum Meer ” ... Liszt . 
 Chorus , ‘ God King ans Bach . 
 92 . Gavotte , 6th V ehineele Sonata . ove Bach . 
 Bourrée , 3rd ditto eco « Bach . 
 Gavotte . ( Orphée ) ... eee ° ° Gluck , 
 93 . Adagio , Quintett .. Mozart . 
 Chorus , “ Behold wicked — bow ( Anthem 
 “ Lord ” ) . _ .. < om pa ‘ Handel . 
 Adagio , Symphony inG ... aid ae poor - Haydn . 
 94 . Scherzo , 2nd Symphony ose . Beethoven . 
 Overture , Messiah Handel 

 Andante , Sonata , major , Pianoforte Violin , Op 




 05 + 

 Beethoven 

 Chorus , way gates , Jubilate ead Handel . 
 Chorus , “ Tellit omens pecans ! etiam O 




 OVERTURES , 


 ARRANGED ORGAN 


 W. T. BEST 


 HANDEL . — ‘ * MESSIAH . ” 


 J. S. BACH 


 48 PRELUDES FUGUES 


 MAJOR MINOR KEYS , 


 ( - TEMPERED CLAVICHORD 


 EDITED COLLATED EDITIONS 


 WORK , 


 NEW EDITIONS ( OCTAVO 


 NEW COMPOSITIONS , 


 J. FAURE 


 STAR 


 KING MINSTREL 


 LOVE SPLENDOUR NIGHT . 


 MARCH ONWARD 


 FAR THEE 


 OLD YEAR 


 O’ER MOUNTAIN . 


 SANCTA MARIA 


 SANCTA MARIA . 


 SON PROPHET . 


 WHEREFORE 


 REAPER SONG 


 SIGHS . 


 NOVELLO 


 SHORT MELODIES 


 ORGAN 


 VINCENT NOVELLO 


 NOVELLO OCTAVO CHORUSES 


 VOCAL SCORE 


 NEW NUMBERS 


 BR EERARRARRRRARRRA EF BEE 


 322 


 334343 " beatwicirsinne Mendelssohn 6d 

 Hearts feel love Thee } Atti 6 
 MassinD. .. pee pet Beethoven 3d 
 Athalia .. ee es « . Handel 3d . 
 Belshazzar ea te ae . 3d 
 Leonardo Leo 2d . 
 Samuel Wesley 44 

 344 . Kyrie eleison . 
 345- mighty power . 
 346 . slow degrees 

 347- Dixit Dominus .. xn oe ve 
 348 .. InexitulIsraei ... Pa = < oe 

 349 . Whereishe . Engedi .. ee + s o « Beethoven 3d . 
 350 . Jehovah , Lord God Hosts .. ian ee « . Spohr 4d . 
 351 . O God , Thy heavenly hand . Joseph . .. Handel 3d . 
 352 . Come torches . Walpurgis ms + » Mendelssohn 4d . 
 353- wrestle pray « J.S.Bach 4d . 
 354- Gentle night , O descend . Cibo s oe - » Spohr 2d . 
 355- thy friends prove faithless . Calvary . 2d 

 356 . earthly race run .. Ke v. . . ad . 
 357- Domine salvam fac . Festival Te Deum .. A. S. Sullivan 4d 




 IR 


 RTE 


 FREREEE 


 


 RR EERRRRRRRRARA 


 OCTAVO EDITIONS 


 SERVICES MODERN COMPOSERS 


 PUBLISHED NOVELLO , EWER CO 


 T. BEST.—A MORNING , COMMUNION 


 BAPTISTE CALKIN MORNING , COMMU 


 D * . JOHN B. DYKES.—A MORNING , COM 


 R. G. M. GARRETT.—A MORNING , COM 


 DWARD HERBERT CHANT TE DEUM , 


 HUBERT H. PARRY.—A MORNING , COM 


 AMUEL REAY.—A MORNING , COMMUNION 


 ENRY SMART . — MORNING , COMMU 


 IR R. P. STEWART.—A MORNING , COM- 


 ERTHOLD TOURS.—A MORNING , COM- 


 HOMAS TALLIS TRIMNELL . — CHANT 


 ALTER MACFARREN.—A SIMPLE MORN 


 R. S. S. WESLEY.—A SHORT CATHE- 


 INSTRUCTION BOOKS ORGAN , N 


 NEUKOMM 


 LONDON : NOVELLO , EWER CO 


 NOVELLO OPERA CHORUSES 


 EDITED 


 NATALIA 


 BYMACFARREN BERTHOLD TOURS 

 AUBER FRA DIAVOLO . DONIZETTI LUCREZIA BORGIA . 
 1 . Comrades , fill yourglasses   . ; P - ‘ 4 4d . | 32 . word . ‘ f ‘ ; P - 
 En bons militaires . Non far motto . 
 2 , Hail , festal morning . ‘ ‘ » ‘ P ‘ . 2d . | 33 . window . ‘ ‘ ‘ z 
 Crest grand féte . « Eisehiaspia é la finestra . 
 34 . know away sorrow . 
 AUBER ’ MASANIELLO . Il segreto esser felice . 
 . Allhail bright auspicious day . F ‘ ad- * nee 
 ’ Du Prince objet de notre amour . 2 MOZART s DON GIOVANNI . 
 4 hail bright auspiciousday . ; é 2 3 1d . | 35- Let enjoy season invites . " . ‘ . 
 Du Prince objet de notre amour . Giovinette , che fate amore . 
 5 BPD Dieu puissant ! ae 1d . MOZART LE NOZZE DI FIGARO . 
 6 . Companions , come . ( Fishermen chorus ) ‘ 2d . | 36 . Come deck flowers . 
 Amis , amis , Giovani liete . 
 y Behold morninsplendour . . 2d . | 37- Noble Lady , fairest roses . 
 Amis la matinée est belle . Ricevete , 0 padroncina . 
 § . Come hither wish buy . ( Market chorus ) 3d . | 38 . voice rejoices . 
 Au marché qui vient de s’ouvrir Amanti , costanti . 
 come , willavenge thee . e d. 
 é Courons & la ~egnene . . ROSSINI IL BARBIERE . 
 +10 Power benign . 39 . Sir , humbly thank honour . ° é 
 Saint bien heureux . Mille grazie , mio signore . 
 fo , hail , noble victor . ( Marchand chorus ) . 6d . 
 Honneur ! honneur et gloire . VERDI IL TROVATORE . 
 40 . darkness night dissolves . ( Gipsy chorus ) 
 BEETHOVEN FIDELIO . Bf Vedi ! le fosche notturne . 3 
 10 , Oh delight . ( Prisoners ’ chorus ) 3d . | 42 : dice invite leisure ! . e 
 O welche Lust . _ co’dadi ma fra poco . 
 u. Farewell , thou warmandsunnybeam . . . . .   4d.| 4 % Miserere Scene . ® m2 oot 
 Leb ’ wohl , du warmes Sonnenlicht . VERDI RIGOLETTO . 
 BELLINI PURITANI . 43- Hush , silence fulfil errand 
 Zitti , zitti , moviamo vendetta 
 wz , yonder bugle callsus ) . . 1d . | 44 . Unto lonely abode directed . 
 Pa ne la tromba squilla . ol Scorrendo uniti remota 
 j0 % ! boc " ie ; 
 festa . WAGNER LOHENGRIN . 
 y. Noble Arthur , welcome . . ee Poe . ° e 1d . 45 . hath summoned betimes 
 Ad Arturo onore , Frith’n versammelt uns der Ruf 
 1S ey Pree thee . 2d . 46 . follow leads ! ° 
 16 . Fatal Po o cara . “ Zum Streite voor y nicht ! 
 BP : . . . : : $ ’ _ P . joy attend thee 
 Ahi ! dolor . : Gesegnet soll sie schreiten 
 48 . Faithful true , lead ye forth 
 BELLINI NORMA Treulich geftihrt ziehet dain 
 17 . Hasten , ye Druids , heights ascend . ‘ ‘ : . 2d . WAGNER TANNHZUSER . 
 tect echt O Druidi . 4 49 . Hail , bright ee ( March ) . ie ‘ 
 ne . . Id. Freudig begriissen 
 Norma viene . vith joy . ( Pilgrim Ch 
 19 , gone ? , linger . 
 Non parti ? finora é al campo 7 
 2 , Vengeance , vengeance . ; 1d . WEBER OBERON . 
 Guerra , guerra ! 51 . Light fairy foot fall 
 Lieve il pie cola volgiam 
 BELLINI LA SONNAMBULA . 52 . Bons joy “ oe - 
 MeL Amina . o   « , .o . ,,8 0 1d . ae Lae 
 Viva ! viva , Amina ! 53 . Glorytothe Caliph . . + 
 22 . Fairest flower mountains fic 6 , « ORME ae besccake ps baron ee 
 wnt lees non aha rosa : Seatac eee 
 23 . dusky twilight 1d . : W eel 
 Ah fosco cielo . 55 . thee hath beauty ( Women voices ) . 
 24 . moment shelter rest 4 2d . ~~ te “ Mixed voices ) 
 Qui la selva é pit foita ed ombrosa . 56 . 0 . o. ( Mixed voices 
 WEBER DER FREYSCHUETZ 
 DONIZETTI LA FIGLIA , 57 . Victoria , victoria 
 25 . pleasure , gladness . ONG < 6 end cee aiee Victoria , Victoria : 
 Cantiamo , cantiamo . 58 . Bridal wreath thee bind 
 26 , Hark , drums arerolling . ‘ F 1d . Wir winden dir den Fungfernkranz 
 Sprona il tamburo e tncora . 59 . joy Hunter . ( Huntsman chorus } 
 27 . prepien . en e “ . . 1d . gleicht wohl auf Erden 
 ataplan , rataplan . 
 game ROSSINI GUILLAUME TELL . 
 ’ 61 . Brightly rosy morn , 
 DONIZETTI LUCIA . < Oucl jour sevein 
 28 . Let roam ruins deserted 1d . | 62 . Come , flowers crown bowers . 
 Percorriamo le spiagge vicine . } Hymenée , ta journée ; 
 29 . Hail , happy bridal day . P . ° ° C % 1d . | 63 . Hark , horns gaily sounding 
 te d’immenso giubilo . | Quelle sauvage harmonie 
 $ 0 . vengeance restrains . 2d . | 64 . Hail mighty ruler . 
 Chi raffrena il mio furore . | Gloire au pouvoir supréme 
 3 . warlike minstrelsy e e P . Seer " 1d . | 65 . Swift bird summer sky ( Tyrolean 

 D'immenso giubilo 




 LONDON : NOVELLO , EWER & CO . , 1 , BERNERS STREET ( W. ) , 35 POULTRY ( E.C 


 NEW YORK ; J. L. PETERS , 599 , BROADWAY 


 NOVELLO - SONG 


 SECOND SERIES . ) 


 COLLECTION 


 - SONGS MADRIGALS 


 MODERN COMPOSERS 


 BOOK 


 SEPARATE NUMBERS , FOLLOWS 


 VOLUME I. CONTAINS 


 SONGS 


 SIR J. BENEDICT . 


 SONGS HENRY SMART 


 SEVEN SHAKSPERE SONGS 


 G. A. MACFARREN 


 SONGS J. L. HATTON 


 VOLUME II . CONTAINS — 


 SONGS BYG.A.MACFARREN 


 SONGS C. A. MACIRONE 


 SONGS HENRY LESLIE 


 MADRIGALS 


 COMPOSERS . 


 VOLUME III . CONTAINS — 


 SONGS HENRY HILES 


 SONGS 


 % FRANCESCO BERGER . 


 VOLUME IV . CONTAINS — 


 SONGS A. ZIMMERMANN 


 SHAKSPERE SONGS 


 G. A. MACFARREN 


 SONGS HENRY LESLIE 


 SONGS J. BARNBY . VOLUME V. CONTAINS 


 SONGS G.A.MACFARREN . 


 SONGS A. S. SULLIVAN 


 SONGS W W. MACFARREN 


 


 SONGS J. LEMMENS 


 SONGS HENRY SMART 


 SONGS CIRO PINSUTI 


 VOLUME VI . CONTAINS 


 THIRTY - SONGS 


 J. L. HATTON 


 S.A.T.B 


 VOLUME VII . CONTAINS 


 J. L. HATTON 


 MEN VOICES . 


 VOLUME VIII . CON TAINS 


 HENRY SMART 


 WALTER MACFARREN 


 ( S.A.T.B 


 - SONGS 


 - SONGS 


 - SONGS 


 VOLUME X. CONTAINS 


 R. L. DE PEARSALL 


 ( S.A.T.B. ) 


 S.S.A.T.T.B. ) 


 VOLUME XI . CONTAINS 


 R. L. DE PEARSALL 


 310 


 311 


 312 


 313 


 314 


 315 


 316 


 317 


 318 


 319 


 320 


 32I 


 « S.A. T.B.B 


 B.B. ) } 


 ET 


 SONGS MADRIGALS 


 SONGS MADRIGALS 


 SONGS A. S. SULLIVAN 


 HYMNARY 


 CONTAINING 


 646 HYMNS TUNES 


 SMALL POST OCTAVO , BIND PEARL OCTAVO PRAYER BOOK : — 


 TREBLE READY DAYS 


 LONDON : 


 NOVELLO , EWER CO . , 1 , BERNERS STREET , W. , 35 , POULTRY , E.C 


 OFFERTORY SENTENCES . COMPOSITIONS 


 APPENDIX FESTAL USE , | SIR JOHN GOSS 


 MA 


 MI 


 BAI