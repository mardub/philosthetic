


HE MUSICAL TIMES


ORAMER’S PATENT AMERICAN ORGANS


FRAMERS AMERICAN ORGANS, 


PORAMER'S £12 AMERICAN ORGAN


ORAMER'S £15 AMERICAN ORGAN 


RAMER'S £22 AMERICAN ORGAN 


ORAMER'S £28 AMERICAN ORGAN 


RAMER'S £34 AMERICAN ORGAN 


JORAMER’S 70 GUINEA AMERICAN ORGAN


JORAMER’S 85 GUINEA AMERICAN. ORGAN


10 STOPS AND PEDALS


12 STOPS, 2 MANUALS


16 STOPS, 2 MANUALS


PROFESSIONAL NOTICES


SIGNOR MARIANO DE LAFUENTE. 


MR. J. TILLEARD, 


USIC ENGRAVED, PRINTED, AND PUB- 


_BUSSELL'S MUSICAL INSTRUMENTS. ; 


HARGREAVES’S AMERICAN IRON FRAME 


GRAND PIANOFORTES


THE PROFESSIONAL AND CONNOISSEURS’ HARMONIUM. 


DRAWING ROOM MODEL, 


13 ” ” ” ” 


BRASS, REED, STRING, AND DRUM AND FIFE BANDS, 


PIANOFORTES, AND HARMONIUMS 


& W. SNELL’S IMPROVED HARMONIU 


PASCALL’S VOICE JUJUBES. 


REAKFAST.—EPPS’S COCOA. — “ 5 


BATEFUL—COMFORTING. 


_—_


RGAN, PIANOFORTE, HARMONIUM, HAR- 


~_ _____ £0 CHORAL SOCIETIES. 


TWO FOUR-PART SONGS BY DR. MONK. 


WILLIAM J. YOUNG’S 


POPULAR PART SONGS FOR S&.A.T.B. 8 


SECOND SERIES


CANTICLES AND HYMNS


OF THE CHURCH


POINTED FOR CHANTING, AND SET TO APPROPRIATE ANGLICAN CHANTS 


SINGLE AND DOUBLE


TOGETHER WITH


EDITED BY THE


AND


EDWIN GEORGE MONK


NOVELLO’S


OCTAVO EDITION OF OPERAS. 


VOLUME XVIII


DONIZETTI’S


LA FIGLIA DEL REGGIMENTO


WITH ITALIAN AND ENGLISH WORDS, WILL BE 


READY ON OCTOBER 1. 


AYDN’S MASSES


LATIN WORDS ONLY


WILLEM COENEN


BY


SONGS. 


PIANO. 


589


HARVEST ANTHEMS


IN AID OF THE


NORTH COATES CHURCH ORGAN FUND


THE VILLAGE ORGANIST


C. JEFFERYS, 57, BERNERS ST. 


THE CHORAL SOCIETY


A COLLECTION OF


THE POPULAR 


SOLOS AND DUETS, 


WILLIE PAPE. 


LLANGOLLEN .. 4 Performed with great success by Madlle. Sophie Heilbron

LOUIS DUPUIS, 
CATHEDRAL GEMS: 
1. Naxres.—Fantasia, on subjects from Mozart's 12th Mass 
2. WurMs.—Fantasia, on subjects from Weber's Mass in G 
8. RoTTERDAM.— Fantasia, on subjects from Haydn's 3rd 
(Imperial) Mass ... 
4, MaLaGa.—Fantasia, on subjects from Beethoven's Mass

i




C. JEFFERYS 57 BERNERS STREET (W


8 0 


590


______—_—_—_—_—_


591


THE MUSICAL TIMES, 


SEPTEMBER 1, 1872


THE LONDON MUSICAL SEASON. be g

time and experience will but ripen those powers, of the 
possession of which she has already given such unmistakable 
proof. Of the German singers especially engaged for 
“Lohengrin” we have little to say. Madlle. Emmy 
Zimmermann did not come at all, Herr Koehler disappeared 
suddenly from the bills, and Madlle. Marianne Brandt re- 
mained to prove to us that she could not sing the music of 
Leonora, in Beethoven’s “ Fidelio,” nor of Zivira in “ Don 
Giovanni,” whatever success she might have achieved in 
Herr Wagner's Opera. Madlle. Saar also made but little 
effect; Signor Casari sang but once, and many others 
announced never appeared. A good word must, however, be 
said for Madlle. Smeroschi, who, although making her début 
at the latter part of the season, created a genuine effect, and 
the excellent singing of Madame Parepa-Rosa must not be 
passed over without due acknowledgment. Madame 
Adelina Patti and Madame Pauline Lucca have been the real 
attractions of the season, the fact of their names being pro- 
minently placed at the top of the bills proving beyond doubt 
that the singers, and not the Operas, are the most important 
announcements of the evening. In all her favourite parts 
Madame Patti has excited the usual enthusiasm; but it is 
gratifying to find that even her exceptional powers could not 
make the public accept Prince Poniatowski’s feeble Opera, 
‘‘Gelmina.” Madame Lucca’s triumphant success as Agaia, 
in “Der Freischutz,” astonished even her most ardent ad- 
mirers ; and we sincerely hope that next season she may have 
an opportunity of repeating so excellent a performance. Why 
Madame Miolan-Carvalho (who was announced for several 
parts in the prospectus) only sang for one night it is impossi- 
ble to say, for we have ceased to enquire into the cause of 
‘mysterious disappearances” at lyrical establishments. On 
the singing of our established favourites during a most 
arduous season it is unnecessary to dwell at length. Madlle. 
Sessi has again proved a highly valuable member of the com- 
pany, especially distinguishing herself in the trying and 
thankless part of the heroine in “ 11 Guarany ;” and M. Faure 
has materially strengthened the cast of every Opera in which 
he was engaged. The want of a really efficient tenor is still 
felt, although both Signor Naudin and Signor Nicolini were 
warmly received in the leading parts; and Signor Bettini was 
as earnest and painstaking as ever. The members of the 
orchestra have done their best to serve two masters; but the 
pernicious effect of dividing the conductorship should by this 
time have become apparent even to the lessee. Considering 
what an important personage Mr. Augustus Harris has be- 
come since the rise of spectacular Opera, we cannot conclude 
our notice without a line of recognition for his services— 
indeed, the fact of his being engaged at St. Petersburg is a 
convincing proof how much clever stage-management has to 
do with the success of modern lyrical works

The prospectus of Her Majesty’s Opera promised only two 
comparative novelties, but even these two pledges were not 
redeemed. Certainly Cherubini’s “Deux Journées” was 
produced (although never repeated), but Auber’s “ Diamants 
de la Couronne,” pushed forward even to the closing night, 
was withdrawn at thelast moment. ‘Then “Der Freischittz,” 
“ Dinorah,” “Don Giovanni,” “ Anna Bolena,” and many 
other Operas contained in the prospectus, were never given at 
all, a fact still more extraordinary when we consider that alt 
the vocalists included in the cast of these works were in the 
theatre during the season. As lessees of Opera-houses, how- 
ever, seem to arrogate to themselves the exclusive right of 
disregarding every promise made to the subscribers at the 
commencement of the season, it is, perhaps, better to treat 
prospectuses like “prophetic almanacs,” and throw them 
aside at the end of the year, with thanks to their authors for 
the few events which have come true, and a feeling of cha- 
ritable forgiveness towards them for those which have not




592Margherita, in Gounod’s “ Faust,” is beyond her powers. 
The return of Madlle. Christine Nilsson (now Madame 
Rouzaud) hag proved to us that America is scarcely a school 
for the development of the refinements of art. Her voice has 
perhaps increased in power, but her natural coldness (which, 
with all her exquisite vocalisation, was ever apparent before 
her departure for the United States,) is now partially con- 
cealed by an exaggeration of manner which, to us at least, is 
scarcely an improvement upon her former style. Madlle. 
Marimon has added but few parts to those which she played 
last season, but she has been a genuine attraction, and we are 
much mistaken if she do not materially increase her reputa- 
tion in each succeeding season. Madlle Grossi made a suc- 
cessful début, but soon di-appeared. Miss Kellogg has been 
received with a warm welcome on her return to the establish- 
ment, and we trust that next year we may not only see her 
as a permanent member of the company, but have the oppor- 
tunity of testing her powers in an entirely new part. 
Madlle. Titiens remains unapproachable in the characters 
already identified with her name, and Madame Trebelli- 
Bettini has rendered invaluable service as a contralto. With 
the exception of Signor Campanini, no tenor has been equal 
to the first parts, although Signor Fancelli and M. Capoul 
have sung their best, and the same may be said of Signori 
Vizzani and Rinaldini. Signor Mendioroz has divided the 
honours with Signor Rota; Signori Foli and Agnesi have 
fally sustained their high reputation as basses ; and, although 
we cannot admit the claims of Signor Borella, the public has 
accepted him in the best buffo characters. We have already 
said that Cherubini’s ‘‘ Deux Journées”’ was too good for the 
audience, and those who wish for something more than 
singers’ Operas must therefore wait patiently for better days. 
The well-known works have, according to annual custom, 
been played the usual number of times, with the usual 
amount of applause, and the usual offering of bouquets to the 
public idols on the stage ; and as the lessee has had, we hear, 
a successful season, nobody, save a few carping critics like 
ourselves, can possibly be dissatisfied. The orchestra, con- 
ducted by Sir Michael Costa, is thoroughly under command, 
and if we could only get a more subdued tone from the brass 
instruments when accompanying yoices, it would be absolute 
perfection. A word of praise for the general efficiency of the 
ballet department must terminate our retrospect of the season 
at Her Majesty’s Opera

The Philharmonic Society is gradually and carefully ex- 
perimenting upon the public taste. As a doctor feels the 
pulse of his patient, and cautiously tries the effect of a new 
diet, occasional doses of the music of modern Germany are 
prescribed, after a careful consultation of the directors, and 
with a result which may on the whole be considered en- 
couraging. When we remember what obstacles the Society 
encountered years ago in forcing the claims of Beethoven 
upon the attention of its subscribers, we can scarcely wonder 
that equal difficulties should be experienced in craving ad- 
mittance for those who work with even a greater disregard 
for the conventional forms of art. Schubert, and even 
Schumann, are now almost safe; but others, whose fame is 
cordially acknowledged in the land of their birth, must be 
content to wait for recognition in conservative England. Our 
thanks are due for such revivals as the Concerto of Handel, 
the Organ Toccata and Concerto of Bach, and the Sym- 
phonies of the late Cipriani Potter and Sir Sterndale Bennett; 
but when we find that the overture to “ Ajax,” by the last 
named composer, was the only new work produced during 
the season, it certainly appears as if the enterprise of the 
Society were on the wane

The Crystal: Palace has thoroughly maintained its cha- 
racter for the performance of the highest class compositions, 
and Mr. Manns, the talented and zealous conductor of the 
concerts, may fairly claim the credit of having blended 
instruction with entertainment. Those amongst the listeners 
who are sufficiently earnest in their attention to the music 
may probably have that attention occasionally disturbed by 
the devotion of some of the feminine portion of the audience 
to their books, crochet, and knitting during the performance, 
and may not like the walking accompaniment outside of 
those who cannot, or do not desire to, enter the concert-r.om; 
but these are minor defects, all of which will, no doubt, be

remedied in time. We must here, too, refer to the National 
Music Meetings, which have been more successful than could 
have been anticipated. considering the novelty of the ex 
ment, No better proof of the interest which they excite 
amongst artists could have been afforded than the fact of» 
many eminent musicians at once consenting to serve ay 
jurors; and, although but few choral societies entered fy 
competition, it can scarcely be doubted that, as the object of 
these gatherings becomes better known and appreciated, 
there will be an increasing desire amongst country choirs t 
display their powers before a London audience. Meanwhile 
it must be recorded that at least four solo vocalists wer 
brought prominently forward who, but for these meetings 
might never have become known, and much facility in sing 
ing at sight was evinced by several choral bodies

At the “Oratorio Concerts ” (this season judiciously te 
moved to Exeter Hall) we have had performances of some of 
the standard sacred works, which have been universally ae 
knowledged as the most perfect ever given in the metropolis, 
The few words with which we opened our present artids 
must, we fear, explain why the revival of forgotten compos. 
tions has not been vigorously proceeded with. If Oratorio. 
lovers prefer to listen again and again to the beauties of 
“ Elijah,” instead of to make themselves gradually acquainted 
with the profundity of Beethoven’s Mass in D, the publig 
and not the directors, must be blamed for the non-production 
of un-familiar works; for, as we have already said, until the 
presentation of art-creations ceases to be a mere commertid 
speculation, we can hope for but’a small amount of progres 
Meanwhile we must not forget that the recognition @f 
Handel’s “ Jephtha” as one of his finest Oratorios, and the 
popularity (we may almost say) of Bach’s S. Matthey 
“ Passion Music” is entirely owing to their excellent per 
furmance by this Society ; and in a review of the past season 
we must chronicle the highly successful production of Bacht 
*«« Passion-Music” according to 8. John, by a‘portion of the 
fine choir of this Association at the Hanover Square 
under the direction of Mr. Barnby, whose zealous services il 
connection with the ‘Oratorio Concerts” cannot be ti 
highly praised

The concerts of Mr. Henry Leslie’s Choir have an especial 
character which must always make them welcome. Unit 
companied part-music has this season formed a more thal 
usually prominent feature in the programmes of these entet- 
tainments, and we need scarcely say that the perfection with © 
which it is interpreted affords unquestionable proof of tht 
devotion, not only of the conductor but of the choir, to th 
especial object for which this Association was instituted. We 
must not omit to mention the production of Carissimis 
Oratorio, “ Jonah,” which was so successful that Mr. Leslie | 
felt himself justified in repeating it, to the great gratification 
of all who are interested in the rise and development 
Uratorio music




EEVECERIESEEFS


EG EFFEERES EEL THREE


F127 2


593we are promised selections from the works of Mozart

and from Handel’s “L’Allegro” and “Il Pensieroso,” and 
the whole of Beethoven’s “ Ruins of Athens.” The vocalists 
ge are Madlle. Tietjens, Madame Lemmens-Sherring- 
adame Patey, Miss Alice Fairman, Mr. Sims Reeves

Vernon Rigby, Mr. Edward Lloyd, Mr. Santley and




594


EE


EEE


533 


FF


TH


595


596


597


TO THE EDITOR OF THE MUSICAL TIMES


QQ 


DIS


——S SSS _ ESS 


J.J. H. TAYLOR. 


THE WRITER OF THE ARTICLE


TO THE EDITOR OF THE MUSICAL TIMES


TO THE EDITOR OF THE MUSICAL TIMES


FULL ANTHEM FOR FOUR VOICES


2. 4 1 4 


REJOICE IN THE LORD


2 I | 


ES SES SS ES


TL


PT


REJOICE IN THE LORD


= —— | — SS 


FERESEDEDEREGEES 7. ETESBLEEOES


603


TO THE EDITOR OF THE MUSICAL TIMES. 


TO CORRESPONDENTS


604


DURING THE LAST MONTA, 


605


ANGLICAN CHORAL SERVICE BOOK


USELEY AND MONK’S PSALTER AND


ULE’S COLLECTION OF 527 CHANTS, 57


DIRECTO 


ULE’'S DIRECTORIUM CHORI ANGLI- 


ORDER FOR THE HOLY COMMUNION


94 TUNES, WITH POPULAR HYMNS, 12 


TENTH EDITION. 


TO THE PROFESSIO 


3. AMONG THE HILLS


606


TONIO SOL-FA EDITION. 


NOW READY, 


TUNES AND CHORALES. 


ARR’S CHURCH OF ENGLAND PSALMODY: 


THE NATURAL PRINCIPLES OF HARMONY, 


THE COLLEGIATE AND SCHOOL 


SIGHT SINGING MANUAL


(COMPANION WORK TO THE ABOVE


COLLECIATE VOCAL TUTOR


NEW SONG, SUNG BY MR. SIMS REEVES, 


“TIS BETTER NOT TO KNOW. 


G. A


J, 8


HANI


HAYD


_ MUSICAL FESTIVALS, 1872


ARE PUBLISHED BY


MESSRS. NOVELLO, EWER & CO


A, § SULLIVAN’S «FESTIVAL TE DEUM.” 


“OUTWARD BOUND.” HANDEL'S « L’ALLEGRO.” 
Octavo, paper cover, 3s. 
Ditto, cloth gilt, 5s

BEETHOVEN’S “ RUINS OF ATHENS.” 
Folio, 10s

HUMMEL’S « MESSE SOLENNELLE,” in E flat. 
Fotio, 8s




MENDELSSOHN’S ORATORIO, « ELIJAH.” 


MENDELSSOHN’S « HYMN OF PRAISE.” 


HANDEL’S ORATORIO, « THE MESSIAH.” 


HAYDN’S ORATORIO, « THE CREATION.” 


HANDEL’S ORATORIO, « SAMSON.” 


WORCESTER AND NORWICH


NOVELLO’S


SECOND SERIES


A COLLECTION OF 


BY MODERN COMPOSERS


NOVELLOS Edited, Corrected according to the Original Scores, and Translated into English, by NATALIA MACFaArrgy, 
Price 2s. 6d. each; or in scarlet cloth, 4s

NOW READY. 
BEETHOVENS FIDELIO

German and English words.) With the two great Overtures as usually performed; being the only Piano 
Score that has been published agreeing with the Original Score as to the notes and signs for phrasing




AUBER’S FRA DIAVOLO


MOZART’S DON GIOVANNI


BELLIN’?S NORMA


VERDI'S IL TROVATORE


DONIZETTIS LUCIA DI LAMMERMOOR


WEBER’S OBERON


ROSSINI’S IL BARBIERE


DONIZETTYIS LUCREZIA BORGIA


MOZART’S LE NOZZE DI FIGARO


VERDI'S RIGOLETTO


BELLINI’S LA SONNAMBULA


WEBER’S DER FREISCHUTZ


WAGNER’S TANNHAUSER


AUBER’S MASANIELLO


BELLINI’S I PURITANI


WAGNER'S LOHENGRIN


TO BE CONTINUED MONTHLY


THE FOLLOWING OPERAS ARE IN COURSE OF PREPARATION :— 


DONIZETTI'S LA FIGLIA DEL REGGIMENTO. VERDI’S LA TRAVIATA


_—_


NOVELLO’S LIBRARY


HERUBINI’S TREATISE ON COUNTER- 


R. MARX’S GENERAL MUSICAL INSTRUC- 


III, 


ETIS’ TREATISE ON CHOIR AND CHORUS 


IV. 


OZART’S SUCCINCT THOROUGH-BASS 


VI. 


VII


ERLIOZ, TREATISE ON MODERN INSTRU- 


VIII. 


R. CROTCH’S ELEMENTS OF MUSICAL 


ABILLA NOVELLO’S VOICE AND VOCAL 


LI. 


INK’S PRACTICAL ORGAN SCHOOL. 


LII. 


UHR’S PAGANINI’S ART OF PLAYING 


ALKBRENNER’S METHOD OF LEARNING 


NEW NUMBERS


BEST’S ARRANCEMENTS


FROM THE SCORES OF THE GREAT MASTERS


FOR THE ORCANNo. 82.—Chaconne (Fmajor), Handel; Andante con Variazioni, and 
Romanza (Op. 3), Weber

No. 83.—Funeral March (Op. 26), Beethoven ; March (B minor, Op. 
Tp anabent; Hunting Song (Op. 82), Schumann; Adagio (Op. 10

eber




OBLONG FOLIO EDITION


RINEK’S PRACTICAL ORGAN SCHOOL


610


NOVELLO’S ORIGINAL OCTAVO EDITIONS OF


FOR THE USE OF THE AUDIENCE WHO ASSIST AT THEIR PERFORMANCES


ENDYMION © 


THE PASSION (S. MATTHEW) 3 


REBEKAH


ST. PETER


THE ERL KING’S DAUGHTER 


SPRING’S MESSAGE


COMMUNION SERVICE, 


SECOND MESSE DES ORPHEO- 


NISTES 


THE SEVEN WORDS OF OUR 


SAVIOUR ON THE CROSS 


DAUGHTERS OF JERUSALEM 


SOLOMON


JEPHTHA


JOSHUA


DEBORAH


SAUL


DETTINGEN TE DEUM


THE KING SHALL REJOICE


ZADOCK THE PRIEST


MY HEART IS INDITING


LET THY HAND BE 


STRENGTHENED


THE WAYS OF ZION


ALEXANDER’S FEAST


ACIS AND GALATEA


ODE ON ST. CECILIA’S DAY


L’ALLEGRO, IL PENSIEROSO, 


ED IL MODERATO


THE SOUL’S ASPIRATION


OUTWARD BOUND 26 400 


MAY-DAY 20 3°6 


THE LAY OF THE BELL ... 1 6 30 


PARADISE AND THE PERI +° 6 4 0 


THE BRIDE OF DUNKERRON 2 6 4 0 


THE FALL OF BABYLON 3 0 5 0 


THE CHRISTIAN’S PRAYER 1 6 3 0 MASS IN E FLAT, Ditto. t~.6 3 0

Hlozart, Haydw, and Beethoven

MOZART’S TWELFTH MASS 2 o ee 
HAYDN’S THIRD, or

IMPERIAL, MASS 2 0 3 6 
BEETHOVEN’S MASS inC ... 2 0 3 6

The above Three Masses, bound in one volume, whole scarlet




MOZART’S REQUIEM MASS 20 3 6 


MOZART’S FIRST MASS... 1 6 3 0 


HAYDN’S FIRST MASS... 2.0 3 6To these Masses are added Mr. E. Hoimes’ Critical Essays, 
extracted from the Musical Times

MOZART’S LITANIA DI VENE- 
RABILE ALTARIS (in E flat) 1 6 3 0 
MOZART’S LITANIA DI VENE- 
RABILE SACRAMENTUM (in 
B flat) ... in 
BEETHOVEN'S MASS in D ibe 
BEETHOVEN’S ENGEDI; or, 
David in the Wilderness ia

NH 
oo




22


OCTAVO EDITIONS OF


PUBLISHED BY NOVELLO, EWER AND CO


T. BEST.—A MORNING, COMMUNION


BAPTISTE CALKIN’S MORNING, COMMU


USELEY.—A CHANT SERVICE FOR THE


HUBERT H. PARRY.—A MORNING, COM


D* G. M. GARRETT.—A MORNING, COM- 


DWARD HERBERT'S CHANT TE DEUM, 


R. R. P. STEWART.—A MORNING, COMMU- 


YERTHOLD TOURS.—A MORNING, COM- 


JALTER MACFARREN.—A SIMPLE MORN


R.S8.S. WESLEY.—A SHORT FULL CATHE


THE ONLY COMPLETE EDITION


MENDELSSOHN’S


9 72


LOADS


2 5 0 | 6 5 


12


SONGS. 


PIANO


ARRANGEMENTS2

Bourée in O, by J. S. Bach = 2 
Second Concerto. Composed for the Harp- 
sichord or Organ by G. F. Handel - 
Scherzo from Beethoven’s Trio, for Violin, 
Viola and Violoncello, Op. 9. No. 1 - 
Menuetto from Schubert’s Quartett for 
Stringed Instruments, Op. 29

or

0) 
0| 
0

6 | Allegretto alla polacca, from Beethoven’s 8 
Serenade, Op. 8, for Violin, Viola 
and Violoncello - 3 0 
| Menuetto from ditto, ditto, Op. 9. No. 2 3 0 
No. 4 of R. Schumann’ s Skizzen fiir den 
Fliigel (Sketches for _- Pedal 
ianoforte) - - 3 0

Compositions rs Herthold aman




SONGS. 


PIANO. 


S 8. 


LONDON: NOVELLO, EWER AND CO., 1, BERNERS STREET (W.), AND 35, POULTRY (E.C.). 


NEW YORK: 751, BROADWAY


LESLIE'S CHORAL MUSIC


EDITED BY HENRY LESLIE. 


_—~ ~_ 


LONDON: NOVELLO, EWER AND CO., 1, BERNERS STREET (W.), AND 85 POULTRY (E.C


THE


1, BERNERS STREET, W


LONDON: NOVELLO, EWER AND CO


CHAPPELL & CO


THE ALEXANDRE NEW SIX-GUINEA ORGAN HARMONIUM 


SOLID OAK CASE, FIVE OCTAVES, AND TWO FOOT-BOARDS


CHAPPELL & CO., 50, NEW BOND STREET


NEW WORK FOR SINGING CLASSES


CHAPPELL'S PENNY OPERATIC PART-SONGS


A NEW WORK FOR THE ORGAN. 


HANDEL’S CHORUSES


CHAPPELL & CO., 50, NEW BOND STREET, LONDON