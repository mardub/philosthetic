


 SINGING - CLASS CIRCULAR 


 FEBRUARY 1 1923 


 MUSICAL TIMES 


 CHARGES advertisement 


 special notice . MOUSSORGSKY song writer 
 ERNEST NEWMAN 
 

 time critic historian 
 wish task simplify 
 destruction music 
 century matter ; think , 
 instance , minor work Mozart , Haydn , 
 Beethoven conscientious historian 
 feel read , hardly 
 worth perform - day . 
 long musicologue consider 
 seriously question wholesale sacrifice 
 second- - rate music , simple 
 reason soon impossible 
 man superficial knowledge 
 write lead 
 composer . day , doubt , international 
 committee form decide 
 work past worth reprint , 
 allow lapse oblivion 
 humanity penny bad 

 point view . ' , 
 , critic , groan 
 magnitude task , equally sympathy . 
 feel understand composer 
 thoroughly know intimately 
 composer write . ' psychologist 
 strong esthete . 
 enjoy bad piece art art , 
 profoundly interested 
 revelation artist ; Oscar Wilde 
 Browning , ' process fool arrive 
 folly dear ultimate 
 wisdom wise . ' man mind 
 piece , critic feel dare neglect 
 manifestation , poor piece work 
 throw good deal light good . 
 , , study artist 
 weakness understand strength ; 
 , reading inferior work 
 composer critic esthetic pleasure 
 — like try meal 
 tares wheat lie hand 
 table — good deal 
 pleasure possible 
 composer , defect 
 quality , act 
 react 




 04 MUSICAL 


 1923 


 XUM 


 1923 95 


 ARTHUR BLISS 


 — _ » -2-*-~--—- = 


 PP 


 1923 


 SS 


 XVLOPHONE 


 PIANOFORTE CONCERTO 


 , CGNL 


 INCERTO 


 


 — — SS=—= — — — 


 time — FEBRUARY 1 


 1923 


 98 MUSICAL 


 list composition 


 time — FEBRUARY 


 99 


 1923 


 MUSICAL 


 ROBERT JONES preface 


 


 100 


 1923 


 FRANCK ORGAN MUSIC 


 n 


 - — — — — — _ SL ~ : = = = ° ° 


 ULSa 

 Scherzo section great originality , , 
 neatly play sharp pace ( 
 « = 120 ) right light registration , 
 effective . prefer omit , , 
 climax develop } need hesitate , portion 
 good example three-| Andante join satisfactory 
 harmony manual , great | . 
 tone . power music| Franck lead iza / e pass 
 , wide - range left - hand | review subject use . inevitably 
 gradual ascent keyboard . unfortunately | remind Beethoven use device . 
 Franck let movement pause | ninth Symphony , comparison leave 
 page 22 23 , string tame | advantage Beethoven . , 
 chord end page 22 . mere | doubt kind thing 
 registration halt indication ! successful help word , 
 accompany . need not|of discordant interruption Beethoven 
 hesitate omit bar follow third| use , fact significant 
 pause page 22 . similarly , are|as word . aid marshalling 
 enthusiastic letter spirit,|of number theme produce merely 

 104 MUSICAL TIMES — FeEsruary 1 1923 




 2 SESE 


 105 


 * ancient conception 


 rhythm | 


 106 MUSICAL 


 TIMES — FEBRUARY 1 


 1923 


 DR . HULBERT lecture 


 LECTURE , 


 LECTURE 2 , 


 lecture 3 . 


 lecture 4 . 


 MUSICAL 


 1923 107 


 MUSIC TIBET 


 MUSICAL 


 108 


 1923 


 FRANCK unpublished WORKSTiersot divide manuscript 
 examine group : firstly school 
 task , consist exercise harmony , counter- 
 point , fugue ( 1883 - 40 ) ; _ original 
 composition write childhood , youth , 
 1847 onwards 

 group comprise - fugue 
 subject , ' worthy [ Tiersot ] 
 transcribe keyboard perform , ' 
 Franck win prize 1840 . second 
 mentioned ' variation pianoforte aria 
 Le Pré - avx - Clercs , César Franck , age 
 vear half , Op . 5 , ' song O Salu / aris , 
 Pianoforte Sonata ( style Beethoven early 
 work ) , symphony orchestra , Op . 13 
 perform , accord note pencil MS . , 
 Orleans 1841 , probably write far early ) , 
 second Pianoforte Sonata ' cyclic ’ 
 principle apply 

 beginning ot 1848 Franck complete 
 tone - poem inspire Hugo Ce gu’on entend 
 sur la montagne , , judge   Tiersot 
 description example quote , 
 worth know . ( Tiersot contention 
 strength work , Franck consider 
 having forestall Liszt invent vere 
 tone - poem altogether admissible : Liszt 
 Mazeppa Etude , instance , final form 
 ( 1837 ) , adduce proof contrary 

 Monde Musical ( December ) report address 
 Vincent d’Indy member Société 
 Frangaise de Musicologie certain early work 
 César Franck 

 1837 1847 , Franck music reveal , 
 melodic point view , influence Monsigny , 
 Méhul , Gluck , Beethoven ; , regard 
 writing , Liszt , Thalberg , Alkan . 
 imitation Liszt particularly obvious Chant du 
 Litre pianoforte , , , 
 characteristic Franck individuality . 
 early pianoforte piece shape — 
 Allegro exposition theme , 
 introduction 

 FO = Th 




 c8 


 1923 109 


 


 FRANCK influence 


 BRITAIN ultra - modernist futurist 


 composer 


 RIMSKY - KORSAKOV BOR / S GODOUNOV 


 NEW LENTEN CANTATA 


 


 110 


 1923 


 CHORAL MUSIC 


 S.A.T.B. , 


 S.A.T.B. , 


 MUSICAL 


 1923 


 1923 113 


 LADY BEECHAM press AGENTAnother violoncello record Maurice 
 Dambois Chopin Nocturne D flat 
 Canzonetta Du Port . phrasing 
 Chopin strike matter - - fact , 
 lacking dreamy elegance 

 Lamond hear H.M.V. 12 - . d.-s . 
 Minuet Beethoven E flat Sonata , Op . 31 , 
 . 3 , Glinka Z’A / ouette . , 
 good reproduction , need comment . 
 Glinka brilliant enjoyable . 
 excellent pianoforte record ' Rachmaninov 
 transcription Minuet 
 L’Arlesienne Suite . 1 . ( H.M.V. - . ) . 
 fault pianoforte record 
 occasional effect jangling overgrown 
 musical - box 

 English Singers fore fine 
 H.M.V. 12 - . d.-s.—Gibbons t life ? 
 Tomkins David hear . 
 advance previous record respect . 
 balance blend ( 
 high note bass occasionally cut 
 texture edgy tone ) 
 word clear . madrigal happen 
 type suit 
 largish choir single voice . big 
 way Handel chorus , carry 
 tone . good performance 
 soloist , inevitably sense effort 
 exacting passage . , 
 splendid Gibbons work sombre weight 
 realise sing choir 




 1923 


 EASTER CAROL 


 TENOR 


 SS 


 SOUTER SSE 


 22 


 22 SSS 22 SSS SS 


 > } - * — _ _ — _ — _ _ > — - — _ — _ _ _ — _ — _ — _ — _ — _ — 


 SSE 


 4,3 64 - 6 } 8 


 ' t 5 


 « ~ - : FT — — > > = > 


 — — — — — — = = 35 


 


 1923 119 


 ROYAL COLLEGE ORGANISTS 


 DIPLOMA distribution 


 PASSE ) fellowship , JANUARY , 1923 


 


 1923 


 pass ASSOCIATESHIP , JANUARY , 1923 


 FELLOWSHIP ORGAN - WORK 


 CHARLES MACPHERSON . 


 A. HERBERT BREWER 


 FELLOWSHIP paper - WORK 


 J. FREDERICK BRIDGE . 


 WALTER PARRATT 


 ASSOCIATE organ - work 


 HARVEY GRACE 


 ASSOCIATE paper - WORK 


 H. A. HARDING 


 PRESIDENT address 


 121fae 

 charge organ — 
 exception Bach great master write 
 . hard nowadays know great 
 master , . use 
 think pretty Beethoven , present time 
 favourite target brick throw 
 young critic . meet follow day 

 item Beethoven interesting Quartet 

 Op . 130 . wewish composer confine 

 apart pious wish , epithet ' interesting ' 
 delightful . word people use 
 acknowledge receipt new work 
 find hard 

 , let use term ' prominent composer , ' 
 run time Bach . Hande ! 
 know great organist , think 
 english organ time possess pedal 
 accustom , write organ work 
 instead concerto . know 
 miss , Handel instrumental music compare 
 badly Bach , choral work . 
 Handel Bach style music change — 
 Bach forget nearly century , irresistible 
 attraction swell orchestra expand opera 
 powerful counter - attraction . musician ne 
 long necessarily church organist , generally 
 , musical boy happen belong ' 
 choir learn organ , exist 
 church . allthe ' prominent composer ' learn clavier 
 pianoforte , write 
 incidentally ; write organ 
 learn ? Haydn study clavier 
 violin , sufficient occupy . 
 Weber Wagner absorb opera , 
 fine pianist . Schubert little 
 organ , principal interest violin . Beethoven 
 play organ boy Bonn , 

 quality , Hopkins 




 


 MUSICAL 


 1923 


 recital BLIND organist 


 ORGAN 


 


 MUSICAL TIMES — FEBRUARY 


 1923 12 


 kb & < exceptionally interesting recital organ music base 
 Christmas theme Mr. Archibald Farmer 
 Presbyterian Church England , Muswell Hill . 
 programme include Maleingreau Vers /a Creche ( Vent 
 Redemptor ) ; piece old french Carol tune Le Bégue , 
 d’Aquin , Boely , Franck , Ropartz ; Christmas hymn- 
 tune Buxtehude , Pachelbel , Buttstedt , Bach , Reger , 
 round Karg - Elert fine Improvisation /2 dudci 
 jubilo . Mr. Farmer announce series historical 
 recital fridaysat8 p.m. place January 
 19 , remain recital February 9 ( French ) , | 
 March 2 ( English ) , March 23 ( general 

 excellent music hear St. Martin - in- 
 - Fields saturday January : Parts 1 2 
 Christmas Oratorio , Holst Songs Voice Violin , | 
 song Farnaby , Movzart , Vaughan Williams , | 
 String Quartets Beethoven Elgar , & c. singer | 
 Miss Beatrice Hughes Pope , Miss Elsa West , Mr. | 
 Norman Stone , Mr. Eustace Belham ; string | 
 player , Mesdames Elsa West , Elsie Bernard , Emily | 
 Wingfield , Hildegarde Arnold . Messrs. Bernhard Ord , | 
 L. Stanton Jefferies , Rev. G. Sydenham Holmes | 
 organ 

 Mr. A. M. Gifford , Hunstanton , write ask 
 reader identify organ work Lemmens , 
 open 




 LEMMENS . 


 ORGAN kecital 


 124 


 1923 


 appointment 


 LEEDS COMPETITIVE FESTIVAL 


 LONDON FESTIVALNo . 3 

 Beethoven 

 choirmaster 




 competition FEBRUARY MARCH 


 14 


 MUSICAL 


 time — FEBRUARY 1 


 1923 


 125 


 ARCH 


 PARSON PLAYS 


 COUNTRY PARSON . 


 GARBLED review 


 MEMORIAM DR . BUNNETT 


 MUSICAL 


 1923it 
 Bermondsey 

 dreadful Prelude Lohengrin . . 
 Adelaida Beethoven . — Darius Milhaud 

 opera libretto general write class 
 people Thompson - Bywaters hanging 
 providentially follow mystery lock - 
 tailor . — /ercy A. Scholes 




 ROYAL ACADEMY MUSIC 


 TRINITY COLLEGE MUSIC 


 R.A.M. : 


 time — FEBRUARY 


 MUSICAL 1923 


 dinner presentation 


 UNION GRADUATES MUSICA series lecture french musical history , 
 M. Louis Bourgeois Institut Francais du Royaume 
 Uni , progress 2 , Cromwell Gardens , S.W.7 . | 
 subject remain lecture : ( February 9 ) | 
 Gabriel Fauré ; ( February 23 ) Charpentier Reynaldo 
 Habn ; ( March 3 ) Messager ; ( March 9 ) d’Indy 

 annua ! West - End Festival Sunday School | 
 choir place Royal Albert Hall on| 
 February 17 . programme perform 
 choir orchestra thousand Mr. W. H. Scott 
 include selection Beethoven Ruins Athens | 
 Spohr Judgement 

 Bristol 




 EDWIN EVANS 


 128 


 1923 


 INCORPORATED SOCIETY MUSICIANS : 


 annual conference 


 RUTLAND BOUGHTON BETHLEHEM 


 COVENT GARDEN OPERA : NATIONAI COMPANY season 


 MUSICAL 


 1923 129 


 season 


 POLLY : SPIRIT ENGLAND 


 MUSICAL 


 1923 


 FEDERATION MUSIC CLUBS 


 CHORAL SOCIETY 


 P. A. S 


 MODERN VIOLIN PIANOFORTE SONATAS 


 P , A. S 


 A. S 


 


 MUSICAL 


 1923 131 


 E , J. MOERAN 


 singer month 


 BLOCH 7hree jewish poem 


 MUSICAL 


 1925BARNSTAPLE.—At Musical Society concert 
 December 11 choir sing Parry 7here zs Old Belief , 
 Sterndale Bennett come , dive , part- 
 \ Sonata flute pianoforte Barnett , 

 song . 
 ’ cello pianoforte Beethoven , 
 instrumental item . Dr. H. J. Edwards Mr. Sydney 
 Harper conduct 

 BIRMINGHAM.—A Sunday night concert Futurist | 
 Theatre introduce Leeds Trio Birmingham . 
 Mr. A. Cohen leader , Messrs. Hemingway 
 Herbert Johnson colleague . Rachmaninov 7?7e 
 Elégiague beautifully play , desire hear 
 , Franck early f sharp minor Trio , 
 play , offer Franck find | 
 . Mr. William Heseltine , Zhe Zmmortal Hou 
 cast London , hear song . — — 
 Christmas season bring concert Madame Elma 
 Baker . seasonal appropriateness secure draw 
 Christmas music Bax , Vaughan Williams , Holst . 
 Boughton choral arrangement * Holly Ivy ’ | 
 carol Aeth / ehem programme.——Sir Henry | 
 Wood conduct Festival Society usual Boxing - Day | 
 performance 7he AMessiah.——Two day later | 
 work Walsall newly - form Free Church | 
 Choirs Society Mr. Graham Godfrey.——At 
 recital December 22 Miss Rebe Hillier sing Chausson | 
 Chanson Perpetuelle , Paul Beard Quartet Mr. 
 Michael Mullinar co - operate instrumental music . 
 occasion appearance Quartet , | 
 member Messrs. Beard , Cantell , Venton , Dennis , | 
 City Orchestra.——Tchaikovsky Trio 
 Mid - day concert year Miss Marjorie | 
 Sotham Messrs. Paul Beard Johan Hock . | 
 second Miss Dorothy Silk sing Brahms song | 
 group old english song exquisitely refined manner . | 
 — — Kunneke light opera , Zhe Cousin , | 
 wide success continent , | 
 english performance Prince Wales Theatre ! 
 Boxing- Day 

 combine choir orchestra Oxford Cambridge 

 perform Beethoven Mass D. programme 
 probably include Vaughan Williams 7owarads unknown 
 Region , Cyril Rootham Arown earth ( chorus , semi- 
 chorus , orchestra ) , Stanford /r7sh Nhapsody . 
 June 2 - 8 Festival british music hold 
 Cambridge , include kind music 
 16th , 17th , 18th , tyth , 20th century . 
 choral orchestral concert , concert chamber music , 
 unaccompanied choral work , 1isth - century opera , folk 

 dancing , possibly river - procession accompani- 
 | ment Handel Water J / ust-.——Prior Albert Hall 

 concert , C.U.M.S. choir orchestra perform 
 Beethoven Mass D , Cambridge , February 9 , 
 soloist ' English Singers . ' 
 CarRpDIFF.—Madame Tetrazzini , Mr. Lauri Kennedy , 
 Mr. John Amadio , Signor Baggiore , M. Bratza , 
 Mr. Ivor Newton ballad concert December 16 . 
 — — Park Hall concert December 17 
 orchestra play Saint - Saéns Ze Nouet ’ Omphale , 
 vocalist Mr. John Perry Mr. Cuthbert Pardoe . 
 — — Capitol date chief feature 
 Schubert Unfinished Symphony.——At Park Hall 
 concert December 3 ! Litolff Overture A’ohespierr 
 Coleridge - Taylor Ballade minor play 
 orchestra . Mr. Adolphe Hallis , south african 
 pianist , play , Miss Beatrice Miranda sing . 
 CuATHAM.—The Royal Marine Orchestra play new 
 Suite de Ballet , J / y Lady Dragon - fly , Finck , 
 December 11.——On date annual 
 concert Medway School Music Mr. Leslie 
 Mackay Choir , singe Svs / er , awake ( Bateson ) , 
 Elgar /¢ come Misty Ages Love tempest , 
 Austin lady-/ove ( female voice ) , hey nonny 
 ( Armstrong Gibbs ) , Lo ! country sport ( Weelkes ) ( 

 male voices).——On January 2 band Royal 




 SS 


 M.C.A. 


 U.M.S. 


 MUSICAL TILES — FEBRUARY | 1923 133 

 Mr. Lloyd Hartley ( pianoforte ) play Beethoven 
 Ravel . — — Subscription Concert January 7 , Bach 
 Concerto violin perform Miss G. Davey 
 Mrs , J. S. Hartley 

 CRAWLEY.—Parry Pied Piper Hamelin 
 Crawley Ifield Musical Dramatic Society 
 January 4 , Mr. Courtenay Robinson conduct 

 information construction history 

 piece play it.——at Patterson orchestral 
 concert , January 8 , programme include Beethoven 
 C minor Symphony , Strauss Don Juan , Berlioz Carnaval 
 Romain Overture , closing scene Gotterdimmerung , 
 Miss Florence Austral Brunnhilde 

 EX&tTeER.—At annual concert , December 29 , 
 Isca Glee Singers sing interesting arrangement 
 W. J. Cotton ( alto party ) / # sheltered vale , 
 baritone solo trio accompaniment , Schafer 
 come away , pretty maiden , Uatton Summer Eve , 
 Horsley Celia arbour.——The central feature 
 programme perform Cl.amber Music Club 




 MUSICAL 


 134 


 1923 


 IRELAND 


 LEEDS MUSICAL FESTIVAL 


 ( correspondent 


 PRESTON CHORAL SOCIETY 


 ( correspondent 


 


 BRITISH MUSIC SERBIA 


 1923 135 


 AMSTERDAMOf Elgar Variations.—It work high value 
 beautiful style . pseudo - classic expression , remark- 
 able disposition light shade , brilliancy , 
 rich tone - relief , highly complex 

 Symphony.—The principal work _ 
 programme Mr. Dunhill Symphony . whilst 
 direct line succession , lead Beethoven 
 Bruckner , Brahms , Mahler , Symphony 
 free modern exemplification sonata - like style 
 standard symphonic composition . Mr. 
 Dunhill construct characteristic theme 
 suitable development , employ plenty contrast . 
 breadth rhythmic fizesse . work 
 enormous success , Mr. 
 Dunhill composer conductor . lead 
 player quiet moderate persuasive gesture , 
 chef Worchestre reveal _ skill _ fund 
 experience enable hold complete ensemble 
 hand . conductor accomplish 
 rehearsal difficult programme 
 music unfamiliar player 

 reception . Franz Schreker Chamber Symphony 
 programme , repetition bring 
 conviction composer allege superiority 
 present - day composer . concert December 20 
 Corelli famous Concerto Grosso Bach B minor Suite 
 item hearer glad able 
 enjoy . occasion excellent parisian violoncellist , 
 M. Gérard Hekking , masterly reading Schumann 
 seldom - hear Violoncello Concerto . Mengelberg having 
 unfortunately undergo operation , concert 
 conduct M. Dopper , direct concert 
 January 4 , scheme comprise Berlioz 
 Overture Carnaval Romain , Goudoever Suite 
 violoncello orchestra ( youthful composer 
 sustain solo ) , Strauss Don /uan , 
 Saint - Saéns B minor Concerto , play superior style 
 M. Zimmermann . concert Prof. Carl Fiedler 
 fine reading Brahms fourth Symphony . 
 concert January i1 — symphony 
 concert conduct Mengelberg prior departure 
 America — include repetition C. R. Mengelberg 
 Symphonic Elegy Mahler fourth Symphony . 
 vocalist evening Mlle . Mia Peltenburg , 
 win assured position 

 subscription concert Madame 
 Berthe Seroen M. E. Cornelis , January © , prove 
 remarkably successful way . 
 chamber concert Concertgebouw 
 Sextet , player hear Chamber Sonata 
 Handel , Scherzo Dopper , Beethoven Trio , Op . 11 , 
 original version ( clarinet ) , interesting 
 novelty Amsterdam , Josef Holbrooke Sextet , Op . 33 , 
 pianoforte wood - wind , gain cordial 
 reception . W. HARMANS 

 MUSICAL TIMES — FEBrRuaryY 1 




 1923 


 136 


 GERMANY 


 PHILIPP JARNACH , POLYPHONIS1 


 BAVARIA RHENAVIA 


 NEW te deum 


 SINGERS 


 PLAYERS 


 NEW YORK 


 ANN 


 1923 137Mr . Kurt Schindler , Schola Cantorum , 
 Christmas concert present material cull 
 field . number programme , 
 half novelty New York listener . 
 old italian Christmas hymn ; old french Christmas 
 song ; catalonian song ; russian child folk- 
 tune , anthem Rachmaninov , basque folk - tune 
 arrange Mr. Schindler . number boy ’ 
 voice assist choir , song sing 
 original language , Latin , french , catalonian , russian , 
 Basque . Mr. Schindler attention fact 
 catalonian song good example 
 ' migrate folk - song , ' melody sing 
 country 

 recital field indebted Mr. Ernest 
 Hutcheson series great master 
 pianoforte music ( Bach , Beethoven , Schumann , Chopin , 
 Liszt ) , afternoon devote composer ; 
 Mesdames Gerhardt Frieda Hempel 

 remarkable new singer hear 
 Metropolitan year Miss Elizabeth Rethberg , 
 young girl Germany , advance press - agent . 
 freshness beauty voice , wide range 
 abundant power , great surprise audience 
 débiit Aida . 
 successful Sieglinde , simply 
 fully ease , sing role 




 M. H. FLINT 


 ROMEAt Augusteum season open Verdi 
 Requiem Mass , hear Rome 
 1913 . repeat time , magnificent 
 success 

 worthy note time writing 
 Parisian Quartet Capet visit Rome , perform 
 entire series Beethoven Quartets Santa Cecilia 

 LEONARD PEYTON 




 VIENNA 


 CZECH ' peaceful penetration 


 138 


 1923from Philharmonic Orchestra , Vienna boast } little substance . Hindemith late work , String ( Quartet , 
 organization rival superb playing Prague ! Op . 22 , hear private production , considerably 

 Philharmonic Orchestra , Vaclav Talich , recently 
 pay short visit . concert 
 form Vienna - Prague Exchange 
 Concert scheme , provide number 
 concert Prague Vienna Symphony Orchestra 
 February , direction Franz Schalk . frankness 
 compel statement hardly fair deal 
 Vienna Symphony Orchestra , decidedly second - rate 
 hardly suited convey correct idea Vienna 
 orchestral status . hand , Vienna pro- 
 gramme Prague Orchestra , lay particular stress 
 present exclusively czech music , hardly permit 
 judgment player ’ capacity classic , 
 definite pronouncement quality withhold 
 pende opportunity hear play Beethoven 
 Brahms . doubt , , power 
 interpret music compatriot , e.g , 
 Smetana Josef Suk . big Symphony last- 
 , entitle Asrac/ , scholarly work 
 dedicate memory composer wife 
 father , Anton Dvorak , form programme second 
 concert . distinguish quality common tothe conductor , 
 M. Talich , member Orchestra , 
 abandon enthusiasm , temperament 
 racial inheritance . hold true great 
 degree Bohemian String ( Quartet , Suk 
 prominent member . fame ( Quartet long 
 stand , present reorganize form artist 
 live standard excellence 
 traditional . important work present 
 new String ( ) uartet , Op . 35 , Vitezslav Novak , 
 composition moderately modern character 
 entirely free german influence 

 Oscar Nedbal , Furtwiingler predecessor Vienna 
 Tonkiinstler Orchestra prominent figure 
 rumanian musical affair , return Vienna 
 long absence . display old ' athletic ' 
 method conducting , sound musician- 
 ship . selection comic opera /easant /acob — 
 work strongly influence Puccini 
 memory Nedbal operetta — overture 
 Marionette Play Jaromir Weinberger , novelty 
 programme . - piece , write 
 czech composer age seventeen , witty 
 clever counterpart Korgnold ballet , 7%e Snowman , 
 contemporary 




 novelty 


 _ — — _ _ _ 


 


 1923 139 


 operatic event 


 PARIS 


 


 140 


 1923 


 dure month . 


 " T“ONIC SOL - FA serie : 


 publish 


 H. W. GRAY CO . , NEW YORK . 


 HAYDN oratorio 


 creation 


 perform 


 SATURDAY , FEBRUARY 171th , 3 p.m 


 ST . NICHOLAS COLE ABBEY , 


 112 , QUEEN VICTORIA STREET , E.C 


 GOUNOD ORATORIO 


 redemption 


 selection 


 ELGAR 


 KING OLAF 


 


 PARRY 


 bl pair siren 


 perform 


 PEOPLE PALACE CHORAL ORCHESTRAL 


 society 


 N.W.6 


 , 


 INGERS 


 » . WT 


 JESUS CHRIST risen - day 


 


 C. V. STANFORD 


 ALTO . 


 TENOR S , 


 T — — KK 


 


 OO 


 OX 


 CE 


 JESUS CHRIST risen - day 


 SSS 


 


 JESUS CHRIST risen - day 


 PES FES 


 


 JESUS CHRIST risen - day 


 JESUS CHRIST risen - day 


 SS 


 ASA tnt RRR ARTE OE 


 ENS 


 QI 


 F 


 MH 


 NEY 


 { OO 


 Z0IN 


 JESUS CHRIST 1 risen - day 


 JESUS CHRIST risen - day