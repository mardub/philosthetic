


 USIC 


 LM . 


 FORD , 


 1 


 FOUNDED 1844 


 RS 


 FINEST ENGLISH 


 CHAPPELL PIANOS 


 CHAPPELLE P4242 , GALLERIES 


 AGENTS PRINCIPAL TOWNS 


 GLAND 


 HOPKINSON ~ 


 SHORT GRAND 


 XUM 


 PURCELL SOCIETY 


 ALAN GRAY , E M. , .D 


 FOLLOWING VOLUMES PUBLISHED : — 


 SCHOOL MUSIC REVIE 


 ESTABLISHED 1892 


 PUBLISHED MONTH 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 FOUNDED 1844 


 PUBLISHED FI 


 RST MONTH 


 AUGUST 


 1917 


 ROYAL CHORAL SOCIETY 


 ROYAL ACADEMY MUSIC 


 YORK GATE , MARYLEBONE ROAD , LONDON , N.W.-1 


 ROYAL COLLEGE MUSIC . 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W .. -7 


 GUILDHALL SCHOOL MUSIC 


 JOHN CARPENTER ST . , VICTORIA EMBANKMENT , E.C.-4 


 PRINCIPAL LANDON RONALD 


 OPERA , 


 BIRMINGHAM & MIDLAND INSTITUTE . 


 SCHOOL MUSIC 


 SESSION 1917 - 1918 


 13 


 ROYAL COLLEGE ORGANISTS 


 ROYAL COLLEGE MUSIC PATRON FUND 


 ROYAL 


 MANCHESTER COLLEGE MUSIC 


 ASSOCIATED BOARD 


 R.A.M. R.C.M 


 LOCAL EXAMINATIONS MUSIC 


 MAJESTY 


 WIGMORE HALL STUDIOS 


 338 


 LONDON COLLEGE CHORISTERS . 


 UNIVERSITY DURHAM 


 VICTORIA COLLEGE MUSIC , 


 LONDON 


 INCORPORATED 181 . 


 18 , 


 INCORPORATED GUILD ‘ CHURCH 


 MUSICIANS 


 DEAN MANCHESTER . 


 ASSOCIATE ( A.1.G.C.M. ) , LICENTIATE § ( L.1.G.C.M 


 COMPETITIONS , 1917 . 


 CHANT 


 MR . W. H. BREARE 


 STUDENTS PROFESSION 


 LONDON COLLEGE MUSIC 


 EXAMINATIONS — LOCAL HIGHER 


 TECHNIQUER 


 PUBLISHED 


 CHRISTIAN YEAR 


 LISTER R. PEACE , B.A. , F.R.C.O 


 NE 


 KALENDAR 


 


 LONDON : 


 


 F.R.C.O , 


 IER , 


 ANTAL 


 E.C.-4 


 XUM 


 339 


 SEND FREE BOOK : 


 CENTRAL DEPOT RUSSIAN MUSIC 


 J. & W. CHESTER 


 TWOPENCE . 


 PROFESSIONAL NOTICES . 


 MR . SAMUEL MASTERS 


 TENOR ) . 


 COMPOSERS ’ MSS 


 REVISED PREPARED PRINTING 


 EVISION MUSICAL COMPOSITIONS 


 PECIAL CORRESPONDENCE COURSES : — 


 A.R.C.M. . 1914 - 17 23 ” ” ” 


 F.R.C.O. SPECIALIST CORRESPONDENCE 


 ADAME LARKCOM , F.R.A.M 


 DR . LEWIS ’ TEXT - BOOKS 


 RTHUR MANGELSDORFF , L.R.A.M 


 RGANIST CHOIRMASTER WANTED 


 EORGE STREET CONGREGATIONAL 


 WJ USIC - TEACHING CONNECTION 


 STRENGTHEN , LOVE , CASTLE 


 FUGUE E MAJOR 


 


 VILLAGE ORGA 


 BOOK 30 . 


 INTERLUDES 


 KEYS . 


 SPECIALLY USEFUL DEPUTY ORGANISTS 


 NIST 


 “ AKT ORGAN PLAYING 


 GRADUATED 


 > — 


 T “ 8 


 POPULAR HARVEST ANTHEMS } & 422 


 M POSER 


 NG 


 341 


 


 10 


 XUM 


 ORGAN 


 SHORT PRELUDES 


 POPULAR 


 


 ORGAN 


 VOLUME 2 


 MARCHES 


 342 


 1917 


 TRINITY COLLEGE MUSIC 


 EARL SHAFTESBURY , K.P. 


 RT . HON 


 PUBLISHED 


 HANDBOOKS MUSICIANS . 


 


 THEORY HARMONY . 


 MATTHEW 


 SHIRLAW , 


 F.R.C.O 


 _ _ 


 BROADWOOD 


 PIANOS 


 BOSWORTH EDITION 


 LYRIC ORGANIST 


 COLLECTION 


 STANLEY ROPER 


 RECITATION MUSIC 


 STANLEY HAWLEY 


 BOSWORTH & CO , , 


 8 , HED DDON N STREET , REGENT STREET , LONDON , W 


 PRIMER 


 ORGAN 


 


 F. ELLINGFORD 


 HERBERT 


 XUNM 


 


 343 


 SINGING - CLASS CIRCULAR . 


 AUGUST 1 , 1917 


 GRANADOS ‘ GOYESCAS . ’ 


 XUM 


 YLIM 


 1917 . 345 


 _ _ 


 XUM 


 346 


 SOPRANO . — 


 ALT 


 ES 


 , 1917 . 347 


 FOUNDATIONS TWENTIETH 


 CENTURY MUSIC 


 INTRODUCTORY 


 - DAY ACADEMICSAcademic minds , , varied type . 
 politics , conservatives pro 
 gressives ; groups ; finally , one- 
 man academies . common denominator 1 
 reliance system admits 
 embodied code . country 
 prominent conservative academics , 
 greatest proficiency generation bees 
 attained Russia sequence reaction 
 imperfect technique Nationalist 
 pioneers , found impediment 
 free expression . significant composer 
 , youth , looked hope 
 Nationalist nicknamed ‘ little Glinka , ’ 
 strongest representative academic music 
 Russia . circumstance practical 
 guide writing orthodox traditional polyphony 
 Russian book corroborative evidence , 
 demand creates supply 

 Progressive academism present ( 0 
 find French soil congenial Europe , 
 largely consequence César Franck having 
 home Paris . position musical history 
 singular . Beethoven died compatriots 
 singularly obtuse reading mus ! 
 testament , described problem 
 unity cyclic form solved means 
 Beethoven variation . German symphonists 

 evaded point altogether . left 




 349Liszt - Wagner , treat zsthetically 
 composer , resuscitate field , 
 throw German criticism scent 

 Rand led Beethoven variation 

 called metamorphosis themes regarded 
 antagonistic identical . attempt 
 set Wagner opposition Beethoven 
 tradition , represent Brahms , 
 humorous episodes I9gth - century music . 
 symphonic field César Franck 
 built securely foundations 
 projected Beethoven , led France 
 form academicism , whilst liberal 
 progressive respect , sternly 
 uncompromising question formal 
 construction . distinguished head Schola 
 Cantorum accepts innovations fill orthodox 
 mind horror , turn away equal 
 disgust orthodox musical texture failed 
 fulfil certain formal conditions . short , motto 
 Franckist tradition France loyalty principle 
 combined utmost liberality detail — 
 words , progressive academicism . motto 
 slightly exclusive principles apply 
 considerable proportion modern French music out- 
 Franckist tradition , working 
 French mind passionately logical . 
 gives impression jumping 
 conclusion , truth intermediary 
 processes thought omitted 
 equally obvious listener worth 
 stating . great deal unexpected French 
 harmony arises manner , modify 
 fundamentally academic character 

 Atypical instance - man academy Scriabin , 
 ingenious development use chromatic 
 alteration led creation personal code system 
 uncompromising 
 dogmatic conservatives . capricious 
 theorist set hypothesis 
 compelled standardise harmonic stage 
 arrived extension 
 process brought point 
 departure 




 NATIONALISM 


 XUM 


 LANGUAGE 


 35 


 POSTSCRIPT MECHANICAL INSTRUMENTS 


 1917 


 ORIGIN MAJOR MINOR 


 MODES . 


 FIG . 


 C D E F | G B C 


 FIG . 7 . 


 24 22 20 18 ( 18 ) 15 13 12 


 14 : 16 , | 


 


 , I9QI7 . 353 


 FIG . 9 


 18 16 15 13 12 11 10 9 


 FIG 


 10 


 16 


 XUM 


 18 20 


 FIG , 11 . 2 


 - — - - - _ — — _ = — — ~ _ 


 , 355 


 1917 


 ANCIENT PIPE - SCALES 


 PRESERVED 


 


 XUM 


 SICILIAN FLUTE 


 EVOLUTION WIND INSTRUMENTS 


 POTENTIALITIES PROGRESSIVE MUSICIANS 


 , 1917 


 REBIKOV MANTLE . 


 1912 


 REBIKOFF : LES ACCORDS EN QUARTES . 


 PERFOR 


 RIGI 


 SETTLE 


 OBITE : 


 DICTU : 


 — SS 


 RIGHTS 


 DISPUTE : 


 SETTLEMENT 


 5 0 


 OBITER 


 DICTUM 


 XUM 


 RE£BIKOF , 1900 


 LISZT ORGAN 


 * B - - C- ] 


 359 


 XUM 


 , — _ — _ — _ — — _ — 


 SSE 


 PETITION GRACES REVEREND 


 ARCHBISHOPS CANTERBURY YORK 


 XUM 


 CHU 


 , 361 


 1917 


 CHURCH MUSIC CONFERENCE EALING 


 ORGAN MUSIC TEMPLE CHURCH 


 ORGAN RECITALS 


 APPOINTMENTS 


 SCHOOL SONGS 


 CHARLI 


 J. W.I 


 HENRY 


 SYDNE ’ 


 W. H. 


 STUDENTS ’ CONCERTS ALIEN MUSIC 


 EDITOR ‘ MUSICAL TIMES 


 XUM 


 364 


 1917Whilst wishing credit efforts 
 English composers develop national 
 lines create English School , efforts 
 hope crowned later success ( 
 way Russians found , musically 
 speaking : outcome years ’ earnest intense 
 striving ) , whilst desirous reasonable 
 seasonable , useless deny time 
 arrived good important claim . 
 charming salon pieces , 
 special work special composer 
 said , ‘ epoch - making — masterpiece 
 world - value future generations study 
 enthusiasm teachers thankful teach 

 assuming transcendent planetary 
 works actually exist parity classics ( 
 Mr. Legge prefers dub ‘ foreign ’ ) , possible 
 student develop musically nation output ? 
 Zs present generation debarred privileges 
 mustcal existence generations profited , 
 including Mr. Legge ? 
 grandchildren speaks denied 
 study Bach Fugue Beethoven Sonata , 
 withheld joy hearing Mozart opera , 
 maturer happiness playing Chopin Ballade ? 
 ( composer taboo , seeing 
 birthplace ‘ enemyised 

 Assuming war Spain Holland , 
 deny art students benefit inspiration 
 ensue study Vandyck , Hobbema , 
 Rembrandt ? ‘ Young man , likewise 
 young woman , , visiting exhibitions galleries , 
 expected dodge enemy tainted canvases , avoiding 
 direct gaze picture save painted native 
 artists . Cooper , Tadema , Stone , Lavery , Orpen , 
 like suitable vision present time , 
 seemly artistic development 
 based purely patriotic local lines 




 ORGAN ACCOMPANIMENT 


 ‘ CATHEDRAL TRADITION . ’ 


 EDITOR ‘ MUSICAL TIMES 


 EDITOR ‘ MUSICAL TIMES 


 XUM 


 ORGAN 


 ULAY 


 XUM 


 SHORT ANTHEM 


 ALTO 


 ~ — _ — - _ 


 S — OS ES 


 GG — — = = = = = . E — — 


 SENS 


 XUM 


 HHH 


 SS 2 SSS S- 2S SS SS SS 


 1917 


 2S SSS : 


 CHOI ! 


 


 WLM 


 , 369 


 * 4 - 0 — = 


 | * MEISTERSINGER 


 CHOIR - TRAINERS ANCIENT MODERN . ’ 


 ROME 


 ' MSION 


 XUM 


 ARTHUR T , FROGGATT 


 MASCAGNI NEW ‘ SATANIC RHAPSODY 


 1917 


 TOSCA . ’ 


 LEONARD PEYTON 


 XUM 


 


 TQI7 . 371 


 BEECHAM OPERA DRURY LANE 


 THEATRE 


 MARRIAGE FIGARO . ’ 


 XUM 


 QUEEN HALL PROMENADE CONCERTS 


 COBBETT COMPETITION , 


 1917 


 MR . H. A. FRICKER TORONTO 


 APPOINTMENTS 


 MUSIC WAR - TIME COMMITTEE : 


 HOSPITAL CONCERTS 


 £ 1,145 


 # 


 \CADE 


 MUSIC 


 ISSEX 


 ; 373 


 1917 


 ASSOCIATED BOARD ROYAL 


 \CADEMY MUSIC ROYAL COLLEGE 


 MUSIC LOCAL EXAMINATIONS MUSIC 


 LATE DR . W. H 


 CUMMINGS 


 LIBRARdirections ) , Sonatas Locke , 
 handwriting Purcell ( £ 29 ) , composer 
 manuscript music Yorkshire Feast ( £ 32 

 Organ book , dated 1448 , Adam Ileborgh , Kendal 
 ( 421 , Mr. Tregaskis ) ; Mendelssohn autograph copy 
 ‘ Midsummer Night Dream ’ ( Scherzo , Notturno , 
 Wedding March ) , ( 471 ) ; Mozart , autograph copy 
 Cadenzas Pianoforte Concertos ( £ 36 ) ; Johann 
 Sebastian Bach , autograph score ‘ Allein Gott der 
 Hoh ’ sei Ehr ’ ) 498 ) ; Schubert ‘ Salve Regina , ’ autograph 
 score , signed 1819 ( £ 52 ) ; Beethoven Grand Trio 
 E flat ( violin , viola , ‘ cello ) , Op . 3 , autograph ( £ 98 ) ; 
 William Byrd , ‘ Psalmes , Sonets , Songs Sadness , 
 Musicke parts , ’ dated 1588 ( £ 34 ) ; 
 Thos . Ravenscroft , ‘ Deuteromelia , ’ dated 1609 ( 416 ) ; 
 Coperario ( Giov . ) Thom . Nupo , Fancies , parts , 
 - G. Coperario ( John Cooper ) 
 Thomas Nupo , manuscript , 228 pages 
 ( £ 40 , Quaritch ) ; Dr. Wilson , music songs ‘ 
 Tempest ’ ( manuscript copied Playford ) , ( 458 , Maggs ) ; 
 1 cambric hand - rufile ( 44 - . 10 - ) , fragment 
 uf Handel clothing extant ( £ 10 10s 

 - TONE SCALE 




 OH ROCK , JULIF . 


 MUSICAL 


 IQI7 


 ROYAL ACADEMY MUSIC 


 DIS PRIZES 


 ROYAL COLLEGE MUSICTHE GUILDHALL SCHOOL MUSIC 

 concert given ( Queen Hall July 3 afforded 
 incontestable evidence value instruction afiorded 
 establishment . programme long , 
 elect mention excellent performance Miss 
 Dorothy Davies Miss Kathleen B. McQuitty Saint- 
 Saéns Variations theme Beethoven , 

 July 14 performance England given , 
 String quartet minor composed Signor Scontrip 
 principal Florence Conservatoire Music , 
 turned important contribution chamber mug , 
 hope secon 
 hearing , trust afforded autum 
 season 




 WIGMORE HALL 


 STEINWAY HALL 


 TRINITY COLLEGE MUSIC 


 SZQLIAN HALL 


 CORRESPONDENTS 


 BIRMINGHAM 


 1917 


 BRISTOL 


 BOURNEMOUTHremain dealt , , concerts took | ‘ Elijah ’ March 23 

 lace prior instrumentalists ’ temporary disbandment . 
 chief interest sixth summer Symphony Concert 
 centred Bournemouth performance 
 Beethoven early Pianoforte Concerto D , posthumous 
 work written composer 
 fourteen years old . Detailed criticism immature 
 composition serve useful purpose , 
 faithfully avow dulcet easily comprehended 
 strains , reminiscent Beethoven early 
 exemplar , Haydn , means lacking pleasurable 
 qualities . appreciation , , enhanced 
 exceedingly polished musicianly reading 
 solo Miss Edith Ashby , highly efficient Winter 
 Gardens accompanist . programme included Arthur 
 Somervell ‘ Thalassa ’ Symphony ; Overture , ‘ L’Epreuvg 
 Villegoise , ’ Grétry ; Sibelius - worn Valse Triste ; 
 Goring Thomas aria , ‘ heart weary ’ — 
 charming number sung good style 
 Miss Nellie Walker . decidedly attractive programme , 
 included Haydn Symphony . 2 , D , 
 Ewna ‘ Hans Andersen ’ Overture , Svendsen ‘ Norwegian 
 Carnival , ’ Prowinsky ‘ Passing Serenade ’ ( orchestrated 
 Sir Henry Wood ) , ballet music 
 Borodin ‘ Prince Igor , ’ arranged following 
 week . soloists appeared — violinist , Madame 
 van den Eeden , played Beethoven Romance F 
 tastefully somewhat small tone ; Miss 
 Winifred Williamson , scored success Ambroise 
 Thomas aria , ‘ Connais tu le pays . ’ interpretation , 
 , impressive 
 thythm elastic , adhered 
 little closely tradition . July 4 , Mr. F. 
 King - Hall , leader Orchestra , conducted absence 

 Mr. Godfrey , programme follows : Overture , 
 ‘ Cosi fan tutte ’ ( Mozart ) ; Symphony D _ minor 
 ( Schumann ) ; Suite . 1 , ‘ Peer Gynt ’ ( Grieg 

 Bristol Musical Club , notwithstanding absence 
 members service , successful 
 season musically . excellent programmes 
 forthcoming , brought season close 
 July 17 included Pianoforte Quintet D Fauré , 
 Paderewski Sonata minor , pianoforte violin , 
 violin solos Tchaikovsky Kreisler . 
 meetings resumed September 25 

 Ladies ’ Musical Club , founded 1906 
 Misses Fyffe , holds meetings Royal West 
 England Academy , satisfactory season , 
 membership having reached highest level ( 140 ) , 
 programmes chamber music having proved 
 enjoyable . outstanding event engagement 
 London String Quartet , concert resulting small 
 balance hand . lectures Miss 
 Denneke Beethoven Schumann , musical illustra- 
 tions . season closed June 29 , 
 meetings resumed autumn probably 
 decided invite London String Quartet second 
 concert 

 DEVON CORNWALL . 
 DEVON . 
 June Torquay Municipal Orchestra 

 madrigal class , Plymouth destitute choral Societig 
 public purposes 

 Band H.M. Grenadier Guards , Capt . 4 
 Williams , toured Devon , playing ( noted ) g 
 Exeter , Torquay July 6 , Plymouth July 7 and§ 
 Exmouth July 9 . Immense crowds flocked th 
 Pier Pavilion Plymouth concerts . band 
 recently returned months ’ sojoun 
 France , played accustomed finish perfectiq 
 ensemble tone effect . principal items , 
 Grieg Lyric Suites , Op . 54 , Beethoven ‘ Leonon 
 Overture , . 3 , ‘ Finlandia ’ tone - poem ( Sibeliny , 
 Schubert ‘ Unfinished ’ Symphony , Suit 
 * Esquisses Caucasiennes , ’ Ippolitov - Ivanov . Devon 
 port , July 5 , Gloucester Street Ladies ’ Choir gave , 
 concert church funds conducted Miss Davey th 
 organ ; board H.M.S. ‘ Indus , ’ July 10 , bo . 
 artificers gave concert . group children performed 
 operetta ‘ Fays Elves , ’ patriotic songs tableau , 
 characteristic songs dances , Peverell ( Plymouth ) , 
 July 11 

 CORNWALL 




 MANCHESTER DISTRICT 


 XXXII 


 YXLIM 


 1917 


 NEWCASTLE DISTRICT 


 LOST MANUSCRIPTS 


 XUM 


 PUBLISHED 


 H. W. GRAY CO . , NEW YORK 


 


 MONTH 


 C ' YLERIDGE - TAYLOR , 


 MUSICAL TIMES . 


 | SCALE TERMS ADVERTISEMENTS 


 OWITZ , ISABEL . 


 VIMEY , 


 50 , SPECIAL NOTICE . 


 OHN £ 


 NTS 


 ST 


 1917 . 375 


 ANTHEMS 


 TRINITYTIDE 


 COMPL ETE LIST 


 “ LUTE : 7 SERIES 


 EDWARD BUNNETT 


 ANTHEMS . 


 SERVICES 


 POPULAR CI 1 U RCH MUSIC 


 SERVICES . 


 SERVICE G 1S 


 ORGAN . 


 CHURCH NTAT A. 


 XUM 


 LS 


 HARVEST FESTIVAL MUSIC 


 CANTATAS . 


 SONG THANKSGIVING HARVEST CANTATA 


 SOPRANO , TENOR , BASS ( CONTRALTO ) SOLI SOPRANO ( TENOR ) CONTRALTO ( 


 CHORUS BARITONE ) SOLI CHORUS 


 ” ROSE DAFFORNE BETJEMANN 


 WORDS WRITTEN ARRANGED 


 MUSIC 


 MUSIC — — 


 SOPRANO TENOR SOLI CHORUS 


 GOLDEN HARVEST 


 TENOR BASS SOLI CHORUS JOHN . WEST . 


 THOMAS ADAMS , CHORUS ORCHESTRA 


 


 SMALL ORCHESTRA SOLO VOICES , CHORUS , ORCHESTRA 


 


 HUGH BLAIR . C. M. VON WEBER . 


 HARVEST CANTATA HARVEST SONG 


 CHORUS , SEMI- — ORGAN SOPRANO SOLO CHORUS 


 


 G EORGE G ARRETT . C. LEE WILLIAMS . 


 HYMNS HARVEST SOWER WENT FORTH SOWING 


 : 7 O LORD HEAVEN , EARTH 


 JOY HARVEST SEA 


 HARVEST HYMN PRAISE COME , YE THANKFUL PEOPLE , COME 


 LONDON : NOVELLO COMPANY , LIMITED 


 SOLU 


 XUM 


 O ( 


 EST 


 RUS 


 ING 


 SST 


 RA 


 


 332 


 1917 


 PUBLISHED 


 


 NEW SONG 


 EDWARD GERMAN 


 CHARMING CHLOE 


 W 


 ROBERT 


 RDS 


 BURNS 


 


 SOUTHERN SLAV SONGS 


 1 . SHEPHERD HILL , 


 2 . THOUGHTS . 


 SRGJAN TUCIC . 


 FANNY COPELAND . 


 PERCIVAL GARRATT 


 ALANNAH 


 SONG 


 WORDS 


 COUNTESS BARCYNSKA 


 MUSIC 


 EATON FANING 


 KEYS 


 LOVE DWELT 


 NORTHERN LAND 


 ( ROMANCE 


 EDWARD ELGAR 


 LISHED 


 NOVELLO SCHOOL SONGS . 


 BOOK 258 


 FAIRY STORY GAME - SONGS 


 IDA M. CARTLEDGE 


 NOVELLO SCHOOL SONGS , 


 SONGS TIMES 


 WORDS MUSIC 


 ETHEL BOYCE 


 . SEA 


 REFLECTIONS . | 4 . DANCING RHYME 


 5 . ENGLISH HYMN 


 STAFF NOTATION TONIC SOL - FA 


 NOVELLO SCHOOL SONGS . 


 ORCHARD RHYMES 


 NURSERY RHYMES ACTIONS 


 


 PERFORMANCE 


 260 


 ETHEL 


 CONSECUTIVE 


 CONTENTS . 


 XUM 


 PUBLISHED 


 HIAWATHA WEDDING - FEAST + 


 > . COLERIDGE - TAYLOR 


 ARRANGEMENT PIANOFORTE SOLO 


 


 JOHN POINTER 


 NAVAL MILITARY 


 MIUSICAL UNION SONG BOOK 


 COLLECTION - SONGS MEN VOICES ( T.T.B.B 


 PRICE SHILLING SIXPENCE 


 3 . ANDA 


 XUM 


 OHN SE STIAN 


 JOHN SEBASTIAN BACH 


 VF 


 INTRODUCTION ERNEST NEWMAN . 


 CONTENTS . 


 


 INTY 


 ORGAN 


 > - NUS 


 COMPOSERS . 


 PUBLISHED 


 COLLECTED 


 SELECTION 


 FOLK - SONG 


 ANGED B 


 CECIL J ] . SHARP R 


 PRICE 


 VAUGHAN WILLIAMS 


 SHILLINGS 


 VOCAL PARTS ( 


 ONTENTS . 


 


 SPIRIT ENG LAND 


 


 LAURENCE 


 POE 


 SET MUSIC TENOR SOPRANO SOLO , CHORUS ORCHESTRA 


 


 EDWARD ELGAR . 


 PRICE SHILLINGS SIXPENCE 


 SEPARATELY 


 COMPANY , LIMITED 


 PUBLISHED 


 MS 


 BINYON 


 XUM 


 FEMALE BOYS ’ VOICES 


 WHATSOEVER THINGS WRITTEN AFORETIME 


 ENGLAND 


 EASY ANTHEM HARVEST 


 WORDS ALICE FLOWERDEW 


 COMPOSED 


 — . - _ 


 = = SS 2 


 FATHER MERCIES , GOD LOVE 


 ™ — _ — _ \»—- SS CO 


 SSS = SS 


 - - , — NS — - — - — _ @—- — — ---- — — — 


 & - — — - _ — _ — — - — _ — ” = — — — — 


 TTT 


 XUM 


 SERIES 


 NOVELLO 


 NTHEMS 


 SHORT 


 SHORT 


 LONDON 


 NOVELLO 


 COMPANY 


 EASY ANTHEMS 


 LIMITED 


 FATHER MERCIES , GOD LOVE . 


 ‘ 5 BSS 


 XUM 


 FATHER MEROIES , GOD LOVE 


 — ES — = = = 


 - _ — + — > _ — _ — _ — - 


 - — — } _ — — } — — — — J — — — - 


 FNS 


 44 


 LY 


 XUM 


 XUM 


 FATHER MERCIES , GOD LOVE 


 = 2 SS SS EE 


 CEN , ' 


 . 4 — _ — — — - — — — — - — _ — _ — - — < < — ' _ 


 — _ _ 


 OA 


 WY 


 FATHER MERCIES , GOD LOVE . 


 SS 


 ROYAL ACADEMY MUSIC 


 YORK GATE , MARYLEBONE ROAD , N.W.-1 


 INSTITUTED 1822 INCORPORATED ROYAL CHARTER , 1830 


 


 SECTION A.—TECHNICAL PRACTICE . BOOKS 


 SECTION B.—STUDIES . BOOKS 


 CLOTH BINDINGS 


 PIECES 


 MAUVE ENGLAND 


 XUM