


 the MUSICAL time 


 MASON and HAMLIN ’s 


 CABINET ORGANS 


 W. J. WESTBROOK 


 RICHARD REDHEAD . 


 2 HENRY W. GOODBAN 


 ILLUSTRATED list post free on application 


 METZLER and CO 


 37 , GREAT MARLBOROUGH STREET , LONDON , W 


 professional notice 


 MR . HENRY WATSON , 


 MR . WILBYE COOPER 


 USIC engrave , print , and PUB . 


 RUSSELL 's MUSICAL instrument . 


 RASS , REED , STRING , and DRUM . and FIFE 


 MISS ELLEN HORNE 


 2"mr . WALTER J. MARKLEY , 


 rateful — comporting 


 _ COCOA . 


 395 


 RGAN , PIANOFORTE , HARMONIUM 


 HE LORD be RISEN indee , HALLELUJAH ! 


 396 


 work by JOHN HULLAH 


 fourth thousand 


 SINGING card 


 the SINGING CARD modulator 


 OR teacher ’ assistant 


 fourteenth edition . 


 the collegiate and SCHOOL 


 sight singe manual 


 ( companion work to the above 


 COLLEGIATE VOCAL TUTOR 


 TO CHORAL SOCIETIES 


 W. H. BIRCH ’S CHRISTMAS MUSIC . 


 MR VERNON RIGBY 'S NEW song . _ 3 


 HE SIREN ’S JUBILEE . A 


 OUNO ! 


 ELMO 


 OPKIN 


 OPKIN : 


 ONS , DR 


 ENT . — * 


 ONK , DR 


 ONK , W. 


 DIUTMAN 


 EWELL , J , 


 TROU D — « 


 THORNE , & , 


 URLE , JAM 


 RIMNELT , | 


 VISE , M , — « 


 the MUSICAL TIMES 


 WE 


 JEFFERYS , 57 , BERNERS ST . 


 the CHORAL SOCIETY 


 a collection of 


 edit by 


 MICHAEL WATSON 


 FELIX GANTIER 


 STEPHEN GLOVER . 


 most popular M , ARCHES 


 LOUIS DUPUIS , 


 CATHEDRAL GEMSROTTERDAM . — Fantasia , on subject from Haydn 's 8rd 
 ( Imperial ) Mass ... 8 0 

 4 . MALAGA . pe © on subject from Beethoven 's Mass 
 in C 8 0 

 5 Roven — Fantasia , on : subject from Gounod ’s Messe 
 Solennelle 8 0 
 6 . CAEN — Fantasia , on subject from Rossini ’ s Stabat Mater 3.0 




 C. JEFFERYS 57 , BERNERS STREET ( W 


 the MUSICAL TIMES , 


 T. PETER ’s , STREATHAM , S.W.—TWO choir 


 _ _ _ _ SOUTHWELL COLLEGIATE CHURCH . 


 399other musician , from Graun to Mendelssohn , have treat 
 other Scripture subject in oratorio that be freely , perhaps , 
 but obviously , frame upon the Bach model . Spohr , Schneider , 
 and some other have write oratorio , didactic and dramatic , 
 to scriptural text and modern , wherein they have reject 
 the ancient german form . the majority of these refer not 
 to particular occasion , but , though they be not rarely give 
 in concert room , they be perform also in church at 
 festival and peculiar celebration 

 the above account reter to North or Reformed Germany . 
 in the South , where the old communion still prevail , the 
 oratorio be mainly represent by such work as the " Creation " 
 and the ' ' season " of Haydn , and the " Mount of Olives " of 
 Beethoven , which be , indeed , not write for Church use . 
 they be more or less secular in character , sometimes in their 
 subject , sometimes in the text which embody this , sometimes 
 in their musical structure . for the most part , then , though 
 not inadmissible , they be not wholly desirable for church 
 use , and their performance within the sacred precinct be the 
 exception rather than the rule 

 in England , the oratorio be introduce by Handel . this , 
 truly , be in a somewhat secular form , and for the entirely 
 secular purpose of divert the company gather in the 
 magnificent mansion of the Duke of Chandos . Racine ’s 
 tragedy of ' ' Esther " be freely translate , with great 
 abridgment , into English . this Handel set to music ; and 
 it be first perform privately at Cannons , there be reason 
 to suppose with action , August 29 , 1720 . it be twice 
 give , also privately , in London , in 1731 . a public perform- 
 ance , in the chief concert room of the day , be announce in 
 1732 , which the composer anticipate , though he could not 
 prevent , by the advertisement of a performance of the work on 
 his own account , at the King ’s Theatre , on the 2nd of May . 
 popular repugnance to the presentation of a biblical story 
 on the stage of a theatre be then so strong , that the adver- 
 tisement in question bear the air of apology in every line . 
 " by his Majesty ’s command , " it begin — a justification 
 which would silence if not overturn every objection , and 
 which show that George II . approve of the class of compo- 
 sition . observe also the following most significant postscript : — 
 " N.B , there will be no acting on the stage,”—a guarantee 
 of the untheatrical nature of the exhibition — « but the house 
 will be fit up in a decent manner for the audience ; ' " to 
 describe this manner and certify its decency would be prolix , 
 but what ensue be of consequence as refer to the serious , 
 if not churchlike , appearance wherewith the theatre be in- 
 vest : ' the music to be dispose after the manner of the 
 Coronation Service . " now , be it remember , the story of 
 ' " ' Esther " be consider sacred , because of the book in which 
 it be find , but have nothing in its nature or incident to war- 
 rant the consideration ; and remember , too , that the poem 
 on which this oratorio be found have no pretension to he 
 anything other than a Grama . mystify by the prevarica- 
 tion show above , and sanction by the royal example the 
 world go in vast number to witness the ' Oratorio in 
 English , " and it have complete success 




 401 


 4q2 


 403cenit 

 vena — 
 be the CRYSTAL PALACE . 
 TMANG Tux Concert on the 10th ult . be in the high degréé 
 \ssohni jateresting , for not only be Dr. Ferdinand Hiller the solo 
 Phany ; janist , but the occasion be select for the first perform- 
 8 Con . ance in this country of his " Symphonische Phantisie , " a 
 nd , " the | work already well know in Germany . on a single hearing 
 i 's , the fit would be impossible to do more than record the favourable 
 suitably § impression produce upon we by listen to a composition 
 Egypt ' | of such pretension . the prevalence of the minor key be , 
 | we presume , intentional , for there can be little doubt that 
 mf féw Germans of the present day write without the definite 
 Vet . jdea of embody in music a continuous chain of thought ; 
 xpresaly and ; therefore , in the absence of any publish " argument , " 
 of the we can only imagine that the brilliant last movement be de- 
 Bach 's sign to express the final release from a train of vague and 
 mand itated feeling , the indication of which in the early part 
 ere the ef the work be too vividly colour to be doubtful . although 
 somewhat partake of the form of a Symphony , there be no 
 ak between the movement , and this perhaps invest the 
 : on the tamposition with a certain heaviness which be . slightly 
 to wit | detrimental to the due enjoyment of the many beauty with 
 ould be which it abound . the instrumentation be charming , the 
 ane by B orchestra be handle with a mastery which command 
 nseemly b theattention throughout , and the rich scoring of thé Finale , 
 county § in which the brass instrument be most judiciously . use , 
 uditory , # elicit a storm of applause from the general audience , with 
 . bette , which the composer , who — we the Symphony , must 
 vursday , assuredly have feel flatter . r. Hiller ’s performance of 
 oe Mozart 's Pianoforte Concerto in A be a marvel of delicacy 
 t people and executive power , and his cadenza show not only his 
 rfect command of the instrument in every conceivable 
 gene m of passage , but his reverence for the composer whose 
 irection b work ' he be interpret , for throughout this brilliant 
 g of . improvisation Dr. Hiller be always secondary to Mozart . 
 imp : : 
 lesson , two little piece , write expressly for this Concert by Dr. 
 I y y , 
 ante and ler , a Ghazel and Waltz — the former expressive of a form 
 ) . Pp 
 rpente , § of verse find in persian poetry — be afterwards play 
 re , Dean ithe composer , and receive with that applause which 
 1d , orit Sates as elegant and refined can scarcely fail to command . 
 repait the vocalist be Madame Lemmens - Sherrington and 
 ignor Agnesi . 
 - n the 17th ult . the chief feature : in the Concert be the 
 exquisite performance of Beethoven ’s Concerto in G , by 
 n fal MedameSchumann . Rubinstein ’s " Don Quixote , " call 
 in ' the programme a " Humoreske , " for orchestra , be a 
 ver dep welcome novelty . it be a clever but , as its title imply , an 
 ily affetm eccentric work , and one , we think , that deserve a second 
 not tom bearing . 
 et which 
 ar music , the ORATORIO concert . 
 thatow = abthe fifth Subscription Concert , on Tuesday , ' the 6th 
 for the ult , Mendelssohn ’s " Lobgesang , " and Rossini ’s ' " Stabat 
 Gibbom — Mater , " attract a large audience . unfortunately , both 
 ion ~Madame Rudersdorff and Mr. Sims Reeves be indisposed , 
 storation , 
 and the whole of the trying soprano music be undertake 
 ose glory . ying soy 
 jevent#m PY Miss Annie Sinclair , whilst the tenor part be entrust 
 Croft , toMr . Raynham . much as we have always admire Miss 

 Sinclair , we scarcely give she ' the credit of be able so 
 successfully to acquit herself of a duty so arduous as that 
 which féll to she share on this occasion ; but chance like 
 thesé have sometimes make the fortune of young vocalist , 
 aid we be bind to say that her brilliant and thoroughly 
 deserve success make we entertain the most sanguine 
 hope that one more accomplished and conscientious artist 
 be add to our list of Oratorio singer . in the " Lobgesang " 
 her rendering of the soprano solo in the opening chorus , of 
 the duet ( with Miss Julia Sydney ) , " I wait for the Lord , " 
 and of the exquisite phrase , with the long sustain high a 




 ROYAL ACADEMY of MUSIC 


 404 


 MR . HENRY LESLIE ’S concert 


 405 


 EXETER HALL . 


 the SEVENTH SUBSCRIPTION CONCERT 


 TUESDAY , MARCH 5 , 1872 , 


 HANDEL 'S 


 ISRAEL IN EGYPT 


 MADAME LEMMENS - SHERRINGTON , 


 MISS HELEN D’ALTON , 


 MR . SIMS REEVES , 


 MR . WHITNEY 


 AND 


 HERR STOCKHAUSEN . 


 MR . BARNBY 


 EXETER HALL . 


 fourth season , 1871 - 2 . 


 the EIGHTH SUBSCRIPTION concert 


 ON 


 TUESDAY , MARCH 19 , 1872 . 


 HANDEL 'S 


 MESSIAH 


 MADAME LEMMENS - SHERRINGTON . 


 MISS JULIA ELTON . 


 MR . SIMS REEVES . 


 MR . WHITNEY 


 HANOVER SQUARE 


 will be give on 


 FRIDAY , MARCH 22 , 1872 . 


 when 


 BACH ’s passion , 


 ‘ accord to 


 SAINT JOHN , 


 will be perform for the first time in LONDON 


 MADAME CORA DE WILHORST . 


 MADAME BENTHAM FERNANDEZ . 


 MR . ARTHUR WADE 


 AN 


 HERR STOCKHAUSEN 


 new number 


 from the score of the great MASTERSNo . 82.—Chaconne ( f major ) . Handel ; Andante con Variazioni , and 
 Romanza ( op . 3 . ) , Weber 

 no . 83.—funeral March ( op . 26 ) , Beethoven ; March ( b minor , op . 
 97 ) , Schubert ; Hunting Song ( op . 82 ) , Schumann ; Adagio ( op . 10 ) , 
 Weber 

 no . 84.—Fugue ( in g minor ) , Mozart ; Marche Triomphalo , Mos- 
 cheles ; Fantasia , from the Sixth Quartet , Haydn 




 arrange for the ORGAN , 


 MENDELSSOHN ’s 


 LIEDER OHNE WORTE 


 BASTER ANTHEM 


 TREBLE . : : 


 F7N UP ) t 2 . 2 2 . 


 — _ _ - _ — — 


 SSS 


 ss 


 PP 


 QD 


 HE 1 RISEN 


 ' U3 L 


 HE be RISEN 


 UW . 


 4 2 


 IN ENGLAND , FRANCE , PRUSSIA , AUSTRIA , BELGIUM , ITALY , and AMERICA 


 10 % ) EX ? MAUKDI 25 ~ 


 2 } [ LONDON 1862.1 , 


 award to 


 JOHN BRINSMEAD & SONS 


 18 , WIGMORE STREET , LONDON , W 


 for their 


 perfect CHECK repeater " action 


 PIANOFORTES 


 GUARANTEED for five year 


 STEAM POWER work : 


 SEd’orrore " ( with Madame Angus Weldon ) , and in Garrett ’s 
 " good night " ' ; and also sing Ferrari ’s " Vieni , vieni , " the 
 " ' angel ' whisper , " and Molloy ’s irish ballad , " Thady 
 O’F linn , " the two last song be encore . Mr. Frewer 
 be the pianist of the evening , and , besides accompany 
 the vocalist , play with much effect a selection from 
 " Oberon , " a Rondo by Chopin , and Weber ’s " ' Rondo Bril- 
 lant . " " Miss Warren may be congratulate on the complete 
 sonoeee of the excellent programme she have provide for her 
 riend 

 Tue Annual Musical Festival at Brighton ( the third of its 
 kind project and carry out by Mr. Kuhe ) have be 
 thoroughly and deservedly successful . the playing of 
 Madame Schumann create a mark -effect , and at the con- 
 clusion of her performance she be unanimously recall 
 and overwhelm with applause . amongst the solo instru- 
 mentalist we must also mention Mr. Carrodus ( violin ) , Mr. 
 H. Chipp and Mr. E. Howell ( violoncello ) , Mr. R. Taylor 
 ( organ ) , Mr. Lockwood ( harp ) , and Mr. Kuhe and Herr Ganz 
 ( pianoforte ) . the orchestral work include Beethoven ’s 
 Symphony in C minor , Mendelssohn ’s Italian Symphony , 
 Mozart ’s Symphony in G minor , & c.- a selection from the 
 work of M. Gounod , conduct by the composer , be an 
 interesting feature , and this include his spirited Saltarello 
 ( which be vociferously encore ) and his Messe Solennelle , 
 which be well give throughout , the chorus be sing 
 by the Brighton Sacred Harmonic Society . one of the 
 important attraction of the Festival be Sir Julius Bene- 
 dict ’s Oratorio , " St. Peter , " the principal part in which 
 be admirably sustain by Madame Cora de Wilhorst , 
 Miss Alice Fairman , Mr. Vernon Rigby and Herr Stock- 
 hausen . Madame de Wilhorst elicit an enthusiastic 
 encore for her rendering of the air , ' ' the Lord hath his 
 way in the whirlwind , " and Miss Alice Fairman receive a 
 similar compliment for her intelligent reading of the beautiful 
 air , " ' o thou afflict . ' " ' the chorus be give with much 
 energy and precision , and the work ( which be conduct 
 by Sir Julius Benedict ) be evidently thoroughly appreciate . 
 in addition to the vocalist already name , Mrs. Weldon , 
 Miss Julia Elton , Miss Blanche Cole , Madlle . Angele , 
 Madame Rudersdorff , Madame Lemmens - Sherrington , 
 Madlle . Natalie Carola , Messrs. Nelson : Varley , G. Perren , 
 Lewis Thomas , & c. , have contribute their valuable service 
 during the festival , all of whom have be warmly receive . 
 it would be impossible to give a list of all the work per- 
 form , but we may mention Gounod ’s " lamentation of 
 Jeremiah " ( ' Gallia ' ) ; Mendelssohn ’s ' ' Lobgesang , " Ros- 
 sini ’s " ' Stabat Mater , ' " ' and Mozart ’s Twelfth Mass , as 
 amongst the most important . Messrs. Kingsbury and 
 Taylor have render efficient aid both with the baton and 
 at the organ 

 Mapame Schumann give the first of two pianoforte recital 
 on the 22nd ult . , at St. James ’s Hall , before a numerous and 
 thoroughly appreciative audience . the programme open 
 with Schubert ’s excellent sonata in A minor , every moye- 
 ment of which she play not only with finished executive 
 power but with the true conception of the composer ’s 
 intention . in Beethoven ’s variation in C minor ( Op . 35 ) 
 she thoroughly enlist the sympathy of her hearer , and 
 receive the warm applause at the conclusion of her 
 performance . amongst the small composition include 
 in the selection , Brahms ’s arrangement of Gluck ’s " ' Gavotte " 
 create quite a sensation , and be most enthusiastically re- 
 demand . the delicacy of feeling Madame Schumann 
 throw into this charming little piece delight every listener ; 
 and we can scarcely doubt that the effect will be to make 
 amateur pianist strive their utmost to re - produce so graceful 
 a realisation of a graceful work . the selection of the 
 " Kreisleriana " be also receive with unqualified pleasure . 
 the vocalist be Madlle . Anna Regan , who give a most in- 
 tellectual rendering of some of Schumann ’s song , two of 
 which be unanimously encore . Sir Julius Benedict 
 accompaniedwith his usual care and judgment 

 Tue decease of Mr. H. F. Chorley , which occur on 
 Friday , the 16th ult . , remove from we a musical critic who , 
 whatever might be think of his opinion , at least write 
 with an honest conviction of the truth of the principle he 
 advocate , and endeavour to uphold the dignity and inde- 
 pendente of an office which , for thirty - five year , he worthily 

 Tue " Johannes - Passion , " of Bach , will ; we waderstand , be 
 hear for ' the first time in this country , on Friday , March 
 22nd , ata morning concert , to be give at the Hanover Square 
 Rooms , for the purpose of.raise aftind for the restora- 
 tion of the Church of St. Anne , Sdho . the principal artist 
 will be Madame Cora de Wilhorst , Madame Bentham- 
 Fernandez and Herr Stockhausen . the important tenor 
 music will be undertake . by . Mr. Arthur Wade , whose vocal 
 qualification be well know to musical amateur . the 
 work will . be produce . under ' the - direction of Mr. Joseph 
 Barnby 

 ar the Beethoven Rooms;.on Wednesday , the 14th ult . , 
 a complimentary concert be give to Mr. John Gill , at 
 which the following distinguished artist give their valuable 
 aid:—Mdme . Florence Lancia , Mr : Trelawney Cobham , 
 Signor Ciabatta , Mr. Wadmore , Mr. Henry - Guy , and Herr 
 Ganz . the late choir - boy of St .. James ’s ; Westmorland 
 Street , also assist . between the part ' of ' the ' concert 
 a presentation of a very beautiful and . massive silver tea 
 service be make to Mr. Gill by nes ae and Mrs. Coster , on 
 the part of the congregation of that church . the teapot 
 bore the following inscription : — * present to John Birch 
 Gill , Esq . , by member of the congregation of St : James ’s 

 Westmorland Street , on his retirement from the choir , as a 




 _ BOO 


 415 


 organist ’ salary .. 


 to the EDITOR of the MUSICAL TIMES 


 CLERICUS 


 to the EDITOR of the MUSICAL TIMES 


 SRSGSECSELEE WET 


 SANEBSS . BPENEEBBERSOP , FETESESES 


 TO correspondent 


 420the MUSICAL TIMES,—Marcu 1 , 1872 

 chair , the overture to Der Freischiitz be finely play . Mendelssohn 's 
 Concerto in G minor be charmingly perform by Mr. Hallé , who be 
 enthusiastically recall . the accompaniment of the orchestra , under 
 the conducting of Mr. C. A. Seymour , be characterise by the usual 
 refinement and delicacy . Herr Stockhausen give Mozart 's air : from 
 Le Nozze di Figaro in the most artistic manner , and Mdme . Louise 
 Kapp sing the scena from Der Freischiite with much effect . of the 
 manner in which Beethoven 's symphony in F ( no . 8) , be play , it be 
 only necessary to say that it be in every way worthy of the composi- 
 tion . similar commendation will apply to the other performance of the 
 band — more especially in the sparkling * Scherzo , " by Glinka , and the 
 overture to Zunnhduser . Mdlle Loewe sing an air from Roberto very 
 tastefully , and be recall . the chief honour of the evening be , 
 however , heap on Mdme . Norman - Neruda . her appearance be 
 greet with the most enthusiastic applause ; and on the conclusion of 
 her performance of Spohr 's " Adagio , " she be overwhelm with 
 brava . Professor Oakeley may justly be congratulate on the complete 
 success which have attend his labour 

 GawtTHorre — on Shrove Tuesday , a tea party and concert take place 
 in the National School . the solo vocalist be Mrs. Barras , Mr. Har- 
 greaves , Mr. Baines , Mr. Overend , and Mr. Geo . Jubb . ' the encore 
 be accord to Mrs. Barras and Mr. Baines for their song , and also 
 for the duet , ' ' when a little farm we keep . " a pianoforte duet by Mr. 
 Oxley and his pupil , Miss Asquith , be play with much taste and 
 executive facility , and Mr. J. W. Oxley ( organist of the Parish 
 Church ) display great delicacy of touch in a pianoforte solo , which 
 be re - demand . Mr. Oxley ofliciate as accompanist 

 Lrek.—The member of the Leck Amateur Musical Society , cop . 
 ducte by Mr. Powell , give their fifteenth concert on the 12th ult , in 
 the Temperance Hall . Mr. W.H. Birch ’s Operetta , Eveleen , the Rose 
 of the Vale , occupy the first part of the programme ; Miss Shute , 
 Mr. Dishley , and Mr. Beckett , of the Society , singe the solo music , 
 and the accompaniment be play by Miss A. Milner , at the piano . 
 forte , and a small band , lead by Mr. Lockett , the Operetta be sing in 
 good style throughout , and be receive with warm applause , and 
 frequent encore . the second part be miscellaneous , and include 
 vocal solo by Mr. G. C. Warburton , of Manchester , and some part - song 

 LivsrPoot.—The Second Subscription Concert of the Philharmonic 
 Society take place on the 6th ult . ; principal artist — Madame Lemmeng 
 and Herr Jules Stockhausen ; solo pianoforte , Dr. Ferdinand Hiller , 
 the overture be that to Gusfare , by , Auber , and that to Schiller ’s 
 Demetrius , by Dr. F. Hiller , the latter an interesting and characteristic 
 work , conduct by its composer . the Sinfonia be Beethoven 's in 
 b flat , no . 4 . Dr. F. Hiller play with the utmost delicacy and artistic 
 expression Mozart 's Pianoforte Concerto in d minor , and three charm . 
 e short piece of his own , " Schummerlied , " " Zur Guitarre , " and 
 " Walzer " ( MS . ) . Madame Lemmens sing with her usual brillianey ; 
 and Herr Stockhausen give with great refinement two song by 
 Schumann and Schubert , and the scena , ' ' wo berg ' ich mich , " from 
 Euryanthe . the choral member sing with much effect the * Hunting 
 Chorus , ' from Haydn 's Seasons ; ' * o the pleasure of the plain , " from 
 Acis and Galatea , and a part - song by Henry Smart , ' ' wind thy horn . " 
 the concert conclude with Mendelssohn 's Wedding March.—Tag 
 third concert , on the plan of the London Monday Popular Concerts , be 
 give in the Philharmonic Society ’s Hall , on the 7th ult . ; executant — 
 Madame Arabella Goddard , Herr Straus , MM . Ries , Zerbini , Percival , 
 Jennings , Paquis , Kleigl and Piatti ; vocalist , Miss Eleanor Arm . 
 strong ; accompanist , Mr. Zerbini . the programme consist of Spohr 's 
 Quartet , in d minor ( Op . 74 , no . 3 ) , for string ; Mozart ’s * * dove sono ; " 
 Dussek ’s Pianoforte Sonata , in C minor ( no . 3 , op . 35 ) ; Haydn 's 
 Quartet for string , in C major ( no . 3 , op . 83)—the last movement 
 vehemently encore ; Pacini ’s cavatina , " il soave ? bel contento ; " and 
 Hummel ’s Grand Septett . the concert be a perfect success , but the 
 septett be the fine performance of the whole.-——mr . Lawsoy ’s third 
 promenade concert be give in St. George ’s Hall , on the 3lst Jana- 
 ary , and attract a large audience . the instrumental department 
 be confide to the band of the 8th ( King ’s ) Regiment of the Line . 
 which , although not quite equal to those of some of the regiment of 
 the Guards , play well ; its execution of the operatic selection , dance 
 music , & c. , be such as to completely satisfy the audience . the 
 vocal music be on the whole satisfactory ; that contribute by Madame 
 Billinie Porter be particularly so . her voice completely fill the 
 spacious hall , but we should have prefer to hear she in something 
 more worthy of her talent than Bishop 's " Ray of hope , " which have be 
 needlessly resuscitate from oblivion . Mr. Alfred Brown , a local artist , 
 who be rapidly gain favour with the public , sing some pleasing bari- 
 tone song in an intelligent , though as yet not absolutely finished , 
 style 

 MAncuesteR.—Mr . Horton C. Allison give a pianoforte recital ia 
 the Memorial Hall on Tuesday evening , the 6th uit . , before a numerous 
 audience , with very decided success . Mr. Allison play Beethoven 's 
 sonata ( no . 12 , op . 26 ) , a prelude and scherzo by Mendelssohn , and selec- 
 tion from the pianoforte work of Bach , Schubert , Liszt , Chopin , Henselt , 
 and Wollenhaupt , all of which be perform entirely from memory . 
 Mr. Allison ’s own composition form an interesting feature in the 
 programme , and include several novelty , viz . , a new song , ' lovely 
 flower , " five of his ' * melodious Studies , " " ' the sea song ; " and two 
 of his well - know piano piece , viz . , ' La fleur de lis " and " Taran- 
 tella , " in a minor , and the song " Again the wood . " the vocal musi¢ 
 be ably render by Miss Jessie Bond , who produce a most favour- 
 able impression . — — on the 7th ult . Dr. Ferdinand Hiller give a piano- 
 forte recital , at the Town Hall , before a large and highly appreciative 
 audience . the whole of the composition perform , with the excep- 
 tion of a Rondo by Mozart , be by Dr. Hiller , and inglude a Sonata , 
 the ' ' operetta without word " ( in which he be ably assist by Mr. 
 Hecht ) , besides several small piece , the concert conclude with an 
 improvisation , in which the theme of a song which have just be give 
 be take as a subject . that Dr. Hiller 's performance be masterly 
 throughout the evening need scarcely be tell , and the warm applause 
 with which his effort be greet must have convince he how 
 thoroughly his eminent artistic quality be recognise in this country . 
 — — the interest of Mr. Hallé ’s concert , on the 8th ult . , be ma- 
 terially increase by the presence of Dr. Ferdinand Hiller , who re- 
 maine in Manchester expressly to conduct his own overture , Demetrius , 
 and Schumann 's Pianoforte Concerto , in A minor , which latter work 
 be finely play by Mr. Hallé , and receive with marked favour . 
 Dr. Hiller ’s new overture be strikingly picturesque , the subject be 
 extremely exciting , and the instrumentation , as might be expect , 
 masterly throughout . the symphony be Beethoven 's , in A ( no . 7 ) , 
 which be magnificently perform , and Mendelssohn 's overture , Ruy 
 Blas , be also include in the programme . Signor Agnesi be the 
 vocalist 

 Market Draytox.—The member of the Market Drayton Musical 
 Union give their second concert this season on the 25th January , to 4 
 fashionable and thoroughly appreciative audience . the part - song and 
 glee , * * o , hush ' thee , my babie " ( Sullivan ) , ' by Celia ’s Arbour " 
 ( Horsley ) , ' the Sands o ’ Dee " ( Macfarren ) , the ballad dialogue , in 
 ten part , " Sir Patrick Spens " ( Pearsall ) , and ' sweet ' and low . ' 
 ( Barnby ) , by their precision and light and shade , reflect great credit 
 on the efficient and careful training of the conductor , Sidney H 




 SHEFFIEI D'Alton , Mr. Vernon Rigby ( in place of Mr. Sims Reeves , who | ful . the reading be most ably support by Mrs. Tattersall , 
 be unfortunately indisposed ) , Mr. H. Pyatt , and Mr. Lewis Thomas , | Messrs. Norbury and Pigott . ' ' God bless the Prince of Wales " be 
 all of whom be thoroughly effective in the music allot to they . | give at the commencement , and be join in by the patient , and the 

 e chorus be remarkably well give throughout . the Orchestra , | National Anthem conclude the evening . Mr. Brook be an able 
 - by Mr. H. Farmer , be everything that could ' be desire ( the accompanist . 
 " March of the Israclites " be anennassey encore , and the organ WaAtsatt.—On Tuesday evening , the 30th January , the member of 
 be most artistically play by Mr. George Essex . much credit | the Fhilharmonic Society hold their annual meeting at the George Hotel , 
 is due to Mr. F. M. Ward for the manner in which he have conduct | Mr , Edward Russell in the chair . the secretary ’s report prove that 
 thechoral rehearsal for this work . -in the evening a miscellaneous | the society be in a flourish condition , the number of member 
 concert take place , in which all the above - mention vocalist ap-| show a considerable increase upon that of former year . the finan- 
 cial position of the association be also highly satisfactory . Mr. George 
 Gill , who retire from the office of secretary , be warmly thank by 
 Mas , Bac . , organist of St. Mary - the - Virgin , that gentleman be pre- the member for the zeal he have always manifest in fulfil the 
 with a testimonial by the congregation and the choir . on | 4utie of his office . : 
 Sunday , the 11th ult . , at the conclusion of the morning service , several Watiinetoy , Norrotk.—On Friday , the 9th ult . , a miscellaneous 
 of theparishioner and other withdraw into the chancel , when the | concert be give in aid of the church fund , under the direction of 
 Vicar , in their name , present Mr. Allchin with a velvet purse contain- | Mr. S. G. Street , of Lynn , assist by a select party of vocalist . the 
 e twenty - five sovereign , accompany the gift with a letter . the principal feature of the evening be the song ' " o , fair dove ! o , fond 
 Rey . J. W. Burgon explain that the amount be not to be regard | dove ! " sing by Mrs. Walters , ' ' Esmeralda , " by Miss Dennis , ' the 
 asthe measure of the appreciation entertain of Mr. Allchin ’s profes- | white squall , " by Mr. Rutter , ' the pilgrim of love , " by Mr. C. 
 sional ability and service , but only as a token of the esteem and good | Smith , and two pianoforte duet by Beethoven , play by Miss R. G. 
 will of : the contributor . no pain have be take to make up a large | and Mr. S. G. Street . several part - song by the church choir be 
 sum . ' the little purse of gold have be a spontaneous offering . Mr. | effectively render and well receive . Miss Street , organist of the 
 Allchin - be request to purchase for himself out of the money some | parish church , accompany throughout the evening , and Mr. 8S. G. 
 small object which might be a permanent memorial of the present occa- | Street conduct . 
 sim Mr. E. Ryman Hall , in the name of the choir ( the boy be ! Wuycaxester.—On Wednesday evening , the 7th ult . , St. John ’s Room 
 draw - up - in two rank to witness the presentation ) , next present to | be cram with an audience to hear Mr. Ellis Roberts , the welsh 
 Mr , Allchin a handsomely - bind Bible contain an appropriate in- | harpist . he be ably assist by Miss Ellen Glanville , whose well- 
 scription . Mr. Hall in a short speech pay a graceful tribute to the | trained voice be admirably suited for the ballad choose , and she 
 aaland ability with which Mr , Allchin have out of rough material | frequently receive the honour of an enthusiastic encore . 
 caeere eee tere oomachak , Me . and Mis . Alishia receive | WoLvERHAMPTON.—On Tuesday evening , the 13th ult . , a concert of 
 4 aa - OLVE AMPTON . — Ss ' " > ec 
 " ee = Snes ARF Me eqeemeny be over . | sacred song be give in the low lecture - room of Queen Street Con- 
 Parxstone , Donset.—An amateur concert , in aid of the fund of | gregational Chapel , in aid of the harmonium fand . the solo singer 
 the Working men‘s Institute , be give on Shrove Tuesday , at St. | be the Misses Aldred , Saunders , and hers 2 gy re — — Hodgetts 
 ool - room . the overture to Eymont and William Tell , and | and Reynolds , who be highly successful . everal anthem be 
 atrioby Corelli , be amongst the instrumental piece perform by pleasingly render by I — yee : among — " may men- 
 .. T. R. Ww . W. i A r , Mrs. W : | tion * by 1 Siloam , " which be specially compose for the occasion 
 madge ana iss Alaviage , ail ot when Ware tape bo icing by Mr. 0 : Terry , jun . , organist b the chapel . ' the concert prove 
 2 ion . the vocal portiun be well sustain by Miss Aldridge , | highly attractive , the large — ae — a . ver pam 
 m , Mr. W. Aldridge , and the Rev. E. Coombes . Bishop 's | audience . the concert be under the direction of Mr. Terry , who pre- 
 duet , " as it fall , " by Miss Aluridge and Mrs. Gosham . temereediy side with his usual ability at the harmonium . : 
 recelved.an encore , as do also one by Mrs. Gosham and Mr. Aldridge.| \yoncesran.—The opening concert for the season of the Worcester 
 the concert be well attend . Vocal and Instrumental Union be give on the Ist ult . , in the Natural 

 ne y i a ) , i History Room . Mrs. Sutton be highly successful in all her vocal solo , 
 PgxpLEToN.—Handel 's Oratorio , the Messiah , be perform in the being encore both in Weber 's Scena , " softly sigh , " and Guglielmo ’s 

 Miss Annie Anyon ( contralto ) , and Mr. Dodds ( bass ) . Dr. Spark accom-| applaud . Mr. H. Binns be encore in " the village blacksmith 

 pe all the song with his usual ability —— on the 12th ult . the mem- | and Mr. Baxter receive a similar compliment for his singing of ' ' the 
 of the Sheffield Tonic Sol - fa Choral Society give a concert in the | wooden wall of Albion . " Miss Doward play with much effect a 
 Music Hall , Surrey - street . the solo vocalist be Miss Twigg . Mr. A. | selection from one of Beethoven 's sonata , and accompany with skill 
 Kenningham ( tenor , of St. Andrew’s,-Wells - street , London ) , and Mr. J. | and judgment the vocal music.——TuE second concert of the Worcester 

 Thurley Beale ( bass , St. Andrew 's , London , and pupil of Mr. Joseph | Musical Society for the present season be give at the Music Hall on 




 422 


 dure the last monta 


 the new NATIONAL SONG 


 GOD SAVE the PRINCE of WALES . " 


 GABRIEL DAVIS . 


 DWARD J. HOPKINS ’S thanksgive an 


 6 ark ! ’ ti the breeze of TWILIGHT 


 NOVELLO ’S OCTAVO edition of OPERAS 


 the only arrangement play by 


 MADAME SCHUMANN , 


 CAVOTTE by C. W. GLUCK , 


 JOHANNES BRAHMS 


 SE 


 PEZETEE 


 423 


 SUNG by MADAME CORA DE WILHORST . 


 HE URDER for the HOLY communion , 


 now READY , 


 music copy - book 


 _ QONTAIN a progressive course of instruction in music , upon a 


 system design by he . 


 SINGING at sight . 


 new work for singing class 


 CHAPPELLS PENNY operatic part - song 


 a new work for the ORGAN . 


 HANDEL ’S chorus 


 8 . let we break their bond 


 ASUNDER 


 CHAPPELL & CO . , 50 , NEW BOND STREET , LONDON 


 3 . we never will bow down . 


 THEM ,