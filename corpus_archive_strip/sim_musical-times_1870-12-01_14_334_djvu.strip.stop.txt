


 MUSICAL TIMES 


 DECEMBER 1 


 ST . JAMES HALL . 


 NEW ORATORIO 


 ST . PETER 


 GIVEN ST . JAMES HALL 


 


 TUESDAY EVENING , DECEMBER 15 . 


 SIMS REEVES , 


 Conductor — Sir MICHAEL COSTA . 
 THIRTY - NINGH SEASON , 1870—1 

 FRIDAY , 16th December , Beethoven ‘ Mass © ” 
 “ Mount Olives . ” usual Christmas Performances 
 Handel “ ‘ Messiah , ” Fridays , December 23rd 30th 

 arrangements respecting . HANDEL FESTIVAL 




 LY DIOCESAN CHURCH MUSIC SOCIETY 


 SYMPHONIES , 


 EETHOVEN 


 PROFESSIONAL NOTICES , 


 ALLCOMPETITION.—T. R. WILLIS 


 USICAL INSTRUMENTS VOLUN . 


 RASS , REED , STRING , DRUM 


 ARMONIUMS ! HARMONIUMS 


 W. HATTERSLEY , 


 HARMONIUM MANUFACTURER , 


 95 , MEADOW - STREET , SHEFFIELD 


 VONCERL 


 ESTABLISHED 1854 


 ST 


 675 


 ESTABLISHED 1857 . 


 PRIESTLEY HARTWELL 


 ACTIONS , - GUINEAS . 


 PIANOS . 


 HARMONIUMS . 


 STAMPED PAPER ENVELOPES 


 COLOUR , 


 


 HENRY REED , . 


 STATIONER ARMORIAL ENGRAVER , 


 57 , OXFORD STREET , LONDON , W 


 JONES & WILLIS 


 LONDON 


 BIRMINGHAM : 


 G RATEFUL — COMFORTINGNew Burlington - street ; Musicsellers . W NG ; 
 , ; EW SONGS expressly Christmas . “ merry 
 TANT Torn ‘ Ary 
 RGAN MUSIC.—A Complete List STAN- Chistinas atey New Year . ” Written Composd 
 DARD WORKS ORGAN , published Messrs./py J. W. Ruopgs . ‘ Christmas come . ” Music 
 BREWER CO . ‘ W. Busu . found superior songs . 3s , 
 AMATEUR ORGANIST . Epwarp Travis . Forty- London : B. Williams , 19 , Paternoster - row . 
 Books , 3s . , volumes bound cloth , 128 . . aE = : = rs 
 AMATEUR PEDALIST . W. Suetmerpine , 38 . HERRY NEW SONG BASS VOICE , 
 AMATEUR INTERLUDIST . Epwarp Travis . ‘ Englishman motto , despair , " Words Watzgn , 
 Books , 4s . . remarkably written vigorous , music equally good , 
 RINK PRACTICAL ORGAN SCHOOL . Edited J. Hires . | “ Mammon , ” “ Lighthouse ; ” words music equally 
 Books , 6s . , complete volume , 20s . effective . Price 3s . 
 VOLUNTARIES ORGAN . H. G. Nrxon , London : B. Williams , 19 , Paternoster - row . 
 2s . . ee = os 
 RECREATIONS ORGAN . J. Hires , 28 . 6d . ea.| NT EW SACRED SONGS . ‘ Simply Thy Cross , ” 
 PRELUDIAL PIECES . E. J. Hopkins , 2s . 6d . il “ ‘ brought waters . ” Poetry 
 PRELUDES INTERLUDES . G. Cooper , 4s . Music W. West ; , Poetry Carpenrnr , 
 ORGANIST ’ STANDARD LIBRARY . James Strmp-|and Music GILL . songs melodious , easy , effective , 
 son . lwenty - Numbers , 3s . , volume , 25s . splendidly illustrated . 3s . 
 HANDEL CHORUSES . J. C. Nicurincaty . London : B. Williams , 19 , Paternoster - row 

 eC ‘ 7-$i N . , ‘ ho > _ — — wee 7JYIW ATL ae ATV\IRDIOA < FEL ... ae 
 — ew Numbers , 1s . , - F HOWELL NEW ORATORIO , “ Land 
 SOFT VOLUNTARIES forthe ORGAN . J.C. NIGHTINGALE . e Promise , ” Dedicated Birmingham Festival Choral 
 Eighteen Numbers , . 6d . , Books , 7s . 6d . . | Society , upwards 250 members purchased copies . 
 BEETHOVEN CHORUSES MOUNT OLIVES . | Price 2s . net , paper cover ; 3s , cloth . 
 J. C. NIGHTINGALE , 5s , London : B. Williams , 19 , Paternoster - row . 
 HAYDN CHORUSES SONGS CREATION . | — — woes eee 
 J.C. NiGuTINGALE . Books . Book , 3s . , Book , 58 . 7 EW COMIC VOCAL DUETS . Words 
 HAYDN CHORUSES SONGS SEASONS . Carpenter . ‘ Naggletons , ” ‘ Married 
 J.C. NicguTincaLe . Books , 48 , . Single . ” Music Mutien . “ latest thing 
 HANDEL OVERTURES . J. C. Nicurincave . Four-| ; ” “ Woman Rights . ” Music Cuerry . 
 teen Numbers . 1s . . point , suitable Concert Drawing- 
 COMPLETE INSTRUCTIONS ORGAN . J. T.!room . post free , 19 stamps . 
 Stone . 5s . ’ z London : B. Williams , 19 , Paternoster - row . 
 CLASSICAL ORGANIST . J. T. Strong . 
 Vols . , oblong folio , 12s . , Thirty - Numbers , 3s . 

 ORGAN STUDENT COMPANION . J. T Srong , . 
 Volumes , 12s . , - Books , 3s . . Favourite Movements 
 forwarded - Stamps , Bound Works 
 cepted , 
 BREWER CO . , 23 , Bishopsgate - s*reet . JULES BENEDICT 
 e al 
 CLASSICAL ORGANIST . J.'T. Stone . ORATORIO , 
 36 Numbers , 3s . ; Vols . , 12s . . Vols 




 BERTHOLD TOURS 


 RUC . 


 LIED 


 PUB . 


 RSON 


 CHRISTMAS CAROLS , 


 NEW OLD 


 W. R. BRAMLEY , M.A. 


 SERIES 


 CONTENTS 


 SECOND SERIES 


 CONTENTS . 


 6773 . ROTTERDAM — Fantasia , subjects Haydn 3rd 
 ( Imperial ) Mass e 

 4 . M A.—Fantasia , " subjects Beethoven Mass 

 5 . ROUE N. — Fantasia , . “ subjects Gounod “ Messe 
 Solennelle « 
 6 . CAEN,.—Fantasia , subjec ts Rossini Stab : Mater 
 7 . MAYENCE.—Fantasia , subjects Haydn Ist 
 Mass , B flat aie 
 8 . COLOGNE . — Fantasia , ‘ subjects . ‘ Mozart Ist 
 Mass , C : 
 9 , PARIS.—Fantasia , su ! djects Mendel ssohn ’ 3 Hymn 
 Praise 
 10 . LONDON.—F : ‘ antasia , n subjects Handel Messiah 
 11 . NORWICH .. —Fantasia , subjects Haydn Creation 
 12 . CANTERBURY . Vines subjects Spohr 
 pants Hart . 
 Beautifully illustrated Color urs , views sof celebrated C ‘ athedrals . 
 “ pieces , like Felix Gantier , reviewed time 
 ago Musical Times , especially intended young players , 
 , think , found equally acceptable teachers 
 pupils . written form little Fantasias , com- 
 illustrations , printed 
 — Musical Times , Feb. , 1870 . 
 . Berners - street . W 

 

 BEETHOVEN 

 Price Shillings 




 FREDERICK BRUCKMAN , MUNICH , ITALIAN GERMAN MUSICIANS 

 Tableaux , containing 100 life - like Portraits 
 celebrities schools , explanatory Key . 
 publishe d following sizes . — 
 7 5 } inches , 2s . 6d . , mounted , : 
 16 } 134 n'a : ee 12s . 6d . 
 25 by20 .. £ 1 5s . Pe £ 1 7s . 6d . 
 Portraits Gluck , Haydn , Beethoven , Mendelssohn , Mozart , 
 Schubert , 15 } 114 . , 12s . 6d . mounted . 
 cabinet size , unmounted , ls . ; mounted , 1s . 6d . 
 J. Gerson , Rathbone - place , W 

 Che Herlin photographic Company 




 OYAL ACADEMY MUSI¢G 


 MUSICAL TIMES 


 DECEMBER 1 , 1870 


 NATIONAL MUSIC NATIVE 


 LAND . 


 680 


 ROYAL ITALIAN OPERACRYSTAL PALACE 

 Tue Beethoven Concerts establishment 
 attracted large audiences past month . 
 performance Symphonies , according order 
 written , proved highly interesting 
 amateurs professors ; need scarcely 
 , instance execution works 
 fully worthy occasion . Overtures 
 ‘ ‘ Leonora ” “ Fidelio ” given 
 concert , welcome account 

 intrinsic beauty , showing gradual develop 




 684 


 ST : JAMES HALL 


 SACRED HARMONIC SOCIETY 


 TREBLE . 


 ALTO . 


 TENOR 


 10 - 138 


 ANTHEM CHRISTMAS . 


 LEE 


 = = F = S 


 SING REJOICE 


 22222 


 1 


 GN 


 LW 


 TGS 


 SSS 


 = [ 22222 


 SS = 


 SING REJOICE 


 PP — — — — — | — 


 SS 


 = — FE 


 — 14 ! 1 : — , . : 


 — _ S 


 SING REJOICE 


 SL 


 50 


 SSS SSS = = SSS ES YE 


 2 SSS SSS SS SSS 


 NE 


 _ _ _ _ — _ @ — 


 ~ TTS . 


 2 ? 2.02 . < GMass Voices Organ . E. Silas 

 Tuts Mass , gained prize offered 1866 
 Belgium composers nations , work great 
 merit . distinctive character 
 proves author allowed original 
 thought marred conventional training . 
 Kyrie calm devotional , Bach - like 
 subject given tenors handled evident 
 skill . Gloria slightly flavoured Beethoven , 
 knew assimilate con- 
 ceptions ! fugal powers composer 
 exercised “ Cum sancto spiritu , ” 
 reserved fur words “ Et vitam venturi . ” Credo 
 probably finest portion work , combining 
 styles best exhibit deep meaning 
 noble Nican formulary . pass 
 Benedictus word commendation : 
 excellent . addition movements belonging 
 Mass proper , Graduale Offertory Feast 
 Immaculate Conception 

 Magnificat Nune Dimiitis Z. Composed John 
 Stainer , Mus . Doc . , M.A 




 EDITOR MUSICAL TIMES 


 CORRESPONDENTSBirmincHam.—A gratifying presentation 
 Monday , 7th ult . , Mr. W. Masefield , jun . , 
 past months acted capacity organist War- 
 wick Street Baptist Mission Chapel . President ( Mr. S. W. 
 Martin ) having taken chair , ‘ unanimous vote thanks 
 passed valuable gratuitous services Mr. Masefield 
 rendered . address presented expressive admiration 
 good wishes , gold pencil - case . Mr. Masefield 
 returned thanks unexpected testimonial , assured 
 friends intention continue services 

 Bisnor Lavixeton.—A Concert given theschool- 
 room , 27th October , aid organ fund . 
 - songs excellently given , vocal solos 
 successfully sang Mrs. Fletcher , Miss Hitchcock , Messrs. Ken- 
 ningham , De St. Croix . Mr. Bambridge pianoforte perform- 
 ances , welcome feature programme , artistic 
 rendering Beethoven ‘ * Moonlight Sonata , ” especially 
 worthy commendation . concert respect 
 thoroughly satisfactory 

 CANNINGTON ( NEAR BripawaTteR).—An Amateur Con- 
 cert , aid parochial funds , given 3rd ult . 
 singing Mr. Poole , jun . , highly effective Beethoven 
 ‘ * Adelaida , ” - music thoroughly satisfactory , 
 especially piece , entitled ‘ care , ” Mrs , M. Bartho- 
 lomew . Mr. C. Lavington ( organist Bridgwater Parish Church ) , 
 conductor , careful training singers 
 attributed great success concerted music 
 Miss M. Shepherd ably presided pianoforte 

 CastLe Biayney , IRELAND.—A successful Concert 
 given Town Hall Tuesday evening , 8th ult . 
 room tastefully decorated filled . concert in- 
 cluded carefully rendered chorus , “ Native Land , ” number 
 songs duets , received , 
 - demanded . performance reflected great credit con- 
 ductor , Mr. George E. Nixon , presided pianoforte 

 support fund , numerous respect . 
 able audience . choir composed band twent 
 members certificated Tonic Sol - fa Class , Mr. Jameg 
 Edelston guidance , music exceedingly rendered , 
 organ solos Mr. T. Woolman highly successful , 
 ScarBoRoucH.—A new Society , conducted Dr , R , 
 Sloman , called ‘ Scarborough Amateur Vocal Instry- 
 mental Society , ” numbers members , held 
 meeting 4th ult . W.H. Smyth , Esq . , Hon . , 
 SourHenp.—On Wednesday evening , 9th ult . , Mr , 
 West gave series Winter Concerts school- 
 room , filled . programme successfully 
 rendered glee class Sydenham . 
 effective pieces Mendelssohn “ Hark voice , ” expressive 
 Chorale , composed conductor , hymn , “ peace , 
 O Lord ; ” Handei duet , “ * O lovely peace , ” Judas Macca 

 bus ” ‘ sung Misses Black Gundry , encored ) ; 
 chorus Oratorio , “ Sing unto God . ” 
 songs duets given effect , 
 ee ha \ . | unanimously - demanded . profits performance 
 Liverpoot.—The Societa Armonica gave thirty-|were given National Society Sick Wounded . 
 “ © ” , aw ; d ~ . 
 second “ open reheareal , ” Saturday evening . 22nd October , ! Syyexnam.—The Concert given atthe Lecture Hall.on 
 Liverpool Institute , Mount - street . instrumental music Sad alt . - aid fund xellot Sak 
 consisted Lindpaintner Overture ‘ Moses , ” Romberg ai Wa ~e ue ‘ ided Anil pone en — 
 Symphony E flat , op . 6 Vogel ‘ ‘ Orientale ” march , | ‘ ¥ 04 4 : bv oo e ‘ lad ie Tt 5 Marseflt : ba ae 
 previously performed inthis town . addition | ® ich ” « Ru sai N : tor Al Hy crag vat ! Watch 
 pieces , Mr. Lawson , leader band played srg - ee l 50 % recip ile rie : ve ar " sinc og 
 usual excellent manner , violin solo . selection Opera ai nt ie Bl ee ‘ ies sige ae > ” obt oe 
 called ‘ “ * Marinette ” composition late Mr. George Har- sass es 2 adept 4 Mr Von Giebn ah ay 8 xis sfalin 
 greaves , important item programme numbers | WC " Merited encore ; Mr. Von “ lenn equally successtul 
 sien x age * ; eager “ aia Bevignani ‘ ‘ Hurrah King . ” members choir 
 given — Chorus , ‘ Hail Vine ; ” ballad , ‘ a| ET der direction Mr. Manns , obliged 
 charm woods ; ” ballad , ‘ clouds misfortune ; ” | 5428 ‘ ‘ Ws ‘ hs hi met AB t £ 25 aaa 2a lined hd 
 duet , “ Good Night : ” song , “ , parting hour ; ” | " Pe ‘ “ th te L ee aes al alison 8 
 trio chorus , ‘ Come , let fill cheerful glass . ” music | ® UPPOrt funda . . ’ ‘ 
 met favour audience } ‘ Truro.—A Concert given New Hall 
 pieces given second time . ‘ principal vocalists , | 31st October , benefit Mr. R. H. Carter Mr. H. G , 
 wane Mise Monkhouse , — af gy eins Mr. nde gene Trembath . programme commenced Mozart Quintett , 
 son , Mr. Lewis , Mr. Hobart . Mr. Armstrong conducted , clarionet strings . included Andante Finale 
 2 xe , gatiey ge ge — = oe nl | 2 ag ec hg — — finely ob er 4 Mr. — 
 8 yould place 7 ecember , » De ‘ | Royal Italian Opera ; Beethoven Pianoforte Trio C minor ; 
 pe ae nn programme consist | Pianoforte Quintett G , Reissiger ; Romance 
 — — o — — = — — -s pees ote Maria | corno bassetto , Salaman , ee , Madame Rosen- 
 rthur , lady nown Liverpool musical circles , gave | vas tl vocalist , ¢ 8 ith effect ‘ Perche 
 Concert Philharmonic Hall , aid ‘ Workshops | jens rhaaia : ® Gah Wik tie . Oneonn Mien duet , * Lad 
 Blind , ” Thursday , 27th October . assisted |darem . ” Theexecutants Messrs. Ralph Vingoe ( violins ) , 
 — — — ae , Ree oo — — Watts , Miss Julia | Mr , Trembath ( tenor ) , Mr. Nunn ( violoncello ) , Mr. H. A. Smith 
 yduey , Mr. Harley Vinning . Miss Arthur , pupil | ( clarionet corno bassetto ) , Mrs. Carter Mr. Trembath 
 Schira , unfortunately suffering severe illness , there- | ( pianoforte ) . — — Friday 16th ult . , successful Con- 
 fore unable effect vocal pieces allotted toher . Mrs. pase given Truro Choral Society . 
 ss gage ce mera Eager eager te ae |the programme miscellaneous , second de- 
 Miss Arthur nae pleasure onauation tl pet ily pari atc ey Bg Pang Bh ga eal 
 Treasurer Charity sum £ 100 3s . 6d.—tThe Philk cae Ut anon , aaah y " 
 surer oO : Vnarity dB 1¢ Philhar-| Mr. Rice , Torquay ; organist , Mr. Carter ; con- 
 monic Society ninth concert took place 8th ult . , 4| ductor , Mr. Trembath , Mus . Bac.——On Tuesday , 15th ult . , 
 interesting “ * successful performance . principal | Organ Recital given Mr. Trembath magnificent con- 
 — — — bese gas Malle . remcwrmag Sign . brn } cert instrument erected Messrs , Hill Sons ( London ) , 
 Solos , Duets , = rio , highly appreciated , | programme consisted compositions Mendelssohn , Wely , 
 - demanded . choral members Society sang | Batiste . & c. , arrangements scores Mozart , Rossini , 
 eal = wif sages - _ 6 igo Fea | Meyerbeer , selection calculated display 
 eareall ) , choruses Weber * * Preciosa ” eyer-| best qualities instrument . 
 beer “ Dinorah . ” overtures “ ‘ Zampa ’’ * ‘ Oberon ’ | bh . . . ° 
 played great spirit , enjoyed , great ) Wansreap.—The season Musical Union 
 instrumental interest evening Mendelssohn } |\commenced Tuesday evening , Ist ult . , - — — 
 Sinfonia , C minor , work , considering author|!met ‘ ‘ Wingfields , ” ” Snaresbrook , practise Handel ‘ Det- 
 boy , looked extraordinary |tingen Te Deum . ” office - bearers appointed 
 — productions . concert closed Costa March | season 1870 - 71 . 
 “ Eli 

 EY F : Wemspon ( NEAR BripGwater).—The - opening 
 Otp Kirkrarrick.—The members Harmonic ! wembdon Church , destroyed fire March , 1868 , took 
 Union , able leadership Mr. J. Drummond Reid , gave | place 4th ult . musical portion Service per- 
 concert 27th October , Parish School - room , | formed amateur choir ( able conductorship Mr. C. 
 completely filled . devoted Lavington , organist Bridgwater Parish Church ) highly 
 ‘ * * Macbeth ” music , solos sung Misses Margaretta | satisfactory manner . Service Clarke E , An- 
 Smyth . Isa McNaughtan , Mr. Geo . Walker , choruses |them “ 0 amiable , ” rendered com- 
 sustained Union . second miscellaneous . he|mendable care . collection aid restoration fund 
 orchestral portion performance ably sustained | amounted £ 40 . add new Organ , Beale , 
 friends Glasgow . pianoforte accompaniments Mr. | Bridgwater , time occasion . 
 Young satisfactory ; considering Society gave entire satisfaction 

 rus ( clarionet ) , Mr. Barrett ( oboe ) , Mr. T. Mann ( horn ) , Mr. 
 J. F. Hutchings . large andieice , entertain- 
 ment highly successful 

 Worvernampron — Mr. Bywater annual Concert took 
 » . - en | place Exchange , Friday evening , 18th ult . , @ 
 Priymouts.—The series Concerts given oe audience . Senmanamael tas chiefly composed classical 
 Plymouth Vocal Association , took place 25th October , | music , included Mendelssohn String Quartet E flat , Trio 
 St. George Hall , Stonehouse . ‘ Judas Maccabeus ” Schumann , pianoforte , violin , violoncello , Beethoven 
 Oratorio selected performance , principal vocal parts Sonata F , violin pianoforte , Haydn String Quartett 
 sustained Miss Blanche Cole , Miss Palmer , Mr. Nelson Varley D , Tours response Gounod “ Meditation , ” violin , 
 Mr. Lander , acquitted arduous | violoncello , pianoforte , organ . executants pieces 
 task thorough satisfaction audience . choruses | Messrs. Flavell Rogers ( pianoforte ) , Hayward Abbott 
 given muck spirit decision especially ‘ Fall'n is| ( violins ) . Roberts ( viola ) , Bywater ( organ ) , Herr Daubert 
 foe , ” * * bow . ” Mr. Fred . N. Lohr ( violoncello ) . interesting features evening 
 conducted Oratorio ability . room | violoncello Fantasia Herr Daubert , rapturously 
 . jreceived . Mr. Bywater achieved decided success singing 
 Preston.—Three sermons afd fund | Handel “ Deeper deeper . ” ey _ peace ’ Pec 

 clearance debt new organ Saul Street Chapel . hile ao song ore composition , ne ) ORE ee , 
 ‘ B < ssatlechaiis . | Miss Rachel Gray . accompanist Mr. Roland Rogers 




 TOKE NEW ING LON ASSEMBLY ROOMS NHE ENGLISH GLEE UNION 


 MR . T. PEARSON 


 MR . J. F. MEEN anton seen finest London ro sialee 
 convenience ; 900 seats . Easy access parts } 
 country , evenings quarter disengaged . ‘ Terms | Beg meyer yn Sane _ — * - 7 m wateal Enter- 
 particular applicati teoRGE F. GyNGELL , Secretary . | tainments Institutions , & c. , ensuing season . 
 Tull particulars application GEon « 2 F. GYNGELL , Secretary | terms , & c. , address J. RuDKIN , Secretary , Sackville House 

 CHI BERT SOCIETY . Beethoven Rooms , 27 , | Brook - green , Hammersmith , W. ic RN alo 

 Harley Street , W. President , Mr. Benepter ; Director . Herr } , VE ENGLISH GLE E UNION.—Miss Daniet 




 CHOIR DIRECTORY PLAIN SONG 


 PUBLISHED SUBSCRIPTION 


 EUROPEAN PSALMIST 


 DEDICATED MAJESTY QUEEN 


 OULE COLLECTION 527 CHANTS , 57 


 PSALTER , PROPER PSALMS , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECT TORIUM . CHORI ANGLI- 


 ORDER HOLY COMMUNION , 


 SUBSCRIPTION PRICE 


 TQNIC SOL - FA EDITION 


 READY , 


 ANTHEMS JOHN GOSS . 


 0 PRAISE LORD HEAVEN 


 SACRED MUSIC ALEX . 8S. COOPER . 


 SECOND SERIES UNISON CHANTS . 


 CHORAL SOCIETIES NATIONAL SCHOOLS . 


 “ ( 10 MORNING SHINETH 


 MESSIAH 


 ADDITIONAL INSTRUMENTATION MOZART , 


 


 BOOK PRAISE HYMNAL 


 SIR ROUNDELL PALMER . 


 JOHN HULLAH . 


 R. SPARK CHRISTMAS ANTHEM , 


 BALAAM PROPHECY ; 


 NEW MUSICAL SERIAL 


 PRACTICAL CHOIRMASTER 


 ALLEN , GEORGE B. BEGINNING WORD , 


 BARNBY . J. GRACE GOD BRINGETH 


 BEST , W. T 


 CLARKE . J. HAMILTON . SHEPHERDS 


 CHRISTMAS CAROLS 


 THORNE , E. H 


 HATTON , J. L. BLESSED LORD GOD ISRAEL . 


 HOPKINS , E.J. LET UNTO BETHLE- 


 LESLIE . HENRY . FEAR , BRING GOOD 


 MONK , W.H. HALLELUJAH ! UNTO CHILD I8 


 BEGINNING WORD . 


 WESLEY , S. 8 . BLESSED LORD GOD ISRAEL 


 7TH 


 ERIES MODERN KYRIES . HYMN 


 MR . BENTLEY SHORTLY PUBLISH - 


 ISTORY PRINCES DE CONDE 


 FOURFOLD MESSAGE ADVENT ; 


 CHARACTERS REPRESENTED 


 ACT 


 ACT II . 


 TIME PERFORMANCE HOUR MINUTES 


 W. H. BIRCH , 104 , LONDON STREET . READING 


 SS 


 MUSICAL TIM 


 


 CONCERTED MUSIC 


 PIANOFORTE ACCOMPANIMENT , 


 EDITED 


 JOHN HULLAHG 


 OLD ELM TREE 


 


 MODERN COMPOSERS 


 JANE MAYO . 


 LONDON : ASHDOWN PARRY , 


 HANOVER SQUARE 


 ! " IMS BREEZE TWI 


 OPINIONS PRESS 


 MUSICAL INSTRUMENTS KINDS . 


 MUSIC INSTRUMENTS.—DECEMBER SALE 


 W. SNELL IMPROVED HAKMUO.- 


 AUTHORISED MUSICAL TEXT - BOOK RUGBY SCHOOL . 


 ELEMENTS MUSIC SYSTEMATI- 


 SINGING MANUAL . 


 IMPORTANT SCHOOLS TEACHERS . 


 DEVELOPMENT VOICE 


 MVHE MUSICAL LADDER . ” 


 SECOND EDITION 


 SINCING CARD 


 SCHOOLS SINGING CLASSES . 


 MYHE LEVLTER - NOTE SINGING METHOD . 


 MUSIC EASY . . 


 CONSISTING FAVOURITE MOVEMENTS SELECTED THB 


 


 NEW WORK ORGAN 


 CHAPPELL ORGAN JOURNAL 


 EMINENT ORGANISTS 


 VIOLIN . 


 FLUTE . 


 CORNET 


 ENGLISH CONCERTINA . 


 GERMAN CONCERTINA 


 VIOLIN 


 INSTRUMENTS 


 CHAPPELL INSTRUCTION BOOKS 


 PIANOFORTE , HARMONY 


 CLARIONET . GUITAR 


 HARMONIUM . SAX - HORN 


 SINGING . SERAPHINE ANGELICA 


 FLUTE 


 CORNET - - PISTON 


 RECOMM 


 ENDED PROFE SSORS 


 CHAPPELL & CO . CHEAP PUBLICATIONS 


 DRUM FIFE , 


 VIOLONCELLO , 


 BANJO . 


 TROMBONE 


 GERMAN CONCERTINA 


 ENGLISH CONCERTINA 


 HARMONIUM 


 CHAPPELL & CO . , 50 , NEW BOND STREET , LONDON 


 S SII 


 SSUPPLEMENT 


 MUSICAL TIMESThe price Opera , sewed paper cover , 2s . 6d . ; , bound scarlet 
 cloth , 4s 

 BEETHOVEN “ FIDELIO , ” German English words , ready , 
 followed , January 1 , AUBER “ FRA DIAVOLO , ” French 
 English words , February 1 , MOZART “ DON GIOVANNI , ” Italian 

 English words 




 LONDON : NOVELLO , EWER CO . , 


 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 MR . GEORCE DOLBY 


 


 


 LONDON CLEE MADRICAL UNION , 


 PIANOFORTE , 


 LE CHEVALIER ANTOINE DE EKONTSEI . ' 


 VIOLIN , | 


 M. SAINTON 


 VIOLONCELLO , 


 1 , BERNERS STREET , W 


 — * 65,000 WORKS 


 LONDON : NOVELLO , EWER CO 


 ILLUSTRATED EDITIONS 


 WORKS 


 REV . J. H. INGRAHAM , LLD 


 THE*PRINCE HOUSE DAVID , 


 PILLAR FIRE , 


 THRONE DAVID , 


 LARGER SUPERIOR EDITION , 


 PRINCE HOUSE DAVID 


 PILLAR FIRE 


 THRONE DAVID 


 LONDON : GEORGE ROUTLEDGE SONS , BROADWAY , LUDGATE 


 SENDS CATALOGUE 


 WILLIAM S. BURTON , 


 GENERAL FURNISHING ITRONMONGER 


 APPOINTMENT H.R.H. PRINCE WALES , 


 GRATIS POST PAID . 


 016 0 


 ENDERS , STOVES , KITCHEN RANGES , 


 18 


 SUPPLEMENT 


 MUSICAL TIMES , 


 DECEMBER 1 , 1870 


 CHRISTMAS MUSIC . 


 _ _ _ _ Nove.io , Ewer Co 

 Fidelio . Opera Beethoven . Edited , 
 English words translated , Natalia Macfarren 

 Tus volume forms . 1 “ Novello Octavo Edition 
 Operas;”and presumed fair speci- 
 men follow , safely affirm 
 public length supplied best 
 compositions lyric stage clearly printed , 
 thoroughly accurate , moderate price 
 hand - books audience , 
 performance Opera , place 
 drawing - room study reliable works 
 reference . Opera music 
 spaced eye’is distressed ; words 
 legibly printed notes , 
 slightest difficulty arise following text 
 music simultaneously . important feature work 
 highly commended — German words , 
 Beethoven wrote music , given , 
 excellent English translation editress , , , 
 German musician , respect admirably 
 fitted task . ‘ T'o feel composer 
 intention shown giving text 
 ps ce suggested music , real boon ; 
 majority audience Opera read 
 English conventional - Book Words , ” 
 little doubt badly translated Italian 
 version service wish 
 tostudy work integrity 
 desire follow sense translation 
 native tongue . English text volume 
 shows earnestly Madame Macfarren felt 
 music , case German words find perfect 
 equivalent translation , merit need 
 searcely rarely met . order lead 

 Opera thorough enjoyment 




 714J. Gerson 

 Portrait Ludwig van Beethoven . Sketched fh 
 Schwirer ; drawn engraved Paul Barfus 

 Turs unquestionably finest portrait emt 
 seen great master . stern features , dishevelled 
 hair , conventional characteristics Beethova 
 found innumerable representations 
 years accepted trust ; 
 profoundly intellectual expression face , te 
 broad , massive forehead denoting exceptional powe 
 nobly proved contributions world 
 appear completely realised likeness , 
 readily believe , stated prospectus accollk 
 panying , great resemblance original ha 
 certified friends pupils Beethoven m0 " 
 living . figure composer appears fall length 
 wrapped cloak , hands resting 
 right knee . Dark clouds gradually breaking overs 
 head ; - ground steeple St. 5 
 Cathedral seen . Lovers art England , th 

 works Beethoven thoroughly appreciated , 
 fail possess portrait ex 
 design execution 

 AvGEener anp Co. 
 Le Sahel . Valse de Salon . Composed Sarah 




 715 


 716 


 _ _ _ 


 COMPOSED CHRISTMAS PRESENT YOUNG FRIENDS 


 F. MENDELSSOHN BARTHOLDY 


 WD 


 G== SS -—. 


 SS 


 — — — — — SS= SS = — 


 = = = = = = = SS 


 ANCIENT CHRISTMAS CAROL 


 


 BR . L. DE PEARSALL 


 — — — ’ P P . = . 


 OLO . 


 HORUS . ‘ 


 = SS SS 


 D 4 


 = SS SEES SS SS 


 EEA 


 CHRISTMAS NUMBER 


 LATE MR . CHARLES DICKENS 


 STUDY 


 BEST CHRISTMAS BOOK SEASON.—VOL . I. “ GRAPHIC . ” 


 PRICE GUINEA . OFFICE , 190 , STRAND , LONDON , W.C 


 ILLUSTRATED MONTHLY PERIODICALS 


 CHILDREN FRIEND 


 FAMILY FRIEND 


 INFANT MAGAZINE 


 BRITISH WORKMAN 


 ILLUSTRATED ALMANACS 1871 


 LONDON : 8 . W. PARTRIDGE CO . , 9 , PATERNOSTER ROW 


 NATALIA 


 COMPLETE UNIFORM EDILION 


 MENDELSSOHN SONGS 


 EDITED , TRANSLATED , 


 MACFARREN 


 34 . 


 47 . 


 LONDON : NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY _ ( E.C 


 03 10 0 6 


 ‘ SEVEN SHAKSPERE SONGS . & , MACFARREN 


 APAHAOPAR HPS R 


 ARMAARMS MARK ARAH 


 ELREA 


 NOVELLO - SONG BOOK 


 SECOND SERIES 


 COMPOSED 


 J. L. HATTON 


 VOL . VI . VOL . VII 


 SONGS S.A.T.B. SONGS MALE VOICES . 


 LONDON : 


 NOVELLO , EWER CO . , 


 1 , BERNEBS STREET ( W. ) , 85 , POULTRY ( E.C 


 MENDELSSOHN 


 LONDON : NOVELLO , EWER & CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 POPULARIn Pianoforte Score , English , French , German , 
 Italian words . 
 ( & English , ¥ French , @ German , 1 Italian 

 BEETHOVEN , EGMONT ( 6 ) « . ( mett)1 4 
 Bettist , NORMA ( ce 1 ) ose eee & & 
 CuErvBINI , DEMOPHON ( c F ) ... sie , & © 
 Giucx , ORPHEUS ( 6c F 1 ) eve ee ‘ wie 

 ALCESTE ( G4 F ) ae 




 DEDICATED , GRACIOUS PERMISSION 


 MAJESTY , 


 ROYAL HIGHNESS PRINCESS BEATRICE 


 


 LITTLE SINGERS 


 WORDS 


 FRANCES RIDLEY HAVERGAL 


 MUSIC COMPOSED ARRANGED 


 ALBERTO RANDEGGER . 


 ILLUSTRATED BROTHERS DALZIEL . 


 NOVELLO CHEAP OCTAVO EDITIONMENDELSSOHN Lieder ohne Worte , 

 8 Books complete ... aad nae 
 * BEETHOVEN Thirty - Sonatas ... 5 0 
 * % Thirty - Miscellaneous 

 Pieces ... er 




 NOVELLO , EWER CO . COMPLETE 


 UNIFORM EDITION 


 MENDELSSOEHN 


 APPENDIX FESTAL USE , 


 SET MUSIC 


 JOSEPH BARNBY . 


 EARLY 1871 


 SECOND EDITION . 


 REVISED , - CONSTRUCTED ENLARGED . 


 EDITED 


 REV . ROBERT CORBET SINGLETON , M.A. , 


 WARDEN ST . PETER COLLEGE , RADLEY , 


 


 ORGANIST CHOIRMASTER YORK MINSTER 


 OUSELEY MONK 


 POINTED CHANTING 


 SIXTH EDITION 


 EDITED 


 EDITED 


 LIST WORKS 


 SUITABLE 


 ALLCOTT NEW HARMONIUM WORK . — cco eo 2 
 co cece 

 Beethoven . 
 Bellini . Himmel , Rossini . cotch Airs . 
 Bishop . Handel . Schumann . Trish Airs . 
 Blumenthal . Horsley . Schubert . German Airs . 
 Callcott , Dr. Hullah . Shield . Russian Airs . 
 Callcott , W. H. Knight , Verdi . 
 “ dwelling place 
 sweet sounds harmonies . ’’ — Wordsworth 

 Lamborn Cock & Co. , 62 , New Bond - street . Price 10s . 6d . Net 




 STRONGLY 


 PRICE SEVEN SHILLINGS SIXPENCE 


 NATIONAL 


 F. A. FRASER 


 F. A. FRASER . 


 W. SMALL 


 E. GRISET 


 T. DALZIEL . 


 H. FRENCH . 


 J. B. ZWECKER 


 T. DALZIEL 


 H. FRENCH 


 W. SMALL 


 T. DALZIEL 


 W. J. WIEGAND 


 E. G. DALZIEL 


 J. MAHONEY 


 C. GREEN 


 A. B. HOUGHTON 


 T. DALZIEL 


 E. GRISET 


 E. GRISET 


 E. G. DALZIEL 


 W. J. WIEGAND 


 J. B. ZWECKER 


 H. S. MARKS 


 G. J. PINWELL 


 F. A. FRASER 


 LONDON 


 BOUND CLOTH , NOVEL DESIGN BLACK GOLD , GILT EDGES 


 NURSERY RHYMES 


 NURSERY SONGS . 


 SET MUSIC J. W. ELLIOTT 


 E. GRISET 


 E. GRISET 


 A. HUGHES 


 E. GRISET 


 J. MAHONEY 


 T. DALZIEL 


 J. B. ZWECKER 


 A. HUGHES 


 E. G. DALZIEL 


 W. J. WIEGAND 


 H. S. MARKS 


 J. B. ZWECKER 


 H. FRENCH . 


 H. S. MARKS . 


 E. DALZIEL 


 E. G. DALZIEL 


 W. J. WIEGAND 


 T. DALZIEL 


 J. B. ZWECKER 


 E. G. DALZIEL 


 F. A. FRASER 


 J. B. ZWECKER 


 E. GRISET 


 J. B. ZWECKER 


 E. GRISET 


 J. B. ZWECKER 


 NOVELLO , EWER , CO 


 1 BERNERS STREET , W. , 35 POULTRY , E.G 


 NN 


 AW 


 CLD NW YY 


 DEATH BURIAL COCK ROBIN 


 AMON 


 EXTRACT PREFACE 


 -TOOM , 


 S. JAMES HALL . 


 ORATORIO CONCERTS 


 CONDUCTOR - MR . BARNBY 


 SEASON , 1871 


 PASSIONdelssohn Oratorio 
 ELIJAH 

 given . arrangements Oratorio Concerts preclude possibility giving per- 
 formance celebration Beethoven , date birth ; , seeing 

 greatest work , 
 MASS D 




 CALVARY 


 ISRAEL EGYPT CAME , ” 


 HANDEL CHANDOS ANTHEMS 


 ST . PETER 


 NALA DAMAYANTI 


 BRIDE DUNKERRON , 


 CRYSTAL PALACE 


 GREAT TRIENNIAL 


 HANDEL FESTIVAL 


 JUNE , 1871 


 THOUSAND EXECUTANTS 


 CONDUCTOR 


 SIR MICHAEL COSTA