


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR


PUBLISHED ON


THE FIRST OF EVERY


MONTH


NOVEMBER 1, 1876


ROYAL ALBERT HALL 


SIXTH SEASON, 1876-77


AND 


AND 


ISRAEL IN EGYPT. 


STABAT MATER & HYMN OF PRAISE


£38. 38


ANDEL’S UTRECHT TE DEUM 


PROFESSIONAL NOTICES


MISS CARINA CLELLAND 


MR. G. MICKLEWOOD 


ALBERT E. BISHOP


USIC ENGRAVED, PRINTED, AND PUB- 


PUB- 


STANDARD 


AMERICAN ORGANS


MANUFACTURED BY


NEW YORK


PRICES, 12 TO 125 GUINEAS


DESCRIPTIVE CATALOGUE TO BE HAD ON APPLICATION TO 


BARNETT SAMUEL & SON, 


31, HOUNDSDITCH, E.C., 


RUSSELL’S MUSICAL INSTRUMENTS. ‘ 


AST LONDON ORGAN WORKS


EAN’S CHEAP MUSICAL INSTRUMENTS. 


LONDON AGENTS, 


PIANOFORTE SALOON, 


55, BAKER STREET, LONDON, W


TESTIMONIAL. 


GTRUNG UPON A SINGLE CASTING


FLAVELL


& W. SNELL’S IMPROVED HARMONIUMS, 


ROGER’S GUINEA CONCERT FLUTES


SECOND


SERIES


CANTICLES AND HYMNS


OF THE 


POINTED FOR CHANTING, AND SET TO APPROPR


CHURCH, 


IATE ANGLICAN CHANTS, SINGLE AND DOUBLE


TOGETHER WITH


EDITED


REV. SIR F. A. GORE


BY THE


OUSELEY, BART., ETC


EDWIN GEORGE MONK


TONIC SOL-FA EDITION


TONIC SOL-FA EDITION


A MANUAL OF SINGING


FOR THE USE OF CHOIR TRAINERS & SCHOOLMASTERS, 


FORTY-THIRD EDITION. 


THE COLLEGIATE AND SCHOOL


SIGHT-SINGING MANUAL


APPENDIX


(COMPANION WORK TO THE ABOVE


COLLEGIATE VOCAL TUTOR


CATHEDRAL CHANT BOOK


NEW EDITION OF


DR. BENNETT GILBERT’S POPULAR WORK


SCHOOL HARMONY


EVERY SUBJECT HAS ITS SPECIAL EXERCISES. 


R. L. PEARSALL


COLLEGIATE SERIES. 5 COSERETION OF


J. P. KNIGHT. ‘ 


LIVERPOOL SACRED HARMONIC 


SOCIETY


PATRONS


REVIVAL OF THE WORKS OF 


HENRY PURCELLX JANTED, from the beginning of January, an 
ORGANIST and CHOIRMASTER for the parish of West- 
mill. Surpliced choir. The present Organist holds the appointment 
of Music Master in the Buntingford Grammar School. There is a good 
field for choral classes and private tuition in the neighbourhood. 
Apply to the Rev. J. A. Ewing, Westmill Rectory, Buntingford

CHUBERT SOCIETY, Beethoven Rooms

27, Harley-street, W.—President: SIR JULIUS BENEDICT




ICHFIELD CATHEDRAL CHOIR.— 


THE MUSICAL TIM


THE MUSICAL TIMES 


AND SINGING-CLASS CIRCULAR. 


NOVEMBER 1, 1876


THE WAGNER FESTIVAL AT BAYREUTH 


BY JOSEPH BENNETT. 


649


TALLIS—WALTHAM ABBEY. 


BY WILLIAM H. CUMMINGS


650


DR. RIMBAULT


652


GEORGE COOPER


THE BRISTOL MUSICAL FESTIVAL. 


(FROM OUR SPECIAL CORRESPONDENT


CRYSTAL PALACEThe Crystal Palace has always been distinguished for 
the attention paid to the works of English composers. It 
was therefore only appropriate that the first concert of the 
present season should open with an Overture by the late 
Sterndale Bennett, the one selected being that to ‘“ The 
Merry Wives of Windsor,” which had not before been

heard at Sydenham. The Overture we are inclined to 
consider one of Bennett’s best works, full of pleasing ideas, 
and marked by that finish of detail which was one of his 
characteristics as a composer. Two other novelties were 
given at this concert—the one a solid and well-written, 
though rather heavy, Concerto by Hans von Bronsart, 
admirably played by Mr. Fritz Hartvigson; the other, a 
selection from a Suite by the French composer Ernest 
Guiraud, assuredly neither solid nor heavy, but on the 
contrary very light and piquant, though with no great 
depth of invention. On the same afternoon a superb 
performance of Beethoven’s Symphony in A was given. 
The vocalists at this concert were Madame and Signor 
Campobello

The second concert included Haydn’s delightful ‘ Mili- 
tary”? Symphony, and Weber’s Concertino for clarionet, in 
which the fine tone, remarkable execution, and excellent 
style of Mr. Clinton (the first clarionet in the Company’s 
band) were heard to great advantage. The special feature 
of the afternoon, however, was a large selection from the 
works of Wagner, including the Overture to ‘‘ Rienzi,” the 
Prelude and ‘“Elsa’s Dream” from ‘‘ Lohengrin,” the 
Funeral March from ‘“ Gétterdammerung,” and the March 
composed for the Centennial Festival at Philadelphia. 
Of these five numbers the first three have often been pre- 
viously heard in our concert-rooms; we need not therefore 
dwell upon them now. The Funeral March, following the 
death of Siegfried, with its truly wonderful instrumentation, 
was splendidly given under Mr. Manns’s direction, and 
created a profound impression, even apart from the stage. 
The Centennial March, on the other hand, is alike unworthy 
of the occasion for which it was written and of the repu- 
tation of its. composer. The vocalists at this concert were 
Miss Sophie Lowe and Mr. W. Shakespeare

At the fourth concert, the Crystal Palace Choir appeared 
for the first time this season; and, so far as can be judged 
from a single hearing, there seems to be a marked improve- 
ment in their performance. The chief feature of the con- 
cert was Gade’s charming Cantata ‘The Erl-King’s 
Daughter,” a work, by the way, which may be recom- 
mended to the attention of Choral Societies, as being full of 
beautiful music, and neither too long (it occupies rather 
less than an hour) nor too difficult. The solo parts were 
sung by Madame Lemmens-Sherrington, Miss Boling- 
broke, and Mr. Maybrick. An Adagio, for strings, from an 
unpublished and very early symphony of Haydn’s, was a 
novelty of special interest. The movement is given in the 
appendix to the first part of Pohl’s “ Life of Haydn,” and 
though decidedly old-fashioned in style, it was worth reviving, 
not merely from a historical point of view, but for its own 
sake. A second novelty was a ‘‘ Marche Héroique,” by 
Camille Saint-Saens, a brilliant composition, but one in 
which the workmanship is more interesting than the ideas. 
The Overture to “ Fidelio,”” Mendelssohn’s ‘‘ Reformation” 
Symphony, and two songs completed the afternoon’s 
programme

For the fifth concert, which took place after our going to 
press (on the 2oth ult.), the chief works announced were 
Raff's ‘Lenore’? Symphony, Schubert’s Overture to 
‘* Alfonso and Estrella,” Sullivan’s ‘‘Ouvertura di Ballo,” 
and Beethoven’s Violin Concerto, to be played by M. 
Wieniawski

CARL ROSA OPERA COMPANY




LONDON CHURCH CHOIR ASSOCIATION. 


656


ANTHEM FOR CHRISTMAS


~ —— = SS . ==: 


AR 


HHH


G— “3B 2 <$-$-33-3) 23 


4 J 


_ UA


,__ QUARTETT. 


QUARTETT


ND 


LI 


2. 4 4 —, 


QUARTETT


OL


QUARTETT. _—__ 


QUARTETT


THE ANGEL GABRIEL WAS SENT FROM GOD


{ S——— E


CN 


C74 


666ALL persons actively engaged in musical tuition will agree 
with us that but little is known, even amongst cultivated 
amateurs, of Mozart’s Sonatas. True itis that some three or 
four of those most popular in character have taken a stand 
in public estimation, and are now and then to be found 
embedded in a heap of showy pieces forming the contents 
of a ‘‘Canterbury”’ in a fashionable drawing-room ; but the 
very fact of these being selected to represent Mozart’s 
genius in this class of composition prevents many from 
searching more deeply into the mine of wealth which Miss 
Zimmermann has brought together in the attractive volume 
before us. And yet, not only for purity of melody and 
masterly construction, but as studies for touch, phrasing, 
and the cultivation of every shade of expression, these 
works remain unrivalled, many indeed of the Sonatas but 
rarely played containing beauties which cannot but surprise 
those who hear them for the first time. The editress, like 
a true artist, approaches her task with reverence; and in 
her Preface, therefore, gives her reasons for altering or 
inserting anything which might offend those who rigidly 
demand the text of Mozart. The little she has done in 
this way, however, needs but small apology, for the slurs 
(some of which are added and others lengthened) accurately 
define, as she says, “ the phrasing and the musical sense 
of the different passages :” these will doubtless be felt as a 
valuable guide to those who study without a master, and 
cannot but help even the professor, who has often to supply 
by explanation to his pupil what should be in all cases 
clearly shown upon the paper. The one change of notation 
in a portion of the Trio of the Sonata No 11 is perfectly 
justifiable ; for as everybody sustains the melody in-these 
passages, it is as well to write them as they are to be 
played. The vexed question of the appoggiatura and 
acciaccatura has been settled by drawing a line through 
the latter wherever it occurs, and allowing the former to 
receive its full value as an emphasised note, retaining in 
both cases the original notation. This is perhaps as_it 
should be; but, after all, the importance of preserving the

old appoggiatura is more a matter for the eye than the ear, 
especially as we know that Mozart himself did not adhere 
to any positive system, in proof of which we may instance 
the. celebrated Sonata in A minor, No 8, in which the 
opening subject, at the commencement of bar 2, has A as 
an appoggiatura before G¥, and on its repetition, in bar ro, 
the same note appears as an ordinary quaver linked to the 
following one. Where an experienced musician like Miss 
Zimmermann, however, carefully separates the appoggiatura 
from the acciaccatura, all doubt upon the subject is set at 
rest, and the notation becomes of little consequence to the 
player. We are glad to find that in this edition every care 
has been taken to ensure correct and intelligent phrasing 
by attention to the minutest marks of punctuation, and by 
plain directions in the Preface as to the manner of pro- 
ducing the required accents by the touch. In Mozart’s 
time, however, it is well known that much was left to the 
taste of the performer, in confirmation of which we could 
cite the bars marked “a piacere,”’ in the last movement of 
the Sonata in C minor, No 14, where, at every one of the 
pauses, we have the authority of a late enthusiastic ex- 
ponent of Mozart’s music for saying, it was the custom (we 
presume imitated from the method initiated by the composer 
himself) to introduce some short ornamental passages, of 
which we find no indication in the notation. In proof of 
the necessity of selecting a first-class executant to edit a 
work of this importance, it may be well to call attention to 
the fingering, which we need scarcely say will be found 
invaluable to amateurs. A great change has taken place 
in the system of fingering lately; and Miss Zimmermann 
is, we see, not conservative enough to adhere to an old 
method when a passage can be more freely executed or 
more accurately phrased by a new one. In conclusion, we 
have only to say that the music is clearly printed, and that 
the date of the composition of each Sonata, wherever it 
could be ascertained, is stated. The purity of the text has 
been certified by reference to the best English and foreign 
editions; and in every respect the volume is a worthy 
companion to that recently issued by the same firm and 
under the same editorship, containing the whole of the 
Sonatas of Beethoven

Musical Myths and Facts. By Carl Engel. Vol. 2




WILLIAM REEVES


B. WILLIAMS


ORIGINAL CORRESPONDENCE


DOUBLE BARS IN HYMN TUNES


TO THE EDITOR OF THE MUSICAL TIMES. 


EXAMPLE :— 


THE STEINWAY PIANOFORTES. 


TO THE EDITOR OF THE MUSICAL TIMES


THE COMMUNION OFFICE. 


TO THE EDITOR OF THE MUSICAL TIMES


TO CORRESPONDENTS


670


BRIEF SUMMARY OF COUNTRY NEWSTHE MUSICAL TIMES.—Novemser 1, 1876. 671

hoven’s Sonata (Op. 12) for violin and pianoforte, which was performed 
by Mr. Peck and Mr. J. W. Phillips. Two overtures—* La Dame 
Blanche” (Boieldieu) and ‘‘ Le Pré aux Clercs” (Herold)—were effec- 
tively played by the orchestra. An Evening Concert, under the same 
management, was also given at the Cutlers’ Hall. Miss Barton was 
the vocalist. Mr. Whitehead contributed: a violoncello solo, and Mr. 
Phillips joined with Mr. Peck in one of Beethoven’s Sonatas. Some 
good glee-singing was also given.——On Monday, the 16th ult., Miss 
Clara M. Linley gave a Concert at the Albert Hall. The pianoforte 
playing of Miss Linley was, doubtless, the chief item of interest, her 
most important pieces being Sterndale Bennett’s ‘‘ Maid of Orleans” 
Sonata, and Liszt’s ‘“‘ Rhapsodie Hongroise,” both of which were 
encored. Mr. John Peck (violin) rendered valuable assistance, 
especially in the duet with Miss Linley, ‘Guillaume Tell,” by 
De Beriot and Osborne, the finale of which was redemanded, and 
Mr. J. Wainwright was highly successful in his performance on the 
English concertina. Madame Thaddeus Wells and Mr. Laxton were 
the vocalists. The band of the 15th regiment, under Mr. Murdoh, 
played several selections with much effect

STAFFORD.—Mr. E. W. Taylor, organist and choirmaster of St. 
Thomas’s Church, gave a most successful Concert in the Shire Hall, 
on Monday evening, the 2nd ult., when the following eminent artists 
were engaged: Madame Lemmens-Sherrington, Miss Jessie Jones, 
Madame Patey, Mr. Hollins and Mr, Patey, vocalists, and Herr 
Theodor Frantzen, solo pianist. An excellent programme was pro- 
vided, and rendered in a most satisfactory manner

SouTHAMPTON.—Mr. J. Ridgway has given two Pianoforte Recitals 
during the past month at Hartley Hall, at which he played selections 
from the works of Bach, Beethoven, Mozart, Mendelssohn, Schu- 
mann, Dussek, Chopin, Liszt, Sterndale Bennett, &c. The vocalists 
have been Miss Amy Aylward and Miss Dones, and Miss Ridgway 
acted as accompanist

SouTHEND.—A Concert was given at the Public Hall, on the oth ult., 
by Miss Goodman, assisted by Miss Annie Butterworth, Mr. Stedman, 
Mr. Thurley Beale, and Mr. Osborne Williams. Miss Goodman’s 
singing of “O mio Fernando” shewed her to be possessed of capa- 
bilities which, united to an excellent voice, should, with study, enable 
her to take a good position. Miss Annie Butterworth gave an artistic 
rendering of Smart’s “Lady of the Lea.” Mr. Stedman and Mr. 
Thurley Beale sang with their accustomed success, and Mr. Osborne 
Williams played two pianoforte solos much to the satisfaction of the 
numerous audience

WALLINGFoRD.—The organ lately erected in St. Leonard’s Church 
was successfully opened on the 26th Sept., on which day the Harvest 
Festival was also observed. The new instrument, built by Messrs. 
Ginns Brothers, of Merton, Surrey, has been pronounced highly 
satisfactory by the most competent judges, and its quality was 
effectively displayed by Dr. Sloman and Mr. A. Eyre, R.A.M., who 
presided respectively at the morning and evening services

WeysripGe.—The Harvest Festival Service took place on Thursday 
evening, the 28th Sept., at St. James’s Church, which was beautifully 
decorated with corn, flowers, ferns and fruit, &c. Evensong com- 
menced at eight o'clock, and the following music was performed: 
Processional Hymn, “ We plough the fields;” Ely Confession and 
Tallis’s Responses, Proper Psalms to Chants by Sir G. Elvey, Magni- 
ficat and Nunc dimittis to Wesley in F; Anthem, “ O give thanks” 
(Sir J. Goss); Hymn before Sermon, “Come, ye thankful people ;” 
after Sermon, ‘‘ The sower went forth sowing ;” Recessional, ‘‘ Hark ! 
hark my soul!’ The music was well sung by the surpliced choir, num- 
bering over 40 voices, assisted by about 30 ladies. The Rev. W. Money 
intoned the service, and the Rector (the Rev. E. Rose) preached an 
eloquent sermon. After the service Mr. Brooke (the organist and 
choirmaster) played a selection from the works of Handel, Beethoven, 
and Bach. The congregation numbered 1,100, and the offertory, for 
the sufferers in Bulgaria, amounted to over £50

Wok1nGHaM.—The Harvest Thanksgiving was held in S. Paul’s 
Church on Sept. 28th. At the choral service in the evening the choir 
of S. Paul’s was assisted by that of All Saints’. The anthem was 
“The Lord hath done great things,” by H. Smart. The church was, 
as usual, decorated by the ladies of the parish




DURING THE LAST MONTH


12. 7 


OVELLO’S TONIC SOL-FA SERIES. 


OVELLO’S OPERA CHORUSES. 


672


GREAT SALE OF MUSICAL PROPERTY. 


IANOFORTES, HARMONIUMS & AMERICAN 


O PIANOFORTE DEALERS, SHIPPERS


BUSINESS FOR SALE. 


SALES SPECIALLY OF MUSICAL PROPERTY. 


EXTENSIVE ASSEMBLAGE OF MUSICAL PROPERTY


TUTTGART HARMONIUM COMPANY. — 


CAN 


RS, 


R. FREDERICK STEVENSON, ORGANIST, 


OUTH LONDON MUSICAL TRAINING


PARTIES MADE UP FROM THE FOLLOWING EMINENT 


IANOFORTE, ORGAN, HARMONIUM, SING


HE YORKSHIRE CONCERT PARTY.— 


HE YORKSHIRE (ST. CECILIA) CONCERT 


ESTABLISHED APRIL 1866. 


T#. ENGLISH GLEE UNION. 


ASSISTED BY 


# HE LIVERPOOL QUARTETTE,” 


ADIES’ SCHOOL, LANGHAM HOUSE


USICAL EXAMINATIONS.—CANDIDATES


RSANIST REQUIRES RE-ENGAGEMENT. 


AKER’S PIANOFORTE TUTOR


DEDICATED, BY PERMISSION, TO


THE ANGLICAN HYMN-BOOK


NEW EDITION, REVISED AND ENLARGED. 


HE ANGLICAN CHORAL SERVICE BOOK. 


USELEY AND MONK’S PSALTER AND 


OULE’S COLLECTION OF WORDS OF 


HE PSALTER, PROPER PSALMS, HYMNS, 


OULE’S DIRECTORIUM CHORI ANGLI- 


OULE’S DIRECTORIUM CHORI ANGLI- 


HE ORDER FOR THE HOLY COMMUNION. 


LBERT E. BISHOP’S MORNING SERVICE 


676


THE VILLAGE ORGANIST


OPINIONS OF THE PRESS


CHORAL SONGS FOR SCHOOL & HOME


FORTY-TWO ORIGINAL SONGS 


FOR 


ONE, TWO, OR FOUR VOICES. 


PART-SONGS FOR CHRISTMAS


THE DEATH OF THE OLD RING OUT, WILD BELLS. 


YEAR. A SONG OF THE SEASON. 


THE SKATER’S SONG


THE HOLLY. 


BE MERRY, ALL. 


THE PLAIN GUIDE TO SINGING, 


IN TWELVE LESSONS. 


SNEINTON, NOTTS


THE STUDENT’S TEXT-BOOK


OF THE


SCIENCE OF MUSIC


VOCAL ALBUM OF PART MUSIC


EDITED CHIEFLY BY


PROFESSOR G. A. MACFARREN


ORDER PROFESSOR MACFARREN’S EDITIONS. 


BARRACLOUGH


JK


IMPORTANT TO PROFESSORS OF SINGING. 


ELEMENTARY VOCAL STUDIES


BY


EDWARD LAND


TO WHICH IS ADDED


SELECT “SOLFEGGI


MUSIC


FOR THE PIANO


FOR THE ORGAN


YDNEY SMITH’S PIANO METHOD


OURNALISM: ITS DUTIES, ITS TEMPTA- 


SIX PART-SONGS 


FOR FOUR AND FIVE VOICES, BY 


JACQUES BLUMENTHAL


WILLIAM J. YOUNG’S 


SUNG BY MR. SANTLEY. 


ONE CAN FLY MY LAW SUPREME


GRUNEISEN


TO ORGANISTS AND CHOIRMASTERS. 


678


COMPOSITIONS


BY


EDWARD HECHT


SONGS


TRIO. 


FOUR-PART SONG. 


QUARTETTS. 


PIANO SOLO


WWWWHWWW 


THE CONGREGATIONAL PSALMIST. 


SONG FOR BASS OR CONTRALTO


COMPOSED BY


REDUCED TO ONE SHILLING. 


ISTORICAL SONG OF ALL THE KINGS 


ONE HUNDRED THOUSAND OF 


D®: FOWLE’S “POPULAR COLUMN” 


RISTOL MUSICAL FESTIVAL, 1876. — 


YE THAT LOVE THE LORD.”* 


“THEY THAT FEARED THE LORD.” 


“IF YE LOVE ME


WELVE NEW CHRISTMAS'- CAROLS, 


DR. FOWLE’S 


R. FOWLE’S “ORGANIST’S MARCH


HRISTMAS DAY.—DR. FOWLE’S 


HRISTMAS DAY.—DR. FOWLE’S


HRISTMAS READINGS.—DR. FOWLE’S 


HRISTMAS DAY.—DR. FOWLE’S 


HRISTMAS DAY.—DR. FOWLE’S


HRISTMAS DAY.—DR. FOWLE’S


HRISTMAS DAY.—DR. FOWLE’S 


HRISTMAS WEDDINGS.—DR. FOWLE’S 


HRISTMAS VOLUNTARIES.—DR. FOWLE’S 


HRISTMAS PRESENT.—DR. FOWLE’S 


HRISTMAS TUNE BOOK.—DR. FOWLE’S 


HRISTMAS CONCERTS:—DR. FOWLE’S 


HRISTMAS INSTRUMENTS.—DR. FOWLE 


HRISTMAS COMPOSERS.—DR. FOWLE 


ANTHEMS - FOR CHRISTMAS


EJOICE... 


ELVEY, SIR G. J—ARISE, SHINE, FOR THY LIGHT 


IS COME 


GADSBY, H.—SING, O DAUGHTER OF ZION 


GOSS, SIR J.—BEHOLD,I BRING YOU GOOD TIDINGS 


HOPKINS, E. J—LET US NOW GO EVEN" ‘UNTO 


BETHLEHEM ... 


MACFARREN, G. A._FOR UNTO US WAS BORN . 


— WHILE ALL THINGS WERE IN QUI&T SILENCE 


MENDELSSOHN.—REJOICE, O YE PEOPLE 


NOVELLO, V.—SING UNTO THE LORD.. $ 


PERGOLESL— GLORY TO GOD IN THE HIGHEST... 0 1} 


MITH, C. W.—BEHOLD, I BRING — GLAD


TIDINGS ° 


SILAS, E. —THE LIGHT HATH ‘SHINED . C7) 


SMART, H.—THE ANGEL GABRIEL WAS SENT


THORNE, E. HIN THE BEGINNING WAS THE


WORD 


NEW CHRISTMAS ANTHEM FOR PARISH CHOIRS


HE SHALL BE GREAT.” 


ANTHEM FOR CHRISTMAS


COMPOSED BY


CHARLES JOSEPH FROST


NEW ANTHEM FOR CHRISTMAS. 


¢ BLESSED IS HE THAT COMETH IN THE 


HURRAH FOR BLUFF KING CHRISTMAS! 


N ENGLISHMAN’S CHRISTMAS SONG; 


A SELECTION


CHRISTMAS CAROLS


FROM 


ARRANGED FOR


MEN’S VOICES


LONDON


CONTENTS


MUSICAL MYTHS AND FACTS


CARL ENGEL


CONTENTS. 


VOL. I


VOL


II. 