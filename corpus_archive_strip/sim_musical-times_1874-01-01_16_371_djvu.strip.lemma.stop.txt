


 MUSICAL 


 singing - class circular . 


 publish month 


 CHARLES HALLE . 


 " PRACTICAL PIANOFORTE SCHOOLSECTION i.—*‘elementary . " 
 s. d. | 

 3 . Kalkbrenner , F. , Kuhlau , F. air variation 
 | 4 . Beethoven , L. van . Sonatina G 
 3 : Hummel , J. N. Rondo c ose 

 Steibelt , Sonata c 




 JANUARY 1 , 1874 


 5 0section ii . — * easy 

 11 . Hiller , F. album leave , op . 117 
 12 . Beethoven , L. van . variation " nel cor piu non , " ' G 
 | 13- Heller , Stephen . Rondino g ove exe 
 14 . Kuhlau , F. Andantino Polacca fp . 
 15 . Beethoven , L. van . Sonata g minor , op . 49 ; . 
 16 . Clementi , M. Sonatina F , op . 36 , . 4 
 | 17 . Mozart , W. A. Rondo D ' 
 18 . Clementi , M. Sonatina e flat , op . 37 , . yt 
 19 . Beethoven , L. van . Swiss Air , variation F . 
 20 , Clementi , M. Sonatina D , op . 36 , . 6 

 4 Clementi , M. Sonatina C , op . 36 , no.1 
 Beethoven , L. van . SonatinainF ... 
 L. Sonata G , op . 20 , . 1 

 4 

 Dussek , J = ~ ee 4 
 4 . Clementi , M. Sonatina G , op . 36 , . 2 4 
 . Kuhlau , F. Sonatina C , op . 20 , no.1 ... tee pep 
 Beethoven , L. van . Sonata G , op . % . 2 os 5 
 4 

 4 

 section iii . — ' ' moderately difficult 

 1 . Scales major minor ti chromatic 6 . Beethoven , L. van . Sonata G major , op . 14 , . 2 

 ales . Field , J. nocturne ... 
 2 . Dussek , J. L. Sonata major , ' Op . 20 , . 4 . Reinecke , Cc . meee ate Song Toceatina , 
 > Hunten , F , Rondoletto C major 
 4 . Haydn , J. Sonata g major os 
 ' Beethoven , L. van . Rondoin c major , Op . 51 , , . 
 6 . Schumann , R. album leave sans selection ) op . 68 
 ~ Mozart , W. A. Sonata inc ma phd 
 Beethoven , van . Air , jariation , g. major 
 9 . Hummel , J.N Rondo villageois , c major , Op . 122 
 . Clementi , M. _ Sonata e flat major , op . 20 ave 
 Bertini , H. characteristic piece , op .. 29 32 
 1 % , Bach , J.S. Minuets gavotte 
 13 . Mozart , W.A. Sonata f major 
 14 . Weber , C. M. von . Andante con Variazioni , ' Minuetto 
 Rondo , op . 3 oo - ace ase 
 15 . Schubert , F. Valses ... * oe 

 IV . — " difficult 

 aa 

 p- 77 
 . Clementi , M. Rondo d major , op . 39 eal ove 
 Heller , Stephen . melody ... 
 - Beethoven , L. van . Rondo g major , op . 51 , " no.2 
 > Mendelssohn , F. Thé Rivulet eve 
 Bach , J.S Prelude , Aria Courante wo wee 
 - Haydn , -f ee e flat ... 
 - Hummel , J. N. " La Contemplazione , ' ° op . 107 
 . Handel , G. F , " Harmonious Blacksmith ... 
 . Mozart , W. A. Sonata major ct 
 s Mendelssohn , F. Songs word ... 
 Dussek , J. L. Andantino Allegro G , op . 39 
 Beethoven , L. van . Sonatina g major , op . 79 oa 

 ory eee 

 oo 

 1 , Hummel , J. N. Rondo e flat , op . ve soe 
 2 . Schumann , R. characteristic piece , op . 124 
 “ s Beethoven , L.van . Andante f major 

 Bach , J. S. ’ Preludes fugue , Das Wohitem 

 eber , C. M. von . Rondo Brillante ine flat , Op . 62 . 
 Semnert , F. impromptu flat , op . 142 , . 2 

 Beethoven , L. van . Sonata Pathétique , op . 13 
 Scarlatti , D. study f , c , 

 Mar 

 24 . Scarlatti , D. Harpsichord lesson cc minor , ' d e 

 25 . Beethoven , L. van . sonata C = gh minor , op . 27 , . 1 , 
 ( " moonlight " ) po ’ 

 26 . & chumann , " Humoresque , " Op . 20 ( 1st movement 




 FORSYTH BROTHERS 


 LONDON : 


 MANCHESTER 


 CROSS STREET SOUTH KING STREET 


 professional notice . 


 HENRY J. LEWIS 


 MR . CHARLES JOSEPH FROST 


 MR- J. TILLEARD , 


 USIC engrave , print , PUB . 


 RUSSELL MUSICAL instrument . 


 W. SNELL improve harmonium 


 grateful — comforting . 


 breakfast 


 345 


 _ — _ — _ 


 HARGREAVES professional 


 connoisseur ’ harmonium . 


 ( register 


 " * professor harmonium . ' " 


 HARGREAVES AMERICAN IRON FRAME PIANO 


 WILLIAM HARGREAVES , 61 , DALE ST . , MANCHESTER 


 _ — _ 


 TETTE 


 important trade , proprietor insti- 


 tution , public general 


 point 


 reduce price 


 OUSELEY MONK 


 PSALTER 


 LONDON 


 NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 TONIC SOL - FA edition . 


 ready . 


 interval counterpoint 


 manual singing 


 use choir trainer & schoolmaster . 


 - edition . 


 COLLEGIATE SCHOOL 


 sight singe manual 


 ( companion work 


 COLLEGIATE VOCAL tutor 


 CATHEDRAL chant book 


 village ORGANIST 


 OL 


 345 


 pastoral OPERETTA . 


 


 MERRIE MEN SHERWOOD FOREST 


 FOREST day olden time , 


 character represent . 


 ACT I. 


 SCENE — SHERWOOD FOREST . home outlaw 


 ACT ii 


 CHAPEL SCENE — wedding ROBIN HOOD MAID MARIAN . 


 DAY festivity 


 ACT iii 


 SCENE I.—A dense forest . capture SCARLETT 


 SCARLETT 


 SCENE III.—SCAFFOLD SCENE market place , NOTTINGHAM . 


 ROBIN HOOD defy sheriff vengeance . TRIUMPHAL RES- 


 cue SCARLETT ROBIN HOOD MERRIE man 


 W. H. BIRCH NEW operetta . 


 EVELEEN , rose vale 


 SCENE I.—THE HAYFIELD . 


 SCENE II.—THE village FESTIVAL 


 C. JEFFERYS , 67 , BERNERS ST 


 CHORAL SOCIETY 


 PRICE twopence 


 GRAND OPERATIC duet . HN OWEN 

 JO 
 melodious melody : — 
 subject select work Beethoven , 
 Mendelssohn , Schumann , Heller , & c. , arrange 
 Organ Harmonium . price 2 . 6d . nett 

 SONGS child 




 SCENE III.—THE HARVEST HOME . 


 entitle 


 KATRINE , pride KILLARNEY 


 CHARACTERS . 


 WILLIE PAPE . 


 C. JEFFERYS & CO . ENGLISH MODEL HAR- 


 67 , BERNERS STREET , W 


 ROYAL ALBERT HALL CHORAL SOCIETY . 


 sixth subscription concert . 


 THURSDAY , JANUARY 8 , 1874 . 


 HAYDN CREATION 


 


 ORGANIST ' . 


 ICHFIELD CATHEDRAL CHOIR.—_WANTED 


 347 


 MUSICAL TIMES , 


 JANUARY 1 , 1874 


 BACH CHRISTMAS ORATORIO 


 348 


 349 


 352 


 352 


 353 


 NATIONAL TRAINING SCHOOL MUSIC . 


 354 


 agn 


 2 ) 4 } 


 ? 1 1 j — : T 54 


 354 


 _ 9 .. 


 TREBLE . 


 ALTO . 


 TENOR - 


 2 = 


 4 7 . 


 RA 


 LADY , 


 " 4 " p < , 


 


 359 


 CRYSTAL PALACEtHe english public . occasion 

 monstrative mark approbation fairly earn 
 eyond question , Liszt work imperatively demand 
 power Herr von Bilow bring task , 
 glad , , find frequent 
 opportunity introduce music english 
 audience . Beethoven ninth Symphony form fitting 
 termination series concert . solo portion 
 sing Madame Otto - Alvsleben , Miss Marion 
 Severn , Mr. Werrenrath , Mr. G. Fox . Mr. Manns 
 , usual , able conductor season 

 ROYAL ALBERT HALL CHORAL SOCIETY 




 360 


 _ — _ 


 361tue concert Wagner Society , 12th ult . , 
 conduct Dr. Hans von Bilow Mr. Edward 
 Dannreuther , , , mainly devote 
 work composer Association bear . 
 admire selection 
 public judge abstract 
 merit descriptive music , far 
 able test effect continuous Opera ; 
 Herr Wagner demand try audience 
 opera - house , concert - room , appear strange 
 effort Society direct 
 effect stage performance 
 tepresentative Operas . , , bind 
 concert Society excellent ; 
 sincerely trust receive support 
 deserve 

 _ tue recital Dr. Hans von Bilow gradually 
 increase attraction , final , 2oth ult . , 
 attend crowd attentive enthu- 
 siastic listener pianist allow room 
 orchestra gain free passage seat . un- 
 questionably great performance Beet- 
 ‘ hoven sonata b flat ( Op . 106 ) , " scherzo " 
 movement ( include im- 
 mensely difficult ' Fuga tre voci ’' ) encore , , 
 wonder audience , repeat 
 ierease diminution power . Weber Sonata 
 D minor marvellous exhibition executive 
 facility , grasp passage , , occasionally 
 de display exceptional gift 
 somewhat disturb equanimity 
 attention direct realisation com- 
 poser meaning . Recital Herr Bilow 
 assist M. Sainton ( violin ) M. Lasserre ( violon- 
 ilo ) , vocal piece contribute Madlle . 
 NitaGaétano . Mozart Trio E major , Beethoven 
 B flat ( Op . 97 ) finely play occasion ; 
 good Herr Biilow small pianoforte solo 
 ee autcia Barcarole G major Liszt 
 es Lutins , " giv 

 nc , » " 
 Herr Ernst Paver lecture South Kensington 
 useum attend real interest 




 362 


 reviewslife Moscheles ; selection Diaries ang 
 Correspondence , Wife . adapt original 
 German A. D. Coleridge 

 life earnest thoughtful artist 
 Moscheles deeply interesting , 
 record career write authenticity 
 confidently rely ; volume 
 copious extract diary corres . 
 pondence , impression art . dearly 
 love , relation celebrated 
 musical contemporary , 
 invariably related word . satisfactory , 
 , peruse recollection , know 
 world express desire Moscheles 
 ; collect 
 arrange supervision wife — thoroughly 
 share husband artistic enthusiasm — scarcely 
 necessary add care bestow 
 portion work , materially enhance value , 
 Moscheles bear Prague 30th , 1794 ; 
 early day horror French Revolution 
 constantly discuss presence . play 
 soldier favourite amusement boy ; 
 military band perform guard . 
 house , young Moscheles hold music 
 bandsman , return open - air con- 
 cert , exclaim enthusiastically , ' ' , , bea 
 musician . " whilst listen sister somewhat 
 clumsy pianoforte playing , felt impel assert 
 perform piece ; trial 
 power grant , permit lesson , 
 , course , rapid advancement , short 
 time pet indiscreet friend " infant 
 prodigy . " luckily father good sense pet 
 ceive son gradually spoil 
 indiscriminate flattery , check 
 mischief Dionys Weber . Moscheles , 
 having deck mother " Sunday 
 good ’' expressly occasion , sit piano- 
 forte , conceit , play Beethoven " Sonata 
 Pathétique . " opinion Dionys Weber 
 write letter gold , hang musi 
 educational establishment : ' candidly speak , " 
 , ' ' boy wrong road , 
 great work , understand , 
 utterly unequal . talent , ! 
 hand ovet 
 year , follow plan letter . 
 year play Mozart , se 
 Clementi , Bach ; — note . 
 Beethoven , persist use ol 
 culate library , . " 
 stipulation father willingly assent , 
 foundation future success securely lay . 
 journey Vienna open new world att , 
 residence study thorough bass 
 counterpoint Albrechtsberger , musi 
 party constantly habit meet Beethoven . 
 quit imperial city artistic career faitl 
 commence . Paris create real sensation , m 
 fame firmly establish , arrival 
 London lesson eagerly seek youlg 
 lady desire acquire quality 
 playing celebrate . return { 0 
 Vienna , London season , Moscheles pay visit 
 Beethoven , accompahie brother , ac 

 _ 

 363 

 Beethoven dislike stranger , ask brother 
 wait whilst feel myway . short greeting , 
 jaske Beethoven , ' allow introduce 
 brother ? ' reply hurriedly , ' , 
 ? ' ' , ' answer . ' ! ? ' 
 , vehemence ; rush stair , 
 seize astonished brother arm , drag 
 middle room , exclaim , ' 
 barbarously rude unapproachable ? ' 
 great kindness stranger . unfortunately , account 
 deafness , converse write 

 Berlin Moscheles appear consider musical 
 matter unimportant compare reception 
 Mendelssohn family . visit write 
 young Felix : ' " family like 
 know . Felix , boy , 
 henomenon . prodigy compare 
 m ? gift child , . Felix 
 Mendelssohn mature artist , 

 NovELLo , EWER Co 

 sonata Pianoforte ; compose L. van 
 Beethoven . " edit finger Agnes Zimmermann . 
 Ir truly remark person 

 Mary possess Bible Shakspeare . 
 tot person musical 
 volume Beethoven Sonatas ? 

 store mental wealth contain work — like 

 age , time . 
 music — term pervert 
 day — fully represent varied style 
 imperishable composition , 
 mixed audience experience delight contemplate 
 obvious beauty , earnest artist search 
 perfect model form distinguish work 
 bygone time , free genial movement 
 gradually develop , 
 profound thought romanticism term 
 " advanced school , " find ample material 
 study . appreciation Sonatas 
 rapidly increase , short time need look 
 recall day country 
 unknown . pianist expo- 
 nent work public remember 
 Monzani Hill edition seek 
 student begin think , tell 
 master worth attention . ' 
 melody Beethoven , " sarcastically remark Mr. 
 Cipriani Potter , whilst play charming 
 portion Sonatas pupil . ' ' listen — 
 ' heavy ' ? , throw , 
 fairy - like touch , beautiful scherzo : 
 remember tell student 
 Royal Academy Music publisher 
 ask Sonata Beethoven ' 
 play , " smile satisfaction return 
 right . Mr. Potter 
 pioneer good cause ; Moscheles present 
 work public find oppor- 
 tunity ; edition , , begin gradually find customer , 
 , spite ' * heaviness , " general impression 
 got abroad ' . ' " ' 
 sanguine musician time , , 
 scarcely imagine , comparatively 
 year , public taste educated 
 composition play 
 teach , demand 
 great England land 
 composer birth . , , 
 credibly inform , fact ; scarcely 
 wonder , , endeavour 
 english publisher present accurate 
 thoroughly reliable shape , Miss Agnes Zimmermann , 
 edit luxurious edition , fully 
 earn right respect responsible position . 
 enthusiastic student Beethoven work , 
 able interpreter public , 
 undertake duty editress high feeling 
 merely correct proof , ' ' work 
 press , " place , know 
 compass pianoforte extremely limited 
 time Sonatas write ; , consequently , 
 ( Miss Zimmermann state preface ) ' ' passage 
 , movement , appear 
 certain form , oblige , recur 
 second , high key , compress , want 
 note . " obvious course open 
 editress , view fact ; place , 
 principle ' " ' , right , " 
 reprint passage original integrity ; , 
 second place , boldly challenge 
 criticism alter , accord enlarged 
 compass instrument . Miss Zimmermann wisely 
 adopt middle course , ' merely suggest form 
 passage , small stave , Beethoven 
 likely write ; leave text 
 stand , prefer , inno- 
 vation , , scarcely sanction , 
 suggestion . case ; , 
 instance , necessity alteration induce 
 composer introduce new beauty phrase , 
 compensate enforce change . 
 editress disarm criticism announce , 
 stich instance occur , " alteration propose ; 
 , compensatory element exist — 
 plain mechanical limitation instrument 

 immortal book — 

 MUSICAL TIMES.—January 1 , 1874 

 passage , passage print shape 
 probably write , key- 
 board Beethoven time present extent . " 
 plan carefully conscientiously carry , deserve 
 warm commendation . mention 
 Sonatas finger , 
 indication necessary ; amateur , , 
 receive gratuitous lesson com- 
 petence task sufficiently prove 
 thorough mastery key - board . feature 
 interest — utmost importance 
 desire fully acquaint composer 
 intention — manner slur 
 place . far , 
 add ( , fugal movement , 
 find necessary repetition subject agree 
 , originally ) ; 
 instance , slur stop short final 
 note passage , carry , 
 punctuation case determine phrasing 
 accuracy . thirty - Sonatas , include , 
 volume contain write composer 
 year age , dedicate Maximilian 
 Frederic , Elector Archbishop Cologne ; dedi- 
 cat Madlle . Eleonore de Breuning — 
 conclude bar add J. Ries — 
 sonatina g f , authenticity 
 doubted . work beautifully print , handsomely 
 bind , correspond volume composition 
 Mendelssohn , Weber , & c. , issue firm . 
 elegant appearance edition , care 
 bestow valuable content , , 
 think , ensure extensive sale ; 
 information wish selection 
 volume , mention Sonata 
 publish separately 

 La Traviata . lyric drama , act . com- 
 pose Giuseppe Verdi . edit Berthold Tours , 
 translate English Natalia Macfarren 




 CASSELL , PETTER GALPIN 


 ORIGINAL correspondence 


 JACKSON service F. 


 editor MUSICAL TIMES . 


 AMATEUR concert . 


 EDITOR MUSICAL TIMES 


 365 


 SEYMOUR SMITH . 


 EDITOR MUSICAL TIMES 


 366 


 correspondent 


 brief summary country news 


 _ _ _ 


 367r . Gurney play good taste violin obbligato 

 Liverroo .. — tenth Subscription Concert Philhar- 
 monic Society , 25th November , 
 especial interest . principal artist : Madile . Titiens Signor 
 Catalina ; pianoforte solo , Dr. Hans von Bilow . vocal 
 programme excellently sustain , chief attraction 
 , naturally , appearance celebrated musician Dr. 
 Hans von Bilow , varied characteristic splendid per- 
 formance delight astonish audience high degree . 
 Symphony Mendelssohn " Italian " ( major , . 4 ) . 
 Dr. Hans von Biilow play Henselt Concerto , f minor ( op . 16 ) , 
 short solo ' ' Deux Etudes de Concert , " ' ' Dans les Bois , " 
 " Ronde des Lutins " ( Liszt ;) Chopin Berceuse ( Op . 57 ) , et 
 Valse ( Op . 42 ) . overture Spohr Fessonda , 
 marche , Beethoven ruin Athens , Gounod 
 Reine de Saba.—T ne opening performance sixth annual series 
 onthe plan London Monday Popular Concerts , 
 Philharmonic Hall , Wednesday evening , 3rd ult . executant : 
 ist violin , Madame Norman - Neruda ; 2nd violin , Herr L. Ries ; 
 viola , Mr. Zerbini ; violoncello , Herr Daubert ( place Signor 
 Piatti , unfortunately prevent appear ) ; solo piano- 
 forte , Mr. Charles Hallé ; vocalist Miss Alice Fairman ; accompanist , 
 Mr. Zerbini . programme contain Mendelssohn Quartet 
 e flat ( op . 12 ) , string ; Beethoven Sonata d major ( op . ro , 
 , 9 ) , pianoforte — encore , movement repeat ; 
 Sch trio b flat ( op . 99 , . 1 ) , pianoforte , violin , 
 violoncello ; Haydn quartet G major ( op . 64 , . 4 ) 
 ce Play execute perfection , heartily ap- 
 te 

 j 16th ult . principal solo excellently sing Messrs. 
 Bee 

 delighted audience . Saturday evening 6th ult . , 
 member Societa Armonica open rehearsal 
 Institute . gathering season increase popu- 
 ty , numerously attend . Kalliwoda Symphony , 
 ( , 6 , F , op . 132 ) , march Ries , Mozart overture 
 Fuan , play . Liverpool Vocalists ’ Union sing 
 chorus , Miss Monkhouse Mr. Busfield highly 
 tive solo . Mr. Lawson lead band , Mr. Armstrong 
 conducted.——T eleventh Subscription Concert Philharmonic 
 Society place eth ult . principal Artists : Mademoiselle 
 aleria , Signor Gustave Gartia , Signor Massini ; solo piano- 
 , Madame Carreno - Sauret ; solo violin , Mons . Sauret . over- 
 ture Merry Wives Windsor ( Nicolai ) , 
 Fidelio , play great spirit . pianoforte solo , 
 accompaniment , Beethoven e flat , perform 

 Madame Carreno - Sauret , great delicacy . instrumental 
 piece orchestra , Thalberg De Beriot duet 
 e violin Les Huguenots ; pianoforte solo , ' ' Andante 

 F ( Beethoven ) , Rubinstein arrangement " Marche des 
 d’Athenes ; " violin solo , " Le streghe " ( Paganini ) , play 

 uret exquisite expression wonderful mastery 

 present season plan London Monday Popular Concerts , 
 Philharmonic Hall Wednesday , 17th ult . execu- 
 tant : 1st violin , Herr Straus ; 2nd violin , Herr L. Ries ; violin , Mr. 
 Zerbini ; violoncello , Signot Piatti ; solo pianoforte , Dr. Hans von 
 Bilow ; vocalist , Mdlle . Nita Gaétano ; accompanist , Mr. Zerbini . 
 open quartet Schumann major , Op . 41 , . 3 , string , 
 follow song Hummel , " L’ombrosa notte vien , " 
 great expression . solo pianoforte sonata , ' maid Orleans " 
 ( Sir W. S. Bennett ) , exquisitely interpret Dr. von Bilow . 
 second commence Rubinstein fine sonata duet pianoforte 
 violoncello , artist . 
 gong Schumann ey Iy ny sing , concert con 

 lude Beethoven ' grand trio D major , op.7o0 , . 1 , piano- 
 forte , violin , violoncello . music enthusias- 
 tically receive large appreciative audience 

 Luton.—The Luton Choral Society successful performance 
 Haydn Creation , Corn Exchange , 26th November . 
 principal artist Miss S. Cole , Mr. Bennett , Mr. O. 
 Christian . Miss Cole " verdure clothe , " Mr. Christian 
 recit . air ' " ' God create great whale , " highly effective . 
 " thee live soul await " render , gem 
 evening duet chorus " star fair . " Mr. C. 
 Inwards conduct , band ably lead Mr. D. Southam 

 MaLvern Linx.—The Malvern Link Choral Union , con- 
 ductorship Mr. Philip Klitz , successful concert 
 ult . , Link Lecture Hall . work select Robin 
 Hood , principal allot Miss Clarke , Messrs. A. 
 Burston , W. H. Edwards , T. Cook Brown . Miss Clarke 
 effective air , " sweet , pretty bird , " receive encore 
 duet , " Thro ' weal woe , " Mr. Burston . unaccom- 
 panie madrigal , " ' sweet echo , sweet nymph , " exceedingly 
 sing , proof good training . Mr. Klitz preside 
 pianoforte harmonium 

 MANCHESTER.—Miss Sophie Flora Heilbron concert 
 Town Hall , Tuesday evening , 25th Nov. , large 
 fashionable audience . highly effective Chopin grand 
 Polonaise Brillante e flat major , Beethoven Sonata , . 1 , 
 Op . 12 , d major , receive warm mark approval 
 audience . assist Miss Thorley ( vocalist ) , Miss Lockwood 
 ( harp ) , Mr. de Jong ( flute ) , Signor Risegari ( violin ) , Mr. Weston 
 ( violoncello)——-mr . YARWooD annual concert 1st 
 ult . , Hulme Town Hall , large audience . principal 
 vocalist Miss Grimshaw , Miss Amy Russell , Miss Harlow , Mr. 
 Allen , Mr. Williamson . Herr Otto Bernhardt , Mr. Charles 
 Hallé concert , play masterly manner , violin sole , " Old 
 England " ( Vieuxtemps ) . Mr. George Julian Yarwood , Birming- 
 ham , classical rendering Beethoven ' ' Sonata Pathétique , " 
 play excellent style ' " Spinnlied " ( Litolff . ) theother 
 portion programme consist glee , duet , masonic 
 glee , ' ' welcome , welcome , " compose J. Yarwood , conduct 
 concert usual ability . — — 6th ult . , Madlle . 
 Carlotta Patti , Signor Camero , eminent pianist M. Theo- 
 dore Ritter , second appearance Mr. de Jong concert 
 addition Miss Héléne Arnim ( contralto ) , Mr. F. H 
 Celli ( bass ) . Madlle . C. Patti sing great spirit brilliancy 
 " shadow Song , " Dinorah , waltz Ritter " La Festa , " 
 encore ' ' come thro ' rye , " spanish composition ( 
 coarse style ) . Signor Camero success- 
 ful rendering " Spirto gentil , " Favorita , ' " M’appari , " 
 Marta . M. Ritter play Mendelssohn G minor Concerto great 
 finish expression . consequence unfortunate breaking 
 hammer pianoforte , M. Ritter remain piece 
 omit , disappointment audience . overture 
 Semiramide ( Rossini ) Haydée ( Auber ) band . 
 — — 12th ult . , Mrs. Stirling reading Shakspere 
 Midsummer Night Dream , Mendelssohn 
 music perform , direction Mr. de Jong . Miss 
 Mary Thorley solo vocalist , favourably re- 
 ceive . Mrs. Stirling reading remarkably fine , highly 
 appreciate audience . music band 
 ( exception opening phrase overture , 
 flute tune , remain repetition 
 passage ) . chorus weak timid 
 work.—HAanpev fresh , everlasting Messiah 
 perform Manchester time fortnight . 
 2oth Christmas Day , Mr. de Jong aid 
 following principal vocalist : Madame Vaneri , Miss Alice 
 Fairman , Messrs. J. H. Pearsor , Federici . Miss Fairman 
 singing " shall feed flock " narrowly escape 
 encore . Mr. J. H. Pearson produce considerable effect " Thy 
 rebuke , " " behold , . " Mr. Federici voice tell 
 ' " * nation . " chorus , , efficiently 
 sing , band desire — — 18th 
 19th ult . , Mr. Charles Hallé performance work 
 following excellent cast principal : — Mesdames Alvsleben 
 Patey , Messrs. E. Lloyd Santley . Mr. Sims Reeves 
 announce performance , un- 
 fortunately prevent illness singe . Mr. H. Walker pre- 
 organ 

 Oxrorp.—Bach Christmas Oratorio perform Christ 
 Church Cathedral , Tuesday evening , 2nd ult . , fol- 
 low Thursday noon . vocalist compose choir 
 Christ Church , Magdalen , New College , 
 amateur , member University , altogether 
 seventy voice . soloist Mr. Donaldson , soprano ; Mr. 
 Hobley , contralto ; Mr. Robson , tenor ; R. W. Macan , Esgq . , Mr. 
 Farley Sinkins , bass , member Christ Church choir , Dr. Corfe , 
 choragus University , act conductor , great credit 
 gentleman admirable manner train 
 choir . Mr. Parrott , organist Magdalen College , preside 




 368 


 1873runcorn.—a concert , conduct Mr. J. Newcombe , 
 Public Hall Tuesday , 2nd ult . glee , cong , & c. , 
 exceedingly render , meet hearty applause 
 appreciative audience , Pinsuti " spring Song , " Bishop ' ' Daugh- 
 ter error " repeat . Mrs. Newcombe highly 
 effective Randegger " Ben e Ridicolo , " Bishop " Lo 
 gentle lark , " encore . Miss James en- 
 core excellent singing Levey ' ' Esmeralda , " Mr. 
 Appleton Mr. Garratt lend efficient aid 

 SALiIsBuRyY.—The Sarum Choral Society second concert 
 season Assembly Rooms Thursday evening , 4th ult . , 
 conductorship Mr. W. P. Aylward . programme 
 select , include overture Guillaume Tell ( Rossini ) , 
 Bridal Chorus , Lohengrin ( Wagner ) , Ave Maria , Vintage Song , Finale , 
 Loreley ( Mendelssohn ) , Beethoven Symphony , . ; b flat 
 Op . 60 , Gade Cantata , Spring message . solo vocalist 
 Miss Julia Wigan , sing song effectively . 
 choral music remarkably sing , unaccompanied - song , 
 " good night , beloved " ( Pinsuti ) , doubt 
 finished performance evening . instrumental music 
 high order , remarkable feature provincial con- 
 cert , reflect great credit band conductor 

 SHEFFIELD.—The organ recently erect St. Mary Church , 
 Walkley , open Thursday , 27th Nov. , 
 service morning , Recital evening George 
 Cooper , Esq . , London , organist choir - master Majesty 
 Chapel Royal , St. James ’ . organ erect cost 
 £ 400 , local builder , way adapt church . 
 manual - stop ; chiefly remarkable 
 account fine swell organ , Mr. Heald 
 appear expend considerable care . programme con 

 369 

 Times . " illustration , consist selection 
 iem , work , large choir voice , 
 te Mr. Frank Spinney , F.C.O. tenor solo sing 
 Mr. Coleridge ; Beethoven air F , variation . 
 Mnoforte , play Mr. Spinney . 
 Woop GREEN.—A concert » Masonic Hall , 
 Tuesday evening , oth ult . , direction Mrs. Weaver , 
 assist Miss Alexandrina Dwight , Mr. Stedman 
 Mr. T. A. Waliworth ; Mr. M. L. Lawson efficiently preside 
 atthe pianoforte . , Clay ' ' wander 
 mountain , " sing Miss Dwight feeling ; Mrs. 
 Weaver sing " pur dicesti " sweetly . Mr. Stedman 
 successful ' ' good night , beloved , " Mr. Wallworth voice 
 hear advantage " O , ruddy cherry . " " ABC " 
 duet Mrs. Weaver Mr. Wallworth , audience , 
 duet , ' " Hassan Zuleika , " Miss Dwight Mr. Sted- 
 man . encore gain second , notably Miss 
 t " o haste , ye bird , " Mr. Stedman ' ' Tom Bowling , " 
 exceedingly render . duet Mrs. 
 Weaver - gentleman , " sailor sigh , " 
 ly ; Mr. Lawson fair share honour 
 ofthe evening brilliant rendering Chopin music . Alto- 
 gether , evening decided success . 
 " Wootwicu.—The fourth Miss S. F. Mascall winter 
 concert place 2nd ult , Town Hall . concert 
 commence Miss Mascall sacred cantata , thy work praise 
 Thee , perform ina highly creditable manner ; Mrs. Sal- 
 lenger , Miss Wheeler , Miss Foss solo . rest 
 miscellaneous . noticeable feature con- 
 certwa admirable manner number juvenile pupil 
 Miss Mascall acquit . Miss Mascall preside 
 pianoforte violin piano , Mr. Davis conduct 

 WortHinc.—On 22nd October 1872 , invitation Mr. 
 L. $ . Palmer , influential body lady gentleman meet 
 form choral class , " Worthing Sacred Harmonic 
 Society . " time concert , Haydn 
 Creation perform , Handel Messiah 
 second , highly successful , artistically 
 financially , sanguine expectation . annual 
 meeting Society , thank vote conductor , Mr. L. S. 
 Palmer , organist , Mr. Herbert S. Cooke , Honorary Secre- 
 taries , indefatigable exertion mainly contribute 

 York.—The inauguration winter classical concert 
 look forward considerable interest music lover 
 city , undertaking great spirit enterprise 
 partof Mr. John Wilson , entrepreneur scheme . 
 drawback concert , owe dense fog , Mr. 
 ‘ Charles Hallé , conductor , arrive overture Der 

 Freischiitz , Rosamunde music play . Rossini 
 overture Guillaume Tell Beethoven symphony F , , 
 excellently , Mr. Hallé experienced direction ; 

 er band , Herr Straus , play Spohr Adagio F 
 brilliant success . pianoforte performance Mr. Hallé 
 course important feature concert ; Mdlle . Bund- 
 sen , vocalist , elicit warm applause 
 song , receive encore rendering " non piu mesta . " 
 atthe second concert , 2nd ult . , instrumental portion 
 programme consist exclusively Chamber music . Mozart quar- 
 tet g minor , Schumann e flat play perfection 
 Mr. Hallé , Madame Norman - Neruda , Herr Bernhardt , ane M. Vieux- 
 emps ; Dussek Andante Rondo b flat pianoforte 
 Violin , Mendelssohn ' ' Tema con Variazioni " D , pianoforte 
 violoncello ( Mr. Hallé join Madame 
 " Neruda , second M. Vieuxtemps ) , receive 




 dure month . 


 LORD , strong BATTLE . 


 _ _ _ _ 


 37 ° 


 ORK.—ARTICLED PUPIL . J. C. MARKS , 


 MATEUR MUSICAL SOCIETY , BRIXTONOR SALE , valuable score ( Folio ) , 
 bind elegantly , - class condition 

 Beethoven symphony , Concertos , Fidelio , & c. ; Handel Messiah 
 Israel Egypt ; Haydn Creation ; Weber Euryanthe ; Meyer- 
 beer huguenot ; Rossini William Tell ; Schumann Paradise 
 Peri ; Spohr Judgment . apply Messrs. W. H. G. 
 H. Dreaper , 96 , Bold - street , Liverpool 

 Te organist require deputy.—sunday 
 service , 3 . ; Week evening , 1 . 6d . letter only.—Joseph 
 Lewis , 139 , Camberwell New - road 




 HURCH CHORAL SOCIETY LONDON . — 


 G A. MACFARREN - anthem 


 anthem special general . 


 YOUNG composer publish . 


 37 


 -1 ANGLICAN hymn - BOOK 


 1em ~ new EDITION , revise enlarge . 


 second serie 


 OULE DIRECTORIUM CHORI ANGLI- 


 QULE DIRECTORIUM CHORI ANGLI- 


 HOLY COMMUNION 


 EDWARD 


 addition 


 REV . T. HELMORE plain song work . 


 SACRED music EDWARD LAWRANCE , | 


 CANTICLES , LICHFIELD pointing 


 372 


 CRUSADERS 


 NEW SACRED CANTATA . 


 LIBRETTO MARIAN MILLAR 


 WILLIAM J. YOUNG 


 - song 


 voice , 


 JACQUES BLUMENTHAL 


 SY AVEYW DH 


 WWWWWHWWWAHDHDNHND 


 W. HUTCHINS CALLCOTT new . song . 


 10 . 


 123 


 13 . 


 14 . 


 15 . 


 16 . 


 17 . 


 18 . 


 20 . 


 21 . 


 22 . 


 23 . 


 24 . 


 25 . 


 26 . 


 276 


 28 


 29 . 


 30 . 


 31 . 


 32 . 


 33 ° 


 34- 


 35 : 


 36 . 


 37 : 


 38 


 EETHOVEN 


 SONATA $ 3 


 ( new complete edition , ~ 


 edit finger 


 AGNES ZIMMERMANN 


 PRICE guinea 


 SINGLY 


 LONDON : 


 NOVELLO , EWER CO 


 BERNERS STREET ( W. ) , 35 , POULTRY " 


 NEW YORK : J. L. PETERS , 599 , BROADWAY