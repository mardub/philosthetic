


 CLARK . — 


 YORK 


 , W.1 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 found 1844 


 publish 


 ROYAL CHORAL SOCIETY 


 ROYAL ALBERT HALL 


 MAJESTY KING 


 SATURDAY , NOVEMBER 235 


 MISS FLORA MANN 


 MISS LILLIAN BERGER 


 MR . STEUART WILSON 


 MR . CLIVE CAREY 


 ROYAI 


 ALBERT HALL ORCHESTRA 


 MR . ALBERT COATES 


 ROYAL ACADEMY MUSIC . 


 YORK GATE , MARYLEBONE ROAD , LONDON , N.W.1 . 


 ROYAL COLLEGE MUSIC . 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W.7 


 ROYAL COLLEGE MUSIC PATRON FUND 


 NOVEMBER 1 


 month 


 1922 


 GUILDHALL SCHOOL MUSIC . 


 VICTORIA EMBANKMENT , E.C.4 


 PRINCIPAL SIR LANDON RONALD , F.R.A.M. , F.G.S.M 


 H. 


 ASSOCIATED BOARD 


 R.A.M. R.C.M. 


 local examination MUSIC 


 ST . ALBAN MARTYR , HOLBORN . 


 MUSICAL 


 ROYAL 


 MANCHESTER COLLEGE MUSIC 


 18 


 BIRMINGHAM & MIDLAND INSTITUTE 


 SCHOOL MUSIC 


 session 1921 - 1922 


 96 , WIMPOLE STREET , W.1 


 TOBIAS MATTHAY 


 PIANOFORTE SCHOOL 


 complete training course teacher 


 MANCHESTER SCHOOL MUSIC 


 LONDON COLLEGE CHORISTERS . , 


 BIRMINGHAM REPERTORY THEATRE 


 SCHOOL OPERA , 


 STATION STREET , BIRMINGHAM 


 WARTINMAS term open MONDAY , SEPTEMBER 18 


 IDEAL CORRESPONDENCE 


 SCHOOL MUSIC 


 expert tutor 


 THEORY MUSIC , HARMONY 


 composition , art teaching , 


 interpretation 


 form memory training 


 MUSICIANS 


 6 , BOLTON ROAD , FARNWORTH , BOLTON 


 DEGREES DIPLOMAS MUSIC . 


 CORRESPONDENCE COURSES 


 complete 


 GODDARD STUDIOS , 59 , SPRING GARDENS , BUXTON 


 XUM 


 » R.A.M 


 DENCE 


 ONY , 


 HING 


 YING 


 JLTON 


 SIC 


 URSES 


 2S 


 BUXTON 


 SQUARE 


 KINNON 


 MUSICAL 


 765 


 1922 


 SINGING - class CIRCULAR 


 NOVEMBER 1922 


 ' modern ' music ? method , far novel , 
 | preferable 

 run result 
 |innovatory tendency . , tune old 
 | satisfy intone 
 willingly . notice interest excerpt 
 old judgment Beethoven Miss 
 Muriel Silburn reproduce month / usica/ 
 Times . student musical history familiar 
 countless instance kind , 
 refer Bach Wagner , 
 Monteverdi , Brahms , Gounod . 
 , compile list composer 
 treat anarchist 
 kind , short 
 compile good composer 

 standard judgment kind 
 originate found faith certain fix 
 rule ( allege law ) , certain trend 
 habit thought . ' opposite attitude 
 people willingly use , reference 
 composer work happen admire 
 despite infringement rule law notice- 
 able , word Edgar Allan 
 Poe apply Shelley : " disdain rule 
 emanation Law , 
 soul law . ' needless 
 emphasise attitude equally 
 uncritical 

 comprehension music , works|each generation certain 
 art , depend apprehension } heritage past . ' lime time 
 relationship element — twofold relation-| happen , happen , genera- 
 ship , concrete positively deter-| tion evince keen interest certain old work , 
 minable hand , purely spiritual other.|and , forego 
 merely aspect the| evince 
 relationship significant point view| evince . similarly tendency 
 art . period differ ( limit , sharply 

 listener dream contend | ) country country , regard 
 relationship more|old music new . amplitude 
 term music Bach Beethoven sett tell , great ; 
 sense , perfectly aware that|their duration . withal know work 
 music imitator outwardly similar | survive test varia- 
 relationship mere shell not|tions kind endure despite far 
 sense worth , reach | temporary variation . 
 stage able perceive spiritual relation- reason confidently 
 ship , tangible relationship go|—and sake paradox — musical 
 making certain idiom style . for| work age . true freshness 

 fc 




 xum 


 1922 767 


 paint musicpainter . think 
 picture musical subject | 
 poetical 

 pictorial subject . refer portrait , 
 class picture musician 
 hold marked advantage poet painter . 
 lack certain — influence , 
 affluence , like — 
 unsuitable sitter eminent painter 
 epoch . refer imaginary 
 portrait musician , branch 
 great popularity begin noticeable . 
 know picture child Handel 
 astonish parent playing — 
 modern pianoforte technique ? 
 know picture 
 Beethoven play friend ? 
 ' Symphony Beethoven , ' sub - title 
 ' picture man , try 
 awake performance Symphony 
 Beethoven . ' record 
 Symphony play , certain 
 eighth , seventh , fifth ; 

 mean paint music . 
 lare picture 




 xuma picture Liszt extemporize 
 theme ( presumably , - line notation , 
 gregorian tone ) miniature pianette , 
 left foot jam sustain pedal 

 poetical pictorial analogy 
 imaginative portrait ? true 
 picture , photograph , Charles Dickens 
 read daughter , Milton 
 dictate paradise regain daughter ( 
 happy artist dutiful daughter ! ) , 
 picture Browning rough 
 draft — poetical equivalent extemporize — 
 ' Red - Cotton - nightcap country , ' Swinburne 
 recite newly - finish poem Watts - Dunton ? 
 reason non - depiction 
 Watts - Dunton time listen , ' 
 work hand . ' 
 picture Raphael paint ' Madonna , ' 
 Fra Angelico paint ' annunciation ' ? 
 truth painter hesitate enshrine 
 | picture picture lest work suffer 
 comparison lack unity style ; 
 composer hesitate use melody 
 Beethoven new work lest invention 
 sound poor 

 /abu / arum genus 
 refer 
 subject , 
 musician music , music prisone 
 |within bar pen man , music 
 native freedom . judge 
 | character expression produce 
 | performer ’ face . Sir Joshua Reynolds 
 picture , " Cherub Choir , ' need 




 CREDO 


 MOZARTFRANK LIEBICH 

 crowd note , free rule bind | 
 stave — sort fancy - dress ball | musician Richard Wagner short 
 notable employée great composers.| story , 4 " end Paris . death - bed 
 Minims dance bacchanalian frenzy , their|last word : " believe God , Mozart , 
 large , vacant face support frail,| Beethoven . ' tale write eighty year 
 semiquavering body ; demisemiquaver , hitherto|ago pot boil Wagner 
 chain like roman prisoner band , | domicile french capital . 
 caper ecstatically momentary freedom ; | interval elapse print 
 sharp , flat , natural , hitherto | faith Mozart fluctuate . 
 wait patiently relentless| 19th century Mozart Society 
 master , demand right consider as|assemblie concert _ trysting - place 
 free independent citizen musical/| lover Salzburg composer . 
 world . rest , wanted|a man work public favour society 
 rest Hampstead Heath ? | propagation preservation faith 

 , , music - day possess visible require . significant Mozart 
 charm ? tendency audience drift | society need closing year 




 XUM 


 ESBe ' ap , ; thought , dancer ; 

 lhe unceasing exploitation musical sound tone , | musician . . . . | place value 
 unending possibility harmonic ! op man praise censure . . . . | continue 
 combination , adjustment ! follow express feeling . ' 
 instrument musical idea mee : ee 
 consequent new appraisement function straightforward simplicity sum 
 amalgamated timbre quality charm pore ho = . 
 strain hearing carefully | music : Beethoven outstrip " ce 
 follow manifestation develop- | 4 ™ 4ster form , unrivalled genera- 
 ment progress modern music the| tion structural extension free varied 
 thirty year . ' Che ear want rest|p ° lyphony . plastic mould , 
 relaxation . Mozart | hammer theme sketch- 
 turn respite refresh and| book rap 
 - sensitise . | physical agony endure process 

 work demode composition . superb volcanic passion ; 
 outworn form method day , a| interpreter age Napoleon ; 
 harmonic system transcend touch Romantics , 
 recognition , elimination | force work late world - wai 
 superfluous detail , avoidance intro-| 204 heroism display 
 spection appeal emotion , his| ! . impersonal modern musician 

 ill - suited confront rough usage ! ' ' ® , | aim straightforward 
 subject Archbishop of| ¢xpression simple form . use 
 Salzburg courtier ; bear poverty | work dramatic npate - aeaie . 
 : - ' | essential ¢0 feel convey 

 hardship later year . | feeling . ' : 
 write father compose | 
 cheerful , happy| easy deride idea simplicity 
 mood music reflect sorrow joy.| connect Stravinsky 
 wistful regretful poetic } term ultra - modern . unfamiliar 
 Adagios Andantes Sonatas , Symphonies , | sonority disconcert unskilled hearer : 
 Quartets , touch morbidity ! familiar resource modern 
 passionate resentment despairing cry as| harmony 18th century , 
 sentimental person discover appreciate in| recognise modern composer 
 Beethoven . direct simple profusion 

 perfect symphony ing minor reveals| mean disposal Mozart 
 nature . tremblingly alive delicate | circumscribed confine straighten harmonic 
 transient manifestation beauty } resource . 
 world . ' free un-| constant use label musical 
 practise jealousy malevolence was/ history crippling 
 sensitive finger - tip good bad| recognise unbroken evolution art . 
 nature come contact , | term classic , romantic , modern , induce partisan- 
 sane brave midst trouble } ship camp . 
 delightful sense fun humour . ) succeed age , proceed 
 toguish countenance Salzburg Hanswurst| surely musical sound , 
 peep Rovdos Fina / e :| material , derive 




 770unorthodox , | z7¢t / e Night Music Mozart 

 contemporary , ask sum creed 
 trenchant term Wagner imaginary hero , 
 begin eliminate Beethoven 

 verbal brickbat statement provoke , 
 explanatory reservation , possible 
 reveal affiliation 
 modern heretical notion sum 
 concise form : believe Mozart , 
 Schubert — , , Mendelssohn 




 1922 771 


 72 thethe gem protest Principal 
 Domestic Workers ’ Bureau , Ltd. like 
 Kensington Mayoress , ( , 
 protest ' woman’—not ' cat’—in 
 line ) , , , hastily assume 
 object class enable girl sing 
 work . ' mistaken impression 
 opening little light badinage , , 
 preliminary explosive * preposterous ! ' 
 wither , alliteration artful 
 aid 

 tax order Carmen 
 cook Pagliacci pageboy , Beethoven 
 butler 7wo Zyes Grey 
 tweeny maid 

 roar laughter ! ache 
 carry good work ' Charpentier 
 chauffeur , Handel housemaid , 
 Gluck governess , Franck footman , 
 Grieg gardener , Thomas Haynes 
 Bayley boot - boy 




 XUM 


 ( 6.30 ) , 


 


 774 musical 


 1922 


 composer clever 


 xum 


 


 musical 


 1922 


 great PIANO clearance sale 


 day . 


 le 


 4/4 


 1922la Musique et les Nations . G. Jean - Aubry . 
 [ London : J. & W. Chester 

 live age passport , frontier 
 bristling , tendency 
 require z / sa journey 
 Paddington Penzance . Piague Antwerp , 
 Carnarvon Cork , citizen proud 
 boast speak language understand 
 number fellow - man . music time 
 lag merry craze mutual incom- 
 prehension . quickly change . 
 year ’ time Bach Beethoven 
 doubt respected position Cicero prose , 
 contemporary musical production thenadays 
 like welsh newspaper catalan poetry- 
 local consumption . music clearly 

 pass sway nationalism 




 778 


 1922 


 thea pile book pamphlet special interest 
 teacher deal briefly , 
 case author ’ _ sufficient 
 guarantee discern educator 

 growth Music , H. C. Colles ( Clarendon 
 Press , 10 . 6d . ) , reprint work 
 establish standard guide . 
 originally appear volume , deal 
 respectively ' Troubadours J. S. 
 Bach ' ; ' Ageof Sonata , C. Ph . E. Bach 
 | Beethoven ' ; ' Ideals 19th century . ' 
 publisher issue volume 
 | — stout , handy book page , 
 delightfully clear attractive style . fact 
 |its describe ' Study musical History 
 | Schools ' justice ; emphati- 
 cally study lot leave school- 
 | day far . book ' musical appreciation 
 | pour forth late . good 
 |of , happily bear shibboleth 
 |on title - page . helpful feature 
 | frequent use cross - reference text . , 
 copious index , synopsis 
 chapter , book convenient hunting- 
 | ground datum . numerous musical 
 | example , considerable length . 
 | Percy A. Scholes Second Book great 
 Musicians ( Oxford University Press , 45 . 6d . , gilt , 55 . ) 
 | course find public wait . Mr. 
 | Scholes adept simple exposition , 
 |is chat kiddy usual , persuade 

 ook 
 ance , 
 ( Country 
 forte solo 




 h. G 


 779 


 780 musical 


 1S 


 GABRIEL FAURE 


 1 


 british music viennese critic 


 


 british music FRENCH composer 


 point form kodaly quartet 


 XUM 


 critk 


 1 M poser 


 TET 


 early christian music 


 rumanian musicmusic letter 

 centenary César Franck ( bear December 10 , 
 1822 , Liége ) forth lead article the| 
 October Music Letters ( 22 , Essex Street , 55 . ) , 
 W. Wright Roberts , excellently judicious 
 readable , stray sheer idolatry ( 
 franckian idolatry pall irreverential 
 generation ) , pick great thing 
 sounding praise . Egon Kornstein , hungarian 
 Quartet , write thoughtful way ' 
 practise String Quartet , ' win 
 quartet Barték , , post - beethoven 
 quartet , great resemblance 
 Beethoven period 

 Eugéne Goossens discuss ' String Quartet 
 Brahms ' ; recognise 
 vivacious , range Mr. Goossens 
 formal observation . Prof. Henry J. Watt 
 article ' rule law music ' 
 miss interested philosophy art . 
 Lady Dean Paul , , page ( ' musing ' ) 
 wsthetic . ' life interlude creation 
 disintegration , art expression man 
 yearning constancy . ' line 
 result depreciation Mahler ( ' conceive 
 intellectually vast effigy 
 philosophy spiritual memorial ' ) appre- 
 ciation Berlioz . ' Jonsonian Masque ' 
 describe Jeffrey Mark 




 PIANOFORTE MUSIC 


 _ — 9 


 2 $ 8888 812 


 ES 


 PIANOFORTE DUETS 


 SONGS 


 H. G 


 


 1922 783 


 STRING MUSIC 


 ORCHESTRAL score 


 784 


 SIR CHARLES SANTLEY 


 1834—1922 


 ANTILEM 


 CHRISTMAS 


 ‘ 255 = : fs=2 > 2 ? 


 ALTO 


 TENOR 


 — - _ 


 _ L 


 SING , 0 HEAVENS 


 NEN 


 NS : 


 SING , o ITEAVENS 


 ES ? = = = — = = < = | | 


 e > | f | | ( ee SS SS ZS 


 t+ 4 


 g -4-*- e 


 6 - 2 = = 


 4—5 


 g * 22 


 SING , 0 heaven 


 — — $ _ — _ _ — _ — _ — — 


 SS 


 2 SS SS SS 


 4 _ 


 SING , o heaven 


 7 | 90 


 > - = 17 


 = 


 - _ - — — — _ 


 ef 


 SS 


 4/7 = 


 4 . 7 = 


 * n ~~ [ 7 . | 


 > 2 _ 


 188 


 HERBERT THOMPSON 


 PROMENADE CONCERTS 


 xumthe lot ) ; beautiful miniature string 
 F. Lawrence , 77is#is ; Frank Bridge impression 
 Summer ( clever essay objective- 
 contemplative mood ) ; Gerrard Williams 
 fragrant Pot - Pourri — worthy welcome item . 
 W. R. 

 QUEEN HALL SYMPHONY CONCERT 
 [ symphony Schumann d minor 
 Concerto Beethoven e flat , 

 L , S.Q 




 1922 


 WILHELM BACKHAUS recital 


 LONDON contemporary music 


 singer month 


 XUM 


 OPERA ' OLD VIC 


 F. E. B. 


 IMMORTAL HOUR 


 REGENT THEATRE 


 796 


 LEEDS MUSICAL FESTIVAL 


 XUM 


 VAL 


 1922 797Verdi Reguiem receive exceedingly fine 
 interpretation , italian vein , , 
 efface recollection Nikisch memorable 
 performance 1913 , suffer little comparison . 
 Grail Scene /arsifa/ suffer 
 chorus big , near hand , bring 
 mystical effect . copious selection 
 Die Meistersinger happy , afford 
 bright spot brilliant Festival . 

 798 MUSICAL TIMES — Novemser 1 1922 
 doubted music al british composer . Mr. McEwen , like Sir Dan , 
 finer interpretation , , with| , man 
 great confidence , Brahms Schicksa / s / ied,| strive represent british music worthily , , 
 inspiration fully realise | Mr. McEwen write british idiom , 
 conductor , choir , orchestra . instrumental | quality music 
 Introduction Epilogue reach great height | fashion hard fighting , every- 
 refined beauty . supremely fine instrumentalist | body right people credit , 
 appear soloist : M. Cortét Beethoven fifth| brilliant scribe begin discover 
 Pianoforte Concerto Franck Symphonic / . certainly class representative 
 variation , Mr. Albert Sammons Elgar Violin ! british composer . shall turn work 
 Concerto , acknowledge exponent . | time come , revel national flavour . 
 orchestral work Scriabin two| ' programme ' work , fact regret , | 
 symphonic work , ? oeme de ? -xtase Prometheus , | think composer command sufficient 
 furnish hard nut audience crack , | right material enable supply absolute 
 leave impression marvellously fine perform- | music free nature guide 
 ance . question were| meaning . Symphony entitle / way , 
 judicious programme , } composer deal actuality . provide literary , 
 fact Mr. Coates close touch Scriabin | , , poetic basis , entitle move- 
 music afford sufficient reason . ments ' spring tide , ' ' moonlight , ' ' Sou’-West 
 choice principal adversely | Wind . ' areall mood picture , isa strong 
 criticise vocalist world- | feeling poetic aspect thing . second 
 wide fame include . matter policy , } composer wholly true land birth . 
 artist } hint ' hill 
 decorate poster , extent fee | Highlands love , ' cleverly 
 subscriber feel money’s-| scottish characteristic fore . , 
 worth , , ] second movement , insistent melodic figure , 
 performance suffer efficiency . Mr. John| freely suggest revelry night 
 Coates , Mr. Robert Radford , Mr. Norman Allin|on mountain , strain , waft 
 , course , head profession , and| , graphically depict . 
 Miss Dorothy Silk recent year rise a| listen ' programme ' music freedom 
 similar height ; Miss Margaret Balfour , who| mind music leave find 
 appearance Leeds Festival , | story , impress 
 stride late art , | hearer thoughtful , individual piece 
 leave pleasant impression beautiful | work lightly dismiss . pleasant 
 voice artistic singing . soprano ! example ' atmospheric ' work , , happily , 
 charming voice musicianship a|an atmosphere like linger 
 worthy colleague artist mention | desire , ' atmospheric ' piece , 
 Bach Magnificat , finished Eva the| outside soon possible . Sir Dan Godfrey 
 Meistersinger Quintet , Miss Elsie Suddaby , a| careful presentation hand excellent 
 Leeds singer fame rapidly extend | orchestra , audience pay composer 
 border , engagement | compliment platform . 
 honour county.| rest programme testify 
 Miss Eleanor Paget , newcomer , powerfully | pliability orchestra , 
 sustain trying soprano chora/| \eistersinger Overture Ravel quaint 
 Symphony , Miss Edith Clegg , Mr. John Adams , | fairy Suite 1/¢7e 7Oye Saint - Saéns Pianoforte 
 Mr. Raymond Hartley , Mr. Percy Heming ( } Concertoin F , Mr. Arthur Shattuck soloist . 
 excellent Amfortas ) efficient } ; Bournemouth learn Easter 
 respective task . Mr. Percy Richardson useful ] Festival establish fact , save year 
 work organ pianoforte ( comfinuo | . 
 Bach Cantatas ) deserve mention . Mr. Albert } , I. F. E. B. 
 Coates good , great vitality his| 
 conducting , altogether Festival z oe . 
 rouse general enthusiasm adi 
 recollect occasion 1886 , ecstatic | Church Organ Music 
 chorus girl pelt Sullivan rose 

 conduct golden Legend . ORGAN 
 October issue quarterly journal 
 fall variety interest . organ Bristol 
 J. B. MCEWEN symphony Cathedral discuss Rev. Andrew Freeman , 
 BOURNEMOUTH St. Paul Cathedral Mr. Somers Clarke . 
 " . . ; Mr. Stuart Archer write Cavaillé - Col organ , 
 Sir Dan Godfrey begin - eighth season ! yy , W. F. Muckle Table Organ William Mace , 
 Symphony .concert Bournemouth onjand Mr. James Matthews organ build 
 October 12 , beating bush . | Pretorius model Freiburg University . Dr. Eaglefield 
 chief number programme new/| Hull continue frank discussion organ tutor , old 
 Symphony , british , work J. B.|new . article Mr. Meyrick Roberts ( 
 McEwen . fact frank acknowledgment reg weer tt : path herBemoagh gm 
 cy ~e ~ » bea u ustre ' 
 policy Sir Dan pursue long , Rouen , Chartres ) , Dr. Audsley , Mr. Abdy Williams , 
 good audience spite weather relate experience temporary maestro dt 
 tempt sand sunshine } -apsel / Capri . correspondence , review , specifica- 
 symphony seriousness , won| tion balance capital number , , 
 public character champion | usual , illustration strong feature 




 XUM 


 . E. B 


 NEW ORGAN WESTMINSTER CATHEDRAL : 


 RECITAL DR . W. G , ALCOCK 


 H. G 


 ST . MICHAEL , CORNHILL 


 LEEDS PARISH CHURCH 


 LONDON SOCIETY ORGANISTS : BATTERSEA 


 CLAPHAM BRANCH 


 ST . MICHAEL COLLEGE , TENBURY 


 late GEORGE HERRERT GREGORY 


 SOUTHWARK CATHEDRAL 


 800 


 1922 


 ORGAN RECITALS 


 appointment 


 XUM 


 S. W. 13 


 want : composition WIND 


 instrument 


 802 


 19 


 early GREEK hymn 


 swishing . 


 3 , 1922 


 AUSTIN 


 ROYAL normal COLLEGE BLIND 


 GLOUCESTER FESTIVAL 


 PURCELL SMALL orchestra 


 MILTON ode nativity 


 warning 


 XUN 


 RAS 


 JACOB 


 ITY 


 word organist 


 ' RAFF CENTENARY 


 ' genial ' SCRIABIN 


 ROYAL ACADEMY MUSIC 


 APPOINTMENTS 


 award 


 R.A.M. CENTENARY THEATRE 


 804 


 1922 


 TRINITY COLLEGE MUSICMusic province 

 BIRMINGHAM DtstrRict.—At series 
 symphony concert City Birmingham Orchestra , 
 Wednesday , October 4 , Town Hall , Mr. 
 Appleby Matthews conduct stimulating , unconven- 
 tional , performance César Franck Symphony . 
 programme include Bantock Sappho song , 
 Orchestral Prelude cycle . 
 composer conduct , vocalist Miss Dorothy 
 d'Orsay , sing admirable control artistic 
 insight . beautiful contralto voice , find 
 complement variety colouring intelligence 
 interpretation . — — Sunday evening concert 
 City Orchestra , regular feature winter 
 musical season , provide interesting music . 
 Mozart /upiter Beethoven fifth Symphonies 
 , ard , lesser - know work , Gustav Holst 
 Marching Song orchestra prove extremely enjoyable 

 Miss Marjorie Sotham introduce mid - day 
 concert Birmingham , venture promise 
 successful . vocal recital Dr. Tom Goodey include 
 song Wolf , Martin Shaw , charming folk - song . 
 Miss Sotham co - operate Messrs. Frank Venton 




 XUN 


 _ _ _ _ HUDDERSFIELD.—At concert September 30 , 
 Huddersfield Phiiharmonic Society present orchestral 
 music include Scherzo finale Dvorak 
 New World Symphony , Ruy Alas Don 
 Giovanni Overtures , light item Moszkowski 
 Délibes Mr. Wilfred Miller , orchestra , 
 play Dream piece Delafosse , clarinet solo 

 Leeps.—An account Leeds Musical Festival 
 appear thisissue . September 23 , Haydn 
 creation sing Oxford Place Chapel , 
 conductorship Mr. Robert Pickard.——-The aserie 
 special orchestral concert hold Belgrave 
 Central Hall October 7 , section Leeds 
 Symphony Orchestra , Mr. Bensley Ghent , play 
 Beethoven Egmont Symphony . 1 ; 
 old english Suite Cowen.——During week 
 begin October 9 , Adrian Beecham musical setting 
 Merchant Venice stage Leeds Theatre Royal . 
 — — Leeds Committee British National Opera 
 Company entertain October 13 luncheon , 
 Mr. Mrs. Percy T. Leigh . state 
 Leeds subscription B.N.O. 
 £ 1,000 £ 3,000 £ 4,000 
 Bradford . Mr. Percy Pitt , artistic director Company , 
 understand visit Leeds Grand 
 Theatre week , commence February 206 . 
 — — tthe winter season Leeds Saturday Orchestral 
 Concerts open October 14 , Borodin second 
 Symphony , b minor , perform time 
 Leeds , Mr. Eugéne Goossens ccnducte . ZS 3 
 Hoggett , lecturer Music University Leeds , 
 series discourse deal ' 
 beginnings Modern Music , ' October 16 , 
 special subject ' Beethoven later work 

 LIVERPOOL.—Mr . Frank Roscoe open course 




 806 


 1922 


 25 


 XUM 


 808 


 1922 


 music IRELANDAMSTERDAM 

 week intervene summer season — 
 come effective close Scheveningen 
 Beethoven ninth — commencement winter 
 season . Mengelberg appear subscription 
 concert , absence unwonted duration . 
 old piece , Mahler Symphony , 
 hearing Henri Rabaud Eclogue , work breathe 
 tender emotion . concert 
 conspicuous Bronislaw Hubermann appear soloist 
 time subscription concert , 
 audience count kind hall - mark . 
 play Concerto Brahms superbly . small wonder 
 announcement solo recital day later 
 cause big hall fill seat . 
 occasion Hubermann play Concertos ( Bach 
 minor Goetz F ) , Beethoven Aventse ? 
 sonata 

 French Musical Festival great event . 
 alleged objective survey present state 
 modern french music . realisation plan , 
 , hamper , inasmuch momentary condition 
 operatic life permit performance french 
 opera scheme . work César Franck 
 exclude time , entire concert reserve 
 celebrate Franck Centenary December 7 




 GERMANY 


 BAYREUTH 


 MUSICAL FOLK - LORE 


 XUM 


 [ ARMANS 


 809 


 HANS PFITZNER 


 romantic OPERAA strange fate befall E. Th . W. Hoffmann opera , 
 Undine , July 29 , 1817 , successful run 
 - night , theatre burn 
 costly decoration material necessary 
 performance . composer refuse stage 
 opera - house , large , 
 refusal pay dearly , , apart 
 unsuccessful performance Prague 1821 , opera 
 lost world . ninety year pass 
 performance unique work Hans Pfitzner 
 publish pianoforte score text ( C. F. Peters , Leipsic ) . 
 hundredth anniversary Hoffmann death , 
 german stage perform Undine , romantic 
 opera , great success . remain 
 success 

 addition event , Messrs. I. Engelhorn , 
 Stuttgart , publish book 
 page , contain Hoffmann literary work , z.e . , 
 musical novel portion musical criticism 
 value . 
 recognise real greatness Beethoven , musical 
 critic write enthusiastically , 
 time correctly , Hoffmann essay , 
 * Beethoven Instrumentalmusick 

 NEW work 




 NEW GERMAN opera 


 NEW YORKMischa Elman open concert season Carnegie 
 Hall absence America 
 year . play , , 1920 , 
 announce abroad rest , study , 
 composition . composition 
 materialise , confess rest study 
 work change Mr. Elman method 
 mannerism . return virtue 
 fault mark career . tone 
 large generally pure , technique excellent , 
 affectation sentimentalising continually 
 evidence , mar attempt work 

 real music play thoroughly artistic way 
 return New York , time 
 year ' Festivals ' 
 country . chief Mrs. Coolidge annual Festival 
 chamber music hold charming home 
 Berkshire Hills . opening programme 
 Wendling String Quartet , Stuttgart , Germany . 
 leader , Carl Wendling , concert - master Boston 
 Symphony Orchestra Gericke . concert 
 devote Beethoven , Schumann , Reger , second 
 Brahms . Sonata . 2 , e flat , Op . 120 , 
 pianoforte viola , superbly play Mr. Ernest 
 Hutcheson Mr. Firestone ( viola player 
 San Francisco Orchestra ) . novelty afternoon 

 concert trio C minor Gabriel Pierné , 




 


 810 


 M. H. FLINT . 


 ROMEAt Augusteum visit promise Stokovski , 
 known New York , visit Italy 
 time , probable Sibelius come Rome 
 year . season inaugurate end 
 November Verdi Mass 

 Accademia di Sta . Cecilia inform Capet 
 ( Juartet , Paris , visit Rome season , 
 interpret Quartets Beethoven 

 finally , December 10 centenary birth 
 César Franck , Amici della Musica organize special 
 Franck commemoration date 




 LEONARD PEYTON 


 TORONTO 


 VIENNA 


 XUM 


 » C. fthe number resident Vienna conductor season 
 considerably enlarge . apart advent 
 Paul von Klenau , allude , Anton von Webern , 
 ultra - radicai composer disciple Arnold Schinberg 
 — String Quartet cause uproar 
 recent Salzburg Chamber Music Festival — 
 newcomer conductor . assume 
 directorship Konzertverein Sunday afternoon 
 popular orchestral concert , débfit surprisingly 
 orthodox . work Clemens Krauss , new 
 young conductor , quickly establish promising 
 operatic Kapellmeister , symphonic débit confirm 
 favourable impression . programme , consist 

 conducting distinguish repose , absence 
 mannerism . pianistic style eminently suited 
 work modern ultra - modern school , lend 
 readily classic work Beethoven Scarlatti , 
 suffer , thing , excessive use 
 pedal 

 sixtieth birthday Emil Sauer , revere pianist , 
 occasion great festival concert honour , 
 - know professional pupil 
 participant . programme include 
 Pianoforte Concertos — grateful legitimate music 
 innocent sort . eve birthday Sauer , 
 recital , play number familiar composi- 
 tion programme 
 vear . return ' romantic ' pianist , Germaine 
 Schnitzer , native city social 
 musical event . sister art , young Erica Morini , 
 develop remarkably year absence , 
 mentally technically 




 812 


 MUSICAL TIMES 


 NOVEMBER 1922 


 dure month . 


 MI " 


 TER , C 


 publish 


 H. W. GRAY CO . , NEW YORK 


 |"MANCHESTER SUNDAY SCHOOL UN 


 MEE PATTISON popular cantata 


 ANCIENT MARINER 


 music text book 


 BARGAIN price . 


 FOYLES 


 121 - 5 , CHARING CROSS ROAD , LONDON , W.C.2 


 MUSIC student . " : 


 degree MUSIC . 


 EXPERT preparation correspondence . 


 < ion 


 FESTIVAL HYMNS 


 XUM