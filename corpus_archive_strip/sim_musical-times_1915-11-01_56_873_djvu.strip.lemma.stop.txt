


 WA = ST 


 MUSI 


 CAL time 


 SINGING - class circular . 


 found 1844 . 


 publish month 


 NOVEMBER 1 


 1915 


 ROYAL CHORAL . SOCIETY 


 ROYAL ALBERT HALL 


 SATURDAY , NOVEMBER 6 


 BLIJAH 


 MISS RUTH VINCENT . 


 MISS PHYLLIS LETT . 


 MR . BEN DAVIES . 


 MR . THORPE 


 BATES . 


 1915 , 3 


 ROYAL ACADEMY MUSIC , 


 YORK GATE , MARYLEBONE ROAD , LONDON 


 N.W 


 ROYAL COLLEGE MUSIC , 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. 


 K.G 


 , ROYAL COLLEGE MUSIC PATRON FUND 


 ROYAL COLLEGE ORGANISTS 


 MENDELSSOHN 


 thousand performer 


 OL IAN " HAL L 


 MONDAY , NOVEMBER 22 , 3.15 


 MR . 


 PERCY SNOWDEN 


 SONG RECITAL . 


 ( direction MESSRS . IBBS & TILLETT 


 PROGRAMME 


 IBBS & TILLETT , 19 , HANOVER SQUARE , W 


 GUILDHALL SCHOOL MUSIC . 


 | JOHN CARPENTER ST . , VICTORIA EMBANKMENT , E.C. 


 LANDON RONALD 


 PRINCIPAL 


 weekly ORCHESTRAL practice conduct 


 H. SAXE WYNDHAM 


 ROYAL 


 MANCHESTER COLLEGE MUSIC 


 042 


 , 1915 


 ASSOCIATED BOARD 


 R.A.M. R.C.M. 


 local examination MUSIC 


 9 1916 . 


 BIRMINGHAM & MIDLAND INSTITUTE 


 SCHOOL MUSIC . 


 dession 1915 - 1916 


 MANCHESTER SCHOOL MUSIC . 


 UNIVERSITY DURHAM . 


 incorporated guild CHURCH 


 MUSICIANS . 


 ASSOCIATE ( A.1.G.C.M. ) , LICENTIATE   ( L.1.G.C.M. ) , 


 REGISTER ORGAN VACANCIES 


 LONDON COLLEGE MUSIC , 


 GREAT MARLBOROUGH STREET , LONDON , W 


 EXAMINATIONS — LOCAL HIGHER 


 VICTORIA COLLEGE MUSIC 


 LONDON . 


 incorporate 1801 


 NATIONAL CONSERVATOIRE 


 CHURCH ORCHESTRAL SOCIETY 


 CHURCH IMPERIAL CLUB 


 ORGAN | 


 VIOLIN F 


 ‘ CELLO 


 CHROMA 


 ACOURS| 


 ROYAL ACADEMY MUSIC . 


 N , W 


 TRAINING TEACHERS 


 UNIVERSITY COLLEGE WALES , 


 ABERYSTWYTH . 


 MUSIC SCHOOL . 


 DEPARTMENT INSTRUMENTAL MUSIC 


 SPECIAL STAFF 


 M. CAMILLE DELOBELLE .. " < VIOLONCELLO . 


 send free book : 


 , 1915 


 PROFESSIONAL notice . 


 MR . SAMUEL MASTERS 


 TENO 


 MR . ERNEST . PENFOLD 


 MR . W. H. BREARE 


 student profession 


 singing 


 MR . F. ROYLE 


 lecturer teacher singing . 


 COACH opera concert work 


 student profession 


 art 


 


 studio : LONDON MANCHESTER , 


 186 , SUTHERLAND AVENUE , LONDON , W. 


 LONDON COLLEGE chorister 


 composer ’ MSS 


 DR . NORMAN SPRANKLING 


 1L.R.A 


 DR . A. EAGLEFIELD HULI 


 revision MUSICAL composition 


 L.R.A.M. ( paper WORK 


 L.R.A.M. EXAMS . , 1910 - 15 


 A.R.C.M. ( paper WORK ) . 


 F.R.C.O. , L.R.A.M .. A.R.C.M. POSTAL TUITION 


 F.R.C.O. SPECIALIST CORRESPONDENG 


 CANDI 


 PERC 


 DR . LEWIS ’ text - book 


 aching DEGREES L. R.A.M. , 1897 - 1914 , 


 undred thirty - success ; A.R.C.M 


 hundre seventy -two suc : 


 C)FEERS want chamber pipe 


 FINE : tone tw O- man ual PEDAL PIPE 


 — — IPON CATHE DRAL.—BASS LAY CLERK 


 organist 


 NORMAN & BEARD 


 pneumatic PEDAL attachment 


 PIANO 


 OLD FIRM . CANTIONES SACRA 


 P. CONACHER & CO . , LTD 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD 


 GOLD medal 


 646 


 ROMAN LITURGY 


 edit 


 REST , O CHRIST — 


 647 


 RA RECITAL serie 


 NN 


 NNNN 


 NNN 


 NNNNN 


 


 ENGLISH CAROL BOOK 


 MARTIN SHAW . 


 PERCY DEARMER 


 LONDON , W 


 concert public meeting 


 KINGSWAY HALL 


 MAGNIFICENT ORGAN PLATFORM 


 splendidly situate & light 


 melodious technique 


 PIANOFORTE 


 J. A. O'NEILL 


 G. E. BAMBRIDGE , F.T.C.L. 


 HANDBOOKS MUSICIANS 


 interpretation 


 MUSIC 


 THI 


 reveal contemporary evidence 


 


 ARNOLD DOLMETSCH 


 APPENDIX , 


 - illustrative piece 


 PIANOS 


 BOSWORTH edition 


 book CHRISTMAS carol 


 WESTMINSTER ABBEY . 


 book 37 church anthem 


 CHRISTMAS CAROLS 


 BOSWORTH & CO , 


 new edition 


 BACH ' 


 48 


 PRELUDES & fugue 


 


 BROADWOOD 


 phem 


 SONS 


 NGED fo 


 , i9i5 . 649 


 SINGING - class CIRCULAR 


 HUGO WOLF LYRIC . 


 9 ° = — # 4 . 


 


 : OWL . 


 , 1915 . 651 


 score BERLIOZ 


 1 modern editing . add Damcke error continue 
 opera publish death , 
 Mlle . Pelletan wish preserve unity 
 edition 

 ' Beethoven ix . Symphonies , ' late 
 Sir George Grove remark ' curious disregard 
 composer [ beethoven’s| wish ' display 
 editor . come 
 recent time appreciate flavour 
 iconoclastic spirit pervade german editing 
 score . example 
 - Complete Edition Berlioz 
 work , complete , probably 
 . leipsic publisher ’ 
 thoroughly . engraving clear , 
 scheme edition excellent , price 
 moderate . fault lie choice editor . 
 , face , appear 
 choose wisely,—Felix von Weingartner , 
 direction fine performance Berlioz 
 , late Charles Malherbe , 
 possess valuable collection Berlioz 
 autograph ( Library Conservatoire ) , 
 creditable work way analysis 
 concert programme . 
 doubt leading spirit 
 , M. Malherbe amiable man 
 struggle successfully teutonic arrogance 

 conductor ideal editor 
 moot point . having effect 
 particular reading work passage , 
 self - persuasion believe 
 author intend reading , 
 absolute text opposed . 
 hand , conductor able elucidate 
 technical point understandable 
 hear orchestra auditorium . 
 Weingartner case , ability , , 
 temper fact conversant 
 french orchestral tradition , prove 
 mutilation Berlioz bassoon . bad 
 , editor know work 
 author . ' deal score , 
 ignorant follow , 
 change constantly method ( 
 possess ) , little acquaintance 
 Berlioz literary work letter , 
 hint find . example 
 cite letter Liszt ( June 27 , 1852 ) , 
 Berlioz , refer violin 
 violoncello scene movement 
 ' Harold ' Symphony , point error 
 second violin french edition , 
 remark wish oboe bassoon 
 add string , 
 psalmody ( repeat G B ’ ) . fault 
 engraving repeat ' monumental ' 
 edition , composer wish disregard 
 respect wood - wind instrument . * 
 example , case 




 652 


 653 


 654render 
 aate wl 

 main fact . Berlioz autograph noi § althoug ! 
 existence , depend MS . ls d greater 
 copyist . finale , time indicatia fhe presen 
 : p. 67 , Allegro non troppo ( j = 80 ) 
 ; 1 919 grould c 
 p. 78 , Doppio movimento ( = % ) * § . , omer ’ 
 evidently clerical error . te TE 
 second © o * similar error bs Hinsrument 
 cause trouble regard tempo J nistake , ar 
 Scherzo Beethoven ninth Symphony . Berlioz cc 
 Dofpio movimento undoubtedly com Bmobable 
 main theme finale appear ® copyist | 
 p. 69 follow : ' cal basso , ' 
 ex . 1 . vould ' 
 9 stance , n 
 6 = r col ¢. 
 ' # ' | 
 p. 93 note double valt fintende | 
 ( , 
 remem bere 
 JE PES Poign : 
 init 
 ano 
 overture 
 lor .4/ 
 french edi 
 figure violin , sei Band | 
 quavers quaver . ime witho 
 editor declare copyist error f fortunat 
 add superfluous tail , write # fia letter o 
 imaginary Dofpio movimento — highly improbabé fin hi 
 thing copyist todo ! double j — — _ _ 
 time alter character piece , # ff * Smolian , 
 Liszt piano 
 * vocal score , indication copyist follow amas q 

 exactly , include mistake add tail semibreve 




 655 


 SS instance editor ’ 
 petty puerility scatter edition . 
 Berlioz , wish prominent , 
 label ' Solo . ' indication editor 
 abolish , reason term 
 old - fashioned,—one naturally expect work 
 composer bear 1803 uptodate ! 
 term employ time Debussy Ravel 
 modern french composer 
 scarcely deem ' old - fashioned , ' 
 Germans . Berlioz alter 
 ( necessity ! ) , rate let 
 alter accordance modern french 
 idea . editor ’ reason , con- 
 ductor score eye ( 
 performer way ! ) , know 
 prominent . , 
 wonder Weingartner 
 german composer interlard score 
 ' hervortretend ' similar expression mean 
 Berlioz ' Solo ' ? 
 time editor perceive , 
 ' King Lear ' Overture ( p. 22 ) 
 cut Berlioz ' Solo ' oboe 
 bassoon add esfress . 

 touch alteration , mutila- 
 tion , impertinence leipsic edition , 
 trust sufficient prove 
 unprejudiced person disgrace musical 
 Germany . aye ! slur France . 
 lead musician engage 
 prepare faithful edition work 
 Bach , Beethoven , Mozart , & c. 

 possible 




 XUM 


 656 


 MUSIC war 


 1914Mansfield Wilson College , Chambersburg , l’a , 
 October 18 , follow 

 Sonata . 2 + » Mendelssohn . 
 Allegretto Pastorale wee Capocc ? . 
 Grand Cheeur Symphonique Purcell J. Mansfield . 
 Andante F — oes Beethoven . 
 Toccata fugue D minor ... Bach . 
 allegretto b flat ves Lemmens . 
 concert overture e flat Faulkes . 
 Rondeau g minor ... Sterndale Bennett . 
 ' worthy Lamb ' * amen ' ... Handel 

 programme centain helpful annotation 




 import 


 musical 


 instrument 


 dance . * 


 XUM 


 659 


 


 660 


 


 661 


 663 


 committee music WAR time 


 OLD ARABIAN TURKISH violin 


 EASY ORGAN MUSIC . 


 664r | come organ music proper , series book 
 — uder title ' Village Organist ' claim 
 attention . suspicion average village 
 organist thank proper 
 , possibly ' village ' use 
 disparagingly , way ' suburban ' 
 ' provincial . ' reason title series 
 unfortunate . numerous book | 
 mention especially useful 

 . 18 contain funeral music — Dead March 
 ' Saul , Beethoven , Mendelssohn , 
 Chopin , effective Mackenzie 
 ' Story Sayid ' ( 6 — c 

 . 17 consist wedding music — stock 
 marche Mendelssohn Wagner , couple 
 Guilmant Hoffman ( c 




 666 


 ' ' = 2 ' | ! 7 | 


 -—+ — _ 0 - 8- — _ _ , _ |-—3 ~~ : = , 


 DR . E. H. THORNE BACH recital 


 1915 . 667 


 ORGAN recital 


 appointment 


 PIANOFORTE MUSIC 


 


 VIOLIN MUSIC , 


 PIETA , SIGNORE ' : 


 ROSSINI STRADELLA ? 


 editor ' MUSICAL TIMES 


 WELSIT HYMN origin . 


 editor ' MUSICAL TIMES 


 ES 


 DOWELL 


 compendious way . 


 671 


 SONGSTHE war musical composition 

 speak Mr. Isidore de Lara - british 
 concert recently , Mr. H. B. Dickin point 
 time national stress follow season 
 musical awakening . people know career 
 Napoleon influence Beethoven , 
 calamitous event 1812 1870 quicken 
 artistic national impulse Russia France . 
 similar phenomenon occur 
 history british music . reason believe 
 John Dunstable , accredit inventor modern 
 music , follow Henry V. France ; war 
 inspiration , cause influence 
 early elizabethan musician founder 
 wonderful madrigal school , come 
 Armada . 18th century produce crop 
 unforgettable patriotic song , 19th Crimean 
 War direct cause foundation 
 Royal College Military Music Kneller Hall . 
 stress present War unparalleled , conse- 
 quent musical awakening proportionately significant . 
 , , bind come country , 
 composer public 

 Pocket Sing - Song Book . Novello . 1s 




 672music village 

 complaint hear dearth good 
 music village small town — rate 
 South England — venture think interest 
 reader learn start musical 
 Society somewhat unusual line , considerable 
 difficulty , encourage organize similar 
 Societies . year find impossible 
 raise quartet , vocal instrumental , tenor 
 viola apparently non - existent . , 
 addition village newcomer , 
 musical , determine try start 
 meeting encourage folk work mutual enjoyment . 
 singer form choir , instru- 
 mentalist orchestra , decide work part- 
 singing singer , chamber - music 
 ' string ' pianist available , arrange 
 meet month house . ' choir ' 
 consist seven member season . 
 attempt - singing , 
 choice music limited , fall 
 chiefly - arrangement , Bishop ' sleep , 
 gentle lady , ' Stevens ' blow , blow , thou winter wind , ' 
 Shield ' o happy fair , ' & c. instrument consist 
 violin , viola , violoncello , excellent 
 pianist , , good solo , practise 
 quartet Beethoven , Haydn , Gade , & c. programme 
 consist item , include duet , trio , 
 quartet possible , object induce 
 member meet practise concerted music . original 
 idea meet drawing - room modify 
 find non - performer want attend 
 listen , village hall . 
 order comfortable , collect chair , 
 screen , xc . , screen hall 
 want , addition drugget room look 
 furnished , crowding . great 
 advantage transfer hall — gentleman 
 point view — smoking allow 

 concert season , venture 
 prove successful , extend , 
 month October March , practice 
 week house ; spite assurance 
 pessimist Society die 
 novelty wear , membership increase 
 season , fourth year like 
 thirty singing member dozen instrumentalist 
 quartet , include non - performer 
 total seventy - member , 
 draw neighbour village . choral item 
 practise Croce ' cynthia , thy song , ' Pearsall ' light 
 soul , ' Converso ' , ' Benet ' 
 creature merry , ' Linley ' let careless , ' 
 Stanford Cavalier song man chorus , Mendelssohn 
 * hear prayer ' ( string accompaniment ) , 
 oratorio chorus , & c. instrumental work bring 
 forward quintet Schubert Schumann , 
 Beethoven C minor G major Quartets , Gliére 
 Quartet , trio Gade , Sinding , 
 Schiitt , occasion , Saint - Saéns arrangement 
 pianoforte variation theme Beethoven 

 regard financial , find subscription 
 5 . member cover expense , chief 
 £ 5 hire hall , 49 pianoforte , 
 £ 3 cart chair , sundry expense : 
 programme cost , type 
 office member . light refreshment ' interval ' 
 provide lady concert . average 
 cost lady caterer 2 . , 
 season 




 PLAINSONG . 


 HARMONY voice - production 


 MUSIC : 


 manage FRANCE 


 compose 


 B. J. DALE WALTER H. SANGSTER 


 R. WALKER ROBSON | JOHN E. WEST 


 7 — — _ 


 NX 


 BSON 


 -4—#4 _ ! _ _ _ _ _ 


 SS 


 


 CARUS 


 OPERA MILAN : 


 CARUSO return year . 


 SP 


 6795 


 PROMENADE concert . | ASSOCIATED BOARD EXAMINATIONThese music - making proceed varied | 1915 SYLLABUS . 
 fortune inevitable lengthy series troublous time . follow suggest metronome rate gy 
 matinée support | Advanced Grade , List B ( 1915 ) , examination 
 evening performances,—a fact serve a| hold December year : 
 pointer concert - giver come winter . Bennett , stay 

 past week musical scheme under- | e   @ 
 sweeping modification . special ' Allies ’ programme ' | Clementi , Study - gh 
 ( russian night especially popular ) , | Renaud , stay . ae 
 new unfamiliar work way | es o 4 
 old try favourite . sound | Bach , /re / ide - = 1 % 
 policy : certainly leave scribe little matter | Beethoven , sova / ... 126 
 aac : : . Parry , Shulbrede Tune 

 Elgar ' Polonia ' receive second performance sit ai nt ania : ' eo 




 vete 


 ATIONS 


 679 


 SHAFTESBURY OPERA 


 4 VETERAN SUGGESTION generous 


 EXAMPLE 


 correspondent 


 BELFAST 


 BIRMINGHAM 


 680 


 BOURNEMOUTHThe conclude Symphony Concerts summer sefis 
 met - deserve appreciation , good windy 
 result - rate performance Sullivan ' Macbeth 
 Overture , César Franck D minor Symphony , Duka ’ 
 * ' L’Apprenti Sorcier ' Scherzo , Rimsky - Korsakor ’ 
 * Scheherazade ’ Suite , item 

 undoubtedly splendid start tk 
 winter season . inaugural Symphony Concert Mr. De 
 Godfrey Orchestra fine fettle , tk 
 commencement hope prove wh 
 successful season highly auspicious . seco 
 concert equally enjoyable , concert ¥ 
 acquaint wholj 
 interesting composition , range Schumann D min 
 Symphony , Borodin symphonic Sketch ' Steppes 
 Central Asia ' ( performance concert ) , 
 Beethoven ninth Symphony , happy frivolity ¢ 
 Percy Grainger ' Shepherd Hey ' pitt 
 hardly attractive . soloist Miss 
 de Benici ( Grieg Pianoforte concerto ) Mr. Philip 
 ( Tchaikovsky Violin concerto ) , w 
 welcome heartily 

 concert know recent year Monday ' pop . 
 designate Monday ' Specials ' ( hitherto ' 
 terminological monopoly daily press ) , open 
 serie October 11 central featu 
 Ambroise Thomas ' Mignon ' Overture ; Mendelssobt 
 ' italian ' Symphony ; Concerto G minor oboe } 
 Handel ( performance concert ) , wis 
 charmingly play Mr. F. Murphy , newcomer te 
 orchestral rank ; ' Charmant Oiseau ' aria fe 
 Félicien David ' La Perle du Brésil , ' sing tastefully } 
 Miss Nora Read , Bournemouth 




 BRISTOL 


 DEVON CORNWALL . 


 TORQUAYPLYMOUTH 

 Misses Smith resume Thursday ' 
 o’clock ' residence Plymouth . visitor 
 enjoy hour programme vocal instrumental music 
 high standard , fund Music War - time reap 
 benefit . Extempore Chamber Music Club continue 
 meeting , past month chance 
 visitor distance introduce freshen 
 influence . difficult find music 
 literature , art philosophy fit time , 
 nearly chamber music seriously study , 
 César Franck Pianoforte quintet , Beethoven 
 Quartets , good work live composer . 
 find bring solace restore balance 
 mind , study rehearsal playing 
 member Club progress appreciably 
 exact art 

 - song sing Presbyterian Church 
 England Ladies ’ Choir September 22 wound soldier 
 Ford , Mr. Percy E. Butchers conduct . organ 
 recital St. Catherine Church , Plymouth , 
 date , Mr. R. Waddy , Mr. A. G. Serle play violin 
 piece vocal solo 




 EXETER 


 SE SRE 


 OTTERY 


 CORNWALLEDINBURGH 

 musical season open Saturday , October 9 , 
 Harrison Concert . artist Miss Carrie Tubb , 
 Madame Edna Thornton , Mr. Maurice D’Oisley , Mr. Robert 
 Radford , Miss Marie Hall , Mr. R. L. Forbes . 
 pianoforte solo particularly receive , 
 notably Rubinstein Barcarole flat . October 16 , 
 M. de Pachmann entertain large audience 
 Usher Hall . seldom fortune artist receive 
 welcome . thirty - Variations Beethoven , 
 Prelude Mendelssohn , Chopin group 
 chief item programme . number 
 add end recital . Mr. Robert Burnett , 
 baritone vocalist , song recital 
 evening , assist Miss Phyllis Graves , Miss Newell , 
 Mr. Theodore Crozier , Mr. Philip Keddie . 
 gratifying able announce Paterson Orchestral 
 Concerts continue - ninth season , 
 conductorship M. Emil Mlynarski . visit 
 conductor Mr. Thomas Beecham Mr. Hamilton 
 Harty . Reid Professor Music , Mr. Donald F. Tovey , 
 appear pianist Beethoven Concerto G ; Miss 
 Fanny Davies , M. Arthur Rubinstein , polish pianist , 
 engage prospective concert . feature 
 series prominence novelty , 
 work British School . Royal Choral Union , 
 Mr. Moonie Choir , intimate intention 
 carry work , remain 
 present condition success attend effort . 
 Mr. Simpson Classica ! concert 
 season , large , remarkable musical 
 prospect good 

 GLASGOW 




 LIVERPOOL 


 , IQI5 . 683 


 MANCHESTER DISTRICT 


 684 


 , IQI5 


 NEWCASTLE DISTRICT 


 SHEFFIELD DISTRICTThe Halifax Choral Society , Mr. Fricker , propose 
 ' selection Franck ' Beatitudes ' 
 ( probably ) ' Dream Gerontius , ' addition 
 usual ' Messiah ' ; Huddersfield Choral Society 
 ( Dr. Coward ) promise , addition ' Messiah , ' Verdi 
 ' Requiem ' ( time Huddersfield ! ) 
 Coleridge - Taylor ' Hiawatha 

 Harrogate Symphony Concerts , 
 - week provide good performance 
 highly interesting programme pay 
 attention native composer , work Sir Frederic Cowen , 
 Sir Charles Stanford , Sir Edward Elgar , Dr. Maclean , 
 Dr. Somervell , Messrs. Hervey , Ernest Farrar , Percy 
 Fletcher , Norman O'Neill , , having include . 
 creditable performance Beethoven Choral 
 Symphony mention outstanding event 
 season 

 long series able soloist , english , 
 appear , Mr. Julian Clifford maintain 
 efficiency orchestra . Harrogate profit 
 misfortune town , phenomenally 
 successful season , hope financial result 
 satisfactory Corporation , run 
 concert attraction town 




 686 


 ADELAIDE 


 BERLIN 


 RUSSIA 


 _ — _ _ 


 content 


 SANGSTER . 


 


 , 1915 


 


 


 dure month 


 limit 


 CHRISTMAS CAROLS.- 


 ~OODEVE , MRS . ARTHUR . — 


 A. B. 


 HARP 


 _ _ 


 publish — 


 0 . 6 


 H. W. GRAY CO . , NEW YORK 


 T ER LI 


 — _ — _ — 


 689 


 ITHE NOVELLO edition 


 ‘ 0 . 2 ) . 3 


 1—4 


 690 


 anthem advent 


 music ADVEN 


 collect 


 SET music 


 S. S. WESLEY 


 adapt arrange anthem 


 SIR FREDERICK BRIDGE , C.V.0O 


 watch YE , PRAY YE 


 WACHET , BETET ) 


 CANTATA 


 SOLI , CHORUS , ORCHESTRA 


 compose 


 J. S. BACH 


 ADVENT HYMN 


 " lowly guise THY KING APPEARETH 


 SOPRANO SOLO CHORUS , orchestral 


 accompaniment 


 compose 


 R. SCHUMANN . 


 bless 


 watch 


 CANTATA advent 


 SOPRANO SOLO CHORUS 


 


 hymn sing congregation 


 music compose 


 HUGH BLAIR 


 advent 


 CHURCH CANTATA 


 word select write 


 compose 


 BLOW YE trumpet 


 zion 


 CANTATA advent 


 compose 


 WARWICK JORDAN 


 


 


 th 


 MU 


 FRI 


 691 


 nicantata CHRISTMAS 


 ADVE 


 RETH " 


 TESTRAL 


 D.D 


 ation 


 t 100 


 100 


 TIN 


 BETHLEHEM 


 solo voice choru 


 word write arrange 


 E. CUTHBERT NUNN 


 music 


 J. H. MAUNDER 


 nativity 


 SOPRANO , TENOR , BASS SOLI , 


 CHORUS 


 word hymn select music 


 compose 


 THOMAS ADAMS 


 story BETHLEHEM CHRISTMAS CANTATA 


 word write 


 SHAPCOTT WENSLEY 


 music SOPRANO , TENOR BASS SOLI , 


 CHORUS ORGAN 


 compose 


 JOHN E. WEST 


 CHRISTMAS EVE 


 CONTRALTO SOLO , CHORUS 


 ORCHESTRA 


 compose 


 NIELS W. GADE 


 CHRISTMAS scene 


 FEMALE voice 


 compose 


 FREDERIC H. COWEN 


 CHRIST MAS ORATORIO 


 J. S. BACH 


 SOLI CHORUS 


 word 


 ROSE DAFFORNE BETJEMANN 


 music 


 JULIUS HARRISON 


 HOLY CHILD 


 SOPRANO , TENOR , BASS SOLI , 


 CHORUS ORGAN 


 THOMAS ADAMS 


 YULE - TIDE 


 THOMAS ANDERTON 


 PALING 


 STARS 


 CHRISTMAS HYMN 


 


 CHRISTINA ROSSETTI . 


 B. J. DALE 


 anthem CHRISTMAS 


 LONDON : NOVELLO COMPANY , LIMITED 


 RRR EE RER SRY REE 


 CRERRRRERR REE 


 ERERERESERRR RR ER ERR RRER SESE BES 


 PRREERER RRR ERRE 


 EERE 


 PRRRRRRRBRER REE 


 693 


 370 


 BSSSSS 2S 3 


 NOVELLO 


 CHRISTMAS CAROLS 


 late number 


 service . 


 ORGAN . 


 CHURCH CANTATA . 


 POPULAR CHURCH MUSIC 


 J. H. MAUNDER 


 


 ANTHEMS 


 ORGAN MUSIC 


 NN 


 SACRED VOCAL MUSIC 


 694 


 ORGAN MUSIC CHRISTMAS 


 village ORGANIST 


 edit 


 JOHN E. WEST 


 CHRISTMAS MUSIC 


 PRICE SHILLING NET 


 NOV ELLO 


 HYMNS tune 


 SUITABLE 


 MEMORIAL 


 SERVICESThou art grave ) 
 — — sleep thy sleep j 

 BEETHOVEN.—Thou art grave 
 BEST , W. T.—Dies irze ( Day wrath ) ... om 
 BONAVIA - HUNT , H. G.—Dies ire ( Day 

 wrath ) ial ide 
 CRESER , W.—O Great Redeemer . memorial 




 01 


 . HYMN . tune 


 CH 


 EDWA 


 MILLION 


 SPLEN 


 POPUL 


 LONDON 


 NOVELLO COMPANY , LIMITED 


 IST 


 695 


 REDUCED price 


 CHRISTMAS CAROLS 


 EDWARD BUNNETT 


 75 , THORPE ROAD , NORWICH 


 million sell 


 POPULAR 


 SPLENDID 


 , 


 favourite CHRISTMAS anthem 


 intercessory hymn 


 adapt HYMN KING ALBERT book . 


 music compose 


 EDWARD GERMAN 


 


 GERARD F. COBB 


 easy short service 


 ORGAN ( optional ) vocal harmony 


 village congregation 


 special suffrage end war - time . 


 anthem moment . 


 D.D 


 FALLEN 


 poem 


 


 LAURENCE BINYON 


 set music CHORUS ORCHESTRA 


 CYRIL BRADLEY ROOTHAM 


 manual PLAINSONG 


 divine service 


 containing 


 canticle note 


 psalter note 


 prepare 3 


 issue net book 


 contain FAV 


 canticle proper psalm . 


 edit point chanting ( 


 PSALTER chant combine . 


 POCKET SI NGSONG book 


 soldier , sailor , school , HOMES 


 ETC . 


 IG containing : NATIONAL anthem , ETC . , ally 


 MARCHING song . 


 NATIONAL FOLK - song . 


 HYMNS 


 PRICE shilling 


 FAVOURITE opera 


 lm adapt self - help singer 


 DAVID C. TAYLOR 


 1 . FAUST 


 2 . IL TROVATORE . 


 3 ) TANNHAUSER 


 MARITANA . | historical music drama 


 prologue , act 


 5 . BOHEMIAN GIRI . 


 


 6 . daughter regiment . | RAYMOND ROZE 


 aaaaaaa ah 


 7 ) MARTHA . 


 NOVELLO COMPANY , LIMITED 


 NOVELLO & COS 


 RECENT publication 


 SONGS 


 PERCY BOWIE . | NOEL JOHNSON . 


 child - maye . ROBERT MASSON . 


 HENRY COATES . — ; 


 supplication . ZUMMERZETZHIRE . 


 HERBERT E. CRIMP . BOTHWELL THOMSON . " 4 


 follow colour . HERBERT W. WAREING . 


 HONEYSUCKLE LANE . " 


 SECRET heart . H. LANE WILSON . 


 BASIL GRAHAM . C. WHITAKER WILSON . DA 


 DREAMLAND , . dreaming . | 


 PIANOFORTE SOLO . ( VIOLIN & PIANOFORTE 


 HUBERT BATH . 


 . : | JOSEPH HOLBROOKE . 


 H. SCOTT - BAKER . | ~~ 


 month 


 sketch PIANOFORTE 


 FREDERIC H. COWEN 


 book 


 content 


 arrangement SMALL ORCHESTRA COMPOSER 


 rd . ' ~ 


 DANCE TUN DOLL’S - HOUSE SUITE 


 ne , 3 


 4 little piece 


 


 collect arrange ly little FOLK 


 CECIL J. SHARP 


 follow dance tune issue compose 


 , boy , . - morning , 


 musicseller 


 700 


 popular 


 marche 


 


 ORGAN 


 album ORGAN 


 NOVELLO 


 7o1 


 SHORT prelude 


 


 ORGAN 


 original composition 


 


 ORGAN 


 SEVEN piece 


 ORGAN 


 compose 


 THEODORE DUBOIS 


 content 


 1 . PRELUDE 4 . INTERLUDE 


 2 . CANTILENERELIGIEUSE]| 5 . PRIERE 


 7- MARCHE - SORTIE 


 SEVEN piece 


 ORGAN 


 compose 


 ALEXANDRE GUILMANT . 


 4 . MINUETTO 


 1 , OFFERTOIRE 


 2 . WEDDING MARCH 5 . MARCHE TRIOMPHALE 


 3 . BERCEUSE 6 . POSTLUDE 


 7- FANTAISIE SUR DEUX MELODIES ANGLAISES 


 NOVELLO ELEMENTARY MUSIC 


 manual 


 physical exercise , dance , 


 game 


 


 INFANT SCHOOL 


 


 MARGARET ALEXANDER HUGHES 


 music arrange 


 RAINBOW 


 music reading ladder beginner 


 - second thousand 


 WEBSTER 


 child primer 


 


 th eory music 


 seventy - 


 melody unfigure bass 


 harmonising 


 unsolicited testimonial . 


 popular - song 


 S.A.T.B. 


 NEW edition 


 PAGANINI 


 ( classic 


 SCH 


 


 art play : ™ 


 BER 


 VIOLIN 


 N 3 


 treatise 


 40 


 LATIN ORGANIST ® 3 


 HEL 


 135 b .. | 


 


 NG 


 NOTE 


 IST 


 793 


 NOVELLO EDITION 


 classic , romantic , work EMINENT 


 MODERN composer 


 PIANOFORTE MUSIC . 


 school study 


 select list 


 CESAR FRANCK work 


 PIANOFORTE SOLO 


 DANSE LENTE 


 PIANOFORTE DUET 


 CANTABILE 


 VIOLIN PIANOFORTE 


 ALLEGRETTO DE LA SONATA 


 CANTABILE 


 ANDANTE QUIETOSO - 


 LENTO DU QUINTETTE 


 LARGHETTO DU QUATUOR CORDES 


 CELLO PIANOFORTE 


 PIANOFORTE , VIOLIN , 


 ’ CELLO 


 VIOLINS , VIOLA , ’ CELLO 


 ORGAN 


 MINIATURE score . 


 LIZA LEHMANN . 


 GUY D'HARDELOT 


 TERESA DEL RIEGO . 


 FLORENCE AYLWARD . 


 DOROTHY FORSTER . 


 ETHEL BARNS . 


 " SOUL . " 


 LILIAN RAY . 


 MARTIN BARCLAY . 


 PAUL A. RUBENS , 


 


 THEORY MUSIC 


 contain 


 numerous test - question answer 


 JULIA A. O'NEILL 


 TIMES 


 MORNING POST 


 musical opinion 


 lady 


 select list 


 CHAPPELL & CO . new popular song 


 NEW YORK , TORONTO , MELBOURNE ; music seller 


 practical guide english VIOLIN MUSK 


 HERMANN LOHR . 


 ROBERT CONINGSBY CLARKE . 


 HAYDN WOOD . 


 F. S. BREVILLE - SMITH 


 HUGH R. HULBERT . 


 " ELEANORE 


 F. PAOLO TOSTI . 


 GRAHAM PEEL . 


 MONTAGUE F. PHILLIPS . 


 " * summertime ATHELNEY 


 OLD 


 edit 


 ALFRED MOFFAT . 


 prefatory note 


 HINE 


 let 


 extra supplement 


 review 


 prospect competition 


 movement 


 MIDLAND ( BIRMINGHAM ) 


 COMPETITION FESTIVAL 


 school JUNIOR choir 


 SECONDARY school choir . 


 FOLK DANCES 


 _ — _ 


 


 1076 . 


 


 BANGOR NATIONAL EISTEDDFOD . 


 retrospect 


 WELSH soldier ’ eisteddfod 


 WINCHESTER 


 soprano 


 contralto . 


 tenor 


 NORTHAMPTON 


 HAUGHTON ( STAFFORDSHIRE ) DISTRICT 


 singe memory . rest 


 bagpipe BATTLE . 


 ALT 


 strict , SHORT anthem introit . 


 XUM 


 O MERCIFUL 


 — 7 — — t 


 = q " " @ = — — — — _ _ 


 SAS 


 P O MERCIFUL 


 XUM 


 O MERCIFUL 


 J — t t 


 19 } . 


 165 . 


 195 . 


 204 , 


 160 , 


 185 . 


 43 


 RECS 


 LONDON 


 6 - 9 - 15 


 SSES SSSR RESP RAR o RRA AER 


 compose 


 W. G. ROSS 


 PRRREARES 


 PERSSASSSSERER b 


 PRBS 


 7 - 9 - 5 


 old carol varied harmony 


 > BAD 


 MI 


 GOOD KING WENCESLAS 


 — _ — _ — — _ — — _ — — 


 = = SS - | — _ = | = E 


 — _ — , 


 L 


 GOOD KING WENCESLAS 


 EE = 


 WY 


 LU 


 HL 


 GOOD KING WENCESLAS 


 S&S ' 


 


 $ — _ — ~ t t = 


 GOOD KING WENCESLAS 


 P 4 ~ 


 GOOD KING WENCESLAS 


 publish net book 


 


 edition 


 INDIA PAPER edition : 


 APPROPRIATE chant ( cathedral PSALTER chant 


 INDIA PAPER edition : 


 point chanting 


 INDIA PAPER edition : 


 MESS 


 YORK 


 NT t