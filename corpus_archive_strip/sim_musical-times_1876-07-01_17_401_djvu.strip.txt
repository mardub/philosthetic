


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


PUBLISHED ON THE FIRST OF EVERY MONTH


JULY 1, 1876


NOVELLO, EWER AND CO


80 & 81, QUEEN-STREET, CHEAPSIDE


EREFORD MUSICAL FESTIVAL. 


OPRANO.—RE-ENGAGEMENT REQUIRED 


OPRANO.— RE-ENGAGEMENT REQUIRE


S}; MARY MAGDALENE, PADDINGTON.— 


514


PROFESSIONAL NOTICES


USIC ENGRAVED, PRINTED, AND PUB


RUSSELL’S MUSICAL INSTRUMENTS. 


EAN’S CHEAP MUSICAL INSTRUMENTS. 


ORGAN-TONED HARMONIUMS. 


HATTERSLEY & CO.’"S CELEBRATED 


ORGAN-TONED DRAWING-ROOM MODEL


HATTERSLEY & CO.’S 


BOOKS OF DESIGNS AND ABOVE 200 TESTIMONIALS 


SHOW ROOMS:— 


STANDARD AMERICAN ORGANS


MANUFACTURED BY 


PELOUBET, PELTON & CO., NEW YORK


METAL EQUITONE FLUTES, WITH KEYS


CYLINDER FLAGEOLETS, WITH KEYS


& W.SNELL’S IMPROVED HARMONIUMS, 


ALS


LONDON AGENT


PIANOFORTE SALOON


55, BAKER STREET, LONDON, W


TESTIMONIAL. 


HENRY TOLHURST, 


AST LONDON ORGAN WORKS


QUARTERLY SALE OF MUSICAL PROPERTY


KIRKMAN, | NIEDERMAYER, 


NEWELL


ALLISON


ESTABLISHED APRIL, 1866. 


HE ENGLISH GLEE UNION. 


ASSISTED BY 


R. WALTER MACFARREN’S MORNING 


CONCERT, ST. JAMES’S HALL, SATURDAY, JULY 1, 


HE YORKSHIRE (ST. CECILIA) CONCERT 


REDUCED


OUSELEY AND


POINTED


PRICES OF 


MONRK’S


PSALTER


NOW READY. 


TONIC SOL-FA EDITION. 


AKER’S PIANOFORTE TUTOR


A MANUAL OF SINGING


FOR THE USE OF CHOIR TRAINERS & SCHOOLMASTERS, 


FORTIETH EDITION. 


THE COLLEGIATE AND SCHOOL


SIGHT-SINGING MANUAL


APPENDIX


(COMPANION WORK TO THE ABOVE


COLLEGIATE VOCAL TUTOR


CATHEDRAL CHANT BOOK


NEW EDITION OF 


DR. BENNETT GILBERT’S POPULAR WORK


SCHOOL HARMONY


EVERY SUBJECT HAS ITS SPECIAL EXERCISES. 


OF


MENDELSSOHN'S WORKS


CRITICALLY REVISED BY


DR. JULIUS RIETZIs now being issued by Messrs. BREITKOPF & HARTEL of 
Leipzig. 
This Edition is in a form similar to that in which ‘the

Works of Beethoven have already been published by the 
same firm

The Works are published (printed from engraved plates) 
in Full Score, in Orchestral and Vocal Parts, and the Vocal 
Works with Pianoforte Accompaniment. The parts of the 
Chamber Music are, for the sake of practical convenience, 
included in the Edition in Full Score




NEW AND EASY EDITION


BRAHMS’S 


UNGARISCHE TANZE


C. JEFFERYS, 67, BERNERS ST


JUST T PUBLISHED


NEW SONGS


ENGLAND’S MUNICIPAL GLEE. 


THE MAYOR AND CORPORATION


OUTHWELL SCHOOL SONGS


OPINIONS OF THE PRESS


518


ADIES’ SCHOOL, LANGHAM HOUSE, 


RINITY COLLEGE, LON DON— 


BUSINESS FOR SALE. 


BUSINESS FOR SALE. 


THE MUSICAL TIMES 


AND SINGING-CLASS CIRCULAR. 


JULY 1, 1876


BACH’S MASS IN B MINOR. 


BY EBENEZER PROUT, B.A. 


ALTO. _———_ 


UAT {= — L


6 6 76


ALTO


TENORE


520


TENORE. 


EEE EE


SS 


222 S25 SS SS SS SSS: 


1" 7 — 


PE OE EO


THE PURCELL SOCIETY


CEDIPUS AT COLONOS


HER MAJESTY’S OPERA


ROYAL ITALIAN OPERA


PHILHARMONIC SOCIETYIr was no doubt a compliment to Herr Rubinstein to 
perform his ‘‘ Dramatic Symphony” at the sixth Concert of 
this Society ; but it must be placed on record that it was 
given by the directors at the cost of the subscribers; for 
to take an hour’s attention from an audience which might 
be bestowed upon the great works in art was indeed to

exact from them an expensive testimonial to our dis- 
tinguished guest. The word “ Dramatic” is an exceed- 
ingly useful one for a composition so fantastic and 
incoherent, for it covers a multitude of what might be con- 
sidered defects in a production of more regular construction. 
There is both melody and beauty of treatment in the 
“‘ Adagio,” but the less that is said of the other move- 
ments the better; indeed the feeling of regret was widely 
spread that so much labour should be spent with so small 
a result. At the seventh Concert Sterndale Bennett’s 
Overture to “‘ The Merry Wives of Windsor’’ was a wel- 
come item in the programme. Written at the age of 18, 
when the composer was a student in the Royal Academy 
of Music, the work unmistakably shadows forth the power 
which was afterwards shown ; and the only wonder is that 
it should still remain in manuscript. At the same Concert 
Mdlle. Anna Mehlig won well-deserved applause for her 
artistic rendering of Beethoven’s Concerto in E flat. The 
morning concerts have been well attended, the only novelty 
introduced being Rheinberger’s ‘‘ Wallenstein’s Camp,” a 
movement displaying much clever instrumentation and 
many catching bits of melody

FESTIVAL OF TRINITY COLLEGE, LONDON




526An excellent Concert was given by the choir of St. 
Thomas’s Church, at Westbourne Hall, Westbourne- 
grove, on the 7th ult., under the able conductorship of Mr. 
H.R. Eyers. We have before had occasion to call atten- 
tion to the classical nature of the programme provided by 
Mr. Eyers; and when we say that on the present 
occasion it included Spohr’s Twenty-fourth Psalm, ‘‘ The 
Earth is the Lord’s,” a Motett by Ferdinand Hiller, ‘Oh! 
weep for those’’ (performed for the first time in this 
country), Mendelssohn’s Ninety-eighth Psalm for a double 
chorus, “Sing to the Lord a new-made song,” and 
Flotow’s Serenade, “Still slumber the leaves to-night”’ 
(the violin obbligato most artistically played by Miss 
Gabrielle Vaillant), it will be seen that the conductor had 
undertaken no easy task either for himself or his choir. 
The principal vocalists—Miss Amy Aylward, Miss Reimar, 
Miss Thekla Fischer, Mrs. J. France Collins, Miss Kate 
Brand, Madame Patey, Mr. Henry Guy, Mr. Harry Selig- 
mann, Mr. Robert George, and Mr. Gordon Gooch— 
acquitted themselves to perfection; and the instrumen- 
talists—Miss M. Bucknall (pianoforte) and Mr. Lazarus 
(clarionet)—elicited the warmest applause. The choral 
singing was uniformly good throughout the evening, and 
in every respect the Concert was a genuine success

Tue Lower Rhenish Festival commenced on Whit 
Sunday with a performance of Handel’s Oratorio ‘“ So- 
lomon,” the solo vocalists being Mdlles. Meysenheym 
(Soprano) and Amalie Kling (Contralto), Herren Ernst 
(Tenor), Adolf Wallndfer (Baritone), and Herman Pfeiffer 
(Bass). The band and chorus numbered over 500, and the 
excellent training of Herr Ferdinand Breunung was evi- 
denced by the uniformly perfect rendering of the choruses. 
The Oratorio was given as originally scored, but with the 
addition of Mendelssohn’s Organ part. At the second 
concert, on the following day, Beethoven’s ‘ Eroica” 
Symphony and the Overture to “ Euryanthe”’ were finely 
performed by the band. Schumann’s Cantata, ‘ Des 
Sangers Fluch,” and Mendelssohn’s Finale to “ Loreley” 
again gave the choir an opportunity of displaying its 
powers, Mdlle. Kling, in the solo parts, also gaining high 
honours. The other important work selected was Brahms’s 
‘‘ Triumphlied,” for a chorus of eight parts. The third day 
was devoted, as usual, to the “ Artists’ Concert,” when a 
most interesting programme was provided. Madame Essi- 
poff was the pianist, and for her admirable rendering of 
Liszt’s Etude in D flat received two encores, both of 
which she responded to. Herr Breunung played Men- 
delssohn’s Sonata No. 1 upon the organ, in order to exhibit 
the new instrument recently erected by a local builder. 
Every credit is due to Herr Breunung, who in the capa- 
cities of pianist, organist, and conductor proved himself 
thoroughly competent

Ar Mr. Henry Leslie’s third subscription Concert on 
the rst ult., a very excellent selection of part-music was 
given, the encores being for Sir W. S. Bennett’s Part-song 
‘Come, live with me ;” R. L. De Pearsall’s Madrigal, ‘* Lay 
a garland on her hearse ;” Morley’s “‘ Now is the month of 
maying;” and Walter Macfarren’s Part-song, ‘‘ Shepherds 
all and maidens fair.” A new and melodious Part-song, 
by the concert-giver, entitled a ‘‘ Dream of calm,” was 
enthusiastically received, and Mr. Abercrombie, who sup- 
plied the place of Mr. Sims Reeves, absent from indisposi- 
tion, elicited warm applause. The instrumentalists were 
Madame Varley-Liebe (violin) and Mr. Charles Hallé 
(pianoforte). The closing concert of the season was in 
every respect highly attractive, for in addition to the truly 
refined singing of the choir, Mr. Sims Reeves appeared 
and sang his very best throughout the evening




FULL ANTHEM FOR FOUR VOICES


O -- 84. 


7 5 \ : 


= 4. 1 4. 


1 4 J 


IN | N 


LORD, I CALL UPON THEE


SS 


53


REVIEWS


ORIGINAL CORRESPONDENCE


PROFESSOR BLACKIE AND PROFESSOR 


OAKELEY


TO THE EDITOR OF. THE MUSICAL TIMES


535


VOCAL AND DRAMATIC ARTISTS’ PROTECTION 


SOCIETY. 


TO THE EDITOR OF THE MUSICAL TIMES


GOD SAVE THE KING.” 


TO THE EDITOR OF THE MUSICAL TIMES


ALFRED B. ALLEN. 


ALFRED ALLEN’S “ GAVOTTE.” 


TO THE EDITOR OF THE MUSICAL TIMES


CLEVELAND WIGAN


TO THE EDITOR OF THE MUSICAL TIMES


A. B. A


WHEN SHOULD ‘BENEDICITE’ BE SUNG?” 


TO THE EDITOR OF THE MUSICAL TIMES


THE “ MACBETH” MUSIC. 


TO THE EDITOR OF THE MUSICAL TIMES


LET


TO CORRESPONDENTS


BRIEF SUMMARY OF COUNTRY NEWSBrixton.—On Tuesday, May 30, the St. Matthew’s Choral Society 
gave its concluding Concert of the season in the schools, Church 
Road, the vocalists being Miss Agnes Larkcom, Miss Adelaide Bliss, 
Mr. Alfred Pitman, and Mr. Robert Pakeman. Mr. J. B. Gaunt and 
Mr. S. Fisher accompanied, and Mr. George Shinn conducted

Buxton, DERBYSHIRE.—The opening Concert of the season, under 
the direction of Mr. Julian Adams, took place on Thursday evening 
the 22nd ult., when the excellence of the programme well war- 
ranted the very large attendance. The vocalists were Madame Sinico, 
Madame Demeric-Labiache, and Signor Campobello, all of whom 
met with their usual success. One of the principal features of the 
evening was the performance of Beethoven’s Concerto Op. 56 in C, the 
solo parts being played by Messrs. Adams, Witte, and Walton in a 
manner which was highly appreciated by lovers of the artistic. Mr. 
Julian Adams, who loses no opportunity to reciprocate the confidence 
reposed in him by his many patrons, may well be congratulated upon 
the great success of the first concert of the season

CANTERBURY.—Handel’s Oratorio, Samson, was perfermed by 
Dr. Longhurst’s choir, on the 14th ult. Miss Anna Williams, Miss 
Joyce Maas, Mr. Cummings, Mr. Wadmore, and Mr. Rhodes were 
the solo vocalists. The choruses were well rendered. Mr. T. 
Morrow played the trumpet obbligato in ‘‘ Let the bright Seraphim,” 
and the performance altogether was highly appreciated. Dr. Long- 
hurst conducted

538 THE MUSICAL TIMES.—Jury 1, 1876

Wadham College Hall. The programme comprised instrumental 
compositions by Mendelssohn, Beethoven, Mozart, &c., and vocal 
pieces by Brahms and Schubert. Mdlle. Sophie Lowe was the vocalist. 
——The Philharmonic Society gave a Morning Concert on the rgth 
ult. in the Sheldonian Theatre. Schumann's Paradise and the Peri 
formed the first part; the second part comprising Mendelssohn’s Over- 
ture Hebrides, Weber's Jubilee Overture, &c. he soloists were 
Mdlle. Sophie Lowe, Mrs. H. Blake, Miss Enriquez, Mr. W. Shake- 
speare, and Mr. H. S. Howell. Mr. Taylor conducted. The Concert 
was very successful

PARKSTONE, near BouRNEMOUTH.—On Wednesday, the 14th ult., 
an Organ Recital, in aid of the funds for the permanent Organ for the 
church, was given by Mr. Alfred J. Eyre, organist of S. Peter’s church, 
Vauxhall, London. The Instrument built by Mr. Pulbrook for Captain 
F. Sandys Dugmore, of the 64th Regiment, is erected on a platform at 
one end of the large Parish School Room, and can thus be made available 
for Recitals, Concerts, &c. The programme consisted of selections 
from the works of Beethoven, Schubert, Heller, Molique, Bach, Smart, 
Mendelssohn, Rink, and Spohr, all of which were well rendered, Bach's 
Prelude and Fugue in D being re-demanded.m—On Monday the 19th 
ult. an Instrumental Concert was given in the same room by several 
local amateurs; the violin playing of Mr. Sanders, of Poole, and the 
performance of Mendelssohn’s Ruy Blas Overture being specially 
worthy of praise. Mr. Eyre contributed several solos on the organ

PHILADELPHIA.—At the last Concert of the Orpheus Club, which 
was given at the Musical Fund Hall, the selection consisted of English 
and German Glees. The club was assisted by Mr. Richard Hoffman, 
the pianist, of New York, who created a great sensation, playing 
Mendelssohn’s “ Capriccioso,” Chopin’s “ Polonaise,” Op. 53, and his 
own admired composition, “Solitude,” and brilliant Rondo “ Cas- 
carilla.” The concert was a decided success




DURING THE LAST MONTH


OVELLO’S TONIC SOL-FA SERIES


19 


MR. CHARLES FRY 


WESLEY TESTIMONIAL FUND


2E NI 


ND


SSELL, 


HALES


HOOCOH HAHN RA


OYAL ALBERT HALL CHORAL SOCIETY. 


TRAINING FOR THE PROFESSION. 


TO STUDENTS, COMPOSERS, AND PUBLISHERS. Dr. Hottoway, F.C.O., Musical Editor, 51, St. Paul’s-road, N.W

OZART AND BEETHOVEN SOCIETY, 
Beethoven Rooms, 27, Harley-street, W. President, the 
Marquis of LoNDONDERRY; Vice-President, Herr ScHUBERTH. The 
next CONCERT, for the Benefit of Herr SchuseRTH, will take piace 
on Thursday, 6th of July. This and the ScuuBEerT Society afford 
an excellent opportunity for young rising artists to make their first

appearance in public. Full particulars on application to




GLUCK’S OPERAS, 


IPHIGENIA IN TAURIS


AND


IPHIGENIA IN AULIS. 


WAGNER’S OPERAS, 


LOHENGRIN


AND


TANNHAUSER


COMMUNION SERVICES 


FRANZ SCHUBERT


NOVELLO’S OCTAVO EDITION


MASSES


FRANZ SCHUBERT


NOVELLO’S OCTAVO EDITION


540


THE ANGLICAN HYMN-BOOK


NEW EDITION, REVISED AND ENLARGED


SECOND SERIES


HE ANGLICAN CHORAL SERVICE BUOK, 


USELEY AND MONK’S PSALTER AND 


OULE’S COLLECTION OF WORDS OF 


HE PSALTER, PROPER PSALMS, HYMNS


OULE’S DIRECTORIUM CHORI ANGLI- 


ANGLI


JOULES DIRECTORIUM CHORI 


HE ORDER FOR THE HOLY COMMUNION. 


SUITABLE FOR HARVEST FESTIVALS. 


PRAISE GOD IN HIS HOLINESS


COMPOSED BY 


BERTHOLD TOURS


541


ALBERT LOWE’S


HARVEST CAROL. 


“HOLY I$ THE SEED-TIME


TWOPENCE. NOVELLO, EWER AND CO


TWELVE HYMNS WITH TUNES


FOR


HARVEST


SELECTED FROM


THE HYMNARY


HARVEST FESTIVALS


EIGHT 


ORIGINAL HARVEST TUNES. 


ARTHUR H. BROWN


PRICE ONE PENNY


PLEAD_ - - - - - - - . 


G- - - - - - 4 


*| SUMMER ENDED, HARVEST O'ER . 


METCALFE


HARVEST ANTHEMS


ALTER MACFARREN’S NEW “SUITE


DE PIECES.” 


FIVE INTRODUCTORY VOLUNTARIES 


ALLCOTT, W. H.—THOU VISITEST THE EARTH AND 


ATTISON, T. MO HOW PLENTIFUL IS THY GOOD- 


AYLOR, W.—THE EYES OF ALL WAIT UPON THEE. 


RIDGE, J. FREDERICK.—GIVE UNTO THE LORD THE 


EETON, HAYDN.—THE EYES OF ALL WAIT UPON 


542


THE REV. THOMAS HELMORE’S


MANUAL OF PLAIN SONG


NOW PUBLISHED


HARMONIES OF THE BRIEF DIRECTORY


THE VILLAGE ORGANIST


NEW SONG. 


SONGS AND DUETS


COMPOSED BY


ANTON RUBINSTEIN. 


THOU’RT LIKK UNTO A FLOWER


EVENING SONG


SONG FROM EGMONT


BYHHHNHKOHHDW YF 


COMPOSITIONS


BY THE LATE


DR: '$.« 8S... WESEEY


SERVICES


ANTHEMS


ORGAN MUSIC


PIANOFORTE


GLEES, ETC


543


NEW WORK FOR THE ORGAN


THE ORGAN


INTENDED TO‘ASSIST THE STUDENT IN ACQUIRING


A SOUND KNOWLEDGE OF THE INSTRUMENT AND 


ITS PROPER MANIPULATION; 


WITH A SERIES OF 


ORIGINAL EXERCISES AND ILLUSTRATIVE 


COMPOSITIONS


BY


PREDERIC ARCHER


LONDON


NOVELLO, EWER AND CO., 1, BERNERS-STREET (W.) 


AND 80 & 81, QUEEN STREET, CHEAPSIDE (E.C.) 


REDUCED TO SIX SHILLINGS


NATIONAL AIRS OF ALL COUNTRIES


WORDS BY


THOMAS MOORE. 


EDITED BY CHARLES W. GLOVER


FOURTEEN SONGS


SET TO 


POEMS OF ROBERT BURNS


CONTENTS


ORIGINAL COMPOSITIONS FOR THE ORGAN


13


BY 


HENRY SMART


NOVELLO, EWER AND CO.’S ONLY COMPLETE AND 


UNIFORM EDITION OF


MENDELSSOHN’S


THIRTEEN TWO-PART SONGS


NEW AND CHEAPER EDITION. 


R. G. W. HAMMOND’S PIANOFORTE 


THE HYMNARY (TUNES ONLY). 


SPECIMEN PAGE.) 


FORWARD! BE OUR WATCHWORD


HENRY SMART


ONWARD, CHRISTIAN SOLDIERS


ARTHUR S. SULLIVAN. 