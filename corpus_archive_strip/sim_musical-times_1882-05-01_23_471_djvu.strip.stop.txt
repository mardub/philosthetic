


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 PUBLISHED MONTH 


 CATHEDRALlral , 

 WEDNESDAY , June 7 = 2d 30 a.m. , Mendelssohn ‘ JAH . ” 
 THURSDAY , jun ae | a.m. , Sullivan “ P PRODIG AL 
 SON 1 Beethoven SYMPHONY 

 THURSDAY , 2.45 p.m. , Spohr “ JUDGMENT 




 RIPTION 


 J.1 


 PRINCIPAL ARTISTS : 


 THI 


 ARRANGEME 


 PROFESSIONAL NOTICES . 


 MISS MARIE COPE 


 MR 


 MR . R 


 M R. 


 USIC SCHOOL.—CHURCH ENGLAND 


 XUM 


 YRAL 


 SOUN 


 


 243 


 ; JOSEF TOR CONCE RT COMPANY . 


 ENOR 


 VOICES W ANTE D 


 WANTED , 


 GAGED 


 ASSO 


 DISEN 


 XUM 


 COMPETE NT 


 ED 


 4 NGAGEMENT WANTED 


 * HOROU GHL Y 


 - ENGAGE MENT . U 


 244 


 LOCAL EXAMINAT IONS ELEMENTARY MUSICAL 


 1882 . 


 LOCAL EXAMINATIONSIN INSTRUMENTAL VOCAL 


 CLASSES LECTURES 


 LATEST NOVE LTY . GREAT SUCCESS . 


 - NEUMEYER 


 MAJESTY ROYAL LETTERS PATENT . 


 PERFECT CHEAPEST 


 INSTRUMENTS . 


 WARRANTED STAND CLIMATES . 


 NEUMEYER CO 


 BELL ORGANS 


 AHEAD 


 MANUFACTURED 


 BELL & CO . , GUELPH , ONTARIO , CANADA 


 NEUMEYER CO 


 W. MORLEY & CO 


 MUSIC PUBLISHERS 


 70 , UPPER STREET , N 


 BEG ANNOUNCE MEET REQUIRE 


 MENTS INCREASING WHOLESALE TRADE 


 OPENED EXTENSIVE PREMISES 


 269 , REGENT STREET , W 


 FUTURE WHOLESALE SHIPPING 


 ORDERS COMMUNICATIONS SEN 


 NOVELLO 


 OCTAVO EDITIONS OPERAS 


 PERFORMEDs . d 

 WAGNER “ LOHENGRIN ” .. aie aa Se 6 
 7 ” “ TANNHAUSER ” ? ma ae Se 16 
 es ‘ Frying DuTcHMAN ” aa § 6 
 “ ‘ ¢ TRISTAN UND ISOLDE ” 10 0 
 BEETHOVEN “ FIDELIO ” ’ .. ee < 3 6 

 London : NovetLo , Ewer Co 




 PUBLISHED 


 WEDDING MARCH 


 COMPOSED MARRIAGE 


 H.R.H. DUKE ALBANY , K 


 


 H.R.H. PRINCESS HELEN ) 6 


 WALDECK 


 CHARLES GOUNOD 


 LONDON : NOVELLO , EWER CO 


 XUM 


 YUIRE 


 TRADE | 


 PPING 


 ENT 


 K.G 


 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 1 


 1882 


 ENGLISH OPERA . 


 XUM 


 245 


 246large and| 
 vivifying influence continental art , 
 inter 

 studied encouraged 
 France Italy . country Europe 
 opera regarded exotic , 
 literature 
 universally treated spirit mingled flippancy 
 unbelief . spirit irreverence 
 old favourite assumption any- 
 thing shape libretto good 
 musical treatment . degradation 
 honoured opera . complaint 
 opera degraded arises question , 
 downward movement 
 complete degeneracy detected ? Gluck , 
 Beethoven , Weber — — successful 
 efforts elevation regeneration . 
 , particularly , remarkably dra- 
 matic compositions , writings letters , 
 shows clear sense vastness unexplored 
 future music - drama . ‘ need fear 
 coming composers dramatic music 
 England necessarily dependent form 
 . structure Wagnerian operas , 
 exponents - called W agnerian theories , 
 chief theorics — essential 
 highest development drama music — 
 articles faith Gluck Beethoven , 
 recognition certain great 
 fundamental truths , unnecessarily clamorous 
 annunciation followers Wagne1 
 prevented universal acceptation 
 truisms . ‘ pamphleteering army , guise oi 
 pioneers , scaled certain - marked heights 
 taken possession Wagner , 
 making h - adquarters vigorous pro- 
 paganda bas : operations future , 
 apparently oblivious fact heights 
 previously occupied Gluck , 
 Mozart unacquainted . 
 degree influence exercised 
 English Opera future teaching 
 example great master modern music - drama , 
 little doubt great 
 stimulus given composers , revival 
 lof English Opera longer regarded 
 visionary 

 NIBELUNG RING ” 
 ANALYSIS RICHARD WAGNER TRILOGY 
 F. CorDer . 
 VALKYRIE ( continued page 190 




 XUM 


 247 


 83 , 


 XUM 


 248 


 SIEGFRIED 


 XUM 


 XUM 


 249 


 250 


 . 53 


 DUSK GODS 


 XUM 


 251 


 XUM 


 252 


 XUM 


 253 


 XUM 


 HEINRICH HOFMANN 


 ACLteristic 

 alities — Russia Hungary — music 
 chiefly remarkable strangeness 
 rhythm , — Scandinavia — strongly marked 
 erratic forms melody . preference 
 result inclination policy , 
 case illustrates tendencies 
 present musical age , , restless 
 search novelty , leaves hole unexplored , 
 corner unswept . great masters eighty 
 years ago satisfied introduce national 
 melody twice lifetime , way compli- 
 ment — Beethoven — indulgence whim 
 — Haydn . symphonies 
 concertos named half - dozen countries , 
 lower walks Art classic themes rhythms 
 elbowed strong uncouth barba- 
 rians . ‘ excite alarm , 
 hope . ancient hero , struck 
 earth , sprang renewed strength gained 
 Great Mother , type things , 
 form Art effete 
 invigorated intervals inoculation 
 sap nature untutored naturalness . 
 emphatically music , , 
 course time , culture taken roughness 

 254 

 _ |derives greater enjoyment free 

 interpretation — granted musi- 
 cally robust stand 
 legs , craving poetic designa- 
 tion application roots poetic 
 instinct . rate , composers publishers 
 reckon , , bounds , 
 gratify , use means popu- 
 larising art . Beethoven Sonata C sharp minor , 
 found , ranked 
 choicest gems music , Cranz 
 Hamburg called ‘ Moonlight ” gave 
 impetus work , , carries easily 
 access difficult . 
 title — stop discuss propriety — 
 invests music poetic significance , 
 arrests attention thousands cases , 
 , result 
 possible . time characteri- 
 sation music carried far , easily 
 exaggerated andcontemptible . noblest 
 purest music appeals 
 directly unaided fancy feeling , moving 
 stimulus , guidance exterior 
 things . writers characteristic music , 
 Hofmann , said , indus- 
 trious comprehensive labourer , dividing 
 attention , like Schumann , equally 
 objective subjective themes . Schumann , 
 fancy , considerable influence 
 respect — , , materially 
 affecting method , determining nature 
 work . recognition fact 
 Hofmann , dedicated 
 pieces entitled ‘ ‘ Diary ” 
 widow illustrious man . names 

 pieces suggest Schumann : ‘ Discourse , ” “ Hunting 
 Scene , ” ‘ Nightingale sings , ” ‘ * Fare- 
 , ” * * Snowflakes " — come like Schu- 
 mann echoes . Let add unworthy 
 connection . Schumann writ- 
 , true , Hofmann 
 write dead master “ Carnival ” scenes ; , 
 way , pages “ Diary ” 
 charming fancy graceful 




 XUM 


 255Hofmann 

 alive spirit buoyant , subtler quality 
 |in Beethoven , Mozart , especially Haydn , 
 | rich , wanting . said 
 |for Hofmann rarely places 
 position deficiency humour con- 
 spicuous . natural habitat gravities 
 gentler graces nature humanity , 
 , rightly claim_to rank expositor 
 ; poet 

 Referring ‘ Italian Love Tale , ” evi- 
 dence Hofmann knows utter musical 
 | language divine passion foundin Andante 
 | ; sostenuto seeks represent dialogue , 
 | fashion Mendelssohn - known * Song with- 
 |out Words . ” essential respects effort 
 |complete success . joy fear love mingle 
 | true proportion . Peace agitation succeed 
 } , music 
 | climax eager , abun- 
 | dance feeling , transcend power art . 
 look . 3 pieces designated “ * 
 Trumpeter Sakkingen , ’ ” ’ Andante sostenuto 
 founded lines 




 256 


 GREAT COMPOSERS 


 257 


 XUM 


 XUM 


 1849 


 MUSICAL TIMES 


 MUSIC “ * MACBETH . ” 


 CUMMINGS 


 PURCELL ’ 


 1669 


 @ 7H 


 1095 


 290 


 1S 20 


 MSS . 


 261 


 262 


 ROYAL ITALIAN OPERA 


 SACRED HARMONIC SOCIETY 


 MONDAY POPULAR CONCERTS 


 CRYSTAL PALACE 


 GUILDHALL SCHOOL MUSIC 


 XUM 


 264MR . GANZ CONCERTS 

 NEW season Concerts began St. James 
 Hallon Saturday , 22nd ult . , encouraging circum- 
 stances , attendance large work 
 interesting . usual , manager conductor sought 
 varied tastes selection pieces , 
 , glad admit , entire success . Ad- 
 mirers classical school , example , rich 
 abundant feast Beethoven ‘ ‘ Egmont ’ Over- 
 ture , B flat Symphony , Mendelssohn 
 Violin Concerto , specially attractive 
 appearance new soloist , Herr Ondricek . 
 gentleman , , believe , 
 nationality Herr Joachim , boast years ’ 
 experience art , flatter 
 possession considerable ability . [ lis tone lacks 
 roundness fulness come time 
 better instrument ; , , pure quality , 
 ‘ * carries ” parts large hall . execu- 
 tant Herr Ondricek shines cantabile passages , 
 , need scarcely add , strength weak- 
 ness violinist decisively . 
 young Hungarian instrument sing charmingly . 
 , better rendering 
 theme Mendelssohn slow movement . brought 
 intense exaggerated expression , witha 

 finish bespoke accomplished artist . bravura pas . 
 sages Herr Ondricek satisfactory , owing 
 occasional lack clearness ease . , suc . 
 cess Mr. Ganz audience complete , cer . 
 tainly disposed complain fact . novelty 
 composition produced Concert bore 
 Liszt , belongs roll great virtuoso - called 
 symphonic poems , having themes “ Hell , ” “ Purga . 
 gatory , ” * Paradise ” Dante ‘ ‘ Divina Commedia 




 MADAME MENTER RECITAL 


 XUM 


 LL 


 LONDON MUSICAL SOCIETY 


 MYSIC BIRMINGHAM 


 265 


 MUSIC MANCHESTER . 


 ( CORRESPONDENT . ) 


 MUSIC YORKSHIRE . 


 ( CORRESPONDEN1 


 MUSICAL TIMES 


 XUI 


 XUM 


 TH 


 OBITUARY 


 267 


 26 


 PP 4 — — — — 


 4 5 — T T 1 : | 1 T | 


 OFFERTORY 


 SENTENCE 


 SU 


 OFFERTORY SENTENCE 


 CTES 


 — _ _ @ 


 OFFERTORY SENTENCE 


 PIANOFORTE ALBUMS 


 8 . 


 13 . 4 


 34 


 44 


 52 . { 


 XUN 


 EL 


 275a 
 5 

 Beethoven ) , Chromatic Fantasia ( J. S. Bach ) , Sonata 

 D minor , Op . 31 , . 2 ( Beethoven ) , Troisitme Ballade 
 flat ( Chopin ) , ] moto continuo ( arranged Brahms ) , 
 Sonata , E flat , Op . 27 , movement ( Beethoven ) , 
 Troisitme Ballade flat ( Chopin ) , Sonata C minor , 
 Op . 111 , movement 
 ( Handel ) , ‘ ‘ Masaniello ” ( Thalberg ) , Nocturne C minor , 
 . 12 ( Chopin ) , Toccata ( Schumann ) , Prelude Fugue 
 E minor ( J. S. Bach ) , Barcarole ( Chopin ) , Fugue 
 flat ( J. S. Bach ) , Ballade G minor ( Chopin ) , Sonata 
 C , movement ( Paradies ) , Aufschwung ( Schumann 

 Italian Concerto , movements ( J. S. Bach ) , Study 

 inF minor , . 4 ( W. Sterndale Bennett ) , Sonata 

 minor , Op . 5 , movements ( Beethoven ) , Selection 

 Etudes Symphoniques ( Schumann ) , Organ Fugue 
 G minor ( J. S. Bach , arranged Liszt ) , Fugue ° ( J. 
 S. Bach ) , Allegro Grazioso ( W. Sterndale Bennett ) , 
 Capriccio B flat minor , Op . 33 ( Mendelssohn ) , Fugue 

 second Concert season St. Stephen 
 Musical Society given Tuesday evening , rsth 
 ult . , Athenzeum , Shepherd Bush , large 
 audience assembled hear Hofmann Cantata ‘ * Melu- 
 sina . ” Considering result , Signor Dinelli reason 
 feel proud choir band . soloists , , 
 fairly good ; , Miss Rose Trevor 
 carcely equal important Melusina , Mr. 
 James Sauvage desired im- 
 petuous Raymond , music suiting admirably ; 
 minor parts exceedingly given Miss Kate 
 Baxter , Mr. Lindeman , Mr. Stewart Beekley . Seeing 
 ‘ * Melusina ” ’ proved successful , cer- 
 tainly advisable Society select com- 
 poser * * Cinderella ’’ concerts . second 
 miscellaneous . Signor Dinelli conducted 
 skill judgment 

 Beethoven ) , Fugue E minor 

 great appreciation manner ) 20 2 
 | compicte t 




 276season 

 Leipzig , 
 h 
 
 Ninth Symphony Beethoven , March 30 

 ed Concerts Gewa Bach 

 Dt . Matthew ” Passion given , benefit } 
 band , Thomaskirche Good Friday . } 
 year Concert teatures special 

 interest , Choral Symphony preceded 
 Choral Fantasia , prelude ( thought ) Beethoven 
 masterpiece . Fantasia piano taken | 
 Herr Reinecke sensitive appreciation . per- | 
 formance Symphony , excellent , wz | 
 signalised remarkably pure exact rendering | 
 scherzo , hard produce confusion . | 
 beginning Concert Sanctus , Agnus | 
 Dei Max Bruch produced time . 
 written 

 Kyrie 




 THEFestival Association announced given 
 consecutive davs , commencing 23rd inst . 

 performed Handel * * Messiah ’ ” ’ l tre 
 Te Deum Jubilate ; Beethoven Symphony C min 
 Choral Symphony ; Bach Cantatas ; selec . 
 tions Wagner ‘ Lohengrin ” ; Berlioz ’ ‘ “ Fall 
 lroy ” ; Mass Schumann , ‘ musical Director 
 s Mr. Theodore Thomas ; vocalist 
 engaged include names Madame Amalia Materna 

 Mrs. Osgood , Miss Annie Cary , Miss Emily Winant , 
 Signor Campanini , Mr. Theodore J. Toedt , Mr. Whitney , 
 Herr Henschel . Mr. H. Clarence Eddy organist 

 E fe 

 Hall Thursday aft ernoon , 25th inst . , excerpts Joachim , L .. Ries , Zerbini , Piatti ) > Chopin Nocturne 
 * Nibelung Ring ” performed , | F minor , Polonaise flat , pianoforte 
 conductorship Seidl , ( Herr Bonawitz : ! ; Spohr Scena Cantante , violin 
 : , : : pianoforte accompaniment | Herr Joachim ) ; Beethoven 
 ru rmances Bach ‘ ‘ Passion according | Sonata C minor ( MM . Bonawitz Joachim ) . 
 ia om . : age s Chi urch , , Soho , hav e re-| item w s excellently performed , elicited applause . 
 peated pad fi : ‘ VPriday t ! 1e past Lent , | | Mademoiselle Kufferath sang lieder Schumann , Rubin- 
 rie ee Friday afternoon , cuseon Mr. ; stein , Brahms success . rex vretted 
 Barnby . occasion shortened form Evensong ithe failure excellent Concerts , financial 
 employed , Magnificat Nunc cimittls | noint view , renders scries uncertain . 
 year sung Mr. Barnby setting EF . flat c pm pose d 
 Festival Sons Cl The| consequence severe domestic bereavement sus- 
 church , Mr. W. Hodge , p1 | tained Herr Alfred Griinfeld , Herr Franke cancelled 
 service , orchestra | rsa contract yeanaronte Recitals 

 performers 




 


 XU 


 _ _ 


 27 


 3 ( MM . 


 XUM 


 28 


 278 


 REVIEWS 


 THI 


 XUM 


 280 


 FOREIGN NOTES . " 


 XU 


 2&1 


 VOUTNE 


 XUMFriedrich Wilhelm Kicken , - known composer 
 songs , died Schwerin , 3rd ult . , age 
 seventy - 

 282 MUSICAL TIMES.—May 1 , 1882 . 
 “ nes 
 W ve subjoin , usual , programmes concerts * re- | mined specified standards . , principk pes 
 cently given leading institutions abroad : — |be right regard general education — thier ’ C 
 Paris.—Gounod Festival Cirque d’Hiver ( April 2 ): Symphony public purse pays musical instruction — sure 145 ° 
 D major , Chanson et Stances “ Sapho , ” Divertissement | wrong insist application Subjecy 
 * * Cinq Mars , ” ao Cecili : ay ” ey Nase soe pa Music . arises question : ‘ 
 " La Reine dé Saba , " Funeral aces Marionette , ” Medi- principle determine standard ? ” } , 
 tation Prelude Bach , violin solo ( Gounod ) . Concert | answering question guided Opinio 
 Spirituel bg ee gg Nog gg si ber RA mercer gin rnind eminent composers . , said , 9 
 irom Septet ( Beet , ie ; Vocal dust ( Gounod ) ; ‘ Andante religioso et partisans thy 
 Allegretto ( Mendels : ohn ) ; Chorus ( Palestrina ) ; Overture , “ ‘ Oberon ” | ] System teaching , safe guided b 
 ( Weber ) . Lamoureux ( Good Friday ): “ La Vallee de| opinion ? , , happens 
 Josaphat , ” Symphonie biblique ( G. Salvayre , time ) ; Requiem principle composers agreed , viz , 
 ( Th . Gouvy , time ) ; Sym shonie espagnole ( Lalo ) ; Act ce . ‘ SiR , 
 “ Lohengrin , ” March “ Tannhauser ” ( Wagner ) . Concert ultimate result al 1 teaching g ought facility : 
 Spirtuel M. Pasdelou ; p ( Good Friday ): Marche Funebre , time ] reading notation language . uhigh write gystem 
 Aaa hag ( Mozart ) ; Benedictus , pages sang : Clip ecard musical thoughts , i.c . , staff notation . Let ype ™ ° 
 ga PS cua Gancecuetaire : ( April 16 ): fied ipnees point guiding principle ; pitaine 
 ( Beethoven ) ; Fragment “ Orpheus ” ( Gluck ) ; Air “ Le ] insist children elementary schools exa , methor 
 Nozze di Figaro ” ( Mozart ) ; Theme vé ariations Scherzo , ¢ mined accordance . oifer suggestio taught 
 Linas fom Septet ( iesinoven ) . Concert Popsle ( ARTA ) , . think musical education . clementay fl 
 Rossini ) ; Violoncello Concerto ’ ( Saint - Saéns ) ; Air ‘ Les|schools long retarded difference opinics 
 Saisons ” ( Massc ) ; “ er ee ot — — av sections musical public ows 
 rf ie hd ys si erod 1¢€ Wassenet egre agitato ~ : ain ) 1 
 ot wae fo « Mireille * eeueds : Guten , ae best MEDS palate desired end , ang 
 Wives Windsor a. believe systems ee 
 Lyons . -Conce * Sainte - Cécile Society ( March 26 ): “ La Fille | prominently public nenti¢ 
 le Grandval ): Air “ Iphige en Tauride ® capable producing best poses results . Th Jate 
 ! ittiioiod ° ° % stem , introduced schools pom 
 ig — St. Thomas Church ( Good : Passion Music , | years , prestige implied given 
 St , Matthew ( J. S. Bach ) . “ Misea Solemnis . ” D | fact ‘ ‘ sanction Committee ing ‘ 
 _ , Cologne . — Cone « ee Heroica ( Beethove ak | Council Education , ” stan¢ ds condemned eyes know 
 ~ Wiesbaden.—Cur - Orchester ( M : | musical men Dr. Hullah reports exami- 
 giel ) ; ymphony , Pract ioe ~ spert } Pn nt | nations training colleges . letter notation , 
 al bi Op . 97 ; Ra : sph . ee Preludes , advocates believe best means leading 
 ymphonic poem ( Lisz t ) . Cacilien - Verein tap ril 4 ): sl pawl children knowledge staff , willing ¢ 
 place competition systems , equal con 

 Society ( April 6 ): “ Christus fac tus 

 tr 
 3altimore.—Pea 5 rz ): phony , C major 
 Schubert ) ; Songs ( R. Schumann ) ; Pianoforte s oe iszt ) ; Faust 

 overture ( R. Wag Peabody Concert ( March 25 ): Symphony , E | : : 
 major ( R. Sc humann ) ; : ( R. eee ig ) ; Pianoforte solos | prej udice staff rise ip , responsi- 
 ak Ph ; o — heir — _ ee agner ) - ts ’ | bility rest head Tonic Sol - fa teachers . ” | 
 yncert Peabody Institute ( March 1 lartet , p. 74 , =) er te eels ¢ : ( ae 
 ( Haydn ) ; Agr ’ Macie Minte meee C ask print account doings 
 major ; ‘ Sympl hony , DB flat major ( Haydn ) . Students ’ Concert ( March | - schoolboys meeting Bristol , December 
 18 ): String Qu a4 , Op . 41 ( R. Sc 1 ) ; Songs(Brahms ) ; | 20 , 1579 . “ results vouched produced 
 forte Trio , E flat major , Op . 100 ( Schu > ma:,.| System rendered Elizabethan era , far 
 York.—Philharmonic Club ( March 7 ) “ Con certo , F major | “ - . . ' 
 Variations “ God save Emperor ’ ’ ( Haydn ) ; Ballade , | country concerned , musical landmark — mean 
 G minor ( Chopin ) ; Septet , Serenade F major ( F .L. Ritter ) : old English , commonly called * * Lancashire ” System 
 e — Pup Pr Wel lege wy : 
 Wellesley ( U.S.)—Pupils ’ Concert Welles lege ( March | Sol - fa . hope perusal convince readers 
 20 ): Pianoforte Pieces ( Schumann , Roec Jer ) ; ‘ Sonata Pathétique Pmauents f cer t ee 
 Beethoven ) ; Rondo Capriccioso ( Mendelssohn ) ; Pianoforte , | question musical edt ion ine nentary schools 
 ( Schu : nann , R hei nberger ) ; Fan ( Mozart ) ; Etu | , th fait ! fully , 
 hoy : ( Mendelssohn , Schumann , Mozart , § REENWOOD , 
 Dolby , . 
 ete Concert Profe Author . 19 , Novello , Ewer Co. 
 ie ‘ * Music Primers , ” ’ 
 oe 4 Trafalgar Villa , Cotham New Road , 
 oO lertoire ( Batiste . orchtight ’ " Bictina 1 < 
 stol , April S , 
 March ( Meyerbeer ) ; Romance , ’ , viol lin ( Beet ! 10ven ) ; pl Bristol , AE oi 
 Fugue E minor ( Flagler ) . NARRATIVE Facts.—One boys wrote blackb 
 — — — — — — — ee changes key C C tiat major , wrote rite . 
 k ori tinal key . Th t boys sang exe 
 CORRESPONDENCE . d keynote s finish found th 
 | tr ha 1other boy write c +4 

 TON SOL - FA STAFF NOTATION 




 SYSTEMS 


 MUSICAL 


 IR 


 XU 


 283 


 EDITOR ‘ ‘ MUSICAL TIMES 


 SIR 


 XUM 


 EDITOR * * MUSICAL TIMES 


 FRED . W. WAREHAM 


 MALE - VOICE CHOIRS 


 284 


 ALFRED 3 ! 


 EDITOR ‘ ‘ MUSICAL TIMES 


 D. WRIGHT 


 31 , 1882 


 ORGAN PEDALS ATTACHED 


 EDITOR 


 PIANOFORTES 


 TIMES 


 MUSICAL 


 BRIEF SUMMARY COUNTRY NEWSWe hold responsible opinions expressed 7 
 Summary , notices ave collated srom loca 
 papers supplied correspondents . 

 Banrr.—The members Musical Association gave Miscel c 
 laneous Concert St. Andrew Hall , , ¢ iu 
 thoroughly appreciative audience . programme incit Jed 
 Macfarren Day choral pieces , exce ! ] t ] 
 rendered . instrumental composition : performed mav | 
 mentioned Beethoven ‘ * Moonlight ” Sonata , finely played b : Herr , 
 Hoffmann , Concertante “ Charles Dar violins : 
 pianoforte ( Miss Dickson Herr Hoffmann violinists Mis P 
 Isa Ramsay pianist ) , warmly deservedly applat sded . } 
 Macfarren Cantata Miss M. Coutts , Queen , | ; 
 highly successful ; vocal solos contributed 

 Miss K , Marti 




 XUI 


 SS 


 XUM 


 286 


 DEVONPORT . 


 XUI 


 KILMARNOC 


 288 


 1882 


 MELBOURNE , AUSTRALIPort Giascow.—The Parish Church Choir , conductor . 
 ship Mr. C. E. Midgeley , gave Concert Sacred Music 
 4th ult . , large audience . Dr. A. L. Peace presided 
 organ , played solos skill expression . 
 choruses sung manner reflecting utmost credit 
 training Mr. Midgeley , came forward time 
 occasion composer , aria , “ Mercy Seat , ” 
 exceedingly received 

 Retrorp.—The second Concert Choral Society 
 present season took place 2oth ult . Gade Spring Message 
 Costa Dream works selected , 
 - songs ‘ Bellman ” ( Dr. Macfarren ) “ Hunting Song ” 
 ( Mendelssohn ) deserve special mention , redemanded , 
 Mrs. Daglish solo soprano , Mr. Gregory solo tenor , 
 giving good rendering Beethoven “ Adelaide , ” 
 Conductor accompaniment . Miss Denman pianist , 
 played success Mendelssohn Concerto G minor , 
 Mr. H. White performing condensed 3 orchestral accom 
 paniments American organ . Mr. F. W. Wells Mr. G , F , 
 Ashley acted efficiently eM ts piano organ 

 Rrpon.—Haydn Passion Music sung Cathedral Choir 
 special nave services held Holy Week . Mendelssohn 
 Hymn Praise sung Easter Wednesday special evening 
 service , Cathedral Choir , assisted Ripon Musical 
 Society Harrogate Vocal Union . Symphony played 
 magnificent organ Cathedral Organist , 
 conducted , Mr. H. Taylor , F.C.O.(a pupil ) taking organ , 
 service greatly appreciated large congregation 

 SHERBORNE 

 SouTHGATE.—The eighth Dedication Festival St. Michael 
 Bowes Park , held Wednesday , rgth ult . , services con 
 sisting Holy Communion 11 choral Evensong 7.30 
 service sung Rev. E , V. Casson , Curate St , 
 Michael , followed appropriate sermon Rev. 
 Thomas Helmore , M : z s Chapel Royal . music , 
 rendered choir vo , included Tours Service F ; 
 Anthem , ‘ glad ” ( Tuckerman ) ; Beethoven “ Hallelujah , ” 
 Mount Olives , Mr. Henry J. Baker conducted ; Mr. Chas , 
 F , South , Organist St. Augustine St. Faith , W atling Street , 
 presiding organ 

 Stratrorp.—A Concert Sacred Music given Hall 




 TUNBRIDGE WEL 


 DEATHS 


 31 , 


 THOOn 13th ult . , Peter Josern Ries , 
 Broadwood Sons , brother 

 d pupil Beethoven , aged gr . 
 ck , County 

 ohn Ries : 
 fr 




 EW , REVISED , ENLARGED EDITION 


 TEW , REVISED , ENLARGED EDITION 


 XUM 


 DU RING NG MONTH 


 AVIS , GABRIEL 


 ERS 


 TEW 


 LN WANTED . 


 290 


 CKFIELD PARISH CHURCH.—ORGANIST 


 ORGAN PEDALS PIANOFORTES 


 IRUMMENS ATTACHMENT 


 SPRING WALTZES IAL 


 PETER PIPER 


 HUMOROUS GLEE , S.A.T.B. 


 


 OVO , 


 COMPOSED . 


 C. T. KUHNE , — 


 OLIVER KING 


 


 MUSICAL TI 


 YRTES 


 NISTS 


 ENT 


 UTHORS ’ WORKS CORRECTED , REVISED , 


 4OR SALE 


 RGAN.—WAN1 ED , 


 RGAN - BUILDING PARTNERSHIP . — 


 292 


 WC 


 MPROVED ORGAN PEDALS 


 PIANOFORTES 


 IMPROVED ORGAN PEDALS ACTION 


 PIANOFORTES 


 ACTION 


 555 


 POWER ” ORGAN 


 JAMES CONACHER SONS , 


 ORGAN BUILDERS , 


 BATH BUILDINGS , HUDDERSFIELD 


 VIOLIN BOW 


 MAKERS REPAIRERS 


 GEORGE WITHERS & C0 


 WHOLESALE IMPORTERS 


 MUSICAL STRINGS 


 FINE COLLECTION ITALIAN INSTRUMENTS 


 51 , ST . MARTIN LANE , LONDON 


 JARIS UNIVERSAL EXHIBITION , 1573-¥S% 


 PARIS , 10 


 MEL 30URNE EXHIBITION , 1881.—FIRST | 


 REIS C 


 RUE DU FAUBOURG - POISONNIRRE , 10 , PARIS 


 CH . J. B. COLLIN - MEZIN , 


 VIOLIN , VIOLONCELLO , BOW MAKER 


 MALEY , YOUNG , & OLDKNOW,)5 


 ORGAN BUILDERS , 


 KING ROAD , ST . PANCRAS , LONDON , N.W. 


 INSPECTION INVITED 


 WORKS : WORCESTER , MASS . , U.S.A. 


 


 XUN 


 , BERNERS STREET , LONDON , W 


 GENERALLY . 


 CHOIR- ‘ BOYS . 


 _ ORCHESTRAS CHOIRS 


 INEW MUSIC - ENGRAV ING 


 STEAM PRINTING ) 


 EXHIBITED 


 50 CENT . CHEAPER . 


 SOW 


 N.W 


 } 62 , HATTON GARDEN , LONDON , E.C. 


 EXCELSIOR ORGAN WORKS 


 EGBERT HAL L , EGBERT STREET , 


 ; YEARS ’ GREAT EXPERIENCE . 


 TESTIMONIALS EMINENT MUSICIANS 


 XUM 


 READY , NEW EDITIONS 


 » 2 SACRED MUSIC ENGLISH WORDS 


 » 3 » MUSIC VOCAL ORCHESTRAL 


 PARTS . 


 4 . MUSIC PIANOFORTE . 


 » 5 » SECULAR VOCAL MUSIC . 


 » 6 SACRED MUSIC LATIN WORDS 


 MUSICAL TIMES 


 CONTAINING - CHORAL PIECES , 


 SACRED SECULAR 


 LONDON : NOVELLO , EWER CO 


 ORGAN WORKS 


 EDITED 


 BOOK I.—EIGHT SHORT PRELUDES FUGUES 


 294 


 1882Adagio , F major oo . _ Pleyel , 
 Sarabande , D minor S. Bach . 
 Gavotte , D major ... ‘ S. Bach . 
 Prelude : E. W. views 
 Book IV.—Andante , “ Julius Cwsar ’ ass Handel . 
 Prayer , Larghetto Maestoso ... “ ss a6 Bellini , 
 Marcia Religioso ; Gluck 

 Beethoven . 
 tw ilhelm Bach 

 Andante Cantabile , Trio , C minor 

 Book V.—Moderato Allegro , seventh Con icerto Corelli 
 Andante Grazioso ... . « Dussek . 
 Andante , Concerto G minor Mendelssohn . 
 Andante , Op . 83 ... : tephen Heller . 
 “ Thou didst leave tee Handel 

 Book VI.—March , second S « t Sonatas ... Handel . 
 Prelude Gebhardi . 
 Andante molto nt : abile , ‘ Sonata , O ; 1 09 Beethoven . 
 Larghetto ‘ * eis 3 ; Weber . 
 Andante Sostenuto Mendelssohn 

 Book VII.—Trumpet Voluntary ... ie ee Purcell . 
 Duet , ‘ ‘ Vater unser ” ... xa ai : Spohr . 
 “ Ich grolle nicht , ” Song ... > ‘ Schumann . 
 Andante , twelfth S ymphony ide Mozart . 
 Minuet Trio , twelfth Symphony ... Mozart 

 Book X.—March , D major . Hummel . 
 Pie Jesu , Requiem Mass Cherubini . 
 Melody , F major ... Adolphe Nordmann . 
 Air , “ Ne men con l’ombri ” ( “ Xerxes " ) sae - Handel . 
 Andante Moderato , C major . ¥ , Lachner 

 Book XI.—Romanza , second 1 Concerto . Mozart . 
 Quintet , “ Giuri ognuno ’ ‘ .. Rossini . 
 Adagio Cantabile , Sonata Path : é ‘ tique | Beethoven . 
 Bourrée , D major ... - _ o. ( del 

 Book XII.—Chant de Berceau , Op . 81 : ahi Heller 
 Andante , G minor , Sonata ; . Hay oa 
 Adagio , 1p ) major , Sonata oii Pinto . 
 Traumerei , Lento , F major — es Schu imann 

 Handel 

 Haydn 
 Old German . 
 Beethoven . 
 Handel 

 La Malinconia 

 Fugue , C major 

 ap S. Bach . 
 Beethoven . 
 H. Jaeschke 

 Book XIX.—Arietta con Coro , ‘ ‘ Invano alcun desir Gluck , 
 Salutaris ... : ‘ Batiste . 
 Prelude , C major 3eethoven . 
 Andante , F major ... . Mozart . 
 Book XX . — — Organ Theme , flat . . A. Hesse . 
 heme , flat son F. Schubert 




 NEW FOREIGN PUBLIC CATION 


 AUG 


 ERAS 


 WAGNER OPER 


 DER RING DES NIBELUNGEN , 


 TRISTAN UND ISOLDE , 


 DIE MEISTERSINGER , 


 TANNHALUSER , 


 LOHENGRIN , 


 DER FLIEGENDE HOLLAENDER 


 SERI 


 XUN 


 _ — _ _ 


 295 


 CHEAP VOLUMES 


 AUGENER CO . EDITION 


 ORGAN MUSIC 


 ANDRE , J 


 NEW COMPOSITION 


 CAL PUBLICATIONS . 


 » YOCKSTRO ( W. S. ) P R ACTICAL HARMONY . 


 < CISES 


 JINSUTI ( C. ) DAILY ' V OCAL EXE 


 B. WILLIAMS THEORE TICAL WORKS 


 MAN 


 ALFRED MUL L E N ’ S EASY COMPLETE 


 


 PIANOFORTE DUET SOLO . SE LECT COMPOSITIONS ROM GREAT 


 PUBLISHED 


 XUM 


 M AJOR SCALES , 


 COMPILED USE PIANOFORTE STUDENTS 


 HENRY THOM . . 


 - SONG SPRING YEAR . 


 SPRING GUSTS 


 W. NRY THOMAS 


 UNCH : ANGING LOVE 


 FRANZ ABT 


 - SONGS 


 TREBLE VOICES 


 ECHOES . SCOTT 


 ARRANGED HARMONIUM AMERICAN ORGAN 


 


 CARL HELLER 


 NEW SONG ( SOPRANO CONTRALTO ) , 


 BETROTHAL RING 


 ( ‘ MIZPAH ” ) 


 


 SONGS 


 SOPRANO TENOR 


 COMPOSED 


 4 " AFT . F ; 


 REDUCED PRICE 


 DER RING DES NIBELUNGE 


 LETTERS BAYREI 


 JOSEPH BENNETT 


 WI 


 ORCHESTRAL SCORE 


 SIR W. STERNDALE BENNETI 


 QUEEN 


 DAILY STUDIES 


 COMPLET E PEDAL SCALZ 


 GEORGE LE RNEST LAKE 


 


 TH APPENDIX , 


 


 CANTATA 


 


 R ORGAN 


 ALBANY 


 GRANDE M 


 COMPOSED 


 ROYAL HI 


 


 GHNESS PRINCE 


 PIANOFORTE 


 LADYE 


 BARBARA GAVOTTE , 


 TREATISE 


 MODERN INSTRUMENTATION 


 MRCHESTRATION 


 


 HECTOR BERLIOZ 


 MARY COWDEN CLARKE 


 TRANSLATED 


 LEOPOLD FE 


 


 JOSE 


 PH BENNETT 


 _ — _ 


 MUSICAL TI 


 TT . " 


 WHITSUNTIDE 


 TRINITYTIDE . 


 ASCENSIONTIDE . 


 SALVAT R MUNDI 


 ; SACRED CANTATA | 


 MUSIC COMPOSED 


 TON 


 GREATLY REJOICE LORD 


 XUM 


 PUBLISHED 


 NOVELLO , EW ER CO . 


 PIANOFORTE ALBUMS 


 PRICE SHIL LING . 


 34 


 31 . 


 NOVELLO ORIGINAL OCTAVO EDITION . _ 


 CHORUSES 


 ORATORIOS , ¢ ANT ATAS 


 ESPECIALLY USE 


 CHORAL SOCIETIES 


 KCs . d 

 BACH PASSION ( ST . MATTHEW ) ... Io 
 BEETHOVEN MOUNT OLIVES ... o 6 
 BENNETT QUEEN - a6 
 GRAUN PASSION Io 
 HANDEL MESSIAH o 8 
 - JUDAS M ACCAB.EUS o 8 

 ea SAMSON oni o 8 




 HYMNARY 


 BOOK CHURCH SONG 


 FOLLOWING EDITIONS READY 


 LONDON : NOVELLO , E EWE R CO 


 BRISTOL TUNE - BOOK 


 SUPPLEMENT 1881 , CONTAINING 37 TUNES , 


 MATCH EDITION 


 BOOK PSALMS ( BIBLE VERSION ) , 


 SON 


 WIDOW NAIN 


 SACRED CANTATA 


 SOLI VOICES CHORUS 


 


 


 EVENING SERVICE 


 VOICES ORGAN 


 HENRY ‘ G 


 HYMN 


 WHITSUNTIDE 


 DU CE D SPE NCE . E “ ACE 


 A. MACFARREN - 


 LE 


 SP 


 ERRING METRICAL TUNES 


 ET HEART TROUBLED 


 LORD SHEPHERD . 


 OMNIA OPE RA 


 1 K 


 DIMITTIS 


 POPULAR ANTHEMS 


 


 J. MAUDE 


 XUN 


 CMULE DIRECTORIUM CHORI ANGLI 


 99 


 SECOND SERIE 


 ANGLICAN CHORAL SERVICE BOOK 


 CATHEDRAL PSALTER 


 CHANTS 


 N.E 


 ULE DIRECTORIUM CHORI ANGLI 


 ORDER HOL ' § COMMUNION . 


 2 V IL LAGE ORGA 


 NOVELLO COLLECTION 


 - 


 GEORGE C. MART 


 1B - ORGANIST ST , PAUL CATHEDRAL 


 ISRAE E “ RESTORED 


 B. VERR INDE ! R ORGAN N PIECES 


 MAGNIFICAT NUNC DIMITTIS , 


 POINTED CHANTING 


 REDUCED PRICES 


 OUSELEY MONK 


 POINTED PSALTER 


 HARROW SCHOOL MUSIC 


 JOHN FARMER | CC 


 ORATORIO . — “ CHRIST SOLDIERS . ” — 


 SINGING QUADRILLES 


 


 HARMONIUM [ TREASURY | 


 SERIES SELECT PIECES ARRANGED | 


 HARMONIUM VOLUNTARIES 


 ARRANGED 


 LONDON : NOVELLO , EWER CO 


 MUSICAL TI 


 EIGHTY - EDITION . 


 PRICE SHILLING , ENLARGED 


 COLLEGIATE SCHOOL 


 SIGHT - SINGING MANUAL 


 MANUAL SINGING 


 USE OP 


 CHOIRTRAINERS & SCHOOLMASTERS 


 


 RICHARD MANN . 


 REVISED EDITION , ADDITIONS 


 NEW 


 


 } COLLEGIATE ORGAN TUTOR | 


 THIRTEENTH EDITION . 


 CATHEDRAL CHANT - BOOK | 


 BOOKS 2 , 8 , & 4 , 


 COLLEGIATE SCHOOL 


 } SINGING MANUALS 


 ROUNDS , - SONGS , MADRIGALS , MOTETTS , 


 SUPPLEMENTAL 


 BOOK EXERCISES 


 USE 


 LEARNING SING SIGHT 


 HENRY GADSBY 


 SHILLING . 


 


 SCHOOL ROUND BOOK 


 COLLECTION 


 ROUNDS , CATCHES , CANONS 


 REV . J. POWELL METCALFE , M.A. 


 COL L E CTION 


 SLLECTED 


 DURANTE , HANDEL , LEO , SCARLATTI , STEFFANI , 


 SHILLING , 


 ORCHE STRA ‘ CHOIR . 


 MUSICAL OPINION MUSIC TRADE ‘ RI SW . 


 S. WORTH 


 TICKED MUSIC 


 302 


 MUSICAL TIMES 


 - SONGS 


 LADIES ’ BOYS ’ VOICES 


 ACCOMPANIMENT 


 PIANOFORT 


 WORDS 


 SINCLAIR DUNN 


 : : | SPRING WELCOME 


 303 


 CHORAL SOCIETIES 


 RECENTLY PUBLISHED EY 


 NOVELLO , EWER & CO 


 ORT 


 8 - SONGS 


 HEINRICH HOFMANN 


 1 YOUNG ORGANIST 


 COLLECTION PIECES MODERATE 


 1O | ™ ; DIFFICULTY 


 ARRANGED 


 W. J. WESTBROOK 


 HANDEL » 


 VS \ORGAN CONCERTOS 


 ESTR : ! EDITED , 


 W. T. BEST . 


 VOLUME , SEVEN SHILLINGS SIXPENCE . 


 SONGS SAILOR 


 READY 


 SCHUBERT SONGS 


 TED , EDITED D TRANSLATED 


 N ATALIA MACPARREN . 


 PRICE 


 


 SHILLING SIXPENCE 


 SONGS 


 


 MEZZO - SOPR : ANO VOICE 


 ACH 


 CONTRAL VOICE 


 H.R.H. DUKE EDINBURGH , K.G 


 BENN LETTE 


 L L. BATTON . 


 VOLUME , OCTAVO , CONTAINING 


 SONGS 


 PRICE SHILLINGS SIXPENCE 


 LONDON : NOVELLO , EWER CO 


 NOVELLO , EWER & CO 


 CIRCULATING MUSIC LIBRARY 


 TERMS SUBSCRIPTION . C 


 CLASS - - GUINEA - = - ANNUM " 


 CLASS B ) _ ~ - . GUINEAS - - ANNUM 


 VE 


 TOWN ' 


 ENG 


 XUM