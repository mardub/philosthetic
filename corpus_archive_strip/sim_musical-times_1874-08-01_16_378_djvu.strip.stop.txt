


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 PUBLISHED MONTH 


 AUGUST 1 , 1874 


 S ORGANIST & CHOIRMASTER.—WANTED 


 566 


 PROFESSIONAL NOTICES 


 MR . J. TILLEARD 


 W. HATTERSLEY 


 CELEBRATED ORGAN - TONED HARMONIUMS , 


 MANUFACTURED : 


 E & W.SNELL IMPROVED HARMONIUMS , AUTION.—Being London , finding thata 
 number imitations celebrated SHORT IRON 

 GRAND PIANOS offered public genuine Instr . 
 ments , think Musical Profession , Amateurs , 
 Trade , following statement : — years » alter 
 thought labour , succeeded completing 0 
 Grand , adopting cross - string principle Steinway , lessen 
 size , making improvements sound - board 
 action ; known musical world 
 originator Pianos . time 
 ditional improvements effected , sale 
 increased , present time 250 work 
 men engaged exclusively manufacture . acknowledged 
 superiority Pianofortes description 
 size caused closely imitated makers ; , 
 instance , “ Beethoven Grand , ” originally 
 distinctive trade mark , abandoned , adopt- 
 ed ; unexampled modesty , public inyited 
 accept copy original , consider genuine spurious , 
 numerous testimonials received ( including 
 medals ) , permitted mention Majesty Kinga 
 Saxony , takes great interest Art Manufactures , isin 
 addition excellent musician , honoured manufactory witha visit § 
 February 25th , pleased express entire satisfaction 
 Depét Pianofortes London Mr. Fravett , 4 , 
 North Audley Street , W. , seen great variety= 
 ERNST KAPS . Sole Importer — CuHarves Russet 

 C)RGANS pedals , Church Chamber , 
 35 guineas)s HARMONIUMS , organ tone , Chapels , 
 Schools , & c. , 6 guineas . ORGAN HARMONIUMS , 
 German pedals , 25 guineas . PIANOS , 7 octaves , 21 guineas . COT . 
 TAGE BIANO , 2 } octaves pedals , 30 guineas.—Wi 
 Sprague , 7 , Finsbury - pavement , London . Established 1837 




 USIC ENGRAVED , PRINTED , PUB- 


 RUSSELL MUSICAL INSTRUMENTS . 


 ECOND - HAND PIANOFORTES—150 


 FRO\RRY 


 FO 


 FO 


 FEEO| 


 CFS 


 BB 


 SF 


 567 


 STANDARD VOCAL UNIONOR SALE , ORGAN , Mahogany Case , gilt speak- 
 ing pipes , 2 manuals , coupler , 2 octaves pedals.—3 , Crescent- 
 tlore - road , Hoxton 

 place , 
 SALE tat great . sacrifice ) , Fine - toned 
 CHORUS ORGAN , 7 stops , good condition , suitable 
 erection Orchestra . , complete String Wind 
 principal Oratorios , Masses , & c. , great Composers , 
 complete parts Symphonies Haydn , Mozart , 
 Beethoven , & c. , 30 Vols . Overtures Orchestra , 
 anda large number Quartetts , Trios , & c.—Apply H. Millington , 
 Music Sa‘oon , Trowbridge 

 RGAN SALE , built Gray , converted intoa 
 ’ C Organ Jarpinez , Manchester , containing 12 stops ( 4 
 swell , 7 great organ , pedals . ) Apply Z. Armitage , 
 Northheld . Warrington 




 ADAME PATEY AUTUMN TOUR — 


 R. STANLEY MAYO CONCERT PARTY 


 WEIPPERT , BANKRUPT.—SALE MUSIC PLATES 


 COPYRIGHTS . 1 


 MUSIC MUSICAL INSTRUMENTS.—AUGUST SALE 


 568 


 SECOND 


 SERIES 


 


 CANTICLES HYMNS 


 


 POINTED CHANTING , SET 


 CHURCH 


 APPROPRIATE ANGLICAN CHANTS 


 SINGLE DOUBLE ; 


 


 EDITED 


 


 REV . SIR F. A. G. OUSELEY , BART . , ETC 


 


 EDWIN GEORGE MONK 


 READY . 


 IME TUNE ELEMENTARY 


 MANUAL SINGING 


 USE CHOIR TRAINERS & SCHOOLMASTERS 


 - SIXTH EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT SINGING MANUAL 


 APPENDIX 


 ( COMPANION WORK 


 COLLEGIATE VOCAL TUTOR 


 CATHEDRAL CHANT BOOK 


 PSALMODY CLASSES . BOOK 


 SUMMER ENDED , HARVEST 0 ’ ER 


 569 


 HARVEST ANTHEMS 


 S , 1S. 6 


 T , W. H.—THOU VISITEST EARTH 


 ACFARREN , G. A.—GOD SAID , BEHOLD , GIVEN 


 ATTISON , T. M © —O PLENTIFUL THY GOOD- 


 AYLOR , W.—THE EYES WAIT : THEE . 


 RIDGE , J. FREDERICK.—GIVE UNTO LORD 


 HYMNS TUNES 


 HARVEST 


 SELECTED 


 HYMNARY 


 PRICE PENNY . 


 SING LORD JOYFUL SONG BARNBY . 


 0 LORD J. 


 C. JEFFERYS , 67 , BERNERS ST 


 VERDI 


 LUISA MILLER 


 


 ITALIAN ENGLISH WORDS 


 WRITTEN ADAPTED REPRESENTATION 


 CHARLES JEFFERYS 


 UNIFORM 


 CHORAL SOCIETY 


 PRICE TWOPENCE 


 MICHAEL WOO 


 ELEMENTS MUSIC 


 SYSTEMATICALLY EXPLAINED 


 HENRY C. LUNN 


 ROYAL ACADEMY MUSIC 


 WILLIE PAPE 


 RECOLLECTIONS MUSIC BOX 


 IL TROVATORE 


 MASSA COLD GROUND 


 C. JEFFERYS & CO . ENGLISH MODEL HAR- 


 67 , BERNERS STREET , W 


 4 0 


 4 0 


 57 


 IRST - CLASS PIANOFORTE & HARMONIUM 


 RTICLED PUPIL.—MR . H. P. G. BROOKE , 


 NEW COMPOSITIONS 


 CIRO PINS TT 


 57 


 MUSICAL TIMES , 


 SINGING - CLASS CIRCULAR . 


 AUGUST 1 , 1874 


 MASSES FRANZ SCHUBERT . * Aruu accurate life Schubert 
 written , appears satisfy our- 
 selves ill - digested work Dr. Kreissle von 
 Hellborn , according master wrote 
 masses — named , ( . 5 , 
 flat ) , unpublished . catalogue 
 Schubert compositions mentions ‘ Deutche 
 Messe , ” written 1827 ; , , 
 mass , strictly speaking , ‘ ‘ German Requiem ” 
 Brahms Requiem , , follows , 
 , single exception , Schubert 
 wrote important solemnity 
 Church . amateur glad 
 works brought easy reach , , thanks 
 justice Time , Schubert occupies posi- 
 tion able command attention bear- 
 ing . compositions — 
 — equal merit , add 
 little fame , increase 
 knowledge man genius , 
 fail enrich treasures art . 
 amateurs , , familiar 
 Schubert Masses special - defined 
 reason hailing publication present 
 form . regard volumes , 
 isnot question revelation 
 Schubert , assumes importance , 
 increasing store classical , 
 time , popular music . Masses differ point 
 value character , , taking 
 altogether , , religious music , refined 
 noble , music se , healthy strong . 
 effort spread works broadcast 
 people deserves encouragement , well- 
 Meaning misdirected enthusiasm : 
 utmost establish false canons taste . pro- 
 paganda met ; point 
 scored right good music 
 accessible 

 estimating works , regard 
 time circumstances pro- 
 duction . point date range 1814 , 
 Schubert seventeen years old , 1828 , 
 months death ; covering , , nearly 
 active period master - short life . 
 , course , presumed reflect artistic 
 growth unformed , precocious genius 
 boy fully - developed , intellectual , 
 iMaginative power man ; toa 
 certain extent , fashion places , 
 outset , face face difficulty . Schu- 
 bert wrote Mass 1814 , Beethoven 
 lovely . 1 seven years published , 
 otra taste ecclesiastical music 

 aydn Mozart largely illustrated . un- 
 necessary describe Church compositions 
 masters , point extent 
 sought musical effect , independent ex- 
 Pression . genius writers perpetuated 
 favour works , ground fitness 

 rom score b 

 intended purpose , inadequate 
 claim , know intimately duty in- 
 cumbent amateur . Looking 
 fashion Haydn - Mozart Mass ex- 
 ample , influence great names 
 ambitious lad , entitled look 
 style Schubert early works . remarkable 
 fact , , Mass F ( . 1 ) shows 
 trace Haydn - Mozart influence , , 
 , largely characterises B flat 
 C , composed respectively , according Von Hell- 
 born , years later . Mass , point 
 fact , Schubert fascinating 
 individuality , inspired Beet- 
 hoven Mass C , emulates mingled 
 fervour chasteness religious style , 
 beauty grandeur effects . 
 reason , based points detail , 
 entitled ask Schubert biographers 
 mistake numbering works 
 — - called Mass 
 follow G , B flat , C , approach 
 near point time E flat , 
 emphatically called sister . Kreissle von 
 Hellborn , true , enters confidently par- 
 ticulars production Mass , 
 telling written centenary festi- 
 val parish church Lichtenthal , Schubert 
 conducted performance person , Mayseder 
 violin , , close Salieri embraced 
 composer saying , ‘ ‘ Franz , pupil , 
 great honour . ” , told 
 MS . hands Dr. Schneider , 
 bears date 1814 . weight given 
 powerful evidence , deserves notice 
 illustrating confusion uncertainty connected 
 life time thought 
 need historiographer , second performance 
 described Ferdinand Schubert re- 
 collected lady — Therese Grob — said 
 taken principal . bound , 
 , accept story Mass F told 
 Von Hellborn , internal evidence points 
 clearly direction confess 
 perplexed . Strange , altogether 
 variance rules governed Schubert 
 career , begin noble dignified 
 religious school Beethoven , passing 
 Haydn Mozart ; ending com- 
 menced . , 
 eccentric freak genius , wholly inexplicable 
 theory causation mind conceive 

 Taking Masses reference nume- 
 rical order — shéuld 
 subjected comparative criticism — arrange 
 groups , respectively 
 Nos . 2 , 3 , 4 , Nos . 1 6 . 
 supposed suggest division members 
 group family likeness equal 
 degree undoubtedly exists 
 second . arrangement , , 
 wholly arbitrary , , Mass G 
 ( . 2 ) better work , distinctive 
 master , companions , nearly 
 allied dimensions character , 
 far equal value relative 
 F E flat . According authority 
 named , Masses followed 
 closely , “ ‘ G ” “ ‘ B flat ” bearing date 1815 , 
 “ ‘ C ” 1816 . accepted , , 
 outcome phase , , com 




 573 


 574THE MUSICAL TIMES.—Auveusr 1 , 1874 

 fugue , elaborate fashion 
 find Mass . 1 . character , , 
 apart contrapuntal skill shown , affected 
 large use chromatic progressions , 
 general result strikes scholastic 
 pleasing . “ Credo ” gives foretaste 
 novelty - bar roll drums preludes 
 entrance voices . Beethoven shown 
 tympani , Schubert 
 betters instructions , impressive 
 effect . drum passage reappears , 
 important feature movement 
 interest . ‘ ‘ Et incarnatus , ” composer 
 resorts - loved canonic form , 
 success rarely , , surpassed . Canon , 
 written soprano tenor voices , 
 melody extreme beauty ; parts flow 
 smoothness , accompaniment enriches with- 
 encumbering . undoubtedly gem 
 “ Credo , ” subsequent passages 
 hearty admiration , esthetic scientific 
 grounds . “ Sanctus , ” peculiarly enterprising 
 progressions , compete “ Et 
 incarnatus ” charm , ‘ ‘ Benedictus ” 
 quartett chorus , run lovely movement 
 hard place . Mere verbal description 
 avails convey idea character ; 
 , , arouse curiosity speaking 
 strongest terms model religious music . 
 solemn “ Agnus ” marvellously beautiful 
 * Dona nobis ” worthy gone , 
 , closing volume obedience 
 exigencies space , express hope 
 soon grand Mass place 
 public esteem fairly deserves 

 word suffice recognise general 
 accuracy completeness edition , 
 state Masses ably 
 adapted Communion Service English 
 Church Rev. J. Troutbeck , M.A. , pub- 
 lished separate form 




 ORIGIN ANGLICAN CHANT . 


 575 


 FES 


 577 


 CRYSTAL PALACE 


 ROYAL ITALIAN OPERA 


 MAJESTY OPERA . guecess character Leonora , ‘ ‘ ) Trovatore 

 singing acting having highest order 
 merit ; added character 
 associated excellent 
 assumption Valentina , ‘ ‘ Les Huguenots ” 
 occasion benefit . Madlle . Titiens deserves 
 praise selecting character Leonora , 
 Beethoven ‘ ‘ Fidelio , ” benefit night ; 
 pleasure recording house filled 
 overflowing . end performance , called 
 stage , Madille . Titiens received perfect ovation , 
 addition usual floral offerings , gifts 
 costly description handed . 
 night season Saturday 18th ult . , 
 extra night , benefit Mr. Mapleson , given 
 following Monday , ‘ Don Giovanni ” 
 gg Madlle . Titiens Donna Anna , Madame 
 ilsson Donna Elvira course 
 desired ; Madlle . Singelli scarcely 
 ideal Zerlina , Herr Behrens somewhat heavy 
 Leporello . Signor De Reschi , , singing 
 music extremely , lacked vitality inseparable 
 Mozart Don Giovanni ; Signor Gillandi Don 
 Ottavio Mr. Perkins Commendatore thoroughly 
 Satisfactory performances 

 PHILHARMONIC SOCIETY 

 d , — overflowing — 
 charmingly melodious phrases , objection 

 want continuity idea arrests 
 attention , compels mind listener 
 follow composer development theme 
 constantly startled appearance new ones . 
 movement , decidedly pastoral 
 character , contains excellent points , second 
 subject , especially , exceedingly happy , 
 instrumentation showing composer 
 deeply studied orchestral effect . work 
 listened profound attention , elicited en- 
 thusiastic applause . Arthur Sullivan MS . Overture 
 ‘ ‘ Marmion ” — remodelled improved 
 performance Society — received , , 
 best works , , doubt , 
 occasionally heard concerts , originally 
 written Society . rendering Mendelssohn 
 Concerto G minor , Madame Essipoff , scarcely 
 surpassed delicacy touch , decision phrasing , 
 perfection execution ; occasionally missed 
 tenderness essential music composer , 
 linger old 
 associations , dismissed majority 
 audience belonging past school executive art . 
 eighth , final concert season , 13th 
 ult . , contained programme - worn works , con- 
 cluding , usual , Weber “ Jubilee Overture . ” 
 Beethoven Concerto G scarcely received justice 
 M. Saint - Saens , small impression 
 audience ; Stradella tiresome Can- 
 tata , “ Il Nerone , ’’ sung Mr. Santley , 
 scored occasion Sir Michael Costa , 
 tamely received . Mr. W. G. Cusins conducted 
 concerts notice usual ability , received , 
 deserved , warmest applause 

 ROYAL ACADEMY MUSIC 

 annual public concert Institution given 
 Hanover Square Rooms Saturday , 25th ult . , 
 able conductorship Mr. Walter Macfarren . 
 anexhibition progress pupils national 
 establishment , executive creative depart- 
 ments art , performance 
 thoroughly satisfactory previous Academy 
 concert ; interest evinced occasion 
 sufficiently proved fact room densely 
 packed persons compelled stand . 
 remarkable compositions students , 
 special commendation given selection 
 Motett Oliveria Prescott — Mr. Walter Fitton 
 played organ , Miss Jessie Jones sang 
 soprano solo — Andante Symphony B 
 minor , Florence Marshall , evidence 
 possession musical feeling knowledge 
 effect , originality thought , 
 carefully directed , place composers head 
 small list ladies created impor- 
 tant branch art . Mr. A. H. Jackson Overture , 
 “ ‘ Dans les bois , ” highly creditable work , Mr. 
 Corder sacred song , ‘ “ ‘ shall ascend , ” ( sung 
 Miss Marian Williams ) considerable merit . 
 pianoforte playing , usual conspicuous feature 
 selection , average talent represented 
 high class . Miss McCarty Mendelssohn Rondo 
 B minor , Mr. Eaton Faning , Rondo Sir Julius 
 Benedict Concerto E flat , Miss Martinin move- 
 ments Beethoven “ * Emperor ” Concerto , Miss Troup 
 movements Mendelssohn Concerto . 
 D minor , Miss Whitaker movement Sir 
 Sterndale Bennett Concerto F minor , Miss Ludovici 
 Pianoforte Hummel Septet D minor 
 ( movement ) , fully sustained reputation 
 Institution , reflected utmost credit them- 
 selves teachers ; mention 
 intelligent — somewhat affected — performance 
 Chopin pieces Mr. Bouténof . rendering 

 Sainton Violin Concertoin , Madlle . Gabrielle Vaillant 
 elicited , deserved , storm applause ; Mr. 
 Palmer performance movement Spohr 

 MALE DEPARTMENT.—Silver Medal : Mr. George Palmer 
 ( Violin ) . Bronze Medals : Messrs. William W. Bampfylde , 
 Eugene W. Bouténof , Joseph A. Breeden , Arthur H. 
 Jackson , Charlton Speer , Dudley Thomas . Prize Violin 
 Bow ( kindly given Institution Mr. James Tubbs , 
 Wardour Street ): Mr. Ladislas Szczepanowski . Books : 
 Messrs. Haydon Aldersey , Arthur Jackson , Alexander G. 
 Jopp , Henry W. Little , Thomas Silver . Sterndale Bennett 
 Scholarship ( Years ’ Free Education Institution ) , 
 awarded Master Charlton Speer . Potter Exhibition 
 ( Pounds cost Years ’ Instruction ) , 
 awarded Mr Walter Fitton . Examiners , 
 Composition Harmony — Principal ( Sir Sterndale 
 Bennett ) , Mr. H.C. Lunn , Dr. C. Steggall . Pianoforte 
 — Mr. W. G. Cusins , Mr. W. Dorrell , Mr H. R. Eyers , Mr. 
 F. B. Jewson , Mr. Walter Macfarren , Mr. Arthur O’Leary , 
 Mr. Westlake . Singing — Mr. F. R. Cox , Signor 
 Ettore Fiori , Signor Garcia , Signor Gilardoni , Signor 
 A. Randegger . Orchestral Instruments — Mr. F. R. Folkes , 
 Mr. H. Weist Hill , Mr. Walter Pettit , M. Sainton , 
 Mr. Watson . Organ — Sir John Goss Mr. G. A. 
 Macfarren 

 23rd ult . , organ St. George Wesleyan 
 Chapel , St. George's - - - East , - opened , 
 rebuilt considerably enlarged . Organ performances 
 given Mr. J. Grout Saints ’ , Poplar , Mr J. 
 Young , St. Ann , Limehouse , Mr. J. S. Nimkey , 
 chapel organist . music performed , 
 mentioned , particularly worthy praise , Wely Offer- 
 toire G , Mr. Grout ; Mendelssohn 4th Organ Sonata , 
 Mr. Young ; Beethoven “ Hallelujah 
 Father , ” Mr. Nimkey . choir assisted 
 anthems , & c 

 vocalists engaged forthcoming 
 Liverpool Festival Madame Adelina Patti , Mdlle 

 Herr Conrad Behrens , new German bass . 

 Festival wiil commence Phiiharmonic Hall . oj 
 Tuesday , 29th September , performance : - 
 Mendelssohn ‘ St. Paul’’—a work Liverpool 
 honour introducing country , 4 
 miscellaneous concert place evening , 
 Mr. G. A. Macfarren new Overture , special } 
 composed Festival , Mendelssohn Italian Sym . 
 phony , Overture Wagner ‘ Tannhauser , ” 
 played . second parts Haydn 
 ‘ “ Creation ; ’’ selections Handel ‘ “ Messiah , ” 
 “ Israel Egypt , ” & c. ; Gounod new Mass , “ Angeli 
 Custodes , ’’ Cantata , “ Joan Arc ; ” Beethoven 
 Choral Symphony , . g , Overture Rossini 
 “ William Tell , ” performed chief features 
 morning evening concerts Wednesday 
 30th . M. Gounod direct Mass Cantata , 
 Mrs. Weldon assist performance 
 . Thursday morning , October 1 , Mr. Arthur 
 Sullivan Oratorio , ‘ * Light World , ” 
 given time Liverpool , composer con- 
 ducting performance . programme evening 
 concert day comprise Mozart Jupiter Sym . 
 phony , pianoforte Concerto Chopin , Mr. J. F , 
 Barnett ‘ ‘ Suite de Pieces ” orchestra , entitled * * 
 Lay Minstrel , ” specially com- 
 posed Festival . Mr. Barnett direct execu- 
 tion work . date production Sir Julius 
 Benedict promised Symphony decided 
 . choir , freely selected , accordin 
 accounts , finest heard Liverpool , 
 orchestra consist executants , 
 - half metropolitan , 
 provincial 

 Mr. ALFRED GILBERT , retirement office 
 organist St. Mark Church , Hamilton Terrace , 
 presented members Choir 
 elegant time piece , bearing suitable inscription , 
 acknowledgment services connection 
 music Church space thirteen years 




 SONG 


 2 AY 


 73 


 69 


 PP 


 NT ! 


 LAKE WATERFALL 


 1 J @. 


 TN 


 LAKE WATERFALL 


 LAKE WATERFALL 


 2 - 1 


 RO 


 584 


 REVIEWS 


 585 


 586 


 LAMBORN COCK 


 ORIGINAL CORRESPONDENCE 


 OFFERTORY ORGANISTS . 


 EDITOR MUSICAL TIMES . 


 588 


 DR . DYKES ANTHEM , “ LORD 


 SHEPHERD 


 EDITOR MUSICAL TIMES 


 _ _ _ _ 


 


 589 


 REVIEW 


 DR . VERRINDER RUSSIAN HYMN . 


 EDITOR MUSICAL TIMES . 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWS 


 59 


 MONTH . 


 OVELLO OCTAVO ANTHEMS 


 SELECTION 


 HANDEL SACRED SECULAR WORKS , 


 PERFORMED 


 FIFTH TRIENNIAL HANDEL FESTIVAL 


 CRYSTAL PALACE , JUNE 24 , 1874 . 


 PRINTED ORDER PERFORMANCE 


 CONTENTS . 


 I.—SACRED 


 II.—SECULAR . 


 SHILLING SIXPENCE . 


 WILLIAM J. YOUNG 


 NEW EDITION 


 W. W. PEARSON 


 FAMOUS - SONGS 


 ARPER SCHOOL TRUMPET 


 IE 


 591 


 ANGLICAN HYMN - BOOK 


 NEW EDITION , REVISED ENLARGED 


 ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 JOULE S COLLECTION WORDS 


 PSALTER , PROPER PSALMS , HYMNS 


 “ JOULE DIRECTORIUM CHORI 


 ANGLI 


 OULE DIRECTORIUM CHORI ANGLI- 


 ORDER 


 HOLY COMMUNION . 


 ADDITIONS 


 EV . T. HELMORE PLAIN SONG WORKS . 


 SACRED MUSIC EDWARD LAWRANCE , 


 SON GOD GOES FORTH WAR . 


 VILLAGE ORGANIST 


 WESTMINSTER ABBEY CHANT BOOK , 


 SOCIETY PROMOTING CHRISTIAN KNOWLEDGE . 


 A. MACFARREN - ANTHEMS 


 USE SUNDAY SCHOOLS . 


 MASSES 


 FRANZ SCHUBERT 


 NOVELLO OCTAVO EDITION , 


 COMMUNION SERVICES 


 


 FRANZ SCHUBERT 


 NOVELLO OCTAVO EDITION , 


 MARCH FRIENDSHIP , 


 GOOD NIGHT . : 


 


 B35 


 MENT 


 REV . R. BROWN - BORTHWICK 


 SERIES 


 MODERN KYRIES 


 HYMN TUNES , 


 CHANTS , ETC 


 


 READY . 


 NOVELLO , EWER CO . 


 COMPLETE 


 CATALOGUES 


 CONTAINING LATEST PUBLICATIONS 


 ORGAN , HARMONIUM 


 SACRED MUSIC 


 


 MUSIC ORCHESTRAL 


 VOCAL PARTS 


 


 THEORETICAL WORKS 


 SONGS DUETS 


 TRIOS , GLEES , MADRIGALS , 


 SACRED MUSIC , LATIN 


 WORDS 


 LONDON : 


 NOVELLO , EWER CO . , 


 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C. ) 


 NEW YORK : J. L. PETERS , 599 , BROADWAY 


 NOVELLO ' OCTAVO EDITION 


 GENERAL HISTORY 


 


 SCIENCE PRACTICE MUSIC , 


 SIR JOHN HAWKINS 


 J. S. BACH 


 48 PRELUDES FUGUES 


 MAJOR MINOR KEYS 


 ( - TEMPERED CLAVICHORD 


 EDITED COLLATED EDITIONS 


 WORK , 


 W. T. BEST 


 NOVELLO 


 SHORT MELODIES 


 ORGAN 


 VINCENT NOVELLO . 


 


 CATHEDRAL PSALTER 


 POINTED CHANTING 


 _ 


 JAMES TURLE 


 


 JOSEPH BARNBY 


 PUBLISHED SANCTION 


 


 SONATAS 


 NEW COMPLETE EDITION . ) 


 EDITED FINGERED 


 LONDON : 


 NOVELLO , EWER CO . , 


 1 , BERNERS STREET ( W. ) , 35 , POULTRY 0NEW YORK : J. L. PETERS , 599 , BROADWAY 

 BEETHOVENS 

 HII 




 TYE 


 PRICE GUINEA , 3 


 RF 


 _ EFOR 


 595 


 OCTAVO EDITIONS 


 SERVICES B 


 Y MODERN COMPOSERS 


 PUBLISHED NOVELLO , EWER CO 


 T. BEST.—A MORNING , COMMUNION , 


 BAPTISTE CALKIN.—MORNING , COMMU- 


 D * OHN B. DYKES.—A MORNING , COM 


 R. G. M. GARRETT.—A MORNING , COM 


 R. G. M. GARRETT.—A MORNING , COM- 


 SF JOHN GOSS.—_TE DEUM LAUDAMUS 


 ALTEK MACFARREN.—A SIMPLE MORN- 


 AMUEL PORTER.—A MORNING , COM 


 HUBERT H. PARRY.—A MORNING , COM- 


 AMUEL REAY.—A MORNING , COMMUNION 


 ENRY SMART . — MORNING , COMMU- 


 R. CHARLES STEGGALL . — MORNING , 


 IR R. P. STEWART.—A MORNING , COM- 


 H. THORNE.—A MORNING , COMMUNION 


 ERTHOLD TOURS.—A MORNING , COM- 


 HOMAS TALLIS TRIMNELL . — CHANT 


 R. S. S. WESLEY.—A SHORT CATHE 


 CHAPPELL & CO . , 504 , NEW BOND STREET , SOLE AGENTS 


 NEW WORK AMATEUR ORGANISTS . ( 1 


 CHAPPELL & CO . , 5 , NEW BOND STREET , LONDON