


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 PUBLISHED FI 


 RST MONTH 


 JUNE 1 , 1877 


 MESSRS 


 NOVELLO , EWER CO 


 MESSRS . DITSON & CO 


 ORDERS STATE 


 NOVELLO EDITIONS 


 GREAT TRIENNIAL 


 HANDEL FESTIVAL , 


 CRYSTAL PALACE , 


 1877 


 MONDAY , JUNE 25 . 


 MESSIAH 


 WEDNESDAY , JUNE 27 . 


 SELECTION 


 FRIDAY , JUNE 29 . 


 ISRAEL EGYPT . 


 258 


 PROFESSIONAL NOTICES 


 ALBERT E. BISHOP 


 MR . CH . J. BISHENDEN 


 ASSISTANT ORGANIST 


 DEPARTMENT EVENING CLASSES 


 RINITY COLLEGE HARMONY CLASS . — 


 RINITY COLLEGE COUNTERPOINT CLASS . 


 RINITY COLLEGE CLASS MUSICAL 


 RINITY COLLEGE CLASSES SOLO 


 RINITY COLLEGE , LONDON.—A HAR 


 T’RINITY COLLEGE , LONDON.—MAN 


 RINITY COLLEGE , LONDON.—HONORARY 


 USICAL EXAMINATIONS.—CANDIDATES 


 LADIES ’ SCHOOL , KETTERING , 


 NORTHAMPTONSHIRE 


 O AMATEUR COMPOSERS.—MSS . REVISED 


 EMINENT ARTISTS , 


 ESTABLISHED , APRIL 1866 . 


 ENGLISH GLEE UNION . 


 ASSISTED 


 O BOOKSELLERS ’ ASSISTANTS 


 N.W. 


 ORGAN - TONED HARMONIUMS . 


 W. HATTERSLEY & CO . 


 CELEBRATED 


 ORGAN - TONED HARMONIUMS 


 UNEQUALLED MAKERS 


 QUALITY TONE , DURABILITY , RAPIDITY TOUCH , 


 PREDOMINANCE TREBLE BASS , 


 PERFECT ACTION , 


 BRILLIANCY SOLO STOPS , NEWLY INVENTED 


 DOUBLE ACTION , SOLID IMPROVED SOUND - BOARD , 


 DOUBLE EFFECTIVE SWELLS 


 W. HATTERSLEY & CO . , 


 3 . P. M. SLATER . 


 LONDON AGENTS 


 MESSRS . MOUTRIE SON , 


 PIANOFORTE SALOON , 


 55 , BAKER STREET , LONDON , W 


 TESTIMONIAL . 


 “ ALFRED J. WHITEHOUSE , 


 CENTENNIAL EXHIBITION 


 NOTICE . 


 NEW YORK 


 STANDARD ” 


 AMERICAN ORGANS 


 PRICES , 12 125 GUINEAS 


 BARNETT SAMUEL & SON , 


 31 , HOUNDSDITCH , E.C. , & 32 , WORSHIP STREET , 


 FINSBURY SQUARE , 


 E. & W. SNELL 


 IMPROVED ENGLISH HARMONIUMS 


 REED ORGANS 


 BEST CHEAPEST EXTANT 


 TOWN , LONDON , N.W. 


 HIGHEST CLASS EXCELLENCE 


 MILLEREAU BAND INSTRUMENTS 


 ARTISTS 


 GUSTAVE MODEL LIBRARY REED 


 EBAIN HARMONIUMS , ORGANO 


 RGAN METAL PIPES.—ORGAN METAL 


 TUTTGART HARMONIUM COMPANY . — 


 RUSSELL MUSICAL INSTRUMENTS . : 


 VERTICAL IRON FRAME PIANOFORTES 


 STRUNG SINGLE CASTING 


 FLAVELL 


 ALPHONSE CARY , 


 USIC ENGRAVED , PRINTED , PUB- 


 N.W. 


 COMPLETE EDITION 


 MENDELSSOHN 


 LIEDER OHNE WORTE . 


 EDITION CONTAINING Lonpon : NOvELLO , EWER Co 

 BEETHOVEN SONATAS 

 NEW COMPLETE EDITION 




 AGNES ZIMMERMANN 


 MOZART SONATAS 


 NEW COMPLETE EDITION 


 EDITED FINGERED 


 AGNES ZIMMERMANN 


 OCTAVO EDITION PREPARATION 


 DECEASED 


 PIANOFORTES D’ALMAINE 


 GREAT SALE 


 588 REMAINING PIANOFORTES 


 EXCELLENT FIRM 


 IMMENSE STOCK 


 DESCRIPTIONS 


 MUSICAL PROPERTY , PIANOFORTES , 


 HARMONIUMS , SHEET MUSIC 


 780 PIANOFORTES 


 EXCEPTIONAL OPPORTUNITY 


 OFFER ADVANTAGE PUBLIC 


 25 ” ” 46 5 ” 


 45 ” ” 85 ” ” 


 FAMED STEEL WREST PLANK PLATE 


 ELEGANT DESIGN , REFINED FINISH 


 PARTICULARS , VIEW , 


 5 , FINSBURY PAVEMENT 


 LETTERS PROMPTLY ATTENDED 


 264 


 USICAL INTERVALS & TEMPERAMENT : 


 ELEMENTARY TREATISE . 


 STUDENT TEXT - BOOK 


 


 SCIENCE MUSIC 


 LETTERS BAYREUTH 


 DESCRIPTIVE CRITICAL 


 WAGNER 


 “ DER RING DES NIBELUNGEN 


 APPENDIX 


 JOSEPH BENNETT 


 WAGNER OPERAS . 


 NOVELLO OCTAVO EDITION 


 LOHENGRIN 


 GLUCK OPERAS . 


 NOVELLO OCTAVO EDITION 


 MUSICAL TIMES 


 SCALE TERMS ADVERTISEMENTS . 


 READY , JULY 1 . 


 


 CATHEDRAL PSALTER 


 CHANTS 


 OBLONG QUARTO , PRICE SHILLINGS 


 LONDON : NOVELLO , EWER CO 


 QUEEN 


 PASTORAL 


 WORDS WRITTEN 


 HENRY F. CHORLEY 


 MUSIC COMPOSED 


 LONDON : NOVELLO , EWER CO . 


 NOVELLO OCTAVO EDITION 


 SONGS 


 


 ENGLISH GERMAN WORDS 


 COMPOSED 


 LONDON : NOVELLO , EWER CO 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 JUNE 1 , 1877 


 SKETCH HISTORY MUSIC- 


 PRINTING , FIFTEENTH 


 NINETEENTH CENTURY 


 PERIOD : XYLOGRAPHY 


 267 


 ENGLISH OPERA 


 1853 


 MUSICAL DEGREES 


 PATRONAGE 


 273 


 REINHOLDS , SINGERS 


 CENTURY . 


 LATE DR . RIMBAULT 


 274 MUSICAL T 


 MAJESTY OPERA 


 ROYAL ITALIAN OPERA 


 SACRED HARMONIC SOCIETY 


 276 


 WAGNER FESTIVAL 


 HERR RUBINSTEIN RECITALSInstead giving dry catalogue music per- 
 formed Recitals month best 
 word noteworthy items 
 programme 

 Recital ( April 30 ) perfect pieces 
 playing , foremost , Liszt transcription 
 Schubert “ Erl - K6nig , ” rendered astonishing 
 technical perfection , fire depth expression 
 unsurpassable ; Schumann “ Etudes 
 Symphoniques ” C sharp minor , magnificently 
 performed . Ina different style , Mozart charming Rondo 
 minor , Nocturnes Field Chopin , 
 perfect grace tenderness . hand , 
 Chopin great Polonaise flat given fury , 
 | ferocity , rendered mere caricature . 
 |It wonderful display execution , im- 
 | possible agree reading . 
 | second Recital ( gth ult . ) Beethoven ‘ ‘ Sonata 
 | Appassionata ’ ” ’ ( Op . 57 ) , Schumann “ Carnaval , ” 
 | large selection Chopin chief works 
 ' performed , especially suited 
 | great pianist style . Haydn Variations F minor , 
 | Mendelssohn * * Lieder ohne Worte ’ ” ’ exqui- 
 | sitely given 

 Recital ( evening 14th ) comprised 
 Weber flat Sonata , work interpretation 
 pianist probably approaches Rubinstein ; 
 Schumann great Fantasia C ( Op . 17 ) , Chopin Sonata 
 B flat minor , selection small pieces Rubin- 
 stein ; fourth performance ( 16th ) 
 | chiefly remarkable fine rendering 
 | Beethoven Sonata ( Op . ) , Schubert “ * Wanderer ” 
 Fantasia ( Op . 15 

 evening concert ( 25th ) fifth Recital 
 ( 28th ) taking place going press , 
 record fact . Recital announced 
 - morrow , 2nd inst 




 PHILHARMONIC SOCIETY 


 MR . HENRY LESLIE CHOIR 


 ALEXANDRA PALACE 


 FESTIVAL SONS CLERGY 


 LONDON GREGORIAN CHORAL ASSOCIATION 


 CAMBRIDGE UNIVERSITY MUSICAL SOCIETY . evening Wednesday 16th ult . highly 
 interesting service held St. Mary Church , 
 Haggerston : Mr. C. J. Frost , Mus . B. , Cam . , directed 
 music presided organ . 
 important parts service consisted Smart well- 
 known elaborate service F , telling Festival 
 Anthem C. J. Frost , Goss ‘ Praise ye 
 Lord , ” rendered commendable pre- 
 cision taste choir . conclusion 
 service Mr. Frost gave organ recital , selection 
 including Sonata Mendelssohn Fugue Bach , 
 performer Sonata flat major Offertory 
 G minor , playing evidencing skill artistic 
 feeling . Mr. Grizelle , formed addition 
 choir , sang Sullivan ‘ ‘ Come , ye children , ” Mendels- 
 sohn “ shall righteous , ” considerable 
 taste , successfully rendered tenor solo 
 Mr. Frost anthem 

 Miss AGNES ZIMMERMANN Concert St. James Hall 
 3rd ult . remarkable , usual , absence 
 attempt seize opportunity exhibiting 
 powers composer ; , exception 
 clever Suite pianoforte , violin , violoncello , work 
 facile pen contained programme . 
 Schumann Fantasia , Op . 17 , Miss Zimmermann refined 
 style artistic feeling effectively displayed , 
 pianoforte Brahms Trio , 
 violin horn ( ably played Messrs. 
 Straus Wendland respectively ) scarcely 
 sympathise . Beethoven Trio B flat ( Op . 97 ) , 
 pianoforte , violin , violoncello , finely rendered 
 Miss Zimmermann , Messrs. Straus Daubert ; 
 singing Mdlle . Redeker , encored song 
 Klengel , completed best classical concerts 
 season 

 TuE admirable series Concerts Chamber 
 Music , direction Mr. J. S. Shedlock 
 Herr Polonaski , given Rooms Allen Street , 
 Kensington , 23rd ult . , pro- 
 gramme consisting chiefly works Bach ; 
 second devoted Handel . instrumental 
 portion sustained Mdlle . Tesche , Messrs. Shedlock , 
 Polonaski , Henri Lutgen , Amor , Bailey , Hann , Trust , 

 Rendall , artistic performances thoroughly 
 appreciated . vocalists Miss Mary Davies , 
 Mdile . Rosa , Mr. Stedman , Mr. George Fox , prominent 
 solos “ heart , faithful ’ ( Bach ) , 
 Miss Mary Davies ; ‘ ‘ Where’er walk ” ( Handel ) , Mr. 
 Stedman ; “ ‘ O ruddier cherry ” ( Handel ) , Mr. 
 George Fox . Mr. E. H. Birch , Mr. George Hooper 
 conducted 

 programme meeting 
 Choirs , Gloucester , unusually interesting . 
 choral service day , “ Elijah ” 
 given ; second day Bach “ Passion ” ( St. 
 Matthew ) Beethoven “ ‘ Engedi ” morning , 
 selections ‘ Creation ” “ St. Paul ” 
 evening ; morning Brahms “ Requiem , ” 
 Wesley ‘ ‘ Wilderness , ” ‘ Lobgesang ; ” ’ 
 morning ‘ ‘ Messiah . ” Concerts 
 Shire Hall place evenings , 
 , pieces , Gade “ Crusaders ” 
 Schumann “ Paradise Peri ” performed . 
 vocalists engaged Mdlle . Titiens , Miss Léwe , 
 Miss B. Griffith , Madame Patey , Messrs. E. Lloyd , 
 Cummings , Maybrick , Santley . Festival 
 commence 4th September 

 2nd ult . Miss Ellen Horne gave concert St. 
 James Hall , assisted vocal depart- 
 ment Miss Annie Butterworth , Madame Poole , Miss 
 Kate Baxter , Mr. H. Guy , Mr. E. Lloyd , Mr. Chaplin 
 Henry , Mr. A. Caink Mr. Thurley Beale ; Malle . 
 Cecilia Brousil ( violin ) , band Royal Horse 
 Guards . singing “ Cantinier ” ( Balfe ) 
 concert - giver , ‘ ‘ Bailift Daughter ” Miss Kate 
 Baxter , “ Luna , veil thy light ” ( C. E. Tinney ) Mr. H. 
 Guy , “ sing thee songs Araby ” ( F. Clay ) Mr. E. 
 Lloyd , ‘ ‘ Largo al factotum ” Mr. A. Caink , “ 
 fear foe ” ( Pinsuti ) Mr. Chaplin Henry , received 
 enthusiasm , encores awarded . 
 Sir J. Benedict , Mr. A. Gilbert , Mr. H. Parker , Mr. C. E. 
 Tinney Mr. T. G. B. Halley Conductors 

 Mrs. Florence Saunders best points 
 evening careful playing Liszt transcription 
 “ Prophéte . ” ” success 
 excellent conducting Mr. E. H. Birch 

 Concert aid Royal Kent Dispensary 
 given New Cross Public Hall Thursday , 3rd 
 ult . vocalists Miss Matilda Roby , 
 deservedly encored songs , Miss Webb , Mr. G. 
 F. Jefferys , Mr. H. E. Milner , Madame Anna Jewell , 
 created favourable impression ad- 
 mirable singing Schubert ‘ Marie ” ( encored ) 
 Arditi Valse . Mr. W. A. Adam played flute solo , 
 Mr. Harry Brett solo euphonium . gem 
 evening Beethoven Trio pianoforte , clarinet , 
 violoncello , played Mrs. Harry Brett , Mr. George 
 Webb , Mr. T. Serjeant 

 Wednesday evening 16th ult . Oratorio 
 “ Elijah ” given Downs Chapel , Hackney , 
 direction Mr. E. J. Wallis , conducted . 
 soloists Madame Clara West , Madame Poole , Mr. 
 ' Stedman , Mr. George Fox , acquitted 
 highly efficient manner . choir 
 showed result excellent training , choruses 
 given precision . accompanists , Mr. 
 Henry Parker piano , Mr. R. Hainworth 
 harmonium , contributed success 
 evening 

 sixth Concert Mozart Beethoven Society 
 took place 16th ult . , devoted 
 entirely compositions masters . second 
 consisted miscellaneous works modern com- 
 posers . artists Mdlle . Nellini , Madame Elma , 
 Mdlle . F. Rocca , Madame A. Roche , Mr. E. E. Granville , 
 Mr. H. Pyatt ( vocalists ) ; Miss Lillie Albrecht , Herr Max 
 Laistner , Herr Hause ( piano ) ; Herr Otto Booth 
 ( violin ) , Herr Schuberth ( violoncello ) , Mr. H. A. Chapman 
 ( flute ) , Madame Sievers ( harmonium ) . Herr Schu- 
 berth conducted 

 CLEVER little comedy , Mr. Burnand , entitled 
 “ . 204 , ” graceful music written Mr. 
 German Reed , produced success 
 Mr. Mrs. German Reed entertainment . exceed- 
 ingly sung acted , Misses Holland 
 Braham , Messrs. A. Reed Law . programme 
 contains new sketch Mr. Corney Grain , illustrative 
 troubles experienced young married couple 
 dinner - party , enables artist exhibit 
 powers imitation , command pianoforte . Mr. 
 George Gear efficient accompanist 




 — — - SSS 


 IU | 


 = = = 2 SS PSS SS SS 


 42S SSS SS SS = SS SS EEE 


 = 5 : F 


 = < SSS = SSS SS 


 G — < — — — — = > E 


 2S : S2= ( 352 SSS SS SS 


 = SS SSS = 2 


 4 . + J 


 F = FI F 


 — /WL 


 + “ TN 


 URCHINS ’ DANCE . 


 TTT 


 = L. J 4 4 4 . 


 289 


 REVIEWS 


 _ @_»@—»—___*- 


 291 


 292 MUSICAL TI 


 FOREIGN NOTESBrazil , monarch takes lively intelligent in- 
 terest phases modern artistic strivings 
 development 

 cyclus concerts completed 
 Gewandhaus Leipzig - sym- 
 phonic works , - overtures , nineteen concertos 
 instruments , choral works orchestral 
 accompaniment , number vocal instru- 
 mental solos , produced , emanating 
 pen contemporary composers , fact speaks 
 activity displayed excellent institution . 
 programme year , included series 
 historical concerts , departed season , 
 interesting experiment repeated . 
 Stern’sche Gesangvercin Berlin — institution 
 masterly direction Herr Julius Stockhausen 
 occupies eminent position musical life 
 Prussian capital — lately given excellent per- 
 formances Beethoven . Missa Solemnis . curious 
 instance recorded Allgemeine Deutsche Musik- 
 Zeitung , conductor versatile talent , 
 energetic application office leader — , 
 properly filling post , ought represent , time 
 , personification work bdton 
 directs . appears occasion repre- 
 sentation Mass , Herr Ernst , tenor 
 solos allotted , failed appearance 
 ‘ ‘ Gloria ; ” Herr Stockhausen 
 directed difficult work , supplied 
 conductor desk absent tenor . hardly 
 necessary add public sparing 
 hearty acknowledgments double peformance 

 great gathering musicians members 
 German Allgemeine Musik - Verein took place Hanover , 
 roth 24th ult . , patronage 
 Emperor Germany . series concerts 
 given occasion , including follow- 
 ing works : Liszt “ ‘ St. Elizabeth ; ” Berlioz ‘ “ Sinfonie 
 fantastique ; ’’ organ compositions Bach , Matthison , 
 Hansen , Ritter ; instrumental soli Saint- 
 Saéns , Raff , Liszt , . operatic repre- 
 sentations selected occasion instanced 
 Byron drama “ Manfred ” Schumann music ; “ Jery 
 und Bately ” ( Goethe text ) , Ingeborg von Bronsart ; 
 opera Peter Cornelius , entitled ‘ ‘ Der Barbier 
 von Bagdad . ” number excellent artists 
 executants 

 Music Festivals held present summer 
 Salzburg Breslau ; - mentioned town 
 scenes Gluck ‘ ‘ Armida ”   Wagner 
 ‘ ‘ Gétterdammerung ” ’ form interesting features 
 programme . idea Salzburg Festival emanates 
 International Mozart Institution , greater 
 performances , expected , consist ot 
 works master 

 Court Theatre Dresden Beethoven “ Fidelio ” 
 day performed hundredth time , show- 
 ing average performances year 
 representation 1814 Vienna 

 Herr Tichatschek , veteran tenor dramatic artist , 
 years associated Royal Opera Dres- 
 den , shortly celebrate seventieth birthday , 
 occasion testimonial presented 
 numerous friends . , like writer , heard 
 years ago Max “ Freischiitz ” 
 Tannhduser Richard Wagner Opera , 
 testify fact remarkable 
 instances retaining relatively advanced age pos- 
 session exceptional vocal dramatic powers . 
 interpret character Tannhduser , , 
 modern phrase goes , “ ‘ created ” 




 MUSICAL DEGREES CAMBRIDGE . 


 S. G. PHEAR 


 SEDLEY TAYLOR 


 , W. SPRATT 


 R. PENDLEBURY 


 G. A. MACFARREN . 


 CORRESPONDENCE 


 TUNE “ ST . MARY . ” 


 EDITOR ‘ * * MUSICAL TIMES 


 CUCKOO . 


 EDITOR ‘ “ ‘ MUSICAL TIMES 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWSBe.rast.—The fifth Concert season Philhar- 
 monic Society took place Ulster Hall April 27 , 
 Mendelssohn Oratorio St. Paul performed orchestral 
 organ accompaniments . performance successful . 
 soloists Miss Essie Lynar , Dublin ; Miss Emily Holden , 
 Belfast ; Mr. Walter Bapty , Dublin ; Mr. Gordon Gooch ( R.A.M. ) , 
 London . band chorus consisted 400 performers , 
 Mr. Cohen Herr Elsner acting respectively principal violinist 
 violoncellist . Mr. Smythe , Mus . B. , presided organ , 
 Herr Henry Stiehl conducted 

 BIrRMINGHAM.—The Carl Rosa , Opera Company concluded season 
 28th April . Wagner Flying Dutchman , repeated 25th , 
 attracted larger audience previously . Beethoven Fidelio , 
 , great success season , house literally 
 crammed.—The Town Hall crowded occasion Mr. 
 Pyatt Concert , Monday , April 30th . Mr. Sims Reeves main 
 attraction , course received ovation appearance . 
 artists Miss Larkcom , Miss D’Alton , Signor Foli , vocal- 
 ists ; Mr. Henry Nicholson , flute ; Mr. Lockwood , harp ; Mr. Roeckel , 
 piano ; Mr. Pearce , harmonium.—A Chamber Concert 
 given Tuesday 2nd ult . harpist , Mr. Ffrench Davis , 
 assisted Miss Isabelle Davis , piano , Mr. F. Ward , violin . Vocal 
 selections rendered members Mr. Bickley Glee 
 Party.——The Mr. Stockley Orchestral Concerts present 
 season given Town Hall Thursday 3rd ult . 
 programme comprised Beethoven Symphony D ( . 2 ) , 
 overtures Ruy Blas , Der Freischiitz , L’Italiana ia Algieri , 
 Funeral March Marionette , Andante MS . Symphony 
 Mr. T. Anderton . performance excellent . 
 Miss Robertson Mr. Graham de Lancy solo vocalists . 
 - songs given members Festival Choral 
 Society , appreciated . Mr. Stockley conducted , 
 Mr. C. J. Stevens accompanied.——A Juvenile Concert , aid 
 Children Hospital , given Town Hall Friday 
 4th ult . choir , consisting thousand children different 
 parochial schools , conducted Rev. F. G. Bussell ; 
 Messrs. Pearce Halliley acted accompanists —— 
 xoth ult . members Edgbaston Amateur Musical Union ( 
 orchestral Society numbering performers ) gave 
 Concert Town Hall , benefit Rhyl Convalescent 
 Home Women . admirable training ofthe band Con- 
 ductor , Mr. C. J. Duchemin , conspicuous ; performance 
 highly successful . vocalists Miss Rose Hersee , Miss 
 Enriquez , Mr. Vernon Rigby , Mr. Lansmere , gave 
 great pleasure performance . Mr. Duchemin , con- 
 ducting , played artistically movements 
 Mendelsso n Concerto D minor Nocturne ( Op . 37 , . 2 ) 
 Polonaise ( Op . 40 , No.1 ) Chopin . Mr. R. M. Winn , Mus . B 

 excellent accompanist —— Chamber Concert connection 
 Royal Society Artists took place Saturday 19th 
 ult . performers Miss Emma Beasley , solo vocalist ; Messrs 
 F. Ward , S. Blythe , W. F. Roden , strings ; Mr. H. Warling ( 
 Leipzig ) , piano ; Mr. T. Anderton , Mus . B. , director accom- 
 panist.——On 15th ult . Gounod Messe Solennelle selection 
 sacred music given Lodge Road Chapel . solos 
 Mass rendered Mrs. Bellamy , Mr. Coley , Mr. Jno . Bel- 
 lamy , accompaniments ( arranged piano harmonium ) 
 played Miss Woodward Mr. G. A. Johnson . solos 
 contributed Miss Jackson Miss Bailey . Mr. Isaac Bradley 
 Conductor . Concert passed satisfactorily 




 298Liverroo.t.—A National Ballad Concert given roth ult . 
 Lord Nelson Street Hall “ Liverpool Quartett . ” 
 programme excellent , audience 
 enthusiastic . Miss Ternan gave capital rendering “ Kathleen 
 Mavourneen , ” “ Auld Robin Gray , ” Miss Haworth 
 successful “ Comin ’ thro ’ rye . ” Mr. Harrison gave “ Heart 
 Oak ” “ Minstrel Boy ” excellent style , “ Tom 
 Bowling ” “ pretty Jane ” sung Mr. Terbutt 
 feeling artistic finish . concerted pieces excellently 
 rendered 

 MANCHESTER.—Mdme . Samson Dunne gave Annual Concert 
 Pianoforte Recital Atheneum Rooms 28th April , 
 patronage Lady Annette de Trafford . attractive pro- 
 gramme presented , special praise accorded perform- 
 ance Mendelssohn ‘ ‘ Andante Rondo Capriccioso ” Mdme . 
 Dunne , ‘ ‘ Kreutzer Sonata ” Master T. Dunne . Miss 
 Redfern ( pupil Mdme . Dunne ) highly successful . 
 vocalist Mdme . Rovina Arnold ; solo , Zither , Miss S. Payne . — — 
 1st ult . Concert given Hulme Town Hall 
 large audience . vocalists Mdmes . Pickering 
 Bowmont ( efforts received ) , Messrs. Dumville 
 Frearson ; instrumentalists Messrs. Risegari Clementi 
 ( violin ) , Arison ( violoncello ) , Horton C. Allison , Mus . B. , Cantab . 
 ( pianoforte ) . Mr. Risegari gave Vieuxtemps ’ “ Air Varié ” Raff 
 “ Cavatina ” effect . Mr. Horton C. Allison rendered 
 movements Beethoven Sonata ( . 12 ) flat 
 good taste skill , improvised airs Gounod Faust , 
 performed ( Messrs. Clementi Arison ) Mendelssohn Piano- 
 forte Trio C minor . - singing choir , 
 Mr. Ambler direction 

 Newport , MoONMOUTHSHIRE.—Miss Righton gave Pianoforte 
 Recital Assembly Room , Town Hall , 17th ult . , 
 select appreciative audience . programme contained selections 
 works Mendelssohn , Beethoven , Benedict , Heller , Hummel , 
 Weber , Thalberg . Miss Righton playing remarkable 
 ease finish , warmly frequently applauded 

 Oxrorp.—On Wednesday evening 19th ult . members 
 Choral Society oe performance Schumann Pilgrimage 
 Rose Mendelssohn Athalic . solo vocalists Miss Giulia 
 Warwick , Mrs. Hubert Blake , Miss Annie Bolingbroke , Mr. Shake- 
 speare , Mr. Shaw , Mr. Wadmore . lyrics Athalie 
 effectively declaimed Mr. Brandram , harp - playing 
 Mr. Dodds feature overture . concert highly 
 training choir creditable Mr 

 Spitssy.—The members Spilsby , Wainfleet , Friskney 
 Amateur Choral Society gave fourth annual Concert Ist 
 ult . , conductorship Mr. Keller , large appre- 
 ciative audience . ( sacred ) contained selections 
 Creation Rossini Stabat Mater , concluding Mendels- 
 sohn 95th Psalm . second ( secular miscellaneous ) 
 consisted - songs , madrigals , duets , & c. violoncello solo , 
 delicately rendered Mr. T. L. Selby , Nottingham , redemanded ; 
 Mr. Dunkeaton , Lincoln Cathedral Choir , Mr. Nunns , 
 Leeds Parish Church Choir contributed genuine 
 success concert . orchestra choir numbered 
 eighty performers 

 Toronto.—Shaftesbury Hall crowded galleries 
 occasion Concert Philharmonic Society 5th April . 
 programme divided parts , consisting ofa 
 selection Creation Andante Beethoven 
 Second Symphony , second devoted entirely Rossini 
 Stabat Mater . solo vocalists Mr. Hampshire , Mrs. Bradley , 
 Miss Hillary , Mr. Warrington . favourite numbers , 
 “ Cujus animam , ” sung Mr. Hampshire , “ Inflammatus ” 
 Mrs. Bradley , “ Pro peccatis ” Mr. Warrington , 
 carefully rendered warmly applauded . ‘ quartett “ Sancta 
 Mater ” unaccompanied quartett “ Quando corpus ” 
 worthy praise . conductor Mr. Torrington , 
 public properly gave credit general result 

 Ware.—On Tuesday 1st ult . performance Messiah 
 given Town Hall presence large audience . 
 solo vocalists Miss Bell , Miss Christie , Mrs. Horley , Miss Edgar , 
 Miss Cobham , Miss Cass , Mr. Trelawny Cobham , Mr. F. Penna . 
 Mr. Cobham sang judgment feeling , rendering “ Thy 
 rebuke , ” “ Behold , ” “ Thou didst leave ” 
 especially appreciated . Miss Christie , Mrs. Horley , Miss Edgar , 
 Miss Cass efficient solos allotted . Miss Bell 
 rendering “ know Redeemer liveth ” remarkable 
 expression sweetness , applauded . Mr. F. 
 Penna highly effective , especially airs “ ‘ nations ” 
 “ trumpet shall sound . ” choruses exceedingly 
 sung . Rev. A. D. C. Thompson , Wormley , conducted . 
 Rev. S. Navine , Hunsdon , presided piano , Mr. Williams , 
 organist Hatfield , harmonium 




 OBITUARY 


 MONTH 


 HICHESTER CATHEDRAL.—WANTED , 


 HEPPARD , REV . H. FLEETWOOD . — 


 R.A.M. 


 M R. CHARLES JOSEPH FROST LADIES 


 ENTWORTH PHILLIPSON “ GUIDE 


 YOUNG PIANOFORTE TEACHERS STUDENTS , ” 


 ETNRITD 


 TEPHEN HELLER PIANOFORTE 


 TEPHEN HELLER TARENTELLES 


 TEPHEN HELLER WANDERSTUNDEN 


 TEPHEN HELLER NUITS BLANCHES 


 TEPHEN HELLER PROMENADES D’UN 


 TEPHEN HELLER DANS LES BOIS : 


 TEPHEN HELLER DANS LES BOIS 


 TEPHEN HELLER VALSES 


 TEPHEN HELLER ALBUM DEDIE LA 


 TEPHEN HELLER SONATAS 


 TEPHEN HELLER PIANOFORTE COM- 


 CHORAL SOCIETY 


 COLLECTION 


 - SONGS , GLEES , 


 EDITED 


 MICHAEL WATSON 


 NEW NUMBERS , 


 COMPLETE LISTS APPLICATION 


 ORIGINAL PIECES HARMONIUM 


 AMERICAN ORGAN 


 PIANOFORTE , 


 NEW NUMBERS . 


 LYRIC STAGE , 


 GRAND OPERATIC DUETS , 


 SUCCESSFUL SONGS 


 SEASON 


 


 R. L. PEARSALL 


 COLLEGIATE SERIES 


 DR . S. S. WESLEY 


 BERTHOLD TOURS 


 DULCIANA 


 SCHUMANN 


 J. P. KNIGHT . 


 NEW BALLADS 


 BERTHOLD TOURS . 


 R. L. PEARSALL 


 CONSECUTIVE FIFTHS OCTAVES 


 COUNTERPOINT 


 ESSAY , R. L. PEARSALL 


 HARMONIUM MUSIC 


 PUBLISHED 


 NOVELLO , EWER & CO 


 SAINT - SAENS , CAMILLE 


 PIANOFORTE HARMONIUM . 


 LONDON : NOVELLO , EWER & CO 


 TS 


 HYMNARY 


 BOOK CHURCH SONG 


 FOLLOWING EDITIONS READY : — 


 NEW HYMNAL . 


 HRW O 


 TONIC SOL - FA EDITION 


 TONIC SOL - FA EDITION . 


 REV . R. BROWN - BORTHWICK ’ 


 


 CATHEDRAL PSALTER 


 POINTED CHANTING 


 PUBLISHED SANCTION 


 REV . DEAN ST . PAUL 


 


 REV . RAR WESTMINSTER 


 SUNG MR . SANTLEY . 


 FLY LAW SUPREME 


 304 


 JUNE 1877 


 YEAR ROUND 


 CONDUCTED 


 CHARLES DICKENS 


 LATE NEW BOND STREET SOHO SQUARE 


 REALISING ESTATE 


 RESERVE 


 780 PIANOFORTES 


 


 588 HAND 


 CAREFUL FINISH , OFFERED 


 USUAL COST PRICE 


 SHEET MUSIC PRICE 


 WASTE PAPER 


 MUSICAL PROPERTY 


 VIEW SALE 


 5 , FINSBURY PAVEMENT 


 MOORGATE STREET STATION 


 SELECT COMPOSITIONS 


 


 GREAT MASTERS , 


 ARRANGED ORGAN 


 A. H. BROWN 


 


 ORGANIST QUARTERLY JOURNAL 


 ORIGINAL COMPOSITIONS . 


 ( PUBLISHED IST JANUARY , APRIL , JULY , OCTOBER . ) 


 EDITED 


 ALF : GUILMANT NEW COMPOSITIONS 


 SER 


 SECOND SERIES . 


 ANGLICAN CHORAL SERVICE BOOK . 


 USELEY MONK PSALTER 


 OULE COLLECTION WORDS 


 OULE COLLECTION 527 CHANTS , 57 


 PSALTER , PROPER PSALMS , HYMNS , 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 ORDER HOLY COMMUNION . 


 OFFERTORY SENTENCES COMPLETE ( 20 ) , 


 COMMUNION SERVICE 


 


 MEN VOICES , 


 TE DEUM LAUDAMUS 


 S. C. COOKE 


 “ VENITE EXULTEMUS DOMINO . ’ — 


 7 ET HEART TROUBLED 


 “ GRASS WITHERETH , FLOWER FADETH . ” 


 ANGLICAN 


 PSALTER 


 CHANTS 


 SINGLE DOUBLE 


 EDITED 


 REV . SIR F. A. GORE OUSELEY , BART . , ETC . , 


 EDWIN GEORGE MONK 


 HARVEST CAROL 


 “ HOLY SEED - TIME 


 WYCLIFFE CANTICLES 


 SERVICE BAPTISM 


 SANCTUS , INTRODUCTORY SENTENCES . 


 N EW ORATORIO , “ BENJAMIN . ” 


 PREPARE YE WAY ” ... ADVENT . 


 ‘ ‘ CHRIST RISEN ” EASTER 


 THANKSGIVING . 


 COMPOSITIONS WILHELM SCHULTHES 


 397 


 MANUAL SINGING 


 USE CHOIR TRAINERS & SCHOOLMASTERS , 


 - EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT - SINGING MANUAL 


 APPENDIX 


 ( COMPANION WORK 


 COLLEGIATE VOCAL TUTOR 


 CATHEDRAL CHANT - BOOK 


 NEW EDITION 


 DR . BENNETT GILBERT POPULAR WORK 


 SCHOOL HARMONY 


 SUBJECT SPECIAL EXERCISES . 


 THEORY MUSIC 


 BOOK ) 


 USIC NEW CODE . — “ 


 ILLUSTRATED BOOKS HOLIDAY PERUSAL . 


 SABILLA NOVELLO . 


 SABILLA NOVELLO 


 NEW CHEAP EDITION 


 SWEDISH MELODIES 


 SUNG 


 SWEDISH MINSTRELS 


 LFRED R. GAUL CHORAL SONGS , 


 AKER PIANOFORTE TUTOR 


 POPULAR POUR - SONGS GABRIEL 


 OPINIONS PRESS 


 CHORAL SONGS SCHOOL & HOME 


 - ORIGINAL SONGS 


 


 , , VOICES 


 ORCHESTRAL SCORES , 


 HANDEL MESSIAH 


 HANDEL ISRAEL EGYPT , 


 HAYDN CREATION 


 LONDON : NOVELLO , EWER CO 


 WINE - CUP CIRCLING ALMHIN 


 BELLS ST . MICHAEL TOWER . 


 SMALLWOOD 


 CLASSICAL & SECULAR EXTRACTS 


 PIANOFORTE 


 NEWTOWN NURSERY MARCH 


 NOVELLO OCTAVO EDITION 


 MOORE 


 IRISH MELODIES 


 EDITED 


 M. W. BALFE . 


 NOVELLO 


 TONIC SOL - FA SERIES 


 TRANSLATED EDITED 


 LONDON : 


 NOVELLO , EWER CO 


 NOVELLO OCTAVO EDITION 


 WORKS 


 NIELS W. GADE 


 SONGS COMPOSED WILLEM COENEN 


 EIG 


 


 NOVELLO OCTAVO EDITION 


 


 


 FEMALE VOICES 


 


 FEMALE VOICES 


 COMPOSED 


 FRANZ SCHUBERT . 


 PUBLISHED 


 EIGHTEEN - SONGS 


 COMPOSED 


 ANTON RUBINSTEIN . 


 CONTENTS 


 LONDON : NOVELLO , EWER CO 


 N OVELLO , EWER CO . COMPLETE 


 UNIFORM EDITION 


 MENDELSSOHN 


 THIRTEEN - SONGS 


 FOURTEEN SONGS 


 SET 


 POEMS ROBERT BURNS , 


 EDITED ADAPTED NATALIA MACFARREN 


 CONTENTS . 


 - SONGS 


 


 R. L. DE PEARSALL 


 CONTENTS VOL . X 


 CONTENTS VOL . XI 


 PIANOFORTE COMPOSITIONS 


 RECENTLY PUBLISHED 


 NOVELLO , EWER & CO 


 J. BAPTISTE CALKIN , . 


 R. F. DALE . 


 E. HECHT . 


 OLIVER A. KING . 


 A. C. MACKENZIE . 


 LEFEBURE - WELY . 


 EDOUARD ROECKEL . 


 E. SAUERBREY . 


 JAMES SHAW . 


 NEW PIANOFORTE PIECE LISZT 


 ROSE , SOFTLY BLOOMING 


 ROMANCE 


 TRANSCRIBED PIANOFORTE 


 FRANZ LISZT 


 PRICE SHILLINGS . 


 NEW WORK ORGAN 


 ORGAN 


 THEORETICAL PRACTICAL TREATISE : 


 INTENDED ASSIST STUDENT ACQUIRING 


 SOUND KNOWLEDGE INSTRUMENT 


 PROPER MANIPULATION 


 SERIES 


 ORIGINAL EXERCISES ILLUSTRATIVE 


 COMPOSITIONS 


 FREDERIC ARCHER 


 LONDON : 


 NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) 


 80 & 81 , QUEEN STREET , CHEAPSIDE ( E.C 


 COMPOSITIONS 


 AGNES ZIMMERMANN 


 YHNNNNHNE 


 DADADADE 


 PIANOARRANGEMENTS 

 Bourrée Ep , J. S. Bach ... 
 Bourrée C , J. S. Bach 
 Gavotte G , J. S. Bach . 
 Second Concerto . Composed Harpsichord 
 Organ , G. F. Handel sf 
 Scherzo Beethoven Trio , Violin , Viola 
 Violoncello . Op.g . No.1 .. ; rane , ce 
 Menuetto ditto , ditto . Op . 9 . . 2 Cae 
 Allegretto alla polacca , Beethoven Serenade . 
 Op . 8 . Violin , Viola Violoncello ... 3 0 
 . 4 , R. Schumann Skizzen fiir den pedal 
 Fligel ( Sketches Pedal Pianoforte ) ... 3 0 
 Corelli Violin Sonatain E minor ... dae ce Se 
 Schubert Menuetto , Spacenets ‘ Stringed 
 Instruments . Op . 2Qe vee oe 3 0 

 London : NoveELto , bean aad Se . 
 Published . 
 VOCAL TRIOS FEMALE VOICES . 
 ComposeEp G. ROSSINI . 
 No.1 . FAITH . No.2 . HOPE . No.3 . CHARITY . 
 8vo , 6d ; , singly , 2d . . 
 London : Nove.ro , Ewer Co 




 AL ADA AD 


 WALTER MACFARREN 


 SCHERZO : 4 0 


 SPINNING SONG 3.9 


 OFFERTORY SENTENCES . 


 APPENDIX FESTAL USE , 


 SET MUSIC 


 JOSEPH BARNBY . 


 LATIN WORDS 


 LATIN WORDS . 


 GEO . R. GRIFFITHS 


 MINOR 


 ANDANTE LA HAYDN DIAPASON 


 NET . 


 . INTRODUCTION MARCH LA HANDEL 


 ANDANTES 


 . ADAGIO 


 MINOR . 


 NEW WORK AMATEUR _ ORGANISTS 


 . DESPISED . ORGIES 


 BEAUTIFUL FEET 


 GOD 


 HELL 


 NEW WORK SINGING CLASSES 


 CHAPPELL PENNY OPERATIC - SONGS . 


 ANTOINE COURTOIS ’ CORNETS 


 CHAPPELL & CO . , 50 , NEW BOND STREET , LONDON