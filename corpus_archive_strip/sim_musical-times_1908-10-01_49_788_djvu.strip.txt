


PUBLISHED ON THE FIRST OF EVERY MONTH


- ROYAL CHORAL SOCIETY. | QUEEN'S HALL


ROYAL ALBERT HALL. 


= | PROME NADE | CONCERTS 


70 MISS AGNES NICHOLLS SATURDAY, DECEMBER 


> VIO I. RECIT : 


‘PIERROT OF A MINUTI WEDNESDAY AFTERNOON, OCTOBER 


MISS PHYLLIS LET! WEDNESDAY AFTERNOON, NOVEMBER 


GERVASE ELWES MR. HARRY DEARTH. - 


( N.W SATURDAY AFTERNOON, OCTOBER 


NEW IMPROVED SEATING AND VENTILATION. THE QUEEN'S HALL ORCHESTRA 


KUM


THE, GUILDHALL SCHOOL OF MUSIC. | BATTERSEA POLYTECHNIC, 


SCHOOL OF MUSIC


I ( EB —— 


CITY OF LONDON COLLEGE, SOMPETTSIONS EUR 


LONDON. REGISTER OF ORGAN VACANCIES. 


GUILD GAZETTE


I CULTURE AND SOLO-SINGING 


H G \ , F.LG.C.M, ; RESTORAI


ION. VOCAL TECHNIQUE, ¢ 


VOCAL RE-ADJ USTMEN1


DEL


THE


SHI


BRI


BR


FOUR-MA


XUM


THE MUSICAL


623


I, 1908


ME /ENDELSSOHN S( UE. AOEEES


FOR CHORAL AND ORCHE STRAL SOCIETIES


( , RECITALS WITH MUSIC. 


THE MACKENZIE’S “DR E AM OF JUBAL” 


F ; : G. L. GOMMI 


T ALS


XC


BRIXTON ORGAN REC


BRIXTON INDEV?ENDENT CHUI


BRIXTON ROAD, S.W. 


N \ W. G., D G ( 


I 20, 22, WIGMORE STREET, W


I, 1908


PROFESSIONAL NOTICES. 


A SPECIAL FEATURE


OF THE BUSINESS OF


NGE OF ADDRESS. 


MADAME HANNAH JONES, A.R.A.M 


IS THE


MISS BARONS E ATON 


MISS MARION


10 


MISS DOROTHY PARKS 


NEW SEVCIK VIOLIN METHOD. 


- MISS ESTELLA L INDEN 


(SOP RANO) 


SOPRANO


UM


TURNBULL


MISS HOW ARD FE INC H


CON TRALTO) 


MADAME ANNIE GREW 


MR. WILL AM. FELL


FRANCIS GLYNN 


<NOR). 


SAMUEL MASTERS


TENOR


MR


MR. ALBERT MONAG HAN 


MR. ERNEST PE NE ‘OLD


MR. CH; ARL ES S: AU NDERS 


MR. ARTHUR WALENN 


(BARITONE


MR. SAMUEL MANN 


MR. JAMES S MOODIE


ORATORIOS, CONCERTS, RECITALS, 1 


ROBIN OVERLE EIGH


BASS-B ARETONE


(CHROMATIC HARPIST


I, 1908. 625


MR. HERBERT PARKER


BASS 


COMP( ISE RS’ “MSS


AND MUS.B


TEACHER OI VOICE PRODUCTION AND SINGING 


THOMAS


LESSONS


LONDON COLLEGE OF MUSIC 


GREAT MARLBOROUGH STREET, LONDON, W. 


FOR MUSICAL EDUCATION AND EXAMINATIONS


HIGHER EXAMINATIONS, 1908


DIPLOMAS IN PRACTICAL MUSIC. 


LICENTIATES (L.L.C.M.). 


ASSOCIATES (A.L.C.M.). 


I I | 


I \. | P I 


] ( \ _ S 


K , S 


\ 1 W 


M \ 4 W \ IW 


IL H I 


I \ \ 1. R \\ S 


TEACHER’S DIPLOMA. 


PRIVA 


CLASS 


A.R.C.O. 


FOUR 


BAC,, 1¢ 


DPD: 


POINT 


ORCHES


FUGUE 


LONDON COLLEGE OF MUSIC. 


DIPLOMAS IN THEORETICAL MUSICThe College is open from 9.30 a.m. to 9.30 p.m. ‘The staff consists of over go‘professors. 
There isa 1 ULL ¢ ere LADIES’ CHOIR, FULL ORC HESTRA OPERA CLASS, DRAMATIC 
CLASS, STRING QUARTET CLASSES, and CLASS for TRAINING CONDUCTORS

Recent performances have included Coleridge-Taylor’s “ Hiawatha’s Wedding-Feast,” also Concertos by 
Beethoven, Mendelssohn, Grieg, and Paganini

Further RECITALS upon the Colleg » Organ are being arranged




1: 2 | COUNTERPOINT, COMPOSITION, ORCHESTRATION


MUSICAL


THE


625


I I


ROYAL ACADEMY OF MUSIC, 


TENTERDEN STREET, W. 


I I I ( 


I AINING ¢ 


THE ROYAL COLLEGE OF MUSIC, 


PRIN¢ CON RT |} AD, SOUTH KENSINGTON, S.W 


I MAJESTY THE KING 


I I IP (A.R.C.M I 


THE ROYAL COLLEGE OF ORGANISTS


H ING, H 


‘ AND THE ABILITY TO PLAY IT.” 


| BEFORE BUYING A PIANO PLAYER 


ILLUSTRATED CATALOGUE ON APPLICATION


BOSWORTH


FESTIVAL


EDITION


ANTHEMS


HARVEST AND GENERAL USE 


SIR FREDERICK BRIDGE


M.V.O


ORLANDO GIBBONS


NEW “COLLEGIATE PSALTER


REV. H. DANIELL-BAINBRIDGE 


BOSWORTH AND CO., 5, PRINCES ST., OXFORD ST., W


BON-BON SUITE


FOR BARITONE SOLO, CHORUS AND 


| ORCHESTRA


THOMAS MOORE


S. COLERIDGE-TAYLOR


CHURCH ORCHESTRAL SOCIETY. 


| H ( G. F. HUNTLEY, I ,M DD 


YUM


1, 1908. 629


OCTOBER 1, 1908


MANCHESTER CATHEDRAL


BENCH END IN THE CHOIR


630


4 MISERERE IN THE CHOIR


THE CHOIR, LOOKING WEST


1600


TT


XUM


1, 1908


XUM


THE CHOIR, SHOWING THE FATHER SMITH ORGAN IN ITS ORIGINAL POSITION. 636 THE MUSICAL TIMES.—Octoser 1, 1908

I'he present organist and master of the choristers| was considered the best English tenor vocalist of 
is Dr. J. Kendrick Pyne, who has held office| his day, next to John Braham. He sang the tenor 
since 1875 and of whom a biographical sketch|solos in Beethoven’s ‘ Mount of Olives’ on the 
is subjoined. Dr. Pyne is shortly relinquishing! occasion of its first performance in England, at

his cathedral duties. He will be succeeded by 
Mr. Sydney H. Nichoison, acting-organist of 
Carlisle Cathedral, who will enter upon his duties 
at the beginning of next year




MR. JAMES KENDRICK PYNE, 


KUM


1905 ; 


XUM


I, 1908. 637


THE 


1905


1908, 


1, 1908


G ESTER ORATORIO SOCIETY


XUM


I, 1908. 639


1S


I, 1908


WCCA


COMPOS¢ 


XUMNot only as a distinguished organist, but as a} And what shall be said of our own composers ? 
man of wide culture and a patriotic citizen, | Happily they are not lagging behind in the noble 
Dr. Pyne has won the regard and respect—may I} fight. Music-lovers are looking forward with keen 
not go so far as to say the affection ?—of the people | interest to the production of Elgar’s Symphony 
of Manchester, and his name is inscribed upon |in A flat, soon to be produced, under Dr. Richter, 
their hearts as that of one who has proved himself|at Manchester. It is rare for a composer to wait 
a benefactor of their city.’ | till his fiftieth year before turning his attention to

the greatest of art forms. Brahms arrived late at 
|that point, though the Pianoforte concerto in 
|D minor, dating from his earliest manhood, was 
IS THE SYMPHONY DOOMED? | Originally meant for a Symphony. It was only 
| changed into a Concerto because the young master 
It would seem that after all the Symphony is| had to confess himself insufficiently versed in the 
loomed to an early death, as the champions | art of instrumentation to do justice to that /ina/ 
f the Symphonic Poem have for some years past} which had long been buzzing in his head, and 
tried to make us believe. Though the latter, like | which was doubtless something like the glorious 
so many of the best things appertaining to our} Fiva/e eventually used for his first Symphony, an 
Art, originated in Germany, and though brought to| ending which, it may safely be assumed, was to 
its present remarkable development by Richard | suggest the apotheosis of Schumann’s genius, just 
Strauss, yet there are plenty of German musicians | as the opening .4//egro of the Concerto, as we know 
who still prefer the grand old form in which the/it now, deals, no doubt, with the terrible tragedy 
great masters since Haydn expressed their deepest | of that master’s last years. 
houghts, and poured out their hearts in deathless} Amongst other British composers who have 
strains. We are continually reading of new] symphonies in preparation, mention may be made 
Symphonies produced abroad; and if they are|of Dr. Walford Davies and Mr. W. H. Bell; both 
not all epoch-making works, if in fact little is| works will be welcomed as efforts by singularly 
breught forth that seems worthy of joining on to | talented men. Some time ago Dr. F. H. Cowen 
Brahms’s great work in E minor, the last of the| was reputed to be engaged upon his ‘No. 7 in 
links in the chain of masterpieces forged by the|1D minor,’ and one would not be surprised if 
classics, yet they are serious efforts and palpable| Sir Charles Stanford were busy with a work to 
proofs that the symphonic form has not lost its | follow his No. 7, the ‘ Watts’ symphony. 
fascination for earnest musicians. | In conclusion, the Symphony has little to 
New symphonies are shortly to be performed,| fear from its later rival the Symphonic Poem. 
or have been recently brought to a hearing, by| A composer may be as ‘modern,’ as thoroughly 
Gustav Mahler, Xaver Scharwenka, August Bungert, | imbued with the Z-/¢geis¢ as is possible ; he may 
Ferdinand Hummel and Felix Woyrsch. The last | have every latest technical device at his fingers’ 
named—whose ‘ Passion music’ will be one of the|}end and seem equipped for the heavy task of 
emulating the finest achievements of Richard 
responsible for a Symphony in C minor, produced | Strauss : but if he has anything to say that is 
at Altona, near Hamburg. This work a leading| worth saying and listening to, he can, in the 
Hamburg paper hails in all seriousness as ‘the| great and enduring form of the Symphony, express 
long-expected Brahms No. 5,’ thus recalling} himself as completely, and convincingly, and 
Hans von Biilow’s dictum that the great Johannes’ | produce the deepest possible impression upon his 
first Symphony, in C minor, might be regarded as | audience as effectually, as can the most zealous 
Beethoven’s No. 10. Woyrsch is certainly a] exponent of the Symphonic poem. 
composer of the type of which symphonic writers} ‘To quote Sir Hubert Parry: ‘It is net likely 
are made, wherefore it may be hoped that | that many will be able to follow Brahms in his 
genuine melodic inspiration will in his case go|severe and uncompromising methods: but he 
hand in hand with the profound contrapuntal | himself has shown more than anyone how elastic 
knowledge and mastery of form which he|the old principles may yet be made without 
unquestionably possesses. | departing from the genuine type of abstract 
Slavonic composers are not idle. Gospodin| instrumental music ; and that when there is room 
Mili Balakirev is about to publish a Symphony in for individual expression there is still good work to 
1) minor, No. 2, which is announced for first |} be done, though we can hardly hope that even the 
performance by the Philadelphia (U.S.A.) Symphony | greatest composers of the future will surpass the 
Orchestra under Herr Pohlig ; and Pan Josef Suk’s |symphonic triumphs of the past, whatever they

work in five movements, bearing the fine, suggestive | may do in other fields of composition

are to enjoy a musical treat, if brought within their means. 
Handel’s Dettinven Te Deum—Purcell’s Jubilate in D— 
Hayes’s anthem, O worship the Lord—Mendelssohn’s

beautiful anthem, As ‘he hart pant with the delightful 
old preces and responses by Tallis, formed the musical 
portions of the service. On the other days the public had 
the opportunity of hearing the following works for the 
most part entire Mendelssohn’s £/ijah, ‘Spring’ from 
Haydn’s Seasons, Haydn’s Creation, Beethoven’s Engedi

a selection from Crotch’s /a/estine, and the ever fresh 
Messiah of Handel. The orchestra numbered about 350 
well selected instrumental and vocal performers. The




XUM


I, 1908. 643


SINGERS, £40 35


XUM


WORCESTER MUSICAL FESTIVAL. 


R SPECIAI


I I 


( M I \ IN M I 


646


FF


I II 


XUM


SEATED


AV


IVOR ATKINS


ORGAN OF WORCESTER


CATHEDRAL


1905


THEOn Thursday the performances were resumed in the

being Sir Charles 
produced at the 
Beethoven’s Violin

cathedral, those the morning 
Stanford’s Stabat Mater (Op. 
Leeds musical festival of 1907 
oncerto (soloist, Mischa Elman); and Dr. Walford 
Das es’s ‘Everyman’: and in the evening Bach’s 
fagnificat; Mr. Ivor Atkins’s ‘Hymn of Faith’ 
‘produc ed at the last Worcester festival, 1905 and 
Mendelssohn’s ‘ Hymn of Praise.’ Detailed criticism 
f these six works is hardly necessary. In placing

performances on rec ord, it should be stated that 
the impressiveness of the Stabat Mater (conducted by 
the composer), no less than that of ‘ Everyman,’ 
was deepened by being heard in such appropriate 
surroundings as a stately cathedral affords. That 
Mischa Elman again triumphed in his performance of 
the Beethoven concerto needs only to be stated, but 
the cadenzas he introduced sounded very trivial in the 
athedral

It was indeed delightful, in the evening, to listen 
to the strains of dear old Bach in his noble 
las How charming are the flutes in the 
ir ‘Esurientes implevit bonis,’ contrasting as the 
ccompaniment does with the jubilation and 
trumpetings of the other parts of this fine work of the 
great Cantor. The inclusion of Mr. Atkins’s ‘ Hymn




1S


KUM


I, 1908. 647 


THE ROVAL COLLEGE OF ORGANISTS: 


NOTES ON ITS EARLY HISTORY. 


63


MUSICAL


648 THE


TIMES


OCTOBER 1, 1908


1866 


XUM


I, 1908. 649


1672 1575


1873


IN MEMORIAM EDWARD BACHE


50


RB S 


R I 


I S ( 


EMMANUEL COLLEGE, CAMBRIDGE


29 


22


AN OLD CITY ORGAN. 


\ | \ ARY LN \ 5 


ATORIK AT BRIXTON PARISH CHURCH


TON ( AN RECITAI 


R \N


1A 


PIANOFORTE MUSIC. 


1860


XUM


THE


I, 1908. 651


SUD UMth

Beethoven round ‘‘ to show him the coplanse” (Beethoven

who c¢ ducted, was then stone-deaf.)’ The addition of the 
word ‘she’ before the word ‘turned’ would have made the 
sentence clearer. One cannot help regretting a lack of 
patriotic feeling in the article ‘ Military Band.’ Of its fifteen




OLIN MUSIK


PART-SONGS BY BRAHMS, 


652 THE MUSICAL


I, 1908


ARASATI 


PAUL HOMEY


UNION


HOME MUSIC STUDY


THI \ 


J. E. LAWRENCE, 


— 94 


XUM


PART-SONG FOR CHORUS OF MIXED VOICES


5-9-5 ——"F — 2 = 7


0-8 7 / —- Z| 


— — | —_—9 - 


. _ PP — - « 


; ¢ 1 #@ _§$ 2 2? 2 #-! / 


P _——_— A 


L 9° "4 3 : 2 - 


P ) / ‘ —_— 


XUM


WAKE THE SERPENT NOT


* — XN XN NN 


A 4 . 


. 4 4 | 


> 4 | ~_—— I


KUM


THE


I, 1908. 657


PROMENADE CONCERTS


‘THE KINGDOM’ AT MELBOURNE. 


MR. COLERIDGE-TAYLOR’S ‘ FAUST’ MUSIC


OPERA IN ENGLISH. 


CHORAL


THE MUSICAL


MUSIC IN SOUTH AFRICA


I, 1908


LONDON CHORAL UNIONSThe remarkable activity and technical attainments of 
Choral Unions formed from the Evening Schools, under the 
London County Council, are evidenced in the works that are 
being rehearsed for performance next Spring. The followin 
list speaks for itself

BATTERSEA, CLAPHAM AND WANDSWORTH Choral 
Union (Conductor: Mr. George Lane). Ruins of Athens 
(Beethoven), Wreck of the Hesperus (A/acCunn

East Lonpon CHORAL UNION (Conductor: Mr, G 
Day-Winter). Flag of England (Arde




SYMPHONY CONCERTS AT QUEEN’S HALL


UM


THE


I, 1908. 659


THE COMING SEASON


MUSIC IN VIENNA


MUSIC IN BIRMINGHAM. 


ROM OUR OWN CORI NDEN 


MUSIC IN GLASGOW. 


(FROM OUR OWN CORRESPONDENTMUSIC IN LIVERPOOL. 
(FROM ¢ OWN CORRES NDENT

In forecasting the coming season’s operations the place of 
honour must be given to the Philharmonic Society, whict 
announces a series of twelve performances, commencing on 
October 13 with an orchestral concert. Among the choral 
works to be performed, English composers are represented by 
Cowen’s ‘Sleeping beauty’ and Brewer’s ‘Sir Patrick Spens,’ 
while Strauss’s ‘ Wanderer’s Sturmlied,’ Beethovens 
‘ Fidelio’ and ‘ Elijah’ are also to be given

The Orchestral Society has arranged an attractive scheme 
on the lines of the progressive policy which has given suc! 
distinction to these concerts. Under Mr. Granville Bantoc




MUS


MU


XUM


THE


I, 1908, 661


NEWCow

ASTLI 
ORRI 
Union announces four ci incerts during the 
Handel’s ‘ Acis and Galatea, °C liffe’s *‘ Ode 
to the North-east wind,’ and Rutland Boughton’s Folk-song 
variations will be given in October ; on February 4, Verdi’s 
(for the first time here), and on February 24, 
ye the Lord’ and Beethoven’s Choral 
Three different orchestras have been engaged, 
the Scottish, and the Halk

AND




ONDI


DISTRICT


NT


MUSIC IN 


FROM OUT The Postal Telegraph Choral Society, under the direction 
of Mr. E. L. Bainton, will give two concerts devoted mainly 
to unaccompanied music. In addition to madrigals of the 
16th and 17th centuries, works by Schubert, Schumann, 
Cornelius, and Brahms will be performed, and modern 
English music will be represented by works of Parry, 
Granville Bantock, J. B. McEwen, G. von Holst, 
W. H. Bell, Havergal Brian, Walford Davies, Joseph 
Holbrooke and Rutland Boughton. Truly an imposing list

The Armstrong College Choral Society will rehearse 
Beethoven’s Mass in C, Vaughan Williams’s ‘ Towards the 
wn region, and some Northumbrian folk-songs

I rateur Vocal Society intend performing Barnett’s 
‘ Ancien go a and the Jarrow Philharmonic Society 
ve selected Parry’s ‘ Juditl a quasi novelty in these parts




71 


MUSIC IN NOTTINGHAM AND DISTRICT Judging from the prospectuses already issued, the musical 
season promises well. In this city the Sacred Harmonic 
Society announces ‘Samson and Delilah’ (Saint-Saéns), 
‘Hymn of Praise’ (Mendelssohn), and Bralms’s ‘ Requiem * 
and ‘Song of destiny,’ in addition to Elgar’s ‘ Dream of 
Gerontir The same Society will give two orchestral

concerts, at which the principal items will be Beethoven’s

eighth Symphony and that by Schubert in C, with Delius’s 
‘Brigg Fair,’ Elgar’s ‘In the South’ overture, Jarnefelt’s 
‘ Preludium,’ and an Intermezzo by Sibelius




1S


MUSIC IN SHEFFIELD AND DISTRI 


662 THE


NEW WELSH WORKS AT THE 


EISTEDDFOD


LLANGOLLEN


I, 1908


BERLIN. 


RRUSSELS


BUSSANG


AUTEREF


ASSEL


HEMNITZ


RESDEN. 


M4


XUM


I, 1908, 663


ELBING


HALLE


HEIDELBERG. 


JENAfestival concert, a festival church-service, the dedication

University buildings, X&c.— including  Liszt’s 
symphonic poem ‘Festklange,’ which was originally 
produced at Jena on February 3, 1861 ; Beethoven’s choral 
symphony, with words by the University’s most famous 
professor, Schiller; Handel’s ‘Zadok, the priest’; and a 
Sonata for double wind-quartet, by Giovanni Gabrieli, the 
16th century Venetian composer. 
irrepressible Max Reger, whose uncanny fecundity is one of 
intellectual marvels of the present day, were also 
included in the festival scheme. These novelties by 
Germany’s strongest present-day representative of abstract 
music were settings of the 1ooth Psalm for chorus and 
rchestra—of which, however, only the first movement was 
sung—and of a ‘Weihegesang’ (consecration-song) for 
contralto solo, chorus of men’s voices, and wind instruments, 
he words by Professor Otto Liebmann. Professor Reger 
as created a doctor by the University, an honour which, 
I musicians, was before him enjoyed by Robert 
amann and Hans von Biilow

new




ILAN 


ARIS. 


RAGUE. 


ROME


1 . 5 


I, 1908purpose

F. W The nine sonatas for the organ by S. de Lange, A! /KINS, J. E.—*‘ Tears, idle tears.” Four-part Song 
” re fine works, but they pany rank with i (No. * ee . Novello’s Part-Song Book.) 3d. 
wor berger and Merk a» COCK, GILBERT A.—‘‘ There be none of beauty’ 
I x 3 \ annot do better than study Stainer’s | + Gaagntess. : Part-Song for A.T.T.1 (No. 455. Zh 
Organ Primer (Novello), as, in addition to being a treatise, it | U7 us.) 2d. 
ives a ripti f the church organ. ACH, J. S.—So there is now no condemnation. From 
I. O I te sonata by Beethoven, men ‘Jesu, priceless treasure.’ (No. S09. Novell 
Ur ISS ; 3 ’ » & lar aS we can ascertal Octavo Choruses. } 3d. 
published ; we will make further enquiries. ANTOCk, GRANVILLE—‘ Wake the serpent not 
. , four-part Song. (No. 788. Zhe Alusica 1 
E. | .—The of which you send t opening : é 
ar ee in t e not. D, as I LAIR, HUGH Benedictus in E flat. (N 737 
writter Novello’s Parish Choir Book.) 3d. 
M \ Bayt I al Wi probat y | gg R, A. HERBERT—* Auf Wiedersehen.” | 
next year i en Gecided on Violin and Pianoforte. 2s. 
D know * Guild HITS, JOIIN—3o0 Caprices. For the Violin only. 5 
Nor D e gulled by G F 
w. Vv We reg “ give im YLERIDGE-TAYLOR, S.—** The Pixies.” Par 
c er Song for 5s.s.A. No. 381. Novello’s Trios, f 
Female Voices.) 3d. 
—— ‘*Encinctured with a twine of leaves.” Part-Song f 
CONTENTS .A. (No. 352. Novello’s Trios, &c., for Fen 
\ oices. } 3d. 
— ‘“*What can lambkins do.” Part-Song for 
No. 383. Novello’s Trios, &c., for Female Voices 
* Sea-drif Rhapsody for 8 voices A.A. 
No. 1076. Novello’s Part-Song Book.) 6d. 
i} ‘ON, Sanctus, in D. 3d. 
( P . 
E LGAR, E. Follow Colours. Arra 
+ Military Band by ¢ cepesle A. STRETTON. 3s 
“~OUN! “ad [. ‘Fat A selection for Con 
I as ar Edited and arranged by 
hye “HUGH t Song. No. 2, 
ntralto or Baritone 2 
kK ING, OLIV ER—* Hark ! hark, my soul Ant 
“. (No. 141. Novello’s Short Anthems.) 14d 
I ENNARD, LADY BARRETT—‘ dold I 
~ Song For Baritone > 
[ ISH MAN, G \ Cricket Song. cal Mar wit 
—< Chorus N The Chester Serie nis 
| LOYD, C. H An Eton Memorial March. Wor 
A. C. AINGI No *. Eton School Sor

DURING




EKKEL, GU 


OOTHAM, ¢ 


Y RIL B 


THE LAST MONTH. 


05 


05 


03 


90 0


06 


00


GAR 


SU L 


BACH 


CLAR


XUM


A. B. 


FOLK-SONGS FROM SOMERSET. 


T' INIC SOL-FA PUBLICATIONS : 


ICNAUGH 


JOHANNES BRAHM


THE H. W. GRAY CO., NEW YORK. 


TOKOVSKI


HYMN TUNES 


LROLS


GEORGE C. MARTIN


CLASSIFIED LIS 


ONE


POPULAR C


HUNDRED


ANTATAS


THE SCHOOL MUSIC REVIEW, 


MENDELSSOHN’S ORATORIO THE SCHOOL MUSIC REVIEW FOR OCTOBE


; : 3 : - [THEORY QUESTIONS AND A> I 


4 . 4 4 


ADORAMUS TE 


FOR ORCHESTRA 


HUGH BLAIR 


\WELCOME. SPRING CULTIVATION AND PRESERVATION OF 


WITH EXERCISES FOR THE USE OF SCHOOLS, CHOIRS 


FRANZ SCHUBERT. JAMES BATES


EXTRACT FROM THE PREFACE


COURSES


RECEN


L.R.A.M. | 


CHOIR E> 


ALWAYS


S!: M 


TENO 


(CONT! 


RGA


KUM


THE


1908


605 


MUSIC BY MAIL. 


THE OLD FIRM


P. CONACHER & CO. 


SPRINGWOOD WORKS, 


HUDDERSFIELD


TWO GOLD MEDALS


NICHOLSON AND CO. 


ORGAN BUILDERS, 


PALACE YARD, WORCESTER. 


(ESTABLISHED 1541


PIANO PEDALS


EVERY ORGANIST


NORMAN & BE ARD'S P ENT 


PNEUMATIC PEDAL AT TACHMENT FOR THE 


PIANO 


NORMAN & BI ARD, I ; 


MALKIN’S


PATENT PEDAL


TTACH MENT


EKCTLY NOISELESS


THE


CANTATAS, ANTHEMS


ALSO


METHODIST


FESTIVAL MUSIC FOR ANNIVERSARIES, 


WIE DE RSEHEN


A. HE RBE RT BREWER


AUF


VIOLIN AND


ARRA EME \ NCI AND PIANOFORTI ™ 


MORRIS DANCE TUNES


COLLECTED FROM TRADITIONAL 


SOURCES 


AND ARRANGED 


CECIL J. SHARP 


HERBERT C


AND 


TWO


MACILWAINE 


SETS. 


PRICE TWO SHILLINGS EACH NET


SET I. 


SET II. 


THE MORRIS BOOK


A HISTORY OF MORRIS DANCING


ITH A DESCRIPTION OF 


ELEVEN DANCES 


AS PERFORMED BY 


THE MORRIS-MEN OF ENGLAND 


BY


CECIL J. SHARP 


HERBERT C. MACILWAINE


6009


ADVENT HYMN


7 ' \ CANTATA FOR ADVENT 


+H SOPRANO SOLO AND CHORUS 


KUM


COMPOSITIONS


EDWARD BUNNETT, M1


ANTHEMS


HOLY COMMUNION 


; SET TO MUSIC BY 


I ( (A.D. 1550). 


BY 


BASIL HARWOOD. 


— . EXTRACT FROM PREFACE, 


~ BY 


T. MEE PATTISON. 


: STORY OF THE ADVENTOER JESUS 


E. W. LEACHMAN 


J]. STAINER. 


P P I 


BY


VEL! ANI


FO


THE


THE TEN VIRGINS AN OPERA


A SACRED CANTATA


ALFRED R. GAUL. CH. GOUNOD


. ‘ I ND AR AN


S W


) T ; > DR HUMOROUS PART-SONGS. 


THE WORDS SELECTED FROM HOLY SCRIPTURE DESCRIPTIVE CHORUSES 


\ W N : N 


XUM


THE MENDELSSOHN CENTENARY


SELECTED LIST OF CHORAL WORKS 


ST. PAUL ATHALIE


ELIJAH 


PSALMS. 


1 5 G


MAN IS


AVE MAR 


DA NOBIS 


MENDELSSOHN CENTENARY


NIGHT


WALPURGIS


FIRST 


BARTHOLOMEW


THE 


WILLIAM


LONDON NOVELLO


XUM


AND COMPANY


I, 1908, 673


HEAR MY PRAYER


_ FOR MALE VOICES 


(EDIPUS AT COLONOS


BARTHOLOMEW


TO THE SONS OF ART


LIMITED


74 THE


HYMN OF FAITH 


CANTATA 


EDWARD ELGA 


IVOR ATKINS 


AN


IORNI 


LI 


IN I 


I M 


IIN AZ} I


MUSICAL


WORCESTER FESTIVAI


THERE IS


MOTET


PEACE


FOR SOPRANO AND BASS SOLI, CHORI 


ORCHESTRA 


C. HUBERT H. PARRY. 


I S 5 N 


W IS S I 


THE TIMES 


DAILY EL.I RAPH. 


MORNING POST 


DAILY NEWS 


I L ‘ 


DAILY CHRONICLE 


YORKSHIRE POS1 


ARDIAN 


N ELI


AND COMPANY


WORCESTER FESTIVAI : WORCESTER FESTIVAI 


ENGLAND, MY ENGLAND 


THE SONG 


: TH DAILY TELEGRAPH 


- 7 - : > I ; 


RNING POST 


2 VESTERN DAII RESS 


I I ! I BIRMINGHAM GAZETTI 


D: I 


] N ( I 


——— FUGAI \NALY SIS 


; MUSICAL FORM I 


“THE TENOR VOICE AND ITS TRAINING. 


- —- AVIDSON PALMER, M I ( I 


YORKSHIRE POST F \ > 


KUM


676 THE


MUSICAL


1908


EVERYMAN


CANTATA FOUNDED UPON THE OLD MORALITY PLAS


SOPRANO, CONTRALTO


HE WO


\ NEW 


TENOR


AND


COMPOSED B


H. WALFORD


EE


FOR


SOLI, CHORUS AND ORCHESTRA


DAVIES


SHILLINGS NET


BASS


DAILY TELEGRAPH 


I I I 1 " 


WESTMINSTER AZETTI 


I I 


I YORKSHIRE POST. 


MANCHESTER GUARDIAN 


SHEFFIELD DAILY TELEGRAPI 


HUDDERSFIELD EXAMINER


GE


A HUNT 


AUSREI


THE NAI 


TO THE 


THE WH 


O JESUS 


SAINT R


XUM


FOLK-SONGS OF ENGLAND 


EDITED BY 


CECIL J. SHARP


FOLK-SONGS 


FROM 


DORSET 


COLLECTED BY


H. E. D. HAMMOND


CECIL J. SHARP. 


C TENTS: 


FOLK-SONGS 


EASTERN COUNTIES 


E AND SET TH AN ACCOMPANIMENT, BY 


R. VAUGHAN WILLIAMS. 


( TENT 


SONGS FROM ESSEX. 


SONGS FROM NORFOLK. 


SONGS FROM CAMBRIDGESHIRE 


GERMAN FOLK-SONG 


ARRANGED FOR S.A.T.B


BY


JOHANNES BRAHMS


ENGLISH 


AND GERMAN WORDS. 


W. G. ROTHERY


TENDER AND PURE, O LOVE, ART THOU (V EDLER 


A EITEN) 


TO THE HOLY MARTYR EMMERANO (\ EILIGE


O JESUS, TENDER SHEPHERD, HEAR (A


NOVELLO’S SCHOOL SONGS 


FOLK-SONGS OF ENGLAND


CECIL J. SHARP. 


FOLK-SONGS


SOMERSET


FOLK-SONG AIRS 


COLLECTED IN SOMERSET AND ARRANGED FO


SONGS FROM


PIANOFORTI 


CECIL J


BY 


SHARP


BOOK


VEIL A


THE PRINCESS


TENNYSON


SET TO MUSIC FOR FEMALE VOICES 


BY 


GUSTAV VON HOLST 


S.S.A.A.). (> 


BLISHED


FOUR PART-SONGS


FOR MIXED VOICES


EDWARD ELGAR


REVEILLE


THE


PART-SONG FOR T 


BRET HARTI


EDWARD ELGAR


FOLLOW THE COLOURS


FOR SOLDIERS


MARCHING SONG


UL COMPANY 


EDWARD ELGAR


ODI TO A NIGHTINGALE


HIN KEATS


THE KNAVE OF HEARTS 


AN OPERETTA FOR CHILDREN


IN THREE ACTS


I V


RTHUR SOMERVELL


TWO NEW SONGS 


BY 


R. H. WALTHEW. 


THE WORDS BY 


» DONALDSON. 


THE HEART’S AWAKENING


SONG 


THE WORDS WRITTEN BY 


FLORENCE HOARE 


HE MUSIC COMPOSED BY


ALBERT W. KETELBEY


TDTIT 


PRUTH 


SONG 


CON TRALTO OR BARITON 


THE S BY 


BEN JONSON 


VICTOR G. BOOTH. 


ON "ERT SONGS 


DR. HOLLOWAY


MY NATIVE HOMI


THE BONNIE BLUE SEA


ARTHUR S


A VALL


WITH N


CROVE'S DICTIONARY OF MUSIC AND


THE 


NEW 


AND REVISED 


EDITION OF THIS 


INVALUABLE WORK, EDITED BY 


J. AJFULLER MAITLAND, M.A., CAN NOW 


BE PURCHASED ON A SYSTEM OF


PAYMENT BY INSTALMENTS


SPREAD OVER 12 TO 15 MONTHS


TECHN IQU I FOR THE PIANOFORTE


EXP RE SSION WILLEM COENEN


WITH NUMEROUS MUSICAL EXAMPLES FROM THE 


WORKS OF THE GREAT MASTERS


XUM


68 THE 


MUSICAL


COMPOSITIONS FOR THE ORGAN


EDWIN H. LEMARE


SONG OF


JOHN E


WEST


ORIGINAL COMPOSITIONS 


ORGAN


BY


EDWIN H. LEMARE


RAL POEM 


I ESTRAUM 


RIN SON I S 


ANDANTINO IN D FLAT 


FOR THE ORGAN


EDWIN H. LEMARE 


RANGEMENT FOR TWO VIOLINS AND PIANOFORTE 


NE W WORKS FOR ORGAN, 


UDWIG BOSLE1


ORGAN 


INTRODUCTION, 


ADESTE FIDELES 


FOR THE ORGAN


HOLLOWAY


COMPOSITION 


HOLLOWAY


RTHUR S


IUS.1


TWELVE 


DANCE MELODIES 


(ZWOLF TANZWEISEN


PIANOFORTE DUET


WICKENHAUSSER


RICHARD


PIANOFORTE PIECE 


DR. ARTHUR S. HOLLOWAY


LOVE’S APPEAL 


INTERMEZZO


SOUVENIR DE PRINTEMPS


COMPOSED BY


JOSEPH HOLBROOKE. 


IN FOUR MOVEMENTS


GAVOTTE MINUET 


SLUMBER SONG MOTO PERPETI


COMI D BY 


A. VON AHN CARSE. 


GRADUATED 


DAILY EXERCISES 


V IOL IN


HENL EY


WIL LIAM 


POSITIONS


IL—FORTY EXERCISES IN THE FIRST TO FIFTH 


POSITIONS


MEVERDE


VIC


HAWK


NEW AND POPULAR ORCHESTRAL MUSIC


. D \ , W D 


I 1 M 4 ( 1 ) 


(5 | I 


} N 5 


I \ 4 I I 


SUITES FOR ORCHESTRAL CONCERTS. 


BALLET RUSSE BALLET RUSSE 


SUITI PART I. SUITE, PART II 


ALEXANDRE 1 “IGINI \LEXANDP} LUIGINI 


S 4 P.S E.I PC E S P.S KI 


S M I N 


. ( D: D


W I I 


W M 


\ I | S N 


M ( N ‘ 


I D


\I I | 


I W


I R ( . 


M M I 


N M A. I I 


I I ‘ 


—T N 


KUM


(NOVELLO'S EDITIONS.) 


PIANOFORTE. SINGING


LOCAL CENTRE. —INTERMEDIATE GRADE. LOCAL CENTRE.—INTERMEDIATE GRADE, 


N I S \ N ) 


¢ MEZZO-SOPRANO 


5 ONTRALTO 


, ‘ BARITONI 


N | P ( E 30 I N >, 21 


N : P 


CENTRE.—ADVANCED GRADI : : 


I RAS 


N ( , I N 


: H \ S )CN I 


S H


LOCAL CENTRE.—AD\VANCED GRADE


SOPRAN( 


N I I I 


I } U N 


MEZZ SOPRAN( 


1 S 


" N S N 


CONT LL 


( I N 


| 1 - I 


N ~ _ 


. (N S \ 


N 4 WV WW ) D 


! RITONI 


I I I


SCHOOL EXAMINATIONS PRIMARY 


SOPRANO AND MEZZO-SOPRANO 


EXAMINATION ELEMENTARY 


SOPRANO 


( I N 


MEZ SO! ‘\ 


LOC 


LO‘ 


LO 


EXAMINATIONS.—ELEMENTARY ( THOMAS F, DUNHILL. 


BARITONE, NE


BARI 


BASS. 


CLASS SINGING EXAMINATION. DES MONTAGNES BLEUES


HARMONY. VIOLIN AND PIANOFORTE 


LOCAL CENTRE.—ADVANCED GRADE. CAVATINA IN G 


; I ( I H ‘ __- 


S M 4 E \ P


EXAMINATIONS.—LOWER DIVISION 


EXAMINATIONS HIGHER DIVISION LYRIC PIECES


_ VIOLIN. HANS SITT 


SCHOOL EXAMINATIONS PRIMARY\ | , P 


5 N ( I 38) 


OE MELODIOUS TECHNIQUE


KUM


LIZA LEHMANN. “ NONSENSE SONGS


W DOTH THE LITTLE CROCODILE 


FURY SAID %O A MOUS 


I YOU ARE OLD, FATHER 


‘ LIAM 


) YOUR LITTLE


SPEAK ROT HLY T


RESA DEL “GLORIA


4 W CA


[THROUGH LOVE'S INFINITE ASCENT (1 


INK SINK, RED SUN, INTO THE WEST (¢ 


LEI LOHR. “GARDEN SONGS


MANN 


PALE STA RE THI 


VE’S MESSENGERS 


DREAMED WE WALKED IN


SES


ARDEN. 


LOHR. ‘SONGS OF


I AN OCEAN 


HERMANN THE


{ IN ND LIEBI \ 


LIZA LEHMANN


COBWEB CASTLE


THREE DANCES


NORSELAND


SIX


JUARTE WILL YOU WALK A LITTLE FASTE 


I'VE A COTTAGE DOWN OUT DEVON WAY ( 


DEAR, IS IT NOTHING ALL THE YEARS 


GOLDEN DAWN


ARDEN


TIME WAS I ROVED THE MOUNTAINS. 


EYES THAT USED TO GAZE IN MINE. 


YOUTH HAS A HAPPY TREAD


SONGS


5. A LEGEND. 


EVENSONG. 


MY LADY'S JESTER


N DAN( PASTORAL DANCI . MERRYMAKERS’ DANCE. 


RNPII MINUET


THREE DANCES


RPROY


“MERRIE ENGLAND


RUSTIC DANCE. ‘ IG 


1 “TOM JONES


I ANC] GAVOTTEI JIG 


M )] \LERIE WHITE “FROM THE IONIAN SEA.” 


NA LAND OF THE ALMOND BLOSSOM. 


OHN ANSEL! THREE IRISH DANCES. 


FLOWER SUITI 


DANCE OF THI 


‘FAIRY


GNOME


SUITE


DANCI


BENYON


DANCES


DANCE OF


OF THREE 


PANSIES 


OF THREE


DANCES. 


SPRITE DANCE. 


NEW 


MAY BE


YORK AND


AND HAD OF


MELBOURNE. 


ALL


MUSIC SELLERS


BLE


AYH


BLESSED BE THE LORD GOD OF ISRAEL


ANTHEM FOR CHRISTMASTIDE


_- ——37"5 7-9-9 


UM


BLESSED BE THE LORD GOD OF ISRAEL


| BE 2 SS SS EES ES = SS BSS ST 


—A'5 - +—-— ' — — ————_ —_——— 


BLESSED BE THE LORD GOD OF ISKAEL


SS


11118 


NI


TTT


XUM


BLESSE 


D BE THE LORD GOD OF ISRAEL


OPS SS SEL 


LK


HEY HH = CL PUP


SH+H


UM


BLESSED BE THE LORD GOD OF ISRAEL


SATE ISR RS TTR SOMES TSN T -_— = 


BLESSED BE THE LORD GOD OF ISRAEL


SS SS SS SS | = 


| FF 


_——_— - - —_


BLESSED BE THE LORD GOD OF ISRAEL


HH 


HAL 


L +H 


HH


—— = = SS SS SS SS SS 


SSS SS SSS SS =E: 


<= SS 


<= [SS JS — 


4 —— —H


SS


THE


A GRE 


UM


EXTRA SUPPLEMENT


COMMENCED AUGUST, 1908


THE ROYAL NATIONAL EISTEDDFOD. 


LLANGOLLEN, SEPTEMBER I TO 4


COMPETED. ENTERED. 


THE CHIEF CHORAL COMPETITION. 


XUM


SALTAIRE. 


NEW BRIGHTON (LIVERPOOL) EISTEDDFOD. 


BLACKPOOL. 


BALLARAT


DATES OF COMPETITIONS


LONDON COMPETITIONS


MED


MISS AG! 


MADAME 


MR. CHA 


MR. DAI


CHILDRE 


A COUR


LOND( 


SYM! 


MONDAY. 


MONDAY, 


MONDAY. 