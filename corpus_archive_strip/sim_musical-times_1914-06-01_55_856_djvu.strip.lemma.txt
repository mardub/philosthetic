


 the 


 ROY AL ACADE 


 PRINCE CONSORT ROAD , S 


 complete MUSICAL E duc : 


 the ROYAL — G 


 I ERM 


 ROYAL CHORAL SOCIETY . | FIRST performance 


 MIDSUMMER HALF - term b 


 entrance examination , 


 CL 


 weekly orchestral practice be conduct 


 school of 


 session 1913 - 1914 


 MUSICAL time 


 AND SINGING - class circular . 


 found in 1844 


 publish on the first of every month 


 JUNE 1 , 1914 


 14—1915 


 ROYAL 


 MANCHESTER COLLEGE of MUSIC . 


 M. ANC HESTER SCHOOL of MUSIC 


 E of ORGANISTS 


 MUSIC . the teaching of teacher . 


 362 the MUSICAL 


 the 


 ASSOCIATED BOARD 


 of the R.A.M. and R.C.M. 


 for local examination in MUSIC 


 PIANOFORTE 


 HALL , NEW 


 GCHUMANN STUDIOS 


 EOLIAN 


 BOND STREET , W 


 COMPLETE training 


 principal : MME 


 for pianist 


 AMINA GOODWIN 


 SCHUMANN SCHOOLS 


 the " AMINA GOODWIN SCHOLARSHIP . 


 ALSO 


 A " MOZART " SC HOLARSHIP for child 


 university of 


 faculty of 


 MANCHESTER . 


 MUSIC 


 7 , MORTIMER STREET , REGENT STREET , 


 LONDON , W 


 THE SECRET 


 LONDON COLLEGE of MUSIC . 


 T. WEEKES 


 incorporate guild of CHURCH 


 musician 


 ASSOCIATE ( A.1.G.C.M. ) , licentiate ( L.1.G.C.M. ) , FEL 


 REGISTER of ORGAN VACANCIES . 


 V VICTORIA COL L EGE of MUSIC , 


 LONDON . 


 incorporate 1891 . 


 SI 17 , YORK PLACE , W 


 } MR . THORPE BATES 


 1914 


 SALES 


 2 I WEDDINGS and SPECIAL service 


 CHARLES HARPER 


 ( SOLO ALTO , ST . MARY , STOKE NEWINGTON 


 | WINIFRED MARWOOD 


 BARITONE 


 of PIANOFORTE - playing . MR . G -EORGE PARKER _ 


 ‘ how to acquire ease of voice production . 


 RALTO 


 A.M 


 r 


 TELL 


 OPRANO 


 ONDON 


 tion 


 365 


 CANTATA — ORATORIO — BALLAD 


 ERNEST COSTA 


 BASS - BARITONE 


 LAND ST 


 MR . WILLIAM COLEMAN 


 the MANCHESTER BASS ) . 


 M R. 


 CHARLESWORTH GEORGE 


 B ASS - BARITONE ) . 


 MR . LEONARD HUBBARD 


 MR . W. H. BREARE 


 TO student and the profession . 


 MESSIAH 


 MISS ETHEL VISICK 


 receive pupil for pianoforte playing . 


 the LONDON COLLSSS for chorister 


 COMPOSERS ’ MSS . 


 DR . A. EAGLEFIELD HULL 


 of MUSICAL composition 


 L.R.A.M. ( paper work ) . 


 late success : — 


 L.R.A.M. EXAMS . , 1010 - 13 


 A.R.C.M. ( paper work ) . 


 F.R.C.O. SPECIALIST in CORRESPONDENCE 


 THI 


 5999 


 coaching for degree . I.R.A.M. , 97 - 101 INE \ TANTED , ORGANIST and CHOIRMASTER 


 GE . 


 ¢ — AR 


 _ _ _ 


 etary 


 ASTER 


 HOIR 


 SALE . 


 the MUSICAL 


 MINIATURE 


 OSTROVSKY APPLIANCE 


 N.W 


 T » 19 


 48 , SOUTHFIELD ROAD , OXFORD . SIX EASY piece 


 126 , HOLT ROAD , LIVERPOOL . for VIOLIN and PIANOFORTE 


 BY 


 VIOLIN and P IANOFORTE 


 compose BY content 


 TRINITY COLLEGE 


 institute 1872 


 WORCESTER 


 IVELLO 'S 


 GRADED PIANOFORTE 


 PLAYER - PIANOS 


 ation 


 10 


 arket 


 ON , W 


 M.A 


 1 chant 


 cree 


 rd 


 CHESTRA 


 clergy 


 369 


 AND SINGING - class circular 


 JUNE 1 , 1914HENRI VERBRUGGHEN 

 when Mr. Daniel Mayer conceive the idea 
 of hold a Beethoven Festival at Queen ’s Hall 
 . this spring , he have first to consider to whom the 
 chief direction of such an important musical 
 enterprise should be entrust . the scheme of 
 the five concert include all the nine Symphonies , 
 the five Pianoforte concerto , the Violin concerto , 
 and various vocal work , and for their due 
 performance the London Symphony Orchestra , the 
 leeds Philharmonic Choir , some of the great 
 live pianist , and other soloist be engage . 
 - as we all know Mr. Mayer ’s choice fall upon 
 Henri Verbrugghen , who , as solo violinist , quartet 
 leader , and conductor , have devote a considerable 
 part of his artistic life to the study of all Beethoven ’s 
 important instrumental composition except the 
 pianoforte sonata 

 it isa pleasure to record that Mr. Verbrugghen 
 prove fully equal to the occasion , which be 
 probably the most important one in his successful 
 artistic career . special interest be create in his 
 interpretation of the Symphonies , because it be 
 know that he would introduce certain bold device 
 of his own in order to adapt the orchestration to 
 the balance of tone in a large modern orchestra . 
 some of the detail of these device be explain 
 below , but it may be say at once that they be 
 conceive and carry out in a spirit of reverence 
 and respect for the great composer , and with no 
 idea of improve his masterpiece 

 1873 . he be the only child of a belgian 
 merchant . his mother have considerable musical 

 culty , which find happy vent in a devoted 
 application to Beethoven ’s chamber music . the 
 home be the resort of many well - know artist , 
 and thus the bias of the boy to the classical 
 master be initiate . Henri take eagerly to the 
 violin , and make a public appearance as a soloist 
 when he be only eight year of age . later 
 his parent desire to shape his education towards 
 medicine rather than music , but when at the age 
 of thirteen a decision have to be make , the advice 
 of Joseph Wieniawski , and Gevaert ( the director 
 of the Brussels Cénservatoire ) , be seek , with the 
 result that music win the day , and Henri enter 
 the Conservatoire and become a pupil of 
 Hubay , and , later , of Ysaye , who take unusual 
 mere in his musical development . whilst at 
 the Conservatoire he win many prize , and when 

 be sixteen he make an important step 
 forward in turn special attention to the study 
 orchestration . soon after he be admit 

 and join the Scottish Orchestra , which 
 be then found by George Henschel . he 
 also during this summer lead the _ orchestra 

 under Jules Riviere at Llandudno , and later he 
 become a member of the Lamoureux Orchestra at 
 Paris . during the three follow year he be 
 deputy - conductor at Llandudno , and it be this 
 experience that finally determine he to adopt 
 the career of a conductor . he become director of 
 music at Colwyn Bay ( a position he retain 
 |for four year ) , and he return to the scottish 
 Orchestra , which be then under Wilhelm Kes 
 ( the founder of the Concertgebouw Orchestra at 
 Amsterdam , now conduct by Mengelberg ) , and in 
 1902 he be appoint leader and deputy - conductor 
 of the Scottish Orchestra , under Dr. ( now Sir 
 | Frederic ) Cowen . in the summer of that year 
 and for three succeed year he be leader of 
 | the Queen ’s Hall Orchestra during the Promenade 
 season . after that period he centre attention 
 } on Glasgow , where at the Athenzum , which be 
 ! one of the most important musical educational 
 institution in Scotland , he be chief violin professor , 
 } and in 1911 he succeed Dr. Coward as conductor 
 | of the Glasgow Choral Union . during recent year 
 |he have conduct symphony concert at Brussels , 
 | Berlin , Munich , and St. Petersburg . notwith- 
 | stand all these varied and widespread activity , 
 he have form a string quartet party , which have 
 appear in many place in Scotland and 
 jelsewhere . the present speciality of this party 
 li the sixteen quartet of Beethoven , the whole 
 of which have lately be perform at chamber 
 music concert at Glasgow and Edinburgh 

 one of the feature of the Athenzeum music 
 school be the orchestral class of forty string player . 
 Mr. Verbrugghen find it perfectly possible to train 
 this class to a high degree of perfection that can not 
 be reach by amateur player on wind instrument . 
 3ut he think it desirable that his player should 
 not restrict their repertory to piece compose for 
 string only , and so for the purpose of study the 
 string part of symphony and other work write 
 for full orchestra be study and the miss part 
 be fill in on the pianoforte . for a few year an 
 operatic class be a success , but it have to be 
 abandon because it be find to absorb dispro- 
 portionately the energy of the school 

 BEETHOVEN ’s symphony 

 the idea of double wind part in Beethoven ’s 
 symphony in order to balance the mass of 
 string now employ in a modern full orchestra be 
 not a new one . it be an obvious apparent escape 
 from a difficulty ; but it be equally obvious that if 
 it be apply crudely it lead to more difficulty than 
 it remove . ' there be numerous passage in the 
 symphony that clearly can only be play properly 

 amember of the Society of Conservatoire 




 37the MUSICAL TIMES.—June 1 , 1914 

 condition , to give scope to his _ individual 
 temperament . ' this consideration alone show that 
 the whole matter must be deal with scientifically . 
 it might seem sufficient to rule that the extra 
 instrument be to be use only in Zxééis . but 
 Mr. Verbrugghen go far than this . he 
 study the physical convenience of his principal 
 wind player whoimmediately after the characteristic 
 gust of forfe and sforsando , in which Beethoven 
 often indulge , be call upon to play delicately : 
 a transition of effort that more or less embarrass 
 the most skilful performer 

 we now give a few example of Mr. Verbrugghen ’s 
 method of treatment 




 = SS SS SS SS 


 TS 


 IN ENGLAND 


 BOUR 


 the B 


 SE 


 MORE 


 the music and MUSICAL INSTRU ment 


 of the bible 


 1914 ) 


 _ — _ _ _ _ 


 , 2200 


 10 


 iv 


 i000 b.c 


 art 


 1200 b.c 


 7 , 94 


 152 


 378 


 5555 , TT SF 


 IA 


 the MUSICAL ti 


 SOME musical epitaph . 


 w,2 k. S 


 380 


 diction . 


 _ = — _ CO 


 ‘ A. & M 


 385 


 386 


 1914 


 MR . FRED COZENS 


 ORGAN recital 


 appointment 


 CEN 


 383 


 — — — — 477 


 W.M 


 the MUSICAL TI 


 MES.—JUNE 1 , 1914 


 book receive 


 403 


 1674 


 the manuscript of CLEMENTI . 


 TO the editor of ' the MUSICAL TIMES , ' 


 WILLIAM 


 II , CUMMINGS 


 A SWELL - BOX for the TUBA . 


 TO THI AL TIMES 


 BURR 


 HUDDERSFIELD organist 


 399public appearance be make as Richard Coeur de Lion in 
 the original production of Sullivan ’s ' Ivanhoe ' in 1891 

 follow a conductor as painstaking and inspiring as Si 
 Henry Wood , who have be compel by his many duty 
 to relinquish this post ; but Mr. Balling soon acquire th 
 confidence of his singer , and the concert have never bee 
 more exhilarating and uniformly enjoyable . the princip 
 choral work be Bach ’s Cantata , ' sleeper , wake , ' and 
 double - chorus , ' Nun ist das Heil , ' Brahms ’s ' Song of destiny , 
 Dvorak ’s ' the Spectre ’s Bride , ' and Hamilton Harty : 
 * Mystic Trumpeter ' ( conduct by the composer ) , all of whit 
 be sing with a spirit and force that never slacken , whil 
 the volume of tone produce by the choir of some 350 voi 
 ' ; ; |(a different body on each occasion ) be excellent . th 
 performance of * Der Rosenkavalier . programme be not confine to choral music , and one coulé 
 AMBROSE AUSTIN , on May 14 , at the age of eighty - seven . | not but realise the value of the object - lesson when the chor 
 for thirty - two year manager of the old St. James ’s Hall , | be listen ( and most of they do listen attentive 
 Piccadilly , Mr. Austin be a well - know personality . to Beethoven ’s eighth Symphony , the ' Oberon ' a0 
 JAMES ALEXANDER BROWNE , on April 25 , in his seventy- ' Meistersinger ' overture , and the ' Thalassa ' Symphony 

 assistant to Sir Arthur Sullivan and to Dr. A. L. Peace , 
 both of whom think highly of his gift 




 first stage of the season 


 the GERMAN OPERAS 


 ITALIAN OPERA 


 the state performance 


 music of ELIZABETHAN CHOIRBOY play 


 the 


 ADOLF HENSELT : in MEMORIAM 


 MR . ARTHUR HERVEY ’s opera 


 the IMPERIAL CHOIR 


 the SOUTH WALES festivalenjoyable programme 

 Beethoven ’s sev 

 style modern , so that all requirement of modern opera be | Sir Frederic Cowen ’s ' the veil ' be give by the Neat 




 SOPRANO 


 ALTO 


 to the conductor and member of the MAIDSTONE VOCAL UNION . 


 ~~ : an unaccompanied part song for § .A.T.B. 


 LOVE WAKES and WEEPS 


 = = SSS SS SSE LSE — — 


 _ _ _ LOVE WAKES and WEEPS 


 — _ { — _ — — 


 @_+5 @ = = 


 ( | $ 34 


 LOVE WAKES and WEEPS 


 # 3 — — — — — = " e be $ 4 


 $ 722 


 LUVE WAKES and WEEPS 


 xn 


 LOVE WAKES and WEEPS 


 POPU : 


 399 


 TRINITY COLLEGE of MUSICPOPULAR CLASSICAL CONCERTS at SWINDON : 
 an experiment in MUSICAL education 

 on April 20 a concert in the Mechanics ’ Institute at 
 Swindon bring to an end a most successful series of 
 Classical Concerts which have be give there during the 
 past winter at genuinely popular price before a genuinely 
 working - class audience . the concert be organize 
 by Mr. Felix Schuster , who give a similar series two year 
 220 , at the request of the Swindon branch of the Workers ’ 
 Educational Association . throughout the season the 
 audience have average from four to five hundred , and the 
 gympathy and intelligent appreciation of the music show 
 by all who hear it have be quite remarkable . the 
 programme have include Violin sonata by Beethoven , 
 Handel , Tartini , César Franck , and Ludwig Thuille , 
 Brahms ’s Pianoforte quartet in a major ( Op . 26 ) , Dvorak ’s 
 Concerto for violoncello and his ' Dumka ' Trio , and a Bach 
 Concerto for two violin . among the vocal item have 
 be Arias by Mozart and Verdi , song by Brahms , 
 Schumann , Couperin , and Gluck , and Vaughan Williams ’s 
 song - cycle ' on Wenlock edge . ' Beethoven ’s Pianoforte 
 snata in A flat ( Op . 110 ) meet with a _ peculiarly 
 enthusiastic reception . on March g Mr. Percy Such ’s 
 String Orchestra play , and the programme consist of 
 Mozart ’s Eine Kleine Nachtmusik and Concerto for pianoforte 
 in E flat ( no . 9 ) ; Bach ’s Concerto for violin in e major ; 
 W. Richter ’s Abendgesang ; and Percy Grainger ’s Mock 
 Morris 

 besides Mr. and Mrs. Schuster , the performer who have 
 generously give help in this series of concert have include 
 Miss Letty Maitland , Mr. and Mrs. Percy Such , Mr. Percy 
 Sharman , Mr. Steuart Wilson , and Mr. A. P. Fachiri . 
 altogether the experiment have be most successful , and it 
 be earnestly hope that it will be repeat next year , 
 and that the example of Swindon will be follow 
 elsewhere 




 the L.C.C. CHORAL UNIONSLondon Concerts 

 the BEETHOVEN FESTIVAL 

 two concert of this excellent series organize by Mr. 
 Daniel Mayer be report in our last issue . at the third 
 concert , on the afternoon of April 22 , the fourth and fifth 
 Symphonies be give , Herr Max Pauer play the fourth 
 Pianoforte concerto , and Mr. Paul Reimers give the song- 
 cycle ' An die ferne Geliebte . " the next two Symphonies 
 follow on the evening of April 23 ; Zimbalist play the 
 Violin concerto and Herr van Rooy sing the Aria ' Mit 
 Madeln sich vertragen . ' there be no need to extol the per- 
 formance , which never descend below the high artistic 
 grade . the climax of the Festival be reach on the 
 evening of April 25 , when the Leeds Philharmonic Choir 
 of 200 voice give magnificent assistance in the Choral 
 Symphony and the choral work ' a calm sea and prosperous 
 voyage ' ( conduct by Mr. H. A. Fricker ) . Miss Ada 
 Forrest , Miss Tilly Koenen , Mr. Reimers , and Herr 
 van Rooy as soloist help to sustain the vitality and 
 attraction of the performance . the third Pianoforte 
 concerto , with Mr. Arthur Rubinstein as soloist , and the 
 eighth Symphony complete the programme . Mr. Henri 
 Verbrugghen , whose life and work be deal with in our 
 leading article , conduct throughout the Festival with 
 insight and masterly ability 

 QUEEN ’S HALL ORCHESTRA 

 the Symphony Concert give on April 25 be 
 distinguish by the superb playing of Herr Kreisler in 
 Brahms ’s Violin concerto , a work which he be perhaps well 
 fit to interpret than any other live violinist . the 
 Symphony be Schubert ’s ' unfinished , ' of which Sir Henry 
 Wood conduct an individual performance , exquisite in its 
 tone - grading . Beethoven ’s ' Coriolan ' Overture , and 
 Bach ’s third ' Brandenburg ' Concerto be also in the 
 programme 

 a programme of unusual character be choose for the 
 Endowment Fund Concert give by this Orchestra under 
 Sir Henry Wood on May 9 . it include ' dance in the sun ' 
 by Arnold Bax , Percy Grainger ’s ' Shepherd ’s hey , ' 
 Debussy ’s ' L’Aprés - midi d’un   Faune , ' Stravinsky ’s 
 ' firework , ' and Schumann ’s Pianoforte concerto , superbly 
 play by Herr Dohnanyi . the pianist also appear as 
 composer and conductor in his excellent suite in F sharp 
 minor , the good of last season ’s * Promenade ’ novelty 

 Herr Kreisler give a concert with the assistance of 
 Sir Henry Wood and his Orchestra , at Queen ’s Hall , on 
 May 14 . it be a keen pleasure to renew acquaintance 
 with his beautiful interpretation of Elgar ’s Violin concerto . 
 both in this and in the Concerto of Beethoven he give 
 unbounded delight 

 NEW SYMPHONY ORCHESTRA 




 LONDON SYMPHONY ORCHESTRA 


 \MATEUR ORCHESTRAS 


 CHAMBERMendelssohn ’s ' St. Paul ' be choose for performance by 
 the Handel Society at ( Jueen ’s Hall on May 12 , and unds 
 Dr. Henschel ’s direction some attractive choral singing 
 Miss Eleanore Osborne , Miss Phyllis Lett , Mi 
 Gervase Elwes , and Mr. Thomas Farmer be the soloist 
 be Mr. E. G. Croager 

 a large audience attend the concert of the Wilbeis 
 Queen ’s Hall on 
 Beethoven ’s Violin concerto , with Erna Schulz as solo . 
 be the chief feature of the programme . ; 
 Mr. Walter Hyde , who have not be hear in London for 
 the Orchestra play throughout wit 
 good tone and capability under Herr Sachse ’s direction 

 May 13 , wher 




 concert . 


 401 


 VOCAL , recital 


 PIANOFORTE recital 


 other 


 recital and concert 


 402 


 125 


 by our own correspondent 


 BIRMINGHAMconcert of the season on May 5 , when Elgar ’s * the dr 
 and ' Sea - picture ' ( with Miss Alice Laki 
 * Leonore ’ overture , no . 3 , ¥ 
 audience . the 

 of Gerontius ’ 
 soloist ) , and Beethoven 's 

 iven before a crowded soloist bes 




 BOURNEMOUTH 


 GSI 4 , 


 BRISTOL . 


 I , 1914 


 AND CORNWALL 


 DEVON 


 the three 


 other DEVONSHIRE TOWNS . 


 MC . L. Miss Elena Gerhardt give a recital of Lieder in the Torquay 
 Pavilion on April 25 ; and at a ballad concert in another 
 sat of the town on the same date in aid of Boy Scouts ’ 
 fand Miss Ethel Hook , Messrs. Hastings Wilson and 
 . Hugh Peyton ( vocalist ) , and Miss [ Isabel Hirschfeld 
 lpianoforte ) be the performer . this party also appear 
 t Seaton on April 28 in aid of another charity 

 the programme play by Teignmouth Orchestral Society 
 n April 23 include Beethoven ’s eighth Symphony . Mr. 
 \ , ] . James conduct , and the vocalist be Miss Kate 
 Rostock . on the occasion of the benefit of Mr. Basil 
 Hindenberg , conductor of the Torquay Municipal Orchestra , 
 Sir Henry Wood visit the Pavilion on April 30 and 
 snducte work by Tchaikovsky , Schubert , and Wagner . 
 Mr. Josef Holbrooke play the solo part of his Poem for 
 ianoforte and orchestra , ' Gwyn - ap - Nudd , ' and conduct 
 isSuite ' Dreamland . ' Mr. Harold Bonarius , leader of the 
 mhestra , be the soloist in Max Bruch ’s Concerto , and the 
 wealist be Miss Carrie Tubb and Miss Ethel Toms . 
 Madame Blanche Newcombe give two recital of german 
 and english song at Exeter on May 1 , when she be assist 
 y Mr. Charles Fry and Miss Maude Dixon 

 the Exeter Orchestral Society score a big success at their 
 concert on May 5 , conduct by Dr. Wood . Mrs. Kenyon 
 Miss Dorothy Wood ) be the soloist in a Mendelssohn 
 Pianoforte concerto , and the hand play some interesting 
 piece by Debussy , Hamish MacCunn , Elgar , and Glinka . 
 fr . Sutton Jones be the vocalist . Plympton Wesleyan 
 ichestra , conduct by Mr. W. M. Wickett . give a concert 
 on May 6 , when a Trio be play by Mr. Wyatt ( clarinet ) , 
 Mr. Long ( flute ) , and Miss Chubb ( pianoforte ) 
 Mr. H. Watt - Smy1k conduct a concert of the Ilfracombe 
 Orchestral Socicty , a band of thirty performer lead by 
 Miss A. Clark 




 CORNWALL 


 405district be the performance on May 7 , by Tywardreath 
 Choral Society of the ' Hiawatha ' trilogy . Mr. W. Brennand 
 Smith conduct 

 on May 4 Penzance Orchestral Society , a capable body 
 with artistic aim , play Schubert ’s ' Unfinished ' 
 Symphony and Beethoven ’s ' Egmont ' Overture . Mr. 
 Walter Barnes conduct the forty player , who be lead 
 by Miss May Stewart 

 DUBLIN 

 the College Choral Society give a performance of 
 Cowen ’s ' St. John ’s Eve ' under Dr. Marchant ’s direction . 
 the solo be all take by member of the Society . in 
 connection with the Feis Ceoil an interesting pianoforte and 
 song recital be give by Mr. Frederick Dawson and 
 Mr. Gordon Cleather on May 5 . Mr. Cleather ( who be for 
 several year resident at Dublin ) sing with great refinement 
 and artistic feeling . Mr. C. W. Wilson be the accompanist 

 on Sunday , May 10 , Dr. Esposito start a summer 
 series of orchestral concert at Woodbrook Concert Hall , at 
 which all Beethoven ’s symphony will be perform in 
 order . Signor Simonetti play Mendelssohn ’s Violin 
 concerto extremely well , and the orchestra show to great 
 advantage in the accompaniment . on May 17 , besides the 
 second Symphony , the programme include Wagner ’s 
 | * Waldweben ' and the ' Meistersinger ' Overture . Miss 
 Nita Edwards , who have just return to Dublin from a tour 
 round the world with the Quinlan Opera Company , be the 
 solo vocalist , and sing with great success 

 the Feis Ceoil have just publish a collection of ' irish 
 Airs ' ( eighty - five in number ) , edit by Arthur Darley and 
 P. J. McCall . they consist of a selection from the large 
 number contribute from time to time at the Festivals hold 
 |s nce 1897 , and be understand to be hitherto unpublished . 
 they should prove of much interest 




 LIVERPOOLThe Walton Philharmonic Society , conduct by Mr. 
 Albert Orton , deserve honourable mention for the performance 
 of Cowen ’s ' sleeping Beauty , ' which close the Society ’s fifth 
 season on April 22 . the tuneful work afford grateful 
 opportunity for an intelligent choir , and the singing be 
 generally mark by precision and expression . the vocal 
 soloist be Miss Margaret Hadfield , Miss Helen Strain , 
 Mr. Lloyd Moore , and Mr. J.C. Brien . by a wise provision , 
 six first - class string - player and a timpanist assist Dr. 
 Stanley Dale as pianist , so that the orchestral scope of the 
 accompaniment be very usefully suggest . with the aid 
 of this small orchestra an admirable performance of Mozart ’s 
 ek flat major Pianoforte concerto be also give , with 
 Mr. Orton as solo player 

 another estimable local organization , whose aim be 
 instrumental rather than choral , be the Anfield Orchestral 
 Society . under the direction of Mr. William Faulkes the 
 Society continue to prosper , and the programme no less 
 than the performance have reflect credit on all concern . 
 at the close concert of the sixth season , on April 29 , 
 orchestral movement by Beethoven , Mendelssohn , and 
 Weber be play ; also Beethoven 's Pianoforte concerto in 
 e. flat , Op . 73 , in which the soloist be Mr. E. K. Harrison . 
 the vocalist be Mrs. Howard Stephens 

 on April 18 , a number of local singer and entertainer 
 combine in a performance , give in Hope Ilall on April 18 , 
 in aid of the widow and daughter of the late Mr. John Henry , 
 who be especially well - know in the welsh community and 
 have long be resident in this city as a singer and teacher of 
 singing 




 ABERDEE 


 ASPATR L 


 BRANDON 


 BRIEFLY SUMMARIZED . 


 EEEE 


 MUSIC IN PARIS 


 M. R 


 1915 . 7 


 409L. Whittake 

 inkerron ’ wf Meresting . : , 
 April 21 , & the Société des Concerts du Conservatoire have close its 
 \. under i < n with two performance of Bach ’s ' Johannes 
 perform : my fasion . " 
 totes te at her song recital give on May ii , Madame Vallin 
 [ r. Lichtore My Hekking have introduce interesting number by 
 " BE Grassiand Roland Manuel . 
 M. Robert Schmitz have give on May 12 , a most 
 — ff itteresting pianoforte recital , devote to modern french 
 and russian music . 
 the last concert of the Société Musicale Indépendante 
 roduce wi ( omprise interesting number by contemporary Spanish | 
 Mile . yom ff composer , and greek song by Emile Riadis . 
 M. message \lbert Roussel ’s ' Evocations , ' Paul le Fleur ’s * Crépuscules | 
 mmende . fAmor , ' a ' Symphonic Suite ' by Darius Milhaud , and a 
 d ’s ' Mami Mf ' Madrigal lyrique ' by O. Klemperer be give at the 
 tale of tem fiteenth concert of the Association des Concerts Schmitz . 
 our . Itsi le _ 
 aracter , k & sia enin - ornaia 
 We ' . | 
 ae Foreign Wotes . 
 wo work 
 nd the ball BERLIN . 
 under the direction of Madame Heyman - Engel 2 french 
 a. theis pera - buffe evening be give at the Hochschule fiir Musik . | 
 and a sal @ tbe duet from ' Der Kapellmeister ' ( Paér ) and Masse ’s one- | 
 very ape # at operetta ' Les Noces de Jeanette ' be the principal | 
 e de Josep ! ature of the evening — the Karg - Elert evening recently | 
 a majority gen at the Harmonium [ all prove a great success . an 
 rs havedal J Mleresting meeting of the Society for the Protection of Art | 
 ic do 2 be hold recently . the first part of the programme 
 on the be HF " onsiste of an address by Herr Metzler on the disfiguration 
 mposet , # af master - work of the 17th and 18th century violin music 
 and warm y unstylish modern arrangement . a concert of chamber 
 music from the 16th to the 18th century follow . 
 ernment & | 
 FRANKFORT - ON - MAIN . | 
 consist a highly interesting concert of french music be recently | 
 -Korsiors Sen at the Palmgarten . work by Aubert , Dubois , Bizet , 
 of humons ff " Massenet , Saint - Saéns , Thomas , and Bourgeois be hear . | 
 aes ! HAMBURG . 
 inal pat ; : | 
 " Nerina ae third great Festival of the German Brahms Society | 
 nducte hy ‘ be hold at Hamburg ( Brahms ’s birthplace ) on June 4 - 8 , 
 1915 . the first two Brahms Festivals take place respectively 
 ‘ cal Soci m1909 at Munich and 1912 at Wiesbaden . 
 te tow JENA . 
 t , incladm Th , = ; 
 ailles . 0 y € recently discover variation by Beethoven for two | 
 une 100 m * S and english horn ( or two violin and viola ) will 
 it the howt rete be publish by the fortunate finder , Prof. Fritz 
 h memmbss ara ata discover also the ' Jena ' Symphony . the 
 sedy - Fast lation , which be on a theme from Mozart 's ' Don Juan , 
 1 ( * ancie " em to have be write about 1795 , when Beethoven 

 compose the 




 LEIPSIC 


 MAGDEBURG . 


 MEININGEN , 


 NAPLES 


 PETERSBURG 


 ST 


 ROTTERDAM 


 VIENNA 


 410400 ) , single 

 second Beethoven Festival have be arrange by 

 by Bach and Brahms in the programme 




 ROYAL ACADEMY OF MUSIC , 


 A.J 


 content 


 LUARD - SELBY 


 ELLINGF ' 


 ARN 


 ELL 


 0Z. 


 AUI 


 AR 


 REL , 


 H OL 


 ONE 


 OB 


 _ _ 


 the MUSICAL 


 VALUABLE V 1OL IN~ ' 


 OR SALI 


 important 


 study S in ORGAN tone | 


 MUSIC publishing 


 WATERSIDE 


 a valuable book for teacher and student 


 TECHNIQUE 


 AND 


 E XPRE SSION 


 PIANOF ‘ ORT E PLAYING 


 FRANKLIN TAYLOR 


 extract from PREFACE . 


 WITH numerous musical example from the 


 work of the great master . 


 MUSICAL TIMES 


 ADVERTISEMENTS 


 the 


 scale of term for 


 notice 


 ENGLISH LYRIC 


 set to MUSIC 


 first 


 set 


 second se ! 


 third set . 


 fourth set . 


 sixth set 


 seventh set 


 eighth set . 


 ninth set . 


 NOVELLO 


 BY 


 NGS and SIXPEN 


 CH 


 KA 


 SEI 


 2 og 


 RRY 


 THI 


 4 MUSICAL 


 AN them 


 FOR 


 trinitytide 


 COMPLETE 


 list 


 the " lute 


 serie 


 DOM 


 CON COO AA 


 MUSICAL setting of the 


 ROMAN LITURGY 


 edit BY 


 ANSITIS PER VIAM 


 413 


 CANTIONES SACRA 


 SAMUEL GREGORY OULD 


 414 


 THE MUSICAL 


 1914 


 ADVENT 


 CHRISTMAS 


 LENT 


 EASTER 


 HARVEST 


 GENERAI 


 ADVENT 


 CHRISTMAS 


 LENT 


 EASTER 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 LENT 


 EASTER 


 HARVES1 


 GENERA 


 HARVEST 


 GENERA 


 CHRISTMAS 


 LENT 


 HARVEST 


 GENERAI 


 ADVENT 


 CHRISTMAS 


 NOVELLO ’S ANTHEM book 


 I 6 


 I 7 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 LENT 


 EASTER 


 HARVEST 


 ( GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 LENT 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 300 K 9 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 HARVEST 


 GENERAL 


 ADVENT 


 LENT 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 THE MUSICAL 


 MUSIC FOR 


 ANTHEMS 


 POPULAR CHURCH MUSIC 


 H. MAUNDER 


 . SERVICES . 


 . CHURCH CANT ATA . 


 NOVELLO 'S 


 ANTH 


 PRECES 


 EMS . 


 HYMNS AN 


 D TUNES 


 » TI : J. STAINER 


 the 


 ferial response 


 WITH LITANY 


 AND the 


 AND 


 WITH LI 


 ACCORDIN 


 TALL 


 EDITED 


 JOSEPH 


 response 


 ANY 


 IS 


 BY 


 BARNBY 


 NOVELLO 


 PENCE 


 EASY ANTHE 


 GILL 'S 


 NOVELLOS 


 MAGNIFICAT AND|N EW SONGS 


 NUNC DIMITTIS 


 READY JUNE 3 


 PRICE two shilling each ne ? ( 


 set to MUSIC in the key of E MINO 


 BASIL HARWOOD 


 S M 


 I ONDON TOWN . 


 \ POEM | 


 ¥ | the pathway THRO ’ the poppy 


 PTOBIAS MATTHAY | the BOLD GENDARME . _ : 


 OVE in the meadow . : 


 \NI 


 STEWART 


 W AREIN 


 OVERLE 


 GRAHA 


 IES 


 " HOMSON 


 0S 


 HOMS 


 NEWTON 


 AREIN 


 ETCHER 


 TCHER 


 SHAWE 


 THE MUSICAL 


 I , 1914 . 417 


 MR . JOSEF 


 HOLBROOKE 'S 


 CELTIC DRAMAS 


 DYLAN 


 DRURY AES THEATRE , 


 THOMAS BEECHAM 


 " the child of DON 


 OPERETTA 


 other ORCHESTRAL work . 


 " APOLLO and the SEAMAN 


 DRAMATIC CHORAL SYMPHONY 


 NEW SONGS 


 EVE of ST . AGNES 


 CANTATA 


 CHORUS , and 


 JOHN KEATS 


 the 


 FOR SOLI , ORCHESTRA 


 the 


 JOHN FRANCIS BARNETT 


 DAILY TELEGRAPH . 


 MORNING POST . 


 SUNDAY TIMES . 


 monthly MUSICAL record 


 the GLOBE . 


 the FLYING DUTCHMAN 


 AN OPERA 


 BY 


 RICHARD WAGNER . 


 selection from ACT IL 


 RISING 


 introduction . 


 SPINNING CHORUS 


 SENTA ’S BALLAD , 


 ETC 


 COMI 


 the VOYAGE of LOVE 


 SONG - CYCLE . 


 the word by 


 HAROLD SIMPSON . 


 the music by 


 CHORAL SONGS for S.AT.B 


 EDWARD ELGAR . 


 the shower | the fountain 


 the word from a poem by the word from a poem B\ 808 . | 


 HENRY VAUGHAN . HENRY VAUGHAN . 


 death on the hill 


 the word adapt from the RUSSIAN of MAIKO\ by 


 ROSA NEWMARCH . 


 — — 838 . ° 


 LOVE 'S TEMPESI SERENADE 


 MAIKOV , by MINSKY , by 


 ROSA NEWMARCH . ROSA NEWMARCH . 


 NEW CHORAL WORKS 


 by 


 GRANVILLE BANTOCK . 


 the world be too much with we ) MARCH of the CAMERON MEX § 46 : 


 ( SONNET . ) for chorus of mix voice . 810 , | 


 WORDSWORTH 


 MARY M. CAMPBELL 


 O can ye sew cushion ? SPRING - enchantment 


 LULLABY . ) five - part song for mix voice 


 for choru of mix voice . HELEN F. BANTOCK . 823 , " 


 choral suite . 


 for MALE , FEMALE , and child ’s voice 


 BALLADE FESTIVAL song 


 the word by 


 CHARLES NEWTON - ROBINSON . H. ORSMOND ANDERTON 


 SIAN 


 1974 


 419 


 E. JAQUES - DALCROZE 'S 


 10 


 988 


 989 . 


 1010 


 IOI 


 VOCAL work with english text 


 celebrate CHILDREN- and action - song 


 the JAQUES - DALCROZE RHYTHMIC gymnastic 


 ( eurhythmic ) 


 now issue as a net book 


 additional hymn 


 WITH tune ch 


 for use with 


 hymn ancient and modern 


 OR any other church hymnal 


 LONDON : NOVELLO and COMPANY , limited 


 now issue as a net book 


 the NEW CATHEDRAL PSALTER ] ° 


 CONTAINING 


 the psalm of DAVID 


 TOGETHER with the canticle and proper psalm . 


 edit and point for chanting by 


 ° 29 5 4 


 just publish 


 NOVELLO ’S handbook for musician . 


 edit by ERNEST NEWMAN 


 CHORAL TECHNIQUE & interpretation 


 by 


 | HENRY COWARD . 


 the teaching and accompaniment 


 OF PLAINSONG 


 BY 


 FRANCIS BURGESS 


 just publish 


 the MUSIC of the bible 


 BY 


 JOHN STAINER . 


 BY 


 " the REV . F. W. GALPIN , M.A. , F.L.S 


 - just publish . 


 + play of WILLIAM SHAKESPEARE 


 write and compile by 


 5 6 MRS . G. T. KIMMINS . 


 5 6 


 contain full instruction on singing , 


 with a detail analysis of some well 


 lishe 


 for 


 compose 


 SCOTT 


 A gu ide to SOL OS SING ING ! PIANOFORTE SOLO 


 D BY 


 BAKER 


 known work and song . 


 ; by 


 GUSTAVE GARCIA , 


 three - part study 


 school and LADIES ’ CHOIRS 


 HUGH BLAIR , 


 WITH preface and direction for practice 


 JAMES BATES 


 for the voice 


 G. HENSCHEL 


 FOU R CHARAC ‘ teristic piece for VIO 


 C. H. LLOYD 


 ARRANGEMEN for SMALI 


 FOR 


 MPOSE 


 SCOTT 


 _ _ _ _ _ 


 ISHED 


 SE PIANOFORTE SOLO 


 BAKER 


 FRO 


 BY 


 ORCHEST 


 two interlude 


 FALSTAFF 


 RA 


 SED 


 ELLO and 


 IN and 


 F. HA 


 SONATA in A 


 PIANOFORTE 


 NDEL 


 BY 


 AP 


 NUMEI 


 the MUSICAL 


 I , i914 . 423 


 LOVERS 


 the new and revise edition of 


 TO music 


 GROVE ’S dictionary 


 of 


 MUSIC and MU 


 sician 


 FIFTEENPENCE WEEKLY 


 prospectus ( a dept 


 write for 


 ST . MARTIN ’S STREET 


 CO 


 LONDON , W.C 


 APRACTICAL guide 


 TO the 


 theory of 


 containing 


 numerous test - question with answer 


 JULIA A. O'NEILL 


 the TIMES . 


 the MORNING POST . 


 musical opinion , 


 music 


 the LADY . 


 HANDBOOK of 


 CAMBRIDGE 


 UNIVERSITY press 


 ABDY WILLIAMS , M.A 


 CAMBRIDGE UNIVERSITY 


 FETTER LANE , LONDON 


 press 


 EXAMINATIONS 


 IN music 


 600 question ' with answer 


 ERNEST a. DICKS 


 preface to the NIN1 T h edition . 


 E. A. D 


 HARVEST FESTIVAL music 


 CANTATAS . 


 SONG of thanksgive HARVEST CANTATA 


 and CHORUS BARITONE ) SOLI and CHORUS 


 the word by 


 ROSE DAFFORNE BETJEMANN 


 the music by 


 SHAPCOTT WENSLEY JULIUS HARRISON . 


 the word write and arrange by 


 for TENOR and BASS SOLI and CHORUS JOHN E. WEST . 


 THOMAS ADAMS , for CHORUS and ORCHESTRA 


 THOMAS ADAMS . for FEM — voice 


 for TENOR and BASS SOLI , CHORUS , and ORGAN or the J UBILEE CANTATA 


 SMALL ORCHESTRA for SOLO voice , CHORUS , and ORCHESTRA 


 BY 


 HUGH BLAIR . C. M. VON WEBER 


 HARVEST CANTATA a harvest song 


 for CHORUS , SEMI - chorus , and ORGAN for SOPRANO SOLO and CHORUS 


 BY by 


 GEORGE GARRETT . C. LEE WILLIAMS . 


 twelve hymn for HARVEST the sower go forth sowing 


 .et all our brethren join in one » 08 


 the joy of HARVEST and SEA 


 A HARVEST hymn of PRAISE come , ye thankful PEOPLE , come 


 . the ORGAN _ _ | original composition 


 ' ' ose by 


 6 . REQUIEM ATERNAM : 1 ¢ 


 CHORUS 


 the ATHEN © UM 


 ORGAN 


 the MUSIC student . 1A n 


 FOR the 


 compose by 


 CHURCH family NEWSPAPER 


 t NEWSP : UR 


 NOVELLO ’S 


 album for the 


 TWELVE select 


 TWELVE select 


 ORGAN 


 piece . 


 piece 


 piece 


 J I 


 piece . 


 piece 


 8 . V 


 UST publish 


 the MORRIS BOOK 


 the MORRIS MEN of ENGLAND 


 CECIL I. SHARP 


 GEORGE BUTTERW ( RTH 


 PART V 


 MORRIS DANCE tune 


 arrange for PIANOFORTE SOLO 


 BY 


 CECIL J. SHARP 


 GEORGE BUTTERWORTH 


 SET IX 


 SET x. 


 N content 


 FRENCH 


 musical diction 


 AN orthologic method for acquire 4 


 perfect pronunciation in the speaking 


 FRENCH language . 


 for the special use of english - speaking PBOPLE 


 BY 


 C. THURWANGER 


 OU 


 THE MUSICAL 


 TIMES 


 JUNE 1 


 IQI4 . 427 


 eee 


 . LITERATURE , 


 LE . 


 LONDON 


 important to choral society . 


 ready 


 NOIV 


 CONCERT version of the popular comic opera 


 TOM JONES 


 compose by 


 EDWARD GERMAN 


 NEW YORK , TORONTO , and MELBOURNE 


 NOVELLO ’S music for military BAND 


 I OUSELEY ) 


 LONDON : NOVELLO and COMPANY , limited 


 ALSBA 


 BESS RIS 


 NOVELLO 's part - song book 


 a collection of 


 PART - song , GLEES , and madrigal 


 ERA 


 NOVELLO ’s part - song book . 


 ( SECOND serie 


 SWEET DAY , so cool 


 PART - SONG for S.A.T.B. 


 the word write by GEORGE HERBERT 


 the music compose by 


 EDWARD GERMAN 


 TS ST = I 


 A SESE ES 


 U.S.A 


 SWEET DAY , so cool 


 SWEET DAY , so COOL 


 = ST = = E — 


 SWEET DAY , so COOL 


 = ; 3 — — — — — — — — — | = F 


 SO — — — 


 7 SSS SS SS 


 80 COOL 


 LSS SS 


 O — 29 


 SWEET DAY , so COOL 


 NOVELLO ’S 


 4 - 3 - 4 


 30 


 506 


 519 


 517 


 542 


 ATAAOLSIAAN , SOLION 


 AONNOGOD SINO SV 


 ATHINNAL AHL NVAI SV 


 NIAVVIVHS