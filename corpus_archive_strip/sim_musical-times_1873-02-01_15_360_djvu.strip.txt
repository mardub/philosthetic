


IK MUSICAL TIME


FEBRUARY 1, 1873


MASON AND HAMLIN’S 


“AMERICAN ORGANS


ON AND HAMLIN’S AMERICAN ORGANS. 


ON AND HAMLIN’S AMERICAN ORGANS. 


SON AND HAMLIN’S AMERICAN ORGANS


BON AND HAMLIN’S AMERICAN ORGANS


AND HAMLIN’S AMERICAN ORGANS. 


NY AND HAMLIN’S AMERICAN ORGANS. 


ASON AND HAMLIN’S AMERICAN ORGANS


ASON AND HAMLIN’S AMERICAN ORGANS


ASON AND HAMLIN’S AMERICAN. ORGANS


ASON AND HAMLIN’S AMERICAN ORGANS


ER AND CO., 37, GREAT MARLBOROUGH STREET, LONDON, W


‘ Y Y ‘ 


13 ” %9 ” 26 ” 


MR. J. TILLEARD


SS HARGREAVES’S AMERICAN IRON FRAME PIAM


PIANOFORIH, HARMONIUM, HAR- 


PASCALL’S VOICE JUJUBES. 


THE PATENT LOOPED-BINDER PORTFOLIO. 


ME ALBERT GLEE TRIO. 


SECOND


SERIES


W. E


CANTICLES “AND HYMNS } 2 


AND POPT 


NOW READY, 


NOTICE


THE COLLEGIATE AND SCHOOL 


SIGHT SINGING MANUAL


(COMPANION WORK TO THE ABOVE


COLLEGIATE SOLFEGCI


CATHEDRAL CHANT BOOK: | 1


NOW READY. 


{THE NATURAL PRINCIPLES OF HARMONY


TO CHORAL SOCIETIES


W. H. BIRCH’S EASTER MUSIC. 


WILLIAM J. YOUNG’S


POPULAR PART SONGS FOR S.A.T.B. 


TOC 


ART-SONGS, BY W. W. PEARSON. 


VI 4


TS


VERNON RIGBY’S NEW SONG. 


C. JEFFERYS, 57 BERNERS ST. 


THE CHORAL SOCIETY


A COLLECTION OF


THE POPULAR 


SOLOS AND DUETS


WILLIE PAPE


LLANGOLLEN .. 40 


LOUIS DUPUIS


CATHEDRAL GEMSeee 
a er

J from. ‘Beethoven’ ~ Mass 
5 Roven.—Fantasia, on subjects from G 
Solennelle

6. CaEN.—Fantasia, on subjects trom Roasini’s Stabat Mater




C. JEFFERYS, 57, BERNERS STREET (W


750


ROYAL ALBERT HALL 


CHORAL SOCIETY


CONDUCTOR, MR: BARNBY


PRESIDENT. 


VICE-PRESIDENTS. 


THE EARL OF FEVERSHAM. 


THE LORD CLARENCE EDWARD PAGET, K.C.B. 


DR. LYON PLAYFAIR, C.B., M.P


A SERIES OF SIX


GRAND ORATORIO CONCERTS


WILL BE GIVEN AT THE


ROYAL ALBERT HALL


MENDELSSOHN’S ELIJAH. 


HANDEL’S BELSHAZZAR. 


HANDEL’S MESSIAH. 


HANDEL’S ISRAEL IN EGYPT. 


ROSSINI’S STABAT MATER


BAND AND CHORUS OF 1200 PERFORMERS, 


ORGANIST, DR. STAINER


ROYAL ALBERT HALL CHORAL SOCIETY


FIRST SUBSCRIPTION CONCERT, 


WEDNESDAY, FEBRUARY 12, 1873. 


ROYAL ALBERT HALL CHORAL SOCIETY


SECOND SUBSCRIPTION CONCERT, 


WEDNESDAY, FEBRUARY 26, 1873. 


HANDEL’S MESSIAH


ADMISSION - ONE SHILLING. 


REENWICH PARISH CHURCH.—SOPRANO, 


NEW NOMENCLATURE OF THE NOTES O¥8 THE SCALE DIATONIC AND 


CHROMATIC


752


753


79


754


FESTIVAL SERVICE AT ST. PAUL’S CATHEDRALE | delighting the hearers

be true that cathedral choristers at the present time, of 
sity become fairly imbued with the spirit of thg 
Church composers, but it is equally true that if the 
to become acquainted with the great works of the 
masters they must perforce go to the concert-ro, 
Bach, Handel, Haydn, Mozart, Beethoven, Mend . 
and Spohr are—even in their sacred works—only adequatgy 
represented in secular places. 
o all those, therefore, who are anxious for the welfargg 
the Church as it exists in this country, such a mo 
as this, boldly inaugurated at Westminster Abbey and F 
mirably followed at St. Paul’s Cathedral, must be a matte 
of profound satisfaction

The promoters must have been somewhat surpeioes at the 
completeness of the success of their scheme. ‘or whereas 
a congregation of 3,000 persons was scarcely so mush 
expected, as hoped for, thenumber of persons actually 
must have been close upon 8,000. The procession of thy 
clergy, choristers and orchestra to their seats in the choir, 
was performed in perfect silence, which was ge 
felt to be a mistake. The solemn opening chords of thy 
overture however fell with a most soothing effect upon th 
ears of the listeners, and powerfully convinced those who 
previously might have been sceptical as to the good effeg 
of orchestral instruments in the Church Service, that not 
only were they devotional in their character, but even mon 
80 than the organ itself. The position selected for the 
sermon, immediately following the overture and p 
the service, is wholly incomprehensible, nor was the necessity 
for a sermon at all very apparent. The ordinary evening 
Service then followed, and was without exception admirably 
rendered, the chanting of the Psalms being parti 
clear and distinct. The Canticles were sung to a setti 
Stephen Elvey, specially scored for the orchestra, but n 
in itself nor in its orchestral dress did this music i 
its hearers with any sense of congruity or power. 
the third collect the Anthem, or in other words, the 
selection from Mendelssohn’s “St. Paul” comm 
and was performed in the following order: Recit, “ Andas 
Saul journeyed; Chorus, “Saui, Saul, why persecutest 
thou Me ?” Chorus, “ Rise up, arise, and shine ;” Chorale, 
** Sleepers, wake;’’ Recit, ‘And his companions;” Air, 
“O God, have mercy ;” Recit, “And there was a disciple ;” 
Air and Chos. ‘‘I praise Thee, O Lord;” Recit, ‘‘ And Ananias 
went his way;’’ Chorus, “O great is the depth;” Rect, 
‘And Paul came to the congregation ;” Duet, “ Now we 
are ambassadors; Chorus, “ How lovely are the met 
sengers ;” Recit, ‘‘ And they all persecuted Paul ;” Air, “Be 
thou faithful unto death;”’ Recit, ‘And Paul sent, and 
called the elders;’’ Chorus, “Far be it from thy path;’ 
Recit, “What mean ye thus to weep;” Chorus, “See 
what love hath the Father;” Recit, ‘‘ And though he hath 
offered ;”” Chorus, ‘‘ Not only unto him

CRYSTAL PALACE

Tue Saturday afternoon concerts were resumed On the 
18th ult., the principal orchestral works being Beethoven's 
Symphony in C minor and Mendelssohn’s “Trumpet 
Overture, the charming music of the Ball-room scene @ 
Auber’s ‘‘ Gustave’’ being also included in the programme. 
Signor Piatti’s performance of his own Violoncello Com 
certo was a marvellous exhibition of expressive and re 
playing, and received, as it deserved, the warmest marks 
approval, the last movement, “a la Tarantelle” especially 
The vocalists were Madame Patey 
and Mr. W. Castle, the powers of the latter really excellent 
singer being somewhat too severely taxed in We

demonstrative Scena from Oberon, “ O, ’tis a glorious si




755


BRITISH ORCHESTRAL SOCIETYMust have convinced him of the triumph he had achieved 
a somewhat critical audience. The Overture to

Der Freischiitz’”’ was so magnificently played as to be 
ae nouly re-demanded ; and Beethoven’s Symphony in B 
Mat (No: 4), kept the majority of the audience to the last 
The vocalists were Madame Florence Lancia, Mr

W. H. Cummings, and Mr. Santley. Mr. Mount obviously 
oves as a conductor, and is gradually getting rid of a 
amount of coarseness which was at first perceptible




BRIXTON CHORAL SOCIETY


756Mr. Joun Lopce ELLERTON, whose decease was announced 
during the past month, was one of the few amateurs who 
earned any name as a composer, beyond the immediate 
circle in which he moved. He wrote an Oratorio of much 
merit, called “Paradise Lost,” and also Symphonies, 
Motets, &c., and a large number of chamber compositions, 
all of which were founded on the most classical models

Tue third of Mr. W. H. Monk’s Classical Concerts was 
given at the Assembly Rooms, Stoke-Newington, on the 
2lst ult. The programme included Mozart’s Quartet in D 
(No. 10) and Beethoven’s Trio in E flat, the executants in 
the first being Messrs. H. Holmes, J. Folkes, H. Burnett, 
and Signor Pezze, and in the second Miss Florence May, 
Mr. Holmes, and Signor Pezze. Miss May achieved a 
brilliant success, not only by her performance in the Trio, 
but by her rendering of Chopin’s Scherzo in B flat minor. 
The vocalists were Miss Katharine Poyntz and Mr. W. Rees. 
The fourth and last of these admirable concerts is announced 
to take place on the 18th inst

Miss Excwo’s second evening concert took place on ¢ 
21st ult., at the Hanover Square Rooms, before a 
audience. The pianoforte solos of the concert-giver w

WE are glad to see that a suggestion which was made hy 
us some time ago, is likely to be carried out. At the Church 
of St. Anne, Soho, a series of services are announced ty 
take place on the Friday evenings in Lent, at which Bachy 
Passion according to St. John, will be performed in place 
of an Anthem. The voices will be accompanied by a full 
orchestra, including harp and organ. It will be seen op 
referring to our advertising columns, that the 
ments for the convenience of worshippers have alre; 
been carefully made ; and looking to the fact that Mr. Barnby 
is director of the music in this Church, nothing is likely to 
be wanting to render the Service worthy of so solemn a 
occasion

Tue fourth of the Monthly Popular Concerts at Brixton 
took place at the Angell Town Institution, on the 14th ult, 
the executants being Messrs. Burnett, Folkes, Ri ; 
Blagrove, and Pettit, with the director, Mr. Ridley Prentice, 
at the pianoforte. The principal concerted pieces were 
Mozart’s Quartet in D major (No. 7), and Mendelssohn's 
Trio in D minor, all of which were excellently rendered,’ 
The performance of Beethoven’s ‘‘ Sonata Arpassion 
Mr. Prentice, must also be named as one of the most suc. 
cessful efforts of the evening. The vocalists were Mr, 
Hale and Miss Adelaide Newton. A

Mr. Henry Hoxtmes’s Chamber Music Concerts, af 
St. George’s Hall, are gradually but firmly gaining ground 
in public estimation. On the 22nd ult. a programme of the 
highest interest was provided, including Schubert’s Quartet 
in A minor (Op. 29), and Beethoven’s Trio in E flat (Op. 7), 
No. 2), in the latter of which Mr. W. H. Holmes, an artist 
but too rarely heard in public, most ably sustained the 
pianoforte part. The stringed instruments were in the ex- 
perienced hands of Messrs. H. Holmes, Folkes, Burnett, 
and Signor Pezze. Miss Purdy was warmly and deservedly 
applauded in both her songs, the effect of which was 
materially heightened by the excellent pianoforte accom- 
paniment of Mr. Walter Macfarren

Tue Euterpean Musical Society, an amateur company, 
under the presidency of the Rev. G. W. Herbert, Vicar 
of St. Peter’s, Vauxhall, gave an evening concert on 
Monday, the 30th of December, in the St. Paul’s School- 
rooms. A carefully selected programme of vocal musie,. 
varied by some well chosen pianoforte solos and duets, was 
very well rendered by the members of the Society. Mr. 
A. J. Eyre, organist of St. Ethelburga’s, Bishopsgate, is the} 
musical director of the Society, which is worthy of all 
commendation




BS TSB RS


758


OE 


NN


NY


BH 


TCS


SNARE RONEN


1 I 


LL


XUM


NOVELLO’S OPERA CHORUSE


AUBER’S FRA DIAVOLO


AUBER'S MASANIELLO. Honneur! honneur et gloire

BEETHOVEN'S FIDELIO

zo. Oh whatdelight. (Prisoners’chorus) . . « 
O welche Lust




BELLINI'S I PURITANI


BELLINI’S NORMA


BELLINI’S LA SONNAMBULA


DONIZETTI’S LA FIGLIA


DONIZETTI’S LUCIA


EDITED BY


NATALIA MACFARREN AND BERTHOLD TOURS


DONIZETTI’S LUCREZIA BORGIA


MOZART’S DON GIOVANNI. 


MOZART'S LE NOZZE DI FIGARO


ROSSINI'S IL BARBIERE


VERDI'S IL TROVATORE


VERDI'S RIGOLETTO


WAGNER'S LOHENGRIN


WAGNER'S TANNHZEUSER


WEBER'S OBERON


WEBER'S DER FREYSCHUETZ


ROSSINI'S GUILLAUME TELL


LONDON: NOVELLO, EWER & CO., 1, BERNERS STREET (W.), AND 35, POULTRY (E.C


NEW YORK: J. L. PETERS, 599, BROADWAY


766


BE


767


DIATONIC AND CHROMATIC SEMITONES. 


TO THE EDITOR OF THE MUSICAL TIMES


WISE’S ANTHEM. 


TO THE EDITOR OF THE MUSICAL TIMES


THE SIGNATURE OF THE MINOR SCALE. 


TO THE EDITOR OF THE MUSICAL TIMES. 


768


BACH’S HYMN IN “THE HYMNARY.” 


TO THE EDITOR OF THE MUSICAL TIMES


C. LAHMEYER, 


BRITISH ORCHESTRAL ASSOCIATION. 


TO THE EDITOR OF THE MUSICAL TIMES


STRANIERO, 


THE YOUTH’S PART IN “ELIJAH.” 


TO THE EDITOR OF THE MUSICAL TIMES


J. W. STEPHENSON


TO CORRESPONDENTS


THE MUSICAL TIMESi, who is about to retire from the musical world, after her 
thened and successful career as a pianist. Her brilliancy of execu- 
combined with a singular delicacy of touch, especially in the

mata Pastorale of Beethoven, would have alone established her fame 
gan artist of the highest rank. The vocalists were Miss Marion 
and Mr. W. Drayton. The sympathetic rendering by Miss

of Macfarren’s song “ Separation,” was especially pleasing




770THE MUSICAL TIMES.—Fessrvary 1, 1873

Madame Billinie Porter created 2 highly favourable impression by her 
singing throughout the evening, and she was warmly and most 
deservedly applauded. The solo instrumentalists were Madame 
Norman-Neruda, Herr Straus, and Mr. Charles Hallé, all of whom 
fully sustained their well-earned reputation. The hall was thronged 
in every part.——The Philharmonic Society commenced the Subscription 
Concerts for the year, with a very excellent one on the 14th ult.; 
principal artists, Madlle. Tietjens, and Signor Piatti. The orchestral 
works performed were the Overture to Die Zauberflite (Mozart), Con- 
certo, violoncello (Piatti), Sinfonia No.4, in B flat (Beethoven), Overture 
Der Berogeist (Spohr), and Overture La Barcarolle (Auber), all of which 
were rendered with much sp‘rit and refinement. Signor Piatti’s Con- 
certo was full of charming orchestral effects, and, of course, of immense 
difficulties in the solo part. Madlle. Tietjens sang excellently “‘ Und ob 
die Wolke” (Weber), refusing an encore, Haydn’s Canzonet, * My 
mother bids me bind my hair,” the grand Scena from Cherubini’s 
Medea, ‘* Dei tuoi figli,” and the recitative, ‘La notte fugge,” and 
lovely aria, ‘‘ Si lo sento,” from Spohr’s Faust. Signor Piatti’s solo for 
the violoncello with pianoforte accompaniment (the latter exquisitely 
played by Sir Julius Benedict), was the ‘ Largo and Gigue” (Veracini), 
and in reply to a vehement encore, he gave two charming transcripts of 
Schubert’s ‘Ave Maria.” The part-music was very good. The 
‘*Skater’s Chorus,” from Le Prophéte (Meyerbeer) was encored; and 
Hatton’s clever part-song, ‘‘ Night thoughts,” much admired

MANSFIELD.—On Friday, December 27th, the Mansfield Tonie Sol-fa 
Choral Society, conducted by Mr. J. Burton, gave W. B. Bradbury's 
Cantata, Esther, which was well received by a crowded and appreciative 
audience. The principal characters represented were Esther (Miss E. 
OCripwell), Ahasuerus (Mr. C. Gilbert), Haman (Mr. Blackwell), Mor- 
decai (Mr. D. Barrows), Prophetess (Miss Katherine Blackwell), &c. 
The choral portion of the work was rendered with extreme accuracy 
and precision, showing a marked improvement in the choir, which 
nothing but untiring zeal and energy on the part of the conductor could 
have effected. Mr. Gilbert gave the music allotted to him with much 
effect. Miss Cripwell sang with appropriate expression; and Miss K. 
Blackwell (a promising juvenile sopfano) made a very successful début. 
Messrs. Blackwell and Barrows also deserve special recognition. The 
most effective choruses were ‘‘He that goeth forth and weepeth.,” 
“When the Lord turned again the captivity of Zion,” and the con- 
cluding chorale and chorus, the last of which was re-demanded. Miss 
I, Peech presided at the pianoforte with much ability

Rypg, Istk or Wicut.—Mr. Augustus Aylward’s evening concert 
took place on the 30th December, the artiats being Miss Amy Aylward, 
Miss Leila Aylward (A.R.A.M.), Miss Gertrude Aylward, Mr. W. P 
Aylward, Mr. W. H. Aylward (R.A.M.), Mr, T. E. Aylward (organist

Llandaff Cathedral), Mr. A. A. Aylward, and Mr. A. B 
programme included, Sonata, in F (Beethoven), for pianoforte ga

violin; Sonata, in D (Mendelssohn), for pianoforte and violong F 
Trio, in G (Beethoven), No.2, Op.1; ‘La Jeune Religieuse” ¢g 
bert), for voice with violoncello and organ obbligati; and Bach's% 
Prelude, in O, arranged for violin, violoncello, organ and piang 
The vocal music was accompanied by the orchestra, and the gg 
was a great success

Satispury.—The Amateur Musical Society gave a very suce 
concert at the Assembly Rooms, on Thursday, the 9th ult. The 
part consisted of Sullivan’s Oratorio, The Prodigal Son, and the 
of a miscellaneous selection of secular music, including Beetho 
Symphony, in C (Op. 21), and several part-songs and vocal solos, 
band and chorus numbered upwards of 80 performers. The p 
singers were Miss Amy Aylward (soprano), Miss Annie Butte . 
(contralto), Mr. H. Taylor (tenor), and Mr. Wadmore (bass). The gos 
instrumentalists were Mr. J. Winterbottom (bassoon), and Mr, W, # 
Aylward (violoncello); leader of the band, Mr. Burnett; conductors, 
Mr. Aylward and Mr. T. E. Aylward

ScarBorouGH.—The organ recitals by Dr. Naylor at the Parigh 
Church, at Christmas and Easter, have become established features 
the holiday seasons. The Christmas performance took place on Thump 
day afternoon, the 26th December, when upwards of a thousand perso. 
formed the, congregation. The programme comprised a numberof 
pieces from’ the works of Bach, Beethoven, Handel, M 
Mozart, Spohr, &c,, most judiciously selected with the threefold obje — 
of displaying the capabilities of the noble instrument, of aff . 
organist an opportunity for exercising his artistic ability, and of i ‘ 
yielding gratification to a mixed company. The performance wa ihe : 
eminently successful throughout; and there can be no doubt that had 
the circumstances of the place allowed of a vote of thanks to Dr. Naylor, 
would have been most heartily rendered him. A few 
vocal pieces were given by the choir of the church at intervals during 
the afternoon, concluding with the chorus ‘‘ Glory to God,” from Han 
del’s Messiah.——On the 1st ult., a vocal concert was given at the Old 
Tow Hall, St. Nicholas-street, the programme of which consistel 
entirely of Christmas carols. The singers were the choirs of St. 
Christ, and All Saints’ Churches, and they were conducted by Dr, 
lor, organist of St. Mary’s. The programme was divided into two parts, 
the first comprising ancient carols, and presenting several pie. 
of the oldest English music extant; and the second consi of 
modern carol compositions, ancient words being in two or 
instances used with modern music. The peculiar plaintive character 
of many of the older pieces was very striking, and the almost 
effect of some of the combinations and cadences could not fail t be 
observed. Amongst the modern carols was one entitled, “Ange 
voices,” the music being the composition of the Rev. R. Brown 
Borthwick, which was warmly applauded. ‘ Sleep, holy babe,”was 
very beautiful in effect, as was also the carol, ‘When I view the me 
ther,” music by Barnby. ‘Though poor be the chamber,” masi¢by. 
Gounod, was sung as a solo by Mr. Bland, with a unisonous chorus 0? 
men’s voices. The solos in the carols were given respectively by Master 
H. Turner, Master Storer, Mr. Bland, Mr. James, and Mr. Watson, 
The concert coneluded with the pastoral recitatives and the following 
chorus, “Glory to God,” from Handel's Messiah. Dr. Naylor a 
companied all the pieces on the pianoforte, kindly lent for the occasion 
by Mr. R. Turner, Westborough. At the close, Canon Blunt, expressed 
his thanks to Dr. Naylor, for his kindness, courtesy, and zeal, in 
getting up the concert; and to the members of the choir for ther 
excellent efforts

Sours Brent.—The first concert of the Choral Society took place it 
the Hall, on the 7th ult., before a large audience. The choir,c 
of 40 voices, sang several choruses, three of which, ‘‘O hush thee, my 
babie” (Sullivan), ‘‘Carnovale ” (Rossini), and ‘‘ Where art thou, beam 
of light ” (Bishop), were encored. Miss Triggs gave with much effect 
two songs, and Admiral Sir L. Harper and Lady Harper sang with 
equal success two duets. Ascher’s ‘‘ Lucrezia Borgia ” (well played by 
Mrs. Spean Cole), ‘‘ Allegro and Allegretto” (Mozart), for concertina ete 
piano (carefully rendered by the Rev. E. Kitson and Miss Kitson). and 
Sainton’s ‘ Fille du Regiment” (cleverly performed by Master Row- 
lands, a lad of 14), were the most effective instramental pieces. The 
members of the choir and their conductor, Mr. W. H. Hannaford, have 
good reason to be pleased with their first entertainment

Stamrorp.—On Friday evening the 10th ult., through the kindness 
of Mr. T. K. Parker, the members of the choir of St. Mary's Church, 
were invited to spend the evening at that gentleman’s residence in St. 
Mary's Street, when they were very hospitably entertained. The pro 
gramme of the after supper entertainment was (as might be expected 
at such a gathering) exclusively musical; solos, duets, trios, 
choruses, being sung in excellent style and with marked precisioa, 
under the able direction of Mr. Pearce, the organist. ne

Strockrort.—A concert was given at the Mechanics’ Institution, 
Friday evening, the 17th ult., for the benefit of Miss Lucy B: ¥ 
under the patronage of the Mayor and borough Magistrates. The vocalist 
were Miss M. Standen and Mr. F. Hollins, who were highly succeseful 
in several songs. Miss Bristow was assisted by Herr F. Vetter (violin) and 
Herr Van Biene (violoncello). The clever pianoforte playing of the com 
cert-giver was the principal attraction. She was heard to great adval- 
tage in the sonata by Beethoven, Op. 26; and the “ Kreutzer” Sonaly 
by the same composer (with Herr Vetter); also a composition by 
Stephen Heller. Miss L. Bristow is studying in London, under Bt 
Bradbury Turner, Mus. Bac., who has every right to be proud of 
talented pupil

Woxtnenam.—Mr. T. 8. Brown gave a concert in the Town Hall on 
the 20th ult. Several glees, from ‘‘The Standard Glee Book,” We 
well rendered, by Miss A. Larkcom, and Messrs. Large. Hunt, af 
Christian. ‘The Fairest Flowers” (Danby), a scarce glee, was intro- 
duced with excellent effect. Mr. Large was encored in “Tell — 
Mary ” (Hodson), and Mr. Christian received a similar compliment

scone AYLOR, JOHN, Mus. ‘Deo —Two Choral Hymns

isting ‘y ere toe behind, the deep before,” and ‘ Let no tears to-day be 
¢, my v 
bo , W. T.—Arrangements from the Scores of ‘the 
with “Grent Masters for the Organ, price 2s. each. 
ed by $3, Adagio from the 3rd Quintett Mozart. 
—_ ah mipeen, Depo ag wicked bend their bow w (Anthem Shia 
; n the Lord” ‘eis ee ee andel. 
howe Adagio, Symphony in G aes 3 : Haydn. 
The No.4. Scherzo, 2nd Symphony ; . Beethoven. 
have erture, Messiah .. Handel. 
‘Wo. 95, re is re pd A major, Pianoforte and Violin, Mes 
: p. 12 ethoven. 
we uy Chorus, Go your way into his gates (Jubilate) » Handel. 
urch, 
n Ste ¥ Chorus, Tell it out among the heathen (Anthem 
pro- “Ocome let us sing”) .. ee Handel. 
a, Po J. MAKINSON.—Andante for the Organ. 
sion, 
“7 . TONE, F. E.—Theme with Variations for 
e Organ. 26, 
ist H, WALTER.—Thou shalt see her. Song. 
=| - Answer to ** Only to see her.” 1s. 6d. 
com EE, ALICE.—Daisies and Violets. Ballad. Poe 
vane 
ae by F. WEASHERLY. Price 1s. 6d. 
ep MMERMANN, AGNES.—True love (Treue liebe). 
‘his duet, 1s. 6d. 
é i in E minor, for the Pianoforte. Op. 20. 1s. 6d. 
i’ STONE, ROBERTSON, Jun.—The Gay Birds 
at Gre Carolling. Four-part Song. 8v0., 3d. 
ne ~ LEY, Professor, Mus. Doc.—Nos. 3 and 4, 
a ‘untin; Song, and “ Love's Philosophy ”) of “ Six Part- Songs 
Uerreateg eices.’ Op. 17, price a1

NOVELLO’S 
ORIGINAL OCTAVO EDITION OF




OPERASEdited, Corrected according to the Original Scores, and 
Translated into English, by Naratia Macrarren. 
Price 2s. 6d.; or handsomely bound in scarlet cloth, gilt 
edges, 4s

NOW READY. 
BEETHOVEN'S FIDELIO

With German and English words.) 
With the two great overtures as usually performed; being the only 
Pianoforte Score that has been published agreeing with the original 
score as to the notes and signs for phrasing




AUBER’S FRA DIAVOLO, 


MOZART’S DON GIOVANNI


BELLINI’S NORMA


VERDI'S IL TROVATORE, 


DONIZETTTS LUCIA DI LAMMERMOOR, 


WEBER’S OBERON, 


ROSSINTS IL BARBIERE, 


DONIZETTI’S LUOREZIA BORGIA, 


MOZART’S LE NOZZE DI FIGARO


VERDI'S RIGOLETTO, 


BELLINIS LA SONNAMBULA


WEBER’S DER FREISCHUTZ. 


WAGNER’S TANNHZUSER


AUBER’S MASANIELLO. 


BELLINIS I PURITANL 


WAGNER'S LOHENGRIN


DONIZETTI’S 


LA FIGLIA DEL REGGIMENTO. 


ROSSINI’S GUILLAUME TELL


772


T. THOMAS’'S CHURCH CHORAL SOCK


O PROFESSORS OF MUSIC.—FOR SALE ; 


IN AID OF 


NORTH COATES CHURCH ORGAN FUND. 


THE SECOND AND CONCLUDING VOLUME


THE VILLAGE ORGANIST


ANGLICAN CHORAL SERVICE BOOK, 


WWSELEY AND MONK’S PSALTER AND 


ULE’S COLLECTION OF 527 CHANTS, 57 


ULE’S DIRECTORIUM CHORI 


ORDER FOR THE HOLY COMMUNION


ANGLI


ADDITIONS TO THE 


EV. T. HELMORE’S PLAIN SONG WORKS. 


8. PERCIVAL


RLE, DR. — MAGNIFICAT 


OHN M. W. YOUNG’S EASY FESTIVAL SER


OUR HYMNS, 


*TRIUNE GOD,—THY PRAISE TO SING. 


CHRIST ALONE! CHRIST ALONE! 


JUST AS I AM.—WITHOUT ONE PLEA. 


BREAST THE WAVE, CHRISTIAN. 


NEW NUMBERS


BEST’S ARRANCEMENTS


FROM THE SCORES OF THE GREAT MASTERS


FOR THE ORCANPrelude and Fugue (E minor) from Pianoforte Works “Mendel 
82. Chaconne (F major) ... Handel. 
Andante con variazioni from Pianoforte Duets, ‘Op. B52 Weber. 
Romanza ditto ditto Weber. 
See the conquering herocomes_... Handel. 
83. Funeral March ‘‘ On the death of a hero,” from ‘Piano

forte Sonata, Op. 26 abe ae ions ... Beethoven. 
March, B minor, Op. 27. No.1 :. . F. Schubert. 
Hunting Song from the * Waldscenen, = Op. 82. R. Schumann. 
Adagio from Pianoforte ameteg st 10 ace * eos Weber

84. Fugue,inGminor ... we eee cam Mozart. 
March Triomphale ... eve - Moscheles. 
Fantasia from 6th Quartet, in E flat major. Haydn

In ‘the Lord”) see P 
Adagio,SymphonyinG@ ..  .. 0... eee

94. Scherzo, 2nd Symphony _.... aud wee we «. Beethoven. 
Overture, Messiah Handel

95, Andante, Sonata, A major, Pianoforte and Violin, Op

12, No.2 oge --- Beethoven. 
Chorus, Go your way into his gates, Jubilate ... nae Handel. 
Chorus, “ Tell it out among the ee (Anthem “O

come let us sing”) .. econ Handel




NEW EDITIONS (OCTAVO


ORATORIOS, CANTATAS, && 


JUST PUBLISHED. 


NOVELLO, EWER & C0.’S


ORGAN AND HARMONIUM MUSIC


SACRED MUSIC, WITH ENGLISH WOBDS | 


MUSIC FOR THE COMING SEASON


UMER, HENRY.—LORD, HOW ARE THEY INCREASED. 


AOLCK, OSCAR.—O LORD, HOW LONG WILT THUU FORGET 


LONDON: NOVELLO, EWER & CO


OPKINS, E. J.—IN MY DISTRESS I CRIED UNTO THE 


RONS, HERBERT 8.—SHEW THY SERVANT THE LIGHT OF 


— WHY RAGE FIERCELY THE HEATHEN. 


OURS, B.—IN THEE, 0 LORD, HAVE I PUT MY TRUST. 


NEW YORK: J. L. PETERS


CHARLES HALLE’S


PRACTICAL ~ PIANOFORTE SCHOOL0 
0

Beethoven, L. van. Sonatina in G 
5 Hummel, J..N. Rondo in C 
6 Steibelt, D. Sonatain C

Section I

1 Clementi, M. Sonatina in.C, Op. 36, No. 1

2 Beethoven, L. van. ‘Sonatina in F

3 Dussek, Jali. Sonata in G, Op. 20, No. 1

Kuhlau, By * tina.in C, Op. 20, No. 1 ss

6 Beethoven, . bon Sonata in G, Op. 49, No.2

7 Clementi; natina in CO, Op..36, No.3... 
8 Schumann; “Six Album Leaves (first selection

Easy

12 Beethoven, L. van. 
non;”.in G *. 
13 Heller, St: Rondino in G aa 
14 Kuhlau, F, . Andantino and Polacca i in LF. ie i 
15 Beethoven, L. L. van.; Sonata.in G minor, OF 
0 
16. Clementi, M: ‘Sonatina in F, Op. 36, No. 4 
17 Mozart, W..A.. Rondo in D 
18 Clementi,.M. Sonatina i in E flat, 1 OP. 87, No

Variations ‘on’ Nel cor

Section III.—‘* Moderately Difficult

1 Scales in all the Major and Minor Keys, and 
Chromatic Scales... = 
2 Dussek, J. L. , Sonata in A major, Op. 20, No. 4 
3 Hunten, F. Rondoletto in C major jai ds 
4 Haydn, J, SonatainG major... * 
§ Beethoven, L: van. Rondo in C mdgjor, Op. ‘61, No.1 
6 Schumann, R. Six Album Leaves (second selec- 
" tion), from Op. 68 . ey Oe eee 
© Mozart, W. A. Sonata in C major. ogre 
8 Beethoven, L: van. Air with Variations, in G. 
..» Major 
9 Hummél, J. N. Rondo villageois, in © major, 
Cp. 122

10 Clementi, M. Sonata in E fiat major, Op. 20°... 
11 Bertini, H. Four characteristic Pieces, from’ Op. 
29 and 32 . ai




IN THE PRESS. 


IN THE PRESS14 Weber, C. M. von. Andante con Variazioni

and Rondo, from Op. 3. 4 
15 Schubert, F. Twelve Valses. 
16 Beethoven, L. van. Sonata in G major, Op. 14, Ni 
17 Field, J. "[Two-Nocturnes. 
18 Reinecke, C. Scherzo, Hunting Song and Too

from’ Op. 77. 
19 Clementi, M. _ Rondo in D:major, from Op, 89, 
20 Heller, St. Three Melodies? 
21 Beethoven, L, van. Rondo in G major, Op. 51, No 
22 Mendelssohn, F, ‘The Rivulet. 
23 Bach, J.'S. ‘Prelude, Aria and Courante. iv - 
24 Haydn, J. Sonata in E flat. ; + 
25 Hummel, J. N. |“ La Contemplazione,” from Op. ¥ 
26 Handel, G. F.. The, Harmonious Blacksmith. 
27 Mozart, W. A. Sonata in A’major. 
28 Mendelssohn; F. . Four Songs without words. 
29: Dussek, J. L.. Andantino and Allegro in G, in OF 
30 Beethoven, L. van. Sonatina in G major, Op, 79

Section IV.—“ Difficult




FORSYTH BROTHERS


IN; REGENT CIROUS, OXFORD STREET


MANCHESTER : . CROSS STREET, AND SOUTH KING STRE


XUM