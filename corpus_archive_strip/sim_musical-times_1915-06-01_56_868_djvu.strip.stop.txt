


 ROYAL CHORAL SOCIETY 


 ROYAL ACADEMY MUSIC , 


 YORK GATE , MARYLEBONE ROAD , LONDON , N.W 


 K.G. 


 F.R.A.M 


 WIDSUMMER HALF - TERM BEGINS MONDAY , JUNE 14 . 


 J. A. CREIGHTON 


 MUSIC 


 ROYAL ACADEMY 


 ROY . AL COLLEGE ‘ MUSIC , 


 RINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. 


 GUILDHALL SCHOOL MUSIC 


 HN CARPENTER ST . , VICTORIA EMBANKMENT , E.C. 


 PRINCIPAL LANDON RONALD . 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 FOUNDED 1844 . 


 PUBLISHED MONTH . 


 ASSOCIATED BOARD 


 R.A.M. R.C.M. 


 LOCAL EXAMINATIONS MUSIC 


 BIRMINGH . & MIDLAND INSTITUTE 


 SCHOOL MUSIC . 


 SESSION 1914 - 1915 . 


 ROYAL 


 | MANCHESTER COLLEGE MUSIC 


 LL.D 


 STANLEY WITHERS 


 COLLEGE ORGA 


 NISTS 


 ROYAL 


 MANCHESTER SCHOOL MUSIC . 


 UNIVERSITY DURHAM . LONDON COLLEGE MUSIC , C 


 ASSOCIATE ( A.1.G.C.M. ) , LICENTIATE ( L.1.G.C.M. ) , FEL 


 


 NATIONAL CONSERVATOIRE 


 ER 


 INGING 


 DN 


 , 1915 . 323 


 M. MACDONALD SMITH 


 19 , BLOOMSBURY SQUARE , W.C. 


 PIANO PEDALS 


 ORGANIST 


 ‘ NORMAN & BEARD 


 PNEUMATIC PEDAL ATTACHMENT 


 PIANO 


 PROFESSIONAL NOTICES 


 MADEMOISELLE 


 PRUDENCE SPANOGHE 


 ( BELGIAN OPERATIC SOPRANO 


 MR . W IL . JF ‘ RID ABOR 


 SAM UE MASTERS 


 MR 


 LONDON COLLEGE CHORISTERS 


 COMPOSERS ’ MSS 


 DR . A. EAGLEFIELD HULL 


 NORMAN SPRANKLING 


 DR 


 EVISION MUSICAL COMPOSITIONS . 


 L.R.A.M. ( PAPER WORB 


 A.R.C.M. ( PAPER WORK ) . 


 324 


 F.R.C.O. SPECIALIST CORRESPONDENCE 


 R. ARTHUR S 


 DR . LEWIS ’ TEXT - BOOKS : 


 ARTHUR MANGELSDORFF , L.R.A.M. , 


 COACHING DEGREES . L.R.A.M. , 1897 - 1914 , 


 THIRTY - SUCCESSES ; A.R.C.M. , 


 1897 - 1914 , - SUCCESSES . 


 ISS MARGARET YOUNG , L. R. A. M. , A.R.C.M. 


 OXFORD 


 NEW COLLEGE CHORISTERSHIPS 


 OXFORD , NEW COLLEGE 


 EXAMI 


 S ! . CUTHBE RT ’ S , KENSINGTON . — BASS 


 _ _ _ _ 


 TANTED . 


 TUNER . 


 PIANOFORTE 


 OLD FIRM 


 P. CONACHER & CO . , LTD . 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD . 


 GOLD MEDALS 


 OPPORTUNITY 


 RARE 


 ORGANISTS MUSICAL 


 PATH LIFE 


 " OLIN 


 OFORT 


 ARRAN 


 TUTE 


 GARP P 


 MESSRS 


 G. P. PUTNAM SONS 


 , COLLEGES 


 N ORTE 


 UDENTS 


 LIFE 


 FAVOURITE 


 1915 . 325 


 NOVELLO ' EDITIONS 


 


 ADAPTED 


 CONCERT USE 


 1 . FAUST . 


 2 , IL TROVATORE , 


 TANNHAUSER . 


 4 . MARITANA . 


 BOHEMIAN GIRL . 


 OPERAS 


 IMPERIAL 


 MES 


 OFORTE STRINGED INSTRU MENTS 


 PRODUCED 


 ROYAL ALBERT HALL , APRIL 11 


 PRO PATRIA ’ 


 ORCHESTRA 


 COMPOSED 


 J. D. DAVIS 


 PRESS : 


 SERENADE 


 


 WAND YOUTH ” 


 ( SUITE ) 


 COMPOSED 


 EDWARD ELGAR 


 PIANOFORTE 


 CELLO PIANOFORTE 


 ARRANGEMENT SMALL ORCHESTRA 


 CLARINET 


 MUSIC COMPOSERS 


 SRS . WEST & CO 


 WEST & CO . , 


 MARCH 


 ENGLISH LYRICS 


 WN 


 C. HUBERT H. PARRY 


 SET 


 SECOND SET 


 SET 


 FOURTH SET 


 FIFTH SET . 


 SIXTH SET 


 SEVENTH SET 


 EIGHTH SET 


 NINTH SET 


 326 


 NEW EDITION ( 1912 


 HANDBOOK EXAMINATIONS 


 MUSIC 


 CONTAINING 


 600 QUESTIONS : S ANSWERS 


 ERNEST ‘ A. DICKS 


 SC 


 PREFACE NINTH EDITION 


 VALUABLE BOOK TEACHERS STUDENTS . 


 TECHNIQUE 


 EXPRESSION 


 PIANOFORTE PLAYIN 


 FRANKLIN TAYLOR 


 EXTRACT PREFACE 


 NUMEROUS MUSICAL EXAMPLES 


 WORKS GREAT MASTERS . 


 , 1915 


 COUNTRY DANCE TUNA 


 CECIL J. SHARP 


 SE 


 EDITED 


 CECIL J. SHARP 


 COMPANY 


 NOVELLO 


 


 


 SECTION A.—TECHNICAL PRACTICE . BOOKS 


 SECTION B.—STUDIES . BOOKS 


 CLOTH BINDINGS 


 PIECES 


 ARETHUSA 


 WILLIAM SHIELD 


 ARRANGED MEN VOICES ORCHESTRA 


 FRANK M. JEPHSON 


 CHIMES 


 


 GLOUCESTER CATHEDRAL 


 ARRANGED PIANOFORTE 


 C. LEE WILLIAMS 


 ORGAN TRANSCRIPTION A. HERBERT BREWER 


 COMPOSERS 


 SELECTED FR 


 _ _ 


 OADWOOD 


 PIANOS 


 BOSWORTH EDITION 


 NEW ENGLISH EDITION 


 ENLARGED REVISED , 


 GRANVILLE BANTOCK BENEDICT 


 TEACHERS . 


 OM CATALOGUE 10,000 WORKS 


 BOSWORTH PORTRAIT CLASSIO 


 CONSTITUTE RECORD CHEAP , CORRECT , 


 HIGH - CLASS ENGLISH PRODUCTIONS 


 BOSWORTH & CO . , 


 PHYSICAL 


 MANUALS 


 EXERCISES , DANCES 


 GAMES 


 


 INFANT SCHOOL 


 MARGARET ALEXANDER HUGHES 


 MUSIC ARRANGED 


 JD 


 VISED , 


 EDICTUS 


 U 


 MODERN 


 00 WORKS 


 LASSIC 


 IRRECT , 


 NS 


 NDON , W 


 PRINCIPLE 


 Y MUSIC 


 GHES 


 SINGING - CLASS CIRCULAR . 


 JUNE 1 , 1915 


 ALEXANDER SCRIABIN . 


 33 


 29 


 22 > . 


 “ 35 


 ENGLISH CARILLONS , 


 CATTISTOCK CARILLON , 


 £ 3,000 


 BELL - TOWERK 


 CATTISTOCK 


 BELL - TOWER 


 LOUGHBOROUGH 


 SCALE . 


 <3 ] SS OS 


 SCALE 


 SCALE . 


 333 


 LOUGHBOROUGH CARILLON 


 SCALE 


 PROPOSED WESTMINSTER CARILLON 


 SCALE 


 334 


 SC 


 MANCHESTER TOWN HALL CARILLON , 


 ALE 


 ABBERLEY HALL CARILLON 


 SC 


 WORCESTER CATHEDRAL CARILLON 


 SC 


 ALE 


 ALE 


 1914 . | 


 335 


 EATON HALL 


 CARILLON 


 1914 


 1914 


 336 


 337 


 XUM 


 G BD F 


 6 , 7 ) 9 ; 13 


 39 


 9 , 11 


 339 


 128 II 


 176 


 BANDS , 


 WAR , 


 RACING , 


 FASHIONS 


 


 QUEEN HALI 


 ORCHESTRA : 


 FINE 


 SUMMER 


 OPERA 


 SEASON 


 MUSIC 


 CAPTIVITY 


 DOINGS 


 CAPE 


 CON 


 341 


 775teresting season . ban placed old German 
 omposers open question , 

 dmitted attractive programmes 
 = de een drawn . find 
 " Weed wondering , , date considered 
 ‘ oncens 4 unpatriotic return Bach Beethoven . 
 mphony , 
 CONCERNING CESAR FRANCK . 
 CECIL GRAY . 
 te # César Franck problem 
 ‘ ation ! § sysical theorists historians — perplexing , 
 hand , claim music national 
 fier tit Biss , singularly un - French characteristics , 
 SolosS Bf nd upholders classic tradition , 
 semens Ben account orthodoxy matters form 
 sible © Fontrasted daring harmonic pro- 
 1 beet B ressions found works . 
 Biscitate admit midst dangerous 
 © , wee ily . beginning realise accept 
 ve beet Bi Franck true spiritual heir Bach , , 
 ‘ , leading wooden horse walls 
 twhose capacious belly hidden score 
 eresting loodthirsty impressionists . admit Franck 
 ne 9 circle elect 
 tion Bumender , tacit admission continuity 
 heal musical development classics 
 ‘ ranck impressionists . look 
 pies tte following passage Franck Symphony 
 talise affinity exists 
 ring 2 modern Frenchmen : 
 bet César FRANCK 

 ee ear . 
 girt pp 
 fr . BI , b ee bee . 
 ohn E ye — 42 = e 
 > matr — . ee — t — t — E — 
 

 , Mr. Colles bases argument 
 largely nature Franck themes . 
 discovers , rightly , Franck themes revolve 
 |round pivot , goes ‘ 
 | plan sonata form , development , particularly 
 recapitulations , conditioned 
 travelling type melody circling 
 . example chooses illustrate 
 theory beautiful passage Schubert 
 ‘ Unfinished ’ Symphony , , Mr. Colles says , 
 ‘ sees antithesis Franck method 
 series adventures keys 
 determining factor development 
 melody 

 , ideal thematic 
 material symphony different . 
 symphonic theme gwé symphonic theme ( Mr. 
 Newman ) brief point , pregnant 
 possibilities , movement 
 inevitably evolves . Schubert unsuccessful 
 symphonist mainly account inability write 
 themes . César Franck Symphony 
 striking example music 
 evolution trom germ . 
 entire thematic material 
 movements derived 
 theme Lento . Furthermore , examination 
 Beethoven melodic material reveals fact 
 symphonic theme par excellence possess 
 characteristic adventuring keys , 
 determining factor development 
 melody . far 
 best symphonic melodies essentially 
 circling variety — example , theme 
 ‘ Eroica , ’ example ‘ hypnotic concen- 
 tration note ’ need look 
 Allegretto Seventh 

 Let thought moment 
 attempting lay law symphony 
 written . merely questioning 
 Mr. Colles statement ‘ plan sonata 
 form . conditioned travelling type 
 melody , circling . ’ mistake 
 imagine symphonic style 
 conform . contrary , possibilities 
 inexhaustible . Time wither custom 
 stale infinite variety . fact Franck 
 symphonic style opposite , , 
 Brahms Schubert , Franck detriment 
 Brahms . symphony stop 
 Haydn , Mozart , Beethoven , 
 Brahms , reason suppose 
 end reached . symphony continue 
 extend , receiving new significance 
 genius touches 

 conclude . Franck Symphony great work , 
 adverse criticism remove 
 place holds greatest symphonies . 
 new addition great chain mountains , 




 MUSICAL 


 


 CATHEDRAL ORGAN 


 EAST 


 ORGAN CATHEDRAL . 


 CATHEDRAL ORGAN WEST 


 S 8) C ¢ \ 


 ACCESSORIE 


 EDINBURGH SOCIETY ORGANISTS : PLEBISCITE 


 RECITAL 


 ORGAN RECITALS 


 APPOINTMENTS 


 LINCOLN INN CHAPEL MUSIC . 


 346 


 ORCHESTRA . 


 ORGAN 


 PIANOFORTE . 


 GREAT 


 347 


 STEGGALL 


 MR . REGINALD 


 48 


 1901 


 35 


 BOOKS RECEIVED , 


 ACADEMIC TEACHING 


 EDITOR OI 


 PALESTRINA 


 * MUSICAL TIMES 


 _ _ 


 SONATA ’ 


 ‘ MUSICAL TIMES 


 CESAR FRANCK 


 EDITOR 


 LBERT 


 CHURCH 


 EDITOR ‘ MUSICAL TIMES 


 EDITOR OThe 

 ssfuence Beethoven . 
 mes coupled connection solely 
 wason began writing pianoforte trios 
 ud publishing composite Op . I. 
 pen begin imitators , Franck conspicuous exception 
 te law , work question written 
 ib previous experience music knowledge 
 canons composition.—Faithfully 

 IRELAND 

 

 D’Indy , zeal cry classical inspiration , 
 ws acumen lead , , amusing 
 nest , tracing origin composition , 
 Beethoven . 
 ‘ eosthumous ’ mood dealt apparently bald 
 fobidding material . questionable 
 wet obtained mightier epic effects directness 
 9 complete absence fortuitous frippery . 
 gant , t00 , Franck repeated astonishing 

 truth reflects spiritual 




 ALEX . COHEN 


 HYMNAL 


 MUSIC 


 AL TIMES 


 H. MACHIN 


 ORIGIN NATIONAL ANTHEM 


 VERONIQUE ’ ADELPHI . 


 ANGELS HOVER . ) 


 CHORUS “ MARITANA 


 STREETS ( MARRERO TT 


 — — — SS 


 ANGELUS 


 4 , — _ 


 58 


 COUNTERPOINT : 


 PLACE MUSIC 


 HARMONthe great powes 
 discrimination . best solution find thes 
 species memory impressions combined w 
 | natural good taste , instinct developed 
 original desire authority subject . Thy 
 compared h 

 people read bovk , 
 Stevenson , instance , pleasure thes 
 gives delight incomparable ! 
 style writer . listen C mi 
 Symphony hear understand nature wi 
 ( cases better intellectual listener ) wie 
 appreciating wonder Beethoven wntz 
 great work apparently scanty maternal 

 influence makes felt spiritual 
 | music : prevents art mere juggig 
 double counterpoints canonic imitations , t 
 ‘ dried bones academic tradition . ’ certal 
 Beethoven possessed great power e 
 appeal listeners understand , shows 
 attention intellectual si 
 great composer , mt 
 Thomas Hardy great writer 
 confined beauty style , wi 
 | evolved great poignant tragedy like ‘ Tes 

 intellectual listener , educa 




 08 


 _ — _ — 


 MUSICAL TI 


 MES.—JUNE 1 , 1915 


 359 


 SHAFTESBURY THEATRE : OPERA ENGLISH 


 360THE MUSICAL TIMES.—June 1 , 1915 

 BACH - BEETHOVEN - BRAHMS FESTIVAL . 
 QUEEN HALL 

 Festival projected Mr. Daniel Mayer 
 Mr. Verbrugghen year . credit 
 gentlemen obstacle War sore feeling 
 Festival consisting exclusively German music 
 tune times deter carrying 
 scheme entirety . evident size 
 audiences attracted British concert - goer 
 regards ‘ B ’ precious personal possession . 
 specially organized choir Leeds , magnificent 
 London Symphony Orchestra , best available British 
 solo executants engaged . Mr. Henri Verbrugghen 
 undertook burden conducting 
 formidable programmes given 

 Brahms concert April 23 perha 
 varied enjoyable series . prope 
 | sisted ‘ Tragic ’ Overture , Pianoforte concerto ig 
 B flat , Rhapsody alto solo , male - voice choir 
 orchestra , Symphony D , excellent scheme 
 containing hackneyed insisting 
 composer . Mr. Evelyn Howay . 
 Jones gave fine performance Cuncerto , playing 
 Finale particularly brilliant . Madame Kirty 
 Lunn deep impression : singing ty 
 Rhapsody , efforts seconded choir 
 orchestral works splendidly played , ms 
 scene enthusiasm close Symphony , 
 peroration Mr. Verbrugghen worked toy 
 terrific pace . Brahms , ws 
 certainly thrilling , audience showed appre . 
 tion recalls 

 hall crowded April 24 , Festival closed 
 Brahms ‘ Requiem ’ Beethoven ninth Symphony . 
 Leeds Choir rose occasion concert , ag 
 gave performance Finale Symphony cam 
 comparison best head 
 ‘ Requiem ’ unequal , quieter 
 passages marred slight flattening 
 trebles tenors . soloists Miss Lim 
 Stiles - Allen , Miss Ruby Heyl , Mr. Harold Wilde , Mi 
 Herbert Heyner . orchestra conductor showed 
 signs fatigue heavy labours d , 
 form . acclamatios 
 loud long close , Mr. Verbrugghen presented 
 laurel wreath times recalled . Congratuh- 
 tions concerned carrying exacting 
 week scheme undoubted success . gid 
 Festival , somewhat different lines , 
 promised year 

 NOTES HISTORY ‘ ADESTE FiDELES ’ 
 W. H. GRATTAN FLoop 

 obscurity close century attached 
 history hymn ‘ Adeste Fideles ’ — regards 
 words tune — recent years ot les 
 cleared ; persons belier 
 ‘ traditional ’ account history , 
 record present notes . let dismiss 
 apocryphal statement beautiful Christmas pros 
 goes ‘ medizeval days , ’ traced 
 ‘ 15th century , ’ found ‘ 16d 
 century Cistercian Gradual . ’ mor 
 certain ‘ Adeste Fideles , ’ fron 
 internal evidence , dated earlier 
 second decade 18th century . ascription 

 innovation welcome . Mr. 
 Verbrugghen conducted considerable zeal skill , | 
 fervidly . 
 April 21 Beethoven scheme submitted , consisting | 
 Overtures ‘ Fidelio ’ ( better | 
 known ‘ Leonora ’ ) , ‘ Emperor ’ Concerto , fifth | 
 Symphony , recitative air ‘ Fidelio . ” 
 musical grounds , performance Overtures | 
 concert commended , students 
 listeners interested composer methods | 
 development likely fully edified . Mr. Verbrugghen | 
 obtained intensely virile performances , 
 Symphony . M. de Greef pianist , playing | 
 conspicuous delicacy , Miss Sybil Vane sang . | 
 April 22 brought Beethoven programme , consisting | 
 Overture Adagio ‘ Prometheus ’ Ballet | 
 music(’celloobbligato , Mr. Patterson Parker ) , ‘ Coriolanus ’ 
 Overture , Beethoven Massin D. Leeds singers 
 grappled task courageously 
 successfully . failed , blame laid 
 composer . work , | 
 Beethoven anybody , contains sublime 

 tune John Reading , ¢. 1680 , merely rests statement 
 Vincent Novello , collection entitled ‘ Home mas 




 361 


 597 


 SS 


 SS OS OOO 


 SS SS SSS 


 362which compiled Shane Leslie J. S. Collins 

 authorised use Ireland , dedicated 
 Cardinal Logue , Archbishop Armagh . consists 
 150 hymns — number Psalter — tunes 
 selected sources . plainchant melodies 
 Sarum York , Vatican chants represented , 
 composers include Palestrina , Goudimel , Pergolesi , 
 Bach , Arne , Beethoven , Mozart , Haydn , Mendelssohn , 
 Gounod , Webbe , Novello , Dykes , Smart , Wesley , Monk , 
 Elgar , Southgate , Stewart , new tunes 
 Rev. V. Russell , Rev. C. Smith , Rev. H. Bewerunge , 
 Dom S. G. Ould , O.S.B. , Paul Clark , T. W. Holden , 
 R. G. Reynolds , editor . hymn - writers 
 Cardinal Newman represented lyrics , 
 English sources include specimens Rolle , Southwell 

 j 




 CLUTSAM CRADLE KEYBOARD 


 1S 


 EILLAISE 


 1 , ” 1001 , ¥ 


 ACCOUNT - BOOK 


 DUBLIN HARPSICHORD MAKER : 


 FERDINAND WEBER , 1764 - 83 


 21780 


 1780 


 1781 


 FESTIVAL BRITISH MUSIC QUEEN HALL 


 ORCHESTRAL CONCERTS BRITISH COMPOSERS 


 LONDON SYMPHONY ORCHESTRA 


 QUEEN HALL ORCHESTRA 


 MUSIC CLUB 


 _ _ _ 


 | LONDON CHORAL SOCIETY 


 MR . JOSEPH HOLBROOKE CHAMBER CONCERTS 


 ALEXANDRA PALACE CHORAL ORCHESTRAL SOCIET 


 L SOCIETY . 


 365 


 CORRESPONDENTS 


 BIRMINGHAMA Serbian Relief Fund concert given 
 Temperance Hall April 30 , provided Mr. Charles 
 Sutcliffe concert party , ‘ Meistersingers , ’ quartet 
 pleasing vocalists , assisted Miss Hilda Gibbs , young 
 pianist great promise , string trio , ‘ Brugeois 
 Haantje ’ ( Mlle . Mariette Wychaert , Mlle . Jeanette 
 Wychaert , M. Willem Jan Wychaert ) . addition 
 artists , Birmingham Victorian Male - voice Choir 
 ( conducted Mr. W. E. Robinson ) , Mr. Percy Edgar , 
 actor - entertainer , contributed lengthy programme 

 fifteenth annual concert promoted students 
 Midland School Music attending orchestral 
 choral classes took place Town Hall 12 , 
 direction Prof. Granville Bantock . 
 interesting varied programme forthcoming , containing 
 novelties . special interest orchestral tone- 
 poem ‘ Dawn , ’ Op . 16 , W. J. Fenney , young student 
 previous occasions figured promising 
 composer . latest contribution showed considerable 
 inventive imaginative ideas way orchestral 
 colouring handling themes : , led 
 assume Mr. Fenney accomplish great things 
 future . entire novelty provided Scene 
 Choral Dances Borodin ‘ Prince Igor , ’ culled Act 2 , 
 work barbaric glitter scored choir orchestra . 
 exposition better rehearsals 
 devoted , especially regards choral portion . 
 rest programme included movements 
 Organ sonata Reubké , played Mr. W. J. Dunn , 
 suiraud Caprice violin orchestra , soloist 
 Miss Kathleen Washbourne , gifted young violinist , 
 Mr. Joseph Yates gave graphic interpretation ‘ Mono- 
 logue ’ Wagner ‘ Mastersingers , ’ Mr. Hubert 
 Simmonds sang ‘ Serenade ’ Moussorgsky , 
 song - cycle ‘ Dances Death . ’ pleasing number 
 Miss Hilda Reybould sympathetic singing Hebridean 
 songs , Miss Violet Lewis played movement 
 Beethoven fourth Pianoforte concerto , 
 efficiently accompanied orchestra 

 BOURNEMOUTH 

 Mr. Gordon Bryan , playing Paderewski Polish 
 Fantasie pianoforte orchestra notable 
 alertness intelligibility ; Miss Elsa Stamford , artist 
 considerable promise , manifested chaste performance 
 Max Bruch G minor Violin concerto ; M. Maurice 
 Dambois , performance Haydn Violoncello 
 concerto artistic , delicate , refined 
 solo features season 

 Nationalism fore planning 
 Monday ‘ Pops . ’ programmes , 
 reproduce items chief importance , 
 noticed expressly designed 
 illustrations certain nationalistic principles ; , 
 , ostensibly serving ends , 
 exposition national theories shape 
 un - Prussianised German music affinity 
 world conflict , , truth , modern German 
 composer — exception Strauss 
 Humperdinck — lost power restoring : 
 April 19 , Slavonic music — Slavonic Dances ( Dvorak ) ; 
 Tone - poems , ( ) ‘ Vysehrad , ’ ( 4 ) ‘ Vitava , ’ Smetana ; 
 movements String quartet E minor 
 ( ‘ Aus meinem Leben ’ ) composer , played 
 neatly best performers orchestra . 
 April 26 , British music — ‘ Irish ’ Symphony ( C. V. Stanford ) ; 
 Tone - poem , ‘ Bamboula ’ ( Coleridge - Taylor ) ; Tone- 
 poems , ( ) ‘ Pines , ’ ( 4 ) ‘ Woods April , ’ Edith 
 Swepstone . 4 , Gems old Masters — ‘ 
 marriage Figaro ’ Overture ( Mozart ) ; Symphony . 9 , 
 C minor ( Haydn ) ; Schumann Toccata , orchestrated 
 Dalhousie Young ( time ) ; Chorale Fugue G 
 minor ( J. S. Bach } ; Largo ( Handel ) ; Ballet music , ‘ Orfeo ’ 
 ( Gluck ) ; Overture , ‘ Fidelio ’ ( Beethoven ) . master- 
 songs folk - songs sung Madame Emily Thornfield , 
 accompanied Mr. E. Thornfield ( conductor 
 Beecham Opera Company ) . 8 , Wagner programme 
 ( extracts ‘ Ring , ’ & c. ) — ‘ Entry Gods , ’ 
 * Forest murmurs , ’ ‘ Song Rhine Maidens , ’ ‘ Siegfried 
 journey , ’ Funeral march , ‘ Ride Valkyries . ’ 
 excerpts , ( ) Spring Song ‘ Valkyrie , ’ ( 4 ) Prize 
 Song ‘ Mastersingers , ’ moderately sung 
 Mr. Joseph Cheetham . concert , , Raoul 
 Vidas , boy violinist , played extraneous solo items 
 rare verve 

 large number miscellaneous concerts 
 winter season brings forth , 
 commands extensive expression goodwill ; allude 
 Mr. Godfrey annual concert , whereat furnished 
 opportunity thanking esteemed director 
 music practical manner strenuous labours 
 behalf . concert year took place April 17 , 
 , personal appeal , attract 
 music - lover thrill , Mr. Godfrey 
 secured M. Vassili Savonov conduct performance 
 Tchaikovsky ‘ Pathetic ’ Symphony . 
 sensational interpretation distinguished Russian 
 conductor obtained countryman work , 
 points presented open question , 
 performance deeply impressive . 
 concert Miss Marie Hall played violin 
 music best style , Mr. Frank Mullings 
 sang items stirring dramatic intensity . 
 event following week visit 
 Russian Opera Chorus . capable body sang 
 selection native compositions 
 effect . efficient soloists M. Moltchanov ( tenor ) , 
 M. Bornov ( bass ) , M. E. Gourevitch conducted 
 capably ; M. Kremnev Miss Hilda Munnings 
 gave excellent dances , Municipal Orchestra 
 played choice selection Russian music . concert 
 given Mr. Graham Peel aid Allied Forces ’ 
 base hospital Boulogne , took form pianoforte 
 song recital Miss Fanny Davies Miss Carmen 
 Hill , established favourites , efforts met 
 warm welcome . Coleridge - Taylor ‘ Hiawatha ’ 
 work chosen final concert Municipal 
 Choir Orchestra . splendid account grateful 
 music forthcoming Mr. Godfrey direction , 
 solo parts safely entrusted Miss Emily Breare , 
 Mr. John Booth , Mr. Charles Tree . April 28 , 
 Mlle . Mimi Carina , soprano vocalist , appeared special 




 BRISTOL DISTRICT 


 367CAMBRIDGE 

 University Musical Club open concert place 
 Term Monday , June 7 , instead performance 
 members Club Philharmonic Quartet 
 play quartets Beethoven ( Op . 59 , . 3 , C major ) 
 wd Debussy . University Musical Society concert 
 fuch Suite , series short works chorus orchestra 
 Dr. Rootham , Beethoven Violin concerto D major , 
 sith Miss Marian Jay solo violinist , played 

 Sunday , June 6 , Clare College Chapel , Bach Mass 
 F major ‘ Stabat Mater Josquin de Prés 
 performed augmented choir . Dr. Ezard , Cambridge , 
 recently brought new edition ‘ Stabat Mater , ’ 
 edition performance work 




 CORNWALL 


 PLYMOUTH 


 DEVON 


 TORQUAY 


 DEVONSHIRE TOWNS 


 CORNWALL 


 LIVERPOOL . 


 368 


 MANCHESTER DISTRICT 


 ABERD 


 369 


 BRIEFLY SUMMARIZED , 


 MUSICAL TI 


 37 ° 


 MUTE . — Counterpoint : _ Music , 
 Thomas Fielden ... 338 

 Shaftesbury Theatre : Opera agiich 3 
 | Bach - Beethoven - Brahms Festival . 
 | Sates aie thie History ‘ Adeste Fideles . ’ W. H 

 Grattan Flood jo 




 MUSICAL TIMES 


 SCALE TERMS ADVERTISEMENTS 


 54 


 SPECIAL NOTICE . 


 TUESDAY , JUNE 22 


 RC 


 


 


 BS 


 ER 


 RIN 


 C RA 


 ZER 


 AR } 


 LIONEL 


 LET 


 ERY 


 OUN 


 320 


 338 


 » W. H. 


 365 


 \LLACE 


 ES . 


 SEMENTS 


 03 ° 


 200 


 400 


 710 0 


 37 


 MONTH . 


 ARKER 


 RAWFORD , 


 1S 


 


 PUBLISHED 


 H. W. GRAY CO . , NEW YORK . 


 CHUBERT , F 


 SPRING FANCIES 


 PRELUDES 


 HARP SOLO 


 COMPOSED 


 HAMILTON HARTY 


 ORGANIST SOUTHWARK 


 ATS 


 MUSICAL 


 TIME 


 S.—JUNE 1 , I9I5 


 ANTHEMS 


 TRINITYTIDE 


 COMPLETE LIST 


 “ LUTE ” SERIES 


 MUSICAL SETTINGS 


 ROMAN LITURGY 


 DOM 


 ALMA REDEMPTORIS MATER 


 ANGELUS AD PASTORES 


 ANGELUS AD VIRGINEM 


 BENEDICTA ET VENERABILIS 


 M. H 


 NOVELLO COMPANY 


 CANTIONES SACRE 


 SAMUEL GREGORY OULD 


 RF HARVEST FESTIVAL MUSIC . 


 CANTATAS . 


 SONG THANKSGIVING — HARVEST CANTATA 


 GY CHORUS BARITONE ) SOLI CHORUS 


 WORDS WRITTEN ARRANGED MUSIC 


 MUSIC 


 SEED - TIME HARVEST 


 TENOR BASS SOLI CHORUS JOHN E. WEST . 


 MUSIC COMPOSED SONG KSGIVING 


 THOMAS ADAMS , CHORUS ORCHESTRA 


 L_LAAR RAR 


 TENOR BASS SOLI CHORUS , ORGAN TH E GLEANE R HARVEST 


 THOMAS ADAMS . FEMALE VOICES 


 TENOR BASS SOLI , CHORUS , ORGAN TH E JU BILEE CANTATA 


 SMALL ORCHESTRA SOLO VOICES , CHORUS , ORCHESTRA 


 


 HUGH BLAIR . C. M. VON WEBER 


 HARVEST CANTATA HARVEST SONG 


 CHORUS , SEMI - CHORUS , ORGAN SOPRANO SOLO CHORUS 


 


 GEORGE GARRETT . C. LEE WILLIAMS . 


 HYMNS HARVEST SOWER WENT FORTH SOWING 


 LET BRETHREN JOIN 


 JOY HARVEST SEA 


 HARVEST HYMN PRAISE COME , YE THANKFUL PEOPLE , COME 


 KRRRBRRERAEARABRRRKRRARABR RRR EB ER LAR ERED BR RP 


 ADVENT 


 CHRISTMAS 


 EASTER 


 HARVEST 


 ADVENT 


 CHRISTMAS 


 EASTER 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 HARVEST 


 GENERAL 


 ADVENT 


 EASTER 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 HARVEST 


 ADVENT 


 CHRISTMAS 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 NOVELLO ANTHEM BOOK 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 LENT 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 LENT 


 EASTER 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 WHITSUN 


 HARVEST 


 GENERAL 


 ADVENT 


 CHRISTMAS 


 EASTER 


 WHITSUN 


 GENERAL 


 300 K 13 


 0PULAR CHURC H MUSIC 


 H. MAUNDER 


 SERVICES 


 ORGAN , 


 CHURCH CANTATA . 


 REDUCED PRICE 


 ORGAN 


 Y MUSIC . 


 NOVELLO COMPANY 


 RECENTLY PUBLISHED 


 EASY 


 NOVELLO ’ — 


 ANTHEMS 


 HYMNS TUNES . 


 J. STAINER 


 F , A. CHALLINOR 


 ALBERTO RANDEGGER 


 J. STAUNER 


 ° .. T , ADAMS 


 & J. GAUNTLETT 


 A. SULLIVAN 


 J ) . STAINER 


 J. STAINER 


 PUBLISHED 


 SET 


 J. STAINER 


 BARLESS PSALTERIM 


 POINTED USE ANGLICAN CHANTS 


 EASY BOOK CHOIR CONGREGATION 


 PSALMS DAVID , 


 CANTICLES PROPER PSALMS , 


 ATHANASIAN CREED , 


 


 SHORT NOTES TEXT & EXPLANATORY PREFACE 


 WALTER MARSHALL , M.A 


 


 SEYMOUR PILE , M.A. , F.R.C.O 


 PUBLISHED . 


 BARLESS PSALTER CHANTS 


 LONDON : NOVELLO COMPANY , LIMITED . 


 ISSUED NET BOOK 


 NEW CATHEDRAL PSALTER 


 CONTAINING 


 PSALMS DAVID 


 CANTICLES PROPER PSALMS . 


 EDITED POINTED CHANTING 


 WORDS 


 NEW CATHEDRAL PS : ALTER CHANTS 


 PS : AL TEI R CHANTS COMBINED 


 AALZLLZAZ 


 AAAAAAAAD 


 


 SA 


 ER } POCKET SING - SONG BOOK 


 


 SOLDIERS , SAILORS , SCHOOLS , HOMES 


 ETC 


 CONTAINING : NATIONAL ANTHEMS , ETC . , ALLIES 


 MARCHING SONGS 


 NATIONAL FOLK - SONGS 


 HYMNS 


 PRICE SHILLING 


 COMPANY , LIMITED 


 NOVELLO 


 LONDON 


 SELF - HELP SINGERS 


 


 COMPASS OCTAVE , DAVID C. T AYLOR 


 


 J. E. VERNHAM , NAVAL MILITARY 


 37 


 1915 


 _ 


 SEVENTEENTH - CENTURY 


 SONGS 


 RESTORATION PERIODS 


 SHAKESPERIAN 


 MPANIMENTS ADDED 


 BRIDGE 


 SIR FREDERICK 


 LASS DELICATE AIR 


 SONG 


 


 DR . T. A. ARNE 


 F , DUNHILL 


 THOMAS 


 LO COMPANY 


 


 BARITONE 


 SONS 


 SONG 


 SEA 


 SAROJINI NAIDU 


 S. COLERIDGE - TAYLOR 


 FOLK - SONGS 


 


 ECIL J. SHARP 


 WORDS 


 SONGS FARMHOUSE 


 ARRANGED S.A.T.B. 


 IMPANY 


 FREDERIC AUSTIN 


 NEW SONGS 


 COMPOSITIONS 


 CHILDREN - SCENES , 


 ACTION , 


 STUDIES 


 


 CALLISTHENIC 


 CALLISTHENIC SONGS 


 ATIONS 


 INVALUABLE EXAMIN 


 WEBSTEI R 


 GROUNDWORK MUSIC 


 RAINBOW 


 MUSIC READING LADDER BEGINNERS 


 MILITARY 


 USE MILITARY BANDS CHAPLAINS 


 PARADE SERVICES CAMP CHURCH , 


 MORNING PRAYER , LITANY , 


 HOLY COMMUNION , 


 


 COMPILED ARRANGED 


 THOMAS CONWAY BROWN 


 PIONS 


 USIC 


 NOV ELLO HANDBOOKS MUSICIANS 


 EDITED ERNEST NEWMAN 


 SOURCES KEYBOARD MUSIC ENGLAND 


 CHARLES VAN DEN BORREN . 


 TRANSLATED FRENCH 


 JAMES E. MATTHEW 


 CHORAL TECHNIQUE & INTERPRETATION 


 HENRY COWARD 


 TEACHING ACCOMPANIMENT 


 PLAINSONG 


 FRANCIS BURGESS 


 MUSIC BIBLE 


 


 JOHN STAINER . 


 


 REV . F. W. GALPIN , M.A. , F.L.S 


 SONGS 


 PLAYS WILLIAM SHAKESPEARE 


 WRITTEN COMPILED 


 MRS . G. T. KIMMINS 


 MUSICAL TIMES 


 MARCHES 


 NOVELLO 


 ORGAN 


 POPULAR 


 ALBUMS 


 NOVELLO COMPANY 


 


 SELE 


 SELECTED PIECES 


 SELECTED PIECES 


 SELECTED PIECES 


 CTED PIECES 


 SHORT PRELUDES | ORIGINAL COMPOSITIONS 


 ORGAN ORGAN 


 300 K II 


 PP 


 200 K 


 ( II 


 MR . HERBERT F. ELLINGFORD . 


 


 FRANCIS EDWARD GLADSTONE 


 FUGUE E MAJOR 


 EDITION NOVELLO 


 RUSSIAN 


 MUS SIC 


 BI 


 PIANOFORTE SOLO 


 " | 7 " 2 


 . SCH . 


 WOL 


 LONDON 


 NOVELLO COMPANY 


 LIMITED 


 EDITION N 


 CLASSICS 


 383 


 WORKS 


 COMPOSERS 


 MUSIC 


 ROMANTICS , 


 MODERN 


 PIANOFORTE 


 SCHOOLS STUDIES 


 K IL . 


 K II 


 C1 ZERNY 


 DUVERNOY , J 


 COENE ! = 


 KRUG , 


 TAYLOR , FR : ANKI IN.—11 


 NOVELLO 


 EMINENT 


 6 | DEBUSSY 


 DEBUSSY 


 NEW FRENCH PUBLICATIONS 


 PIANOFORTE MUSIC 


 MUSIC 


 CHAMBER 


 DELUNE , L 


 TORGERSON , H.-S 


 HARMONIUM . 


 ORGAN 


 VOCAL MUSIC . 


 DUHAMEL , M. 


 HUE , ( 


 MOULLF 


 NET . 


 5 0 


 4 9 


 10 


 15 


 2 6 


 2 0 


 20 


 20 


 2 6 


 3 6 


 2 6 


 12 


 12 


 12 0 


 10 


 RANI 


 , I9I5 


 


 SKETCHES 


 MONTHS 


 PIANOFORTE 


 FREDE RIC . H. COWEN 


 BOOKS . 


 CONTENTS 


 ARRANGEMENTS SMALL 


 COMPANY 


 ORCHESTRA COMPOSER . 


 LIMITED 


 PRACTICAL GUIDE 


 


 THEORY MUSIC 


 INTAINING 


 NUMEROUS TEST QU ESTIONS ANSWERS 


 JULIA A. O'NEILL 


 MORNING POST . 


 MUSIC AL , OPINION . 


 LADY 


 TIMES . 


 OLD 


 ENGLISH VIOLIN MUSK 


 ALFRED MOFFAT 


 PREFATORY NOTE 


 SONATA 


 CHARLES MACKLEAN ? 


 TRIO - SONAT 


 PIEC 


 NESS , 


 STANLEY 2 


 COLLETT 2 


 ACKLEAN 2 


 MPHRIES 


 VINCENT 


 GLASGOW . 


 ADJUDICATOR CRITICISM . 


 1E SCHOOL , 1915 


 SCHOOL MUSIC 


 ILKLEY ( WHARFEDALE ) FESTIVAL . 


 PIANOFORTE SOLO ( OPEN 


 CONTRALTO SOLO . 


 TENOR SOLO . 


 MALE - VOICE CHOIRS . 


 FREE CHURCH MUSICIANS ’ UNION 


 NE , 19I5- 3 


 HOOL CHOIRS . 


 HOOLS 


 SCHOOL MUSICMiss Kathleen Cosens Mr. Joseph Mould 

 INSTRUMENTAL . 
 Pianoforte ( 18 entries ) . 
 Eighteen years age . 
 Rondo ‘ Sonata Pathétique ’ ( Beethoven ) . 
 * Standchen ’ ( Sinding 

 Miss Lilian Weatherseed 

 Mr. Laurie O’Beirne 

 String Quartet ( entries ) . 
 Allegro Andante ( Beethoven 

 Miss E. Bazett party 




 BRISTOL 


 DATES COMPETITIONS , 


 GIRLS 


 STOCKSBRIDGE ( NEAR 


 TREE 


 ING GIRLS 


 XUM 


 EE—7 


 WHOSO DWELLETH 


 DEFENCE HIGH 


 EVENING ANTHEM 


 COMPOSED 


 GEORGE C. MARTIN 


 


 . — — — — — 4 - ( — = 7 


 = 7 = 4 


 WHOSO DWELLETH 


 DEFENCE HIGH 


 SSS 


 OO , — — — — — — — — — — — 


 EN 


 WHOSO DWELLETH DEFENCE HIGH 


 SS SSS SS SS 


 SESS SSS SESS SSE 


 2 - 1 


 WHOSO DWELLETH DEFENCE HIGH . 


 — JS ® 


 


 EB 


 WHOSO DWELLETH DEFENCE UF HIGH 


 3 + SB — 2 — 


 - — * _ _ & _ * _ 


 TENOR TREBLE SOLO 


 WHUSO DWELLETH DEFENCE HIGH 


 EFENCE HIGH 


 WHOSO DWELLETH D 


 _ — = — F 


 = = SSE SS = = 


 « = 


 — — — — — SF 


 EY - _ 


 PESSESUESEESEES 


 WHOSO DWELLETH DEFENCE HIGH 


 SSS = ES LE 


 SSS LS 


 WHOSO DWELLETH DEFENCE HIGH . 


 PP 


 XN } 


 WHOSO DWELLETH DEFENCE HIGH 


 SS = = = : < = SSS 


 N RB N 


 10 


 SEEN 


 WEY " RH 


 _ — _ — — — _ 


 WHOSO DWELLETH DEFENCE HIGH 


 7 5 — PPP } = = = SSS — _ 


 2 — — _ — 7 F=- SSS = = 


 TH 


 PRINCE 


 RC 


 BIRMID 


 SUMMER * 


 MAN