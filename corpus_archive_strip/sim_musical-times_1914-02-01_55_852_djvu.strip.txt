


ROYAL


CHORAL SOCIETY. 


ROYAL ALBERT HALL


(ELGAR). 


MISS MURIEL FOSTER. 


MR. JOHN COATES. 


MR. MONTAGUE BORWELL


ASH W EDNESDAY, FEBRU ARY 25, AT 8


THE REDEMPTION 


MISS CARRIE TUBB. 


MISS MAY SANSOM. 


MISS PHYLLIS LETT. 


MR. LLOYD CHANDOS. 


MR. IVOR FOSTER. 


MR. GEORGE PARKER


BAND AND CHORUS, ONE THOUSAND


ROYAL 


YORK GATE


ACADEMY OF MUSIC. 


MARYLEBONE ROAD, N.W


LENT HAILF-TERM BEGINS MONDAY, 


ENTRANCE EXAMINATION, WEDNESDAY, 


FEBRUARY 23. 


FEBRUARY


THE ROYAL COLLEGE OF MUSIC, 


PRINCE CONSORT ROAD, SOUTH KENSINGTON, S.W. 


THE ROYAL COLLEGE OF ORGANISTS


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


FOUNDED IN 1844. 


PUBLISHED ON THE FIRST OF EVERY


FEBRUARY 1


DREAM OF GERONTIUS


MONTH


IO14


THE GUILDHALL SCHOOL OF MUSIC. 


JOHN CARPENTER ST., VICTORIA EMBANKMENT, E.C. 


WEEKLY ORCHESTRAL PRACTICES ARE CONDUCTED 


ROYAL


MANCHESTER COLLEGE OF MUSIC 


BIRMINGHAM & MIDLAND INSTITUTE


SESSION 1913-T1QT4


UNIVERSIT Y (OF 1 DURHAM


MANCHESTER SCHOOL OF MUSIC. 


96, WIMPOLE. STREET, W


SHORT COURSES FOR


SPECIAL TEACHERS. 


EDWARD H. COLE


THE


TIMES.—FEBRUARY 1, 1914


HALL


ME. AMINA GOODWIN. 


MORTIMER STREET, REGENT


LONDON, W 


A.R.C.M., 


IN‘ ORPORATE D GUILD OF 


MUSICIANS


L.G.C.M.), LICENTIATE 


C.M.) EXAMINATIONS 


ZETTE(Q


REGISTER OF ORGAN VAC 


CTORIA COLLEGE


LONDON. 


INCORPORATED


M I 


H. I 


COMPETITION


FOURTH YEAR


; :AIRSTOW, 


ARR’ EVANS


ANDREW’S HALL, 


NEW BOND STREET, W. 


LONDON COLLEGE OF MUSIC


GREAT MARLBOROUGH STREET, LONDON, W


EXAMINATIONS—LOCAL AND HIGHER. 


NATIONAL CONSERVATOIRE 


LEAMINGTON AND COUNTY 


MAY 7, 8, 9, 1914


_ OE 


INTERNATIONAL COMPETITION


THE TONIC SOL-FA ASSOCIATION


TH


IC. ROYAL


R.C.O


EXAMINERS


ERS


ACADEMY OF MUSIC. ROYAL ACADEMY OF MUSIC


IN HAR


IONY


IN VIOLIN


1 N


MUSICAL


PHE


70


PROFESSIONAL NOTICES


MISS ISABEL CLEAR (CONTRALTO 


MR. GEORGE LEYLAND (TENOR). 


MISS LYDIA JOHN (A.8.A.M


CONTRALTO) 


WILFRID ABOR


N.W


TENOR). 


VIR. FRANCIS GLYNN 


VIR. SAMUEL MASTERS


TENOR) 


FEBRUARY 1, 1914


MR. W. H. BREARE 


TO STUDENTS AND THE PROFESSION


VE 


MUS. 


THE LONDON COLLEGE FOR. CHORISTERS


WINIFRED MARWOOD — 


‘ELEMEN 


MATERIA 


M R. 


M ISS 


LR.A.M


F.R 


OACHID 


HUNDRE 


913, 


ECH 


40, \ 


HE 


HE 


SINGING 


IN


TREET, W


ISTERS


ICE GIVE) 


ITIONS 


RIK). 


RC) 


RK). 


\. RC.) 


COURS


THE


MUSK


AL ‘TIM


ISS H. HEALE COACHES 


EX: \M- 


DR. LEWIS’ TEXT-BOOKS


ISS F. HELENA MARKS 


HUNDRED AND TWENTY-SEVEN SUCCESSES; A.R.C.M., 


933, THREE HUNDRED AND THIRTY-FOUR SUC- 


R. H. SCOTT-BAKER, A.R.A.M., L.R.A.M., 


ECHSTEIN HAL L STU DIOS, 


40, WIGMORE STREET


HE REVISION OF MSS


M ISS MARGARET YOUNG, L. R.A.M.,A.R.C.M. 


| OLY TRINITY CHURCH, WANDSWORTH. 


( RGANIST AND CHOIRMASTER WANTED


\ ARYLEBONE PRESBYTERIAN CHU RCH, 


16


TEACHER


> XPERIENCED 


*TUDENT


THE MUSICA


TANTED


FEBRUARY 1, 1914


THE OLD FIRM. 


P. CONACHER & CO., LTD, 


SPRINGWOOD WORKS


HUDDERSFIELD. 


TWOG 3OL D MEDALS


ESTABLISHED 1750


GRAY & DAVISON, 


ORGAN BUILDERS, 


PRATT STREET, N.W. 


48, SOUTHFIELD ROAD, OXFORD


HOLT ROAD, LIVERPOOL. 


128


PIANO PEDALS


EVERY ~ ORGANIST


NORMAN & BEARD


PNEUMATIC PEDAL ATTACHMENT 


FOR THE PIANO


JUST PUBLISHED


ORGAN REC ,ETALS


RE VE RIE


AND


A PASTORAL


DOUGLAS


FOR


NET


ANCHESTER 


RDS 


[MENT


LONDON, W


LS


AMITED


THE MUSICAL


TIMES


FEBRUARY 1, IQI4. 79


MEMORY


BAND INSTRUMENT OUTFITTING 


AND REPAIR: BUSINESS


PROSPEROUS UNENCUMBERED


PIANO-PLAYER PATENTS


THE SENIOR OFFICIAL RECEIVER IN COMPANIES


LIQUIDATION, 


A FINE MODERN VIOLIN


SOLD BY W. E. HILL & SONS


AT THEIR PLACE OF BUSINESS, 


140, NEW BOND STREET


LONDON, W


EVE OF ST. AGNES 


CANTATA


AND ORCHESTRA


THE 


FOR SOLI, CHORUS


JOHN KEATS. 


FRANCIS BARNETT


JOHN 


SONATA IN A


FOR VIOLIN AND PIANOFORTE 


BY 


G. F. HANDEL. 


C. EGERTON LOWE


THE AL


MUSIC


SIC


A PRACTICAL GUIDE


FEBRUARY 1, IQ14


BROADWOOD 


PLAYER-PIANOS


GRANDS AND UPRIGHTS


ILLUSTRATED CATALOGUE ON APPLICATION


RAPID PIANOFORTE TUTOR


LASSEN’S


IMES N NOVELLO'’S MUSIC PRIMERS, 1 > 


OS


AND SINGING-CLASS CIRCULAR


OK


OKS


SONG


82 THE MUSICAL TIMES.—FEBRUARY 1, 1914


THE SCHOOL CONCERT CHOIR


SIGHT-SINGING ANI) FAR-TRAINING


1910


RUGBY SCHOOL


CONCERTS


PROGRAMMEPART I

Overture... * Egmont’ ... Beethoven | 
Song owe * Had a horse ” Korba | 
kK. E. Bonnerjee

Pianoforte, Finale of ‘ Etudes Symphoniques* Schumann




PART IL. 


PART II. 


PART I


INDIVIDUAL INSTRUCTION. 


I, 1545


72. 1 


THE MUSICAL TIM 


HIGHER INSTRUCTION IN MUSIC. 


ORCHESTRATION, THE EROICA.’ 1910What is the compass of the alto pos., the fag., the 
fl., and the va

In what respects does the orchestration of Beethoven 
ditfer from that of Wagner

What is remarkable about bars 6 and 7 on p. 46




HARMONY AND FORM. = 1908Write a single chant in F minor or 
point, 3rd species (crotchets), above the C.F. 
board

In Beethoven’s Sonata No. 8, what keys are passed 
through in the introduction

Explain the following chords in this passage: 
(i.) Bar 1 on third crotchet ; (ii.) bar 2 on third 
crotchet ; (iii.) bar 6 on third crotchet ; (iv.) bar 10 
first bass chord

What were the principal changes in or additions to

the form of a Sonata introduced by Beethoven

1s




ES.—FEBRUARY 1, 1914


IMPOSED


(CHOSEN


(UNISON SINGING


IN


MUSIC PREPARATORY SCHOOLS


S6


PIANOFORTE MUSIC


MUSK PIANOFORTE


THE SINGING-CLASS


WADE’S


GENERAI


BRIDGE


THE MUSICAL


59


RUST CASE: ENDING 


ND 


CALVOCORESSI


VEVSUS


THE MUSICAL ‘TIMvel have

to Beethoven and how far internal evidence 
alone would have enabled us to discern in it, even 
in a feeble measure, the lion’s mark Now, 
perhaps, looking backwards, it might have done 
so: exactly as we are able to see to-day that a 
musician of fiftv or even twenty years ago was a

precursor

ES.—FEBRUARY 1, 1914

g —= ee 
| sections, which is carried to twice its origin 
length by spurious additions which fail to shod 
even men whoare not unwarrantably acknowledge 
as experts in musical matters, then the fundamer 
of our methods of judging form is very untrus 
worthy. We sneer at the ineptitude of thos 
conductors of yore who thought it permissible 
certain of Beethoven

borrowed from othe

replac e sections ol 
sections 
symphonies of his ; and yet, if ever a docume 
came forth conclusively to that what ha 
hitherto passed as the Scherso of one symphony 
in fact the Scherse of another symphony, nothir: 
would remain but to bow to the inevitable ap 
recast, in the Beethoven literature, all that refer 
to the wonderful fitness of that Scherzo as par

the whole to which it was thought to belong. 
* Styl




1S


50 


ARunder: 
lorio 
hich 
robal 
Wi 
n

i 
its Origing 
Li] to shod 
knowledge 
> fundamer 
ery untrus 
le of thos 
rmissible ¢ 
Beethoven 
from othe 
a documer 
it what ha 
symphony 
my, nothin 
vitable ar 
| that refer 
vas par.) 
elong. 
le. 
um of tf 
make up 
We 
nature as 
ypletree an 
nsistency 5

Style




TIM


THE MUSICALand a ready answer to, the supercilious judgments 
ysually passed on his works by professional critics. 
Would a majority of experts fall into the same 
trap ? I could name at least one professional 
ritic who did not ‘strike upon the authorship 
of M. Ravel’s ‘ Valses nobles et 
produced for the first time on that occasion

There are excellent musicians who aver that the 
famous chord in Beethoven’s Choral Symphony, 
that consists of the seven notes of the diatonic 
gale, is the outcome of a mere misprint. Having 
mce alluded to that chord with to a

sentimentales




THE REFORM OF CHURCH MUSIC


A REVIEW OF THE MWORN/JNG POST


CORRESPONDENCE


ES.—FEBRUARY I


IQI4


2 THE


MUSICAL


FEBRUARY 1


MUSICAL


TIMES


FEBRUARY I, IQ14


__


THE MUSICAL ‘TIMality chamber-music of the highest type by the best

tists. The range of the orchestral concerts we have 
referred to is from Beethoven, brahms, and Wagne1 
jown to, say, Heérold’s ‘Zampa’ and the like, with a 
stinct leaning towards the better end of it rhe full 
realisation of the extent and nature of all this musical

tivity gave a decided fillip to our patriotic optimism




1 1914


THE MUSIC OF THE ANCIENTS


RESUME OF FOUR LECTURES DELIVERED AT THI


BRITISH MUSEUM. 


BY KATHLEEN SCHLESINGE


THE MUSICAL TIMES. 


THE COMPLEAT ORGANIST. 


THE MUSICAL


FEBRUARY 1, 1914


VASSAL


1S


30, 01 


MUSICAL


SO


TIMES


FEBRUARY 1


IQT4


AL


DR. ALCOCN’S NEW BOOK ON ORGAN 


TECHNIQUE


THE


MUSICAL


ES.—FEBRUARY 1, 1914. IO


THE MUSICAL TIM


ES


FEBRUARY I, IQT4


___


APPOINTMENTS, 


HORAT


MUSIC


_ O 


THE MUSICAL


ANTILEMS. 


TIMES


FEBRUARY 1


103


BOOKS RECEIVED


HARGITI S—=3-9-4 


CHORUS 


NDEN


CHORUS


SOLO ——$$—— 


RIGHTEOUSNESS


GATES OF


=} 6-3 ———— ——— 2 —— —2 | 


=== 2S >= 25 SSS ES ES 


OPEN ME THE GATES OF RIGHTEOUSNESS


Z I 


62> 2 2S 5 = 2. 2


— ——- SS 


OPEN ME THE GATES OF RIGHTEOUSNESS


1914


OPEN ME THE GATES OF RIGHTEOUSNESS


=F —_ — 4 = | =H 


. ~ + SS 


175 QUARTER 


=> 5 ‘N ‘N 7 = | - 1 


_ ; = _ - — - 


ME THE GATES


OF


RIGHTEOUSNESS


19


THE MUSICAL TIM_—_- 
: p 
Obituary. 
It is with deep regret that we record the death of 
J - RaouL PuGNo, the famous pianist, which occurred at 
= \oscow on January 3. M. Pugno was a familiar figure on 
London concert-platform and a great favourite with 
sh l The deli AC} t, and il the

ntinents. irs ago he and M. Ysaye gave 
emorable Beethoven Sonata recitals a 
ween’s Hall. 10 was born at Monrouge, France, 
d June 2}, > Iie became pupil ut the Paris 
- nservatoire, Where he won the first pianoforte prize in 1866. 
\ er, In 1806 he returned to the Conservatoire as professor of tl 
forte Hlis compositions include an Oratorio, 
" resurrection de Lazare,’ four operettas, several ballets

1€ planoforte




LICK


INCORPORATED SOCIETY OF MUSICIANS


ANNUAL CONFERENCE


I *F


S ' R


I W 


T T 


L TIMES. 


= _ ™ ——————_——_} 


THE MUSICAL TIM


ES.—FEBRUARY I, 1914. 11


HESTRAL CONCERT. 


S 1 ( 


WHAT ATTITUDE SHOT D TEACHERS ADOPT TOWARDS


MODERN MUSIK


HALIFAN MA


VACATION CONFERENCE ON MUSIC: 


EDUCATION


DRIGAL §S


NVIC 


USICAL


J > T 


MUSICAL


WHEELER. 


NOVELLO AND ©O


BLACKPOOL. 


RRESPON DENT


GUILDHALL SCHOOL OI


HE PHI AKRMONIC SO


I M I


QUEEN'S HALL ORCHESTRA


CHAMBER CONCFRTS 


VI


THE TIM


MUSICAL


RECITALS


(BY OUR OWN CORRESPONDENTS


BIRMINGHAM


ES.—FEBRUARY 1, 1914


119


BOURNEMOUTHFestival. Mr. Dan Godfrey had evidently bestowed an 
immense amount of care upon its preparation, and the 
orchestral playing was splendid ; but although the audience

applauded the performance in generous measure, it is not 
likely that the composition will benefit by many performances 
country. We have enjoyed performances of such 
works as Dvorak’s stirring ‘ Husitska’ Overture ; 
melodious ‘Irish’ Symphony; Glazounov’s 
‘Stenka Razine,’ of the Russian 
| musician’s best works ; Beethoven’s C minor Symphony ; the 
Overture to ‘Die Meistersinger’ ; and the Symphony of 
Franck. The soloists have been Miss Tosta di Benici 
(pianoforte), Miss Leila Doubleday (violin), Mr. Arnold 
Trowell (violoncello), and Miss Marjorie Dorning (violin). 
Eclecticism has been the keynote of the Monday 7 Pops,’ 
reference to the chief details of the concerts will show 
‘International’ Programme—Overture, 
‘Le Carneval Romain,’ by Berlioz; * Bavarian Dances,’ 
by Elgar; Overture *Leonore,’ No. 3, by Beethoven ; 
Suite No. 1, ‘Peer Gynt,’ by Grieg; ‘Scenes Caucasiennes,’ 
Ippolitov-Ivanov. December 22: ‘Christmastide ’ 
| Programme—Overture, ‘The Cricket on the Hearth,’ by 
| A. C. Mackenzie; ‘ Shepherd Fennel’s Dance,’ by Balfour 
Gardiner ; Tone-poem, * Christmas Eve,’ by Southey Frost, 
a local Trio for two flutes and harp, from 
* L’Enfance du Christ,” by Berlioz, capitally played by Messrs. 
Jean and Pierre Gennin and Miss Jacoba Wolters, members of 
the Orchestra ; Variations on the Austrian Hymn, by Haydn ; 
Meditation ‘ Lux Christi,’ by Elgar; Suite, ‘ Where the 
| rainbow ends,’ by Roger ()uilter, first performance at these 
concerts ; and the two the ‘Ave Maria’ of 
| Max Bruch and Bach-Gounod former for the first 
|} time at these concerts—well sung Miss Nora Read, 
Bournemouth’s popular — soprano. December 29: 
* Tchaikovsky ” Overture, ‘Romeo and

in this 
pleasing 
| Stanford’s




15


BRISTOL


BRI


THE


LE


MUSICAL


LIVERPOOL AND DISTRICT 


FEBRUARY 1, 1914. 121


MANCHESTER AND DISTRICT


I ’ S


FEBRUARY I, IQ14


YORKSHIRE


1 N 2


THE MUSICAL ‘TIM


BRIEFLY SUMMARIZED. 


ES.—FEBRUARY 1, 1914


AR VANCOUVER (B.C.).—A notable event in the musical life 
lof this town was the annual performance of Handel’s 
‘The Messiah,’ given on December 30 at First Presbyterian 
Church by the Vancouver Musical Society. Mr. George P. 
| Hicks conducted an interpretation that earned the highest 
praise. The solo work was undertaken by Mrs. Chandler 
Sloan, Mrs. Gideon Hicks, Mr. Karl Johnston, and Mr. 
| Hamilton Earle. Choir and orchestra numbered two 
hundred performers

WokING.—‘ Beethoven House’ was inaugurated recently

with a miscellaneous concert organized by Messrs. Clark, of 
Guildford. The artists were Miss Frances Crook (vocalist), 
| Miss Dorothy Vincent (pianist), and Mr. Harold Montague




I I SICAL ‘TIMES.—Frervuary 1, 1914

PARIS he had introduced on a similar occasion exactly twelve yes 
r he Grand- | ago. Ile also played works by Moussorgsky, 
g 1 acting d rve warm | Balakirev’s ‘Islamey.” At the Société Ind 1 
the st Wagnerian | January 14, he gave for the first time M ‘T 
le-par rv well tournes en tous sens,’ a set of three small pig c 
t i Mil Br S nar r Hat 
sot 1 M. Lestelly as s rt of the Societe Indé nd 
| ver-Maic wel r riginal anc autiful sets sones 
g 
M. Andre sky I Japar lyrics, y M elage, 
yrics, and by M. Ravel, on p ems \ r 
' \\ r’s last | were Mile. Rose Féart, Madame | T 
se ¢ I rn f me Nikitir k 
DI t t ne Nikitina repeated M. Stravinsky's si 
, t letter of less suc sat Ecole des Hautes Etudes S les 
' uld | the sar ( sion M. Léo Ornstein made his first I 
On sidering ; wish | 2ppearance t Paris as pianist and composer, pla 
! . sw rom pte M. S I rg” pieces, Opp. Il and 19, und his 
t that | ‘ Impress s de Notre-Dame,’ which were receiv , 
" ler ling | fa t 4 
t dit M. Kk t eV g a series of recitals to wh! 
rsif c rformed | Bach's \\ perirtes Klavier,’ the last ten Sona 
kur Beet! ind Works rr 
! wher \ M. Ferru Lb s gi the & et 
at Fr Erar s pl " Concert . 
\ ron d_ that | Conservatoi Dr. Saint-Sa 
rt os, Wi S r | ( l Ss re e Le 
' . it ly | of Honouw 
' S ' Madan I) pop Defoss is given remark 
rt, t n if | good recit ¢ Borodin, Balakire Toussorgal Three 
sam t Rimsky-Korsakov, and Rachmaninoy Ip. 
m or ) = rt Kuss ra and ballet season will t r Tno 
f certain 1 ssary | at tl Grand-Opera. It will include the pr tion prod 
re eas el than | M. Igor Stravinsky’s ‘ The Nightingale,’ and Dr. Ric with 
Strauss's * Joseph.’ S 
rgotten that among The monthly concert of the Schola Cantorum was dev 
rong I n against | t lental music,’ the numbers being Beet! 
Ilea t firs r widely | ‘I mit,’ M. J. G *s *Peécheurs d'Islan 
sand M. Claude D 6 Biz l’ Arlésienne, ndy’s * Médce Sinfor 
sly with t¢ 7 A pi rte of l ew kind will shortly ag xo | 
l tenets the Germa n the Paris market t we are told ibrate aved 
" \ several writers, | the infl t of hammers, but of electric cu $ tl 
f * Parsifal,’ speak of it | tone said to be wonderfully pure and full. M 
mit nd certainly, I 
ris ! en vear 4 mir 
rer 1 ; st 
, ity ‘ 4 r T 
ha yee Forcign Wotes. = 
s at the rra f Hochsi 
thir d Kundry’s 
ADEN-I EN 
¢ n M. Manuel de The mai iture the third Symphony Concer Darit 
t I * Frances Kapellmeister Hein was the first performance er May, t 
ter wit ! Edward Elgar’s * Enigma Variations.’ At the san ncjeand * | 
mpri Mesdames | Herr Fran! rave brilliant int rpretation of A roduce 
Messr brar ll nd | Vjoloncell I rto. Beethoven’s C minor | 
k » Mile. Vix, mcerto (with Carl Friedberg as soloist) ar Brahog 
yi k m ' nductec C minor Symp! yn ymmposed at Baden) were give! ; Chris 
odern Ft music at | third Subscription Concert 
~ pe as com 
N I Athos

Pin , I os , ® ( rt-G Ils haft was “ oh 
() tranger works IX rd Str S 1) , ‘Ein Heldenl tl 
I t ands t irles} (for notorte d r¢ 
, Tl lists were vere ( ducted by e composer, who was cree 1 
stically lr} Volkschor-Orchestra, under Inder Ata ] 
M. Delvincourt’s | baton, gar excellent performance of Max Reggie prog 
mber { I s | put * Romar T Suite, ne t th best works i ] 
I spr 1 i I be Ss String quart 2) 
\ her | Scheu gs admirable Stri juartet, Op 
ury If, nsisted of two | finest in modern quartet-literature), were recent 
n ; M. J. Guy|the barmen String ()uartet. rh ncert 
Cr T ( rts- | Meiningen Court-Orchestra under Max Rege 
ul ry IS, Mahler’s | sensation. The features of the programme we 
5+ an interesting Festival | third Sympl y and Reger’s ‘ Variations and lugue Redem, 
merry theme 5 \ Hiller.’ R Strauss’ * Fest mm ociety ‘ 
February 2 cert of | Preludiun and * Heldenleben, and Saint-Sal 4

i¢rmann

THE MUSICAL TIMES.—Feprvary 1, 1914. 125 
i incacicecnsancuiiacteummipsenccnetasitamanhomeamtnte ati : 
BASEL. | MUNICH, 
e The new Christmas Oratorio, ‘ Weissagung und Erfiillung’ It will cause wide satisfaction to hear that the first 
|. Erik Sate (‘The oracle is fulfilled’), for soli, mixed choir, children’s | complete critical edition of Mozart’s letters and those of his 
' pi and orchestra, by the national composer, | family, by Ludwig Schiedermair, music historian of the 
has been produced by the Basler-Gesangverein | Bonn University, has just been published. Vol. i. and 
Indéper Suter. The work was a great success, and | vol. ii, contain Mozart’s letters ; vol. iii, and vol. iv. contain 
S songs C d performers were much ng lated. the letters of his family ; vol. v. a Mozart-ic mography. 
I. Delage Bruckner’s Mass in F minor made a deep impression 
y Mallar BERLIN, when recently performed here under Herr Rohr’s 
} 1- Bat he performan e of * Parsifal’ at the he Opera conductors} Ip. At the Musikalische Akademie, Mahler’s 
k place on January 5. A new instrument, a Tonreines- | third Symphony was executed under Bruno Walter, the 
ys songs Kontra-Glockengeliut, invented by Kapellmeister Gorter | composer’s favourite pupil. M. Schwickerath conducted 
S , nd constructed by Max Enders, of Mayence, is used at | the performance of Berlioz’s ‘ Faust’ given by the Conzert 
his first pull these performance S. The song recital given by Frau | g sellschaft Symphonies by Schumann, Beethoven, and 
poser, pla Hemine d’Albert at the Bechstein Hall was one of Bruckner, Pianoforte concertos by Mozart and Brahms, and 
and his nsiderable interest. The chief attractions were Wolf's | two novelties—‘ Riccio,’ a Prologue for orchestra by 
received ¥ Der Genesene ar die Hoffnung’ and Hause g rer’s settings Landsbherger, and a ‘Serenade’ for small orchestra by 
FE. Meyer's beautiful poems, ‘Lenz Wanderer,’ ‘ Len Walter Braunfels—were heard at the Concertverein under 
) Wik,” and ‘Lenz Triumphator.” Hausegger played his Ferdinand Loewe. 
ten Sonata m accompaniments. Symphonies by Brahms (No. 2), NICE, 
— o } . ty ’ y y 
ls the § age an on pee Seances nohecenoke The Gest ayepneny concert was given under Camille 
a one a bs eel Kittel cael Rie tees odd ths Chevillard, with Madame Jane Morti ras solo St. Works 
to aaa Orchestra gave a first hearing Wolf's oy ' — Franck (the Syn phony), Paul Dukas, Debussy, 
: ~ Bag te - “ e 1) Indy, and Fauré, and three scenes from Berlioz’s ‘ Rome« 
f the Le hristnacl ind Bruckner’s 150th Psalm. f the = : 
$ = aan gees ae ; * Oks _, | et Juliette’ were on the programme. 
Popular concerts an interesting performance of Sa Saens s 
remarkif Prelude to ‘Le Deluge’ (for strings only) was given. RAGUE, 
loussorgsigl Three new works by Pt vel urwenka 4 Sonata for viola On January 1 ‘ Parsifal” was performed at the 
- (Up. 100), a Muet for — —_ viola (Op. 105), and 8] rentsche Theater, and at the Czech National ” 
wilh ( t ae pe ae), VOOR s wie’ ain iiss ae ccess! lly Hans Winkelmann, a son of Hlermann Winkelmann (tl 
re Huction #55 prod Seas aa Harmoniumsaal Ibsen's * Peer Gynt first Parsifal at Bayreuth), took the leading role in the 
a i. ith Edvard Grieg’s music, is to be given at_ the first-mentioned productior t 
Schauspielhaus. Madame Nina Grieg, the widow of the] - ich. = ; 
im was dev clebrated N rwegian composer, will be present at the first PRASSBUR 
g Beethor eemANCe Max Reger’s Pa ariations and Fugue on a \ considerable success was obtained y Herman 
urs d'Tslat my theme by A. Hill r, was the feature of the fourth | 7ijcher’s ‘Liebesmesse’ (a secular oratorio in three parts 
Sinfonie-Abend of the Court Orchestra. Herr Reger was | produced here on December 10, under Hans Pfitzner. 
shortly aj so heard. in Bach’s fifth ‘ Brandenburg’ Concerto. Tle 
ibrate un ayed the pianoforte part at a uniform eave ss7 The 
currents. ] tin Mozart-Community announced for January 18 and 19 
Mozart-Festival, with Lilli Lehmann, Max Fiedler, and , 
r Schnabel as soloists. The programme includes the Miscellaneous. 
minor Mass, under Fritz Riickward. The profits of thi Ae 
stival will go to the Sal rtheum. \ 
appreciated performance by the Jaques-Dalcroze The following awards are announced by the Royal 
f pupils was given at the concert-hall tl KOnigliche- | Academy of Musi Tl Battison Haynes Prize (com 
Hochschule fiir Musik. position) to Gilbert Bolton ; the Hine Prize (compositior 
>; the R.A.M. Club Prize (composition) t 
Cate PIAN I the Rutson Memorial Prizes (for contraltos) 
Concer During the National Festival, which will be held here in | '® Janie Biake ; (for basses) to Leonard F. Hu bard 5 tt 
ce here of} May, two new Norwegian national operas, ‘ Frithlingsnacht ’ Potter Exhibition (pianoforte) to Arthur Brian Nash ; 
e same compan ‘Ein Feiertag,’ both by Gerhard Schjelderup, will be | t Westmorland Scholarship (singing) to Gwenetl 
. of WA roduced at the National Theatre. Roberts; the Sainton-Dolby Prize (singing) to Eleanor 
nor | Evans ; the Philip L. Agnew Prize (pianoforte) to Phily 
. Brahnf ! LU. | A. Levi; the Broughton Packer Bath Scholarship (violon 
re gi ry a a a Sorwecian composer, | Clo) to Giovanni Battista Barbirolli, of London. The 
Sending, Ux —— oe PNPOSCT; | following forthcoming examinations are also announced 
‘scompleted his first opera, * The holy Mountain * (Mount The Thal erg Schol " », for female British-born pianist 
Athos), the libretto of which is by Dora Dunker. The}, er ¥ tie ae 
. , : ae between the ages of fourteen and twenty-one, April 3 
ft w s di " “ The : SS. ere, under Frar Mik rey, at the and May 2. Che Pare Dp Rosa S« hol hin. for female 
1 Hleldenle s — British-born vocalists between eighteen nd twenty-one 
nd rc , RENCE, vears of age, May 2. The Ada Lewis Scholarships, 
creeted ef British-born violoncellists not over twenty-two years of . 
jer Inderm#{} Ata Pianoforte recital recently given by Mr. Leigh Henry, | April 30. The Sterne Bennett Scholarship, for British 
Max Regge programme censisted of works by Cyril Scott, T. Varteg | born male musicians between fourteen and twenty-one years 
s of the ea] Ernest Bryson, Leigh Henry, and well-known | of age, April 30 and May 2. 
4} 127 il composers of the modern school. | The following awards have been made at the Royal 
wrthue : \VENHAGI Cellege of Music : Council Exhibitions, singing : Myfanwy

Crawshay Dorothy C. Giles, £9; Clara M. Simon




THE MUSICAL


FEBRUARY I, IQT4


CONTENTS. 


M.-D


CHOC


OSTER


THE MUSI


DURING THE LAST MO


MYLES B


I ISTER, 


H™: A. 


CHINSON, VIE


WESAY, F. H. E


7G


HE NSTROM, WILIITELMINE


200


A SI


CAI


ON TH. 


LIMITED


AS. LINLEY


TIMES


N EWTON, I


FEBRUARY I, I9QT4. 127


NG THE I


DURI AST MONTH


A B 


PHEN, 


VO. 1294


CLOW


2157


REDUCED


2159


PRICES. 


FOR


NEW YORK


PUBLISHED


THE H. GRAY CO


ECKSCHER, C. D


BEAUMONT. 


| M I M 


| ( | M.A. (S ! 


} N M ( I 


I I \ HOL} BM 


EXTRA CHAI I RR THIS ENLAI ED ISSUE, 


‘THE MUSIC STUDENT,” LIMITED, 


1) ) I I W 


SPECIAL NOTICE TO STUDENTS. 


ORIGINAL COMPOSITIONS 


FOR THE 


ORGAN 


ALFRED HOLLINS


THE MUSICAL TIMES


SCALE OF TERMS FOR ADVERTISEMENTS. 


I I 


SPECIAL NOTICE. 


FRIDAY


FEBRUARS (FIRST


THREE-PART


STUDIES


SCHOOLS AND LADIES’ CHOIRS


HUGH BLAIR


WITH PREFACE AND DIRECTIONS FOR


JAMES BATES


NOVELLO'S 


ANTHEMS


I 2 J. HLM 


HYMNS AND TUNES. 


I I I 


I I DE 


N ‘ J}. A 


S S ] E. W 


M } 1. \ R 


, W T.A 


I H. J.G 


TICE


CA


LONDON


NOVELLO AND COMPANY


LIMITED


THE STORY OF THE CROSS ANTHEMS IN THE 


THE WORDS BY 


13 THE


MUSICAL


TIMES


FEBRUARY I, IQI4


NOVELLO'S ANTHEMS FOR LENT


A.W 


LONDON


K ( S 


I W


14 


14 


NOVELLO AND COMPANY


SAS


J. V.R 


LIMITED


THE


MUSICAL


TIMES.—FEBRUARY 1, 1914. 131


CANTATAS FOR LENT


THE STORY OF CALVARY


FOR TENOR AND BASS SOLI AND CHORUS


ROSE DAFFORNE BETJEMANN THOMAS ADAMS. 


IN THE DESERT AND IN THE VIA DOLOROSA


GARDEN A DEVOTION


FOR BARITONE SOLO AND CHORUS


AND CHORUS THE WORDS DERIVED MAINLY FROM ANCIENT SOURCES


1E WORDS WRITTEN AND SELECTED BY THE MUSIC COMPOSED BY


VIOLET CRAIGIE HALKETT E. CUTHBERT NUNN


. THE MUSIC COMPOSED BY —_ 


TH E CRUCI FI XION TENOR AND BARITONE SOLI AND CHORUS 


A MEDITATION THE CONGREGATION 


ON THE 


THE WORDS SELECTED AND WRITTEN BY 


2DS SELECTED AND WRIT BY 


SET TO MUSIC BY J. H. MAUNDER. 


THE DARKEST HOUR (STABAT MATER


FOR SOLI, CHORUS AND ORCHESTRA


SOPRANO, TENOR, AND BARITONE SOLI COMPOSED BY , 


AND CHORUS - ANPTAN , ” 


THE WORDS SELECTED, AND THE MUSIC COMPOSED, BY THE ENGLISH ADAPTATION BY ‘FRED. J. W. CROWE, 


THE CROSS OF CHRIST WRITTEN AND COMPILED BY


JOSE PH BENNETT


WORDS SELECTE FROM THE HOLY SCRIPTURES, INTERSPERSED 


WITH API ‘PRIATE HYMNS, BY THE MUSIC COMPOSED BY 


MAURICE ADAM: C. LEE WILLIAMS 


JOSEPH BENNETT 


1 C. LEE WILLIAMS


NOVELLO AND COMPANY


LONDON: LIMITED


NEW EDITION. NOW READY


PENITENCE 


PEACE 


J. H. MAUNDER


MUSIC FOR LENT AND EASTER


ORGAN MUSIC


ANTHEM ‘S THE RAIN COMETH DOWN. 


SUITABLE FOR LENT


WATCH YE, PRAY YE


WACHET. BETET) 


\ CANTATA 


FOR SOLI, CHORUS, AND ORCHESTRA


J. S. BACH. 


THE PASSION


GETHEI


HYMNS TO BE SUNG BY THE CHOIR AND


CONGREGATION


J. VARLEY ROBERTS


THE VILLAGE ORGANIST


MUSIC FOR LENT AND HOLY WEEK


JOHN E. WEST


MUSIC FOR EASTER


I ED BY 


JOHN E. WEST


I G. 3 


BENEDICITE 


IN A FLAT


4 4 


THE PASCHAL VICTOR) EASTER HYMN 


| FOR SOPRANO AND TENOR SOLI, CHORUS, AND 


THE MUSIC COMPOSED BY E MN L LU S 


| SEBASTIAN MATTHEWS. EMMAUS 


. : S, KC. 


THE


MUSICAL


134 TIM


ES.—FEBRUARY I, 1914


WORLD-WIDE POPULARITY. FAVOURITES


COMPOSED BY CALEB SIMPER


EVERYWHERE. 


COMPOSITIONS BY


ALFRED R. GAUL


BARD OF AVON


SHAKESPEAREAN 


THE


PASSION SERVICE. 


\ I I I W I 


POPULAR EASTER ANTHEMS 


_DW


N A. CLARE


OMPOSED BY


N ( I


TWO-PART SONGS


FOR TREBLE VOICES 


I 4. H. LINTON 


I I 


NEW HUMOROUS PART-SONG FOR S.A.T.B. 


DEEP. 


\ ES


F ALF


THE BIRDS. 


THE


EASTER ANTHEMS


JUST PUBLISHED. 


ASTE


ADDITIONAL HYMNS


THE MUSICA


NOW


ISSUE


AS A NET BOOK


ARY TI, IQT4


WITH TUNES


FO


HYMNS ANCIENT AND


OR ANY OTHE


R USE WITH


R CHURCH HYMNAL


LONDON


NOVELLO AND COMPANY


LIMITED


NEW NUMBER


CON


LONDON : NOVELL


TENTS


10


AND COMPANY, LIMITED


MODERN


GENERAL USE


SS


ISSUED AS NET BOOKS


SUITABLE FOR PRESENTS


THE CATHEDRAL PSALTER 


WITH PROPER PSALMS 


POINTED FOR CHANTING, WORDS ONLY


MINIATURE MUSIC EDITION OF HYMNS ANCIENT AND MODERN


THE


CATHEDRAL PRAYER BOOK


WITH CANTICLES AND PSALTER POINTED FOR CHANTING 


MINIATURE MUSIC EDITION OF HYMNS ANCIENT AND MODERN 


(1889 EDITION). 


NOW ISSUED AS A NET BOOK


THE NEW CATHEDRAL PSALTER


THE PSALMS OF DAVID


NOGETHER WITH THE CANTICLES AND PROPER PSALMS


EDITED AND POINTED FOR CHANTING BY


WORDS ONLY. 


PSALTER AND CHANTS COMBINED. 


PRODUCED AT


FALSTAFF 


SYMPHONIC-STUDY 


FULL ORCHESTRA


FOR


EDWARD ELGAR 


MINIATI RE SCORE


FULL SCORE AND ORCHESTRAL PARTS ON HIRE


THE TIMES


THE DAILY NEWS


NOVELLO


AND COMPANY


THE DAILY CHRONICLE. 


EVENING NEWS 


PRODUCED AT THE LEEDS MUSICA] ESTIVA


MYSTIC TRUMPETER


WHITMAN


WALT 


BARITONE SOLO. CHORUS, AND OR


FOR HEST


HAMILTON HARTY


THE DAILY NEWS. 


THE PALL MALL GAZETTF. 


THE YORKSHIRE POST. 


RAINBOW 


OUR MUSIC READING LADDER FOR BEGINNER


RICA


LOSH


__


NOTE 


PREF. 


THE | 


RECT


ESTIVA


TER


JUST PUBLISHED


SONGS FROM THE PLAYS OF


WILLIAM SHAKESPEARE


WITH DANCES AND INCIDENTAL MUSIC


AS SUNG AND DANCED BY 


THE BERMONDSEY GUILD OF PLAY


WRITTEN AND COMPILED BY


MRS. G. T. KIMMINS


CONTENTS


PTHE MORNING POST. 


THE QUEEN, THE BRISTOL TIMES AND MIRROR. 


THE LOOKER-ON, 


NOVELLO AND COMPANY, LIMITED


LONDON


PRODUCED AT THE LEEDS MUSICAL FESTIVAL. PRODUCED AT THE LEEDS MUSICA] ESTIVA


SYMPHONIC-STUDY 


RLUDES IN A MINOR POEM


FOR FULL ORCHESTRA. . } 


Y WALT WHITMAN. \\ 


MINIATURE SCORE


FULL SCORE AND ORCHESTRAL PARTS ON HIRE


THE DAILY NEWS. > 


THE YORKSHIRE POST. 


IGI4


139


JUST PUBLISHED


SONGS FROM THE PLAYS OF 


WILLIAM SHAKESPEARE


WITH DANCES AND INCIDENTAL MUSIC


BERMONDSEY GUILD OF


WRITTEN


MRS. G


THE PLAY


ND COMPILED BY


T. KIMMINS


CONTENTS


THE STEPS


SONGS, DANCES, DESCRIPTIONS, AND 


ILLUSTRATIONS


THE STAR 


THE MORNING POST. 


THE QUEEN. THE BRISTOL TIMES AND MIRROR, 


THE LOOKER-ON, 


NOVELLO'’S


PRICE TWO SHILLINGS EACH NET


THE GLORY OF THE MORN. 


JOHNSON


COUNTY PALATINE. 


T8


PARS! IN BROWN


ONDON TOWN 


VW ITHIN YOUR EYES. 


THE PATHWAY THRO’ THE POPPIES


THE BOLD GENDARME


ENTLEMAN JOHN


GONG OF THE FUGITIVES. 


OVE IN THE MEADOWS. 


ALL MY HEART. 


WITH


NEW SONGS


KINGSTON-STEWART


THOMSON


THOMSON


THE


DRAMATIC SONGS} | 


IMAGINATION. ) 


UNWELCOME 


NEW AND POPULAR SOMR] © 


SACRED: G. I 


“] WONDER. | 


‘THE PLANTER. 


“IF I SHOULD TELL YOU. 


“MY KINGDOM. 


“THE SPIRIT OF THE STORM. ; 


“A SONG OF HOPE. CA 


“THE LITTLE DUTCH TILE. 


“AMONG THE ROSES. = 


FOUR SONGS 


RY 


TENNYSON. ; 


A. C. MACKENZIE. 


| P ED, 


FOR BARITONE . 


CHARLES DAWSON SHANLY) 


A. C. MACKENZIE 1 


SONGS


AM


SO


ION


THE MUSICAL


TIMES


CAMBRIDGE 


UNIVERSITY PRESS


FEBRUARY 1


IQ14


THE ORGAN


WALTER G


M.V.O


ALCOCK


CAMBRIDGE UNIVERSITY PRESS THE SCOTSMAN, 


B RA H M : THE WESTERN MORNING NEWS. 


BRUCH. 


IMPORTANT NEW WORK 


ORIGINAL COMPOSITIONS | PROGRESSIVE STUDIE


THE


ORGAN


MUSICAL


[WELVE PIECES


JOHN STAINER 


IN TWO BOOKS 


\ 6 N 


II 


\ I I 


P I 


ORIGINAL COMPOSITIONS 


ORGAN


BASIL HARWOO


I, 1914


FOR


THE VOICE


G. HENSCHEL 


IN


TWO PARTS


MUSICAL DICTIO


AND COMPANY


AN ORTHOLOGIC METHOD FOR ACQUIRING 4 


PERFECT PRONUNCIATION IN THE SPEAKIN( 


AND ESPECIALLY IN THE SINGING OF THE 


FRENCH LANGUAGE. 


FOR THE SPECIAL USE OF ENGLISH-SPEAKING PEO! 


BY 


C. THURWANGER 


A VALUABLE BOOK FOR TEACHERS AND STUDENT


AND


EXPRESSION 


PIANOFORTE PLAYING 


FRANKLIN TAYLOR


EXTRACT FROM PREFACE


NEW I


NOVELLO & COMPANY


TO


JIRING 4 


PEAKING 


OF THE


ING PEO


THE


MUSICA


14


YEW EDITION, WITH ADDITIONAL ILLUSTRATIONS


THE MUSIC OF


J. STAINER


ITH SUPPLEMENTARY NOTES


HE


W. GALPIN, M.A. F.LS


PRACTICAL METHOD OF 


TRAINING CHORISTERS 


J. VARLEY ROBERTS


WEBSTER’S 


GCROUNDWORK OF MUSIC


WEBSTER’S 


ONE SHILLIN


PRICE FOR LEN PRICI 


MERBECKE‘’ 


550


CHARLES


NEW FOREIGN PUBLICATIONS


PIANOFORTE MUSIC. NI 


CHAMBER MUSIC. 


NOVELLO’S CATALOGUES, 1A HARMONIUM MUSIC 


NOVELLO'S CATALOGUES, 1


VOI READY


CONCERT VERSION OF THE POPULAR COMIC OPERA


TOM JONES


LYRICS BY 


CHARLES H. TAYLOR


COMPOSED BY


IMPORTANT TO CHORAL SOCIETIES


AN


AL 


NEW YORK, TORONTO, AND MELBOURNI 


EXAMINATION FOR ASSOCLYTESHIPHT


ROYAL COLLEGE OF MUSIC, 1914. 


FOR THE PIANOFORTE 


EDITED, ARRANGED IN GROUPS, AND THE FINGERING REVISED A


FRANKLIN TAYLOR


COMPRISED IN 56 BOOKS. 


PRICE ONE SHILLING EACH


ND SUPPLEMENT


PERA


ON


AIP


TESSIVE 


(SECOND SERIES


IN PRAISE OF NEPTUNE


PART-SONG FOR S.A.T.B. 


THE WORDS WRITTEN BY T. CHAMPION


THE MUSIC COMPOSED BY


EDWARD GERMAN


4 1 » XN 4 


LS SE: 


; SE ES SS = = — = ——FF 


[SSS EE SS = SS = 


MENT


1914


SS


IN PRAISE OF NEPTUNE


XUM


CRN


STS > SS = SSS SS


SSO OS ZT = = 


IN PRAISE OF NEPTUNE


SS 2445


2 2


CBN


CNL


=> =>  ~=>=- —_ 2


___ —_ 


SS 


1N PRAISE OF BEPTURE


AF PSE


IN PRAISE OF NEPTUNE


NN 


IN PRAISE OF NEPTUNE


_— * 1


CE = SS = SF — SS 