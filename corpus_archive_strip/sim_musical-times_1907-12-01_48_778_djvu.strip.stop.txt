


 MUSICAL TIMES 


 FOUNDED 1844 


 PUBLISHED 


 48 . 


 ROYAL CHORAL SOCIETY 


 ROYAL ALBERT HALL . 


 W EDNE SDAY , JANU AR Y 1 , 8 


 MISS AGNES NICHOLLS 


 MADAME ADA CROSSLE 


 MR . LLOYD CHANDOS 


 MR . WATKIN MILLS 


 BAND CHORUS , THOUSAND . 


 STANFORD * STABAT MATER 


 E COMPOSER ) 


 HANDEL 


 URSDAY , JANUARY 


 MISS MARY CONLY 


 MISS PHYLLIS LETI 


 MR . BEN DAVIES 


 MR . DAN PRICE 


 MR . HARRY DEARTH 


 ROYAL ACADEMY MUSIC 


 F. W. RENAUT 


 ROYAL COLLEGE MUSIC , 


 CONSORT ROAD , SOUTH KENSINGTON , S.W. 


 H.R.H. PRINCE WALES , K.G. 


 


 PRINCE 


 76 


 SC HOL ARSHIP 5 


 FRANK POWNALL 


 


 DECEMBER 


 MONTH 


 1907 


 LONDON 


 CHORAL 


 SOCIETY . 


 WEDNESDAY , 4 DECEMBER 


 , ENOIT HOLLANDER 


 SYMPHONIC POEM 


 POMPEII 


 M ESTA D’'ARGO . M EUNETA TRUSCOTT 


 M EDITH MILLER . M HAIGH JACKSON 


 SYMPHONY CONCERTS 


 QUEEN HALL ORCHESTRA 


 QUEEN H ORCHESTRA 


 SATURDAYS 


 VILI E GIVEN 


 QUEEN HALL ORCHESTRA . 


 ROBERT NEWMAN , M 


 YSAYE VIOLIN REC ALS 


 DEATH . 


 R.A 


 CONE 


 


 ASSOCIATED BOARD 


 MUSIC SCHOLARSHIPS 


 \L EXAMINATIONS MUSIC . CARNEGIE MUSIC SCHOLARSHIPS 


 W P : 


 P H.R.H. PRINCE WALI 


 O H | 


 M 21 , 23 , 24 , 25 , 2¢ OS S 


 MORECAMBE 


 GRANVILLE BANTOCK 


 ROYAL INCORPORATED 180 . 


 7 » 4 { , 


 XUM 


 THI 


 LOWS 


 F.R.C.O : 


 INCORPORATED GUILD CHURCH W. FE . HILL & SONS 


 MUSICIANS . 


 THI 


 


 REGISTER ORGAN VACAN ( 


 IES . , 


 CORRESPONDENCE SCHOOL MUSIC , ESTIMATES FREE . 


 LONDON , 


 ANALYSIS , ORCHESTRATION , 


 NATIONAL CONSERVATOIRE 140 , NEW BOND STREET , LONDON , W. 


 . ) INCOF 


 NATIONAL COLLEGE MUSIC 


 NORMAN BEARD P NT 


 ) IO . 


 IRGAN W 


 UM 


 TEACHERS PROFESSIONAL NOTICES . 


 CHRISTMAS HOLIDAY SESSION 


 N NIN ‘ ANUAI 


 CLAVIER HALL , NEW SEVCIK VIOLIN METHOD 


 PIANOFORTE TECHNIQUE | MR . SIVORE LEVEY 


 RAPIDLY ACQUIRING 


 S ) PRANO ) 


 SOPR ANO 


 ENGAGI 


 UM 


 MUSICAL 


 ( YORKSHIRE SOPRANO ) 


 . MENTS , 1 


 MISS ETHE L RADBURN 


 ( SOPRA ANO ) . 


 MISS IRE NE “ SPONG 


 MISS LILIAN TURNBULL 


 MR . HENRY AU MONT 


 ( FESTIVAL TENOR 


 BENSON 


 MR . GE ORG S 


 ( 


 FRANCIS G LYN N 


 MR . SAMUEL MASTERS 


 LIVERPOO . PHILHARMONIC SOCIETY 


 FEBRUA 


 MR . ASHBRIDG E “ MILLER 


 1907 . 773 


 MR . GWIL LYM RICHARDS 


 ARTHUR WALENN 


 MR . SAM U L MANN 


 ROBIN OVE RL E IGH 


 BASS - BARITONE 


 MR . HENRY SUNMAN [ RAM 


 MR . HERBERT TRACEY 


 BASS ) 


 MR . DUTTON SOLO BOYS 


 MUS.D. MUS.B 


 ALLISON 


 EVISION MUSICAL COMPOSITIONS . 


 A.M 


 


 77 , 1907 


 THEORY CORRESPONDENCE 


 \ H N 1 PREPARE STRETTON SWANN , M B. , F.R.C.O 


 , | | ( 1 . R.C.O L.R.C.O J 


 ( H H | N.W 


 DURHAM XFORI \M DGE , LONDON 


 . 2 " . = " ( . WW 


 W ) 


 DR . LEWIS ’ TEXT - BOOKS : ( 


 : . * HOIR EXCHANGE , 1 FIFTH AVE . , NEW 


 { CHING 


 CO . CHOIR EXCHANGE 


 MUSICAI 


 \ | | 0 . ) OFORT 2 ) 


 \\ K W 


 D2 : 


 SSISTAN 


 CTUDI 


 PUPII 


 TAN ] 


 RGA 


 MUSICAL 


 ( ( W 


 H 


 VACANCY RESIDENT PUPIL 


 & < R 


 STUDENT MUSIC TEACHER 


 . | 60 , W S W 


 } \ 


 CORRES 


 ‘ S 


 C RGAN SALE . 


 1907 . 775 


 O , ‘ O 


 1869 


 N ( " W. A. G .. N 


 W : W 


 TEACHING CON 


 SOMPOSERS 


 DINHAM 


 MUSIC 


 TRINITY COL LEGE MUSIC . _ _ “ PIANOFORTE 


 ABILITY PLAY 


 BUYING PIANO PL AYER 


 ILLUSTRATED CATALOGUE APPLI N 


 HIGHER EXAMINATIONS MUSK JOHN BROADWOOD & SONS , LTD . , 


 W 


 BOSWORTH & CO 


 MUSIC CHILD PLAY 5 , PRINCES STREET , OXFORD STREET , LONDON , W. 


 SUITE ROYAL COLLEGE ORGANISTS . 


 ) CHRISTMAS NEW YEAR 


 £ 30,001 


 ( 1753 ) | 


 ( 1011 - 


 753—1 


 DECEMBER , 1907 


 I.—PRINTED 


 MR . GEORGE F. BARWICK , B.A. 


 1838 


 18 , 


 UM 


 779 


 MONTAGUE HOUSE 


 


 


 GARDEN 


 MUSICAL TIMES.—DEcCEMBER , 1907 

 Mozart , Beethoven , Spohr , Cherubini , Mendelssohn ; 
 operas , quartets , symphonies , named ? 
 England producing organ composers , operatic 
 composers , instrumental composers ; new era 
 arisen , incumbent , achievements 

 matter record 




 MUSICAL LIBRARIAN BRITISH MUSEUM . 


 UM 


 781 


 READING RO 


 MUSIC ROOM 


 MACAULAY 


 OM FREQUENTED CARLYLE 


 TRUSTEES BRITISH MUSEUM 


 EA ‘ ) 


 LIM 


 783 


 BYRD FANTASIA C 


 1900 


 784 


 J 2 = 


 BYRON 


 UM 


 MUSICAL 


 MR . FRED . R 


 ETAR\ LEE 


 SPARK , J.P 


 SIC AL 


 MUSICAL 


 736 


 1907on Nove om r 4 5 5 . manuscripts , came 

 collections Julius Stockhausen , Wilhelm 
 faubert Maurice Schlesinger ( Paris ) 
 supreme interest . Subjoined ‘ Lots ’ 
 ind amounts sold : 
 4 
 BEETHOVEN . Pianoforte Sonata E ( Op . 109 ) 800 
 String quartett F ( Op . 135 ) - - . - 735 

 Musik zu einem Ritterballet ’ : unpublished 
 pianoforte score , written Beethoven 
 age , purely 
 orchestral composition - - : . . 250 

 conversation - book ’ year 1825 , 
 containing lines autograph _ — - 
 Portrait , autograph dedication - - 43 
 BERL10z . Marseillaise , arranged esshestmn 
 chorus ” - 55 
 BRAHMS . arrangement pianoforte duet 
 Symphony D ( Op . 73 - 85 
 Orchestral arrangement Schubert ’ s song ‘ 
 Schwager Kronos , ’ wspudlished - 45 
 Variations Hungarian melody ( Op . 21 , 
 . 2 ) , composed age - 
 years ( signed ‘ ] . Brhms ’ ) - - - 45 
 Cuopin . Waltz E flat ( Op . 18 ) - - - 140 
 Mazurkas . - - . - 
 Harfvy opera * La Juive * score - - 152 
 Haypn . interesting letter Artaria , Vienna , 
 publisher - 51 
 Lisz1 * Réminiscences des Hu ; snnete ’ ( ( Ip . 11 ) 
 — " age - seven years ” - 25 
 MEYERBEER . ‘ Robert le Diable , ’ Act I. , tageiner 
 hitherto unpublished scene - - 50 
 SCHUBERT . Variations pianoforte flute 
 ( Op . 160 ) - - - - 2290 
 Dance , pianoforte , unpublished - . - 60 
 SCHUMANN . Etudes symphoniques ( Op . 13 ) - - 60 
 * Des Sangers Fluch ( Op . 139 ) , score - - 35 
 seethoven , seen , head shoulders 

 rest great men prices 
 autographs realized creative achieve- 
 ments . amounts , great , 
 pale insignificance compared 
 sum £ 2,100 Mr. Karl W. Hiersemann , 
 Leipzig , asks autograph Beethoven ‘ Thirty- 
 variations waltz Diabelli ’ ( Op . 120 

 Messrs. Sotheby 
 November 8 , letters written Mendelssohn 
 realized £ 3 10s . , £ 2 55 . , £ 1 12s . respectively . 
 communications Planché , 
 criticizing words opera submitted 

 UM 

 MUSICAL TIMES.—Decemper 1 , 1907 . 787 
 musical discoveries end.| Mr. G. T. Smith , Birmingham , probably 
 Songs b » Lortzing , pieces Paganini , recently | oldest living member Birmingham Festival 
 turned « xp , important names be| Chorus , took meeting 1843 , 
 mentioned Mozart , hitherto unknown Violin Crotch ‘ Palestine ’ performed Dr. S. S. 
 concert ? reference p. 868 . In| Wesley , solo organist , gave remarkable display 
 addition foregoing , Beethoven ‘ Elf Wiener | powers playing ‘ O ruddier cherry ’ 
 Tinze ’ recently published byj|asa pedal solo . Mr. Smith writes : ‘ 
 Messrs. Breitkopf & MHiartel , | number MUSICAL TIMES , 
 performed subscription concert given | eighty - seventh year , thankful 
 Hambourg Quartet November 19 , Bechstein| play sing . ’ estimate 
 Hall . dances supposed identical|enjoyment veteran correspondent derived 
 mentioned Schindler , earliest ] music ? solace delight 
 edition Biography Beethoven , having | eventide long life ! 
 written master musicians played | ooneie 
 inn near Modling , met composer . Ninety - competitors entered prize 
 cy preserved — archives ) guineas offered Mr. W. H. Ash behalf 
 Thomass¢ aaa , Leipzig , long known , | Worshipful Company Musicians , 
 res irded probably Conpeeeae Weber . |i , 4 Liveryman , words Marching Song . 
 Dr. Hugo Riemann , , examined , | prize awarded Captain W. de Courcy 
 internal evidence come the| ctretton , late Royal Artillery . Sir Edward 

 conclusion Beethoven wrote , reasons 
 gives recent publication 
 editor . dances consist waltzes , minuets , 
 Landler , ‘ Landerer , ’ named . 
 music scored strings wind instruments . 
 music interesting , , expect , 
 represent great Beethoven 
 ‘ Missa solemnis ’ period , work 
 engaged 1819 , year supposed 
 written dances question 

 striking success Madame Tetrazzini 
 La Traviata ’ ‘ Lucia ’ Covent Garden 
 reminder 2/7 de/ canto dead 
 younger generation opera - goers supposed . 
 art alive — alive , 
 , true human feeling — die ; 
 application truism present case 
 old honourable art 7/ de/ canto , 
 appearances , vitalising 
 factor opera . charged 
 weakness snare ; performances old Italian 
 masterpieces sneered ‘ concerts in| 
 costume , ’ mistresses ag7/7¢a Jenny Lind | 
 Patti disparaged mere vocal | 
 gymnasts ; triumphs demonstrated , | 
 Madame Tetrazzini , /oriture | 
 decorative — essential 
 place dramatic scheme 

 Elgar undertaken compose music 
 song , published early year 
 Messrs. Novello 

 recently - published Pianoforte concerto D minor 
 Carl Philipp Emanuel Bach announced 
 performance Beethoven Hall , Berlin , 
 Bruno Hinze Rheinhold , November 28 . 
 work said composed 1749 
 Frederick Great . C. H. Bitter , , 
 ‘ Life C. P. E. Bach , mentions Concerto 
 D minor , 1748 , date written 
 ‘ Potsdam ’ ; , notwithstanding difference 
 date , apparently refers work 

 Susanne Morvey , child years old , 
 recently appeared concert Berlin played 
 pianoforte concertos — Beethoven E flat 
 Tchaikovsky B flat minor ! Early débuts 
 harmful , case 
 unsatisfactory ; selected 
 stupendous works little girl appearance 
 deserves strong censure , child 
 sympathy 

 strange thing 
 written ode organist . odes 
 music - makers sorts — nightingales , skylarks , 
 organ - grinders 




 


 ERNST 


 LENGYELERNST LENGYEL 

 pianoforte pupils , interest great 
 years old began 
 instruction music . rapid 
 progress age years played 
 select audience Budapest selection pieces 
 included Rondo Beethoven ‘ Sonata 
 pathétique ’ Chopin Impromptu . 
 occasion newspapers hailed ‘ Hungarian 
 Mozart . ’ Following phenomenal success 
 won municipal subvention carrying 

 music greatest pianists . ‘ play Liszt 
 time times : Liszt B minor Sonata 
 piece fond playing , ’ reported 
 said ; satisfactory know 
 likes Beethoven Bach , ‘ particularly 
 Appassionata Sonata Chromatic Fantasia . 
 notice boy appearance London , 
 genial guidance sponsor , Dr. Richter , 
 found p. 808 

 CHURC 
 nl 
 English 
 latest 
 theme , DU 
 open , 
 , 
 heard 
 Italian } 
 Ravenna , 
 Lombard 




 UM 


 , 1907 . 789 


 CHURCH MUSIC NORTHERN ITALY . 


 MILAN CATHEDRAL : VIEW 


 CHOIR , SHOWING DIVIDED ORGAN 


 790 MUSICAL 


 TIMES 


 DECEMBER , 1907 


 UNFINISHED 


 B MINOR . 


 C.B 


 LON 


 19907 791 


 1907 


 22 » 22 . & 


 1500 


 MUSICAL 


 CONCERT CHURCH MUSIC . 


 , 1907 


 KUM 


 MUSICAL 


 ST . PAUL CATHEDRAL . 


 MBER , 1907 


 URPIN 


 DR . T NERAL . 


 HANDELS ORGAN NDLING HOSPITAI 


 1907 


 796 


 } ALFRED HOLLINS . 


 VETERAN ORGANIST 


 ATION 


 PRESENTATIONS 


 CHURCH ORCHESTRAL SOCIETY 


 ORGAN RECITALS 


 10F 


 XUM 


 MUSICAL 


 ORGANIST CHOIRMASTER APPOINTMENTS . 


 WESLEY E. 


 ES.—DECEMBER , 1907 


 UM 


 800 MUSICAL 


 1907great 

 forms series — edited Mr. Newman — 
 entitled ‘ new hag music , ’ include 
 Mozart , Mr. W. H. Hadow ; Beethoven , Mr. D. F 

 Tovey ; Handel , Mr. R. A. Streatfeild . 
 volumes treat ‘ ‘ history musical criticism 




 BOOKS RECEIVED . 


 ASS . 


 - SONG 


 - SON P | | 


 ~—% = — — _ 2 - 


 2 — — _ - 


 PP — = 


 CHIMES CHIME TUNES 


 UM 


 07 


 OPERA 


 808 


 MUSICAL TIN 


 UEEN HAI ORCHESTRA , \ OZART ( 7 ) CONCERTOThe concert season 

 Ilall — extended notice , programme 
 familiar . Beethoven Pastoral 
 Symphony-—not played — chief feature , | 
 orchestral pieces bore company 
 Berlioz ‘ Carnaval Romain ’ overture , ‘ Tristan ” Prelude 
 Liebstod , Lalo ballet suite ‘ Namouna . ’ 

 named work , somewhat novelty , proved attractive 
 feature afternoon music , charm 
 Miss Julia Culp beautiful singing added enjoyment 

 ung 
 performer , interpretation giving evidence gifts whict 
 doubtless ripen experience . remainder 

 programme -- long afternoon concert — consisted 
 Beethoven ‘ Coriolan ’ overture , Tchaikovsky 
 Arthur as| Symphony F minor ( . 4 ) , ‘ Finlandia ’ 
 symphonic poem Sibelius . Mr. Henry J. Wood 
 | conducted concerts wonted alacrity . 
 LONDON SYMPHONY ORCHESTRA 

 large audience naturaily attracted ( Jueen Hall 

 second concert , November 158 , programme 
 opened exceptionally fine performance Elgar 
 overture ‘ South ( Alassio ) . ’ new work 
 offered , interest concert centred chietly 
 | appearance England boy pianist , Ernst Lengyel , 
 , Lengyel von Bagota . Dr. Kichter 
 reputed enthusiastic exploits juvenile 
 prodigies , , introduction Lengyel , 
 fourteen years age , exceptional 
 testimony boy extraordinary powers . 
 | pieces selected début Liszt Concerto E fiat 
 jand Bach Chromatic Fantasia Fugue . Lengyel 
 | performance exacting compositions displayed 
 | marvellous technique , age , masterful power 
 jinterpret . true occasionally felt conscious 
 | making allowance age , ol 
 | boy bids fair world great 
 | pianists . portrait brief biographical sketch Master 
 | Lengyel found p. 788 

 Gnieg ‘ Holberg ’ suite string orchestra performed , 
 | said special effect ; 
 | magnificence playing impressiveness interpretation 
 | performance Beethoven C minor Symphony 
 described great achievement concert 

 vious 




 ROVA \CADEMY MUSIC 


 VOI 


 MR . ¢ 


 MUSICAL 


 N HA ) CONCERTS 


 MRrecord . concert - giver 

 A. 1 
 late Mr. Dannreuther , _ possesses considerable 
 rts : . 
 ve ability , scarcely sufficient command 
 nt justify electing heard Beethoven 

 E flat . 
 n znicéek , oO 
 Imperial Opera , 

 composed Mr. Hamilton 

 IR . CHARLES WOOI SYMPHONI \RIATION 
 e assistance New Symphony 
 estra , conducted Mr. Thomas Beecham , gave 
 urtistic concert November 14 , 
 talented pianist heard Beethoven Concerto G 
 1 Concerto C minor Saint - Saéns , playing 
 h womanly charm engaging vivacity 
 heartiest manifestations appreciation 
 concert opened Mozart 

 Miss Myra Hess , th 
 , 




 UM 


 , 1907 . SOQpianist , vic Steinway 

 November 18 , speak 
 . Mlle . Selva given Bach , Beethoven , 
 Schumann recital , works composers 
 interpreted familiar , pianist 
 readings special interest . Bach plavit st 

 satisfactory , rendering Beethove 




 5 


 15 


 TIMES 


 DECEMBER , 1907 


 BARROW - - FURNESS . § 


 MUSICAL TIM 


 RCESTER COUNTY ( MASSACHUSETTS 


 JUBILEE FESTIVAL 


 ES.—DECEMBER , 1907 . S11 


 MUSIC VIENNA . 


 R ECIAI RRESPONDIrendering ; plays temperament ‘ . tone 
 orchestra excellent apt forget 
 organization recently established 

 Ferdinand Loewe , Concert Society , given 
 adinirable performances Brahms , Beethoven fifth , 
 Schubert ‘ Unfinished ’ Symphonies . concerts , 
 , | eard Mozart ‘ rece ntly discovered ’ Violin 
 concerto . interesting music , 
 wonder Mozart wrote : work 

 ver , - trained , gifted composer ; 
 violin technique appear Mozcart . 
 addition Concerto , Professor Petri played 
 Joachim Orchestral variations , memory departed 
 master , months ago , world- 
 famed associates , performed Beethoven ( Quartets 
 enthusiastic audiences . Special p 

 formances recently 
 riven memory composers Edvard Grieg 

 irieg Pianoforte concerto Briill Rhapsody 
 pianoforte orchestra , Frl . Vera Schapira playing 
 solo parts 

 Beethoven ninth Symphony performed 
 Philharmonic Society direction Schalk ; 
 went , especially choral section . 
 concert played composer C minor , 
 seldom - heard original engaging Symphony 
 B minor Haydn . Schalk conducted extremely 
 fine impressive performance Bach B minor Mass 
 concert Gesellschaft der Musikfreunde . 
 soloists best Frau Cahnbley ( soprano ) Herr 

 Senius ( tenor ) . Specially effective difficult trumpet 




 812 


 MUSICAL TIMES.—DE 


 EMBER , 1907 


 1995 , DV 


 MA ; 


 MUSIC BELFAST . 


 ) RRESPONDENT 


 MUSIC BIRMINGHAM . 


 ( KR CORRI INDENT 


 MUSIC BRISTOL DISTRICT 


 CORRESPONDENT . ) Colston Hall , November 6 , Bristol Harmon 
 Male - voice Choir gave annual concert , t 
 direction Mr. Jenkins . addition glees , Signor 
 Natale Morro gave operatic solos , songs | 
 Miss Gertie Somerton Mr. W. Morgan . Mr. Georg : 
 Riseley played accustomed ability organ 

 Clifton Quintet opened season concert 
 Victoria Rooms , November 7 . executants wer 
 Messrs. Maurice Alexander Ilubert Hunt ( violins 
 Ernest Lane ( viola ) , Percy Lewis ( violoncello ) , Herber 
 Parsons ( pianoforte ) . Grieg ( Quartet G minor ( Op . 27 ) , 
 Beethoven ( ) uartet C ( Op . 59 ) skilfully played 
 Mr. Lewis executed Ballade Serenade Joseph 

 




 


 EDINBURGH . 


 RESPONDENTMUSIC 

 
 Nove r 11 , Dr. Cowen conducted fine performances 
 Beethoven ‘ Leonora overture 3 , Schumann 
 D r Symphony , Tchaikovsky ‘ Capriccio Italien ’ 
 ’ p. 45 ) , bourrée hornpipe Hlandel ‘ Water 
 music , ” d new march , ‘ Pomp Circumstance , ’ 
 Elzar . Madame Blanche Marchesi vocalist , 

 ns including Senta ballad ‘ flying 

 f interpretations Cesar Franck Sonata 
 \ major ‘ Kreutzer ’ Sonata . M. Thibaud played 
 Saint - Sacns ‘ Introduction Rondo Capriccioso , ’ 
 M. Cortot gave transcription organ concerto 
 riedmann Bach . Mrs. Henry J. Wood , accompanied 
 Mr. W , vocalist . 
 October 29 Edinburgh String ( Quartet ( Messrs. 
 Colin Mackenzie , J. H. Hartley , R. de la Haye 
 D. Millar Craig ) performed quartets Mozart ( 

 Beethoven ( E flat , Op . 74 ) , Dvorak ( F , Op . 96 

 Mr. Robert Burnett , fame baritone vocalist 
 local , gave annual recital October 30 . 
 programme contained fewer - songs , 
 considerable number indicated given 




 GLASGOW 


 CORRESPONDENT 


 MUSIC 


 OWHerbert Walton A. M. Henderson 

 Choral Orchestral Union auspiciously opened 
 work November 12 , Dr. Cowen 
 Scottish Orchestra warmly greeted large audience . 
 personnel orchestra practically 
 year , successful season anticipated . programme 
 opening concert familiar lines , included 
 Beethoven ‘ Fidelio . ” . 4 , 
 Pianoforte concerto , Schumann Symphony D minor , 
 Elgar ‘ Pomp 
 Circumstance ’ March , . 4 . solo Concerto 
 brilliantly played Signor Busoni , Dr. Cowen 

 obtained best band symphony . Elgar 

 new march proved popular number . 
 Saturday Popular Concert November 16 , 
 Dr. Cowen submitted highly attractive programme , 

 popular numbers Beethoven overture * Leonora ’ 
 . 3 , Mozart Symphony FE flat . Sibelius march 
 suite * Karelia , ’ Nos . 2 4 Holbrooke 
 ‘ Scenes Dreamland ’ played time 
 , | eing espe cially received . 
 additional attraction fine singing Madame Blanche 
 Marchesi 

 Associated Scottish Orchestra , Choral Union 




 MUSIC GLOUCESTER DISTRICT 


 CORRI DENT 


 


 814 MUSICAL TIM 


 MUSIC LIVERPOOI 


 ES.—DECEMBER 1 , 1907MUSIC MANCHESTER 
 M ( N ( NDEN 
 second Halle concerts , October 
 Marie Hall played Max Bruch Violin « erto 
 Saéns Rondo capriccioso , 1 « ¢ lled 

 performances Beethoven 

 Ilebrides 

 Oympnony 

 following concert October 31 , played | 
 Pianoforte conc : César | 
 Symphonic Variations . orchestral 
 Brahms Symphony , ‘ Leonora ’ ov 
 ‘ Parsifal . ” tl 
 November 7 , Flgar ‘ Dream Geront 
 given — sixth time concerts . pr 
 Miss Alice Lakin , Mr. John Coates Mr 
 Brown . programme November 14 
 Beethoven ‘ Eroica ’ symphony , Strauss * Ein 

 Good Fridav music 




 OOK 


 UM 


 MUSICALcerts , given November 16 , 

 Mr. Carl Fuchs , Sevcik ( Quartet Prague 
 ( Mess Lhotsky , Prochazka , Moravec Vaska ) appearec 
 da fine impression performance 
 W string quartets — Brahms B flat ( Op . 67 
 F ( Op . + Beethoven F minor ( ‘ 5 
 la pianoforte recital given November 15 , 
 y Mr. S. Speelman Mr. R. J. Fort 
 consisted sonatas Brahn 
 rig clarine und = pianof 
 ‘ Mar nbilder “ Schumann ( Op . 113 ) 
 Mr. S man sympatl playing 
 prov audience genuine 

 Miss Sylvia Loring sang tw ) sets songs } 
 e Mr. Brand Lane subscription concerts 




 MUSIC 


 NEWCASTLE DISTRICT 


 FRO N CORRI NDENT 


 1907 . S15 


 MUSIC NOTTINGHAM ISTRICT 


 


 MUSIC SHEFFIELD DISTRICI 


 HH 


 MUSICAL TIM 


 YORKSHIRE 


 NDENT . ) 


 ES.—DECEMBER , 1907orchestra , Mr. F. E. Naylor efficient organist 
 devotional character work 
 emphasized performance conditions Mr 

 following evening Wakefield Chamber 
 Concerts took place , assuming form enjoyable 
 recital violin pianoforte Madame Soldat 
 Mr. Leonard co - operated 
 Beethoven — ‘ Kreutzer , ’ , early 
 Sonata F ( Op . 24)—and Schumann ( minor 

 Hull Symphony Oychestra season active , 
 interesting afternoon concerts 
 reviewed : October 24 , November 7 
 Mr. Wallerstein inspiring direction smart 




 UM 


 BERLIN 


 ILAN 


 MOSCOW , 


 NICE . 


 V F. 


 ~ DEN 


 MUSICAL 


 TIME 


 DECEMBER , 1907 . 819 


 CONTENTS . P 


 M 4 


 N 


 


 RMAN , RF 


 WER , 


 ALAHORRA , REMIGIO O * 


 TTON , 1 


 OWAY 


 MONTH . 


 LI 


 AOT 


 


 MUSICAL 1 , 1907 


 454 


 759 


 N ? ELLO CHRISTMAS CAROLS 


 W. H. SANGSTER 1 


 N 30 . 


 . 5 


 YURCELL , 


 1 


 S ‘ HOOL MUSIC REVIEW 


 507 


 ZIMMERMANN 


 IM 


 1608 


 160 


 1600 


 TILLA 


 MUSICAL 


 TIMES . — DECEMBER 


 THI 


 , IC SOL - FA PUBLICATIONS 


 MCNAUGHT : — 


 ROLAND ROGERS 


 BI 


 REDUCED PRICI 


 MISS WINIF 


 WHITSUNTIDE , 190 


 DECEMBI 


 “ DELLE SEDIE SCHOOL SINGING , 


 MR . MONTAGUE 


 BARITONE 


 BORWELL 


 ELAJAH D N 


 D MARWOOD 


 CONTAINS 


 STERNDALE BENNETT , M.A. , N 


 ( \ ‘ \ 


 M 


 TRUE LOVE HATH 


 HEART 


 SONG 


 SIR PHILIP SIDNE\ 


 IVOR ATKINS 


 MIE TREASURES DEEP 


 SONG 


 HAROLD HARFAGER 


 SIR WALTER SCOTT 


 BARITONE SOLO , CHORUS ( MALE VOICES ) 


 ORCHESTRA 


 JOHN POINTER 


 MUSICAL TIMES 


 SPECIAL NOTICE . 


 SCALE TERMS ADVERTISEMENTS . 


 H. 


 


 WILI 


 POPULA 


 STA 


 MUSICAL 


 OLD FIRM 


 


 P. CONACHER & CO . 


 RINGWOOD WORKS , 


 HUDDERSFIELD . 


 NICHOLSON CO . 


 ORGAN BUILDERS 


 PALACE YARD , WORCESTER . 


 ( ESTABLISHED 1841 . ) 


 ORG . STUDIES 


 IENDED EMINENT 


 TEACHERS 


 INGLY RECOM ) 


 RECITAI 


 ISTS 


 EN ( SONS , \\ 


 M S 


 


 POPULAR ANTHEM TENOR 


 REV . F. A. JARVIS . 


 STAR ATTENDANCE CARD , 1907 , 


 R R £ ( M 


 BOOK 


 TS HAVI 


 CLOISTER CHANT 


 CATHEDRAL ORGANIS 


 O 


 SPECIALLY CONTRI ! TED WORK 


 ( W W S 


 E XE _ ISES TENOR VOICE ; 


 


 CHORUS 


 HEART AWAKENING 


 SONG 


 FLORENCE HOARE 


 ALBERT W. KETELBEY 


 AAR MARCHES 


 ORGAN 


 POPU 


 H M J. B. 


 K. 


 M ( ( G 


 B. M 


 | H 


 \I B. 7 


 \\ H. R. 


 M \ 


 M : S ! 


 M ( ) R. W 


 1 M ( 


 M : P. 3 


 R. W 


 ( ) R. W 


 N \ ( 


 S 7 TENOR VOICE " ERAS ISEESG . 


 PI RIOD 


 INDISPENSABLE LIBRARY REFERENCE BOOK . 


 NOVELLO COLLECTION 


 WORDS ANTHEMS 


 CONTENTS 


 L\THEDR CHICHESTER CATHEDRAI ST . MARY CATHEDRAI 


 CANTERI RY CATHEDRA WORCESTER CATHEDRAI ETON COLLEGE . 


 LORGE CHAPEI WINDS SALISBURY CATHEDRAI ST . PETER ’ , CRANLEY GARDENS 


 CHAPEI YA S ( MI S PETERBOKOUGH CATHEDRAI S.W 


 R CATHEDRAI LLANDAFF CATHEDRAI LINCOLN ’ INN CHAPEI 


 1 CATHI RA NEW COLLEGE , OXFORD “ GLASGOW CATHEDRAI 


 CATHEDRA CHRIST CHURCH , OXFORD * “ MANCHESTER CATHED ! 


 PARISH CHURCH 


 ANTHEM BOOK 


 ANTHEMS 


 PRICE SHILLINGS 


 


 ANTHEMS ADVE NT 


 MUSICAITI 


 1907 


 ~ - _ ACCOMPANIMENT 


 F BLOW YE TRUMPET 


 


 NOVELLO CHRISTMAS CAROLS 


 N } 


 ( 1 


 SERIES 


 A. HI 


 J. B 


 4 . S 


 J. B. ¢ 


 A. 3 . 5 


 G. C. M 


 \. H. 1 


 W. H. M 


 TI 4 


 154 1 : 


 BRAMI 


 STAIN ! J¢ 


 157 . 4 


 MUSICAL 


 1907 


 NOVELLO 


 18 — — 


 24 


 CHRISTMAS 


 344 + 


 M. A. 


 CHRISTMAS 


 BUNNETT 


 EDWARD 


 CAROLS 


 CONTENTS SET 


 13 . CAROL , SWEETLY CARO ! : LET MERRY . 


 4 . HRISTMAS BELLS . = + ; HARK ! ANGELS SINGING . 


 ANGELS ’ CAROI \ CHILD BORN . 


 { SHEPHERDS WATCHED O SAVIOUR DEAR 


 \ CHRISTMAS CHIMI 


 ( 2 . RING , SWEET CHIMES BRIGHTEST MORN . 


 ( 3 . BRIGHTEST BES CHIMES SNOW . 


 ( 4 . HAIL : HAPPY MORN 4 ANGELS ’ SONG . 


 ( 5 . O LET VOICES 5 SONG FAITHFUL . 


 CHRISTMAS EVE . 6 . HERALD BELLS . 


 2 . ARK THI BELLS > O DAY DAYS . 


 NOEI . ANGEL TIDINGS . 


 CONTENTS FIFTH SET . 


 PRICE , COMPLETE , NINEPENCE . 


 RAISE ICES 4 . BETHLEHEM . 


 YULETIDE BELLS ANGEL MESSAGE . 


 HAIL , WONDROUS MORN OLDEN CAROL . 


 BACH 


 CHRISTMAS ORATORIO 


 CAROLAU NADOLIG NOVELLO 


 THI 


 


 GLOR 


 GLO 


 UM 


 829 


 NEW 


 PUBLISHED . 


 SING O HEAVENS 


 


 MAUNDER . 


 SHEPHERDS WATCHE ] ) 


 REV . E. VINE HALL . 


 DAY CHRIST BORN 


 MEN VOICES 


 BASIL HARWOOD . 


 THINGS QUIET 


 SILENCE 


 


 HEALEY WILLAN . 


 RECENTLY 


 GLORY GOD 


 PUBLISHED . 


 


 


 BAYLEY 


 CLOWES 


 LORD RIGHTEOUSNESS 


 


 HUGH BLAIR 


 


 JESUS 


 


 JOSEPH HOLBROOKE . 


 SING REJOICE 


 JOHN E. WEST . 


 DAY 


 J ] . FREDERICK 


 BRIDGE . 


 HEAVENS 


 


 MACPHERSON 


 DECLARE 


 CHARLES 


 GOD 


 E. MARKHAM 


 GLORY HIGHEST 


 LEE 


 NX 


 CHRISTMAS ANTHEMS 


 HIGHEST 


 NOVELLO 


 


 STORY 


 SHORT SACRED CANTATA 


 WORDS WRITTEN 


 SHAPCOTT WENSLEY 


 COMPOSED 


 JOHN E. WEST 


 HOLY CHILD 


 EASY CHRISTMAS CANTATA 


 BETHLEHEM 


 CHRISTMAS CANTATAS 


 SOPRANO , TENOR , BASS SOLI , CHORUS 


 ORGAN 


 COMPOSED 


 THOMAS ADAMS 


 CHRISTMAS EVE 


 SHORT CANTATA 


 COMPOSED 


 NIELS W. GADE 


 CHRISTMAS 


 CANTATA FEMALE VOICES 


 COMPOSED 


 FREDERIC H. COWEN 


 SUITABLE CHRISTMAS 


 WORDS 


 HELEN MARION BURNSIDE 


 MUSIC 


 MYLES B. FOSTER 


 YULE - TIDE 


 CANTATA 


 COMPOSED 


 THOMAS ANDERTON . 


 SCENES 


 CONTRALTO SOLO , CHORUS ORCHESTRA 


 COMING KING 


 SACRED CANTATA FEMALE VOICES 


 


 COMPOSITIONS 


 SERVICES 


 ANTHEMS 


 ORGAN MUSIC 


 ' 


 ANTHEMS 


 ERNEST EDWIN MITCHELL . 


 SUN SOUL . 


 HEAVENLY VISION 


 NEW EDITION . READY , 


 PENITENCE 


 PEACE 


 J. H. MAUNDER 


 1907 


 CAROLS 


 ‘ ANGELS REALMS 


 ‘ CHRISTMAS BELLS 


 * YULETIDE 


 CHU RCH USE 


 IENTS 


 ALFRED R. GAUL . 


 ANTHEM CHRISTMAS 


 BLESSED LORD GOD 


 


 PRINCE PEACE 


 SACRED CANTATA 


 SOLO VOICES , CHORUS , 


 ORCHESTRA 


 WORDS SELECTED HOLY SCRIPTURE 


 MUSIC COMPOSED 


 RED R. GAUI 


 ALFRED R. GAUL . 


 


 COMMUNION SERVICE 


 KEY F 


 INCLUDING 


 OFFERTORY SENTENCES 


 INTROIT , “ HEARD VOICE HEAVEN 


 


 ET MUSIC 


 ALFRED R. GAUL 


 AGNUS DEI 


 ORIGINAL COMPOSITIONS 


 ORGAN 


 ALFRED R. GAUL 


 CONTA 


 PRI 


 NEW 


 BEHOLI 


 HOSAN 


 


 HARK , HEAVENLY SOUNDS EDITED 


 FLOATING . JOHN E. WEST . 


 PEACEFUL SLUMBERS LYING . SPECIAL OCCASIONS 


 831 


 CHRISTMAS CAROL CHRISTMAS MUSIC . 


 THOMAS H. HILL . . VOLUME VIII . 


 N ( MERBECKE COMMUNION SERVICE . 


 


 1907 


 VOI 


 NOVELLO LIST WORKS SUITABLE FORCHRISTMAS PRESENTS & SCHOOL PRIZES 

 CORONATION SERVICE , Music performed Coronation Majesti , 
 KinG Epwarp VII . QUEEN ALEXANDRA . 
 oO , Supe Royal , 2s . 6d . ; Bound cloth , gilt , 5S. ; Bound leather , gilt , 7s . 6d . ERI 
 - . FREDER 
 Edition de Luxe Japanese Vellum Paper , red lines ( restricted 500 copies ) , bound FREI 
 Crushed Polished Morocco , 63 : 
 OrR¢ 
 rea 
 J. S. BACH — W Ml f ( MENDELSSOHN—1 forte Works ( comp Ditto 
 168 | . \ ' ; th , gi R 
 JOSEPH BARNBY — O H R J. S. BAC 
 ' Worte . complete edit Kight : 
 STERNDALE BENNETT—1 . Folio . cloth , silt , Ge . : Ratle GRAND § 
 ; -ERLIO“Z a4 g : : 
 BERLIO . ~*~ “ Fug 
 D } K K } 
 BEETHOVEN~—S \ . ar 
 : g Rutla wv . T. BI 
 S ( Ss f ( D ) 
 ~ RICK ren \s Ml l oO ‘ 2 ( Dp ARRANG 
 kk t t \ . 
 S g GRI 
 CAPTAIN C. R. DAY 1 , silt 6 
 Plates 4 : D Deep \ g : , 
 Ditto . MOORE~ Irish M ate os 
 th , zg 4 rans 
 CATHEDRAL PRAYER BOOK M MOZART-—S folio , cloth , gilt 18 
 N De ( P - gilt 18 Ma : 
 ges ¢ Life M t. O Jann K Ditto 
 ; Vhree Vi 
 N W ( Wet | 7 
 Psalt ( ( EMMA MUNDELLA — Day S$ Hymn B COLLEC ! 
 Cha edg t c ! 
 D FREDERICK NIECKS~ Progr e Musi . . 
 N ; ' ‘ cb r LA ° 8 t ; Co ? 
 } OVELLO COLLECTION WORDS ANTHEMS 
 > anes ‘ ends ane thems . New ART OI 
 Cr ‘ t ! edges 
 ( z r . : red edges 7 Ditto 
 fTHE PARISH ANTHEM BOOK — Cont g words 
 CATHEDI H 0 favourite lot CH . GOL 
 ORATORIOS CANTATAS—(Novt Original Octay , 
 CHRISTMAS CAROLS NEW OL ! psi Jon . Ream , romnds SEI 
 , } H } ‘ “ cc 
 . Kes , | rked pe f paper ra 
 er « J. 
 Dit “ e th , gilt 7 6 ) 4 , RANDEGGER- Sacred Songs Little Singers . Words P 
 FE . DANNREUTHER — M D FRAD R vy H t th , gilt SEL 
 M t RUBINSTEIN~—1 ns " 7 % 
 SCHUMAN N — ( tions Pianoforte . E G. 
 J. B. DYKI yA.Z \ th , gilt 
 F.G. EDW D Vol . \ gilt 7 HANDE ] 
 J \ \ g t 3 4 
 Srx 
 Ww . FI ‘ x Songs . N. Macrarri g O } 
 CARL | ] , \ \ \ . t CHORU 
 g gilt 4 Ty 
 STAINER — Q 
 Sit o N S Exerci ~ ) 7 
 1D S JOHN | 
 HANDEI ! W. A. g 
 » H \ HAND 
 rAINER , J ( KE . W. B. NICHOLSON SHOR 
 EDUA . Duf , ; 
 } Vol . ' — 
 MORI { NN = MS . M EDWIN 
 \ \ N | | \ ry 
 rHUR Sl \N — ] Tut 
 H. HERk NLIGHT SON ( \ U 
 LADY Song , 
 PSALTER NOTED — ( s ( S ' Vol . 
 S H. | } W. H. Free y 

 A. Vol . | 
 | ] W. ¢ \ 
 NI N Vol . 
 FRANKLIN TA\ Vol . 
 a. C. MACK HN E. WEST — O ' Vol . 
 NOVELLO COMPANY , Limirep 




 VOLUMES ORGAN MUSIC 


 


 SUITABLE 


 FREDERIC ARCHER . 


 J. S. BACH 


 ARRANGEMENTS SCORES 


 COLLECTION PIECES CHURCH 


 CH . GOUNOD . 


 SELECTION MOVEMENTS 


 REDEMPTION . ” 


 SELECTION MOVEMENTS 


 G. C. MARTIN 


 HANDEL 


 1 , NIXON 


 JOHN HILES . 


 EDWIN H. LEMARE . , 


 COMPOSITIONS ORGAN : 


 LONDON 


 NOVELLO 


 WG 


 


 


 PRESENTS 


 J. LEMMENS 


 STYLE 


 MENDELSSOHN 


 JOSEF RHEINBERGER 


 ORIGINAI COMPOSITIONS PHI 


 ORGAN : 


 C. H. RINK . 


 HENRY SMART . 


 W. SPARK 


 ORGANIS1 T QUARTERLY JOURNAL 


 VILLAGE ORGANIST 


 LEFEBURE WELY . 


 W. J. WESTBROOK . 


 834 MUSICAL TIM 


 


 \ COURSE STUDY SCHOOLS , COLLEGES , 


 GENERAL READERS 


 THOMAS WHITNEY SURETTE 


 DANIEL GREGORY MASON 


 EXTRACT PREFATORY NOTE 


 ADULT MALE ALTO 


 


 COUNTER - TENOR VOICE 


 G. EDWARD STUBBS 


 M.A. , MUS . DOC 


 PRACTICAL HINTS TRAINING 


 CHOIR BOYS . 


 CURRENT METHODS TRAINING | 


 30VYS ’ VOICES 


 MANUAL INTONING 


 CLERGYMEN . 


 ES.—DECEMBER 1 , 1907 


 COMPOSITIONS 


 


 CANTATAS 


 PARKT - SONGS 


 SERVICES 


 EDUCATIONAL 


 HENRY GADSBY 


 SOPI 


 


 


 PUBLISHED 


 VISION LIFE 


 CHORUS , ORCHESTRA 


 SOPRANO BASS SOLI 


 


 C. HUBERT 


 H. PARRY 


 TIMES 


 DAILY TELEGRAPH 


 DAILY CHRONICLE 


 PRICE SHILLINGS SIXPENCE . 


 MORNING POST 


 MORNING LEADER 


 PALL MALL GAZETTE 


 ATHEN.£UM 


 COMPANY , LIMITED 


 826 MUSICAL TIM 


 PRODUCED ARDIE MUSICAL FESTIVAI 


 GIVETH 


 BELOVED SLEEP 


 CONTRAI 


 SOLO 


 


 ELIZABETH BAKRETT BROWNING 


 FREDERIC H. COWEN 


 W MS 


 DAILY TELEGRAPH . 


 / STANDARD . 


 MORNING LI EI . 


 ATHEN. © UM ‘ 


 THENUM 


 ES.—DECEMBER 


 1907 


 SIR PATRICK SPENS 


 BALLAD 


 BARITONE S 


 COMPOSED 


 A. HERBERT BREWER 


 ARDIFF MUSICAL FESTIVAL 


 1.0 , CHORUS , ORCIIESTRA 


 PRODUCI 


 EN 


 ACCOMP 


 FOU 


 UM 


 PRODUCED LEEDS FESTIVAL , OCTOBER , 1907 . | ~ COMPOS SITIONS 


 


 SPRINGTIME % MES PATTON 


 TIMES . ORIGINAL ORGAN COMPOSITIONS 


 DAILY TELEGRAPH = 


 STANDARD . 


 PALL MALL GAZETTE . HUMOROUS - SONGS . 


 NEW SERIES 


 SACRED SONGS 


 EDITED , MARKS EXPRESSION PHRASING , 


 ALBERTO RANDEGGER . 


 PRICE SHILILINGS NET BOOK . 


 SACRED SONGS SACRED SONGS 


 SOPRANO TENOR 


 SACRED SONGS 


 CONTRALTO 


 ( SET 1 


 SACRED SONGS 


 BASS 


 SET ) . 


 LONDON : NOVELLO 


 SECOND SET 


 EASY 


 ORGAN 


 SHORT PIECES 


 


 


 EDWARD BUNNETT 


 


 ANDANTINO G FLAT 


 ORGAN 


 SONATA 


 D MINOR 


 ORGAN 


 COMPOSED 


 ALFRED H. ALLEN 


 COMPANY 


 ORIGINAL COMPOSITIONS 


 ORGAN 


 HARWOOD 


 LIMITED 


 BASIL 


 ORG : WORKS RECIT ALS 


 


 THI 


 


 


 


 KUM 


 MUSICAL 


 


 MELODIOUS TECHN 


 PIANOFORTE 


 MIQUE 


 J. A. O'NEILL . 


 CONGREGATIONAL 


 CH , ANT- BOOK 


 C Y 


 H. NIC HOLSON 


 SYDNEY 


 SHORT TREATISE 


 RUDIMENTS MUSIC 


 HEALE 


 NARCISSUS ECHO 


 CANTATA CHORUS , SOLI , ORCHESTRA 


 COMPOSED 


 EDWIN C. . 


 HINTS 


 


 YOUNG VIOLINISTS 


 C. EGERTON LOWE 


 BOOK 


 NEW FOREIGN PUBLICATIONS 


 ER MUSIC 


 CHAMB 


 DUCOUDRAY 


 BOURGAUL 


 ORGAN HARMONIUM MUSIC 


 SCORE ORCHESTRAL PARTS 


 VOCAL MUSIC . 


 IMPORTANT NOTICE CHORAL SOCIETIES . ~ 


 READY 


 CONCERT VERSION 


 


 MERRIE ENGLAND 


 WRITTEN 


 BASIL HOOD 


 COMPOSED 


 EDWARD GERMAN . 


 GUY D’HARDELOT , EDWARD GERMAN . 


 “ THY SONGS “ ENGLAND . ” 


 “ YEAR AGO “ MAIDENS - MAYING . " ” 


 ~ GARDEN LOVE . “ LOVE MEANT GLAD . ” 


 4 . 7 — * LIG HTERMAN TOM . 


 MAUDE VALERIE WHITE . “ JACK . ’ 


 SONGS HAV } PASSED \W \\ ‘ OLD BLACK MARE , ” 


 TERESA DEL RIEGO . “ LOVE VALLEY . ’ 


 GREEN HILLS IRELAND . “ ROSE ENCHANTED . 


 RING HERMANN LOHR . 


 “ 4 COON LULLABY SONGS NORSELAND : 


 ; ‘ SWEET , BEFORI SWALLOWS FRANCO LEONI . 


 : * PRINCESS SUNNY SMILE . 


 CHARLES BRAUN . “ LEAVES RIVER . 


 GOLDEN BROOM “ COOLAN DHU . 


 H. WALFORD DAVIES . LESLIE STUART . 


 HAMI “ OLD SHIELD 


 FRANK E. TOURS “ ROAD TIPPERARY , 


 NA ‘ ~ 


 RED ROS } S. LIDDLE . 


 MOTHER O ° MINI “ CHRISTMAS BELLS . 


 ALMA GOETZ . ERNEST NEW TON . 


 GOLDEN DAY ‘ VIVANDIERE 


 RS SUNY “ MAGIC MONTH . 


 DOROTHY FORSTER “ LOVE ECHO . ” 


 “ ROSE BUD CHARLES A. TREW . 


 VERE SMITH . “ LOVE THEE . 


 FAIRYLAND ROBERT CONINGSBY CLARKE . 


 BOTHWELL THOMPSON “ MASTER MAN . 


 LOVE - LIL\ “ KNEW . 


 “ BENEDICT LAMENT . FRANK LAMBERT 


 CHARLES BENNETT . ‘ BUD BRIAR . 


 M AY MUSIC SELLERS 


 BRITISH MUSEUM READING ROOM 


 XUM 


 JOHN GOSS 


 XUM 


 ANTHEM 


 ALTO . = = < , = 


 92 


 2 S459 2 


 S.A 


 XUM 


 LET WICKED FORSAKE WAY 


 3S = = : = 


 — _ _ _ — _ — _ — _ _ — _ — = — — — _ — _ 


 LET WICKED FORSAKE WAY 


 EXTRA SUPPLEMENT 


 NOVELLO CHRISTMAS CAROLS 


 RING JOYFUL SALUTATION 


 33 


 68 


 69 . 


 70 


 71 . 


 72 . 


 73- 


 74 


 75 


 76 


 47 


 78 . 


 95 


 97 


 99 . 


 100 . 


 102 . 


 103 . 


 104 . 


 9 A. 


 105 . 


 106 . 


 107 . 


 108 . 


 109 . 


 10 


 112 . 


 113 


 II4 


 II 


 4 4 


 RING JOYFUL SALUTATION 


 REDUCED PRICE 


 BABYLON WAVE 


 PARAPHRASED 


 265 


 100 


 EXTRA SUPPLEMENT 


 NOVELLO CHRISTMAS CAROLS 


 263 


 264 . 


 * 265 


 266 . 


 267 . 


 268 . 


 269 


 270 


 271 . 


 272 . 


 273 . 


 274 . 


 275 


 270 


 ENDING YEAR 


 284 . 


 285 . 


 286 


 341 . 


 342 


 343 


 344 


 EZ 


 ENDING YEAR 


 - SONGS 


 CHRISTMAS 


 HYMNS CAROLS 


 155 . 


 156 


 157 


 158 . 


 159 . 


 100 , 


 161 


 162 


 163 . 


 164 


 165 


 TIDE 


 166 . 


 167 . 


 168 


 EXTRA SUPPLEMENT 


 EARTH 


 CHRISTMAS S 


 CHRISTMAS BELLS 


 ONG PRAISE 


 116 . 


 117 . 


 118 , 


 119 . 


 120 . 


 121 . 


 # 122 


 123 . 


 124 


 125 


 126 


 155 


 156 


 157 


 155 


 166 


 14 


 SSS = = = = 


 SSS 


 SSS SS SS SS SS 


 2 SS SS SS SES SS 


 ( SSS SS = — — — 


 < = = . SSS FS S== 


 _ | 4 


 | N MSR ROE