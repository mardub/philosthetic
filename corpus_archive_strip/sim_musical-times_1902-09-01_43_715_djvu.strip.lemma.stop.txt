


 MUSICAL time 


 singing - class circular . 


 publish month 


 WORCESTER MUSICAL FESTIVAL 


 SEPTEMBER , 1902 


 SCARBOROUGH MUSICAL FESTIVALBAND choru 300 

 programme . 
 WEDNESDAY evening , Sept. 17 . — ' ' Elijah ' ( Mendelssohn ) . 
 THURSDAY AFTERNOON . — miscellaneous , Stanford ' ' Re- 
 venge , ’' Tschaikowsky ' ' Symphonie Pathétique . ’' song , Miss 
 Ada Crossley 
 THURSDAY evening . — " ' Faust " ' ( Berlioz ) . 
 FRIDAY AFTERNOON.—Miscellaneous selection , 
 Beethoven . 7 Symphony . 
 FRIDAY evening . — * Messiah ’' ( Handel ) . 
 communication address , Hon . Secretaries , Musical Festival , 
 Scarborough 

 pe 




 SHEFFIELD 


 triennial MUSICAL festival 


 MAJESTY KING . 


 MAJESTY QUEEN . 


 T.R.H. prince princess WALES 


 ELIJAH , GARETH LINET , triumphlie . 


 dream GERONTIUS , coronation ode . 


 WANDERERS ’ storm SONG , ODE passion , 


 selection ISRAEL EGYPT , STABAT MATER , 


 bl pair SIRENS , hymn PRAISE 


 MURIEL FOSTER . 


 BEN DAVIES 


 ELLA RUSSELL . 


 AGNES NICHOLLS . 


 MAGGIE PURVIS . WILLIAM GREEN . 


 MARIE BREMA . | JOHN COATES . 


 KIRKBY LUNN . | DAVID BISPHAM . 


 ADA CROSSLEY . FFRANGCON DAVIES . 


 ORCHESTRA VOLUNTARY chorus 


 300 voice 


 SEPTEMBER 1 


 BRISTOL TENTH MUSICAL FESTIVAL 


 1902THURSDAY , Oct. 9.—1 o’clock , CORONATION ODE ( Elgar ) , 
 ST . CHRISTOPHER ( Parker ) ; 8 p.m. , HIAWATHA ( Coleridge- 
 Taylor 

 FRIDAY , Oct. 10.—1 o’clock , TRAUER - MARSCH , GOTTERDEM- 
 MERUNG ( Wagner ) , REQUIEM ( Berlioz ) , RIDE 
 WALKYRIES WOTAN ' ABSCHIED ( Wagner ) , 
 BERGLIOT ( Grieg ) , OVERTURE , 1812 ( Tschaikowsky ) , & c. ; 
 8 p.m. , EMPEROR CONCERTO ( Beethoven ) , POLISH 
 FANTASIA ( Paderewski ) , OVERTURES FAUST ( Wagner ) 
 TANNH - EUSER ( Wagner ) , & c 

 SATURDAY , Oct. 11.—2.30 p.m. , MESSIAH 




 BORWICK . 


 CARDIFF MUSICAL FESTIVAL 


 M. PADEREWSKISong Destiny ’' 
 Miscellaneous 

 THURSDAY , October 9 , 1.30 , ' ' beatitude 
 miscellaneous ; 7.45 , ' ' Flying Dutchman " 
 Symphony , ' " ' Eroica " ' ( Beethoven 

 FRIDAY , October 10 , 1.30 , ' ' Stabat Mater ' ( Rossini ) , Pianoforte 
 Concerto minor ( Schumann ) , Miscellaneous ; 7.45 , 
 ' * Samson Delilah " ' ( Saint - Saéns 




 ROYAL CHORAL SOCIETY 


 ROYAL ALBERT HALL 


 UNIVERSITY DURHAM 


 57 


 BLACKPOOL 


 second annual MUSICAL FESTIVAL 


 thirty class 


 


 SOLO , CHORAL , ORCHESTRAL 


 competition 


 £ 250 prize 


 SPECIAL evening concert 


 GRAND evening concert 


 


 MALE VOICE CHORAL COMPETITION 


 BIRMINGHAM MIDLAND INSTITUTE 


 SCHOOL MUSIC . 


 SESSION 1902 - 03 


 ROYAL 


 MANCHESTER COLLEGE MUSIC . 


 GUILD CHURCH MUSICIANS 


 NATIONAL CONSERVATOIRE 


 ROYAL ACADEMY MUSIC , _ 


 TENTERDEN STREET , W 


 1902 


 ROYAL COLLEGE MUSIC . 


 ROYAL COLLEGE ORGANISTS 


 VICTORIA COLLEGE MUSIC , 


 LONDON . ' 


 BOARD EXAMINATION 


 EDUCATIONAL DEPARTMENT . 


 YIM 


 57 


 PROFESSIONAL notice 


 MISS KATE GREGORY 


 MR . WILLIAM D. VINCENT 


 MR . MRS . WALLIS A. WALLIS 


 STROH VIOLIN SOLOIST . 


 MR . FRANCIS G. CONSTANTINE 


 MADAME DE BOUFFLERS 


 ( SOPRANO ) 


 tv - 


 MISS FANNY CHETHAM 


 ( DRAMATIC SOPRANO ) 


 MISS ESTELLA LINDEN 


 YORKSHIRE SOPRANO , ORATORIO ) 


 BEATRICE PALLISTER 


 change address 


 MISS AMY SARGENT 


 A.R.A.M. ( SOPRANO ) 


 MISS NELLIE WILLIS 


 SOPRANO 


 MISS BERYL CLIVE 


 CONTRALTO ) , 


 MISS GRACE DAY - WINTER 


 CONTRALTO 


 KATHERINE HAW KINS 


 CONTRALTO ) , L.R.A.M. 


 MISS ADELAIDE LAMBE 


 572 


 MR . CLAUDE ANDERSON 


 MR . WRIGHT BEAUMONT 


 MR . OTTO DENE 


 TENOR ) . 


 MR . TREVOR EVANS 


 MR . BRIGHT JONES 


 MR . JAMES LEYLAND 


 TENOR ) . 


 MR . SAMUEL MASTERS 7 


 TENOR 


 MR . FRED NORCUP 


 MR . JOS . REED 


 PRINCIPAL TENOR ) , 


 MR . GWILYM RICHARDS 


 ( TENOR 


 MR . HARRY STUBBS 


 MR . ARTHUR WALENN 


 BARITONE ) 


 MR . MONTAGUE BORWELL 


 


 MISS WINIFRED MARWOOD 


 MR . JOHN BROWNING 


 ( BARITONE ) , 


 MR . HENRY DOBSON 


 ( BARITONE 


 MR . CHARLES KNOWLES 


 ( BARITONE ) 


 MR . CHARLES TREE 


 MR . BERNARD FOUNTAIN 


 XUM 


 HARP 


 LON 


 PC 


 PRE 


 MI 


 


 ML 


 GUA 


 MR 


 LL 


 MR . ARTHUR SEDGLEY 


 BASS ) . 


 MR . STANLEY BRADSHAW 


 MISS ELLEN CHILDS 


 CHROMATIC harpist ) 


 SOLO BOYS 


 LONDON TRAINING SCHOOL chorister , 


 POLYTECHNIC , REGENT ST . , W 


 MR . DUTTON SOLO boy 


 MR . TANN SOLO boy . 


 MUNRO DAVISON MALE - voice choir . 


 LONDON WIND QUINTETTE 


 guaranteed popular concert 


 party tour country 


 MR . STEDMAN MUSICAL : AGENCY 


 BROOKE & ARTHUR , 


 MUSICAL AGENTS 


 7 , MADDOX STREET , REGENT STREET , W. 


 SPECIAL QUARTET oratorio : 


 HUGO GORLITZ , 


 MUSICAL AGENT , 119 , NEW BOND STREET , LONDON , W. 


 SOLE manager 


 


 KUBELIK 


 KUBELIK TOUR 


 ALICE E. JOSEPH 


 OPERA CONCERT AGENCY 


 LONDON CONCERT & CHOIR AGENCY 


 evision MUSICAL composition 


 574 


 degree MUSIC . 


 preliminary art . 


 MUS : BAC . , . MUS : DOC . , 


 preparation correspondence . 


 A.R.C.O 


 R. A. MANGELSDORFF , L.R.A.M. , A.R.C.M 


 F.R.C.O 


 ROMPTON ORATORY.—WANTED , BASS 


 XUM 


 N.W 


 15 GUINEAS.—PIANO , " DUCHESS " MODEL 


 SWANTON £ 50 challenge 


 unaccepted 


 dainty brochure 


 asking . 


 water , GAS , electricity 


 SWANTON WATER ENGINE , 


 SWANTON ECONOMIZER 


 COMPOUND CENTRIFUGAL ORGAN 


 BLOWER 


 HUGH SWANTON 


 LEADING EXPERT ORGAN BLOWING , 


 ORGAN - blowing installation operation 


 OLD FIRM 


 P. CONACHER & CO , 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD 


 gold medal 


 NICHOLSON CO . , 


 ORGAN BUILDERS 


 PALACE YARD , WORCESTER . 


 ( establish 1841 . ) 


 576 


 TRINITY COLLEGE LONDON 


 institute 1872 


 - eighth half - yearly higher examination 


 licentiate MUSIC . 


 associate MUSIC 


 preliminary certificate 


 matriculation certificate 


 certificate vocalist 


 certificate violinist 


 practical licentiate ( ORGAN 


 certificate organist 


 ASSOCIATE pianist 


 certificate pianist 


 counterpoint certificate . 


 choir training CERTIFICATE 


 number candidate , 285 ; 


 PASSES , 126 


 total number 


 limit , 


 NEW SHORT OVERSTRUNG GRAND 


 105 guinea . 


 ' " " MULTUM PARVO 


 33 , GREAT PULTENEY STREET , LONDON , W 


 QUEEN HALL 


 promenade concert 


 nightly 8 till NOVEMBER 7 . 


 QUEEN HALL ORCHESTRA 


 LONDON ORGAN SCHOOL 


 INTERNATIONAL COLLEGE MUSIC , 


 BOSWORTH EDITION 


 ALEX . C. MACKENZIE 


 OPERA act 


 cricket HEARTH 


 ORCH ESTRAL SUITE 


 LONDON day day 


 A. J. RANDEGGER 


 WERTHER SHADOW . 


 OSCAR BERINGER 


 NEW PIANOFORTE TUTOR 


 BOSWORTH CO.,5 , PRINCES ST . , OXFORD ST . , W. 


 XUM 


 MUSICAL TIMES 


 SINGING - CLASS circular . 


 SEPTEMBER 1 , 1902 


 coronation 


 KING EDWARD SEVENTH 


 QUEEN ALEXANDRA 


 COLLEGIATE church 


 ST . PETER WESTMINSTER , 


 YUM 


 play 


 coronation 


 play coronation 


 PREFACE 


 XUM 


 WIAA 


 MUSICAL TIM 


 ES.—SEPTEMBER , 1902 . 579 


 BRID¢ 


 MUSIC 


 SIR FREDERICK 


 DIRECTOR 


 ORGANIST 


 WESTMINSTER 


 SS 


 3E , MUS . DOC . , M.V.O. 


 coronation , 


 ABBEY 


 


 685 . 


 589 , 


 CHORAL REHEARSAL CORONATION MUSIC , 


 GREAT hall 


 CHURCH HOUSE , WESTMINSTER , JUNE 23 , 1902 


 582 


 XUM 


 XUM 


 7 , 


 7OUG 


 w/ ( ) u 


 reduce FACSIMILE cover official book 


 CORONATION SERVICE MUSIC 


 specially dfsigne MR . JOHN R. CLAYTON , FIRM MESSRS . CLAYTON BELL , 


 GLASS painter KING 


 584 


 CORONATION ORCHESTRA 


 KING band . 


 clarinet . 


 player 


 violin . 


 FANFARE trumpeter . 


 CORONATION CHOIR 


 second violin . 


 contrabass . 


 flute . 


 oboe . 


 player 


 violin . 


 SECOND violin . 


 VIOLAS . 


 VIOLONCELLOS 


 bassoon . 


 HORNS 


 trumpet . 


 TROMBONE . 


 TIMPANI . 


 EXTRA drum . 


 HARP 


 SECRETARY LIBRARIAN 


 KING BAND . 


 CONTRABASSES . 


 PICCOLO . 


 HORN . 


 trumpet . 


 trombone . 


 CONTRA TROMBONE . 


 TUBA 


 EXTRA drum 


 XUM 


 585 


 ALTOS 


 tenor 


 XUM 


 M.P 


 BASSES 


 586 


 C.B 


 HORATIO PARKER 


 XUM 


 XUM 


 587 


 gift mother 


 PARKER 


 MRS . ISABELLA G. 


 nature memory 


 sudden awakening 


 LATE MR . CHAKLES EDWARD PARKER 


 


 SEAL YALE UNIVERSITY 


 RHEINBERGER favourite pupil _ 

 german critic . Munich , Parker 
 acquaintance Franz Lachner , intimate 
 friend Schubert fascinating old man , 
 Lachner journey Munich Vienna ona 
 raft penniless student , 
 year 1825 . warm personal 
 friend Schubert , walk 
 , carry torch , funeral 
 Beethoven . Schubert die shortly 
 interesting episode 

 good work NEW YORK BOSTON 




 YALE university 


 xum 


 1893 


 1701 , 


 wim 


 589 


 curious epitaph 


 UNIVERSITY orchestra 


 professorial activity 


 horatio 


 parker 


 childhood 


 59 


 composition 


 OP 


 OY ANEWD 


 op 


 44 


 5i 


 NEW ORGAN concerto 


 HOBBIES 


 XUM 


 ENGLAND english musician 


 house AUBURNDALE , MASSACHUSETTS , professor HORATIO PARKER bear 


 personality 


 592 


 HANDEL MESSIAH : 


 preface NEW edition . 


 ii 


 22 


 xum 


 41 


 594 


 SS SS 


 HAPPY return day 


 2,000 


 595 


 bruge ' SCHOLA cantorum . ' 


 note tour 


 corner BRUGES 


 LE QUAI DU ROSAIRE ET LE BEFFROI 


 yum 


 596 


 xum 


 147 ) . 


 1 1894 


 597the lecture - debate ( different person step 
 platform ) note . M. Bordes 
 act showman illustration . Baron Henri 
 Cochin , deputé du Nord ( ' French Flanders ' ) , 
 eloquent harangue ' l’ame flamande . ' 
 Mgr . Foucault , Bishop St. Dié , able 
 prelate , suave tenor voice , combination 
 beguile mathematical theory 
 plain - chant consistent elastic 
 character . M. Gastoué , second conductor , support 
 chief . Dom Gatard Farnborough 
 , , England . 
 Dom Laurent Janssens , Rector Benedictine 
 University St. Anselmo , Rome , apparently 
 Cardinal hat far distant , speak 
 astonishing fervour Flemish French . 
 Dom Pothier mention 
 carry practical plain - chant question 
 pocket . Mr. R. R. Terry Westminster speak 
 manfully ( English ) english early church 
 style . M. Tinel , composer , Principal Malines 
 Conservatoire , Professor Brussels Conservatoire , 
 expect succeed aged Gevaert , 
 lecture applaud . Pére Villetard , 
 curé Serigny , remark Rassegna Gregoriana 
 newspaper Rome 

 music ' Chanteurs ' ( supplement 
 local chorus orchestra ) plenitude , old 
 new . unaccompanied gregorian Alleluias 
 Graduals ( equal voice mix ) revelation 
 effect expression . manner conduct 
 wave hand curious . music- 
 style illustrate franco - flemish , Franco- 
 Walloon , french , german , italian , spanish . 
 concert ' Un Jubé Modéle , ' 
 specimen organ - loft ( congregational plain- 
 chant ) performance ; Jubé rood - screen . 
 abundance Bach . Beethoven ' Elegischer 
 Gesang ' ( op . 118 ) , memory Baroness 
 Pasqualati , die 1811 , voice string , 
 great effect . Franck oratorio ' redemption ' 
 ( 1871 ) exciting work , inspired soprano 
 solo strong rhythmic chorus . [ England 
 welcome warm style , ' beatitude ' 
 October Cardiff . Franck 
 mysticism Elgar , superficial , 
 walk confined harmonic sphere . 
 plaintive melodious , occasionally 
 Walloon joyous lilt . W. W. Cobbett lecture 
 Musical Association January , 1901 , 

 598 




 CHARLES MACLEAN 


 BAND 


 XUMMonpay ( September 8).—rehearsal Cathedral 
 Public Hall 

 Tuespay ( September 9).—morning , 11.30 : National 
 Anthem , arrange Elgar ; ' Elijah , ' Mendelssohn . 
 evening , 7.30 : Coronation March ( Op . 21 ) , Percy 
 Pitt ; ' king shall rejoice , ' Handel ; Symphony 
 C minor , Beethoven ; * + ' song Deborah 
 Barak , ' Hugh Blair 

 WepneEspbay ( September 10 ) . — morning , 11.30 : prelude 




 PRINCIPAL vocalist 


 SCHUBERT LINCOLN INN field 


 600the replica portrait Sir John Stainer Professor 
 Herkomer view Lord Mayor Vestry , 
 St. Paul Cathedral , present month . 
 ultimate destination Music School , Oxford , 
 Stainer Memorial 

 Promenade Concerts Queen Hall 
 announce begin 23rd ult . , 
 management Mr. Robert Newman , 
 Mr. Henry J. Wood usual rostrum cop . 
 ductor orchestra . english composer 
 write work series concert : 
 Messrs. Rutland Boughton , Ernest Blake , Josef Hol- 
 brooke , Charles Maclean , Percy Pitt , 
 Miss Ethel Smyth represent dance 
 music arrange opera ' Der Wald , ' 
 interest musician amateur 
 announcement successive Tuesdays 
 Schubert symphony , entire 
 orchestral work Brahms , play ; 
 Wednesdays symphony Tschaikowsky 
 form chief feature bill fare ; 
 Beethoven ' immortal , ' Fridays , 
 sure attract large audience . thirty- 
 year ago — 1870 , centenary great 
 composer birth — Beethoven 
 symphony successive Saturdays 
 Crystal Palace , year later ( 1881 ) 
 Schubert symphony similarly set 
 habitué Sydenham concert - room . 
 venture think Tuesday Friday 
 evening Queen Hall music - making 
 popular good sense term . Mr. Newman 
 announce production novelty 

 splendid series concert recently 
 New Brighton Tower deserve record 
 high commendation . weekly programme 
 great interest sterling merit — 
 devote entirely work british com- 
 poser , J. C. Bridge , Cowen , 
 Elgar , Mackenzie , Parry , Somervell , Stanford 
 Sullivan figure scheme . occasion , 
 Mr. Granville Bantock , initiate concert , 
 conduct composition — ' Eugene 
 Aram ' overture , ' Songs Persia , ' ' Dante ' 
 Symphonic Poem — addition Mr. Josef 
 Holbrooke tone - poem , ' skeleton Armour . 
 Mr. A. E. Rodewald , honorary musical director , 
 heartily congratulate result 
 artistic enthusiasm 

 complete conquest haughty beauty 

 Beethoven 

 ' haughty beauty ' compose _ 
 Coriolan Overture 




 XUM 


 1581 ) 


 FP . W. 


 FHE TUNI 


 ABRIDGE 


 collection PSALM TUNES 


 602 


 S. S. WESLEY cree E 


 boy 


 ORGAN recital 


 organist choirmaster , choir appointment 


 ent 


 BARITONE SOLO . 


 XUM 


 - song 


 9 - 2 _ f _ ; 


 TENOR . + = — 


 — — » se @ 


 LAUGH 


 LOVING 


 SE 


 SS 


 TORE 1 G SESS 


 XUM 


 ( 


 


 XUM 


 — - — — — _ — — — _ cg — $ — $ _ _ _ 


 SS 


 VIOLIN MUSIC 


 WAGNER BAYREUTH MUNICH . 


 ( SPECIAL CORRESPONDENT 


 O12 


 MUNICH 


 HERBERT THOMPSON 


 XUM 


 2 2.43 


 MUSICAL TIMTWO interest festival . 
 HOVINGHAM 

 Musical Festival beautiful village 
 Hovingham , Yorkshire , year past 
 highly artistic function , zealous enthu- 
 siastic direction Canon Pemberton . 
 place 7th 8th ult . exception rule . 
 day Sir Hubert Parry fine oratorio , 
 ' Judith ' perform manner 
 doubtless gratify composer — choir , orchestra , 
 conductor alike enter dramatic spirit 
 music , solo vocalist , Miss Agnes Nicholls , 
 Mrs. Burrell , Mr. William Green , admirable 
 exponent respective . novelty 
 Festival Dr. Wood ' Song tempest , ' 
 8th ult . , work breadth dignity . 
 follow special event meeting , 
 performance Beethoven Violin Concerto , solo 
 interpret Dr. Joachim — manner 
 needless state , furth great violinist 
 good form ably support 
 orchestra . evening concert , terminate 
 Festival , include Bach Cantata , ' God time 
 good , " Schumann Pianoforte Concerto ( Miss Fanny 
 Davies , soloist ) , Mozart E flat Symphony , Bach 
 5th Concerto D major , Miss Davies play 
 important pianoforte , sub- 
 ordinate violin flute play 
 Dr. Joachim Mr. Eli Hudson . hope 
 Canon Pemberton able continue 
 direction Festivals , notwithstanding removal 
 locality 

 NATIONAL co - operative FESTIVAL 
 CRYSTAL PALACE 

 correspondence . MATLOCK BANK . Miss Lucie Johnstone suc- 
 cessful concert Victoria Hall , 21st ult . , 
 GEORGE SMART . : assist Madame Adelaide Mullen , Mr , 
 et : f Saye \ _ | Henry Beaumont , Mr. Hulbert L. Fulkerson ( vocalist ) 
 six,—the notice Henry Smart number ! ang Miss Ethel Knapp ( pianoforte ) . Mr. Fulkerson 
 suggest question certain George Smart , treasurer | -ave music - recitation entitle ' Te Deum ' ( Stanley 
 sii founder New Musical Fund , Hawley music ) powerful effect , Miss Lucie 
 establish in1786 . diedin Edinburgh September 4 , | Johnstone render song artistic charm . Miss 
 1818 , age - seven , bury St. james’s| Gertie de S. Webster accompany , Miss Ethel 
 Church , Hampstead Koad , , — Knapp pianoforte solo appreciate . 
 ' restorer ' work — tablet 
 memory . father Sir George Smart ? MerLbovurne ( Australia).—The programme North 
 _ look George Smart , an| Suburban Choral Union Concert June 10 , 
 inventor , patent 1800 | Masonic Hall , Ascot Vale , include Cowen cantata 
 1822 . wasa timber merchant , carry business , | « St. John Eve , ' Macfarren - song ' break , break , 
 Camden Town Ordnance Wharf , | cold grey stone , o sea , ' Mendelssohn ' son 
 te } ~4 ar } sy . , je ' p . 
 Westminster Bridge . ' strange ' overture , Strauss concert waltz ' Morning 
 letter come notice member | | eaves ' ( time ) 
 . . . - = o 
 Smart family , able throw|__the inclusion apparently 
 light question . eee remark Mvusicat Times January , apropos 
 sR ; R.B.P. | introduction good light music classical 
 think George Smart , . 1 , refer our| concert programme . solo vocalist Miss Doris 
 correspondent , grandfather Henry Smart,|Carter , Miss Florrie Gordon , Mr. G. Baulch , 
 composer.—ep . W.T. Mr. T. H. Lightfoot . Mr. E. A. Jager conduct 

 WELLINGTON ( NEW ZEALAND).—The Orchestral 
 WORDS , word , WOKDS ! society concert twelfth season 
 Opera House , June 18 , direction Mr. 
 singer intelligible ? listen singer , | Robert Parker . Mozart ' Jupiter ' Symphony receive 
 suppose rank , find extremely | admirable interpretation , Finale 
 difficult accurately determine language , | especially noteworthy . item excellent 
 language , vocal effort pre-| programme Mendelssohn ' Ruy Blas ' overture , 
 sente audience . doubt ] movement composer violin concerto 
 librettist composer answer for| ( admirably play Miss Grace Kennedy ) , Edward 
 shortcoming , handicap | German dance Henry VIII . , Schubert 
 vocalist handiwork . , ' Rosamunde ' Ballet Music . Miss Phoebe Parsons 
 singer culprit . 1 afraid | vocalist —— Mr. Maughan Barnett ( pianoforte ) 
 bad bad . ' national defect , ' | Herr Max Hoppe ( violin ) , assistance Mr. A. 
 inclined retort . truth , | Hamerton ( violoncello ) , concert July 7 , 
 justification observation . day , | programmeinclude Kheinberger Trio ( Op . 112 ) , 
 come , walk main|Gade Novelletten ( Op . 20 ) , soli 
 thoroughfare highly respectable neighbourhood , | respective instrument , - artist . Miss 
 overhear remark respectably dressed| Amy Murphy vocalist —— July 15 , Mr 
 person : ' hav’nt ocean head . ' think } Maughan Barnett pianoforte recital , principal 
 : ' , certainly ] item programme Beethoven ' sonata 
 case water brain,’—Yours reflectively , pathetic , ' group piece Chopin , include 
 PLAIN SPOKEN . Polonaise flat , Rubinstein Staccato Study C 

 second Rhapsodie Hongroise Liszt 




 ) . 112 ) , 


 MEF 


 MUSICAL TIMTHEORY.—You supply 
 book Theory Music Examination . 
 read theoretical article Grove ' Dic- 
 tionary Music , ' especially form , Sir Hubert 
 Parry , Bart . Harmony Course Bridge 
 Sawyer help far 

 W. P.—(1 ) know book ' arrange 
 score . ' matter practical experience 
 study good model . ( 2 ) Beethoven Op . 63 64 
 arrangement Op . 4 3 — 
 pianoforte , violin violoncello , second pianoforte 
 violoncello 

 CoLoniaL.—You right regard 
 ambiguity compound time signature ' unsatisfac- 
 tory . suggestion amendment , 
 altogether novel , ingenious . simply 
 impossible change likely 
 generally accept country , , , 




 dure month . 


 b 


 heory question . . 


 SEPTEMBER number contain follow music : — ( SOPRANO ) 


 6 " MADAME TERA LAW M 


 BUTTON 


 P 


 ENS 


 ALTO 


 LANE 


 617 


 CHURCH MUSIC 


 anthem 


 - song 


 CORONATION CHORAL MARCH 


 JESU , THOUGHT THEE 


 compose RY 


 ERNEST EDWIN MITCHELL 


 MUSIC 


 AMBROSE ABBOTT CO . , 


 popular voluntary . 


 attractive anthem 


 compose PY 


 EDWYN A. CLARE . 


 LLISTON " organ tuning . " 


 new valuable work POPU LAR HARVES ST anthem 


 musical examination . 


 E. A. SYDENHAM 


 , easy , effective 


 OU estion & SW e rs | | se unto LORD . 


 GREAT LORD 


 preface ly 


 effective HARVEST - tide 


 FUGAL analysis 


 HARVEST thanksgiving HYMN =| _ JOSEPH H. ADAMS . 


 ‘ IMANT , 


 trinitytide 


 world 


 anthem 


 light 


 FINAL choru 


 compose PY 


 EDWARD ELGAR 


 BEHOLD , GOD great 


 E. W. NAYLOR 


 O joyful light 


 compose 


 BERTHOLD TOURS 


 ALPHA OMEGA 


 compose 


 J. VARLEY ROBERTS 


 complete 


 EIS 


 NOVELL 


 cantione SACR 


 musical setting 


 ROMAN liturgy 


 MONK order ST . BENEDICT 


 TA ET VENE RABIL S 


 14 . response mass FICE 


 26 . BENEDIC 


 35 . SALVATOR MUNDI 


 13 . SALVE , REGINA 


 5 . SALVE , REGINA .. 


 620 


 RAINBOW PEACE 


 EASY HARVEST - TIDE CANTATA 


 HARVEST song 


 SEED - time HARVEST 


 HARVEST CANTATA 


 HARVEST - TIDE 


 JUBILEE CANTATA 


 GLEANER HARVEST 


 hymn HARVEST 


 AUTUMN STREWS EV’RY 


 PLAIN 


 LET brethren join 


 


 sowing reap 


 CHORUS dance REAPE 


 GLEANERS 


 CAST LOAD 


 HARVEST FEAST 


 HARVEST dance 


 HARVEST thanksgiving MARCH 


 JOY HARVEST 


 sower forth sow 


 MELODY 


 heart 


 SOWETH 


 O LORD HEAVEN , EARTH 


 SEA 


 HARVEST FESTIVAL book 


 contain TALLIS prece response , canticle special psalm 


 point chanting SET new appropriate chant 


 SIR J. BARNBY , MYLES B. FOSTER , SIR A. C. MACKENZIE , SIR J. STAINER 


 


 


 ‘ new MN tune 


 new HYMN tune 


 compose expressly 


 


 NOVELLO , EWER CO . , NEW YORK 


 , 1902 


 complete list 


 TE 


 DEUM LAUDAMUS 


 TE 


 


 COMPANY 


 DEU 


 LIMITED 


 H. W 


 F. A. V 


 A.W 


 MI LAU DAMUS 


 D 


 voice instrument 


 service B FLAT 


 ST . CECILIA ’ DAY , 1694 


 compose RY 


 : ~~ — 


 - _ — LATIN arrangement R. TERRY . 


 NOVELLO HARVEST anthem 


 publish : ' DAY 


 manual PLAINSONG 


 DIVINE service 


 CONTAINING 


 canticle note 


 psalter note 


 litany response 


 new edition 


 prepare 


 GENERAL superintendence 


 JOHN STAINER 


 PREVFACE 


 village tune book 


 hundre popular HYMN tune 


 extract PREFACE 


 OFFICIAL edition 


 print order majesty stationery office 


 


 form order 


 


 service perform ceremony observe 


 coronation 


 majesty 


 KING EDWARD VII 


 QUEEN ALEXANDRA 


 ABBEY CHURCH S. PETER , WESTMINSTER 


 MUSIC SUNG 


 LEATHER GILT , seven shilling sixpence . 


 EDITION DE LUXE 


 SUNG LICHFIELD CATHEDRAL 


 popular CHU RCH music edit 


 


 4 = word 


 military ARTHUR C. BENSON 


 CHURCH PARADE service B ARTHUR M. GOODHART . 


 consisting 


 selection hymn open - air service t 


 compile arrange PY R U 7 H 


 THOMAS CONWAY BROWN , : 


 content . suitable use place WORSHII 


 suitable 


 thanksgiving service 


 XUM 


 


 625 


 WORCESTER FESTIVAL 


 novelty 


 TEMPLE 


 oratorio 


 word select bible 


 SET music 


 SOPRANO , TENOR , BARITONE SOLI , CHORUS 


 ORCHESTRA ORGAN 


 H. WALFORD DAVIES . 


 ( op . 14 


 song DEBORAH 


 BARAK 


 SHORT ORATORIO 


 SOPRANO BARITONE SOL ] , 


 ORCHESTRA 


 compose PY 


 HUGH BLAIR 


 CHORUS 


 O PRAISE LORD 


 ( dedication ode ) 


 ORCHESTRA CHORUS 


 compose 


 A. HERBERT BREWER 


 LORD SUN 


 SHIELD 


 CANTATA 


 ALTO , BASS SOLI 


 ORCHESTRA 


 


 SOPRANO , CHORUS , 


 J. S. BACH 


 publish 


 god save king 


 origin history 


 MUSIC word 


 NATIONAL ANTHEM 


 WILLIAM H. CUMMINGS 


 NOVELLO COMPANY 


 GRAUN , C. H 


 perform 


 WORCESTER MUSICAL FESTIVAL , 


 


 GERONTIUS 


 ELGAR 


 38 


 dream 


 EDWARD 


 ST . CHRISTOPHER 


 HORATIO W 


 PARKER 


 STABAT MATER 


 A. DVORAK . 


 society 


 CHORAL 


 NOVELLO CHEAP edition 


 chorus onlycantata , ORATORIOS , opera 

 s. d. 
 BACH . — " Passion " ( St. Matthew ) oO 
 BEETHOVEN.—- * * Mount Olives AP aper vheeei . ) 0 6 

 Tonic Sol - fa ) o 6 




 produce NORWICH MUSICAL 


 FESTIVAL 


 CORONATION ODE 


 word 


 CANTATAS . 


 music ; 


 music compose ? 


 ' calm , beautiful " — _ — _ — — 2 


 STRATHEARN SERIES - MUSIC 54 


 holyrood SE rie S album organ 


 XUM 


 627 


 NEW edition 


 handbook 


 examination 


 600 question 


 ERNEST A. DICKS 


 MUSIC 


 answer 


 preface revise edition 


 NOVELLO ’ 


 edit 


 S HUBERT H 


 recent number 


 B. W 


 B.WFreel Taylor 2 0 

 54 . sonata form .. V.H. Hadow 2 6 
 55 . Dictionary V iolin Makers ; . C. Stainer 2 6 
 analysis Bach 48 prelude Fu ; gues ( , 
 art , . ) ... F. lliffe 3 0 
 7 50 question form tonality Beethoven 
 pianoforte Sonatas ( appendix ' tlysis hy form " ' ) 
 H. A. Harding o 6 

 58 . Steps harmonisation melody 




 new 


 foreign publication 


 628 MUSICAL tin 


 CANT 


 UNA 


 sixth SEVENTH thousand . 


 produce NORWICH MUSICAL FESTIVAL , 


 OCTOBER 4 , 1893 . 


 virgin 


 produce 


 BIRMINGHAM 


 UTMOST 


 FESTIVAL 


 JOAN 


 success 


 CHORAL 


 


 SOCIETY 


 ARC 


 SEVENTH thousand 


 _ wilderness 


 1592 


 NOVELLO 


 5S 


 SCHOOL music 


 ELFIN HILL 


 FAIRY OPERETTA 


 foiler DEEP 


 CANTATA 


 FEMALE voice 


 legend 


 WOOD 


 JUVENILE OPERETTA 


 


 month 


 vocal duet school 


 use 


 VOCAL trio school use . 


 BIKDS 


 D COMPANY 


 LIMITED 


 THEI , 1902 . e29 

 symphony BEETHOVEN 

 perform QUEEN HALL promenade concert 




 dure season . edition 

 BEETHOVEN 

 symphony 




 GEORGE 


 REVISED 


 GROV S , GBPRICE , CLOTH , GILT , shilling 
 recognise , small hesitation , 
 impor tant valuable recent contribution musical litera- 
 ure inform professional musician learn 

 great deal master - work Beethoven Sir George 
 Grove , wide reading acute perceptiveness enable 
 lim marshal astonishing array fact , intimate 
 acquaintance spirit master qualify throw 
 ight page , , obscure . . 
 satisfied remark , earnestly recommend 
 vho recognise Beethoven greatness immortal 
 Symphonies obtain Sir George Grove volume , walk 
 uminous path ready conduct trust 
 s guidance . ”’-—Daily Telegraph 

 London : NoveELLo Company , Limited 




 selected song 


 JOHANNES | BRAHMS 


 BOOK viii . 


 content : 


 british king 


 MENDELSSOHN ' ' ELIJAH ’' perform 


 approaching festival WORCESTER , PRESTON , 


 SCARBOROUGH , SHEFFIELD , BRISTOL , CARDIFF , 


 NORWICH 


 second edition 


 history 


 MENDELSSOHN oratorio 


 ELIJAH 


 F.G. E DW ARDS . 


 introduction SIRGEORGE GROVE , C B 


 PRELUDE 


 high low voice | EDW ARD ELGAR . 


 SONG 


 word write 


 HAROLD BEGBIE 


 music compose EY 


 ALICIA ADELAIDE NEEDHAM . | 


 seven 


 pull thread 


 verse write 


 W. B. YEATS 


 music compose 


 EDWARD ELGAR 


 NOVELLO company 


 RES spon ES 


 lady ’ voice 


 


 A. HERBERT BREWER 


 VERS sicle 


 boy 


 PREFACE 


 A. HERPERT BREWER . 


 HOLY communion | DRAMATIC cantata 


 unclude BENEDICTUS AGNUS DE ] | JAMES SMIETON , M.A. 


 SUITE D MINOR 


 


 SONG | author 


 1rom 


 ‘ RECOLEECTIONS : CHIEDHOOD " | sixth edition . 


 solo voice , : CHORUS , 


 music compose | 


 PERCY E.-PEETCHER : publish 


 DRAMATIC CANTATA 


 


 OPERETTA act young PEOPLE 


 ARTHUR SOMERV ELE 


 XUM 


 ere 


 30 


 , 1902 . 6 


 BAC 


 MUS 


 A. CHALLINOKR 


 A.R.C.M 


 CANTATAS 


 JUDAH 


 BABYLON . 


 SMITH 


 BETHANY . 


 ALEXR . ROBERTS 


 GARDENS LORD . 


 SACRED CANTATA 


 ALICE G. DYER 


 


 word write 


 LONDON 


 BAYLEY FERGUSON 


 JOURNEY LIFE . 


 SACRED CANTATA young PEOPLE . 


 song 


 ODE 


 write HENRY BRANC 


 SEA 


 GREAT MARLBOROUGH STREET , W 


 chorus 


 dream GERONTIUS 


 EDWARD ELGAR 


 OP . 38 


 


 2 , merciful , gracious . 


 4 . finale : 


 tune 


 use school child festival . 


 MUSK 


 compose 


 


 child use 


 music compose PY 


 NOVELLO edition . 


 PIANOFORTE . SINGING . MOoSCHELES . study C , Op . 70 , . 1 ( Franklin Taylor ( arm ! ) 12 song tenor 

 study , book 2 " : ; 1 0 | handel Gorgias Handel Oratorios 2 
 BEETHOVEN . seat ( F ‘ inale ) , ' sonata b flat , Op . 22 ( sound alarm ) ( edit A. Randegger 

 sonata , . 11 ) . ' ; .. 3 0 | Mozart.—-Dalla sua pace ( contentment ) . Tenor Songs 
 Czerny.—Study E , oe . 834 , . 25 ( Franklin Taylor * * Don Giovanni , ’' Book iii . edit A. Randegger ... 2 

 Studies , book 15 ) . roo sITONE 
 BEETHOVEN.—Adagio , Sonata nc minor , op . 13 ( Sons tas . BARITONE 

 a8 .. 2 6 } PANSERON.—42 exercise Baritone Bass . 1 , . 2 , 
 ve canis aes — _ aie ese 3 10 ( edit A. Randegger ) oe 
 SC hool examination . elementary , 12 song ait 
 Czerny.—Study D , op . 599 , . 65 ( Franklin Taylor aa feel deity within§ tone , Handel ; 
 Studies , book 28 ) ... pee ae ee ae ae | ' " arm , arm , ye brave ... oratorios . ( edit ) . 
 SCHOOL EXAMINATIONS.—LOWER division . _ \ a. Randegger ) 
 Bacu.—Invention , . 8 , o Ors . : BASS . , B 
 BreETHOVEN.—Tempo di Menuetto , Sonata , op . 49 , , . 2 Coxconer.—4o Lessons Bass Baritone . . 9 , 20 

 7 y TAUBERT , W.—Good night , Recollections Childhood . ex 
 \ IOLIN . . 4 ( edit A. Randegger ) ... oe es ce entr 
 LOCAL CENTRE — J UNIORS . CONTRALTO . Pri 

 nt , J.—Zwanzig Fortschreitende Uebungen , op . 38 , heft 2 . 3 0 | PANSERON.—42 Vocal exercise Contralto , I. , . 15 - 
 BEETHOVEN.—Rondo G major Violin Pianoforte 17 ( edit A. Randegger ) ... ius oe ion 

 Peters ) ... ee ik ee ae res vo ses wk TENOI 
 c cea — TENOR . 
 LOCAL CENTRE — senior . ae pr 
 ia = : e HANvEL.—Let ine wander unseen ( L'Allegro ) ... 1 0 
 Rarr.—Tarantelle , Morceaux de Salon ( Pianoforte SA et 




 school EXAMINATIONS.—HIGHER division . BASS .