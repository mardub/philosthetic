


 CA 


 the MUSICAL time 


 OCTOBER 1 , 1870 


 EST HACKNEY CHURCH . — TREBLE 


 HANDEL ’S 


 CHANDOS ANTHEMS 


 LONDON : NOVELLO , EWER & CO . , 


 1 , BERNERS STREET ( W 


 professional notice . 


 MADAME MONTSERRAT 


 MR . J. TILLEARD , 


 MR . C. 8S. JEKYLL 


 USIC engrave , print , and PUB 


 lishe in the good style , and on moderate 


 usical instrument for VOLUN- 


 EYOND ALLCOMPETITION.—T. R. WILLIS , 


 AND W. SNELL ’s improve harmo 


 MR . C. J. BISHENDEN , 


 RASS , REED , string , and DRUM and 


 armonium ! harmonium ! 


 W. HATTERSLEY , 


 HARMONIUM MANUFACTURER , 


 95 , MEADOW - STREET , SHEFFIELD . 


 EW PIANOFORTE 


 of moderate difficulty . 


 MUSIC 


 PIANOFORTE piece moderately easy . 


 NEW PIANOFORTE DUET . 


 NEW song by HATTON . 


 bateful — COMFORTIN G 


 the european psalmist 


 to be publish by subscription 


 morning and evening 


 by 


 SAMUEL SEBASTIAN WESLEY 


 price to subscriber 


 also , to be publish by subscription , by the same author 


 _ _ _ _ _ _ _ _ 


 the 


 OF 


 concert music , 


 with PIANOFORTE accompaniment , 


 edit by 


 JOHN HULLAH 


 LONDON : ASHDOWN and PARRY , 


 HANOVER SQUARE . 


 BREAVINCTON and son , 


 84 , NEW BOND STREET , and ROYAL POLYTECHNIC 


 agent 


 FORSTER and ANDREWS 


 FOR 


 PIPE ORGANS 


 MASON and HAMLIN ’s 


 gold medal FREE REED & CABINET organ 


 SMITH ’s AMERICAN ORGANS 


 chickering and son 


 GRAND pianoforte 


 award 76 gold and silver medal 


 BREAVINGTON and son 


 TI 


 ELEME 


 character represent . 


 act i 


 act ii . 


 time of performance one hour and ten minute 


 W. H. BIRCH , 104 , LONDON STREET , read 


 now READY , SECOND EDITION . 


 A singing CARD 


 for the use of choir , choirmaster , school , 


 METZLER and CO . ’s 


 new publication 


 WAR song of FRANCE and GERMANY 


 J. T , TREKELL . 


 I. LIEBICH . 


 NHE MESSAGE from the BATTLE field . 


 ETZLER and CO . ” ’s NEW HARMONIUM 


 NEW COMIC opera by CH . GOUNOD . 


 ETZLER and CO . ’S SHILLING OPERA 


 37 , GREAT MARLBOROUGH STREET 


 RIXTON CHORAL SOCIETY , 


 ANGELL TOWN INSTITUTION . 


 MR . H. ASHTON 


 MR . T. PEARSON 


 MR . J. F. MEEN 


 MR . J. RUDKIN , RAM . 


 assist by 


 MISS FANNY DANIELSON 


 HE LONDON ORPHEUS QUARTETT 


 TOKE NEWINGTON ASSEMBLY room 


 OTTINGHAM MECHANICS ’ institution 


 SATURDAY popular concert 


 SOLO instrumentalist or vocalist 


 ONCE 1 


 the MUSICAL TIMES , 


 OCTOBER 1 , 1870 


 the BIRMINGHAM MUSICAL FESTIVAL . 


 616 


 _ _ ae of the Ball - room than the Concert - room . but Mr , 
 ce of thy | Sullivan ’s theme be so melodious and instinct with 
 enough , | tefined feeling , his instrumentation so graceful and 
 cores in | ingenious , and his treatment of the subject so 
 ongratn . thoroughly musician - like , that his composition ap- 
 mt I be als as much to the educated as to the uneducated 
 audience | ear , and the applause which it receive be general 

 g of this | and spontaneous . it may be a question whether , if 
 Madi . | Mr. Sullivan could not be request to furnish a 
 nguishel | high class of work , he should not have be pass 
 otte tof over altogether until a more fitting opportunity pre- 
 singing sente itself ; but , having receive the commission , 
 4 , " with } weare glad to find that the composer , rely upon 
 e mostly f his already well - earn reputation , not only accept 
 hich wef it , but bring his good power to bear upon the 
 , even of execution of the task . ' the second part of the con- 
 m want } cert consist of an interesting selection from the 
 too , be work of Beethoven , in which , to the astonishment 
 > instr . | of all , Madlle . Ilma de Murska be cast for the 
 the con | Scena " ah ! perfido , " which however she sing very 
 or ista } much well than could have be expect . we 
 he trio , | think it 2 great pity that the whole of the piece 
 ‘ od , who | from " Fidelio , " conclude with the matchless 
 ve have | finale , be not place together , as some kind of 
 resident continuity would then have be establish in a 
 nducte | programme which have the effect of having be 
 applause throw together by accident . amongst the vocal 
 | retire § success we must mention Madlle . Tietjens ’s " Qual 
 n of his { furor , " Mr. Sims Reeves ’s ' ' Adelaide " ( with Madame 
 Arabella Goddard ’s pianoforte accompaniment ) and 
 Madlle . Drasdil ’s " in questa tomba . " the great 

 ng com- elie ° 
 ‘ Shakes . composer 's pianoforte music be worthily represent 
 9 music by the " Emperor " Concerto , in e flat , ( play 

 618 the MUSICAL TIMES.—Ocroszr 1 , 1870 . ag 

 Birmi 
 short orchestral movement , be carry with good|on the Pianoforte ; but it have a strange effect to ge a det 
 effect through a portion of the tenor solo , ' ' Oh|Mr . Benedict , Signor Randegger , Mr. Sullivan , ang alone 
 peace divine . " mention must also be make of the|even another accompanist , to we , at least , a stran work 
 manner in which the orchestra introduce and accom-|come in to perform this responsible duty , as if by with 
 panie the dramatic solo for Damayanti , ' " ' a tremor|accident . surely at a great Festival like this , where the 1 
 thro ’ the air be steal ; " and the love - duet which / everything be usually so excellently organise , a dram : 
 succeed this derive much of its beauty from the| artist should be definitely appoint for this offic exces 
 masterly treatment of the instrumentation . the / as at the London concert , and his name print ip ment 
 Finale , which be commence by a bold orchestral| the programme . effect 
 prelude , may be also cite as an excellent instance ) it be natural that the performance on Friday « the 
 of the power of the composer over all the modern re-| morning should attract a large audience ; for " § ¢ , mene 
 source of instrumentation , the combination through- | Peter , " an Oratorio write expressly for the Festival nse 
 out this movement , indeed , show not only the by Jules Benedict , be produce ; and the career of modu 
 result of earnest study , but of long practical ex-|its composer in this country have be of a nature + lieh 
 perience . no praise can be too great for Miss Edith ! to ensure the utmost amount of interest in a work Lord 
 Wynne , who , in the character of Damayanti , gave| which have a right to be regard as contain his of be 
 the whole of the trying music with an accuracy of| maturest thought . it be well know that " $ f , ( repe 
 intonation , arefinemert of expression , and a dramatic Peter " be a subject which Mendelssohn once con . miter 
 power which charm every hearer , and so delight ’ ceive the idea of take for an Oratorio ; and inone I 
 the composer , who conduct his work , that , as if to ! of his letter to Pastor Schubring he discuss the sha 
 show how much his success depend upon this| matter , and express his doubt whether the charac . beaut 
 clever vocalist , he lead she forward , to share the ter of St. Peter could be make of sufficient importance deve ! 
 applause which await he at the conclusion of the for a sacred work of this character , as where our ame 
 performance . Mr. Cummings be also entitle to the Lord appear , he must of necessity , he say , " lay ee 
 utmost praise for the manner in which he sing the claim to the chief interest . " the libretto supply I 
 tenor part , more especially as much of the music be to Mr. Benedict rather ignore , than answer , this pe 
 by no means graciously write for the voice ; and ) question ; and as the design of the author of the ae 
 Mr. Santley ( who have not a very arduous part ) be as text be express in a brief preface which accompany a 
 thoroughly efficient as the composer could desire , his| the book of word , we quote it , in explanation , re a | 
 voice and style tell with powerful effect in the grand | ' ' the subject of St. Peter might be treat in various Ay . 
 dramatic duet with Damayanti , commence with the | way for the purpose of Oratorio . within no ordi- ¥ al 
 choral movement , ' ' pow’r above . " the chorus in| nary limit , however , could all the important event oe : 
 some part betray unmistakable sign of the want ) of the Apostle ’s life , and all the significance of his sa 
 of sufticient rehearsal ; but much of the choral character and position , be illustrate . the aim of rai 
 music be extremely difficult , and great cred't be there-| the present work be very simple . it affect neither ~ .. 
 fore due to all concern for the hearty will with | to show , exclusively as such , Peter the Disciple nor _ 
 which they work to earn a warm reception for a , Peter the Apostle ; its object , moreover , be not to all a 
 composition which , as musician , they must have| treat the chief personage concern in any symbolical d va 
 felt deserve their good attention . in every respect } or representative capacity . what have be attempt i } 
 * ¢ Nala and Damayanti " be a decide success , and| be merely the illustration of a few of those occurrence 50 , 
 one which we be convince will be materially|in St. Peter ’s life which most invite musical treat- ea 
 strengthen by repeat hearing . only a few / ment , and at the same time exhibit the galilean wb 
 word be necessary on the second part of the even-| fisherman as an object of the divine regard which tod , 
 e concert , which , however , contain some highly}|so pre - eminently distinguish he . " after all , we b 
 attractive piece , foremost amongst which we must| question whether this be not the right view of the this j 
 place the overture to " William Tell , " which was|subject . it be always a difficult matter to create hand : 
 not only a fine performance , but the fine perform-| dramatic interest where there be no dramatic action ; oi ' 
 ance we have ever hear of this grand orchestral | and although it be the custom to believe that a sacred Tet } 
 prelude .. so overwhelming , indeed , be its effect ) work must necessarily derive much of its effect from whi , 
 upon the audience that it be encore with acclama-|the sympathy of the audience with the story which Th r. 
 tion ; and most person be disappoint that the | it illustrate , it become a great doubt whether any " aly 
 Allegro only be repeat , if only because it deprive | of the hearer ever do more than listen to the musi¢ , aie 
 they of again hear the exquisite violoncello play-| apart from the character of the person who uttersit . p wfe , 
 ing of Mr. Edward Howell in the opening movement . | in confirmation of this , let we mention that Elijah , " eee 
 amongst the instrumental feature , too , we must|the most dramatic of all Oratorios , be invariably he 
 mention the very fine rendering of Beethoven's|performed with such an utter disregard for the pa > 
 sonata in F , for Pianoforte and Violin , by madame| individuality of the character in the work that ay 
 Arabella Goddard and M. Sainton , which be warmly|angel and ' Jezebel be sing by one vocalist , the 7 . 
 and most deservedly applaud . the most suc-| Widow and the Boy by another ; and , even if the ih oe 
 cessful of the vocal performance be " en vain|part of the Prophet himself be divide between a 
 Jespere , " by Madame Lemmens - Sherrington , the ] two equally efficient baritone , we question whether Mada 
 ‘ ' Carneval de Venise " — brilliantly sing by Madlle . | any objection would be raise . tralt 
 Ilma de Murska , and repeat , we presume , incon-| be we merely to record the unqualified success ch 
 sequence of its enthusiastic reception at the first|of " St. Peter , " we should announce what everybody intelli 
 evening concert — Blumenthal ’s ' " requital " — excel- acquainted with its composer ’s power must have ey 
 lently render by Mr. Sims Reeves — and Sir Michael | fully anticipate ; but when , independently of its uialit 
 Costa ’s effective Trio , ' ' Vanne a colei " — give with| enthusiastic reception , we express our confident a 
 so much energy by Madlle . Tietjens , Mr. Sims| opinion that a production of sterling worth have be Mrs. 
 Reeves , and Mr. Vernon Rigby as to elicit a decisive|added to the treasure of sacred art , it become # J 11 . | 
 encore . all the vocal piece not score for the or-|matter of congratulation for the public as well as I 
 chestra at these concert be efficiently accompanied|the composer ; and to the spirited Directors of the 

 alilean 
 which 
 ill , we 
 of the 
 create 
 ction ; 
 sacred 
 t from 
 which 
 r any 
 music , 
 ‘ er it . 
 Lijah , " 
 riably 
 wr the 
 - that 
 t , the 
 if the 
 tween 
 hether 




 619 


 20 


 1870 , 


 SOF , 


 CHORALE . 


 SS 


 we WATCH’D she breathing 


 _ — — _ 


 ~ — WY 


 2 SSS SS 


 1870 


 627 


 628art rule to an attempt exemplification of they ; but 
 jit will be fortunate for the follow sonata , if its merit 
 be test by the rule alone , and not by a high standard 

 of Beethoven , it be a fault which may be easily forgiven,| Mr. Macfarren may rest assured that his rule lie too 

 and for the commission of which he may quote many 
 worthy precedent , ' there be bold and efiective writing 
 throughout this composition , which show that its author 
 have be train in a good school . the slow movement , 
 in the relative minor , have a somewhat thin eflect in 
 part ; but the subject be melodious , and the triplet pas- 
 sage form a good contrast with the placid opening . 
 there be more originality in the rondo than in any other 
 part of the work . we like the phrase in the tonic 
 minor : and the change into C and F major ( in which 
 latt key the subject be introduce ) be effective , and 
 seem to prove that the composer have write on a distinct 
 plan . it this Sonata be really Mr. Wrigley ’s first venture 




 TO CORRESPONDENTSInstitution , on Monday , the 19th ult . the principal vocalist be 
 Mrs. R. Cowley Squier , Mr. Grogan , Mr. C. R. Walton and Mr. R , 
 Andrews , all of whom be highly successful . Miss Winstanley 
 and Miss Jones lend efficient aid in some pianoforte duet with Mr. 
 R. Andrews ; and a feature of the evening be the solo piano- 
 forte playing of the concert - giver . the performance no doubt 
 realise a satisfactory sum for the fund 

 CamporyE.—A concert of sacred music be give in 
 the Assembly Rooms , on the 23rd ult . , by the Wesley Chapel Choir , 
 in aid of the chapelfund . the programme commence with the 
 ' hallelujah ' Chorus from Beethoven 's " Engedi , " and include 
 Gounod ’s ' Messe Solennelle , " selection from Handel 's " Saul , " 
 and Professor Bennett 's * * Woman of Samaria ; Mozart 's first 
 motett , ' * o God , when thou appearest , " & c. the concerted music 
 and solo be excellently perform , and the chorus be give 
 with admirable energy and precision . the principal vocalist 
 be the Misses Mitchell , Mr. Pryor and Mr. H. A. Smith . Mr , 
 G. J. Smith conduct . there be a large and appreciative 
 audience 

 Care Town , Care or Goop Hore.—The following 
 programme of Sacred Music be perform at the annual celebra- 
 tion of the Confession of Augsburg , in the Evangelican Lutheran 
 Church , on the 26th June : — morning service : — Te Deum ( Bridge- 
 water ) ; ' * o Lord , whose mercy numberless , " ' ' Saul " ( Handel ) ; 
 Anthem , " Awake ; " Air , ' ' o have I Jubal ’s lyre " ( Handel ) ; 
 Sanctus ( Haydn , in b flat ) . evening service : — Gloria ( Haydn , in 
 b flat ) ; ' * Lord , to thee each night and day " ( Handel ) ; ' * Inflam- 
 matus , " ( Rossini ) ; ' ' with verdure clothe " ( Haydn ) ; Anthem , 
 ' * Jerusalem , my glorious home " ( Mason ) . the principal solo be 
 sing by Misses Sederstrom and Wahl . Mr. J. B. Smithers pre- 
 side at the organ , and Mr. J. H. Ashley conduct 




 dure the last month , 


 OHR , FRED . N 


 the 


 LAND of PROMISE 


 AN ORATORIO , by 


 FRANCIS HOWELL 


 BIRMINGHAM FESTIVAL 


 CHORAL SOCIETY 


 EW PLAIN - SONG TE DEUM 


 edit by the 


 REV . ROBERT CORBET SINGLETON , M.A 


 FIRST WARDEN of ST . PETER 's COLLEGE , RADLEY , 


 AND 


 ORGANIST and CHOIRMASTER of YORK MINSTER 


 HE ANGLICAN CHORAL SERVICE BOOK , 


 USELEY and MONK ’s PSALTER and 


 OULE ’s collection of 527 chant , 57 


 HE PSALTER , PROPER psalm , HYMNS 


 OULE ’s DIRECTORIUM CHORI ANGLI- 


 OULE ’s DIRECTORIUM CHORI ANGLI- 


 HE ORDER FOR the HOLY COMMUNION , 


 TONIO SOL - FA EDITION 


 £ 8 , 58 


 now ready , 


 upplemental HYMN and TUNE book , 


 ERIES of MODERN KYRIES , HYMN 


 WICK 


 634 


 hem by JOHN GOS 


 33 


 will greatly rejoice in the LORD . 


 TO CHORAL SOCIETIES and NATIONAL SCHOOLS . 


 " " 10 when the morning SHINETH . " 


 hirty - six original HYMN tune , 


 authorise musical text - book at RUGBY SCHOOL . 


 he element of MUSIC systemati- 


 manual of the rudiment of MUSIC , 


 singe manual 


 he letter - note singing method . — 


 important to school and teacher . 


 new MUSICAL serial , 


 to be commence early in 1871 


 he practical choirmaster 


 Z edit by 


 prospectus 


 he development of the voice 


 ST . PETER 


 compose by 


 JULES BENEDICT 


 opinion of 


 the TIMES 


 1,998 £ 2,051 16 4 


 86417 6| 


 -- 818 15 10 


 the PRESS 


 the DAILY TELEGRAPH 


 the DAILY NEWS 


 the STANDARD 


 the MORNING POST 


 the ECHO . : 


 639 


 the ILLUSTRATED LONDON NEWS 


 the SUNDAY TIMES 


 the MANCHESTER GUARDIAN 


 the MANCHESTER EXAMINER and TIMES 


 the LIVERPOOL MERCURY 


 a new work for the ORGAN 


 CHAPPELL ’S ORGAN JOURNAL 


 consisting of favourite movement select from the 


 by the 


 most eminent organist 


 CHAPPELL & CO.S CHEAP publication 


 for various instrument 


 CHAPPELL ’s 


 instruction book 


 VIOLIN . CLARIONET . SERAPHINE ANGELICA , 


 FLUTE . | HARMONIUM . DRUM and FIFE , 


 CORNET . SINGING , VIOLONCELLO , 


 ENGLISH CONCERTINA . | HARMONY . BANJO 


 GERMAN CONCERTINA . | GUITAR . TROMBONE , 


 PIANOFORTE . SAX - HORN 


 VIOLIN 


 FLUTE 


 CORNET - A - PISTON 


 GERMAN CONCERTINA 


 ENGLISH CONCERTINA 


 HARMONIUM 


 CHAPPELL & CO . , 50 , NEW BOND STREET , LONDON 


 FS