


 MUSICAL TIMES 


 MUSIC NUMBER . 


 O LORD , GOD 


 RECONSTITUTED 1868 


 S. JAMES HALL 


 


 FOURTH SUBSCRIPTION CONCERT 


 GIVEN 


 WEDNESDAY , APRIL 21 . 


 HAYDN “ CREATION . ” ’ 


 MADAME LEMMENS - SHERRINGTON 


 MR . SIMS REEVES , 


 MR . MONTEM SMITH , 


 


 MR . LEWIS THOMAS 


 MR . JOSEPH BARNBY 


 IMMEDIATE PATRONAGE 


 H.R.H. PRINCESS CHRISTIAN 


 


 H.R.H. PRINCESS LOUISE 


 PLACE 


 TUESDAY EVENING , APRIL 


 13 . 


 HERR DEICHMANN , 


 MR . ZERBINI , 


 HERR DAUBERT , 


 


 MR . JOSEPH BARNBY CHOIR . PSALTER CANTICLES , Kiya Cotiges . 
 preparation 
 CHANTS , ANCIENT ani MODERN 

 small town objected tu . Beethoven , Novello , Ewer Co 

 Communications Glebe Field , Stoke Newington , N 




 PROFESSIONAL NOTICES 


 MISSES BARTH 


 MR . TEMPLE 


 MR . LLOYD OWEN 


 USIC ENGRAVED , PRINTED , PUB- 


 UTLER MUSICAL INSTRUMENTS . — 


 CORNETS , SAXHORNS , DRUMS , FLUTES , CLARIO- 


 EYOND ALLCOMPETITION.—T. BR . WILLIS 


 35 


 MENDELSSOHN 


 LIEDER OHNE WORTE 


 BOUKS 1 8 


 NEW SONGS JOSEPH BARNBY 


 ROSE NIGHTINGALE . 


 SONGS DUETS 


 ANTON RUBINSTEIN 


 7 . SONG EGMONT 1 6 


 A. BARRY COMPOSITIONS 


 PIANOFORTE SOLOS . 


 SONGS 


 EW - SONG . 


 ROBERT COCKS CO . NEW MUSIC . 


 MR . W. T. WRIGHTON NEW VOCAL DUETS 


 O PLAY ORGAN , HARMO 


 ILLEM COENEN COMPOSITIONS 


 SONGS . 


 30 


 3 0 


 BAPTISTE CALKIN EASY MORNING 


 USELEY.—A CHANT SERVICE 


 DWARD HERBERT CHANT TE DEUM , 


 NEW WORK ORGAN 


 READY , 


 ORGANIST TOWN HALL , & C. , LEEDS 


 METZLER CO . 


 NEW PUBLICATIONS 


 LORD KING . 


 UNDAY EVENINGS HARMONIUM . 


 HOU HAST MERCY , O LORD . 


 NISON SERVICE 


 EXETER HALL 


 MAGAZINE SACRED MUSIC . 


 . XV . , VOL . 4 . APRIL , 1869 


 MACFARREN 


 G. 


 MESSIAH . CREATION . 


 SPACIOUS NEW WAREROOMS , GENTLEMAN , large Experience , time 

 undertake Choirs Train , London 
 South Western Railway . , Organduty . Terms moderate . 
 Beethoven , Novello , Ewer Co 

 GENTLEMAN , spent years 




 IMPORTANT NOTICE > MUSICAL PROFESSORS ANDTo continued 

 INCIDENTS LIFE BEETHOVEN . 
 R. M. HAYLEY . 
 ( Continued p. 540 , Vol . 18 

 Beethoven keen sense right 
 deeply wounded found deceived 
 character man 
 long friendly terms . lawsuit 
 involved Malzel , brother artist Vienna , 
 concluded kind compromise , ~ 
 Beethoven consented let matter endon paying 
 half costs . composition , battle Vit- 
 toria , performed congress 
 Vienna , gave rise litigation . appears , 
 statement case Beethoven 
 drew counsel , spontaneously , 
 remuneration , composed Battle 
 Symphony , agreed , 
 adapted orchestra , performed , to- 
 gether Beethoven works , concert 
 benefit soldiers wounded war . 
 Beethoven happened time great want 
 money . loan ducats 

 titled ‘ “ ‘ Aus Mozart Leben , ” published 
 Leipziger Allgemeine Musikalische Zeitung , ! 
 Friedrich Rochlitz , councillor state Saxe 
 Weimar , esteemed musical critic . 
 gave tolerably circumstantial account | 
 mysterious circumstances attending origin 
 Requiem , important , 
 bearing present story , statement , 
 ‘ second appearance messenger , 
 Mozart set earnestly 
 work , weeks ready ( 
 completed , ist er fertig ) ; fallen 
 asleep 

 1813 , Gerber published celebrated Bio- 
 graphical Lexicon Musicians , gave 
 notice Mozart life , mentioned 

 offered Miilzel accepted . 
 strength disbursement , money 
 soon repaid , Milzel affected consider 
 Symphony present Beethoven , an- 
 nounced publicly property . 
 took Munich , performed , 
 forwarded London . Indignant 
 dishonourable conduct , Beethoven published 
 protest circulated members 
 musical profession . ‘ composition , 
 cause vexation , produced 
 September , 1814 , Congress assembled 
 Vienna , ind author received , acknowledg- 
 ment merits , marks distinction . 
 Empress Russia present 200 ducats , 
 society amateurs England sent 

 Requiem . said , messenger presented / valuable pianoforte , manu- 
 , immediately Mozart death , to|facturers country . magistracy Vienna 
 claim copy , received unfinished ; that/ presented freedor . city ; 

 deliver personally Prince hands . | hater 

 step , , attended ver 
 difficulties , persons highest rank , 
 indiscriminately , admitted 
 Royal presence . appearance letter 
 calculated ensure speedy reception , 
 handwriting Beethoven generally 
 illegible ; , particular instance , 
 taken pains write distinctly 
 usual , laboured defect . 
 Secretary Austrian Embassy , re- 
 quested deliver letter hands 
 Prince , stated position entitle 
 , matter arranged 
 channel . attempt proved 
 fruitless ; letter , eventually given 
 Prince Page , ardent admirer 
 Beethoven compositions , elicited Prince 
 asa word acknowledgment . Beetho- 
 ven bitterly complained want cour- 
 tesy , said letter Ries : ‘ * Prince 
 sent butcher knife , 
 turtle . ” bad probably heard Prince 
 fond variety dainty dishes , 
 allusion . cold neglect bis favourite 
 works formed marked contrast signa 
 honours conferred court Frederic 
 William II . , King Prussia . kindness re- 
 ceived monarch in- 
 delibly engraved memory 

 
 h 

 difficulties Beethoven time 
 contend , restore cheerful- 
 ness serenity mind . , usual , 
 frequent attacks illness ; , addition 
 death brother , , use words , 
 * exercised powerful influence mind 
 works , ” destined suffer 
 trying loss . Salomon , celebrated vio- 
 linist , native Bonn , like Beethoven , died 
 London years ’ residence city . 
 amember Philharmonic Society , 
 distinguished meritorious exertions 
 music Haydn better known England , 
 Beethoven ; compositions , 
 especially Symphonies , brought forward 
 public concerts . Beethoven mind 
 continually distracted difficulties 
 pecuniary nature . writing friend Ries 
 London , 10th March , 1816 , says : ‘ 
 ducats , farthing come hand , begin 
 fear English generous 
 abroad , Prince Regent 

 | 
 y great ) Beethoven stranger tender 
 /passion , , seldom exempt 

 influence , proved concurrent testimony 
 friends , object youthful 
 affections , young lady Cologne , 
 spent weeks family Brennings , 
 Bonn . pretty , lively blonde , pleasing 
 manners appearance , passionately fond music , 
 capable singing grace sweet- 
 ness . wife Austrian officer . 
 fell love lady , likewise dis- 
 tinguished beauty accomplishments , 
 attachment lasted time . Vienna , , 
 conquests ; shewed , occasions , 
 admiration fair sex , treating 
 deference attention . ad- 
 vanced years , loss hearing shut 
 society entirely ; necessary 
 lead life comparative seclusion . Music 
 occupied time ; declined invi- 
 tations , fearing deafness render 
 presence society burden . 
 extraordinary , delicate regard 
 feeliugs , prompted step , 
 Beethoven , unwearied advocacy political 
 liberty , suffered restraint im- 
 posed free expression opinions , what- 
 sentiments company 
 placed . spoke openly , 
 reserve , an1 slow censure 
 government , police , customs 
 prevailed aristocratic portion 
 community . peculiarity , known 
 Vienna , winked authorities , 
 indulgence eccentricities , respect 
 genius ; cause Beethoven 
 maintain greater liberty speech 
 exist thanin Vienna . heau ¢deal constitution 
 England ; standard 
 measured new appearance political 
 world 

 knew high estimation 
 works held country . 
 received unequivocal proof , 1817 , 
 invited Philharmonic Society 
 London , proceed place purpose 
 composing new symphonies . ill - health , 
 circumstances , obliged 
 idea visit ; lively interest long 
 took projected journey evidenced 
 correspondence subject Ries 

 ing ; nay , thanks , orally 
 writing . income amounts 3,400 florins 
 paper money . pay 1100 house - rent . ser 

 stances Beethoven life . 9th July , 
 1817 , writes follows : ‘ offer 

 jlast letter flattering . 

 man profession , 
 demands unreasonable . , 
 request communicate 
 directors . 1 . Ishall London middle 
 January , 1818 , latest . 2 . great 
 symphonies composed expressly Society shall 
 ready , shall remain sole 
 exclusive property Society . 3 . Society 
 shall engage pay sum 300 
 guineas , 100 guineas travelling expenses ; 
 , , cost , shall 
 require companion journey . 4 . shall 
 commence composition symphonies imme- 
 diately , Society shall advance , order 
 paid , 150 guineas , order 
 provide , delay , neces- 
 sary journey . 5 . conditions stipulated , 
 viz . , appear orchestra , lead , 
 Society preference similar en- 
 gagem ents , accepted ; , sense 

 appeared . ‘ + Beethoven , ” writes , ‘ 
 country , knows . 
 written Baden friends Vienna , 
 Baden . temperis said 
 insupportable ; , , 
 mad . easy talk way — God for- 
 ! poor man , , completely deaf . 
 Figure situation,—to fingers 
 keys , hear note ! gave curious speci- 
 men absence mind day . went 
 restaurateur , sat table , 
 having remained passive motionless hour , 
 called waiter . ‘ pay ? ’ ‘ , 
 sir , ; 
 ? ’ ‘ Bring like , 
 business . ’ ” ‘ spite faults 
 found Beethoven , ” says Zelter , letter 
 period , “ ‘ reason , 
 Vienna object great respect curiosity . 
 promised , matters business , 
 morning shop Steiner , music pub- 
 lisher . Steiner eager tell great man 
 coming , time , pay visit 
 little shop , hold 
 persons . appearance , 
 crowding door standing street 

 consisted artists , men letters , persons 

 Society approve proposals . ” 
 postscript , enquires strength 
 orchestra , number violins muster , 
 dimensions acoustic properties hall 

 appears preliminaries settled , 
 Beethoven uecessity putting 
 intended journey score ill - health . 
 time , affairs embarrassed 
 state . ‘ * wish , ” writes Ries , “ 
 prosperity increase daily . 
 . people starve ; , 
 , : imagine , 
 , suffer . beg write soon . 
 wish , slightest possibility 
 , start sooner intended , escape 
 irretrievable ruin ; case , shall 

 glimpse man heard , rarely 
 seen . waited vain : Beethoven , fatigued 
 journey day , overslept , 
 broke appointment 

 Notwithstanding obstacles , hope visiting 
 England wholly left . Ina 
 letter Ries , written 6th April , 1822 , apo- 
 logising protracted silence , says : “ 
 cherish hope visiting London , probably 
 spring , health permit . find , 
 dear Ries , appreciator pupil , 
 great composer , knows 
 art benefit reunion ? , , 
 wholly devoted muse , find 
 happiness life . ” letter , mentions 
 Mass ( missa solennis ) recently com- 
 posed , asks 
 London . answer given question , 
 applied Peters , music - seller Leipzig , 
 writes , 26th July , 1822 , offering 
 work 10U0 florins , convention money Vienna . 
 adds : ‘ * competition purchase 
 works present active , thank 
 Almighty , having hitherto suffered heavy losses . 
 , | foster - father brother son , 
 left wholly unprovided . boy , 
 years age , shows great aptitude scientific pur 

 44 MUSICAL TIMES.—Apniz 1 , 1869 

 bare subsistence . ‘ great pleasure , ” says , 
 months later , writing friend Ries , 
 London , ‘ ‘ accepting proposal write new 
 symphony Philharmonic Society . 
 liberality English far 
 means nations , write 
 musicians asking remuneration , 
 poor Beethoven . 
 London , 1 compose 
 Philharmonic ? Beethoven compose , 
 God thanked , truly . 
 Heaven grant health ( 
 better ) 1 meet demands 
 Europe — aye , North America , , 
 live prosper world . ” writes 
 strain Peters , March , 1823 . ‘ “ ‘ 
 circumstances render necessary 
 guided pecuniary considerations . 
 different work . composing , 
 think , merely write 

 Beethoven complained obliged 
 recourse teaching means eking 
 income . expresses point 
 letter Ries : ‘ stay Archduke Rudolph 
 Vienna lasted nearly weeks . 
 time obliged lesson daily 
 hours - half , hours . 
 lessons man scarcely fit think , 
 write , invariably miserable condition 
 requires [ set work moment 
 notice , order earn sutlice 
 wants day 

 Liberality prominent feature Beethoven 
 character ; poverty , 
 years , reduced , source 
 greatest unhappiness , fact necessarily 
 circumscribing power good . 
 letter Ries , dated 5th September , 1823 , 
 says : ‘ * poor live 
 pen , accept farthing Phil- 
 harmonic Society . , | wait till 
 remittance payment symphony comes 
 hand . order , , proof 
 affection , confidence members 
 Society , sent overture , 
 placed disposal . Ileave 
 ... ... brother John , 
 keeps carriage , wished 
 , , consulting , offered 
 said overture Boosey , publisher London . 
 good mistake . 
 purchased , perceive , 
 view turning account . O Frater ! 
 eh eiliers J received symphony 
 dedicated . consider dedication 
 kind challenge , inscribed 
 . Ithought ought tosee 
 work . pleasure afford 
 testify gratitude way . , 
 truth , deeply indebted many‘favours 
 proofs attachment . health derive 
 benefit mineral waters , shall kiss 
 wife London 1824 

 long - anticipated , long - talked project 
 execution . England 
 honour receiving soil distinguished com- 
 poser ; , years rolled , impediments 
 contemplated visit greater in- 
 surmountable . Beethoven world - wide celebrity 

 musical society ; pleasure derived 
 charming style composition 
 unrivalled , confined generation 
 lived 

 career Beethoven , especially 
 , painful instance uncertainty 
 attaches popular favour . affecting passages 
 letters friends , plainly 
 unhappy circumstances illustrious man passed 
 days . contrive , life toil 
 privation , scrape pittance 
 died possessed , remember anxiety 
 cost , life embittered , 
 broken health decayed powers , 
 regard future save visions penury 
 destitution haunted mind . 
 reasonably expect sympathy effectual aid 
 hands , experience 
 taught , previously met 
 indifference neglect ? English writer makes 
 following pertinent remarks subject : 
 * * Germany , musical land , far , 
 commonly supposed , paradise 
 musicians . Mozart struggled life difli- 
 culties , obliged toil incessantly , 
 fame , daily bread . widow saved 
 destitution second marriage , 
 respectable man , , , father 
 dead husband children . sister , celebrated 
 girl shared triumphs childhood , 
 uame associated memory , 
 died old age , extreme penury , 
 actually supported charity . Beethoven lived 
 unpatronized great , neglected 
 public , barely able subsist life labour 
 parsimony , unknown unheeded country- 
 men , great resounding 
 Europe , transcendent 
 genius unaccompanied suppleness 
 courtier , arts man world . Let 
 musicians think little things 
 join common cry country , 
 repine ‘ lot cast pleasant 
 places ’ Germany 

 continued 




 HENRY C , LUNN 


 OPERA 


 CRYSTAL PALACE 


 ORATORIO CONCERISMISS AGNES ZIMVERMANN CONCERT 

 Tue Sotrées Musicales given 
 accomplished pianist Hanover.square Rooms , 
 9th ult . , success fully justified usin 
 time time predicted future 
 artist richly endowed perfectly trained . 
 Beethoven Sonata ( Op . 5 , . 1 ) Pianoforte 
 Violoncello , excellently played Miss Zimmer- 
 mann Signor Piatti ; interesting feature 
 concert Handel Organ Concerto , B flat 
 ( adapted pianofurte skill Miss Zim- 
 mermann ) , performed concert - giver , 
 entirely memory , vigour finish 
 reconciled want sustaining power 
 instrument carefully arranged 
 Miss Zimmermann Sonata , D minor , Pianoforte 
 Violin , played composer Herr Joachim 
 time public ; excessive merits , 
 composition performance , acknow- 
 ledged , deserved , warm applause 
 thoroughly musical audience . 
 reviewed Sonata length pages , 
 add hearing confirmed 
 advanced favour . work 
 young composer , shows power construc- 
 tion scarcely expected ; 
 snatches thought appear somewhat 
 advance requisite power development , hopeful 
 sign safely pointed 
 heed friendly counsel indiscriminate applause . 
 Schumann Trio , D minor ( Op . 63 ) . Pianoforte , 
 Violin , Violoncello , rendered faultless style 
 Miss Zimmermann , Herr Joachim , Signor Piatti , 
 concluded excellent concert utmost satisfaction 
 audience . Madaine Rudersdorff solo vocal- 
 ist , gave effect Schumann Songs , 
 “ « Abendlied ” “ Schiéne wiege , ” exceed- 
 ingly graceful melodious song , Signor Ran- 
 degger , called « Peacefully Slumber ” ( accompani- 
 ments pianoforte , violin , violoncello ) , 
 received marked applause . Mr Joseph Barnby 
 Choir increased interest concert singing 
 selection songs delicacy precision ; 
 attractive Miss Zimmer- 
 mann ‘ Daffodils ” ‘ Good Morrow , ” Mr. 
 G. A. Macfarren * ‘ Fishers . ” conductors 
 Mr. Joseph Barnby Signor Randegger 

 MR . HENRY LESLIE CONCERTS 

 Tue Concerts took place , 4th ult . , 
 programme selected , scarcely 
 relying attraction efforts choir . 
 efficient orchestra performed Beethoven Symphony , C 
 minor , effect ; Herr Joachim played Men- 
 delssohn Violin Concerto hope , 
 movement , , taken pace 
 rapid everybody solo performer , 
 appeared perfectly ease . played Beetho- 
 ven Romunce , G , refined expression , 
 rewarded enthusiastic applause , , , 
 consummate artist , like Mendelssohn , 
 ceas'-d consider applause scarcely reward worth 

 ecepting . choir sang grand Psalm 

 ar 
 |of Mendelssohn , “ Judge , O God , ” late 

 Samuel Wesley fine Motett , « exitu Israel , ” 
 unnecessary , we.can add 
 oft repeated eulogiums perfection Mr. Leslie 
 forces unaccompanied works , Mendelssohn 
 Psalm enthusiastically - demanded . Mdlle . Lieb- 
 hart scarcely home Mozart “ Voi che sapete . ” 
 Gounod Ave Muria ( engrafted Bach prelude ) 
 better sung , , usual , pleased audience 
 immensely , concert concluded excellent 
 performance Beethoven « Chorus Dervishes , ” 
 “ Turkish March , ” Ruins Athens 

 fourth Concert , 18th ult . , consisted chiefly 
 choral , entirely sacred , music . ‘ Sanctus , ” 
 Mr. John C. Ward , organist choir , 
 performed time , scarcely judged 
 according merits single hearing . , 
 , skilfully constructed , shows 
 intimate knowledge choral effect . 
 ‘ Ave Maria , ” Mr , John Barnett , composer 
 ‘ “ « Mountain Sylph ” ( works , unfortunately , 
 seldom heard ) , novelty , 
 think likely advance Mr. Barnett writer 
 religious music . Itis extremely beautiful , 
 received marked applause audience . 
 hope shortly opportunity hearing 
 composition briefly record 
 favourable impression 




 ROYAL ACADEMY MUSIC 


 ROYAL SOCIETY MUSICIANS 


 ( SOLOMON PRAYER . ) 


 72 


 = = SS = = = E 


 CNS 


 13 


 - . . ‘ 


 XUM 


 JEUM 


 CIA- 


 XUMPHILHARMONIC SOCIETY 

 Turs old established Society , music musi- 
 cians Owe country , yielded 
 popular demand good music reasonable price , 
 moved aristocratic quarters Hanover - square 
 People Music Hall Regent - street , step 
 glad find attended complete suc- 
 cess , subscription present season far 
 advance year . concert , 
 took place St. James Hall , 10th ult . , 
 respect excellent . Woelfl Symphony , G 
 minor , performance commenced , 
 welcome resuscitation , think 
 attended good result ; long works 
 allowed rest oblivion 
 deservedly taken rank art , firm faith 
 good old music dies , bad new music 
 lives . seurcely , , believe 
 Symphony attractive mixed audience ; 
 appreciate solid writing construc 
 tive power areal ar ist like Woelfl — 
 ranks Philharmonic subscribers — 
 thank Society allowing place ina 
 concert . Respecting Mendelssohu Scotch Symphony , 
 , save played 
 utmost effect . ‘ orchestral 
 pieces overtures Auryanthe Lodoiska , 
 went perfection Herr Joachim playing 
 Beethoven Violin Concerto , usual , complete 
 realisation composer idea , demands 
 executive power : performer forgotten 
 ina reading thoroughly intellectual , Equally perfect 
 rendering Schumann Adendlied 
 Loure Allegro , Kk , Seb . Bach , 
 received warmest marks approval . 
 vocal department , Mdlle . Anna Regan , débutante , 
 highly favourable impression , Mozart 
 “ Non mi dir , ” air Lotti , “ Pur dicesti ’ ; 
 Mr. Vernon Rigby gave worn * Salve ! dimora , ” 
 taste , violin obbligato excellently 
 played Mr. Viotti Collins , Mr. W. G. Cusins conducted 
 intelligence decision invariably 
 shown appointment director orchestra . 
 analytical programme concert , ably written 
 Mr. G. A. Macfarren , issued occasion , feature 
 think add materially interest 
 performances . concert attended Royal 
 Highnesses Princess Louise Prince Arthur 

 GENOA 

 Conductor . interested advance good music 
 wish Society success 

 Unver title “ Musical Winter Even- 
 ings ” series Concerts commenced St. 
 George Hall , 2nd ult . , performance , , 
 strangely , taking place afternoon . Mozart 
 Quartett , D minor , Beethoven , E minor , 
 rendered Messrs. Holmes , Folkes , Burnett , 
 Signor Pezze ; Hummel Trio , E flat , 
 Mr. Holmes , Signor Pezze , Mr. Lindsay Sloper . 
 named artist ( , best resident 
 pianists , scarcely heard public ) gave Mendels- 
 sohn Capriccio , E major , excellent effect , 
 Miss Watts highly successful vocal solos 

 requested state Mr. F. N. 
 Lihr appointed Hon . Conductor Plymouth 
 Amateur Vocal Association , room Mr. Cottman , 
 resigned 

 subject . emolument arising exercise ex . 
 ceptional talent Mr. Reeves necessarily 
 large ; ulcerated sore throat paid , 
 abandonment lucrative engagement , 
 payment heavy damages non - perform . 
 ance contract physically prevented 
 fulfilling , small chance singer 
 able provide time splendid 
 faculties delighted thousands longer 
 available 

 Mr. Greenuti1 Concert took place 
 Beethoven Rooms , Thursday evening , 18th ult , 
 programme arranged . Miss Robertine Hen- 
 derson , Madame Emmeline Cole , Mdlle . Erna Steinhagen , 
 Messrs. Greenhill , Carter , Maybrick , Herr Stepan , 
 Mr. Orlando Christian contributed songs _ ballads , 
 concerted pieces Mozart Sestetto , Sola , 
 Sola ( exceedingly given ) , Quintette , 
 Rival Beauties , * * surprise confounded , ” 
 Trio , “ 1 Naviganti , ” Randegger , sung Madame 
 E. Cole , Mr. Greenhill , Mr. O. Christian , heartily 
 encored . Mdlle . Steinhagen Herr Stepan 
 highly successful Mozart Duet , Crudel Perché . Mr , 
 Walter Bache solo pianist , Signor Alberto 
 Randegyer conductor . Altogether concert 
 successful 

 HIGHLY interesting series lectures , 
 ‘ Sacred Secular Art , exemplified Music , ” 
 lately delivered Mr. G. A. Macfarren , 
 London Institution . Considering music twofold 
 application sacred secular subjects , lectures 
 arranged : — . 1 , “ Church Music ” ; . 2 , 
 “ Opera ” ; . 3 , ‘ Oratorio ” ; . 4 , 
 “ Chamber Music . ” efficient choir , direc- 
 tion Mr. J. Proudman , illustrated lecture 
 excellent selection sacred music , 
 mention - written hymn , varied accompa- 
 niment , composition lecturer , 
 encored . illustrations second lecture 
 entirely taken Mozart opera , Don Giovanni ; , 
 , Handel Jephtha , Mendelssohn 
 Hymn Praise . fourth lecture included 
 instrumental selections , Mr. Walter Macfarren performing 
 effect specimens pianoforte 
 works Bach , Scarlatti , Mendelssohn , Sterndale Bennett , 
 & e. , joining Herr Carl Deichmann 
 Beethoven Sonatas Pianoforte Violin . 
 vocalists Miss Annie Sinclair , Miss Banks , Madame 
 Patey - Whytock , Mr. Wilbye Cooper , Mr. George Perren , 
 Mr. Ralph Wilkinson , Mr. Renwick , Mr. Patey , & c. ‘ lhe 
 lectures listened earnest attention 
 appreciative audience 

 Ara dinner given Organist Choir- 
 men St. Botolph , Bishopsgate , 27th February 
 ( member choir ) , testimonial , consisting 4 
 large handsomely framed photographic group 
 choir , volumes musical works , 
 presented choir - master , Mr , W. J. Shoosmith , 
 recognition zeal performance duties , 
 mark personal regard 




 54Musical Sketches , Abroad , Home . 
 Ella 

 pieasanT book gossip music musicians . 
 Mr. Ella travelled abroad ; keen 
 observation , fixed determination pick 
 scrap information respecting art , brought 
 interesting anecdotes , read 
 pleasure profit . urged 
 personality author obtrudes 
 somewhat work , recol- 
 lected great taken diary , 
 man perfect right talk 
 pleases . critical objection 
 book “ Musical Union ” 
 held model musical Society ; 
 aristocratic assembly reader constantly 
 referred music high class mentioned ; , 
 instance , speaking sensibly 
 necessity cultivating knowledge musical science , 
 order fully enjoy great works Art , says , 
 “ captious votary revels roulades medio- 
 crity Art , tauntingly jeers enthusiastic devotee 
 higher regions divine muse , totally 
 wanting sympathy admirers intellec- 
 tual productions engage executants 
 ‘ Musical Union . ’ ” Considering “ intel- 
 lectual productions , ” “ executants ” 
 heard musical season St. James 
 Hall , price reach , 
 reason alluding solely exclusive coterie , , 
 excellent itis way , little known 
 general public . respects , 
 said , book merit ; especially 
 great deal valuable information respecting state 
 music continental cities , scattered 
 pages ; reminiscences author date 
 years . agreeably anecdotes told 
 glad quote 
 benefit readers . instance pursuit 
 musical knowledge difficulties , , 
 refrain mentioning case workman 
 cotton - mills Stockport , wrote 
 author inclosing postage - stamps copy “ Record . ” 
 Half - - dozen publications sent , 
 acknowledging receipt says , “ 
 ‘ Records ’ guide , anticipate ( God willing ) hours 
 enjoyment , sit leisure moments hum- 
 ble cottage , walls decorated por- 
 traits Handel , Mozart , Beethoven , Mendelssohn , Bach , 
 Weber , Sterndale Bennett , Shakspeare , . ” 
 expressing earnest wish success Mr. 
 Ella attempt establish musical library stu- 
 dents , says ; “ ‘ , rehearsal , distance 
 seven miles Manchester . ” conclusion , 
 illustrations work , 
 interesting portrait Mozart , 
 love song addressed young composer 
 lady , prior passion Constance Weber . 
 melody accompaniments musical trifle , 
 anticipated , extremely beautiful 

 John 




 CHARLES JEFFERYS 


 57 


 LEEDS TUNE BOOK . ” 


 EDITOR MUSICAL TIMES 


 EDITOR MUSICAL TIMES 


 EDITOR MUSICAL TIMES 


 58 


 R. W. P 


 EDITOR MUSICAL TIMES 


 EDITOR MUSICAL TIMES 


 J. W 


 EDITOR MUSICAL TIMES 


 CORRESPONDENTS 


 59Maccabsus rehearsal Society , performance 
 complete oratorio looked distant date 

 EprxsureH.—The Annual Concert University 
 Musical Society , years ex- 
 istence , took place 17th ult . , Music Hall . 
 orchestra . numbering - performers , leader- 
 ship Mr. Mackenzie , able direction Mr. Hamilton , 
 performed Beethoven Symphony C , . 1 , overtures , 
 style reflected highest credit concerned . 
 choruses , especially “ Fair Semele , ” Mendelssohn 
 Antigone , Vintage Chorus composer 
 unfinished opera , Loreley , rendered effect 
 choir ninety voices ; feature concert 
 Students ’ song , ‘ * Alma Mater , ” composed expressly per- 
 formance Professor Oakeley , instead sung 
 harmonized form chorus , given , alternate 
 verses , unison , voices , harmony select 
 number , composer playing accompaniment 
 organ . Professor Oakeley pianoforte solos received , 
 deserved , warmest applause.——Ow 18th ult . 
 Professor Oakeley gave interesting organ performances 
 Music Class - room , Park Place . attendance 
 numerous previous occasion , available seat 
 occupied . selection judiciously , blending 
 severe music suited expressly instrument , pieces 
 popular character . ‘ ‘ Funeral March ” evidently intended 
 bear reference recent death Hector Berlioz , French 
 composer eminent musical critic . forms posthu- 
 mons works Mendelssohn , solemn impressive com- 
 position . proficient tasteful executant Professor 
 Oakeley , instrument capable fine combinations 
 command , hardly necessary 
 performance respects excellent , 
 afforded greatest satisfaction present 

 Guascow.—The Choral Union gave Concert 
 present season City Hall , 2nd nult . , Handel 
 Oratorio Jud 1s Maccabeus performed utmost success . 
 principal vocalists Madlle . Vanzini ( soprano ) , Madame 
 Lincey - Nalton ( contralto ) , Mr. Nelson Varley ( tenor ) , Signor 
 Foli ( bass ) ; second soprano duets taken 
 Miss Margaretta Smyth . ‘ solos calling especial praise 
 Madlle . Vanzini ‘ ‘ mighty Kings , " Mr. Varley ‘ ‘ Sound 
 alarm , ” Signor Foli ‘ ‘ Lord worketh wonders . ” 
 choruses given admirable effect , particularly 
 “ bow , ” “ Fallen foe , ” “ 
 conquering hero comes , ” triumphantly proved 
 Choral Union fully able grapple grandest specimens 
 choral writing standard sacred works . Mr. Peace accom- 
 panied skilfully organ , Mr. Lambeth conducted 
 usually efficient manner , orchestra ( consisting 
 thirty professional players , addition members 
 instrumental section Union ) thoroughly satisfactory 




 60Leicesrer . — Special Sermons preached St. An- 
 drew Church Sunday , 28th February , behalf 
 Organ Fund . collected £ 10 . service 
 usual , Gregorian ; anthem morning Att- 
 wood ‘ Turn Thy face sins , ” Mr. Webster singing 
 solo . evening anthem Spohbr “ pants hart , ” 
 went exceedingly , Master Crane taking solo ina 
 manner great credit . Psalms evening 
 particularly sung ; 136th , refrain 
 verse , ‘ * mercy endureth , ” followed mournful 
 “ waters Babylon , ” followed joyful 
 138th , effective contrast , skilful accompani- 
 ments Mr. Crow , presided organ 
 usual taste , added greatly success attended 
 day services 

 Liverroot.—The Subscription Concert 
 Philharmonic Society present season took place 23rd 
 February . ; solo artistes Madame Rudersdorff , Miss Angele , 
 Mr. Wilbye Cooper Mr. Lewis Thomas . works selected 
 performance Beethoven Mass C , preceded 
 Overture Spohr Judgment , followed 
 second parts Creation 24th February , second 
 series plan London Monday Popular Concerts , 
 given Room Philharmonic Society , crowded 
 enthusiastic audience . performers inthe stringed quartett 
 series — viz . , Herr Joachim , 
 Mr. L. Ries , Mr. Zerbini , Signor Piatti . pianoforte 
 occasion taken Mr. Charles Hallé . vocalist 
 Miss Anna Jewell ; concert , unreasonable 
 demand encores , partially complied , 
 spoke enjoyment hearers 
 discretion.——TueE fourth Subscription Concert Philharmonic 
 Society year took place 9th ult , devoted 
 excellent performance Judas Macc : beus , principal voca- 
 lists Miss Edith Wynne , Miss Julia Elton , Mr. George 
 Perren , Mr. Lander . Mr. Perren took originally 
 allotted Mr. Vernon Rigby ( prevented indisposition 
 appearing ) , short notice ; energy 
 gave songs ‘ ‘ Judas ” showed lack prepara- 
 tion . solo music , , carefully rendered ; 
 duets ladies . — — 17th ult . , 
 series performances , plan London Mon- 
 day Popular Concerts , given Philharmonic Hall , 
 performers Herr Joachim , Mr. L. Ries , Mr. Zerbini , 
 Signor Piatti ; pianoforte , Herr Ernst Pauer ; solo vocalist , Miss 
 Edith Wynne . programme , previous Concerts 
 series , highest class , rendered perfection . 
 room crowded thoroughly appreciative audience . — — 
 TuE Musical Society gave successful performance Judas 
 Maccabeus 15th ult . , St. George Hall . Principals , Miss 
 Anna Hiles , ( performance ‘ mighty Kings ” 
 deservedly applauded ) , Miss Chadwick , Miss Fanny Bennett , Mr. 
 George Perren ( received enthusiastic encore ‘ Sound 
 alarm ” ) , Mr. Henri Drayton ; leader band , Mr. C. A. 
 Seymour ( Manchester ) . organist , Mr. W. T. Best , conductor , 
 Mr. James Sanders . band chorus efficient . 
 “ , conquering hero comes ” encored ; march 
 given . Hall crowded 
 expected . St. Paul announced Concert 
 society . — — “ 28th open rehearsal ” Societa Armonica 
 took place Liverpool Institute , Saturday evening , 20th 
 alt . , presence large appreciative audience . 
 ‘ ‘ rehearsal ” opened overture Schubert , Italian 
 style ( performance Liverpoo ) ) , played great 
 refinement expression ; Romberg Cantata , “ * 
 Transient Eternal . ” Mozart Symphony D , 
 composer adagio violins , viola , bass , horn , 
 equally performed , narrowly escaping encore . 
 solo vocalists Miss F. Armstrong , Miss Monkhouse , 
 Mr. T. J. Hughes . Mr. Lawson efficient trustworthy 
 leader , Mr. Armstrong conducted sgill 

 Mancuester.—As confidently anticipated , im- 
 mense audience assembled Choral Concert Mr. Hallé 
 present season , 4th ult . announcement perform- 
 ance recently revived Jephtha gratifying 
 frequenters concerts . keen appreciation suc- 
 ceeded curiosity , musica ! public surprised 
 manifold beauties choral grandeur great work 
 remain long comparatively unrevealed , oratorio 
 acknowledged favourite . performance great 
 success , crowning choral triumph season 
 worthy brilliant concerts preceded . 
 chorus , “ Ammon God King , ” sung 
 vigour effect , magnificent ‘ ‘ loud 
 voice , ” majestic written , 
 remarkably weil given . melodious beautifully varied 
 ‘ Cherub Seraphim , unbodied forms , ” satis- 
 factory achievement ; best , , , sublime 




 61 


 62 


 MONTH , 


 OLLENHAUPT , H. 


 20 REWARD 


 THGATE 


 PIANO- 


 TUNER 


 63 


 UPPLEMENTAL HYMN TUNE BOOK , 


 TUNES CHORALES 


 TONIC SOL - FA EDITION 


 GREATLY REJOICE LORD . 


 CHESTER CATHEDRAL 


 NEW CHEAPER EDITION ; 


 ONGREGATIONAL CHURCH MUSIC . 


 SUNG UNISON , 


 ALEX . 8 . COUPER 


 AUTHORISED EDITION RUGBY SCHOOL . 


 ELEMENTS MUSIC SYSTEMATI- 


 CHAPPELL ’ 


 SONGS 


 CONTINUED . ) 4 


 EEZSSR SERRE PERER LS 


 VOCAL LIBRARY 


 CHAPPELL MUSICAL MAGAZINE 


 VOCAL PIANOFORTE MUSICSeventh Selection 

 38 Fashionable Dance Book , Pianoforte . 
 87 Country Dances , Reels , Jigs , & c. , Pianoforte . 
 36 Christy Buckley Minstrel Airs Pianoforte . 
 85 Christy Minstrel Songs . ( Second Selection ) . 
 34 Christmas Album Dance Music . 
 33 Juvenile Vocal Album . 
 82 Beethoven Sonatas . Edited Charles Hallé ( . 6 ) . 
 31 Beethoven Sonatas . Edited Charles Hallé ( . 5 . ) 
 30 Beethoven Sonatas . Edited Charles Hallé ( . 4 . ) 
 29 Contralto Songs , . Mrs , R. Arkwright , & c. 
 28 Beethoven Sonatas . Edited Charles Hallé ( .. 3 . ) 
 27 Sets Quadrilles Duets , Charles d’Albert , & c. 
 26 Thirty Galops , Mazurkas , & c. , d’Albert , & c , 
 25 Sims Reeves ’ Popular Songs . 
 24 Thirteen Popular Songs , Barker , Linley , & c. 
 23 - Juvenile Pieces Pianoforte . 
 22 - Christy Minstrel Songs . ( Selection . ) 
 21 Pianoforte Pieces , Ascher Goria . 
 20 Beethoven Sonatas . Edited Charles Hallé ( . 2 . ) 
 19 Favourite Airs Messiah , Pianoforte . 
 18 Songs Verdi Flotow , English words . 
 17 Pianoforte Pieces , Osborne Lindahl . 
 16 Sacred Duets , Soprano Contralto Voices . 
 15 Eighteen Moore Irish Melodies . 
 14 Songs , Schubert . English German Words . 
 13 Popular Duets Soprano Contralto Voices , 
 12 Beethoven Sonatas . Edited Charles Hallé , . 1 . 
 11 Pianoforte Pieces , Wallace . 
 10 Pianoforte Pieces , Brinley Richards , 
 9 Valses , C. d’Albert , Strauss , & c. 
 8 Polkas , Charles d'Albert , Jullien , Koenig , & c. 
 7 Sets Quadrilles , Charles d ' Albert , complete . 
 6 Songs , Handel . 
 5 Sacred Songs , Popnlar Composers . 
 4 Songs , Mozart , Italian English Words 
 8 Songs , Wallace . 
 2 Songs , Hon . Mrs. Norton . 
 1 Thirteen Songs , M. W. Balfe 

 SONGS PIANOFORTE ACCOMPANIMENTS 




 CHAPPELL & CO . , 50 , NEW 


 BOND - STREET , LONDON