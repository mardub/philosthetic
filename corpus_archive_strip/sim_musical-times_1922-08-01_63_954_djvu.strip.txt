


RK. 


GAN. 


‘TED


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR


FOUNDED IN 1844


ROYAL CHORAL SOCIETY. 


ROYAL ACADEMY OF MUSIC. 


YORK GATE, MARYLEBONE ROAD, LONDON, N.W.1 


THE ROYAL COLLEGE OF MUSIC. 


PRINCE CONSORT ROAD, SOUTH KENSINGTON, S.W.7. 


A. R. C. M


THE ROYAL COLLEGE OF MUSIC PATRON'S FUND 


GUILDHALL SCHOOL OF MUSIC


VICTORIA EMBANKMENT, E.C.4. 


PRINCIPAL SIR LANDON RONALD


AUGUS


PUBLISHED ON THE FIRST OF EVERY MONTH


ROYAL


MANCHESTER COLLEGE OF MUSIC. 


THE PHILHARMONIC CHOIR


BIRMINGHAM & MIDLAND INSTITUTE 


SCHOOL OF MUSIC


SESSION 1921-1922


THE LONDON COLLEGE FOR CHORISTERS. 


MUSICAL


THE


96, WIMPOLE STREET, W.1


TOBIAS MATTHAY 


PIANOFORTE SCHOOL


COMPLETE TRAINING COURSE FOR TEACHERS 


MANCHESTER SCHOOL OF MUSIC. 


THE IDEAL CORRESPONDENCE 


SCHOOL OF MUSIC


EXPERT TUTORS 


THEORY OF MUSIC, HARMONY, 


COMPOSITION, ART OF TEACHING, 


INTERPRETATION


FORM AND MEMORY TRAINING 


FOR MUSICIANS


6, BOLTON ROAD, FARNWORTH, BOLTON


THE TECHNIQUER


A.R.C.M., 


N.W.8


MUSICAL COMPOSI TIONS REV ISED


OF DISTINCTION


WRITTEN TO SONGS


A. CARRINGTON RRIGGS, 


TION BY A MASTER


M. MACDONALD SMITH, 19 


LONDON, W.C. 1


BLOOMSBURY SQUARE


FINGER MANIPULATION


AND CONTROL 


INSTRUMENT


MAXALDING FINGER-SPEEDING


| MAXALDING,” I , CRANBOURN CHAMBERS, 


JUARE


THERS


S WHICH 


TY, AND 


MENT


THOMAS PETRE JAMES LEVEY


REDING


BERS


1922 541


AND SINGING-CLASS CIRCULAR 


AUGUST I 1922


BRITISH PLAYERS AND SINGERS


THE LONDON STRING QUARTET


42 THEplayed at the first concert, 
1910, at Bechstein (Wigmore) 
Hall, we called ourselves the “New” 
Quartet. The programme was: Dohnanyi 
in D flat, Tchaikovsky in D, and a Fantasy 
Quartet (No. 1) of my own. We fetched the 
newspaper critics first go-off, and their com 
pliments were our reward for the previous two 
years of preparation. Our next concert, in 
June, 1910, was of Debussy in G minor, the

first of Beethoven’s Aasoumovskys, and 
a Fantasy of Balfour Gardiner’s. We early 
saw that “New” would soon be a _ super

annuated name, and Warwick-Evans suggested 
calling ourselves the “London ”—a_ bold 
suggestion, but it gave us something to live 
up to




THE L.S.Q. IN WAR-TIME


THE L.S.Q. ABROADthe “Temple of Music,’ near

Frank Bridge’s E minor (Sologna) Quartet, 
Beethoven in E minor, and Mr. Waldo Warner’s

f all 
our

Before crossing the Atlantic, we are to go 
again to Spain, and after the American tour 
we go on to Australia, and so shall be 
away from home for eighteen months

HOMAGE TO BEETHOVEN

And what of the music they play? Mr. Waldo 
Warner can admire many masters, Mozart and 
Brahms, Schubert and Debussy, but, as befits a 
true ‘quatuorvir,’ his divinity is Beethoven

It was Mr. Simpson (of Methven & 
Simpson, Edinburgh) who first suggested 
that we should give a week of playing all the 
Beethoven (Quartets. That was in 1920. 
After Edinburgh we gave the cycle in London, 
and then at Stockholm, Christiania, and in 
America—in all ten cycles, including three in 
London

I think I shall never get beyond my awe 
at the architecture and the wealth of the 
Beethoven Quartets. I am not a Beethoven 
idolater to the point of not seeing that he 
sometimes wrote poorish music. Possibly 
even some of the movements of the 
Symphonies are* not so lastingly great as 
they might have been. But as for the 
(Quartets, one never seems to come to the 
bottom of their wonders. They are in- 
exhaustible. And so far from “ familiarity

breeding contempt,” my feeling is that the 
more one works at them the more one may, 
and one can go back to a quartet one knows 
as well as anything on earth and still quake 
almost at the idea of pretending to rise to the 
immensity of the thought that is in this music. 
Beethoven, more than anyone, makes his

Mrs. Coolidge, by the way, is now opening

Bridge in E minor and G major |says 
Mr. Waldo Warner] and McEwen’s Aiscayv 
and Zhrenody Quartets have really become 
| favourites. We gave the first performance 
in London of Dame Ethel Smyth’s Quartet

The repertory of the London String Quartet has 
comprised the works of Haydn, Mozart, Beethoven, 
| Schubert, Mendelssohn, Schumann, Brahms ; one 
‘quartet of Boccherini; and, in modern music, 
|works by Frank Bridge, three string quartets, 
pianoforte quartet, pianoforte quintet; Dyson, 
string quartet; J. D. Davis, two string quartets 
land Variations on the Londonderry Air; Walford 
| Davies, Peter Pan Suite ; Delius, string quartet ; 
iG. Dorlay, string quartet ; Elgar, string quartet, 
| pianoforte quintet ; Percy Grainger, J7o//y on the 
| Shore ; Balfour Gardiner, string quartet ; Goossens, 
string quartet and small pieces ; Armstrong Gibbs, 
two string quartets ; Holbrooke, two string quartets, 
quintet, sextet, pianoforte quartet and quintet ; 
Julius Harrison, quintet for pianoforte, harp, 
and strings, clarinet quintet ; Herbert Howells, 
pianoforte quartet; Arthur Hinton, pianoforte 
quintet ; Ketelbey, quartet ; Levine, quartet; J. B. 
McEwen, four quartets ; Norman O’Neill, quartet ; 
Cyril Rootham, string quintet; Ethel Smyth, 
|quartet ; J. Speaight, quartet and small quartet 
| pieces; H. Waldo Warner, four quartets and 7x1 
| Ring Suite; R. Vaughan Williams, quintet and 
|On Wenlock Edge song-cycle, and pieces by 
| Donald F. Tovey, Wolstenholme, Haydn Wood, 
|Sir Charles Stanford, Arensky, Debussy, 
| Dvorak, Dohnanyi, d’Erlanger, Fauré, Franck, 
| Glazounov, Grieg, Ravel, Scontrino, R. Strauss, 
|Schénberg, Smetana, Svendsen, Steinhammer, 
| Schafer, Tchaikovsky, Turina, Hugo Wolf, Wolf- 
Ferrari, Guy Weitz, Saint-Saéns, and Stravinsky

i




THE


STUDIES ON THE HORN 


398 


1725


1922


545


BENEATH THAT SHADE’ 


RITORNELLO TO BASS ARIA


546 THE MUSICAL


1800-10


1772


THE


1922 547


KORNGOLD, STRAUSS, AND OTHERS: 


‘SUBJECTIVE’ CRITICISM


548 THE


I 1922


549


77


AN 


EXHIBITION


PARThave thought it impossible to compile a programme

of the kind without including something of Mozart 
and Beethoven. And where was the English idol

Handel




DS ; 


55


BRITISH MUSIC SOCIETY


ANNUAL CONGRESS


P. A


1922 553


MODAL COUNTER POINT


THE MUSICAL ASSOCIATION


554 THE MUSICAL


B. V


FOR ASPIRING PIANISTS Advanced students will find some useful practical 
hints in the chapter dealing with the playing of 
thirds, sixths, and octaves. The subject of fingering 
is briefly treated, and by means of musical examples 
the author illustrates some of the principles on 
which he bases his own fingering. On the subject of 
pedalling one or two sentences are perhaps mislead- 
ing. Thus we are advised that when applying the 
pedal ‘it should never be banged on, but pressed 
down gently and gradually.’ Gently by all means, 
but surely not gradually! In the majority of cases it 
requires a swift, sharp movement, avoiding, of course, 
any violence or jerkiness. Again, we read that the 
pedal should never be taken directly on the first beat 
of the bar, but in syncopation with that beat. 
Although this is in the main true, there are occasions, 
as in the case of detached chords or notes, where it 
is usually advisable to depress the pedal simultaneously 
with the playing of the notes

Some other points dealt with are ‘ Playing in 
public,’ ‘ How to make the pianoforte sing,’ ‘ How to 
choose and care for a pianoforte,’ ‘Some common 
mistakes and how to avoid them,’ ‘ A specimen lesson 
on the first movement of the J/oonlight Sonata of 
Beethoven,’ while at the end of the book, under 
the title of ‘The daily pianist,’ is an abridged 
compendium of five-finger exercises, scales, thirds

arpeggi, and octaves as practised by the great pianist




CHURCH MUSIC


XUM


SONGS


PIANOFORTE MUSIC


556


G. G


STRING MUSIC


EASY PIANOFORTE MUSIC


7. G


THE557

peaceful Adagio or a song of thanksgiving, because 
there is no restless, stormy first movement to contrast 
with it. His mood is always one of meditation. He 
is fond of dances and of movement, but he views 
these things from the standpoint of the poet and the 
scholar, not of the realist like Berlioz, for whom 
almost any dance ends in bacchanalian riot. The 
Scherzo of the Quintet is lively enough, but it never 
rises to irony or tragedy like the great Beethoven 
scherzi; the Burlesca is a jest, but a jest that never 
goes beyond the limits of courtly politeness. No 
other composer sets more definite boundaries to his 
art, but no one else has found within those boundaries 
equal distinction and refinement. That is the reason 
why the finest of all combinations—the string quartet 
or quintet—suits this music to perfection

Of the new editions of old music we have received, 
mention must be made of Messrs. Augener’s issue of 
Wieniawski’s popular Faust Fantasie, revised by 
M. Emile Sauret. Vivaldi’s Sonata da Camera in 
E minor has been adapted to string orchestra and 
pianoforte by Mr. James Brown for the Polychordia 
String Library of Messrs. Stainer & Bell. BV




SOME H.M.V


THE MUSICAL


P Idraw the town. But there is no 
brilliantly sung they are champagne; otherwise, 
swipes

Chaliapin is not at his best in Beethoven’s /7 
guesta tomba. 1 suppose this is a fine song, since 
so many good judges say it is. Those who differ 
from the good judges will not be converted by this 
performance of Chaliapin’s. He is better suited in 
Moussorgsky’s SZ// zs the Forest, but there is some 
want of clearness in the quieter Both 
these records are 12-in

passages




MADAME SUGGIA AT QUEEN’S HALI


B. V


BTR TIES


XUM


}.M.V


1922 


SIC, 


10W 


559


MUSIC AND THE ARMY


THE GARDE REPUBLICAINE 


THE ORIANA SOCIETY


HAROLD SAMUEI


22


KUTERPE


BATCH OF SINGERS


TO DR. HUGH BLAIR, AND THE CHOIR OF HOLY TRINITY, NARYLELON


ANTHEM FOR HARVEST OR GENERAL USE


LONDON


NOVELLO AND COMPANY, LIMITED


ORGAN - ’ 


——*_ €&-°-—-F 


ALTO > > A 


TENOR ; > > -— - 


222 > 2 = = | 


NIV 


OWI 


WW


ER __. A 


U.S.A


— _— —— A LN 


—_ _— ° 


XUM


THE GOD OF ABRAHAM PRAISE 


= - —_- - 4 


+} 4 —E — — 


=” = | SS 


== = = SS SS | | 


LL ____ — - | 


———_ —— A — 


1, 1922


AAN


THE GOD OF ABRAHAM PRAISE


XN - N 


CSS = === — SS ==. == = 


SS SS 


++ = | J - _ ‘ = SS — — — 


TT


S22 S “SSS FSS SSS


— LL 


PRESENTATION TO 


SIR FREDERICK BRIDGE 


BRITISH NATIONAL COMPANY’S


SEASON


CLOSE OF THE


NY’S


1922 569


ORPHEUS AT WARWICK


A. K


ROYAL COLLEGE OF ORGANISTS


THE


1922


RE


1922


NI


ASSOCIATES 


FELLOWS


FELLOWS 


FELLOWSHIP PAPER-WORK


ALAN GRAY


CHARLES MACPHERSON


ASSOCIATE PAPER-WORK


STANLEY MARCHANT. 


GEORGE J. BENNETT


ASSOCIATE ORGAN-WORK


E. T. SWEETING,. 


HOMAGE TO DR. JOHN BLOW


THE GREGORIAN ASSOCIATION


THE ORGAN


XUM


573


WESTMINSTER CATHEDRAL ORGAN


WHITEHAVEN RURIDECANAL CHOIR FESTIVAL


NATIONAL UNION OF ORGANISTS


ORGAN RECITALS


574 THE MUSICAL


1922


OWLS


APPOINTMENTSKeen lovers of music are cordially invited to join the East 
London Orchestral and Choral Society. Patrons: Madame 
Elsa Stralia, the Mayor of Stepney, Isador Epstein, 
Esq., &c.—For full particulars please write SECRETARY, 
E. L. O. S., 98, Whitechapel Road, London, E.1

Wanted for trio, ‘cellist for mutual practice, standard, 
Beethoven, Gade, Hurlstone trios. —Apply Sec., 
17, Curzon Street, Wolverhampton. Also members 
wanted for new Y.M.C.A. amateur orchestra.—Apply, 
Y.M.C.A., Lichfield Street, Wolverhampton, or above 
address

Pianist (twenty) would like to meet instrumentalist (male) 
for mutual practice. Pendleton district, Manchester.— 
H. B., c/o Alusical Times




29 


32


S.E.4


TRINITY COLLEGE EXAMINATIONS IN


INDIA


XUM


INDIA


‘THE BALLAD IN AMERICA


LEIPSIC’ OR *LEIPZIG


OUR DECADENCES1r,—I am a constant reader of your journal, and have 
perused with considerable interest, but some _ bewilder- 
ment, Mr. Rutland Boughton’s article in your July issue on 
“Our Decadence.’ As a non-musician, but a sincere lover 
of the art, I must confess, with all respect, that I am 
unable to follow Mr. Boughton’s reasoning in the ninth 
paragraph of his article, particularly the concluding 
sentences. He says

Now I firmly believe that the spirit informing the 
music of Beethoven is, work for work, a greater spirit 
than that which informs the music of Mozart. But it 
is the spirit of passion, egoism, rebellion, and severance ; 
and it tends to dissolution even in its creativeness

Ilow can music which exhibits the bad qualities just 
named be greater than music which is, admittedly, free 
from such blemishes? Mr. Boughton believes that music 
went astray after Mozart’s death, and that the present cult of 
cacophony and the ultra-modern movement, headed by 
Stravinsky, owe their existence to the bad example set by 
that misdirected genius, Beethoven! Now, if I am not 
mistaken, it is just Stravinsky and his followers who utterly 
repudiate Beethoven’s methods, asserting that he wrote no 
music ; they would relegate all his works to the lumber-room, 
avowing themselves worshippers of Mozart! Possibly, 
like many professing Christians, their faith does not always 
appear to conform to their practice

I possess a little book on Beethoven’s works by a 
distinguished English professor of music, who doubtless 
knows what he is writing about and has studied his subject 
critically. In the final chapter, entitled * Beethoven’s Music 
as a Whole’ he says

Like all great creators, Beethoven came not to 
destroy but to fulfil: in what one might call the legal 
sense of the words, he neither abandoned nor invented 
anything. . But with all this access of feeling 
Beethoven preserved throughout his life the sense of 
the supreme importance of form—not as meaning mere 
mechanism, but rather that balanced unity of design 
without which all artistic expression of emotion is but 
aimless and wasted hysteria

The same writer also alludes to the ‘ formal perfection’ 
of this composer’s works. In my opinion, this point 
of view is diametrically opposed to Mr. Rutland Boughton’s 
conception of Beethoven’s art, hence I am frankly puzzled. 
Speaking personally (and I do not think mine is an isolated 
case), I never come away after hearing a genuinely great 
work of Beethoven’s finely performed without feeling 
mentally refreshed and invigorated, or without a deepened 
conviction that there is indeed a ‘ Divinity that doth shape 
our ends.” Mr. Boughton asserts that Beethoven’s music 
is passionate from the outset. If it is admitted that 
Beethoven, like Shakespeare, held the mirror up to Nature 
and Man, then the reflection should be a true and 
faithful one. Passion exists side by side with other nobler 
and more spiritual qualities, in its due proportion. I am 
thus constrained to think that any decadence in modern 
music may be traceable not so much to a master of the past, 
but rather to the present influences of our materialistic and 
machine-ridden age.—Yours, Xc

Pall Mall, S.W.1. 
July 17, 1922

PERPLEXED

Sir,—It must have surprised other readers beside 
myself to read in Mr. Rutland Boughtun’s article on ‘Our 
Decadence ’ in your last issue, the statement that Beethoven 
sought self-expression rather than the service of art ; and 
this made, seemingly, an adverse criticism. We had 
thought that self-expression through the medium of art, and 
the service of art, were synonymous, and that a man might 
be a good craftsman and have excellent taste, but if his 
work did not express some part—if not the whole—of 
himself, it could hardly be called good art

The question arises, ‘What is the self?’ It is difficult to 
answer, but as soon as we consider the matter we see that 
the self must be much more complex than is ordinarily 
assumed. There seem to be various levels of consciousness, 
some of which are exclusively individual, others not so, but 
ever-widening as we penetrate further, till at length is 
reached by some few that region where all limits seem to 
have been passed—the self is felt to be limitless, and 
universal con-ciou-ness is attained




THE MUSICAL


3761922

Most musicians, I should think, would agree that 
Beethoven produced works that could be placed respectively 
in these three categories—the good, the great, and the 
supreme.— Yours, Xc., ARTHUR PHILLIPS, 
Harrow Road, W.2 
July 7, 1922

21c




THE THREE CHOIRS FESTIVAL


“GEORGE R


ROLAND AUSTIN 


PARRY MEMORIAL FUND


1922 577


‘INSTRUMENTATION: SOME STRANGE 


SURVIVALS


PLAYER-PIANO PROJECTSBRENT CHILTON

BEETHOVEN’S ‘JARRING A FLAT

Sir, — Writing in your July number, Mr. Rutland 
Boughton permits himself to make the following remark : 
‘Some of Beethoven’s ineptitudes[!] were probably due 
to carelessness,’ and mentions in support of his extra- 
ordinary contention ‘the jarring A flat’ in the Zrorca 
Symphony. Mr. Rutland Boughton is, then, apparently 
quite ignorant of the historic fact that the famous ‘jarring 
A flat’ in the sublime first movement of the Zvorca 
was no error on Beethoven’s part, and that he purposely 
put it there in order to produce the most wondrously 
humorous and entrancing effect to be found in the entire 
range of music.—Yours, Xc., ALGERNON ASHTON

22a, Carlton Vale, Maida Vale, N.W.6




A LIVERPOOL SETTLEMENT


CHARLES P. D. CANNON 


(B.A., A.RC.O., LRA MM). 


W. P. HENINGHAM


DUPRE AND ENGLISH ORGAN MUSI


THE ORIGIN THE BATON


OF


WALLACE


WILLIAM 


THE RAFF CENTENARY


JEFFREY PULVER


NEW FIDDLES FOR OLD


FARRELL


LACK


VER


LL


MEMORIAL WINDOW TO THE LATE VINCENT NOVELLO, 


ROYAL ACADEMY OF MUSIC 


THE CENTENARY CELEBRATIONS


THE MUSICAL


580


I 1922


OPERA AND DRAMA 


SERVICE AT ST. 17) 


THE RECEPTION


THE ORCHESTRAL CONCERTS


TRINITY COLLEGE OF MUSIC


THE U. G. M. AT CAMBRIDGE At ten o’clock, in the room of the U. M. C., Mr. Arthur 
Bliss gave an address on * The Musical Renaissance ’—a title 
that was very misleading, the subject of the paper being 
something quite different—namely, the present condition of | 
music in England. This condition the speaker evidently | 
regarded as eminently satisfactory. He appeared to be | 
under the impression that until quite recently executant | 
musicians of English birth had very little chance of success. | 
Coming to creative paid the customary 
homage to the memory of Henry Purcell—a homage which 
has been, perhaps, a trifle exaggerated. Purcell was 
undoubtedly a genius, but genius is a gift which has not 
been denied to some other English musicians, and if we say 
of Purcell that he was the greatest English composer of the 
second half of the 17th century, it may be that we have 
said enough

Mr. Bliss attached great importance to the cultivation of | 
folk-song, apparently regarding it as the principal means of 
musical salvation for this country. But here, again, some 
of us are inclined to think that folk-song has lately been 
worked a little bit too hard, It is true that a few com- 
posers seem to experience a trifling difficulty in the invention 
of melody, and in their case it is doubtless a convenience to 
be supplied with a melody ready-made. But we cannot 
help feeling, occasionally, that the peg is hardly strong 
enough to support all the clothes which are hung upon it. 
Mr. Bliss, who was obviously convinced of the absolute 
correctness of his views upon things in general, named the 
three men who, more than any others, had prepared the way 
for the present happy state of affairs—Parry, Stanford, and 
Elgar. The holder of the championship for the moment— 
until a*greater than he should arise, was Dr. Vaughan 
Williams. The labours of these men had, fortunately, 
rendered it impossible that any self-respecting person could 
any longer listen to such a work as 7he Golden Legend. In 
the realm of extravaganza, indeed, Sullivan having aimed 
low had succeeded in hitting the mark, but in all other 
respects was beneath contempt. By a quick transition (too 
abrupt to be termed a modulation) we were reminded how 
Rousseau had sought and found inspiration in the elevating 
and chaste lucubrations of Voltaire. After a few of the 
great masters, such as Haydn and Beethoven, had been 
called up for judgment and disposed of summarily, we were 
informed that music wasstill only a baby in the cradle, and 
that the more or less inarticulate noises with which she had 
hitherto regaled our ears were as nothing to the full tide of 
song which awaited us in the coming centuries. A 
peroration in praise of the steam engine followed, but I did 
not gather that the lecturer advocated its inclusion in the 
orchestra of the future

This breezy paper naturally led to a long and very 
amusing discussion. Dr. Alan Gray, while expressing his 
agreement with most of Mr. Bliss’s statements, put in a plea 
for Haydn, who, he thought, went deeper than Mozart. 
Another speaker wanted to know why music, which 
is as old as Genesis, is still only a baby. Others were 
desirous of learning the best method of inoculating their 
pupils with the latest virus. But, as is often the case on 
these occasions, while many addressed themselves to the 
subject of the paper itself, others were content with 
a comment on the remarks of the previous speaker whoever 
that might happen to be. A somewhat curious result of 
this was that we presently drifted back to one of Dr. 
Rootham’s topics of the previous afternoon, namely, the 
best method of teaching composition. However, when Mr. 
Bliss, in closing the discussion, told us that Schonberg, at 
Vienna (where even the hotel-waiters love string quartets) 
always ‘taught by line,’ I think we all felt thatthe subject 
was closed




MY SINGING LESSONS


XUM


BIRMINGHAM REPERTORY TIIEATRE


A MOZART SEASON


584 THEMUSICAL TIMES—AvucustT 1

HARROGATE.—Conducted by Mr. Howard Carr, the | 
Symphony Orchestra, on June 22, played Brahms’s first | 
Symphony, a symphonic poem, Owt of the Mist, by Miss 
Lilian Elkington, and Tchaikovsky’s /rancesca da Rimini 
Overture.——At the symphony concert on July 13 
Tchaikovsky’s Overture on the Danish National Anthem, 
two tone-poems by Eric Fogg—Sea-sheen and Fast the | 
lilac clover field—Bach’s Brandenburg Concerto in F, 
and Beethoven’s Zrotca Symphony were played

MANCHESTER. —On June 27 Miss Dorothy Crewe gave a_| 
pianoforte recital, playing Bach’s Chromatic Fantasy and | 
Fugue, a group of pieces by Scarlatti, and 7he Lament of | 
the lower nightingale, by Enrique Granados.——On | 
July 4 Madame Bella Baillie sang modern songs, including | 
two by Strauss—Dream in the twilight and Devotion, and 
Eric Fogg’s song from Tagore, One morning in the flower 
vardaen




CHAMBER MUSIC AT CALCUTTAattend musical performances by reason of the effects of the

climate on the instruments, and, incidentally, the performers 
themselves. All the more credit is, therefore, due to those 
who take part in such performances. At a series of 
six chamber concerts (one each month, commencing 
November 22, 1921), the following music was performed : 
String Quintet, Op. 39, in A, Glazounov; Pianoforte 
(Quintet, Op. 44, Schumann; String Quartets, Op. 12, 
in EF flat, Mendelssohn ; Noveiletten, Frank Bridge; in A, 
Borodin; Op. 76, in D, Haydn; Op. 135, in F, 
Beethoven; Pianoforte Quartets, Op. 25, in G minor, 
Brahms; Op. 15, in C minor, G. Fauré; two Violins 
and Pianoforte, Partita in G, J. S. Bach; Concerto in 
D minor, J. S. Bach ; Pianoforte Trios, Phantasy in A minor, 
John Ireland; Op. 99, Schubert; Trio in one movement, 
Armold Bax; Op. 70, No. I, in D, Beethoven ; Violin and 
Pianoforte Sonatas, Op. 100, in A, Brahms; in D, Mozart ; 
in G minor, Tartini; in A minor, John Ireland ; Violon 
cello Solos, Ao/ Nidret, Max Briich ; Spinnerlied, D. Popper ; 
Orientale, Arensky ; Sonata in D, Locatelli; Pianoforte

Fantasia in F minor, Chopin

On March 17, 1922, the following were again performed 
in St. Paul’s Cathedral, before a large and appreciative 
congregation: String (Juartet in F, Op. 135, Beethoven ; 
String (Quintet in A, Op. 39, Glazounov

MUSIC IN IRELAND




XUM


D. C. 


THE


1922 585


AMSTERDAM


BERLIN pesehited with instruments improved upon by the new 
| invention ; leading artists are giving chamber concerts upon 
| such instruments, and all agree that the quality of tone is 
| quite as good as if the artists played upon their own 
valuable instruments! All this is done for the sake of art

Berlin is at the present moment the city of the visiting 
conductor. Four of these Stabvirtuosen deserve special 
notice. Hugo Reichenberger, of Vienna, a man of strong 
| personal impulses, introduced to Berlin a Symphony in 
|D minor by Robert Heger, of Munich, a_ well-formed, 
| honest composition with a leaning towards the classical. 
| Franz von Hoesslin, of the Mannheim National Theatre, 
| an exceedingly conscientious, nervous leader with a strongly 
| personal note, conducted Beethoven’s C minor Symphony 
| and the Emperor Concerto, with Duscha Funke as pianist. 
| Friedrich ()uest, of Herford, made a very favourable 
|impression. His reading of Bruckner’s eighth Symphony 
| showed him a master of the orchestral apparatus. Finally, 
| Hermann Scherchen, now settled at Leipsic, and who is 
| one of the supporters of the modern school, broke a lance 
}in favour of Ernst Krenek, a pupil of Schreker. The 
| Symphony, in one movement, is a boldly-conceived work of 
| Strongly creative impulses, and does not copy the futuristic 
| stammering of the Schinberg school. There are elementary 
| musical forces seeking after untrammelled development, 
| yet this work, as well as Franz Petyrek’s Variations for 
| pianoforte on a Styrian melody—lasting half an hour—go to 
| prove that the anarchism of our time has swallowed up two 
hopeful composers

Altogether, the entire picture of Berlin musical life is one 
of haste and restlessness. With an excess of concerts, many 
of them superfluous, business is flourishing, and art, as has 
been pointed out before, is being cultivated in the smaller 
German towns only. The points of rest in all this hurry 
are the serial concerts of the various permanent orchestras, 
which live a real musical life. But even they are unable to 
| settle down to systematic work and study, Conductors come

whose members have unanimously chosen as their head 
Wilhelm Furtwingler, a full-blooded musician, who looks 
upon music as an animate factor and not as a formula

Considering the large number of first-class concert-halls of 
Berlin—among which the Philharmonic, the Oberlicht-Saal, 
| Beethoven-Saal, the Niinstlerhaus-, Bechstein-, Schwechten-, 
| Brahms-, Bliithner-, Klindworth-Scharwenka-, Zeurich-, 
Sezessions-, and Hochschulsaal are all well-patronised—it 
seems superfluous to add any more. Yet new opera houses 
| and concert-halls are constantly added to the old stock, the 
| latest being the August Firster-Saal in the Keith-Strasse, 
| 7.e., Berlin W.W., built and owned by a firm of pianoforte 
It was opened by Frederic

Lamond, who played compositions by Reger, Beethoven, 
Brahms, Chopin, and Liszt with his accustomed mastery

The abundance of music showered upon Berlin year by 
| year, regardless of principle, school, or style, is bewildering 
| and inartistic. Yet in spite of all this wealth of output the 
city by no means stands at the head of German musical 
life. Compositions that, owing to the support given to music 
| by princes and principalities, were formerly produced in 
{a small towns, often find their way to Berlin 
months and years later. Music is decentralised in Germany, 
and that is well for art

BEETHOVEN’s OP, ITI

As a birthday gift to the first great Beethoven Music 
Festival at Bonn since the war, the Drei Masken Verlag 
(Munich) has issued a facsimile edition of Beethoven’s 
last Pianoforte Sonata, Op. 111. It always fills one with 
feelings of awe when the eye rests on the handwriting of 
the master, but especially in the case of this last work, 
which he himself never hear: with the outer ear

RUDOLSTADT AND DONAUESCHINGEN




586 THE MUSICAL


COMPOSER


A NEW


OLD TREASURES UNEARTIIE


VIENNA


INTERNATIONAL NOVELTIES


XUM


587ANCIENT WORKS

Vienna recently witnessed what was virtually the first 
performance in our time of Beethoven’s original version 
of Fidelio, presented in concert form. Comparison 
between the first reading of this opera and the form which 
has been generally adopted for purposes of performance, not 
only disclosed the truly fantastic demands which the original 
version put upon the singers, but also fully justified all 
changes which Beethoven ultimately adopted. Nor could 
we, on hearing Cherubini’s rarely-performed Acgurem, 
approve the judgment of Beethoven, who is known to have 
considered it a masterpiece. To our modern taste it is a 
dull and dreary work, and, at best, of purely historical 
interest. Rubinstein’s biblical opera, 7he Zower of Babel, 
and the 137th Psalm, by Hermann Goetz, which were 
performed here in quick succession recently, are also, if 
anything, examples illustrative of a taste which is no 
longer ours, and the Peri’s pilgrimage for paradise which is 
the subject of Schumann’s oratorio, /’aradise and the Peri, 
is almost as vain as is this work’s quest for public 
favour. In spite of its many beautiful passages, this 
Schumann work lacks the strength and dramatic vigour 
whereby to hold the hearer’s interest for an entire evening

An ancient work which was yet, in a sense, a novelty, and 
whose performance has been a unique experience, is an 
Overture in C minor by Anton Bruckner which now, 
twenty-six vears after its composer’s death, received its 
belated premitre. This Overture, written by Bruckner 
more than fifty years ago, was recently discovered among 
his manuscripts; it is not to be counted with his greatest 
or most significant compositions




SOLOISTStruly products of our time. Three of these new 
pianists have created veritable sensations at Vienna 
this season: .\lexander Borowsky (who is a Russian

Eduard Erdman, and Walter Giesking, radical modernists 
all three, and all three equally remarkable each in 
his own individual way. Borowsky is, perhaps, the 
most intellectual and Erdman the most temperamental 
among the three, and Giesking the most polished. 
Carl Flesch’s mastery as a violinist is still unsurpassed ; 
he excelled in his wonderfully transparent reading of 
Beethoven’s Concerto, and even succeeded in lending a 
seeming importance to a new Violin Concerto, Op. 27, by 
Ernst von Dohnanyi, which is rather a harmless affair. 
Great success fell also to Alma Moéddie, the Australian 
violinist, while Willy Burmester’s violin playing again 
confirmed the impression that his art is chiefly a matter of 
cold technique. An event of towering importance was 
the farewell appearance of Prof. Karl Straube who, at 
St. Thomas’s Church at Leipsic, holds the position of 
‘Cantor’ once occupied by Johann Sebastian Bach, and 
whose organ recital again rallied a large and enthusiastic 
assembly composed of Vienna’s most cultured musical 
element

GUSTAV MAHLER




I 1922


CHOOL SONGS


DURING THE LAST MONTH


XUM