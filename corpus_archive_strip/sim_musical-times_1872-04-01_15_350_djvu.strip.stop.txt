


 K MUSICAL TIMES 


 HBNRY W. GOODBAN 


 37 , GREAT MARLBOROUGH STREET , LONDON , W 


 PROFESSIONAL NOTICES 


 MISS ELLEN HORNE 


 MR . HENRY WATSON 


 MR . WILBYE COOPER 


 MR . J. TILLEARD , 


 RUSSELL MUSICAL INSTRUMENTS . 


 RASS , REED , STRING , DRUM FIFE 


 EAN CHEAP MUSICAL INSTRUMENTS 


 ( SATE UL — COMPORTING 


 427 


 ROSEWOOD GRAND SQUARE PIANOFORTE 


 RGAN , PIANOFORTE , HARMONIUM , 


 PASCALL VOICE JUJUBES 


 ANGLICAN 


 PSALTER CHANTS 


 SINGLE DOUBLE 


 EDITED 


 EDWIN GEORGE MONK 


 LONDON : NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 85 , POULTRY ( E.C. ) . NEW YORK : 751 , BROADWAY 


 PRESS NOVELLO EDITIONS 


 MENDELSSOHN LIEDER OHNE WORTE 


 NOVELLO FOLIO EDITION 


 MENDELSSOHN LIEDER OHNE WORTE 


 NOVELLO OCTAVO EDITION 


 MENDELSSOHN LIEDER OHNE WORTE 


 NOVELLO COMPLETE FOLIO EDITION 


 MENDELSSOHN PIANOFORTE WORKS 


 NOVELLO OCTAVO EDITION 


 MENDELSSOHN PIANOFORTE WORKS 


 MENDELSSOHN LIEDER OHNE WORTE 


 MENDELSSOHN LIEDER OHNE WORTE 


 MENDELSSOHN NEW CAPRICCIO 


 E MINOR . 


 MENDELSSOHN ALBUM - BLATT 


 LONDON : NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C. ) . 


 NEW YORK , 751 , BROADWAY 


 429 


 MUSIC ASCENSION DAY 


 MUSIC WHIT SUNDAY 


 AYES 


 MUSIC TRINITY SUNDAY . 


 C. JEFFERYS , 57 , BERNERS ST 


 CHORAL SOCIETY 


 COLLECTION 


 EDITED 


 MICHAEL WATSON 


 CSCOBNAMAR OD 


 POPULAR 


 SOLOS DUETS 


 


 


 FELIX GANTIER 


 STEPHEN GLOVER 


 POPULAR MARCHES 


 LOUIS DUPUIS 


 CATHEDRAL GEMS1 . NantEs.—Fantasia , subjects Mozart 12th Mass 
 2 . Wurms.—Fantasia , subjects Weber Mass G 
 8 . RoTTERDAM . — Fantasia , subjects Haydn 3rd 

 Imperial ) Mass ... 
 4 , nee — Fantasia , subjects Beethoven Mass 

 wwrrnppn ow 
 SOSSKSAMQM O&O 




 C. JEFFERYS 57 , BERNERS STREET ( W 


 430 


 MUSICAL TIMES , 


 EXETER HALL , 


 FOURTH SEASON , 1871 - 2 , 


 NINTH SUBSCRIPTION CONCERT 


 WEDNESDAY , APRIL 10 , 1872 , 


 MENDELSSOHN 


 MADAME RUDERSDORFR 


 MADAME BENTHAM - FERNANDEZ , 


 MR . SIMS REEVES , 


 HERR STOCKHAUSEN 


 EXETER HALL | 


 FOURTH SEASON , 1871 - 2 


 SAINT PAUL 


 MADAME LEMMENS - SHERRINGTON . 


 MISS JULIA ELTON . 


 MR , SIMS REEVES . 


 MR . WHITNEY 


 431 


 MUSICAL TIMES , 


 APRIL 1 , 1872 


 DISTANT MUSIC 


 432certificates old ; composers cau- 
 tious allowing influenced adula- 
 tions local papers . criticisms provincial press 
 country rarely written persons ac- 
 quainted art , proof mention 
 newspaper lately reached , panegyric 
 performers , vocal instrumental , took 

 rt concert , writer metaphorically pats Beethoven 
 saying song “ ‘ Adelaida ” composi- 
 tion “ ‘ containing pleasant surprises 

 Pursuing researches state “ ‘ Distant Music , ” 
 learn Adelaide , Philharmonic Society 
 prosperous condition , Ballarat best Oratorios 
 given “ Harmonic Society ” 
 success . Tasmania hear excellent concerts ; 
 , incidentally mentioned ( 
 scope present musical survey ) Hankow , 
 China , Easter Sunday , 1869 , “ Hallelujah Chorus ” 
 sung anthem sopranos , alto , tenors , 
 basses , “ went exceedingly . ” 
 Madras , Philharmonic Society time 
 existence year 1865 , Sir Hope Grant given 
 100 rupees “ Instrument Fund . ” satis- 
 factory news progress art Natal ; 
 Cape Town , “ Good Hope Choral Union ” constantl 
 performing works ‘ “ Messiah , ” “ St. Paul , ” 
 « “ Mount Olives , ” & c. , aconcert “ Intermediate Tonic 
 Sol - fa Class , ” direction Mr. J. H. Ashley , 
 spoken terms favour Cape Town paper . 
 Port Elizabeth , , reports musical 
 entertainments , ‘ ‘ Monster concert , ” given Town - hall 
 Mr. Edward Newbatt , having largely attended , 
 highly successful . West Indies 
 informed music zealously cultivated , earnest 
 mentioned Barbados Philharmonic 
 Society contemplation found vocal instru- 
 mental schools . Quebec receive interesting 
 musical news ; Montreal series vocal instru- 
 mental concerts given Nordheimer Hall , 1867 , 
 followed , doubt , equally interesting 
 character 

 433 

 7 
 firing cannons , electricity con- 
 ctor stand , fine effect , instantaneous 
 sharge beat measure chorus , 
 compared striking large drum . 
 chorus , ’ ‘ Il Trovatore , ’ chorus , 
 chestta , artillery ( outside ) accompaniment , anvil 
 formed 100 anvils Boston Fire - brigade , 
 excellent , - demanded acclamations . ” 
 , , undoubted proof 
 Byiiding adapted music , music 
 Biisnted building ; 
 slimited number listeners , 
 limited number performers . , , 
 olos soprano voice sung ‘ young ladies , ” 
 d ut , vocal instrumental , multiplied 
 ‘ Hoportion ; anvils , sledge - hammers , artillery , 
 ihe “ additional instruments , ” whicn feeble scores 
 Beethoven , Mendelssohn , modern composers 
 jp enriched Festival use . hope believe 
 America destructive idea further- 
 pee pure art , healthy reaction place 
 shen renewed experiments clearly demonstrate real 
 ajoyment music dependent power hearing 
 good 
 nd healthy music prominent place 

 pel gistic gatherings , sincerely wish Americans 




 BACH JOHANNES PASSION . 


 BACH PASSION MUSIC WESTMINSTER ABBEY 


 434 


 CRYSTAL PALACEPHILHARMONIC SOCIETY 

 Tue sixtieth season Society commenced St. 
 James Hall 20th ult . , , glad find , 
 increased subscription , consequent doubt , re- 
 moval locality popular aristocratic 
 audience appealed . Symphony D , 
 late Mr. Cipriani otter , welcome thoroughly 
 artistic work complimentary tribute 
 England accomplished composers . instrumenta- 
 tion Symphony masterly extreme , 
 profitably studied delude 
 belief effect gained redundancy scoring . 
 orchestral works Mendelssohn Scotch 
 Symphony , Beethoven Overture ‘ “ Leonore ” ( . 1 ) , 
 Weber Overture “ Der Freischiitz , ” went 
 , steady baton Mr. W. G. Cusins . Herr 
 Bagheer highly favourable appearance 
 violinist , creditable seeing Herr Joachim 
 played concert . brilliant confident 
 performer , Tartini “ ‘ Trillo del Diavolo ” 

 “ Duo concertante , ” Spohr ( Herr Joachim ) elicited 
 warm deserved applause . Madame Peschka - Leutner 
 ( Leipsic ) displayed fine voice dashing style 
 execution Spohr Scena , “ Tu m ’ abbandoni , ” 
 like hear pronouncing 
 capabilities . Madame Patey vocalist , 
 created marked effect Gounod popular sacred song , 
 “ green hill far away 




 ORATORIO CONCERTS 


 MR . HENRY LESLIE CONCERTS 


 435ad Herr Joachim played masterly style Romances 

 Beethoven , second response enthusiastic 
 facore , Spohr ‘ ‘ Andante , ” 9th concerto 

 y 




 ; CORRESPONDENT . 


 436THE MUSICAL TIMES.—Apnziz 1 , 1872 

 21st February , Mr. Walter Macfarren performance 
 pianoforte Sonatain F , Paradies , interesting 
 item inthe programme . “ ‘ music future ” 
 steadily progressing , good “ ‘ music 
 past ” forgotten ; shall 
 glad find resuscitation works placed 
 able hands . reception Sonata proved Mr. 
 Macfarren miscalculated taste audience . 
 concerts series promised 
 best specimens chamber - music Beethoven , 
 Mendelssohn , Spohr , Mozart , Schumann , & c. ; 
 works living composers , glad find 
 & . Silas 

 Tue Prospectus Royal Italian Opera present 
 season ( commenced 26th ult . ) interest 
 apart promises - known works , supported 
 - known artists , Wagner ‘ ‘ Lohengrin ” ’ 
 opera placed list , anticipated 
 spirited lessee trial remarkable com- 
 poser English jury prove important 
 event session . Lovers musical progress unite 
 thanking Mr. Gye boldly challenging opinion 
 subscribers public “ music 
 future ; ’’ , mere commercial speculation , 
 question 

 lieta ’’ selected contrast style ; 
 glad hear vocal pieces 

 importance . Herr Ries gets Beethoven Romane 
 F usual skill ; songs Mr. 
 Thomas completed enjoyable little concert 

 Tue Birmingham Amateur Harmonic Association 
 nounces concerts , commencing 14th , t 
 programmes include Beethoven “ Mount 
 Olives , ” Handel “ Esther , ” Hummel Mass , # 
 Sullivan Cantata , ‘ ‘ Shore Sea . ” yearl 
 Association produced Handel “ Jephtha , ” tt 
 Birmingham , forthcoming performances 
 comprise works little known , seen ® 
 concerts deserving*of highest patronage 

 Tue 157th Annual Festival Dinner “ Ms 
 Honourable Loyal Society Ancient . Britons , 
 celebrated St. David Day Willis Rooms , St. Jame 
 presidency Right Hon . Lord Justice Jami 
 object festival promote interests d j 
 Welsh Charity School , situated Ashford , Middle 
 dinner selection music played bi 
 Royal Artillery , including “ ‘ Carmarthen Mareh , 
 “ Glamorgan March , ” “ March Men Harlech , 
 & c. grace dinner “ Clod Dduw fyddo bye 
 ei vaeth vendithion ’ oll , ” arranged ancient Wa 
 air Mr. Brinley Richards , sung great effect te 
 choir , conducted Mr. William Davis ( Mynorydd ) . Aftert 
 chairman given health Queen , ‘ ‘ God savel 
 Queen ” sung Miss Edith Wynne , Miss Severt , 
 choir . toast , ‘ Prince Princess 
 rest Royal Family , ” followed 




 437 


 MADRIGAL VOICES 


 SS 


 SSS = SSS 


 NY 


 SS 


 


 SING LULLABY 


 4 1 


 NK 


 PP 


 443 


 444 


 445 


 EDITOR MUSICAL TIMES 


 JOHN STAINER 


 EDITOR MUSICAL TIMES 


 33 


 EDITOR MUSICAL TIMES 


 EDITOR MUSICAL TIMES 


 EDITOR MUSICAL TIMES 


 EDITOR MUSICAL TIMES . 


 CORRESPONDENTSWe hold responsible opinions expressed 
 Summary ; notices collated local papers , 
 supplied tous occasional corresp.ndents 

 BLacKPooL.—On Monday evening , 26th February , Blackpool 
 Vocal Society gave second concert season Assembly 
 Rooms , Talbot - road , large audience . selection sacred music 
 formed programme , included portion 
 Mozart Twelfth Mass , ‘ ‘ love hath Father " ( Mendelssohn ) , 
 duet chorus , ‘ ‘ O bow ” ( Handel ) , “ Halle- 
 lujah ” ( Beethoven ) . second comprised selection secular 
 songs , choruses , & c. principal vocalists Miss Poole , Miss 
 Webbe , Mrs. Maries , Miss Grindrod , Messrs. Birtland , Gardner , Kay , 
 Waddington , Johnson , Stanton ; accompanists , Mr. Grindrod 
 ( pianoforte ) , Mr. J. G. Wrigley , Rochdale ( harmonium ) ; con- 
 ductor , Mr. N. Dumville , Manchester . choruses , 
 , sung , reflected great credit conductor . Miss 
 Grindrod Sullivan air , ‘ ‘ Love world , ” Mr. Stanton 
 Haydn “ Rolling foaming billows , ” highly successful ; 
 Mr. Gardner received encore singing Loder ‘ ‘ Thanks- 
 giving song . ” Jackson glee , “ sisters sea , ” 
 greatly applauded . instrumental piece duo har- 
 monium pianoforte , Ch . Gounod , ‘ ‘ Hymne 2 Sainte Cecile , ” 
 excellently rendered Messrs. Grindrod Wrigley . 
 “ God save Queen ” concluded concert 

 Boottx.—On 12th ult . , Mr. Charles Edward Horsley Liverpool 
 Ballad Madrigal Company gave highly successful concert 
 Molyneux Assembly Rooms . - singing excellent through- 
 , Mr. Horsley pianoforte solos loudly emg 
 applauded . Miss Marie Arthur sang expression “ Wi 
 verdure clad , ” Miss Nicholls ‘ ‘ O rest Lord , ” Mr. C. Wilson 
 ballad ‘ * lips , ” Mr. T. J. Hughes aria 
 Horsley Joseph , “ Oh head waters , ” arecit . 
 air , “ Song Martyr , ” Mr. Alfred Phillips ( duo piano- 
 forte harmonium accompaniment Mr. Horsley Mr. W. J. 
 Argent ) received favour 




 448direction Mr. Whomes . vocalists Miss Jones , R.A.M. , 
 Mr. Arthur Thomas , Mr. Chaplin Henry ; solo trumpet , Mr. 
 Dearden . performance highly successful 

 Corx.—The second concert Cork Musical Society given 
 5th ult . , Imperial Room , crowded , . 
 programme included selection Beethoven Mass C , 

 rincipal parts excellently sustained Miss Lambkin , 
 eae . Harvey , Scott , Seymour , E. Hackett . ‘ ‘ Benedictus , ” 
 especialiy , charmingly sung , choruses given 
 commendable precision care . miscellaneous , Miss 
 Lambkin gave highly successful rendering Weber “ ‘ Softly sighs , ” 
 vocal pieces contributed Mrs. Baily , Mrs. Empson , 
 Miss Morgan , Misses Corbett , Messrs. Harvey , Scott . Hackett . 
 orchestral pieces excellently played , able di- 
 rection Dr. Marks ; trombone solo Mr. Dittrich created 
 marked effect audience 

 special Hymn , ‘ “ O Thou , soul salvation , ” sung con- 
 gregation , numbered thousand persons , including 
 local clergy , surplices . collection aid St , 
 Paul Restoration Fund . St. George Church music wag 
 performed , leadership Dr. Spark , organist church , 
 churches special choral services evening 

 LiverPooL.—Messrs . Horsley Thomas Classical Recitals 
 Dreaper Rooms manifestly gaining interest publig 
 support . programme 2nd ult . included Beethoven Sonata 
 minor , excerpts Concerto Spohr , piano 
 violin . interesting item programme Moscheles ’ fing 
 duet pianofortes , ‘ ‘ Hommage 2 Handel , ” admirably 
 played original form Messrs. Horsley A. W. Borst . * 
 vocalist Mrs. Billinie Porter . selection comprised Sullivan 
 “ Orpheus lute , ” canzonet Spohr , violoncello obbii- 
 gato , new song , “ thee , ” composed Mr , 
 Horsley . better described Scena , , 
 form , afforded ample opportunity display Mr , 
 Horsley power higher walks art . Mrs. Porter 
 adequate justice new work , , , successful 
 undertook . performance concluded trio Mr. Horsley , 
 performed author , assisted Mr. Thomas Mr. Haddock , 
 known local violoncellist . duties accompanist dis- 
 charged Mr. Wrigley . — — Tue Subscription Concert Phil- 
 harmonic Society , 5th ult . , devoted Mozart Requiem 
 Mendelssohn Lobg g. principal artists Requiem 
 Miss Edith Wynne , Miss Chadwick , Mr. Maas , Mr. J. G. Patey ; 
 Lobgesang , Miss Edith Wynne , Miss Green , Mr. Maas , 
 duet , ‘ ‘ waited Lord , ” encored , choruses 
 adagio Sinf applauded.——T ue fourth concluding 
 concert present series , plan London Monday Popular 
 Concerts , given Philharmonic Hall 6th ult . 
 Executants : violin , Herr Joachim ; second violin , Herr L , Ries ; 
 viola , Mr. Zerbini ; violoncello , Signor Piatti ; solo pianoforte , Mdme , 
 Schumann ; vocalist , Mr. Edward Lloyd ; accompanist , Mr. Zerbini . 
 instrumental portion programme consisted Beethoven 
 String Quartett C major ( op . 59 , . 3 ) , Schumann Quartett E 
 flat ( op . 47 ) . pianoforte , violin , viola , violoncello , Haydn String 
 Quartett G major ( op 76 , . 1 ) , Beethoven solo Sonatain 
 major ( op . 101 ) , pianoforte , finely rendered Madame 
 Schumann . vocal pieces excellently given 
 received , “ Celia arbour , ” Schubert “ ‘ Post ” re- 
 demanded.——TueE Societa Armonica gave thirty - eighth open re- 
 hearsal Liverpool Institute , Mount - street , Saturday evening , 
 9th ult . , large audience . programme comprised 
 overture De Sargino ( Paer ) ; Kyrie Gloria , Mass E 
 flat , . 2 ( Hummel ) ; Symphony C ( MS . ) ( Henry Gadsby ) ; Chorus , 
 ‘ Susceptible hearts , ” Ruins Athens ( Beethoven ) ; Adagio , 
 Symphony E flat ( F. Lohr ) ; March Chorus , ‘ ‘ Twine ye 
 garlands , ” Ruins Athens . instrumental 
 performance highly successful , prominent feature Mr , 
 Gadsby Symphony , performed time Liver- 
 pool . chorus , limited , effective . vocal soloist 
 Madame Billinie Porter ( daughter talented conductor , Mr. 
 Armstrong ) . sang , finished style , recitative aria 
 Eli , “ exalt Thee , ” ‘ thee , ” written expressly 
 Mr. C. E. Horsley . Mr. Lawson effective leader ; 
 success rehearsal attributed devoted 
 attention Mr. Armstrong , conductor.——The Fourth Subscription 
 Concert Philharmonic Society 19th ult . , devoted 
 performance Mendelssohn Llijah . Principal artists , 
 Madame Titiens , Mad Bentham - Fernandez , Mr. Bentham , 
 Herr Stockhausen . additional voices quartetts trios 
 supplied members Society — Miss M. Philipps Mrs. 
 Keef , Mr. Foulkes , Mrs. Armstrong ; unfortunate indis- 
 position Madame Titiens preventing fulfilling 
 arduous soprano , place , portions 
 work , ably filled Miss Edith Wynne . choruses went 
 great spirit , general effect excellent . good 
 word said Madame Bentham charming rendering 
 * O rest Lord 

 Lonpon , OntTar10.—On 22nd February , sacred secular 
 concert , aid funds Church England Young Men 
 Association , given City Hall success . Miss 
 Fanny Chatfield , Ladies ’ College , created marked impression 
 singing air “ beautiful feet , ” 
 bright little song Randegger , called ‘ ‘ Marinella . ” voice 
 excellent , execution displays high state culture , encore 
 elicited - named composition earned 
 legitimate means . Miss Williams , Miss E. Raymond , Mrs. Crow , 
 Messrs. Beaton , Coles , Fewings , E. Plummer , highly 
 effective solo music ; Miss Hall ( Ladies ’ College ) 
 gained - deserved applause performance solos 
 pianoforte . choruses carefully rendered , 
 Mrs. Raymond conducted concert ability 




 449 


 MONTA 


 450 


 M\HE ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 OULE COLLECTION 527 CHANTS , 57 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 ORDER HOLY COMMUNION , 


 TONIO SOL - FA RDITION . 


 READY , 


 DWARD J. HOPKINS THANKSGIVING 


 TFARK ! " LIS BREEZE TWILIGHT 


 NOVELLO OCTAVO EDITION OPERAS 


 NEW NUMBERS 


 SCORES GREAT MASTERS 


 GLEES MEN VOICES . . 82.—Chaconne ( F major ) . Handel ; Andante con Variazioni , 
 Romanza ( Op . 3 . ) , Weber 

 . 83.—Funeral March ( Op . 26 ) , Beethoven ; March ( B minor , Op . 
 oe Hunting Song ( Op . 82 ) , Schumann ; Adagio ( Op . 10 ) , 
 eber 

 . 84.—Fugue ( G minor ) , Mozart ; Marche Triomphale , Mos- 
 cheles ; Fantasia , Sixth Quartet , Haydn 




 MENDELSSOHN 


 LIEDER OHNE WORTE 


 ARRANGEMENT PLAYED 


 MADAME SCHUMANN . 


 GAVOTTE C. W. GLUCK , 


 JOHANNES BRAHMS 


 FIFTEENTH EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT SINGING MANUAL 


 ( COMPANION WORK 


 COLLEGIATE VOCAL TUTOR 


 MR . VERNON RIGBY ' NEW SONG . 


 NOVELLO - SONG BOOK . 


 ( SECOND SERIES 


 COLLECTION 


 WALTER MACFARREN 


 NOVELLO ORIGINAL OCTAVO EDITION 


 ACCORDING 8S. JOHN . ) 


 


 E MINOR 


 ANTHEMS . 


 CHORAL SOCIETIES . 


 W. H. BIRCH CHRISTMAS MUSIC . 


 EDITED 


 WM . SPARK , MUS . DOCG 


 NATI 


 CRYSTAL PALACE 


 NATIONAL MUSIC MEETINGS 


 OVELLO , EWER CO . EDITION PIECES 


 PREPARED COMPETITION 


 


 WILLEM COEN 


 EN 


 PIANO . 


 454 


 62 


 74 


 76 


 79 


 61 


 63 . 


 64 . 


 65 


 66 


 67 


 68 


 69 


 70 


 71 


 72 


 73 


 15 


 80 


 CONTENTSLondon : Novello , Ewer Co 

 Bach 
 « . Spohr 
 Beethoven 
 Mozart 
 Haydn 

 Handel 
 Mozart 

 Mozart 
 Mendelssohn 

 Beethoven 
 Mendelssohn 

 F. Schubert 
 Handel 

 R. Schumann 

 Beethoven 
 F. Spindler 
 Handel 

 F. Chopin 

 Op . 94 , . 2 ) ag « . F , Schubert 
 Largo Pianoforte Sonata , major 

 . 2 , Op . 2 ) ase ane — Beethoven 
 Allegretto 4th Sonata ( Op . 70 ) ove Weber 
 Tema con Variazione Serenade ( Op . 8) ... Beethoven 
 Short Prelude Fugue C ove . Jd . L. Krebs 
 Fugue G minor , Pianoforte Works ... J.S. Bach 
 Fantasia F , composed Mechanical Organ Mozart 
 Sarabande Gavotte E ( Suites Francaises ) ... Bach 
 Gavotte G ditto vie Bach 
 Gavotte Musette G minor ( Suites Anglaises ) Bach 
 Sarabande F ditto spe Bach 
 Sarabande Bouréein minor ditto eee Bach 
 Gavotte Musette D minor ditto ose Bach 

 78 . Chorus , “ ‘ Music spread thy voice ” ( Solomon ) Handel 
 Chanson Orientale ( Op . 66 , No.4 ) ... R. Schumann 
 Fugue B minor ars eee Albrechtsberger 
 Fugue F ( Suite de Piéces ) oho ose Handel 
 Chorus , “ Hallelujah ” ( Occasional Oratorio ) ... Handel 
 Minuetto ( Notturno , Wind instru 




 NOVELLO LIBRARY 


 DIFFUSION 


 HERUBINI TREATISE COUNTER 


 II . 


 R. MARX GENERAL MUSICAL INSTRU . 


 III . 


 ETIS ’ TREATISE CHOIR CHORMS 


 IV . 


 ATEL TREATISE HARMONY . 5 


 VII . 


 ERLIOZ ’ TREATISE MODERN INSTRU . 


 R. CROTCH ELEMENTS MUSICAL 


 IX . 


 ABILLA NOVELLO VOICE VOCAL 


 LI . 


 INK PRACTICAL ORGAN SCHOOL 


 LII . 


 UHR PAGANINI ART PLAYING 


 LIII . 


 ALKBRENNER METHOD LEARNING wry GS . 
 Crocus gathering - - - 2 6 ) heart sair somebody - 
 war - - - 2 6 Blow , blow , thou winter wind - 
 Oh , Maying Exile ’ s Song 
 Love , tarry - - 2 6)Six - Songs ( S.A.T.B. + , 8vo . , 
 Stars Voyager - 2 6 ) singly — 
 Sweetly glows early morn = - = = 2 6 ) Fairy Song = : ie 
 Ringlet-—No : 1 . Ringlets | Good night pes , anes 
 look golden gay = - 26 ... eh . : : : 
 . 2 . O Ringlet , eva night pg Heche 
 day 2 6 |Good mocow 0s 20 
 — _ 0 . 
 Sonate fiir Pianoforte und Violin , - 16 de 0;Marche . Op.13 - 
 Mamrka . Op . 11 - 0 } Ditto . Arranged Organ J. 
 Presto alla Tarantella . Op . 15 oe Stainer - - net 
 Bolero . Op . 9 - - - 4 0 ) Drei Clay ierstiicke — . 1 , Caprice . 
 Barcarolle . Op . 8 - - 3 0 . 2 , Auf dem wasser - - 
 Gavotte . Op . 14 - 2.2 . 3 , Scherzo - - 
 Ditto . Organ W. J. Westbrook net1 6 Spring Melody - . . 
 ARRANGEMENTS 

 Bourée Ep , J. S. Bach - - 2 6 ) Allegretto alla polacca , Beethoven 
 Bourée O , J. S. Bach - - 2 6 Serenade , Op . 8 , Violin , Viola 
 Gavotte G , J. 8 . Bach - 2 6 Violoncello 
 Second Concerto . Composed forthe Harp- Menuetto ditto , ditto , Op . 9 . : 2 

 sichord Organ G. F. Handel - 5 0/ . 4 R. Schumann Skizzen fir den 
 Scherzo Beethoven Trio , Violin , edal Fliigel eumeunine Pedal 

 Violaand Violoncello , Op . 9.No.1- 3 0 Pianoforte 




 SONGS 


 PIANO . 


 MUSIC 


 ORAXOO 


 ENTIRELY NEW WORK MUSICAL EDUCATION . 


 - . WALTER MAYNARD 


 MUSIC COPY - BOOKS 


 CONTAIN PROGRESSIVE COURSE INSTRUCTION MUSIC , 


 NEW WORK SINGING CLASSES 


 CHAPPELL PENNY OPERATIC - SONGS 


 NEW WORK ORGAN . CI 


 HANDEL CHORUSES 


 4 . GAVE HAILSTONES , 8 . LET BREAK BONDS . ;