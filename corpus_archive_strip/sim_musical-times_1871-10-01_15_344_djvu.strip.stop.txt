


 MUSICAL TIMES 


 EXETER HALL . 


 ORATORIO CONCERTS 


 FOURTH SEASON , 1871.2 


 BACH PASSION , 


 HANDEL MESSIAH , ISRAEL EGYPT , J EPHTHA , 


 JUDAS MACCABZUS , 


 ELIJAH . 


 MR . SIMS REEVES 


 NMR . SANTLEY . 


 1871 . WEDNESDAYS , NOVEMBER 15 , DECEMBER 6 , DECEMBER 20 . 


 1872 . TUESDAYS , JANUARY 20 , FEBRUARY 6 , FEBRUARY_20 , MARCH 5 , MARCH 19 


 WEDNESDAYS , APRIL 10 , APRIL 24 


 PROFESSIONAL NOTICES , 


 SIGNORA SCHLOSS , 


 MR . HENRY WATSON , 


 MR . H. P. G. BROOKE , 


 MR . J. TILLEARD 


 EYOND ALLCOMPETITION.—T. R. WILLIS , 


 USIC ENGRAVED , PRINTED , PUB 


 RUSSELL MUSICAL INSTRUMENTS . 


 RASS , REED , STRING , DRUM 


 MR . WALTER REEVES , 


 UARTERLY SALE UNREDEEMED 


 ROSEWOOD COTTAGE PIANOFORTE 


 CHARACTERS REPRESENTED 


 ACT I. 


 ACT II . 


 TIME PERFORMANCE HOUR MINUTES 


 W. H. BIRCH , 104 , LONDON STREET , READING 


 TWELFTH EDITION . 


 E COLLEG 


 SIGHT - SINGING MANUAL 


 EDITION 


 SINGING CARD 


 USE CHOIRS , NATIONAL VILLAGE 


 SCHOOLS , SINGING CLASSES . 


 GLEES MEN VOICES 


 METZLER CO . NEW LIST 


 EDITED RY 


 II . READY 


 CONTENTS 


 METZLER CO . 


 PENNY - SONGS 


 ARRANGED VOICES , 


 EDWARD F. RIMBAULT 


 EMY ROYAL MODERN ‘ PIANOFORTE 


 ATTON FAVOURITE CHRISTMAS AR- 


 PUBLISHED 


 ILLUSTRATED 


 ALBUM MUSIC 


 SERIES 


 NATIONAL GEMS 


 MUSICAL PICTORIAL 


 ARRANGED 


 LOUIS DUPUIS 


 LONDON 


 C. JEFFERYS , 57 , BERNERS STREET 


 5 9 = = * 


 C.J. 


 231 


 MUSICAL TIMES , 


 OCTOBER 1 , 1871 


 GLOUCESTER MUSICAL FESTIVAL . 


 232 


 233 


 234THE PASSION - PLAY OBER - AMMERGAU . 
 ( SPECIAL CORRESPONDENT 

 Arter Beethoven Festival Bonn , 
 sent account month , best , 
 way Munich , travelling ’ 
 night Frankfort arriving 

 236 MUSICAL TIMES.—Ocrosrr 1 , 1871 




 AW " 


 — — _ — — _ 


 MAIDEN “ FLEUR DE LYS 


 55 " = } = = SSS -5 2 ° 


 SSS SEF 


 44 - 7 


 SSF = = = = = = = 


 - @ 6 2 T 


 SS — Z — — — — — — — — — = 


 ENGLAND , FRANCE , PRUSSIA , AUSTRIA , BELGIUM , ITALY , AMERICA 


 AWARDED 


 JOHN BRINSMEAD & SONS 


 


 PERFECT CHECK REPEATER ” ACTION 


 PIANOFORTES 


 GUARANTEED YEARS 


 STEAM POWER WORKS 


 104 , CHENIES STREET ; 8 , LITTLE TORRINGTON STREET ; 


 243 


 244 


 246 


 EDITOR MUSICAL TIMES 


 OBSERVER 


 CORRESPONDENTSA . B.—We notice letter sent , 
 inserted , journal 

 C. Krott Laporte.—The interesting account Beethoven 
 moments , furnished correspondent , ap- 
 peared English ical publicati 

 Briel Summary Country PNelws 




 247Hallelujah Chorus evening given marked effect 
 close service . text morning sermon , 
 preached Incumbent , Psalm cxliv . , 13 , 14 , 
 evening , preached Rev. H. J. Wardell , 
 curate , Gospel day 

 SkELMoriLIZ.—On Friday , 25th August , evening 
 concert given members Skelmorlie Parish 
 Church Choir , management organist choir- 
 master , Mr. J. E. R. Senior , assisted Miss Fawcett , 
 Yorkshire concerts . choir rendered following pieces : — 
 “ Welcome Home , ” Mendelssohn - Songs , “ ‘ 
 Cuckoo , ” “ Spring fairy foot returning ” ( William Tell ) , * * 
 barley , ” ‘ ‘ Slow eastern sky , ” Gipsy 
 Chorus ( Preciosa ) . Miss Fawcett highly successful , re- 
 ceived - merited encore , song , “ ‘ margin fair 
 Zurich waters . ” duets , ‘ wander’d dreams , ” 
 * Home mountains , ” joined Mr. Senior , 
 played pianoforte solo , accompanied vocal music.—~—An 
 organ Recital given Mr. Senior church Wednes- 
 day , 30th August . programme included Overture , 
 Occasional Oratorio ( Hande ) ; “ Marche Solennelle ” ( Schu- 
 bert ) ; “ Toccata Fugue ” ( Bach ) ; Mendelssohn 5th Sonata ; 
 “ Harmonious Blacksmith , ” arranged Dr. Chipp ; War March 
 Priests ( Athalie ) ; “ Hallelujah ” Beethoven 
 Engedi solos anthems sung choir 
 Miss Fawcett , including ‘ ‘ O Lord God ” ( 8S. S. Wesley ) , 
 “ mighty kings , ” ‘ * Let bright Seraphim , ” ‘ * shall 
 fleetest ” ( Barnby ) , ‘ ‘ mourn dove ” ’ ( Benedict 

 SourHsEa.—An amateur concert given 
 Portland Hall om Wednesday evening , 6th ult . , aid 
 fund Organ St. Jude Church , recently 
 enlarged Messrs. Gray Davison . Mrs. Perey Smith 
 Mrs. Frank Conway Gordon sang exceedingly , Miss L. 
 Jeaffreson ( possesses good contralte voice ) highly 
 successful ‘ Batti Batti . ” Mr. C. FE . McCheane song 
 effectively rendered received decided encore 
 duet , “ ‘ Love War . ” Mr. Benson sang taste 




 MONTH , tata , “ ‘ Placida : Christian Martyr ; ” Mendelssohn “ Judge 

 , O God , ” “ Hear prayer ; ” Beethoven “ Halle 

 lujah , ” Royal Albert Hall , Dec. 5th . Immediate application 




 249 


 NEW EDITION , REVISED ENLARGED 


 ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 OULE CULLEC 527 CHANTS , 57 


 OULE DIRECTORIUM CHORI ANGLI- 


 JOULE 


 OULE DIRECTORIUM CHORI ANGLI- 


 ORDER HOLY COMMUNION , 


 READY . 


 UPPLEMENTAL HYMN TUNE BOOK , 


 TONIO SOL - FA RDITION . 


 READY , 


 NEW WORK CHURCH ORGANISTS 


 


 1 . DEST 


 16 


 19 


 ORGAN PIECES CHRISTMAS 


 NEW NUMBE2 contains — Chaconne ( F major ) , Handel ; Andante con Varia- 
 zioni , Romanza ( Op . 3 ) , Weber 

 83 contains — Funeral March ( Op . 26 ) , Beethoven ; March ( B 
 minor , Op . 27 ) , Schubert ; Hunting Song ( Op . 82 ) , Schumann ; 
 Adagio ‘ Op . 10 ) , Weber 

 84 contains — Fugue ( G minor ) , Mozart ; Marche Triomphale , 
 Moscheles ; Fantasia , Sixth Quartet , Haydn 




 CHARACTERISTIC PIECES 


 3 0 


 3 0 


 NOVELLO , EWER CO . COMPLETE 


 UNIFORM EDITION 


 MENDELSSOEUN 


 LADIES ’ VOICES 


 , SINGLY : — 


 NOVELLO LIBRARY 


 DIFFUSION 


 HERUBINIS TREATISE COUNTER . 


 R. MARX GENERAL MUSICAL INSTRUC . 


 ETIS ’ TREATISE CHOIR CHORUS 


 IV . 


 OZART SUCCINCT THOROUGH - BASS 


 VI . 


 VII . 


 ERLIOZ ’ TREATISE MODERN INSTRU- 


 VIII . 


 R. CROTCH ELEMENTS MUSICAL 


 ABILLA NOV ELLO VOICE VOCAL 


 LI . 


 INK PRACTICAL ORGAN SCHOOL . 


 UHR PAGANINI'S. ART PLAYING 


 LIII . 


 ALKBRENNER METHOD LEARNING 


 _ — — _ 


 STR HENRY R. BISHOP 


 LONDON : 


 NOVELLO , EWER CO . , 


 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 EDITED 


 WM . SPARK , MUS . DOG 


 PASTE SEPP 


 254Compositions Agnes Simmernvann 

 SONGS . 
 Crocus gathering - - - 2 6| heart sair somebody - 26 
 war - - - - 2 6 ) Blow , blow , thou winter wind - ae 
 Oh , Maying = 32 4 | Exile Song = 2 6 
 Love , tarry - - 2 6/)Six Four- -part Songs ( S.A.T.B. ) , 8vo . , 1s . , 
 Stars Voyager - 2 | singly — 2 Vocal 
 Sweetly glows early morn ~ 2 6 | Fairy Song ' Pa 7 06 
 Ringlet — . 1 . Ringlets ut night ~eh aa 
 ok golden gay » 2 6 - — ... ‘ ; : : ° : 6 
 ’ o wers . 
 . 2 . O Ringlet , kiss’d nig ht | Daffodils > aoe : 
 day ~ “ oe { Good morrow ... ~tf 13 ° oe 
 PIANO . 
 Sonate fiir Pianoforte und Violin , Op . 16 15 0|Marche . Op.13 - 2 6 
 Mazurka . Op . 11 - - 3 0 / Ditto . Arranged Organ z 
 Presto alla Tarantella . Op . 15 - 3 0 Stainer - - net 1 6 
 Bolero . Op . 9 - - - 4 0 ) Drei Clay ‘ erstticke — . 1,Caprice - 3 0 
 Barcarolle . Op . 8 - - 3 0 . 2 , Auf dem wasser - - 3 0 
 Gavotte . Op . 14 - - 2 0 . 3 , Scherzo - - 3 0 
 Ditto . Organ W. J. Westbrook net 1 6\Spring Melody - - - 290 
 ARRANGEMENTS . 
 Bourte EP , J. 8 . Bach - - 2 6 ) Allegretto alla polacca , Beethoven 
 Bourée C , J. 8 . Bach - - 2 6 Serenade , Op . 8 , Violin , Viola 
 Gavotte G , J. S. Bach - - 2 6 Violoncello - 3 0 
 Second Concerto . Coteus forthe Harp- Menuetto ditto , ditto , Op . 9.No:2 3 0 
 sichord Organ G. F. Handel- 5 0| . 4 R. Schumann Skizzen fiir den 
 Scherzo Beethoven Trio , Violin , pedal Fliigel ( Sketches Pedal 
 Viola Violoncello , Op . 9.No.1- 3 0 Pianoforte ) - - - 8 0 
 j ae | | qi , 
 Compositions Merthold Cours , 
 SONGS . 
 Tears Childhood- - - 8 O);Awish - - 20 
 ( Sang hy Miss REG Wyune ) , | shall picture thee , ladye fair - 3 0 
 Blossoms - aye iid | woman love bought - 26 
 Stars Summer night - - 8 . O16 wother dear good night - . = 
 Sea hath pearls - - - 3 0| ’ ( Sung Madame Patey ) . 
 PIANO . 
 Berceuse : - - 8 0)Séraphine . Morceau de Salon ’ - - 40 
 Juvenile Album , containing | Ephéméron . Caprice Etude - - 3 0 
 characteristic pieces hands , Sonatina . Dedicated Little Players - 4 0 
 intended played Master Dreaming . Sketch - - 3 0 
 Pupil ( primo kept Deux Esquisses ( en forme de danse ) - 3 0 
 Witches Dance . - - - 3 0 

 generally compass 




 SACRED 


 MUSIC 


 1 0 


 1 6 


 J. BARNBY . 


 SIR JULIUS BENEDICT . 


 W. T. BEST . 


 OSKAR BOLCK . 


 J. BAPTISTE CALKIN . 


 JOHN GOSS 


 CH . GOUNOD 


 E. J. HOPKINS 


 HENRY LESLIE . 


 G. A. MACFARREN . 


 REV . SIR FRED , A. G. OUSELEY 


 WILLIAM REA . 


 HENRY SMART 


 ARTHUR 8S. SULLIVAN 


 E , H. THORNE . 


 BERTHOLD TOURS . 


 JAMES TURLE 


 ENTIRELY NEW WORK MUSICAL EDUCATION 


 WALTER MAYNARD 


 MUSIC COPY - BOOKS 


 CONTAIN PROGRESSIVE COURSE INSTRUCTION MUSIC , 


 SYSTEM DESIGNED 


 SINGING SIGHT 


 NEW WORK ORGAN 


 HANDEL CHORUSES 


 3 . BOW . 6 . HALLELUJAH 


 RECOMMENDED PROFESSORS , 


 CHAPPELL & CO . , 50 , NEW BOND STREET , LONDON