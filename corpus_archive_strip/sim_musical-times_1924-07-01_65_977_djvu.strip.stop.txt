


 593 


 SINGING - CLASS CIRCULAR 


 JULY 1924 


 CONDUCTORS CONDUCTING 


 594 


 1924 


 1924 595 


 COMMENTARY MENDELSSOHN 


 596crescendo getting upper hand formal 
 idea . 
 consider E 
 fugal opening , employing later contrapuntal 
 symphonic means attain object . 
 criticism distinction considered 
 refinement academicism , relieves 
 necessity quarrelling fine music 
 conflict academic formula 

 Fugue said strongly 
 influence Bach . doubt 
 Mendelssohn indirectly influenced Bach , 
 little . Bach love fundamental , 
 forgotten B minor JZass 
 owes resuscitation largely ; 
 work Bach Beethoven 
 popularity - day Mendelssohn responsible 
 instance . precise influence 
 Bach Fugue negligible non 
 existent , , begin , romantic spirit 

 




 _ _ _ 


 SSS 


 598 


 1924 


 WORRYING 


 LARYNX 


 NX 


 599 


 BRAHMS : THOUGHTS 


 - VALUATION 


 MUSsatisfied claim war admirable excuse|the evidence 

 calling zsthetic battle 
 finding difficult sustain face 
 increasingly effort general musical 
 public Brahms fairly , . 
 ingenuousness Excusists proved 
 founded . ‘ - day interest Brahms 
 . 
 surfeit extravagant dubious modern 
 music stimulated healthy inclination 
 submit old judgments test recent 
 experience . cause , clear 
 comfortably - accepted view Brahms 
 forbidding formalist carried art 
 music farther Beethoven left 

 Possibly natural reaction 

 generous 

 4 composer , maintains develops 
 harmonic traditions Romantic School , shall 
 classical form Beethoven left , 
 aid free conventions greatest 
 masters wholly succeed loosening , 
 carry stage raise fuller 
 organization 

 — little ’ ( ‘ ’ 
 — quotation apt bear extension 

 Season mists mellow fruitfulness 

 Brahms learned Schubert 
 Beethoven self - evident , 
 superficial criticism content assert 
 merely borrowed ideas , types themes , 
 tricks colour , leave . 
 true , Hadow pointed , 
 Schubert pour ‘ profuse strain 
 unpremeditated art , ’ art larger manifestations 
 gainer premeditation . result 
 premeditation Brahms 
 nature ( cause effect 
 Pateresque withdrawal social busy - ness ) 
 enhance Schubertian 
 music depth thought certain intensity 
 character work Schubert rarely 
 owned . Schubert , , best short- 
 breathed symphonic craftsman ; Brahms possessed 

 sense 

 athletic musical stamina enabled 

 serve , think , only| 
 Brahms tie perfect 
 finish ends Beethoven leave 

 tangled — admitted 
 opponents better job 
 parcel great predecessor — 
 , recourse methods 
 Programmists , distinct personal 
 contribution material Romance music 

 ame 
 e pitch 
 s , 
 ythmic 
 

 MUSICAL TIMES — Juty 1 1924 601 
 coeeaaeaama 
 later works B flat Sextet ; C major|we penetrated innermost shrine 
 Trio ( Op . 87 ) , Brahms stride up| Symphony , 
 exultingly autumnal evening | conceal ( different 
 brilliant light summer day ; the| starting point ! ) Symphony 
 Horn ‘ Trio ( Op . 40 ) , passages which/ Beethoven key . works point 
 suggest closer relationship Wagner than| Brahms biggest minds day 
 Schubert . mistake , , | bigger - round musical stature 
 assume peculiar poetic bloom no| subsequent symphonic writer , possible 
 incidental - crop emerging here| exception Elgar . Emil Naumann 
 work preoccupied | finished summing - Brahms 
 entirely dissociable antithetic aim . Its| middle life phrase 
 persistence single golden thread folk-| pitiably curious : ‘ man 
 song Pianoforte Sonata in| worthy placed Rudinstein . 
 great closing compositions Brahms life } Naumann incarnation Teutonism 
 suggests , , direct effect ! music - criticism day , declaration 
 fundamental informing spirits work.| meant praise . interesting turn 
 incipient , hinted , slow| Paul Landormy _ recently published ///story 
 movement con espressione theme } Afusic moder declaration — 
 Allegro Op . 1 — little commonplace | significant M. Landormy 
 lush sentiment maybe , beguiling } pro - German Naumann pro - French : 
 inclinations commonplace things 
 ; found , developed polished , 
 neglected F minor Pianoforte Sonata Op . 5 ; 
 traced Protean manifestations | 
 Magelone Romances — Brahms lightly ny 

 ensemble qualities entirely , 
 touches Bach , Gluck , Handel , Beethoven , music composer recognises 
 Schumann , passes — succeeding| hesitation . . . refined 
 chamber music , second Symphony , } poet - musicians 10th century . 
 sublime Clarinet Quintet . second 
 Symphony , workmanship ( like 

 composer playing , Joachim arene ) ' S| widely opening pastures , channels 
 inevitable aera certainty touch , lead farther farther away . 
 poetry inspiration gathered ( Mr. Langford truthfully said ) 
 garland late summer flowers , Brahms remains ; principles remain , , 
 foredoomed toa short life , fragrance refreshes | stubbornly alive work 




 602B singing . day advertising| piece advice editors daily 
 experts realise //ese music - lovers are|papers weeklies 
 music - lovers right kind . lthe matter , : ‘ Cease treat music 

 casual mention , Programme include| point view mere news . 
 works Bach , Beethoven , John Doe , Richard | notion best policy provide 
 Roe , ’ bad mention . ! public public wants 
 right kind music - lover wish know | supposed want . Aim assuming leadership 

 going hear works Bach | music , matters consider 

 Beethoven recent concerts | important . way advertise 
 given overdose . , , | music . ’ 
 grant prospect particular artist’s| point view average editor 
 interpretation turn scale . | cone -ert usually news , musical work . 
 names John Doe Richard Roe|I heard editor declare 
 position different . represent unknown | account concert use 
 quantities , additional information conveyed | appeared day concert . 
 titles works ~7/. | important work played , 
 music - lover right kind according to| critic important comments offer 
 definition , case wish|work concern : time - limit 
 hear John Doe Richard Roe music | mattered . 
 hope provide valuable additions } editor American musical periodical 
 stock musical experience . |once explained useless 
 buy concert ticket order hear| publish articles composers 
 works unknown composers terrible gamble . | | thousand readers heard 
 listen new works unknown composers | . 1910 , London editor emphatically 
 risk people endowed | asserted readers paper 
 undiscriminating appetite keen sense | slightest interest articles per- 
 duty face matter course . average |formances Russian Ballet Paris — 

 rest 
 
 thin 
 app 
 
 
 colt 
 c 




 604 


 605 


 6061924 

 words written round passage 
 ZEroica strings play dominant 
 harmony horns enter theme 
 tonic . ears - day think 
 clash , worth remembering 
 daring composer Berlioz abide 
 , good judges - day hold 
 clashes , wild poetic flash 
 Beethoven , merely note 
 having got wrong lineor space . ‘ flat 
 violin intended G , 
 notes horn 
 slips D F D. know Beethoven said 
 stuck passage stands , 
 need rule possibility having 
 accident penmanship . 
 liked result slip , ( likely 
 ) pretended like mere 
 stubbornness . Berlioz says passage 
 ‘ intention Beethoven , 
 truth anecdotes current 
 subject , admitted whim 
 amounting absurdity . . .. difficult 
 find justification musical caprice . ’ 
 point worries little day 
 average listeners notice 
 orchestra played passage written 
 amended early editors . amusing think 
 quarts ink having shed 
 ordinary passage . ( shedding 
 drops 

 turning Bach Ex . 2 , 
 came case clear 
 editors blindly followed 
 sticking presumably original manu- 
 script version . Dorian ’ Toccata 
 progression unsatisfactory stands 
 surprised hear organists playing written 
 mere shifting accidental beat 
 farther logical effective . 
 passage appears edition 
 known 




 SS 


 1924 607 


 ERBA , URIO , STRADELLA , HANDEL 


 URIO - STRADELLA CONFIRMATION 


 ERDBA 


 STRADELLA 


 STYLE 


 DEBUSSY BRAHMS 


 609 


 1924friend Debussy dine 
 following evening restaurant close State 
 Opera - House . ‘ treat 
 store new young French friend . ’ Brahms 
 said sorry entertain 
 house , living ‘ homely 
 people , bit ill ease manners , 
 slightest idea French politeness . ’ 
 met evening arranged , 
 dinner Brahms crackling wit 
 repartee . conclusion Brahms said 
 ‘ treat store ’ performance Carmen , 
 - performance 
 attended , practically ‘ coming age ’ 
 connection withthe work . hadsecured box , 
 title - undertaken best actress- 
 singerat Vienna . performance German 
 master followed note closest attention , 
 intervals delivered commentary- 
 lecture principal numbers , criticisms 
 singers ’ performances respective parts 

 Brahms subsequently devoted entire day 
 conducting Debussy places musical 
 interest Vienna . visited graves 
 Beethoven Schubert , Conservatorium , 
 inspected famous collection musical 
 manuscripts autographs Imperial Library . 
 leaving Vienna , Debussy called house 
 Brahms . ‘ home ’ time , , wishing 
 Claude 40 m voyage successful career , great 
 German master embraced young Frenchman 
 like son . said ‘ crusty ’ old bachelor 
 fatherly feeling fortunate 
 married man 

 Summer Vacation Course Adults Dalcroze 
 Eurhythmics held Paris Dalcroze School , 
 52 , Rue de Vaugirard , Paris , V ° , August 4 16 . 
 Course personal direction M. Jaques- 
 Dalcroze . English - speaking students obtain 
 information Dalcroze School , 23 , Store Street , 
 W.C. ( tel . : Museum 2294 ) . Early inquiry necessary , 




 1924 


 EXHIBITION 


 ROYAL ACADEMY ARTS 


 3 ¥ ARTHUR T , . FROGGATT 


 G. A. 


 1452 ) , 


 1924 611 


 PRAGUE FESTIVAL 


 MUSICAL T 


 612 


 ‘ DEATH EATING CHOCOLATE , ’ 


 LATE , ’ good deal musical discussion lately 
 taking place English Mechanics . Clearly 
 particular English mechanics sort use 
 modern music . way delivers 
 concerning Elgar 

 demur “ J. G. B. ” speaks 
 “ sweet sanity ” Elgar , calls “ 
 world greatest composer . ” Truly 
 pieces Elgar containing beautiful 
 melody inspiration , com- 
 positions later works | 
 listened , ears , mere jumbles sound . 
 remind person idly improvising 
 pianoforte thoughts 
 definite idea wanted 
 play . ( Compare compositions 
 exquisite spontaneous outpourings melody 
 emanated Rossini ! ) Elgar 
 compositions lack saving grace 
 striking novel harmony characterise 
 music modernists . . . 
 occasional short — short — flashes 
 melody , composer recollected 
 definite , 
 mere flashes , [ Violoncello ] Concerto 
 consists truly Elgarish , drawling , aimless , 
 meaningless meanderings . . . . Surely time 
 composers realised compositions 
 useless sense 
 ear . want - day composer 
 d modern harmony melody ... . 
 combine , , Beethoven 
 Holst music term 
 “ gorgeous ” aptly apply 

 letters evoked 
 quote brief passage 




 614 


 1924 


 WILLIAM BAINES 


 1899 - 1922 


 O.C , 


 GERMAN CRITIC MUSICAL ENGLAND 


 FRENCH CONDUCTOR UPPER CLASSES 


 ZEMLINSKY LYRIC SYMPHONY 


 HEAR TONES ACTUALLY SOUND 


 NEW MATERIAL WAGNER BIOGRAPHERS 


 BERNARD ZWEERS 


 RONSARD MUSIC 


 SAINT - SAENS 


 TOSCANINI 


 M.-D. CALVOCORESSI 


 1924 


 NEW STRING MUSIC 


 CHORAL MUSIC : UNISON SONGS 


 1795 


 B.V 


 1795 , 


 MUSICAL T 


 FEMALE - VOICE CHOIRS 


 MALE - VOICE CHOIRS 


 S.A.T.B 


 1924 621 


 622 


 1924either negative highly qualified 
 affirmative . ’ 
 points 

 love singing plain people 
 sustained Bach ; violin 
 violoncello playing gentlemen Esterhazy 
 courts inspired Haydn String 
 Quartets ; wide diffusion musical 
 feeling Austrians sang 
 played Beethoven possible . . . 
 life - giving amateur spirit 
 largely succumbed large - scale production 
 professional expert direction 

 




 1924 623best , written descriptive notes music , 

 copious music - type illustrations , glossary , good 
 deal interesting general information , biographical 
 historical , particulars price , & c. , 
 chosen records , publishers prices 
 scores music dealt . works 
 discussed range Byrd Beethoven . Inevitably 
 readers grumble work 
 left , , think , hard 
 complain chosen . 
 patience , seeing title holds 
 promise blessings . H. G 

 
 Oscar Cremer 




 624 


 1924 


 ANTHEM TENOR SOLO CHORUS ‘ * THY MERCY , 0 LORD 


 ORGAN . 


 SSS = SSS SSS 


 = SS SS 


 SSS SS SSS SE 


 EXCELLENT THY MERCY 


 WA 


 MI 


 QO ) 


 4 _ 


 1924 


 SSS SS SS FE : 


 ULL . = 


 SSS E 


 = — _ — — — _ 


 2 


 _ _ = 


 PP , | ] | ! 


 — — — — FZ 


 VY 


 630 


 1924 


 1924 631 


 B .. G 


 632reproduction , cases trifle thin musical 
 interest 

 Chamber music poorly represented month 
 Cherniavsky Trio , plays Scherzo 
 Beethoven Op . 1 , declines badly 
 giving poor piece 
 called A/ Brook ( Col . - . d.-s . ) . pity 
 |players try cater entirely different 
 publics . month ago bracketed 
 Boccherini Minuet hopelessly feeble Ay “ 
 Waters Minnetonka 

 Frieda Hempel ( Schumann Du meine seele 
 Mozart Schlafe , mein Prinzchen ) , 
 Polonaise brilliant Galli- 
 Curci . Apropos Frieda Hempel | 
 wish correct slip June notes . | 
 described singing Tchaikovsky 
 weary heart old song Phyllis 
 charming graces ‘ good 
 effort . ’ slip happened know , | 
 revoke heartily : record average . 
 slips confined reviewers . label 
 says weary heart flute 
 obbligato played Louis Fritzi . ; 
 violin obbligato , effectively played , pre- 
 sumably Louis aforesaid 




 .M.V. 


 1924 633 


 ‘ WHEREFORE ’ 


 RULES HARMONY 


 PRACTICE GREAT MASTERS 


 REASON 


 MUSICAL T 


 634 


 INTERVALS MELODY 


 FALSE RELATION 


 


 TREATMENT 


 DISSONANCE 


 PREPARATION 


 SECOND INVERSIONS 


 RESOLUTION 


 CONSECUTIVES 


 1924 


 ROYAL COLLEGE ORGANISTS 


 DISTRIBUTION DIPLOMAS 


 NEWCASTLE BACH CHOIR 


 SCOTSON - CLARK WESTMINSTER CATHEDRAL 


 WIDOR RECITALS 


 637 


 LONDON SOCIETY ORGANISTS 


 ORGAN RECITALS 


 APPOINTMENTS 


 MUSIC PUBLIC SCHOOLS638 MUSICAL TIMES — Jvuty 1 1924 

 musical artistic realisation , honourable | look use ? ; rightly 
 thing ; build , ‘ ’ the/ wants handle . Mr. Claughton fails 
 boy musical culture stopped listening . This|to convince mere knowing ‘ 
 guality hands teachers mould develop . | music ’ important boy ‘ 
 Teaching , worthy , creative art | play game . ” know plenty boys 
 beginning end . Regarded , teaching music certainly | pleasure holidays doubled able 
 matter ‘ free - wheeling - worn traditional | participate chamber music - making family 
 track . ’ Real teaching rules question fear | friends , hearing concerts , taste 
 ‘ exploitation active exclusion } listeners , criticism , eclectic , ‘ 
 receptive , ’ , provided teachers @ wellas /each , | ’ work school real 
 power develop receptive stimulus far - reaching value Mr. Claughton somewhat 
 active . , fact , acts reacts a| sordid assessment ‘ important asset school . 
 degree passive listening , Jey se , master parent view . ' 
 aspire . Boys , ¢.g . , encouraged join chamber| Naturally , din indescribable ( inevitable ) 
 music , appreciate , com-| hear dozen pianofortes practice , 
 parison passive listeners , school chamber concerts , | violin ’ cello throwa . 
 understanding . , pianoforte need , reasonably - appointed 
 course music - master time or| music school , pathetically unusable condition 
 encountered types boy instanced by| asked believe generally , 
 Mr. Claughton . Let admit a| reason assume din simultaneous practice 
 certain number absolutely aptitude music , | represents little ‘ eyewash . ’ reasonably 
 , cases , care . With|some work progress , 
 little ; final states Mr. | teachers realise greatest needs pupil 
 Claughton examples b2 depressing outlook . | taught 4ow practise . Supervised practices , , 
 hope form finished artist school ? Very|are unknown , depends system vogue , 
 rarely , ; expected . provided | co - operation staff carrying . 
 , 2 — unpromising performer — keen , discerning| need follow ‘ active exploited 
 expert teacher probably find reason / exclusion , , rate , paralysing 
 lack improvement performer , , | receptive . ’ Thetwo sides co - exist 
 possibly . wy , ask , | stimulate . 
 . 3 skill reading accuracy,| fortunate lot served 
 perforce leaning low - class music ? , , | assistant exceptionally inspiring sane director 
 hope . 4—‘the earnest plodder’—than| music , experience public school musical life 
 Mr. Claughton sees fit credit ; wise|I come dry - - dust pedantry 
 follows music plods level | Mr. Claughton laments . exists , doubt , 
 receptive faculties , type blossoms | certainly existed past , teachers 
 forth expected . Intellects , making | bring vital meaning music studies , 
 especially , constituted alike ; earnestness | create living interest boys , giving 
 type spur intelligent focussing , | best demonstrations , pompous , 
 bit bit , technical musical principles ultimate | exclusive , awe - inspiring mystery , art directed 
 self - expression . ‘ forced labour ’ , nor| awaken feeling sense beauty natural 
 need Mr. Claughton types summarily | boys , turn right wishing express 
 dismissed ‘ worth account good|it way . rests 
 , ’ use term | teacher replenish receptive train active 
 ‘ self - expression ’ inferred sort | expression . boy , , lively conception 
 good view fora boy . atall . boy principles constitute language music , 
 school finds wider scope work serving | simplest air play realisation — technically , 
 good community , house gatherings , grammatically , artistically — worth , 
 house singing , accompanying , useful . | build . little 
 responsibility leader fresh demands | conscious effort hisown . teacher train 
 ; musicianshipalsoistested . histeacher| boy think , principles 
 /va / zer , , matter think , hack term , ‘ learning 
 morale musically ; rests teacher | music , ’ sense Mr. Claughton rightly condemns , 
 trained musician allowed run | place synonymous artistic musical 
 seed ‘ pianoforte typist . ” boy turned | training . term applied universally 
 having ‘ missed vocation ’ asa musician , nothingis | public school music Mr. Claughton find 
 easier , triumphantly discouraging ! | adverse criticism plea outlook . able 
 Continental teachers particular way of/| vouch achievement 
 threatening pupil lessons talent . | public school . gramo- 
 Hiow tell pupil talent is| phone library contains excellent selection records , 
 given fair chance sound musical instruction | carefully chosen director , examples 
 coupled certain sympathetic insight ? | musical culture times , 
 reminded teacher despairing new pupil | principal works great masters . Boys free 
 4uows . course ; comes be| choose records house private gramophones , 
 taught , teacher expect boy under-| interesting inquire freedom 
 stand level ; gauge boy’s / tends ‘ merely frivolous , ’ ‘ 
 potentialities , making ideas sufficiently lucid | resistance , ’ ‘ yokel street - boy taste . ’ 
 simple grasped ; talking boy head , | . reason , school - boy nature universally 
 wil ! prove receptive . perverse ; system showing best 
 , surely , ‘ live ’ teacher dream making attractive , . school chamber concerts 
 ‘ wonderful occult mystery ’ elements music , | addition , orgin recitals , lectures music , musicians 
 conceive object , agree Mr. | methods , reproach neglect 
 Claughton elements , position notes , scales , time-| receptive merited . active work 
 signatures , rhythm , & c. , easily learned away | stimulated goes saying , school 
 pianoforte — boys rate . | able produce boys public performance 
 parcel pupil equipment , use| annual competitions standard works 
 — tools fact ; boy shown | ‘ 48 ’ Bach , Sonatas Beethoven , Etudes , & c. , Chopin , 
 serve , naturally wants practical use . | Rhapsodies , & c. , Brahms , Violin ’ Cello Sonatas 
 teacher carpentry explain wood , hammer , | Mozart , Beethoven , Schumann , Grieg , & c. , 
 plane , saw , expect boy remain content | chamber music ( including wind ) original instrumental 

 pitch 




 1924 639 


 DOH - MINOR 


 ACT TOUCHJune , 1924 . Percy RiIpeouT 

 FINGERING SCALES suff 
 PIANOFORTE 
 S1r,—I reading interest correspondence 
 subject scale - fingering , notes 
 system adopted prove useful . jg goo 
 taken J. A. Johnstone Scales , Chords , Arpegi wee 
 published Messrs. J. Williams . 7 7 oe 
 ( a. ) scale consists sounds , 8th js pity 
 Ist octave higher , need finger = 
 playing octave . — 
 ( 4 . ) little finger outer extremes . 
 scales . 
 ( c. ) leaves seven different notes played pe 
 different fingers , orderly arrangement 
 2 3 group , 2 3 4 group . 
 ( @. ) 4th finger bor 
 note . position 4th finger memorized ans 
 rest easy . oe 
 come actual grouping ( major scales ) : - 
 I.—Scale C sharps ( C G D ): ; 
 R.H.—4th note key - note . ane 
 L.H.—4th note key - note . rp 
 II.—Scales black keys ( B C ? ; FZ G ) ; 1 
 C # DP ): h 
 R.H.—4th ® ( B ? ) . cee 
 L.H.—4th F% ( G2 ) . - 
 III.—Scales flats ( F , B ? , E ? , AD ): Tn 
 R.H.—4th B ? vo 
 L.H.—4th 4th degree scale . 4 
 ( Exception : F , L.H. 4th G. ) eli 
 Harmonic Minors fingered way , Yor 
 following exceptions : ( 
 R.H. FS C$ — 4th 2nd degree scale , 
 L.H. B ® E9—¥4th G ? 
 Melodic Minor exceptions : S 
 - s Ascending 6th degree . : 
 Co RH . 4th } Descending 2nd degree . ar 
 ( Ascending GZ 
 B ? L.u . 4th j Descending G ? 7 
 ED ? L.u . 4th GD Gra 
 ? L.H. 4th { -weemts D ? 
 ' Descending G ? thre 
 system found particularly useful . cou 
 author system fingering arpeggi , 
 enter discussion need quoted thos 
 here.—Yours , Xc . , J. C. Brypsox . wit ! 
 Kegworth , zr . Derby . tho : 
 June , 1924 . obt : 
 ake K 
 S1r,—The rules advanced correspondents [ ¢ 
 memorising fingering major scales interesting . 
 habit ( years ) higt 
 following , simpler far given : supe 
 Sharp scales—4th finger key - note ; flat scales — | 
 R.H. , 4th B flat ; L.H. , 4th sew flat . arti 
 exceptions rules obvious easy 
 B left , F right , F sharp ( ) . 
 Like Mr. Swinburne , sure evolved 
 , fancy . think , , S 
 seen like pamphlet | agai 
 it.—Yours , Xc . , C. STANLEY PARSONSON . 
 Launceston , Cornwall . Eng 
 June , 1924 . 
 char 
 REPLY ‘ TRAVELLER ’ 
 S1r , — ‘ Traveller ’ evidently labouring ignorance . assu 
 blame — entirely fault . | , upor 
 , , keenly interested bad char 
 music . , changed . know exact thes : 
 reason ; happy present state wish class 
 inquire . know began think te 
 probe taste . found interest bad musit earn 
 result bad environment . believe : 
 jazz music music worth hearing . Fe 
 age ; sixteen . 
 real turning - point reached volume pres : 
 Beethoven Sonatas given . mustered oO. 
 E 

 
 




 ) E 


 YDSON 


 ONSON 


 64I determined follow analysis 
 good music . Saving penny got , bought — 
 weeks waiting — volume Mendelssohn Songs 
 Words Liszt Liebestraume . 
 pity encouraged betterment 
 taste . , , wanted guidance — found 
 music encourage effort 

 gone forward , regretting 
 change taste . familiarised 
 Bach , Handel , Haydn , Mozart , Beethoven . 
 given score Lohengrin , resulted 
 immediate investigation Wagner . libraries 
 borrowed Riens ? , Flying Dutchman , rest , 
 Ring Parsifal , found wonderfully engrossing — 
 somuch play quantities 
 music - dramas memory 

 interest Wagner views study Gluck 
 French opera , led Italian 
 opera German opera , terminating Strauss 
 Electra 




 BACH COURSE 


 WAGNERIAN CALUMNIES 


 RECOVERY VOICE 


 STERLING MACKINLAY 


 PRIEST - ORGANISTS 


 MR , F. J. CROWEST : APOLOGY 


 ROYAL ACADEMY MUSIC 


 R. E. F 


 \MPTON 


 1924 643 


 ROYAL COLLEGE MUSIC 


 TRINITY COLLEGE MUSIC 


 UNIVERSITY COLLEGE NORTH WALES 


 644Archdeacon Groome wrote 

 good performer piano , 
 harmonies organ 
 stood corner entrance room Little 
 Grange good listener . 
 bit Mozart Masses , 
 Beethoven Operas . times 
 fill harmonies voice , true 
 resonant 

 critic FitzGerald presents curious features . 
 lived , course , recent revival Bach- 
 appreciation ( able find reference 
 Bach letters ) , consequently Handel occupied 
 high place esteem . remarkable thing 
 thought Handel best work contained 
 operas , — commonplace - day — 
 saturated Italian influences little 
 Handel left . FitzGerald attitude Handel 
 summed passages . 1863 , writing W. B. 
 Donne , remarked : 
 * [ Handel ] good old Pagan heart , 
 ( till yield fashionable Piety England ) 
 stuck Opera Cantatas , Acs 
 Galatea , Milton Penseroso , Alexander Feast , & c. , 
 revel plunge frolic 
 tied Orthodoxy . ( 
 mind ) great works : Coronation 
 Anthems , Human Pomp accompanied 
 illustrated 

 years later declared roundly : ‘ certainly 

 greatest Opera world . ’ 
 FitzGerald understand Beethoven , 

 

 admitted genius . looked 
 composer formalist , ‘ Beethoven 

 analytical erudite’—a view author la 

 Quartets Sonatas difficult appreciate 

 said Tennyson I842 : 
 * Beethoven analytical erudite 

 inspiration true ... 
 think , strictly speaking , thinker 

 friend years later 

 Beethoven , experience , 
 depth reached . admit 
 bizarre , , think , morbid . 
 original , majestic , profound . 
 music ( fhinks : Gluck : 
 Mendelssohn 

 Fidelio FitzGerald favourite Beethoven 

 Ah , like hear Fide/7o , 
 heard . find * * Melody ” 
 : understanding Melody asserts 
 independently Harmony , Mozart Airs . 
 miss especially Leonora ‘ ‘ Hope Song . ” , 
 story , Passion Power 
 Music set , opera 
 hear repeated 

 filth Symphony wrote 

 Finale C minor noble . heard 
 twice Jullien . like hear Mozart 
 better ; Beethoven gloomy . , incontestably 
 Mozart purest mzsician ; Beethoven 
 Poet Painter , great deep 
 Soul Imagination 

 words truth recurs 

 

 time listen works Master 
 Bonn . Beethoven teacher , phil 
 sopher , musician narrower 
 sense word , , Edward Carpenter said 

 0 




 645 


 1924 


 VIOLIN METHODS : OLD NEW 


 1924 


 646 


 PHILHARMONIC CHOIR 


 SINGERS MONTH 


 1924 647 


 ITALIAN OPERA 


 648 


 1924 


 PELLEAS ’ B.N.O.C 


 FIDELIO ’ SCALA THEATRE 


 649season , valiant singers face 
 discouraging rows seats 

 Fidelio , course , dates time singers ’ operas . 
 prophetic Beethoven foresaw type opera 
 good singers wasted . 
 public singing , fashion , 
 Fidelio - established supremely fine singing . 
 Covent Garden filled sake Ad ! Fors ’ é lui . 
 Fidelio song Hope , tenor music 
 beginning second Act ? , 
 Leonora . 3 Overture , remains , , 
 justification Fidelio , labour tedium 
 operas justified fine fruit — 
 magnificent fruit music , growth ripening 
 necessary dramatic plot 
 business concrete world excited 
 mind composer — musical fruit world 
 enjoying know opera 
 superannuated Greek Elizabethan theatre . 
 Wagner troubles Azzy mythology 
 Evolution slow , mysterious way bringing birth 
 Siegfried ‘ Funeral March , ’ conceivably 
 treasured epochs completely lost trace 
 Fricka 

 return Fzde / io mention respect convic- 
 tion sung spite English text 
 calculated wind sail . 
 principals Miss Eva Turner Mr. William Boland . 
 singers gifts , community 
 reckless artistic values , held precious 
 worthy right fostering . fear , 
 eminent , described coarse | 
 performances . impression singers | 
 accustomed insensitive audiences — audiences | 
 counts gros moyens . musical | 
 listener taking pleasure grand culminating 
 outburst tone right place . like | 
 artfully prepared , know effective | 
 . lay success singing 
 Rose Cavalier , - days’-wonder 
 , cared music London ? 
 prodigious gift single singers , 
 cultivation finer shades . Miss Eva Turner 
 remarkably gifted young woman . blame 
 musical England generally 
 better artist . Mr. Boland , , , feel , achieved 
 Providence reach achieving . 
 brace singing bright , clarion tones , 
 apparently think soft singing worth 
 trouble . fzano nondescript , practically 
 toneless 




 MUSIC WEMBLEY EXHIBITION 


 AB 


 1924 651 


 POT - HUNTING PALACEThe programme included Bach Chromatic Fantasia 
 Fugue , Sonatas violin pianoforte Handel 
 Purcell 

 BIRMINGHAM.—To welcome Mr. Adrian C , Boult 
 appointment director music conductor City 
 Birmingham Orchestra , reception given 
 Lord Mayor , Alderman T. O. Williams , June 4 . 
 occasion Mr. Boult addressed gathering represen- 
 tative citizens , outlining scheme season . 
 symphony concerts given Tuesdays , - 
 Sunday Saturday concerts , series Saturday 
 afternoon concerts children . modern work 
 included symphony programme . 
 half Sunday programme devoted 
 composer , end expression nationality 
 music . works promised performance 
 Holst Planets , Arthur Bliss Colour Symphony , John 
 Ireland Symphonic Rhapsody , work Bax . 
 visiting conductors season Eugéne Goossens , 
 Sir Landon Ronald , Bruno Walter.——lIn October , 
 exchange visits Mr. Joseph Lewis 
 conductor Brahms Society Vienna . Mr. 
 Lewis conduct Vienna Society , foreign 
 musician conduct Wolverhampton Musical Society . 
 annual orchestral concert Midland Institute 
 Music given June 4 . solo works out- 
 standing item performance Dvorak Cello Concerto 
 Miss Mabel Whymark . orchestra assisted 
 professional players , Prof. Granville Bantock 
 conducted.——At sonata recital June 2 , Miss Marjorie 
 Chapman played Liszt B minor Sonata . Beethoven 
 Waldstein given Miss Alice Clayton , Miss 
 Lilian Niblet played Mozart Sonatas D , 
 Miss Winifred Lowe Miss Winifred Morris sang songs 
 Vaughan Thomas Debussy 

 BRIDGWATER.—Mrs . T. J. Sully , local pianist 
 great worker music district , gave recital 
 Miss Dorothy Silk , 16 , played Tausig 
 arrangement Bach Toccata Fugue D minor , 
 Beethoven Variations Original Theme F. 
 Miss Silk sang Bach arias , Schubert , songs 
 Murray Davey ( Zpitaph ) , Maurice Besly ( Zéstening ) , 
 Peter Warlock ( /%ggesnie ) , Purcell , Robert Jones 

 BRISTOL.—The University Male Choir sang Grieg 
 Landerkennung , Agincourt Song , Dunhill Pilgrim 
 Song , Terry Sea Chanties , Stanford Songs 
 Fleet , Negro Spirituals , Colston Hall June 5 , 
 String Orchestra played Holst St. Pauls Suite 
 Grainger Hande/ Strand 

 DOLGELLEY.—The Grammar School selected 
 Sir Walford Davies making experiment fostering 
 instrumental music , co - operation headmaster , 
 Mr. J. Griffith , Miss Griffith . 23 school 
 orchestra members played Haydn Symphony G , 
 raise funds visit Wembley , 
 invited school concert . choir sang 
 - songs 

 EXETER.—Under auspices Chamber Music 
 Club , Mr. George Parker ( vocalist ) Miss Thelma Davies 
 ( pianoforte ) gave recitals 21 . Thesongs included 
 Dr. Ernest Bullock ( pianoforte ) , 
 Bairstow , Stanford , Shaw , Geoffrey Gwyther , 
 Wolf , Vaughan Williams . members ’ meeting 
 Club , 28 , Beethoven Septet strings wind 
 instruments played , - songs Stanford 
 ( Peaceful Western Wind ) Roberton ( Shepherdess ) 
 sung.——On June I0 band H.M. Coldstream 
 Guards played Suite E flat Holst , Suite , 
 Seasons , Glazounov , numbers Ansell 
 Shoe , conducted Lieut . R. G. Evans 

 LIVERPOOL.—The local Association Schoolmasters held 
 second annual Festival St. George HallonJune4 . 
 choir consisted entirely boys , number , 
 drawn elementary schools , gave Old English 
 compositions modern pieces . Mr. O. R. Owen 
 conducted 

 MUSICAL TIMES — Juty 1 1924 

 Oxrorp.—On Sunday Eights Week unaccom- 
 panied music Holst Vaughan Williams sung 
 cloisters Magdalen . Flizabethan Singers sang 
 Christ Church following day , Mrs. Gordon 
 Woodhouse played harpsichord music . Exeter College 
 Magi String Quartet played Dvorak Tchaikovsky , 
 choir sang - songs . Holst Cloud Messenger 
 sung Keble Choir , orchestra played 
 Hamilton Harty arrangement Handel Water- 
 Music Vaughan Williams Wasfs Overture . 
 — — 31 Eglesfield Musical Society brought 
 Eights Week musical programme close 
 Queen Coliege . College Choir sang Shenandoah , 
 Buck Zhe Blackbird ( boys ) , Holst 
 Bring good aie.——On June 4 Miss Dorothy 
 Moulton gave Lieder recital , assisted Miss Margaret 
 Deneke pianoforte , aid Endowment Fund 
 Lady Margaret Hall.——On June 12 Oxford 
 Bach Choir joined Orchestral Society celebrate 
 centenary Beethoven Choral Symphony Mass 
 D. , Ayrze , Credo , Agnus Dei 
 performed , followed Symphony , 
 solo parts taken English Singers 
 ( Mr. Archibald Wilson replacing Mr. Steuart Wilson ) , 
 Sir Hugh Allen conducted 

 SEATON.—On June 4 Choral Society , numbering 
 eighty voices , assisted string orchestra , performed 
 Hiawatha Wedding- Feast , conducted Mr. W. C. Walton 




 BRITISH MUSIC BELGRADE 


 MODERN SETTINGS SHAKESPEARE 


 IRELAND 


 PARIS 


 LOUIS AUBERT ‘ LA FORET BLEUE 


 1924 653 


 TORONTOInteresting programmes continue provided 
 New Symphony Orchestra . seventeenth , eighteenth , 
 nineteenth , twentieth Twilight Concerts drawn 
 good audiences hear Dr , Ernest MacMillan impressive 
 Concert Overture , conducted composer , von Kunits 
 scholarly inspired Violin Concerto E minor , played 
 composer , Schumann C major Symphony , 
 Prelude Love Death 77istan [ solda , Ruy Blas , 
 movements Schubert seventh Symphony , Prelude 
 Introduction Act 3 Lohengrin , Henry V//1 . 
 Dances , Casse - Noisette Suite , Blue Danube Waltz , 
 Aida Rose soloist ‘ Mighty Kings ’ ( /udas 
 Maccabeus ) , Norah Drewett Chopin F minor 
 Concerto 

 permanent string organization , 
 called Hart House Quartet ( University Syndicate 
 emanates ) , including Geza de Kresz , 
 H. Adaskin , M. Blackstone , Boris Hambourg . 
 programme ( invitation ) splendidly received . 
 comprised Haydn D minor , Op . 76 , . 2 , 
 Beethoven F minor , Op . 95 , E flat , Op . 74 ( Adagio 
 ) . Great things expected promising 
 start 

 Toronto Conservatory Orchestra , Frank 
 Blachford , gave initial performance works Elgar , 
 Holst , Walford Davies , Grainger , soloists 
 Betty Marlatt Scott Duncan . 
 promising young pianists , career keenly 
 watched . large audience 




 H. C. F 


 VIENNA 


 OPERATIC EVENTS 


 BRUCKNER PREMIERE654 

 second movements ( A4//egro Andante ) warm fingers minutes 
 unknown . Chronologically work comes | gas - jet ! leaving school acted organist 
 Bruckner ( 1865 ) second ( 1871 ) Symphonies . churches , good deal recital work . 
 acquainted Best , years 
 UNIQUE PERFORMANCE BEETHOVEN ‘ NINTH ’ hardly week attend St , 
 George Hall recitals . real appreciation 
 great player written Statham appendix 
 book referred . Soon coming 
 London , thirty years age , began series 
 organ recitals Albert Hall Sunday afternoons 
 , June , July , continued 
 years , recitals voluntary ; counted 
 “ rewarded enjoyment . ’ 
 public announcement—‘I desire 

 centenary day Beethoven Ninth 
 Symphony produced — 7 , 1824 — com- 
 memorated notable performance Konzerthaus . 
 Paul von Klenau , Danish conductor 
 season débiit London , gave unique 
 experience hearing Symphony original form . 
 scoring identical Beethoven original 
 manuscript , alterations additions Wagner 
 Mahler eradicated . original ¢emfz - . ) \ 
 restored , resulted unusually slow papers ... commenced Sunday 
 pace , particularly second movement . , audience , 
 course , matter conjecture Beethoven | ended Sunday July audience 
 demanded considerably faster ¢emfi | thousand thousand , _ 
 foreseen technical perfection possibilities modern | years unpaid organist St. Jude S , W hitechapel — 
 wind instruments . rate , performance Canon Barnett sChurch . — — editor Zhe Builder 
 highly interesting experiment , great credit } - years , wes near end life 
 interpretative powers Paul von Klenau . rest | Prolific writer variety subjects , chiefly 
 programme identical 1824 , including | 
 apart Ninth , Overture Die MWethe des Hauses 
 Kyrie , Credo , Agnus Dei JA/7ssa 
 Solemnis . event commemorated 
 unveiling memorial tablet house 
 Ungargasse Ninth completed 
 winter 1823 - 24 

 INTERNATIONAL CHAMBER MUSIC 




 655uestions general musical interest . 
 stated simply briefly , sent , 
 written separate slip . undertake 
 reply post 

 Q.—(1 ) information Beethoven 
 Scottish Dances ? ( 2 ) meaning ‘ Storm ’ 
 applied Liszt Hungarian March ? — J. J. B 

 A.—(t ) Zcossaises pianoforte , 
 military band — posthumous . Beethoven arranged 
 - Scotch songs voices chorus , 
 pianoforte , violin , violoncello . ( 2 ) Liszt composed 
 pieces called Ungarischer Sturm - Marsch ( Hungarian Storm- 
 March ) . way word ‘ Storm ’ music 
 weather . Liszt uses term 
 military sense — storm attack . pieces battle 
 marches 

 Q.—Which correct edition Bach ‘ 48 ’ ? 
 Hallé , Kohler edition . Prelude 
 . 26 Hallé marks opening phrases s¢accafo , 
 Kohler marks /gato . right ? — 
 A.R.C.M , STUDENT 

 R. G.—(1 ) analyses works . 
 ( 2 ) fact use pedal indicated 
 guide . piece Mendelssohn F minor 
 Prelude sound poor . scope 
 pedalling practically pianoforte music , 
 earliest . simpler , earlier , polyphonic 
 music , greater need discretion — 
 word taste 

 G. B.—Mason Zouch Technique ( Schirmer : 
 Winthrop Rogers ) ; Hanon Piantste Virtuoso , 
 German edition Beethoven Sonatas 
 Novello 

 C. E. D.—In ‘ Gave thee life bade thee feed , ’ 
 bade ’ pronounced ‘ bad 

 G. B. S.—In bar 27 Prelude G major Book II . 
 ‘ 48 , ’ play E D sharp_C sharp D sharp turn 

 J. L. V. H.—We play Beethoven Waltzes 
 paces varying ¢ { = 104 g = 138 

 LAVENGRO.—For information copyright 
 Tennyson poems inquire Messrs. Macmillan 




 SPECIAL NOTICE 


 1924 


 CONTENTS 


 MONTH . 


 YOVELLO & CO . , LIMITED 


 PAGE 


 GEORGE 


 GEORGE RATHBONE 


 ONIC SOL - FA PUBLICATIONS 


 PUBLISHED 


 H. W. GRAY CO . , NEW YORK 


 INDEPENDENCE , FLEXIBILITY , STRENGTH FINGERS , 


 MUS SICAL TIMES 


 CHARGES ADVERTISEMENTS 


 A. B 


 ORK 


 JRSES 


 TRETCH , 


 INGERS 


 LONDON 


 COMPOSED 


 JOHN E. WEST 


 EXTRA SUPPLEMENT 


 672 


 933 . 


 ENGLAND 


 1923 


 NOVELLO 


 OCTAVO EDITION 


 ANTHEMS 


 ETERNAL GOD THY REFUGE 


 ANTHEM FESTIVAL GENERAL USE 


 COMPOSED 


 S. J 


 C55 


 Q — — _ & = - 


 2 < J. 


 ETERNAL GOD THY REFUGE 


 — — — 7 CO 


 SES EE — E — EEE _ EES — — — — _ — # 9 


 67 C8 


 EXTRA SUPPLEMENT . 


 ETERNAL GOD THY REFUGE 


 ETERNAL GOD THY REFUGE 


 SS ? SSS SS SE EEE ESSE 


 ETERNAL GOD THY REFUGE 


 TI , 


 — = SS | = 


 ETERNAL GOD THY REFUGE 


 EEEE 


 ETERNAL GOD THY REFUGE 


 ETERNAL GOD THY REFUGE 


 7D - 


 ETERNAL GOD THY REFUGE 


 ( 11 


 NOVELLO 


 OCTAVO EDITION 


 


 1016 


 827 . 


 745 . 


 797 . 


 410 . 


 45 . 


 151 . 


 846 


 ANTHEMS 


 RRERERREL SE 


 BRERRERERSESEEREKRERRRERR RR ERRAR ERE SERERRER