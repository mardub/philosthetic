


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 PUBLISHED MONTH 


 JULY 1 , 1876 


 NOVELLO , EWER CO 


 80 & 81 , QUEEN - STREET , CHEAPSIDE 


 EREFORD MUSICAL FESTIVAL . 


 OPRANO.—RE - ENGAGEMENT REQUIRED 


 OPRANO . — - ENGAGEMENT REQUIRE 


 S } ; MARY MAGDALENE , PADDINGTON . — 


 514 


 PROFESSIONAL NOTICES 


 USIC ENGRAVED , PRINTED , PUB 


 RUSSELL MUSICAL INSTRUMENTS . 


 EAN CHEAP MUSICAL INSTRUMENTS . 


 ORGAN - TONED HARMONIUMS . 


 HATTERSLEY & CO.’"S CELEBRATED 


 ORGAN - TONED DRAWING - ROOM MODEL 


 HATTERSLEY & CO . 


 BOOKS DESIGNS 200 TESTIMONIALS 


 ROOMS : — 


 STANDARD AMERICAN ORGANS 


 MANUFACTURED 


 PELOUBET , PELTON & CO . , NEW YORK 


 METAL EQUITONE FLUTES , KEYS 


 CYLINDER FLAGEOLETS , KEYS 


 & W.SNELL IMPROVED HARMONIUMS , 


 ALS 


 LONDON AGENT 


 PIANOFORTE SALOON 


 55 , BAKER STREET , LONDON , W 


 TESTIMONIAL . 


 HENRY TOLHURST , 


 AST LONDON ORGAN WORKS 


 QUARTERLY SALE MUSICAL PROPERTY 


 KIRKMAN , | NIEDERMAYER , 


 NEWELL 


 ALLISON 


 ESTABLISHED APRIL , 1866 . 


 ENGLISH GLEE UNION . 


 ASSISTED 


 R. WALTER MACFARREN MORNING 


 CONCERT , ST . JAMES HALL , SATURDAY , JULY 1 , 


 YORKSHIRE ( ST . CECILIA ) CONCERT 


 REDUCED 


 OUSELEY 


 POINTED 


 PRICES 


 MONRK 


 PSALTER 


 READY . 


 TONIC SOL - FA EDITION . 


 AKER PIANOFORTE TUTOR 


 MANUAL SINGING 


 USE CHOIR TRAINERS & SCHOOLMASTERS , 


 FORTIETH EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT - SINGING MANUAL 


 APPENDIX 


 ( COMPANION WORK 


 COLLEGIATE VOCAL TUTOR 


 CATHEDRAL CHANT BOOK 


 NEW EDITION 


 DR . BENNETT GILBERT POPULAR WORK 


 SCHOOL HARMONY 


 SUBJECT SPECIAL EXERCISES . 


 


 MENDELSSOHN WORKS 


 CRITICALLY REVISED 


 DR . JULIUS RIETZIs issued Messrs. BREITKOPF & HARTEL 
 Leipzig . 
 Edition form similar ‘ 

 Works Beethoven published 
 firm 

 Works published ( printed engraved plates ) 
 Score , Orchestral Vocal Parts , Vocal 
 Works Pianoforte Accompaniment . parts 
 Chamber Music , sake practical convenience , 
 included Edition Score 




 NEW EASY EDITION 


 BRAHMS 


 UNGARISCHE TANZE 


 C. JEFFERYS , 67 , BERNERS ST 


 T PUBLISHED 


 NEW SONGS 


 ENGLAND MUNICIPAL GLEE . 


 MAYOR CORPORATION 


 OUTHWELL SCHOOL SONGS 


 OPINIONS PRESS 


 518 


 ADIES ’ SCHOOL , LANGHAM HOUSE , 


 RINITY COLLEGE , LON DON — 


 BUSINESS SALE . 


 BUSINESS SALE . 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 JULY 1 , 1876 


 BACH MASS B MINOR . 


 EBENEZER PROUT , B.A. 


 ALTO . _ — — — _ 


 UAT { = — L 


 6 6 76 


 ALTO 


 TENORE 


 520 


 TENORE . 


 EEE EE 


 SS 


 222 S25 SS SS SS SSS : 


 1 " 7 — 


 PE OE EO 


 PURCELL SOCIETY 


 CEDIPUS COLONOS 


 MAJESTY OPERA 


 ROYAL ITALIAN OPERA 


 PHILHARMONIC SOCIETYIr doubt compliment Herr Rubinstein 
 perform ‘ ‘ Dramatic Symphony ” sixth Concert 
 Society ; placed record 
 given directors cost subscribers ; 
 hour attention audience 
 bestowed great works art 

 exact expensive testimonial dis- 
 tinguished guest . word “ Dramatic ” exceed- 
 ingly useful composition fantastic 
 incoherent , covers multitude con- 
 sidered defects production regular construction . 
 melody beauty treatment 
 “ ‘ Adagio , ” said move- 
 ments better ; feeling regret widely 
 spread labour spent small 
 result . seventh Concert Sterndale Bennett 
 Overture “ ‘ Merry Wives Windsor ’’ wel- 
 come item programme . Written age 18 , 
 composer student Royal Academy 
 Music , work unmistakably shadows forth power 
 shown ; wonder 
 remain manuscript . Concert 
 Mdlle . Anna Mehlig won - deserved applause 
 artistic rendering Beethoven Concerto E flat . 
 morning concerts attended , novelty 
 introduced Rheinberger ‘ ‘ Wallenstein Camp , ” 
 movement displaying clever instrumentation 
 catching bits melody 

 FESTIVAL TRINITY COLLEGE , LONDON 




 526An excellent Concert given choir St. 
 Thomas Church , Westbourne Hall , Westbourne- 
 grove , 7th ult . , able conductorship Mr. 
 H.R. Eyers . occasion atten- 
 tion classical nature programme provided 
 Mr. Eyers ; present 
 occasion included Spohr - fourth Psalm , ‘ ‘ 
 Earth Lord , ” Motett Ferdinand Hiller , ‘ Oh ! 
 weep ’’ ( performed time 
 country ) , Mendelssohn Ninety - eighth Psalm double 
 chorus , “ Sing Lord new - song , ” 
 Flotow Serenade , “ slumber leaves - night ” ’ 
 ( violin obbligato artistically played Miss 
 Gabrielle Vaillant ) , seen conductor 
 undertaken easy task choir . 
 principal vocalists — Miss Amy Aylward , Miss Reimar , 
 Miss Thekla Fischer , Mrs. J. France Collins , Miss Kate 
 Brand , Madame Patey , Mr. Henry Guy , Mr. Harry Selig- 
 mann , Mr. Robert George , Mr. Gordon Gooch — 
 acquitted perfection ; instrumen- 
 talists — Miss M. Bucknall ( pianoforte ) Mr. Lazarus 
 ( clarionet)—elicited warmest applause . choral 
 singing uniformly good evening , 
 respect Concert genuine success 

 Tue Lower Rhenish Festival commenced Whit 
 Sunday performance Handel Oratorio ‘ “ So- 
 lomon , ” solo vocalists Mdlles . Meysenheym 
 ( Soprano ) Amalie Kling ( Contralto ) , Herren Ernst 
 ( Tenor ) , Adolf Wallndfer ( Baritone ) , Herman Pfeiffer 
 ( Bass ) . band chorus numbered 500 , 
 excellent training Herr Ferdinand Breunung evi- 
 denced uniformly perfect rendering choruses . 
 Oratorio given originally scored , 
 addition Mendelssohn Organ . second 
 concert , following day , Beethoven ‘ Eroica ” 
 Symphony Overture “ Euryanthe ” ’ finely 
 performed band . Schumann Cantata , ‘ Des 
 Sangers Fluch , ” Mendelssohn Finale “ Loreley ” 
 gave choir opportunity displaying 
 powers , Mdlle . Kling , solo parts , gaining high 
 honours . important work selected Brahms 
 ‘ ‘ Triumphlied , ” chorus parts . day 
 devoted , usual , “ Artists ’ Concert , ” 
 interesting programme provided . Madame Essi- 
 poff pianist , admirable rendering 
 Liszt Etude D flat received encores , 
 responded . Herr Breunung played Men- 
 delssohn Sonata . 1 organ , order exhibit 
 new instrument recently erected local builder . 
 credit Herr Breunung , capa- 
 cities pianist , organist , conductor proved 
 thoroughly competent 

 Ar Mr. Henry Leslie subscription Concert 
 rst ult . , excellent selection - music 
 given , encores Sir W. S. Bennett - song 
 ‘ Come , live ; ” R. L. De Pearsall Madrigal , ‘ * Lay 
 garland hearse ; ” Morley “ ‘ month 
 maying ; ” Walter Macfarren - song , ‘ ‘ Shepherds 
 maidens fair . ” new melodious - song , 
 concert - giver , entitled ‘ ‘ Dream calm , ” 
 enthusiastically received , Mr. Abercrombie , sup- 
 plied place Mr. Sims Reeves , absent indisposi- 
 tion , elicited warm applause . instrumentalists 
 Madame Varley - Liebe ( violin ) Mr. Charles Hallé 
 ( pianoforte ) . closing concert season 
 respect highly attractive , addition truly 
 refined singing choir , Mr. Sims Reeves appeared 
 sang best evening 




 ANTHEM VOICES 


 O -- 84 . 


 7 5 \ : 


 = 4 . 1 4 . 


 1 4 J 


 | N 


 LORD , THEE 


 SS 


 53 


 REVIEWS 


 ORIGINAL CORRESPONDENCE 


 PROFESSOR BLACKIE PROFESSOR 


 OAKELEY 


 EDITOR . MUSICAL TIMES 


 535 


 VOCAL DRAMATIC ARTISTS ’ PROTECTION 


 SOCIETY . 


 EDITOR MUSICAL TIMES 


 GOD SAVE KING . ” 


 EDITOR MUSICAL TIMES 


 ALFRED B. ALLEN . 


 ALFRED ALLEN “ GAVOTTE . ” 


 EDITOR MUSICAL TIMES 


 CLEVELAND WIGAN 


 EDITOR MUSICAL TIMES 


 A. B. 


 ‘ BENEDICITE ’ SUNG ? ” 


 EDITOR MUSICAL TIMES 


 “ MACBETH ” MUSIC . 


 EDITOR MUSICAL TIMES 


 LET 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWSBrixton.—On Tuesday , 30 , St. Matthew Choral Society 
 gave concluding Concert season schools , Church 
 Road , vocalists Miss Agnes Larkcom , Miss Adelaide Bliss , 
 Mr. Alfred Pitman , Mr. Robert Pakeman . Mr. J. B. Gaunt 
 Mr. S. Fisher accompanied , Mr. George Shinn conducted 

 Buxton , DERBYSHIRE.—The opening Concert season , 
 direction Mr. Julian Adams , took place Thursday evening 
 22nd ult . , excellence programme war- 
 ranted large attendance . vocalists Madame Sinico , 
 Madame Demeric - Labiache , Signor Campobello , 
 met usual success . principal features 
 evening performance Beethoven Concerto Op . 56 C , 
 solo parts played Messrs. Adams , Witte , Walton 
 manner highly appreciated lovers artistic . Mr. 
 Julian Adams , loses opportunity reciprocate confidence 
 reposed patrons , congratulated 
 great success concert season 

 CANTERBURY.—Handel Oratorio , Samson , perfermed 
 Dr. Longhurst choir , 14th ult . Miss Anna Williams , Miss 
 Joyce Maas , Mr. Cummings , Mr. Wadmore , Mr. Rhodes 
 solo vocalists . choruses rendered . Mr. T. 
 Morrow played trumpet obbligato ‘ ‘ Let bright Seraphim , ” 
 performance altogether highly appreciated . Dr. Long- 
 hurst conducted 

 538 MUSICAL TIMES.—Jury 1 , 1876 

 Wadham College Hall . programme comprised instrumental 
 compositions Mendelssohn , Beethoven , Mozart , & c. , vocal 
 pieces Brahms Schubert . Mdlle . Sophie Lowe vocalist . 
 — — Philharmonic Society gave Morning Concert rgth 
 ult . Sheldonian Theatre . Schumann Paradise Peri 
 formed ; second comprising Mendelssohn Over- 
 ture Hebrides , Weber Jubilee Overture , & c. soloists 
 Mdlle . Sophie Lowe , Mrs. H. Blake , Miss Enriquez , Mr. W. Shake- 
 speare , Mr. H. S. Howell . Mr. Taylor conducted . Concert 
 successful 

 PARKSTONE , near BouRNEMOUTH.—On Wednesday , 14th ult . , 
 Organ Recital , aid funds permanent Organ 
 church , given Mr. Alfred J. Eyre , organist S. Peter church , 
 Vauxhall , London . Instrument built Mr. Pulbrook Captain 
 F. Sandys Dugmore , 64th Regiment , erected platform 
 end large Parish School Room , available 
 Recitals , Concerts , & c. programme consisted selections 
 works Beethoven , Schubert , Heller , Molique , Bach , Smart , 
 Mendelssohn , Rink , Spohr , rendered , Bach 
 Prelude Fugue D - demanded.m — Monday 19th 
 ult . Instrumental Concert given room 
 local amateurs ; violin playing Mr. Sanders , Poole , 
 performance Mendelssohn Ruy Blas Overture specially 
 worthy praise . Mr. Eyre contributed solos organ 

 PHILADELPHIA.—At Concert Orpheus Club , 
 given Musical Fund Hall , selection consisted English 
 German Glees . club assisted Mr. Richard Hoffman , 
 pianist , New York , created great sensation , playing 
 Mendelssohn “ Capriccioso , ” Chopin “ Polonaise , ” Op . 53 , 
 admired composition , “ Solitude , ” brilliant Rondo “ Cas- 
 carilla . ” concert decided success 




 MONTH 


 OVELLO TONIC SOL - FA SERIES 


 19 


 MR . CHARLES FRY 


 WESLEY TESTIMONIAL FUND 


 2E NI 


 ND 


 SSELL , 


 HALES 


 HOOCOH HAHN RA 


 OYAL ALBERT HALL CHORAL SOCIETY . 


 TRAINING PROFESSION . 


 STUDENTS , COMPOSERS , PUBLISHERS . Dr. Hottoway , F.C.O. , Musical Editor , 51 , St. Paul’s - road , N.W 

 OZART BEETHOVEN SOCIETY , 
 Beethoven Rooms , 27 , Harley - street , W. President , 
 Marquis LoNDONDERRY ; Vice - President , Herr ScHUBERTH . 
 CONCERT , Benefit Herr SchuseRTH , piace 
 Thursday , 6th July . ScuuBEerT Society afford 
 excellent opportunity young rising artists 

 appearance public . particulars application 




 GLUCK OPERAS , 


 IPHIGENIA TAURIS 


 


 IPHIGENIA AULIS . 


 WAGNER OPERAS , 


 LOHENGRIN 


 


 TANNHAUSER 


 COMMUNION SERVICES 


 FRANZ SCHUBERT 


 NOVELLO OCTAVO EDITION 


 MASSES 


 FRANZ SCHUBERT 


 NOVELLO OCTAVO EDITION 


 540 


 ANGLICAN HYMN - BOOK 


 NEW EDITION , REVISED ENLARGED 


 SECOND SERIES 


 ANGLICAN CHORAL SERVICE BUOK , 


 USELEY MONK PSALTER 


 OULE COLLECTION WORDS 


 PSALTER , PROPER PSALMS , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI- 


 ANGLI 


 JOULES DIRECTORIUM CHORI 


 ORDER HOLY COMMUNION . 


 SUITABLE HARVEST FESTIVALS . 


 PRAISE GOD HOLINESS 


 COMPOSED 


 BERTHOLD TOURS 


 541 


 ALBERT LOWE 


 HARVEST CAROL . 


 “ HOLY I$ SEED - TIME 


 TWOPENCE . NOVELLO , EWER CO 


 HYMNS TUNES 


 


 HARVEST 


 SELECTED 


 HYMNARY 


 HARVEST FESTIVALS 


 


 ORIGINAL HARVEST TUNES . 


 ARTHUR H. BROWN 


 PRICE PENNY 


 PLEAD _ - - - - - - - . 


 G- - - - - - 4 


 * | SUMMER ENDED , HARVEST O'ER . 


 METCALFE 


 HARVEST ANTHEMS 


 ALTER MACFARREN NEW “ SUITE 


 DE PIECES . ” 


 INTRODUCTORY VOLUNTARIES 


 ALLCOTT , W. H.—THOU VISITEST EARTH 


 ATTISON , T. MO PLENTIFUL THY GOOD- 


 AYLOR , W.—THE EYES WAIT THEE . 


 RIDGE , J. FREDERICK.—GIVE UNTO LORD 


 EETON , HAYDN.—THE EYES WAIT 


 542 


 REV . THOMAS HELMORE 


 MANUAL PLAIN SONG 


 PUBLISHED 


 HARMONIES BRIEF DIRECTORY 


 VILLAGE ORGANIST 


 NEW SONG . 


 SONGS DUETS 


 COMPOSED 


 ANTON RUBINSTEIN . 


 THOU’RT LIKK UNTO FLOWER 


 EVENING SONG 


 SONG EGMONT 


 BYHHHNHKOHHDW YF 


 COMPOSITIONS 


 LATE 


 DR : ' $ . « 8S ... WESEEY 


 SERVICES 


 ANTHEMS 


 ORGAN MUSIC 


 PIANOFORTE 


 GLEES , ETC 


 543 


 NEW WORK ORGAN 


 ORGAN 


 INTENDED TO‘ASSIST STUDENT ACQUIRING 


 SOUND KNOWLEDGE INSTRUMENT 


 PROPER MANIPULATION ; 


 SERIES 


 ORIGINAL EXERCISES ILLUSTRATIVE 


 COMPOSITIONS 


 


 PREDERIC ARCHER 


 LONDON 


 NOVELLO , EWER CO . , 1 , BERNERS - STREET ( W. ) 


 80 & 81 , QUEEN STREET , CHEAPSIDE ( E.C. ) 


 REDUCED SHILLINGS 


 NATIONAL AIRS COUNTRIES 


 WORDS 


 THOMAS MOORE . 


 EDITED CHARLES W. GLOVER 


 FOURTEEN SONGS 


 SET 


 POEMS ROBERT BURNS 


 CONTENTS 


 ORIGINAL COMPOSITIONS ORGAN 


 13 


 


 HENRY SMART 


 NOVELLO , EWER CO . COMPLETE 


 UNIFORM EDITION 


 MENDELSSOHN 


 THIRTEEN - SONGS 


 NEW CHEAPER EDITION . 


 R. G. W. HAMMOND PIANOFORTE 


 HYMNARY ( TUNES ) . 


 SPECIMEN PAGE . ) 


 FORWARD ! WATCHWORD 


 HENRY SMART 


 ONWARD , CHRISTIAN SOLDIERS 


 ARTHUR S. SULLIVAN .