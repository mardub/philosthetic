


 the MUSICAL TIMES 


 found in 1844 . 


 publish on the first of every month 


 ROYAL CHORAL SOCIETY , LONDON 


 ROYAL ALBERT HALL . CHORAL 


 HANDEL ENRICO BOSSI ’S 


 MR . HARRY DEARTH 


 AND 


 TENTERDEN STREET , W. | SATURDAY , JANUARYLENT term begin Thursday , January 10 . entr : - | SATURDAY . FEBRUARY oR 
 tion , Monday , January 7 , at 2 . , . I 4 
 ~ introduction to Act iii 

 prospectus , entrance form , and all further information of | ot , 
 ‘ a ws vance of the Apprentice 
 F. W. RENAUT , Secretary . Procession of the master | 
 the ROYAL COLLEGE of MUSIC , maphony Io . 9 , in J Beethoven . 
 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. Ne 

 incorporate by Royal Charter , 1 | : ernst von Dohninyi . 
 lelegrams — " * Initiative , London . Pelephone — * * 1160 , Weste 4 




 MUSICAL 


 the 


 LONDON 


 H.R.H | W ! KA ; 


 ROYAL INCORPORATED GUILD of CHURCH 


 MANCHESTER COLLEGE of MUSIC . MUSICIANS 


 BIRMINGHAM and MIDLAND INSTITUTE . NATIONAL CONSERVATOIRE 


 composer ’ rehearsal 


 NEWMAN STREET 


 UNIVERSITY of DURHAM . 


 ( a few door from OXFORD STREET 


 XUM 


 MACDONALD S ) MITH ’S 


 system 


 of TOUCH and ' TECHNIQUI 


 ' from BRAIN to KEVBOARD . ' ) 


 TO MUSIC master 


 teacher of the 


 VIOLIN 


 W. E. HILL & son 


 HIS MAJESTY ’S VIOLIN maker 


 NEW BOND STREET , LONDON , W 


 49 


 FREE TRADE HALL , MANCHESTER 


 MR . BRAND LANE 'S 


 concert : SEASON 1906 - 7 


 SATURDAY evening , JANUARY 10 


 MANCHES 


 SOCIETY 


 PIANOFORTE MUSICAL INSTRUMENT 


 r and other 


 MAKE 


 WARDOUR ST 


 TER PHILHARMONIC 


 ELLIOTT , SON 


 TO 


 AN 


 BE 


 BOYTON 


 LET 


 PROFESSIONAL notice . — MISS LILLIAN DEWS ( L.R.A.M. ) 


 ( CONTRALTO 


 4 , | R i ( TENOR ) . 


 ALEXANDER GUNNER ( TENOR ) 


 ( TENOR 


 class private lesson for oratorio and concert 


 ( SOPRANO VOCALIST ) . 7 \ } LE S STE EL 


 MISS ELLEN CHILDS 


 MISS MVF ANWY JONES — 9 


 MISS ESTELLA LINDEN " JOHN BROW ? NING 


 change of ADDRESS 


 MISS EMILY SHEPHERD , ange of ADDRE 


 LRC.M,_(SOPRANO ) MR . JOHN RIDDING 


 MISS EMILY HART MR . HERBERT TRACEY 


 N OT . 


 PAR 


 XUM 


 THE MUSICAL 


 I , 1907 . 5 


 MR . CUTHBERT ALLAN 


 B — BARITONE ) . 


 MR . DUDLEY STOW 


 BASS - BARITONE ) . 


 FREDERICK MOORE ( ARAM 


 CONCERT ORGANIST ) , 


 MR . GUSTAV A. W IDMANN 


 GOLD MEDALLIST ) 


 BOYS 


 SOLO 


 MR . DUTTON ’S SOLO boy 


 VOICE , SONG , and SPEECH 


 H. GRIFFITHS 


 » obtain degree 0 } MUS.D. and MUS.B. , 


 evision of MUSICAL composition 


 XUM 


 RC , 0 - 6 


 DF ARTHUR S. HOLLOWAY , 


 PREPARI 


 composition 


 counter- 


 ACOUSTICS 


 DR . LEWIS ’ text - book 


 material 


 of 


 6 the MUSICAL 


 TIMES 


 JANUARY 1 , 1907 


 ARAM F.R.C.O. ) PIANOFORTE , HARMONY , 


 COUNTFRPOINT , COMP ' SITION ORCHESTRATION , 


 1 \ I YU ( A.R.C.0 . 


 OLUNTARY TENOR voice require 


 A. H. H 


 XPFRIENCED ORGANIST 


 CBO EXCHANGE 


 FIFTH AVE . , NEW 


 1 30 


 UNER 


 RGANS 


 SALE 


 VE 


 O.P 


 PLE ASE 


 D’ALMA 


 PNEUMA 


 ORGAN 


 = RECITAL SERIES 


 ZR ! S 


 XUM 


 : BOSWORTH EDITION . 


 EXAMINATIONS . 


 SHELLEY FISHER , S 


 OF SHAKESPEARE 


 musical quotation from the page 


 IS find for 


 the ROYAL COLLEGE of ORGANISTS 


 six song the last four century 


 ; MUSICAL expression 


 FELIX MANSFIELD by 


 XUM 


 the MUSICAL 


 JANUARY 1 , 1907 


 special notice . 


 ST . PAUL ’s CATHEDRAL . 


 PAUL ’S CROSS 


 WALA 


 VW 


 1907 


 the BREWER and the BAKER 


 MARRIAGE at ' TOPP of POWLES 


 NORTH 


 the MUSICAL 


 1907 . 13 


 the child of ST . PAUL ’s , and THI 


 PLAYS they act . 


 NORTH - WEST TOWER of the 


 PRESENT 


 CATHEDRAL . 


 1559 


 1590 


 IOOI 


 14 


 1907 


 08 


 26 


 the MUSICAL 


 TIMES 


 JANUARY 1 


 1907 . 15 


 m . 2035 b. / 1 


 IZABETH R 


 CROSS 


 NI ) lantern of the PRESENT CATHEDRAL . 


 HIS admission as organist of the CHAPEI 


 ROYAL ( 1660 


 1907 


 FOLK - SONG collecting 


 xum 


 17 


 JOHN BARLEYCORN , 


 A SOMERSETSHIRI 


 FOLK - SONG 


 SINGER . 


 FAREWELI NANCY 


 _ _ ~ " 


 the evolution of the 


 CHOIR ORGAN . 


 1ap 


 19 


 20 


 the MUSICAL 


 RICHARD 


 RASHAW ( 16137 - 16049 


 ASHTON , 


 1882 , 


 S. H. 


 1907 


 24 the MUSICAL 


 the LOST score of MENDELSSOHN ’s 


 ' MIDSUMMER NIGHT ’s DREAM ' 


 overture 


 1520 


 XUM 


 F. G. E 


 MOZART ’S SYMPHONY in G MINOR . 


 a+ 2 


 b " = 


 vi 


 — . = 22 2 


 = . 2 = " 2 - 


 _ - — — — _ 


 the MUSICALtw . note which end the two section of the first 
 the ne in the fifth and ninth bar of no . 1 , and other 
 pa ive 

 th regard to the figure with which the viola 
 ac ) ompany the principal subject at the opening , an 
 anecdote be tell on Ferdinand Hiller ’s 
 au'hority . Liszt arrange all Beethoven ’s symphony 
 for pianoforte and in the preface to his 
 arrangement he maintain that every orchestral effect 
 cc : be produce opr the pianoforte . when 
 Mendelssohn read this he turn to Mozart ’s g minor 
 Symphony and say : ' let I hear the first eight 
 bar , with the viola figure render as it sound in the 
 band , and I will believe it . ' 
 the second movement , an Avdante , be in 
 every way a worthy successor to the A//egro molto . 
 it be in e flat major , 6 - 8 time , and start as follow 

 interesting 




 5 SS 


 VS 


 " SS 0 


 28 the musical 


 N MINUI 


 TIMES 


 JANUARY I , 1907 


 vi 


 oe 


 SS 


 I 2after this a rattle passage close the first section ot 
 the Allegro 

 in the working - out of the /7a / e Mozart again , as in 
 the first movement , bring all his science into play : 
 and here , as before , the unscientific hearer little 
 suspect the amount of modulation and contrivance 
 which animate the deeply poetical passage that form 
 the rapid fabric of this wonderful section , and in which 
 the science be keep clear by the art of the part- 
 the section begin in b flat major and 
 go through the key of D , G , C , F minor , e fiat , 
 C minor , f sharp , & c. , until it end in the original 
 G minor and the resumption of the first theme . an 
 interesting variation on ordinary practice be make by 
 repeat the second theme , not in the major but in 
 the minor key ; and an equally interesting change be 
 make in the conclude passage , which be 
 develop so as to form a coda — short , it be true , but 
 sufficient to show that Mozart know the necessity of 
 this portion , which he construct so magnificently 
 three week afterwards in the ' Jupiter ' Symphony , but 
 which it be leave for Beethoven to show the full 
 significance and importance of in his no . 2 , a dozen 
 year later 

 we regret to record the death , at Florence , on December 6 , 
 of ConTE GIOVANNI GiGLiuccl , eld son of the Countess 
 Gigliucci ( née Clara Novello ) , with whom much sympathy be 
 feel by her english friend 




 ASI 


 the musical 


 time 


 JANUARY 1 


 1907 . 29 


 a true poet of the church 


 JOHN MASON NEALE 


 30 the 


 the sand of time be sink 


 1907 


 1866 


 CHRETIEN URHAN 


 1790 - 1845 ) . 


 ST . JOHN ’S COLLEGE , CAMBRIDGE 


 AN recital 


 ORG 


 organist 


 AND CHOIRMASTER appointment 


 DEVONSHIRE COMPOSER . 


 | edward 


 M. D’A i on 
 Weinga 

 Herr V 
 fame , wh 
 recognize 
 Beethoven 
 musician 
 ninth Syr 
 art - work it 
 ustifie . 
 an absolut 
 post - symph 
 complemen 
 elegant ' ; | 
 his fundam 
 schedule of 
 States , equa 

 with Ber 
 French co : 
 ‘ without in 
 hand , Liszt 
 the music . 
 powerful ' * * 
 later work 
 word music . 
 brief out ] 
 well acquain 
 translation 

 W. Reeves . | will not escape notice . the Andante be base on an 

 Herr Weingartner as a conductor have win well - deserve | expressive theme . the Sv / erza be lively , while a bright /7va / e 
 fame , while his merit as a composer have be duly in the major key , precede by a minor / evo , base on the 
 recognize . Ife now come before we as a critic of post | principal subject of the opening movement , bring the work 
 Beethoven symphonist ; but he first tell we — and most | to an effective close . 
 musician will agree with he — Wagner ’s opinion that the 

 th ym phony ( ler stn Beethoven . by Felix 

 ninth Symphony be a ' direct transition ' towards the kook receive 

 B 
 ( Barcelona : Tipografia ' L’Avene 

 complement * of Beethoven ; Mendelssohn be ' clev and | 
 elegant ' ? ; Schumann , in try to become a classic , damage 
 his fundamental nature ; so the author run through the 

 schedule of symphony writer , none of whom , he rightly | the Story of Organ Music . by C. F. Abdy Williams 

 state , equal , far less surpass , Beethoven . | pp . xiv . 295 ; 35 . 6d . net . ( the Walter Scott Publishing 
 with Berlioz , however , there come a new departure . the } Co. , Ltd 

 french composer work on ' poetic basis ' line , but | Zhe Story of the Organ . by C. F. Abdy Williams 




 four - part SONG 


 ; _ — _ — _ - - ' 


 _ = - _ — _ — — ~ — 


 PHILLIS in the NEW - MADE HAY 


 ~~ — _ _ — 


 — _ — _ — 


 — _ - . 4 | 


 2 5 - — } — — | 


 9 5 * = : - _ | 


 PHILLIS in the NEW - MADE HAY 


 PHILLIS in the NEW - MADE HAY 


 pp 4 - - ~ 


 - _ 7 - 2 


 35 the MUSICAL 


 SIK CHARLES STANFORD on MUSIC 


 publishing . 


 TIM 


 ES.—JANUARY i , 1907 


 CHARLES V. STANFORD , 


 WAGNER in AUSTRALIA 


 the MUSICAL TIM 


 DIAMOND JUBILEE OF the EXETER 


 ORATORIO SOCIETY 


 by our SPECIAI 


 CORRESPONDENT 


 ES.—JANUARY 1 , 1907 . 39 


 ROYAL ACADEMY of MUSIC 


 I , 1907surpasse for ec 

 good in ol ur modern music . on December 1 the 
 selection consist of the theme , variation and Polonaise 
 from Tchaikovsky ’s Suite No . 3 in G , Beethoven ’s eighth 

 svmphony , Mozart ’s Clavier concerto in e flat and Cesar 




 HONY or ESTRA . 


 VI 


 1907 


 43 


 MR . ALBERT SPAI ING 's CON 


 ert 


 music in BELFAST 


 SIC in BIRMINGHAM 


 I RRE \ NT 


 MUSIC in BRISTOL and DISTRICT 


 from our own correspondentthe Clifton Quintet hold their second concert for 

 season on December 6 , at the Victoria Rooms . the 
 executant be Mr. Ilerbert Parsons ( pianoforte ) , Messrs 
 Maurice Alexander and Hubert Hunt ( violin ) , Ernes 
 Lane ( viola ) , and Percy Lewis ( violoncello ) . there wer 
 admirable performance of Beethoven ’s ' Ilarp ' quartet 

 E flat ( Op . 74 ) ; Brahms ’s sonata in D minor ( Op . 105 ) for 
 pianoforte and violin , and Saint - Saéns ’s Pianoforte trio 




 4 AI 


 the MUSICAL 


 TIM 


 MUSIC IN CAMBRIDGE 


 from our own correspondent 


 MUSIC IN DUBLIN , 


 JANUARY 1907 . 45 


 MI 


 from our 


 IN 


 OWN 


 EDINBI 


 CORRESIONDI 


 RGII 


 ntnclude Schumann ’s overture , Scherzo and Finale , and 
 Liszt ’s symphonic poem ' Hungaria . ' these , together wit 
 Elgar ’s ' Cockaigne ' overture , be very finely play 

 Mr. Kreisler be b in the Violin concerto of Beethoven 

 super 

 distinction be be engage . I be glad to say that the 
 concert be be e well supp yrte 

 the second of the University concert ( December 12 ) be 
 devote t three of Beethoven ’s Pianoforte tri is , | lay | 
 Messrs. Denhof ( pianoforte ) , Ver ! then ( violin ) and 

 Messeas ( vio Ilo ) . of concert by local artist those of 
 most outstar interest have be the vocal recital by 
 Mr. Alfred C. ing , an excellent baritone , on November 21 ; 
 Miss Jean Waterson , a very promising young soprano , on 
 December i ! the violin recital by Miss Copeland ( an 
 excellent first appearance ) , on November 20 ; and the 
 pianoforte recital of Miss Muriel Kerr Brown , who show 
 pronounce advance in her art , on November 22 . a 




 MUSIC in GLASGOW . 


 \ OM our OWN CORRESI’ON NT . ) ure of the Choral and Orchestral 

 concert on November 27 be the first performance here of 
 Liszt ’s symphonic poem ' Hungaria , " and Herr Fritz 
 Kreisler ’s dignified rendering ol Beethoven ’s Violin 

 mcerto . on December 4 , Mr. Henri Verbrugghen , the 
 accomplished leader of the Scottish Orchestra , occupy the 
 conductor 's desk with conspicuous success . with the 




 the 


 1907i \ Miss h r S 1 gelis . 
 ir ' i m i concel Ss 
 : " : l ri ( ra e incidental music 
 r 2 i 5 nde , ' give for the firs 
 , wer wy a Misses Gl i é harles Tree be the vocalis 
 W 1 Ger l Messrs. Al i t 1 l 
 Fowler Bur . ' rte ] < 
 ir | ' — 
 iusic in MANCHESTER . 
 music in GLOUCESTER and DISTRICT . r k ; n1 
 ( n ) at the H Dr. Kicht 
 , 1 n i Scand 
 i ' r I » \ pr s Tes 
 ' ' ld hor progra 
 W rfor i i Dre Geror s ' . 
 } . wil Dvor verture 
 ) ! r 2 Shire h t r l ’ stified | , : xs ang ‘ 
 Dr. i c 1 lin concer ur uve the solo in 
 fT ! ver i i Ss ( sire ne . ' ' 
 la é W er 2 Berlioz ’s ' Faus 
 | i i i i i 
 reache " ty - second pertor e ese concert 
 i > 7 - 
 . . the soloist be Miss H | » Mr. John Harrison , 
 L / t i ver 5 r 7 
 , Mr. Charles Clark , and Mr. wler Burto \ 
 r 5 r i re be nse ¢ y/. : rea ' } 
 e ving concert ( Vecember ) e \ ariation 
 " Ss r saster ' " : 
 ; ° fu n merry there , ( rg > mann , wer 
 r lw rar ce . " A ' RB 
 NM ; 
 I Phi por 
 I ara 
 | ver Mr. ¢ i klw Miss 
 : I i Mr. W. Hi 
 rs ! rs f 
 t i ral s i rt work wi 
 r. | \ Lier ‘ A 
 Mu " r ~ araw le r 
 Mr. A ’ y. Brewer 1 his t 
 I Cir ( ' f wh Mr. A. H 
 ! perforn 
 M Par ( i d i wi ind 
 i i m Dor W a 
 | 3 . & I Mr. | E. Miles 
 Mt \\ Sartets ea ae ; ) 
 7 f = ni ll display excellent taste ) render group 
 ‘ . ng in German , French , and English . those in Englis 
 I I ! aris ; , 
 } 7 . includ ree litt ywer - song vig s by Mr. Ha 
 r r t our } } - 
 Harty , who himself admirably ympanie throughou e 
 il ) Str i r ag - . ' 
 I Mr. Isidor Cohn play witt acc 1 

 M = I wi Se 1 ' * Novelletten , ' Beethoven ’s ‘S 
 ly ' erir 

 c ee xe Appassi , and other piece . at Mr. Brand Lane ’s thir 
 ; uy " y - age Aes " mm . | subscrif r 24 , Dudley Buck ’s eig 
 [ \ i t w \ ( orgar f ae 




 alist 


 the MUS 


 19075 lyrical vein in taste and feel the amateur 

 r 4 , ' tu f the Beethoven Society at its concert on 

 D er II play Gounod ’s ' Mirella ' overture and 




 MUSIC in NEWCASTLE and DISTRICT 


 IRRES 


 district . 


 ndent . ) 


 MUSIC in NOTTINGHAM and DISTRICT . 


 KOM O own CORRI NT . ) 


 MUSIC in OXFORDrendering 

 and Beethoven ’s C minor Symphony , with the 

 the /iva / e of the latter be take too s VY ; f 
 + ; f+ = } ' sall ; ne * ' sit 




 the MUSICAL 


 TIMESJANUARY I , 1907 

 the professor f music , Sir Hubert Parry , give an 
 nteresting lecture on ' the function of thematic material 
 musical organization " to an appreciative audience , 
 illustration be well render by Miss W. Evans , Miss 
 Colton and Mr. Bolton . ve vice - chancellor be present at 
 the lecture , which take place in the Sheldonian Theatre on 
 November 28 
 on the follow afternoon Dr. Joachim and the Berlin 
 Quartet give a chamber concert in the Town Hall in 
 nnection with the Musical Clul the programme 
 consist of ret juartet — Mozart ’s in D minor 
 ( kk . no . 421 ) , Brahms ’s in b flat ( Op . 67 ) , and Beethoven ’s 

 n C minor ( op . 1 } ) . of course , they be as nearly 
 y play , but in this concert it be again 
 mstrate that three quartet in su without 
 constitute a strain on the audience 




 19 . 


 MUSIC in SHEFFIELD and DISTRICT . 


 MUSIC in the SOUTH - WEST county 


 FROM © own cokresvondent 


 the MUSICAL 


 1907 


 ONSHIRE TOWNS 


 SOMERSETSHIRE . 


 MUSIC in STAFFORDSHIRE 


 MUSIC in YORKSHIRE . 


 ( from our own respondent . ) 


 LEIand Dvorak ’s 
 od tHlect . Al 
 performance , 
 Clouds ' of 
 Ww hic h deserve 

 quartet by Ilaydn and Beethoven ( Op . 135 
 so - call ' Dumky ' Pianoforte trio , ( 
 event of more than common interest be the 
 by student of Leeds University , of ' TI 

 Aristophanes on 




 the MUSICAL 


 I , 1907 


 1907 


 ANN 


 \RIS , 


 BRIEFLY SUMMARIZEDDecer 

 i rin Memorial Hall befor large audience . the 
 ' er n Mr. Robert Jones , give exc t 
 render Ilear my prayer ' ( Mendelssohn ) ar 
 t St. George ’ ( Elgar ) Ihe soloist be Miss 
 \nr i ] Mr. Charles Keywood . the pianofor 
 ! accompaniment re in the ble hand of 
 y Shepperd and Mr. A. F. Varker respectively . 
 the Musical Soci g its first concert of 
 Nov ber 27 in the Corn Exchange . the 
 progr m « lude Brahms ’s ' Song of destiny , ' Beethoven ’s 
 rtur Coriol 5 Bach ’s Suite in D for flute and 
 ' ra Pia rte iolin solo be contribute 
 " ccess by Herr Back } s and Herr Sebald 
 t ( ( vocalist ng Ma e Lhombino . the 
 1 chorus , number 25 
 e ol elr ul 
 hare r c tne rior 
 i rhe first concert give this season by the 
 St. ( ilia and Voca n take place on Dece er 10 in 
 he | I Hall , wi Handel 's ' Israel in Egypt ' be 
 per ] I } rus hroughout be well give , 
 ' rchestr lecte sual from Hallé ’s Orchestra 
 r wi t } choir , number 250 performer . the 
 principal vocalist be Miss Maggie Jaques , Miss Parr , 
 Miss Gertrude Lonsdale , Mr. Herbert Parker , Mr. Bridg 
 Peters and Mr. Webster Millar . Dr. E. C. Bairstow 

 BROMLEY ( KEN?T).—The Choral Society open its season 
 in the Drill Hall on December 6 with a performance of 
 Barnett ’s ' ancient mariner , which be well render by 
 the choir and orchestra under the direction of Mr. F. Fertel , 
 the solo part be sing by Miss Nellie Dunford , Miss 
 Esther Franklin , Mr. F. H. Blamey and Mr. Graham 
 Smart . the miscellaneous second part include Eaton 
 Faning ’s part - song ' Liberty 




 RK 


 KIDDERMINSTER , 


 120 , 


 MA 


 SEVE 


 STRI 


 TUN ! 


 LOLLY 


 MI . 


 W. D 


 J. W 


 ACES 


 the MUSICAL TIS 


 ES.—JANUARY I , 1907 


 B. H.C 


 special notice 


 the MUSICAL 


 the LAST 


 JESSE A 


 IURING month 


 FIELD 


 LLO ’S CHRISTMAS CAROLS : — 


 ; T. EIN 


 W \M 


 F R ' BSON , S. WALKER 


 S " \RP , CECIL J. , & HERBERT C. MACILWAINE 


 FRANCIS H. D 


 NAU 


 N 192 


 HT : - — 


 MI ' 


 the school MUSIC review . 


 I \ I I S 


 I N 


 CITY of LONDON COLLEGE 


 HITE STREET , MOORFIELDS , E.C 


 MISS E 


 MISS EMILY 


 " ENOR want ( £ 12 


 m d | ( \ 


 I H 


 \ 9 S 


 I I ( 


 NIVERSAR\ SELEC pion ( 


 POST - card at 


 and a handbook 


 ALLEGRO 


 FOR STRINGS 


 ( QUARTET and ORCHESTRA 


 examination in music 


 600 question with answer 


 EDWARD ELGAR . ERNEST A. DICKS 


 MORNING POST - 


 , \ preface to the SEVENTH edition . 


 DAILY CHRONICLI " 


 YORKSHIRE POS 


 ) 4 4 


 ARTHL 


 SI 


 NY 


 the 


 MUSICAL 


 " | = passion of a CHRIST . G. F. HANDEL . 


 the story of the 


 FOR voice and ORGAN 


 cross 


 the REV . E. MONRO 


 J. STAINER , MYLES B. FOSTER 


 the REPROACHES 


 CHARLES GOUNOD 


 LONDON 


 NOVELLO 


 TIMES 


 AND 


 NI 


 ARY 


 the 


 BE N E 


 MODERN 


 1907 


 music for LENT 


 r 


 COMPOSE 


 BENNETT , GEORGE J. ¢ D ) 


 BUTTON , H. ELLIOT ( LD ) 


 GLADSTONE , F. E. ( C1 ! 1 ) 


 LLOYD , C. HARFORD 


 FOSTER , JOHN 


 WOOD , W.G ( DD 


 WRIGLEY 


 anthem in 


 TE 


 LU 


 COMPANY 


 serie 


 ITED 


 DICITE 


 THE 


 NOVELLO 'S anthem for LENT 


 the 


 5 . « I 


 H M 


 musical 


 b. 1 5 


 e. h. T 


 A. D 


 BE . A.S 


 A. W 


 G. A. M 


 h. H 


 \. WI 


 J ) . B 


 M. E 


 J. V.R S 


 TIMES 


 4 SS ) 


 " TI 


 " TI 


 4 U 


 4 W 


 JANUARY 


 1907 


 HL 


 FOR 1 


 SHI 


 A SACRED CANTATA 


 lling scene in the last day OI 


 SAVIOUR ’ 's life on EARTH 


 CHORUS 


 ersperse with hymn to be SUNG 


 I I 


 SHAPCOTT WENSLEY 


 J. H. MAUNDER 


 OLIVET to CALVARY 


 NE ) and 


 BY 


 DAILY TELEGRAPH ( 1 ) , A 


 CHURCH TIMES , M 


 MUSICAI , opinion 7 


 BIRMINGHAM DAILY MAIL , M 


 NOVELLO and COMPANY 


 sacre passion of THI 


 1907 


 the crucifixion _ 


 A meditation 


 REV . W. J. SPARROW - SIMPSON , M.A 


 J. STAINER 


 the cross of CHRIST 


 W. MAURICE ADAMS 


 THOMAS ADAMS 


 NEARER , my GOD , to THEI 


 GETHSEMANE 


 4 CHURCH CANTATA 


 JOSEPH BENNETT 


 EM 


 C. LEE WILLIAMS 


 the last night at 


 BETHANY 


 A SHORT CHURCH CANTATA 


 BENNETT 


 JOSEPH 


 C. LEE WILLIAMS 


 be it nothe to you 


 AN EASY CANTATA 


 REV . E. V. HALL , M.A 


 HOLY REDEEMER 


 passion of CHRIST ALFRED R. GAUL 's 


 G. F. HANDEL . PASSION service 


 EBENEZER PROUT . LENT and GOOD FRIDA 


 NEW EDITION . now ready . 


 SOLI , CHORUS , and ORCHESTRA PENITENCE 


 I I 


 BENEDI ITE , OMNIA OPERA.—S 


 MAGNIFI 


 TRULY G 


 ALL HAI 


 CE 


 prince of PEACE service 


 ALFRED R. GAUL . , - " 


 ‘ HIRE CORRESPONDENT , 


 S ; S 


 S 2 6 ) the training of the child voice 


 NT 


 raining 


 NEW anthem B\ LOUIS NIEDERMEYEI 


 CHURCH MUSIC MORNING and evening service 


 COMPOSITI ( NS 


 MEE 


 IN PRAISE 


 RT - SON I 


 AKT - SONG I 


 LIVER WENDELI 


 C. LEE WILLIAMS 


 PART - S 


 PETER CO 


 ENGLI 


 FI 


 IN I 


 ORI EAR 


 HI LDIE!I I 


 H } IN¢ 


 BY I 


 CO N I 


 VW 


 PATTISON 


 M W S 


 I d h 


 S W 


 humorous part - song . 


 a ] G i ( C 


 I N ( i 


 INI 


 NARCISSUS and ECHO 


 CANTATA for chorus , soli , and orchest ! 


 compose BY 


 EDWIN C. SUCH 


 RNELIUS 


 CHOIR BOYS ’ card 


 N element of MUSIK exercise 


 ) teach boy a second PAR 


 FRANCIS HOWELL ’ CANTATA 


 the song of the month 


 N W 


 LNQUIL NI I ‘ 


 ! I ( 5 , 


 LN ) ] I ) . } MUNRO 


 the 


 the 


 the musical 


 TIMES.-—J 


 AMBRIDGE UNIVERSITY PRESS 


 the indebtedness of 


 handel to works 


 7 how to work they 


 SEDLEY TAYLOR , M.A. , | 


 : W I 


 | PRUE theory of voice - production 


 reveal . 


 ; E. DAVIDSON PALMER , — 


 ( M i ] I " 


 ; MORRIS DANCE TUNES 


 BY the same author collect from TRADITIONAL 


 the TENOR voice and its training source 


 PERI 


 OD . 


 a practical 


 PRAINING chorister 


 DR . VARLEY ROBERTS 


 method of 


 EXAM 


 HERBERT C. MACILWAIN EK 


 6 . M 


 CECIL 


 INATION QUE 


 AND 


 the BOY ’S VOILE e at the breaking , WITH PIANOFORTE ACCOMPANIMENT 


 J. SHARP 


 TWO set 


 SET I. 


 SET II 


 STTO 


 NS 


 the ORIANA 


 LTTE ORITAN 


 collection of early madrigal 


 british and foreign 


 the 


 DAILY TELEGRAPH 


 STANDARD 


 ; ; leader 


 the TRIBUNE , 


 the 


 MUSICAL 


 EDWARD 


 LONDON 


 AN 


 PRICE 


 100 


 time 


 JANUARY 


 ORATORIO 


 COMPOS 


 five 


 D I 


 SHILLINGS . 


 WEST 


 LGAR 


 J.J 


 PALI 


 1907 


 MALI 


 KINGDOM 


 GAZETTI 


 DAILY CHRONICLI 


 DAILY GRAPHIC . 


 BIRMING 


 MIN 


 NI 


 STI 


 R GAZETTI 


 HAM 


 DAILY MAII 


 ~~ | original composition 


 how can I tell for the ORGAN BRI 


 ALFRED R. GAUL . 


 N I 


 ILLEM COENEN . 


 M. TOPLAD\ . INTERM 


 ’ CESAR CUI I 


 MOZARI * SCHUBERT | ae = y i 
 ORTI » FANTASIA axnp FUGUE 1x c munor C. P. E. BACH PEC 
 ar ¢ J xn FE . Wes ' si . I 

 range by 
 WITTEN to . PRELUDI PART IL . ( ' Tue A LES " ) 
 we aes COENEN EDWARD ELGAR 
 ' arrange by G. R. S : x 
 i 
 tr , final : SYMPHONY N BEETHOVEN 
 N ( v. Limite arrange by A. B. P es 
 12 , ADORAMUS TI HUGH BLAII 
 t \rra yl i I ) 
 , anil . : INTERMEZZO ( " Tue I \ ANES ) PI . 
 SYMPHONY in FE : li a C. H. H. PARRY 
 > 14 . BRIDAL MARCH and FINALE ( " Tue Bret I 
 , \ Es ) Cc . H. H. PARRY 
 ; r oo oe \ g W.G. A 
 FULL ORCHESTRA ANDANTE ( 1 s c , o 
 ' \ e , W | . BRAHMS 
 FREDERIC H. COWEN . AN DANTE ( p ; n ( o 
 \ | k. W . BRAHMS 
 I SCOR ] IODERATO CANZONA ( 1 . 
 i t n vi . ) \ ] p ‘ 
 | URCI 
 p ( nue . ) 
 i n c any , 1 

 the MUSICAL TIMES sue cenccnrectiannaratsoeatncsat 
 ; Sa aa = ADORAMUS 4 
 CA » 2 » ag Na , he 




 BRIDAL MARCH & FINALE 


 ird " of ARISTOPHANES 


 C. HUBERT H. PARRY 


 O WW . G. ALCOCK 


 S. COLERIDGE - TAYLOR ’S | 


 INC NTAL MUSK STEPHEN PHILLIPS ’S drama 


 ' thr 


 : nero . \ 


 PROCESSIONAL MARCH 


 first ENTR’ACTE ( * nero " ) 1 ‘ 


 S I 


 AVA \BLE b < for teacher and student 


 2 , peasant 


 WII numerous musical example from the 


 NOVELLO 


 A suite 


 OLD ENGLISH DANCES 


 FREDERIC H. COWEN 


 DANCI 


 DANCE 


 DAILY TELEGRAPH 


 morning 


 EVENING STANDARD 


 DAILY NEWS 


 the DAILY CHRONICLE 


 WESTERN DAILY PRESS 


 GLASGOW HERALD 


 GLASGOW EVENING 


 SUNDAY TIMES 


 SCOTSMAN 


 GLASGOW NEWS 


 AND CO 


 the ORGAN WORKS _ 


 JOHN SEBASTIAN BACH 


 edit by 


 \N i \ 


 i i 


 I i ( 


 book viii | ( 


 i i ( 


 i i i ) 


 p i ( 


 i d d ) 


 | i \ s \ 


 book 


 i i d1 


 I ! I ( 


 I i 


 LONDON NOVE 


 LLO 


 20 


 2a d 


 3a. d 


 5a. d 


 a d 


 BY 


 b.—studie . in six book 


 OB 


 PIECES 


 SONATINA IN 


 NOVELLO 


 AND COMPANY 


 DITEI 


 progressive study 


 arrange in group , and the fingering revise and supplement 


 FRANKLIN TAYLOR 


 NN 


 » 3/2 


 » 4 3 . " 


 » 5/1 39 . * STACCATO 


 LI 


 VEI 


 ITI 


 * > | 41 . " REPETITI 


 I 99 99 - : 


 BI 


 BI 


 fifty - six book , price one shilling each . 


 from the above : 


 select PIANOFORTE study 


 gressively arrange by 


 FRANKLIN TAYLOR . 


 k PRICI one shilling and sixpence each book 


 2 BLE 


 3 KY 


 MAI 


 , LO ! 


 8 . * he 


 2 WH 


 1 , IF 


 NOVELLO 'S 


 CHORISTER serie 


 NTI 


 CHURCH MUSIC 


 XUM 


 the MUSICAI 


 | ANO ! SOLOS 


 RB 


 FA 


 PIANOFORTE DUETS 


 VIOLIN 


 P p h 


 IOLONCELLO S 


 HAN MUSI 


 LOHENG 


 ITKOPE & 


 IGH ST 


 BRI 


 MARLBOROI 


 BREITKOPF & HARTEL 


 1907 


 HARTEL , 


 REGENT ST 


 ORCHESTRAL MUSIC . 


 ( also full score . ) 


 ( N n i 


 I M 


 f. H ( i 


 + J ) S W 


 A.S ( i 


 d i 


 S = ( i 


 h.—o i 


 CHORAL work . 


 P M ( 


 song . 


 I i i k 


 i i ( 


 ) \ i G i 


 book 


 I ( g 


 ( p i 


 i i i 


 picture . 


 ICHA KD WAGNER 'S 


 RIN 


 LONDON 


 ST . PAUL ’s cross in the year 1620 . 


 xum 


 extra supplement . 


 NOVELLO ’S part - song book 


 second serie 


 the MARRIAGE of the FROG and 


 the MOUSE 


 humorous part - song 


 the word from THOMAS RAVENSCROFT ’S MELISMATA ( 1611 ) 


 the music compose by 


 A. HERBERT BREWER 


 p _ — _ — 


 the MARRIAGE of the FROG and the MOUSE . 


 = — = RSS 


 n|- # s 


 ? 2 2 — = = : 


 t 7 : : 


 5 = 2 = = = 3 


 the ma 


 RRIAGE of 


 the FROG and the MOUSE 


 the marriage of the FROG and the MOUSE 


 # 2"-s| 3 er sr ] 


 » CHORUS . 


 the MARRIAGE of the FROG and the MOUSE 


 92 = = _ — — 


 3 - = \ ' \ | 


 MARRIAGE of the FROG and 


 3 ] NS 


 THR MOUSE 


 7 4 


 _ — _ — — _ 


 7 4 


 440 


 BND SNS SENDS 


 the MARRIAGE of the FRO 


 G and the MOUSE 


 15 


 st z | 7 z 


 the MARRIAGE of the FROG and the MOUSE . 


 = = £ — _ = — — _ _ 


 22 


 NS 


 ORGAN 


 CHAMBI 


 LENT H 


 the 


 PRINCE 


 the R