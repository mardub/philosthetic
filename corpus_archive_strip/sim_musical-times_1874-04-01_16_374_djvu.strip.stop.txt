


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 MONTH 


 PUBLISHED 


 APRIL 1 , 1874 


 OUTHWELL COLLEGIATE CHURCH.—A| 


 ROYAL ALBERT HALL CHORAL SOCIETY , 


 SEASON , 1874 


 ROYAL ALBERT HALL . 


 PASSION WEEK PERFORMANCES 


 HANDEL MESSIAH 


 HYMN PRAISE 


 ROSSINI STABAT MATER 


 BACH , PASSION 


 T. MATTHEW ) . 


 BACH , PASSION 


 MATTHEW ) . 


 BACH PASSION 


 ST . MATTHEW ) . 


 HANDEL MESSIAH 


 438 


 PROFESSIONAL NOTICES . 


 MR . J. TILLEARD , 


 USIC ENGRAVED , PRINTED , PUB 


 HARGREAVES PROFESSIONAL 


 CONNOISSEURS ’ HARMONIUM . 


 ( REGISTERED 


 “ « PROFESSOR HARMONIUM , ’ 


 HARGREAVES AMERICAN IRON FRAME PIANO 


 WILLIAM HARGREAVES , 61 , DALE ST . , MANCHESTER 


 HARMONIUMS ! HARMONIUMS ! 


 W. HATTERSLEY 


 CELEBRATED ORGAN - TONED HARMONIUMS , 


 MANUFACTURED 


 GRATEFUL — COMFORTING . 


 BREAKFAST 


 


 MITH 


 PIANO 


 TER 


 ELS , 


 XUM 


 439 


 SAFE TIDY PRESERVATION 


 UNBOUND MUSIC 


 STONE PATENT BOXES 


 RUSSELL MUSICAL INSTRUMENTS . ? 


 W.SNELL IMPROVED HARMONIUMS 


 QUART ERLY SALE MUSICAL PROPERTY . 


 REDUCED 


 PRICES 


 OUSELEY MONK 


 POINTED 


 LON 


 NOVELLO , EWER CO 


 PSALTER 


 DON 


 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 TONIC SOL - FA EDITION 


 READY . 


 1 6 


 R. BENNETT GILBERT SCHOOL HAR- 


 MONY 


 MANUAL SINGING 


 USE CHOIR TRAINERS & SCHOOLMASTERS . 


 - FIFTH EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT SINGING MANUAL 


 APPENDIX 


 ( COMPANION WORK 


 COLLEGIATE VOCAL TUTOR 


 CATHEDRAL CHANT BOOK 


 POPU 


 


 OPU 


 WAX 


 _ — _ 


 100 


 CWO OO 


 441 


 - SONGS 


 VOICES , 


 JACQUES BLUMENTHAL 


 PSYCHE : 


 DRAMATIC CANTATA 


 SOLO VOICES CHORUS . 


 ; MUSIC J. F. H. READ . 


 WILLIAM J. YOUNG 


 NEW - SONGS 


 MEN VOICES 


 MENDELSSOHN 


 ENTLY SIGHS EVENING BREEZE . 


 CHARGE LIGHT BRIGADE . 


 CHORAL SOCIETY 


 PRICE TWOPENCE 


 GRAND OPERATIC DUETS 


 000 000 


 ELEMENTS MUSIC 


 SYSTEMATICALLY EXPLAINED 


 HENRY C. LUNN . 


 ROYAL ACADEMY MUSIC 


 _ WILLIE PAPE . 


 LLANGOLLEN 


 IL TROVATORE .. 40 


 67 , BERNERS STREET , W 


 442 


 RIXTON CHORAL SOCIETY , Monday , 20th April , 1874 , 
 SIR Jj . BENEDICT “ ST . PETER 

 Goa Best SOCIETY , Beethoven Rooms , 27 , 
 Harley - street , W. President , Sir Julius Benedict . Director , 
 Herr ScHuBerTH . Eighth Season , 1874 . SECOND CONCERT 
 place WEDNESDAY , 29th ApRIL , 1874 . 
 Programme devoted R. Schumann Vocal Instrumental Com- 
 positions . Society affords excellent opportunity young 
 rising artists appearance public 

 LOUCESTER COUNTY ASYLUM , near 
 Gloucester.—_MALE ATTENDANTS WANTED . Wages 
 commence { 24 annum , Board , Lodging , Washing . 
 ( Musicians preferred . ) Previous Asylum experience necessary . 
 Applications , stating age , musical capabilities , sent 
 Superintendent Asylum 




 _ — _ } 


 MUSIC TRADE 


 INTRODUCTION . 


 MODES TONES . 


 STANDARD CONCERT PITCH 


 EG | 


 GG | F G | D 


 | BFE DFE | 


 BB B 8 


 QO » HO RB TMH 


 44.0 


 ST 


 F F C F 


 E D # B D¢ | | 


 MUSICAL NEIGHBOURS . 


 447CRYSTAL PALACE 

 Tue Symphony , Mr. Ebenezer Prout , given 
 time , 28th February , achieved success , 
 proves audience ready appreciate 
 acknowledge worth , found ; 
 credit Mr. Manns , love music 
 countrymen known , affording hear- 
 ing awork suchimportance byan Englishman . 
 carefully planned composition Mr. Prout Sym- 
 phony unjust , single hearing , 
 impresses conviction 
 production profound thinker art , 
 movement defined plan ; subjects melodious , 
 skilfully treated ; instrumentation shows 
 intimate knowledge orchestral effect . début Miss 
 Emma Barnett , played Beethoven pianoforte con- 
 certo G , marked feature concert , 
 performance remarkable intelligence , 
 fluency execution , rhythmical accuracy . 
 cadences , written brother , Mr. J. F. Barnett , 
 excellently played , elicited - deserved burst 
 approbation . following concert Beethoven fine 
 music ‘ ‘ Egmont ” given , received en- 
 thusiasm . Brahms variations , orchestra , 
 theme Haydn , performed , remark- 
 able work , originality variety composition , 
 masterly instrumentation variation , keep- 
 ing attention hearers alive note . 
 loudly applauded , , trust , frequently 
 place excellent concerts . Schubert Otett , 
 played , r4th ult . , stringed 
 instruments orchestra , wind instruments 
 doubled , prefer speak , 
 occasion freely expressed opinion desecra- 
 tion fine work . Mr. Sullivan Overture “ ‘ 
 Sapphire Necklace ’ opened concert , 21st ult . , 
 spirit ; hackneyed ‘ “ Qui la voce , ” 
 sung Madame Sinico , came Mendelssohn ever- 
 welcome violin concerto , played Herr 
 Joachim play . absolute novelty 
 programme “ Song Destiny ” ( Schicksalslied ) , 
 chorus orchestra , Brahms , shared fate 
 novelties , indifferently performed , 
 movement demanding , chorus , 
 refinement , hardly received hands 
 Crystal Palace choir . entering analysis 

 448 MUSICAL TIMES.—Aprit 1 , 1874 

 merits , mention recognised 
 hearers work genius highest order , 
 opinion cordially agree . verdict 
 given nearly musical Germany 
 composition occasion unanimously endorsed 
 critical audience assembled 
 Sydenham ; Herr Brahms reckon 
 cordial reception accorded future work 
 pen . English translation text 
 Rev. J. Troutbeck , M.A. Beethoven Symphony ( . 4 , 
 B flat ) received rendering hardly looked 
 outside Crystal Palace — Adagio narrowly escap- 
 ing encore , , extreme length 
 concert , inevitable . item 
 interest great extent spoiled , placed 
 end long programme . English 
 people alive fact high - class music can- 
 properly listened fatigue 
 brain ; , great work follows close 
 concert , attention 
 time wearied ? experienced sense 
 fatigue produced regulation Overtures , 
 Symphonies , Concerto , form ordinary 
 orchestral programme classical concerts ? 
 , , good example given 
 ‘ * Exhibition Concerts ” year , Over- 
 ture , Symphony Concerto , ballet music , 
 March , vocal pieces , formed programme 
 satisfied satiating . return — orchestral 
 variations Brahms produced effect 
 performance concerts , 
 reason far seek : came hours anda 
 quarter classical music break 

 MAJESTY OPERA 




 ROYAL ALBERT HALL CHORAL SOCIETY 


 SACRED HARMONIC SOCIETY 


 XUM 


 449 


 MR . WILLEM COENEN CHAMBER CONCERTSWAGNER SOCIETY 

 fifth concert , 13th ult . , contained 
 popular extracts Wagner works 
 increase longing hear legitimate 
 place — Operatic stage . chorus Messengers 
 Peace , early Opera “ Rienzi , ’ 
 wish - called development composer 
 genius arrested , thoroughly legitimate 
 vocal effects , pure attractive melody . 
 Overture “ Die Meistersinger von Nurnberg , ” 
 finely played received enthusiasm ; choral 
 song ‘ * Wachet auf , ” Opera , encored . 
 warm welcome accorded specimens 
 “ Lohengrin , ” Prayer Finale Act 
 eliciting solid marks approval . Beethoven 
 “ ‘ Choral Fantasia ” , notwithstanding 
 pianoforte ably rendered Mr. Walter 
 Bache ; composer Overture ‘ “ King 
 Stephen , ” , conceive , place 
 concerts , received ample justice . Ina song Abbé 
 Liszt , Miss Sterling , created marked effect , 
 gave good expression Rubinstein ‘ ‘ Die Waldhexe , ” ’ 
 accompanied pianoforte Mr. 
 Dannreuther , conducted concert usual 
 energy skill 

 BRITISH ORCHESTRAL SOCIETY 




 MR . HENRY LESLIE CHOIR 


 450 


 1874 


 TENOE 


 XUM 


 DIALOGUE VOICES . 


 LU 


 LJ 


 2 SR 


 AY 


 5 = 2 || 


 = PS EL : 


 XUM 


 = SS — — — _ 


 SSS 


 YUM 


 -- -0- ( W 


 TTT 


 45 


 REVIEWS 


 456 


 XUM 


 457 


 ASHDOWN PARRY 


 459 


 ORIGINAL CORRESPONDENCE 


 CHURCH SINGING . 


 EDITOR MUSICAL TIMES 


 THOMAS VINCENT , 


 WAGNER CONTROVERSY — POETIC BASIS . 


 EDITOR MUSICAL TIMESpublic opinion : ‘ ‘ far shalt thou , farther . ” 
 addition seventh string lyre aroused 
 opposition . Similar obstructiveness attends pro- 
 gressive effort . strife yields fruits peace . 
 expressed recent review “ letter ” 
 Wagner , tacitly acknowledged 
 spirit Mr. Joseph Bennett carefully worded contribu- 
 tion subject , Number ably 
 edited Journal March . leading article temperately 
 approaches pith matter , candidly 
 sets forth new views . permit add Ido think 
 single hit great musician . 
 says — says eloquently — rivets 
 mind continuously growing deepening 
 conviction Wagner right , opponents 
 understand . province re- 
 introduce invective . let allowed simply 
 State view conclusions foregone . 
 meas clear sunlight music grow . 
 utter oft repeated words -adverse parties 
 irksome ; , help saying , briefly 
 stating case occurs mind , deem 
 Wagner opponents error 

 says Beethoven truly said . composers 
 dismissed having “ tell , ” justly dismissed . 
 project separating ‘ ‘ man ’ ” ’ 
 ‘ theory , ’ , ‘ ‘ . ’ ? music poetry 
 half arch , “ Yes . ’ ” ’ programme makers know 
 fact , , find legend 
 attached instrumental piece , invent tack 
 . “ Lion love , ” concerto 
 Beethoven ; ‘ “ ‘ Shake evil spirit , ” violin 
 solo . line poetry , legend , story , 
 . Illustrations flow ad infinitum . composers 
 work ina comatose state ? ‘ ‘ Yes . ” Beethoven knew 
 ? ‘ “ . ’ ? Beethoven left record struggles ? 
 , “ Yes , . ” write choral 
 symphonies ? ‘ Straight jacket resulted . ” 
 Instrumental music incomplete ? ‘ Certainly 

 allusions compelled pass , 
 , pleasure , 
 expatiate head question considerable 
 length . written believe 
 subject paramount importance pursuit art 
 , 




 POETIC BASIS MUSIC 


 THEORY HERR WAGNER GENERALLY 


 EDITOR MUSICAL TIMES 


 460This logical definition definitions . 
 public , matters altogether 
 aside , afford practical proof . public 
 sit hours , enjoy note 
 Meyerbeer Operas , end hour instrumental 
 music tired — end hours wish 
 hear note music long 
 lived 

 retorted pander 
 people — true . musical audience appreciate 
 Mozart , Meyerbeer , Beethoven Operas , taken 
 fair criterion healthy state ear , 
 case musicians 

 Instrumental performers , professional amateur , 
 particularly players stringed instruments , 
 passionately fond instruments difficulties 
 surmounted acquiring ( , , let 
 charitably suppose , unconsciously ) , 
 intolerable bore circumstanced . 
 ( omitting works great masters , 
 excelled took hand ) think 
 refer origin continuance string 
 quartetts quintetts peculiarity . cases 
 enjoyment shared hearers ; , hearing 
 execrable combination runs , strums , trills , 
 Thalberg ‘ ‘ Home , Sweet Home , ” hearer struck 
 labour required learn , startled 
 interested ‘ ‘ sleight hand ” required . 
 cases difficulty produces pleasure hearer 
 player ; way , example fugue D 
 minor ( . 4 ) J. S. Bach , violin ; everybody : 
 knows better instruments , 
 person , d priori , fugue 
 instrument impossibility . masterly way 
 gives principal parts ingenious devices 
 stretto , pedal , & c. , constitute excellence 
 commend cultured musician end 
 time . Opera , , highest excellence , 
 shall farther treat 
 thesis temporarily omitted . admit ‘ ‘ Instru- 
 mental music incomplete , needs aid words 

 Let Wagner contradict 
 . Opera , “ ‘ Le Vaisseau Fantéme , ” says 
 Overture ( instrumental course ) com- 
 plete development libretto . ( sec . 1 ) 
 description ship cast rocks , ( sec . 2 ) rocks 
 suffer strike , ( sec . 3 ) crew sign compact 
 evil presence captain , & c 

 melody , chord , rhythm idea 
 substantives printed italics ? leave reader 
 discover ; help degree . , , 
 wilfully rushing ‘ “ ‘ Beethoven error , ” with- 
 Beethoven excuse ignorance , “ yearning ” 
 truth 

 Let farther Wagner embodies error 
 doctrines . says music ( instrumental , presumably 
 context ) produce 
 effect quiet walk sunset wood far 




 XUM461 

 , suffice lack regular 
 phrased style ( soul true music ) Mozart , 
 Gluck , Meyerbeer , & c. , Italian school , 
 fervent energy Beethoven , ana bold harmonies 
 Handel 

 , Sir , , & c. , 
 Joun W. Hinton , A.B. , Mus . B 




 ‘ “ * MOVEABLE DOH ” 


 EDITOR MUSICAL TIMES 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWSueen powerful rendering trying . 
 main excellences performances lay chorus singing . 
 good , times artistic . Altogether , opera 
 departments — grouping supernumeraries , gipsies , 
 soldiers , courtiers — completely satisfactory 
 company given 

 EpinBuRGH.—In connection visit Mr. Mapleson Italian 
 Opera Troupe , excellent concerts given Edinburgh 
 Choral Union crowded houses . , February 21st , com- 
 prised selection solos choruses Handel 
 works , second , operatic solos . prin- 
 cipal artists Mdlle . Titiéns , Mdme . Sinico , Mdme . Trebelli- 
 Bettini , Mdlle . Macvitz , Signori Bettini , Borella , Perkin , Agnesi . 
 Mr. F. H. Cowen presided pianoforte . second concert , 
 r4th ult . , Handel Messiah given , complete orchestra , 
 Madlle . Titiens , Mdme . Trebelli - Bettini , Signor Fabrini Signor 
 Agnesi principals . chorus , numbered 250 voices , 
 sang great care intelligence ; Mr. Adam 
 Hamilton , conducted concerts , deserves great credit forthe 
 high state efficiency brought . organ- 
 ist Mr. T. Hewlett , Mus . Bac . , Oxon , playing contributed 
 greatly success concerts —— PRoFESSOR OAKELEY gave 
 Organ Recital Music Class Room , 12th ult . , 
 excellent programme performed alarge audience . Bach prelude 
 fugue E minor remarkably played astudent . Pro- 
 fessor Oakeley song , ‘ ‘ ’ Tis thou art fair , ” encored , 
 played little canzonet , “ Sempre pili t’amo . ”——THE 
 seventh annual concert University Musical Society given 
 Wednesday evening , 18th ult . , Music Hall , large 
 audience . number choristers , amounting 200 , attained 
 standard singing far years , equal 
 improvement observable orchestra . Professor Oakeley , 
 coming forward assume conductor place , greeted 
 demonstrative applause audience choristers . 
 concert , miscellaneous , opened students ’ song , 
 “ ‘ Condiscipuli Canamus , ” composed Professor Oakeley 
 concert 1869 Latin words Professor Maclagan . Mr. Galletly , 
 student , played , Mr. Carl Hamilton , Beethoven sonata F , 
 Op . 17 , pianoforte violoncello , originally written piano 
 horn . Beethoven Symphony commenced second 
 programme ; promising young student - pianist , Mr. 
 Coates , played Schumann “ Schlummerlied , ” Schubert Im- 
 promptu flat great neatness , light touch , clear articula- 
 tion . concert , highly successful , ended 
 “ National Anthem 

 EvertTon.—The annual concert Choral Society given 
 Tuesday evening , roth ult . ,in Schoolroom , filled 
 large fashionable audience . _ conductorship Mr. 
 Hamilton White , organist Parish Church , East Retford , 
 Society great progress , choral pieces given 
 general excellence . Songs duets contributed Miss Annie 
 Williamson , Miss Naylor , Misses Postlethwaite , Miss Roe , 
 assisted Mr. Dimock Retford ; Mr. White delighted audi- 
 ience playing pianoforte solo 

 logical definition definitions . 
 public , matters altogether 
 aside , afford practical proof . public 
 sit hours , enjoy note 
 Meyerbeer Operas , end ove hour instrumental 
 music tired — end hours wish 
 hear note music long 
 lived 

 retorted pander 
 people — true . musical audience appreciate 
 Mozart , Meyerbeer , Beethoven Operas , taken 
 fair criterion healthy state ear , 
 case musicians 

 Instrumental performers , professional amateur , 
 particularly piayers stringed instruments , 
 passionately fond instruments difficulties 
 surmounted acquiring ( , , let 
 charitably suppose , unconsciously ) , 
 intolerable bore circumstanced . 
 ( omitting works great masters , 
 excelled took hand ) think 
 refer origin continuance string 
 quartetts quintetts peculiarity . cases 
 enjoyment shared hearers ; , hearing 
 execrable combination runs , strums , trills , 
 Thalberg ‘ ‘ Home , Sweet Home , ” hearer struck 
 labour required learn , startled 
 interested “ sleight hand ” required . 
 cases difficulty produces pleasure hearer 
 player ; way , example fugue D 
 minor ( . 4 ) J. S. Bach , violin ; everybody . 
 knows better instruments , 
 person , d priori , fugue 
 instrument impossibility . masterly way 
 gives principal parts ingenious devices 
 stretto , pedal , & c. , constitute excellence 
 commend cultured musician end 
 time . Opera , , highest excellence , 
 shall farther treat 
 thesis temporarily omitted . admit ‘ ‘ Instru- 
 mental music incomplete , needs aid words 

 Let Wagner contradict 
 . Opera , “ Le Vaisseau Fantoéme , ” says 
 Overture ( instrumental course ) com- 
 plete development libretto . ( sec . 1 ) 
 description ship cast rocks , ( sec . 2 ) rocks 
 suffer strike , ( sec . 3 ) crew sign compact 
 evil presence captain , & c 

 melody , chord , rhythm idea 
 substantives printed italics ? leave reader 
 discover ; help degree . , , 
 wilfully rushing ‘ * Beethoven error , ” with- 
 Beethoven excuse ignorance , “ yearning ” 
 truth 

 Let farther Wagner embodies error 
 doctrines . says music ( instrumental , presumably 
 context ) produce 
 effect quiet walk sunset wood far 




 XUM461 

 Ofthe , suffice lack regular 
 phrased style ( soul true music ) Mozart , 
 Gluck , Meyerbeer , & c. , Italian school , 
 fervent energy Beethoven , bold harmonies 
 Handel 

 , Sir , , & c. , 
 Joun W. Hinton , A.B. , Mus . B 




 ‘ “ * MOVEABLE DOH ” 


 EDITOR MUSICAL TIMES 


 D. KIPPEN , 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWSgreatly success concerts.——ProFESsOR OAKELEY gave 

 Organ Recital Music Class Room , 12th ult . , 
 excellent programme performed alarge audience . Bach prelude 
 fugue E minor remarkably played astudent . Pro- 
 fessor Oakeley song , ‘ ‘ ’ Tis thou art fair , ” encored , 
 played little canzonet , “ Sempre pid t’amo . ”"——THE 
 seventh annual concert University Musical Society given 
 Wednesday evening , 18th ult . , Music Hall , large 
 audience . number choristers , amounting 200 , attained 
 standard singing far years , equal 
 improvement observable orchestra . Professor Oakeley , 
 coming forward assume conductor place , greeted 
 demonstrative applause audience choristers . 
 concert , miscellaneous , opened students ’ song , 
 ‘ ‘ Condiscipuli Canamus , ” composed Professor Oakeley 
 concert 1869 Latin words Professor Maclagan . Mr. Galletly , 
 student , played , Mr. Carl Hamilton , Beethoven sonata F , 
 Op . 17 , pianoforte violoncello , originally written piano 
 horn . Beethoven Symphony commenced second 
 programme ; promising young student - pianist , Mr. 
 Coates , played Schumann “ Schlummerlied , ” Schubert Im- 
 promptu flat great neatness , light touch , clear articula- 
 tion . concert , highly successful , ended 
 “ National Anthem 

 EvertTon.—The annual concert Choral Society given 
 Tuesday evening , roth ult . ,in Schoolroom , filled 
 large fashionable audience . _ Mr. 
 Hamilton White , organist Parish Church , East Retford , 
 Society great progress , choral pieces given 
 general excellence . Songs duets contributed Miss Annie 
 Williamson , Miss Naylor , Misses Postlethwaite , Miss Roe , 
 assisted Mr. Dimock Retford ; Mr. White delighted audi- 
 ience playing pianoforte solo 




 462The programme miscellaneous , comprising 
 Overtures , excellently performed military band , 
 direction Mr. J. J. Murdoch ; songs duets , Miss Joubert , 
 Rev. C. Darroch , Mr. Julius Carey ; pianoforte solos Mr. 
 Mrs. Brion . second devoted Mr. Brion Cantata , 
 Marathon , solo parts admirably sustained Miss 
 M. G. Sheppard Messrs. G. Sheppard C. Korner . 
 choruses exceedingly rendered choir ; entire 
 performance successful . Cantata , scored 
 military band ( strings island ) , 
 accompanied 

 LreaMINGTON.—Mr . Julian Adams gave successful Chamber 
 Concert Saturday , February 28th . Mendelssohn pianoforte 
 trio , D minor , Mr. Adams co - operation Herr 
 Otto Bernhardt ( violin ) Mr. Turner ( violoncello ) , rendered 
 great vigour marked emphasis . important work 
 class , Mayseder trio B flat , admirably played 
 - named executants . vocal portion concert 
 sustained Miss Katharine Poyntz Mr. Henry Pyatt , 
 encored Rockstro song , ‘ “ ‘ Reefer . ” great attraction , 
 , solo performance Mr. Julian Adams ; bravura 
 style playing mechanism fully displayed Fantasia 
 Russian Airs , vociferously encored , responded 
 playing musical sketch , composition , ‘ “ ‘ Les Patineurs . ” 
 Sonata G , pianoforte violin ( Beethoven ) , 
 performed finished execution 

 Ler.—An amateur concert , aid local benevolent case , 
 given 27th February Belmont Park Rooms , 
 conductorship Mr. Charles J. Frost . programme consisted 
 - songs , trios , songs . Mr. Frost new - song , ‘ ‘ Winds , ” 
 rendered Miss Maberley , Miss Austin , Mr. Law , Rev. John 
 Kempthorne ; Sir F. A. G. Ouseley charming little trio , “ 
 sight unwise , ” Misses Bumpus Miss Austin . 
 Mr. Frost playing ( memory ) Beethoven Sonata , Op . 10 , 
 . 1 , C minor , Schubert F minor Impromptu , gave un- 
 qualified satisfaction . concert successful 

 LeicesTER.—The Leicester Musical Society gave concert 
 season 1873 - 4 , Temperance Hall Tuesday evening , 
 roth ult . , large audience , Mendelssohn Oratorio Elijah 
 performed . principal vocalis&$ Madame Thaddeus Wells , 
 Miss D’Alton , Mr. Henry Guy , Mr. Santley . Madame Wells 
 efficient Widow . Miss D’Alton 
 successful , rendering ‘ ‘ Oh , rest Lord , ” eliciting 
 warmest plaudits . Mr. Santley magnificent voice , sang 
 Elijah - known power vigour , de- 
 clamation recitatives , exquisite interpretation airs 
 characterised ‘ varied excellences 
 famous . Mr. Guy executed share solos 
 highly satisfactory manner . trios , 
 double quartets , principal vocalists ably assisted 
 Misses . Deacon Clowes , Messrs. Adcock F. M. Ward . 
 choruses exceedingly executed Society . 
 performance band , especially strengthened 
 occasion , equally satisfactory , accompaniments 
 finished judicious . Mr. Nicholson officiated 
 conductor 




 XUM 


 463YaRMouTH.—A concert given Masonic Hall , Wednes- 
 day evening , 4th ult . , members Mr. Deane choral class . 
 consisted Haite Cantata , Abraham Sacrifice , 
 work produced vicinity , deserves 
 better known . music allotted Abraham effec- 
 tively rendered Mr. C. Panchen ( Bass ) . Miss Botwright , Miss 
 E. Cole , Messrs. Bly Smith , acquitted 
 creditably . omit mention instrumental piece , 
 “ ascent mountain , ” beautifully given 
 band . second miscellaneous . instrumental 
 quartet Robert le Diable warmly encored . Miss Hulley 
 played violin , Mr. omg second violin , Mr. Deane violoncello , 
 Mr. A. Howard bass ; Mrs. Panchen accompanied ; Mr. Deane 
 conducted 

 Yorx.—The series winter concerts given 
 Tuesday night , roth ult . , instrumental programme 
 erformed manner gave utmost pleasure present . 
 osart charming quartet D minor special feature 
 evening . Mr. Hallé played Beethoven E flat Sonata , Op . 31 , 
 usual facile style finished manner ; Madame Norman - Neruda 
 contributed solo violin . quintet Schumann , E flat , 
 ended programme . Miss Ferrari vocalist 

 OrcAN APPOINTMENTS.—Mr . Geo . W. R. Hoare , St. James ’ 
 Church , Clapham Park.——Mr . George Kitchin , Holy Trinity , 
 Sydenham.——Mr . E. F. Seppings , St. James ’ , Littie Heath , Chad- 
 , Essex.——Mr . R. Peel , assistant organist St. Mary , Wigan , 
 Lancashire.——Mr . Frederick G. Cole , St. Mary , Staines . — — Mr. 
 E. J. Mummery ( assistant organist Christ Church ) , Castle Church , 
 Stafford.——Mr . R. Nottingham , organist choir master Winder- 
 mere Parish Church , choir master Choral Union , Winder- 
 mere.——Mr . E. J. Griffiths , organist choir master Parish 
 Church , Broadstairs , Kent . Mr. Frederick G. Edwards , Surrey 
 Chapel ( Rev. Newman Hall ) , Blackfriars Road , S.E-——-Mr . Thomas 
 Edward Trotter , St. Saviour , Brockley Road , Forest Hill.——Mr . 
 H. Walmsley Little , organist choirmaster Christ , Church , 
 Woburn Square , Bloomsbury 




 RGAN , PIANOFORTE , HARMONIUM , HAR- 


 IANOFORTE BUSINESS.—PARTNERSHIP . — 


 VOICE . MUSIC LANGUAGE . 


 PERFORMED CRYSTAL PALACE 


 SONG DESTINY 


 SCHICKSALSLIED 


 COMPOSED > 


 JOHANNES BRAHMS , 


 REV . J. TROUTBECK , M.A. 


 MONTH 


 ENEDICITE , OMNIA OPERA DOMINI , 


 


 GEORGE 


 ENI 


 ANGLICAN HYMN - BOOK 


 NEW EDITION , REVISED ENLARGED 


 SECOND SERIES 


 ANGLICAN CHORAL SERVICE BOOK 


 PSALTER , PROPER PSALMS , HYMNS , 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI 


 ORDER HOLY COMMUNION . 


 ADDITIONS 


 EV . T. HELMORE PLAIN SONG WORKS 


 SACRED MUSIC EDWARD LAWRANCE , 


 A. MACFARREN - ANTHEMS 


 466 


 WESTMINSTER ABBEY CHANT BOOK , 


 T;HE CHURCH PSALTER HYMN - BOOK . 


 DAY . 


 SON GOD GOES FORTH WAR . 


 ANTHEMS SPECIAL GENERAL 


 VILLAGE ORGANIST 


 NEW WORK HARMONIUM . 


 SCHOOL 


 


 HARMONIUM 


 KING HALL 


 PRICE SEVEN SHILLINGS SIXPENCE 


 CATHEDRAL PSALTER 


 POINTED CHANTING 


 


 PUBLISHED SANCTION 


 REV . DEAN ST . PAUL 


 


 REY . DEAN WESTMINS 


 EDITIONS PREPARATION 


 467 


 CHOIR 


 ERAL 


 COST 


 NIST ; 


 IUM 


 7 M 


 ENCE . 


 TER 


 UTBECH 


 \ RNBY 


 AUL 


 MINS 


 TION 


 MUSIC 


 COMING SEASONS 


 PUBLISHED NOVELLO , EWER CO 


 ASCENSION DAY 


 ARNBY , JOSEPH.—KING GLORIOUS 


 


 OGERS , B.—LORD , SHALL DWELL ? 


 WHIT SUNDAY 


 ARTHOLOMEW , R.—COME , HOLY SPIRIT , COME . 


 VO . , 2 


 ONGHURST , W. H.—GREAT LORD . 


 ACFARREN , G. A.—-O HOLY GHOST , MINDS 


 EAY , S — LOVE GOD . 


 TRINITY SUNDAY 


 ANDEL.—WHEN EARTH FORM . 


 AKELEY , HERBERT , S. , M.A — WHATSOEVER BORN 


 LONDON : NOVELLO , EWER 


 CO . NEW YORK : J. L. PETERS 


 NEW SERVICE HOLY COMMUNION , 


 G. A. OSBORNE . 


 CHAPPELL & CO . , 50 , NEW BOND STREET 


 NEW WORK AMATEUR ORGANISTS 


 PRICE SHILLINGS . 


 CHAPPELL & CO 


 ALEXANDRE NEW SEVEN - GUINEA ORGAN HARMONIUM . 


 SOLID OAK CASE , OCTAVES , FOOT - BOARDS 


 CHAPPELL & CO . , 50 , NEW BOND STREET 


 NEW WORK SINGING CLASSES 


 CHAPPELL PENNY OPERATIC - SONGS . 


 CHAPPELL & CO . , 504 , NEW BOND STREET , LONDON