


THE MUSICAL TIMES


FOR THE MUSIC CONTAINED IN THE FIRST 48 NOS. 


162 THE MUSICAL TIMESi. 78. 6d.—ii. 78 6d.—iii. 6s. 6d.—iv. 7s. 6d.—v. 8s.—vi. 78. 6d. 
—vii. §8.—vili. 38.—ix. 78.—x. 58.—xi. 38.—xii. 78.—xiii. 5s. 
—xiv. 4s. 6d.—xv. 7s.—xvi. 8s. 
_ HAYDN’S MASS, each Number singly, Quarto size. 
i. 48. 6.—ii. 48.—ili. 38. 6d.—iv. 4s.—v. 58.—vi. 4s. 6d.— 
Vil. 38.—vill, 28.—ix. 48.—x. 38.—xi. 1s. 6d.—xii. 38. 6d.— 
xlil. 38.—xiv. 38.—xv. 48.—xvi. 55

BEETHOVEN in C. Folio, 8s. 6d.; the same Quarto, ss

BEETHOVEN in D, Folio, 14s

HUMMEL in E flat, 8s. HUMMEL in B flat, 7s




PIANOFORTES, for the Pianoforte. Berger, Fd., 
Two easy Studies on the popular Airs ‘¢ The last Rose 
of Summer,” and * Robin Adair,” Op. 98, No.2. Price 3s. 
Cramer, H., Divertisement de Salon sur une, Chanson favorite 
de Kiicken “ Ach wenn du warst mein eigen,” Op. 49. 
Price 2s. 6d. 
Kuuner, W., Rosa Polka, Op. 113. Price 1s

Cramer, Henri, Collection of Potpourris from the following 
Operas; each 2s, 6d :-— 
No. 2.—Lortzine, Czaar und Zimmermann. 
99 3--—Kreurzer, Das Nachtlager in Granada. 
yy 10.—Lacuner, Catharina Cornaro, 
99 19.—Mozart, Don Juan. 
99 25.—BEETHOVEN, Fidelio. 
99 30.—Mozart, Die Zauberfléte. 
99 35-—SPour, Faust. 
»» 39-—FLorow, Stradella. 
9 44.—Weser, Der Freischutz

ainzer’s Musical Grammar. 8vo




THE MUSICAL TIMES. 163


THE MUSICAL TIMES


HISTORIC SKETCH OF CHURCH MUSIC


164 THE MUSICAL TIMES


TO CORRESPONDENTS


TREBLE


= SE


=== = 22:2. —= 


SS


CRY ALOUD AND SHOUT


SS } ! = 


CRY ALOUD AND SHOUT


= £ = E — - E = 


TR 


CRY ALOUD AND SHOUT


CRY ALOUD AND SHOUT


OER


170 THE MUSICAL TIMES


UNDER ROYAL PATRONAGE


PERFECT FREEDOM FROM COUGHS IN TEN MINUTES


J. E. BIGNELL


EDWARD PAGE


7A


BOYCE’S COLLECTION OF CATHEDRAL MUSIC


VINCENT NOVELLO. 


THE MUSICAL TIMES


ADVERTISEMENTS


172 THE MUSICAL TIMES


AND 


CCOMPANYING HARMONIES TO THE 


PSALTER NOTED


THREE HALF-PENCE PER PAGE