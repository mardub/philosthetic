


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


PUBLISHED ON THE FIRST OF EVERY MONTH


CHARLES HALLE’S “PRACTICAL PIANOFORTE SCHOOL


CHARLES HALLE’S 


EDITION OF NEW AND POPULAR WORKS


CAREFULLY FINGERED AND REVISED. 


RAFF, JOACHIM


HAYDN, JOSEPH. RUBINSTEIN, ANTON. 


LISZT, FRANZ. WAGNER, RICHARD. 


NKHWNHNW HWWPW 


SNHHWHO WL 


WENHNDNOAWAHHUN 


FORSYTH BROTHERS, 2724, REGENT CIRCUS, OXFORD STREET, LONDON, 


AND CROSS STREET, SOUTH KING STREET, MANCHESTER


PROFESSIONAL NOTICES


ORCESTER CATHEDRAL.—PRECENTOR


ING’S COLLEGE, CAMBRIDGE— 


ALBERT E. BISHOP. 


£110 


I. T


40


TRINITY COLLEGE, LONDON


WEYMOUTH STREET, PORTLAND PLACE


DEPARTMENT OF EVENING CLASSES


TRINITY COLLEGE, LONDON


HIGHER EXAMINATIONS FOR WOMEN


TRINITY COLLEGE, LONDON


RESIDENT STUDENTS


COLLEGE, LONDON. 


HONORARY MEMBERS


TT RINTTY COLLEGE, LONDON


TRINITY


£5 PRIZE. SENIOR DIVISION. 


3 PRIZE. JUNIOR DIVISION. 


O AMATEUR COMPOSERS.—MSS. REVISED 


O COMPOSERS WISHING TO PUBLISH


USIC ENGRAVED, PRINTED, AND PUB- 


51


MADE UP FROM THE MOST EMINENT ARTISTS, AND 


ESTABLISHED, APRIL 1866. 


HE ENGLISH GLEE UNION. 


ASSISTED BY 


HE YORKSHIRE CONCERT PARTY.— 


ADAME TREBELLI’S CONCERT TOUR.— 


AST LONDON ORGAN WORKS


GTUTIGART HARMONIUM COMPANY. — 


EAN’S CHEAP MUSICAL INSTRUMENTS.— 


RUSSELL’S MUSICAL INSTRUMENTS. ; 


GREAT SALE OF MUSICAL PROPERTY


D’ALMAINE’S PIANOS


485 ) 


AT HALF-PRICE


IN CONSEQUENCE OF THE DEATH OF THE LATE 


PROPRIETOR THE


WHOLE OF THIS SPLENDID STOCK


IS OFFERED AT


15 AND 17 GUINEAS


20 AND 25 GUINEAS 


DOUBLE CHECK ACTIONS ALL HALF-PRICE


SEVEN YEARS WARRANTY


ON VIEW AT


5, FINSBURY PAVEMENT


MOORGATE STATION. 


LONDON AGENTS


MESSRS. MOUTRIE AND SON, 


PIANOFORTE SALOON


55, BAKER STREET, LONDON, W


TESTIMONIAL. 


HIGHEST CLASS OF EXCELLENCE


MILLEREAU’S BAND INSTRUMENTS 


FOR ARTISTS


MILLEREAU’S FRENCH VOCAL HORN IN C. 


ORGAN-TONED HARMONIUMS. 


W. HATTERSLEY & CO.’S


CELEBRATED


ORGAN-TONED HARMONIUMS


UNEQUALLED BY ANY OTHER MAKERS


QUALITY OF TONE, DURABILITY, RAPIDITY OF TOUCH, 


PREDOMINANCE OF THE TREBLE OVER THE BASS, 


PERFECT ACTION, 


BRILLIANCY OF THE SOLO STOPS, NEWLY INVENTED 


DOUBLE ACTION, SOLID IMPROVED SOUND-BOARD, 


AND DOUBLE EFFECTIVE SWELLS


W. HATTERSLEY & CO., 


W. P. M. SLATER. 


E. & W. SNELL’S


IMPROVED ENGLISH HARMONIUMS


* TOWN, LONDON, N.W


CENTENNIAL EXHIBITION


NOTICE. 


NEW YORK, 


FOR THE GENERAL EXCELLENCE OF THEIR


STANDARD” 


AMERICAN ORGANS


PRICES, 12 TO 125 GUINEAS


BARNETT SAMUEL & SON, 


31, HOUNDSDITCH, E.C., & 32, WORSHIP STREET, 


FINSBURY SQUARE, 


MUSIC STRINGS—WHOLESALE AND RETAIL. 


J. P. GUIVIER AND CO


MANUFACTURERS AND IMPORTERS OF 


ALL KINDS OF MUSIC STRINGS FOR 


ALL MUSICAL INSTRUMENTS


25, SHERWOOD STREET, REGENT CIRCUS, PICCADILLY. 


LOUCESTER MUSICAL FESTIVAL, 


IN THE CATHEDRAL, 


EEDS MUSICAL FESTIVAL, 1877, TOWN 


HALL, LEEDS. 


PRINCIPAL VOCALISTS :— 


MDLLE. ALBANI, 


MDME. EDITH WYNNE, MRS. OSGOOD, 


MDME. PATEY, 


MDLLE. REDEKER, MRS. UDIE-BOLINGBROKE, 


MR. EDWARD LLOYD, MR. WM. SHAKESPEARE, 


MR. SANTLEY, BAND AND CHORUS OF 400 PERFORMERS. 
Organist: Ui. SPARK. Chorus Master: MR. BROUGHTON. 
Ourtiine ProuraMMes.—WeEpDNESDAY: Elijah. Evening: The

Five-King (new Cantata), by Walter Austin, and Miscellaneous Selec- 
tion. THurspay: Mendelssohn’s Walpurgis Nacht, Beethoven's 
Symphony (No. 8), and Miscellaneous. Evening: Solomon, Fripay: 
h, by G. A. Macfarren (written for this Festival). Evening: 
aff’s Symphony in G minor, and Miscellaneous. SAaturRDAy: Bach’s 
Magnificat in D, Mozart’s Requiem, and Beethoven’s Mount of Olives. 
FRONT SEATS AND GALLERY TICKETS (Reserved) at the 
FEsTIVAL Offices. 
Serial Ticket for the seven performances (transferable) ... £5 os. 
Single Ticket, MOrning ose ceo oes see vee cone ore vee £E TS

itto CE RIE ae FESS pean eile ey ae A 
SECOND RESERVED SEAT TICKETS only at Hopkinson 
Brotuers and Co., Commercial Street. 
RINNE ocr ooe” ncn econ see) aes See Sas > SO 
nnn HS Bac’ Lobel ive: Seek tere ols) tng »- aan es 
FULL FESTIVAL PROGRAMMES may be had gratis at the 
FesTIvAL OFFices, and at the nog bg iol 
OHN WM. ATKINSON, 
PRED. R- SPARK, } Hon, Secs. 
Festival Offices, Great George Street (Town Hall), Leeds




ROYAL ALBERT HALL CHORAL 


SOCIETY


THE GROSVENOR CHORAL SOCIETY. 


HE MANCHESTER GENTLEMEN’S GLEE


MUSIC IN THE HOUSE


THE MUSICAL TIMES


SCALE OF TERMS FOR ADVERTISEMENTS


THE MUSICAL TIMES 


UNITED STATES, CANADA, AND 


AUSTRALIA


NOVELLO & CO., 1, BERNERS STREET. W


MESSRS


NOVELLO, EWER AND CO


MESSRS. DITSON & CO


ALL ORDERS SHOULD STATE


NOVELLO’S EDITIONS


THE STUDENT’S TEXT-BOOK


OF THE


SCIENCE OF MUSIC


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


SEPTEMBER 1, 1877


MUSICIANS AND THEIR MASTERS. 


THE GREAT COMPOSERS, SKETCHED BY 


THEMSELVES. DUSSEK’S PIANOFORTE SONATAS. 
By EBENEZER Prout, B.A

Ir is a requent subject of complaint with those 
pianoforte teachers who are conscientious in the 
selection of music for their pupils that they find it 
difficult to get good classical pieces of only moderate 
technical difficulty. Until a player is fairly advanced 
it is worse than useless to give Beethoven’s Sonatas, 
and one cannot be always teaching Mozart. It is 
a curious thing that these teachers frequently 
ignore altogether—indeed are often themselves un- 
acquainted with—the pianoforte works of Haydn, 
Clementi, and Dussek. With the object of directing 
, their attention to a perfect mine of musical beauty, 
I propose to make some remarks on the Sonatas 
of the last-named of these three composers

Comparatively few, even among educated musicians, 
know how much Dussek did toward developing the 
technical resources of the piano. The contemporary 
of Mozart, and a great pianist, he composed more 
than fifty Sonatas, twelve Concertos for the piano with 
orchestra, a Quintett, Quartett, and Trio for piano and 
strings, and a literally countless number of small 
pieces, Variations, Rondos, &c. Of thisenormous mass

424 THE MUSICAL TIMES.—SeEptemsper 1, 1877

almost recalls Beethoven’s earlier style in the bold- 
ness of its harmonies ; while the long Adagio patetico

has the grace and suavity of Mozart. Here again the 
very florid ornaments seem to have furnished more 
than one hint to Hummel. The Finale is preceded 
by a short Intermezzo, which is in fact nothing more 
than a Prelude. Though full of spirit and very tune- 
ful, it cannot be said that this movement is at all equal 
to the rest of the Sonata. Its chief theme is very 
trivial, and not without a tinge of vulgarity; and as 
it forms the principal material for the subsequent 
developments, the music nowhere rises to a high level




ENGLISH MUSICAL PRIVILEGES. 


426


GEORGE TOWNSHEND SMITH


SALE OF DR. RIMBAULT’S LIBRARYfided, and who acted as one of the judges. Both the 
instrumental and choral performances were of great ex- 
cellence. The competition was very close, and resulted im 
the award of the following band prizes: 1. Strand Union 
School, Edmonton; 2. St. Pancras School, Leavesden ; 
3. West London District School, Ashford; 4. St. Mary’s 
Orphanage, North Hyde, Hounslow; 5. Exmouth Train- 
ing Ship; 6. Milton Schools, Portsmouth. The singing 
prizes were given to the best of the six competing choirs 
as follows: 1. St. Mary’s Orphanage; 2. Milton School, 
Portsmouth ; 3. South Metropolitan Schools, Sutton. In 
presenting the prizes to the band and choir masters Sir F. 
Fitzwygram expressed his gratification with the success of 
the competition, and his intention to give similar prizes in 
the next competition

THE Royal Society of Musicians has just received some 
interesting gifts to add to the choice souvenirs of the great 
departed already in its possession. The first is a proof 
engraving of the portrait of the late Sir W. Sterndale 
Bennett, signed by the artist and engraver, Millais and 
Barlow. This fine work of art was presented to the 
Society, framed and with a suitable inscription, by the 
late eminent musician’s daughter, Mrs. Case. The other 
gifts are from Mr. Charles Neate (son of the pianist of 
that name whose death was noticed in the MusIcaL 
Times of May last), and are valuable memorials both of 
Neate and his master Beethoven. They consist of an 
engraved portrait of Beethoven (date 1814), with a few 
words recording the gift and signed by the mighty master, 
who accidentally let fall a large blot of ink on the paper 
and was desirous of cancelling the presentation and sub- 
stituting another; but to this Neate demurred, saying “a 
blot from Beethoven was superior to a page of any other 
material from an inferior author.” To the portrait Mr. 
Neate has added a letter from Beethoven to his father, in 
French, in which Beethoven says, should Neate decide 
on having a benefit concert, his services will be avail- 
able in any way he may desire. The Royal Society of 
Musicians possesses many excellent portraits and other 
musical treasures, which are most carefully preserved in 
the large meeting-room, Lisle Street, Leicester Square; and 
it is pleasant to find that the Charity is not forgotten by 
the relatives of those who during a long life were not only 
members but also warm friends of this very deserving 
Institution, which distributes the whole of its funds in 
relieving the wants of aged and distressed musicians

THe Dean and Chapter of St. Paul’s having kindly 
agreed to sanction a special Service for the. working 
classes in the Cathedral, after consultation with Mr. Robert 
Alderson Turner, the Hon. Secretary of the Gregorian 
Association, it was determined that the Festival Service as 
given in May last should be repeated on the oth ult., and 
the working classes especially invited to attend. The 
choir, numbering about 1,000 voices, including nearly roo




430


REVIEWS


PART-SONG. 


4 — ! ! 4 


PIANO. | 


LZ I I J 


1 -2


TT § 


TR


SSS SS SS


__{}. +9494 — 4


BEAR


= SS | 


—_—_ 


437


FOREIGN NOTESVocal performances of especial interest will be given by 
the Association known as “ Renner’s Madrigal Quartett ” 
in connection with the hundredth anniversary of the foun- 
dation of the Germanic Museum at Nirnberg. The 
programme will comprise a number of Madrigals from the 
time of the ‘ Meistersinger,’ as well as compositions of a 
similar character by Senfl (1520), Orlando di Lasso (1520- 
94), Thomas Tallis (1585), Thomas Morley (1600), and 
many others rarely, if ever, heard even in these days of 
antiquarian research

According to the Neue Zeitschrift fiir Musik a number 
of autograph letters, &c., by eminent composers, formerly in 
the possession of a wealthy amateur, has just passed into 
the hands of Dr. Joseph Miiller (late editor of the Alige- 
meine Musik-Zeitung). ‘The importance of this collection 
is greatly enhanced by the fact that its contents have 
hitherto been entirely unknown, being jealously guarded by 
its previous possessor, who has, moreover, by testamentary 
direction, interdicted the publication for a number of years. 
The letters are thirty-seven in number, viz. J. S. Bach (3), 
C. Ph. E. Bach (2), Beethoven (4), Couperin (2), Gluck (4), 
Grétry (2), Handel (7), Haydn (3), Di Lasso (1), Lully (3), 
Morley (1), Mozart (4), and Heinrich Schitz (1), nearly all 
of them being documents of considerable importance to 
the student of art-history. Besides these letters, the 
collection contains unpublished compositions by Bach, 
Couperin, Handel (a complete opera), Haydn, Lully, and 
Schitz

In January next the town of Hamburg will celebrate the 
two hundredth anniversary of the first representation on any 
stage of an original German Opera. The work then per- 
formed was called ‘‘ Adam und Eva, oder der erschaffene, 
gefallene, und aufgerichtete Mensch” (‘*‘ Adam and Eve, or 
man created, fallen, and anon raised up”) ; the text is 
written by the laureate poet Richter, the music by Johann 
Theile, the quondam Capellmeister at the great Hansa 
Town




CORRESPONDENCE


MUSIC IN THE UNITED STATES OF COLOMBIA. 


TO THE EDITOR OF ‘‘THE MUSICAL TIMES


WHEREAS


THE “STICKER ACTION” IN PIANOFORTES. 


TO THE EDITOR OF “THE MUSICAL TIMES


A MUSICSELLER OF THIRTY YEARS’ STANDING


THE BARKER FUND. 


TO THE EDITOR OF ‘‘ THE MUSICAL TIMES.” 


HENRY SMART


THE HARMONIOUS BLACKSMITH.” 


TO THE EDITOR OF “ THE MUSICAL TIMES


TO CORRESPONDENTS


BRIEF SUMMARY OF COUNTRY NEWSHEPTONSTALL, NEAR Hattrax.—On Tuesday, the 31st July, thesacred 
Cantata, Jonah, was performed in the Parish Church by the Halifax 
Parish Church Choir, assisted by members of the choir at Heptonstall. 
The Rev. F. Pigou, M.A., Vicar of Halifax, gave an appropriate 
address on Church Music. The principal parts of the Cantata were 
taken by Mrs. Whitehead (soprano), Miss Empsall (contralto), Mr. 
Verney Binns (tenor), and Mr. Morton (bass). Dr. Roberts, the 
composer of the work, presided at the organ, The Cantata was most 
efficiently rendered, and produced a very favourable impression

LEVERINGTON, WisBBCH.—On the rst ult. anew Organ of fifteen stops, 
built by Conacher and Co., Huddersfield, was opened in the Parish 
Church. Mr. Arthur C. Thacker (Organist of Thorney Abbey) presided 
at the instrument. There was a full Choral Service at 11.30, which 
was ably rendered by the choir of S. Augustine’s, and their Rector, the 
Rev. E. J. Littlewood. The sermon was preached by the Lord Bishop 
of Winchester. At four o’clock Mr. Thacker displayed his accustomed 
efficiency in an Organ Recital from the works of Handel, Beethoven, 
Mendelssohn, Batiste, &c., when the church was crowded in every part. 
The organ is admirably constructed, and the tone very fine and rich

Marcu.—On the rst ult. a successful Festival of Parochial Choirs 
was celebrated in the Church of St. John, under the able direction of 
the Organist, Mr. Charles Greenwood. The following Choirs took part 
in the service, viz. St. John’s, March; St. Mary’s, March; Holy 
Trinity, Coates; Coldham; Welney; total number of voices, 104. 
The interior of the church was tastefully decorated, and the congre- 
gation very numerous. The service commenced with a processional 
hymn, sung by the surpliced choirs, ‘‘ Hark, the sound of holy voices.” 
The Anthem was taken from Psalm cl., “ Praise God in His 
holiness.” The Hymns were 157, 323, 335, concluding with a Reces- 
sional (197, Ancient and Modern), the latter to the grand old tune, “ St. 
Ann’s.” The Ely Confession (as usual at St. John’s) was sung, and 
Tallis’s Responses were used. The Canticles were sung to a service 
by Dr. Wesley, and the three special Psalms to Anglican chants. A 
very eloquent and appropriate extempore sermon was preached by the 
Rev. Canon Bulstrode, of Ely Cathedral, founded upon the text “ Seek 
ye first the kingdom of God, and His righteousness.” The offertory 
was liberally responded to. The proceeds will be devoted to the 
“Choral Fund




OBITUARY


DURING THE LAST MONTH. 


CONTENTS


PAGE 


RGAN METAL PIPES, ORGAN PIPES


T? THE TRADE AND PROFESSION.—TWO


HE NEW NATIONAL PART-SONGS.— 


HARVEST ANTHEMS


TAINER,' — SHALL DWELL IN THE LAND. 


ALLCOTT, W. H.—THOU VISITEST THE EARTH AND 


ACFARREN, G. A.—GOD SAID, BEHOLD, I HAVE GIVEN 


ATTISON, T. M—O HOW PLENTIFUL IS THY GOOD- 


AYLOR, W.—THE EYES OF ALL WAIT UPON THEE. 


RIDGE, J. FREDERICK.—GIVE UNTO THE LORD THE 


TWELVE HYMNS WITH TUNES


FOR


HARVEST


SELECTED FROM


THE HYMNARY. 


PRICE ONE PENNY


O LORD, THE HEAVEN THY POWER 


GERMAN


ALBERT LOWE'S


NEW HARVEST ANTHEM


THE EARTH IS THE LORD’S,” 


ALBERT LOWE'S


HARVEST CAROL 


“HOLY IS THE SEED-TIME


EW HARVEST ANTHEM.—*O THE 


“O SING UNTO THE LORD WITH THANKSGIVING.” 


NEW ANTHEM FOR HARVEST. 


WHILE THE EARTH REMAINETH


HARVEST. 


THOU VISITEST THE EARTH


GOD IS THE LORD


WHEN THE LORD


COMPOSED BY 


CHARLES JOSEPH FROST


ARVEST ANTHEM.—‘ 1 HOU CROWNEST 


WORSHIP THE LORD IN THE BEAUTY 


HARVEST


TEN KYRIES. 


TEN GLORIAS (BEFORE GOSPEL). 


TEN GRATIAS (AFTER GOSPEL


REDUCED PRICE. 


ANGLICAN


PSADLTE R: CHA N-F


SINGLE AND DOUBLE


EDITED BY THE


REV. SIR F. A. GORE OUSELEY, BART., ETC


AND


EDWIN GEORGE MONK


NOVELLO’S OCTAVO EDITION OF THE 


IN PAPER BOARDS


PAPER BOARDS, WITH CLOTH BACKS AND GILT LETTERING, 


HANDEL’S MESSIAH - - - - - - 2 6 


” JUDAS MACCABZUS - - - * 2 6 


” ST. PAUL - - - - 2 6 


HAYDN’S CREATION - - - - - - 3, 6 


LONDON : NOVELLO, EWER AND CO


J OS * NEW SONGS FOR CONTRALTO VOICE. 


AN ORATORIO. 


4-7


447


EDITED BY DR. STAINER


NOW READY, PRICE TWO SHILLINGS


THE PIANOFORTE


ERNST PAUER


PRINCIPAL PROFESSOR OF THE PIANOFORTE AT


THE NATIONAL TRAINING SCHOOL FOR MUSIC


CONTENTS


SN DAB YW NS


CATHEDRAL PSALTER 


CHANTS


EDITED BY 


S. FLOOD JONES, M.A., J. TROUTBECK, M.A., 


PRECENTOR OF WESTMINSTER; MINOR CANON OF WESTMINSTER; 


ORGANIST OF WESTMINSTER; ORGANIST OF ST. PAUL'S; 


AND


JOSEPH BARNBY


PRECENTOR OF ETON


LONDON: NOVELLO, EWER AND CO


CATHEDRAL PSALTER


POINTED FOR CHANTING


PUBLISHED WITH THE SANCTION OF 


THE VERY REV. THE DEAN OF ST. PAUL’S


AND


THE VERY REV. THE DEAN OF WESTMINSTER


OON WH OF 


LONDON: NOVELLO, EWER AND CO., 


1, BERNERS STREET (W.), AND 80 & 81, QUEEN STREET (E.C


449


THE HYMNARY


A BOOK OF CHURCH SONG


THE FOLLOWING EDITIONS ARE NOW READY :— P 


THE NEW HYMNAL. 


TONIC SOL-FA EDITION


TONIC SOL-FA EDITION


7 BURNLEY TUNE-BOOK.—NOTICE


HE MISSIONARY MANUAL TUNE-BOOK. 


THE MISSIONARY MAN UAL OF ‘HYMNS AND PRAYERS. 


HE REV. R. BROWN-BORTHWICK'S 


ENTWORTH PHILLIPSON’S * GUIDE TO 


YOUNG PIANOFORTE TEACHERS AND STUDENTS.” 


AKER’S PIANOFORTE TUTOR


THE NEW METHOD. 


USIC FOR THE NEW CODE.—‘“ THE 


450


SYDNEY SMITH’S 


FOUR NEW PIECES


SELECT 


PIANOFORTE PIECES


STEPHEN HELLER


AUBADE


UNE PETITE FEUILLE... .. .. .. 3 0


LONDON: ASHDOWN & PARRY


HANOVER SQUARE


FOUR-PART SONGS


BY


R. L. DE PEARSALL


CONTENTS OF VOL. X


CONTENTS OF VOL. XI


BY


R. L. DE PEARSALL


COLLEGIATE SERIES


DR. S. S. WESLEY


BERTHOLD TOURS


DULCIANA


SCHUMANN. 


J. P. KNIGHT


J. L. ROECKEL


G. B. ARNOLD, MUS. DOC. 


C. E. HEY


ERNEST LINDE. 


C. JEFFERYS


67, BERNERS STREET


NEW PUBLICATIONS


MELODIOUS MELODIES 


ARRANGED FOR THE HARMONIUMThe subjects selected from the Works of

Auber. Haydn. Rossini. 
Batiste. Hiller. Schubert. 
Beethoven. Méhul. Schumann. 
Boccherini. Mendelssohn. Spohr. 
Chopin. Meyerbeer. Verdi. 
Donizetti. Mozart. Wagner. 
Handel. Rink. &e

NEW NUMBERS OF




HOUSEHOLD MELODIES 


PIANOFORTE 


VE HAVE LIVED AND LOV ED TOGETHER


14. 


15. WINGS


16. O ERIN, MY COUNTRY


17. OH, FOR THE BLOOM


18. COME, LET US BE HAPPY TOGETHER


LONDON: 


ECOND SERIES


HE ANGLICAN CHORAL SERVICE BOOK. 


USELEY AND MONK’S PSALTER AND


OULE’S COLLECTION OF WORDS OF 


OULE’S COLLECTION OF 527 CHANTS, 57 


HE PSALTER, PROPER PSALMS, HYMNS


OULE’S DIRECTORIUM CHORI ANGLI- 


OULE’S DIRECTORIUM CHORI ANGLI


HE ORDER FOR THE HOLY COMMUNION


“ NOT UNTO US, O LORD.” 


ANTHEM


OFFERTORY SENTENCES 


(COMPLETE) 


SET TO MUSIC BY 


CHARLES JOSEPH FROST


QC)FFERTORY SENTENCES COMPLETE (20


THE OFFERTORY SENTENCES 


WITH AN APPENDIX FOR FESTAL USE, 


SET TO MUSIC BY 


JOSEPH BARNBY. 


Y SOUL TRULY WAITETH STILL UPON 


DR. SPARK’S NEW ANTHEM


AND NOW, ISRAEL


HOSO DWELLETH UNDER THE 


ARVEST ANTHEM.—“ PRAISED BE THE


LORD DAILY.” 


ING WE MERRILY UNTO GOD OUR


AROLS FOR USE IN CHURCH DURING 


HE CAPTIVITY.—* THEY THAT WAIT 


A MANUAL OF SINGING


FOR THE USE OF CHOIR TRAINERS & SCHOOLMASTERS. 


FORTY-NINTH EDITION. 


COLLEGIATE AND SCHOOL 


SIGHT-SINGING MANUAL


COLLEGIATE VOCAL TUTOR


COLLEGIATE ORGAN TUTOR. 


EDITED BY FREDERIC ARCHER. 


COLLEGIATE PIANOFORTE 


TUTOR


CONSECUTIVE FIFTHS AND 


OCTAVES IN COUNTERPOINT


AN ESSAY, BY R. L. DE PEARSALL


CATHEDRAL CHANT-BOOK


NEW BALLADS


BERTHOLD TOURS


R. L. DE PEARSALL


WHO KILL’D COCK ROBIN? ... —«.. 38


NEW EDITION OF 


DR. BENNETT GILBERT’S


SCHOOL HARMONY


TRUTH AND EASE IN MUSIC. NO MORE SHARP OR FLAT 


KEYS FOR INSTRUMENTALIST OR VOCALIST


THE SEQUENTIAL SYSTEM OF MUSICAL 


NOTATION


454 


THEORY OF MUSIC. WILLIAM J. YOUNG’ 


O OUNG’S 


LOUISA GIBSON. POPULAR PART-SONGS 


A SECOND BOOK 


POEME DE PAUL POIRSON ET LOUIS GALLET. 


STREET, MIDDLESEX HOSPITAL, LONDON, W


POPULAR FOUR-PART SONGS BY GABRIEL


THE POET’S DAUGHTER


PART-SONG 


WORDS BY ROBERT BURNS 


SET TO MUSIC FOR S.A.T.B. 


LFRED R. GAUL’S CHORAL SONGS, 


OPINIONS OF THE PRESS


CHORAL SONGS FOR SCHOOL & HOME


FORTY-TWO ORIGINAL SONGS 


FOR 


ONE, TWO, OR FOUR VOICES. 


SELECT COMPOSITIONS 


FROM THE 


GREAT MASTERS, 


ARRANGED FOR THE ORGAN 


A. H. BROWN


AN ANDANTE INA FOR THE ORGAN 


SAMUEL SEBASTIAN WESLEY


N AIR FOR HOLSWORTHY CHURCH 


SMALLWOOD’S 


CLASSICAL & SECULAR EXTRACTS


FOR THE 


PIANOFORTE. 


NOVELLO, EWER AND CO.’S ONLY COMPLETE AND 


UNIFORM EDITION OF


MENDELSSOHN’S 


THIRTEEN TWO-PART SONGS


NOVELLO’S 


TONIC SOL-FA SERIES


TRANSLATED AND EDITED BY


HEAR MY PRAYER...  .. —... MENDELSSOHN 0 3 


FULL ORCHESTRAL SCORES


ONE GUINEA EACH


HANDEL’S MESSIAH


HANDEL’S ISRAEL IN EGYPT 


HAYDN’S CREATION


LONDON: NOVELLO, EWER AND CO


CHALLEN AND SONS 


GOLD MEDAL PIANOS


20, OXFORD STREET, LONDON


ESTABLISHED 1804. 


| PRIZE MEDAL, LONDON, 1862. | | GOLD MEDAL, SOUTH AFRICA, 1877


CHALLEN & SON, 20, OXFORD STREET. 


IN THE PRESS


CHRISTIAN THE PILGRIM


THE PILGRIM’S PROGRESS 


SACRED CANTATA


SET TO MUSIC BY


WILFORD MORGAN


18, SURREY STREET, LONDON, W.C