


 the MUSICAL TIMES 


 MUSIC in this number . EXETER HALL . 


 GRANT , WE BESEECH THEE 


 the 


 PROFESSIONAL notice 


 L lishe in the good style , and on moderate 


 usical instrument for VOLUN- 


 UTLER ’s MUSICAL instrument . — 


 CORNETS , SAXHORNS , DRUMS , FLUTES , CLARIO- 


 EYOND ALLCOMPETITION.—T. R. WILLIS , 


 MISS FANNY CHATFIELD 


 MADAME MONTSERRAT 


 RASS , REED , STRING , and DRUM and 


 M. HOLDICH ’s ORGAN MANUFACTORY 


 AND W. SNELL ’S improve HARMO- 


 419 


 : PIANOFORTE TUNERS . — want , 


 REV . ROBERT CORBET SINGLETON , M.A , 


 organist and choirmaster of YORK MINSTER 


 GOLD medal drawing - room organ 


 SEVENTY - five gold and silver medal 


 award to 


 the MASCN and HAMLIN CABINET organ 


 W. STRICKLAND MALONE 


 BREAVINGTON and SONS , 309 , RECENT street . 


 MR . LAHEE ’S NEW CANTATA 


 HENRY LAHEE 


 PART i 


 introduction . 


 PART ii . 


 LONDON : 


 METZLER and CO . ’S 


 list for APRIL 


 A. MACFARREN ’S NATIONAL melody , 


 SMART.—SIX original melody for 


 ETZLER and CO ’s new clarionet 


 ETZLER and CO ’s new harmonium 


 nthem for choral festival . 


 LEXANDRE ’S new 7 - stop harmonium , 


 LONDON 


 METZLER and CO .. WHOLESALE agent , 37 , GREAT 


 MARLBOROUGH STREET , W 


 422 


 CHOIRMASTER , 


 MR . WALTER MACFARREN’SPal ay + i 
 Chree Natinees , 
 io ) YY 
 HANOVER SQuaRE Rooms , * . SaturDAys , APRIL 9 , May 7 , and 
 UNE 4 

 first programme will include trio , Piano , Clarionet and Viola 
 ( Mozart ) ; Fantasia , Op . 77 ( Beethoven ) ; album - blatt , Op . 117 , 
 and variation in e flat , Op . 82 ( Mendelssohn ) ; Grand Duo in 
 a flit ( Hummel ) ; and New Sonata , Piano and Violin ( Walter 
 Macfarren 

 artiste : 
 Miss EDITH WYNNE , Miss RANDAL , 
 Mr. HENRY HOLMES , Mr. LAZARUS , 
 Mr. WALTER MACFARREN , 
 and his Pupil 




 APR . LANSDOWNE COTTELL ’S concert . 


 INFIDDLE - CLASS BOARDING - SCHOOL . — 


 423 


 the MUSICAL TIMES , 


 APRIL 1 , 1870 


 BACH ’S GROSSE PASSIONS - MUSIK . 


 ( ST . MATTHEW . ) never mean unkindly to his own , so dearly do he 
 love they to the end ; and in the music to this , the 
 pertinence of the dissonant harmony on the word 
 ' " ' bése " ( unkindly ) , and the heavenly sweetness of 
 the change of key for the final phrase , attest the 
 subtlest power of the artist 

 allusion have be make to the recitative , ' ' o 
 grief , " which be succeed by the aria , ' ' with Jesus 
 I will watch and pray , " for tenor with chorus . More 
 than an allusion to this extraordinary piece could but 
 prove the powerlessness of word to represent its 
 beauty . in this and all the accompany recitative , 
 there be not the freedom for the singer which mark 
 those of Mozart and Beethoven , and some — such as 
 " deep and deep , " in Jephtha — of Handel ; 
 Bach ’s be rhythmical declamation exact the 
 high dramatic power of the vocalist , but deny 
 to he the liberty that mostly belong to recitative 
 singing . the solo phrase constitute the interlude 
 to the choral , which be here give with the verse be- 
 gin , ' ' o Saviour , why must all this ill . " its 
 melody be slightly varied , so as to make the more 
 gentle its expression of the touching sentiment , and 
 such variation may indeed be call embellishment . 
 the resolve set forth in the Aria to excel in devotion 
 the three choose apostle , and to watch ever with 
 Jesus , be beautifully relieve against the phrase for 
 chorus , ' ' and so our sin will fall asleep , " the rock- 
 e motion of which have a soothing , lulling effect , 
 which more than picture , it realise the sweetly 
 calm influence of prayer . the countless point : 
 of technical interest that mark this piece , such as 
 the wondrous harmony of the Recitative , the 
 double counterpoint to the first phrase of the Aria , 
 and the chromatic progression m the phrase that 
 ensue , must be study to be understand , and hear 
 to be admire 

 after the prayer in the Garden that the cup of 
 agony may pass , occur the Recitative , ' ' the Saviour 
 falleth low , " and Aria , " ' what tho ' trial , " for bass , 
 a piece of less intensity than either of those which 
 have be notice , but not the less true to its place 
 and purport . a spirit of cheerfulness infuse its 
 tuneful phrase — cheerfulness in the endurance of the 
 earthly ordeal for his sake who be the everlasting 
 pattern of patience . the concluding strain , to the 
 line 




 426 


 SENSATION MUSIC . 


 427place ; henceforth let they do the bidding of scene- 
 painter and property - man , and be thankful . yet , 
 in fully carry out this style of drama , theatrical 
 manager must not blind themselves to the absolute 
 necessity of keep an author ; forit be by no means 
 easy to illustrate the sensational scenery and effect 
 which may have be previously arrange . for 
 instance , place an insensible person across the rail 
 of the Underground Railway may be an excellent 
 idea ; but it be not every writer who would conceive 
 the happy notion of contine a manin a cellar which 
 look upon the Metropolitan line , so that he should 
 burst out and release the intend victim at precisely 
 the right moment . lessee and author , therefore , 
 should fraternise , if they wish to consult their own 
 interest , for the ' ' Sensation Drama " can only flourish 
 by their mutual co - operation 

 but , although we have arrive at a thorough un- 
 derstanding of the subject as apply to the drama , 
 it will be at once see that ' ' Sensation Music " be still 
 in its infancy : indeed , it must be admit , that the 
 pioneer in the cause have commit the same mis- 
 take which we have attempt to point out with 
 regard to dramatic production , viz . , that of merely 
 present establish work , aid by new and 
 startling additional effect . we may cite the late 
 M. Jullien as one of the most earnest disciple of 
 the movement ; for not only do he agree with Mr. 
 Richardson that the work of the great master be 
 capable of improvement by the introduction of thea- 
 trical accessory , but he actually use they at his 
 own concert . the storm in Beethoven ’s Pastoral 
 Symphony , for instance , be no long present 
 to the audience in its originally feeble state , but pea 
 be rattle in a barrel to represent rain , and sheet 
 of copper be shake to imitate the rolling of the 
 thunder . these attempt be of course very meri- 
 torious ; and it might reasonably be imagine that , 
 as in the similar case of the drama , they might 
 shortly lead to the notion that the composer should 
 be consider merely as a secondary personage , em- 
 ploye to illustrate musically certain effect already 
 prepare and decide upon , strangely enough , 
 however , although " sensation Music , " not only here , 
 but in America , have latterly very much obtain , the 
 primitive idea on the matter still linger , and ma- 
 terially hinder its development . in a great measure , 
 we believe , this may arise from the want of suitable 
 building , which should be fit up with a view to 
 the production of those grand effect which have 
 lately be so attractive . bell of different size , for 
 instance , might be judiciously hang throughout the 
 hall ; the room might be make to rock , so as to re- 
 present a storm at sea , or an earthquake ; subterra- 
 neous passage might be construct , from which 
 unearthly noise could issue ; and a communication 
 should be establish between the orchestra and a 
 row of cannon outside the building , so that , when 
 require in the music , the gun could by fire , by 
 mean of an electrical battery . such preparation as 
 these would materially promote the success of the 
 undertaking ; for , although our present concert- 
 room be very well adapt for the performance of 
 those unpretentious composition which have de- 
 light former generation , they be extremely in- 
 convenient in many respect for the due carrying out 
 of these new idea . we have already hear com . 
 plaint that , at a recent Festival , no proper accommo- 
 dation be provide for those who be engage in 

 who also assist in the performance of the composi- 
 tion , be extremely nervous for fear of mar the 
 work , owe to the novelty of the business and the 
 want of proper rehearsal . but , even suppose that 
 building such as we have describe should be erect , 
 we must move onward , as we have already do in 
 the drama ; and instead of fit the effect to the 
 composition , endeavour to fit the composition to the 
 effect . let we imagine a piece , for instance , de- 
 scriptive of the mutiny in India , illustrate by Tab- 
 leaux vivants , with real Sepoys . what magnificent 
 opportunity for the introduction of varied sensa- 
 tional appeal to the eye and ear ; and what a grand 
 climax when Sir Colin Campbell enter Lucknow , 
 to the sound of one hundred and twenty bagpipe 
 play by steam power , the National Anthem rise 
 above the din , sing by four thousand people , with 
 fifty gun fire simultaneously at the commencement 
 of each bar 




 the ORATORIO concert _ zealous and indefatigable conductor , Mr. Joseph Barnby . 
 that a composition present such enormous difficulty 
 of execution , should have be so perfectly sing by the 
 choir , be a sign of the care bestow upon the minute 
 detail during rehearsal ; but that it should have be so 
 sympathetically render throughout as to arouse the 
 spontaneous enthusiasm of the audience , be a proof that a 
 real love of art must have accompany the patient study 
 which it demand ; for no amount of mere mechanical 
 practice — no dry recapitulation of note by note , and pas- 
 sage by passage — could have ensure an interpretation so 
 instinct with that appreciation of the sublime grandeur of 
 the work , without which even the noble creation in 
 musical art can never be fully realize . the usual con- 
 ventional compliment upon " precision of attack " may 
 suffice for the excellent manner in which the several de- 
 partment of the choir enter in the " Credo , " or for the 
 boldness with which the complication of the double 
 fugue , " et vitam venturi , " be sustain throughout ; 
 but the exquisite singing of the choral part of the 
 " Benedictus , " the truly religious breathing forth of the 
 deeply sorrowful ' ' Miserere , " and the contrast of teele 
 show in the " Dona nobis , " be point which no cold 
 word of praise can sufficiently do justice to . apart from 
 these general remark on the truthful conception of the 
 composer ’s meaning in every phrase of the choral portion 
 of this work , we must particularly notice the brightness of 
 tone with which the soprano sustain and repeat the 
 exceptionally high note so constantly occur ; and it 
 must also be remark , that , wherever the male voice 
 be hear alone — as , for instance , where the bass lead 
 off the fugue , " in gloria Dei Patris , " and the commence 
 phrase for the tenor in the " quoniam tu solus”—the 
 tone be extremely full and resonant . in every part of 
 the Mass where the whole power of the choral body be 

 the high degree satisfactory ; and the delicate shading 
 of tone so essential to the expression of the varied feeling 
 of the word , appear to be thoroughly under the con- 
 ductor ’s control . the exacting music give to the solo 
 vocalist be sing by Madame Rudersdorff , Madlle . De 
 Salewska , Madame Patey , Mr. Cummings and Herr Car ] 
 Stepan . to say that Madame Rudersdorff give the 
 soprano part with that accuracy and devotional expression 
 indispensable to the due realisation of this important por- 
 tion of the work , be only to place on record that in every 
 respect this ready and competent vocalist fully sustain 
 her well - know reputation ; but word of warm congratu- 
 lation from all competent to judge , should be address to 
 Madame Patey , who sing with a precision , a purity of 
 style , and a real reverence for the noble task before she , 
 which can not fail to place she in the foremost rank of her 
 profession , a fact perhaps render even more highly 
 creditable to she when we consider that she supply , at a 
 short notice , the position assign to Madame Sainton- 
 Dolby , who be unfortunately absent from indisposition . 
 the tenor music be , as might be expect from an artist 
 so reliable and conscientious as Mr. Cummings , render 
 not only with the utmost refinement , but with an accuracy 
 of vital importance in the many intricate portion of the 
 Quartetts which be weave in with the chorus ; and Herr 
 Stepan ’s fine bass voice tell with admirable effect , the 
 whole of his music be deliver with an earnestness 
 which prove that to his eminent quality as a vocalist 
 he unite an enthusiastic passion for his art . the only 
 portion of the soprano part not give to Madame Ruders- 
 dorff , be the " Benedictus , " which be exceedingly well 
 sing by Madlle . De Salewska , who appear to have care- 
 fully train a pure soprano voice to the power of execute 
 the high order of sacred music . the orchestra be 
 thoroughly efficient in every department . the principal 
 violin be hold by Mr. Carrodus , who play the obb / igato 
 to the " Benedictus " with a delicacy which charm every 
 hearer ; and we trust that he duly appropriate a con- 
 siderable share of the applause with which the movement 
 be receive . no analysis of this sublime work , even of 
 the brief description , could be attempt here . we 
 could linger for hour over the fervent " christe eleison , " 
 so appropriately introduce in the relative minor of the 
 key of the previous ' ' Kyrie”—the solemn " et incar- 
 natus , " first calmly and gently deliver with the tenor 
 voice , the very vagueness of the key give an inde- 
 scribable charm to the subject — the exquisite dreaminess 
 of the " Benedictus , ' with the subdued strain of the 
 violin soar above the voice with a heavenly beauty 
 which defy description — or the stirring and grandly 
 develop fugue , in which the profound scholastic know- 
 ledge of the master be never display save tu intensify 
 the illustration of the text . but it would be impossible 
 to convey even the faint idea of a composition which 
 embody the deep thought , and the most mature 
 power of a mind like that of Beethoven . consider 
 the grand design of this work , and the excessive intricacy 
 of all its detail , the fact that its performance in a concert- 
 room should not only delight the many artist present , but 
 stimulate the general audience to audible demonstration 
 of approval , be , as we have already say , a triumph of which 
 Mr. Barnby and all associate with he have a right to be 
 proud . the clearness with which every point be bring 
 out , the excessive brightness of the orchestral accompanl- 
 ment , and the intelligent conducting of Mr. Barnby , be 
 fact which can not be overpraise ; and should this success 
 spur on the projector of these concert to renew exer- 
 tion , we sincerely hope that many composition upon 
 which the seal have be place for year , may yet find 
 their way to the heart and home of the music - love 
 public of England . Beethoven ’s Choral Fantasia be 
 give before the Mass , the pianoforte part be play 
 by Madame Arabella Goddard , with all that finish exe- 
 cution and refinement of style which the composition de- 
 mand . the principal vocal part be ably sustain by 
 the same singer as those engage in the Mass , with the 

 bring forth , the excellent balance of the voice be in 




 429 


 PHILHARMONIC SOCIETYMR . HENRY LESLIE ’S concert 

 at the second of these concert , Mendelssohn ’s music 
 to " Antigone " be give with a perfection which , as 
 far as the choir be concern , have never be equal in 
 this country ; and ifthe orchestra be not everything that 
 could be desire , the fault do not rest with the conductor , 
 the singing of the noble chorus , so strikingly illustrative 
 of the spirit of the greek drama , be not only admirable 
 for accuracy and perfection of intonation , but every 
 member of the choir throw so much real vitality into his 
 work , that the effect upon the audience , consider the 
 almost coldly classical nature of much of the music , be 
 perfectly marvellous . the " hymn to Bacchus " be 
 re - demand , and also the Quartett , " O Eros , " which 
 be most carefully render by Messrs. Lord , Pearson , 
 Musgrave . and Hubbard . a good word must also be 
 say for Mr. Chaplin Henry , who give the bass solo 
 with much intelligent expression . why Mr , P. B. 
 Phillips be select to recite the illustrative verse be a 
 mystery which we do not attempt to unravel . luckily , 
 however , the reading be abridge to the utmost possible 
 extent . in the second part , Herr Joachim play Beet- 
 hoven ’s Violin Concerto as he alone can play it , and be 
 receive with an enthusiasm which must have convince 
 he how highly his exceptional power be appreciate . 
 a successful début be make by Miss Stephens , who sing 
 " with verdure clothe " in so purely legitimate a style as to 
 evoke the warm applause . the chorus of Dervishes 
 and turkish March , from Beethoven ’s " ruin of Athens , " 
 bring an admirable concert to a termination.—the third 
 concert contain a very excellent selection of choral 
 music , amongst which Bach ’s motett , " I wrestle and 
 pray , " and Mendelssohn ’s " judge I , 0 God " be the 
 most prominent , the latter be , as usual , enthusiastically 
 encore . a great feature in the programme be Herr 
 Joachim ’s performance of Mendelssohn ’s violin concerto . 
 Beethoven 's symphony in C minor be the principal 

 the prospectus of both Italian Opera Houses 
 be now publish ; and if only a portion of the promise 
 therein give should be fulfil , the season will be an 




 430 


 the collect for the day 


 _ . ' 2 = 


 » 2S 


 = 72 


 GRANT , we BESEECH THEE 


 = — _ = - _ = — 


 = = > > SS 


 CHI2 


 SS SS 2 SS E 


 NEW volume . 


 for the ORGAN . 


 arrange by JOHN HILES 


 POOK XIX . 


 BOOK XX . 


 BOOK XXL . 


 BOOK XXIL 


 BOOK XXII . 


 BOOK XXIV . 


 BOOK X 


 BOOK XXVL 


 BOOK XXVII . 


 NOVELLO ’s 


 second serie 


 a collection of 


 by MODERN composer 


 LONDON : NOVELLO , EWER and CO 


 08 


 435 


 _ — _ a concert in aid of the building fund of the 
 school in connection with the Martyrs ’ Memorial Church 
 in St. John - street - road , be give on the 11th ult . , at the 
 schoolroom , Amwell - street , Pentonville . the selection 
 be chiefly choral , be support by a strong and ad 
 mirably train choir , under the direction of Mr. W. C 
 Batchelor , choir- master of St. Clement ’s , Eastcheap . the 
 soloist be Miss Lancaster , Miss Weller , Mrs. Batchelor , 
 Mr. Hagon , Mr. Hodsdon , Mr. Willis , and Mr. F. O. 
 Stevens , the first part of the programme be devote 
 tosacre music , and comprise selection from the work 
 of Mendelssohn , Mozart , Barnby , Gounod , and Handel , all 
 of which be admirably render . in the secular 
 part , several solo , duet , and glee be sing with good 
 effect , and much applaud . ' the accompanist be Mr. 
 J , J. Stephens and Miss Edith Lancaster . the attendance , | 
 unfortunately for the object for which the concert was'| 
 give , be not large 

 Mapame Matton give her second concert at ! 
 the Clapham Hall , on ' Tuesday , the Ist ult . , before a ! 
 fashionable and most appreciative audience . the great ! 
 feature of the evening be the pianoforte performance of| 
 the concert giver , the piece select be Beethoven ’s 

 sonata in e flat ( Op . 27 ) , and Thalberg ’s Fantasia on 

 AN evening concert be give in Camden 
 Hall , Camden Town , on the sth ult . , by Mr. Ellis 
 Roberts , assist by Miss Ellen Glanville , Miss Neville , 
 Miss Adelaide Newton , Mr. R. Temple , and Mr. Stanley . 
 pianoforte , Miss Kate Roberts ; violin , Mr. Ellis Roberts , 
 junr . ; flute , Mr Perey Keppel . the concert give great 
 satisfaction , and be numerously attend 

 Tue Schubert Society give its first concert of 
 the fourth season on the 24th February at the Beethoven 
 Rooms , the first part consist of composition by 
 Schubert , and the second part be miscellaneous . the 
 vocalist be Miss Barry Eldon , Miss Gertrude Mayfield , 
 Mr. Stedman , and Mr. Renwick , all of whom be highly 
 successful . Herr Schuberth play a violoncello solo by 
 Schubert , and be join in other instrumental piece by 
 Herr Ludwig and Herr Jung ( violin ) , Herr Eberwein 
 ( viola ) , and Herr Schrattenholz ( piano . ' the selection 
 be well receive by a crowded and fashionable audience 

 on the 28th February a concert , in aid of 
 charity , take place at Myddelton Hall , Islington , under 
 the able conductorship of Mr. W. H. Monk . the pro- 
 gramme comprise , amongst other vocal piece , Handel ’s 
 as when the dove , " sing with much feeling by Miss 
 D’Almaine , and Meverbeer ’s song , " ' my pretty fisher- 
 maiden , " well give by Mr Croft . mention must be make 
 of the performance of Miss Rye , a young piani - t who 
 display considerable talentin Sloper ’s * Galopde Concert , " 
 and obtain a well merit encore tor the " Fairies ’ 
 Reverie , " by Tito Mattei . a noticeable feature of the 
 evening wasalsoa selection of madrigal , & e. , by the " St. 
 John ’s ( Irpheus Quartett , " which be render with such 
 taste and precision as to gain repeat encore . the 
 hall be tolerably well fill ; and we have every reason to 
 believe that a handsome sum be realize for the charity 




 436the MUSICAL TIMES.—Arri 1 , 1870 

 the solo singer be also highly successful . Mr. Bishen- 
 den be much applaud in " the Friar of order grey , " 
 the audience insist upon anencore . Mr. Fletcher give 
 a solo on the horn , and the band execute in good style 
 the march from Le Lrophéte and Beethoven 's third Sym- 
 phony 

 aw excellent performance of the Messiah be 
 give , under the direction of Mr. F. A. Bridge at , Burdett 
 Hall , Limehouse , on Thursday , the 24th February . the 
 principal vocalist be Madame ' Talbot - Cherer , Miss 
 Helen Barron , Mr. Arthur Thomas , and Mr. F. A. Bridge . 
 the chorus consist of about 120 voice . pianoforte , 
 Miss E. Stirling ; harmonium , Mr. John C. Ward ; solo 
 trumpet , Mr. J. Dearden 




 agency 


 TO correspondent 


 439Bevrast.—The last of the Monday Popular Concerts 
 be give in the Ulster Hall , on the mth ult . , when Mr. Wm , 
 Moss 's glee party appear for the first time at these performance . 
 the hall be crowded in every part , include the orchestra . ' the | 
 glee select be , " in the lonely vale of stream " ( Calcott ) , | 
 " Ave Maria ' ( H. Smart ) , the lark " ( Mendelssohn ) , and * hail 
 to the chief ' ( Bishop ) . the first and last be enthusiastically 
 tneore . it be hope that the engagement of Mr. Moss 's party 
 will help to revive these weekly concert next season . the band of 
 the 18th Royal Irish be in atiendance , conduct by Mr. C , Fitz- 
 patrick . during the evening Mr. Alfred Collier play two selec- 
 tion on the grand organ 

 Brkevueav.—Mrs . Beesley , of Liverpool , give a concert 
 at the Music Hall , on the 23rd uit . , assist by Madame Florence 
 cia , Mr. Edward De Jong , ( solo flute ) , and M. E. Vieuxtemps 
 violoncello ) . the programme comprise selection from Beet- 
 ven , Handel , Mozart , Weber , Haydn , Bellini , & c. Mrs. Beesley 
 teceive much applause for her excellent performance of Beethoven 's 
 moonlight Sonata , Madame Florence Lancia sing * * qui la voce , " 
 purituni , with much brilliancy , and be also highly successful 

 ia Weber 8 Scena , ' softly sigh , " Mozart 's " ' Voi che sapete , " and 
 Handel 's * let I wander . " we regret that there be not a large 
 attendance 

 accompany with illustration on the harmonium . the audience 

 be large and appreciative . the lecturer be listen to with 
 much attention , and frequently applaud . at the close a hearty 
 and unanimous vote of thank be pass to the lecturer , and also 
 to the member of the choir for their gratuitous assistance . — — the 
 second concert in aid of the Postmen ’s Provident Society be give 
 in the Town Hall on the 16th ult .. before a numerous audience . 
 the principal vocalist be Miss Edith Wynne , Madile . Drasdil , 
 Mr. W. H. Cummings , and Herr Carl Stepan . Miss Wynne be 
 highly successful in the solo part of Mendelssohn 's " hear my 
 prayer ; " and Madile . Drasdil ’s excellent contralto voice be hear 
 to the utmost advantage in two song . the seeond of which , ' ' the 
 Robin be weep , " be enthusiastically encore . Mr. Cummings 
 and Herr Stepan also gain much applause in their vocal solo ; the 
 scena " Madamina , " by the latter gentleman , be especially well 
 give . Mr Heap play Benedict 's Pianoforte Fantasia ou ' * where 
 the Bee suck , " and M. Sainton a violin solo , and with Mr. Heap , 
 part of Beethoven 's Op . 30 , both artist be receive with warm 
 demonstration of approval . the concert be in every respect 
 thoroughly satisfactory 

 Braprorp.—The first concert of the Bradford Church 
 Literary Institute Choir be give in the Ashley - street School 
 the programme include 
 Locke 's music to Macbeth , and a selection of part - song , glee , duet , 
 and song , which be sing in a very satisfactory manner by the 
 member of the choir . the principal vocalist be Misses Watson , 
 Blakey , Sewell , and Goode , and Messrs. Hartop , Taylor . Patterson , 
 and Knowles . Mr. J. H. Rooks ( professor of music and accompa- 
 nist to the choir ) preside at the pianvforte in a very efficient 
 manner , and Mr. G. F. Sewell conduct with his usual ability . 
 the concert be thoroughly successful 

 Bromtey.—The Lenten Concert of the Bromley Insti- 
 tute Choral Society take place on Tuesday the 8th ult . , when Men- 
 delssohn ’s Oratorio , St. Paul , be perform with much effect under 
 the able direction of Mr. Walter Latter . the principal vocalist 
 be Mrs. H. Spooner , Mrs. Arnaud , Mr. T. Pearson , Mr. A. Ben- 
 nett , and the Rev. O. Vignoles , all of whom acquit themselves 
 well in the arduous solo music entrust to they . the chorus 
 be give throughout with commendable care and precision , prov- 
 e unquestionably the excellent result of Mr. Latter ’s energetic 
 effort in train the member of the choir . the accompanist 
 be Mr. J. F. Meen ( pianoforte ) , and Mr. Harrison ( harmonium ) 
 whose efficient aid contribute materially to the success of the per- 
 formance 

 Campripce.—The eighth meeting of the Amateur 
 Orchestral Guild be hold on the 22nd and 23rd February , when 
 two very successful concert be give for the benefit of Adden- 
 brooke ’s Hospital and the building fund of a new church . the 
 symphony perform be Spohr ’s " historical , " and Haydn 's 
 " Oxford . " the overture * * Der Schauspieldirector , " ( Mozart ) , Op . 
 12 , Spohr " Die Najadin , " and ' Die Waldnymphe , " ( Bennett ) , 
 * Abu Hassan . " ( Weber ) , ' ' a Midsummer Night 's Dream , " ( Men- 
 delssohn ) , * * King Stephen , " ( Beethoven ) , and * * the Merry Wives of 
 Windsor , " ( Nicolai ) . the vocal portion of the concert be ably 
 sustain by the Trinity College Musical society , the whole be 
 under the direction of Dr. Haking . a concert of sacred and 

 secular music , be give in the concert room of the Workmen 's 




 440Dostriy.—The member of the St. Jude ’s Choral Union 
 give their first concert of the season’in the Lecture Hall at South 
 Richmond , on the 22nd February . the programme include 
 amongst other composition selection from Haydn ’s Seasons . the 
 principal vocalist be Master Thos . Ashe ( St. Patrick ’s Cathe- 
 dral ) , Miss Cooke , Mr. Allen , Mr , Holmes , Mr. Wade , and Mr. Dyas . 
 much of the success of the performance be owe to Mr. R. Mac- 
 lagan , who conduct with his usual ability 

 EprxsoureH.—The annual concert of the University 
 Musical Society be hold in the Music Hall on the 18th ult . there 
 be , as on former occasion , a chorus of past and present student , 
 number about sixty ; and the orchestra consist of amateur , 
 chiefly from the St. Cecilia Society , and of wind instrument player 
 from the band of the 17th Lancers , the conductor be Mr. Adam 
 Hamilton . a feature in the programme be a march and chorus 
 * Abendscene , " by K. Appel , score especially for this concert by 
 the composer , which be well give , and elicit much applause . 
 the playing of the orchestra in Mozart ’s E flat Symphony show a 
 marked improvement upon the performance of former year ; and 
 two overture be also highly effective . Dr. Hamilton and Mr , 
 Makgill be well receive in their vocal solo ; and two amateur 
 also produce a marked impression , both by the excellence of their 
 voice and the style of their vocalisation . the solo singer have 
 the advantage of Professor Oakeley as accompanist ; and a great 
 attraction in the concert be the Professor ’s pianoforte piece , 
 Book 3 , no . 1 , and Book 4 , no . 3 of Mendelssohn ’s ' ' Lieder ohne 
 worte , " both of which be play with good expression and execu- 
 tive power . the choral music — include the ' * Students ’ song , " 
 compose by Professor Oakeley for last year ’s concert — be give 
 with much taste and precision . — — on the 24th ult . , Professor Oakeley 
 give a selection of organ music in the Music © lass - Room , the interest 
 of the performance be much enhance by some very excellent 
 prefatory remark , those especially upon Bach be in the high 
 degree instructive to student . the programme contain a most 
 judicious selection of classical composition , and include some of 
 Bach 's organ prelude on chorale , an organ Fantasia in C minor , 
 by the late Adolph Hesse , and the Funeral March from Beethoven 's 
 Pianoforte Sonata in A flat , select as a tribute of respect to the 
 memory of Moscheles , of whom Professor Oakeley learn the 
 Sonata at Leipzig 

 FropsHam — the member of the Parish Church Choir , 
 assist by Miss Winward , of Manchester ; Mrs. Hammersly , Miss 
 Roberts , and Miss Atkinson , of Cleckheaton ; and Miss Latham , of 
 Frodsham ; give their annual concert in the Town Hall , on Thurs- 
 day evening , the 19th ult . , to a large and influential audience . the 
 programme consist of a well - select number of glee , part - song , 
 duet , and ballad . Mrs. Atkinson accompany on the pianoforte 
 with excellent taste , and Mr. Riley , the organist , conduct 




 441 _ 

 she be announce in the programme , and her fresh voice and 
 excellent style procure for she a warm welcome . the chief instru- 
 mental piece of the concert be Spohr ’s Symphony in C minor ( no . 
 5 ) , which be well play , and much admire . Schubert 's fine 
 overture to Rosamunde be also very successful ; and Weber 's Zu- 
 overture be brilliantly perform . Gounod ’s ' ' meditation " 
 on Bach ’s Prelude ( perform on organ , pianoforte , and violin ) , be , 
 as usual , encore . a portion of Beethoven 's Septuor be give ; and , 
 although it be excellently play , we must protest against such 
 partition , however charming even a " portion " may be . Meyerbeer ’s 
 ' Marche aux Flambeaux " in C major , ( compose on the ion of 
 marriage of the english Princess Royal ) , bring the concert 
 toa successful conclusion.——t#e fourth subscription concert of 
 the Philharmonic Society be one of particular excellence , both in 
 selection and performance . the principal artist be Miss Edith 
 Wynne , Madame Patey , Mr. Cummings , and Mr. Maybrick . the 
 e commence with Sullivan 's overture , " in memoriam , " 
 which go very well , and be much applaud . this be fol- 
 low by the Prodigal Son , both work be conduct by the 
 composer . the audience would willingly have encore more than 
 one of the number , but the unaccompanied quartett be the only 
 piece which be repeat . the second part of the concert be 
 by Spohr ' ’s overture to the " last judgment , " then come 
 Yendelssohn ’s hymn and chorus ' ' hear my prayer , " the solo of 
 which be finely sing by Miss Edith Wynne . Spohr 's cantata , 
 " God , thou art great , " and the ' ' war March of the priest , " in 
 ie be the closing work of a most admirable performance . 
 — — ttuee third of the series of four performance on the plan of the 
 Jondon " Monday Popular Concerts " be give at the Philharmonic 
 Hall , on the 9th ult . , with much success . the executant be 
 Madame Schumann ( pianoforte ) , Herren Joachim , and L. Ries , Mr. 
 Yerbini , and Signor Piatti , for the stringed quartett , and Miss 
 Edith Wynne , vocalist . the programme include Mendelssohn ’ 
 tt for string in e flat ( Op . 12 ) ; song , ' " ' Rose softly bloom- 
 e , " ( Spohr ) , Ciaconna in g minor , violin , with pianoforte accom- 
 t ( Vitali ) , finely play by Herr Joachim ; song , ' I know 
 asong " ( Benedict ) , ' ' Andante Spianato " ( Chopin ) , and Scherzo in 
 Eminor ( Mendelssohn ) , most characteristically render by Madame 
 Schumann : stringed quartett in F major ( Haydn ) ; song , ' ' Whither " 
 and ' the question , " Schubert , and the grand quintett for piano- 
 forte , and stringed quartett in e flat major , Op . 44 ( Schumann ) , 
 which go to perfection . the hall be crowded , and the applause 
 most enthusiastic 

 Me.sourne.—Operatic performance , under the direc- 
 direction of Messrs. Lyster and Smith , have be give here with a 
 very excellent company . the prospectus announce that several 
 opera new to the Colonies be to be present during the season , 
 which extend over nine week from the 5th February . Herr Siede 
 is musical conductor , and Mr. Perraton chorus - master 

 Mertuyr.—A very attractive amateur concert be 
 give , on the 3rd ult . , at the Temperance Hall , before a large 
 audience . several choral piece — amongst which be Men- 
 delssohn 's ' ' vintage song , " Sullivan 's ' ' o , hush thee my babie , " 
 and a " Swabian Volkslied”—were excellently sing , under the able 
 direction of Mr. Lawrance , to whom in a great measure the success 
 of the concert be due . a number of solo be also give with 
 much effect during the evening , and some instrumental piece be 
 most efficiently render , a pianoforte solo by Miss Fowler , be 
 teceive with well deserve applause 

 Norwico.—On Wednesday evening , the 23rd Feb- 
 ruary , Mr. H. W. Kingston Rudd give a pianoforte recital at 
 Mr. Noverre ’s Room , before a large and most appreciative audience . 
 the programme include | Beethoven 's Sonata in a flat major ( Op . 
 26 ) , Weber ’s brilliant Rondo ( Op . 62 ) one of Handel ’s Harpsichord 
 lesson , and some light piece , in all of which , accord to the 
 local paper , Mr. Rudd display talent of a very high order . the 
 ee be assist by several vocalist from the Amateur 

 ety 




 442 


 443 


 dure the last month , 


 ESLEY , SAMUEL SEBASTIAN 


 authorise musical text - book at RUGBY SCHOOL . 


 , element of music SYSTEMATI- 


 sonc by BERTHOLD TOURS 


 how shall I picture THEE , LADYE FAIR ? 33 . 


 HE ANGLICAN CHORAL SERVICE BOOK , 


 USELEY and MONK ’S PSALTER and 


 OULE ’s collection of 527 chant , 57 


 for CHORAL festival , 


 the simple form of intoned SERVICE.—THE 


 OULE ’s DIRECTORIUM CHORI ANGLI- 


 OULE ’s DIRECTORIUM CHORI ANGLI- 


 HE order for the HOLY COMMUNION , 


 R. WESLEY ’s NEW chant - SERVICE , 


 R. HILES ’ NEW EVENING SERVICEIN FP , 


 TONIO SOL - FA edition . 


 445 


 upplemental HYMN and TUNE BOOK , 


 § TAINER , WESTLAKE , CALKIN , REINAGLE , BARNBY , WARD , VER- 


 erie of MODERN KYRIES , HYMN 


 ° MR . BENTLEY will publish , 


 istory of the PRINCES de conde 


 HE FOURFOLD MESSAGE of ADVENT ; 


 the QUEEN 


 morning and evening 


 BY 


 SAMUEL SEBASTIAN WESLEY 


 price to subscriber 


 with an appendix for festal use 


 JOSEPH BARNBY 


 \ e declare unto you glad tiding . 


 446 


 SWEET spring be come 


 to CHORAL SOCIETIES and NATIONAL school 


 410 when the morning SHINETH 


 NEW WHITSUNTIDE anthem 


 he starry firmament on high 


 will greatly rejoice in the LORD . 


 _ _ _ 


 . the * 


 concert music 


 edit by 


 JOHN HULLAKE 


 LONDON 


 ASHDOWN and PARRY 


 HANOVER SQUARE 


 VOM 


 A SACRED IDYLL 


 the music compose , and dedicate to 


 SIR MICHAEL COSTA 


 EDMUND T. CHIPP 


 HOPE IN 


 A SINGING CARD 


 for the use of choir , choirmaster , school , 


 to teacher of SINGING 


 NEW 


 ma8¢4 


 MAGNUS DOMINUS . 


 TO organist . 


 _ — _ 


 JERTHOLD 


 447 


 character represent 


 act i 


 act ii . 


 time of performance one hour and ten minute 


 W. H. BIRCH , 104 , LONDON STREET , read 


 the 


 edit by DR . SPARK . 


 content . 


 KERBUSCH 


 DR . SPARK ’S 


 christ being raise from the dead . " 


 V. GABRIEL 's NEW song . 


 tribute to SPRING . 


 448 


 CHAPPELL 


 0S CHEAP publication 


 for various instrument 


 CHAPPELL ’s instruction book 


 VIOLIN 


 FLUTE 


 CORNET 


 ENGLISH CONCERTINA . 


 GERMAN CONCERTINA . 


 PIANOFORTE 


 CLARIONET . 


 HARMONIUM . 


 SINGING . 


 HARMONY . 


 GUITAR . 


 SAX - HORN 


 SERAPHINE ANGELICA , 


 DRUM and FIFE , 


 VIOLONCELLO 


 BANJO 


 TROMBONE 


 VIOLIN 


 FLUTE 


 CORNET - A - PISTON 


 GERMAN CONCERTINA 


 GUITAR 


 ENGLISH CONCERTINA 


 HARMONIUM 


 CLARIONET 


 SAXHORN 


 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDON 


 ECAIRCAIED I 


 4/55 


 7 - 3 


 eec