


E MUSICAL TIMES


FEBRUARY 1, 1869


MUSIC IN THIS NUMBER. 


BLESSED ARE THE MERCIFUL 


S. JAMES’S HALL


THE


THE FIRST SUBSCRIPTION CONCERT


WILL BE GIVEN ON


FRIDAY, FEBRUARY 5 


HANDEL’S “JEPHTHA


MENDELSSOHN’S 


LIEDER OHNE WORTE,. 


BOOKS 1 TO 8


ALBAN’S, HOLBORN. —THREE BOYS 


FIRST-CLASS VOCALISTS ONLY.—A


NEW SONGS BY JOSEPH BARNBY


SONGS AND DUETS BY


ANTON RUBINSTEIN


. MORNING SONG .... 


. EVENING SONG 


’ SONG FROM EGMONT - . 


PROFESSIONAL NOTICES. 


MISS BLANCHE REEVES


MR. WILBYE COOPER


I" LISHED IN THE BEST STYLE, AND ON MODERATE 


RASS, REED, STRING, AND DRUM AND


EYOND ALLCOMPETITION.—T. R. WLLLIS; 


INSTRUMENTS


UTLER’S MUSICAL 


CORNETS, SAXHORNS, DRUMS, FLUTES, CLARIO- 


WF USICAL INSTRUMENTS FOR VOLUN 


MR. W. HUTCHINS CALLCOTT


MR. W. SUTCH


MR. J. C. BEUTHIN, 


AND W. SNELL’S IMPROVED HARMO


ROGER’S CELEBRATED GUINEA COM 


659


REAKFAST—EPPS’S COCOA. 


660


NEW ANTHEM FOR LENT. 


IN THE PRESS. 


96TH PSALM). 


ACRED SONGS FOR LENT


SIXPENNY MUSICAL MANUAL. 


AUNDERS’S PRACTICAL INSTRUCTIONS 


HAMILTON’S PATENT UNION” MUSICAL NOTATION. 


57


661


PUBLISHED BY


C JEFFERYS, 


57, BERNERS STREET, LONDON, W


FELIX GANTIER’S 


ORIGINAL 


OPERATIC GEMS


AARPAARARMARARARARARAMAMAAD


PICTURES OF PALESTINE. 


3 0


METZLER AND CO.’S


MUSICAL BIJOU


TWELVE SACRED VOCAL DU


9.—THIRTEEN CHRISTY MINSTRELS’ COMIC SONGS 


11—FIFTY POPULAR HYMNS


OPPLER


EXETER HALL


MAGAZINE OF SACRED MUSIC. 


NO. XIII, VOL. 4, FOR FEBRUARY, 1869


LONDON


C. JEFFERYS, 57, BERNERS STREET, LONDONTestimonials from eminent Musicians and Clergymen, as to capa

bilities as an Organist, Choir-trainer, Pianist, Vocalist, Composer 
and Teacher. Beethoven, care of Novello, Ewer and Co

RGANIST.—A Gentleman is willing to take a




HYMNS ANCIENT AND MODERN


FOR USE IN THE SERVICES OF THE CHURCH


0 4


HYMNS WITH ACCOMPANYING TUNES


CH


663


THE MUSICAL TIMES, 


FEBRUARY 1, 1869


MUSICAL PITCH. 


664ductors hesitate to perform the monumental choral | bY @ genius so brilliant, and a nature so pure and 
works of the great masters of the whole of the | gentle, writes of Mendelssohn as an enthusiast ; but

eighteenth, and the greater part of the first-half of her reminiscences include so many incidents in which 
the present century.” It is obvious, therefore, that | Others bore a conspicuous part, that we have indis- 
the letter of Mr. Reeves was the one thing wanting | PUutable proofs of the esteem in which he was held by 
fointroduce a reform, the desirability of which had #!! who were intimately acquainted with him. _ It is, 
been silently admitted for years ; and when we know | indeed, interesting to read how, as a boy of fifteen, 
that many of the works, even of Mozart and| When Moscheles (then scarcely thirty years of age 
Beethoven are, in England, approached with terror | 8°V° his first concert in Berlin, he listened wi 
byall engaged in their interpretation, there can be breathless excitement to the comparatively mature 
little doubt that the sooner the change is made the| C°MP0S¢r8 E fiat major concerto. How, at a supper 
fetter, both for art and artists, The firm establish-|P@*ty at his father’s house after the concert (where, 
ment of the French pitch is now the grand point to besides Moscheles, Hummel, Berger, Zelter, and 
be gained ; no compromise should be made; for any other celebrities were present) the “ handsome boy in 
variation of the diapason normal would be again|® jacket” stood motionless beside the piano, whilst 
ignoring the question of the absolute necessity of Hummel and Moscheles were performing; and then, 
miformity ; and, by fixing a third pitch, not only | being pressed to play himself, how he burst into 
virtually challenging a decision which has been ar- tears and rushed from theroom. The sensitive nature 
tived at by the most eminent men at the French of his youthful pupil could scarcely have been suffi- 
conference, but compelling Continental artists who ciently evident to the somewhat stern Zelter, or he 
visit us to submit to a mere national caprice. In would hardly have met the boy's imploring looks by 
wging the adoption of this diapason, however, let|*8* requesting him to play; still less could he have 
ws hope that in future the conductor may be all- wounded his feelings by exclaiming,—‘“ What on 
erful. It has been too much the custom for|¢@"th is the matter with you, boy? Are you going 
mstrumentalists to tune to each other, and not to to show the white feather, after playing fearlessly in 
a fixed pitch given by the conductor; it therefore | S84 concerts, and before our Gothe, in Weimar ? 
often happens, that at the end of the first part of a What must I write to him about you?—that you 
concert, the wind instruments (sharpened by the have become a poltroon?” ‘Tears, indeed, could be 
breath) bring up the strings to their standard; and the only reply to such an uncouth rebuff. Moscheles 
thus the pitch of the whole orchestra is elevated. must have been somewhat surprised when, after 
The conductor should declare the normal diapason|Pecoming instructor to the young Felix, at the 
at starting; and when an opportunity occurs, he earnest request of his mother, he heard him, two days 
should see that all the players agree with it, instead after the memorable supper-party we have mentioned, 
of leaving them to settle the matter amongst them- play the E flat concerto which had so delighted him 
selves, Once establish this law, and instrumental-|®* the concert, with all the fire, impetuosity, and 
ists will be the kind friends, instead of the bitter|™USical feeling of a finished artist. ‘The intellectual 
enemies, of vocalists; for as the conductor's pitch| ining of Mendelssohn must have been materially 
would reign with despotic sway, tuning may, for the aided by his constant intercourse with men of the 
time, mean flattening, as well as sharpening. highest mark in science, art, and literature. At his 
_Inconelusion, we sincerely hope that all persons|W home not only was found that life of love and 
interested in the question may give up small points|Pe#ce which springs spontaneously from a mutual 
of difference for the sake of unanimity. The French |{#™ily affection, but he was surrounded by a constant 
iapason has been fixed upon after mature delibera- | ®t™mosphere of intellect ; music, of course, forming a 
tion; and has been proved to be essentially a prac- principal part in the delightful meetings continually 
tical one. Be it remembered, then, that in adopting | ‘king place ; but conversation, lifted far above the 
itwe not only have an excellent working standard dead level of ordinary “‘ society,” invariably reigning 
pitch, but we set a worthy example to other nations | SUPTeme. When Mendelssohn left this happy home, 
which may still remain in a state of uncertainty with his father, to seek the valuable counsel of 
pon the subject. Such a bond of brotherhood is| Cherubini, in Paris, there can be little doubt that he 
striving for. As we have said in our opening | “ied with him a mind so cultivated to the appre- 
temarks, the signs by which the art is expressed are| “tion of all that was high and noble, that the 
intelligible alike to all the world : let then the prac- debasing influences of the world could have little or 
exposition of these signs be equally agreed|2° effect upon his character. The journey of the 
upon; for, although the confusion of tongues may |YOU28 composer to Scotland, with his dear friend 
have placed a barrier between man and man, music lingeman, was an event which he always looked 
Semich bes ever been. and ever will be, the gentle back to with the utmost pleasure; and were it not 
Medium between man and his Creator—should at| that it might be considered a violation of a sacred 
be an universal language. trust, we should be glad if those letters, so full of 
youthful enthusiasm, which he wrote to Moscheles on

ee




4EPEBGEEN BES


667has been so long and so deservedly celebrated. The first 
concert, on the 4th inst., will include Mendelssohn’s 
music to 4 Midsummer Night's Dream, the same com- 
poser’s pianoforte concerto, in D minor, to be played by 
Madame Schumann; Samuel Wesley’s fine motett, “ In 
exitu Israel,” and Schubert’s “‘ Song of Miriam.” At the 
second concert a shoit work, by Mr. G. A. Macfarren, 
called “ Songs in a Cornfield,” will be performed for the 
first time

Tue Annual Report of the Birmingham 
Amateur Harmonic Association shows that the Society 
is in a highly prosperous condition; for although the 
present year is the one immediately following the Bir- 
mingham Triennial Festival, there has been no diminu- 
tion in the number of members, as was too often the case 
on former occasions. Amongst the works rehearsed, we 
find Beethoven’s Mass in D, Mendelssohn’s Antigone and 
Loreley, Schubert’s Grand Mass in E flat, and many other 
Important woiks. ‘The financial position of the Associa~ 
tion is also thoroughly satisfactory ; and the members of 
the committee have every reason to congratulate them- 
selves upon the result of their indefatigable endeavours 
to promote the interest of the Society, and the advance- 
ment of good rausic in Birmingham

Ay interesting lecture was given, on the 13th 
ult., at the Literary and Scientific Institution, Hackney, 
by the Rev. John Curwen, on ‘* Some views ot music as 
an art and a science, suggested by the Tonic Sol-fa 
method of teaching it.” Charles Reed, Esq., M.P., oceu- 
pied the chair. During the evening some satisfactory 
exhibitions of readiness in composing and singing at 
sight were given by disciples of the Sol-fa system




668Minuet and Trio, for the Pianoforte. Composed by T. 
Ridley Prentice

WE are always glad to see a young composer working 
upwards ; and are therefore disposed to view Mr. Prentice’s 
modest Minuet and Trio in a more favourable light than 
we might have done had it formed a portion of that 
*‘Grand Sonata” with which so many inexperienced 
writers almost commence their career. In a large work 
of this kind so many good ideas are effectually buried 
that the composer very often receives much less than his 
due meed of praise; and thusa really meritorious aspirant 
for fame may encounter a check at the very time when 
he stands in the greatest need of sympathy and encourage- 
ment. That Mr. Prentice hashad Beethoven in his mind 
when he composed this Minuet and Trio does not in the 
least degree detract from the merit of his music. Both 
movements are well written. ‘The Minuet, in B flat 
major, is melodious; and the passages of imitation show 
that the composer has studied ina good school. The 
Trio, in the tonic minor, is well contrasted with the 
Minuet; the accompaniment, moving in triplets, and the 
division of the subject between the treble and bass part of 
the instrument, having a very excellent effect. We hope

in to meet with Mr. Prentice in a composition of 
somewhat greater pretension




669


CHARLES JEFFERYSTue first of these pieces is an animated Polonaise, in 
D flat major, the themes in which are treated more artis- 
tically than we are accustomed to see in the majority of 
modern pianoforte compositions. There is frequent 
change of key; and some alterations in the harmony at 
the recurrence of the first subject have an excellent effect. 
The second theme, in A flat major, is afterwards intro- 
duced, most unexpectedly, in A natural major, leading to 
the first subject in the tonic minor (written in C sharp 
minor), and the composition coneludes with some brilliant 
passages’ in the original key. This will be found a good 
piece, both fur practice and performance; and the ama- 
teur will be glad to learn that the leading fingering has 
been marked by the eomposer, wherever a difficulty is 
likely to be found. The second piece, “ Flower de Luce,” 
we have already noticed in the review of Hanover Square 
for September. It is an elegant and placid ‘song with- 
out words ;” and thoroughly within the reach of players 
who have been trained to use their fingers for expression, 
as well as execution. The fact of its being reprinted 
from the serial in which it originally appeared is a suffi- 
cient proof of the favour with which it has been received

Tema con var aziont, from Beethoven's Septett. Arranged 
for the Pianoforte by Frederic N. Lohr

Tr always gives us pleasure to see such excellent music 
as this adapted to our household instrument; for, al- 
though but a faint reflection of the original, it inculcates 
a taste for what is really good ; and increases the grati- 
fication of listening to it whenever it can be presented as 
the composer intended, The arrangement before us is 
skilfully written; the effects of the various instruments 
being reproduced without presenting any great difficulties 
to the executant. We particularly admire the 4th varia- 
tion (in B flat minor) the various touches in which are 
clearly indicated, so that the contrast of the several parts 
may, with a well-trained player, be effectively brought 
out. We are glad to find that this piece will form No. ] 
of a series of similar arrangements of classical works




TO THE EDITOR OF THE MUSICAL TIMES


TO THE EDITOR OF THE MUSICAL TIMES


THE MUSICAL TIMES


COMPOSED BY


BLESSED ARE THE MERCIFUL


FULL ANTHEM FOR FOUR VOICES


SACRED MUSIC (PRICE THREE-HALFPENCE EACH). 


177 


297 


186 


213 


44


334 


182 


54 


345 


70


127 


237 


308 


WA


373 


379 


384


23 


249 


| 131 


123 


155 


100


363Barnby

There were whisp’ agi (Christmas 
Carol) . T. Cooper 
Thou art gone to the grave Beethoven 
Teach me, O Lord ... Dr. Rogers 
Teach me, O Lord . T. Attwood 
Teach me Thy way... «.. Croce 
Te Deum laudamus . . T. Cooper

Te Deum laudamus ... Dr. §. S. W




340 


FULL ANTHEM FOR FOUR VOICES


TENOR : 


@ = 60


# SAE AS


N | 4 


PP 


— -§_@——__6——_2—_,__02-——_—___ 


BLESSED ARE THE MERCIFUL


BLESSED ARE THE MERCIFUL


2A 


7 §-PS———-B> 


SS


VR


QO


> SS 


111


TO THE EDITOR OF THE MUSICAL TIMES


TO CORRESPONDENTSPage and Index to Vol, VILL, will appear in the

The continuation of the“ Incidents in the Life of Beethoven,’ 
of other matter |x rae A received, it is somewhat invidious to select 
|any for special commendation

success




676


678both oceasions. The Concerts were a great success, 
Lypnry.—Mr. J. A. Matthews’s Annual Con- 
cert was given at the Assembly Room, on Monday, the 
18th ult. The vocalists were Miss Clarke, Miss 'l'aylor, 
Messrs. R A. Matthews, A. Thomas, and J. A. Matthews. 
Instrumental solos, &e., were played by Mr. Poole (flute) ; 
Mr. Hooper (harmonium), and Mr. Matthews (pianoforte). 
The Concert was highly successful. 
Mancurster.—Mr. Yarwood’s Concert, given 
at the Free Trade Hall, on the 9th ult. was fully and 
fashionably attended, most of the patrons being present. 
Miss Chadwick was well received in all her vocal solos 
(being twice encored), and Mr. Carlos Lovatt and Miss! 
Henderson were also highly successful, A feature in the! 
programme was the Part-song, “Gently sighs” (the words 
and music by the concert-giver), which was sung by the 
several glee parties combined, with the utmost effect, and 
most enthusiastically received. Solos were given by 
Herr Otto Bernhardt (violin), and Mr. Avison (violon- 
cello; and the Lancashire Bell-ringers performed some 
selections, which were warmly applauded.——Tue An- 
nual performance of the Messiah, on the 24th December, 
was unquestionably the finest given in Manchester for 
many years. ‘The principal vocalists were Madame Lem- 
mens-Sherrington, Madame Patey-Whytock, Mr. Sims| 
Reeves, and Mr. Santley. Mr. Sims Reeves sang through- | 
out the Oratorio as only he can sing; the intensity and} 
pathos which he threw into the recitatives and airs de- 
scriptive of the sorrows of the Redeemer, and the power | 
with which he gave the energetic song, “ Thou shalt dash| 
them,” forming a lesson of legitimate vocalization, which | 
should be taken to heart by all young singers. All the| 
principal vocalists were also in excellent voice, and sang! 
their very best on the occasion, The fourth of the series | 
of Concerts by the Manchester Vocal Society was given | 
on the 19th ult., with much success. Several part-songs | 
were given with good effect; one by Mr. W. J. Young, 
“Come let us be merry,” being especially well received. 
The principal solo vocalists were Miss Fanny Henderson, 
Miss Tomlinson, Mr. W. Dumville, &c., all of whom gave 
the utmost satisfaction in the music allotted to them

Marztzorovau.—The Concert which forms 
the concluding scene of the “ Christmas half,” at Marl- 
borough College, was this year unusually successful. The 
programme was well selected; and contained several) 
glees and part-songs, which were excellently rendered. 
“The Welcome,” (words by a Master of the College, and 
music by Mr. Bambridge), was most effectively given ; 
and the successful teaching of Mr. Bambridge was amply 
proved by the artistic pianoforte performance of no less 
than six of his pupils. A great feature of the Concert 
was the selection from Beethoven’s Sonata Op. 2, No. 1. 
which was played with true musical feeling by Mr. Bam- 
bridge. The performance concluded, as usual, with the 
holiday song, ‘ Carmen Marlburiense,” which was sung 
by the whole school.—A selection of Christmas Carols 
formed a pleasing portion of an entertainment given in 
this town, on the evening after the College Concert. The 
training of the choir, under Mr. Bambridge (who also 
composed several of the carols which were sung), was 
the theme of general admiration

Maryrort.—The Annual Charity Concert and 
Ball took place in the Music Hall, on Tuesday evening, the 
Sth ult., and was in every respect a great success. The 
music, as usual, was under the direction of the Messrs. 
Graham, assisted by Messrs. Scott, Buttifant, and Barber 
{of the Cathedral Choir), Mr. Casson (Wigton), and Mr. 
H. Thompson (Harrington). The programme contained 
songs, glees, and instrumental pieces, all of which were

Roystoy.—The Amateur Musical Society held 
its 26th open meeting, on the 26th Dec., in the Hall of 
the Institute. Amongst the instrumental compositions 
performed were Haydn’s “ Kinder Symphonie,” an organ 
solo by the conductor, Dr. Garrett, and Mozart’s Sonata 
in E flat, for pianoforte and violin. The vocal music 
comprised Spohr’s 84th Psalm, a selection from Haydn’s 
Mass, No. 1, and several part-songs. All the pieces were 
well rendered; and proved that the Society is making 
steady progress

Sartspury.—Two Concerts were lately given 
by the Amateur Orchestral Union, at the Assembly 
Rooms, which were fully and fashionably attended. The 
programme contained many excellent instrumental works, 
amongst which were the overtures to Lgmont, Melusine, 
Hebrides, &e., Beethoven’s Symphony in I, (No. 8), and 
Mendelssohn’s violin concerto. One of the principal 
pieces in the vocal selection was Weber’s ‘‘ Ocean, thou 
mighty monster,” which was excellently rendered. Solos 
on the pianoforte, by Mr. De Windt, and on the harmo- 
nium, by Mrs. De Windt, were also much admired. The 
band of the Union contained several well-known ama- 
teurs, many of whom are members of the Wandering 
Minstrels, and other Societies. The Concert was given in 
aid of the funds of the Diocesan Choral Association

SHEFFIELD.—The Sheffield Choral Union gave 
the first of the series of performances for this season on 
Saturday, the 26th Dec., in the Music Hall. The 
Oratorio on this occasion was the Messiah. The principal 
vocalists were Miss Helena Walker, Miss Phillips, Mr. 
Robson, and Mr. Briggs. The solos were well sung; and 
the choruses given with much precision and energy. Mr. 
W. Stubbs most ably conducted, and Mr. Phillips pre- 
sided at the organ. The concert was a great success. 
—Tue St. Stephen’s annual gathering was held in the 
Temperance Hall, on the 29th December. The choir was 
assisted by Miss Harrison, Mrs. House, Messrs. Kay, 
Hattersley, and Styring. A selection of songs and glees 
was effectively given; and a lecture, by the Rev. J. 
Burbidge, was exceedingly well received, On the 18th 
ult., the Sheffield Amateur Harmonic Society gave a 
selecti-> from the works of Dr. Sterndale Bennett, as a 
complimentary concert to, and in the presence of, that 
eminent composer. The Woman of Samaria was the 
chief item in the programme




DURING THE LAST MONTH, 


KORO 


COC AWS


XUM


681


OULE’S COLLECTION OF 527 CHANTS, 57 


T= PSALTER, PROPER PSALMS, HYMNS 


01


OULE’S DIRECTORIUM CHORI ANGLI- 


OULE’S DIRECTORIUM CHORI ANGLI- 


HE ORDER FOR THE HOLY COMMUNION, 


HE ANGLICAN CHORAL SERVICE BOOK, 


ULE


J BAPTISTE CALKIN’S EASY MORNING 


USELEY


AND MONK’S PSALTER AND 


NOW READY


EDITED BY 


AND 


DWARD HERBERT’S CHANT TE DEUM, 


MUSIC AND MUSICAL INSTRUMENTS.—FEBRUARY SALE. 


IMPORTANT NOTICE TO MUSICAL PROFESSORS AND 


GFF ASHWORTH’S NEW PATENT LOOPED


AUTHORISED EDITION FOR RUGBY SCHOOL. 


HE ELEMENTS OF MUSIC SYSTEMATI


IVE ME THE JOVIAL WINTER TIME. 


)HN- 


, ELISE


683


UPPLEMENTAL HYMN AND TUNE BOOK, 


TUNES AND CHORALES


HE


TONIC SOL-FA EDITION


NEW WORK FOR THE ORGAN


NOW READY, ‘PART ONE OF THE 


EDITED BY WM. SPARK, MUS. DOC


ORGANIST OF THE TOWN HALL, &., LEEDS


THE


CONTENTS OF PART I


RUDIMENTARY MANUAL EXERCISES


NEAR THE CLOSE. 


TOUCH, 4S WELL AS THE MODE OF FINGERING REQUISITE IN 


CERTAIN PASSAGES, IN ORDER TO SUSTAIN THE SOUND DULY


ENDINGS, 


1615.) 65. ALLEGRETTO PASTOR- 


END OF THE FIRST PART


XUM


CED


L615


RGAN 


[TE IN 


DULY


TAVES. 


1008


30


DASTOR- 


XUM


685


NEW WORK FOR THE ORGAN


W. T. BEST


ASCENDING OR DESCENDING A DEGREE


ASCENDING AND DESCENDING SCALE PAS- 


SAGES FROM A GIVEN SOUND, IN ORDER TO 


ACQUIRE CERTAINTY IN PLAYING THE 


VARIOUS INTERVALS THROUGHOUT THE 


ENTIRE RANGE OF THE PEDAL-BOARD


THREE EXERCISES OF GREATER DIFFICULTY, 


ON AN ASCENDING OR DESCENDING DE- 


GREE, IN VARIOUS PARTS OF THE SCALE


THIRD, INCLUDING AN EXAMPLE FROM 


VARIOUS KEYS


REE EXERCISES FOR THE USE OF THE 


ARRANGED FOR THE ORGAN. 


CONTENTS. 


SHEWING HOW PASSAGES OF SPECIAL 


DIFFICULTY ARE TO BE PLAYED


THE SAME NOTE


LONDON: NOVELLO, EWER AND CO., 1, BERNERS STREET (W.) AND, 35, POULTRY (£.C


MUSIC FOR THE COMING SEASON


TTWOOD, T.—ENTER NOT INTO JUDGMENT. 


YATTISHILL, J.—CALL TO REMEMBRANCE. 


EASED- 


OLCK, OSCAR.—O LORD, HOW LONG WILT THOU FOR- 


ALKIN, J. BAPTISTE.—OUT OF THE DEEP HAVE I 


— THE SACRIFICES OF GOD ARE A BROKEN SPIRIT, 


IEMER, P. H.—HE WAS DESPISED AND REJECTED. 


ARRANT, R.—CALUL TO REY 


G OSS, JOHN.—COME AND LET US RETURN UNTO THE 


OUNOD, CHARLES.—THE SEVEN WORDS OF OUR


OPKINS, E. J.—IN MY DISTRESS I CRIED UNTO THE


RONS, HERBERT S.—SHEW THY SERVANT THE LIGHT 


IFIOURS, B.—IN THEE, O LORD, HAVE I PUT MY TRUST. 


— T. A—PONDER MY WORDS, O LORD. 


\ INTER.—HEAR MY PRAYER, O LORD. 


| ENT.—WHO IS THIS THAT COMETH FROM EDOM? 


\ FACFARREN, G. AA—HOSANNA TO THE SON OF DAVID. 


ONK, W. H.—THE IMPROPERIA, OR REPROACHES. 


RUST


LORD. 


MENT. 


FROM 


EDOM


DAVID


SALVA- 


28, 18. 


)ACHES


XUM


687


LLEN, G. B.—NOW IS CHRIST RISEN FROM THE DEAD. 


ARNBY, J.—AS WE HAVE BORNE THE IMAGE OF THE 


EST, W. T.—J&SUS CHRIST IS RISEN TO-DAY, ALLE- 


LARKE, J. HAMILTON.—CHRIST IS RISEN FROM THE 


OUNOD, CHARLES.—BLESSED IS HE WHO COMETH IN 


OPKINS, J. L.—LIFT UP YOUR HEADS. 


OPKINS, E. J—WHY SEEK YE THE LIVING AMONG


THE LORD IS MY STRENGTH. 


ITTMAN —THE LORD IS KING. 


EBBE.—CHRIST BEING RAISED FROM THE DEAD. 


BREITKOPF AND HARTEL’S EDITION. 


MOROAOCSCAACOHR


BREITKOPF AND HARTEL'S EDITION. 


[{RANZ SCHUBERT’S PIANOFORTE COM- 


ST. JAMES’S HALL


ORATORIO CONCERTS


CONDUCTOR, MR. JOSEPH BARNBY


FRIDAY, FEBRUARY 5


HANDEL’S JEPHTHA


MISS BANKS. MDLLE. DRASDIL. 


THURSDAY, FEBRUARY 25, WEDNESDAY, APRIL 21


MENDELSSOHN’S ELIJAH. HAYDN’S SREATION. 


MDME. RUDERSDOREFF. MDLLE, DRASDIL. MADAME LEMMENS-SHERRINGTON. 


MR. SIMS REEVES. MR. SIMS REEVES, 


HOLY WEEK PERFORMANCE OF MENDELSSOHN’S LOBGESANG 


HANDEL’S MESSIAH. : AND 


MISS JULIA ELTON. MADAME RUDERSDORFF, MDLLE. DRASDIL


WEDNESDAY, JUNE 9, 


MENDELSSOHN’S ST. PAUL. 


MADAME LEMMENS-SHERRINGTON, MDLLE. DRASDIL. 