


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 FOUNDED 1844 


 PUBLISHED MONTH 


 ROYAL CHORAL SOCIETY . ROYAL COLLEGE MUSIC 


 VICTORIA EMBANKMENT , E.C.4 


 FACULTY MUSIC 


 PRESIDENT 1926 : HARVEY GRACE , F.R.C.O 


 1926 


 96 & 95 , WIMPOLE STREET , W.1 


 TOBIAS MATTHAY 


 PIANOFORTE SCHOOL 


 COMPLETE TRAINING COURSE TEACHERS . 


 ROYAL 


 MANCHESTER COLLEGE 


 MUSIC . 


 ADOLPH BRODSKY 


 STANLEY WITHERS , M.A 


 GLASGOW 


 ATHENAUM SCHOOL MUSIC 


 SESSION 1926 - 27 


 MANCHESTER SCHOOL MUSIC . 


 16 , ALBERT SQUARE 


 LONDON SCHOOL SINGING 


 REAL USE 


 RELAXATION 


 ENTIRELY UNIQUE CORRESPONDENCE COURSE 12 


 * * MODERN TEACHING APPLICATION WEIGH ’ 


 CHARLES A. GILLETT 


 MOLTON STREET 


 BIRMINGHAM & MIDLAND INSTITUTE 


 SCHOOL MUSIC 


 SESSION 1925 - 1926 . 


 NATIONAL ASSOCIATION COMPET , TIVE 


 CHOIRS 


 PRESIDENT 


 1100 SPECIMEN WORKINGS . 


 RECENT SUCCESSES , 


 DOF 12 


 


 TIONS 


 


 MUSICAL T 


 593 


 SINGING - CLASS CIRCULAR 


 JULY 1 1926 


 REFLECTIONS WORK 


 SCRIABIN 


 SOMEIt owing illogical application 
 half - truths Scriabin muddled reasoning ran 
 destruction . Half - truths 
 danger ill - balanced minds 
 half - truths . - balanced mind sees exactly 
 dividing line truth falsity 
 lies . difficult distinguish true 
 false statement man bronzed 
 sun , copper - coloured moon , 
 freckled stars . difficult 
 disentangle true false reasoning old 
 lady ( probably old gentleman ) , , seeing 
 writing advertisements sky , 
 exclaimed : “ Look , wireless 
 messages caught fire . ’ muddle - headed 
 reasoning good fun farce , 
 dangerous offered seriously . 
 Scriabin offered muddle - headed reasoning 
 seriously value later work 
 suspected 

 Metaphorically , speak colour music . 
 , , D flat major dark - brown , 
 reason saying music 
 march * Rheingold ’ slow move- 
 ment Beethoven Sonata F minor ( Op . 57 ) 
 formed minds connection 
 D flat major darkness . music 
 played C major sound 
 dark . Similarly think G major 
 light green fresh spontaneous 
 tunes know key . 
 exactly colour transposed 
 flat major G flat major . primarily 
 question association . people aver 
 colours certain common chords 
 played . ‘ red B flat played , 
 ‘ God Save King ’ played 
 |B major people red 

 Scriabin , accepting half - truth 
 ‘ truth , built ‘ Prometheus ’ assumption 
 |that music definite colours , 
 novelist , prove sense architecture , 
 | write chapters form Norman 
 arches , mouse ‘ Alice - - Wonderland ’ 
 /told history pictorial appearance tail . 
 Scriabin reasoning illogical , 
 | question colour , little moment , 
 | affected work , * Prometheus , ’ 
 |but grave importance applied 
 question harmony possible 
 expansion , reduced later works 




 594 


 1926 


 596 


 1926 


 C NATURAL B SHARP 


 1926 : 597 


 PERSONALITIES MUSICAL 


 CRITICS 


 VI.—-ROBIN LEGGE 


 RECOLLECTIONS BRAHMS 


 ( ILONA EIBENSCHUTZrecitals . months , Frau Schumann 
 gave musical party house , 

 following programme : 
 Beethoven Sonata , Op . III Ilona Eibenschiitz 
 Song Julius Stockhausen 

 Schumann Carnival Clara Schumann 

 musicians Vienna , — Brahms 

 played Etudes Symphoniques 
 Schumann , smaller pieces Brahms 
 Scarlatti , Prof. Anton Door , vice- 
 president Tonkiinstlerverein , came 
 letter tell audience Herr Griitzmacher 
 sent word minute 
 unable play . Brahms came , said : 
 “ Na , da spielen Sie doch mal die Op . 111 von 
 Beethoven . ’ ( , play 
 Op . 111 Beethoven 

 , believe henceforward Brahms 
 friend . supper , concert , 
 sit . wished know 
 plans , Frau Schumann wanted 
 . told arranged Mr. Arthur 
 Chappell engage Monday Popular 
 Concerts London , going 
 London time 




 1926 599was happy . Ischl 
 days , afternoon Brahms paid visit . 
 great moment , felt 
 happy . tell London ; 
 interested 

 March 2 , 1891 , G major String Quintet 
 performed : time Joachim , 
 Piatti , Ries , Strauss , Gibson , Monday 
 ‘ Pop . ’ concert played B flat 
 Trio , Op . 97 , Beethoven , Joachim 
 Piatti , heard rehearsals 
 Quintet . told Brahms care- 
 fully Joachim rehearsed Quintet , 
 splendid reception met , pleased 

 days later Brahms came dinner 
 ( Mittagessen , o’clock ) , soon 
 habit come nearly week 
 dinner . liked 
 fuss , asked 
 anybody ‘ meet Brahms . ’ Mittagessen 
 simple . beginning aiways 
 came , later asked 
 bring Prof. Gustav Wendt , old friend 
 Karlsruhe , came summer Ischl 
 time Brahms . ‘ came 
 Prof. Késsler , Budapest , liked 
 . brought friends 
 afternoons , Fritz Steinbach 
 wife , Adolf Menzel , Stockhausen , & c 

 state 

 material bat , speak . 
 adoption folk - tunes 
 thematic bases years ago , 
 ‘ original ’ themes taken place 
 little tags ¢/chés 
 folk - tunes , worse olf 
 better . building national 
 school composition ? , surely 
 built . true Haydn 
 Beethoven ( especially ) use 
 folk - tunes occasionally instrumental music , 
 said work 
 occasional hint idiom 
 folk - song ? folk - idiom Bach 
 Wagner , save purposes local 
 deny searching beauty 
 “ Banks Green 
 Rhapsody 

 writing 




 602 


 1926 


 11592 


 NEW LIGHT LATE TUDOR COMPOSERS 


 MUSIC TOLSTOY LIFEsaw 

 father relation Beethoven somewhat 
 complex . composer — apart period 
 produced strong impression . 
 ‘ Boyhood ’ describes effect produced 
 ‘ Pathetic ’ Sonata hero ; ‘ Family Happiness 
 refers lovingly ‘ Moonlight ’ Sonata ; 
 ‘ Kreutzer Sonata ’ borrows title 
 Beethoven work ; ‘ Appassionata ’ excited 
 ; finally , played Beethoven 
 Sonatas orchestral - handed ) . 
 demanding , instance cases , 
 critical attitude accepted idols , 
 protested exceptional cult Beethoven , 
 considering predecessors Haydn Mozart 
 equal superior . considered 
 Beethoven culminator period 
 music greatest development , forefather 
 decline ull remembe : 
 artist genius 
 gives works merely new content , 
 form ; Haydn created forms sonata 
 symphony , Mozart opera , 
 existing forms 
 Mozart 
 ought 

 
 judgment 
 distinct 
 

 saying 
 new 

 created 
 Haydn 
 , asked 
 consider Beethoven yvenius . 
 father mistaken 
 Beethoven . symphonic forms 
 predecessors 
 considered new forms . Beethoven construction 
 broader freer ; presents new 
 Scherzo ) , true genius 

 new forms , fully applicable 

 , father considered 
 comprehensible comparatively 
 small number people , listener 
 somewhat pamper music spoil 
 taste understand Beethoven , 
 artificial 

 Wagner , Liszt ( exception 
 arrangements ) , Berlioz , Brahms , Richard Strauss , 
 modern composers , 
 father great liking . * accepted 
 Grieg pieces . true 
 - acquainted newer composers , 

 Beethoven 
 

 previously 
 extent 
 
 
 

 

 Y * 
 creates m 
 Beethoven 

 Beethoven 

 ordet 




 1926 605certain musical 
 understanding Chopin . 
 ‘ probably taste spoilt 

 addition said 
 Beethoven , remember father said 
 Sonata G major ( Op . 14 , . 2 ) , 

 j hears , , cenversation 
 husband wife , general toy 
 sonata . Sonata E flat major ( Op . 
 considered 77ze Scherzo ( E flat minor 




 ‘ FALSTAFF ’ 


 JOHN W. KLEIN 


 VERDI 


 1926 


 _ — _ 


 HANDEL USE UNCOMMON 


 INSTRUMENTS COMBINATIONS 


 1725 


 609 


 510 


 CONSECUTIVE FIFTHS 


 SCHUBERT 


  — — _ — _ Hadow 

 Seeley , Service & Co. , 5s . vol . | 
 * Beethoven Op . 15 Quartets Sir W H 
 Hadow . 
 { Oxford University Press , | ! 
 \ ( omparison Poetry Musi Henry 
 Sidgwick Lecture , 1925 Sir W. H. Hadow 
 [ Cambridge University Press , 25 
 volumes Studies , n 
 eleventh edition , long past review stage . 

 retain piace examples 

 Henry care pinned 

 early opinions . , conditions affecting 
 contact general publi ith music 
 undergone profound changes , espe lally 
 past years . pa e 
 reads curiously - day 
 \ generation ago Englishmen played piano 
 forte non - existent , Englishwomen 
 nded education * Batt Prague . ’ 
 amateur level thi ntry 
 high , ind vet little « e ot lam liartsing 
 Beethoven Schubert , bringing 
 firesides , admitting friend 
 ship . ts , course , prin ipal nm good 
 Art neglected . appreciate best musi 
 hear : hear liv 
 : live mmpany 

 whem played ng 

 Frederick Chopin Dvorak ; + Johannes 

 Brahms . 
 little book Beethoven Op 18 Quartets 

 

 takes miniature score 

 rule good deal 
 Beethoven methods ( Op . 16 , 
 ) obtained fat 

 volume 




 H.Gbeen - written enlarged . ‘ Studies ’ 

 Bach , Cherubini , Beethoven , Meyerbeer , ¢ hopin , 
 Mendelssohn , Parry , Reger ; ‘ Caprices ’ play 
 round ‘ Creed Custom , ’ ‘ Going 

 Scenes , ’ ‘ Negative Music , ’ ‘ Slender Reputa 

 . 

 volumes 
 amateur ; Mr. 
 sense musical 
 feel liberty fall 
 present writer , example , 
 blind Beethoven - worshipper , agree 
 Beethoven , Mr. Brent 
 Smith says , ‘ undeniably dull moments ’ ; 

 estimate compose 

 fault usually causes set 
 forth \re magnificent 
 : surely characteristics Beethoven 

 initial inspirations 

 glint hasty safety - pin 

 true immature Beethoven 

 surely Beethoven best . 
 find mastery construction texture 
 look ? 
 \mong failings set ‘ trite 
 codas . ” , ‘ 

 thought 
 Beethoven g fair add 
 brief quotations set glowing 

 finest music , 
 point 
 interminable 




 1865 ourse , greatest mus 

 course ’ good ' page 
 irgued Brahms chamber - music improve 
 ment Beethoven , page bid 

 farewell , ‘ clothed mantle Johann 




 BS1 collapse cis 
 viacial aye 

 heart book 
 beethoven music future . 
 musi sull 
 pessimistic world 
 ruins theological 

 declaration 
 Beethoven 
 contains 
 fringe appreciating 

 - day , littered 

 sociological hope s , star Beethoven spirit 

 W. J 

 shines . sudden author grows 
 sympathetic . felt , 
 expresses far better . 
 | praise Beethoven springs fine poetic idea , 
 sake 1s ine lined excuse good 
 author hateful denigrations . ends 
 | chapter ( p. 88 

 question , ‘ 

 live ? ’ answered , reply 

 Beethoven rendered ridiculous 

 Years Army Lieut.-Col 




 ORGAN MUSIK 


 616 


 — _ = — _ 


 ~@-?_@ = _ — 


 MUSICAL 


 


 BORWICK TRANSCRIPTIONS 


 1926 


 G. G 


 CHORAL MUSIC : UNISON 


 - SONGS CHILDREN FEMALI 


 VOICES 


 MALE - VOICK 


 MIXED - VOICI 


 PIANOFORTI 


 620 


 1926 621 


 EASY 


 PIANOFORTE MUSIC 


 CHAMBER MUSIC 


 B. V 


 VIOLIN 


 1926 


 CELLO 


 1892 


 FOLK - DANCES BRASS BAND 


 SCORES 


 IMPRESSIONS MUSIC 


 DOMINIONS 


 ALTO 


 TENOR 


 - SONG 


 LS = = 


 _ — _ = = = = ~ = = 


 — _ — F — | | 4 


 = = F = = : = = — CS 


 4 -38—5 — _ — — — — 


 5 — 5 = — — — — — — S — S — 


 LOVE WAKES 


 1 7 


 1926 629 


 COLUMBIA 


 H.M best Russians mere dabblers 

 Kreisler usually gives gramophonists trifles , 
 Beethoven Gavotte 
 Bach Minuet . ‘ True , transcriptions , 
 splendidly 
 played , despite heavy handling 
 little Bach piece ( DA777 

 Sharp recorded tn Palmgren ‘ Rococo 
 folk - tune ‘ Gentle Maiden , ’ arranged 
 




 E424 


 D 1087THE MUSICAL TIMES — JuLy 1 : 1926 6351 

 Tito Schipa , sings arrangement Liszt Beethoven Variations Theme , 
 yackneyed * Liebestraiime ’ poor ‘ Ave Maria’|*Tandeln und Scherzen , ’ heard , 
 ot perpetration ( DB873 apparent reason — 
 complete form , events . Ethel Leginska 
 VOUCALION playing 1s , like work , unequal . repeated 
 : chord accompaniment far heavy . 
 \ ery appropriately , view Weber centenary , : . " e oth 
 * } | hand , crisp playing delightful fughetta 
 omes capital record Overture ‘ Euryanthe , . : ; 
 : é - rate . Variation reminder fact 
 played Lolian Orchestra , conducted Stanley ; 
 4 ote ; , 4 ’ | Beethoven wrote vood fugues , 
 Chapple . ‘ recording ‘ new ’ type , : ‘ PF , 
 capital hand neat lively fughetta 

 great power ; reproduction tone 




 KOLITAN 


 OPERA WIRELESS 


 OPERAS BETTER HEARD SEENrelapse sobrie 

 Johannesburghers ‘ moving secure 
 exciting time veat witha Beethoven 
 Festival example organized energy 

 city home copy 




 


 HANDEL FESTIVAL 


 1 1926 635 


 YFORD : HITHERTO UNNOTICED 


 CATALOGUES EARLY MI 


 PI 


 1953 


 1659 - 65 


 1620 


 1612 ( B.M 


 1O 


 . 20 


 CATALOGUE 


 


 MUSIC K+*BOOKES 


 DD 


 1655 


 420 ) \ 


 16050 


 640 


 1926 


 ORCHESTRAL NOTATION 


 MUSICAL 


 1926 641 


 SES SSS SES — S _ 


 0 0 0 


 SS 


 642 


 MUSICAL T 


 ROYAL COLLEGE ORGANISTS 


 ANNUAL GENERAL MEETING 


 DISTRIBUTION DIPLOMAS 


 FELLOWSHIP 


 ASSOCIATESHIP 


 CINEMA ORGANIST 


 H. W. 


 1927 


 MUSICAL T 


 CHURCH MUSI « QUETTA 


 FOUNDLING HOSPITAL : FINAL SERVICE 


 RECITALSSonata . 6 , J / endels Larghetto ( Symphony 

 D ) , Beethoven ; Fugue G minor , Bach ; Imperial 
 March , £ /gar 

 Mr. H. E. Knott , St. 
 Psalm - Prelude . 2 , 
 * Lovely , ’ Vaughan William 
 Epilogue , Healey Willan 




 APPOINTMENTS 


 _ B&B 


 MAINZER MUSICAL TIMES 


 UNIFORM PITCH 


 BALFE : CORRECTION 


 1926 645 


 IRISH HORSE GUARDS ’ DRUM 


 SECCOMBE 


 RUSKIN 


 ND 


 MUSIC 


 WHITE BOUGHTON BAINES 


 SIRws denied ordinary sources musical education 

 circumstances sympathise . 
 study 
 pleased classics , masters , great , 
 ind admiration Beethoven Bach truly profound 

 Mr. White concludes Mr. Boughton article 
 Baines * young man considerable natural talent , ’ 
 ind opines England * thousand 
 natural talent equal 
 Boughton , article , use 




 LAUGHTON 


 CENTENARY WEBER 


 RIPON 


 LATE DR . E. J. CROW , 


 TENOR _ VOICE THUNDER , 


 


 MUSICAL T 


 1926 


 ROYAL ACADEMY MUSIC 


 ROYAL COLLEGE MUSIC 


 OPEN SCHOLARSHIPS , 1920 


 1926 647 


 TRINITY COLLEGE MUSIC 


 CONCERT 


 HAROL ! 


 SAMUEI 


 SECOND CONCERT 


 G. THALBEN - BAL 


 JELLY ARANYI HAROLD SAMUEI 


 CONCERT 


 648 MUSICAL 


 FOURTH CONCERT 


 STEVART 


 FOLKS 


 THI 


 COLLINS 


 HOME ’ : ¢ 


 COMPOSER 


 OLD ENTENARY 


 STEPHEN 1826 


 STEPHEN C , FOSTER , 


 W. H. GRATTAN FLOOD 


 ENGLISH FOLK - DANCE 


 SOCIETY 


 FESTIVAL 


 1542 


 1926 649 


 SINGERS MONTHShe possibilities good voice , 
 technique style faults . py , 7 , K , 
 PIANISTS MONTH , 
 Moritz Rosenthal ( ueen Hall gave _ strictly 

 regulation programme . unnecessary refer 
 1is technical powers pianist , 
 striking characteristics capacity preserve 
 note rapid passages . 
 played Beethoven Sonata C Ill , 
 Chopin Sonata , Op 58 , complete understanding 
 programme concluded brilliantly 

 clearness 




 650 


 1926 


 PASDELOUP ORCHESTRA 


 MR . WARLOCK SONGS 


 ENGLAND WALES 


 HASTINGS , 


 15 , 17 , 18 , 


 ABI 


 14 , 


 108 : 


 LAY 


 SCOTLAND 


 651 


 BENFIELDSIDE , 


 BRIGHTONElgar 

 HARROGATI summer symphony concerts 
 progress able direction Mr. Basil Cameron . 
 Beethoven Dvorak ‘ New World ’ given 

 20 June 2 




 LEED B minor repeated 
 infl 
 promised season definitely 

 quarters . Beethoven 

 January 27 , March 10 Bantock new ‘ Song 
 Songs ’ receive Lancashire hearing , Miss 
 Caroline Hatchard Messrs. Mullings Allin 
 solists Messiah , ’ December 23 24 , com 
 pletes probably prove memorable 

 C , odowsky 

 Suggia , Cassado , Catterall , William Primrose 
 included string soloists . Formichi sing 
 final concert March , hope 
 programme publicity announcements observe 
 season proportion singer 
 symphony casts Berlioz , Bach , Handel , 
 Beethoven choral concerts _ perfect models 
 efficiency Wagner concert given : 
 soloists appear time Messrs. Roy 
 Henderson Gaspar Cassado , leading 
 members Orchestra _ soloists — 
 Messrs. Alfred Barker Clyde Twelvetrees . 
 Oxrorp.—Mrs . E. S. Coolidge , famous American 
 patron music , gave chamber concert - Sheldonian 

 end term , 
 ‘ Canticle Sun 




 YORK , 


 GERMANY 


 METROPOLITAN CAST 


 BADEN - BADEN 


 _ — _ 


 TI 


 


 1926 653 


 POST - WAGNERIAN OPERA WEIMAR 


 PAUL WEISSMANN 


 TORONTO 


 VIENNA 


 654 


 1926 


 \ MOZART PREMIERE 


 WEBER CENTENARY 


 PAUL BECHER 


 R.A. 


 MUSICAL 


 1926 


 655 


 CONTENTS 


 625 


 MONTH . 


 B * WER , HERBERT 


 RR , ALAN.—‘“‘O 


 LI 


 , ; LETCHER , PERCY | } 


 1142 


 HENR 


 * CHOOL MUSIC REVIEWIsland , ” Percy E. FLETCHER 

 forte , BEETHOVEN . 2d 

 CHOOL SONGS.—Published intwo forms . 




 SCHUBERT 


 PUBLISHED 


 H. W. 


 S ' IWERBY , L. 


 


 GRAY CO . , NEW YORK 


 


 SONGS 


 ARRANGED 


 SEA 


 PIANOFORTE 


 DUET 


 


 GEOFFREY SHAW 


 1926 


 FOYLE MUSIC DEP 


 TENDERS 


 CITY MELBOURNE ( AUSTRALIA 


 SUPPLY GRAND ORGAN 


 HALI 


 MELBOURNE TOWN 


 DELIVERY , ERECTION GRAND ORGAN 


 TOWN HALL , MELBOURNE 


 SUPPLY 


 1926 


 MUSICAL TIMES 


 CHARGES 


 ADVERTISEMENTS 


 SPECIAL NOTICE 


 W.1