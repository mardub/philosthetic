


 24 . 


 24 , 


 44 , 


 MUSICAL time 


 NOVEMBER 1 , 1869 


 music number . 


 high time awake 


 ANDREW , WELLS STREET.—A TENOR 


 N evening SHAKSPEARE — 


 professional notice 


 MR . JOHN GOSS 


 MISS HELENA WALKER 


 lishe good style , moderate 


 UTLER MUSICAL instrument — 


 CORNETS , SAXHORNS , DRUMS , FLUTES , CLARIO- 


 EYOND ALLCOMPETITION.—T. R. W 


 RASS , REED , STRING , DRUM 


 W. SNELL improve HARMO . 


 MADAME MONTSERRAT 


 MR . EDWARD CRAIG 


 ROGER CELEBRATED GUINEA CON- 


 mportant NOTICE.—THOMAS CAMP- 


 FEFEGEEE 


 SHWORTH new patent " loop 


 _ 7 — 1 


 HAMILTON PATENT union " musical notation , 


 edit 


 REV . ROBERT CORBET SINGLETON , M.A , 


 — , RADLEY , ' 


 organist choirmaster YORK MINSTER 


 TUNE . climate 


 good CHEAPEST drawing - room CHAPEL organ 


 offer . 


 thirty - thousand use . 


 seventy - gold SILVER medal 


 MASON & HAMLIN CABINET ORGANS 


 BREAVINCTON & SONS , 


 SOLE agent GREAT BRITAIN IRELAND . 


 309 , REGENT STREET , LONDON . 


 OW 


 OW D EDITION . 
 san METZLER CO . 
 
 ' ( ' D nals 
 Supplemental Hyman Cune Pook , new publication . 
 publish . 
 new appendix rr ) . ' . 
 ( ) \ otet FEMALE voice . Joux 
 ‘ 4 7 2OIWN vv 4 Hutian . price , 1 . net . . 1 . Lord Shep- 
 edit ROBERT BROW N - BORTH ° ICK , herd . . 2 . thy word Jantern . ' 
 INCUMBENT GRANGE , KESWICK . Metzler Co. , 37 , Great Marlborough - street , W. 
 dedicate , express permission , H.R.H. Vicrorta , Crown ero & Co. - song magazine . 
 Princess Prussia , Princess Royal England . 4 ) t ( voice Treble , Alto , Tenor , Bass . ) price 3d . 
 , ras . number . follow new number ready . 
 ind sanction Lord Bishop Worcester . . 13 PORTRAIT oi Elizabeth Stirling . 
 } _ 6d . past ind ) 6d . 14 song ZETLAND FISHERMEN UM . H. Bell . 
 = price € s. 6d . word slone ( bind ) 6d 15 FADED flower ... .... Elizabeth Stirling . 
 e opinion press . 66 > rt 
 gst Pall - Mall Gazette . e xx , te r h l l. 
 RT " Mr. Brown - Borthwick volume need apology = MAGAZINE SACRED MUSIC . 
 - issue small form present , assuredly require ] VOL . iv . ready , contain work following com- 
 und , enlargement add interest ] posers.—V. Gabriel , Smart , Elizabeth Philp , Macfarren , Barker 

 andvalue . * * * Mr. John Goss , Mr. E. J. Hopkins , Mr. J. Redhead , Allen , iB. Tours , Liebich , Beethoven , Mozart , & c. , & c. 
 Baptiste Calkin compose unison tune volume , | elegantly bind cloth , price 5 . post - free , 5 , 6d . 
 , exception , model kind . " Metzler Co. , 37 , Great Marlborough - street , W. 
 ) Choir Musicat Record . ry : 
 . , " heartily recommend collection gem con- e k bk r h l l 9 
 f nd Tune Boox . " } ° 
 taine iene ory oe VOL . V. ready , contain work follow Com 

 far interesting publication late year way | Posers , Mozart , Smart , Nava , Lefebure - W ely , Spohr , J. L. Hatton 




 XUMR . F. LUCAS , Music Sellers ’ accountant , 
 i\ 26 , Maddox - street , W.—All matter connexion 
 Trade , partnership , transfer Business , 
 arrangement , promptly carry 

 BEETHOVEN 

 cover , 2 . ; scarlet cloth , 3 




 NEW CHRISTMAS MOTET 


 O SING GOD ( NOEL 


 CH . GOUNOD 


 - prelude 


 t HYMN BOOKBook Common 

 Prayer . work follow celebrated Masters 
 Church Composers appear book , : — Mozart , Handel , 
 Haydn , Beethoven , Weber , Mendelssohn , Spohr , Dr. Clarke , Dr , 
 Croft , Dr. Howard , Wyvill , Dr. Randall , Lord Mornington , 
 J. Turle , Esq . , Rev. J. B. Dykes . Redhead , Dr. Greene , Wilson , 
 Courteville , Dardwell , Knapp , Madan , Holmes , Purcell , Lawes , 
 yibbon . Tallis , Webbe , Stanley , Coombs , Dr. Mason , Sir G. 
 Smart , Dr. Eivey , A. Reinagle , Bishops Ewing Turton , 

 Jones , 79 80 




 ORGANIST quarterly journal . 


 MUSICIAN , ORGANIST , 


 ND CHOIR 


 HOIR 


 10 


 ot 


 263 


 MUSICAL TIMES , 


 NOVEMBER 1 , 1869 


 management CATHEDRAL 


 CHOIRS 


 MOZART " DON GIOVANNI 


 fabulous event occur travel- 


 LING ENTHUSIAST 


 ravel 


 265 


 266 


 CRYSTAL PALACETue Saturday concert establishment , ( 
 look forward ' real interest 
 amateur professor ) commence 2nd ult 

 able direction Mr. Manns . principal 
 novelty bright overture Schubert , write 
 age 18 , little operetta , « Die beiden 
 freunde von Salamanka , " composition receive 
 favour graceful genial work 
 command , pro . 
 foundly impressed riper genius evince 
 composer later production . overture , ¢ 
 Beethoven , unknown , perform : 
 need scarcely , equal merit 
 great work class popular , fully 
 worthy reputation composer . instru . 
 mental portion programme concert haye 
 uniformly good ; regret Claribel 
 trashy ballad find place performance 
 profess educate , administer , public 
 taste . Saturday 23rd ult . Handel Acis ang 
 Galatea effect , principal 
 assign Madame Florence Lancia , Mr. G. Perren , 
 Mr. Montem Smith , Mr. Edward Connell 

 excellent series Monthly Popular 
 Concerts organise Mr. Ridley Prentice , 
 Angell Town Institution , Brixton . programme 
 select plan Monday Popular Con- 
 certs , St. James Hall . Messrs , H , 
 Blagrove Weist Hill announce violin , 
 Messrs. Amor Ralph second violin , Messrs , R. 
 Blagrove Burnett viola , Messrs. W. 4 . 
 Aylward W. Petitt violoncello , Mr. Ridley 
 Prentice pianist . favourite vocalist 
 engage ; enterprise promise deserve 
 success . concert place Thursday 
 21st ult 




 269 


 _ _ _ 


 anthem voice , compose 


 JOSEPH BARNBY . 


 verse 


 17 


 KH 


 1 — = — = — < b- 


 > $ 35 SSS 55 ss = . 


 = ! 4 ! ~ F 


 XUM 


 VERSE 


 275it indicative state church music pre- 
 sent day , find work un- 
 equal merit . frankly admit cireum- 
 stance far justify fear 
 previously speak , viz . , success 
 splendid service endanger failure 
 work specially write purpose 

 musician point view , 
 fate - open service Grantham Parish 
 Church , look vain gleam freshness 
 beauty specially compose Anthem . vapid in- 
 troduction compose wear - sequence , follow 
 chorus , somewhat remarkable score 
 accent modulation , . 
 example , " J , " 
 " open gate righteousness . " 
 chorus , triple time , find 
 " gate Lord . " 
 painful - mention want freshness . . 
 find 
 long ago . Haydn , Mozart , 
 Beethoven , Spohr , Mendelssohn existence 
 Dr. Dixon experience , shadow 
 influence discernible . Boyce , King 
 Travers appear author work 
 principal source inspiration 

 Mr. Thorne Anthem , hand , 
 remarkable work kind 
 ; hesitation add 
 opinion stand comparison 
 fine work ot class , freshness invention 
 masterly treatment . , like Anthem , open 
 instrumental introduction , bar 
 fail attract notice musician 
 , breadth dignity , daring use dis- 
 cord , introduction large outline , 
 chorus flow break , 
 subject use manner bold new . 
 second subject , ' ' foot shall stand thy gate , o 
 Jerusalem , " singularly graceful apposite , 
 counter - subject , " Jerusalem build city 
 unity , " present effect great novelty . 
 bar modulation 
 strike thing work , bring 
 second movement , soprano solo chorus : ' " o pray 
 peace Jerusalem . " difficult describe 
 word effect portion anthem . ' 
 prayerful dignity subject , novelty 
 second , word " peace , " 
 soprano voice , echo tenor , gorgeous 
 harmonization , render movement 
 discredit reputation 
 composer live . movement , " 
 brother companion ’ sake , " fall somewhat 
 short elevation attain previous 
 , far small work . appear 
 mind stretch state 
 tension entire Anthem . 
 regretable reason . seldom 
 music high class , intend Church use , 
 come notice , principal feeling 
 satisfaction find 
 likely emulate work good 
 old church composer 




 correspondent 


 dure month , 


 MUSIC INSTRUMENTS.—NOVEMBER sale . 


 OULE collection 527 chant , 57 


 DR . S. S. WESLEY new service . 


 PSALTER , PROPER psalm , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 ORDER HOLY COMMUNION , 


 ANGLICAN CHORAL SERVICE BOOK 


 USELEY MONK PSALTER 


 DWARD HERBERT CHANT TE DEUM , 


 USELEY.—A chant service 


 R. WESLEY new chant - service 


 BAPTISTE CALKIN EASY morning 


 erie " MODERN KYRIES . HYMN 


 compusition F. DAINTREE 


 real work cat hedral , 


 O morning SHINETH . " 


 TONIC SOL - FA edition . 


 ONGREGATIONAL chanting : free 


 ALEX . 8 . cooper 


 282 


 character represent 


 act ii . 


 time performance hour minute 


 W. H. BIRCH , 104 , LONDON STREET , read 


 - song 


 S.A.T.B. ) 


 compose 


 MENDELSSOHN 


 price shilling 


 97.~the SHEPHERD song 


 29.—the victor ’ return 


 31.—remembrance 


 34.—in forest 


 publish 


 NOVELLOS - SONG book 


 second serie 


 


 - song 


 S.A.T.B. ) 


 compose 


 CIRO PINSUTI 


 NEW PIANOFORTE MUSIC C. a. BARRY . editation FLOWER GARDEN , 
 Piano , E. Beswick , Pendleton , Manchester . post free 
 19 stamp 

 AUER COMPLETE edition 
 classic PIANO . Large 8vo . : — 
 BEETHOVEN.—79 Piano work , 2 vol . 12 . , singly : 
 Vol . i.—38 sonata , Portrait , Biography , Metronome , & c. 6s . 
 Vol . ii.—41 variation small piece , Piano duet . 6s . 
 Mozart.—63 Piano Works , complete 2 vol . 10 . , singly : 
 Vol . i.—22 sonata , Portrait , Biography , Metronome , & c. 5s . 
 Vol . i1.—41 variation small piece , 8 Piano duet . 5s . 
 Bacu.—Popular piece , finger . 2s . 
 Cuorin.—Complete , 10 waltz . 3s . 
 Freip.—Complete , 9 nocturne , 2s . 
 Scnupert.—Piano sonata , biography , & c. 
 ScuuMann Album , advice Young Musicians . 4s . 
 WeseER.—Complete , 23 piano work , Portrait , Biography , 
 & e. 5s . 
 Augener Co. , Beethoven House , 86 , Newgate - street , Fou- 
 bert’s - place , Regent - street , London ; Palace - place , Brighton . 
 MULLEN PIANOFORTE   INSTRUC- 
 e TIONS.—The easy publish . lesson strictly 
 progressive , teach beginning end . 

 invaluable teacher . price 4 . 
 London : B. Williams , 19 , Paternoster - row 




 N EW 


 53 


 T. ALBION ALDERSON 


 " TARANTELLA , " 


 NOVELLO 


 CHEAP OCTAVO EDITION 


 OFPIANOFORTE classic 

 BEETHOVEN thirty - sonata 
 BEETHOVEN thirty - miscellaneous piece 

 SCHUBERT sonata 
 SCHUBERT dance , complete 
 SCHUBERT piece 
 MOZART eighteen sonata 




 LONDON 


 NOVELLO , EWER CO.,1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 element MUSIC SYSTEMATI- 


 omposition BERTHOLD tour . 


 PIANOFORTE . 


 SONGS 


 3.0 


 anthem . 


 CRYSTAL WALTZES 


 J USIC MOURN plaintive accent . 


 ise soul stretch THY WINGSHuddersfield . London : Novello , Ewer Co 

 284 MUSICAL TIMES:.—Novemper 1 , 1869 . 
 OBERT COCKS Co. highly popular AY DREAM WALTZ . F. Darmzner . 
 MUSIC : — Hamilton Modern Instructions Piano , price 4s . 
 700th edition , 4 . ; Hamilton Modern Instructions Singing , B. Williams , Paternoster - row ; Author , Highbury College , 
 39th edition , 5 . ; Clarke Catechism Rudiments Music , ] y tha > ) . : 
 123rd edition , 1 . ; Hamilton Dictionary 3,500 musical term , n= ' - song , f ublishe , price 
 90th edition , 1 . ; Clarke Catechism Thorough Bass , 2 . ; Penny , 
 George F. West question relate theory Music , 1s . arise , love ove « . J. L. Hatton . 
 . publish day , grati postage | - Days wae « .. Henry Smart . 
 free , list new music Pianoforte voice . | Fairy Bells ... sve « . G. A. Macfarren . = e 
 NEW PIANOFORTE MUSIC.—Tenth edition ( gratis post-| ' ' ' Fairy Bells ' original effective : 
 age free ) ROBERT COCKS Co. catalogue song day . ”—The Choir Musical Record . 
 thousand WORKS PIANOFORTE , good Com- ' * Mr. Macfarren write ingenuity 
 poser , publish firm , New Burlington - street , effect . ”—Sunday Times . 
 London . pianist consult valuable catalogue . | London : F. Pitman , 20 , Paternoster - row . O 
 ig song voice , Piano publish , Second Edition , reduce 1 . 6d . , 
 . 1 . God save queen . edit Jonny Bisnor . MANUAL singing , use Choir 
 . 2 . God bless Prince Wales . BrixLey RICHARDS . trainer schoolmaster . RicHarD Mann , organist 
 . 3 . God bless Sailor Prince . STEPHEN GLOVER . g Abbey Church , Cirencester , & c. Contents:—The theory 
 post free stamp , seven stamp . music explain concise manner , use 
 GEORGE FORBES'3 new music FIANOFORTE . technical term possible , direction 
 Pleasant Dreams . 3s . Spring Showers . 3 , trainer mode teach ; series progressive exer- LOD 
 Tunes street . 4s . Fantasie ila Valse . 3s . cis , , voice ; exercise cultivation 2 D 
 Hark Goat Bells ring . Fantasie la Marche , 3s . voice ; list easy sacred secular music , & c. 31 
 Melody . H. Smarr . 3s . Fantasie hla Polka . 3s . | work offer clergy amateur choir trainer 4 . B 
 half price , extra stamp postage . ' systematic simple course instruction , bl 
 O fair DOVE ! 0 fond DOVE ! word Jean INcriow . teach pupil read music sight , comparatively short 
 music A.S. Garry . ' seldom meet time . London : Novello , Ewer Co , 6 M 
 beauty conceal apparently simple song . ”"—Vide Edinburgh ™ TIO SONGS ; 
 Courant . price 3 . order Muste - sellera . : | gerne s ART - SONGS . , use 1 . T 
 London : Robert Cocks Co. , New Burlington - street . Army , Navy , * Volunicers . edit J. Tiu.narp . 8 . Ss 
 Fact valle ’ oo Book e vilines Pade ton " ae , price Ong 9 . T 
 . " ~ ere os ENNY . Mired Voices ( S.A.T.B. 4 
 SARUM HYMNAL , Proper Tunes . ) _ ( God save queen , - 
 cloth board , red edge , gilt lettering , 5 . ; post . 5s.6d . 1 TheIsland .   ... oe « . Weber . DF 
 Music edit THEopoRE Epwarp AyLWarpb . dedicate , | England Heroes _ _ ... ove Reichardt . B.S 
 permission , right Rev. Walter Kerr , Lord Bishop Salis- 2 Ark Freedom + » Haydn , " Ti 
 bury , Precentor Province Canterbury . 3 Wellington ( blasen die Trompeten ? ) 
 1 , 2 , 3 , price 1 . ; post , 1s . 2d . . part4 , 4 british Grenadiers 16 . H 
 2s . ; post , 2s . 3d . large type edition , treble 5 Bonnie Dundee IE 
 Choirs , & c. , nearly ready . 6 { Emigrant Ship ... ots Gross . 18 . t 
 SARUM HYMNAL . Square 32mo . , cloth limp , 6d . ; cloth Song Songs eee « . , Methfessel . 19 . T 
 board , 8d . fine toned paper edition , cloth gilt , red edge , 1s . ; 7 March Men Harlech . 90 . 8 
 roan , . 3d . 4 large type edition , square 16mo . , cloth , 1s.2d . ; , 8 Yefree - bear son ... ... ove J. Tilleard . 
 cloth board , 1 . 8d . 9 Dunois brave ( Partant pour la Syric ) 
 clergyman send post - office order BrownandCo . , 10 rule , Britannia co ove Dr. Arne . ‘ ¢ 
 100 copy Sixpenny Edition rate 4d . copy ; andthe 11 danish Nati nal Song ove « . Hartmann 
 large type edition . dozen 11 . 14 , respectively . | M : n Voices ( a. T.B.B. ) , mark . 
 Salisbury : Brown Co. , W. P. Aylward . London : Simp- wef God save Queen . 
 kin , Marshall Cu . , Metzler Co. — — * oe Oe ; oe Reichardt . 
 — — tr WEST ete = — — — - n 13 Wellington ( blasen die Trompeten ? ) 
 CAPTIVITY.—F. Howe Oratorio , 14 British Volunteer ne ' « . Swiss Volkslied . 
 perform Birmingham , & c. , publish complete , 15 British Grenadiers ( 1.7 b. ) . 
 volume , price 14s . Songs , Choruses , & c. , 16 Ye Mariners England ( 1.7.b. ) — « . Dr. Callcott . 82 . Si 
 separately . Metzler & Co. , 37 , Great Marlborough - street , London , Emigrant Ship ... son w. Gross , $ 1 El 
 publish 1870 . 7 { ne Trafalgar ... bee vais e y 
 + mtl rien : Jaudeamus ze " F ... studentenlie . 79 Li 
 T. JOHN T BAI PIsT. Oratorio ( per- 18 March Men Harlech . 78 si : 
 ) _ form Kidderminster Philharmonic Society , Nov. 12 , 19 rule , Britannia wise aie Dr. Arne . 77 si : 
 1867 ) . compose Wintram Taytor , Mus . Bac . , Oxon , organist * order Lords ’ Commissioners Admiralty 76 Th 
 St. Mary Parish Church Philharmonic Society , | supply ship ’ school . 75 Se 
 Kidderminster . London : Novello , Ewer Co. % 4 th 
 price subscriber ( Pianoforte Score , Folio ) , £ 1 1 . ; non- 73 Bij 
 subscriber , £ 1 11s . 6d . publish , 72 fr 
 subscriber ’ receive Novello , Ewer Co. M s s 71 ni 
 ~e : = 70 te 
 singing card 69 th 
 > q ¢ | 7 68 si : 
 use choir , ETC . contain simple V OICES ORGAN , 67 La 
 lessons , rudiment music , sure easy method special graduale . 66 tw 
 singe sight , accord t » old notation , price 6d . allow- offertory feast immaculate conception 65 
 ance Cler , ymen Choirmasters - copy 64 Ch 
 upwards . specimen copy send po - t free forseven stamp . London : iy 63 Bri 
 Novello , Fwer Co. ; Birmingham : Mr. Charles Russell ; Edu- e s s 62 tw 
 cational Trading co. e h j ° 61 ly 
 ee near 66 tw 
 publish , price 23 . , handsomely bind clovh , gilt edge , mass write great International Competition 59 Fa 
 UVENILE SONGS , set Music ‘ T. Crampron , Sacred Music , hold Belgium , 1866 , obtain 58 Th 
 oJ " child delight graceful little offering . seveuty - Competitors different Natious , 7 Fif 
 music sin ple ; prize , consist Gold Medal 1,000 franc . 
 case happily characteristic word . "—Musical Times | oval score : gs 3 6 bet 
 " pleasing volume , admirably suit intelligence | vocal score , 8s . chorus , 3s . 9d . 55 ch 
 feeling youthful portion public London : Novello , Ewer Co , 54 ch 
 use specially destine . ’"—Ji / ustrated London News , | eas 53 ch 
 London : F , Pitman , 20 , Paternoster - row . | publish , grati , post - free stamp . = 
 rrangement ORGAN , Jou NOVELLO Ch 
 Srarmer . number , 28 . | " 48 ch 
 . 1 . oo ® fro pnt ary e flat ( op . 4 ) ... Beethoven . | CATALOG UE 47 th 
 [ cnc - oo | si 
 Overture , semele ... ae ae .. Handel . oo r gc n mi u s 4 r ia 
 3 . intro‘uction Allegro Symphony Haydn . | 43 
 — - multo , Pianvforte Sunata ( op . | : 42 
 22 ) ae aeé oe « . Schubert . 
 Sinfonia ( judgment ) ... & .. Spohr . | s cc r e d ml u S | Cc 
 Kyrie eleison ar om ‘ ) « . Eybler , | english word . 
 oo continue , } | 
 London : Novello . Ewer Co , } London : Novello , Ewer Co. , 1 , Berners - street , 35 , Poultry 

 MUSICAL TIMES.—Novemser 1 , 1869 




 ROSSINI MESSE SOLENNELLE 


 ORGAN 


 CHAPPELL ORGAN JOURNAL 


 CHAPPELL VOCAL LIBRARY 


 SONGS 


 CONTINU ED 


 CHAPPELL ' musical magazine 


 VOCAL PIANOFORTE MUSIC14 Fuir Maid Mill . german Songs Schubert . 33 Juvenile Vocal Album 

 73 eighteen ( hristy Minstrel song arrange vocal duet . 32 Beethoven sonata . edit Charles Hallé ( . 6 ) . 
 72 Fra Diavolo Pianoforte . 31 Beethoven sonata , edit Charles Hallé ( . 5 . ) 
 71 set Quadrilles , Belgravia Waltz , & c. , Dan . Godfrey . 30 Beethoven sonata , edit Charles Hallé ( . 4 . ) 
 70 song , Gounod , 29 Contralto song , Mrs. R. Arkwright , & c 

 69 Bohemian Girl , Opera Pianoforte . 23 Beethoven sonata . edit Charles Hallé ( . 3 

 68 sixteen operatic song Verdi , Flotow , & c. 27 set Qu+drilles duet , Charles d’Albert , & c , 
 67 La Belle Hél&@ne , Opera Pianoforte . 26 thirty galop , Mazurkas , & c. , d’Albert , & c 

 62 song vy Benedict A. 8 . Sullivan . 
 61 L'Africaine , Opera Pianoforte 

 21 Pianoforte piece , Ascher Goria . 
 | 20 Beethoven sonata . edit Charles Hallé ( . 2 . ) 
 60 - tive old english ditty . 119 favourite air Messiah , Pianoforte . 
 59 Faust . Opera Pianoforte . | 18 song Verdi Flotow , english word . 
 58 thirty new Polkas Galops , & e. , C. d'Albert , & . | 17 Pianoforte piece , Osborne Lindahl 

 87 wa'tze . Quadril!es , Galops , & e. , D. Godfrey , include 16 sacred duet . Soprano Contralto Voices . 
 Mabel , Guards , ke . 15 eighteen Moore " s irish Melodies 

 55 Christy Minstrel Songs . ( seventh selection ) , 13 popular duet Soprano Contralto Voices 

 54 Chappell Popular Church Se : vice . ( selection ) . | 12 Beethoven sonata , edit Charles Hallé , . 1 . 
 53 Chappell Popular Church Services . ( second selection ) . | 11 Pianoforte piece , Wallace 

 52 Chappell Popular Church Services . ( selection ) . 10 Pisnoforte piece . Brinley Richards 




 song PIANOFORTE accompaniment 


 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDONSTAINER . 9 -soe 28 

 . 1 . Andan‘e , quintet e flat ( Op . 4 ) .... Beethoven . | catalogue 

 Vocal Score , 8 , chorus , 33 . 9d . 
 London : Novello , Ewer Co 




 ORCAN MUSIC 


 


 ROSSINI MESSE SOLENNELLE 


 ORGAN 


 CHAPPELL ORGAN JOURNAL 


 CHAPPELL VOCAL LIBRARY 


 song 


 CHAPPELL MUSICAL magazine 


 VOCAL PIANOFORTE MUSIC1 Fair Maid Mill . german song Schubert . | 33 Juvenile Vocal Album 

 73 eighteen ( ' hristy Minstrel Songs arrange vocal duet . 32 Beethoven sonata . edit Charles Hallé ( . 6 ) . 
 72 Fra Diavolo Pianoforte . 31 Beethoven sonata . edit Charles Hallé ( . 5 . ) 
 71 set Quadrilles , Belgravia Waltz , & c. , Dan . Godfrey . | 30 Beethoven sonata . edit Charles Hallé ( . 4 . ) 
 70 song , Gounod , 29 Contralto song , Mrs , R. Arkwright , & c 

 69 Bohemian Girl , Opera Pianoforte . 23 Beethoven sonata . edit Charles Hallé ( . 3 

 68 sixteen operatic song Verdi , Flotow , ce . 27 set Quidrilles duet , Charles d’Albert , & c. 
 67 La Belle Héléne , Opera Pianoforte . 26 thirty galop , Mazurkas , & c. , d’Albert , & c 

 63 Brinley Richards ’ popular national Airs . | 22 - Christy Minstrel Songs . ( selection 

 62 song vy Benedict A. S. Sullivan . | 21 Pianoforte piece , Ascher Goria . 
 61 L’Africaine , Opera Pianoforte . | 20 Beethoven sonata . edit Charles Hallé ( . 2 . 
 66 - old english ditty , | 19 favourite air Messiah , Pianoforte . 
 59 Faust , Opera Pianoforte . 118 song Verdi Flotow , english word , 
 58 thirty new Polkas Galops , & e. , C. d'Albert , & c. |17 Pianoforte piece , Osborne Lindahl . 
 57 waltz . Quadril'es , Galops , & c. , D , Godfrey , include | 16 sacred duet . Soprano Contralto Voices 

 Mabel , Guards , & c. 15 eighteen Moore ’ 8 irish melody , 
 56 Santley popular song . | 14 song , Schubert . english german word . 
 55 Christy Minstrel Songs . ( seventh selection ) , | 13 popul ur duet Soprano Contralto Voices . 
 54 Chappell Popular Church Se : vice . ( selection ) . | 12 Beethoven sonata , edit Charles Hallé , . 1 . 
 53 Chappell Popular Church service . ( second selection ) . | 11 Pianoforte Pieces , Wallace . 
 82 Chappell Popular Church Services . ( selection ) . 10 Pisnoforte piece . Brinley Richards , 
 51 - Scotch Songs , word . Valses , C. d'Albert , Strauss . & e , 
 £ 0 Christy Minstrel Songs . ( sixth selection ) . polka . Charles d'Albert , Jullien , Koenig , & c. 
 49 Christy Minstrel song , ( fifth selection ) . set Quadrilles , Charles d’Albert , complete . 
 48 Christy Minstrel song , ( fourth selection ) . song , Handel . 
 47 thirteen standard song Shakespeare . sacred song , Popnlar Composers , 
 46 sea song , Dibdin , & e. | song , Mozart , italian english word 
 45 hymn christian season , domestic use . 3 song , Wallace , 
 44 welsh english melody Pianoforte . | 2 song , Hon . Mrs , Norton . 
 43 Scotch melody , arrange Pianoforte . 1 thirteen song , M. W. Balfe . 
 42 irish melody , arrange Pianoforte 

 song pianoforte accompaniment 




 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDON 


 P 9 , DWIUWED 


 music coming season 


 BARNBY , J. high time awake 


 GAUNTLETT , H.J. THOU WILT perfect 


 GOSS , JOHN . wilderness solitary 


 GOUNOD , CHARLES . bless cometh 


 HARK ! GLAD sound . 


 HUMPHREYS , P. 


 HARRIS , EDMUND 


 HEAR , O HEAVENS 


 LUTHER HYMN . great GOD , 


 MAC FARREN , G. A. HOSANNA son DAVID . 


 MENDELSSOHN . lovely messenger . 


 PURCELL . REJOICE LORD , ALWAY . 


 REDFORD . REJOICE LORD 


 WISE . AWAKE , AWAKE , thy strength , O 


 HARRIS . EDMUND . HARK ! HERALD angel SING . 


 MENDELSSOHN . HARK ! HERALD angel SING . 


 COOPER , J.T. " whisp'ring 


 HANDLEY , EDWARD . carol . 94 . 


 OLD ENGLAND . " 134 


 SULLIVAN , S. night year . 


 WESTBROOK , W. J. HOLLY BERRIES , HOLLY BERRIES . 


 ALLEN , GEORGE B. beginning word . 


 BARNBY , J. GRACE GOD BRINGETH 


 BENNETT , A. O ZION BRINGEST good tiding . 


 BEST , W. T. shepherd watch 


 CHAWNER . , C. F. F. DEVOUT man carry STEPHEN 


 CLARKE , J. HAMILTON . shepherd 


 CROCE . BEHOLD , bring glad tiding . 8.4.73 


 DE 


 ROL , 


 BEEF 


 VORD 


 GETH 


 18 copy 


 DINGS 


 


 PHEN 


 herd 


 8.4.1.3 


 ht 


 HATTON , J. L. BLESSED ' LORD GOD ISRAEL . 


 HOPKINS , E.J. let unto BETHLE- 


 LESLIE , HENRY . FEAR , bring GOOD 


 MONK , W.H. HALLELUJAH ! unto child 


 PURCELL . BEHOLD , bring glad tiding . 


 SMITH , CHARLES W. BEHOLD , bring glad 


 _ dure month , 


 


 music compose 


 VITTORIA . BEHOLD , bring glad tiding 


 WEBBE , FULNE 


 content . 


 MATEUR CHOIRS 


 S. JAMES HALL 


 ORATORIO CONCERTS 


 conductor , MR . JOSEPH BARNBYTue projector Oratorio Concerts beg announce ensue season , 
 Subscription Concerts , follow : — 
 WEDNESDAY , DECEMBER 8 , 
 HANDEL DETTINGEN TE DEUM ACIS GALATEA . 
 additional accompaniment MENDELSSOHN 

 TUESDAY , DECEMBER 21 , WEDNESDAY , APRIL 27 , 
 HANDEL MESSIAH . MENDELSSOHN ELIJAH . 
 THURSDAY , JANUARY 20 , 1870 , WEDNESDAY , 11 , 
 HAYDN season . REBEKAH , JOSEPH BARNBY , 
 WEDNESDAY , FEBRUARY 16 , 
 HANDEL JEPHTHA . HANDEL ALEXANDER FEAST . 
 WEDNESDAY , MARCH 9 , WEDNESDAY , 95 , 
 BEETHOVEN MASS D. | BEETHOVEN CHORAL SYMPHONY 
 TUESDAY , APRIL 5 , 
 BACH PASSION MUSIC . MENDELSSOHN LOBGESANG 

 composition enumerate list find unknown english 
 audience . ' ' Passion Music , " Bach , engage attention Mendelssohn early life thoroughly 
 , spite innumerable difficulty , rest satisfied produce . Zelter 
 hesitate express doubt result ; , conjunction friend Devrient ( , 
 ' ' recollection Mendelssohn , " relate account event ) young composer persevere 
 intention ; 11th March , 1829 , sublime composition perform Leipsic 
 extraordinary success . England , auspex Bach Society , earnest 
 energetic effort Professor Sterndale Bennett ( founder conductor Society ) , 
 present time 1554 ; production work 
 Oratorio Concerts , assistance proffer Professor Bennett , kindly aid 
 highly estimate , thankfully acknowledge . Beethoven Mass D , composer 
 late work , rarely hear England , principally account enormous difficulty , 
 partly consequence high pitch hitherto prevail country ; 
 attempt performance concert , state curtailment alteration 
 kind . Haydn season , , assuredly good , composi- 
 tion , strangely neglect country lately , confidently hope revival 
 cordially welcome subscriber public . Handel Alexander Feast , work 
 , contain beautiful , popular piece , seldom perform 
 entirety , produce , Mozart additional accompaniment . Beethoven Choral Symphony 
 Mendelssohn Lobgesang evening , belief certain 
 interest create contrast great work somewhat resemble construction . 
 IIandel Acis Galatea , Dettingen Te Deum know need word recommend- 
 ation ; Oratorio Concerts perform , time England , 
 additional accompaniment Mendelssohn . accompaniment , appearance Herr 
 Devrient " ' recollection , " allude , know existence ; mention 
 time letter write Mendelssohn Devrient , ask search 
 send score , library ' ' Singakademie , " Berlin 

 success Handel Jephtha series Oratorio Concerts , render necessary 
 repeat , additional accompaniment , Arthur S. Sullivan . scriptural 
 idyll , scene , Rebekah , word Arthur Matthison , music Joseph Barnby , 
 perform time 

