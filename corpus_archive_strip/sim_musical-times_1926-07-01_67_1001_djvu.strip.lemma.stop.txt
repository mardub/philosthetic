


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 found 1844 


 publish month 


 ROYAL CHORAL SOCIETY . ROYAL COLLEGE MUSIC 


 VICTORIA EMBANKMENT , E.C.4 


 FACULTY MUSIC 


 president 1926 : HARVEY GRACE , F.R.C.O 


 1926 


 96 & 95 , WIMPOLE STREET , w.1 


 TOBIAS MATTHAY 


 PIANOFORTE SCHOOL 


 complete training course teacher . 


 ROYAL 


 MANCHESTER COLLEGE 


 MUSIC . 


 ADOLPH BRODSKY 


 STANLEY WITHERS , M.A 


 GLASGOW 


 ATHENAUM SCHOOL MUSIC 


 session 1926 - 27 


 MANCHESTER SCHOOL MUSIC . 


 16 , ALBERT SQUARE 


 LONDON SCHOOL SINGING 


 real use 


 relaxation 


 entirely unique CORRESPONDENCE course 12 


 * * MODERN teaching APPLICATION WEIGH ' 


 CHARLES A. GILLETT 


 MOLTON STREET 


 BIRMINGHAM & MIDLAND INSTITUTE 


 SCHOOL MUSIC 


 session 1925 - 1926 . 


 NATIONAL ASSOCIATION COMPET , TIVE 


 choir 


 president 


 1100 specimen working . 


 recent success , 


 DOF 12 


 


 tion 


 


 musical t 


 593 


 SINGING - class circular 


 JULY 1 1926 


 reflection work 


 SCRIABIN 


 SOMEIt owe illogical application 
 half - truth Scriabin muddled reasoning run 
 destruction . half - truth 
 danger ill - balance mind 
 half - truth . - balance mind exactly 
 divide line truth falsity 
 lie . difficult distinguish true 
 false statement man bronze 
 sun , copper - colour moon , 
 freckle star . difficult 
 disentangle true false reasoning old 
 lady ( probably old gentleman ) , , 
 writing advertisement sky , 
 exclaim : " look , ’ wireless 
 message catch fire . ' muddle - headed 
 reasoning good fun farce , 
 dangerous offer seriously . 
 Scriabin offer muddle - headed reasoning 
 seriously value later work 
 suspect 

 metaphorically , speak colour music . 
 , , D flat major dark - brown , 
 reason music 
 march * Rheingold ' slow move- 
 ment Beethoven sonata F minor ( Op . 57 ) 
 form mind connection 
 D flat major darkness . music 
 play C major sound 
 dark . similarly think G major 
 light green fresh spontaneous 
 tune know key . 
 exactly colour transpose 
 flat major G flat major . primarily 
 question association . people aver 
 colour certain common chord 
 play . ' red B flat play , 
 ' God save king ' play 
 |b major people red 

 Scriabin , accept half - truth 
 ‘ truth , build ' prometheus ' assumption 
 |that music definite colour , 
 novelist , prove sense architecture , 
 | write chapter form Norman 
 arch , mouse ' Alice - - Wonderland ' 
 /told history pictorial appearance tail . 
 Scriabin reasoning illogical , 
 | question colour , little moment , 
 | affect work , * Prometheus , ' 
 |but grave importance apply 
 question harmony possible 
 expansion , reduce later work 




 594 


 1926 


 596 


 1926 


 C natural B SHARP 


 1926 : 597 


 personality musical 


 CRITICS 


 VI.—-ROBIN LEGGE 


 recollection BRAHMS 


 ( ILONA EIBENSCHUTZrecitals . month , Frau Schumann 
 musical party house , 

 follow programme : 
 Beethoven Sonata , Op . iii Ilona Eibenschiitz 
 Song Julius Stockhausen 

 Schumann Carnival Clara Schumann 

 musician Vienna , — Brahms 

 play Etudes Symphoniques 
 Schumann , small piece Brahms 
 Scarlatti , Prof. Anton Door , vice- 
 president Tonkiinstlerverein , come 
 letter tell audience Herr Griitzmacher 
 send word minute 
 unable play . Brahms come , : 
 " na , da spielen sie doch mal die op . 111 von 
 Beethoven . ' ( , play 
 Op . 111 Beethoven 

 , believe henceforward Brahms 
 friend . supper , concert , 
 sit . wish know 
 plan , Frau Schumann want 
 . tell arrange Mr. Arthur 
 Chappell engage Monday popular 
 Concerts London , 
 London time 




 1926 599was happy . Ischl 
 day , afternoon Brahms pay visit . 
 great moment , feel 
 happy . tell London ; 
 interest 

 March 2 , 1891 , G major string Quintet 
 perform : time Joachim , 
 Piatti , Ries , Strauss , Gibson , Monday 
 ' pop . ' concert play b flat 
 trio , Op . 97 , Beethoven , Joachim 
 Piatti , hear rehearsal 
 Quintet . tell Brahms care- 
 fully Joachim rehearse Quintet , 
 splendid reception meet , pleased 

 day later Brahms come dinner 
 ( Mittagessen , o’clock ) , soon 
 habit come nearly week 
 dinner . like 
 fuss , ask 
 anybody ' meet Brahms . ' Mittagessen 
 simple . beginning aiways 
 come , later ask 
 bring Prof. Gustav Wendt , old friend 
 Karlsruhe , come summer Ischl 
 time Brahms . ' come 
 Prof. Késsler , Budapest , like 
 . bring friend 
 afternoon , Fritz Steinbach 
 wife , Adolf Menzel , Stockhausen , & c 

 state 

 material bat , speak . 
 adoption folk - tune 
 thematic basis year ago , 
 ' original ' theme place 
 little tag ¢/ché 
 folk - tune , bad olf 
 . building national 
 school composition ? , surely 
 build . true Haydn 
 Beethoven ( especially ) use 
 folk - tune occasionally instrumental music , 
 work 
 occasional hint idiom 
 folk - song ? folk - idiom Bach 
 Wagner , save purpose local 
 deny search beauty 
 " Banks Green 
 rhapsody 

 writing 




 602 


 1926 


 11592 


 new light late tudor composer 


 music TOLSTOY LIFEsaw 

 father relation Beethoven somewhat 
 complex . composer — apart period 
 produce strong impression . 
 ' boyhood ' describe effect produce 
 ' pathetic ' Sonata hero ; ' Family Happiness 
 refer lovingly ' Moonlight ' Sonata ; 
 ' Kreutzer Sonata ' borrow title 
 Beethoven work ; ' Appassionata ' excite 
 ; finally , play Beethoven 
 sonata orchestral - handed ) . 
 demand , instance case , 
 critical attitude accept idol , 
 protest exceptional cult Beethoven , 
 consider predecessor Haydn Mozart 
 equal superior . consider 
 Beethoven culminator period 
 music great development , forefather 
 decline ull remembe : 
 artist genius 
 work merely new content , 
 form ; Haydn create form sonata 
 symphony , Mozart opera , 
 exist form 
 Mozart 
 ought 

 
 judgment 
 distinct 
 

 
 new 

 create 
 Haydn 
 , ask 
 consider Beethoven yvenius . 
 father mistaken 
 Beethoven . symphonic form 
 predecessor 
 consider new form . Beethoven construction 
 broad free ; present new 
 Scherzo ) , true genius 

 new form , fully applicable 

 far , father consider 
 comprehensible comparatively 
 small number people , listener 
 somewhat pamper music spoil 
 taste understand Beethoven , 
 artificial 

 Wagner , Liszt ( exception 
 arrangement ) , Berlioz , Brahms , Richard Strauss , 
 modern composer , 
 father great liking . * accept 
 Grieg piece . true 
 - acquaint new composer , 

 Beethoven 
 

 previously 
 extent 
 
 
 

 

 y * 
 create m 
 Beethoven 

 Beethoven 

 ordet 




 1926 605certain musical 
 understanding Chopin . 
 ' probably taste spoilt 

 addition 
 Beethoven , remember father 
 sonata G major ( op . 14 , . 2 ) , 

 j hear , , cenversation 
 husband wife , general toy 
 sonata . Sonata e flat major ( op . 
 consider 77ze Scherzo ( e flat minor 




 ' FALSTAFF ' 


 JOHN W. KLEIN 


 VERDI 


 1926 


 _ — _ 


 HANDEL use uncommon 


 instrument combination 


 1725 


 609 


 510 


 consecutive fifth 


 SCHUBERT 


  — — _ — _ Hadow 

 Seeley , Service & Co. , 5 . vol . | 
 * Beethoven Op . 15 Quartets Sir W H 
 Hadow . 
 { Oxford University Press , | ! 
 \ ( omparison Poetry Musi Henry 
 Sidgwick Lecture , 1925 Sir W. H. Hadow 
 [ Cambridge University Press , 25 
 volume study , n 
 eleventh edition , long past review stage . 

 retain piace example 

 Henry care pin 

 early opinion . , condition affect 
 contact general publi ith music 
 undergo profound change , espe lally 
 past year . pa e 
 read curiously - day 
 \ generation ago Englishmen play piano 
 forte non - existent , Englishwomen 
 nde education * Batt Prague . ' 
 amateur level thi ntry 
 high , ind vet little « e ot lam liartse 
 Beethoven Schubert , bring 
 fireside , admit friend 
 ship . t , course , prin ipal nm good 
 Art neglect . appreciate good musi 
 hear : hear liv 
 : live mmpany 

 whem play ng 

 Frederick Chopin Dvorak ; + Johannes 

 Brahms . 
 little book Beethoven Op 18 Quartets 

 

 miniature score 

 rule good deal 
 Beethoven method ( Op . 16 , 
 ) obtain fat 

 volume 




 H.Gbeen - write enlarge . ' study ' 

 Bach , Cherubini , Beethoven , Meyerbeer , ¢ hopin , 
 Mendelssohn , Parry , Reger ; ' caprice ' play 
 round ' creed custom , ' ' 

 scene , ' ' negative music , ' ' Slender Reputa 

 . 

 volume 
 amateur ; Mr. 
 sense musical 
 feel liberty fall 
 present writer , example , 
 blind Beethoven - worshipper , agree 
 Beethoven , Mr. Brent 
 Smith , ' undeniably dull moment ' ; 

 estimate compose 

 fault usually cause set 
 forth \re magnificent 
 : surely characteristic Beethoven 

 initial inspiration 

 glint hasty safety - pin 

 true immature Beethoven 

 surely Beethoven good . 
 find mastery construction texture 
 look ? 
 \mong failing set ' trite 
 coda . " , ' 

 think 
 Beethoven g fair add 
 brief quotation set glow 

 fine music , 
 point 
 interminable 




 1865 ourse , great mus 

 course ' good ' page 
 irgue Brahms chamber - music improve 
 ment Beethoven , page bid 

 farewell , ' clothe mantle Johann 




 bs1 collapse cis 
 viacial aye 

 heart book 
 beethoven music future . 
 musi sull 
 pessimistic world 
 ruin theological 

 declaration 
 Beethoven 
 contain 
 fringe appreciate 

 - day , litter 

 sociological hope s , star Beethoven spirit 

 W. J 

 shine . sudden author grow 
 sympathetic . feel , 
 express far . 
 | praise Beethoven spring fine poetic idea , 
 sake 1 ine line excuse good 
 author hateful denigration . end 
 | chapter ( p. 88 

 question , ' 

 live ? ' answer , reply 

 Beethoven render ridiculous 

 year Army Lieut.-Col 




 ORGAN MUSIK 


 616 


 — _ = — _ 


 ~@-?_@ = _ — 


 musical 


 


 BORWICK transcription 


 1926 


 G. G 


 CHORAL MUSIC : UNISON 


 - song child FEMALI 


 voice 


 MALE - VOICK 


 MIXED - voici 


 PIANOFORTI 


 620 


 1926 621 


 EASY 


 PIANOFORTE MUSIC 


 CHAMBER MUSIC 


 B. V 


 VIOLIN 


 1926 


 CELLO 


 1892 


 FOLK - dance brass band 


 score 


 impression music 


 dominion 


 ALTO 


 TENOR 


 - song 


 ls = = 


 _ — _ = = = = ~ = = 


 — _ — f — | | 4 


 = = f = = : = = — CS 


 4 -38—5 — _ — — — — 


 5 — 5 = — — — — — — s — s — 


 LOVE WAKES 


 1 7 


 1926 629 


 COLUMBIA 


 H.M good Russians mere dabbler 

 Kreisler usually gramophonist trifle , 
 Beethoven Gavotte 
 Bach Minuet . ' true , transcription , 
 splendidly 
 play , despite heavy handling 
 little Bach piece ( da777 

 Sharp record tn Palmgren ' Rococo 
 folk - tune ' Gentle Maiden , ' arrange 
 




 e424 


 d 1087the MUSICAL TIMES — JuLy 1 : 1926 6351 

 Tito Schipa , sing arrangement Liszt Beethoven variation theme , 
 yackneyed * Liebestraiime ' poor ' Ave Maria’|*Tandeln und Scherzen , ' hear , 
 ot perpetration ( db873 apparent reason — 
 complete form , event . Ethel Leginska 
 voucalion playing 1 , like work , unequal . repeat 
 : chord accompaniment far heavy . 
 \ ery appropriately , view Weber centenary , : . " e oth 
 * } | hand , crisp playing delightful fughetta 
 ome capital record overture ' Euryanthe , . : ; 
 : é - rate . variation reminder fact 
 play Lolian Orchestra , conduct Stanley ; 
 4 ote ; , 4 ' | Beethoven write vood fugue , 
 Chapple . ' recording ' new ' type , : ' PF , 
 capital hand neat lively fughetta 

 great power ; reproduction tone 




 KOLITAN 


 OPERA WIRELESS 


 OPERAS hear seenrelapse sobrie 

 Johannesburghers ' secure 
 exciting time veat witha Beethoven 
 Festival example organize energy 

 city home copy 




 


 HANDEL FESTIVAL 


 1 1926 635 


 YFORD : hitherto unnotice 


 catalogue early MI 


 PI 


 1953 


 1659 - 65 


 1620 


 1612 ( b.m 


 1o 


 . 20 


 catalogue 


 


 MUSIC k+*booke 


 dd 


 1655 


 420 ) \ 


 16050 


 640 


 1926 


 ORCHESTRAL notation 


 musical 


 1926 641 


 se SSS se — S _ 


 0 0 0 


 ss 


 642 


 MUSICAL T 


 ROYAL COLLEGE ORGANISTS 


 annual GENERAL meeting 


 distribution DIPLOMAS 


 fellowship 


 ASSOCIATESHIP 


 CINEMA ORGANIST 


 h. W. 


 1927 


 MUSICAL T 


 CHURCH MUSI « QUETTA 


 FOUNDLING HOSPITAL : final service 


 RECITALSSonata . 6 , J / endels Larghetto ( symphony 

 D ) , Beethoven ; fugue G minor , Bach ; Imperial 
 March , £ /gar 

 Mr. H. E. Knott , St. 
 Psalm - Prelude . 2 , 
 * lovely , ' Vaughan William 
 Epilogue , Healey Willan 




 appointment 


 _ B&B 


 MAINZER MUSICAL time 


 UNIFORM PITCH 


 BALFE : correction 


 1926 645 


 IRISH HORSE GUARDS ’ drum 


 SECCOMBE 


 RUSKIN 


 ND 


 MUSIC 


 WHITE BOUGHTON BAINES 


 sirw deny ordinary source musical education 

 circumstance sympathise . 
 study 
 pleased classic , master , great , 
 ind admiration Beethoven Bach truly profound 

 Mr. White conclude Mr. Boughton article 
 Baines * young man considerable natural talent , ' 
 ind opine England * thousand 
 natural talent equal 
 Boughton , article , use 




 LAUGHTON 


 CENTENARY WEBER 


 RIPON 


 late DR . E. J. CROW , 


 TENOR _ VOICE THUNDER , 


 


 MUSICAL T 


 1926 


 ROYAL ACADEMY MUSIC 


 ROYAL COLLEGE MUSIC 


 OPEN SCHOLARSHIPS , 1920 


 1926 647 


 TRINITY COLLEGE MUSIC 


 concert 


 HAROL ! 


 SAMUEI 


 second concert 


 G. THALBEN - BAL 


 JELLY ARANYI HAROLD SAMUEI 


 concert 


 648 MUSICAL 


 fourth concert 


 STEVART 


 FOLKS 


 THI 


 COLLINS 


 home ' : ¢ 


 COMPOSER 


 OLD ENTENARY 


 STEPHEN 1826 


 STEPHEN C , FOSTER , 


 W. H. GRATTAN FLOOD 


 ENGLISH FOLK - DANCE 


 SOCIETY 


 FESTIVAL 


 1542 


 1926 649 


 SINGERS MONTHShe possibility good voice , 
 technique style fault . py , 7 , k , 
 pianist MONTH , 
 Moritz Rosenthal ( ueen Hall _ strictly 

 regulation programme . unnecessary refer 
 1is technical power pianist , 
 striking characteristic capacity preserve 
 note rapid passage . 
 play Beethoven Sonata C Ill , 
 Chopin sonata , Op 58 , complete understanding 
 programme conclude brilliantly 

 clearness 




 650 


 1926 


 PASDELOUP ORCHESTRA 


 MR . WARLOCK song 


 ENGLAND WALES 


 HASTINGS , 


 15 , 17 , 18 , 


 ABI 


 14 , 


 108 : 


 LAY 


 SCOTLAND 


 651 


 BENFIELDSIDE , 


 BRIGHTONElgar 

 HARROGATI summer symphony concert 
 progress able direction Mr. Basil Cameron . 
 Beethoven Dvorak ' new world ' 

 20 June 2 




 LEED B minor repeat 
 infl 
 promise season definitely 

 quarter . Beethoven 

 January 27 , March 10 Bantock new ' song 
 song ' receive Lancashire hearing , Miss 
 Caroline Hatchard Messrs. Mullings Allin 
 solist Messiah , ' December 23 24 , com 
 plete probably prove memorable 

 C , odowsky 

 Suggia , Cassado , Catterall , William Primrose 
 include string soloist . Formichi sing 
 final concert March , hope 
 programme publicity announcement observe 
 season proportion singer 
 symphony cast Berlioz , Bach , Handel , 
 Beethoven choral concert _ perfect model 
 efficiency Wagner concert : 
 soloist appear time Messrs. Roy 
 Henderson Gaspar Cassado , lead 
 member Orchestra _ soloist — 
 Messrs. Alfred Barker Clyde Twelvetrees . 
 Oxrorp.—Mrs . E. S. Coolidge , famous american 
 patron music , chamber concert - sheldonian 

 end term , 
 ' Canticle Sun 




 YORK , 


 GERMANY 


 METROPOLITAN CAST 


 BADEN - BADEN 


 _ — _ 


 TI 


 


 1926 653 


 post - wagnerian opera WEIMAR 


 PAUL WEISSMANN 


 TORONTO 


 VIENNA 


 654 


 1926 


 \ MOZART PREMIERE 


 WEBER CENTENARY 


 PAUL BECHER 


 R.A. 


 MUSICAL 


 1926 


 655 


 content 


 625 


 dure month . 


 B * WER , HERBERT 


 RR , ALAN.—‘“‘O 


 LI 


 , ; LETCHER , PERCY | } 


 1142 


 HENR 


 * chool MUSIC REVIEWIsland , " Percy E. FLETCHER 

 forte , BEETHOVEN . 2d 

 chool SONGS.—Published intwo form . 




 SCHUBERT 


 publish 


 H. W. 


 S ' IWERBY , L. 


 


 GRAY CO . , NEW YORK 


 


 SONGS 


 ARRANGED 


 SEA 


 PIANOFORTE 


 DUET 


 


 GEOFFREY SHAW 


 1926 


 FOYLE music dep 


 tender 


 CITY MELBOURNE ( AUSTRALIA 


 supply GRAND ORGAN 


 HALI 


 MELBOURNE TOWN 


 DELIVERY , erection GRAND ORGAN 


 TOWN HALL , MELBOURNE 


 supply 


 1926 


 MUSICAL TIMES 


 CHARGES 


 advertisement 


 SPECIAL notice 


 w.1