


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR


PUBLISHED ON


THE FIRST OF EVERY MONTH


FEBRUAR


Y 1, 1874


REENWICH PARISH CHURCH.—WANTED


ROYAL ALBERT HALL CHORAL SOCIETY. 


EIGHTH SUBSCRIPTION CONCERT, 


THURSDAY, FEBRUARY 5, 1874. 


MENDELSSOHN ’S ELIJAH


AND 


ORGANIST 


ROYAL ALBERT HALL CHORAL SOCIETY. 


NINTH SUBSCRIPTION CONCERT, 


WEDNESDAY, FEBRUARY 138, 1874. 


HANDEL’S MESSIAH


MR. SIMS REEVES


AND 


LBERT MUSICAL SOCIETY. — WANTED


374


PROFESSIONAL NOTICES. 


MR. J. TILLEARD, 


M USIC ENGRAVED, PRINTED, AND PUB- 


RUSSELL'S MUSICAL INSTRUMENTS. 4 


GRATEFUL—COMFORTING. 


BREAKFAST


HARGREAVES’S PROFESSIONAL AND 


CONNOISSEURS’ HARMONIUM


REGISTERED


“THE PROFESSOR’S HARMONIUM.’” 


HARGREAVES’S AMERICAN IRON FRAME PIANO


WILLIAM HARGREAVES, 61, DALE ST., MANCHESTER


ARMONIUMS! HARMONIUMS! 


W. HATTERSLEY’S 


CELEBRATED ORGAN-TONED HARMONIUMS, 


MANUFACTURED ONLY BY 


W. SNELL’S IMPROVED HARMONIUMS


REDUCED PRICES OF 


OUSELEY AND MONK’S


POINTED


PSALTER


LONDON: 


NOVELLO, EWER AND CO., 1, BERNERS STREET (W.), AND 35, POULTRY (E.C


TONIC SOL-FA EDITION. 


NOW READY. 


FROM INTERVALS TO COUNTERPOINT.” 


A MANUAL OF SINGING


FOR THE USE OF CHOIR TRAINERS & SCHOOLMASTERS. 


TWENTY-THIRD EDITION. 


THE COLLEGIATE AND SCHOOL


SIGHT SINGING MANUAL


(COMPANION WORK TO THE ABOVE


COLLEGIATE VOCAL TUTOR


CATHEDRAL CHANT BOOK


377


ARRANGED FOR S.A.T.B., WITH ACCOMPANIMENT


ASY PART-SONGS FOR BOYS’ AND GIRLS


HE SCHOOL AND FAMILY BOOK OF


SHORT AND EASY CANTATAS. 


WILLIAM J. YOUNG’S 


THE ROYAL MARRIAGE


THE BRIDAL LAY


A CANTATA, WITH PIANOFORTE ACCOMPANIMENT, 


DEDICATED (BY SPECIAL PERMISSION) TO 


HRH. THE DUKE OF EDINBURGH. 


LIBRETTO BY H. PIERCY WATSON. 


MUSIC BY EDMUND ROGERS


C. JEFFERYS, 67, BERNERS ST


THE CHORAL SOCIETY


PRICE TWOPENCE EACH


MICHAEL WATSON. . 


GRAND OPERATIC DUETS


000000


THE ELEMENTS OF MUSIC 


SYSTEMATICALLY EXPLAINED BY 


HENRY C. LUNN


ROYAL ACADEMY OF MUSIC


WILLIE PAPE


C. JEFFERYS & CO."S ENGLISH MODEL HAR- 


67, BERNERS STREET, W


378


T° JOURNEYMEN ORGAN _ BUILDERS— 


379


THE MUSICAL TIMES, 


AND SINGING-CLASS CIRCULAR. 


FEBRUARY 1, 1874


ASTER 


SStreet

music on the stage ; a Mozart to combine with it the 
expression of all the emotions of which the human 
heart is capable; a Beethoven to hold up to us, in his 
one great dramatic work, the ideal of human gran- 
deur and purity, to which alone he thought his music 
capable of being wedded on the stage

Glancing at the list of operatic performances 
during a season, here in London as elsewhere, one 
would indeed think that the voice of these great 
masters was drowned amidst the dance tunes of the 
modern Italian school; that the ideal to which they 
have pointed was crushed by the encyclopedian 
monstrosities of the Parisian ‘“‘ Grand Opéra




380pristine dignity, “the greatest silence will be im

on all spectators.” Alas, when will that looked for 
time come! We are afraid we are yet somewhat 
removed from the realisation of the dreams of this 
eighteenth century critic. Since he wrote his little} | . 
book we have seen the Glucks and the Mozarts, the sullied 
Beethovens and the Webers, and they have passed 
away; and yet we sorely needed a second Algarotti 
to remind us of the fact that our opera-houses ought 
to be the temples devoted to the highest interests of 
art, and into which we should enter with reverence

This has been emphatically done by Wagner, and 
it is, we repeat, among his chief merits in his position 
as a critical writer. How far he has succeeded, asa 
creative artist, in approaching the ideal of the 
‘‘ Drama of the Future” in his own music dramas, it 
is not now our object to investigate: they are yet 
too little known in this country to admit of any pr- 
fitable criticism

That in spite of the shallowness of popular tastes, 
the musical art does advance: that it will eventually 
reconquer that vast field for the display of its highest | ™*° 
qualities, the Stage; that, in fact, there is a futurey™ 
and a great future for the musical drama, we do not 
for a moment doubt. The progress of all art, between | ™™ 
one phase of its development and another, may be|% 
slow, nay, almost imperceptible. It is during these, 
more or less protracted, hazy intervals that the crit 
cal police are busiest, going their rounds and turning}. 
their dark lanterns this way and that, in the often 
vain attempt to show us whither we are going. And 
while criticism is still thus engaged, the rising sung’ °™ ' 
of creative genius will, sooner or later, scatter the} 
fogs before it, till we are standing once more im the |B 
glorious light of anew day. We are now inastalt 
of transition: but beyond the somewhat labyrinthiat 
strivings of the day, we look with the eye of faithtoj ™™ 
the fulfilment of the prophecies, the legacies aff Pld pro: 
Mozart and Beethoven, contained in “ Don Giov: 
and ‘ Fidelio

OLD MUSIC




381


382


CO — —_


383


SIETY


WAGNER SOCIETY


384BRITISH ORCHESTRAL SOCIETY

THE second season of this Society commenced on the 
22nd ult. at St. James’s Hall. With the exception of 
Sir Sterndale Bennett’s ever welcome Overture, ‘* The 
Naiades’’ (which had an exceptionally fine rendering) and 
a clever Saltarello, for orchestra, by Mr. Hamilton Clarke, 
no work by an English composer was selected. The 
performance of Mozart’s Concerto in E flat, for two piano- 
fortes, by Miss Linda Scates (of the Royal Academy of 
Music) and her master Mr. Walter Macfarren, afforded a 
good opportunity of exhibiting native executive talent to 
the greatest advantage; and Beethoven’s ‘“ Eroica” 
Symphony and Cherubini’s overture ‘‘ Les Deux Journées ”’ 
tested the best qualities of the band, the thoroughly 
‘“‘ British ’? character of which can be the only reason why 
the title of the Society is retained. The vocalists were 
Miss Edith Wynne and Miss Augusta Roche. In com- 
pliment to the Duke of Edinburgh (who is the Patron of 
the Society) the National Anthem and the Russian Hymn 
were performed by the orchestra, which is again placed 
under the able direction of Mr. Mount

ST. PAUL’S CATHEDRAL




TS


385


REVIEWSthe betrothed of the Athenian leader, lamenting the loss of

her beloved. This is a simple and highly expressive Air. 
In No. 4, the bass announces the approach of the vic- 
torious army; and No. 5 isakind of choral march, bidding 
welcome to the heroes, a piece whose strongly marked 
accent and sonorous vocal distribution secure for it an 
impressive effect. Then we have a Recitative, in which 
the enraptured Egina recognises her lover’s form among 
the victors, which leads, of course, to a Duet, consisting 
of the delighted exclamations of the happily reunited pair. 
A most difficult situation is this for musical rendering; 
many admirable composers have fallen short in the attempt 
to paint in notes the ecstacy of faithful hearts that meet in 
joy after a separation in despairing anguish, and the fact 
of Beethoven’s perfect success in the wondrous piece in G, 
in Fidelio, but adds to the difficulty of after writers; to 
set comparison aside, it is high praise to say that the 
instance before us is far from unsuccessful, and is certain 
of good effect. The final number is chiefly choral, being 
interspersed with strains for the three solo voices. It is 
full of spirit, its generally exultant character being varied 
by touches of tenderness, and it will leave the hearers with 
a most favourable last recollection of the work

Mr. Brion is a young musician of whom nothing so 
extensive nor so important as this Cantata has yet appeared. 
Let him take encouragement from his successful accom- 
plishment of an arduous task, and aim again and again at 
the same high mark with the growing certainty of a still 
more practised hand




SSS SS SSS 


J L 


——— —_——_—— 8 


—_—_


BS


XY 


4 } 4. ) 


GF *— = 


TREB


ALT


TEN 


P3— —— 


692. P 


SS2


OL


T T — 


2 EESEEAESS


391


LAMBORN COCK


392


BARRAS AND BLACKET, ROTHERHAM


THE MUSICAL TIMES


FEBRUARY I, 1874. 393


ADDISON, MANCHESTER ; HUTCHINGS AND ROMER


BREITKOPF AND HARTEL, LEIPZIG


394


ORIGINAL CORRESPONDENCE


CHANGE OF THE SYLLABLES IN MODULATION. 


TO THE EDITOR OF THE MUSICAL TIMES


A. ORLANDO STEED, 


ROYAL ACADEMY OF MUSIC. 


TO THE EDITOR OF THE MUSICAL TIMES


CHARLES LAWRENCE. 


TO CORRESPONDENTS


395


_———_____—_


BRIEF SUMMARY OF COUNTRY NEWSDoncasTER.—The new organ, built by Messrs. S. Meacock, of High 
Street, for Catherine Street Free Church, was opened on Friday, the 
and ult., by Dr. Spark, organist of Leeds Town Hall, who played 
through a well-selected programme in a most able manner, showing 
off the instrument, which has a fine tone, to the greatest advantage. 
The proceedings were opened by the Vicar of Christ Church witha 
short prayer

EpINBURGH.—On Wednesday, the 24th December, Professor Oake- 
ley played on the organ in the Music Class Room, a selection of 
music appropriate to Christmas Eve, before a large audience. 
Particularly noteworthy was a very interesting addition to the statues 
of musicians—a beautiful bust of Beethoven, modelled by Professor 
Schaller, of Vienna, in 1825, a present from the London Philharmonic 
Society to the Edinburgh Music Chair. Two old chorals in the 
Christmas Oratorio, ore said to be 270 and the other 330 years old, 
and Bach's Pastoral Symphony, which, like Handel's, is in the style of 
the old melody played in the streets of Rome in the Chistmas week, 
were very acceptable. The music from the Messiah being better 
known, was more universally appreciated. A Pastorale by Kullak was 
also very warmly received, and the Professor extemporised on two 
well-known hymns at the close.——ANn excellent concert, in aid of the 
Mars Training Shipat Dundee, was given on the r4thult.,in the Music 
Hall, by the Edinburgh Sacred Harmonic Society, presided over by its 
founder, the Rev. John Mackenzie. The choir numbered nearly 120 
voices; the conductor was as usual Mr. Geikie ; Mr. Hewlett presided 
at the organ, and Mr. Martin at the pianoforte. The first part of the 
concert consisted of selections from Spohr’s Die letzten Dinge (The 
Last judgment), the choruses in which were sung with a high degree of 
precision, delicacy and intelligence. The tenor solo and chorus, 
“ Holy, holy,” went remarkably well, the solo part being entrusted to 
an amateur, and the same may be said of the soprano solo and chorus, 
“ All glory to the Lamb that died.” The second and miscellaneous 
part of the concert opened with a worthy rendering of one of the finest 
choruses that Handel has written, ‘Sing, ye Heavens,” from Bel- 
shazzar, an Oratorio so seldom performed as to be almost unknown to

396

HuppersFIELD.—On the 13th ult., Dr. Spark, organist of the Leeds 
Town Hail, gave a lecture on ‘‘ Pianoforte Music, Ancient and Modern.” 
with illustrations. After a brief sketch of the history of music, the 
lecturer proceeded to describe the mechanism of the harpsichord, 
showing how it differed from the pianoforte ; and amongst the speci- 
mens given of the various styles of music written for these instruments 
were included “The Carman’s Whistle,” Scarlatti’s “ Cat's Fugue,” a 
Fugue by Bach, and various pieces by Handel, Mozart, Haydn, and

Beethoven, the playing of which drew forth much applause. At the 
close of the lecture the chairman (the Rev. R. Bruce), thanked Dr. 
Spark for the pleasure he had afforded to the audience. F

Huti.—The Organ at Coltman Street Wesleyan Chapel was opened 
by Mr. W. T. Best, on the 13th ult. The chapel was well filled and 
the selection of classical organ compositions performed by Mr, Best 
was much appreciated. The organ has been built by Messrs. Forster 
and Andrews, of Hull

THE MUSICAL TIMES.—Fepsruary 1, 1874. 397

the overture to Leonora, a new concert overture by Rietz, and Gounod’s 
overture to Mirella. Mrs. B. Porter was the vocalist, and was highly 
effective in the Scena from Der Freischiitz “‘ Softly sighs,” and Handel’s 
“Let me wander not unseen.” Madame Norman-Neruda secured 
agenuine success by her playing of Beethoven’s Romance in F, and 
(with Herr Straus) Bach’s Concerto in D minor. Mr. C, Hallé con- 
ducted, and was encored for his reading of Chopin’s Polonaise in A 
flat

Lynn.—The first concert of the Philharmonic Society for the present 
season was given in the Music Hall, on the 16th ult., the principal 
vocalists being Miss Matilda Scott and Mr. Stedman. The programme 
included a selection from Handel’s L’Allegro, both the solos and 
choruses in which were well rendered. Mr. Stedman received an 
enthusiastic encore for his singing of ‘‘ The Anchor’s weighed,” and 
one of the most successful efforts of the choir was in the part-song 
“Silent night,” which was warmly applauded. Mr. B. J. Whall was 
conductor, Mr. J. Bray, leader of the band, and Mr. W. O. Jones 
accompanists

MANCHESTER.—On the 26th December, Signor Giulio Perkin 
made his first appearance at Mr. Charles Hallé’s concerts, and sang 
the following airs, ‘‘ Se il regor e la vendetta” (La Fuive), “ Fallen is 
thy throne, O Israel” (Sir John Stevenson), and “Infelice” (Verdi), 
with much success. Signor Piatti was the solo instrumentalist, and 
was much applauded in his various pieces. His Concerto in D minor, 
performed for the first time here, and his smaller solos, “ Abendlied ” 
(Schumann) were exquisitely played. Mr. Hallé and Sigror Piatti 
gave most excellently the Tema con Variazioni in D major, by 
Mendelssohn. The band played Beethoven’s C minor Symphony ina 
faultless style, and also the overtures, Zauberfléite (Mozart), 
Mirella (Gounod), and Manfred (Reinecke).—— On the 2nd ult., 
Haydn’s Creation with Madame Lemmens-Sherrington, Mr. 
Edward Lloyd, and Signor Agnesi, as principal vocalists, drew a 
Jarge audience. Mr. Hallé conducted as usual. On the 8th ult., 
Madame Sinico was the vocalist, and sang in her accustomed charm- 
ing manner. Mr. Hallé played Litolf's Concerto Hollandais in E 
flat, No. 3, ‘‘ Das Lob der Thranen” (Schubert and Liszt), and “ II] 
moto continuo” (Weber). The Concerto proved most interesting. 
Herr Straus was the solo violinist and received a well-merited encore 
in Spohr’s Potpourri on Irish Airs. The overtures performed were 
Les Abencerrages (Cherubini), L’Eclair’: (Halévy), and Introduc- 
tion to Lohengrin (Wagner). The Symphony was Mozart's 
Parisian, in D, No. 9.——Ow the 15th ult., a very interesting programme 
was performed with the assistance of Madame Norman-Neruda, 
Signor Piatti, and Mr. Hallé. Mdlle. Gaetano was the vocalist. The 
chief atraction was Beethoven's Grand Triple Concerto in C major, 
(Op. 56) for pianoforte, violin, and violoncello with full orchestral 
accompaniments. This was played to perfection, and was heartily 
appreciated by the audience. The novelties of the evening were Raff's 
Grand Symphony, “Im Walde,” in F (first movement); the overture 
Lustspiel (Rietz), and a Ballad and Polonaise, in G, by Vieuxtemps 
for the violin——On the 22nd ult. a crowded audience was attracted by 
the new Oratorio, St. John the Baptist, by Macfarren, with the follow- 
ing principal vocalists: Mesdames Lemmens-Sherrington and Patey, 
Messrs. E. Lloyd and Santley. The performance was one creditable 
to all concerned.—— On Monday evening, the roth ult., Mr. W. T. Best 
gave an Organ Recital, at St. Peter's Church. The programme, 
which included compositions by Bach, Mendelssohn, Mozart, H. 
Smart, J. Lemmens, A. Guilmant, G. F. Hatton, W. T. Best, &c., 
was attentively listened to by a crowded audience

MeLBourNE, AUSTRALIA.—Six organ concerts have been recently 
given by Mr. Summers, in the Town Hall, with a success which shows 
how rapidly the taste for good music is spreading in Australia. A week's 
Festival is to be held at Easter, with a fine orchestra and an efficient 
choir, numbering nearly 1000 members. Amongst the works to be per- 
formed will be included Bach's Passion (St. Matthew), Israel in Egypt, 
St. Paul, The Seasons, and a new Oratorio by Mr. Summers

R1icHMOND, YorKS.—On Monday evening, the 5th ult., Mr. J. H. 
Rooks introduced for the second time Handel’s Oratorio, the Messiah, 
which was given in the Town Hall, by the members of the Parish 
Church Choir, assisted by several members of the Darlington Choral 
Society—altogether numbering about fifty performers. The pieces 
which were more particularly admired were ‘“‘ But who may abide,” 
“O thou that tellest,” “He shall feed His flock,” “‘ Come unto Him,” 
“But thou didst not leave,” ‘‘ How beautiful are the feet,” “‘I know 
that my Redeemer,” and “ The trumpet shall sound.” The singing of 
the Misses Young, Mrs. J. G. Croft, and Messrs. Greathead, Todd, and 
Burgin, was excellent. The choruses were most carefully rendered. 
An efficient band was engaged for the occasion. Mr. Marshall, of 
Darlington, presided at the pianoforte, and the concert was under the 
able direction of Mr. Rooks

Ripon.—The Church Institute gave a Soirée in the Town Hall, on 
Thursday, the 15th ult., at which the Lord Bishop presided, and the 
Marquis of Ripon delivered an address. ‘The members of the Cathe- 
dral Choir, with Mr. A. Ramsden and a few other friends, assisted by 
asmall band, performed a good selection of music, including Men- 
delssohn’s Sons of Art, Haydn’s rst Symphony, and the overtures to 
Masaniello and Clemenza di Tito. Mr. E.J. Crow, Mus. Bac., the 
newly appointed organist of the Cathedral, conducted, and played ina 
most able manner Beethoven's Sonata in D, Op. 10. The proceedings 
closed with a vote of thanks to the performers, and an expression of 
hope that Mr. Crow might enjoy a long, happy, and prosperous reign 
over Ripon Cathedral

SHEFFIELD.—On Christmas Eve Handel’s Messiah was performed in 
the Music Hall, Surrey Street, with Mrs. Holt, Madame Whitaker, 
Mr. Bywater, and Mr. Wood as principal vocalists. The choir con- 
sisted of the Sheffield Choral Society, and the Rotherham Hospital 
Musical Union, and the band of members of the Amateur Harmonic 
Society, assisted by several professional instrumentalists. Mr. C. 
Harvey, who originated the concert, must be congratulated on the 
success of the performance, the choruses being in great part given 
with much force and precision, whilst the singing of the principals 
gave great satisfaction. Herr Schollhamer officiated as conductor. 
—On Christmas Day, the Sheffield Tonic Sol-fa Choral Society, 
assisted by the Sheffield Choral Society, gave the same Oratorio, on 
which occasion the Music Hall was crowded in every part. The prin- 
cipal singers were Miss Helena Walker, Miss Moseley, Mr. A. Ken- 
ningham, and Mr. Brandon. The performance opened with “ Christians, 
awake,” accompanied by the free band and organ. Mr. Robinson 
(trumpet), and Mr. J. W. Phillips, as organist, played with their usual 
ability. Mr. Samuel Hadfield conducted. The Messiah was also 
given on the same evening in the Theatre Royal to a full house. 
——Many musical entertainments have been given in the new Albert 
Hall, foremost amongst which must be placed that in which Mr. Pyatt, 
of Nottingham, was the caterer on the 3rd ult, on which occasion the 
splendid band of the Grenadier Guards under the leadership of Mr. 
Dan Godfrey, gave a highly attractive performance. Madame Osborne 
Williams was the vocalist on the occasion, and sustained her part in a 
highly satisfactory manner. M.Guilmant,of Paris, on the 14th ult.,as well 
as on two previous occasions during the month, gave recitals on the 
splendid organ built by M. Coll,of Paris. The hall was crowded on 
each occasion, and the performer most effectively displayed the capa- 
bilities of this fine instrument. Mr. Charles Harvey on the 13th 
ult., inaugurated the first of a series of orchestral and ballad concerts, 
and must be congratulated on the success which attended this, the first 
of the series. ‘The vocalists were Miss Edith Wynne, Miss M. Severn, 
and Mr. Henry Guy. Mr. Charles Harvey was conductor.——On the 
22nd ult.,the directors of the Athenaum gave a concert before a crowded 
and appreciative audience. The vocalists were Mdlle. Titiens, 
Madame Sinico, Mdlle. Justine Macwitz, Signori Fabrini, Borella, 
Campobello and Giulio Perkin. Mdlle. Titiens was in fine voice, her 
“Ernani involami” was exquisitely given, and “ It was a dream ” was 
unanimously encored. The other singers were also highly successful, 
and M. Colyn, in his violin solos, created more than ordinary interest




DURING THE LAST MONTH. 


RGAN, PIANOFORTE, HARMONIUM, HAR- 


SIX PART-SONGS 


FOR FOUR AND FIVE VOICES, BY 


JACQUES BLUMENTHAL


EDWARD DAVIDSON PALMER. 


“WAKE, MAID OF LORNE.” 


A SET OF SEVEN SONGS. 


A BIRD SANG IN A HAWTHORN TREE. 


BLOSSOMS. 


RIPPLING WAVES. 


SING, NOR LET ONE NOTE OF SADNESS. 


SONG OF THE SEA BREEZE


THE VILLAGE CHURCH. 


PSYCHE: 


DRAMATIC CANTATA


FOR SOLO VOICES AND CHORUS. 


THE MUSIC BY J. F. H. READ. 


400


ECOND SERIES. 


HE ANGLICAN CHORAL SERVICE BOOK


USELEY AND MONK’S PSALTER AND 


THE PSALTER, PROPER PSALMS, HYMNS, 


ADDITIONS TO THE 


REY: T. HELMORE’S PLAIN SONG WORKS, 


SACRED MUSIC BY EDWARD LAWRANCE, 


OULE’S DIRECTORIUM CHORI ANGLI- 


OULE’S DIRECTORIUM CHORI ANGLI- 


HE ORDER FOR THE HOLY COMMUNION. 


SUNG BY MR. HENRY LESLIE'S CHOIR. 


G A. MACFARREN’S TWO-PART ANTHEMS 


CIX EA‘ 


OR LI


OR LI


STAINE! 


ONSOLATI 


EASTER ANTHEM. 


THE LORD IS MY STRENGTH


ANTHEMS SPECIAL AND GENERAL


) TO YOUNG COMPOSERS ABOUT TO PUBLISH


CRED SONGS, BY MILES BENNETT


COMPOSITIONS BY


SIR JOHN GOSS


HE CHURCH PSALTER AND HYMN-BOOK. 


THE VILLAGE ORGANIST


HE LORD IS RISEN INDEED, HALLE


MUSIC FOR THE COMING SEASON


AU MER, HENRY. —LORD, HOW ARE THEY INCREASED. 


OLCK, OSCAR.—O LORD, HOW LONG WILT THOU 


CAE 1. BAPTISTE.—OUT OF THE’ DEEP HAVE I 


IEMER, P. H.—HE WAS DESPISED AND REJECTED 


OUNOD, CHARLES. —THE SEVEN WORDS OF OUR 


LONDON: NOVELLO, EWER AND CO. NEW YORK: J.L. PETERS


TRY ME, OG


COME AND LET US RETURN. 


— I WILL LOOK UNTO THE LORD


NY


— TRY ME, O GOD


REND YOUR HEARTS. 


WE


LET US LIFT UP OUR HEART. 


——O GOD, WHOSE


OPKINS, E. 


— ALL WE LIKE SHEEP HAVE GONE ASTRAY. 


OURS, B.—IN THEE, O LORD, HAVE I PUT MY TRUS


ALMISLEY, T.A.—PONDER MY WORDS, 0 LORD. ¥ 


TTISON, T. MEE.--I DID CALL UPON THE LORD403

BEETHOVEN’S SONATAS

NEW AND COMPLETE EDITION). 
EDITED AND FINGERED BY AGNES ZIMMERMANN. 
In One Volume, Folio size, handsomely bound in Cloth, gilt edges. Price ONE GUINEA




PREFACE


KNOW MIN 


UNTO TH


JS RETURI cal score, 6d

NoTWITHSTANDING the many editions of Beethoven’s 
Sonatas that exist already, the present one will stand in 
need of no justification if it should prove a help towards 
the better rendering and clearer understanding of these 
great works. This it aims to be

Firstly: by the fingering. Many passages from their 
complication, present difficulties almost insurmountable 
to amateurs without some guidance, others again admit of




| UDGMEN! 


2 2 : 


A) 4. : 


I 1 L ea” = 2S —— 
; In both these examples the introduction of the Pedal-note

in the top part is so novel and beautiful, that it more than 
Compensates for the loss of the original form of the phrase 
~wherever similar instances occur (and there are many), 
no alteration is proposed; but where no such compensatory 
tlement exists, where it is plain that the mechanical 
limitations of the instrument alone prevented a complete 
teproduction of the original passage, such passage is here 
Pinted in the shape in which it would probably have been 
Witten, had the keyboard in Beethoven’s time had its 
Present extent. Such alterations are offered as sugges- 
tons only ; whoever prefers, can of course play the passage 
according to the original text

_ The places where the present edition varies from others 
M notes, and the reasons for such variations, will be found




DH2 
the left hand, instead of ees as in other

editions, since Beethoven evidently intended the strict 
imitation of the fugal subject, having even written 
perfect fifths to ensure this (see same page, line 4

bar 2 = 
i 
ee : f 
Page 360, line 4, bar 7, F instead of G in the bass, to




LONDON, NOVELLO, EWER AND CO., 1, BERNERS STREET (W.), AND 35, POULTRY (E.C


A NEW WORK FOR AMATEUR ORGANISTS


PRICE THREE SHILLINGS EACH


CHAPPELL & CO


THE ALEXANDRE NEW SEVEN-GUINEA ORGAN HARMONIUM. 


SOLID OAK CASE, FIVE OCTAVES, AND TWO FOOT-BOARDS


CHAPPELL & CO., 50, NEW BOND STREET


CH \FE


NEW WORK FOR SINGING CLASSES


CHAPPELL’S PENNY OPERATIC PART-SONGS. ‘ 


CHAPPELL & CO., 5, NEW BOND STREET, LONDON. 