


 XUM 


 TEXAS TECHNO SRT C 


 AB 


 EXHIBITION : ¥ 


 B. SQUIRE & SON 


 LONDO 


 PIANOS ORDER SUIT CLIMATE 


 BEARE~ SON 


 VIOLIN SPEGIALISTS 


 WRITE 


 SPECIALITY STRINGS 


 32 , RATHBONE PLACE , LONDON , W.I , TORONTO , CANADA 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 FOUNDED 1844 . 


 PUBLISHED MONTH 


 JUNE 1 , 1918 


 CHORAL SOCIETY 


 ROYAL 


 ROYAL ACADEMY MUSIC . 


 YORK GATE , MARYLEBONE ROAD , LONDON , N.W.-1 


 MAJESTY KING . 


 ROYAL COLLEGE MUSIC 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W.-7 


 ROYAL COLLEGE MUSIC PATRON FUND 


 ROYAL 


 MANCHESTER COLLEGE MUSIC 


 ARIE BREMA . 


 ROYAL COLLEGE ORGANISTS . 


 ASSOCIATED BOARD 


 R.A.M. R.C.M. 


 LOCAL EXAMINATIONS MUSIC 


 MAJESTY 


 GUILDHALL SCHOOL MUSIC . 


 PRINCIPAL ° LANDON RONALD . 


 BIRMINGHAM & MIDLAND INSTITUTE . 


 SCHOOL MUSIC 


 SESSION 1917 - 1918 


 UNIVERSITY DURHAM 


 ADVANCE PRICES 


 242 


 LONDON COLLEGE CHORISTERS . LONDON COLLEGE MUSIC . 


 UNIVERSITY COLLEGE , READING 


 DEPARTMENT MUSIC . 


 SCHOLARSHIPS MUSIC 


 FRANCIS H. WRIGHT 


 COLLEGE VIOLINISTS , LIMITED , 


 91 , ST . MARTIN LANE , LONDON , W.C .. -2 . 


 EXAMINATIONS 


 MADRIGAL SOCIETY 


 COUNTY BOROUGH HUDDERSFIELD . 


 RS . SUNDERLAND ” MUSICAL 


 COMPETITIONS . 


 PRIZES ORIGINAL MUSICAL 


 COMPOSITIONS . 


 JULY 


 VICTORIA COLLEGE MUSIC , [ 5 { .. 


 INCORPORATED GUILD CHURCH | ° ™ 


 MUSICIANS . 


 J. & W. CHESTER 


 1 ) , GREAT MARLBOROUGH STREET , LONDON , W.-1 


 HO 


 IC 


 N , W.-1 


 SIC 


 URCH 


 , D.D 


 ' L.1.G.C.M. ) , | 


 BLE CHANT 


 _ _ 


 , W.-1 


 243 


 


 GOOD PIANIST 


 CONSTANT “ PRACTISING 


 SEND ILLUSTRATED BOOK : 


 M. MACDONALD SMITH 


 TECHNIQUER 


 R.A.M. 


 MR . CHARLES TREE 


 PROFESSIONAL NOTICES . 


 MR . SAMUEL MASTE RS 


 TENOR 


 STUDENTS PROFESSION 


 COMPOSERS MSS . 


 REVISED PREPARED PRINTING . 


 EVISION MUSICAL COMPOSITIONS 


 PECIAL CORRESPONDENCE COURSES 


 1914 - 18 27 


 A.M. 


 F.R.C.O. SPECIALIST CORRESPONDENCE 


 244 


 ADAME LARKCOM , F.R.A.M. , 


 DR . LEWIS ’ TEXT - BOOKS 


 ARTHUR MANGELSDORFF , L.R.AM . , 


 LICHFIELD CATHEDRAL CHOIR 


 R.C.M 


 APPOINTMENT . — 


 OXFORD , NEW COLLEGE . 


 WAN TED , ORGAN IST CHOIRM ASTER 


 RGANIST CHOIRMASTER WANTED 


 ECOND - HAND - MANUAL PIPE 


 SALE.—RUMMEN ORGAN PEDALS 


 “ ORS & KALLMANN 


 W.-1 


 PUBLISHED SHORTLY . { LLU STRATED 


 STERNDALE BENNETT & SHEFFIELD 


 22 


 245 


 1 , 1918 


 TRINITY COLLEGE MUSIC 


 RT . HON . EARL “ SHAFTESBURY , K.P. 


 NEW COMPOSITIONS 


 GRANVILLE BANTOCK 


 SONGS . 


 TOMB UNKNOWN 


 WOMAN 


 FEAST LANTERNS . 


 


 OQOUICK STEP . 


 SHORT PIANO ALBUMS 


 FANCIES . ’ “ EVENING SKETCHES 


 REEL 


 LYRIC 


 0 , BEAK STREET , REGENT STREET , LONDON , W 


 BROADWOOD 


 PIANOS 


 BOSWORTH EDITION . 


 BRITISH COMPOSERS 


 MODERN PIANO ALBUMS . 


 


 BOSWORTH & CO . , , 


 PLAYED W 


 ANDANTINO 


 SQUIRE 


 COMPOSED 


 EDWIN H. LEMARE 


 ARRANGED 


 VIOLONCELLO PIANOFORTE 


 


 W. SQUIRE 


 JMS 


 > » RINCIPLES 


 RTE 


 247 


 SINGING - CLASS CIRCULAR . 


 JUNE 1 , 1918 


 ENGLISH SONG ‘ WENLOCK EDGE ’’ 


 248 


 249 


 250 


 ENGLISHWOMAN MEMORIES 


 DEBUSSY . 


 251 


 7- _ 


 PRINCIPLES MODERN COMPOSITION . 


 SF BF 


 252 


 PURCELL ‘ DIDO ANEAS , ’ 


 253 


 EVIDENCE EARLIER DATE 


 II.—EVIDENCE LATER DATE 


 254 


 COMPETITION 


 FESTIVALS 


 TOf 

 methods digress therefrom , 
 shereas old masters broke rules suited 
 slavishly follow footsteps 
 predecessors . Mozart , Beethoven , Wagner shocked 
 contemporaries . great composers found 
 rew style expression . progress , 
 stagnation . Academics critics ready 
 condemn progress . music - day , 
 — romantic , classical . Romantic music 
 classical dust ages settled 
 i. Composers create new , necessity 
 beautifal : true romanticist honest fullower 
 Beautiful according estimate . Mr. Scott 
 sid ‘ futurism ’ arrogant attitude mind , 
 pitfalls . present generation 
 uderstand . futurist , beauty importance . 
 romanticist recognised newness limits , 
 futurists newness limits . Advanced futurists 
 ‘ monsterists ’ Mr. Scott smote hard . attitude 
 ally deformity mind strove fcr novelty 
 harmonic device , regarded melody polyphony 
 antiquated worn - . futurist rely 
 comprehension future , tecomirg mere experi- 
 mentalist lost sight real functions art . Mr. Scott 
 cogently compared classicalist pedestrian 
 resolved roads , futurist 
 discarded roads roam fields . 
 romanticist man freedom 
 following leaving road pleased . Speaking 
 tonality , lecturer said ‘ ’ abolished 
 key - signatures , argued irregularity rhythm 
 tended variety . Freedom fourm necessity , 
 harmony - important . Bach great harmonic 
 inventer , Wagner , later Strauss 
 Debussy . Compared harmonic tissues 
 moderns ( lecturer ) consid « red classics sounded 
 somewhat thin obvious . classics appealed chiefly 
 old people , new school young . Asa 
 temperaie expression speaker strongly individual 
 views lecture enhanced selection Mr. 
 Scott picturesque pianoforte pieces songs . 
 require dust « f ages settle 
 classicalists readily appreciate charm — certainly 
 Mr. Scott plays . vocalist Miss 
 Marie Skellorn , Tubin School , sang taste 
 expression 

 thought harmonic 
 probings Mr. Scott shown taint ‘ futurism ’ 
 hedenounces . clear definition 
 constitutes ‘ futurism ’ music enables 
 include exclude modern composer 




 PROGRAMME MUSIC ORGAN . 


 257 


 CONGREGATIONAL SINGING 


 258 


 ORGAN RECITALS 


 VIOLIN MUSIC 


 ORGAN TRANSCRIPTIONS 


 MUSIC WALES 


 EDITOR ‘ TIMES . ’ 260 MUSICAL TIMES.—June 1 , 1918 

 Ki 
 criticism embodied portion evidence the| musical results — — — _ ordinary material Foe 
 Report Royal Commission attention is| female khaki elements mutually attractiy E 
 directed ; contend evidence sufficiently | assist regular attendances rehearsal . villag ~ 
 substantiated taken text , remarks based j 1,000 people , close camp number , th = 
 lose weight consequence . written | village — broken War , season beg aor 
 appears musical genius Wales ‘ dying } revived ; - balanced choir voices , 
 inanition . ’ far representing truth . | gave ‘ Messiah ’ Christmas Day ( Handel origin “ 4 
 intimate experience speak musical life Wales | accompaniments ) miscellaneous concert later , boi — [ L 
 past thirty years , testify great | good musical results enjoyment th estim 
 progress branches art . | singers . — & c. , hi 
 Details easily supplied , sufficient CHAPLAIN Forcss , heart 
 state number complete works performed Wales } Patrington , East Yorks . score 
 tenfold compared thirty years ago . wher 
 works include Bach ‘ St. Matthew ’ Passion , Beethoven } [ correspondent encloses programme * Pep 
 ‘ Choral Symphony , ’ Brahms ‘ Requiem , ’ Franck’s| Concerts given Society . eighteen items > exam 
 ‘ Beatitudes , ’ Elgar choral works , Bantock ‘ Omar | note following : Choral : ‘ Sing chaunt ’ ) myse 
 Khayyd4 m , ’ practically choral master- ( Pearsall ) , going lonely bed ’ ( Edwards ) jegen 
 pieces , obviously criticism standard ‘ Omar Hymn Sun , ’ male voices ( Goss ) , ‘ Th ? 
 performance elementary justified . Progress Snow ’ ( Elgar ) . Vocal Solos : * welcome ’ ( Goring ) 
 instrumental music phenomenal . standard | Thomas ) , ‘ Sea - wrack ’ ( Harty ) . /sstrumental : Violin Sona | evide 
 pianoforte organ playing incomparably higher , | ! 9 E minor ( Corelli ) , Andante Concerto ( Mendelssohn})| E 
 player thirty years ago - day . er ea 

 , populous districts South Wales creditable = 
 orchestra organized choral performances MANUAL 32 - FT . STOP , Etc . 
 difficulty . : ‘ en eae oe 




 MES 


 LIN 


 MES 


 ‘ PUBLIC SCHOOL SPIRIT 


 QUALIFICATIONS NECESSARY SINGER 


 LATYMER 


 ART TEACHING SINGING . 


 WOMEN 


 262 


 CHARACTER 


 TEACHER KNOWLEDGE 


 PHYSICAL TRAINING 


 TRAINING EXPRESSION EMOTION 


 KNOWLEDGE NECESSARY 


 TEACHERS 


 PHYSIOLOGICAL 


 TESTING CERTIFICATION CAPACITY 


 INTERPRETATION STANDARD WORKS 


 264 


 VLADMIR ROSING GREEK MUSIC 


 SONG POOR WANDERER 


 NIDES 


 ACOUSTICAL PROPERTIES WIND 


 INSTRU MENTS 


 MASTER SYDNEY NOKES 


 PROFESSIONAL CLASSES WAR RELIEF COUNCIL : 


 HELP GIVEN MUSICIANSMadame d ’ Alvarez gave interesting recitals 
 4 . emotional intensity , usual , strong Frid 
 appeal . appropriately applied ay 
 ‘ Agnus Dei , ’ Bach B minor Mass , 
 Debussy ‘ Air de Lia . ’ Queen Hall Orchestra , 
 Sir Henry Wood direction , accompanied 
 perfection . 

 Mr. Victor Benham , emboldened success les — organi : 
 spacious arenas , gave recital hall 15 . se 
 played works Chopin , Liszt , Rubinstein , Beethoven , 

 Schumann , Bach , conspicuous ability . Polyte 
 permes 
 ample 

 LONDON STRING QUARTET 

 new series Saturday afternoon concerts W & ) areinfo 
 commenced 4 . Mr. J. B. McEwen ‘ Biscay | Beatric 
 Quartet welcome item . 11 , Beethoven ¢ Delmot 
 played , assistance players , an¢ 
 notably Miss Olga Haley , freakish work Stravinsky , 
 entitled ‘ Pribaoutki , ’ repeated . Trio viola , Ris . 
 oboe , pianoforte , Miss Elsie Hamilton , display ’ Gg ... , 
 lady gifts advantage . Mozart Quartet E fit thre 
 principal item . 18 , Beethoven . 3 , veste 
 Op . 18 , minor , Op . 50 , Pianoforte Trio d 
 Tchaikovsky ( Myra Hess pianoforte ) , jo ... 
 pieces Eugéne Goossens , chief items . ar 
 playing excellent . Applica 

 April 27 , Mile . Raymonde Collignon gave ' ® date 
 unique recitals ‘ acted ’ songs . brought forwards§ Organis 




 267 


 ROYAL CHORAL SOCIETY 


 ALEXANDRA PALACE CHORAL SOCIETY 


 CARL ROSA OPERA COMPANY 


 TRIBUTE ALGAROTTI 


 FREDERICK GREAT MEMORIAL ALGAROTTI 


 268 


 SALE LATE MR . A. H. LITTLETON 


 LIBRARY 


 TRINITY COLLEGE 


 ASSOCIATED BOARD LOCAL 


 EXAMINATIONS 


 CARNEGIE TRUST . 


 Y.M.C.A. APPEAL MUSICIANS 


 62 6 6 


 ORGA 


 S = SS SS SS SS 


 £ 1,700 . , — 


 XUM 


 -3 - 4 


 _ SS 


 ADORATION BENDING 


 90 - 5 


 — — — — — —   _ — — _ 


 11 


 ADORATION BENDING 


 SSS SS SS = ES = 


 273 


 TORONTO MENDELSSOHN CHOIR 


 MR . H. A. FRICKER 


 CONCERT 


 GOD SAVE KING , CLASSICISM FALSE VALUES 

 meeting Musical Association April 16 
 paper subject Mr. G. H. Clutsam read 
 author absence Mr. Edwin Evans . Mr. Clutsam 
 suggested placed unduly high value work 
 come category classical . 
 deny anybody right enjoy fullest 
 actually delight , mind Mozart , Haydn , 
 early Beethoven extravagantly - praised . 
 modern composer possibly content 
 limited means expression disposal old 
 masters .. hand , large proportion 
 musical electorate , hypnotised educated processes 
 critical , found infinite expression , melodious grace , 
 unapproachable symmetry , imaginative genius , good- 
 ness knows unassailable virtues 
 ordinary vision appear genius 
 helpless state crudity . section , fully appreciating 
 historical aspect question , found compelled 
 battle natural cause development , generous 
 logical development proceeded apace 
 knowledge present generation . , unfortunately , 
 opposed larger section , honestly , 
 recognise fact music stood Bach , 
 Beethoven , Wagner , established , 
 exclusive minds , point art received 
 fullest expression . neutrals 
 pretended enjoyment things , position , like 
 neutrals , logically tenable . power 
 majority section strongly reinforced vote 
 entrusted education young aspirant 
 honours creative world 

 criti¢s dogmatising , work 
 creator establishing new formule 
 pretensions insight analysis considerably 
 gear . Harmony , rhythm , form , style — altogether 
 general significance purport musical expression 
 established classics , suffered severe shaking , 
 disintegration , course thirty years . 
 particular divergence thought musical circles 
 beginning period mainly concerned 
 denying upholding power music illustrative 
 medium . brave attempt distinguish 
 absolute -- , classical — romantic music . 
 ultimate result argument defender 
 absolute classical faith time onward 
 driven violent literary endeavour impose 
 emotional programme — find reasonable 
 opportunity — definitely formal music ; , 
 far composer concerned , obviously 
 literary programme . question Bach 
 Spitta , Schweitzer , 
 , like questioning authenticity 
 Gospels . music emotionally analysed 
 bar bar , astounding musical thought discrimination 
 discovered utterances fine old fellow 
 possibly claimed day work . 
 position classics midst 
 attributed enthusiastic literary endeavours well- 
 meaning d / ettanti , technical grasp situation 
 measured technical equipment particular 
 protégés . accept work expression 
 period , able admit 
 adjunct performances period . Interest 




 7 + ‘ . . 7 


 MILAN . 


 ( CORRESPONDENTThe ‘ Madrigal Group ’ Royal Conservatorium , 
 conducted Maestro Bartoli , gave interesting concer 
 April 14 Societa del Canmtatte . Numerous select 
 pieces executed repertory second hal 
 16th century half 17th century 
 madrigals , motets , songs . applause met 
 execution ‘ Villanelle ’ ‘ Ballate , ’ th 
 graver forms , including fine Motets 0 
 Palestrina 

 ( Juartette Society gave fourth concert hall th 
 Royal Conservatorium April 22 . performers wer 
 Lonati ( pianoforte ) Signorina Spera ( violin ) . Lonati , 
 established favourite public , best wher 
 playing Mozart Concerto . 6 , D minor . Signorim 
 Spera played Concerto Nardini , accompaniment 
 stringed instruments organ ; Paganini Concerto . | 
 pianoforte accompaniment ; Mozart Rondo G 
 Beethoven Romanza F , minor pieces th 
 17th century . Signorina Spera fine violinist , possessing 
 exceptional technique . Lonati heartil 
 applauded . accompanying orchestra conducts 
 Maestro Polo 

 Signorina Antonietta de Isaia Lanzarini , comig 
 female pianist - composer , exceptionally active late ! 
 concert conferences cities Italy . com 
 memoration Claude Debussy gave Musical Conference 
 Bologna , April 28 , aid Signorina Ans 
 Lagarde , vocalist . programme consisted entirely 
 Debussy : songs seven pianoforte pieces , ei 
 Signorina de Isaia preceded short explanator 
 converse . young lady genius , fact 
 woman great handicap forging 
 successfully musicalcircles Italy , female intrusi # 
 appear acutely resented judging advert 




 * .CESARI 


 275 


 ROME . 


 LAUDI SPIRITUALE . 


 ( 4 ) M ) 


 RETURN LORENZO PEROSI 


 XUM 


 SS = = 


 CENTENARY ROSSINI ‘ MOSE 


 BERNARDO PASQUINI 


 LEONARD PEYTON 


 277te . Se Overture opera ‘ Lurline ; se .. ' , Wallace | orchestral compositions gave promise great 
 enon Si titties * beeen inne KF ee future . young composer striving create idiom 
 ee Ballad ( Op . 47 ) . ilies : 1 C Mackensie | 0f , compositions heard recital 
 San Cathe , 8 s thine aie : 9 , trio excepted , cast sombre mould . 
 : ana Miss Betta Epwarps , pianoforte . vocalist Dr. Tom Godfrey , instrumentalists Mr. 
 eer rer Variations Orchestra aa Edward Elgar Fenney , Mr. Manton , Mr. T. Henry Smith , Mr. Percy 
 Ge Celebrated Airs ( transcribed Violin ) . Orchestrated Hall . Mr. Leonard Rayner , scholarly 
 simph ine ae ae — accomplished pianists Midlands , gave concert 
 s ‘ Miss E\ uboccl . 58 - 1695 ° , x 

 merriment Serenade small orchestra ane Percy Pitt | Midland Institute 13 , assisted Mr. Horace Lott , 
 ‘ save thi ] Rule , Britannia ‘ = Dr. Thomas Arne | Vocalist , Lieut . A. K. Blackall , accompanist . 
 4 + 7 di 1710 - 1776 pianoforte solos included Schumann rarely - heard Sonata 
 es F God Save King . . — = Bull |G minor , Op . 22 , Beethoven Andante F , Brahms 
 f « 7= : ; ee Ay Capriccio , Op . 76 , Schumann Novellette D , Chopin 
 0 Mose , Conductor : M. Leon Jenin , Maitre de Chapelle H.S.H. Nocturne C minor , Impromptu flat , Besceuse 

 1e COMposer Prince Monaco . . ; ’ mp Pp ms 
 : Scherzo B flat minor , Op 31 . performance 




 75 


 MUSICAL TIMES 


 JUNE 1 , 1918 


 BRISTOLCAMBRIDGE 

 8 Miss Rosamond Ley M. Désiré Defauw 
 gave pianoforte violin recital Guildhall . Miss 
 Ley played ‘ Sposalizio , ’ * Lighting Candles , ’ * Chimes , ’ 
 Old Provengal Carol Capriccio E minor , Liszt , 
 M. Defauw , Bach Sonata violin pianoforte , 
 major , Beethoven Kreutzer Sonata . M. Defauw 

 F played Chaconne G minor Vitali . concert 
 auspices University Musical Society 




 DEVON CORNWALL 


 DEVON 


 CK 


 KS 


 1 D ) 


 .. G.A. 


 279 


 CORNWALL 


 EDINBURGH 


 LIVERPOOL 


 1S 


 280 


 MANCHESTER DISTRICT 


 281 


 1918 . 


 PUBLISHED 


 H. W. GRAY CO . , NEW YORK 


 VALSE IMPROMPTL 


 ALFRED HOLLINS . 


 


 B RIERLEY\ , W. B 


 MONTH . 


 F * * PERCY 


 SPECIAL NOTICE 


 MUSICAL TIMES 


 SCALE TERMS ADVERTISEMENTS 


 ORK 


 TRESS 


 MUSICAL 


 ANTHEMS 


 


 TRINITYTIDE 


 COMPLETE LIST 


 “ LUTE ” SERIES 


 PP PY SET ! PS 


 NOVELLO 


 ANTHEMS 


 HYMNS TUNES 


 MORNING SONG ICHANSON D'ESPOIR 


 4 


 COMPOSED ~ T 


 ALFRED HOLLINS . ORGAN 


 — — COMPOSED 


 1 J. A. MEALE , 


 


 ™ SCHERZO 


 ALFRED HOLLINS . 


 ORGAN 


 C. HUBERT H. PARRY . 


 F I\ E \ RIA ION » ORIGINAL COMPOSITIONS ORGAN 


 OIR 


 RGAN 


 GAN 


 DEAR GOLDEN DAYS 


 SONG 


 P. J. O'REILLY 


 FRANK JEPHSON . 


 CHARMING CHLOE 


 WORDS 


 ROBERT BURNS 


 MUSIC 


 EDWARD GERMAN . 


 ALANNAH 


 SONG 


 WORDS 


 COUNTESS BARCYNSKA . 


 MUSIC 


 EATON FANING . 


 KEYS . 


 HEART BLOSSOM 


 SONG 


 ETHEL M. DE FONBLANQUE 


 EATON FANING . 


 


 SOUTHERN SLAV SONGS 


 1 . SHEPHERD HILL . 


 2 . THOUGHTS . 


 SRGJAN TUCIC 


 FANNY COPELAND . 


 PERCIVAL GARRATT 


 KING EDWARD EDITION 


 CATHEDRAL PRAYER BOOK 


 OLD 


 ENGLISH VIOLIN MUSIC 


 EDITED 


 ALFRED MOFFAT 


 PREFATORY NOTE 


 PUBLISHED 


 HIAWATHA WEDDING - FEAS ] 0 


 ARRANGED PIANOFORTE SOLO 


 JOHN POINTER . 9 


 1 . LE 


 PRICE SHILLINGS SIXPENCE . — ’ 


 16 , 


 4 1 . VI 


 22 


 VALSE BOHEMIENNE . 3 . VALSE DE LA REINE . : 


 4 . FI 


 _ ‘ 25 . ° 


 COMPOSED 2 » . B 


 S. COLERIDGE - TAYLOR . 8 , SI 


 29 


 PIANOFORTE SOLO : 


 PROGRESSIVE STUDIES 


 S ] PIANOFORTE 


 EDITED , ARRANGED GROUPS , FINGERING REVISED SUPPLEMENTED 


 FRANKLIN TAYLOR 


 - BOOKS , PRICE SHILLING . 


 


 SELECTED PIANOFORTE STUDIES 


 SONG - WRITER WORK DELIGHTS WORLD 


 CARRIE JACOBS - BOND 


 NEW SONGS 


 SOUL 


 COTTAGE GOD GARDEN 


 FREDERICK HARRIS CO , 


 MUSIC PUBLISHERS , 


 40 , BERNERS STREET , LONDON , W.-1 


 MONTHS 


 SKETCHES PIANOFORTE 


 


 FREDERIC H. COWEN 


 BOOKS 


 CONTENTS 


 BOOK I. 


 ARRANGEMENTS SMALL ORCHESTRA 


 COMPOSER 


 ROYAL ACADEMY MUSIC 


 YORK GATE , MARYLEBONE ROAD , N.W.-1 


 INSTITUTED 1822 INCORPORATED ROYAL CHARTER , 1830 


 SUPERB GRAND : UPRIGHT 


 PIANOFORTES 


 PIANOS INSPIRE 


 MARSHALL ROSE . ; ‘ 


 TH E PURCELL SOCIET Y 


 FOLLOWING VOLUMES PUBLISHED : —