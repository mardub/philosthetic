


 MUSICAL TIMES 


 SEPTEMBER 1 , 1870 


 NORTH LONDON FESTIVAL CHOIR 


 TOKE NEWINGTON ASSEMBLY ROOMS 


 BIRMINGHAM 


 MUSICAL FESTIVAL , 1870 


 READY 


 JULES BENEDICT ... 


 ORATORIO 


 ST . PETER 


 COMPOSED EXPRESSLY 


 FESTIVAL 


 LONDON : NOVELLO , EWER CO 


 BIRMINGHAM 


 MUSICAL FESTIVAL , 1870 


 READY 


 DR . F. HILLER 


 DRAMATIC CANTATA 


 NALA & DAMAYANTI 


 COMPOSED EXPRESSLY 


 FESTIVAL 


 LONDON : NOVELLO , EWER CO 


 PROFESSIONAL NOTICES . 


 MADAME MONTSERRAT 


 USIC ENGRAVED , PRINTED , PUB . 


 USICAL LNSTRUMEN'IS FORK VULUN.- 


 EYOND ALLCOMPETITTION.—T. R. WLLLIS 


 W. SNELL IMPROVED HARMO 


 RASS , REED , STRING , DRUM 


 MRS . E. B. HARPER 


 W. HATTER#LEY , 


 HARMONIUM MANUFACTURER , 


 95 , MEADOW - STREET , SHEFFIELD . 


 DEASANT HARMONIUM MANUFACTORY , 


 579 


 NEW VOCAL MUSIC . 


 AUTHORISED MUSICAL TEXT - BOOK KUGBY SCHOOL . 


 SINGING MANUAL 


 CONGREVE HARVEST SONG , 


 EUROPEAN PSALMIST 


 PUBLISHED SUBSCRIPTION 


 MORNING EVENING 


 


 SAMUEL SEBASTIAN WESLEY 


 PRICE SUBSCRIBERS . 


 , EE PUBLISHED SUBSCRIPTION , AUTHOR 


 HARVEST ANTHEMS , 


 BREAVINCTON SONS , 


 WHOLESALE 


 AGENTS 


 MASON HAMLIN 


 PRIZE & GOLD MEDAL CABINET ORGANS 


 CHICKERING 


 7 ; OCTAVE AMERICAN GRAND 


 SONS ’ 


 UPRIGHT GRAND PIANOFORTES 


 BC . 


 > CHARACTERS REPRESENTED 


 ACT 


 ACT II . 


 TIME PERFORMANCE HOUR MINUTES 


 W. H. BIRCH , 104 , LONDON STREET , READING 


 READY , SECOND EDITION , 


 SINGING CARD 


 USE CHOIRS , CHOIRMASTERS , SCHOOLS , 


 METZLER CO . 


 NEW PUBLICATIONS 


 NEW ILLUSTRATED EDITION 


 MESSAGE BATTLE FIELD 


 WAR SONGS FRANCE GERMANY . 


 \PETZLER CO STANDARD EDITION 


 ARCHING BAND . 


 NEW COMIC OPERA CH . GOUNOD . 


 \ ETZLER CO . SHILLING OPERA 


 87 , GREAT MARLBOROUGH STREET 


 MR . H. ASHTON 


 MR . T. PEARSON 


 MR . J. F. MEEN 


 MR . J. RUDKIN , RAM . 


 ASSISTED 


 MISS FANNY DANIELSON 


 MN\HE LONDON ORPHEUS QUARTETT 


 OTTINGHAM MECHANICS ’ INSTITUTION 


 SATURDAY POPULAR CONCERTS 


 SOLO INSTRUMENTALISTS VOCALISTS 


 | QO CONCERTINA ‘ TUNERS . — WANTED , 


 583 


 MUSICAL TIMES , 


 SEPTEMBER 1 , 1870THE LONDON MUSICAL SEASON . 
 Henry C. Lunn 

 Tue hopeful signs progress music 
 Eng!and , means numerous sanguine 
 enthusiasts lead imagine ; 
 separate worshippers art worshippers 
 artists , shall find class 
 sad minority . record past session thrown 
 conventional language usual resumé , 
 merely recapitulation 
 seasons , set 
 type beginning year , blanks 
 left filling names dates . Certainly , 
 Opera - houses opened , old estab- 
 lished Musical Societies given stipulated 
 number concerts , 
 fashionable world requires . shall 
 turn proof appreciation highest 
 class music steadily increase ? 
 Opera favourite singers favourite parts 
 real attraction , whilst ‘ ‘ Medea , ” 
 “ Fidelio , ” given 
 classicalists long ; hitherto 
 unknown Operas , ‘ L’OQca del Cairo ” “ Abu 
 Hassan , ” played times 
 comparatively houses . true Wag- 
 ner Opera , ‘ Der Fliegende Hollander , ” 
 accepted welcome novelty ; Cam- 
 pana ‘ * Esmeralda ” Ambroise Thomas ‘ Mig- 
 non , ” works chosen 
 management accustomed cater audience 
 highly cultivated taste . popular Oratorios , 
 operatic singers filled concert - rooms over- 
 flowing , whilst comparatively unknown sacred works 
 left struggle notice principle , 
 presume , love art virtue 
 reward . exception 
 fine performance Mendelssohn ‘ * Antigone ” 
 Mr. Henry Leslie choir , single presentation 
 Beethoven Mass D — altered suit re- 
 quirements Sacred Harmonie Society — 
 “ Oratorio Concerts ” shown activity 
 performance - worn works 
 years haverepresented classical element ; 
 interest excited 
 music - loving , revival concerts 
 greatest compositions master 
 minds creative art , production new 
 Sacred Cantata , modern composer , 
 unconscious unusual occurrence 
 disturbed flow London “ Musical 
 Season . ” steady perseverance 
 good cause eventually alter state things , 
 impossible predict ; , meantime , 
 let believe taste musical England 
 1s rapidly improving , simply long 
 fashion . fact stated 
 undeniable — good , , effected 
 refusing believe , harm 
 boldly stating 

 Royal Italian Opera , pertinacity 
 Madlle . Sessi forward 
 parts identified artists actually 




 584Journées”—a work long waited 
 hear perfect form ; , ( considering Mr. 
 Santley ready Michel , water- 
 carrier ) , admirably adapted company . 
 Let hope thatso welcome revival delayed 
 season . , thank 
 management revivals Mozart “ L’Oca del 
 Cairo , ” Weber * ‘ Abu Hassan ; ” , 
 bravely venturing Opera abused , 
 abusing composer , Wagner , un- 
 measured defiance critics , evidence 
 possession genius , means proof 
 want . line unqualified praise 
 admirable manner Signor Arditi 
 condueted season , 
 conclude notice Mr. Wood operatic 
 campaign 

 concerts Philharmonic Society 
 thoroughly maintained character instru- 
 mental department ; ensure appearance 
 higher class vocalists , help thinking 
 definite arrangementsshould thecom- 
 mencement season , names eminence 
 announced prospectus . 
 disposition engage - known singers 
 concert , procured 
 days ’ notice ; second rate artists 
 constantly pressing hearing , 
 great danger greater deterioration 
 vocal department programmes , 
 system mentioned adopted . 
 , let heartily praise Directors 
 giving excellent final concert ‘ ‘ honour 
 Beethoven . ” Certainly , Society right 
 represent feeling England occasion ; 
 , apart having means introducing 
 Beethoven works country , 
 immortalised voluntarily giving substantial 
 aid great composer hour sickness 
 need 

 ‘ New Philharmonic ” Concerts , 
 “ Monday Popular ” Concerts particular 
 notice , save line commendation efficient 
 manner conducted ; 
 ‘ ‘ Sacred Harmonic Society , ” 
 production Handel - neglected 
 Oratorio , ‘ ‘ Deborah , ” performance 
 mutilated version Beethoven Mass D , 
 shown desire introduce novelty 
 programmes 

 Mr. Henry Leslie given excellent 
 concerts season , choir 
 principal attraction — performance 
 Mendelssohn music ‘ Antigone , ” especially , 
 success easily forgotten — 
 taken field concert- 
 giver extensive scale , principal singers 
 Opera engaged , fashionable , 
 musical , portion London public 
 appealed programme Italian 
 music , reminding olden days ‘ Benefit 
 Concerts . ” Oratorios , principal parts 
 sustained Operatic vocalists , 
 given Mr. Leslie direction , 
 attracted large audiences 




 HEREFORD MUSICAL FESTIVAL 


 587 


 SE 


 GES = SS SS SSS 


 7 - 6 = FS — INS NE SS SS 


 REDBREAST 


 TT = _ — - ' 1 


 ] 4 


 SS SS 


 SS 


 SS SSS 


 SJ = “ 545 


 SSS 


 255 


 J J } 


 = SS 


 22S SSS SS SS SF SS 


 J XN ; = | | { 


 1 — _ _ ~ , NY 


 = = = > = S223 


 REDBREAST 


 5 5 SS SS 


 595 


 596The Cuckoo . Ballad . Poetry Wordsworth . 
 Composed William Pinney 

 Ir great benefit composers cuckoos 
 decide song ( 
 called ) , drop minor major , some- 
 thing . true cuckoo - clocks 
 resolve question doubt ; Beethoven , 
 ‘ Pastoral Symphony , ” given major 
 true song ; musical listeners rarely hear 
 interval bird , presume 
 matter remain doubt . Mr. Pinney 
 written minor cuckoo men- 
 tioned , effect , , absurd verse 
 word “ unto , ” composed 
 pretty melody Wordsworth suggestive verses , 
 merit extreme simplicity . excellent 
 manner harmonised deserves 
 word commendation 

 Brewer Co 




 VOLUNTARY CHOIR SERVICE . 


 EDITOR MUSICAL TIMES 


 TL 


 CORRESPONDENTS 


 O PIANOFORTE MUSIC TRADE 


 MONTH , 


 


 LAND PROMISE 


 FRANCIS HOWELL 


 BIRMINGHAM FESTIVAL 


 CHORAL SOCIETY 


 IVE UNTO LORD GLORY . 


 REV . ROBERT CORBET SINGLETON , M.A. , 


 WARDEN ST . PETER COLLEGE , RADLEY , 


 ORGANIST CHOIRMASTER YORK MINSTER 


 ANGLICAN CHORAL SERVICE BO OK , 


 USELEY MONK PSALTER 


 OULE COLLECTION 527 CHANTS , 57 


 PSALTER , PROPER PSALMS , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 ORDER HOLY COMMUNION , 


 TONIO SOL - FA RDITION 


 LT 


 READY , 


 UPPLEMENTAL HYMN TUNE BOOK 


 ERIES MODERN KYRIES , HYMN 


 DR . 8 . 8 . WESLEY NEW SERVICE . 3 


 BAPTISTE CALKIN EASY MORNING 


 USELEY.—A CHANT SERVICE 


 DWARD HERBERT CHANT TE DEUM 


 CHORAL FESTIVALS . 


 SIMPLEST FORM INTONED SERVICE.—THE 


 ACH - PRELUDES NOVELLO OCTAVO EDITION 

 BEETHOVEN MASS D 

 Vocal Score , Pianoforte Accompaniment . Paper 




 NOVELLO , EWER CO . , 


 1 , BERNERS STREET ( W. ) , 85 , POULTRY ( £ .C 


 WORDS SELECTED HOLY SCRIPTURES . 


 COMPOSED 


 I. II . 


 LONDON 


 H 


 


 EDITED 


 WM . SPARK , MUS . DOG 


 CONTENTS . 


 NEW VOLUME . 


 ORGAN 


 ARRANGED JOHN HILES . 


 BOOK XIX . 


 BOOK XXI . 


 BOOK XXIII . 


 ‘ BOOK XXIV 


 BOOK XXV . 


 BOOK XXVI . 


 BOOK XXVII . 


 CONTENTS VOL , 


 NOVELLO , EWER & CO . , 


 1 , BERNEBS STREET , ( W. ) 85 , POULTRY ( B.C 


 CONTENTS VOL . II . 


 CONTENTS VOL , III . 


 LONDON 


 REREREERERES 


 SEEF 


 INSTRUCTION BOOKS ORGAN 


 W. T. BEST 


 JOHN HILES 


 NEUKOMM 


 RINK . 


 SCHNEIDER . 


 CHARLES STEGGALL 


 LONDON : NOVELLO , EWER CO 


 MODERN 


 COMPOSERS 


 PUBLISHED NOVELLO , EWER CO 


 F. ARCHER . 


 W. T. BEST . 


 J. BAPTISTE CALKIN . 


 E , T. CHIPP 


 J. BARNBY . 


 CHARLES COLLIN 


 R. W. CROWE 


 R. FIELDWICK . 


 F. E. GLADSTONE . 


 ROBERT HAIN WORTH 


 C. 8 . JEKYLL . 


 J. LEMMENS 


 LEFEBURE - WELY 


 FRANZ LISZT . 


 MENDELSSOHN . 


 W. J. PRICHARD . 


 W. H. SANGSTER . 


 E. SILAS . 


 HENRY SMART . 


 CHARLES E. STEPHENS . 


 J. T. STONE 


 E. H. THORNE 


 C. G. VERRINDER 


 NEW WORK ORGAN 


 CHAPPELL ORGAN JOURNAL 


 CONSISTING FAVOURITE MOVEMENTS SELECTED 


 


 EMINENT ORGANISTS 


 CHAPPELL & CO . CHEAP PUBLICATIONS 


 INSTRUMENTS . 


 CHAPPELL INSTRUCTION BOOKS 


 VIOLIN , CLARIONET . SERAPHINE ANGELIVA , 


 FLUTE . HARMONIUM . DRUM FIFE , 


 CORNET . SINGING . VIOLONCELLO , 


 ENGLISH CONCERTINA . HARMONY . BANJO . 


 GERMAN CONCERTINA . GUITAR . TROMBONE . 


 PIANOFORTE , SAX - HORN 


 FLUTE 


 CORNET - - PISTON . 


 GERMAN CONCERTINA 


 ENGLISH CONCERTINA . 


 HARMONIUM 


 4 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDOW