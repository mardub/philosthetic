


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


FOUNDED IN 1844. 


PUBLISHED ON THE FIRST OF EVERY MONTH


ROYAL CHORAL SOCIETY. QUEEN’S HALL. 


| PROME NADE CONCERTS


ROY AL ACADEMY OF MUSIC, W


YORK GATE, MARYLEBONE ROAD, N.W. SEPTEMBER, 1911. 


THE ROYAL COLLEGE OF MUSIC, MANCHESTER COLLEGE OF MUSIC 


PRINCE CONSORT ROAD, SOUTH KENSINGTON, S.W


SCHOOL OF MUSIC


562


LONDON, “CON 


MUSICIANS. S


PRELIMINARY ANNOUNCEMENT. 


REGISTER OF ORGAN V ACANCIES. A COMPETITION FESTIVAL _ 


VICTORIA COLLEGE OF MUSIC


LONDON


THE


SHERWIN’S 


ACADEMY


ADAME AMY 


\ VOCAL 


CONCERT BUREAU, 


213, REGENT STREET, W


“SHEFFIEI D CHOIR EMPIRE TOUR. 


CONCERT BUREAU 


ORRESPON DENCE SCHOOL OF MUSIC, 


27, BERNERS STREET, LONDON, W 


GRADUATED POSTAL LESSONS IN HARMONY, | 


COUNTERPOINT, ELEMENTS OF MUSIC, FUGUE, FORM 


PIANO PEDALS


R. H. P. COLEMAN, 


THE 


VIOLIN, VIOLA


AND


VIOLONCELLO BOWS


MADE BY


W. E. HILL & SONS


MU


NDON, 1862


SOCIETY OF


85. 


EXHIBITION, 1889. 


EXHIBITION, 1897


W. E. HILL


SONS, 


140, NEW BOND STREET, LONDON, W


SIC AND MIRACLES


DO YOU NEED TO


PRACTISE YOUR MUSIC


BEFORE YOU CAN


PLAY IT SATISFACTORILY? 


L. M. EHREMAYER, 27, CHANCERY LANE


EHREMAYER SYSTEM OF 


SIGHT-PLAYIN


THE 


PIANOFORTE


THE MUSICAL TI


564


MES.—SEPTEMBER I, IQII


PROFESSIONAL NOTICES. 


MISS EDITH NUTTER


CONTRALTO


A.R.A.M


MR. ERNE :ST PENFOLD


N.W


MR. JACKSON POTTER © 


MR. W. H. BREARE


BARITONE). 


QS ESTELLA LINDEN


MISS MARION P ERROTT


DRAMATIC SOPRANO). 


MISS AGNES W AL KE R


SOPRANO


M [SS DAISIE E. AVIS 


MISS LYDIA JOHN (LRAM


MR. JOHN


BOOTH


TENOR), 


MR. SAMUEL MASTERS


MR. MONTAGUE


BARITONE


BORWELL


MISS 


WINIF RE D MARW OOD 


HARROGATE


MISS ELLEN CHILDS _


CHROMATIC HARPIST) A 


SOLO PIANIST). 


160


THE LONDON COLLEGE FOR CHORISTERS. 


COMPOSERS’ MSS. 


H. ELLIOT RUTTON


D AL L ISON 


MUS.D


DEGRE . S OF 


‘| EVISION OF MUSICAL COMPOSITIONS. 


L.R.A.M. 


NGING


20


DRED SUCCESSES; A.R.C.M., 1897-1910, TWO HUNDRED


IM


COUNTERPOINT


DR. LEWIS’ TEXT-BOOKS : 


A.G.S.M., A.R.C.M


R. CECIL MONT AGUE


HEORY BY POST.—LOW FEES


A.R.A.M., F.R.C.O.), _PIANOFORTE, HARMONY, 


COMPOSITION, ORCHESTRATION


FRANK 


RECENT SUCCESSES


THE


THE


MANAGER, 40


NGAGE


ALE, 


RGAN 


IANOS WANTED 


FIRGIL PRAC 


>OR SALERATIS.—“ Diagrammatic : View of Principal 
J Har Rises and Falls in the First Movement of a Sonata in 
y Form. With Explanatory Text, Musical Illustration, and Refer

to Beethoven's Pianoforte Sonatas Free to music students 
A. C., St. Cecilia House, Balfour Ri ad, Wimblex s.W

VERY




ESTABLISHED 1750


GRAY & DAVISON, 


ORGAN BUILDERS, 


PRATT STREET, N.W 


48, SOUTHFIEI D RO AD, OXFORD


72, UPPER PARLIAMENT STREET, LIVERPOOL. 


THE OLD FIRM


P. CONACHER & CO


SPRINGWOOD WORKS 


HUDDERSFIELD


POSED BY


EDW ARD ELGAR


FOR


THE MUSICAL TIMES


SEPTEMBER I, IQII. 567


SERE ENADE


FOR SMALL ORCHESTRA


PERCY PITT


W PARTS 6 


DAIL\ TELEGRAP H. 


DAILY GRAPHIC


DAILY MAIL. 


SUNDAY TIMES. 


PIANO PEDALS. | 


BEST AND CHEAPEST


WORKMANSHIP GUARANTEED


PIANO PEDALS


EVERY ORGANIST


NORMAN & BEARD'S 


| PNEUMATIC PEDAL ATTACHMENT 


FOR THE PIANO


TRINITY COLLEGE OF MUSIC.) BROADWOOD 


(I ED 1872.) 


LICENTIATES 


W.M 


A. P IN PREPARING FOR THE EAR TRAININ 


VIOLIN [PRICE -  - ONE SHILLING 


NEW EDITION


THE


PASSION OF OUR LORD


ACCORDING TO S. MATTHEW. 


S M \ 


JOHN SEBASTIAN BACH 


] N ( I


JOHN S'TAINER


THE ROYAL COLLEGE


IMPANY


OF ORGANISTS


VOS 


[ARKET 


TION


ION, W


XUM


NOMASTE] ROVAI MARINES PORTSMOT TH DIVISION


_—_


HE BAND OF THE ROYAL : 


THE


57


I, IQII


R MARINES. MASTER OF THE BAND. 


ACCOM 


1001 


TIMES.—SEPTEMBER


THE


THE DOUBLE BAND. 


MILITARY BAND


395 | 


ORCHESTRAL BAND. 


470


RCHESTRAL ORGANIZATION. 


MILITARY ORGANIZATION, 


THE


573


IQII


THE ROYAL ACADEMY OF MUSIC


MAC


SIR KENZIE


ALEXANDER


574


MR


FREDERICK CORDER


MR. ALDERMAN E. E. COOPER


J.S. 


575


FEMALE STUDENTS


MR. C. E, RUBE


MALE STUDENTS


MR. F. W


RENAUT


1891


XUM


578


THE NEW ‘WAGNER-LISZT.’ 


II


XUM


CHER 


579


RICHARD WAGNER


W. ALE


W.H. 


OW


TS 262-4; 


THE MUSICAL TIMES.—SEPTEMBER 1


81


10


A. E


W.H


‘GCinstrumental soloists are M. Ysaye, who will play the

Flgar concerto, Herr Moriz Rosenthal, and Lady 
Speyer. The chief orchestral work is Beethoven’s

The vocalists are Madame Agnes 
Nicholls, Madame Lillian Blauvelt, Miss Ada Forrest, 
Madame Kirkty Lunn, Madame Ada Crossley, Miss 
Phyllis Lett, Miss Ellen Beck, Mr. Gervase Elwes, 
Mr. Herbert Hegner, Mr. Joseph Reed, Mr. Thorpe 
Bates and Mr. Wilfrid Douthitt. Mr. Haydon Hare 
is chorus-master, organist and conductor of the con- 
concert. Sir Henry J. Wood 
conductor-in-chief. This will be the thirtieth Norfolk 
and Norwich Festival and not, as erroneously printed 
in the advertisement on p. 497 of our August issue, 
the thirteenth




LO


582 THE MUSICAL


2 I LANCE 


A LINK WITH MENDELSSOHN


PIANOFORTE TOUCH


A SIMPLIFICATION OF TERMS. 


XUM


THE


IS


1SIf these conclusions are allowed to stand, examina- 
tion wi ill disclose _many passages that are really 
mezzo-legato or retero, although not called so 
Two examples wi ll suffice: the semi iqui avers marked

i. at the beginnin g of the V7vactssin ate of the 
Beethoven Sonata in E flat, Op. 81.A, are really to be 
played messo-/egato; and the octaves marked ‘f/f 
in the sixty-third bar after the Prestissimo in the

Rondo of the ‘Waldstein’ Sonata are undoubted

examples of /eggiero playin

It may further be pointed out that the direction 
‘non legato, which occurs just after the Adagio 
in the first movement of the Beethoven Sonata 
in C minor, Op. 111, is best interpreted, hot as 
a /ortamento indication, but as merely cancelling 
the slurs that accompany each pair of semiquavers 
just before, since at the tempo given (A//egro con 
brio ed appassionata) the switt /egato playing is the

only kind that is physically possible. 
In conclusion it may be mentioned that the s

ind @ @ o

is not a matter of indifference,’ should be co iredl 
jwith the ‘Bemerkungen zu L. v. Beethoven's 
Klaviersonaten’ which Carl Krebs

Sonata

to the edition of the Beethoven 
Breitkopt & Hartel in their 
Original Texts) classischer Musikwerke. 
to original manuscripts for this edition it wa

that Beethoven used dashes ('), sometimes

collection

i ' ! ! 
a ee on

in the violin part, and this was changed by Beethoven 
into

Coa




584 THE MUSICAL


1 = 1 ! In the original MS. of the minor Quartet, above | 
mentioned, the sfaccafo is Porn non b indicated by 
dashes and portamento (spiccato, in violin phri iseology

by dots. Hence the pi assage in the letter is claimed | 
to refer only dots under a curved line,—and | 
Beethoven is supposed to have chosen the t as a

ewan representation of the sharper blow of the




THE ‘ETERNAL MELODY’ 


AN EASTERN LEGEND. 


XUM


THE MUSICAL TIMES


SEPTEMBER I, IQII. 585


ORGANS BUILT FOR THE ROYAL PALACE 


OF WHITEHALL. 


ANDREW


II.—THE BANQUETING HOUSE


THE MUSICAL TIMI


S.—SEPTEMBER I, IQII


ORGAN AT ST, PETER-AD-VINCULA, TOWER OF LONDON


P. 4


III. THE QUEEN’S CHAPEL, OTHERWISE KNOWN


AS THE * POPISH CHAPPELL


YUM


CONC2 


=‘ TI


KNOWN


TT . . 


407. 


588


THE MUSICAL TIMES.—SEPTEMBER 1, I9QII


TRAINING COURSES 


TEACHERS. 


THE ROYAL ACADEMY OF MUSIC


FOR MUSIC


XUM


589


MUSIC AT THE THREE-CHOIRS FESTIVALSOne would suppose that the conditions at these Cathedral 
Festivals are, if anywhere, ideal for the performance of the 
unaccompanied motets of Bach, like ‘ Sing ye to the Lord vs 
‘Jesu, priceless treasure’ ; ‘ The Spirit also helpeth’ ; ‘Come, 
Jesu, come’; or the noble ‘ Festival and Commemoration 
Sentences’ of Brahms, Fest- und Gedenk-spriiche (Op. 109, 
Nos. I, 2, 3); or the three motets of Cornelius’s ‘ Liebe’ 
cycle (‘ The surrender of the soul’ was done at Worcester in 
1905), but with the latter exception, all these works await 
their first performance at the Festivals of the Three-Choirs ; 
the same being true of César Franck’s ‘ Psalm 150’; Max 
Reger’s ‘ Psalm 100’ and ‘Palm Sunday morning’ ; 
Schubert’s ‘ Mass in E flat,’ and Graun’s ‘ Der Tod Jesu

In the sixteen Festivals from 1895-1910 Sir C. Hubert H. 
Parry has been represented chorally fourteen times by 
eleven works : ‘Job,’ ‘The love that casteth out fear,’ and 
‘Beyond these voices there is peace,’ each having been 
repeated between 1907-1910. Only once in the past 
fifteen years have the following been sung: Bach’s B minor 
Mass ; Berlioz’s ‘Te Deum’; Walford Davies’s ‘ Everyman’ ; 
César Franck’s ‘ Beatitudes’; Verdi’s ‘Stabat Mater’ and 
‘Te Deum’; Dvordk’s ‘Te Deum’; Brahms’s ‘ Song of 
Destiny’; Mendelssohn’s ‘St. Paul’; Schiitz’s ‘ Lamentatio’; 
Leonardo Leo’s ‘ Dixit Dominus’ ; Beethoven’s ‘ Mass in C.’ 
Spohr’s ‘ Last Judgment’ has had three performances in five 
years (1897, 1899, 1901); Goetz’s innocuous setting o 
Psalm 137 was sung in 1896 and again in 1910. Has 
Havergal Brian’s setting of this ‘ By Babylon’s wave’ psalm 
(performed at the Musical League in Liverpool, September, 
1909) ever been considered

Verdi’s ‘ Requiem’ has been sung in 1896, 1900, 1901, 
1907, 1910 (on the last three occasions at Gloucester) ; to 
some minds this will perhaps be the most startling feature in 
a review of this period. Beethoven’s ‘ Choral’ Symphony 
had never been performed before 1900, when it was given at 
Hereford (it is in this year’s Worcester scheme), yet Brewer’s 
‘Emmaus,’ produced in 1901 at Gloucester, was repeated 
there in 1907, and Gloucester is still waiting to hear ‘ No. 9

Considering the comparative restriction imposed upon the 
conductors in the choice of music, by the necessity of the 
performances being in the various cathedrals, it does seem




BRADFORD AND DISTRICT ORGANISTS’ 


ASSOCIATION AT RIPON. 


MR. MOODY ON ‘THE ORGANIST AND MODERN MUSIC


THE ORGANIST AND MODERN MUSIC


THE ORGANIST’S GENERAL CULTURE


ACCOMPANIMENT. 


THE CHOIR


IMPROVISATION


THE ORGANIST AND 


MODERN MUSIC


ALCOCK


XUM


_—_


IC


LOOK


592 THE


PASSED FELLOWSHIP, JULY IQII


0 RECITA 


ORGANIST CHOIRMASTER APPOINTMENTS


AND


XUM


XUM


FOUR PART-SONG (UNACCOMPANIED


— SRE SET EO 


2 2322. 22S = = PS SSS


AN 


YY 


NI


XUM


2)2) - - ’ — - : -_ = 


92 “ 


————— SS SS | — 


) 4 4 ¢ T 


XUM


OWINEBy Gustav Merkel

Theme by Beethoven. 
from the Violin Concerto. 4 )p

the te 61. By 
Arranged for the Organ by




THE MUSICAL TIM


595


ES.—SEPTEMBER 1, I9QII


ECEIVED 


‘COME, LIVE WITH MI 


I ( ‘THE MUSICAL TIME 


‘THE WHITEHALL ORGANS


1021


PIANOFORTE TECHNIQUE. 


XUM


VIOUS, 


XUM


THE


STUDENT COMPOSITIONS AT MANCHESTER


A.R.M.C.M


SAMUEI


ROYAL ACADEMY OF MUSIC CLUB


600 THE MUSICAL 


THE SHEFFIELD WORLD-TOUR CHOIR


TIMES.—SEPTEMBER 1


IQII


___ 


20-2


SCHOOL SINGING DEMONSTRATION


THE CORONATION CHOIR


XUM


XUM


IQII. 601


THE PROMENADE CONCERTS. On Monday, August 14, the usual Wagner night spectacle 
was presented. Sir Henry Wood had made a selection of 
excerpts characterized, on the whole, by brightness, and its 
cheering influence pervaded the concert. The programme 
of August 15 contained the *‘ Leonora’ overture No. 3, Elgar's 
first ‘ Wand of Youth’ suite, Arensky’s Pianoforte concerto, 
Op. 2 (Mr. Edward Goll), Dvorak’s ‘ Slavische ’ Rhapsody 
No. 1, and an excerpt from Delibes’s ‘Coppélia’ Ballet music ; 
nobody could complain that it lacked interest

The first novelty of the season was given on August 16, in 
the shape of a * Pavane pour une infante défunte,’ by Maurice 
Ravel. The arch-modernist of France wrote this work with 
singular restraint. There is little beyond some quaint and 
pleasing out-of-the-way through — ordinary 
harmonies to disturb the most conservative ear. The piece is 
based upon afew simple and charming ideas, and moves with a 
sedate and sweetly mournful grace and an old-world mien that 
admirably befit the subject. The same concert provided 
performances of Debussy’s ‘ Danse sacrée et danse profane,’ 
with Mr. Alfred Kastner as harpist, Strauss’s ‘Don Juan,’ 
and Tchaikovsky’s fifth Symphony—again an interesting 
evening. The quality of interest was maintained on the 
following night, when the Orchestra their first 
performance of Svendsen’s picturesque legend * Zorahayda,’ 
In 
the first classical programme, on August 18, Beethoven’s 
first Symphony occupied its usual position

On August 19, the second ‘ popular’ 
provided a re-hearing of some former l’romenade successes 
Scheinpflug’s ‘Overture to a comedy of Shakespeare,’ Jan 
Blockx’s three ‘ Flemish dances,’ and Dr. Walford Davies's 
fine ‘ Festal overture

under the direction of Mr. Julian Clifford

602 THE MUSICAL TIMES.—Sepremper 1, rort. 
Music in the Provinces. | C ARMARTHEN. —The counties of Carmarthen, Cardigan 
jand Pembroke held a great united Psalmody Festival in th: 
(BY OUR OWN CORRESPONDENT.) | Eisteddfod Pavilion at Carmarthen on August 16. The 
choir of 10,000 voices, conducted by Mr. Harry Evang, 
MANCHESTER AND DISTRICT sang, in addition to hymns, Handel's ‘ Worthy is the 
in: weil aenetllinie eaialeall tue ill Ui wei tee Deal Lamb * and ‘ Hallelujah ° Chorus, and Mr. Emlyn Evans's 
vertises m the local Press of the fact thet the! Eisteddai Teithiwr Blin.” An orchestra of eighty assisted, 
. s of Schiller-Anstalt are for sale However, MELBOURNE.—The University Conservatorium gave the 
: uttem pt being made to resuscitate the club, | mid-winter students’ concert before an audience which quite 
d to |] t 1 2 der footing Altogether it is filled the Town Hall, on July 11. An excellent orchestra 
improbable that Manchester be permanently deprived accompanied the numerous concerted items, and the first 
) sel to the n ! Established some fifty | fruits of the policy of encouraging the study of wind 
years since, it would appear to have outlived its utility as a| instruments by offering bursaries were seen in thirtee; 
] pure ] nd ¥ t r osmopolitan Manchester | student players of flute, oboe, clarinet, bassoon, horn 
life re i re need than ever to-day for some body or | trumpetand trombone. The programme included movement 
ussociation serve as a medium for the interpret ition of | from Pianoforte concertos by Beethoven, Rubinstein, Grieg, 
German thoucht rt to Britishers. rior to the coming | and Tchaikovsky, Violin concertos by De Beriot and 
the Mid] HH its theatre into our midst, German | Vieuxtemps, a Flute concerto by Mozart, the ‘ Kreutzer 
dr ti ies 1 the Schiller each winter; ever | and a Grieg Sonata for pianoforte and violin, and solos

1886 ! sic of t] highest grade has bee for organ, viola, Xc. 
. rd wit ts W .- A _ " a or eg chamber NEW BRIGHTON, The concert given at the Tower, on 
acne ; " re « vel thong? Fy é psy “ a July 2, was a novel experience to audiences of the district, 
a 1 101 e Visit otrauss ever paid to Manchester, | for the choir and orchestra of 125 were conducted by a lads 
al quartet or trio of established repute but has enjoyed Madame de Boutllers. The choir, consisting” of the 
its hospitality. Mr. Car its music irector, did not Liverpool Ladies’ Choir and the Liverpool Vocal Union, 
cont his attentions to Continental artists, not] were heard in a number of operatic selections, _ the 
frequently deviating | from the usual run of chamber | «}fallelujah’ chorus, and in part-songs. Madame de 
concerts, wit! nsider justification. For instance, it | Boufilers herself sang the soprano part in the ‘Inflammatus, 
vas at these concerts that what has been termed the ‘ newer




BRIEFLY SUMMARIZED


XUMIn celebration of the twenty-fifth anniversary of the opening

lling, : ; ; - 
Ncipals f the Municipal Theatre, a festival performance of Beethoven's and choir (conductor, Herr Philipp Wolfrum). 
; Rena ‘Fidelio was given with great success

ae JENA. 
SCCOI e 
5€COI EISENACH. | The recently founded local branch of the International 
Ny th : “oe : > 
lly the ’ sal Society ofess Ste -hmz an iining 
, ' The Neue Bachgesellschaft announces that the autumn Musical Society (Professors ‘ rv Lehmann ag oe nings 
— Bach aap festival in Eisenach will take place on | forming the committee) has decided to revive - —— 
inded . " . sic a , , —- . . ., 
1 th September 23 and 24. It is intended to give two chamber- | ™Usicum for the cultivation of ancient chamber er




HEIDELBERG


XUM


THE


MUSICAL


TIME


S.—SEPTEMBERI, IQIl

Messrs. Si Saens and Julien Tiersot, and the performances, 
vy Messrs. G. Dor nd Gabriel Grovlez, with 
Mil ( rbonnel (from tl Paris Opéra-Comique) in the 
title-par were f the ghest merit. The whole scheme 
vith complete s 2 
. TOS 0 
I 5 ! nd conductor, M. Sergius 
. witzsky, has f endowed a new permanent 
S y Orchestra of seventy performers. The association, 
vhicl ly t ecom mportant factor in Moscow’s 
e, W mel! ts activities during the coming 
I M ! W r Festival commenced in the 
| 1 Prinzrege theater with fine performances 
2 G ni I und Isolde,’ under the 
rshij Messrs ind Otto Lohse. -A 
f Symphony conce been arranged under 
lirectior f Herr Ferdinand Léwe, at the Tonhalle. 
The programmes of these includ Beethoven’s nine 
S\ honies, works ydn, Mozart, Schubert, Liszt, 
Ber Brahms, Tcl wsky, Bruckner, Mendelssohn, 
\ r, S unn, Wagner, and Richard Strauss. Under 
t s Herr Max Reinhardt (of Sumurun fame) 
ta performances have been given with enormous 
Kiinstlerthe Among the works included 
scheme | r er yach’s * Die schéne Helene’ 
wenty-f times t l houses), and a new 
ta * The I by J : 
' : e- o 
I St f Bavar s il 
sculptor B It w placed temporarily in the 
| 1 is ult tely included in the national 
re Karl Ble latest work, * Ein 
Hat Klang r alt é ir and orchestra, has 
rod A siderable ss by the Akademischer 
‘ ing I 
\PLES 
\ ¥ oO era iciro,’ composed by 
Onoft \lt to tl to of Alberto Colantuani, has 
prod y att Teatro Mercadente. 
PAI . 
I n Prix Rome for young composers has this 
I uwarded to M. Paul Paray. Of the thirty voters 
tl t r ttee, twe nty nine were in his favour. 
Hew rnin 1886, 1 was pupil of Messrs. Charles 
d Paul \ t the Conservatoire 
RAGUE. 
T : t Deutsches Landestheater again 
ted with festival I The works selected 
Wagner's ‘ Isolde,’ and ‘Die 
M r er, Mozart's . i, Verdi's ‘ Ballo in 
3 r I * Rigoletto,’ 1d Ambroise Thomas’s 
H he ter r operas in Italiar Among the 
I gag were Messrs. van Rooy and 
I 
YRM 
\ | Br Reger festival to place on July 20 and 
2 ‘ rable s A very favourable reception 
accorded Herr Max Reger, who conducted the Bliithner 
Orchestr ] e of his own work 
r r 
Att Theats Art fragments of Lully’s opera 
t l r t ‘ Psyche’ proved of great 
ir re r 
{A} 
I I hare Strauss’s ‘Elektra’ and 
I ] n been lately included in the 
‘ r e ( rt Oper Another interesting feature 
f Lortzing 1 comic operas (Spielopern), 
/ar 1Z ermann, * Der Wildschiitz,’ ‘ Der 
\ c A ‘Die beiden Schiitzen’ and

I 
‘ r theatre e) three special 
r fl wig T} ile opera ‘ Lobetanz,’ 
. der the direction of Dr. Hess. 
T t } t é ssful




ADEMIC SCHOLARSHIPS, PRIZES, ETC


THE MUSICAL


I, IQII. 605English

been published, it will trace the influence of the opera on 
Beethoven and Goethe

Mr. Edward Collier of 8, Hornsey Rise, London, N., | 
draws our attention to his scheme of graduated claviers. He




SPECIAL NOTICE. 


THURSDAY, SEPTEMBER 21


THE MUSICAL


600


TIMES.—SEPTEMBER 1


IQII


DURING THE LAST MONTH. 


! LB HARWOOD.) 2 


LH ICHINSON, T


NSEN, ADOLF


J E


I ENNARD, LADY BARRETT.- Jubilate Deo, in E. For a.t.1.B. (No. 95, Novello

Services, Anthems, Xc., for Men’s Voices.) 3d. 
\ ERKEL, GUSTAV.—Variations on a theme by 
4 Beethoven. Op. 45. Edited by Jounn E. West

No. 22, Original Compositions for the STAY




XUM


ONIC 


1909


1970


THI 


NDR 


(*ARP 


CABIN 


THRE


THE


DURING LAST MONTH


ONIC SOL-FA_ SERIES


607


BARNETT'S CANTATAS


XUM


CONTENTS. FELLOWSHIP EXAMINATIONS. 


> ‘ > 7 10 


THE MUSICAL TIM


608


POPULAR MARCHES 


ORGAN


AND PIANOFORTI


MYLES B. FOSTER


ES.—SEPTEMBER I, IQITI


NEW CHORAL WORKS


TALE


A OF


OLIVER WENDELL HOLMES. 


: 7 ORGAN 


EDWARD


ELGAR


TERRY


22 OG 


HAY


ANTHEMS


FOR


TRINITYTIDE


COMPLETE LIST


LUTE” SERIES 


THE 


NEW


H ARVEST ANTHEMS


THOU


THAT MEN WOULD PRAISE


WHATE'ER THE


IT


WV


UNTO


COME


PUBLISHED


ART PRAISED 


CUTHBERT HARRIS


THE


OQ GOD, IN SION


LORD


COMPOSED BY


HUGH BLAIR


BLOSSOMED SPRING


AND CHORUS


RECENT ISHED


RECITATIVE, TERZETTA, 


SED BY


COMI


J. HAYDN


TO GIVE


IS A GOOD THING 


THARES


THOMAS ADAMS


GIVE EAR, 0 YE HEAVENS


MPOSED BY 


W. G. ALCOCK. 


E ARTH REMAINETH


SED BY 


WAREING


WE


HILE THE


COMP


HERBERT W


QO GOD, DO GIVE


THANKS 


JOHN E. WEST. 


YE THANKFUL PEOPLE


STEANE


THEE


COME 


BRUCE


L VV ATA 


THE LUTE SE RIES OF HARVEST ANTHE} MS. 


XUM


THE


HARVEST FESTIVAL MUSIC


CANTATAS. 


SONG OF THANKSGIVING HARVEST CANTATA


FOR SOPRANO, TENOR, AND BASS (OR CONTRALTO) SOLI FOR SOPRANO (OR TENOR) AND CONTRALTO (OR 


AND CHORUS BARITONE) SOLI AND CHORUS 


ROSE DAFFORNE BETJEMANN 


THE WORDS WRITTEN AND ARRANGED BY THE MUSIC BY


SHAPCOTT WENSLEY JU LIUS HARRISON


THE MUSIC BY


FOR SOPRANO AND TENOR SOLI AND CHORUS


FOR TENOR AND BASS SOLI AND CHORUS JOHN E. WEST. 


HENRY KNIGHT 


THOMAS ADAMS, FOR CHORUS AND ORCHESTRA 


FREDERIC H. COWEN. 


FOR TENOR AND BASS — CHORUS, AND ORGAN TH E GLEAN ER S HA RVEST


THOM AS AD AMS FOR FEMALE VOICES 


MAS 2 ALS. BY 


MS


TARVEST


FOR TENOR AND BASS SOLI, CHORUS, AND ORGAN OR THE JU BIL EE CANTATA 


SMALL ORCHESTR: \ FOR SOLO VOICES, CHORU S, AND ORCHESTRA


HARVEST CANTATA A HARVEST SONG 


FOR CHORUS, SEMI-CHORUS, AND ORGAN FOR SOPRANO SOLO AND CHORUS 


BY


GEORGE GARRETT. C. LEE WILLIAMS


TWELVE HYMNS FOR HARVEST THE SOWER WENT FORTH SOWING 


LET ALL OUR BRETHREN JOIN IN ONE 7 = 


THE JOY OF HARVEST AND SEA


A HARVEST HYMN OF PRAISE COME, YE THANKFUL PEOPLE, COME 


LONDON


T FESTIVAL 


BOOK


TALLIS’S PRECES AND RESPONSES, THE 


PSALMS


HARVES


CONTAINING 


CANTICLES AND SPECIAL


AND SET TO AND APPROPRIATE


CHANTS BY 


BARNBY, MYLES B. 


MACKENZIE, SIR J


POINTED FOR CHANTING NEW


FOSTER, 


STAINER


SIR J. 


SIR A. C


FOUR NEW HYMN TUNES 


SIR J. BARNBY, SIR J. STAINER


JOHN E. WEST


MUSIC 


F ESTIVALS


43


VIL LAG E ORGANIST


CUNNINGHAM


ORGAN


THE 


F. WOODS


SERVICES. 


ORGAN. 


CHURCH CANTATA, 


COMPOSITIONS 


BY


ORGAN


ANTHEM


PIANOFORTE


POPULAR CHURCH MUSIC


J. H. MAUNDER


NEW HARVEST ANTHEMS 


WHEN THOU HAST GATHERED THY CORN


COM


JSIC


THE


MUSICAL TIMES


613


SEPTEMBER I, IQII


YE THANKFUL PEOPLE, COME


HARVEST


COME


HYMN FOR 


DEAN ALFORD


ORDS BY | 


J. BARNBY


SHORT ANTHEM FOR HARVEST. | 


THINE, O LORD, IS THE GREATNESS


FOR FOUR VOICES


EDWARD BUNNETT, | 


RUTH | 


A HARVEST PASTORAL


IN PLACES OF WORSHIP 


PURPOSES


SUITABLE FOR USE 


OR FOR CONCERT


WORDS BY


EDWARD OXENFORD


MUSIC BY


THANKSGIVING SERVICES


THE OFFICE FOR THE 


HOLY COMMUNION 


JOHN MERBECKE


A.D. 1550). 


EDITED, WITH AN ACCOMPANIMENT FOR ORGAN


BY 


BASIL HARWOOD


MISSA DE ANGELIS 


ADAPTED FROM THE VERSION IN THE SOLESMES 


WITH AN ACCOMPANIMENT 


FOR THE ORGAN


HARWOOD


GRADUAL


BASIL


TE DEU M LAUDA) [US


MENZIES


MILNE


W. J


MUSIC BY ERNEST EDWIN MITCHELL. 


THE


COMPOSITIONS FOR THE ORGAN


EDWIN H. LEMARE


IQII


A SACRED CAN


NOVELLO AND COMPANY


STAFF NOTATION, EIGHTEENTH THOUSAND


THE TEN VIRGINS


TATA


LIMITED


FOR FOUR SOLO VOICES, CHORUS AND 


ORCHESTRA 


BY 


ALFRED R. GAUL. 


STAFF NOTATION, FORTIETH THOUSAND. 


JO AN OF ARC 


AN HISTORICAL CANTATA 


FOR THREE SOLO VOICES, CHORUS AND 


ORCHESTRA 


MUSIC BY 


TRL » - T 


ALFRED R. GAUL. 


NOVELLO'S


NEW SONGS


PRICE TWO SHILLINGS EACH NET


R OF MY SOUL. 


O ALTHEA


REED


HE SONGSTERS’ 


HEART


YING SONG. 


C AVALIER


T* BUT ’TWERE


. |S E


Z, UMMERZEIZH


D. DAVIS


CON


THE


EING


OY


EED


MAN


LOR


WE


CONCERT EDITION OF BIZET'’S


CELEBRATED OPERA


CARMEN


VOCAL SCORE, PRICE 4/- NET


42, GREAT MARLBOROUGH STREET, LONDON, W


COLLECTED AND ARRANGED FOR PIANOFORTE BY THE 


CECIL J. SHARP


CECIL J. SHARP


THE


HYMNS A


OR


THE NEW


THE


EDIT


COSMO GORDON LA 


CHARLES H. LLOYD


ANY


LONDON


ARCHBISHOP OF YORK


LONDON


REDUCED PRICE


ADDITIONAL HYMNS 


WITH TUNES


FOR WITH


NCIENT AND MODERN 


OTHER CHURCH HYMNAL


USE


NOVELLO AND COMPANY, LIMITED


CATHEDRAL PSALTER


CONTAINING


PSALMS OF DAVID


ED AND POINTED FOR CHANTING BY


NOVELLO AND COMPANY, LIMITED


TOGETHER WITH THE CANTICLES AND PROPER PSALMS


WORDS ONLY


PSALTER AND CHANTS COMBINED


AAAAAWAAAAH


10


SEB


4 6 


2 0 


4 6 


20 


4 6 


3 0 


4 6 


3 0 


4 6 


3 0 


4 6 


5 6 


5 6 


5 6 


5 6 


THE


MUSICAL TIMES


617


THE


HENRY


NOVELLO’S


TWELVE SELECTED PIECES


OXFORD UNIVERSITY


PRESS


UNISON CHANT


CHOIR-BOOK


BY 


ARTHUR W. POLLITT :. BRYSON 


ALBUMS FOR THE ORGAN


SHORT PRELUDES


ORGAN


NO. 300K |, 


618 THE MUSICAL


TIMES.—SEPTEMBER


I, 1911


ISHED, 


LA SAVANNAH 


AIR DE BALLET 


FOR ORCHESTRA 


COMPOSED BY 


A. C. MACKENZIE, 


J P ISHED


SCHERZO 


IN G MINOR. 


FELIX MENDELSSOHN-BARTHOLDY


SOL


DREAMING


UBLISHED


VIOLIN AND PIANOFORTE. 


COMPOSED BY 


HERBERT BREWER


PLEADING


SONG


COMPOSED BY 


EDWARD ELGAR


ARRANGED FOR SMALL ORCHESTRA, WITH 


HARP OR PIANOFORTE


OR CLARINET


MUSIC


ARRANGED


NOVELLO'S 


FOR MILITARY BAND


BY


AND


OTHERS


W \ 5 S ) ) I ‘GS 


} I 5 ) 


LONDON


NOVELLO AND COMPANY, LIMITED


XUM


619


WEBSTER'S | 


CHILD’S PRIMER 


OF THE THEORY OF MUSIC


SIMPLE AND SYSTEMATIC


CHORISTER’S AID TO 


MONOTONING


C. S. FOSBERY, M.A


FROM “DIE 


FROM “DIE WALKURE.” 


W. G. ROTHERY. 


RICHARD WAGNER. 


GONG


EDITED


ORGAN 


TRANSCRIPTIONS 


GEORGE J. BENNETT


PSCHAIKOWSKY


6 THREE MINUETS. 7 MINUET

Symrnonies 1x C, G Minor, AND 
MOZART 
iii.) 
BEETHOVEN 1 
MACKENZIE 1 
“Brest Pair oF 
- ‘ C. H. H. PARRY 1 
“A MipsumMer Nicut’s Dream” 
MENDELSSOHN 1

Sonata in E erat). (Op. 31, 
& PRELUDE.—(“ Cotompa”) A.C. 
9 FINALE




SIRENS


1 NOTTURNO


O MAY WE ONCE AGAIN


MEISTERSINGER


WITH NUMEROUS MUSICAI


ALLEGRETTO 


W. WOLSTENHOLME


ARRANGEMENT FOR SMALL ORCHESTRA: 


AUF WIEDERSEHEN


A. HERBERT BREWER


SMALL ORCHESTRA


TECHNIQUE 


EXPRESSION 


PIANOFORTE PLAYING 


FRANKLIN TAYLOR


EXTRACT FROM PREFACE


EXAMPLES FROM THE


WORKS OF THE GREAT MASTERS


EDITED, ARRANGED IN GRO


1. FIVE-FINGER STUDIES


3. SCALES 


5. BROKEN CHORDS ... 


10. LEFT HAND


II. ” = 


12. 


13. 


14.*° ARPEGGIO


10. 99


17. VELOCITY


20


FIGURES IN SEQUENCE


NN 


OCTAVES 


OCTAVES


28. SHAKES


IN TWO SETS (EIGHT BOO


LONDON


26. BROKEN THIRDS, SIXTHS


BROKEN THIRDS, SIXTHS


FIFTY-SIX BOOKS


PROGRESSIVE STUDIES


FOR THE PIANOFORTE


UPS, AND THE FINGERING REVISED AND SUPPLEMENTED


AND 


AND 


FROM THE


SELECTED PIANOFORTE STUDIES


FRANKLIN


TAYLOR


39.* STACCATO


53. EXERCISES FOR THE WEAKER FINGERS. 


ARNOLD KRUG


55. EXERCISES FOR FACILITATING _INDE- 


56. PRELIMINARY STUDIES IN PLAYING 


PRICE ONE SHILLING EACH


ABOVE


PROGRESSIVELY ARRANGED BY


FRANKLIN


TAYLOR


KS), PRICE ONE SHILLING AND SIXPENCE EACH BOOK


NOVELLO AND COMPANY


LIMITED


ENTED


NGERS. 


INDE. 


D KRUG. 


AYING 


NOVELLO'S


STRINGED INSTRUMENTS


TWO VIOLINS, VIOLA, AND VIOLONCELLO


ALBUMS FOR PIANOFORTE AND


LONDON


17


18


19


20


21


NOVELLO AND COMPANY


LIMITED


ND


PIANOFORTE


ICAL CENTRE.—INTERMEDIATE GRADE, ‘*%*! CONTRALTO. 


LOCAL CENTRE.—ADVANCED GRADE. SOPRANO. 


‘ 13) : 


\ N ) I CONTRALTO. 


} SCHOOL EXAMINATIONS.—ELEMENTARY. 


\ I SCHOOL EXAMINATIONS.—HIGHER DIVISION. 


SINGING. CONTRALTO. 


LONDON


LLO, PAGE 623


XUM


THE


MUSICAL TIMES


623


THE


IQI2


ORGAN. 


INTERMEDIATE GRADE


LOCAL CENTRE


ADV ANC E D G R. ADE


CENT RE


MENDELSSOHN


LOCAL


SCHOOL EXAMINATIONS.—LOWER DIVISION


SCHOOL EXAMINATIONS.—HIG HER DIVISION


VIOLIN


LOCAL CENTRE.—INTERMEDIATE GRADE


SCHOO! EXAMINATIONS PRIMARY. 


SCHOOL EXAMINATIONS. ELEMENTARY 


VIOLONCELLO. 


LOCAL CENTRE. -INTERMEDI ATE


GRADE. 


LOCAL CENTRE.—ADVANCED GR: ADE


ANI


EF. 


NEW FOREIGN PUBLICATIONS


PIANOFORTE MUSIC. 


BRODERSEN, VOp. 48

In 3 Books ‘ oe @acl 
LAZARUS, G.—Album for 32 Plancfirte places ee os oe 
MALIPIERO, G. F.—Three Pianoforte pieces. 1. Gavotta, 1s. 
net; 2. Mir uetto, TS. net; 3. Giga . - . 
le Jeunesse Valselente. Pianoforte So 
Polka italienne. For Pianoforte Solo, rs. 
Pianoforte Duet ee ee oe os ee 
RU R INSTE IN, Romance. Op. 44. Transcribed for two 
Op. 9. ‘Three Pianoforte pieces. 1. Burletta, 
Bohmisch each 
Op. 16. Piéces antiques. For Pix forte Solo 
WINDING, A. Cwenty-four Preludes. For Pianoforte 
Solo. New Edition by A. F. Wouters . 
ZILCHER, P.—Ornamental Technic. 100 examples fron 
Beethoven, Chopin, Liszt, &c

CHAMBER MUSIC. 
LE BORNE, F.—Pensée Nuptiale. For Violin and Pianoforte 
NE ~ IA, F. P. Op 1g. 1. Intermezzo; 2. Capriccio-Walzer




ORGAN AND HARMONIUM. 


VOCAL MUSIC. 


NET. 


10


4 6 


1 6


MUSICAL


624 THE


TIMES.—SEPTEMBER 1


RECENT NUMBERS


THE MUSICAL


‘OVELLO’S PART-SONG. BOOK. 


NOVE LL O'S OCTAVO ANTHEMS. NOVELLO'S | P: 


4 4 


M N I) F G. | ' 


4 > 4 ¢ , 


DANI