


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 PUBLISHED 


 MONTH 


 SEPTEMBER 1 , 1875 


 TRINITY COLLEGE , LONDON 


 INCORPORATED ( JUNE , 1875 ) ADVANCEMENT 


 CHURCH MUSIC 


 ESTIVAL CHOIRS . — | 


 LAPHAM PARISH CHURCH . — TREBLE , 


 CONCORDIA 


 ANNUAL SUBSCRIPTION , INCLUDING POSTAGE , 


 NINETEEN SHILLINGS SIXPENCE 


 OFFICE — LONDON : 1 , BERNERS STREET , W 


 CONCERT TOUR 


 PRIZE WINNERS NATIONAL 


 MUSIC.MEETINGS , 


 MISS LARKCOM , MISS A. BUTTERWORTH , 


 MR . GEORGE SYLVESTER , & MR . E. WHARTON 


 


 SIGNOR RENDANO 


 CELEBRATED PIANOFORTE PLAYER 


 PROFESSIONAL NOTICES 


 STANDARD AMERICAN ORGANS 


 MANUFACTURED 


 PELOUBET , PELTON & CO . , NEW YORK 


 METAL EQUITONE FLUTES , KEYS 


 CYLINDER FLAGEOLETS , KEYS 


 AGENTS REQUIRED TOWN 


 NEW PEDAL ATTACHMENT 


 PIANOFORTES 


 TESTIMONIALS . 


 JESSE MINNS , F.C.O. , 


 RUSSELL MUSICAL INSTRUMENTS . 


 ROGER CELEBRATED VIOLIN STRINGS 


 AST LONDON ORGAN WORKS 


 O PIANOFORTE DEALERS , AUCTIONEERS 


 MR . RICKARD 


 BASS VOCALIST , 


 HALIFAX , YORKSHIRE 


 TENTH SEASON , 1875—76 . 


 ENGLISH GLEE UNION , 


 ASSISTED 


 ANGELL TOWN INSTITUTION , BRIXTON . 


 YORKSHIRE CONCERT PARTY , 


 ANGLICAN 


 CHANTS 


 SINGLE DOUBLE 


 EDITED 


 


 


 EDWIN GEORGE MONK 


 TONIC SOL - FA EDITION . 


 MANUAL SINGING 


 USE CHOIR TRAINERS & SCHOOLMASTERS . 


 THIRTY - EDITION . 


 COLLEGIATE SCHOOL 


 SIGHT SINGING MANUAL 


 APPENDIX 


 ( COMPANION WORK 


 COLLEGIATE VOCAL TUTOR 


 CATHEDRAL CHANT BOOK 


 SECOND EDITION DR . BENNETT GILBERT 


 SCHOOL HARMONY 


 HELMHOLTZ TONE . 


 HARVEST ANTHEMS 


 ALLCOTT , W. H.—THOU VISITEST EARTH 


 ACFARREN , G. A.—GOD SAID , BEHOLD , GIVEN 


 ATTISON , T. M — O PLENTIFUL THY GOOD- 


 AYLOR , W.—THE EYES WAIT THEE . 


 RIDGE , J. FREDERICK.—GIVE UNTO LORD 


 EETON , HAYDN.—THE EYES WAIT 


 HYMNS TUNES 


 HARVEST 


 SELECTED 


 HYMNARY 


 PRICE PENNY 


 O LORD , HEAVE : 


 FATHER , BLESSING SEED 


 LORD HARVEST , THEE 


 C. JEFFERYS , 67 , BERNERS ST 


 JEFFERYS ’ MUSICAL JOURNAL 


 MELODIOUS MELODIESArranged Organ Harmonium Joun OwENn . 
 Shilling , net . 
 

 Andante ( Heller ) , Lied ( Mendelssohn ) , Schlummerlied ( Schumann ) , 
 March ( Schumann ) , Andante ( Oesten ) , Andante Moderato(Schumann ) , 
 Andante Cantabile ( Beethoven ) , Allegretto ( Mendelssohn ) , Traiimerei 
 ( Schumann ) , Andante ( Beethoven ) , Wedding March ( Mendelssohn 

 II 

 Angels bright fair ( Handel ) , Lied ( Mendelssohn ) , Andante 
 ( Russell ) , Adagio Cantabile ( Beethoven ) , Cujus Animam ( Rossini ) , 
 Faith ( Glover ) , Hope ( Glover ) , Charity ( Glover ) , Sanctus ( Bartniasky ) , 
 Caprice ( Blumenthal ) , Gloria , Twelfth Mass ( Mozart 

 III.—OPERATIC . 
 IV.—VOLUNTARIES 




 SONGS CHILDREN . 


 CONTENTS 


 LONDON : C. JEFFERYS , 67 , BERNERS STREET , W 


 198 


 USTHALL , TUNBRIDGE WELLS . — 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 SEPTEMBER 1 , 1875THE LONDON MUSICAL SEASON . 
 Henry C. Lunn 

 BELIEVING genial Charles Lamb 
 grace occasions 
 day dinner , think 
 occasions hearing 
 Symphony Beethoven , entering room 
 listen Messiah , great 
 musical work placed offering 
 mental , food physical , wants . 
 greater thankfulness ought 
 banquet ; expectation apt some- 
 querulous , whilst satisfaction generates 
 feeling placid gratitude accordance 
 frame mind suitable utterance 
 words . epicures art , , content 
 wait feast , instead 
 hoping anticipating appetite 
 appealed wish , calmly reflect 
 placed , shape 
 remarks according circumstances . 
 author quoted says ‘ ‘ short form 
 occasions felt want reverence ; 
 afraid escape charge impertinence . ” 
 desire lay open 

 _ accusations , endeavour 

 TuosE frequent Operas Concerts 
 metropolis surprised following an- ’ 
 nouncement , issued Moore Burgess 
 Minstrels : “ ‘ great company numbers 
 ranks finest vocalists England : 
 instrumentalists selected 
 orchestras Opera houses Philhar- 
 monic Society . ” notification fact 
 necessary attend smaller room St. James 
 Hall hear ‘ ‘ finest vocalists England ” 
 effect drawing real music 
 lovers Moore Burgess concerts ; 
 told “ instrumentalists 
 selected orchestras Opera 
 houses Philharmonic Society , ” 
 time find grossly deceived 
 imagining listening best orchestral 
 players favourite bands . Sir Michael Costa , 
 Signor Vianesi , Signor Bevignani , Mr. Cusins 
 certainly wonders arduous 
 season , considering work 
 orchestra band Moore 
 Burgess Minstrels previously “ selected ; ” 
 future years consider 
 principal instrumentalists 
 definitely secured Operas Philharmonic 
 Society weaned away 
 enterprising caterers public “ 
 perform St. James Hall 

 proceedings ‘ ‘ Musical Association 
 Investigation Discussion Subjects connected 
 Art Science Music ” printed 
 forwarded notice . difficult readers 
 idea matters treated extracts 
 papers read Society ; quotations 
 prove interest . Dr. W. H. Stone , opening 
 lecture , ‘ ‘ extending compass increasing 
 tone stringed instruments , ” states 
 years endeavouring extend compass 
 orchestral instruments downwards : ‘ wind depart- 
 ment succeeded , introducing 
 old instrument , contra - fagotto , remodelled , , 
 hoped , improved . found want 
 string department . exhibited double bass , 
 strung pitch , CCC organ , 
 Exhibition 1872 . note frequently 
 Beethoven , Onslow , great writers ; 
 Gounod B flat . author said 

 increasing success real “ National Train 

 readers glad hear new edition 
 Sir John Hawkins valuable comprehensive “ History 
 Music ” issued Messrs. Novello ; 
 work printed better larger paper , 
 considerably reduced price 

 Inan article past musical season , musical critic 
 ofthe Standard writes follows : — * simultaneously 
 commencement Popular Concerts , Messrs. 
 Novello Co. inaugurated series concerts 
 Albert Hall . 7th November Boxing - day 
 gave concert evening . Mondays 
 devoted ‘ Ballads , ’ Tuesdays ‘ English ’ music , 
 Wednesdays ‘ Classical ’ music , Thursdays ‘ Oratorio , ’ 
 Fridays ‘ Modern German ’ music , Saturdays 
 ‘ Popular ’ music , immense energy spirit 
 scheme adhered months . success 
 means commensurate deserts , 
 Christmas performances reduced 
 aweek . little later allattempts toa fixed arrange- 
 ment dropped , concerts given occasion- 
 ally , complete scale . Mr. Barnby , 
 entire management entrusted , engaged 
 thoroughly ‘ efficient band seventy 
 performers , season orchestral per- 
 formances high excellence . 
 probably better conductor 
 , different conductors 
 impossible band play alike . 
 Mr. Barnby conducted Classical Oratorio concerts , 
 Mr. J. F. Barnett English music , Mr. Dannreuther 
 modern German programmes , frequently 
 orchestral accompaniments vocal solos con- 
 ducted Mr. Randegger . classical concerts 
 admirable performances , Beethoven , Mendelssohn , 
 symphonies , finest overtures 
 played need wish hear , concertos 
 played best solo performers London , given 
 season . English concerts , Mr. J. F. 
 Barnett direction , furnished opportunities perform- 
 ance orchestral works native composers merit 
 hearing far usually obtain . 
 concerts performances given 
 London pieces composed Liverpool 
 Musical Festival 1874 . Mr. Barnett descriptive piece 
 orchestra , ‘ Lay ofthe Minstrel , ’ Professor 
 Oakeley ‘ Edinburgh March , ’ Mr. Macfarren ‘ Festi- 
 val Overture , ’ Mr. W. G. Cusins pianoforte concertoin 
 minor , Benedict symphony G minor , Sterndale Bennett 
 overtures , Gadsby overture ‘ Andromeda , ’ Sullivan 
 overtures ‘ Sapphire Necklace ’ ‘ Memoriam , ’ 
 English compositions , performed . 
 Mr. Barnby Royal Albert Hall Choral Society formed 
 chorus oratorio nights , fully sustained re- 
 putation . Mdlle . Johanna Levier débit 
 concerts , rapidly worked way public favour . 
 , , Herr Wilhelmj appearance 
 absence England ‘ years , 
 performances heard frequently ; Mr. 
 Whitney , American basso , good service 
 season , especially Elijah 
 Mendelssohn Oratorio , bass music Handel 
 Messiah . 15th , series careful re- 
 hearsals , Verdi Requiem produced Albert Hall , 
 performances given . band consider- 
 ably increased , Albert Hall Choral Society formed 

 oo ’ en ’ af . em ook . © tet aw 2 et 




 206 


 REVIEWS 


 ANTHEM SOPRANO SOLO CHORUS 


 ICED ~ — = - & - = , = -@- = ] E 


 PP — 


 24 — : 3 E — — — 


 _ — — — — _ { 


 TCE . ‘ 2 . 


 O COME , LET WORSHIP 


 AML ® ; : 


 TH = TNS TRE 


 3 2 \ 


 O COME , LET WORSHIP 


 TC\. 4 WB 


 ORIGINAL CORRESPONDENCE 


 ORGANISTS SELECTED ? 


 EDITOR “ ‘ MUSICAL TIMES 


 01 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWSGounod ) , Tantum Ergo , ( Batchelder ) , Date Sonitum ( Costa 

 ADELAIDE , AUSTRALIA.—The prospectus Adelaide Amateur 
 Musical Union 1875 - 6 announces season fol- 
 lowing works , selections , produced : Beethoven 
 “ Choral Symphony ” “ Fidelio ; ” Mozart “ Jupiter Sym- 
 phony ; ” Spohr ‘ Calvary ; ” Weber “ Euryanthe ; ” Verdi “ Re- 
 quiem Mass ; ” Wagner “ Rienzi ” “ ‘ Lohengrin ; ” Randegger 
 “ Fridolin ; ” Benedict ‘ ‘ Undine ; ” Balfe “ Enchantress ; ” ’ Schu- 
 mann “ Pilgrimage Rose ; ” Gabriel “ Evangeline , ” & c. 
 intimated usual - songs glees con- 
 tinued , best instrumental music obtainable Adelaide form 
 feature programmes , sufficiently shown every- 
 thing committee secure patronage 
 support public 

 Buxton.—A special concert given Thursday evening 
 12th ult . , largely attended . Mr. Sims Reeves Men- 
 delssohn sacred song ‘ hearts , ” ballad “ 
 , ” “ Tom Bowling , ” created extraordinary success ; 
 Madame Cave - Ashton , sang “ Scenes brightest , ” ‘ ‘ 

 S1pMouTH.—On ‘ Friday evening 6th ult . , amateur concert 
 given London Hotel Assembly Room , conductor- 
 ship Mr. H. A. Harding , F.C.O. , aid funds Sidmouth 
 Needlework Society . programme consisted 
 sacred music , commencing Handel “ Occasional ” overture , 
 played manner reflected credit 
 professional band . Miss Godolphin Osborne presided har- 
 monium ; Mrs , Hine Haycock Miss King piano . 
 choruses - songs rendered great care marked 
 precision , reflecting greatest credit Conductor 

 SovuTusza.—A Pianoforte Recital given Portland Hall , 
 r8th ult . , Mr. G. S. Lohr , occasion assisted 
 Mr. C. E. M‘Cheane , Mr. H. B. Bethune , Mr. R. H. Lohr , 
 band ofthe Royal Marine Light Infantry ( direction Herr 
 Kreyer ) , company gentlemen styling Minne 
 Singers . Mr. Lohr performed classical pianoforte composi- 
 tions , including Beethoven Concerto E flat , Mendelssohn 
 Capriccio B minor , Chopin Study C sharp minor Valse 
 flat ; , conjunction Mr. R. Ohr , selection 
 Brahms Hungarian Dances . feature programme wasa Con- 
 cert Overture E composed concert - giver played 
 Band . songs written Mr. Lohr , ‘ ‘ twilight dews , ” 
 sung Mr. M‘Cheane , ‘ ‘ , ” sung Mr. Bethune , 
 received , pieces contributed Minne Singers 

 WotverHAMPTON.—The Tettenhall Wood Congregational Church 
 special Services connection opening Organ , recently 
 erected , held Sunday , 8th ult . , preacher morning 
 evening pastor , Rev. J. P. Driver . Morning 
 Service Sanctus sung Kocher music , psalms 
 chants Allcock Blow . hymns Nos . 229 , 261 , 256 , 
 anthems “ O praise God holiness , ” ( Weldon ) , “ 
 lift eyes ” ( Dr. Clarke ) . Evening Service 
 Sanctus sung arrangement Bach , psalms 
 Norris chant . hymns Nos . 256 , 773 , 944 , anthems 
 Morning Service . choir Church 
 assisted members choirsin town , total number 
 voices 34 . organ consists 2 manuals pedals , 
 contains 17 stops , arranged follows : great organ , open diapason , 
 stopped diapason ( treble ) , ditto ( bass ) , viol de gamba , flute , principal , 
 twelfth , fifteenth , sesquialtra , swell organ , open diapason , stopped 
 diapason , principal , trumpet , bourdon , pedals , bourdon , couplers , great 
 pedals , swell great . composition pedals . 
 collections amounted £ 30 




 O CHORAL SOCIETIES , CATHEDRAL 


 RGAN , PIANOFORTE , HARMONIUM , HAR- 


 PEDAL PRACTICE . — PEDAL REED 


 O HARMONIUM MANUFACTURERS , 


 MONTH 


 OVELLO - SONG BOOK . 


 O PIANOFORTE MANUFACTURERS . — 


 NOVELLO , EWER CO . COMPLETE 


 UNIFORM EDITION 


 MENDELSSOHN 


 THIRTEEN - SONGS 


 NOVELLO 


 SHORT MELODIES 


 ORGAN 


 VINCENT NOVELLO . 


 NEW NUMBERS 


 NOVELLO - SONG BOOK 


 OULD GAIN MAIDEN HEART . 


 ANGLICAN HYMN - BOOK 


 NEW EDITION , REVISED ENLARGED 


 SECOND SERIES 


 ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 OULE COLLECTION WORDS 


 ADDITIONS 


 EV . T. HELMORE PLAIN SONG WORKS , 


 “ PSALTER , PROPER PSALMS , HYMNS , 


 OULE DIRECTORIUM CHORI ANGLI- 


 JOULE DIRECTORIUM CHORI ANGLI- 


 ORDER HOLY COMMUNION . 


 LATE DR . MONSELL DEVOTIONAL POEMS 


 LATE DR . ELVEY PSALTER . 


 VILLAGE ORGANIST 


 ANGLICAN CHANT SERVICES 


 CANTICLES 


 NEW CHEAPER EDITION . 


 PRELIMINARY NOTICE . 


 “ OQ WORSHIP LORD BEAUTY HOLI- 


 SECOND EDITION . 


 YMN TUNES , CHANTS , KYRIES . — 


 JAMES YOUNG 


 AGNIFICAT , TONUS REGIUS , NUNC 


 PUBLISHED 


 ANTHEM 


 


 HUMPHREY J. STARK 


 FOUND SUITABLE 


 HARVEST FESTIVALS 


 SUNG MR . VERNON RIGBY . 


 UNREST REST 


 WORDS 


 MUSIC COMPOSED 


 PLAYED 1,000 CHURCHES . ( SECOND EDITION . ) 


 R. FOWLE STANDARD ANTHEMS , 


 GOD LOVE SHEPHERD 


 NEW SACRED SONG 


 


 RIDLEY PRENTICE 


 ARRANGED GREGORIAN TONES 


 


 J. STAINER 


 SAMUEL WESLEY 


 MOTETT DOUBLE CHOIR 


 PERGOLESI 


 STAB A‘T MAT ER 


 FEMALE VOICES . ) 


 MANFRED 


 DRAMATIC POEM , ACTS , 


 MUSIC 


 ROBERT SCHUMANN , 


 SEA 


 SONG DESTINY 


 SCHICKSALSLIED ) , 


 COMPOSED 


 TOHANNES BRAHMS , 


 TROUTBECK , M.A 


 READY . 


 


 CATHEDRAL PSALTER 


 POINTED CHANTING 


 PUBLISHED SANCTION 


 REV . DEAN ST . PAUL 


 


 REY . DEAN WESTMINSTER 


 HYMNARY 


 BOOK CHURCH SONG 


 FOLLOWING EDITIONS READY : — 


 BYBest 

 MUSICAL TIMES.—SepremsBer 1 , 1875 . 219 
 [ EW- 
 ee , MARCHES MARCHES 
 — COMPOSERS , ARRANGED 
 
 ong , 
 wae ORGAN 
 PIANOFORTE SOLO 
 Arranged s. d. z 
 Adam , Adolphe ... Marche Religieuse eZ. Best un . 2 0 
 Alexander , Alfred March aa 2 0 
 iat Beethoven e. Military March , Posthumous W. r. Best « bale 0 
 . oe March ( Tarpeja ) = 20 d 
 ooo h Oo o aa , Ss . a. 
 : Wenaeel Moreh ( Op . 26 ) 45 ) . 2 © | Allen , Alfred B. .. March ofthe Choristers   . . Pree 
 » March ... 2 o } Arnold , G. B. ‘ ] March Oratorio “ Ahab ” . « 26 
 ya arch , ( Egmont ) ... eee os Barry , C. Birthday March 
 r » C. A. ms “ ae 4 rm ° 
 ee Best , W.T. + » March Church Festival , Barnby , J ... .. Bride March , * ‘ Rebekah ” Sar 1 : ° 
 Y w ae 1 16 ” + + 3 © ] Beethoven .. .- Military March , “ King Stephen ” . 2 0 
 = ) ny Sa ” ore : ; ie las March “ 4 Io 
 ” ad oo ” oe . “ « riumphal March _ , . Io 
 Cherubini « Marche Religieuse , composed : Py .. Turkish March , “ Ruins Athens ” 1 6 
 Coronation ; Benedict , Sir Julius Malcolm ( Marche Triomphale ) .. 40 
 Charles X. pists .. H.J. Lincoln r 3 ] Brabham , James .. March Pilgrims ae pe 3 0 
 . ¥ Burstall , F.H. .. Childwall March ry ie 3 0 
 — ang ora — _ gel W. T. Best ... 2 o| Calkin , J. B. .. Festal March .. Pe 3 0 
 ‘ Egghard , Jules .. Marche dela Garde Imperiale ° 
 h B. Marche R 4 
 e oe } . “ 9 ae 4 eligieuse ... eee 2 Ol BTiote , ’ J. * Victoria Cross March oe 
 .- Festal March ore s+ = wwe 2 ( © ) Bivey , SirGeorge .. Festal March , performed mar- 
 come William .... Processional March “ Placida ” ate : SD fake SED riage H.R. 1 Sete Louise 
 » George « . Grand Festival Marchese 2 « + » 2 0 Marquis Lorne 9A ) og 95 aoe 
 Res Collin , Charles .... Marches Iat3s . ,2at2 6 Bales Ferdisand ng vata gi 14 . Saul € + : : 
 Dunning , T. W .... National Provincial March oe & $ 1 Jacoby , S. .. « - March , dedicated Volunteers 
 k Marcia Funébre , : Great Britain - 2 6 
 sai , oe hands W , J. Wes stbrook .. .. 1 6 { Kriger , W ... .. Marche des Bohemiens Russes 4 0 
 Glover , H.C. S ..... Helena March 2 6|Lohr , Fred . N. .. tal March ( Dedicatedto H.R. 
 Handel ... « + . March , “ Scipio ” Ww . x " Best « 20 > 6 pay ak “ > ieatedto H. 
 March . “ Prince Wales ) . 4 0 
 ” = March e Ode S. Cecilia ’ sDay ” » ~ 2 ; Longhurst , W.H ... Baech af noel David Army , “ David 
 , s aii y+ane e ae vs aay fe eelders Maer eC 
 ” < Funeral — ee ” + + 2 © ) Mendelssohn + » March composed celebration ’ 
 ” s+ oe Dead March “ Saul ” * W. T. Westbrook ws ‘ visit painter Cornelius Dres- 
 ~ oe oe . uo ° J. est 00 E den , 1841 3 0 
 ” ote ore March Joshua eve W. T. Best 2 0 e . War March , “ “ Athalie ” b Pty 
 Hatton , J.L.   .... Triumphal March ae ae ¥ ws " 7 eA 7 ) Funeral March composed military 
 Hiles , Dr. H. ... Festival March « 1 . 0 tee ore 2 band .. = * - 3 8 
 R OD ... ne Cee AR ent , om a6 ) aeninect © ” : Wedding March oritettecdia yt > * 
 swig Liszt ee ove « 6 Triumphal March “ Vom -- Hero March . ast dee 
 9 Fels zum Meer ” ... W. T. Best ... 2 © pei .. Pilgrim March se “ = 
 Lefebure - Wely ... M ha Molique , B ... -- March , “ Abraham ” .. 2 6 
 4 Pl ote wo Marenee , es ° Novello , V. os 2 Marches — 2 Mozart , 1 ‘ Gluck 20 
 ~~ aan + March , C major , “ Idomeneo W.T. Best ... 2 0 Oakeley , H.S. .. Funeral March , Op . 23 Rigo 
 3 ¥ ae ini F ” - 2 0 Pa - . Edinburgh March ( Dedicated 
 [ Fe aes ove Ke ‘ Posthumous , D major ~ oo 2 @ H.R.H. Duke Edinburgh .. ees 
 3 Meyerbeer ... + Coronation March “ Le Prophéte , ” , , 2 0 } O'Leary , ... - . 5 Marches — . 1 , ‘ Hastento 
 m oe ee Schiller March ... ow ve 20 fight . ” No.2 , “ Union . ” . 3 , 
 Mendelssohn ... War March , “ Athalie ” pi neal 20 ‘ ph att Beek Worse 
 ie dee e é r. Steggall ... 0 t 
 March . ” Complete 5s . h , r Singly 20 
 ” soe E * pe tals mee ee aa Ww oo . 6 Sloman , R. .. TheAbyssinian March . 2 0 
 — ” ee rn March , Op . " r03 . Te aeeree , E. “ + ane beer . je 20 
 - cr sve n ” = orne , ecessiona arch .. e- ee 40 
 ‘ ee » Wedding March e. E. wil s+ 2 0 ] Wesley , Dr. S. = ; .. March . FF 3 0 
 Molique RARER Ps y vilfing . “ 0 - 6 Wollenhaupt ee saneee Marche de Concert ’ ex 3 
 Moscheles ....... Marche Triomphale ... 20 " -- , Marche Hongrcice ilitetze “ hi hy Pip 
 ps .. Grande Marche Militaire ae 
 Monk , E. G. + » March , “ Bard ” ... G. A. Hardacre ... 1 0 Wagner .. 2s March , “ Tannhauser ” F.Spindler .. 3 0 
 ee . Sir March , “ Polycarp ” L. Colb egal es W. Kriger .. 5 0 
 YE dle aaa aaah olycarp ” ... L. Colborne .. © ! Zimmermann , Agnes MarcHe .. .. « 2 « + . « 20 
 ° Richardson , J. E ..... March ob ai . 6 ; 
 Schubert ... ... Grand Funeral “ March , ‘ written . 
 6 occasion death = eed 
 Alexander > . Silas ... @ 
 4 . ‘ ns ses March Sol Op . ‘ “ Ww . J. “ Westbrook 1 6 
 ¥ isa eu arch Suess e ‘ Op . 40 , 
 3 0.5 ~. W.T.Best .. 2 0| MARCHES . PIANOFORTE DUETS . 
 ° * ove ove , Op . ‘ 40 , . ’ iar & ws 2 0 Hi 
 pa eee Grand March , Op . 40 , . 2 — _ . 3 
 ° Ni ies March , B minor , Op.27,No.1 _ , , ‘ oe 
 ° Spohr ay ... March Notturno , Op , 9h eas ~~. 2 Calkin , J.B , ... . » Marche Religieuse oat , cao ey 
 Schumann ... + March , Op.85 _ ... pe oot 3 ” es eee - Festal March ot < 0 oee o- 4 0 
 fs ) Spindler , F. « . March , C minor ... Pe Fi .-- 2 © } Mendelssohn ... es Hero March , Op.22 « se eco mm 3 2 
 6 Smart , Henry .... Grand Solemn March ... e. ” ] Wedding March , Op . 6r . - 30 
 Steggall , Dr. « March , Philistines , ‘ ‘ Samuel ”   ... a4 ’ ” oe oo War March Priests ‘ 
 ° Stone , J. T. + March Triomphale _ ... ons woo Athalie , ” Op . 74 4 0 
 Thorne , E. H. « . Festive March .. pen 2 ” . . Pilgrims ’ March Italian “ Sym- bi 
 |S ee .   W.J. Westbrook 1 6 phony , Op . 90 § 
 Wagner aed « » March , “ Tannhauser ” F. Archer ... 2 0 ” oo + Funeral March , Op . 103 « . ad oo 4 0 uy 
 ‘ ) Weber ites " .. March neers Duets W.T. Best 2 6 1 59 es » se » Cornelius March , Op . ~ ae oi 
 RIES La ct MMO ste , es ) ae J. Hiles 1 6|Molique , B. < 2 ... March “ Abraham ” 4 0 
 6 Westbrook , W. J. .. Festival March | tb ... 1 0| Moscheles,1 . - | » March ( . 1 Duets Piano- F 
 - Nos ... py : ee rp rpeeryg eco eco eos ee . 6 x 
 oe March pits ta ie ik wee 6 ] Silas , & . oe ‘ unera Marc woe és0 eco ese ° o L 
 4 Zimmermann , Agnes March ... ..   .. Dr. Stainer ... 1 6|Wigan , C.   .. « + » ~Triumphal March ... ... oe eve 0 f 
 Marches contained , F 5 
 : pieces , Numbers Best Arrangements , & c. sold half - price . : 
 LONDON : NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C. f 
 ’ f 

 220 MUSICAL TIMES.—Sepremser 1 , 1875 




 NOVELLO 


 ORIGINAL OCTAVO EDITION 


 OPERASEdited , Corrected according Original Scores , 
 Translated English , 
 NATALIA MACFARREN BERTHOLD Tours . 
 Price 2s . 6d . ; scarlet cloth , 4s . 
 READY 

 BEETHOVEN FIDELIO 
 ( German English words 

 great Overtures usually performed ; " 
 Piano Score published agreeing Original 
 Score notes signs phrasing 




 AUBER FRA DIAVOLO 


 MOZART DON GIOVANNI 


 BELLINI NORMA 


 VERDI IL TROVATORE 


 DONIZETTI LUCIA DI LAMMERMOOR 


 WEBER OBERON 


 ROSSINI IL BARBIERE 


 DONIZETTI LUCREZIA BORGIA 


 MOZART NOZZE DI FIGARO 


 VERDI RIGOLETTO 


 BELLINI LA SONNAMBULA 


 WEBER DER FREISCHUTZ 


 WAGNER TANNHAUSER 


 AUBER MASANIELLO 


 BELLINI PURITANI 


 WAGNER LOHENGRIN 


 DONIZETTI LA FIGLIA DEL REGGIMENTO 


 ROSSINI GUILLAUME TELL 


 MOZART DIE ZAUBERFLOTE 


 VERDI LA TRAVIATA 


 FLOTOW MARTHA 


 VERDI ERNANI 


 MOZART IL SERAGLIO 


 GLUCK IPHIGENIA TAURISSubscribers ’ Names received Publishers , NovELLo , 
 Ewer Co. , 1 , Berners - street , 35 , Poultry , London ; J. L. 
 Peters , 843 , Broadway , New York ; Music Booksellers 

 BEETHOVEN 
 SONATAS 

 NEW COMPLETE EDITION . ) 
 EDITED FINGERED 




 AGNES ZIMMERMANN 


 PRICE GUINEA , 


 SINGLY : — 


 LONDON : 


 NOVELLO , EWER CO . , 


 BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 M4 


 NEW YORK ; J. L. PETERS , 843 , BROADWAY 


 Y2 


 HILES ’ SHORT VOLUNTARIES 


 ORGAN 


 BOOK XXVIII 


 BOOK XXIX 


 BOOK XXXPraise Lord , O soul . Anthem Sir John Goss 
 BOOK XXXII 

 Adagio un poco moto ( Op . 73 ) Beethoven 

 Andante Grazioso ( Quartett Op . 74 , . 2 ) Haydn 




 BOOK XXXIII . Adagio ( Quartett ) ae Kozeluch 
 Voluntary . Allegro Moderato 
 Peter , faithless , thrice denies . Choral J. S. Bach 

 Thranenregen F. Schubert 
 O wondrous love . Choral ( Passion , S. Fohn ) ) J. S. Bach 
 Thou didst free   pmemeesie Mendelssohn 
 Moderato quasi Adagio ( Sonata ) Kalkbrenner 
 BOOK XXXIV . 
 Choral ( Op . 58 ) ... aa Mendelssohn 
 O Love Lord . Anthem ... .. A. Sullivan 
 Ich dich mit Fleiss bewahren . Choral J. S. Bach 
 Musette J.P. Rameau 
 Religious March ( King Stephen ) ... Beethoven 
 Chorus Apostles holy warriors F. Schneider 
 went daughter Jephtha --- Carissimi 
 Pilgrim Home . Hymn . .. E.H. Thorne 
 O Lord , dares suite Thee . oom J. S. Bach 
 Allegro ( Fantasia ) ax A.P.F.Boély 
 Voluntary " Léfébure - Wély 
 Andante con moto ‘ ( qth Symphony , Op . 90 ) Mendelssohn 
 clear beams . Chorus(King Stephen ) Beethoven 
 Thou wilt keephim perfect peace . Anthem Dr. S.S.Wesley 

 Prelude Julius Andre 
 floors shall wheat . Anthem . Sir John Goss 
 BOOK XXXV 




 BOOK XXXVI 


 LONDON : NOVELLO , EWER CO 


 COMPLETE EDITION 


 MENDELSSOHNS SONGS 


 EDITED TRANSLATED 


 NATALIA MACFARREN 


 LONDON : NOVELLO , EWER CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 223 


 NOVELLO OPERA CHORUSES 


 EDITED 


 NATALIA MACFARREN BERTHOLD TOURS 


 DONIZETTI LUCIA 


 WEBER OBERONAUBER FRA DIAVOLO . 
 : des , fill glasses ... « . 4d . | 28 . Let roam ruins 51 . Light fairy foot fall 2d , 
 : “ bons militaires . deserted ro Lieve il pie cola volgiam . 
 2 , Hail , festal morning .. ... Pies Percorriamo le spiagge vicine . 52 . Honour joy .. whe aun ad . 
 Crest grand féte . 29 . Hail , happy bridal day ow Gloria ! ommagio . 
 te d’'immenso giudilo . 53 . Glory tothe Caliph ... feb oll 
 AUBER MASANIELLO . = vo ee 2d . 54 . po pee coral cave ... 4d 
 l bright auspicious d : pax Chi vafrena ilmio furore . Chi potria fra l'onde restar . 
 - 7 edad aonaiaine 3 t. warlike minstrelsy ...   ... 1d . | 55- thee hath beauty omen 
 4 . hail bright ey mara day ... 1d . ’ D'immenso giubilo . bag omy ah 3d . 
 Du Prince objet de notre amour . ‘ . 
 5 . Oholy Power ... wee > d » DONIZETTI LUCREZIA 56 . . . ( Mixed voices ) 2d . 
 O Diew puissant ! BORGIA . 
 6 . Companions , come . abereets WEBER 
 chorus ) pee 2d . 32 . word BOP tee ae ee ee DER FREYSCHUETZ . 
 Amis , amis . Non far motto . 
 7 . Behold morn splendour « » 2d . | 33 . far mots 1d , | 5 % Victoria , victoria sae 1d . 
 Amis la matinée est belle . Rischiarata é la finestra . Victoria , Victoria 
 8 . Come hither wish buy 34 . know away 58 . Wire beng oe oes wetind . 1d . 
 ( Market chorus ) sorrow sy wile ty winden dir den Fungfernkranz . 
 Au marché qui vient de s'ouvrir . Il segreto . esser felice . 59 . joy Hunter . { untsman 
 come , willavenge thee ... 3d . " ictehi ee Ee 2d . 
 | PA ncion , th Ag sas MOZART DON GIOVANNI 

 fo , teil , noble victor . ( March ose aie : wee . sone GUILLAUME TELL 
 . . invites eco Ee . 
 d ch + le ” 
 chorus ) ee oe iit ’ 6d Giovinette , che fate ’ amore . 63 : rosy morn et 
 uel jour serein . 
 ’ MOZART 62 . Come , flowers crown bowers 2d . 
 BEETHOVEN FIDELIO . LE NOZZE DI FIGARO . Hyménde , ta jowrnde , 
 10 . Oh er setiehn . ( Prisoners ’ chorus ) 3d . pon 63 . ae — _ mp coon sounding 2d . 
 welche Lust . 36 . Come deck flowers os uelle sauvage harmonic . 
 11 . Farewell , thou warm sunny beam 4d . Giovani liete . 64 . Hail tne mighty ruler 2d . 
 Leb ’ ‘ wohl , du warmes Sonnenlicht . 37 . Noble Lady , fairestroses ...   ... 1d . loive au pouvoir supreme . 
 Ricevete , o padroncina . 65 . Swift bird summer ~ 
 BELLINI PURITANI . 38 . voicenowrejoices   .. ... Id. yg ore ree . 
 manti , costanti . que Voiseau ne suivrait pas . 
 12 , yonder bugle calls ... rahe 
 Quando la tromba sate ROSS . GLINKA 
 13 . Rejoice ! bon , don « 94 » 3s INI IL BARBIERE . LIFE CZAR . 
 festa . ; 
 14 . Noble Arthur , welcome ote 39 . Sir , humbly thank honour ... 2d . 66 . Noble Chief ! thee hail ( eindaee 
 culls Atince - enery . . Mille grazie , mio signore . Chorus ) po 3 
 15 . soug ’ Fi ) hee 
 ‘ 4 06 0 cara . ‘ VERDI IL TROVATORE . MOZART 
 16 . Fatal day .. Se eer eer + 
 Ahi ! dolor . 40 . darkness deine DIE ZAUBERFLOTE . 
 dissolves . ( Gipsy chorus ) .. 1d . 67 . Oh Isi iri 
 BELLINI NORMA . é Vedi le fosche pr 7 sis Osiris ( Chorus Priests ) 1d . 
 rt . dice invit 1 es . 
 ” Heaten , ye peti beighte ascend ad . | * co ’ dad ma ira poco . " VERDI LA TRAVIATA . 
 18 , Norma cometh ” + gyncg ae senaPtinaeie St W{Deiahing som rae nae beckoning 
 Novma viene . rinki jorus ) ad . 
 19 . sant " = et linger ... VERDI RIGOLETTO . Libiamo , ne lieti calict . 
 9 . x pe hPa y ? y g 1d . 69 . brave " Matadors ( Chorus 
 Non parti ? finora al campo . z 
 20 . V 2 +9 acs Tieng 43- Hush , silence fulfil ourerrand ad . Spanish Matadors ) . 2d . 
 " Guerra , ‘ guerral Zitti , zitti , moviamo vendetta . Di Madride noi siam Mattadori . 
 44 . Unto lonely abode directed . cer ? © 7@. Lo , pride people 
 BELLINI Scorrvendo uniti remota , Taw decd lag 1d . 
 LA SONNAMBULA . : 
 WAGNER LOHENGRIN ’ 
 2 . Hail ! Amina . d e FLOTOW MARTHA . 
 Viva ! viva Amina ! 
 F 45- hath summoned betimes 2d . 71 . Bright buxom lasses ( Chorus 
 ~ nae Binetc non oho ve Id. Friih'n versammelt uns der Ruf farmers ) pi < r 
 23 . dusky twilight . sa 4 46 . follow leads ! ... oe 3 Madchen brav und treu . 
 Ah fosco se ny es ooo , 1G. Zum Streite séumet nicht ! 72 . Finale . fair beaiee sound 
 4 . ’ moment shelter OY cee een ) Der Markt begins . ” * 
 rest . 2 e 
 48 . Faithful true lead ye forth .... 1d . 
 Qui la selva 2 pit folta ed ombrosa . Treulich gefihrt xichet dahin , VERDI ERNANI . 
 DONIZETTI LA FIGLIA . WAGNER TANNHAZUSER 73 . Day Fr gladness Catroduction galop ? 
 Rey jeateensor jis 49 . Hail , bright abode ( March ) . R Praline lumb : 
 ail , bright abode ( Marc : eee . Rouse lo li 
 26 , Hark , drums arerolling ... 1d . Freud ig begriissen . - " kere th mae gla — 1d . 
 @. Rate wg 4 = — e incora . - 50 . bar Bas oy joy . ( Pilgrim - eee il Leon . 
 . ’ ee ee aay . Pik . welcome , hail thee ( Chorus ) 1d- 
 Rataplan , vataplan . cipealtckt darf nun dich Oh come felice . = e 

 continued 




 LONDON : NOVELLO , EWER & CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C 


 NEW YORK : J. L. PETERS , 843 , BROADWAY 


 NOVELLO 


 CIRCULATING MUSIC LIBRARY 


 1 , BERNERS STREET ( W 


 65,000 WORKS 


 REGULATIONS