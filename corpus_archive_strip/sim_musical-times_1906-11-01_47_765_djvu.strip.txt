


THE MUSICAL TIMES


FOUNDED IN 1844. 


PUBLISHED ON THE FIRST OF EVERY MONTH


ROYAL ALBERT HALL


HIAWATHA -UNDON 


NZ 4


MADAME EMILY SQUIRI SOCIETY


MR , Q . MICHAELMAS HALF-TERM begins Monday, November s. is lo-play Tests are:—Sonata No. 2 in C minor (1st movement), 
LECTURES by Sir A. C. Mackenzie, Mus.D., F.R.A.M., on | J. yo Vol. 1, p. 11); (Novello & Co., Book 4, Pp. 97); 
Che Bohemian School of Music,” Wednesdays, Nov. 7, 14 and 2t (Aug rener & Co., Vol. 8, p. 520); (Breitk pf & Hartel, Vol. 6, p. 25). 
t 3.1 |e igue in A flat m Brahms (Alfred Lengnick, 58, Berners Street, W.); 
CHAMBER CONCERT at Queen's Hall, Monday, November 19, | vello & Co.); (Augener & Co.). Sonata No. 16, G , See minor 
t3p.m . | ( rol 2 movements), Rheinberger, Op. 175 (N vel & Co.); 
mc F = (Augener & C

BR Ri )UGHTON PACKER _- H St = eae my » for Vi . ——4 The A.R.C. Oo E xamination begins on January 14th. The subject for 
— Male and Female — lidates, and Violoncello(Male Candidates | 1}. Essay will be taken from “ Beethoven and his Nine Symphonies, 
y) Last day wor a re en = saa #3 : | by Sir George Grove, C.B. (Novello & Co

GEORGE MENCE SMI [TH SCHOLARSHIP, for Female] “To be obtained of the Publishers or any Booksellers. (of at the 
calists (any voice). Last day for entry, December 20. | College




- . ANT) SCHESTR SOCIETY 


718 THE MUSICAL


1906


ADE MY OF


ROYAL J “MUSIC


METROPOLITAN EXAMINATION, 5S


I CANDIDATES 


IN HARMONY 


IN SINGING 


IN PIANOFORTE PLAYIN( 


O M ( I ( ( 1 H J 


| I W W W \\ I I 


\\ ( \ \ M W I 


( N ( S I 


W I E. | I M 


\N PLAYVIN( 


I \ H ’. H.W 


IN HESTRAL INSTRUMENTS 


1OLIN AVING 


ILONCEI ) LAVIN 


\ I I H M


J I H


THE ASSOCIATED BOARD 


YAL ACADEMY OF MUSIC AND ROYAL COLLEGE O} 


IC FOR LOCAL EXAMINATIONS IN MUSIC


CH EXAMINATIONS (5S B) 


I M I 


R.C.M I \ 


I \ I 


NATIONAL SOCIETY OF MUSICIANS


HE GUILDHALL SCHOOL OF MUSIC. 


EC


ROYAL 


MANCHESTER COLLEGE OF MUSIC. 


BIRMINGHAM AND MIDLAND INSTITUTE


SCHOOL OF MUSIC


SESSION 1906-1907


VICTORIA COLLEGE OF MUSIC, 


| LONDON. 


INCORPORATED 1861. 


CORRESPONDENCE SCHOOL OF MUSIC, 


LONDON


GRADUATED POSTAL LESSONS IN HARMONY


COUNTERPOINT THEORY OF MUSIC, FORM AND 


NATIONAL CONSE RV ATOERS


PIANISTS AND | 


DISESTABLISHMENT


OF 


SCALE PRACTISING AND 


EXERCISING


FIVE-FINGER


THE 


NEED. 


MACDONALD SMITH’S SYSTEM


AND TECHNIQUE 


BRAIN TO KEYBOARD


ERFECT TOUCH


FROM


THE RESULT


ARE, W.C


INCORPORATED GUILD OF


MUSICIANS 


CHURCH


ASSOC TATH E (A.1.G.C.M.), LICE NTIATE (L.1.G.C.M.), FEL- 


NATIONAL COLLEGE OF MUSIC


A.N.C.M., L. -M., F. N.C.M


SOCIETY 


ONLY SEVCIK “SCHOOL” USED 


PROFESSIONAL NOTICES


CREA


CHANGE OF ADDRESS


MISS EMILY


MADAME


MR. 


MISS BESSIE HOLT 


ALEXANDER GUNNER 


MR. ASHTON THOMAS 


(TENOR) 


HUGH WILLIAMS 


MISS FLORENCE


PROFESSOR KOENIG 


MR. SIVEY LEVEY 


RECITATIONS AT THE PIANO. 


BOW NES


SOPRANO VOCALIST). 


MISS ELL EN CHIL DS


CHROMATIC 


NEW CHROMATIC 


HARPIST


MISS ESTELLA LINDEN


PRINC 


MISS AMY SARG I: NT


MISS E


4.R.A.M. (SOPRANO). 


MILY SHEPHERD


A.R.C.M. (SOPRANO) 


720 THE MUSICA


L TIMES


NOVEMBER 1, 1906


INTRODUCTION


SOPRANO


MONTI TRUSLOVE 


SOLO VIOLIN


GRAND RECITAI


ATURDAY, NOVEMBEI


NYE


MASTER


MISS ETHEL RADBURN


SOPRANO


MISS M \RY_ SCHOLES


MISS AG NE S WALKER


DRAMATIC SOPRANO


MISS LILL L, AN DEWS (L.R.A.M


CON TRALTO


MISS EMILY HART


CONTR ALTO). 


MR. HE NRY BE AU MONT 


MR. TOM CHILD 


MR. DOUGL \S G R. INV 1. L E


MR. ELIHU MITCHELL


CHANGE OF ADDRESS


MR. ANDERSON NICOL


CHANGE OF ADDRESS


MR. F RE DE RIC K NORCU


MR. ERNEST PIKE 


MR. HE ENRY ? PLEVY


MR. GWILYM RICHARDS


GEES


JIM STE EL


MR. W ILLIAM WIL D


TENOR VOCALIST). 


MR. MONTAGUE BORWELL


BARITONE) 


MISS WINIFRED MARWOOD


ARTHU R | W AL ENN


MR. 


(BARITONE


MO


MR 


MR. HERBERT 


(BASS-BARITONE). 


MR. JAMES COLEMAN


CARGILL


F RE DE RIC K


PRESTON


PRINCIPLES 


MODERN PIANOFORTE


TECHNIQUE


SOLO BOYS


MR. . DUTTON'S SOLC


VOICE, SONG, AND SPEECH


I EVISION OF MUSICAL COMPOSITIONS 


SUCCESSES; A.R.C.M., 1897-19006, ONE HU NDRED AND 


THE


MUSICAL


722 


DR. LEWIS’ TEXT 


I ‘ M 


P \ BULARY M \ 


ACCOMPANIST


D*: HAMILTON ROBINS 


4.R.A.M F.R.C.O


PIAN


COUNTERPOINT, COMPOSITION 


UT


L.R.A.M., A. RC. M., 


\ SPECIAI COACHING COURSE


M FLORENCE WEEI 


N\ R. PERCY WOOD, F.R. 


4 MONY, COUNTERPOINT, & 


' F.R.C.O 


I ATH 1 


SUCCESSES


KING'S COLLEGE


HORISTERSHIPS A TRIAL OF


COU RSE » | 


RGAN PRACTICE


C. O., 


HOIR EXCHANGE


- ; 136, FIFTH AVE., NEW


BEL 


RA


O.P


THE MUSICAL


1900


LLO PLAYER 


IANOFORTE TUNER-REI 


EACHING CONNECTION 


ORGAN (2


ORGAN


MUSIC 


TUNER


INDIA WANTED


SULSORD


SALE


PLEASE


ORGANS. 


I "ALMAINE PIANOS AND 


CLEARANCE SALE 


NOTE


ALSO SPECIAL


PIANO PEDALS


EVERY ORGANIST


PIANO 


AS USED BY MAN


ISTS


N.W. 


ORIGIN. AL “HOPE” VIOLIN, 


RACTICE. ROOM


N USIC STUDIOS. 


304, 


ME SSRS.1 ? UT


P. CONACHER & CO., 


SPRINGWOOD WORKS, 


HUDDERSFIELD. 


TWO GOLD MEDALS. 


NICHOLSON AND CO., 


ORGAN BUILDERS, 


PALACE YARD, WORCESTER. 


(ESTABLISHED 1841.) 


VOVELL


724 THE


TRINITY COLLEGE OF MUSI 


LONDON


INSTITUTED 1872


MUSICA


LORD COLERIDGE, M.A., K.C. 


TEACHING DEPARTMENT


LOCAL EXAMINATIONS


LOCAL EXHIBITIONS


SCHOLARSHIPS


S. COLERIDG 


INCIDENTAL MUSK


E-TAYLOR’S 


» STEPHEN PHILLIPS’S DRAMA


NERO


EASTERN DANCE: 


COLLEGE ORGANISTS


FOR FELLOWSHIP


ROYAL


EXAMINATION


JANUARY, 1907. 


1900


BOSWORTH EDITION. 


ENRICO BOSSI. 


‘PARADISE LOST.” 


JOHANNES BRAHMS. 


‘ REQUIEM.” 


AND CO., 5


SONG OF THANKSGIVING 


FROM SUITE


LONDON DAY BY


OXFORD ST., ¥ 


PRINCES ST., 


I » VIENNA ANI


BOSWORTH


DAY


GRAND OPENING SERVICE 


HEREFORD MUSICAL FESTIVAL, 


BOSWORTH & CO., 


NOVELLO’S MUSIC PRIMERS


NOVEMBER 1, 1906


BRISTOL


ST. MARY REDCLIFFE CHURCH, FROM THE NORTH-EAST, 


THE CHATTERTON MONUMENT IN THE FOREGROUND


THE HANDSOME IRON GATE


S OF QUEEN ANNE’S TIME


1, 4 


THE NAVE, LOOKING WEST. 


43


TI 


THE REREDOS AND ORGAN


THE MUSICAL


H 4 


\ DD


REDCLIFF CHURCH, BRISTOL


4 = =——J == = 


VIEW FROM NORTH TO SOUTH


735


LAWSON


MR. J. W. 


ASTI FRE


IRN


LADY VIOLINISTS


THE MUSICAL TIM


OS 


10, 


THE SISTERS MILANOLLO


CHARLES RAQUEILLE! 


ELIZABETH FILIPOWICZ, 


GAUTHEROT, ATHEN.EUM, February 20, 1869

It is a strange coincidence that a lady violinist playing 
music of the highest class should be just now drawing 
attention to herself in Paris and in Boston. In the 
former capital Madame Norman- Neruda performed 
Mendelssohn’s concerto at the last of M. Pasdeloup’s 
concerts, while Madame Camilla Urso has several times 
of late played Beethoven’s concerto in the American 
city. The fair sex are gradually encroaching on all 
man’s privileges

Man’s privileges’! What would Mr. Chorley 
now say to the legion of lady violinists




1872


THE MUSICAL TIM


740


ES.—NOVEMBER 1, 1906


F. G. E


OLD FIDDLERS’ BOOKS


SO


A BECK


DRUM IN


CAN


THE PLAINS OF WATERLOO


THE MUSICAL TIM


1906. 741


ANDREW MARVELL (1681


TYOS¢? 


LTOSSE


742 THE MUSICAL


TIMES


NOVEMBER 1, 1906


I IS


IIL., 


OSTA’S DEBUT AT BIRMINGHAM 


AND AFTER


NICCOLO ZINGARELLI


744 THE MUSICAL TIM


ES.—NOVEMBER I, 1906


A YORKSHIRE CHOIR IN GERMANY


| 4 7” 


142 149The social functions in which the West Riding 
singers participated added not a little to the enjoyment 
of their visit to Germany. Warmly welcomed and 
most hospitably entertained, our well-tuned Yorkshire- 
folk had every reason to be gratified with the reception 
so enthusiastically accorded to them. May they not 
have added another note to the harmony of that 
‘entente cordiale’ which, under the wise leadership of 
| our gracious King, is doing so much to unite England 
with other nations in the bonds of peace and goodwill’ 
In this connection genial telegrams were received 
from King Edward and the Kaiser in response to 
| messages sent to their respective Majesties

On the homeward journey a deputation from the 
|choir placed a wreath on the tomb of Beethoven at 
| Bonn, and on Saturday, September 29, after just a 
| week’s absence, the singers returned safe and sound 
|to their respective cities, having experienced a most 
enjoyable tour. In the admirable arrangements made 
for the comfort and convenience of so large a party, 
|no less than in the musical and financial part of the 
| proceedings, special and honourable mention must be

ase eae




ES.—NOVEMBER 1, 1906. 745


THE MUSICAL TIM 


1S


JULIUS STOCKHAUSEN


CHURCH MUSIC AT THE BIRMINGHAM FESTIVAL


746 THE MUSICAL TIME


MUSIC AT THE CHURCH CONGRESS. 


1790, 1 


WESI 


PEST


THE MUSICAL TIMI


2S.—NOVEMBER 1, 1906. 747


DR. JOHN WORGAN


WESLEY'S HYMN-TUNES ‘ AURELIA’ AND ‘ EDEN,’ 


13


ESTIVAL SERVICE AT WINCHESTER CATHEDRAL


WALKER ORGAN


| ( ER 


O} ANIST AND CHOIR APPOINTMENTS


CHOIRMASTER, This volume commences with Palestrina, after which 
follow Purcell, Bach and Handel, then the great composers 
of the so-called classical and romantic periods, and so on 
through Verdi and Wagner, Berlioz and Liszt, up to and 
including Richard Strauss. The earlier composers are 
generally spoken of as ‘old masters,’ and therefore would 
seem to be out of place in a volume bearing the above title ; 
but the author remarks that ‘to the union of rhythm with 
harmony modern music owes its birth,’ and that justifies the 
inclusion of music of the 16th, and still more so of the 17tl 
and 18th centuries

The author's aim in writing the book was not to relate the 
lives of great composers, or to discuss their art-work, 
excepting so far as these served to trace ‘the growth of a 
poetic basis in music.” Thus once more we are face to face 
with the vexed question of programme verszs abstract music. 
The fact is duly recognized that ‘ programme’ music existed 
before Haydn, but the author considers his attitude 
towards the ‘poetic basis in music’ of importance; he 
indeed thinks Haydn might well be called, not only the 
‘ father of the symphony,’ but the ‘ father of the symphonic 
poem.’ Mozart never gave a name to any of his works, 
so it seems very probable that in the titles ‘ Eroica,’ 
‘ Pastoral,’ Beethoven was influenced to some extent by 
those which Haydn placed at the head of many of his 
Beethoven, of course, plays an important 
role in the discussion, and Mr. Streatfeild justly says that

all that great master wrote after he reached maturity is




ANTHEM FOR SOPRANO OR TENOR SOLO AND CHORUS


SOPRANO. 


4% ‘ = NS] 


TENOR. — 


| ? = 2 | 2 3 P =4 $ = - 


} 5) J - | ‘| - 4 


—_—_——— — 


1 _———— 


GLORY TO GOD IN THE HIGHEST


7! L : 


——_—_——— 


—_—_—_——_— _—_ 


5 0


V4 . = = | - = = = 


_ | -_ ~~ — -_ 


. = 9, — 7 


” — — ——— SE 


=) * 2 } . - 1%, @ 4 + — | 


6 > | 2 | 


PP 


- - MPA —— —— + 


GLORY TO GOD IN THE HIGHEST


——— —_—_ 


FA 


. -_——_ 


4H + = 


3 _ 


—_—_— 


_—$$$$$ $$ $$ — -— — 2 


—_—_—- ———_————_. —<—$<<$——._ 


— _—_ 4 


1PI


_ — — LO 


PP 


* 52 7


_ - —_—_ «<3 


THE MUSICAL TIM


1732 


ES.—NOVEMBER I, 1906. 757 


BOOKS RECEIVED


THE BIRMINGHAM MUSICAL FESTIVAL. 


(BY OUR SPECIAL CORRESPONDENTment three years ago, and I think it was more than 
maintained at the recent festival, when was heard 
some of the most musical, refined, and highly-finished 
| chorus-singing within my experience. The Birmingham

tenors are unrivalled for fine quality,—it is said that the 
choirmaster can afford to reject all voices that are not of 
pure tenor quality. The basses were exceedingly fine ; they 
have not quite the ring of the Yorkshire basses, but their 
volume of tone was magnificent and impressive. The 
sopranos were also very good, and only a little stronger 
stamina, which would have made the trying altitudes of 
Beethoven’s great Mass more easily compassed, was wanting 
to render them altogether exempt from unfavourable 
criticism. |The contraltos were not conspicuous,—the 
character of the voice precludes it,—but were worthy of a 
chorus that was well balanced in its exsemd/e as well as 
excellent in its parts. Throughout the week it showed a 
familiarity with its task, for which Mr. R. H. Wilson, the 
| Manchester choirmaster, deserves a chief share of the praise, 
| for he had evidently done his work most thoroughly

While dealing with the executive forces, I may add that 
| Dr. Richter had command over an orchestra of 12




758 THE


THE59

he great event of the festival was, beyond a doubt, the 
performance of Beethoven’s colossal Mass on the Friday 
morning. All concerned, from Dr. Richter downwards, did 
their utmost, and the result was the most deeply impressive

reading within my recollection. Richter realizes almost 
bettcr than any other the majesty and depth of Beethoven, 
and while I have heard the work perhaps more brilliantly 
given I have never listened to a performance which seemed 
to go so far into the heart of the matter. The chorus sang

finely, and the sopranos left only the slightest sense of effort 
in attacking their exhausting passages. But the great merit 
of the performance was that one lost sight of technique and 
had one’s attention firmly fixed on the essential qualities of 
the music; there was no straining for effect, but only a desire 
to worthily interpret a masterpiece. The solo quartet, 
Miss Agnes Nicholls, Miss Muriel Foster, Mr. John Coates 
and Mr. Robert Radford, was altogether admirable. Miss 
Nicholls deserves a special word of praise for her highly- 
artistic singing, and Mr. Radford’s 
bass quality, gave remarkable dignity to his part. 
Mischa Elman played the violin solo in the Benedictus most 
artistically, but one felt he was too young in spirit to quite 
realize the nature of his task. In Bach’s motet for double 
chorus, ‘Sing ye to the Lord,’ we had a fine piece of 
unaccompanied singing, forceful yet always perfectly musical. 
Of the more familiar choral works ‘ Elijah’ met with the 
best treatment, receiving an exceptionally fine interpretation, 
full of colour, bright and well polished. The ‘ Messiah,’ one 
is sorry to say, was less satisfactory, a feeling of slackness 
pervading the whole performance. Stanford’s ‘ Revenge’ 
was well sung, if hardly with the freedom of declamation it 
demands, and the ‘Hymn of Praise’ furnished a popular 
finale to the festival

The most noteworthy orchestral performances were of 
Brahms’s first Symphony and the ‘Don Juan’ and ‘Tod 
und Verklarung’ of Richard Strauss. Three of Wagner’s 
overturesand the ‘Carnaval Romain’ of Berlioz were brilliantly 
played, and Mischa Elman gave artistic and wonderfully 
mature readings of Beethoven’s and Tchaikovsky’s Violin 
concertos, the latter receiving a really superb interpretation, 
fully realizing its exuberant emotion without conveying any 
sense of exaggeration. In addition to the vocalists already 
named, Madame Albani, Miss Gleeson-White—who sang a 
fine air from ‘Euryanthe’ with much dramatic fire—and 
Mr. John Harrison took part. A word due to Miss 
Agnes Nicholls for her extremely brilliant singing of a fine, 
florid air from ‘Il Seraglio,’ and Miss Muriel Foster, who, 
to the grief of all musicians, is soon to retire from her 
brilliant public career, made her farewell to the Birmingham 
festival in a most interesting solo cantata by Christian 
Ritter, ‘OQ amantissime sponse Jesu.” This precursor of 
Bach and Handel is little more than a name to us, but the 
cantata, which has recently been revived, and edited by 
Mr. Richard Buchmayer, reveals a great power of expressing 
quiet devotional sentiment. Few artists could make it as 
interesting as did Miss Foster, and her noble, moving 
interpretation intensified one’s sense of the loss which her 
retirement will cause

is




INTERNATIONAL MUSICAL CONGRESS 


AT BASLE


BY OUR SPECIAL CORRESPONDENT.) 


760 THE


CHARLES


MACLEAN


FESTIVAL


CORRESPONDENT


HOVINGHAM MUSICAL


BY OUR SPECIAIThe choral compositions were Bach’s cantata ‘ Sleepers, 
wake’; Haydn’s ‘ Spring,’ from ‘The Seasons’; Dvorak’s 
‘Te Deum’; Elgar's ‘ The Black Knight,’ and Coleridge- 
Taylor’s ‘Kubla Khan,’ a thoroughly interesting and 
representative group of works. ‘Kubla Khan,’ which was 
the nearest approach to a novelty, is a characteristic example 
of Mr. Coleridge-Taylor’s individual style. He has set the 
poem to music which, in its richness of colour and wealth of 
melody is most appropriate, and the romantic picturesqueness 
of the verses has its equivalent in the score. The contralto 
solo was very artistically sung by Mrs. Burrell, and the 
performance was vigorous, if not as varied and subtle in 
expression as some others during the festival. Generally 
speaking the chorus-singing was very bright and intelligent, 
and showed a distinct advance in refinement, while the 
orchestra was quite excellent

Beethoven’s Violin concerto was played by Mr. Kruse, 
and Mr. Withers gave a brilliant reading of Saint-Saéns’s 
Violoncello concerto in A minor. Both artists were heard in 
Brahms’s double concerto, which they played admirably, 
making one regret that opportunities are so rare of hearing a 
work the austerity of which diminishes as its not too obvious 
charms are gradually revealed. Canon Pemberton conducted 
a bright performance of Mozart’s so-called ‘ Jupiter’ 
symphony, and the other orchestral pieces were Mendels- 
sohn’s ‘* Hebrides’ overture and Stanford’s first Irish 
rhapsody, of which Mr. Noble gave a remarkably brilliant 
and interesting reading. Throughout the festival indeed 
Mr. Noble proved his fitness for the task he had undertaken, 
and with further experience of what it involves he should 
go still farther, adding to the vitality of his readings an 
increased dignity and repose

The festival opened with an interesting chamber concert, 
the most important thing in the programme being Stanford's 
recent Serenade in F for strings and wind, in which Messrs. 
Claude Hobday (double-bass), Eli Hudson (flute), W. H. Hall 
(clarinet), Borsdorf (horn) and E. Hall (bassoon) joined 
the Kruse (Quartet, which was also heard in string quartets 
by Beethoven and Grieg. The serenade has been heard on 
one occasion in London, but is still unfamiliar. Its melodic

charm and beautiful structure should make it a general




761


BLACKPOOL


KEIGHLEY. 


PROMENADE CONCERTS. 


762 THE MUSICAL TIM


ROYAL ITALIAN OPERA


THE REV. H. E. HODSON’S ‘GOLDEN LEGEND


M. I 


THE MUSICAL TIM


ES.—NOVEMBER 1, 1906. 763


PIANOFORTE RECITALSring and very welcome in these days of storm and stress

Although prodigies are now too plentiful to be accounted 
prodigious, except in numbers, record must be made of the 
first appearance in England, at the Royal Albert Hall 
Sunday afternoon concert on October 14, of Pepito Arriola, 
a Spanish boy, born at Ferrol on December 14, 1896. 
\ sturdy little fellow with bright eyes and dark hair, his 
musical gifts were so pronounced at the age of three that he 
was examined as a phenomenon at the Psychological 
Congress then assembled at Paris. Herr Nikisch, while on 
tour in Spain with the Berlin Philharmonic Orchestra, heard 
the boy play and persuaded his parents to send him to 
Leipzig to study, a grant being secured for that purpose 
from the privy purse of the King of Spain. At the Albert 
Hall he was heard in Beethoven’s third Pianoforte concerto, 
n which he played with a beauty of touch, sympathetic 
expression and clearness of phrasing more amazing than his 
wonderful executive facility. It is satisfactory to note that 
his parents have no intention of exploiting him as a wonder- 
child, but an occasional appearance in public is considered 
advisable to encourage him in his studies

The two most important pianoforte recitals of the past 
nonth were those given respectively by Mr. Mark Hambourg 
and Signor Busoni, on Saturday afternoon, October 20, the 
former playing at ()ueen’s Hall and the latter at Bechstein 
Hall. A remarkable coincidence on this occasion was that 
both artists included in their programmes Beethoven’s great 
sonata in C minor (Op. 111) and, designedly or otherwise, 
the work was so placed that enthusiastic amateurs were able 
to hear the interpretation by both artists. Those who did so 
had an educational experience, for each player is a master of 
his instrument and has the courage of his opinions

LIN RECITALS




VARIOUS CONCERTSMadame Clara Butt made her re-appearance in public at 
her annual concert at the Albert Hall on October 13, when 
she was assisted by Mr. Kennerley Rumford, Miss Edith 
Evans, M. Holman and the London Symphony Orchestra 
conducted by Dr. Cowen. She introduced a new song 
entitled ‘ The exile,’ by Cecil Engelherdt, and Mr. Rumford 
|sang for the first time a cycle entitled ‘Songs of the 
Norseland,’ by Herman Lohr. The latter has a picturesque 
orchestral accompaniment, but otherwise does not call for 
comment. The audience was large and enthusiastic

The London Trio, consisting of Madame Goodwin, 
Messrs. Simonetti and Whitehouse, commenced its ninth 
season on October 19 at .Kolian Hall. The programmes 
included Vincent dIndy’s Trio in B flat (Op. 29) and 
Beethoven's Trio in E flat, which were excellently rendered. 
Some tasteful singing by Miss Neill-Fraser provided 
agreeable variety

Master Lionel Ovenden, the remarkably clever English 
violinist and pianist, played with noteworthy success at the 
Crystal Palace on October 13. Another violin recital during 
the past month worthy of record was that of Miss Gertrude 
Turner-Schaerer on October 3 at .Eolian Hall




MUSIC IN AMERICA. 


OWN CORRESPONDENT.) 


FROM OUR


764 THE MUSICAL TIM


MUSIC IN BELFAST


FROM OUR OWN CORRESPONDENT.) 


MUSIC IN BIRMINGHAM. 


(FROM OUR OWN CORRESPONDENTpresently refer

The season practically opened with a pianoforte recital, 
given in the Masonic Hall on September 9 by Mr. Arthur 
Cooke, a local performer of pronounced ability and talent 
who is likely to attain a prominent position in the ranks of 
our foremost English pianists. The programme, whic! 
ranged from Bach and Beethoven to Liszt, included several 
novelties, the most interesting being a Khapsody by Ju 
Harrison, a student of the Midland Institute of Music

The forecast of the season reveals an exceedingly interesting 
scheme, the most important features naturally being centred 
in the doings of the various choral and orchestral societies 
which have now completed their programmes for the winter. 
I am glad to be able to state that a prominent place has 
been assigned to Sir Edward Elgar’s new oratorio ‘The 
Kingdom,’ produced at the recent festival, inasmuch as the 
work will be given twice during the season, namely, by the 
Festival Choral Society under Dr. Sinclair, and by the City

Choral Society under Mr. Fred W. Beard. The inclusion 
of Mendelssohn’s ‘ St. aul’ in the Festival Choral Society’s 
| programme will no doubt prove a welcome revival, as it is 
|nine years since this Society gave Mendelssohn’s earliest 
|oratorio. The scheme also includes Sullivan’s ‘ Golden 
Legend,’ Beethoven’s Mass in D, and Bach’s motet ‘ Sing 
ye to the Lord

Owing to the absence of Mr. Fred W. Beard, who is 
fulfilling an engagement in Australia, the City Choral 
Society’s first concert of the season will be conducted by




(J. A. 


THE MUSICAL TIM


ES.—NOVEMBER 1, 1906


MUSIC IN EDINBURGH. 


(FROM OUR OWN CORRESPONDENTThe prospectus of the Orchestral Concert series is now 
issued, and gives every reason to hope and expect that 
in all respects the high standard and fine tradition of 
the t will be maintained. The band will be as hereto

fore, with Mr. Henri Verbrugghen as leader. Dr. Cowen 
will conductor-in-chief, and on the occasions of his 
absence his place will be filled by Dr. Richter, Messrs. Henry 
|. Wood, Hausegger, Peter Raabe, and Henri Verbrugghen. 
Only one choral concert will be given, at which will 
be per! ymed Beethoven’s ‘Choral’ symphony and Parry’s 
‘ Blest pair of sirens,’ when the choral portion will be sung 
by Mr. Kirkhope’s choir

The Choral Union will take up the study of Bach’s 
B minor Mass. The University Musical Society has in 
preparation Leo's ‘ Dixit Dominus,’ Mackenzie’s ‘ Procession 
of the Ark’ (‘ Rose of Sharon’), and Stanford’s ‘ Phaudrig 
Crohoore.’ Mr. Moonie’s choir will perform at its first 
concert ‘ The Messiah,’ and at its second concert Max Bruch’s 
‘Fair Ellen,’ Cowen’s ‘John Gilpin,’ and ‘ Killiecrankie’ 
(J. A. Moonie). For his second concert Mr. Kirkhope 
unnounces Mendelssohn's 98th Psalm, ‘ Neenia’ (Brahms




MUSIC


FROM OUR


IN GLASGOW. 


OWN CORRESPONDENT


MUSIC IN GLOUCESTER AND DISTRICT. 


(FROM OUR OWN CORRESPONDENTThe Gloucester Choral Society will open its season’s work 
with Elgar’s ‘ Dream of Gerontius

The Stroud Choral Society has decided to perform Spohr’s 
‘ Last Judgment’ and ‘ Elijah’ during the season, and the 
Cirencester - Society is rehearsing Sullivan’s ‘Golden 
Legend.’ A choral society has been started at Painswick 
this season, and will do Stanford’s ‘ Revenge,’ and the 
Gloucestershire Orchestral Society (Dr. A. Herbert Brewer, 
conductor) has arranged for the following programme : 
Beethoven’s C minor Symphony, Wagner's ‘ Meistersinger ’ 
overture, Dream Pantomime from ‘ Hansel und Gretel’ 
(Humperdinck), and ‘ Scenes for the Ballet’ (W. H. Read

The Cheltenham Philharmonic Society (conductor, Mr. 
C. J. Phillips) announces two concerts, at which will be 
performed Wagner’s ‘ Lohengrin,’ Stanford's Symphony 
No. 6 in E flat (the ‘ Watts’ symphony), and ‘Songs of the 
Sea,’ conducted by the composer, and Parry’s ‘* Pied piper 
of Hamelin




MUSIC IN MANCHESTER. 


(FROM OUR OWN CORRESPONDENTAs is generally the case, local teachers and professors have 
anticipated the season with interesting performances. Miss 
Bertha Guthrie, a contralto singer, gave a recital on October 8, 
when she was assisted by Miss Bessie Blackburn (soprano) 
7 Forbes at 
A selection of vocal duets by Schumann

ing feature of the programme. On October 11 Miss Nora 
Meredith (soprano) gave a vocal recital, Mr. Forbes enriching 
the concert, as well as relieving the programme, with 
pianoforte solos. Here again our rapidly growing musical 
resources were emphasised, in songs composed by Mr. 
Graham Peel and by Mr. F. Nicholls, the latter of whom 
accompanied the rendering of his two Tennysonian lyrics, 
‘ Elaine’s song,’ and ‘ The splendour falls.’ On October 15, 
Mr. James Richardson gave his sixth annual violoncello 
recital, with Mr. Forbes at the pianoforte, in the performance 
of Saint-Saéns’s Suite in D minor (Op. 16), of Beethoven's 
Sonata in G minor (Op. 5, No. 2), and for the first time in 
Manchester of a Sonata in F sharp minor by the Bologna 
composer, Giuseppe Martini

On October 18 the Hallé Orchestra, under the direction of 
Dr. Hans Richter, attracted to the Free Trade Hall an 
audience of, I suppose, 3,000 musical enthusiasts. The 
programme contained Richard Strauss’s ‘Tod und Ver

klarung’; the ‘ Euryanthe’ overture, and Beethoven's

766 THE MUSICAL TIME




S.—NOVEMBER 1, 1906


MUSIC IN NEWCASTLE AND DISTRICT


FROM OUR OWN CORRESPONDENTJarrow Philharmonic Society is busy with Sullivan’s 
‘Martyr of Antioch,’ and the choral society of the 
neighbouring town, South Shields, will perform Dvorak’s 
*Stabat Mater’ and Elgar’s ‘ King Olaf

Two of Mendelssohn’s symphonies, the ‘Scotch’ and | 
the ‘Reformation,’ and Beethoven’s seventh Symphony, | 
Smetana’s overture ‘Die verkaufte Braut,’ in addition to 
other items, are being rehearsed by the Northumberland | 
Orchestral Society. There will be no lack of chamber | 
music this season in Newcastle. Besides the set of concerts

provided by the Chamber Music Society—an old-established 
institution, applications for membership of which are so 
numerous that those who wish to join frequently have to | 
wait some years before gaining admission—the Newcastle




BERLIN


BUDAPEST


COBURG


COLOGNE


LEIPZIG


MANNHEIM


MILAN


767


PALLANZA


PARIS


STUTTGART


WEIMAR


768 THE MUSICAL TIM


ES.—NOVEMBER 1, 1906Buston.—Your Harrison & Co.’s edition of Handel’s 
Coronation and Funeral Anthems is of no particular pecuniary 
value. The date you assign to it, 1730, is much too early, 
as houses in London streets were not numbered until about 
forty years later. According to Mr. Frank Kidson’s ‘ British 
Music Publishers,’ your book was published about 1784, 
or a little later

S. I.—Over forty pieces by Schubert arranged for the 
organ are included in Messrs. Novello’s catalogue. The ‘Quick 
movements from Beethoven’s symphonies ’ arranged for the 
organ, are the A//egrettos from the seventh and eighth 
symphonies (arranged by Best), and the Finale of the 
C minor symphony (arranged by A. B. Plant

A. B. S.—To estimate the financial value of your com- 
positions—‘ 2 Songs (one with violin obbligato), 2 Violin 
solos (both a fair length), two or three Pianoforte solos, and 
a Valse’—is an undertaking from which we naturally shrink. 
We are glad to hear that ‘ a// of them are much admired 
when heard




H. S. B. 


G. B. 


F. E. P


SPECIAL NOTICE


THE MUSICAL TIMES


SCALE OF TERMS FOR ADVERTISEMENTS. 


OTF 


CONTENTS


DURING THE LAST MONTH. 


EC 


GRAH


DURING THE LAST MONTH. 


7CKERSLEY, REV. Galleon.” Song, for

i 
Comments Beethoven’s

z




N OVELLO'S CHRISTMAS CAROLS


432° 


300


S.S.A


EXTREMELY SUITABLE FOR PERFORMANCE AT THE 


SEASON OF CHRISTMAS


THE 


PRINCE OF PEACE 


A SACRED CANTATA 


FOR FOUR SOLO VOICES, CHORUS, AND 


ORCHESTRA 


THE WORDS SELECTED FROM HOLY SCRIPTURE


AND THE MUSIC COMPOSED BY


ALFRED R. GAUL


SOUTH KENSINGTON, LONDON, S.W


| H.R.H. THE PRINCE OF WALES, K.G. 


M.A., M D 


I I ‘ 


MORECAMBE MUSICAL FESTIVAL


THE SCHOOL MUSIC REVIEW


THE SCHOOL MUSIC REVIEW FOR NOVEMBER


CONTAINS


SCH M I N 


THE SCHOOL MUSIC REVIEW


(BASS) 


JOHN SEBASTIAN BACH


7 BENEZER PROUT’S WORKS


AR. ;' < - 1 


FUGUE


FUGAL ANALYSIS


MUSICAL FORM


APPLIED FORMS


MESSRS. 


PUTTICK AND SIMPSON


SPECIAL SALE


OF


VALUABLE VIOLINS


EARLY IN) DECEMBER


INSTRUMENTS FOR THIS SALE SHOULD BE SENT TO THE AUCTIONEERS AS


EARLY AS POSSIBLE


PUTTICK AND SIMPSON, 


47, LEICESTER SQUARE, W.C


TELEPHONE—1561 GERRARD, ESTABLISHED, 1794


THE COUNCIL SCHOOL HYMN BOOK


A COLLECTION OF HYMNS, WITH PRAYERS, 


FOR USE IN COUNCIL SCHOOLS 


COMPILED TO MEET THE PARTICULAR NEEDS OF THE NEW EDUCATION 


AUTHORITIES ESTABLISHED BY THE ACT OF 1902


PRICES. 


PREFATORY NOTE TO THE MUSIC EDITION


DENMAN STREET, PICCADILLY CIRCUS, LONDON, W


AN FOR


ADVENT


LONDON NOVELLO AND COMPANY


I, 1906. 


ADVENT HYMN


IN LOWLY GUISE THY KING APPEARETH.” 


FOR SOPRANO SOLO AND CHORUS, WITH ORCHESTRAI


ACCOMPANIMENT 


COMPOSED BY 


ATE ) HE GE { KE 


BLESSED ARE THEY WHO


WATCH 


\ CANTATA FOR ADVENT 


FOR SOPRANO SOLO AND CHORUS


WITH


HYMNS TO BE SUNG BY THE CONGREGATION


HUGH BLAIR


THE TWO ADVENTS


CHURCH CANTATA


COMPOSED BY


GEORGE GARRETT, M.A., 


BLOW YE THE TRUMPET


ZION


CANTATA FOR ADVEN' 


COMPOSED BY


C. WARWICK JORDAN


STORY OF THEADVE NTOF JESUS 


E, W. LEACHMAN


J. STAINER


CHURCH MUSIC


PHE


MUSICAL


1906


NOVELLO'S CHRISTMAS. CAROLS


I I] 


FIRST 


SECOND


THIKD 


ERIES. ‘ 


SERIES () 


H I


7 5. € ( @) 


I O J 


H G. M,. ¢ 


) I MM 


J. F.1 


( 1 H


' : | }. T.1 


P ( ( A 


| ( S W 


I N 


; WW 


\ ON 


H | S | ) 


W. H. M K \\ H 


I ‘ , M I 


: | N » H ( ( 1) \.H 


. - I \ H 


I I \ I \ 


= \ S ( M 


THE MUSICAI


S.—NOVEMBER 1, 1906


CHRISTMAS CAROLS _ |COMPOSITIONS BY EDWIN A. CRUSH 


BY 


— ORGAN MUSIC. 


N I ( M


BENEL 1) 


BLESS


COMI


NEW NOVELLO'S


P CHRISTMAS ANTHEMS.|CHRISTMAS CANTATAS


SHA


GLORY TO GOD IN THE HIGHEST STORY OF BETHLEHEM 


A SHORT SACRED CANTATA


CLOWES BAYLEY. 


THE WORDS WRITTEN BY 


\ HUGH BLAIR. | COMPOSED BY


AN EASY CHRISTMAS CANTATA 


BLESSE]) BE THE LORI) GOD OF ISRAEL | FOR SOPRANO, TENOR, AND BASS SOLI, CHORUS AND 


ORGAN


REV. E. VINE HALL


THOMAS ADAMS


THERE WERE SHEPHERDS


1 HEALEY WILLAN. 


: CHRISTMAS EVE 


B LUARDSELBY. CHRISTMAS SCENES 


; CANTATA FOR FEMALE VOICES 


' FREDERIC H. COWEN. 


J. FREDERICK BRIDGE. 


‘ ; THE COMING OF THE KING 


CHARLES MACPHERSON, A SACRED CANTATA FOR FEMALE VOICES 


= WORDS BY 


GLORY TO GOD IN THE HIGHEST HELEN MARION BURNSIDI 


E. MARKHAM LEE. MYLES B. FOSTER. 


COME, YE GENTLES, HEAR THE STORY] 


YULE-TIDE


EDWARD C. BAIRSTOW


THE NEW-BORN KING THOMAS ANDERTON


THREE SHORT AND EASY SETTINGS


COMMUNION SERVICE


W. J. CLEMSON


TWO POPULAR ANTHEMS


“TARRY WITH ME, O MY SAVIOUI


S. A. BALDWIN


FROM SINAI'S CREST 


ANTHEM


FOR BARITONE SOLO AND CHORI


FERDINAND DUNKLEY


N \ I H. W. ¢ ( 


ADESTE FIDELES 


DOM SAMUEL G. OULD 


CONTAINING THE COMPLETE LATIN TENT


I I) S ( 


I W 


A PRACTICAL METHOD OF 


TRAINING CHORISTERS


DR. VARLEY ROBERTS


I I


— BACH’S 


CHRISTMAS ORATORIO


IN SIX SECTIONS


YEAR’S DAY


1 I


CAROLAU NADOLIG NOVELLO. 


I S = 


WW K


XUM


CHRY 


CHRIS


LET 


HOSA


NOW 


SONG 


EIGH’ 


METZ 


LIGH


CALD


THE


ALFRI


STAID 


CARM 


PINA| 


THE


ION


XUM


METZLER’S CHRISTMAS MUSIC


MC., XC., AKC


METZLER'S BOUND VOLUMES 


SUITABLE FOR CHRISTMAS PRESENTS AND SCHOOL PRIZES. 


METZLER’S LATEST NOVELTIES. 


SELECTION OF SACRED SONGS 


ARRANGED FOR THE PIANOFORTE BY H. M. HIGGS


NEW COMPOSITIONS FOR THE ORGAN 


NOW READY. 


METZLER & CO., LIMITED, 


THE ORIANA


COLLECTION OF EARLY MADRIGALS 


BRITISH AND FOREIGN


2. WITH ANGEL’S FACE AND BRIGHTNESS... ( 4, )... DANIEL NORCOME 


7. THE NYMPHS AND SHEPHERDS DANCED ( 4,  )... GEORGE MARSON 


33. TRUST NOT TOO MUCH, FAIR YOUTH (5 5) ORLANDO GIBBONS 


36. O FLY NOT, LOVE (5 ,,  )... THOMAS BATESON 


41. THOSE SWEET, DELIGHTFUL LILLIES (5 5  )--» THOMAS BATESON 


PRODUCED


AT


THE


BIRMINGHAM MUSICAL


FESTIVAL


INGDO


ORATORIO


BY


EDWARD ELGAR


AN


COMPOSED


TOBER 1906


1. MORNING LEADER. | 


LGNDON NOVELLO AND COMP ANY, LIMITED


SACRED SYMPHONY, IN F


THE


UP YOUR HEARTS


THREE ELIZABETHAN 


PASTORALS


AN IDYLI 


AMONGST THE WILLOWS


WALFORD DAVIES. THE MORRIS DANCI


N ORCHESTRA


A. HERBERT BREWER


W " I 


1 “ I 


IN¢ ) 


\ } I I 


\LL M I KTTI 


| I DD 


! THE ATHEN.EUM 


THE YORKSHIRE POST 


: PI 


M 1) 


SHEFFIELD DAILY TELEGRAPH. 


HEREFORI) TIMES 


GLOUCESTER JOURNAI 


M D | 


XUM


AL FESTIVAL, 1906


LATEST SUCCESSFUL .SONGS. 


N “OLD SALTS


CHARLES L ISTE R BRADLEY


OLD SALTS


OLD ENGLISH ORGAN MUSIC 


PREFATORY NOTE


AND COMPANY, LIMITED


NOVELLO


LONDON


COMPOSED BY . = " . 


— HYMN TO DIONYSUS 


PE | 2 R - 6) RN IE % IUS THE WORDS FROM THE 


BACCH-Z OF EURIPIDES 


ANSLATION BY 


GILBERT MURRAY 


FOR CHORUS AND ORCHESTRA


WITH ENGLISH WORDS


FLEETING LIF


PILGRIM’S SON‘ ERNEST WALKER 


THE O1.D SOLDIER'S DREAM


THE SANDS OF DEE 


BALLAD 


FOR CHORUS AND ORCHESTRA


MIXED VOICES


LOVE AND YOUTH " CHARLES A. E. HARRISS. 


O DEATH! THOU ART THE TRANQUIL NIGHT


' COMPOSED BY 


PATTISON. EDWIN C. SUCH


ANTATAS


DESCRIP TIVE CHORUSES. 


PRODUCED AT THE HEREFORD MUSICAL FESTIVAL, 19006


THE SOULS RANSOM 


A PSALM OF THE POOR 


(SINFONIA SACRA) 


FOR 


SOPRANO AND BASS SOLI, CHORUS AND ORCHESTRA 


COMPOSED BY 


C. HUBERT H. PARRY. 


PRICE TWO SHILLINGS. 


THE TIMES. : DAILY CHRONICLE


LONDON


ONE


JUST PUBLISHED


ILLUNDRED 


(BIBLE VERSION) 


THE CANTICLES


POINTED FOR CHANTING


EXTRACTS FROM THE PREFACE


PRESS NOTICES


PRI 


NDOD NOVELLO AND COMPANY, LIMITED


PSALMS


V A 


THE MUSICAL


A SUITE 


OLD ENGLISH DANCES 


FREDERIC H. COWEN


», PEASANTS’ DANCE. | 4. OLD DANCE


PIA S 6 


S P 


THE TIMES 


DAILY TELEGRAPH


MORNING POST 


EVENING STANDARD. 


; DAILY NEWS. 


SUNDAY TIMES. 


WESTERN DAILY PRESS


SCOTSMAN, 


GLASGOW HERALD 


ASGOW NEWS


GI 


GLASGOW EVENING TIMES


NDON


TIMES


PRELUDE.—(“I 


. ANDANTINO


CORONATION MARCH 


NOVEMBER


NEW- ORGAN MUSIC


I, 1900


PASTORALI G I 


FESTAL


PROCESSIONAI


POSTLUDI


MAKCHI I 


ALLEGRETTO PASTORAL : I 


MARCHE CHROMATIOUI ‘ I 


IRIGINAL COMPOSITIONS


HENRY HACKETT


ORGAN 


TRANSCRIPTIONS 


J. BENNETT


ANY


GEORGE 


HENGRIN ") WAGNER 1 


PSCHAIKOWSKY 2 


SYMPHONIE ( G MINOR, AND


MOZART 2


MINORE FLAT

Up. T, ti.) 
BEETHOVEN 1 
MACKENZIE 1 
Bue PAIR 
PARRY 1

OF




SIRENS C. H. H


MENDELSSOHN 1


HARTEL, 


H STREET, W


BREITKOPF AND 


GREAT MARLBOROU


CLARISSE MALLARD’S 


SONG-CYCLE FOR THE 


CHILDREN 


SUITABLI ENTERTAINMENTS, I


FOR SCHOO


ORIGINAL COMPOSITIONS


NEW MUSIC TE 


PUBLISHED AND IMPORTED BY 


BREITKOPF & HARTEL — 


PIANOFORTE SOLOS ORCHESTRAL MUSIK athoms below

topmost branches. 
WIND INSTRUMENTS. 
Solo Book for Horn, taining pieces by Beethoven, 
born, Goltermann, Haydn, Hofmann, ozvart, Reine« 
hubert, Schumann, Sinigaglia, Volbact

PES. 
No




PIANOFOR


ORGAN. 


M. E. I 


VOCAL SCORES 


ORCHESTRAL MUSIC


LL SCORES) 7


BREITKOPF & HARTEL, 


54, GREAT MARLBOROUGH


ST., REGENT 


ST... 


MARSHALI


LONDON, W. 


HAMILTON, KEN1


THE


EXTRA SUPPLEMENT. 


NOVELLO’S OCTAVO ANTHEMS


LORD OUR RIGHTEOUSNESS


ANTHEM FOR CHRISTMAS


COMPOSED BY


HUGH BLAIR


“3 THE PROPHECY. 


F =_— . 


THE LORD OUR RIGHTEOUSNESS


XUM


= A | | 5 


THE SHEPHERDS IN THE FIELDS. 7 


THE LORD OUR RIGHTEOUSNESS


XUM


> 2S 


THE LORD OUR RIGHTEOUSNES


GL


217


THE LORD OUR


RIGHTEOUSNESS


SS SS 


20-4 8 } — ——— = | 


- PPP. —— — —— — 


5 5-4. >) “ : } 


PP } 1 ! | | ' , 


2 — 2 — 


SS TS SS A ESOT TE NAY SSN OSE = 2 SF OE 


| P } | 1 


— OH 


SS


THE LORD OUR RIGHTEOUSNESS


= _ —_ - . —— 


_—— _— | , | | 


CONGREGATION WITH CHOIR


THE LORD OUR RIGHTEOUSNESS


7 -_— — 


THE LORD OUR RIGHTEOUSNESS


== SS == = 


- - —__ __ @ —__@ @ : 


. =| 4 _ 4 = - 


TH 


QI. 


93 


CHRISTMAS CAROL


ENG


P N 


J <= SN) 


, : 0.8 


E:4 — _—-9- »—_+—_ 


SS


NY 


IN BETHLEHEM, THAT NOBLE PLACE


XS


JA


NOVELLO'’S CHRISTMAS CAROLS


215. 


216


236


44 


24 


246 


245 


249


254


262


264


05 


266 


267 


268


270 


271 


272


277 


279


XUM


XUM


TH 


PRINCI 


FOU! 


ORCHI


0 76


HAN


LEN


SOHO


STREET