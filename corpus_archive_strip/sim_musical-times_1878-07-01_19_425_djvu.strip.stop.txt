


 | MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 PUBLISHED MONTH 


 CHALLEN SON 


 GOLD MEDAL PIANOS 


 20 , OXFORD STREET , LONDON 


 ESTABLISHED 1804 


 PRIZE MEDAL , LONDON , 1862 . | | GOLD MEDAL , SOUTH AFRICA , 1877 


 CHALLEN & SON GOLD MEDAL PIANETTE , 


 CHECK ACTION , ELEGANT WALNUT ROSEWOOD CASE . 


 25 GUINEAS 


 CHALLEN & SON GOLD MEDAL SEMI - COTTAGE , 


 CHECK REPEATER ACTION . 


 82 GUINEAS 


 CHALLEN & SON GOLD MEDAL COTTAGE ; 


 TRICHORD ; BRASS BRIDGE ; CHECK REPEATER ACTION . 


 60 GUINEAS 


 EXCELLENCE GUARANTEED , INSTRUMENT WARRANTED 


 HIGHEST CLASS WORKMANSHIP MATERIALS 


 EBONISED GOLD PIANOS 


 CHALLEN & SON , 20 , OXFORD STREET . 


 STEAM WORKS — CARDINGTON STREET , EUSTON SQUARE 


 PROFESSIONAL NOTICES . 


 363 


 USIC ENGRAVED , PRINTED , PUB 


 NEW MUSIC PRINTING PROCESS 


 SUPERIOR PEWTER PLATES METAL TYPES . 


 CENT . CHEAPER . 


 PATENT PAPER TYPE COMPANY , 


 12 , BERNERS STREET , LONDON , W 


 CONCERT - GIVERS ENTREPRENEURS 


 GENERALLY 


 CLERGY . 


 CHOIR - BOYS 


 ORCHESTRAS CHOIRS 


 ESTABLISHED , APRIL , 1866 


 ENGLISH GLEE UNION . 


 ASSISTED 


 OCARINA 


 PRICES , INCLUDING BOOK INSTRUCTIONS : — 


 BARNETT SAMUEL SON 


 32 , WORSHIP STREET , FINSBURY SQUARE 


 “ STANDARD ” 


 AMERICAN ORGANS 


 MANUFACTURED 


 PELOUBET , PELTON , CO . , 


 NEW YORK 


 PRICES , GUINEAS UPWARDS 


 32 , WORSHIP STREET , 


 FINSBURY SQUARE 


 EAN CHEAP MUSICAL INSTRUMENTS . — 


 RUSSELL MUSICAL INSTRUMENTS . : 


 WARD 


 ROYAL PIANOFORTE 


 HARMONIUM LAMP 


 PROVIDING CENTRAL & DIRECT LIGHT 


 PERFORMER 


 PROSPECTUSES POST - FREE . 


 LONDON AGENTS 


 MESSRS . MOUTRIE SON , 


 PIANOFORTE SALOON 


 55 , BAKER STREET , LONDON , W. 


 TESTIMONIAL . 


 MILLEREAU BAND INSTRUMENTS 


 WOOD , METAL , VULCANITE 


 O VIOLINISTS.—COOKE NEW PATENT 


 99 — - GRAND PIANOFORTES , 


 PUBLISHED 


 MOZART SONATAS 


 NEW COMPLETE EDITION 


 EDITED FINGERED 


 AGNES ZIMMERMANN 


 NEW MARCH GOUNOD 


 _ — _ 


 MARCHE SOLENNELLE 


 


 CH . GOUNOD 


 TE DEUM LAUDAMUS 


 E FLAT , 


 


 HENRY GADSBY . 


 MAGNIFICAT & NUNC DIMITTIS 


 C 


 


 EATON FANING . 


 COMPOSED FESTIVAL SONS 


 CLERGY ST . PAUL CATHEDRAL , 1878 . 


 MAGNIFICAT & NUNC DIMITTIS 


 GEORGE C. MARTIN . 


 READY 


 LIFE MOZART 


 INCLUDING CORRESPONDENCE , 


 


 EDWARD HOLMES . 


 CHERUBINI 


 MASS 


 CORONATION MASS 


 NEW NUMBERS 


 NOVELLO - SONG BOOK 


 ROBERT FRANZ 


 FRANZ ABT . 


 A. C. MACKENZIE . 


 E. PROUT . 


 J. L. HATTON . 


 PUBLISHED 


 PIANOFORTE COMPOSITIONS 


 


 LEFEBURE - WELY 


 HARVEST ANTHEMS 


 MART , HENRY.—THE LORD HATH GREAT 


 ALLCOTT , W. H.—THOU VISITEST EARTH 


 ACFARREN , G. A.—GOD SAID , BEHOLD , GIVEN 


 OPKINS . E. J — THANKS UNTO THEE , 


 ATTISON , T. M — O PLENTIFUL THY GOOD- 


 AYLOR , W.—THE EYES WAIT THEE . 


 EETON , HAYDN.—THE EYES WAIT 


 LONDON : NOVELLO , EWER CO 


 PUBLISHED 


 MEYERBEER 


 LETOILE DU NORD 


 EDITED , PIANOFORTE ACCOMPANIMENT 


 REVISED , 


 BERTHOLD TOURS 


 ENGLISH VERSION 


 HENRY F. CHORLEY 


 LONDON : NOVELLO , EWER CO 


 HYMNS TUNES 


 


 HARVEST 


 HYMNARY 


 PRICE PENNY 


 O LORD HEAVEN , EARTH , 5 


 LONDON : NOVELLO , EWER CO . 


 ALBERT LOWE 


 NEW HARVEST ANTHEM , 


 “ EARTH LORD 


 ALBERT LOWE 


 HARVEST CAROL , 


 “ HOLY SEED - TIME 


 J. S. BACH 


 - PRELUDES & FUGUES 


 MAJOR MINOR KEYS 


 W. T. BEST 


 HOPE 


 CHORUSES WALTZ FORM MALE VOICES , 


 PIANOFORTE ACCOMPANIMENT 


 ST . JAMES HALL , 


 WEDNESDAY , JULY 10 , 8.30 P.M 


 VERDI REQUIEM 


 CLUB SOHOall 

 R. WILLEM COENEN MATINEE 
 MUSICALE , 3 , Soho Square ( kind permission Messrs. 
 Kirkman Son ) , SaTuRDAY , June 29 , o’clock . PRO- 
 RAMME.—Sonata , Op . 53 , C , dedicated Count Waldstein 
 { Beethoven ) , Mr. W. Coenen ; Song , “ old Chelsea Pensioner 24 
 ( Molloy ) , Mr. Bernard Lane ; Solo , violin , “ Ballade et Polonaise de 
 Concert ” ( Vieuxtemps ) , Mons . V. Buziau ; Song , ‘ ‘ Treue Liebe ” 
 ( True Love ) , ( Coenen ) , Miss Héléne Arnim ; Solo , pianoforte , ‘ ‘ Rhap- 
 sodie Hongroise , ” . 2 ( Liszt ) , Mr. W. Coenen ; Sonata , G , Op . 13 , 
 pianoforte violin ( Rubinstein ) , Messrs. W. Coenen V. 
 Buziau ; Song , “ Yes ” ( Coenen ) , Mr. Bernard Lane . Mr. Coenen 
 play couple solos melo - pianoforte , recently invented 
 Messrs. Kirkman Son . Song , “ 1 come , love , wel- 
 come ” ( Macfarren ) , Miss Arnim ; Solos , a. ‘ Twilight ” ( Coenen ) , 
 b. “ Caprice ” ( Miiller ) , c. “ Etude ” ( Rubinstein ) , Mr. W. Coenen . 
 Conductor , Mr. J. B. Zerbini . Tickets , Half - - guinea , Chap- 
 pell Co. , 50 , New Bond Street , Novello , Ewer Co. , 
 x , Berners Street 

 TRINITY COLLEGE , LONDON 




 INCORPORATED SPECIAL CHARTER 


 SPECIAL NOTICE COUNTRY PROFESSORS 


 COLLEGE ORGANISTS . 


 PUBLISHED 


 HEZEKIAH 


 SACRED CANTATA 


 COMPOSED 


 LONDON : NOVELLO , EWER CO 


 MUSICAL TIMES 


 SCALE TERMS ADVERTISEMENTS 


 NEW CAREFULLY REVISED EDITIONS 


 


 MENDELSSOHN 


 CHEAP EDITION 


 COMPLETE EDITION 


 LONDON : NOVELLO , EWER CO 


 JONES & WILLIS 


 CHURCH FURNITURE 


 MANUFACTURERS , 


 43 , GREAT RUSSELL STREET 


 LONDON , W.C 


 260 , EUSTON ROAD 


 


 TEMPLE ROW , BIRMINGHAM 


 ORGANISTS CHOIRMASTERS 


 CHOIR CASSOCKS SURPLICES 


 LARGE SELECTION STOCK . 


 CASSOCKS SURPLICES HIRE CHOIR 


 ATTENDANCE CHOIR PRACTICES MEASURE- 


 MENTS TAKEN , DESIRED 


 369 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 JULY 1 , 1878 


 MUSIC PARIS . 


 ODReaders MusicaL TimEs aware 
 arrangement International exhibition 
 music secured time 
 history ‘ ‘ World Fair . ” know 
 countries Europe invited 
 send compositions , 
 performers , Paris , liberal conditions 
 offered induced accept act 
 idea . time England , represented execu- 
 tive sense orchestra Royal Italian 
 Opera Mr. Henry Leslie Choir , appear 
 scene . Italy turn ; 
 fortune present initial per- 
 formance orchestra La Scala , Milan , con- 
 ducted Signor Franco Faccio . honour 
 public spirit sent musicians Alps 
 assembled Europe erewhile ‘ land 
 song ” achieve highest regions 
 art . good practice music 
 far disassociated purely mercenary considera- 
 tions , connected honourable emulation . 
 Italy , let , reason ashamed 
 champions . orchestra appeared 
 platform 115 strong ; , 90 strings , 24 
 wind instruments , harp . Signor Taccio 
 divided army strings : — 24 violins , 20 
 second violins , 16 violas , 16 violoncellos , 14 
 double - basses . readily supposed , effect 
 produced hall resonant great , 
 French connoisseurs readily admitted Signor 
 Faccio right pride power 
 “ quartett . ” strikes , respect 
 , open eyes ears 
 wide “ strings ” English 
 orchestra play building . instru- 
 ments Italians little , , better 
 quality generally heard French 
 orchestras , quality number 
 tells . Accustomed magnificent tone 
 “ strings ” hands English players , 
 confess having impressed 
 Italian “ ‘ quartett ” apparently mass 
 audience . , wind instru- 
 ments - ninety , domi- 
 nated unusual degree ; result partly 
 acoustics building ; wood 

 wind placed , usual , 
 brass . playing orchestra remark- 
 able characteristics , prominent feature 
 immense superiority ‘ ‘ strings ” 
 ‘ ‘ wind . ” confessed “ strings , ” 
 matter execution , splendid . fire 
 immense conductor let loose , 
 French amateurs , strangers im- 
 pulsiveness , declared Italian impetuosity 
 somewhat great fortheir taste . critic called 
 Signor Faccio men “ Zouaves symphony , ” 
 avowed , need , improvise 
 music forgotten place desks . 
 “ ‘ ” Milanese curious 
 effect , manner 
 results presented features great attraction . 
 interesting compare well- 
 known works , like Beethoven Overture ‘ Coriolan , ” 
 rendered Italians , master- 
 piece given English orchestra . ftéxt 
 , general character , 
 respects widest possible divergence 
 challenged curious notice . , , 
 music matter temperament widely 
 different interpretation legitimately 
 different people . feature remains 
 notice , excessive attention 
 paid nuances . Refinement , question , 
 quality orchestra seek ac- 
 quire thing absolutely essential , 
 sought great earnestness , 
 general detriment . examines picture 
 magnifying glass necessarily lose 
 tout ensemble attention details , 
 precisely case Italian orchestra . 
 conductor whele mind , followers , 
 engrossed minutiz work , 
 consequence exaggeration minutie 
 expense breadth effect . ‘ ‘ Blue pencil ” 
 , doubtless , useful article hands con- 
 ductor , easily abused — fact 
 need hardly Paris proof — , , 
 music simply question varying dynamics 
 shades tempo . Italian programme con- 
 tained , expected , large proportion 
 Italian music , wholly . 
 Signor Faccio thought explain stating 
 wished cosmopolitan public Paris 
 hear orchestra classical piece chosen 
 reference school , era , nationality . 
 overture ‘ ‘ Coriolan ” place 
 scheme . desired mark 
 homage French hosts , performance 
 Berlioz “ Carnaval Romain ” accordingly given , 
 evident satisfaction audience . 
 Italian music began overture Foroni , 
 , having particular distinctiveness style , 
 fell somewhat flat . think 
 badly chosen representative national art , 
 character approximated 
 German Italian school . short 
 movements , ‘ “ ‘ Contemplazione ” ‘ ‘ Scherzo , ” 
 Signor Catalani , followed producing 
 effect ; , contra , Overture “ Il Guarany , ” 
 know England , aroused like 
 enthusiasm , played wonderful verve . 
 pretty little Gavotte , courtly elegant 
 old dance , Bazzini , met better 
 reception . Arranged strings , rendered 
 delicately , gained easy encore . 
 came Overture Ponchielli “ Promessi Sposi , ” 
 Funeral March , ‘ ‘ Amleto , ” con- 
 ductor pen . greatest effect 
 — result manner 

 resources orchestra employed 




 373 


 LITERATURE NATIONAL MUSIC . assistance . 
 perusal following survey , case 
 previously given particular attention National 
 Music science . , reason sur- 
 mise subject new musicians ; 
 events , present essay claim 
 ‘ * * supply long - felt want literature . ” , who- 
 obtained insight rich treasures 
 popular songs tunes , hitherto 
 little explored , probably convinced 
 study National Music sure 
 gradually appreciated earnest promoters 
 art 

 regards term National Music , 
 remembered , taken widest sense , desig- 
 nates music , composed 
 peculiar taste nation appertains , 
 appeals powerfully music 
 feelings nation , consequently pre- 
 eminently cultivated certain country . 
 sense Haydn , Mozart , Beethoven re- 
 garded representatives German National Music ; 
 Rossini , Bellini , Donizetti Italian National 
 Music ; Auber , Boieldieu , Hérold French 
 National Music . , distinguished composers 
 developed style great measure 
 studying works previous masters different 
 countries . peculiar characteristics music 
 nation strongly exhibited 
 popular songs dance - tunes traditionally 
 preserved country - people lower classes . 
 society , form great majority nation . 
 musical conceptions , generally simple 
 unpretending construction , retain popu- 
 larity long period , views senti- 
 ments uneducated simple - minded man 
 subjected external influences 
 educated ambitious man . Thus- per- 
 haps explained fact find 
 rural population countries tunes sung 
 known century old . True , 
 somewhat altered course 
 time . surprising alteration 
 great , considering preserved 
 traditionally mouth mouth , 
 country - people 

 kind music , National 
 Music strict sense term , 
 elaborate productions distinguished com- 
 posers , reader invited occupy 
 attention . , asked , 
 advantage carefully investigating inartistic 
 musical effusions ? reply : study 
 National Music useful account great 
 originality popular tunes . Professional musicians 
 inducements compose perform music 
 feel , untaught peasant 
 sing heart emotions impel . 
 wonder musical effusions , artless 
 , truer expression feelings 
 - constructed productions clever artists 




 NATIONAL SONGS 


 376The Wends Slavonic race living 
 villages Lusatia , Germany . comprehensive 
 collection popular songs , edited Haupt 
 Schmaler ( Grimfna , 1841 , 4to , vols . ) , contains 
 530 songs thetunes . words Wendish , 
 German translation ; interesting work 
 illustrated coloured plates representing Wen- 
 dish men women picturesque costumes , 
 objects illustrative manners 
 customs 

 curious instance , showing distin- 
 guished musician deceived hunting 
 national tunes subject 
 study , occurs “ Presni Polskie Ruskie Ludu 
 Galicyjskiego , ” collection popular songs 
 Polish Russian people Galicia , published 
 Venceslas Zaleski ( Lemberg , 1833 , 8vo , vols . ) 
 second volume contains 160 airs , pianoforte 
 accompaniments , Charles Lipinski , celebrated 
 violinist . native Galicia ; 
 admits publication , . 80 set 
 tunes , air construction expression 
 different native country 
 surmised Galician , 
 aware melody 
 ‘ * Nel cor pit non mi sento , ” Opera ‘ ‘ La 
 Molinara , ” Paisiello , Beethoven 
 composed beautiful variations , known 
 England air ‘ ‘ Hope told flatt’ring tale . ” 
 Galician popular songs collected 
 published Zegota Pauli ( Lemberg , 1838 , 1839 

 Czechs Slavonic inhabitants 
 Bohemia . valuable collection songs , 
 edited K. J. Erben ( Prague , 1862 1864 , 8vo , 
 vols . ) , contains 811 tunes original words . 
 earlier publication , Erben Martinowsky 
 ( Prague , 1847 , 4to ) , contains 300 songs piano- 
 forte accompaniments . Particularly interesting 
 incorporated old songs Hussites , 
 friend Erben committed notation 
 lips old minstrel , district Budweis 
 Bohemia . Hussite tunes 
 minor key , wild sad . note- 
 worthy publications Bohemian National Music 
 Rittersberg Weber ( Prague ) ; Frantisek 
 Martinec ( Prague , 1856 , oblong 8vo ) ; J. Vashak 




 LONDON UNIVERSITY MUSICAL DEGREES . 


 378acquired handle freedom 
 mastery logical forms composition ; 
 exceptional cases , Mozart , 
 power come instinct . 
 small London University programme , 
 credited scheme 
 arming professor music points 
 thoroughness hardly 
 proposed hitherto country . com- 
 pleteness curriculum , ‘ preventive 

 measure , ” little question . 
 great composers taken degrees 
 . ‘ Bach , strong logical brain , 
 power steady application , 
 likely . 
 imagine Handel dismissing , ‘ Te tevil ! 
 vot vor sall tell de vhenomena zound , 
 hydrosdadigs hydrauligs — dom degree ! ” 
 Mozart laughing , jaunty little note 
 trés cher correspondents ; Beethoven w rapping 
 bread cheese ; Chopin dreamingly musing 
 “ principles melodial progression ” “ 
 phenomenaattending combinations twosounds . ” 
 Mendelssohn chance making 
 , got degree 
 given mind ; certainly 
 ; fond society 

 , course , said object 
 Mus . Doc . curriculum great com- 
 posers , point distinguishing honour 
 certain men taken trouble learn 
 learned history science 
 music , receive kind imprimatur re- 
 cognised authorities , , dis- 
 tinguished composer , information 
 knotty point needs science 
 study genius unravel . , , 
 way musical degrees hitherto 
 regarded . course , leave 
 question sham degrees merely nominal 
 knowledge rightly falling 
 utter disrepute ; existing recognised musical 
 degrees include mainly necessary good 
 standard professional knowledge musician , 
 chiefly based evidence sound 
 attainment counterpoint elabo- 
 rate forms composition ; , course , 
 known sought clever 
 young musicians initial advantage 
 means connection , want affix 
 help making 
 start professional life . reason 
 degree sought object ; 
 recognised London University 
 degree , proposed , necessarily stand 
 different footing . “ pay ” man 
 curriculum obtain degrees 
 merely means getting forward . 
 obstacle merely large 
 scientific knowledge regard physical basis 
 music demanded , , 
 said , practical value ordinary 
 pursuit musical profession , fact 
 , musical degree attacked 
 , candidate passed general 
 matriculation examination , requires 
 competent knowledge following 
 subjects:—1 . Latin ; 2 , list 
 languages ( Greek , French , German 




 GOD SAVE KING . ’ 


 381 


 OPERA ORATORIO 


 383 


 384 


 MAJESTY THEATRE 


 OH 


 SS 


 = — — _ — _ 


 7 ) = 1 SS & = 


 N XS 


 _ M P 


 T T 


 CHORAL SOCIETIES 


 ORCHESTRAL PARTS 


 SIXPENCE SHEET , 


 NOVELLO OCTAVO EDITION 


 


 PAPER BOARDS . 


 6| HAYDN CREATION 


 ISRAEL EGYPT 6|ROSSINI STABAT MATER 


 MENDELSSOHN ELIJAH _ _ ... 6 GOUNOD MESSE SOLENNELLE 


 CHORUSES 


 


 ORATORIOS , CANTATAS , & C 


 CHORAL SOCIETIES 


 DAAAAS 


 STERNDALE BENNETT QUEEN , 


 ROSSINI STABAT MATER 


 LONDON : NOVELLO , EWER & CO 


 393 


 ROYAL ITALIAN OPERAPHILHARMONIC SOCIETY 

 instrumental soloists seventh Concert , 
 12th ult . , Mr. Alfred Jaell ( pianoforte ) M. 
 Wieniawski ( violin ) , giving skilful rendering 
 Beethoven Concerto E flat , perform- 
 ing Concerto composition abounding 
 difficulties , surmounted ease 
 elicited warmest applause . musical point 
 view ‘ ‘ Romance ” best movements ; 
 ‘ ‘ Finale ” effectively written prin- 
 cipal instrument orchestra . Miss Emma C. 
 Thursby , created favourable impression 
 concert , proved claim rank 
 foremost vocalists , singing Mozart Scena 
 Aria , ‘ ‘ Ma , che vi fece , o stelle , ” ‘ ‘ Sperai vicino 
 il lido , ” remarkable , certainty execution , 
 truthful expression augurs 
 future career . orchestral works Mr. Cusins 
 clever Overture , ‘ * Les Travailleurs de la Mer , ’ Haydn 
 Symphony D ( . 7 Saloman set ) , Mendels- 
 sohn Music ‘ * Midsummer Night Dream , ’’ 
 effect course lessened omission 
 vocal portion . Mr. W. G. Cusins conducted , usual 

 ROYAL ACADEMY MUSIC 

 Tue orchestral concert students institution 
 given St. James Hall roth ult . , included 
 compositions decided interest , pupils — Concert 
 Overture F minor , Henry Lohr , containing 
 clear - defined writing ; Psalm soprano solo 
 chorus , A. G. Thomas , remarkable specimen 
 result sound healthy system training 
 highest school sacred art , reflecting utmost credit 
 young composer instructor ; 
 movements Symphony D minor , Miss 
 Oliveria Prescott , earnest ener- 
 getic student rapidly adding reputation 
 acquired public performances 
 Academy ; movement Symphony C 
 Tobias Matthay , reception warm 
 merited . Special praise awarded Miss 
 Nancy Evans excellent artistic performance 
 movement Beethoven Pianoforte Concerto 
 G ; ; pianists — Miss Margaret Bucknall , Weber 
 Concertstiick , Miss Shapley , Mendelssohn Rondo 
 Brillante B minor — evincing talent . 
 vocalists Miss Marian Williams distinguished 
 careful sympathetic singing solo portion 
 Mr. Thomas Psalm , Mrs. Irene Ware gave sacred 
 song Randegger good expression , finale 
 act ‘ * Don Giovanni ’ ? rendered 

 Misses Ada Patterson , Hallowell , Clara Samuell ; 
 Messrs. Seligman , Hutchinson , Brereton , Jarratt . Mr , 
 Walter Macfarren , usual , highly efficient Con- 
 ductor 




 MR . OLIVER KING CONCERTMADAME VIARD - LOUIS CONCERT . 7 

 fourth series Concerts given St. 
 James Hall afternoon 28 , orchestral 
 pieces , usual , attractive portions 
 programme . selection , including excellent 
 compositions , scarcely satisfactory 
 preceding Concerts ; Beethoven ‘ ‘ Choral 
 Symphony , ” choral portion omitted , somewhat 
 disappointing musical listeners ; , save 
 abstract beauty , Serenade Spohr Symphony , 
 ‘ ‘ Power Sound ” ( mere fragment great 
 work ) , hardly welcomed concert classi- 
 cal pretensions , Madame Viard - Louis pianoforte per- 
 formances ambitious intention , consisting 
 Weber ‘ ‘ Concertstiick ” Mendelssohn Capriccio 
 E major ; know seeks impartial 
 criticism , prefer record comment 
 share programme . Max Bruch Violin Concerto 
 ( Op . 26 ) played Herr Kummer , Romance 
 Horn effectively rendered M. Stennebruggen , 
 deservedly favourable reception accorded 
 clever Gavotte strings Conductor Concert , 
 Mr. Weist Hill . vocalists Mdlle . Christiani 
 Signor Foli 

 DR . HANS VON BULOW RECITALS 

 Recitals given St. James Hall 
 past month eminent pianist 
 principal attractions season ; - , 
 like , exception readings 
 classical works , admit possession 
 marvellous artistic powers . best 
 performances mentioned selections Bach , 
 especially Liszt transcription Organ Prelude 
 Fugue B minor , played 
 clearness intelligence sufficiently proved 
 reverence works great master ; 
 shake eccentricities style 
 evidenced calm unpretentious rendering 
 Schubert melodious Impromptu G ( . 3 , Op . ) . 
 conception works Beethoven remarkable 
 originality ; performance composer Sonatas 
 decisive exhibition 
 devout admirers term “ individuality ” displayed 
 pieces selected pro- 
 gramme . Sonata C minor ( Op . 3 ) , second 
 Recital , cited prominent illustration 
 truth remarks ; bound record 
 overwhelming applause greeted 
 tend confirm lessen confidence 
 believe necessity artist 
 , like Dr. Von Bilow , continues reproduce 
 memory moment public audience 
 multiplicity works infinite variety styles 

 MARIO BENEFIT CONCERT 




 MILITARY SERVICE ST . PAUL 


 CATHEDRAL 


 REVIEWS 


 398 


 FOREIGN NOTESThe publishing firm Schott , Bruxelles , 
 issue interesting work volumes , entitled , 
 “ Histoire du Théatre Frangais en Belgique depuis son 
 origine jusqu’a nos jours ” pen M. Frédéric 
 Faber 

 Royal Opera House Berlin closed 
 usual summer vacation r4th ult . performance 
 “ Fidelio . ” According statistical notice contained 
 Neue Berliner Musikzeitung , 223 performances 
 opera taken place institution 
 months 24th August , including dramas 
 incidental music , Weber * Preciosa , ” & c. 
 numerical order , Richard Wagner ranks 
 thirty - representations works ; comes 
 Mozart , - representations works ; 
 follow names Verdi , Meyerbeer , Briill , 
 nineteen , eighteen , seventeen performances 
 respectively . Weber operas heard 
 occasions ; Beethoven operatic work seven ; 
 Gluck appears list times ; 
 Cherubini twice . novelties produced 
 period named , viz . , Ignaz Brill ‘ * ‘ Der Land- 
 friede ” Richard Wiierst ‘ ‘ Die Offiziere der Kaiserin 

 performance Munich month Herr 
 Wagner “ Siegfried ’’ said highly success- 
 ful . Herr Vogel , representative athletic hero , 
 Madame Vogel , character Brinnhilde , 
 specially mentioned having acquitted 
 exceptionally difficult task great ability . 
 theatre Bavarian capital annual private 
 performances , singular - minded king forms 
 sole audience , recommenced . occasions 
 house brilliantly illuminated , gala - night , 
 directly Majesty enters royal box , per- 
 formance tragedy , comedy , opera commences , 
 servant admitted disturb 
 solitude royal listener 

 institution . facts speak , 
 peculiar privilege prejudice listen 
 teaching 

 recent number Allgemeine Deutsche Musik 
 Zeitung contains interesting article “ History 
 Baton de Mésure , ” writer traces 
 introduction chief music - cultivating countries 
 comparatively modern insigne orchestral leader . 
 notice . 23 Neue Zeitschrift fiir Musik , 
 heading “ Zur Beethoven Literatur , ” Herr 
 Nohl temperate reply Mr. Thayer recently pub- 
 lished critical observations respecting phases 
 life Beethoven , represented Herr Nohl biography 
 great composer 

 According announcement Leipzig 
 Signale , projected Mozart Festival Salzburg 
 held year 

 record death Franz von Holstein , 
 successful composer operas , artist univer- 
 sally esteemed Germany . died age fifty- 
 

 years chief librarian musical section 
 Royal Library , indebted knowledge 
 zeal numerous valuable additions . 
 lent valuable aid standard editions works 
 Bach , Mozart , Beethoven , published Leipzig . 
 years age 

 subjoin , usual , programmes Concerts 
 recently given leading institutions abroad 

 Exhibition Concert Chamber Music ( June 14 ): Quartett 
 E flat ( Cherubini ) ; Trio pianoforte , violin , 
 violoncello ( Widor ) ; Quartett . 5 , stringed instru- 
 ments ( A. Morel ) . Second Concert Officiel ( June 18 ): 
 Overture “ Le Roi d’Ys ” ( Lalo ) ; Fragments 
 ‘ “ ‘ L’Arlesienne ” ( Bizet ) ; Symphony C ( Gouvy ) ; 
 Idylle et Danse des Satyres ( Destribaud ) ; Frag- 
 ments ‘ Eucharis ” ’ ( Deldevez ) ; Overture 
 “ Zampa ” ( Hérold ) . Concert Orchestra 

 La Scala ( June 19 ): Symphony C ( Foroni ) ; ‘ ‘ Con- | 
 templation ” Scherzo ( Catalini ) ; Overture ) 
 “ Guarany ” ( Gomes ) ; Gavotte stringed instruments | 
 ( Bazzini ) ; Overture ‘ ‘ Promessi Sposi ” ( Ponchielli ) ; | 
 Overture , ‘ ‘ Coriolan ” ( Beethoven ) ; Funeral March | 
 “ Amleto ” * ( Faccio ) ; Overture ‘ ‘ Vespri Siciliani sf 
 ( Verdi ) ; Overture ‘ Carnaval Romain ” ( Berlioz ) ; | 
 Overture “ Siege de Corinth ” ( Rossini ) . Exhibi- | 
 tion Concert Chamber Music ( June 21 ): Quartett 
 strings , Op . 56 ( Gouvy ) ; Suite flute piano- | 
 forte ( Madame de Grandval ) ; Trio , Op . 17 ( A. de Cas- | 
 tillon ) . 
 Leipzig.—Concert Riedel’scher Verein ( June 2 ): | 
 Praludium B minor ( Bach ) ; ‘ ‘ Stabat Mater ” ( Pales- | 
 trina ) ; Air violin ( Goldmark ) ; 137th Psalm ( Liszt ) ; | 
 117th Psalm ( R. Franz ) ; & c. Conservatorium ( June 6 ): | 
 Quartett F ( Schumann ) ; Sonata G violin ( Beet- 
 hoven ) ; Sonata pianoforte ( Schumann ) ; vocal soli . 
 Berlin . — Concert Sternsche Gesangverein | 
 ( Thanksgiving preservation life ) 
 Emperor , 26 ): March Chorus “ Ruins | 
 Athens ” ( Beethoven ) ; Duet , ‘ ‘ Lord Man 
 War ” ’ ( Handel ) ; Dettingen Te Deum ( Handel 

 CORRESPONDENCE 




 TENOR CLEF . 


 EDITOR * ‘ MUSICAL TIMES 


 EDITOR ‘ * MUSICAL TIMES 


 GOD SAVE KING . ” 


 EDITOR ‘ * MUSICAL TIMES 


 402 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWS 


 OBITUARY . 


 404 


 MONTH . 


 OVELLO TONIC SOL - FA SERIES . 


 OVELLO - SONG- BOOK . NEW 


 MADAME CROSS LAVERS , 


 MR . HENRY CROSS , 


 AAPWDHH 


 COMPOSERS . 


 MUSIC PRINTED PUBLISHED 


 BEST STYLE 


 ESTIMATES FREE RECEIPT MSS . 


 SIMPSON & CO . , 33 , ARGYLL STREET , LONDON , W 


 SECOND EDITION 


 DICTIONARY 


 MUSICAL TERMS 


 EDITED 


 


 LONDON : NOVELLO , EWER CO 


 NOVELLO OCTAVO EDITION 


 GENERAL HISTORY 


 


 SCIENCE & PRACTICE MUSIC 


 SIR JOHN HAWKINS 


 EDOUARD BATISTE 


 COMPOSITIONS 


 


 ORGAN 


 ORGANIST TOWN HALL , LEEDSNOW READY 

 s. d. 
 . 34 . OFFERTOIRE B minor 3 0 
 » 35 . OFFERTOIRE flat 3 0 
 » 36 . OFFERTOIRE E minor . 3 0 
 » 377 GRAND OFFERTOIRE “ Theme 
 ‘ “ ‘ Kreutzer Sonata ” Beethoven ... 2 6 
 » 38 : HERO ( “ Judas 9 Miles 
 bus ” ) 2 6 
 » 39 . OFFERTOIRE G .. Ve — és utt ecg 

 Catalogues Nos . 1 33 sent gratis post - free 
 world 




 BOOK VII . 


 BOOK VIII . 


 AVE MARIA CHERUBINI 


 SLOW MOVEMENTS 


 


 ORGAN SONATAS 


 LONDON : ASHDOWN & PARRY , 


 HANOVER SQUARE 


 


 R. L. DE PEARSALL 


 COLLEGIATE SERIES 


 DR . S. S. WESLEY 


 BERTHOLD TOURS 


 DULCIANA 


 SCHUMANN 


 J. P. KNIGHT 


 J. L. ROECKEL 


 G. B. ARNOLD , MUS . DOC . 


 C. E .. HEY 


 ERNEST LINDE . 


 RICORDI 


 DEPOT ITALIAN MUSIC 


 23 , CHARLES STREET , MIDDLESEX HOSPITAL , 


 LONDON , W 


 NEW SONGS 


 RICORDI 


 23 , CHARLES STREET , MIDDLESEX HOSPITAL , 


 READY 


 CARL ENGEL 


 CONTENTS VOL . 


 CONTENTS 


 VOL . II 


 MACBETH , 


 VINCENT 


 NOVELLO 


 


 407 


 NOVELLO , EWER & CO . 


 MUSIC PRIMERS 


 EDITED 


 DR . STAINER 


 READY . 


 RUDIMENTS MUSIC 


 11 . SCIENTIFIC BASIS MUSIC 


 16 . ELEMENTS BEAUTIFUL 


 PREPARATION 


 5 . SINGING - - - - A. RANDEGGER . 


 CHURCH CHOIR TRAINING 


 19 . DOUBLE COUNTERPOINT 


 , STRONGLY BOUND 


 PAPER BOARDS , PRICE 6D. EXTRA 


 LONDON : NOVELLO , EWER CO 


 REDUCED PRICE , SHILLING 


 USE 


 


 RICHARD MANN 


 NEW REVISED EDITION , ADDITIONS , 


 SUPPLEMENTAL 


 BOOK EXERCISES 


 USE 


 LEARNING SING SIGHT 


 COMPILED ARRANGED 


 HENRY GADSBY 


 DR . BENNETT GILBERT 


 SCHOOL HARMONY 


 CLASS SINGERS ’ ABC 


 SHORT SYSTEM PIANOFORTE . 


 408 


 - FIFTH EDITION . 


 PRICE SHILLING , ENLARGED . 


 COLLEGIATE SCHOOL 


 SIGHT - SINGING MANUAL 


 COLLEGIATE VOCAL TUTOR 


 SECOND EDITION 


 COLLEGIATE SOLFEGGI 


 ARRANGED 


 VOICES 


 SPECIALLY ADAPTED 


 HIGH SCHOOLS GIRLS , PUBLIC 


 SCHOOLS , & SINGING - CLASSES 


 BOOKS I. II . , DUETS ; BOOK III . , TRIOS 


 COLLEGIATE ORGAN TUTOR . 


 EDITED FREDERIC ARCHER 


 COLLEGIATE PIANOFORTE 


 TUTOR 


 AVOID 


 CONSECUTIVE FIFTHS 


 OCTAVES COUNTERPOINT . 


 ESSAY , R. L. DE PEARSALL 


 TWELFTH EDITION 


 CATHEDRAL CHANT - BOOK 


 PREPARATION 


 CATHEDRAL CHANT - BOOK 


 PSALTER 


 BERTHOLD TOURS 


 R. L. DE PEARSALL 


 - SONGS 


 FANNY HENSEL , 


 - SONGS 


 COMPOSED 


 JACQUES BLUMENTHAL 


 ENGLAND GLORY ” 


 PATRIOTIC - SONG S.A.T.B. 


 NOVELLO , EWER CO . COMPLETE 


 UNIFORM EDITION 


 MENDELSSOHN 


 THIRTEEN - SONGS 


 12 


 321 


 THI 


 SOU 


 ORIG 


 FR 


 REDUCED PRICES 


 OUSELEY MONR 


 POINTED PSALTER 


 NOVELLO ORIGINAL OCTAVO EDITION 


 CHORUSES 


 


 NOVELLO OCTAVO EDITION 


 HANDEL ORATORIOS 


 SEMELE PASSION 


 CHORAL SOCIETIES . 


 ALEXANDER BALUS 


 TRIUMPH TIME TRUTH 


 HANDEL MESSIAH 


 HAYDN CREATION 


 MENDELSSOHN ELIJAH 


 LONDON : NOVELLO , EWER CO 


 NOVELLO 


 R ™ DCHODWOAMCOMMMOD 


 HYMN PRAISE TONIC SOL - FA SERIES , 


 WORKS PAPER BOARDS , 


 PRICE 6D. EXTRA 


 LORELEY .. MENDELSSOHN 0 6 


 LORD , LONG WILT ‘ THOU 


 INTENDED ASSIST STUDENT ACQUIRING 


 SOUND KNOWLEDGE INSTRUMENT 


 PROPER MANIPULATION ; 


 SERIES 


 ORIGINAL EXERCISES ILLUSTRATIVE 


 COMPOSITIONS 


 FRED ERIC ARCHER 


 LONDON : NOVELLO , EWER CO 


 LON DON NOVELLO , EWER CO . 


 


 MORNING EVENING SERVICE 


 F. MENDELSSOHN BARTHOLDY . 


 HYMNARY 


 BOOK CHURCH SONG 


 FOLLOWING EDITIONS READY : — 


 TONIC SOL - FA EDITION 


 TONIC SOL - FA EDITION . 


 ANGLICAN HYMN - BOOK 


 MISSIONARY MANUAL TUNE - BOOK . 


 


 UNTO , O LORD . ” 


 EW ORGAN MUSIC . — ORIGINAL 


 SECOND SERIES . 


 ANGLICAN CHORAL SERVICE BOOK . 


 USELEY MONK PSALTER 


 OULE COLLECTION 527 CHANTS , 57 


 OFFERTORY SENTENCES 


 APPENDIX FESTAL USE , 


 SET MUSIC 


 JOSEPH BARNBY 


 OFFERTORY SENTENCES 


 ( COMPLETE ) , 


 SET MUSIC 


 CHARLES JOSEPH FROST 


 PSALTER , PROPER PSALMS , HYMNS , 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI 


 ORDER HOLY COMMUNION . 


 Y SOUL TRULY WAITETH 


 EASY ANTHEMS 


 O WORSHIP LORD BEAUTY HOLINESS . 


 KYRIE ELEISONS 


 CONSIDERETH POOR 


 412 


 MENDELSSOHN 


 SONATAS 


 PRELUDES FUGUES 


 SONATAS 


 


 ORGAN 


 COMPOSED 


 J. LEMMENS . 


 ORGAN PIECES 


 FREE STYLE 


 COMPOSED 


 J. LEMMENS 


 2 . CHRISTMAS OFFERTORIUM . 


 3 . FANTASIA MINOR . 


 4 . GRAND FANTASIA E MINOR . 


 SHORT VOLUNTARIES 


 


 ORGAN 


 ARRANGED 


 JOHN HILES 


 REDUCED SHILLING 


 BACH 


 PROGRESSIVE STUDIES 


 PRELIMINARY - PRELUDES 


 FUGUES . 


 ORGANIST COMPANION , 


 COLLECTION VOLUNTARIES , 


 SIR JOHN GOSSHARMONIUM MUSIC 
 LOUIS | ENGEL 

 s. d. 
 SACRED AIRS ... vey sa 
 singly ees « . o 6 
 . 1 . Air , ‘ 7th Century / se .. Stradella . 
 2 . mighty pens ( Creation ) av Haydn . 
 3 . Agnus Dei ( 1st Mass ) .. Mozart . 
 4 . Cujus animam ( Stabat Mater ) Rossini . 
 5 . Dead March ( Saul ) Handel . 
 6 . Lord , remember David Handel . 
 > . Holy , holy ‘ oe Handel . 
 8 . Angels bright fair ... Handel . 
 0 . os tollis ( Twelfth Mass ) Mozart . 
 ro . Eia Mater ( Stabat Mater ) ... Rossini . 
 11 . shall feed flock ( Messiah ) « Handel . 
 12 . Pastoral Symphony Handel . 
 en gs ScHUBERT ... ul wid oe 2 6 
 singly ‘ jos kat .. o 6 
 ' No.t . Ave Maria . 
 2 . Hark , hark lark . 
 3 . hunter . 
 4 . L’Adieu . 
 5 . Serenade . 
 6 . Hark , bell tolling . 
 DUETS PIANO AREAS 
 No.1 . Marion .. ey . .. Engel 1 6 
 2 . La Sonnambula . ia Bellini 20 
 3 . Adelaida Em odd os .. Beethoven 2 0 
 4- Mosé Egitto . ass ‘ Rossini 1 6 
 5 . Serenade Ave Maria ye .. Schubert 1 6 
 6 . Lied ohne Worte Mendelssohn 1 6 

 LONDON : NOVELLO , EWER CO . 
 RGAN SOLO.—Introduction , Variations , 




 SELECT COMPOSITIONS 


 


 GREAT MASTERS , 


 ARRANGED ORGAN 


 ARTHUR HENRY BROWN 


 WER 


 AANGCOA 


 ‘ “ MARSCHNER ANDANTE . ” — PRE- 


 EW FLUTE MUSIC.—SIX ORIGINAL 


 SMALLWOOD 


 CLASSICAL & SECULAR EXTRACTS 


 


 PIANOFORTE 


 AFRAID 


 C. SWINNERTON HEAP 


 JERUSALEM 


 


 CH . GOUNOD . 


 TENOR SONGS 


 MUSIC 


 STEPHEN S. STRATTON 


 HH HO 


 ZINGARA 


 WORDS L’ALLEGRO , MILTON . 


 MUSIC COMPOSED 


 HENRY LAHEE 


 CHORAL SONGS SCHOOL & HOME . 


 - ORIGINAL SONGS 


 


 , , VOICES 


 REDUCED PRICES 


 SIR HENRY R. BISHOP 


 COMPOSITIONS 


 WILH ELM SCHULTH ES 


 LONDON : NOVELLO , EWER CO 


 MORNING & EVENING SERVICE 


 


 KYRIE , CREED , SANCTUS , 


 DR . JOHN SMITH , 


 SWEET LOW , 


 ] . BARNBY ADMIRED - SONG , 


 ARRANGED PIANOFORTE , 


 WALTER MACFARREN 


 HUSARENRITT 


 PIANOFORTE 


 


 FRITZ SPINDLER . 


 NEW EDITION , CAREFULLY REVISED . 


 PUBLISHED 


 MENDELSSOHN OVERTURES 


 NEW EDITION , CAREFULLY REVISED . 


 PUBLISHED 


 MENDELSSOHN SYMPHONIES 


 PUBLISHED 


 TRIOS FEMALE VOICES 


 


 FRANZ ABT . 


 MARCHE CHEVALERESQUE 


 PIANOFORTE 


 COMPOSED 


 SIEGFRIED JACOBY . London : NovELLo , Ewer Co 

 published . 
 BEETHOVEN 
 CALM SEA PROSPEROUS 
 VOYAGE 

 CHORUS VOICES 




 TWINE YE GARLANDS 


 SOLDIER LEGACY 


 JESSY LEA 


 OPERA DI CAMERA ACTS 


 TE 


 SELECTION FROMNOVELLO CATALOGUE ORGAN MUSIC 

 LEFEBURE - WELY . HENRY SMART . s. d. 
 GRAND OFFERTOIRES ... sg ee « = eve . Cth : . 0 } ORIGINAL COMPOSITIONS : — 4 
 1 . Offertoire , Bflat ... 1 0 ] 4 . Offertoire , inG ... 1 9 I. — Variations ... ooo eco eve ove ovo . ; 
 2 . Offertoire , F 6 ] 5 . Offertoire , inA .. 0 2 . byes cg . pian “ ove Te 
 3 . Offertoire , C r 0 ] 6 , Offertoire , inE .. 1 6 3- Le ee sane Bile th Pb cc PG AB 
 MODERN ORGANIST : Collection Organ Pieces Ps Andante , . 2 , major oa Ss se 
 styles . adaptation English Organs W. T. 6 . Andante , No.3,inE minor ... ig ea ps 2 - 6 
 Best . Numbers , mn Volume . cloth 18 o 7 . Sheet Easy Dlicda ” Woe . tends . = “ er 
 No.1 . 1s , 6d . . 4 . No.9 . 2s . 6d . 8 . Short Easy Pieces . Nos.3and4 .. « 6 
 Pastorale . Offertoire & Prelude . Fantasia Pastorale . g. Short Easy Pieces . Nos.5and6 .. 1 6 
 Communion . Elevation . Offertoire Christ- 10 . Air Variations Finale Fugato oe 3 0 
 Prelude Hymn No.5 . 28 mas . a2 . ae Sberts pad   hvan , ‘ « . , 
 . Feast rtoire . 12 . Short , Easy Pieces , 5 od 
 S$ Prateak . aac oar . 10 , 2s . 13 . Short , Easy Pieces , 9 to12 ... ove eee ae .@ 6 
 Offertoire . No.6 . 2s . Offertoire , DR . STAINER . 
 No.2 . 1s . 6d . ays sony ng Hymn , | Prelude . ARRANGEMENTS , Numbers — 2 0 
 levati Com- < . 11 . 2s . 1 . Andante Quintett , E fiat ( p. « - Beethoven . 
 - non con sh ge wera Postlude . Minuet Pignofore Duet = < 0 ee 
 elude . . 7 . 28 . F ‘ 2 . Andante Piano orte Duet ... « .. Mozart . 
 Difertoire . Allegretto Cantabile . poe . Overture , ‘ ‘ Semele ” ... Handel . 
 Postludium . < Introduction Allegro ‘ Symphony .. Haydn . 
 . 3 . 28 . . & 1s . 6d . , 12 . 28 . Andante molto Pianoforte Sonata ( Op . 122 ) Schubert . 
 Communion . Postlude . Offertoire . 4 mor ae ‘ @ nM — — second ) 
 Fugue . Preludes , Fugue . yrie eleison , Mass . 1 + . Eybler . 
 Offertoire . Communion . March . 5 . Adagio expressivo Symphony , - Schumann . 
 8 FRANZ Liszr . Cavatina Violin Quartett . 13 
 Andante el , 
 S AVE MARIA D’ARCADELT ies iti tiv & Andante P.F. Sonata , . 3 , “ Op . 120 . Schubert . 
 G. A. MACFARREN . CHARLES E. STEPHENS . 
 SONATA , inC ... ne eve , 4 « | FANTASIA ( Chorale , “ S. James ” ) ... hases sce 
 SET MOVEMENTS _ ...... wk Si 
 MENDELSSOHN . No.1 . Elegy . No.2 . Andantino alla Canone 

 vO 




 GUSTAV MERKEL . 


 B. MOLIQUE . 


 AUGUST MOOSMAIR 


 ANDANTES .. 


 W. J. PRICHARD , 


 SCHUBERT . 


 E. SILAS . 


 ORIGINAL COMPOSITIONS : — 


 DR . W. SPARK . 


 ELIZABETH STIRLING . 


 J. T. STONE . 


 6 & . , THORNE . 


 6| ORIGINAL COMPOSITIONS : — 


 : . VAN EYKEN . 


 DR . C. G. VERRINDER . 


 DR , S. S. WESLEY . 


 AGNES ZIMMERMANN . 


 LONDON : NOVELLO , EWER CO 


 416 


 * ( 2 . ALLA MARCIA . 


 4 . FUGHETTA . 


 STREAM ( STILLEN 


 WEETLY DREAM ( TRAUME SUSS 


 1 . PRELUDE .. 


 . 2 . SARABANDE 


 . 4 . MENUET 


 » 6 . TAMBOURIN 


 H M.S. PINAFORE , READY 


 NEW SONG BLUMENTHAL . 


 CROSS FAR BLUE HILLS , 


 PUBLISHED . 


 NEW EDITION . 


 ETZLER & CO.’"S - 


 POPULAR WALTZES , QUADRILLES , 


 ETZLER & CO.”S - 


 ETZLER & CO.”S - 


 LONDON : METZLER CO 


 387 , GREAT MARLBOROUGH STREET , W