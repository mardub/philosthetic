


 YWLIM 


 MUSICAL TIMES 


 singing - class circular 


 publish 


 JUNE 


 


 month 


 , 1883 


 SISHERIES INTERNATIONAL exhibition . 


 RICHARD LEMAIRE , 


 ISS KILMANSEGG GOLDEN 


 XFORD.—NEW COLLEGE.—CHORISTERS 


 306 


 MUSICAL TIMES 


 JUNE 1 , 1853 


 PROFESSIONAL notice . open engagement Concerts Oratorios , 
 54 , Duchess Road , Edgbaston , Birmingham . 
 MADAME CLARA WEST ( soprano ) . 
 MISS LOTTIE WEST ( contralto 

 Oratorios , Cantatas , Vocal Duets , ballad , & c. Beethoven Villa , 
 King Edw ard Road , Hackney , London 

 MISS AGNES MARY EVERIST ( contralto 




 EE 


 VIIM 


 WIAA 


 12 , BERNERS STREET , LONDON , W 


 CONCERT - giver entrepreneur 


 generally 


 clergy 


 CHOIR - boy 


 ORCHESTRAS CHOIRS 


 NM USIC SCHOOL.—CHURCH ENGLAND 


 S.E 


 RGANIST 


 TRINITY COLLEGE , LONDON 


 HIGHER EXAMINATIONS DIPLOMAS , special 


 local examination pianoforte playing , 


 notice scholarship 


 perform LEEDS festival 


 publish shortly . 


 JOACHIM RAFT 


 EXD WORLD 


 ORATORIO 


 SOLI , CHORUS , ORCHESTRA 


 LIFE MOZART 


 OTTO JAHN 


 translate GERMAN 


 


 PAULINE D. TOWNSEND . 


 volume , cloth , 


 portrait , 


 


 preface SIR GEORGE GROVE , D.C.L 


 TIMES 


 DAILY NEWS 


 ATHEN - EUM 


 MARCH CALVARY 


 


 


 BERTHOLD TOURS . 


 MUSICAL REVIEW | 


 WAR MARCH 


 


 MENDELSSOHN * ATHALIE " 


 PIANOFORTE HARMONIUM arrangement 


 J. W. ELLIOTT 


 overture 


 MENDELSSOHN " ATHALIE 


 PIANOFORTE HARMONIUM arrangement 


 J. W. ELLIOTT . 


 MUSICAL TIMES 


 SINGING - CLASS circular . 


 JUNE 1 , 1883 


 ROYAL COLLEGE MUSIC 


 310 


 letter WAGNER 


 mother 


 birthday 


 VIIM 


 311hoven express surprise wonderful tone 

 poem emanate man 
 teach believe sort tolerate barbarian , 
 rarely appear fellow - man , 
 narrow sympathy family 
 friend . day find pore re- 
 cently publish volume Beethoven letter , 
 confess entirely change 
 opinion , discover composer 
 sacrifice time , money , tranquillity 
 mind hope benefit brother 
 child , death — great 
 measure cause nephew cruel neglect — 
 leave sole heir effect . letter , 
 , write scapegrace relative 
 scan delighted reader 
 convert ; especially 
 |draw attention conclude ' " * 
 farewell , darling ! deserve . retain 
 money require ; want shall 

 purchase come . embrace , 
 /and hope good , studious , noble son 

 impatience restraint 
 | defiance exist form doubt reason 
 |why Wagner aggressive earn 
 | character know 
 | point resemble Beethoven . 
 ; remember Wagner personality 
 ; whilst , lately , little know 
 | Beethoven save , difliculty , 
 gain access converse . 
 admirer 
 great operatic reformer anxious feeling 
 antagonism work shall enter 
 estimate true character . leave , , forthe 
 moment music question , let turn 
 heartfelt word pen away turmoil 
 world , mean eye 
 mother . birthday 
 brighten ' ' touch nature , " 
 stern duty artist 
 dull loving kindness son 

 GREAT COMPOSERS . 
 3y JoserH BENNETT . 
 . XHI.—CHERUBINI ( continue page 259 ) . 
 trustworthy evidence effect 
 Cherubini considerable figure Lon- 
 don . example , little 
 english musical history period . Lord Mount 
 Edgcumbe mention composer 
 date - know ' Reminis- 
 cence , " Parkes , oboist , chatty 
 " memoir " personal observation , dis 




 YVIIM 


 313 


 314 


 XUM 


 X UM 


 315 


 MUSICAL DIRECTORIES MUSICAL 


 AMATEURS 


 316 


 1 


 317 


 CRYSTAL PALACESACRED HARMONIC SOCIETY 

 Tus Society earn golden opinion April 27 
 produce St. James Hall , time 
 London , adequate scale , Schubert Mass e flat — 
 leave world truly inspire com- 
 poser . like Schubert work Church , 
 Mass divisible highly contrasted — 
 beauty power , formal , constrained 
 unexpressive . — , , appear 
 think — depart tradition great 
 master . Haydn , Mozart Beethoven observe 
 rule prescribe contrapuntal treatment certain 
 portion musical service , Schubert follow 
 example , difference . 
 scholastic bond bad . 
 perfectly train master , write contrapuntally 
 like student , present fugal number 
 academic exercise . Mass e flat great work , 
 , , despite , movement 
 Schubert appear cap gown carry- 
 e Gradus . Mass offer example 
 crowded beauty — music delight 
 ear pleasurably excite imagination . " Kyrie , " 
 movement ' Gloria , " ' * Incarnatus , " 
 ' " Dona nobis " — illustrate Schubert good 
 andmost characteristic quality manner 

 asi iad 




 PHILHARMONIC SOCIETYcomposer , work perform , conclu- 
 sion Mr. Mackenzie receive ovation warm 
 thoroughly deserve . " Ballad " worthy 
 contribution treasure Societ : ' , cordial 
 reception , trust , insure pe nent place 
 répertoire . violin - playing Signorina Tua 
 mark excessive refinement 
 accuracy invariably characterise performance ; 
 like hear music Max 
 Bruch Concerto G minor , 
 rendering Adagio reveal possession high 

 artistic quality . Chopin Pianoforte Concerto F 
 minor finely play M. Vladimir de Pachmann , 
 , deservedly , accept 
 |the perfect exponent music polish 
 |composer , termination piece 
 | greet enthusiastic applause . Signor Mierzwinsky 
 | Romanza ' * * o ! muto asil , " trom ' Guillaume 
 Tell , " " fra poco , " ' * Lucia , " vocal 
 | power undue exaggeration style ; Signorina Tua 
 play short piece good eff second 
 | ; orchestral work — Beethoven ' * pastoral " 
 |Symphony , Marche Hongroise , Berlioz ’ 
 | * Faust ' — successfully display power band , 
 | aikhoogh tempo movement Beet- 
 |hoven Symphony strangely iance 
 |the character music . exception Mr. 
 |Mackenzie ' Ballad , " Concert 
 conduct , usual , Mr. W. G. Cusins 

 tas 

 RICHTER CONCERTS 

 new season Concerts begin auspiciousiy 
 7th ult . usual place uncer customary 
 condition ; , fine orchestra overa 
 performer " lead " Herr Ernst Schiever , 
 conduct great chef enterprise 
 . dispense Herr Richter 
 interpretation great classic famous modern 
 | work . viennese musician , unknown 
 year ago , necessary completeness 
 | London season , fact hi f 
 adequate testimony extraordinary 1¢ Concert , 
 | devote memory Richard Wagner , present , 
 | need , liberal selection master work , 
 linclude ' Faust ' ? overture , preiude 
 ' " ' Parsifal " * Tristan , " Liebestcd last- 
 drama , dead March ' Gétterdam- 
 merung ? " ' good representation 
 possible , concert - room , composer write 
 stage , transfer difficult . 
 music , new advance ; 
 , , bear emphatic witness great merit 
 performance . distinguished 
 crowded audience care litt!e selection 
 jcould fail perfection 
 rendering Concert worthy tribute Wagner 
 memory . Beethoven Symphony C minor complete 
 programme , splendidly . Con- 
 certs respect impressive opening 

 second performance rapidly follow , 
 place day later . varied general 
 interest predecessor , programme 
 chief feature Raff Symphony " m Walde , " 
 lengthy , respect , masterly work , 
 associate Brahms second Concerto violin 
 orchestra , overture * * Coriolan " ' ' * " Tann- 
 hauser . " occasion vocal music introduce — 
 time , believe , apart choral work — 
 Miss Orridge singe Gluck ' ' che faro . " " 
 understand experiment orchestral concert , 
 unrelieve song , prove successful 
 point desire ? , Crystal Palace authority 
 wisdom withstand year past demand 
 vocal music Saturday Concerts 
 abolish . difficulty Raff Symphony easily 
 meet Herr Richter skilful guidance , , 
 time St. James Hall , amateur enable judge 
 work fair condition . regard- 
 e Brahms Concerto , impossible Herr 
 Richter , charm wisely , endow soloist 




 BACH CHOIR 


 MR . WILLING CHOIR . 


 MRS . LAMBORN COCK CONCERT 


 YUM 


 MUSICAL TIMES.—Jvne 1 , 1883 . 321 

 Mr. Cusins , purely orchestral piece pro- 
 gramme Beethoven Overture " ' Egmont " 
 Mendelssohn March ' " Athalie . " respect 
 Concert decided success , reflect 
 high credit energy artistic feeling 
 bénéficiare 

 MISS ZIMMERMANN concert 

 SENOR SARASATE concert 

 tue orchestral concert St. James Hall , 
 Friday , 4th ult . , look kind 
 formal leave - taking eminent spanish violinist 
 public accord 
 unqualified tribute admiration . course 
 great gathering , enthusiasm characterise pro- 
 ceeding . Senor Sarasate interpre- 
 tation Beethoven Concerto — great test 
 violinist powers—-i remarkable player 
 sensuous beauty tone genuine passion 
 intellectual vigour . reading individual 
 passage difference opinion justly prevail ; 
 scarcely dispute un- 
 suitability long laboured cadenza intro- 
 duce movement , simply 
 character music . virtuoso 
 hear Vieuxtemps Fantasia Appassionata , Wieniawski 
 Airs Russes spanish piece pen . 
 concert open Mendelssohn Italian Symphony , 
 Philharmonic orchestra , Mr. Cusins , 
 maintain usual standard merit 

 M. DE PACHMANN recital 

 Chopin , music play remarkable taste , 
 finish distinction . understand 
 care know interpreter master , 
 happy combination circumstance , 
 pianist fit render Chopin music 
 , public blame prefer 
 hear capacity , , , performer 
 par excellence polish master work hold 
 enviable position . master 
 instrument man endow subtle appreciation 
 fine sensibility . Chopin Recital , 
 course , successful . M.de Pachmann play large 
 number favourite selection , need specify , 
 hold audience seat , attentive 
 end . execution present usual charm- 
 e quality exquisite finesse , delicate shading , com- 
 prehensive sympathy composer vary mood 

 second Recital , 22nd ult . , scarcely 
 | attend , albeit M. de Pachmann 
 | programme include little Chopin . little , how- 
 , comprise Sonata Funeral March 
 | - know Valse flat , artist 
 |with special brilliancy , delight 
 |heard . selection Bach , 
 | Schubert , Mendelssohn , Beethoven , Moscheles , & c. 
 j M. de Pachmann fall short uniform success 
 | gain previous occasion , skill 
 | inadequate , temperament , , 
 | variance spirit . russian pianist 
 | mean cold , intellectual performer , like Von Bilow , 
 play master equally . execute feel , 
 sympathy , want , necessarily affect 
 ' execution . , pleasurable 
 | interesting hear gifted artist play , fact 
 audience abundantly testify 

 MADAME SOPHIE MENTER 




 MR . CHARLES HALLE concert 


 322 


 MR . RICKARD PIANOFORTE RECITAL 


 MISS HOLLAND concert 


 BOROUGH HACKNEY CHORAL ASSOCIATION 


 TUFNELL PARK CHORAL SOCIETY 


 HIGHBURY PHILHARMONIC SOCIETY 


 


 NE , 1883 . 323 


 GUILDHALL SCHOOL MUSIC 


 festival son CLERGY 


 MUSIC BIRMINGHAM . 


 ( correspondent . ) 


 324ersion Mr. H. B. Farnie . company Madame 
 Soldene , play Cérisette , deficient 
 spirit dramatic talent , musical qualification 
 limited , hardly fair judge work 
 present occasion effect produce . Von 
 Suppé Operetta ' ' Boccacio , " produce evening 
 later , company home , performance 
 meet general approval 

 17th Mr. D. F. Davis se - annual 
 Harp Festival , stock feature band 
 dozen harp play lady amateur , friend 
 pupil bénéficiaire . occasion lover 
 minstrelsy respond appeal wonted 
 liberality , — — attraction 
 harp largely supplement voice , 
 represent singing Miss Emilie Lloyd , Mr. 
 Vernon Rigby local lady . performance 
 criticism . piece general choose 
 display somewhat limited musical capability 
 bardic instrument , result appear 
 appreciate friend relation fair 
 performer , constitute mean proportion 
 audience , sestet arrangement Beethoven Choral 
 Fantasia , fairly play , injudicious 
 unsuitable work ineffective . Miss 
 Emilie Lloyd sing good feeling effect Watson 
 " Winter Story , " Behrend ' ' Auntie , ’' Moore 
 ' * Minstrel Boy , " Mr. Vernon Rigby delight 
 audience ' Salve dimora , " Schubert serenade " Thro ' 
 night , " Schumann " devotion , " Nelson ' * Mary 
 Argyle , " way encore , perennial 
 " la donna ¢ mobile " * ' Rigoletto 

 22nd interesting vocal instrumental | 
 concert new Lecture Theatre 
 Midland Institute , local charity , principal per- 
 , service gratuitously , Mr , 
 Miss Saniley Miss Agnes Miller ( pianoforte ) . 
 programme comprise Spohr Double String Quartet 
 D minor , Beethoven Sonata Patetica Miss Miller , 
 Rubinstein Violin Pianoforte Sonata G major 
 ( Op . 13 ) , charmingly play Mr. F. Ward Miss 
 Miller , couple Salonstiicke composer , 
 excerpt Beethoven quintet Arrangement 
 Septet e flat . Mr. Santley , fine voice , sing 
 Gounod arab love - song * * Medjé , " " Mendelssohn ' * Shep- 
 herd Lay , ' " ' Hatton * Anthea , " Miss Maud V. 
 White ' * devout lover " ( accompany 
 composer ) fervent finished style , win 
 encore - mention song . Miss Santley 
 contribution comprise Molloy " child prayer , " 
 Rubinstein ' ' Nicht mit Engeln , " Miss White im- 
 passioned song ' soul enchanted boat . ' 
 playing Beethoven Sonata Miss Miller 
 feature evening 

 annual Orchestral Concert Edgbaston 
 Amateur Musical Union , 25th , chief feature 
 Beethoven Symphony D , . 2 , Over- 
 ture ' " ' Lodoiska " ( Cherubini ) ' * Le Cheval de 
 Bronze " ( Auber 

 MUSIC BRISTOL . 
 ( correspondent 




 MUSIC SOUTH STAFFORDSHIRE . 


 MUSICAL TIMESMr . Sims Reeves pay farewell visit 
 Wolverhampton , accompany Mr. Santley , Misses 
 Clements Spencer Jones , Messrs. Herbert Reeves , 
 Nicholson ( flute ) Sidney Naylor ( conductor ) ; 
 vast audience fairly 
 enraptured performance . Mr. Santley effort 
 equally successful 

 arrangement forthcoming Triennial 
 Festival Wolverhampton forward state , 
 rehearsal twice week vigorously proceed 
 . hitherto Festival limit single 
 day ; fresh vitality having infuse 
 management , movement place 
 broad sound basis , decide 
 performance successive day . 
 programme include * Elijah , " Gounod * * Messe 
 Solennelle , ' " ' Beethoven ' * Mount Olives , " Hummel 
 ' ' Alma Virgo , " Macfarren ' " ' Lady Lake , " 
 Mackenzie ' " Jason , " miscellaneous selection 
 include evening programme . artist engage 
 include Misses Davies , Williams Emilie Lloyd , Madame 
 Patey , Messrs. Edward Lloyd , Maas King , Signor 
 Foli . Mr. Carrodus lead band performer , 
 Dr , Swinnerton Heap appoint conductor . 
 pain expense spare musical 
 success thorough , highly satisfactory financial issue 
 assure 

 MUSIC YORKSHIRE . 
 ( correspondent 




 MACKENZIE ' COLOMBA 


 326 


 ROYAL ACADEMY MUSIC . 


 1883 . 


 YWLIM 


 327 


 328 


 anthem voice . 


 9—)- — _ — = : 


 : : — _ 6 = 


 O love LORD . 


 - o-3 


 LI } ~ = 


 WIAA 


 XUM 


 PA 


 O LOVE LORD 


 95 - bb — 


 SAAR ! 508 


 XUM 


 335solo F. David finished style , Mr. Docker 
 contribute pianoforte solo . Miss Pottinger | pianoforte ) 
 Mr , G. E. Croager ( organ ) render valuable service 
 accompanist 

 HERR Kocu annual Pianoforte Recital 
 Northfield Hall , Highgate , Thursday evening , 
 17th ult . programme include Bach Fantasia 
 |c minor , Beethoven Andante ( op . 35 ) , Mendelssohn 
 Andante con moto Presto , Mendelssohn Songs 
 word , Chopin Scherzo B minor ( Op . 20 ) 
 Polonaise . 5 , Liszt hungarian Rhapsody , . 12 , 
 original galop , ' ' Bucephale . " ' vocal solo 
 contribute Mdme . Bonner Mr. Fulkerson , 
 notable ' " ' far great lowly state " 
 ( Gounod ) , * Adelaide ' ( Beethoven ) , " o swallow , 
 swallow " ( Moncrief ) , effective rendering 
 duet ' ' Una notte Venezia " elicit - deserve 
 encore 

 Tuer Senate Royal University Ireland ap- 
 point Sir Robert Stewart Dr. Joseph Smith joint 
 Professors Music institution . Sir Robert Stewart 
 year hold corresponding post 
 | University Dublin . Dr. Smith , young 
 |man , prove worthy honour 
 } confer . 1880 award 
 | prize offer London Sunday School Choir 
 | composition - song anthem , 
 | year obtain Welsh Listedfodd Prize 
 | Guineas Gold Medal setting 
 67th Psalm . hold , past 
 year , post Examiner Music Board 
 Intermediate Education Ireland 




 336at Gloucester Musical Festival , , 

 state , commence September 4 , fol- 
 low work perform : " Elijah , " ' * Redemption , " 
 ' " ' Lobgesang , " " Messiah , " ' Mass C ( Beethoven ) , ' * Wal- 
 purgis Night , " Symphony G minor ( Mozart ) . 
 addition following , believe , 
 specially compose Festival : " St. Mary Magdalen " 
 ( Dr. Stainer ) , ' ' Sennacherib " ( Dr. Arnold ) , ' Elegiac Sym- 
 phony " ( C. Villiers Stanford ) , short choral work ( Dr. 
 Hubert Parry ) , Anthem ( C. Harford Lloyd ) . 
 vocalist engage Miss A. Williams , Miss Mary Davies , 
 Miss Avigliana , Madame Patey , Miss H. Wilson , Mr. E. 
 Lloyd , Mr. Newth , Mr. Il ’ . King , Mr. Brereton , Mr. 
 Santley 

 successful Concert Miss Annie 
 Matthews Brixton Hall , Thursday evening , 3rd 
 ult . , assist Madame Adeline Paget , Miss Lizzie 
 Evans , Miss Edith Daniel , Miss Marian McKenzie ; 
 Messrs. Arthur Thompson , Wakefield Reed , Franklin 
 Clive , James Budd , R. Odell . Miss Matthews sing 
 ability , gain cordial reception 
 contribution ; flute solo excellently 
 Mr. Collard . Mr. Turle Lee accompany 




 ES 


 338 


 REVIEWS 


 1 , 1852 . 339 


 foreign NOTEStook place St. Thomas Church , chamber- 
 music performance Festival begin Gewand- 
 haus . noteworthy work suite 
 violin , viola violoncello , D minor , Eduard 
 de Hartog , anda Pianoforte Quintet , Friedrich Kiel . 
 pianist , Herr Reisenauer , play Transcriptions Liszt , 
 fourteen vocal solo form remainder pro- 
 gramme . Concert , following morning , 
 bring programme ofchamber - music , particular 
 feature String Quartet Rimsky Kor- 
 sakoff , theme variation piano Heinrich 
 von Herzogenberg , String Quartet G minor Volkmann , 
 vocal quartet pianoforte duet Hans 
 Huber . evening grand Orchestral 
 Concert place , interesting novelty 
 A. Borodin Symphony E flat ; follow 
 Brahms Violin Concerto , play Herr A. Brodsky , 
 chorus male voice Peter Cornelius 
 Liszt e flat Pianoforte Concerto , play country- 
 man Eugene D’Albert . talented artist create un- 
 bounded enthusiasm excellent performance 
 difficult Concerto , compel play 
 Rubinstein Etudes encore . second 
 Concert devote memory Richard 
 Wagner , consist Eine Faust Overture , 
 Epilogue Adolf Stern , Prelude Finale 
 act " Parsifal . " exceptionally in- 
 teresting Concert , difficult bestow sufficient at- 
 tention organ performance , place 
 morning St. Nicholas Church . church , how- 
 , filled , programme interest- 
 e , contain striking novelty . 
 Concert Festival place Leipzig 
 Crystal Palace , prove worthy finish in- 
 tereste proceeding , ' number brilliantly 
 orchestrate March unpublished Opera ' " King 
 Hiarne , " ' Ingeborg von Bronsart . follow 
 solo violoncello , & c. important 
 item Eine Faust Fantasia orchestra , Mihalo- 
 vich , Brahms new Cantata ' ' Gesang der Parzen . " 
 second programme contain Liszt 
 grandiose symphonic Poem , chorus , ' " * Prometheus 
 unbind , " vocal instrumental solo , 
 Wagner Kaisermarsch chorus . pro- 
 duce effect final hymn 
 Emperor audience rise applaud 
 enthusiasm seldom witness 

 sixtieth annual Festival Lower Rhine 
 place Cologne 13th , 14th 15th ult . , 
 conductorship veteran Dr. Ferdinand von Hiller . 
 official programme publish occa - ion con- 
 tain interesting summary history 
 celebrate Festivals . begin Dusseldorf 
 year 1818 annually ( 
 exception ) town Elberfeld , 
 Aachen Cologne . conductor find 
 famous Ries , Mendelssohn , Spohr , Kreutzer , 
 Rietz , Schumann , Liszt , Otto Goldschmidt , Rubinstein , 
 Joachim , Hiller , soloist 
 interpret work produce 
 select celebrated artist . list 
 work perform contain important compo- 
 sition Bach B minor Mass 
 modern writer . programme year lestival , 
 artist engage , fully sustain reputation 
 gathering . day Beethoven ' * Eroica " ’ 
 Symphony Haydn ' ' creation " . 
 second day prove interesting , comprise 
 Bach Cantata ' ' God time good , " ' 
 Handel Concerto Grosso orchestra , Mendelssohn 
 114th Psalm , Hiller Cantata ' ' Richard Coeur de Lion , " 
 second Pianoforte Concerto Brahms ( play 
 ) , Bruch ' * fair Ellen , " Beethoven Overture 
 ' * Leonora . " programme day con- 
 siste chiefly solo piece contain Brahms 
 second Symphony D ( conduct ) , Wagner 
 Eine Faust Overture overture ' ' Manfred , " 
 Schumann . M. Wilhelmj , announce play 
 violin solo , appear , stead Fraulein 
 Soldat , Berlin , perform Mendelssohn Violin Concerto 
 hungarian dance Brahms 

 5 ee oe oe ee et . ee ae 




 XUM 


 musical TIME S- — JUNE iin long report musical event 
 Leipzig Musikalisches Centralblatt : ' 
 astonish reader hear musically sterile 
 England produce new Operas native com- 
 poser day . ' Esmeralda ' Griffin ( ' ) 
 Thomas , Mackenzie great work ' Colomba , ' | 
 receive enthusiasm , 
 singer theatre - goer usually frequent 
 Italian Opera , musician general 
 educate class society . ' defi- 
 nition indicate peculiarity 
 composer . Thomas study Paris 
 inspiration Gounod , Mackenzie disciple | 
 new german school rank | 
 Brahms Raff , classification allow . 
 libretto ' Colomba ' found - know 
 novel Prosper Merimée . noble tale 
 dramatise rare ability Times critic , Dr. 
 Francis Hueffer , usual complaint un- 
 satisfactory libretto hinder t 
 assist unfold wing cf genius 
 p lace . Asin Mackenzie new talent ap- 
 peare destitute field english opera , 
 way author libretto deserve gre 
 talent knowledge stage effect construct 
 powerful drama simple novel . ' Colomba ' 
 surely find home german stage , , 
 compose english word , prove bar 
 compeser admiration great 
 ' Fatherland , ' Germany usually England 

 Anton Rubinstein , ask Herr B. Senff 
 publisher Siguale , edit new edition 
 classical work Beethoven , Mozart , & c. , write 
 interesting letter reply . acknowledge 
 good intention editor publisher , 
 world carefully prepare splendidly 
 execute edition musical cl : 
 impossible man revise publisher 
 bring edition accept 
 standard edition future . propose , instead , 
 principal publisher join invite 
 competent musician annual meeting , 

 8 
 nis 




 1883 . 343 Madame Marcella Sembrich , know /rima 
 donna Covent Garden , short stay Paris 
 way London , appear time 
 capital evening m ncert . introduce 
 vocalist pianist , play 
 Chopin 

 Concert recently M. Pasdeloup 
 Eden Theatre Paris , performance Beethoven 
 celebrate Kreutzer Sonata , M. Théodore Ritter 
 pianist violin orchestra 
 play violin . imagine 
 shocking misrepresentation great work 
 great master . poor Beethoven 

 M. Alexandre Guilmant Organ Recitals Orchestra 
 continue attract crowd Trocadéro Paris . 
 programme fourth Concerts con 




 correspondence 


 restriction music . 


 editor ' ' MUSICAL TIMES 


 MUSIC relation art . 


 editor ' * MUSICAL TIMES 


 correspondent . 


 brief summary country news 


 XUM 


 


 345concert cheneatrel Union place . Tem 

 8th ult . , numerous audience . princiy al p 
 band — Beethoven Symphony C minor Ove ertures 
 kuy Blas ( Mendelssohn ) " Poet — " ( Suppé)—were 

 play , able direction Mr. E song con- 
 tribute success Miss Annie Thomas ; violin 
 solo Mr. F. Ward , performance Weber Concertstiick 
 Mr. Ellis interesting item selection . song 
 eifectively accompany Mr. Hancock 




 1S 


 346 


 MUSICAL TIMES 


 JUNE 1 , 1883wen 

 rate 
 Beethoven 

 SaLorp.—The member Choral Society 
 Annual Concert , roth ult . , conductorship Mr 




 o1 


 XUM 


 


 MUSICAL TIMES 


 DEATHS . 


 STANISLAS M 


 HEN 


 J EW , revise 


 BNL arge EDIT 


 N & IW , revise , E enlarge edition 


 CHRIST CHU ] rch 


 ion 


 dure month . 


 OBB , G. 


 » W74 


 ATHER , REV . G. — 


 348 


 MESSIAH 


 permission 


 MAJESTY QUEEN . 


 ANTHEM . = 


 TH LORD SHEPHERD . 


 HENRY VIII 


 OPERA , act , 


 compose 


 CAMILLE SAINT - SAENS 


 LAKME 


 OPERA act | 


 compose 


 LEO DELIBES 


 SLOW movement QUINTET 


 MINUET " MINUETS 


 


 GEORGE C. MARTINae . » SCHUMANN 

 ORCHESTRA " BEETHOVEN 

 London : Novet LLO , EWER Co 




 new revise edition . 


 air 


 MENDELSSOHN " ELIJAH " 


 arrange ORGAN 


 GEORGE CALKIN 


 


 ORIGINAL 


 12 . " " " " 5 » & 2 0 


 YUM 


 349 


 IANO TUNER , 


 engagement 


 PIANGFORT E 


 R. STOLBERG VOICE LOZENGE 


 G. H 


 ORTH WALES MCSICAL DEPOT.—G. 


 HARLES B 


 LUCY J. MULLEN 


 author work correct , revise 


 YUM 


 YUM 


 , 1883 . 351 


 violin . 


 QARIS UNIVERSAL EXHIBITION , 1878 . — 


 GOLD MEDAL , PARIS , 1879 ; GOLD MEDAL , ROME , 1880 , 


 ARTISTIC HOUSE , 


 CH . J. B. COLLIN - MEZIN 


 VIOLIN , VIOLONCELLO , BOW MAKER 


 MALEY , YOUNG , & OLDKNOW , 


 ORGAN BUILDERS . 


 KING ROAD , ST . PANCRAS , LONDON , N.W. 


 inspection INVITED 


 BATE BOURLET , 


 EAST LONDON ORGAN work . 


 patent REED action . 


 warehouse : L IV ERPOOL colony . 


 CHE ENHAM , ENGLAND . 


 2,676 


 majesty royal letter patent 


 patent action 


 attaching 


 J. AIN SWORTH 


 D ORGAN pedal 


 price patent action 


 pedal 


 pleasant auxiliary absolutely free 


 annoyance . 


 SATISFACTION GUARA 


 PROF BSSION LIBE ALLY dealt 


 trade 


 TE ' STIMONIAL S. 


 caution . 


 JOHN AINSWORTH , 


 BRINSCALL , CHORLEY , EN 


 GLAND 


 35 ? 


 | VIOLIN BOW 


 MAKERS REPAIRERS 


 GEORGE WITHERS & CO 


 WHOLESALE importer 


 musical string 


 fine collection italian instrument 


 JAMES CONACHER SONS , 


 ORGAN BUILDERS 


 BATH BUILDINGS , HUDDERSFIELD . 


 NEW MUSIC - ENGRAVING 


 STEAM printing ) 


 exhibit 


 50 cent . CHEAPER . 


 PATENT PAPER TYPE COMPANY , 


 62 , HATTON GARDEN , LONDON , E.C 


 © 40 


 establish 1854 


 P. CONACHER CO . , , 


 ORGAN BUILDERS , 


 HUDDERSFIELD 


 OWER " ORGAN 


 research 


 early history 


 


 VIOLIN family 


 CARL ENGEL 


 , 1883 


 


 NEW OPERA 


 COLOMB 


 


 MACKENZIE 


 VOCERO 


 CORSIC LOV E- SONG 


 OLD CORSICAN BALLAD 


 sat 


 AH , mind 


 , PRICE SHILLINGS . 


 SOLD HALF - price 


 


 LONDON : NOVELLO , EWER CO 


 YIIM 


 XUM 


 353 


 recent addition 


 


 ORGAN CATALOGUE 


 EDWIN ASH 


 EDOU ARD BATISTE . 


 selection composition 


 ARTHUR HENRY BROWN 


 selection overture arrange 


 ORGAN PEDAL OBBLIGATO 


 SURO NHSixty - S1x . , PRICE 3S. 

 VAN BEETHOVEN . 
 edit Frep . ARCHER ... 
 GROVER 

 Alagio , op . 2 , no.1 




 F , MIRUS 


 SEYMOUR SMITH 


 SYDNEY SMITH . 


 DR . WM . SP : ARK . 


 book 21 SHORT ORGAN PIECES 


 book 22 SHORT ORGAN PIECESJ . WODE HOUSE 

 classical composition ( arrange ) : — 
 .. » BEETHOVEN 

 BEETHOVEN 
 M. Haypx 

 THAYER 




 LONDON : EDWIN ASHDOWN , 


 HANOVER SQUARE , W 


 PA 


 


 MULLER , 


 PAPININEW FOREIGN PUBLICATIONS 

 BECKER , A.—LXII . 
 paniment Piano Organ . 
 BEETHOVEN.—Adagio Choral Symphon dy , arrar nge 

 Violin , Harmonium , Piano , Ritter 10 
 — — March Chorus ( Ruins Athens ) , arrang ge Vio n , 
 Violoncello , Organ ( Harmonium ) , Piano vel Sachs 6 
 berthold , ' 1 H.—Fugue d minor Organ ; 2 
 choral : Herr wie du willst , Organ ... 2 
 br : AHMS , J.—Two trio Pianoforte , Violin , ' Violon- 
 llo , arrange Sestets String instrument , 
 Theod Kirchner : — 
 no.1 . op . 4 nae aa ka asa aes ' eee 26 
 » 2 op . 36 € ' ons oe o 24 
 --= ty gt ina flat mir , Org van 3 
 LUZIAU , V.—Tarantelle Brillante Violin , Accom pani 




 subscript — edition 


 MOZART work 


 publish 


 BREITKOPF & HARTEL , LEIPZIG . 


 354 


 GRAND OLD - FASHIONED CANTATA 


 


 CAROLINE HOLLAND 


 GOLDEN LEGEND 


 DRAMATIC CANTATA 


 


 HENRY EDWARD HODSONJQOBERT COCKS & CO . publication 

 BEETHOVEN waltz . 
 book 1 6 , 1 . 6d . net . 
 duet , 2 , net . 
 edit Gro . F. West . 
 NM OZART waltz . 
 4 book 1 6 , t , 6d . 
 edit Geo , F. Wi 
 aaa WALTZES . 
 book 3 , 2 . net . 
 cite Geo . F. West . 
 pay \ ‘ ment , s , stam 
 ew B ferns Foo Street , London , V 
 SOLE LONDON age ns CARPE ? ers CELE ! 
 AMERICAN ORGANS . 
 Li st Dray wing gratis 

 ORIGINAL 
 Compositions Organ 




 PHILIPPI 


 SACRED CANTATA CHURCH oratorio 


 WORDS 


 REV . J. POWELL METCALFE 


 music compose 


 FF , E. GLADSTONE 


 SRATED 


 MODERN ORGAN 


 THOMAS CASSON 


 reduce price , shilling SIXPENCE 


 LEFEBURE - WELY 


 NEW TUN ] 


 P PLOUGH 


 VIIM 


 XUM 


 MUSICAL ti 


 REDUCED price , shilling 


 manual singing 


 use 


 choirtrainer & schoolmaster 


 


 RICHARD MANN 


 NEW revise edition , addition , 


 PI : ANO 


 NOVELLO , EWER CO . 


 PIANOFORTE ALBUMS 


 SHILLING . 


 SHILL 


 1 , composition BACH 


 2 , composition BACH 


 composition BACH . 


 4 . composition HANDEL 


 5 . composition HANDEL 


 6 . composition HANDEL , 


 7 . marche 


 8 marche 


 CHORAL society 


 NARCISSUS echo 


 compose 


 element music . 


 W. SWAN SONNENSCHEIN & CO 


 USIC , good 


 method future 


 jianoforte play high 


 new method " pianoforte 


 ittle difficulty occur 


 ow form train village 


 xongregational singing : 


 hymnary 


 book CHURCH song 


 follow edition ready 


 LONDON : NOVELLO , EWER CO 


 BRISTOL TUNE - BOOK 


 match edition 


 BOOK PSALMS ( bible VERSION ) , 


 ANGLICAN HYMN - BOOK . 


 reduce threepence . 


 * a. MACFARREN - anthem 


 revise enlarge edition 


 NORTH COATES supplemental 


 ANP JESUS SPAKE " unto disciple 


 anthem consecration reopening 


 church 


 surely build THEE house 


 T. TALLIS TRIMNELL . 


 NOVELLO collection 


 - 


 GEORGE C. MARTIN 


 SUB - organist ST . PAUL CATHEDRAL 


 easy offertory sentence 


 intend chiefly parish choir 


 


 GEORGE C. MARTIN 


 XUM 


 XUM 


 , 1883 . 357 


 ANGLICAN CHORAL SERVICE book . 


 USELEY MONK PSALTER 


 OULE collection 527 chant , 57 


 PSALTER , PROPER PSALMS , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- ! } 


 order HOLY communion . 


 HOLY CITY 


 SACRED CANTATA 


 


 ALFRED R. GAUL 


 RUTH 


 SACRED CANTATA 


 TONIC SOL - FA edition , shilling . 


 WIDOW NAIN 


 SACRED CANTATA 


 SOLI voice CHORUS 


 


 CHRISTIAN PILGRIM 


 


 PILGRIM PROGRESS 


 CANTATA 


 


 WILFORD MORGAN 


 O conductor CHORAL society . 


 second serie 


 


 canticle hymn 


 church , 


 point chanting , SET appropriate anglican chant , SINGLE DOUBLE 


 


 response commandment 


 edit 


 REV . SIR F. A. GORE OUSELEY , BART . , M.A 


 


 EDWIN GEORGE MONK 


 HARROW SCHOOL MUSIC 


 JOHN FARMER 


 ORATORIO . — " CHRIST soldier . " 


 " CINDERELLA”—A FAIRY OPERA act . 


 NOVELLO ANTHEM BOOK . = + chorus 


 PRICE 3S. 6D. 


 YUM 


 LE 


 MUSICAL TI 


 DAVIS 


 SONGS 


 2 0 


 - SONGS , S.A.T.B 


 NNNNNNNN 1 


 - SONG 


 GENTLE SPRING 


 following work composer 


 GERMAN HANDEL SOCIETY 


 CHILDREN hour 


 NEW - SONG . 


 ALFRED R. GAUL 


 PRICE 3D 


 t d ) 


 NEW song 


 compose 


 WILLIAM J. YOUNG 


 A. C. MACKENZIE 


 life true motto 


 new effective song 


 360 


 1883 


 new work CHORAL society 


 GRAZIELLA 


 CANTATA 


 music 


 SIR JULIUS BENEDICT 


 CHAPPELL 


 serie 


 popular instruction book 


 VIOLIN 


 FLUTE 


 CORNET 


 ENGLISH CONCERTINA . 


 GERMAN CONCERTINA . 


 PIANOFORTE . 


 CLARINET 


 HAPPE 


 good master 


 HARMONIUM 


 SINGING . 


 HARMONY . 


 GUITAR 


 SAX HORN 


 BANJO 


 PART- SONGS , & C 


 SERAPHINA ANGELICA . 


 DRUM FIFE . 


 TROMBONE . 


 VIOLONCELLO 


 AMERICAN ORGANLL VOCAL LIBRARY 

 arrange PRICE arrange PRICE 
 1 . Dulce domum s. A.T.B. . G.A. Macfarren 1d . 45 . OThou , power(prayer " Mosé " ) Rossini 2d . 
 2 . dead man . s. A.T.B. oe ‘ ie 1d . 46 . Guard onthe Rhine . S.Aa . T.B. A. Macfarren 1d . 
 3 . girl leave . s.a.7.B .... ' 1d . 47 . german fatherland . s. A.T.B. 1d , 
 4 . british Grenadiers . s. A.T.B. _ 1d . | 48 . Lord ene ean $ .A.1.B. G.A. Osborne 2d . 
 5 . long live England future Queen . s.a . tp . Dr. Rimbaule 2d.| 49 . Te DeuminF ... « . » Jackson 2d . 
 6 . task end ( song chorus ) . a.1.b.b .. .. Balfe 4d . | 50 . Te DeuminF ... én ove eos Nares 2d . 
 7 . spake summer day . S.A.T.B. Abt 2d . , 51 . Charity ( La Carita ) . s.s.s .... ie eee ss Rossini 4d . 
 8 . soldier ’ chorus . T.T.B.B. . Gounod 4d . 52 . Cordelia , A.T.T.B. . ess ie ee Osborne 4d . 
 g. Kermesse ( scene « R aust " ) pa ose " 6d . | 53 . Iknow . s. A.7.B Walter Hay 2d . 
 10 . . quit thy bower . S.A.T.B. = ... Brinley Richards 4d . | 54 . Chorus Handmaidens ( " F ridolin " ) A. Randegger 4d . 
 11 , Maidens , - woo . S.S. T.T.B. ww . G. , Macfarren 2d 55 . Offertory Sentences ... « » Edmund Rogers 4d . 
 12 . Faggot - binders ’ Chorus ms ee Gounod 4d./ 56 , Red - Cross Knight Dr. Callcott 2d . 
 13 . Syivan hour ( female voice ) " Joseph Robinson 6d . 57 . Chough Crow | Sir H.R. Bishop 3d . 
 14 . Gipsy chorus . ... SK son Balfe 4d . 58 . " Carnovale " ° Rossini 2d . 
 15 . Ave maria se ose = icemm 1d . 59 . softly fall moonlight Edmund Rogers 4d . 
 16 . Hark ! herald angel sing . S.A.T.B. wie Mendelssohn 1d . 60 , air Himmel ... Henry Leslie 2d . 
 17 . England ( Solo chorus ) . S.A.T.B. Sir J. Benedict 2d . 61 , Offertory Sentences E. Sauerbrey 4d . 
 18 . Shepherd Sabbath day . S.A.T.B. ... J. L. Hatton 2d . 62 , resurrection ; C. Villiers Stanford 6d . 
 19 . thought childhood . s.a . T.B. ... ° Henry Smart 2d . | 63 . Boys , New patriotic Song H. A.J. ron & W.M. Lutz 4d . 
 20 . Spring Return . s. A.7T.B. ... = 2d . 64 . Men Wales ee Richards 2d . 
 21 . Anold Church Song . s. A.T.B. i. 2d . 65 . Dame Durden ... 1d . 
 22 . Sabbath Bells . s.a . T.B. — 2d . 60 . little farm till . - wie " Hook 1d . 
 23 . serenade . S.A.T.B.   .. win 2d.| 67 . simple maiden ... | GA . Macfarren 1d . 
 24 . cold Autumn wind . s. A.T.B. wee ue 2d . | 68 . Fair Hebe ' sia ni " ave pe 1d , 
 25 . Orpheus lute . s.s.s . « . Bennett Gilbert 2d . | 69 . love maiden ' fair ... ie ae 1d . 
 26 lullaby . S.A.A. eva wis = 1d.| 70 . Jovial Man Kent aie oe < 1d . 
 27 . , " native land . s.a.7.B , G.A.Macfarren 1d . ' 71 , Oak andthe Ash . 2 1d . 
 28 . March Men Harlech . S.A.T.B. Dr. Rimbault 2d.| 72 , Heartofoak ... ee " 1d , 
 29 . God save queen . s. A.T.B. és ee " 1d . 73 , come sunset tree . W. A. Philpott 4d . 
 30 . rule , Britannia . s. A.T.B. bs hae 99 1d . 74 . . S.A.T.B. W.F. Banks 2d . 
 31 . retreat . T.7.B.B. soe " = eo » L.de Rille 2d . 75 . pure , lovely innocence iG ll Redi Lahore ) chorus female 
 32 . Lo ! morn break . s.s.s . Cherubini 2d . | voice ... " ‘ . Massenet 4d . 
 33 . Weare spirit , s.s.s .. 5 G.A.Macfarren 4d . | 76 . Love Idyl . s.a.7.8 . ' ee . » R. Terry 2d . 
 34 . Market Chorus ( " Masaniello " ) . S.A.T.B. Auber 4d . 77 . hail tothe wood . a. TT . Di z Yarwood 2d . 
 35- Prayer ( ' * Masaniello " ) , s.a . T.B. " id. | 78 , near town Taunton Fe Thomas J. Dudeney 2d . 
 36 . Water Sprites . s.a . T.B. ose Kiicken 2d . 79 . merry boy sea . A.T.T . Yarwood 2d . 
 37- Eve glitter star . s.a . T.B. Beek oats ie 2d . 81 . sun set o'er swonuasinn ( " Tl Demonio " ) 
 38 . primrose . s. A.T.B .. 2d . A. Rubinstein 3d . 
 39 . odewdrop bright . s.a . T.B. = ae 1d . 82 . hymn Nature sie Beethoven 3d . 
 40 . Sanctus , " Messe Solennelle . " . A.T.u . Rossini 4d . | 83 . Michaelmas Day ( humorous ' part- song , ' . 1 ) ' W. Maynard 4d . 
 41 . kyrie , ancient modern .. J. Gill 2d . 84 . Sporting note ( humorous part- no.2 ) W. Maynard 4d . 
 42 . Sunofmy soul . s.a.7t.b. ee ~. Brinley Richards 2d.| 85 . austrian National hymn ... se Haydn 4d . 
 43 . ’ twas fancy , ocean ' spray . S.A. Tr . B. G. A. Osborne 2d./ 86 . Carol . s.s.c . ... ane aes . " . Joseph Robinson 4d . 
 44 . prayer Sea . s.a . T.b . esa 2 2d . | 87 . bright- hair'd Morn . A.7.T.B. T ‘ heodor L. Clemens 3d 

 CHAPPELL & CO 




 LONDON 


 50 , NEW BOND STREET , W 


 2 - 8 


 MEPS 


 YUM