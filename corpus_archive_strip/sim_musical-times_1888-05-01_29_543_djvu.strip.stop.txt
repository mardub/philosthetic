


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 PUBLISHED MONTH 


 NOVELLO , EWER & COVS CHESTER 


 


 EASI E7 TH STREE PATRONS : 


 54 , GREAT MARLBOROUGH BUGH STREET , LONDON , W. } 


 EXAMINAT IONS PRACTICAL 


 LIST WORKS PERFORMED 


 THEORETICAL MUSIC . - ORTTITD CAPLET LL Db . " Feahaawel Music , Edinburgh Universi 

 y : | PSALM CXXXVIL . ) Soli , Chorusand Orchestra . 
 ve — — Presi oe , SympHonic Cantata } OLIVER KING . 
 Mu . H. Cumainas , Esq . , Professor , k. A.M. ; Hon . Treasurer , Roya ’ sed expressiv » Festiv : 
 Society Musicians ; Conductor Sacred Harmonic Society . Composed expressly Festival . 
 College holds periodical Examinations Pianoforte SYMPHONY C MINOR ( Beethoven ) . 
 Playing , Singing , Theory Music , grants Certificates REQUIEM ( Verdi ) . 
 successful Candidates , irrespective age . | ~ ’ . 
 Examination place July , names | Fripay Mornino , 11.30 
 entered . Forms Entry Secretary . | = _ ee ° . 
 Local Representatives required Vacancies Towns near SYMPHONY B MINOR ( Schubert ) . 
 London ; parts country . Apply Secretary . ENGEDI ( Beethoven ) . 
 iiss PIV NT SSVe | LOBGESANG ( Mendelssohn ) . 
 MR . BARTON McGUCKIN , | compar Evexixe._THE REDEMPTION ( Gounod ) 
 ANNOUNCING RETURN AMERICA JUNE , Fripay Eventnc.—THE REDE } TION ( Gounod 

 Begs al communications respecting Concert sal Oratorio En- | et ee 
 gagements addressed sole Agent , Mr. Alfred Moul , MUSIC HALL 




 58 THEPRUFESSIONAL | ly NOTICES 

 MRS . BARTER ( Soprano ) 
 ( Pupil W. H. Cummings , Esq . ) . 
 Oratorios , Classical Ballad Concerts , Homes , & c. , addre 
 Westbury Road , Wood Green , N. ; , Messrs. Novello , Ev 
 Co. , 1 , Berners Street , W. 
 MISS MARIAN BATES ( Soprano ) . 
 Concerts , Oratorios , & c. , address , Mr. Brook Samps 
 Beethoven House , Northam pton 

 MISS ADA BECK ( Soprano ) . 
 Oratorios , “ s assical Ballad Concerts , Homes , & c. 
 Address , Brompton Cc ttage , New Bron ipton , Kent 




 MISS EFIMISS ELLIOT RICHARDS ( Soprano 

 Oratorios , Concerts , & c. , address , 9 , Oakley Streei , Northampton 
 + O 5 , Ato C , 
 MISS FANNIE SELLERS ( Soprano ) . 
 Oratorios , Classical Ballad Conc erts , Cra 3 Ci tt 
 MADAME CLARA WEST ( Sop rano ) , 
 MISS LOTTIE WEST ( Contraito ) , 
 Beethoven Villa , King Edward Road , Hackney , 
 MRS . LINDLEY WHIT 
 Yor Concerts Homes , address , 12 , Union Rd . , 
 MISS JUL IE ALBU ( Contr alto ) 
 ( Puy pil late Madame Jenny Lind - Gold midt ) 
 Bit , Concerts , Homes , 4 , Elgin Aven 

 MISS BERTHA B ion 

 rove , 

 MISS CONSTANCE POOCK ( Cc ontralto ) & Pianist 
 A.M. Honours Sin g 
 ads , ‘ ‘ Beethoven ” ( md c 
 Lecture ) , ‘ oe ul Classica il Popul ar Vocal Pianoforte 
 Selections , & c. , 8 , Bucking Palace Road , S.W. 
 “ Miss Poock sang songs , ‘ Wiil come ’ ( § Sullivan ) ‘ 
 Castle Gate ’ ( Jude ) , good style ; rapturously ap plauded 
 encored . ’’—Vide Press.—St . Andrew Hail , Norwich , Feb. 11 , 15 

 MISS MARY WILLIS ( Contralto Mezzo- Soprano ) 
 Pupil late Madame Sainton - Dolby , Assistant Professor 
 Academy ; Professor Hy de Park Academy Music ) , 
 Oratcrios , asm & c. , address , 9 , ne Terrace , 
 Camden Road , N.W 




 MR . GREGORY HAST 


 MADAME GREGORY HAST 


 MR . R. ARTHUR M. SHORE , R.C.M. 


 COLLET 


 MR . C. 


 LESLIE 


 ISS 


 HN 


 OXON 


 ... LAWRENCE FRYER 


 A.C.0 . : 1¢ 


 260 


 HARMONY , V 


 LTO WANTED , 


 S.W 


 R. CHARLES FRY RECITALS : MER- 


 


 CHORAL SCHOLARSHIPS VAC ANT 


 XUM 


 XUM 


 ENGAGE 


 BEIND PIANOFORTE TUNER . — A. M 


 TION 


 VHAMBER ORG . 


 HAMBE R PIPE 


 MESSRS . BEARE 


 34 , RATHBONE PLACE , LONDON , W. , 


 THR 


 OLD VIOLINS , TENORS , CELLOS , 


 DOUBLE - BASSES . 


 NEW QUEEN MODEL 


 PIANO AMERICAN ORGAN CHAIRS 


 _ MERCHANTS , SHIPPERS , 1 RADE SU PPLIED 


 PEDAL ORGAN HARMONIUMS . 


 RNENTERON VIOLIN 


 STR INGS 


 “ SALI E , SET 


 OM JSICAL STUDENTS “ PROFESSION 


 EW PATENT TOUCH REGULATOR 


 INVENTED E. A. SYDENHAM 


 EXT RACTS F ROM TESTIMONIALS . 


 ADDRESS — E. A. SYDENHAM , SCARBOROUGH 


 COLLIN -MEZIN PARIS . 


 JAMES CONACHER SONS 


 ORGAN BUILDER 


 BATH BUILDINGS , HUDDERSFIELD 


 CCUR . ATE 


 263 


 2 , BERNERS STREET , LONDON , W 


 CONCERT - GIVERS ENTREPRENEURS 


 GENERALLY . 


 FRE CLERGY 


 CHOIR- BOYS 


 ORCHESTRAS CHOIRS . 


 OXFORD CONCERT AGENTS , 


 3 ° » REGENT OXFORD CIR 


 STREET 


 MUSICAL AGENCY 


 LONDON 


 MU SICAL AGENCY 


 SPECIAL GUINEA CONCERT , 


 EMS OPERAS ” CONCERT COM- 


 P. CON ACHER & CO 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD 


 ESTAG LISHE D 1854 . 


 SPECIFICATIONS FREE APPLICATION . 


 D'ALMAINE 


 LSON CO .. 


 RGAN BUILDERS 


 PALACE YARD , WORCESTER . 


 ( ESTABLISHED 15831 . ) 


 264 


 TRINITY COLLEGE , LONDON 


 GRANTING CERTIFICATES DIPLOMAS 


 TRINITY TERM : APRIL 30 — JULY 21 


 READY 


 NEW COVENANT 


 ODE { 


 GLASGOW INTERNATIONAL EXHIBITION , | 


 1888 , 


 | 


 ROBERT BUCHANAN . 


 SET MUSIC 


 A. C. MACKENZIE . 


 PSALMS 


 BIBLE VERSION 


 POINTED CHANTING 


 REV . DR . TROUTBECK 


 MUSICAL ASSOCIATION 


 FOURTEENTH SESSION , 1887 - 88 . 


 PAPER READ 


 “ POINTS INTEREST CONNECTED 


 ENGLISH SCHOOL 


 SIXTEENTH CENTURY . ” 


 SERVICES 


 SACRED SONG 


 SDITED 


 REV . DR . TROUTBECK . 


 PRICE SIXPENCE 


 GIPSY SONGS 


 KAREL BENDL . 


 FAIRIES ’ ISLE 


 CANTATA FEMALE VOICES 


 WORDS WRITTEN 


 EDWARD OXENFORD 


 MUSIC COMPOSED 


 BATTISON HAYNES 


 


 265 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR 


 1 , 1888 


 MUSIC 


 266 


 HITCHIN MAYERS ’ SONG 


 


 1888 , 267 


 MINOR NOTATION TONIC 


 SOL - FA SYSTEM . 


 ES 


 268 


 QD 


 XUM 


 269 


 PO 


 GREAT COMPOSERS 


 270 


 MUSICAL TIMES 


 LOS 


 272 


 MATERIAL MUSIC 


 273 


 274 MUSICAL TI 


 GLASGOW EXHIBITION ODE 


 276 


 277 


 278 


 ROYAL ALBERT HALL CHORAL SOCIETY 


 PHILHARMONIC SOCIETY 


 VIIMreally excuse decline attach importance 
 desires distinctions . preference regard 
 music , broadest relation 
 scenes inspired ; consider burdened 
 arbitrary significances , , fundamentally , 
 . 
 Allegro movement orthodox symphonic form , some- 
 rhapsodical , expected poetic 
 basis , noisy suggest tempest 
 raged . music highly coloured , respect 
 manifests Mr. Widor familiarity resources 
 art , discover great merit 
 properly belongs musical builder deco- 
 rator . Evidence imaginative genius certainly 
 wanting , effect intelligent listener 
 hollow - sounding . Andante , , come 
 music properly called . composer 
 means , gives predominating strings 
 passages charm , alike themes 
 attendant harmonies , chiefly real parts . 
 final movement , fantastic dance , judged 
 high standard companions , suffices 
 distinctive melodies help skilful orchestration 
 sustain large interest . , 
 “ Walpurgis Night ’’ disappointment , failed 
 meet expectations formed composer ability 
 highest walk art . Mr. Widor conducted 
 clearness force , applauded considerable 
 heartiness 

 late Sir G. A. Macfarren Overture ‘ ‘ Romeo 
 Juliet ’ novelty , composed years 
 ago . reason Sir George withdrew 
 work youth public notice , despite fact 
 popular known , obtained 
 close connection Shakespeare play accepted 
 prelude . think composer mistaken 
 acting , work singular clearness , force , 
 suggestiveness . picturesque withal , 
 fairly expresses , far possible music , certain 
 scenes personages drama . 
 allowed drop sight mind . items 
 programme Concert Beethoven Seventh 
 Symphony Overture “ Der Freischitz , ” 
 admirably played Mr. Cowen direction . 
 vocalist , Miss Hilda Wilson , indisposed 
 justice Prout scena * * Judith 

 CRYSTAL PALACE 

 Ir probably fallen lot Sydenham 
 audience hear inadequate rendering Beethoven 
 Pianoforte Concerto E flat ( . 5 ) given 
 day March . Miss Martha Remmert , Court 
 pianist Weimar pupil Liszt , un- 
 deniable qualities executant , notably excellent 
 shake , powerful wrist , absolute freedom 
 nervousness . set want 
 dignity conception , refinement phrasing , 
 added infelicitous sense rhythm , 
 irrepressible tendency exaggerate , produce results little 
 short disastrous . orchestra , magnificent 
 rendering ‘ Leonora ’’ Overture ( . 3 ) , 
 demoralised soloist Concerto . 
 inefficiency natural feeling 
 reluctance bizarre interpretation 
 masterpiece . Miss Remmert far element 
 later Liszt Hungarian Fantasia , revelling 
 called ex ungue scale passages , 
 piece accent occasionally fault . Spohr 
 symphonic masterpiece , ‘ ‘ Die Weihe der Tone , ” 
 heard Concerts years , formed 
 central number programme performed con 
 amore band , emancipated co - opera- 
 tion pianoforte best 
 afternoon . vocalist Miss Alice Gomes , 
 unaffected rendering Weber Aria ‘ “ ‘ O Fatima ” ( Abu 
 Hassan ) appreciated audience 
 lead repetition . mere charm voice 
 simplicity style effect little songs Mendels- 
 sohn setting Goethe Sonnet “ Die Liebende 

 279 




 280 


 MR . MANNS BENEFIT CONCERT 


 HERKOMER STAGE - PLAY 


 XUM 


 281 


 BATH PHILHARMONIC SOCIETY . OTTO HEGNER RECITALS 

 notice wonderfully gifted young per- 
 given Pianoforte Recitals 
 Princes ’ Hall — . March 28 11th 
 21st ult . occasion overflowing 
 audience , programmes mainly formed 
 pieces , repeated . 
 regarded error judgment , 
 restriction little pianist répertoire . 
 able play small number compositions , 
 rendering marked mechanical 
 exactitude , higher qualities . far 
 case , Hegner performances chiefly remarkable 
 intellectuality , correct 
 , spirituality . trips platform 
 seats keyboard , dis- 
 tinguish lads age . 
 instant begins play transformed 
 . Perfect absorption work moment , 
 extraordinarily keen perception composer meaning , 
 determination impress meaning audience , 
 highest qualities pianist art ; 
 Otto Hegner possesses degree little short 
 miraculous , taking age consideration . lies 
 undoubted superiority Josef Hofmann ; 
 evinces highest form talent , Hegner possesses 
 , mysterious gift term 
 genius , acquired un- 
 remitting industry perseverance . 
 remarkable performances boy recent Recitals 
 numbered Mozart rarely heard Variations 
 G Air Gluck , ‘ * ‘ Unser dummer Poébel meint ” ; 
 Beethoven Sonata B flat ( Op . 22 ) , Mendelssohn Rondo 
 Capriccioso , Chopin Etude flat , Liszt Trans- 
 cription Wagner Spinnerlied . 3oth ult . 
 announced Orchestral Concert St 
 James Hall , interpret Beethoven 
 Concerto C ( . 1 ) Mendelssohn Capriccio B 
 minor ( Op . 22 ) . occurred , course , late 
 notice present issue 

 ow 




 MISS WINIFRED ROBINSON CONCERT . 


 282MISS MASTER BAUER CONCERT 

 Ir stated , credit clever Bauer 
 family youthful performers , appearances 
 public desire advantage 
 present craze prodigies . contrary , 
 field , having given series Concerts 
 Portman Rooms winter , months 
 star Josef Hofmann rose horizon . 
 time young people steadily improved , 
 proved Musical Evening Princes ’ 
 Hall , 17th ult . Miss Ethel Master 
 Harold Bauer claim rank juvenile “ prodigy . ” 
 brother sister simply talented students 
 cultivated natural gifts assiduity 
 attained degree proficiency unusual 
 age . Master Bauer efficient pianist , 
 ability chiefly displays violin , 
 occasion showed remarkable feeling expression 
 favourite Adagio Spohr Ninth Concerto , equal 
 technical skill “ Air Varié , ” Vieuxtemps . Miss 
 Bauer rendering Schumann Carnaval 
 spirit , times flurried , false 
 notes result . heard greater advantage 
 Grieg Ballade G minor ( Op . 24 ) . programme 
 likewise contained Bach Trio C , violins 
 pianoforte , Miss Winifred Bauer , younger sister , 
 took , Beethoven ‘ “ Kreutzer ” Sonata . Miss 
 Marguerite Hall sang Schubert “ ‘ Die junge Nonne ” 
 Kjerulf Lieder 

 STROLLING PLAYERS 




 ENGLISH MUSIC AMERICA 


 XUM 


 YUM 


 283 


 MUSICAL ASSOCIATION 


 284 


 TRINITY COLLEGE 


 MUSIC BIRMINGHAM . 


 ( CORRESPONDENTThe Midland Musical Society performance Gounod 
 Sacred Trilogy ‘ * ‘ Redemption , ” Good Friday , 
 fully appreciated , long time announced 
 commencement Concert , Town Hall 
 crammed , hundreds people 
 reluctantly turned away doors available 
 space auditorium utilised utmost . 
 performance extremely creditable 
 concerned . choruses sung 
 vigour , crispness , intelligent appreciation com- 
 poser meaning ; band accompaniments , 
 body entirely consisting amateur performers , 
 rendered creditably . Miss Clara Surgey , Miss Florence 
 Bourne , Mr. A. Percy Taunton , Mr. George Harris , Mr. 
 Walter Vernon rendered good service solos . 
 organ accompaniment Mr. W. Astley Langston 
 distinctive feature Concert , Mr. H. M. 
 Stevenson conducted energy , ability , sound 
 discretion 

 Concert Musical Guild , 
 12th ult . , principal items Beethoven String 
 Quartet ( Op . 59 , . 1 ) Rubinstein Trio B flat 
 ( Op . 52 ) , piano strings . Quartet playing 
 Messrs. T. M. T. R. Abbott E. W. A. J. 
 Priestley noteworthy perfection ensemble 
 theevident care work rehearsed . 
 final Allegro , based Russian melody , com- 
 pliment presumably Count Rasowmowski , 
 work dedicated , rendered rare spirit dash . 
 Rubinstein Trio , picturesque striking , 
 absolutely great , work , Dr. Winn masterly 
 playing pianoforte contributed largely 
 effect performance , artist greatly 
 delighted audience finished rendering 
 Rubinstein Barcarole ( Op . 50 ) Chopin Etude 
 C minor ( Op . 25 ) . Reinecke fine Violin Suite ( Op . 
 153 ) found competent exponent Mr. T. M. Abbott , 
 brilliant execution Finale evoked great 
 enthusiasm , Mr. A. J. Priestley acquitted 
 creditably expressive Romance ( Op . 5 ) Belgian 
 violoncellist , Adolphe Fischer , Tarantella Lachner . 
 Miss Marie - Louise Rallo , possesses pure cultured 
 soprano voice , sings taste expression , invested 
 Haydn Canzonet ‘ Recollection ” pathos , 
 wanting subdued passion appropriate 
 Gounod amorous Berceuse ‘ Quand tu chantes , ’ ” ’ 
 effect little enhanced Mr. Abbott 
 violin obbligato 

 - called open Rehearsal Amateur Harmonic 
 Association , took place 17th ult . , 
 formal public Concert considerable musical interest . 




 XUM 


 MUSIC EDINBURGH . 


 ( CORRESPONDENT286 MUSICAL TIMES.—May 1 , 1888 

 Mr. Daly , Mr. Carl Hamilton Conductor . Men- 
 delssohn “ Italian ’ ? Symphony ; Overtures Mozart 
 “ « Zauberfléte , ” Mendelssohn ‘ ‘ Ruy Blas , ” Weber 
 “ Oberon ” ; Saltarello A. C. Mackenzie 
 “ Colomba , ” Beethoven Concerto , piano- 
 forte orchestra , Mr. Della Torre pianist , 
 performed . Mr. Glencorse sang Schubert ‘ Wanderer ” 
 ‘ Thine heart , ” received - earned 
 recall , great success Concert , artistic 
 point view , augurs Mr. Carl Hamilton scheme 
 giving popular orchestral Concerts season 

 Heckmann Quartet performed , evening 
 2oth ult . , Haydn Quartet E flat , Schumann 
 minor , selections single movements Quartets 
 Cherubini , Bazzini , Sgambati , Svendsen . Mr. Albert 
 Bach vocalist . following afternoon 
 performers gave Beethoven Concert , consisting 
 Rassoumowski Quartets ( C major , Op . 59 ) , 
 F minor ( Op . 95 ) , minor ( Op . 132 ) , solo 
 Herr Heckmann Adagio Violin Concerto . 
 Edinburgh Society Musicians held meeting 
 evening . Mr. T. H. Collinson , Mus . Bac . , read 
 paper * Musical Education . ” followed dis- 
 cussions points given lecture . Mr. 
 Edmunds , President Society , occupied chair 

 Sunday evening , 22nd ult . , Rossini ‘ * Stabat 
 Mater ” performed Roman Catholic Church 
 Sacred Heart , principally members Carl Rosa 
 ra . Company , announced week perform- 
 ance Lyceum 




 MUSIC GLASGOW . 


 ( CORRESPONDENT 


 MUSIC LIVERPOOL . 


 ( CORRESPONDENT 


 FRIEND , ALICE PENSO 


 - SONG . 


 LLOUINS 


 QE 


 XUM 


 YUIM 


 - SONG 


 XUM 


 XUM 


 291 


 MUSIC MANCHESTER . 


 ( CORRESPONDENT 


 MUSIC MONMOUTHSHIRE 


 SOUTH WALES . 


 MUSIC PARIS . speak theatrical world . 
 hopes founded Salvayre ‘ ‘ La dame de 
 Monsoreau , ” produced Opéra , fulfilled 
 ; work failed public 
 interest musicians , short , sickly life 
 died away entirely forgotten . Mr. Lalo 
 ‘ Le Roi d’Ys ” daily expected come 
 Opéra Comique , , agreeably approved Parisian 
 custom , appeared , troupe said 
 busy rehearsals , new opera 
 produced time . Lalo 
 undoubtedly best French composers , 
 times compositions somewhat dry , nobleness 
 conception musicianship second , there- 
 fore entertain great expectations premicre — 
 , inadequate interpretation stand 
 way . performed Brussels , reckon Mr. 
 Godard new opera “ Jocelyn ” recent French per- 
 formances . Criticism , especially periodicals fighting 
 cause musical drama versus opera , 
 bitterly composer , public Brussels 
 sided young French musician , “ Jocelyn ” 
 scored decided , opinion , deserved success . 
 far opening ‘ Jocelyn ” Paris . Mr. 
 Paravey , Opéra Comique , commissioned Godard 
 write new opera , ‘ “ * Ruy Blas , ” produced 
 November , date 
 “ Jocelyn ” rehearsal . firmly believe 
 Parisian public fail appreciate beauty 
 score , , opinion , remarkable 
 late written French foreign 
 composer 

 Apart theatrical performances , Societies 
 gave periodically orchestral Concerts — ihat , 
 Conservatoire Concerts , conductorship Mr. 
 Garcin ; Lamoureux Concerts , conductorship 
 Lamoureux ; Colonne Concerts , Concerts 
 Chateau d’Eau , respectively conducted Colonne 
 Montardon . series , Mr. Lamoureux 
 takes lead ; better performances seldom 
 heard . justice style modern 
 classic composition . programmes chiefly 
 devoted works Wagner , vocal _ instru- 
 mental , opportunity afforded hearing 
 faultless interpretations best inspirations 
 great master mitigates absurdity Parisian theatres 
 allowed produce popular 
 universally accepted operas Wagner . Concerts 
 Conservatoire , known , 
 classical character . Beethoven forms basis , 
 novelties introduced caution amounting 
 panic . Mr. Gargin said skilled musician , 
 set conductor years ago , 
 , , lacks decision steadiness 
 long practice naturally 
 gifted men . Mr. Colonne Concerts , 
 orthodox Conservatoire , accurate 
 Lamoureux , decidedly popular , national , 
 showy character . Modern French music , interpreted 
 familiar artists , isserved weekly enthusiastic audiences , 
 apparently pleasure skill displayed 
 soloists , singers players , 
 exhibit taste intrinsic merits selections . 
 , , series Concerts 
 devoted works Tschaikowsky , 
 , artistic result ad- 
 mirers Russian Maéstro wished , 
 performances fail adequate idea 
 worth prominent modern 
 composers . Mr. Montardon 
 ; good work , , process time , 
 performances acquire greater perfection 
 title ranked , patronised 
 paying public , older institutions 

 Good , regular Concerts chamber music constitute 




 XUM 


 YUM 


 293 


 MUSIC WEST . 


 ( CORRESPONDENTConcerts , Salisbury Cathedral Choir took 
 , given Winterbourne , Wilts , 5th , 
 Breamore , Hants , 13th 

 Cheltenham Quartet Society , series Classi- 
 cal Concerts developing taste 
 high class music place , gave Concert 
 season 11th ult . , Rotunda . 
 Quartets performed — Beethoven B flat Haydn 
 inD minor . Schumann Quintet included 
 programme , Miss Olga Néruda exceptionally brilliant 
 performance pianoforte evoking greatest 
 enthusiasm 

 Cirencester Choral Society gave annual Con- 
 cert Corn Hall , 12th ult . , 
 leadership able instructor Conductor , Mr. 
 Edward Brind . works presented Dr. Stainer 
 Cantata * “ * Daughter Jairus , ” ” Mendelssohn Motett 
 ‘ * Hear prayer , ” Spohr Cantata ‘ “ ‘ God , Thou art 
 great ” ? ( works new Cirencester audiences ) , 
 concluding selections Haydn masterpiece 
 ‘ Creation . principal vocalists engaged Miss 
 Kate Fusselle , Madame de Lisle , Mr. Albert G. Bailey , 
 Mr. Thomas Woodward . band Mr. Wood- 
 ward able experienced leadership , included 
 local amateur instrumentalists 




 OBITUARY 


 XUM 


 YVWIIM 


 295Ar Concert West London Male Voice 
 Hall , 14th ult . , 
 Conductor , Mr. Albert Reakes , presented hand 
 ivory silver bdton suitably inscribed . pro- 
 gramme selected , novelty part- 
 song “ King lullaby , ” Ernest Lake ( words 
 King Charles [ . ) , encored , 
 contributions Messrs. Bryant Bantock Pierpoint . 
 Mr. Ernest Lake presided pianoforte . 
 Society gave somewhat similar programme People 
 Palace , 18th ult . , large demon- 
 strative audience ; soloists Miss Hallam , 
 Mr. Charles Chilley , Mr. Albert Reakes . Mr. Ernest 
 Lake accompanied , Mr. Albert Reakes 
 conducted usual care success 

 Mr. G. Aucustus Hotes , Organist St. George , 
 | Camberwell , gave Organ Recital church 
 15th ult . ‘ programme consisted compositions 
 Hesse , Sullivan , Beethoven , Elvey , Silas . Smart , 
 given excellent style . 
 vocalists Miss L ydia Davis , R.A.M. , Mr. Frank 
 Swinford . ‘ eighth season 
 series . St. Mark , Walworth , Mr. Holmes gave 
 Organ Recital 11th ult . , programme 
 comprised—-Festal March ( Smart ) , Fantasia Theme 
 Handel ( Guilmant ) , Colonial Indian March 
 ( Holmes ) , Andante Violin Concerto ( Mendels- 
 sohn ) . Miss Marion Holmes Mr. C. Constanduros 
 sang vocal pieces . offertory 
 ' behalf Organ Renovation Fund 

 hei 




 296A Concert given St. James Hall , 17th ult . , 
 aid funds Postman Rest Convalescent 
 Home , Brighton . following ladies gentlemen 
 kindly volunteered services : Madame Antoinette 
 Sterling , Madame Minnie Gwynne , Miss Bertha Moore , 
 Miss Eleanor Rees , Madame Berta Foresta , Madame Evans 
 Warwick , Mr. F. Barrington Foote , Mr. Dalgety Hender- 
 son , Mr. Lucas Williams , Mr. H. J. Russell ; pianoforte , Mr. 
 Kuhe ; violin , Miss Clara Titterton Mr. T. Adamoroski ; 
 violoncello , Mr. Leo Stern . Marquis de Leuville 
 recited original poem . Mr. F. R. Kinkee acted 
 accompanist , band telegraph boys played 
 operatic selections 

 12th ult . Mr. J. H. E. Ashworth , Organist 
 St. Margaret , Wandsworth Common , gave 
 Popular Concert St. Andrew Hall , Balham , 
 directed creditable performance Beethoven 
 C minor Symphony Schubert ‘ ‘ Rosamunde ” Over- 
 ture . orchestral parts represented 
 grand pianos large American organ . Mr. Ash 

 worth , prospectus , deprecated comparison 
 orchestral performance , sought , 
 means disposal , ‘ familiar 
 finest instrumental works great musicians . ” ” Madame 
 Ashworth , Madame Rich , Mr. Ager Grover contributed 
 vocal pieces 




 XUM 


 WLM 


 MUSICAL TI 


 REVIEWS 


 XUM 


 YUM 


 299 


 300 


 MUSICAL TIMESNovello , Ewer Co 

 Tuere degree finish elegance pieces 
 recommend strongly pianists refined 
 tastes . . 1 , , gavotte , gavotte 
 form regarded exhausted , Mr. Porter 
 managed impart freshness example , 
 chromatic opening principal subject arresting 
 attention . - appearance bars trio 
 close Beethovenish cfiect . Nos . 2 
 3 Impromptus Bb minor E fiat respectively . 
 lively little piece 6 - 8 time , 
 somewhat rustic flavour ; sedate 
 character song words . 
 pieces pleasing simple , 
 preference gavotte 

 Children Sliding ; 
 Snow - white Doe . Humorous - Songs 




 5D 


 XUM 


 VIIM 


 301 


 302 


 FOREIGN NOTESA “ Stabat Mater , ” Signor Bernardo Bellini , recently 
 performed Naples , met enthusiastic recep- 
 tion , said remarkable work . composer , 
 way related celebrated author 
 ‘ * Norma ” “ La Sonnambula , ” professor 
 Naples Conservatoire 

 fine spacious Concert Hall , capable accommo 
 dating 1,800 people , inaugurated , r1th ult . , 
 Amsterdam , festival programme including per- 
 formance Haydn ‘ Seasons , ” 
 ‘ “ * Hallelujah ” Chorus ‘ Messiah , ” 
 Beethoven Ninth Symphony 

 new music journal , entitled Orpheus , 
 started Amsterdam . Holland peculiarly deficient 
 literature description , exception 
 minor publications devoted special branch 
 profession , country hitherto possessed 
 journal devoted art generally — viz . , Cecilia , pub- 
 lished Hague , editorship M. Nicolai 




 XUM 


 393CORRESPONDENCE 

 MISPRINTS BEETHOVEN SONATAS . 
 EDITOR ‘ ‘ MUSICAL TIMES 

 S1r,—In communication Neue Berliner Musik- 
 Zeitung time , Herr Ludwig Bussler draws 
 attention misprints Beethoven Sonatas 
 appear hitherto escaped notice 

 movement Sonata B flat major 
 ( Op . 106 ) , middle second section , occurs 




 GLEES MADRIGALS . 


 EDITOR ‘ MUSICAL TIMES 


 MUSIC AMERICA 


 EDITOR “ * MUSICAL TIMES 


 304 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWS 


 XUM 


 MUSICAL TIMESFaversHAM.—Under auspices Faversham Institute 
 Singing Instrumental Class , Mr. R. Pearson Conductor , 
 performance Handel Messiak given Lecture Hall 
 4th ult . choruses went exceedingly . solos 
 entrusted Miss Ada Beck , Miss Josephine Cravino , Mr. C. Kenning- 
 ham , Mr. R. Rhodes 

 Fo _ kestTone.—An Organ Recital given aid expenses 
 incurred repairing improving ‘ organ , Mr. Alfred 
 Oake , 4th ult , Pieces Handel , Archer , Mendelssohn , 
 S. Bennett , Lemmens , Clark , Stainer , Cowen , Lemaigre formed 
 programme . Kev . H. A. Wansbrough , M.A. , 
 solo vocalist . — — Organ Recital given ‘ St. Michael , 
 Mr. J. H. Holloway , r6th ult . pieces played selected 
 works Bach , Mendelssohn , Bennett , Beethoven , 

 Wilson conducted abili ty , ‘ des serves 




 LEAMINGTON 


 SC 


 _ M MIDENHEAD 


 XUM 


 5 


 WARMINSTER . 


 MONTH 


 ARNELL , FRANCIS DRAKE . — 


 N ' OVE ‘ LLO ’ S PUBLIC : ATIONS TONIC 


 TEW , REVISED , ENLARGED EDITION 


 RUDALL , CARTE COS 


 INDESTRUCTIBLE EBONITE 


 CLARINETS , FLUTES , OBOES , 


 BASSOONS , & C 


 PRICE LISTS APPLICATION MANUFACTORY 


 RUDALL , CARTE CO . , 


 23 , BERNERS STREET , OXFORD STREET , LONDON , W. 


 M I4 


 XUM 


 XUM 


 ANTHEMS 


 WHITSUNTIDE 


 TRINITYT IDE 


 15 


 TRINITY SUNDAY . 


 HAIL ! ADORED TRINITY . 


 WHITSUNTIDE ANTHEM . 


 SUDDENLY 


 GREATLY REJOICE LORD . 


 CAME 


 OND THOUSAND.—ON TUNING ORGAN : 


 SIR GEORGE MACFARREN ADDRESSES 


 GEORGE 


 EPITOME LAW PRACTICE 


 CONNECTED WIT 


 PATENTS INVE NTIONS 


 1955 


 VOR MALE VOICE CHOIR.—THE NATIONAL 


 RICHARD WAGNER WORKS 


 SUBSCRIPTION EDITION 


 1888 


 SACRED COR POSITIONS 


 ERNEST C. WINCHESTER 


 SHEPHERD . TINLEY 


 MORNI 


 NG 


 HOLY CO } IMU N ION . 


 EVENING SERV ICE 


 MISCELL ANEOUS 


 H.R.H. DUCHESS ALBANY 


 CROWNING 


 ERNEST C. “ WINCHESTE K 


 - QUEEN 


 DON 


 CHURCH USE , 


 GREAT [ LORD 


 , EASY , FESTIVAL ANTHEM 


 AGNIFICAT 


 HOSPITAL SUD ‘ DAY ANTHEMS 


 VOICE S ORGAN , 


 CONSIDERETH 


 POOR NEEDY . 


 LORD ISFULL COMPASSION . 


 LORD RIGHTEOUS 


 WAYS 


 DAVIS 


 GABRIEL 


 S ’ DEALE . ) 


 - SONGS , S.A , T.B 


 XUM 


 SEEN 


 LESSED LORD G OD ISRAEL . 


 DOMINO 


 THRE E PE NCE E ACH 


 REDUCED 


 A. MACFARREN 


 HURCH MUSIC . 


 . PRAISED TORD " DA 


 ILY 


 » 5 © BEGINNING TH 


 6.1 THANKS . 


 » 7 THY WORKS PRAISE 


 E WORD 


 THEE , O LOK 


 


 | - DAY 


 1888 . 311 


 STAFF NOTATION . 1311 THOUSAND 


 RUTH 


 ASTORAL CANTATA 


 PERFORMED INGHAM 


 D 


 ARRANGE 


 1 PIANOFORTE 


 ROSE 


 HYMN PRAISE—3 : 6 


 GOD , THOU ART GREAT — S 


 CALVARY 


 ST . MARY MAGDALEN—7 . 


 BRISTOL TUNE - BOOK 


 MATCH EDITION 


 BOOK PSALMS ( BIBLE VERSION ) , 


 ANGLICAN HYMN- BOOK 


 HOME HYMN - BOOK 


 SECOND SERIES . 


 ANGL ICAN CHORAL SERVICE BOOK 


 USELEY MONK PSALTER 


 HYMNARY 


 BOOK CHURCH SONG 


 LONDON & NEW YORK : NOVELLO , EWER CO 


 OULE COLL E ‘ CTION 52 


 CHANTS , 57 


 PSALTER , PROPER PSALMS , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI 


 OULE DIRECTORIUM CHORI 


 ORDER HOLY COMMUNION . 


 DR . HILES 


 PAPERS 


 ' STRUMENTATION 


 INSTRUME ! N 


 ( ORGAN ORCHESTRA 


 CONTINUED 


 QUARTERLY MUSICAL REVIEW 


 J. S. BACH ORGAN WORKS 


 ANGLI 


 XUM 


 MUSICAL TIMES 


 1888 


 313 


 PERFORMED BIRMINGHAM FESTIVAL 


 CHORAL SOCIETY 


 JOAN ARC 


 HISTORICAL DRAMATIC CANTATA 


 WORDS 


 FREDERICK ENOCH 


 ALFRED R. GAUL 


 LONDON & NEW YORK : NOVELLO , EWER CO 


 NOVELLO , EWER CO . 


 ALBUMS 


 


 VIOLIN PIANOFORTE 


 MARCHES 


 F. MENDELSSOHN BARTHOLDY 


 TRANSCRIBED 


 BER T HOLD T OU RS 


 THIRTY LODIES 


 BERTHOLD TOURS 


 TRANSCR RIPTIONS 


 ‘ DELSSOHN ’ S “ ELIJAH 


 RTHOL D TOU RS 


 MEN 


 GOUNOD “ MORS ET VITA 


 BERTHOLD TOURS 


 TWELV E “ SKETCHE S 


 DATT ISON H. \YN 


 GARL DANCES 


 CRIEED 


 SIEGFRIED JACOBY 


 VES 


 COMPOSED 


 IPPOLITO RAGGHIANTI 


 PIECES 


 OLIVER KING 


 MORCEAUX DE 


 JO : \CHIM RATT 


 LONDON & NEW YORK : NOVELLO , EWER CO 


 SALON 


 314 


 WORKS 


 JOHN FARMER 


 DIRECTOR 


 GUST 


 


 JOSEPH WILLI 


 AMS , 


 PUBLISHED 


 SPECIALLY ADAPTED HIGH 


 IIOOLS 


 FRANCES E. G. 


 


 TAM 


 1 . .CONT 


 AINS 


 POL . JOHN LACY . 


 V\EW SONG 


 GUI ] ANCE 


 FRANCE S MARY OWEN 


 MUSIC 


 . TILTMAN . 


 HOMEWARD BOUNI 


 HENRY 


 3 0 


 WORKS PUBLISHED 


 24 , BERNERS OTREET , VV 


 & COLT 


 J. CALDI 


 19 


 S.A.T.B. - SONGS 


 


 JOHN GREIG , 


 


 WI 


 XUM 


 


 MUSICAL TIMES 


 NEW EF 


 TS 


 LIERESKIND , J.- 


 MENDELSSOHN 


 SCHMIDT , O.- 


 LONDON & NEW YORK 


 ORIG GIN AL VOLUNTARIES 


 OLIVER KING . 


 NOVELLO , EWER CO 


 REIGN PUBLICATIONS 


 KELLY 


 DR . DFORD 


 IRKS 


 BRA 


 SONG JUBILEE 


 SACRED CANTATA 


 


 STEPHEN 


 RATTON 


 ETRY ; 


 > ( 20 


 MUSIC CLEAN TIDY 


 EQ 


 


 SONS , 7 , WATER STREET , , 


 LONDON 


 SOFT VOLUNTARIES 


 ORGAN 


 COMPOSED 


 REDUCED 


 PRICES 


 OUSELEY MONR 


 POINTED 


 PSALTER 


 LONDON 


 FRANZ SCHUBER 


 NEW EDITION COMPLETE W 


 SERIE IX 


 INTRODUCTION , VARIATIONS , FUGUE 


 “ JERUSALEM GOLDEN . ” 


 VARIATIONS , FUGHETTA , CANON “ AURELIA . ” 


 NE SW TE " § ONG . 


 LOVER 


 HENRY T. TILTMAN . 


 NEW PIANOFORTE MUSIC 


 AIR DE “ BALLE T 


 HENRY T. TIL TMAN . 


 WILLIAMS 


 NEW YORK 


 NOVELLO , EWER CO 


 AVE MARIA 


 ANTHEM 


 GLAD SAID 


 UNTO 


 


 AMEN 


 COMPOSED 


 PIANOFORTE STUDENT DAILY STUDY 


 ORGAN ARRANGEMENT 


 OVERTURE “ DER FREISCHUTZ 


 - SONGS 


 FEMALE VOICES 


 WORDS WRITTEN 


 EDWARD OXENFORD . 


 MUSIC COMPOSED 


 BATTISON HAYNES . 


 VIIA 


 317 


 COMPOSED 


 CHARLES JOSEPH ! ROST , 


 DESCRIPTION , 


 DESCRIPTION 


 THOU VISITEST SAR ’ H 


 JULIUS STOCKHAUSEN 


 TRANSLATED 


 SOPHIE LOWE 


 PRICE SHILLINGS 


 SYMPHON Y F 


 ( IRISH ) 


 € . VILLIERS STANFORD 


 SCHUMANN 


 IFTY - FIVI 


 + CLOTH < < 


 PIANOFORTE WORKS 


 AGNES ZIMMERMANN 


 ALBUM 


 TANOF 


 SEPARATELY 


 CONTENT 


 JRTE PIECES 


 HOME - LIFE ART - LIFE 


 LAA WN 


 LITTLE 


 PLAYERS 


 URED PLAYERS 


 ALBUM LEAV ES 


 PIANOFORTE PIECES 


 ROBE 


 RT ‘ SCH 


 UMANN 


 AGNES ZIMMERMANN 


 CONTENT 


 SCHOOLS 


 “ TY \TINTIOT EDITED 


 OTTO DIENEL USE BACH CHOIR 


 


 7 . F ? 


 5 » GERM VOL 


 “ ‘ ; LONDON & NEW YORK : NOVELLO , EWER CO . 


 ORIGINAL » ‘ . 


 12 . ” ” ” » 5 » 8 2 0 


 XUM 


 


 LC NT 


 PEN 


 


 TT ? 


 MACKE 


 NZ 


 BOOK I. 


 LOOK II . 


 STANCE EET 


 SETTINGS 


 


 HOLY COMMUN 


 AGN 


 D GENERAL 


 SHORT 


 HIAL 


 EI 


 GEORGE 


 DICTUS 


 DOOTL 


 ITED 


 C. MARTIN 


 SONGS 


 319 


 R & COS 


 XS 


 MUSICAL BIOGRAPHY 


 NOVELLO , EWE 


 PRIME 


 JOSEPH BENNETT 


 HECTOR BERLIOZ 


 FREDERIC CHOPIN 


 OSSINI 


 CHERUBIN MEYERBEER 


 | » SHI LLI NG 


 PRO : SI ECTUS 


 , 2 B 


 CO 


 LONDON NEW YORK 


 NOVELLO , EWLR CQ 


 NOVELLO , EWER CO . 


 PIANOFORTE 


 ALBUMS 


 EDITED BERTHOLD TOURS 


 HANDEL . 


 COMPOSERS . 


 H. A. WOLL E NHL . \UPT . 


 O. SCHWEIZER . 


 FRITZ SPINDLER . 


 HERMANN GOETZ . 


 J. RHE INB ERGE UR . 


 BERTHOLD TOURS 


 J. MOSCHELES . 


 HALFDAN KJE RULF . 


 EDVARD GRIEG . 


 NOVELLO , EWER CO . 


 VOCAL ALBUMS 


 KAREL NDL . 


 STERNDALE BENNETT . 


 4 0 


 BERLIOZ 


 BR AHMS 


 J ; W. ELLIOTT . 


 LADY AR THUR HIL L. 


 HOLIDAY SONGS ... 2 6 - 


 = 7 ISZT . 


 | MARL \ NI 


 MOORE . 


 RUBINSTEIN 


 SCHUBERT 


 VOCAL ALBUM . 2 6 4 6 


 LONDON & NEW YORK : NOVELLO , EWER , CO 


 LONDON & NEW YORK : NOVELLO , EWER CO 


 XUM