


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 PUBLISHED MONTH 


 ROYAL CHORAL SOCIETY , 


 ROYAL ALBERT HALL 


 THIRTY - SEASON , 1903 - 4 . 


 PROSPECTUS 


 CONCERT , THURSDAY , NOVEMBER 5 , 8 . 


 MENDELSSOHN “ ELIJAH 


 ROYAL ACADEMY MUSIC , 


 TENTERDEN STREET , W 


 UNIVERSITY DURHAM 


 OCTOBER 1 , 1903 


 BIRMINGHAM MUSICAL FESTIVAL , 


 OUTLINE PERFORMANCES 


 SINGER OVERTUREFripay Mornina : Bach MASS B MINOR 

 Fripay EvENING : Bruckner TE DEUM ; Dvorak SYMPHONIC 
 VARIATIONS ; Brahms ALTO RHAPSODIE ; Beethoven 
 CHORAL SYMPHONY 

 Principal Vecalists : 
 Mesdames ALBANI , AGNES NICHOLLS , MURIEL FOSTER , 
 KIRKBY LUNN , CLARA BUTT . 
 Messieurs BEN DAVIES , WILLIAM GREEN , JOHN COATES , 
 ANDREW BLACK , KENNERLEY RUMFORD , 
 FFRANGCON - DAVIES 




 ROYAL COLLEGE MUSIC . 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. 


 ROYAL COLLEGE ORGANISTS 


 GRAND CHAIR EISTEDDFOD . 


 QUEEN HALL , LONDON , W. 


 GREAT MUSICAL COMPETITIONS . HANDSOME PRIZES . 


 634 


 GUILDHALL SCHOOL MUSIC . 


 BIRMINGHAM MIDLAND INSTITUTE 


 SCHOOL MUSIC 


 SESSION 1902 - 03 


 ROYAL 


 MANCHESTER COLLEGE MUSIC . 


 LONDON COLLEGE CHORISTERS 


 POLYTECHNIC , REGENT STREET , W. 


 


 80 , CAMBRIDGE TERRACE , HYDE PARK , W 


 CITY LONDON COLLEGE 


 WHITE STREET , MOORFIELDS , E.C 


 GUILD CHURCH MUSICIANS 


 SOUTH LONDON SCHOOL | MUSIC , 


 BERMONDSEY SETTLEMENT , 


 DWARD G.CROAGER MUSICAL SOCIETY 


 THIRTEENTH SEASON 


 INTERNATIONAL | 


 MALE - VOICE COMPETITION 


 CARDIFF 


 BOXING DAY , 1903 . 


 ADJUDICATOR 


 PRIZE , 100 GUINEAS , CUP VALUE I0 GUINEAS , CONDUCTOR , & b ) ‘ * King Worlds ’’ ( Dard _ — Curwen Sons , London 

 Tenor Solo — ‘ Adelaide " ’ , : . Beethoven 

 Bass Solo—‘‘I prithee send 1 heart ny Maud Valérie White 




 PROGRAMMES , SIXPENCE 


 SILVANUS DAVIES , 


 CHURCH ORCHESTRAL SOCIETY 


 MANCHESTER SCHOOL MUSIC . 


 LAVENDER HILL EVENING SCHOOL 


 VI 


 ORS 


 SIES 


 MAJESTY VIOLIN MAKERS ) 


 VIOLINS , VIOLAS , VIOLONCELLOS 


 NATIONAL CONSERVATOIRE 


 174 , WARDOUR STREET , W 


 VICTORIA COLLEGE MUSIC , 


 LONDON . 


 INCORPORATED 1891 . 


 2 , BERNERS STREET , OXFORD STREET , LONDON , W 


 BOARD EXAMINATION . 


 EDUCATIONAL DEPARTMENT . 


 AUSTRALIA . 


 1904 . OCTOBER . — 1904 . 


 CRYSTAL PALACE 


 MISS AMY FLETCHER . 


 MISS ESSIE ANDREWS . 


 MR . JOHN PROBERT . 


 MR . EGBERT ROBERTS . 


 HUGO GORLITZ 


 1193 , NEW BOND STREET , LONDON , W 


 KUBELIK SEASON , 1903—1904 . 


 KUBELIK WILHELM BACKHAUS 


 MARTHA CUNNINGHAM ALICE HOLLANDER 


 AMSTERDAM ORCHESTRA 


 BOHEMIAN STRING QUARTETTE 


 KUBELIK AUTUMN TOUR 


 RICHARD STRAUSS FESTIVAL 


 JOHN HARRISON SUCCESS 


 MADAME AMY SHERWIN 


 PROFESSIONAL NOTICES . 


 MISS KATE GREGORY 


 MR . MRS . WALLIS A. WALLIS 


 SEVCIK NEW VIOLIN METHOD . 


 NEW SEVCIK VIOLIN METHOD 


 MISS TERESA BLAMY 


 SOPRANO ) . 


 MISS FLORENCE BOWNESS 


 SOPRANO ) . 


 SOPR . ANO 


 MISS 


 BEATRICE PALLISTER 


 SOPRANO 


 MISS AMY SARGENT 


 A.R.A.M. ( SOPRANO ) , 


 MISS LILIAN TURNBULL 


 SOPRANO ) . 


 MISS GERTRUDE WESLEY 


 MEZZO - SOPRANO HARPIST ) 


 MISS SYDNEY BUSHNELL 


 ( CONTRALTO ) . 


 KATHERINE HAWKINS 


 CONTRALTO ) , L.R.A.M. 


 THOD 


 ANO ) , 


 MISS FLORENCE HOOLE 


 MISS LUCIE JOHNSTONE 


 CONTRALTO ) . 


 MISS ADELAIDE LAMBE 


 ( CONTRALTO ) . 


 MISS MAUD SANTLEY 


 CONTRALTO ) 


 MISS HILDA STRAUSS 


 MR . CLAUDE ANDERSON 


 MR . HENRY BEAUMONT 


 TENOR , ORATORIO ) . 


 MR . WRIGHT | BEAUMONT 


 TENOR ) 


 MR . TOM CHILD 


 YORKSHIRE TENOR ) 


 MR . FRED . FALLAS 


 TENOR 


 ENGLAND SCOTLAND NOTE FOLLOWING 


 BOOKINGS 


 CUMBERLAND MUSICAL FESTIVAL , JAN . 1 , 2 , 3 , 1904 . 


 MR . HENRY FRANCKISS 


 MR . FRANCIS GLYNN 


 TENOR ) , 


 MR . GEORGE PERRINS 


 TENOR ) 


 MR . JOS . REED 


 PRINCIPAL TENOR ) , 


 MR . GWILYM RICHARDS 


 TENOR ) . 


 MR . WILLIAM A. SHEEN 


 MR . JOHN BROWNING 


 MR . OTTLEY CRANSTON 


 ( BARITONE ) 


 CHANGE ADDRESS 


 MR . MONTAGUE BORWELL 


 BARITONE ) . 


 


 MISS WINIFRED MARWOOD 


 MR . JOHN RIDDING 


 BARITONE ) 


 MR . ARTHUR WALENN 


 ( BARITONE 


 MR . ARTHUR SEDGLEY 


 MR . HENRY SUNMAN 


 BASS ) . 


 MR . HERBERT TRACEY 


 MR . CHARLES TREE 


 MARIAN JAY 


 SOLO VIOLINIST ) . 


 MISS ELLEN CHILDS 


 CHROMATIC HARPIST ) . 


 MISS 


 MARGUERITE SWALE 


 VOICE PRODUCTION SINGING 


 MR . DUTTON SOLO BOYS 


 MR . TANN SOLO BOYS 


 EVISION MUSICAL COMPOSITIONS 


 RECENT SUCCESSES . 


 884 ) , 


 INS . 


 US.B , 


 C.0 


 .NDI- 


 “ BRAIN KEYBOARD . ” 


 MACDONALD SMITH SYSTEM 


 TOUCH TECHNIQUE 


 SYSTEM EXPLAINED 


 COMPLETE COURSE LESSONS , CORRESPONDENCE , GUINEAS . 


 TERMS CONDITIONS PERSONAL LESSONS ( CONCERT PLAYERS ) APPLICATION 


 1903 ; OXFORD MUS . BAC . , 1903 ; ASSOCIATED BOARD , 


 A.R.C.M. , 1897 - 1903 , SIXTEEN SUC-| 


 M ADAME ELISE J. HEMERY . VOICE - PRO- 


 640 


 EXAMINATIONS MUSIC TEACHERS 


 PUBLISHED SHORTLY 


 NOVELLO OCTAVO EDITION 


 DRAMATIC CANTATA 


 HECTOR BERLIOZ 


 NGLISH TRANSLATION 


 PAUL ENGLAND 


 LIMITED 


 NEW SHORT OVERSTRUNG GRAND 


 MULTUM “ PAIKVO 


 33 , GREAT PULTENEY STREET , LONDON , W 


 ( LONDON ORGAN SCHOOL ) . F 


 VIOLA 


 BERTHOLD TOURS 


 EDITED RY ALFRED GIBSON . 


 BOSWORTH EDITION 


 OSCAR BERINGER 


 CELEBRATED 


 DAILY TECHNICAL STUDIES 


 SCALE & ARPEGGIO MANUAL 


 PIANOFORTE TUTOR 


 NEW SONATINAS . 


 1 . SONATINA PASTORALE . 


 2 . SONATINA MARTIALE . 


 BOSWORTH CO . , 5 , PRINCES ST . , OXFORD ST . , W. 


 TEUR 


 03 


 641 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 OCTOBER 1 , 1903 


 BANGOR CATHEDRAL 


 642 


 DISTANCE 


 


 SNOWDON RANGE 


 


 644 


 ORGAN SHRUBSOLE DAY . 


 SAAR ADA AD 


 SARABAZA ADS 


 ORGAN CONSOLE , 


 { SA 


 646 


 ACCESSORIES . 


 7 ‘ WATIOUS « : . < < . 920 


 647 


 BANGOR CATHEDRAL NORTH : 


 WINTER VIEW 


 648 


 M * WILLIAM SHRUBSOLE 


 TOMBSTONE WILLIAM SHRUBSOLE 


 BUNHILL FIELDS 


 649 


 DR . ROLAND ROGERS . 


 ORGANIST BANGOR CATHEDRAL 1871 1892 . 


 650 


 MR . T. WESTLAKE MORGAN 


 ORGANIST BANGOR CATHEDRAL . 


 VINCENT NOVELLO . 


 1781 - 1861 


 MR . J. ALFRED NOVELLO . 


 OW 


 $ 87 


 FELIX MENDELSSOHN BARTHOLDY 


 ORIGINAL 


 MELODIES , 


 


 COMPOSED 


 


 F. MENDELSSOHN 


 F. G. E ] 


 HAPPY RETURNS DAY 


 BERLIOZ ENGLAND . 


 CENTENARY RETROSPECT 


 TRRAWOE TEPRESENT AOR ERTRAOBOIRARE 


 MALVENUTO CELLING 


 PASQUINADES WITTERAIAES EL ARLEQUINADES 


 BERLIOZ — MAN ORCHESTRA . 


 MUSICAL TIM 


 655 


 656 


 F.G.E 


 DR . ELGAR ORATORIO 


 ‘ APOSTLES . ’ 


 657The Worshipful Company Musicians proposes 
 hold Special Loan Exhibition June , 1904 , 
 Fishmongers ’ Hall , London Bridge . Exhibition 
 -—-in commemoration Tercentenary 
 granting King James I. Company Charter 
 Incorporation — include musical instruments 
 ( special feature 17th 18th century 
 productions ) , autograph manuscripts , printed music , 
 portraits , personal mementos distinguished 
 musicians , addition original editions early 
 music , especially madrigals written 
 England golden period English music . 
 strong Exhibition Committee Company 
 formed Sir Homewood Crawford chair- 
 man , members Master Senior 
 Junior Wardens , Sir Frederick Bridge , Sir 
 Ernest Clarke , Mr. J. C. Collard , Mr. E. E. Cooper , 
 Mr. Clifford B. Edgar , Mr. A. H. Littleton , Sir George 
 Martin , Mr. C. E. Rube , Mr. T. L. Southgate , 
 Mr. T. C. Fenwick , Clerk Company . 
 state Honorary Secretariat 
 capable hands Mr. Arthur F. Hill Mr. J. F. R. 
 Stainer , success interesting Exhibition 
 regarded foregone conclusion 

 Good news comes Dublin efforts 
 reconstitute Dublin Orchestral 
 Society , existing seasons , came 
 end April . band Society , 
 able conductorship Signor Esposito , 
 consists entirely professional players resident 
 Dublin . concerts given ( afternoon ) 
 Great Hall Royal University Ireland , 
 kind permission Senate 
 placed disposal Society . Experience 
 proved series concerts costs 
 £ 650 , total entrance money 
 ( exclusive subscriptions ) 
 depended £ 125 . order meet 
 difference caused number ladies 
 gentlemen issued circular inviting promises 
 annual subscriptions ( seats ) donations . 
 response appeal far resulted 
 promises £ 325 , , 
 grant £ 50 Corporation Dublin , leaves 
 £ 150 raised . Committee 
 confident sum forthcoming time 
 necessary arrangements 
 series concerts coming season . 
 proposed members reconstituted 
 Society shall consist ( 1 ) Annual Subscribers 
 seats value £ 2 ; ( 2 ) Annual 
 Donors £ 1 , 
 eligible election , vote , act Executive 
 Committee . Society good work 
 past , necessary mention 
 , Second , , Fifth Eighth Symphonies 
 Beethoven , ‘ Pathetic ’ Tschaikovsky 
 Second Symphony D Brahms , 
 played excellent organization . things 
 Dublin music - making special mention 
 commendation : firstly , orchestra 
 entirely local ; secondly , grant £ 50 
 given Corporation Dublin furnishes 
 interesting instance municipalization 
 music . citizens Irish capital 
 little proud musical history,—the 
 production Handel ‘ Messiah ’ wit — 
 confidently assumed lines 
 appear print remaining £ 150 
 promised . success attend efforts Signor 
 Esposito enthusiastically 
 associated good cause 

 658 




 SB 


 MA 


 CATHEDRALS CHURCHES 


 NORTH GERMANY . * 


 INTERESTING BOOK 


 NAVE , LOOKING WEST 


 ORGAN - CASE ST . MARTIN CHURCH , BRUNSWICKTHE MUSICAL TIMES.—Ocroser 1 , 1903 

 661 
 ts choir . elaborate music ritual lingered brief space enjoy specimen 
 captivate senses , rendered earnest unisonous congregational singing , peculiarly 
 buses . manner congregation , — largely composed solemn awe - inspiring circumstances 
 ni young men — fervent adoration , endurance introduced . 
 ie , ersonal inconvenience , nay positive discomfort , ? 
 doubly worthy admiration : unisonous extract , speak : — 
 vill singing hymns ( accompanied grand broad Sunday service peculiar Germany , 
 1e des . style organ ) congregation numbering desirous observing religious 
 ‘ nelish thousands , combined majesty country fail present , 7.¢. , 
 om pile gathered , impressed far People s Mass. takes place capitular paro- 
 deeply elaborate Mass Beethoven , chial churches High Mass , 
 > Mozart , Schubert . invariably attended crowds , flock enjoy 
 1 ‘ syle es = popular chorales , , forming great 
 vhole congregational singing North Germanchurches _ judiciously arranged services , sung unison , 
 » madea deep impression Mr. Bumpus , manner overpoweringly affecting 
 pro- frequently refers . aresome extracts — simplicity . wonder German Catholics 
 here- Cathedral Paderborn , Saturday _ resort service largely , seeing music 
 » evening service honour Liborius , tutelar character man , 
 Wuen- saint city diocese : — woman child church join 
 ; | settled player difficulty feeling voice 
 ! FF great organ west end cathedral struck unduly conspicuous . attempts 
 > prelude displaying power instrument . , everybody — melody . ; 
 3 presently merged stately hymn honour obvious temptation 
 = = St. Liborius , taken sung bythe quotation Mr. Bumpus informing pages 
 i. vast concourse books , manner affecting | resisted . Architecture occupies place 
 ani tears simple solemn grandeur , affections , knapsacked rambler treats 
 insta making wish spontaneous congregational rood screens , fonts , pulpits , choir stalls , stained glass , 
 singing find place services . critical acumen thorough grasp 
 ring ff following day ( Sunday ) - visited subject . Finally , eighty - illustrations , 
 heir — ) Cathedral heard hymn - tune familiar 2 , glossary technical words , capital index . 
 * * English congregations . records : — book read . 
 oa hymn sung reached 
 great south porch , thronged render DUBLIN HANDELIANA HOSPITAL . 
 ible , HOLIDAY NOTE . 
 recent brief visit Dublin chanced , 
 ° P ‘ making short cut , come Mercer 
 s Hospital . ‘ Surely , ’ thought , ‘ Hospital 
 sort Handel gave proceeds resulting 
 ‘ performance “ Messiah ” 
 sive Irish capital , 1742 . ask 
 lave old Minute Books existence . ’ 
 Registrar , Mr. John Robinson , courteously 
 — received , moment brought forth 
 astall safe old beautifully - kept 
 rtin records 160 years ago . appears previous 
 Handel arrival Dublin custom 
 + t os Governors Mercer Hospital organize 
 oral annual performance sacred music 
 ts St. Andrew ( Round ) Church , 
 ‘ cited owing benevolent zeal philan- 
 ordal thropic gentlemen music Handel 
 hell introduced people Dublin . 
 siken ) entry Minutes Governors 
 ovel took fancy dated February 13 , 
 oat 1741 , contains early reference 
 : duties conductor modern acceptation 
 nell term — , ‘ appointed beat 
 Miss time . ’ 

 ct . Deans Cathedrals 
 — & Christ Church & St. Patricks compliance 
 luties request Governors 
 noble Hospital direct Cathedral Service 
 ome , shou'd performed Choir usual & 
 ower necefsary regular 
 urch- performance Perfon 
 drals appointed beat time . Ordered 




 662 


 663 


 JOSEPH GANTHONY 


 P.M 


 — — — — - . 44 + } . _ _ _ _ 4 - 5 — — — — — 


 6 6 6 5 6 6&6 


 SS 


 COLSTON HALL ORGAN 


 664 


 CURIOUS CHURCH INSTRUMENT 


 REV . GODFREY THRING 


 MUSIC HENRY HUGO PIERSON . 


 868 ) , 


 1S 


 - SONG 


 / 2 : 7 


 | 2S SS FS SS SS SSS SS 


 PUBLISHED OCTOBER 3 , 1903 


 APOSTLES 


 ORATORIO 


 EDWARD ELGAR . . 


 PRICE SHILLINGS 


 REDUCED PRICES 


 


 F. MENDELSSOHN BARTHOLDY . = 


 * STRING PARTS .. : 


 WIND PARTS 


 / 


 UES 


 669 


 CHURCH CONGRESS BRISTOL 


 SERVICES . 


 ANTHEMS . 


 ORGAN VOLUNTARIES ( OUTGOING 


 ORGAN RECITALS 


 ORGANIST , CHOIRMASTER . CHOIR APPOINTMENTS 


 ANTHEMS . 


 670 


 PIANOFORTE MUSIC . 


 - SONGS . 


 HEREFORD MUSICAL FESTIVAL . 


 ( SPECIAL CORRESPONDENT 


 1540 - 1876 


 AL 


 671 


 MR . COLERIDGE - TAYLOR CANTATA ‘ ATONEMENT 


 672 


 PROMENADE CONCERTS . 


 BRITISH NOVELTIES 


 ENGLISH OPERA NEW OPERA 


 CROSS CRESCENT 


 MUSIC BIRMINGHAM . 


 ( CORRESPONDENT 


 MUSIC GLASGOW . 


 ( CORRESPONDENT 


 674THE MUSICAL TIMES.—Ocroser 1 , 1903 

 include Berlioz ‘ King Lear ’ overture , Jonciéres ’ 
 ‘ Sérénade Hongroise , ’ Beethoven Symphony D , 
 pianoforte violin concerto 

 choirs connected religious societies usual 
 devote energies - known oratorios . 
 Young Men Christian Association Choir , Central Section , 
 ‘ Messiah ’ ‘ Judas Maccabzus , ’ 
 Southern Section , ‘ Samson , ’ choir 
 Sunday School Union ‘ Creation , ’ ‘ Elijah ’ 
 forms winter programme choir Renfield 
 Street United Free Church 




 MUSIC LIVERPOOL DISTRICT . 


 ( CORRESPONDENT 


 MUSIC MANCHESTER DISTRICT , 


 ( CORRESPONDENTThe concerts given usually certain Wednesday 
 evenings Mr. Percy Harrison , Birmingham , acquire 
 special importance season engagement 
 Mr. Wood Queen Hall Orchestra 
 held February 10 , 1904 . series called 
 Ladies Concerts announced given 
 Midland New Hotel . held times 
 week , beginning 7th inst . , 
 management Messrs. Broadwood . Brodsky 
 Quartet concerts held Midland Hall , 
 subscription raised meet cost 
 change luxurious room . 8th inst . 
 Miss Nora Meredith , singer went 
 greater training Manchester , gives 
 evening recital hall 

 Hallé scheme season choral works 
 represent following composers:—Bach , Handel , 
 Beethoven , Mendelssohn , Berlioz , Elgar ( twice ) ; 
 operatic evening , extracts 
 Wagner , Beethoven , Cornelius . Works given 
 time concerts ‘ Prinz Igor ’ 
 overture ( Borodine ) , 7th Symphony ( Bruckner ) , Sym- 
 phony ( Glass ) , ‘ Maurische Rhapsodie ’ ( Humperdinck ) , 
 Symphonic Variations ( Parry ) , Symphonic Poem ‘ Sarka ’ 
 ( Smetana ) , ‘ sprach Zarathustra ’ ( Strauss ) , 
 Overture ‘ Die Feen ’ ( Wagner ) . 
 entire certainty Parry 
 Variations heard . 
 pianists engaged include Messrs. Busoni , Godowsky , 
 Lamond ; violinists , Dr. Brodsky , M. Kreisler , 
 Lady Hallé 

 MUSIC NEWCASTLE . 
 ( CORRESPONDENT 




 CT 


 675 


 MUSIC NOTTINGHAM DISTRICT . 


 ( CORRESPONDENT 


 MUSIC SHEFFIELD DISTRICT . 


 ( CORRESPONDENTBERLIN 

 Stern Choral Society perform season 
 Mendelssohn ‘ St. Paul ’ anniversary 
 composer death , Bach cantata ‘ Ein ’ feste 
 Burg , ’ Enrico Bossi ‘ Hohes Lied , ’ Beethoven 
 ‘ Missa solemnis ’ ; - named work , - - way , 
 included present concert scheme 
 Singakademie . Philharmonic Choir , 
 direction Siegfried Ochs , perform Bach B minor 
 Mass , Requiems Berlioz Brahms , , 
 way novelty , choruses Hugo Wolf . 
 — Victor Hollander operetta ‘ Konig Rampsinit ’ 
 given times considerable success , 
 new work kind , ‘ Der Sonnenvogel , ’ 
 produced travelling Viennese company 
 favourably received 

 BORNES 




 BOULOGNE - SUR - MER 


 CARLSRUHE 


 COLOGNE 


 COPENHAGEN 


 COTE - SAINT - ANDRE 


 677 


 HAGUE 


 LEIPZIGMAINZ 

 - days ’ festival held spring 
 1904 Kaim Orchestra Munich , 
 direction M. Kufferath . concert 
 devoted Berlioz , second Schumann , Mendelssohn 
 Brahms , Schubert Weber , 
 fourth Beethoven 

 MUNICH 




 PARIS . 


 ST . PETERSBURG 


 678 


 STEDMAN ” MUSICAL 1 AGENCY 


 SOUTH PLACE ORCHESTRAL SOCIETY 


 SOUTH PLACE INSTITUTE , SOUTH PLACE , FINSBURY , E.C , 


 CONTENTS 


 MONTH . 


 4 . B 


 MUSICAL TIMES 


 SPECIAL NOTICEMR . HENRY PLaVY 

 Beethoven Festival , Fee s S Fal Choral Crystal Palace 
 Concerts . ) 
 Solo Tenor , Christ Church , Lancaster Gate . 
 Address : 30 , Hamilton Gardens , N.W 

 MISS WINNIE HEMMING 




 FAMOUS CHILD SOLO HARPIST ) 


 680 


 SCHOOL MUSIC REVIEW 


 ANNUAL SUBSCRIPTION , INCLUDING POSTAGE , 2S 


 CONTAINS : — 


 GENERAL NOTES . 


 LESSONS RUDIMENTS MUSIC 


 THEORY QUESTIONS . 


 REVIEWS . 


 CORRESPONDENCE . 


 ANSWERS THEORY QUESTIONS 


 SCHOOL MUSIC REVIEW 


 PASTORAL MELODY 


 LAMENT 


 ORGAN 


 JOHN E. \ E. WEST 


 FESTAL COM [ MEMORATION 


 ORGAN 


 JOHN E. WEST 


 SHALL THEE 


 SONG 


 WALTER E. GROGAN 


 HAROLD L. BROOKE 


 TARANTELLA 


 ( REVEL ) 


 GIPSY SUITE 


 EDWARD GERMAN 


 COMPOSER . 


 MAGNIFICAT 


 


 NUNC DIMITTIS 


 


 SERVICE B FLAT 


 


 C. VILLIERS STANFORD 


 PULL SCORE . 


 SWEET LITTLE KATUSHA 


 WORDS WRITTEN 


 MICHAEL MORTON 


 ADOLF SCHMID . 


 TREATISE 


 MODERN INSTRUMENTATION 


 ORCHESTRATION 


 HECTOR BERLIOZ 


 TRANSLATED MARY COWDEN - CLARKEJOSEPH BENNETT 

 Price Shillings 
 masterly treatise accepted classic subject 
 Berlioz consummate master , copiously 
 illustrated excerpts scores works 
 Gluck , Mozart , Beethoven , Weber , Spontini , Meyerveer 

 especially author 




 E 


 


 ST . PETER , EATON SQUARE . 


 VACANCIES CHOIR 


 IDDLE PARISH CHURCH , PAISLEY 


 HOIR EXCHANGE.—CHURCH POSITIONS 


 ARRANGING , TRANSPOSING , COPYING , & . WM . LANE 


 5 GUINEAS . — — PIANO , “ DUCHESS ” MODEL 


 SALE . — 


 F. W. EBRALL , | 


 SHREWSBURY 


 NICHOLSON CO . , 


 ORGAN BUILDERS 


 PALACE YARD , WORCESTER . 


 ( ESTABLISHED 1841 . ) 


 RUMMENS ’ ORGAN PEDALS 


 PIANOFORTES 


 INVALUABLE HOME PRACTICE ORGAN MUSIC . 


 OLD FIRM . | 


 CONACHER & CO . , 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD 


 GOLD MEDALS 


 HUGH SWANTON 


 LEADING EXPERT ORGAN - BLOWING 


 APPARATUS BLOWING ORGANS WATER , 


 GAS , ELECTRICITY 


 CONSULTATIONS FREE . 


 62 , WHITEHORSE STREET , STEPNEY , LONDON , E 


 CHURCH MUSIC 


 PY 


 SERVICES . 


 ANTHEMS . 


 PLEASING ANTHEMS 


 PARISH CHOIRS 


 CHRISTMAS SONGS 


 - SONGS MIXED VOICES 


 SHORT , EASY ANTHEM SAINTS ’ DAY 


 SAINTS GOD 


 NDON , N , 


 0 £ 2,000 , 


 NTHE\ , 


 


 DAY . 


 NOVELLO EDITION 


 BACH CHURCH CANTATAS 


 SHILLING 


 7 | LITANY INCARNATION 


 JESUS | J. STAINER 


 WRITTEN 8Y 


 SET MUSIC 


 J. STAINER ADVENT 


 ERNEST EDWIN MITCHELL . 


 ANTHEMS SAINTS ’ DAY 


 GENERAL USE 


 DAY WRATH 


 ( DIES IR ) | 


 HYMN - ANTHEM ADVENT 


 J. STAINER . RIGHTEOUS LIVE EVERMORE 


 ANTHEMS ADVENT 


 ADVE NT HYMN 


 “ LOWLY GUISE THY KING APPEARETH 


 SOPRANO SOLO CHORUS , ORCHESTRAL 


 ACCOMPANIMENT 


 COMPOSED 


 R. SCHUMANN . 


 TRANSLATED GERMAN FRIEDRICH RUCKERT 


 BLESSED 


 WATCH 


 CANTATA ADVENT 


 SOPRANO SOLO CHORUS 


 


 HYMNS SUNG CONGREGATION 


 MUSIC COMPOSED 


 HUGH BLAIR 


 ADVENTS 


 CHURCH CANTATA 


 WORDS SELECTED WRITTEN 


 COMPOSED 


 GEORGE GARRETT , M.A. , 


 BLOW YE TRUMPET 


 ZION 


 CANTATA ADVENT 


 COMPOSED 


 C. WARWICK JORDAN 


 CHRISTMAS EVE 


 SHORT CANTATA 


 CONTRALTO SOLO , CHORUS , ORCHESTRA 


 COMPOSED 


 NIELS W. GADE . 


 STORY BETHLEHEM 


 SHORT SACRED CANTATA 


 WORDS WRITTEN 


 SHAPCOTT WENSLEY 


 COMPOSED 


 JOHN E WEST 


 CHRISTMAS ORATORIO 


 


 JOHN SEBASTIAN BACH . 


 ENGLISH TRANSLATION ADAPTATION ® Y 


 TH . ” 


 ESTRAL 


 ERT 


 HO 


 ATION 


 


 STRA 


 685 


 PUBLISHED 


 PRODUCED HEREFORD FESTIVAL , 1903 


 ATONEMENT 


 SACRED CANTATA 


 SOLI , CHORUS , ORCHESTRA 


 WORDS WRITTEN 


 PARSONS 


 ALICE 


 MUSIC 


 COLERIDG 


 COMPOSED 


 


 


 E - TAYLOR 


 TIMES 


 STANDARD 


 DAILY NEWS 


 DAILY CHRONICLE 


 YORKSHIRE POST 


 BIRMINGHAM DAILY GAZETTE 


 BIRMINGHAM DAILY POST 


 BRISTOL TIMES MIRROR 


 WORLD 


 


 NOVELLO , EWER CO . , NEW YORK 


 EVENTIDE 


 MELODY 


 NEW WORK 


 


 SUITE D MINOR 


 COMPOSED 


 GEORGE J. BENNETT 


 ORGANISTS & CHOIRMASTERS 


 - SONGS , MUSIC LEAFLETS 


 SENT GRATIS POST FREE 


 MOTHER WEEPING 


 ( STABAT MATER DOLOROSA ) 


 ENGLISH TRANSLATION 


 REV . J. MONSELL 


 MUSIC 


 F. ROLLASON 


 PUBLISHED 


 JOHANN SEBASTIAN BACH 


 CANTATAS 


 5 . 6 . 


 50 HATH SALVATION , 


 FUNERAL ODE ( TOMBEAU ) . 


 BRIGHT RAY 


 STRENGTH 


 GRANT , LORD GOD , GRANT HOPE 


 BREITKOPF & HARTEL , 


 54 , GREAT 


 MARLBOROUGH STREET , LONDON , W 


 JL 


 OVE 


 0 


 6 


 0 


 0 


 1 6 


 3 0 


 687 


 PUBLISHED . 


 PRODUCED HEREFORD FESTIVAL , 1903 


 VOCES CLAMANTIUM 


 VOICES CRY 


 MOTET © 


 SOPRANO BASS SOLI , CHORUS , ORCHESTRA 


 


 PARRY 


 PRICE 


 TIMES 


 STANDARD 


 MORNING POST 


 WESTMINSTER GAZETTE 


 SHILLINGS . 


 DAILY NEWS 


 DAILY CHRONICLE 


 BIRMINGHAM DAILY POST 


 BIRMINGHAM DAILY GAZETTE 


 OBSERVER 


 SUNDAY TIMES 


 WORLD 


 


 NOVELLO , EWER CO . , NEW YORK 


 PRODUCED HEREFORD FESTIVAL , 1903 


 MAGNIFICAT 


 


 NUNC DIMITTIS 


 


 IVOR ATKINS . 


 TIMES . 


 DAILY TELEGRAPH 


 DAILY NEWS . 


 GUARDIAN . 


 ANTHEMS ADVENT 


 “ LUTE ” SERIES 


 JOY , LIGHT - WING’D 


 ( “ “ REQUITAL ” ) 


 PRIZE GLEE , A.T.T.B. 


 


 DREAM GERONTIUS 


 


 EDWARD ELGAR 


 


 PRINCE PEACE 


 SACRED CANTATA 


 SOLO VOICES , CHORUS , 


 ORCHESTRA 


 WORDS SELECTED HOLY SCRIPTURE 


 MUSIC COMPOSED 


 ALFRED R. GAUL 


 CHORAL SOCIETIES 


 SOUTH AFRICAN EVENING SONG 


 S.A.T.B. 


 MUSIC 


 ARTHUR ABBOTT 


 BENEZER PROUT WORKS MUSIC . 


 


 JRE 


 1903 . E89 


 NOVELLO SCHOOL SONGS 


 


 OLD ENGLISH SONGS 


 GRADE II 


 COMPOSERS 


 SEVEN 


 UNISON SONGS 


 ( GRADE II . ) 


 


 COMPOSERS . 


 - SONGS 


 ( GRADE II . ) 


 


 COMPOSERS . 


 


 - SONGS 


 S.S.A.A. 


 ( GRADE II 


 COMPOSERS 


 PRODUCED ROYAL ALBERT HALL , 


 APRIL 30 , 1903 


 WAR PEACE 


 SYMPHONIC ODE 


 


 SOLI , CHORUS , ORCHESTRA 


 COMPOSED 


 C. HUBERT H. PARRY 


 “ 


 TRAGEDY COCK ROBIN 


 SHORT ACTION PIECE 


 T. ARTHUR BURTON 


 NOVELLO SCHOOL SONGS 


 - SONGS 


 MUSIC STUDENT 


 MANUSCRIPT NOTE BOOK 


 


 PRACTICE REGISTER 


 ARRANGED 


 C. EGERTON LOWE 


 PREFACE 


 FUNERAL MARCH 


 MUSIC 


 “ GRANIA DIARMID 


 COMPOSED 


 EDWARD ELGAR 


 ARRANGEMENT PIANOFORTE SOLO 


 STRING QUARTET 


 MINOR 


 TEACHERS . 


 ELEMENTS MUSIC SIMPLIFIED 


 GRADUATED EXERCISES 


 HUMOROUS PART- SONGS 


 DESCRIPTIVE CHORUSES . 


 HUSH - - , SWEETIE 


 SONG 


 EDWARD OXENFORD 


 MUSIC COMPOSED 


 FRANK E. TOURS 


 NEW ORGAN COMPOSITION 


 INTRODUCTION , 


 


 “ ADESTE FIDELES ” ’ 


 ORGAN 


 


 ARTHUR S. HOLLOWAY 


 MUS.D. , OXON 


 NARCISSUS ECHO 


 CANTATA CHORUS , SOLI , ORCHESTRA 


 COMPOSED 


 EDWIN C. 


 COMPOSITIONS 


 


 T. MEE PATTISON 


 ORIGINAL ORGAN COMPOSITIONS . 


 LBUM SONGS . — 


 NEW HUMOROUS - SONG 


 IMPORTANT NOTICE CHORAL SOCIETIES 


 PUBLISHED 


 NEW EDITION 


 HECTOR BERLIOZ 


 DAMNATION FAUST 


 ENTIRELY NEW ENGLISH VERSION 


 WILLIAM WALLACE 


 PRICE SHILLINGS SIXPENCE NET 


 RIGHTS PERFORMANCE FREE 


 BREITKOPF & HARTEL 


 54 , GREAT MARLBOROUGH STREET , LONDON , W 


 CHORAL SOCIETIES 


 PLAYED JAN KUBELIK 


 VIOLIN MUSIC 


 PIANOFORTE ACCOMPANIMENT 


 KING ARTHUR 


 JAMES SMIETON , M.A. SONATA E MINOR 


 MUSIC COMPOSED 


 SOLO VOICES , CHORUS , 


 ORCHESTRA 


 JOHN SMIETON 


 SIXTH EDITION 


 SOUVENIR 


 SALTELLATO - CAPRICE 


 DANCES 


 COMPOSED 


 FRANK E. TOURS 


 ARRANGEMENT PIANOFORTE 


 AUTHORS 


 SIXTH EDITION 


 ARIADNE 


 DRAMATIC CANTATA 


 


 SOLO VOICES , CHORUS , 


 ORCHESTRA 


 MAUNDER © 


 CHURCH 


 MUSIC 


 SOLO VOICES , CHORUS , ORGAN . 


 43 


 693NEW 

 BEETHOVEN.—Deux Sienie @ 
 Harmonium 

 DOHN : { NYI , E.—Quartet ‘ 




 WH NWA 


 NH NW 


 20 


 FOREIGN PUBLICATIONS 


 SCADCKW FE NHWH O 


 CONCERTO 


 


 ORGAN 


 


 ORCHESTRA 


 ( STRINGS , BRASS , HARP , DRUMS ) 


 COMPOSED 


 HORATIO PARKER 


 ARRANGEMENT ORGAN SOLO , 


 DAVID STANLEY SMITH 


 DIRECTORS 


 MUSICAL EDU CATION 


 


 GUIDE 


 EDITED 


 RECITAL SERIES 


 


 ORIGINAL COMPOSITIONS ORGAN 


 NOCTURNE 


 COMPOSED 


 WILLIAM FAULKES . 


 NOVELLO PRIMERS MUSICAL 


 BIOGRAPHY 


 HECTOR BERLIOZ 


 


 JOSEPH BENNETT 


 694 


 ORGAN TRANSCRIPTIONS 


 


 A. HERBERT BREWER 


 PRELUDE 


 


 ANGEL FAREWELL 


 ( GERONTIUS ) 


 EDWARD ELGAR 


 FUNERAL MARCH 


 MUSIC 


 GRANIA DIARMID 


 EDWARD ELGAR 


 ORIGINAL COMPOSITIONS 


 ORGAN 


 JOSEF RHEINBERGER 


 CHARACTERISTIC PIECES 


 MONOLOGUES . 


 SHORT ‘ PIECES . 


 PIECES 


 SCHERZO SYMPHONIQUE 


 COMPOSED 


 WILLIAM FAULKES 


 PUBLISHED OCTOBER 7 


 - SONGS 


 MEN VOICES 


 ( T.T.B.B 


 GREEK ANTHOLOGY 


 ENGLISH 


 ALMA STRETTELL , RICHARD GARNETT , 


 EDMUND GOSSE , 


 MUSIC COMPOSED 


 EDWARD ELGAR 


 GERMAN TRANSLATION JULIUS BUTHS 


 SCENES 


 FAIRY - LAND 


 


 PIANOFORTE 


 COMPOSED 


 ARNOLD KRUG 


 BOOKS 


 CONTENTS . 


 ETT 


 NG 


 


 PARISH CHOIR MANUAL 


 CONTAINING 


 CANTICLES MORNING EVENING PRAYER 


 SET APPROPRIATE CHANTS 


 FERIAL FESTAL RESPONSES , LITANY , RESPONSES COMMANDMENTS , 


 FINAL AMENS , VESPER HYMNS VESTRY PRAYERS 


 


 ORGAN ACCOMPANIMENTS LORD PRAYER , APOSTLES ’ NICENE CREEDSThe Responses Commandments selected Services composers 
 Myles B. Foster , Dr. Garrett , C. Gounod , Dr. E. J. Hopkins , Dr. C. H. Lloyd , Merbecke , H. Smart , 
 John E. West , S. S. Wesley , 

 Sir John Stainer Sevenfold Amen included , Vesper Hymns Beethoven , Sullivan , 
 ; concluding Vestry Prayers S. S. Wesley Rev. Canon Hervey 

 PREFACE 




 READY 


 SELECTED PIANOFORTE STUDIES 


 PROGRESSIVELY ARRANGED 


 


 FRANKLIN TAYLOR . 


 SETS ( BOOKS 


 EDITOR PREFACE 


 PRICE SHILLING SIXPENCE BOOK 


 CHAPPELL & COS 


 LATEST BALLAD CONCERT SUCCESSES 


 PRICE SHILLINGS NET 


 MAUDE VALERIE WHITE . | GUY D’HARDELOT . 


 “ KISSES . ” ’ “ JT HID LOVE . ” 


 ‘ ‘ LAND ALMOND BLOSSOM . ” | “ . ” 


 “ APRIL LADY . ” “ TI KNOW LOVELY GARDEN . ” 


 ‘ * CANZONE DI TAORMINA . ” H ‘ , LOVE 


 FLORENCE AYLWARD . TERESA DEL RIEGO 


 “ SILENT RIVER . ” “ WAKING SPRING . ” 


 “ LOVE BENEDICTION . ” “ GOD SPEED , DEAR . ” 


 LIZA LEHMANN . NOEL JOHNSON . 


 “ LONG AGO EGYPT . " . ; “ 


 “ SOUL BL ” 2ST . 


 FRANCO LEONI . “ PURPLE PANSIES . ” 


 “ SYMPATHY . ” + 


 ‘ RETORT . ” ‘ “ NELSON GONE - SAILING . ” 


 “ PRETTY ROSE . ” 


 “ VANITY FAIR . ” > 


 ‘ RITORNELLO . ” | “ CARESSANTE . ” 


 NEW & POPULAR INSTRUMENTAL MUSIC 


 MUSICSELLERS