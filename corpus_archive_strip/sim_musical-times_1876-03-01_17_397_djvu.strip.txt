


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


PUBLISHED ON THE FIRST OF EVERY MONTH


MARCH


I, 1876


DYKES MEMORIAL FUND


COMMITTEE. 


SECRETARIES. 


TREASURER. 


WAYSIDE SKETCHES FOR THE PIANOFORTE


HE ROAMED IN THE FOREST


LISTENING. 


THE SHADOW ON MY HEART. 


THE MUSICAL ARTISTS’ SOCIETY. 


PRESIDENT: 


ROSETTA O'LEARY VINNING’S 


POPULAR SOPRANO SONGS


PROFESSIONAL NOTICES


USIC ENGRAVED, PRINTED, AND PUB- 


RUSSELL’S MUSICAL INSTRUMENTS. : 


EAN’S CHEAP MUSICAL INSTRUMENTS. 


METAL EQUITONE FLUTES, WITH KEYS


CYLINDER FLAGEOLETS, WITH KEYS


387


ORGAN-TONED HARMONIUMS


HATTERSLEY & CO."S CELEBRATED


ORGAN-TONED DRAWING-ROOM MODEL


HATTERSLEY & CO.’'S 


BOOKS OF DESIGNS AND ABOVE 200 TESTIMONIALS 


SHOW ROOMS:— 


STANDARD AMERICAN ORGANS


MANUFACTURED BY 


PELOUBET, PELTON & CO., NEW YORK


W. SNELL’S IMPROVED HARMONIUMS


TO SOLO VIOLINISTS


ROGER’S ITALIAN SOLO FIRST STRINGS


ORGANS. 


42


ROADWOOD BOUDOIR GRAND PIANO, 


ANGLICAN


SINGLE AND DOUBLE


EDITED BY THE


AND


EDWIN GEORGE MONK


READY BY EASTER. 


TONIC SOL-FA EDITION. 


A MANUAL OF SINGING


FOR THE USE OF CHOIR TRAINERS & SCHOOLMASTERS. 


THIRTY-FOURTH EDITION. 


THE COLLEGIATE AND SCHOOL


SIGHT-SINGING MANUAL


APPENDIX


(COMPANION WORK TO THE ABOVE


COLLEGIATE VOCAL TUTOR


CATHEDRAL CHANT BOOK


389


A COLLECTION OF


ORGAN PIECES


COMPOSED BY


CHARLES JOSEPH FROST


FOURTH LIST OF SUBSCRIBERS


C. JEFFERYS, 67, BERNERS ST


NELSON'S POPULAR SONG, 


“THE ROSE OF ALLANDALE


NEW SONGS


JEFFERYS’S MUSICAL JOURNAL. 


MELODIOUS MELODIESPart 1. VOLUNTARIES; Subjects by Heller, Mendelssohn, Schu

mann, Beethoven

2. VOLUNTARIES; Subjects by Handel, Beethoven, Rossini, 
Mozart, &c

3- OPERATIC MELODIES, by Mozart, Donizetti, Auber, 
Verdi, Meyerbeer, &c




TENTH THOUSAND. 


SONGS, 


HARMONIZED FOR SCHOOL PURPOSES, 


H. C. LUNN’S ELEMENTS OF MUSIC


NOW BEING PERFORMED BY THE CARL ROSA 


OPERA COMPANY


VERDI’S IL TROVATORE


39


SHADGETT, 


ERRY CATHEDRAL.—TWO CHORISTER


ITY CHURCH CHOIR.—TWO SOPRANOS


CONCORDIA 


ANNUAL SUBSCRIPTION, INCLUDING POSTAGE, 


NINETEEN SHILLINGS AND SIXPENCE


OFFICE—LONDON: 1, BERNERS STREET, W


TRINITY COLLEGE, LONDON


THE MUSICAL TIMES 


AND SINGING-CLASS CIRCULAR. 


MARCH 1, 1876


HANDEL’S CHANDOS ANTHEMS. 


BY EBENEZER PROUT, B.A


392


393


394


THE “ AUTHORS,’ COMPOSERS,’ AND 


ARTISTS’ COPYRIGHT AND PERFORMING 


RIGHT PROTECTION SOCIETY


BY HENRY C. LUNN


396


1S NO 


CRYSTAL PALACE


SACRED HARMONIC SOCIETY


398THE annual festival of the Royal Society of Musicians 
will take place at Willis’s Rooms on the 22nd inst. ; 
President of the day, the Earl of Shrewsbury. Several 
artists of eminence have volunteered their services on the 
occasion

Tue three Recitals of pianoforte music given by 
Miss Florence May at the Beethoven Rooms, are, in 
every respect, entitled to the highest praise. The versatility 
of her powers has been successfully proved by her render- 
ing of a selection of classical works of the most varied 
schools of composition ; and at each performance she has 
elicited the highest marks of approbation. Without doubt 
her talents seem to have been especially directed towards 
the due interpretation of works requiring energy and 
impulse ; but she is an artist of exceptional qualifications, 
and has worthily won her name

AT a recent meeting of the Academical Board of Trinity 
College, London, it was decided that at all future examin- 
ations for musical diplomas the candidates will be required 
to pass a preliminary examination in English grammar and 
composition ; arithmetic; geography and English history ; 
and, (in the case of candidates for the highest diploma), 
elementary Latin, or instead, a modern foreign language




ALTO


TENOR 


110


L + {TH 


1 \—-)— | — PPP 


THE LAST WILD ROSE


LA 


I = A J ¥ 


————___ 


1876. 


AY 


THE LAST WILD ROSE


CTRSC


ANT


1 . * 1 —— 


403


REVIEWS


404


406The chapter on the expression stop we heartily com- 
mend, but think a few easy exercises on its management 
would have been an improvement. The general rule 
enunciated that ‘‘the expression stop should always be 
used, except in pieces requiring uniform loudness through- 
out,” is a thoroughly sound one. It is the not using this 
stop which renders an average performance on the harmo- 
nium so painfully monotonous. With a player who knows 
how to avail himself of it, it gives a command of light 
and shade hardly inferior to that attainable on the violin, 
and far superior to any that can be produced either on the 
piano or organ

The eighty-two pages which form the remainder of the 
book are filled with a selection of studies and pieces 
arranged in sections, for harmoniums containing from one 
to four sets of vibrators. In these the resources of the 
instrument are fairly, though by no means exhaustively set 
forth. The exercises are very good; the selection of pieces, 
thirty-eight in number, might, with advantage, have been 
much more varied. We find five by J. B. Calkin, five by 
Sir John Goss, and nine by Mendelssohn, but not one by 
Beethoven, Handel, Haydn, Mozart, or Schubert. The 
pieces given are good enough, but an important feature of 
the harmonium—its adaptability for the transcription of 
orchestral music—is almost entirely ignored. Many of the 
slow movements (and even of the Allegros) of Haydnh’s 
and Mozart’s symphonies have acharming effect on the

harmonium, but not one of these is given. We must 
protest, too, against such an arrangement as that on 
pp. 28, 29, of one of Mendelssohn’s Songs without Words, 
in which the whole accompaniment is altered. If it is not 
suitable for the harmonium in its original shape it ought to 
be let alone




LAMBORN COCK


ORIGINAL CORRESPONDENCE


DR. STAINER’S “ HARMONY.” 


TO THE EDITOR OF THE MUSICAL TIMES


A PUZZLED TYRO IN HARMONY


408


TO CORRESPONDENTS


BRIEF SUMMARY OF COUNTRY NEWSTHE MUSICAL TIMES.—Marcu 1, 1876. 409

B flat, and Brahms'’s variations on a theme by Haydn, tested with much 
success the powers of the fine orchestra assembled; and the evening 
concert, which was crowded to overflowing, included Beethoven's 
Symphony in A major, which was excellently rendered, and General 
Reid's “ Introduction, Pastorale, Minuet and March,” played in deference 
to precedent. Mr. Charles Hallé’s pianoforte performances were an 
important feature, Beethoven’s Concerto in C major No. 1, and Stern- 
dale Bennett’s Caprice in E, being especially deserving of the highest 
praise. Madame Antoinette Sterling and Mr. Lloyd achieved the 
most signal success in the vocal music allotted to them, although 
the lady was obviously suffering from hoarseness. The festival was 
brought to a close on the 14th ult., a novelty in the programme being 
Raff's Symphony in E, No. 5, which elicited much applause and 
excited the deepest interest. Mr. Hallé was again the pianist, and 
Madame Sterling and Mr. Lloyd the singers. In every respect the 
performances have been highly successful, and Mr. Hallé deserves 
every credit for the admirable manner in which he conducted

GawTHorPE.—The Victoria Brass Band gave a Concert in the 
National Schoolroom on Wednesday, January 26th, assisted by several 
local vocalists, including Mr. R. Punyer (tenor), and Mr. J. Overend 
(bass). Mr. W. Squires gave two solos on the violin, and Mr. H. 
Pickles accompanied. The band played several selections which were 
well received. Mr. B. D. Jackson conducted




410Stone.—Mr. Gundry gave a Concert at the Town Hall, on Wednes- 
day evening, the oth inst., when the following artists were engaged :— 
Madame Clara Suter, Madame Alice Barnett, Mr. Wallace Wells, 
Mr. Henry Pyatt, Mr. T. Harper (solo Trumpet), and Mr. Arthur H. 
Jackson, R.A.M., (Pianoforte

TuNBRIDGE WELLS.—Mr. Jobn H. Gower gave a Morning Concert 
on the 17th ult. The programme included a sacred quartett, ‘‘ O Lord, 
my God,” composed by the concert giver, which was well received. 
Miss Annie Sinclair was recalled in “I love my love,” Pinsuti, as was 
also Mr. Mellor, in “If with all your hearts,” Mr. T. Smith gave 
“He was despised” with expression, and Mr. O. Christian was 
much applauded in Gounod’s Nazareth. Mr. Gower's rendering of 
Beethoven’s ‘‘ Moonlight” Sonata was all that could be desired. 
There was a large and fashionable audience

WALTON-ON-THE-HILL, LANCASHIRE.—A successful Concert was 
given in the Walton Institute, under the direction of Mr. Carmichael, 
on Thursday evening, January 27th, in aid of the Building Fund for 
The Soloists were Miss Madeline Roe, Miss Marie Ternan, Miss 
the proposed new Church. The programme comprised selections 
from the works of the following composers, viz., Barnby, Cberry, 
Hatton, Balfe, Verdi, Levey, Roeckel, Blockley, and Carpenter. 
Agnes Robinson, Mrs. Carmichael, and Messrs. Hobart, Peake, C. J. 
Lenton, and C. Wilson. Mr. C. Wesley Evans was the accompanist

WaAKkEFIELD.—The second of the series of classical chamber Con- 
certs, promoted by Miss Fitton, was given on the 4th ult., in the 
Music Saloon, and fully sustained the reputation for artistic selection 
and masterly performance of a high-class programme, which had 
been so deservedly earned on the previous occasion. Beethoven’s Trio 
in C minor, for pianoforte, violin, and violoncello was well given 
by Messrs. R. S. Burton, Sykes, and Weston. The other artists were 
Miss Wood, and Messrs. Acomb and Bowling

Wa LtHamstow.—The Musical Society gave a performance in the 
National School-room, on the 14th ult., of Mendelssohn’s Elijah. 
Mr. Reed conducted, and Mr. H.R. Bird presided at the piano. Thesolo 
vocalists were Miss Brand, Miss Winn, Mr. Howells, and Mr. Winn

WoLvERHAMPTON.—The second Concert of the present season was 
given by the Festival Choral Society, on the 2nd ult., at the Exchange. 
Beethoven's trio in G major, for piano, violin, and violoncello, ex- 
cellently played by Madame Norman Neruda, Mr. Charles Hallé, and 
Monsieur Vieuxtemps, and a violin solo, a Fantasia on Der Freischiitz, 
by Madame Norman Neruda, accompanied by Mr. Hallé, were 
features in the programme. new part-song, composed by Mr. 
Alfred R. Gaul, of Birmingham, “ The Silent Land,” was well sung by 
the choir. Mr. Charles Hallé playeda pianoforte solo—Nocturne in 
D flat, and “ Wanderer’s Song” (Chopin), and a “Hunting Piece” 
(Rheinberger), so brilliantly that an encore was elicited. Madlle. 
Clelland was the vocalist, and was highly appreciated in all her solos. 
Schumann's “ Gipsy Life” well sung by the choir, brought the pro- 
gramme toatermination. Mr. W. C. Stockley conducted

Wrnpsor.—Mr. O. Christian gave a Concert in the Town Hall on 
the 15th ult., which was well patronised. The most successful pieces 
in the first part, which consisted of selections from Oratorios and 
Cantatas, were ‘“‘ Morning prayer,” Costa, charmingly sung by Miss 
M. Hancock, “The soft southern breeze,” from Barnby’s Rebekah, 
sung by Mr. Pearson, and encored, “I rejoice in my youth,” from 
G. A. Macfarren’s St. John the Baptist, well rendered by Miss Agnes 
Larkcom, and “ He that hath the bride,” from the same composition, 
most effectively given by Mr. Christiah. The second part included 
two violin solos, by Miss Bertha Brousil, and a pianoforte solo by 
similar compliment was awarded to Miss Larkcom in “ Casta Diva,” 
Mr. J. H. Gower, ‘‘Rigoletto,” which were well received. Mr. 
Christian was encored in Gounod’s “ Maid of Athens,” and a 
to Miss Margaret Hancock in Cowen’s “It was a dream,” and to Mr. 
Pearson in “‘ Laura,” a song by Rastrelli. Randegger’s Trio, “ I Navi- 
ganti,” brought a successful concert toa close. Mr. Couldrey presided 
at the harmonium, and Mr. John H. Gower was conductor




DURING THE LAST MONTH. 


PRINTE RS’ ;, FIFTH ANNUAL DRAWING, 


TICKETS, ONE SHILLING EACH


AST LONDON ORGAN WORKS


US


O COUNTRY MUSICSELLERS.—WANTED


TENTH SEASON, 1875—76. 


HE ENGLISH LEE UNION, 


ASSISTED BY 


412


RGAN, PIANOFORTE, HARMONIUM, HARHE YORKSHIRE CONCERT PARTY, 
consisting of the following eminent Artists: Miss Pauline 
Grayston, Soprano; Miss Emma Kennedy, Contralto; Mr. J. H 
Goodhall, Tenore; Mr. Dodds, Basso; Mr. Jno. Shaw, Solo Pianist 
and Accompanist. For terms for full Concert Party, or individually, 
for Oratorios, Operatic Selections, Miscellaneous Concerts, &c., 
address C. G. Taylor, 31, Portland-crescent, Leeds

BEETHOVEN—MOZART. 
Will shortly be Ready. 
EAUTIFULLY MODELLED PARIAN 
BUSTS, 114 inches high, of the above. 
Price 21s. the pair to Subscribers. 
Specimens may be scen and orders received at Messrs. Wood & Co., 
3, Great Marlborough-street, London

ARPS.—P. HOLCOMBE, 43, Berners-street, 
London, REPAIRS and RE-DECORATES all kinds of 
Gothic and Grecian Harps. Terms moderate; 35 years’ practical 
experience. A large selection of SECOND-HAND HARPS by Erard 
and others always on SALE. Harps of every description bought




AKER’S PIANOFORTE TUTOR. 


NEW OCTAVO EDITION


MOORE’S IRISH MELODIES


EDITED BY 


M. W. BALFE. 


O CHORAL SOCIETIES, CATHEDRAL 


A SELECTION FROM 


MOORE’S IRISH MELODIES. 


INT. 


A, 38, 


OIR- 


5, &C


AN


001. 


BT


THE ANGLICAN HYMN-BOOK


NEW EDITION, REVISED AND ENLARGED


SECOND SERIES


HE ANGLICAN CHORAL SERVICE BUOK, 


USELEY AND MONK’S PSALTER AND 


OULE’S COLLECTION OF WORDS OF 


THE PSALTER, PROPER PSALMS, HYMNS, 


ADDITIONS TO THE


EV. T. HELMORE’S PLAIN SONG WORKS


OULE’S DIRECTORIUM CHORI ANGLI- 


OULE’S DIRECTORIUM CHORI ANGLI- 


HE ORDER FOR THE HOLY COMMUNION


414 THE MUSICAL TIM


THE VILLAGE ORGANIST


GIVE THANKS UNTO THE LORD.’ 


THE LORD IS MY SHEPHERD


THE 23RD PSALM.) 


66


HE LORD IS RISEN INDEED: HALLE- 


EIGHTH THOUSAND


EASTER ANTHEM. 


“THE LORD IS MY STRENGTH


SHORT, EASY, AND EFFECTIVE. 


CAROLS FOR EASTER-TIDE


REV. THOMAS HELMORE. 


REV. J. M. NEALE, D.D


ANTHEMS FOR EASTER


BARNBY, J.—-AS WE HAVE BORN THE IMAGE OF 


THE EARTHY 


— IF WE BELIEVE THAT JESUS DIED ° 3 


GOUNOD, CHARLES.—BLESSED IS HE “WHO 


HOPKINS, E. J.— WHY SEEK YE THE LIVING 


SULLIVAN, ARTHUR.—I WILL MENTION THE 


THORN. 'E. H—CHRIST IS’ "RISEN FROM THE ' 


— THE LORD THAT BROUGHT U ° 3. 


NINE HYMNS WITH TUNES


FOR


EASTER


SELECTED FROM


THE HYMNARY


PRICE ONE PENNY


WELCOME, HAPPY MORNING ARTHUR SULLIVAN


HENRY SMART


THE DAY OF RESURRECTION 


FOR LENT. 


THE BENEDICITE


SET TO MUSIC BY THE FOLLOWING 


MODERN COMPOSERS


VEY 


AN


GRATIS TO SUBSCRIBERS


A SACRED CANTATA 


THE MUSIC COMPOSED BY


T. BROOKS, R.A.M


HANDEL’S 


CHANDOS ANTHEMS


6 IGHT WELCOME HOME, O PRINCE.” 


R. FOWLE’S STANDARD MARCHES 


AS VOLUNTARIES. 


HE NEW NATIONAL SONGS.—VICTORIA 


ADAME SAINTON-DOLBY’S NEW SONGS. 


TO CHORAL SOCIETIES. 


NEW AND CHEAPER EDITION. 


JUST PUBLISHED


CHORAL SONGS FOR


SCHOOL AND HOME


A COLLECTION OF 42 SONGS FOR ONE, TWO, OR FOUR VOICES


WRITTEN SPECIALLY


FOR SCHOOL USE, BY


ROBERT JACKSON


LONDON: NOVELLO, EWER AND CO


NEW EDITION OF 


DR. BENNETT GILBERT’S POPULAR WORK


SCHOOL HARMONY


EVERY SUBJECT HAS ITS SPECIAL EXERCISES. 


NEW PART-SONGS


BY W. W. PEARSON


SOUL OF LIVING MUSIC


SUMMER AND WINTER


NEW


VOCAL ALBUM OF PART MUSIC


EDITED CHIEFLY BY


G. A. MACFARREN


PART-SONGS


BY


CHARLES HARFORD LLOYD


NEW PART-SONG FOR S.A.T.B


COMPOSER OF


THE POPULAR PART-SONGS


SOL-FA EDITIONS OF


MENDELSSOHN’S WORKS


PUBLISHED BY NOVELLO, EWER AND CO


ST. PATRICK’S DAY