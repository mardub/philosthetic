


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 FOUNDED 15844 . 


 PUBLISHED MONTH 


 ROYAL CHORAL SOCIETY.|GUILDHALL SCHOOL MUSI 


 ROYAL ALBERT HALL , JOHN CARPENTER ST . , VICTORIA EMBANKMENT , E.C. 


 SATURDAY , MARCH 4 , 1916 , 3 . | » 


 ELGAR . 


 MR . FREDERICK RANALOW , . — 


 BAND CHORUS . 


 DREAM GER TELS MANCHESTER COLLEGE MUSIC 


 SESSION 1915 - 1916 


 PRIN S : , ' 


 UNIVERSITY DURHAM . 


 MUSICAL 


 1916 


 VICTORIA COLLEGE MUSIC , 


 LONDON , 


 INCORPORATED 1891 


 INCORPORATED GUILD CHURCH 


 MUSICIANS 


 ( A." . G.C.M. ) , LICENTIATE   ( L.1.G.C.M 


 ASSOCIATE 


 FELLOWSHIP ( F 


 COMPETITIONS 1016 


 REG ISTER ORGAN VACANCIES 


 MR . W. H BREARE 


 STUDENTS PROFESSION . 


 JOFORTE TEAC ‘ RS’E XAMIN ATION 


 A.R.C.M. DIPLOMA 


 PIAN 


 LONDON COLLEGE MUSIC , 


 EXAMINATIONS — LOCAL HIGHER 


 NATIONAL CONSERVATOIRE 


 KEYBOARD PRACTICE 


 SEND FREE BOOK 


 “ * LIGHT PIANOFORTE PLAYING . ” 


 ADDRESS : — : 


 FRIL 


 L.R.A 


 SIC , 


 1 . SCHOOL 


 LLOW . 


 CONCERTS 


 IDEAL 


 PUBLIC MEETINGS 


 KINGSWAY HALL 


 MAGNIFICENT ORGAN PLATFORM 


 SPLENDIDLY SITUATED & LIGHTED 


 GLASGOW 


 ST . ANDREW HALLS , GLASGOW 


 EDWIN H. HALE 


 D. GLEN MACKEMMIE 


 MR . F 


 115 , RENFIELD STREET , GLASGOW 


 ROYLE 


 COACH OPERA CONCERT WORK 


 STUDIOS 


 LONDON MANCHESTER 


 18 , SUTHERLAND AVENUE , LONDON , W 


 NEWCASTLE - - LYME 


 CHARLES LISTER BRADLEY 


 VOICE - CULTURE , SINGING , INTONING , ETC . 


 OPPORTUNITY STUDENTS 


 PROFESSIONAL NOTICES 


 MR . SAMUEL MASTERS 


 ( TENOR ) . 


 LONDON COLLEGE CHORISTEKS . 


 COMPOSERS ’ MSS . 


 REVISED PREPARED PRINTING . 


 DR . A. EAGLEFIELD HULL 


 DR . NORMAN SPRANKLING 


 EVISION MUSICAL COMPOSITIONS 


 L.R.A.M. ( PAPER WORWB 


 A.R.C.M. ( PAPER WORK ) . 


 F.R.C.O. SPECIALIST 


 68 MUSICAL TIMES 


 FEBRUARY , 19106 


 DR . LEWIS ’ TEXT - BOOKS : 


 ANI ) THIRTY - SUCCESSES ; A.R.C.M. , 


 1897 - 1914 , T HRE E HUNDRE D SEVENTY - SUC- 


 WELLS CATHEDRAL . 


 CLEARANCE SALE ORGANS- 


 CENTRAL DEPOT RUSSIAN MUS 


 J. & W. CHESTER 


 


 PNE 


 NORMA 


 _ — _ — _ 


 ARENSKY , : 


 RUBINSTE ! 


 SCHULHOF ? 


 RUBINSTEI ! 


 SCHULHOFF 


 VO 


 _ _ 


 — _ _ 


 S.W 


 MUSICAL 


 TIMES.—FEBRUARY 1 


 1916 . 69 


 PIANO PEDALS 


 NORMAN & BEARD 


 PNEUMATIC PEDAL ATTACHMENT 


 PIANO 


 DOLL'S - HOUSE SUITE 


 LITTLE PIECES 


 LITTLE 


 PIANOFORTE SOLO 


 FOLK 


 COMPOSED 


 HUBERT BATH . 


 NOVELLO EDITION . 


 . 523A 


 RUSSIAN POLISH DANCES 


 MELODIES 


 ALBUM 


 


 CONTENTS : 


 IMPRESSIONS 


 L'ALMANACH AUX IMAGES 


 SUITE CHORUS 


 LADIES ’ VOICES 


 VOCAL SOLI 


 POEM 


 TRISTAN KLINGSOR 


 MUSIC 


 GUSTAVE FERRARI 


 WOODLAND DANCES 


 SET EASY PIECES 


 


 PIANOFORTE . 


 COMPOSED 


 ERNEST NEWTON 


 MILITARY 


 USE MILITARY BANDS CHAPLAINS 


 PARADE SERVICES CAMP CHURCH 


 CONSISTING 


 MORNING PRAYER , LITANY , 


 HOLY COMMUNION , 


 COMPILED ARKRANGED 


 THOMAS CONWAY BROWN 


 MILITARY BAND 


 GEORGE MILLER 


 MUSICAL 


 TIME 


 S.—FEBRUARY 1 , 1916 


 96 


 51 


 GOOD - NIGHT 


 


 . * MAYBELLS ‘ FLOWERS , 


 NOVELLO OCTAVO EDITION 


 5 . * SEEK REST . FRANZ ABT 


 164 . * AUBADI . - . [ IRELAND 


 ‘ 98.§*BIRD SONGS ° H. A. J. CAMPRELL 


 PEACE KAREL BENDL 


 7 SONG “ IRELAND 


 FAIRY WORKMEN 


 . * FATHOM .. ‘ .. J. IRELAND 


 . “ HARVEST FIELD , ° MENDELSSOHN 


 * HOME , FAR AWAY MENDELSSOHN 


 HOUR PARTING , RUBINSTEIN 


 MARRIAGE SONG " > BENDL 


 SONG SCHUMANN 


 MENDELSSOHN 


 LONDON 


 38 . * O WERT THOU CAULD BLAST 


 42 . * SONG “ RUY BLAS " ‘ MENDELSSORN # 


 133 . LOVE HATH ENTANGLED ; 


 115.8*WINTER SONG , H. A. J. CAMPBELL . 


 179 . “ ZEPHYR FL OWERS 


 46 . * ZULEIKA HASSAN MENDELSSOHN 


 5234 . 


 191 


 92 . 


 192A - C 


 I-1 


 CS 


 SH AR 


 NSTEIN 


 FOSTER 


 INECKE 


 POSTER 


 LSSOHN 


 NSTEIN » 


 HLFELD 3 


 ERMAN 


 SSOKN 


 M PRELL 


 INECKE 


 INSUTI 


 -SSOHN 2 


 OSTER 


 INECKE 


 SSOHN 


 RNETT 


 { PBELL 


 LSSOHN 2 


 OSTER 


 NSTEIN 


 HREND 


 ‘ Z ABT 


 { PBELL 


 UMANSN 


 " OSTER 


 LLCOCK 


 /ELAND 


 SMART 


 COWEN 


 COWEN 


 COWEN 


 SAYLOR 


 UMANN 


 APBELL 3 


 INECKE 


 NSTEIN 


 INFCKE 


 [ ATTON 


 COWEN 


 RAHMS 


 TCHER 3 


 HREND 36 


 { PBELL * 


 7OSTER 2 


 LRDSON 3 


 TCHER 


 SSOHN 


 MUSICAL 


 TIMES.—FEBRUARY 1 , 1916 


 NOVELLO EDITION 


 RUSSIAN MUSIC . 


 PIANOFORTE SOLO 


 39 ) . 


 


 MUSICAL 


 TIMES 


 FEBRUARY 1 , 1916 


 NOVELLO OCTAVO EDITION 


 37 . * AUTUMN SONG .. ° MENDELSSOHN 


 ‘ , * BELLS EVE “ 4 . ABT 


 < 4 . BEWARE ° - CARL RE!INECKE 


 98.§*BIRD SONGS H. A. J. CAMPRELL 


 156 . SCHRISTMAS GREE TING , A. .. E. ELGAR 


 176 . " CLOUD , RUBINSTEIN 


 53.5 CONTENTMENT .. CARL REINECKE 


 110.§*DOUBT THY FATHER CARE E. ELGAR 


 18 . E ‘ ENING HYMN . NATALIE DAVENPORT 


 86 . EVENING PEACE KAREL BENDL 


 2 . “ EVENING SONG IRELAND 


 44 . “ EVENING SONG MENDELSSOHN 


 21 . * FAINT , FEAR HENRY SMART 


 136 * FOUNTAINS , WAKEN .. , A. RICHARDS 


 36 . * GREETING .. MENDELSSOHN 


 43 . “ HOME , FAR AWAY ‘ MENDELSSOHN 


 34 . * 1 LOVE . MENDELSSOHN 


 28 . * SONG SCHUMANN 


 MENDELSSOHN 


 160 , 


 2 . * MERRY MAIDENS , 


 50 . MOONLIGHT SONG .. 


 11.5 MOUNTAIN MUSIC 


 MUSIC WOODS 


 . * BARK BOUNDING 


 45 


 123 . * NIGHT 


 103.§*NIGHT WIND 


 59 


 12 YE 


 131 LEAVE THY S 


 2 . * O COME , 


 O LADY 


 38 . * O WERT 


 13 . O'ER SANDS 


 14 % . BLUE SEA 


 47 . OUSEL , 


 27 . * SU NSHINE 


 139 . “ HILL , DALE 


 130 . " MOUNTAIN 


 93 . PHG@BUS 


 49 . PRAISE MUSIC 


 125 . PROUD DELIA 


 40 . * SABBATH MORN , “ 


 16 . SABBATH SEA 


 15 . * SABBATH REPOSE . 


 SEA FAIRIES , 


 § * SHADOW DANCE 


 | 152 . “ SHEPHERD SWEE T LOT , “ 


 MEETING WATERS , 


 FLOW'RETS 


 IRISH AR 


 UBINSTEIN 


 + CARL REINECK 


 


 RUBINSTEI 


 ILKEN THREAD 


 THOU CAULD BLAST 


 MENDELSSOKN 


 1 CAMPBELL 2 


 119 . SISTER , AWAKE ! .. NATALIE DAVENPORT 2 


 42 . * SONG “ RUY BLAS ” MENDELSSOHN 2 


 SUMMER EVENING , 


 SUMMER NIGHT , 


 7.§*SUMMER SONG , ' 


 JOY 


 177 


 166 . THOUGHT HOME , 


 172 . DAFFODILS 


 171 . - DAY 


 129 . DIANA 


 181 . * TOM - TIT 


 30 . * NIGHTINGALE 


 102.§ * TRAVELLER , 


 57 . TRUTH TRUST 


 117 . * WANDERER ' ’ JOY , 


 55 . WANDERER SONG , 


 138 . * WATER - LILY 


 78 . LOVE 


 167 , BIRDS 


 133 . LOVE HATH EN 


 23 . WEARY AR 


 . * WILD FLOWERS .. ° 


 WINTER ° 


 5.8*WINTER SONG , 


 3 . * WISH MOUNT 


 72 . WOOD NOTE , 


 & . * WORK PL AY ° 


 * ZEPHYR F 


 ZULEIKA HASSAN 


 AVE 


 R. SCHUMANN 


 H. A. J. CAMPBELL 3 


 RUBINSTEIN 2 


 REINECKE 


 TANGLED 


 H , . ‘ . J. CAMPBELL 


 AINS , A. 


 LOW ERS 


 MENDELSSOEN 


 LONDON 


 NOVELLO COMPANY , LIMITED 


 5234 » 


 125A. 4 


 125B. 


 125C. 


 12A - C 


 1—4 


 


 ‘ STEIN 


 ‘ OSTER 


 NECKE 


 ‘ OSTER 


 SSOHN 


 ARRAY 


 STEIN 3 


 PBELL 3 


 LPELD 34 


 ERMAN 


 RRABRBRR 


 SSOHN 2 


 ENDL 3 


 LOYD 3 


 OSTER 2 


 SHAW 3 


 ENDL 34 


 STEIN 


 [ TREND 


 FELD 


 : ABT 


 } OYCE 


 BELL 


 MANN 


 STER 


 LGAR 


 ENDL 


 BELL 


 WEN 


 ELBY 


 JOYCE 


 Y NES 


 LAND | 


 MART 


 WEN 


 WEN 


 WEN 


 AR RAR RA 


 SHAW 3 


 YLOR 


 LRRRRS 


 MUSICAL 


 TIMES.—FEBRUARY 1 , 1916 


 71 


 NOVELLO EDITION 


 RUSSIAN MUSIC 


 PIANOFORTE SOLO 


 42 


 203A - B 


 10 


 MUSICAI 


 TIMES 


 FEBRUARY TI , 1916 


 TRINITY COLL E GE MUSIC 


 INSTITUTED 


 G. E. BAMBRIDGE , F.T.C.L. , F.R.A.M. 


 NOVELLO EDITIONS 


 FAVOURITE OPERAS 


 ADAPTED 


 CONCERT USE 


 1 . FAUST 


 2 . IL TROVATORE 


 JHAUSER . 


 3 } TANN 


 4- MARITANA 


 BOHEMIAN GIRL 


 5 . 


 DAUGHTER REGIMENT 


 7 . MARTHA 


 BROADWOOD 


 


 BOSW ORTH EDI TION 


 LYRIC ORGANIST . 3 V OLS 


 LIVES OFOUR GREAT MUSICIANS 


 TEACHERS 


 SELECTED 40,000 WORK 


 CATALOGUE 


 BOSWORTH & CO 


 READY 


 


 ( CONTAINING 


 


 BACH 


 48 


 PRELUDES & FUGUES 


 - TEMPERED CLAVICHORD 


 NOS . I—24 


 FINGERED 


 


 DA ) 


 VOLS 


 00 WORK 


 NGERED 


 UES 


 ORD 


 SINGING - CLASS CIRCULAR 


 FEBRUARY 1 , 1916 


 DAN GODFREY BOURNEMOUTH 


 MUSICAL STOCK 


 CHARLES GODFREY , 


 1790—1863 , 


 DANIEL 


 1831 - 1903 , 


 1893 


 ADOLPHUS FREDERICK 


 1837 - 1882 , 


 CHARLES 


 1868 - 1904 , 


 ARTHUR EUGENE CHARLES GEORGE HERBERT 


 1895 - 1896 


 1889 - 1897 , 


 1897 - 13898 , 


 74 MUSICAL 


 , 1916 


 1914 , ¢ 


 15 , 191 


 , CC 


 1893 - 4 


 2 , 1893 : 


 6 PHE 


 MUSICAL 


 


 B MUSK , 


 € F , 1914 , 


 XUM 


 80 MUSICAL 


 TIMES.—FEBRUARY 1 


 1910 


 PROGRESS POVERTY . 83 

 Violent fluctuations opinion , course 
 years , impossible te critic 
 conscientiously studied work delivers 
 judgment . inclined 
 older critics failed 
 eatness Beethoven Wagner , remember 
 rule listen men works 
 opportunity previous study score . 
 better musician man - day , cautious 
 passing opinion big new work 
 merely performance . — 
 e unfurtunate feature case — modern music 
 isso expensive critic afford 
 tobuyallheneeds . latest play Hauptmann 
 Tchekov , instance , shillings ; 
 single opera Strauss Moussorgsky Rim - ky- 
 Korsakotf pay shillings . 
 works living poet novelist 
 und ; score * Salome’—a - hour 
 opera — costs £ 30 . Ifa man intends bee 
 musical critic thing 
 born rich parents . money 
 buy music wants , liable error 
 closely attached individuals 
 rties . Brahmsians , example , given 
 singularly little gvod Brahms criticism , 
 personal devotion man , 
 influence exercised younger 
 critics grew tutelage . Occasionally 
 zsthetic prejudice clique react disastrously 
 critic judgment classical music . Spitta , 
 example , honest atutude 
 Bach pictorialism pictorialism 
 supposed vice Lisztians , Spitta 
 stalwart anti - Lisztian Brahmsian camp . 
 critic , fact , wishes reproached 
 inconsistency , ample income , friends , 
 unlimited time study , choice time 
 writing , , 
 sentence pens , ‘ wonder read 
 years 

 Saint - Saéns wrote article 
 Paris /igaro days ago 
 attitude French musicians 
 adopt Wagner . 
 moved indignation attempt excuse Wagner 
 “ Capitulation . ” difficulty proving 
 futility argument Wagner set 
 tosatirize Germans fetish 
 French French . 
 sophistry , wiser look 
 episode indiscretion , misery 
 Wagner endured France palliation . 
 main point Saint - Saéns article , , 
 . speaks senseless imitation 
 Wagner French composers , reminds 
 acting opposition 
 true spuit Wagner , urgedall German composers 
 toremain German . ‘ , ’ says , ‘ Wagner spoke 
 words gold , ’ Frenchman Wagner 




 SAINT - SAENS 


 WAGNER 


 ORPHEUS CHEZ NOUS . Vivace irate 

 little quotation Beethoven , brought 
 date triplanal harmony , expresses feelings 
 gramophone ! contrivance 
 worse human , accompanied : 
 whirring , scratching sound,—for , tne 
 extra charge . goes ino 
 metallic maw suffers sea - change 
 strange , rich . good voice goes clear 
 reappears thetin , tinny . orchestral performant 
 interned balanced , comes insignificant 
 details unexpectedly taking place whole- 
 flute drowning tuba , notes tht 
 oboe hitting eye like blast Dom 
 heard gramophone disgorge performance 
 “ Adeste Fideles ” cathedral choir . othe 
 end instrument know choir 
 outstanding excellence . reached pained 
 ears occasion ill - balanced rabble , 
 apparently early stages asthma . smile 
 pianola , gramophone comes tht 
 door Orpheus flies window . ’ 

 suggestions kind 
 music home consumption ? ’ asked 




 03 


 COVETY 


 86 


 - PRELUDES 


 FUGUES . 


 BACH 


 0 6 — — — 


 1748—6 


 XUM 


 87 


 LA GUERRE DES BOUFFONS 


 ORGAN RECITALS 


 APPOINTMENTSNovello & Co. , Ltd 

 pieces Album standard type 
 organists prepared provide occasion . 
 Funeral Marches Beethoven ( flat ) , Chopin , 
 Handel ( ‘ Saul ’ ) , Tchaikovsky ( C minor ) , Mendelssohn 
 ( ‘ Song words ’ ) . Players want change 
 - worn works find excellent material 
 chorus ‘ Blest mourn . ’ Brahms 
 * Requiem , ’ admirably arranged John E. West , effective 
 Funeral March William Faulkes , arrangements ‘ know 
 Redeemer liveth , ’ ‘ O rest Lord , ’ 
 Schubert ‘ Marche Solennelle ’ E flat minor , Finale 
 Tchaikovsky ‘ Pathetic ’ Symphony , touching | 
 litle ‘ Lament ’ John E. West . Album 
 useful organists times , account 
 intrinsic excellence contents , brings 
 cover necessary music likely 
 scattered copies , hand wanted 

 Organ Loft . Nos . 118 - 120 . 
 [ G. Schirmer , Ltd 




 BOOKS RECEIVED 


 BELL - MASTER CARILLONNEUR . 


 EDITOR ‘ MUSICAL TIMES 


 


 , 1916 


 EXCESS SALIVA SINGING . 


 EDITOR ‘ MUSICAL TIMES 


 REQUEST . 


 P. H. TURNBULL , 


 ORIGIN TUNE ‘ MONKLAND . ’ 


 DULCIMER . 


 INSTRUMENTS SYMPATHETIC 


 STRINGS 


 94 


 , 1916 


 RUTLAND BOUGHTON NATIVITY DRAMA 


 GLASTONBURY . 


 ( CORRESPONDENT 


 MADAME LIZA LEHMANN ‘ EVERYMA 


 TH 


 Y MAN , ’ 


 > COMPOSE ! 


 MUSICAL 


 SHAFTESBURY THEATRE 


 CHARACTERS OPERA 


 STARLIGHT EXPRESS . ’ 


 MUSIC SIR EDWARD ELGAR . 


 06 MUSICAL 


 TIMES.—FEBRUARY 


 , 1916 


 PRESTON PARISH CHURCH 


 ROYAL COLLEGE MUSIC . } 


 TRINITY COLLEGE MUSIC , LONDON 


 ROYAL CHORAL SOCIETY 


 LONDON CHORAL SOCIETY 


 4 ALT 


 ORGAN 


 ANTHEM LENT GENERAL USE 


 — — — 2 — $ — $ $ $ — $ — _ — _ — _ _ , — _ — — — — 49 _ _ 


 DISTRESS 1 CRIED UNTO LORD 


 ® = = — _ 2 — — — 


 2 — SS 


 NG - — 


 _ _ _ _ — _ 


 HL 


 — — 3 PSS — : 


 XUM 


 DISTRESS CRIED UNTO LORD 


 6 | = SSE SS 


 _ — ~ — — _ _ & - = — _ _ — _ 


 7 _ 


 SF 


 DISTRESS 


 N N 


 “ = — — _ _ > 


 = SS = S   S S 


 SS =   S ES 


 SS _ 2—- — 2 ? 


 ' | SS | 


 NS 


 DISTRESS 


 C4 “ ' ! 


 4 _ - — _ 


 DISTRESS | CRIED UNTO LORD 


 -).2 = = _ _ LL . 


 ( 5 . A.T.E 


 XUM 


 ABBEY GLEE CLUB 


 


 ORIANA MADRIGAL SOCIETY 


 AD 


 UMFRIES SUNDAY CONCERT 


 SOUTH PLACE POPULAR CONCERTS 


 CORRESPONDENTS 


 BIRMINGHAM 


 106 MUSICAL 


 TIMES.—FEBRUARY 1 


 BOURNEMOUTHDuring past month musical activity usual 
 place shown abatement . , satisfactory 
 state audiences considerably larger 
 earlier season , despite continued drain 
 male element population national purposes 

 Symphony Concerts exceedingly pleasant musical 
 functions , programme contains 
 appeal individual taste . example , 
 attractive works specified hereunder representative 
 period , school , country importance ; 
 breadth outlook //ws intelligent interpretation 
 combination compels attention : Overture , ‘ Coriolan , ’ 
 Symphony . 7 , , ‘ Leonore ’ Overture ( . 3)—all 
 Beethoven ; Tchaikovsky fourth fifth Symphonies , 
 ‘ Romeo Juliet ’ Overture ; Smetana 
 * Bartered Bride ’ Overture ; ‘ Jupiter ’ Symphony ( Mozart ) ; 
 March Russian Theme Glazounov ( performance 
 concerts ) ; ‘ Unfinished ’ Symphony ( Schubert ) ; 
 Overture , ‘ Polyeucte ’ ( Dukas ) . old friend 
 Bournemouth public , Dr. Charles Maclean , paid 
 periodical visits twelfth Symphony Concert 
 December 23 . occasion brought 
 brand - new orchestral sketches entitled ‘ Absence ’ 
 * Recollection ’ ; linked earlier work 
 composer free use quotations . 
 music characteristic composer , 
 opinion , stronger heard ; 
 * Absence , ’ particular , think generally voted 
 Dr. Maclean best . conduct per- 
 formance work somewhat anxious 
 ordeal composer , case Dr. Maclean 
 orchestral forces hand , ensuring 
 beneficial results 

 average standard ability solo performances 
 month attained higher level 
 earlier season . Dr. Rumschisky played 
 brilliantly Saint - Saéns fourth Pianoforte Concerto , 
 interpretation revealing nimbleness finger 
 associated loftier attributes thoughtfulness 
 concentration purpose ; Violin Concerto Nardini 
 ( time ) excellent medium Mr. Max 
 Mossel finished art , Birmingham _ _ professor 
 performance charming old - world music 
 natural grace suavity . wise maxim warns 
 running walking powers properly 
 established , Miss Muriel Poliska , 

 
 prompted attempting play Ctacy 
 work Beethoven ‘ Emperor ’ Concerto . 
 pianist marked talent , restrain 
 natural ambitions far . M. Philip Abbas y , 
 successful Lalo ‘ Cello Concerto , 
 instrumental tone musicianly qualities winning ¢ogjj 
 appreciation . Rimsky - Korsakov Pianoforte Concerto sy 
 inviting little work , nicely calculated effects appareni ; 
 appealed forcibly Miss Edith Leah , - knoy , 
 local performer , interpretation greeted yj 
 evident signs pleasure audience large dimensig 

 series * ‘ Monday Specials ’ justifies js 
 existence affording opportunities performane , 
 music unavoidably crowded 
 Symphony Concerts . running weekly concerts 
 high - class music seven mont 
 Mr. Dan Godfrey position bring forward num 
 compositions necessarily drop ks 
 favourable conditions . Recent reappearances interestix 
 works * Specials ’ bave follows : Introductiy , 
 Act 3 , * Tannhduser ’ ( Wagner ) ; Overture , ‘ 
 Flute ’ ( Mozart ) ; ‘ Brandenburg ’ Concerto , . 3 ( Bac 
 — concert exclusively devoted Russian music — t 
 Polovtsian March , Overture , Polovtsian Dances fn 
 Borodin ‘ Prince Igor ’ ; composer Symphon : 
 Sketch , ‘ Steppes Central Asia ’ ; Moussorgshy ’ 
 Fantasia , ‘ Une Nuit sur le Mont Chauve ’ ; Tchaikovsirs 
 ‘ 1812 ’ Overture ; Rimsky - Korsakov Fantasia « 
 Serbian themes . highly - attractive concert therews , 
 , graceful performance Messrs. Whitake , 
 Solomon , Riviere , Wolters , members Orchesn , 
 middle movements Ippolitov - Ivanov Strix 
 Quartet minor . eleventh concert Mi . 
 F. King - Hall , leader Orchestra , played Wagner : 
 ‘ Dreams ’ violin solo able fashion , af 
 Miss Dorothy Phillips sang ‘ Ah ! fors é lui , ’ Verdi : 
 ‘ Traviata , ’ commendably . January 3 , ‘ Sounda 
 alarm , ’ Handel ‘ Judas Maccab.eus , ’ capitall 
 sung Mr. Sam Hempsall 




 BRISTOL 


 DEVON CORNWALL . 


 DEVON 


 CORNWALL 


 DUBLIN 


 EDINBURGHOn December 20 superb performance Elgar Concerto 
 given Mr. Sammons Scottish Orchestra . 
 anall - British programme . Overture Norman 
 O’Neill , Rhapsodie Vaughan Williams , ‘ 
 wild geese , ’ Hamilton Harty , completed altogether 
 delightful concert . December 27 , Miss Carrie Tubb 
 gave memorable performance ‘ Ah , perfido ’ Aria 
 ‘ Cosi fan tutte . ’ Symphony Tchaikovsky 

 E minor . ‘ Sigurd Jorsalfar ’ Suite Grieg wy 
 given time . Popular Concert given 
 Orchestra January 3 . Elgar ‘ Carillon ’ regi 
 Mile . Scialtiel Thomé poem ‘ La Fianeée 
 Timbalier . ” items created sensation , ao 
 evidently type composition large numbe , j 
 supporters . Royal Choral Union cong , 
 gave fine performance ‘ Revenge . ’ Mr. W. G , aly 
 new conductor , congratulated reception 
 obtained . New Year Day Royal Choy 
 Union gave annual recital ‘ Messiah . ’ ‘ Lhe soloigs 
 Miss Caroline Hatchard , Miss C. Mentiplay , Mr , Jote 
 Harrison , Mr. Frederic Austin . Scottish Congr 
 given evening Union 
 soloists . writing ‘ Messiah , ’ mention 
 Mr. Moonie annual Christmas performance 
 work December 25 . Choir meeting 
 regular rehearsals , excellent performance given , 
 January 10 Beethoven night arranged th 
 orchestral directors , proved populy 
 concerts series . Miss Fanny Davies played th 
 ‘ Emperor ’ Concerto short pieces , mog 
 notable Toccata Debussy wel 
 nigh - forgotten Scherzo Mendelssohn . Septuor ws 
 welcome resurrection 

 lesser concerts recitals mentioned 
 pianoforte ‘ cello recital Mlle . Claude Dizan ai 
 Mile . Valérie Valensar , Belgians . lady ws 
 pupil Popper , ‘ Rhapsodie Hongroix 
 displayed fine technique artistic finesse . Mlle . Dizm 
 young lady pianoforte - playing remarkable fa 
 years . gave fine performance Glazounor 
 Sonatas , surmounted technical difficulties eas 




 GLASGOWAt seventh concert , December 21 , attractive 
 programme British music given , included fou 
 novelties , viz , ‘ Humoresque ’ Norman O'Neill , tk 
 Overture ‘ Wreckers , ’ Dr. Ethel Smyth , Norfolk 
 Rhapsody . 1 , R. Vaughan Williams , Hamilton 
 Harty Symphonic - Poem , ‘ wild geese . ’ Thes , 
 ‘ Valse gracieuse ’ Suite D minor ly 
 E. German , Elgar Violin Concerto ( magnificent 
 played Mr. Albert Sammons ) , completed programme 

 December 28 , possibly Mr. Mlynarski interesting 
 reading Dvoraik ‘ New World ’ Symphony : 
 Grieg Orchestral Suite ‘ Sigurd Jorsalfar ’ given 
 time , Miss Carrie Tubb solo vocalist 
 Choral Union vigorous direction Mr. Warret 
 Clemens gave usual ‘ Messiah ’ performance Ne * 
 Year Day , reason semi - religious functio 
 year attract customary overflowing audienct . 
 repetition work given popular audience 
 City Hall , January 13 . Elgar Introduction ani 
 Allegro string orchestra brought hearing # 
 Glasgow tenth concert January 4 . programme 
 included particularly fine performance Tchaikovsky ’ 
 fourth Symphony , F minor . Miss Agnes Nicholls sol0 
 vocalist charmed audience singing Beethoven ’ 
 scena , ‘ Ah , perfido ! ’ Miss Fanny Davies solo pianist 
 January 11 , giving finished performance Moazarts 
 Pianoforte Concerto G major ( . 17 B. & H. ) . Ao 
 attractive novelty Rimsky - Korsakov Symphonic - Suité 

 Antar . ’ performance Bantock 0 




 MUSICAL TIMES.—FEBRUARY 1 , 1916 . 109 

 Hebrides , ’ announced twelfth 
 concert January 18 , performance 
 tponed later date . substitution thereof , Mozart 
 Symphony E flat Beethoven Symphony . § 
 ¢ minor given . Miss Elsie Cochrane , young Glasgow 
 soprano , soloist . ; 
 Saturday Popular Orchestral Concerts merit 
 notice . , December 18 , - Russian 
 ramme given included Pianoforte Concerto 

 iy Rachmaninoff , solo played Mr. Philip 
 f , Halstead , best local pianists ; second , 
 December 25 , Mr. Herbert Walton , accomplished 
 organist Glasgow Cathedral , gave excellent performance 




 LIVERPOOL 


 MUSICAL 


 110 


 MANCHESTER DISTRICTThe season chronicle resumed Savonor ' 
 appearance Hallé November 18 , programm 
 compounded old recognised masterpieces sampls 
 lesser - known Russian school : Glazounov Preis : 
 ‘ Middle Ages ’ Suite , Liadov ‘ Enchanted Lake 
 Ippolitov - Ivanov ‘ Circassian Village . ’ Candow 
 compels state orchestral miniatures havew 
 especial fitness Hallé programme . pleas 
 undoubtedly hearing ; pleasure le 
 enhanced second hearing probably doubtful,—and ye 
 felt heard couldn 
 happier interpretative conditions . Ths 
 impression strengthened experience December2 
 Savonov conducted orchestral ‘ Te Deum ’ 
 seventy bars length , Sgambati , Scherzo , ‘ 
 fronda e fronda , ’ Victor de Sabata , Swedish 
 Rhapsody , ‘ Midsommervaka , ” Alfven . 
 concert schemes Manchester works te 
 entirely appropriate , Hallé look bigger thing 
 journey , Savonov guidance , worldd 
 unknown men music interest , left litt 
 satisfaction . concert derived mu 
 distinction Scriabin Symphony E , mor 
 ments Sibelius ‘ King Christian ’ Suite . Th 
 Symphony impressed people freshness , geniality 
 intensely lyrical character ; recall ay 
 conductor acceded encore repetition 
 symphonic movement insistent clamour , ba 
 vivacious Scherzo 9 - 8 rhythm éi 
 orchestral playing imagined , mthe 
 clashes ideas symphonic development pemt 
 repetition . Savonov given & 
 Sibelius ‘ En Saga , ’ concerts 
 memorable supreme interpretation Novem ’ 
 18 programme ‘ Francesca da Rimini ’ ready 
 Sibelius , played Savonov , brings forcibly te 
 finest features conductor style 

 Beecham concerts November 25 December 4 
 fairly epitomized enthusiasms , grasp vaned 
 musical idiom , indomitable energy face @ 
 difficulties daunted determi 
 man : Weber . Mozart , Beethoven , Debussy , C 
 d’Indy , Moussorgsky , Borodin , Delius , 
 Gardiner afforded amplest variety matter 
 manner , greatest triumph came ol 
 sought seldom found — accompaniment & 
 Beethoven Violin Concerto ( soloist , Mr. Arthur Catterall 
 Martchester chronicler write _ moderatis 
 Catterall , - fruits 
 Manchester School . great distinction s } * 
 emotional warmth linked intellectual control 

 Beecham Weber — ‘ Oberon , ’ lyrical — tends 1 
 somewhat effeminate elegance - bodit ® 
 ripe - flavoured beauty ; , violent contrasts 
 tempi excite convince . ‘ 
 boundless faith certainty 
 artistic savoir vivre , received severe vil 
 December 9 , effect crook listeners 

 Approact m , doubt impartial minds | January , 1914 , issue Journal , spoke early life 
 e old hat ‘ Iberia ’ Suite differs Chabrier ‘ Espafia’|among . second Harrison concert ( November 23 ) , 
 sa Whistler cheap coloured print . gave actual experience native tongue 
 > judice Balfour Gardiner orchestral ‘ Fantasy ’ ranks } music lips Princess Iwa . Clad native attire 
 ched ins 9 yorthily best British music . superbly | performed animated folk - song , sudden ejaculatory 
 rong bias ved phrases punctuating normal flow melody ; encore 

 Ip : ‘ Ifi choral selection Moussorgsky ‘ Boris Godounov ’ | * Lullaby ’ restricted length compass . 
 chane § ( Coronation Scene ) sufiered partial preparation , | voice eludes strict classification . Possessed 
 cz ff dye late arrival copies . revealed of|quite high notes , inclines contralto quality . 
 characteristic qualities familiar country } Mr. William Samuell bids fair distinguished 
 Savonor ' Tchaikovsky Church liturgical settings . The| baritone ; ballad concerts display vocal quality , 
 fogramm : reat scene Act 2 memorable M. Auguste | higher essentials making perfectly- 
 d samples ff Rouilliez vivid interpretation Boris sense blood- | equipped singer . 
 S Prelok guiltiness . Miss Gwendolen Thomas Mr. Alfred second Brodsky ( Quartet Concert November 20 , 
 ed Lake ’ Jf Heather sang music Feodore Shouisky . second Bowdon Chamber Concert December 7 
 Candoc : Mr. Brand Lane provision Manchester | fully interesting programmes , Brahms D minor Sonata 
 shave ff mosical needs Christmas New Year holidays , | ( Dr. Brodsky Mr. John Wills ) Verdi E minor 
 e ff ihe swing musical life resumed January 6 ; | Quartet main items , 
 would’e ff jut dealing , allusion } Miss Susan Morvay Mr. Anton Maaskoff played Brahms 
 — ye J } regrettable inevitable tendency Municipal quarters | Sonata ( Op . 100 ) ‘ Kreutzer ’ Beethoven , 
 couldnx § tcartail expenditure music summer - time our| vocal duets given Misses Edith McCullagh 
 ns . Ths ff oaks open spaces . result tendency } Helen Anderton . 
 cember2 , ff throw greater prominence desirability utilising ] concerts given auspices 
 Deum ! J numerous small choirs Manchester . These|of Committee Music War - time . 
 e120 , ‘ § choirs ready willing , probably at| November 23 Mr. Frederick Dawson gave pianoforte 
 Swedish Jf ‘ allest strength male ranks . necessary| recital , November 30 choir Henshaw Blind 
 Bsa suitable form platform alcove - turntable| Asylum appeared , December 14 choir newly 
 J variety , secure choir having sing| formed Mr. Sydney H. Nicholson , connection 
 er things Jf wind ; satisfactory results be|the movement , appeared time . second 
 world ! mined . open style circular ‘ brass band ’ type of| series music - makings took form Tuesday 
 Jeftlitte Jf platform useless , save days absolute calm , and| ‘ Dinner - hour ’ popular concerts , Ancoats 
 mab § sch days coincide open - air concert ! Girls ’ Institute Choir sang , January 8 , - varied 
 Wo Mr. Landon Ronald programme January 6 contained | selection - songs heard recent festivals ; 
 te . Th ff ‘ Meistersinger ’ ? Overture , Prelude closing Scene | January 25 Mr. Frederick Dawson , second time 
 genialit , § ‘ Tristan , ’ Tchaikovsky B flat minor Pianoforte Concerto | season , gave services cause . February 
 ecall aa } ff ( Miss Scharrer ) , Tchaikovsky fourth Symphony . This|concerts include recital Mr. Arthur Catterall 
 petitions ff xheme ‘ old ’ music drew big ‘ gallery , ’ change | ( accompanist Mr. John Wills ) ; vocal recital Mr. Manitto 
 mout , bet vas thoroughly appreciated . season having as| Klitgaard ; achoral programme Manchester Orpheus 
 t det itdiciousa blend sorts musical styles | Choir , orchestral concert Beethoven Society 
 tit rath ff exacting reformer desire ; impression that| Orchestra . arrangements possible 
 pemit § Mr. Ronald tends somewhat ponderous dignity style | generosity artists placing 
 given © ff retived ample confirmation occasion . are| services disposal executive helping 
 voncert ® § thee world pianists successfully tackle | musicians fortunately circumstanced . 
 November Tehaikovsky B flat minor Concerto , lady — but| Ancoats Chamber Concerts , November 28 , 
 reading : isthe lady Miss Scharrer ? Messrs. G. Archer Hill E. Bennett North played 
 rcibly% & ff Beecham absence Italy politico - musical mission | duet form movements orchestral ballet , ‘ Judith 
 _ ff Bsnecessitated - arrangement programmes , | Holofernes , ’ Walter H. Mudie , Madame Gertrade 
 embet ff January 13 brought 1 ‘ Omar Khdyyam,’| Brooks gave Archer Hill song , ‘ Homewards , ’ 
 vas J mdacted composer : short , miscellaneous | poem Phyllis Mudie . 
 | face ot scond half , conducted Mr. Hamilton Harty , succeeded Catterall Quartet , Mr. Mrs. Frank Merrick , 
 mer creating ideally appropriate atmosphere in| staff Mr. Albert J. Cross School Music 
 tbussy ‘ L’Aprés - midi ’ that’I heard a| severally attempted find Houldsworth Hall , right 
 Ballot Hf Bitish conductor . Corot - like haze enveloped | heart city traffic , suitable home chamber 
 vatter Hh work , positively felt ( heard ) the| music . accurately gauge acoustics . 
 | $ 0 ofits Bt amosphere heat languorous ease . November 17 Mr. Mrs. Merrick gave 
 & writer number hold view | ‘ guess composer ’ programmes , advertisement 
 Catters ” 9 ‘ Omar ’ entirety given ‘ cuts ’ sanctioned | days later giving clue . 
 poder tcomposer , early 1915 , public loses by| series recitals eminent organists Town Hall 
 t - fruits & lengthy abbreviation 1 . omitted stanzas|has attracted public expected . 
 SF Hh 46 ) heard January 13 , the| Messrs. Swinnen , Ellingford , Alcock , Perkins 
 l. wader music came home added power ; not| played . Reubkeé 94th Psalm Fantasia given 
 — tends ® ff vilingly miss philosophic stanzas | occasions winter . 
 ll - bodie # 8 Frederic Austin hand , pathos solemnity ] connection cinema houses good music 
 ntrasts © Bt * Earth answer ’ chorus — ] growing rapidly . Oxford Picture House 
 [ cont Bmple features . performance took place somewhat | orchestra members , including dozen Hallé men , 
 y _ BS Bh ater point time season scheme was|and addition musical accompaniment films 
 shock - fair percentage cf choir new to|they miniature popular orchestral concert . Mr. J. 
 er work , finished choral performance | Fielding Crompton conductor 

 xUM 




 NOTTINGHAM DISTRICT 


 YORKSHIREEnigma ’ Variations , inclining reflect 
 immense advance orchestral playing West Rigi » 
 past years , advance , 
 directly establishment popular concerts log 
 professional orchestras , Leeds . Anothy 
 feature programme Arensky Pianoforte Concer , 
 effective particularly individual youthful work , th 
 solo brilliantly played Miss 
 Truman . Miss Bertha Armstrong vocalist , 
 distinct impression naturally expressive gy } 
 Aida song , ‘ Ritorna vincitor , ’ Scene ; 
 Verdi opera . concert deserves brief mentiq 
 given Miss Fennings , aid violy 
 pupils , January 13 , aid War charity , whenty 
 soundness methods teacher demonstrated 

 Leeds , January 17 , Mr. Frederick Dawson gave 
 pianoforte recital Great Hall University aij 
 deserving object , local ‘ Music War - time ’ scheme 
 engaging professional musicians concerts t 
 soldiers Leeds Infirmary . pto 
 gramme exceptional interest , including typical 
 sets Variations — Bach ‘ Alla maniera _ Italiam 
 Beethoven ‘ Thirty - , ’ C minor , book ¢ 
 Brahms ‘ Paganini ’ Variations . , thing , 
 including Schumann ‘ Papillons , ’ played M 
 Dawson invariable brilliance certainty 

 Leeds University centre artistics 
 intellectual activities Leeds , mid - day recitak 
 primarily delectation members , mu 
 enjoyed larger circle privileged hearers . Ob 
 January 18 Mr. Alex Cohen ( violin ) Mr. Herter 
 Johnson ( pianoforte ) gave short recital thy 
 played ‘ Kreutzer ’ Sonata,—which oust al 
 Beethoven violin sonatas concert reperton , 
 — movements César Franck fine Sonata . Th : 
 | pestoemens ’ musicianly readings music gave mot 
 | enjoyment thoroughly appreciative audience , Franck ’ 
 | work sympathetically treated 

 Ilkley , January 6 , chamber concert Russ 
 music given Miss Vera Dawson ( pianoforte ) , Mis 
 Sylvia Sparrow ( violin ) , Miss Elma Godfrey ( violoncello , 
 played Pianoforte Trios Tchaikovsky ( mina 
 Arensky ( D minor),—both ‘ elegiac ’ intention , # 
 happens,—with great success . Mr. W. Hayle te 
 vocalist . formed suitable complement series 0 
 University Extension Lectures Russia bea 
 given recently Ilkley 




 BRIEFLY SUMMARIZED 


 AUCKLAND , N.2 


 BLACKBURN 


 BRIGHTONCAPE TOWN 

 conductorship Mr. Theo . Wendt , excellent 
 programmes rule Corporation Concerts . 
 4mong important works recently performed : 
 ‘ Unfinished ’ Symphony ; ‘ Leonore ’ . 3 ; Glazounov 
 Symphony ‘ Scenes de Ballet ’ ; Goldmark * Sakuntala ’ 
 Overture ; Liszt E flat Concerto ; Brahms Symphonies 
 Dand E ; Beethoven 7th , Sth , 9th Symphonies ; 
 Dokas ‘ L’Apprenti Sorcier ’ ; Tchaikovsky ‘ Francesca 
 da Rimini ’ ; Elgar ‘ Enigma Variations 

 DUNEDIN , N.Z 




 HANLEY 


 MELBOURNE 


 MERTHYR 


 SOUTH PORT . 


 TORONTO 


 XUM 


 WINNIPEG 


 114 


 DU 


 SPECIAL NOTICE . | 


 NDEI 


 VRR 


 EHS FEHR AHK HR HK 


 


 MUSICAL 


 TIMES.—FEBRUARY 


 MONTH . 


 NOVELLO & CO . , LIMITED 


 ETHERSTON , REV . SIR GEORGE RALPH . — 


 RACE , HARVEY 


 OODEVE , MRS 


 AYNE , W. BRUCE , K. M. E. LILLINGSTON 


 ’ 4 . B. 


 REDUCED PRICE 


 , 1916 . 


 ORIGINAL COMPOSITIONS 


 ORGAN 


 YATHEPHONE . 


 MUSICAL TIMES 


 SCALE TERMS ADVERTISEMENTS 


 116 MUSICAL 


 , 1916 


 MUSIC LENT 


 PASSION CHRIST . G. F. HANDEL . 


 PASSION LORD . 


 PASSION “ LORD . 


 ION WAYS L ANG UISH . 


 BARNE 


 STORY CROSS 


 VOICES ORGA 


 WORDS 


 REV . E. MONRO 


 SET MUSIC 


 REPROACHES _ 


 SET MUSIC 


 COMPANY 


 BENEDICITE 


 SET MUSIC 


 FOLLOWING COMPOSERS . 


 BAIRSTOW , 4 


 GADSBY 


 ER 


 3Y , 


 ZZ 


 


 ANTHEMS 


 LUTE SERIES 


 LIMITED 


 SACRED 


 HYMNS 1 


 ¥ 


 TH 


 WORDS SE 


 DL ‘ CED PR ICE . ) : \ : ( REDUCED PRICE . ) 


 < < — — MUSIC COMPOSED MUSIC COMPOSED 


 SSN DESERT STORY CALVARY 


 . = TENOR BASS SOLI CHORUS 


 - SOPRANO , TENOR , BARITONE SOLI ROSE DAFFORNE BETJEMANN 


 — J. STAINER . E. CUTHBERT NUNN . 


 CANTATAS LENT 


 DARKEST HOUR TENOR BARITONE SOLI CHORUS 


 INTERSPERSED HYMNS SUNG 


 SOPRANO , TENOR , BARITONE SOLI CONGREGATION 


 CHORUS WORDS SELECTED WRITTEN 


 HYMNS SUNG CONGREGATION SHAPCOTT WENSLEY 


 MUSIC 


 WORDS SELECTED HOLY SCRIPTURES , INTERSPERSED 


 APPROPRIATE HYMNS , COMPOSED 


 W. MAURICE ADAMS ANTON DVORAK . 


 XUM 


 18 MUSICAL 


 NOVELLO ANTHEMS LENT 


 CRDEEELERE ED ERP ER REE PEE 


 MUS’CAL 


 TIMES 


 FEBRUARY , 1916 


 119 


 EASTER ANTHEMS . 


 XUM 


 MUSICAL 


 120 


 TIMES.—FEBRUARY 


 INSTRUMENTAL MUSIC 


 


 VOCAL 


 ANTHEMS . 


 PENITENCE 


 PEACE 


 J. H. MAUNDER 


 ANTHEM MOMENT . 


 K D.D 


 WAR VESPER . 


 “ GRANT THY PEACE . ” 


 VERSES 


 CHA 


 POPULAR EASTER ANTHEM 88 


 COMPOSED EDWYN A. CLARE 


 CRUCI 


 4 SACRED CANTATA 


 DEPICTING 


 SAVIOUR LIFE 


 


 SOLO VOICES ( TENOR BASS ) CHORI 


 SCENES 


 EARTH 


 FREDERICK ILIFFE , M.A. , 


 PRICE : 


 DARKNESS NIGHT , 


 FEM 


 CARY 


 SET T 


 R | { SIMPLE FORM CHANT SERVICE ] MODERN CHURCH MUSIC 


 


 MUSICAL 


 , 1916 . 121 


 ‘ 3 1 . SON 


 - LAURENCE BINYON ARNOLD BERESFORD . 


 SET MUSIC CHORUS ORCHESTRA 


 FEMALE BOYS ’ VOICES . 


 44 . WHATSOEVER THINGS WRITTEN AFORETIME 


 NOVELLO COMPANY 


 LIMITED 


 LONDON 


 NOVELLO HANDBOOKS MUSICIANS . 


 EDITED ERNEST NEWMAN 


 


 3 REVEALED CONTEMPORARY EVIDENCE 


 APPENDIX 


 SOURCES KEYBOARD MUSIC ENGLAND 


 F 


 CHARLES VAN DEN BORREN 


 JAMES E , MATTHEW 


 CHORAL TECHNIQUE & INTERPRETATION 


 R ) : 


 TEACHING ACCOMPANIMENT 


 PLAINSONG 


 F FRANCIS BURGESS 


 


 JOHN STAINER 


 


 REV . F. W. GALPIN , M.A. , F.L.S 


 124 


 MUSICAL TIMES.—FEBRUARY 1 


 1916 


 NOVELLO VOCAL ALBUMSIs . 6d 

 BEETHOVEN.—Twenty - Soncs . Vol . I. English 
 German words . English Version Rev. 
 J. TRouTBECK . Price . 6d 

 BEETHOVEN.—SEVENTEEN Soncs . Vol . II . English 
 German words . English Version Rev. 
 J. TRoUTBECK . Price . 6d 

 BEETHOVEN.—TWENTY - Sones . Vol . III . English 
 German words . English Version Rev. 
 J. TRouTBECK . Price . 6d 

 BENNETT , W. STERNDALE . — Twetve Sons , 
 English German words . Price . ; cloth , gilt , 
 2s . 6d 




 S NOVELLO 


 PIANOFORTE ALBUMS . 


 126 MUSICAL 


 TIMES.—FEBRUARY 


 , 1916 


 STUDENT 


 MUSIC 


 -READING SIGHT 


 SLATCHER , 18 , BARGERY ROAD , 


 S.E 


 _ WEBSTER'S. 


 CHILD PRIMER 


 1 


 THEORY MUSIC 


 - THOUSAND 


 CATFORD , LONDON 


 RAINBOW 


 MUSIC READING LADDER BEGINNERS 


 NOVELLO ELEMENTARY MUSIC 


 MANUALS . 


 PHYSICAL EXERCISES , DANCES 


 GAMES 


 INFANT SCHOOL 


 MARGARET ALEXANDER HUGHES 


 MUSIC ARRANGED 


 ELEVENTH PFHOUSAND . 


 NEW EDITION 


 PAGANINIS 


 ART PLAYING 


 VIOLIN 


 TREATISE 


 SINGLE & DOUBLE HARMONIC NOTE 


 CARL GUHR . 


 TRANSLATED ORIGINAL GERMAN 


 NOVELLO 


 


 SABILLA 


 REVISED 


 C. EGERTON LOWE 


 OLD 


 ENGLISH VIOLIN MUSIC 


 EDITED 


 ALFRED MOFFAT 


 PREFATORY NOTE 


 NI 


 CHARLES MACKLBAN 


 9 PIECES .. 


 12 . 


 125C. 


 5 C 


 1SA - D. = 


 26 . C 


 6 C 


 4A - F. — 


 224 - 3 . — 


 134 . H 


 380 K 


 1408 , _ 


 MI 


 2 % . MC 


 454 - 8 , — 


 1 . PA 


 4 . RO 


 149 . SC . 


 ® . ST ) 


 43 , — 


 7 ‘ TA 


 ‘ 3 . TH 


 ™ TS 


 2 = ( WI 


 3 WO 


 NG 


 BERTINI 


 127 


 NOVELLO EDITION 


 CLASSICS , ROMANTICS , WORKS EMINENT 


 MODERN COMPOSERS 


 PIANOFORTE MUSIC . 


 SCHOOLS STUDIES 


 100 


 1727 


 PIANOFORTE SOLO . ~ — 


 PIANOFORTE DUET . 


 PIANOFORTES , HANDS . 


 CHAMBER MUSIC . 


 VOCAL MUSIC 


 MINIATURE SCORES . 


 MUSICALPIMPS Y P oz 

 F uneral March ( Pianoforte Sonata , Op.26 ) BEETHOVEN 

 know Redeemer liveth ( ‘ * Messiah ” ) 
 Price viii stil Cloth , gilt , Shillings 




 


 NOVELLO 


 ALBUMS ORGA 


 SELECTED PIECES 


 FUNERAL MUSIC 


 VILLAGE 


 FUNERAL MUSIC . 


 LONDON 


 ORGANIST 


 NOVE LLO & COMP : , LIMITED 


 MENDBLSS0 ® ) 


 MENDELSS0 


 PUBLISHED 


 


 SHOR ] 


 


 COMPOSERS 


 CONTENTS 


 EASY PIECES 


 ORGAN 


 DELS 


 CHUBEE 


 LIKOWSKF 


 LI KOWS 


 ROYA 


 


 HIAW 


 RC 


 YORK G 


 PAREPA 


 STERNDAL 


 JUNIOR 


 


 PRINCE C 


 ROY 


 RO