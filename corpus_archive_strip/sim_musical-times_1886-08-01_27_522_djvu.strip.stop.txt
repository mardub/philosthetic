


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 PUBLISHED MONTH 


 AUGUS 


 LEEDS TRIENNIAL MUSICAL 


 FESTIVALTuurstay Morninc.—Bach MASS B MINOR . Principal 
 Miss ANNA WILLIAMS , Miss HILDA WILSON , Miss DAMIAN , 
 Mr. BARTON McGU CKIN , Mr. SANTLEY 

 TuursDAY Eventnc.—New Work , Chorus Orchestra , 
 REVENGE , C.V. Stanford ; SYMPHONY C MINOR , 
 . 5 ( Beethoven ) ; WALPURGIS NIGHT ( Mendelssohn ) , Prin 

 Fuay Morninc.—ST . LUDMILA . Oratorio written 
 Festival Antonin Dvorak . Principals : Madame ALBANI , 
 Madame PATEY , Mr. EDWARD LLOYD , Mr. SANTLEY 




 FREDERIC KING 


 SANTLEY | 


 AUGUST 16 


 GLOU CESTER MUSICAL FESTIVAL . 


 DUS . 


 NOVELLO ORATORIO CONCERTS 


 SEASON 


 J. B. CRAMER & CO . , 


 | CONCERT AGENTS , 


 LIVERPOOL 


 EMS OPERAS . ” 


 1886 - 57 


 “ GEMS OPERAS ” CONCERT COMPANY 


 POPUL : AR CHAMBER CONCERT 


 PARTY 


 HUTCHIN 


 MISS JESSIE M. HILEY 


 MR . F. WARD 


 MR . J. OWEN 


 O COMMITTEES INSTITUTIONS , 


 LITERARY ARTISTIC SOCIETY CONCERTS , 


 MANCHESTER CONCERT AGENCY 


 YUM 


 PROFESSIONAL NOTICES . 


 MDLLE . JOSE D ’ ARGONVILL E , R.A.M 


 USIC SCHOOL . — CHURCH ENGLAND 


 XUM 


 N.W 


 CHOIRS , 


 _ — _ 


 RAM 


 3 LAND 


 TORIUL 


 443 


 HECKMANN QUARTET . 


 NEWINGTON CHARITY ORGANISA- 


 XUM 


 12 , LERNERS STREET , LONDON , W 


 CONCERT - GIVERS ENTREPRENEURS 


 GENERALLY . 


 CLERGY . 


 CHOIR - BOYS . 


 ORCHESTRAS CHOIRS . 


 444 


 XUM 


 BESET 


 445 


 YUM 


 446 


 VIOLIN BOW 


 MAKERS REPAIRERS 


 GEORGE WITHERS & CO 


 WHOLESALE IMPORTERS 


 MUSICAL STRINGS 


 FINE COLLECTION ITALIAN INSTRUMEN?7S 


 1 , ST . MARTIN LANE , LONDON 


 VIOLINS 


 CH . J. B. COLLIN - MEZIN , 


 VIOLIN , VIOLONCELLO , BOW MAKER , 


 ARTISTIC HOUSE 


 PARIS , 10 , RUE DU FAUBOURG - POISSONNIERE , 10 , PARIS 


 JAMES CONACHER SON S , 


 ORGAN BUILDERS , 


 BATH BUILDINGS , HUDDERSFIELD . 


 | REGENT HALL , LONDON 


 1 . PIANOFORTE & KEYBOARD INSTRU . 


 MENTS . 


 II . EXAMINATION PIANOFORTE TUNERS 


 P. CONACHER & CO , , 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD 


 ESTABLISHED 1854 


 SPECIFICATIONS FREE APPLICATION 


 CURE DEAFNESS . 


 PIANOFORTE ALBUMS 


 EDITED 


 BERTHOLD TOURS 


 XUM 


 TRU 


 RS , 


 1886 


 OYS 


 TON 


 CH 


 UNIVERSAL LOZENGES 


 BLE SPEAKERS SINGERS . 


 SIMPLY INVALUA 


 MRS 


 BASKER 


 MISS JESSIE JONES , R.A.M 


 MR 


 HENRY CROSS , R. A. M 


 PREPARED 


 J. A. BASKER , F.C.S 


 - SONGS 


 MALE VOICES 


 ACCOMPANIMENTS HORNS 


 COMPOSED 


 R. SCHUMANN 


 XUM 


 SACRED DUETS 


 SOPRANO CONTRALTO 


 COMPOSED 


 HENRY SMART 


 SONGS 


 COMPOSED 


 7 . STERNDALE BENNETT 


 : SHILLING ; 


 NOVELLO , EWER & CO 


 PRIMERS 


 JOSEPH BENNETT 


 READY 


 HECTOR BERLIOZ 


 FREDERIC CHOPIN 


 ROSSINI 


 CHERUBINI MEYERBEER 


 SHILI JING 


 PR ISPECTUS 


 ARRANGEMENTS 


 PIANOFORTE 


 GOUNOD REDEMPTION ... DERTI 5 


 ~ CHORAL SOC TETIES . 


 4 CO NING 


 SUPPLIED 


 SIXPENCE 


 CLASS I. , PRICE SIXPENCE ( 92 PAGES).—1 ° 74 


 CLASS II . , PRICE SIXPENCE ( 50 PAGES).—1873 


 448 


 TRINITY COLLEGE , LONDON 


 SESSION 1886 - 87 


 MICHAELMAS TERM COMMENCES SEPTEMBER 28 


 ALBUMS GERMAN SONG 


 SLLECTED , WORDS TRANSLATED ENGLISH , 


 FRANCIS HUEFFER 


 PRICE SHILLING ¢ SIXPENCE , 


 - SEVEN SONGS 


 BRAHMS 


 JOHANNES E 


 SONGS 


 SET POEMS ROBERT BURNS 


 


 GEORGE J. BENNETT . 


 VOCAL DUETS 


 SOPRANO CONTRALTO 


 COMPOSED 


 CIRO PINSUTI 


 ADMIRABLY SUITABLE 


 DRIVING ORGANS 


 FOLLOWING CHURCHES 


 CATHEDRALS PURPOSE 


 SERIES 


 EDITED ARRANGED 


 ALFRED WHITTINGHAM 


 PSALMS 


 POINTED CHANTING 


 


 REV . DR . TROUTBECK 


 CLOTH , SHILLING 


 PUBLISHED , NEW EDITION . 


 


 CATHEDRAL PSALTER 


 


 CANTICLES PROPER PSALMS 


 SET APPROPRIATE CHANTS . 


 NE W EDITION , ADDITIONS 


 WORDS ANTHEMS 


 EDITED 


 


 D.D 


 XUM 


 


 449 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 AUGUST 1 , 1886 


 LONDON MUSICAL SEASON . 


 XUM 


 450 


 MUSICAL TIMES 


 XUM 


 ICCESS , 


 SG 


 451unwearied exertions maintain character 
 choir 

 Concert Bach Choir Dr. Villiers 
 Stanford appearance Conductor 
 Society , place Mr. Otto Goldschmidt , 
 warmly received . ‘ rendering Bach 
 Cantata , ‘ Gott ist mein Konig , ” per- 
 formed Concerts , fully maintained high 
 position choir , Beethoven rarely - heard 
 Cantata ‘ ‘ Elegischer Gesang ” interesting 
 novelty . success Concert 
 reason believe stability 
 Society fully established 

 glad find enterprise Mr. 
 Austin giving series * Patti Concerts , ” 
 Royal Albert Hall , liberally rewarded . 
 appearance Madame Patti anxiously 
 looked London season ; 
 state operatic stage , 
 exceilent performances , extremely 
 doubtful wish year 
 gratified . Apart attraction Madame 
 Patti , , respect pro- 
 grammes reflected utmost credit Mr. 
 Austin management , shall pleased 
 welcome renewal Concerts season 




 452 


 RUSSIAN MUSIC . 


 454 


 XUM 


 TT , 


 XUM 


 MUSICAL TIMES 


 OL NAV VEAININLINH 


 456 


 XUM 


 457 


 GREAT COMPOSERS 


 YUM 


 458 


 XUM 


 LT 


 SUCCESS 459 

 come Schubert excursion 
 native city . took place September , 1527 , 
 Gratz , resided avery musical family 
 named Pachler . ‘ head house , Carl Pachler , 
 advocate profession , carried 
 business brewer , ‘ ‘ ran ” hotel . wife , 
 Maria , spoken woman great beauty 
 accomplishments . Beethoven praised 
 performances Pianoforte Sonatas , 
 friendly household 
 visited 1827 illness death . 
 Pachlers hospitable folk , loved re- 
 ceive artists roof . wonder 
 find anxious entertain Schubert long 
 privilege fell lot . master , 
 , promised 1826 , 
 friend Jenger wrote confidently Madame 
 Pachler point : ‘ possibly leave 
 autumn , friend Schubert events , 
 painter Teltscher , dear Madame , 
 appearance . ” Schubert clung Vienna , 
 time Jenger wrote positive : 
 “ Friend Schubert determined travelling 
 Gratz year , accompany 
 plan sure fall , year . ” 

 occasion said : ‘ * Schubert , 
 knowing , gracious lady , sends 

 assurance devotion , delighted 

 acquaintance earnest worshipper 
 Beethoven . God grant unanimous wish 
 come Gratz year fulfilled . ” 
 wrote : ‘ ‘ best plan , think , 
 set Gratz beginning month 
 September . sure bring Schubert , 
 second friend , Teltscher , lithographer . ” 
 Presently ( June 12 ) Schubert despatched 
 letter Madame Pachler 

 gracious Lady,—Although ata loss 
 understand deserving hands friendly 
 invitation forwarded letter sent Jenger , 
 supposing power 
 sort return kindness , 
 accept invitation 
 enable Gratz , praises 
 place familiar , 
 honour personally acquainted 
 . remain , sentiment respect , 
 obedient servant,—Franz ScuuBeRrtT 




 XUM 


 460 


 XUM 


 461 


 MUSICAL DEGREES 


 LONDON 


 UNIVERSITY 


 XUM 


 462 


 BACHELOR DEGREE 


 INTERMEDIATE . FINAL . 


 3 3 2 


 6 3 2 


 6 2 


 5 . 


 DOCTOR DEGREI 


 ISSI 


 1884 2 2 


 1885 2 2 


 463 


 SUC , OC 


 KUM 


 ORIGIN HARMONY 


 RET 


 464 


 XUM 


 TEES , 


 XUM 


 465 


 466THE MUSICAL TIMES.—Aucusr 1 , 1886 

 AttTHouGcu programme Leeds 
 Festival appeared , known , 
 sketch programme sources , 
 generally accurate idea days ’ work . 
 following distribution relied upon:—Wednes- 
 day morning , October 13 , ‘ Israel Egypt ” ; Wednes- 
 day evening , Mackenzie “ Story Sayid , ” selection 
 ‘ Cosi fan tutti , ” Prize Song “ Meistersinger , ” 
 Overture ‘ Der Fliegende Hollander . ” Thursday 
 morning , Bach Mass B minor ; Thursday even- 
 ing , Dr. Stanford ‘ ‘ Revenge , ” Beethoven Sym- 
 phony inC minor , Mendelssohn “ Walpurgis Night . ” 
 Friday morning , Dvordk “ Saint Ludmila ” ; 
 Friday evening , Mendelssohn ‘ ‘ Scotch ” Sym- 
 phony , new Overture F. kK. Hattersley , 
 Overture ‘ Euryanthe , ” Schumann ‘ “ Advent 
 Hymn . ” Saturday morning , Sullivan ‘ Golden 
 Legend ” ; Mendelssohn “ St. Paul ” ; 
 Saturday evening ( extra Concert ) , “ Elijah . ” 
 vocal artists engaged following : — Mesdames 
 Albani , Hutchinson , Anna Williams , Patey , Damian , 
 Hilda Wilson ; Messrs. Lloyd , McGuckin , McKay , 
 Santley , King , Brereton , Watkin Mills . 
 glad know prospects Festival 
 excellent . nearly thousand - guinea 
 serial tickets sold — increase 
 Festival . 494 guarantors , 
 answerable £ 20,000 ; 

 




 XUM 


 XUM 


 467 


 ROYAL ITALIAN OPERA 


 MR . CUSINS ’ CONCERT 


 RICHTER CONCERTS . place June 28 , St. James Hall , programme 

 exclusively occupied Beethoven Mass D. 
 expected Bruckner promised Symphony 
 given , consequent postponement previous 
 Concert ; reasons , doubtless , good ones 
 Mr. Richter baulk anticipations 
 audience , Bruckner remain unheard . time 
 come , probably , autumn series , , , 
 worse months ’ delay . 
 performance Mass best given 
 Mr. Richter guidance country , fact 
 better acquaintance difficult text , 
 mainly , , wise course adopted 
 strengthening chorus addition number 
 voices drawn Leeds Festival Choir . fine , 
 sonorous tones Yorkshire singers , combined 
 characteristic energy attack sustaining power , 
 effected marvellous improvement . , save Leeds 
 1883 , heard Beethoven intricate trying 
 choral music given better effect . Londoners 
 stimulated presence Leeds people , 
 healthy emulation set , times results 
 startling regards power dash . need scarcely add 
 deep impression , 
 weaken common conviction Mass impossible 
 point view embracing thoroughly satisfactory 
 interpretation . orchestra important share 

 work challenging adverse criticism , 
 solo quartet — Miss Marriott , Miss Lena Little , Mr. Winch , 
 Mr. Henschel — got arduous task 
 sanguine expected , looking 




 ROYAL COLLEGE MUSIC . named opera geniality marked 

 performance , showing decided improvement 
 matter intonation , second hearing 
 confirmed opinion impolicy assigning 
 leading female voice operatic - contralto 
 calibre . Concert 14th ult . noticeable 
 merely happy choice pieces performed , 
 exceedingly meritorious manner 
 executed . Beethoven String Quartet D ( Op . 18 , . 3 ) , 
 Concert opened , given refinement 
 precision , players , Mr. Sutcliffe , Miss 
 Donkersley , Mr. Kreuz , Mr. Squire performing 
 balance worthy old hands . occasion 
 speak terms high commendation 
 Miss Kellett capabilities pianist , rendering 
 Schumann exacting ‘ * Etudes Symphoniques ” marked 
 greater breadth warmth expression 
 manifested . Nervousness obviously hampered 
 start , earlier 
 portion work admirably given . Miss Anna 
 Russell large voice , tones sym- 
 pathetic quality , intonation excellent , style 
 pure , grounds decidedly 
 satisfactory soprano singers heard 
 Royal College . occasion rendering , 
 English , Giovannini ‘ Willst du dein Herz mir 
 schenken ” ( generally incorrectly ascribed Bach ) 
 charming performance . welcome number 
 programme aselection Schumann ‘ “ Mahrchen- 
 Bilder , ” Mr. Kreuz , promising young viola player , 
 heard advantage . Andante con variaziont 
 Spohr Double Quartet ( Op . 89 ) Mendelssohn 
 Trio C minor served exhibit proficiency 
 College instrumentalists , Messrs. Price , Ridding , 
 Fischer entered great spirit dramatic 

 XUM 




 ALTO 


 XUM 


 1 . — _ _ _ — 


 XUM 


 XUM 


 ANTHEM VOICES 


 ALTO 


 TENOR 


 ORGAN . 


 @ — 96 


 CYTES 


 XUM 


 SS SSS 


 XUM 


 XUM 


 MR . HENRY LESLIE CHOIRg 
 TRUE new , successful policy | Jacques , ” Goring Thomas graceful “ Nuit d’été”’—the 
 adopted season , Mr. Leslie secured artists | best style ; Madame Haas contributed 
 highest eminence support programme | pieces Chopin , 
 Concert 30th ult . Madame Albani contributed | 
 y , _ rer tr , oo > : 2ctre ’ ? ey > 7 ? aye “ . 2 vO . , rer Tr ’ Wwrrer 7 
 lovely prayer * * Spectre Bride , ” * “ Let | MUSIC WEST 

 bright Seraphim ” ; Mr. Santley sang Purcell fine air | 
 “ Let dreadful engines , ” Mr. Lloyd Preislies | 
 ‘ Die Meistersinger ” ; M. de Pachmann played annual Festival Bristol Church Choral Union 
 pianoforte solos Raff Chopin . scheme | took place Cathedral evening rst ult . , 
 partook nature high - class miscellaneous Concert , | larger number choirs took 
 famous choir longer sufficient attrac- | previous occasion , manner musical 
 tion , blame attach conductor | service rendered showed commendable progre 
 seeking win public means . open | choirs , numbering aggregate 720 voices , 
 question fault lie nearer home . ! follows : — Cantoris , Fishponds , Frenchey , Ashton Gate , 
 season , hesitation need felt stating | St. John ( Bedminster ) , St. Paul ( Bedminster ) , St. 
 Leslie Choir requires reorganisation | Savidur ( Woolcott Park ) , Emmanuel ( Clifton ) , St. Mary 
 maintain position leading | ( Tyndall Park ) , St. Paul ( Clifton ) , Christ Church 
 choral bodies metropolis . Voices | ( Cjifton ) . Decani , Eastville Mission Church , St. Michael 
 , delicate way , glance | ( Bishopston ) , Horfield , St. Mark ( Lower Easton ) , St. 
 orchestra suggested reason pitch } Barnabas ’ , St. Andrew ( Montpelier ) , St. James , St. 
 maintained , quality tone good . | George ( Brandon Hill ) , St. Augustine , St. Stephen , 
 selections familiar repertory , new part-| St. Nicholas , St. Mary Redcliffe . Preces 
 songs included programme ; ‘ “ Rove Responses Tallis ; Magnificat Nunc dimittis 
 Rhine , ” Mr. J. C. Ward , impression , | sung J. Barnby E G ; anthems 
 owing imperfect rendering ; * | « Rejoice Lord , ” John Redford , 
 peace , ” Mr. Berthold Tours , charming little com- unaccompanied admirably rendered , Dr. Garrett 
 position , assuredly heard . “ Praise Lord , ” tenor solo taken 
 Mr. Morgan . Mr. John Barrett , Conductor 
 PRINCE HALL . year , directed singing , Mr. George Riseley 
 Organist . manner service rendered 
 Tue Chamber Concerts given Italian artists , | deserving high praise , 
 Signor Cesi , Signor Papini , Signorina Barbi , | thoroughly appreciated large congregation . 
 12th 17th ult . , received greater attention ] evening 22nd ult . number ladies 
 taken place favourable period gentlemen , members musical profession , assembled 
 year . ‘ idea present examples ] invitation Imperial Hotel , White Ladies ’ Road , 
 chamber music , vocal instrumental , historical order . | Bristol , hear Mr. J. Brotherhood , C.E. , Canada 
 Concert , solos fewer seventeen | ( native Bristol ) , explanation 
 composers included , commencing Frescobaldi , | * * Technicon , ” apparatus hand development 
 1587 - 1654 , including D. Scarlatti , Couperin , Rameau , | pianoforte playing . Mr. John Barrett having introduced 
 Bach , Handel , Graun , Jomelli , Mozart , Rossini , | Mr. Brotherhood , said apparatus 
 Beethoven Kreutzer Sonata effective finale . | new , use , 
 second Concert thirteen modern composers represented , | explained illustrated Royal College South 
 work importance Schumann Sonata | Kensington Guildhall School Music . 
 minor piano violin , Op . 105 . Signor Cesi | desired accomplish means invention 
 leading professor pianoforte Naples Con- | hand sensitively responsive brain , 
 Servatoire , executant great ability . gave | struggling pianoforte player , tried overcome 
 satisfaction , , pieces requiring light | difficulties modern compositions , hand 
 delicate treatment , forward great deal obey mind . ‘ Technicon , ” _ developing 
 superfluous energy , tone hard un- | technique , explained , merits developing 
 pleasant . pianists fall Ercles ’ vein|the hand pianoforte playing shown Mr. 
 damage artists . Signor Papini | Brotherhood , studied muscular action , 
 Capacity violinist known need discussion , | displayed resources invention manner 
 occasion speak in|that greatly interested auditors . stated 
 favourable terms vocal powers Signorina Barbi.| best way use apparatus practising 
 powerful mezzo - soprano voice trained , | instrument , executive power kept 
 sings expression . audiences ] advance interpretive power . reply question 
 Concerts consisted mainly foreigners , whoexpressed | greatest executants succeeded 
 satisfaction demonstrative fashion . independently mechanical aid , said probably 
 , Liszts Thalbergs found ? 
 : : WET ANIge EVAN et stated , incidentally , means ‘ * Techni- 
 MR . SAM FRANKO CONCERT . con , ” schiiveuess touch blind 
 co - operation artists Madame Haas , | persons increased . close Mr. Brother- 
 Miss Carlotta Elliott , Mr. Henschel , Mr. Sam Franko , | hood remarks , thanks meeting tendered 
 clever violinist New York , enabled offer } motion Mr. George Riseley , seconded Mr 

 FrRoM CORRESPONDENT 




 476 


 MUSIC GLASGOW WEST 


 SCOTLAND . 


 MUSIC OXFORD . 


 ( CORRESPONDENT 


 WELSH EISTEDDFODAU 


 XUM 


 477 


 TORONTO MUSICAL FESTIVAL 


 478 


 _ _ _ _ .. 


 OBITUARY 


 XUM 


 COE . 


 479 


 XUM 


 480 


 MUSICAL TIMES 


 XUM 


 XUM 


 481 


 REVIEWS482 MUSICAL TIMES.—Avausr 1 , 1886 

 tion books harmony contain precisely |are extended sense , nearly ‘ key . 
 unscientific assertions , adhere relics } boardish ” pianistic Germans . Scientific men 
 exploded theories , local general , primes upper | infallible reason outside logic 
 partials heard , Helmholtz | facts . Helmholtz objects term “ natural harmony , ” 
 written . fact means unintelligible ; | , humbly think , perfect justice . 
 years ago Mr. Ellis expressed opinion Helm- | fourth edition talks “ natural scale ” ; whilst 
 holtz * ‘ sounded knell equal temperament ' ’ ; /translator intensely interesting chapter non- 
 , 1885 , Mr. Ellis second edition naturally| harmonic scales , asserts 
 suggests questions — knell | thing ‘ natural scale , ” leads 
 sounding ? funeral procession | readers conclusion thing 
 , defunct friend formally | asa scale . takes great pains under- 
 consigned tomb ? partial answer queries | props scale possessed — fixed 
 found fourth German edition ‘ * Sensations of| tetrachordal sounds . Helmholtz scolds 
 Tone , ” appears page 428 present English| piano , admit mechanism 
 edition . Helmholtz says : ‘ ‘ Musicians contested , in| basis system music , feel tempted 
 dogmatic manner , correctness propositions | ask enharmonic keyboards , different systems 
 advanced . ” ( Anallusion proposal harmony | tuning keyed instruments , basis 
 taught pedagogically principle | system music ? Helmholtz 
 intonation . ) ‘ donot doubt fora moment , ” continues , | suggest ; havea 
 ‘ antagonists perform | suspicion , eyes English translator , 
 good music , ear forces play } duty man musician attend 
 better intended , better | ‘ ‘ duodenes . ” ” Mr. Ellis chastises author 
 case actually carried regulations the| notes signed “ translator ” ; 
 school , played exactly Pythagorean tempered | scarcely kindly Helmholtzian doctrine , 
 intonation . hand , generally possible | essential basis music melody . contrary , 
 convince oneself , writings , ! warns enter appendix approach 
 writers taken trouble mathe- | ‘ Duodenarium , ” ‘ ‘ harmony chief considera- 
 matical comparison tempered intonation . / tion . ” , complaints Helmholtz 
 invite hear , uttering | directed musician teacher pedagogue , 
 judgments founded imperfect school theory , con- | transferred music—-that , music 
 cerning matters personal ! pericd , Beethoven downwards . Helmholtz 
 experience . time observations | told equal temperament indispensable modern 
 , rate , glance literature period | music . , , musician asked teach 
 equal temperament introduced . | ‘ pedagogically ’ intonation , student 
 organ took lead musical instruments | theoretic interest ? musicians 
 tempered . pianoforte , doubtless , | practical men sit write manuals harmony 
 useful instrument making acquaintance musical | adapted psalmody , toa system practically 
 literature , domestic amusement , accompanying | non - existent instrumental music ? 
 singers . artistic purposes , importance | hazarded opinion years ago , cacophony 
 require mechanism basis | Wagnerian orchestra , far representing 
 system music . ” | music future , sign decline , evening 

 protest German “ pianism ” keyboard | music age ; probable outcome 
 theories , brutal logic writing voice - parts F flat , | Wagnerian drama return respect 
 boldly putting staffsignature E natural major | old worship sensation ; , dramatic declama- 
 organ pianoforte accompaniment , passage | tion accompanied simple harmonies rendered 




 XUM 


 XUM 


 XUM 


 XUM 


 455Te Deum Laudamus . Frederick Tolkien . 
 | Spottiswoode Co 

 ordinary setting Ambrosian Hymn 
 church use , elaborate work - pages , | 
 composed commemoration Queen Jubilee . | 
 examination music unfortunately leads con- | 
 clusion composer wasted time labour . | 
 ideas tonality vaguest , - writing | 
 shows lamentable ignorance capacity | 
 human voice , , like Beethoven , regards 
 ordinary mechanical instrument . im- 
 possible speak cf Mr. Tolkien Te Deum musicianly 
 achievement , bears unmistakable traces natural talent . 
 impressive beautiful phrases 
 discovered , like oases desert , encourage 
 hope careful study composer produce 
 worthy hearing 

 Send Thy Light . Anthem Whitsuntide ge : 
 use 




 SATCC 


 MUSICAL TIM 


 486FOREIGN NOTES 

 CORRESPONDENT writes Berlin Allgemeine Musik 
 Zeitung Rome : — ‘ Mozart ‘ Don Giovanni ’ per- 
 formed time June 20 , achieved 
 brilliant — fiasco . feeble attempts applause 
 speedily drowned general demonstrations disap- 
 proval , chorus hissing yells funeral 
 dirge accompanied Mozart masterpiece 
 grave , far capital concerned . rejection 
 ( similar accorded time Beethoven 
 ‘ Fidelio * ) ‘ Don Giovanni , ’ Roman public 
 lay open charge vandalism , 
 undeniable fact greater lamentable 
 failure owing incredibly bad performance 
 work . chaste muse master 
 scarcely appeals taste modern Italians — 
 accumulation drastic effects required - - days — 
 lengthy secco recitatives , , little appre- 
 ciated . , incapacity shown 
 executants conductor rendering 
 work undoubtedly exculpates audience considerable 
 degree . 
 artistic training 

 g , sang title rd / e , efforts , 
 Nannetti , likewise - trained artist sang 
 Leporello , owing opera listened 
 end 

 furt musical 

 result indicated . 
 new Court Th 
 21st ec appro 
 ing perform : mce Gluck 
 preceded ve es lo rue ( dr 
 tr von Putlitz , , Herr oys Schmitt h 
 usic . second day ( Septembe 
 Maria Stuart ’’ given , thir¢ 
 icent concert - room attached buildin ris 
 augurated Beethoven Choral Symphony , ar 
 Bach , Handel , . n 
 structed entirely stone iron , replac 

 destroyed fire years , occasion 




 488both regards attendance pupils artistic | 
 results obtained 

 - known Girzenich Concerts Cologne , estab- | 
 blished late Ferdinand Hiller , | 
 direction Dr. Willner , resumed October . | 
 following works , , obtain hearing | 
 season:—Mendelssohn ‘ “ Elijah , ” Haydn | 
 ‘ * * Seasons , ” Bach Passion Music , Symphonies ‘ | 
 Beethoven , Schumann , Niels Gade 

 music festival recently held Dortrecht ( Holland ) | 
 proceedings included highly successful performance | 
 Albert Becker Grand Mass B flat minor , 
 direction Herr W. Kes , presence 
 composer 

 International Theatre planned Berlin 
 performance alternately remarkable dramatic 
 lyrical productions civilised nations 

 Herr Xaver Scharwenka , - known pianist 
 composer residing Berlin , conduct series 
 concerts German capital coming winter , 
 course number interesting vocal 
 instrumental works Beethoven , Liszt , Brahms , Berlioz , 
 Wagner produced 

 German theatre Prague , direction 
 Herr Angelo Neumann , complete “ cycle ” Mozart 
 operas announced place October , 
 followed , November , similar scheme regard 
 Shakespeare historical dramas 




 CORRESPONDENCE 


 M. SAINT - SAENS NORMAL METRONOME . 


 EDITOR “ MUSICAL TIMES 


 XUM 


 YUM 


 489 


 GATE 


 HOUSE . 


 TRANSLATION . 


 MUSICAL SCALES 


 


 NATURAL 


 


 EDITOR MUSICAL TIMES 


 5 27 


 OSEPH GOOLD 


 _ STRU 


 MUSICAL 


 IMENTS 


 TIMES 


 NEGLECTED SOLO 


 EDITOR “ 


 490 


 ADELAIDE MUSICAL CITY 


 TIMES 


 MUSICAL 


 ASS 


 REGISTER 


 UNKNOWN IRISH TUNE 


 EDITOR “ MUSICAL TIMES , ” 


 CN 


 LK | 


 MARSEILLAISE HYMN . 


 EDITOR “ MUSICAL TIMES . ” 


 THIRVALD LAURSEN , 


 CORRESPONDENTS 


 BRIEF SUMMARY COUNTRY NEWS 


 STOCKTON - - TEES.—A S 


 492 


 MONTH 


 TAMMERS , I. HERBERT . — 


 FELLOW . 


 MENDELSSORN ) 


 EW , REVISED , ENLARGED EDITION 


 CATHOLIC HYMNS 


 ACCOMPANYING TUNES 


 EDITED 


 A. E. TOZER . 


 1886 


 HARVEST ANTHEMS & HYMNS 


 TW ] SLVE HY ) INS TUNES 


 


 HARVEST 


 SELECTED “ HYMNARY . ’ 


 PRICE PENNY 


 HARVEST 


 JUBILEE CANTATA 


 HARVEST CANTATA ) 


 SOLO VOICES , CHORUS , ORCHESTRA 


 COMPOSED 


 C. M. VON WEBER 


 WINCHESTER 


 HARVEST ANTHEM 


 PRAISE LORD 


 NEW HARVEST ANTHEM . 


 GREAT LORD 


 E. A. SYDENHAM 


 O THANKS 


 NEW ANTHEM HARVEST FESTIVALS . 


 O MEN WOUL ) PRAISE LORD 


 DR . JOSEPH C. ' B RIDGE , M.A. , 


 N EW HARVEST ANTHEM 


 EYES WAIT THEE 


 ANTHEM HARVEST GENERAL USE 


 O GOD , SOUL THIRSTETH 


 COMPOSED 


 HARVEST 


 THANKSGIVING MARCH 


 J. BAPTISTE CALKIN 


 ARVEST FESTIVAL ANTHE 


 NEW HARVEST CAROL . 


 ARVEST ANTHEM.—I MAGNIF Y 


 TENTH THOUSAND 


 RUTH 


 HARVEST PASTORAL 


 ALFRED R. GAUL 


 SUITABLE 


 THANKSGIVING SERVICES 


 VOCAL SCORE , OLD NOTATION 


 PERFORMED TOWN HAL ~ BIRMINGHAM , 


 SEVEN OCCASIONS 


 HARVEST SERVICES , 


 NEW HARVEST HYMN . 


 > . 


 ARIADNE 


 DRAMATIC CANTATA 


 WORDS 


 JAMES SMIETON , M.A 


 MUSIC 


 J. SMIETON 


 SCOTSMAN . 


 SCOTTISH NEWS 


 DUNDEE ADVE RTISE R , 


 WEATHERLY WRITTEN 


 | 


 CANTATA 


 CHOR AL SOCIETIES 


 NARC 


 SSUS ECHO 


 COMPOSED 


 MAID ASTOLAT 


 C. SWINNERTON HEAP 


 PATTISON , 


 MASS MALE VOICES 


 COMPOSED 


 ODOARDO BARRI 


 OW LEARN PLAY PIANOTHE MUSICAL TIMES.—Avcusr 1 , 1886 . 495 

 H 3 ] sats 
 NEW FORE IGN PUBLICATIONS . | London Music Publishing Co. Publications 
 een ee ee s. d. SASY HARV MUSIC . 
 AGGHAZY , C.—Rondo ’ ongharese . Pianoforte Duet . | 4IGHT HA RV EST HYMNS . Words 
 Op . 18 , No.1 .. “ “ ee ve 3 9 ) Dea S , Citys Crarxe ; Music Dr. Dykes , Dean 
 — Marcia . Pianoforte Duet . “ Op . 18 . 2 wee ee 3 OFT aNGRAN , ree Mr. A. H. Br ; Ww 
 -Variations original Pianoforte 2 0 } 75 ner y > . 
 A.—Romance . Op . 17 . Flute Pianoforte 3 o| ‘ hymns , include fine Processional , 
 ETHOV EN . — " Der Kuss . ” Op . 123 . Song ( German words ) 1 0 ; written , suitab set : ant d mus ic win 
 BELT JENS , CH.—“‘A Beethoven ” Poem ( Frenc net 1 assurealy 5 . 

 BIEHL , A. ie hree easy melodious Sonati 




 Y 1 


 WEL VE 


 LONDON : NOVELLO , EWER CO . 51 , GREAT MARLBOROUGH STREET 


 496 


 HYMNARY 


 BOOK CHURCH SONG 


 LONDON : NOVELLO , EWER CO 


 BRISTOL TUNE - BOOK 


 MATCH EDITION , 


 BOOK PSALMS ( BIBLE VERSION ) , 


 ANGLICAN HYMN - BOOK 


 REDUCED THREEPENCE , 


 A. MACFARREN - ANTHEMS 


 SECOND SERIES , 


 " ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 OULE COLLECTION 527 CHANTS , 57 


 PSALTER , PROPER PSALMS , HYMNS 


 OULE DIRECTORIUM CHORI ANGLI 


 OULE DIRECTORIUM CHORI 


 ANGLI 


 ORDER HOLY COMMUNION 


 NEW ANTHEM . 


 TE DEI 


 JUBILA 


 MAGNI 


 HEAR ! 


 OFFI 


 TE 


 MA 


 


 RCH , 


 VELLO , 


 > » 57 


 REDUCED PRICES 


 OUSELEY 


 POINTED 


 ND MONR 


 PSALTER 


 CHURCH MUSIC 


 


 DR . FRANK BATES 


 


 ( OFFICE HOLY COMMUNION 


 CHURCH MUSIC 


 - SONGS , 


 GEORGE KETT . 


 TE DEUM JUBILATE DEO 


 SET MUSIC E MAJOR 


 


 JOSEPH MOSENTHAL , 


 FESTIVAL SETTINGS 


 


 MAGNIFICAT NUNC DIMITTIS 


 ACCOMPANIMENTS ORGAN ORCHESTRA 


 ERLIOZ TE DEUM . 


 ANCIENT PLAI 


 


 ATHANASIAN CREEL 


 J. STAINER 


 N - SONG 


 498 


 ALBUM ITALIAN SONGS . 


 - SONGS SET ITALIAN WORDS , 


 COMPOSED 


 ANGELO MARIANI . 


 


 D. AVIS . 


 SONG 


 ZINGARA 


 L " ASS O ' BRAY 


 - SONGS , S.A.T.B. 


 STUDY 


 OIANOFORTE STUDENT DAILY 


 MUSIC 


 MONTHS 


 VOCAL DUETS SCHOOL USE 


 MUSIC 


 ALFRED R. GAUL 


 - SONGS 


 GLEES , 


 TILTMAN . 


 SONGS H. T. 


 HA OMEWARD BOUND . 


 | ANDREWS 


 JACK HORNER , 


 KARLUS ’ WALTZES . 


 JERUSA 


 RDY 


 499 


 PIANOFORTE 


 COMPOSED 


 WM . ARTHUR BLAKELEY . 


 NEW WORK ORGAN 


 FAVOURITE AIRS ORGAN 


 ARRANGED 


 DR . W. J. WESTBROOK 


 DESPISED 


 SHORT SETTINGS 


 


 JFFICE HOLY COMMUNION 


 PAROCHIAL GENERAL USE 


 EDITED 


 GEORGE C. MARTIN 


 _ NOVELLO , EWER COV 


 ALBUMS VIOLIN PIANOFORTE 


 MARCHES 


 F. MENDELSSOHN BARTHOLDY . 


 TRANSCRIBED 


 BERTHOLD TOURS 


 THIRTY MELODIES 


 COMPOSED 


 BERTHOLD TOURS . 


 TRANSCRIPTIONS 


 MENDELSSOHN “ ELIJAH ” 


 BERTHOLD TOURS . 


 TRANSCRIPTIONS 


 GOUNOD “ MORS ET VITA 


 BERTHOLD TOURS 


 LONDON : NOVELLO , EWER CO 


 VOLUNTARIES 


 } ARRANGED 


 HARMONIUM 


 


 J. W. ELLIOTT . 


 ROBERT SCHUMANN 


 PIANOCFORTE WORKS 


 AGNES ZIMMERMANN . 


 ALBUM YOUNG 


 ALBUM LEAVES 


 20 PIECES 


 SCENES CHILDHOOD 


 FOREST SCENES 


 22 PIECES 


 500 


 ORIGINAL 


 OTTO DIENEL 


 CHURCH ORGANIST 


 COLLECTION PIECES USE 


 DIVINE SERVICE 


 BUR HHO DN HD 


 10 


 300 K V. 


 GEORGE C. MARTIN 


 MENDELSSOHN ORGAN MUSIC 


 SONATAS 


 PRELUDES FUGUES 


 SOFT VOLUNTARIES | 


 ORGANCOMPOSED 
 GEORGE CALKIN . 
 Books , price Shillings Sixpence . 
 London : Novetto , Ewer Co 

 Schumann . 
 Beethoven . 
 Schubert . 
 Handel . 
 Spohr . 
 Schubert . 
 Handel . 
 Schumann . 
 Beethoven . 
 Scarlatti . 
 Schumann . 
 Schumann . 
 Bach 

 ORGAN WORKS 




 JOHN SEBASTIAN BACH 


 EDITED 


 300 K IIL — PRELUDES , FUGUES , TRIO ... 4 , 


 PEDA 


 NEW REVISED EDITION 


 AIRS 


 MENDELSSOHN “ ELIJAH ” 


 ARRANGED ORGAN 


 GEORGE CALKIN 


 MANUALS 


 3EBEKAI 


 QUE 


 CRUSADE 


 REDEMP ’ 


 MORS ET 


 GALLIA — 


 MES 


 CRE 


 - DAY . 


 ROSE | 


 ATHALIE- 


 ELIJAH — } 


 HEAR 


 1 MARY 


 501 


 MYRTHEN 


 MYRTLES 


 CIRCLE SONGS 


 COMPOSED 


 ROBERT SCHUMANN 


 PIANO & HARMON [ UM 


 ACCOMPANIMENTS 


 SCHUBERT SONGS 


 SELECTED , EDITED , TRANSLATED 


 NATALIA MACFARREN 


 PRICE SHILLING SIXPENCE 


 SONGS 


 MEZZO - SOPRANO VOICE 


 CONTENTS 


 SONGS 


 


 CONTRALTO VOICE 


 CONTENTS 


 = . AYLWARD 


 WINDEYER CLARK 


 WINDEYER CLARK 


 WINDEYER CLARK 


 UND 


 NUNN DN 


 SONGS 


 SOPRANO TENOR 


 CONTENTS 


 LONDON : NOVELLO , EWER CO 


 CHILD GARLAND | DOUBLE SCALES 


 COLLECTION | SYSTEMATICALLY FINGERED 


 COMPOSED 


 FRANZ ABT . EXISTING PIANOFORTE SCHOOLS 


 | 


 SUPPLEMENTAL 


 JUVENILE SONGSTER : BOOK EXERCISES 


 USE 


 NEW REVISED EDITION |'LEARNING SING SIGHT 


 CONSISTING 


 COMPILED ARRANGED 


 THIRTY - SONGS | HENRY GADSBY . 


 NINETY - 


 


 - SONGS | CHOIRS SCHOOLS 


 | 


 | JAMES GREENWOOD 


 PARTS 


 CHILDREN OLD & YOUNG 


 COLLECTION 


 - SOLFEGGI 


 MUSIC COMPOSED 


 CAROLINE WICHERN 


 COLLECTION SELECTED 


 ROUNDS , CATCHES , CANONS DURANTE , HANDEL , LEO , SCARLATTI , STEFFANI 


 


 REV . J. POWELL METCALFE , M.A. JAMES HIGGS 


 ACCOUNT CHORAL SERVICE 


 DEVELOPMENT MODERN MUSICAL INSTRUMENTS | 


 ANCIENT TYPES , PRACTICAL SUGGESTIONS ORGANISTS 


 SELECTION TREATMENT CHURCH MUSIC 


 STAD 


 GOD 


 GOD 


 GOD ’ ! 


 S 


 0 LIG 


 BIDE 


 ASTI 


 MAG 


 THOL 


 JESU , 


 WHE 


 REBE 


 | 


 ENGE 


 MOU 


 MASS 


 MAS : 


 RUIN 


 SIR \ 


 EXH1I 


 ASO 


 ROCI 


 


 JEP 


 REOI 


 THIRI 


 


 


 ZION . 


 SPRIN 


 CHRIS 


 


 ORPH 


 TI 


 NGENI 


 DE PR 


 MESSI 


 


 SAV ] 


 DAUG 


 * GALL 


 


 XUM 


 MERS 


 MERS 


 ES 


 ANI 


 MERS 


 NT 


 O 


 USIC 


 XATORIOS , C 


 TATAS , MASSIPRICE SHILLING 

 THOMAS ANDERTON . 
 NORMAN BARON , 
 WRECK HESPERUS , 
 E. ASPA . 
 GIPSIES . 
 ASTORGA . 
 STADAT MATER . 
 BACH . 
 GOD LOVED WORLD . 
 GOD GOETH SHOUTING . 
 GOD TIME BEST . 
 SPIRIT HEAVINESS . 
 0 LIGHT EVERLASTING . 
 BIDE . 
 STRONGHOLD SURE . 
 MAGNIFICAT . , 
 THOU GUIDE ISRAEL . 
 JESU , PRICELESS TREASURE . 
 GOD RECALL SPIRIT . 
 . BARNBY . 
 REBEKAH . 
 BEETHOVEN , 
 CHORAL FANTASIA . 
 ENGEDI . 
 MOUNT OLIVES . 
 MASS , C. 
 MASS , C. 
 RUINS ATHENS , 
 SIR W. STERNDALE BENNETT . 
 EXHIBITION ODE , 1562 . 
 J. BRAHMS . 
 ASONG DESTINY . 
 J. F. BRIDGE . 
 ROCK AGES , 
 E. BUNNETT . 
 DEEP ( Psam 139 ) . 
 CARISSIMI . 
 JEPHTHAH . 
 CHERUBINI . 
 REQUIEM MASS , C MINOR . 
 MASS , ( Coronation ) . 
 FOURTH MASS , C 

 SIR M. COSTA 




 DREAM . 


 NIELS W. GADE . 


 ZION . 


 CHRISTMAS EVE . 


 ERL - KING DAUGHTER . 


 GLUCK , 


 ORPHEUS . 


 HERMANN GOETZ 


 WATERS BABYLON , 


 NG : NIA 


 CH . GOUNOD . 


 DE PROFUNDIS ( 130TH PSALM 


 SEVEN WORDS 


 SAVIOUR CROSS . 


 DAUGHTERS JERUSALEM . 


 * GALLIA 


 J. O. GRIMM . 


 SOUL ASPIRATION 


 XUM 


 LONDON : NOVELLO , EWER 


 HANDEL . 


 CHANDOS TE DEUM . 


 ODE ST , CECILIA DAY . 


 WAYS ZION . 


 MO 


 KING THAMOS . 


 MASS . 


 SEVENTH MAS 


 TWELFTH 


 MASS 


 DETTINGEN TE DEUM . , REOUL TASS . 


 UTRECHT JUBILATE . R. P. PAINE . 


 O PRAISE LORD . GREAT LORD 


 ACIS GALATEA . , 


 ACIS GALATEA 


 PERGOLESI . 


 E. PROUT 


 


 ARNBY , 


 HAYD 


 SPRING . SUMMER . MN . PURCELL . 


 WINTER . TE M JUBILATE , 


 ROMBERG . 


 SECOND MASS , C , TRANSIENT L1 


 DR . HILLER . MASS , B FLAT . 


 SONG VICTORY . MASS , C 


 H. HOFMANN . MASS , F 


 MASS , G. 


 SCHUMANN 


 HUMMEL 


 393 


 RNAL 


 MASS , B FLAT . PILGRIMAGE ROSE . 


 MASS , D. MIGNON REQUIEM 


 4 . JENSEN NEW YEAR SONG 


 A. JENSEN . > CTT . 


 FEAST ADONIS , E. SILAS 


 MASS , C. 


 ALICE 


 SONG 


 LEONARDO LEO . 


 DIXIT DOMINUS 


 MARY SMITH 


 ADVENT HYMN , “ LOWLY GUISE 


 LITTLE BALTUNG 


 SONG BALDER . ODE NORTH - EAST WIND . 


 SPOHR 


 OUTWARD BOUND . 


 DAY , JUDGMENT . 


 UNTO . A. GORING THOMAS , 


 HEAR PRAYER . E. H. THORNE . 


 MIDSUMMER NIGHT DREAM . VAN BREE , 


 MAN MORTAL . ST . CECILIA DAY . 


 PSALM 46 


 CHRISTUS , * MASS , G. 


 SONS ART . MASS , E FLAT . 


 MEYERBEER . 


 S. WESLEY . 


 DINIT DOMINUS 


 S. S. WESLEY . 


 O LORD , THOU ART GOD 


 VOICE - TRAINING EXERCISES 


 EMIL BEHNKE 


 CHARLES W. PEARCE 


 “ FREDERICK A. G. OUSELEY 


 PRINCIPAL PROFESSORS 


 SINGING . 


 LABLACHE 


 LABLACHE INSTRUCTION SINGING : — 


 B T 


 PIANOFORTE 


 CHAPPELL AMERICAN ORGAN TUTOR 


 LONDON : 


 CHAPPELL CO . , 50 , NEW BOND STREET , W 


 XUM 


 BARTON 


 FREDER