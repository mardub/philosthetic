


 MUSICAL TIMES 


 1 , 1867 


 MUSIC NUMBER . 


 WAKE THEE , DEAR 


 _ _ 


 ST . JAMES HALL 


 REGENT - STREET PICCADILLY 


 CONCERT 


 GIVEN 


 THURSDAY EVENING , 


 CHOIR ASSISTED 


 23RD 


 PROGRAMME . 


 I. Pianoforte Solo , 
 Miss ZIMMERMANN . 
 - Song , . “ O hush thee babie ” A. 8 . Sullivan . 
 - Song , « Fairy Song " A. Zimmermann . 
 Recit . Air , ‘ “ * Deeper deeper ” ‘ Handel . 
 . Mr. Stus REEVEs . 
 Anthem ( parts ) , ‘ Judge , O God ” Mendelssohn . 
 II 

 Motett ( double choir ) , “ wrestle pray ” e J. 8 . Bach 
 Song , ° ° Adelaida ° Beethoven . 
 Mr. Srus Reeves 

 - Song ° * Awake , awake ” Henry Leslie . 
 Scena , e “ Casta diva ” - Bellini . 
 Madame ViILpa 




 ADMISSION ... - SHILLING 


 46 


 PROFESSIONAL NOTICES . 


 SIGNOR J. ALFRED NOVELLO 


 MR . C. COWDEN CLARKE , 


 USICAL INSTRUMENTS VOLUN 


 MR . J. C. BEUTHIN 


 MR . JOSEPH SCATES 


 PATENT PORTABLE METRONOME 


 AVIES 


 LISHED BEST STYLE , MODERAMEMOORE 


 _ _ RORTHAND . — PITMAN PHONO- 


 AMPTO 


 BROOK 


 XUM 


 MUSICAL TIMES.—M 


 49 


 MUSICAL TIMES 


 — T 1 , 1867 


 TION 


 HG , 3 


 49 


 MUSICAL TIMES , 


 1 , 1867 


 MUSIC ENGLISH CHURCH . 


 XUM 


 50 


 MAJESTY THEATRE 


 ROYAL ITALIAN OPERA 


 AL 


 XUM 


 XUM 


 DAWN 


 TNL 


 TT 


 UT 


 ZF 


 OL 


 XUM 


 _ — _ 


 6 MUSIC 


 AL TIMES 


 MUSICAL KNOWLEDGE 


 THEORETICAL SERIES 


 CHERUBINI TREATISE COUNTERPOINT 


 FETIS ’ TREATISE CHOIR CHORUS SINGING , 


 BERLIOZ ’ TREATISE MODERN INSTRUMENTATION 


 DR , CROTCH ELEMENTS MUSICAL COMPOSITION ; 


 PRACTICAL SERIES 


 KALKBRENNER METHOD LEARNING PIANO- 


 TOVELLO ORIGINAL OCTAVO EDITIONS 


 HAYDN 


 20 


 SEASONS 3 


 HANDEL . 


 DEBORAH ... 


 SAUL .. 


 KING SHALL REJOICE ° HEART INDITING .. . 
 LET THY HAND STRENGTHENED 
 WAYS ZION MOURN 

 ALEXANDER FEAST eve sas oo 2 
 ACIS GALATEA ‘ oo | 
 ODE ST . CECILIA DAY ace oe FE 
 , bound vol . , scarlet cloth , 6s . 6d . 
 L'ALLEGRO , IL ales sieeamaaai ED IL MODE- 
 RATO 3 
 MENDELSSOHN . 
 PSALMS , complete volume 
 singly , : — 
 HART PANTS ( 42nd Psalm ) .. ooo J 
 COME , LET SING ( 95th Psalm ) 1 
 ISR : KE L EGYPT . CAME 
 ( 114th Psalm ) . Voices . 1 
 UNTO ( lléth Psalm ) ae ove 8 
 ST . PAUL 3 ; ns ie aus aa S 
 HYMN PRAISE 2 
 Hymn Praise * Hart Pants , ” bound 
 volume , scarlet cloth , 5s . ‘ 
 MOZART , HAYDN . BEETHOVEN . 
 Favorite Masses , Latin words , English 
 adaptation R. G. Loraryg , Esq 

 ANNO AQNoH 




 5 0o 

 MOZART TWELFTH MASS 20 3 6 
 HAYDN , IMPERIAL MASS zo 78 - 3 
 BEETHOVEN MASS C ... oe 2 0 3 6 § 
 Masses . volume , scarlet cloth , 7s . t 
 MOZART GRAND REQUIEM MASS ws ge 
 MOZART MASS ove ( 2 om 
 HAYDN ’ MASS « - 2 O 3 6 
 Masses added Mr. E. Hoses ’ Critical Essays , 
 extracted Musieal Times . ' 
 MOZART LITANIA DI VENERABILE t 
 ALTARIS ( E flat ) ove 2 6 3 0 
 MOZART LITANIA DI VENERABILE 
 SACRAMENTUM ( B flat ) oss aw a6 3 0 
 CHERUBINI . 
 REQUIEM MASS ( saline Latin 
 English words eo aa 20 3 6 
 SPOHR . 
 CALVARY ... ae 2 6 40 
 FALL BABYLON . 3 © g 0 4 
 JUDGMENT ... 2 0 4 0 
 original words , Professor TAYLor . 
 CHRISTIAN ' PRAYER os 8 © 3 0 
 GOD , THOU ART GREAT ... 10 
 Cc . M. VON WEBER . 
 MASS ( G ) , Latin English words .. 1 0 2 6 
 MASS E flat Ditto mA = 30 
 BEETHOVEN . | 
 ENGEDI ; , David W aaa ‘ ent 
 Olives ) Pe ue 2 6 30 YF 
 RO MBERG . x 
 LAY BELL .... « * 6 30 § 
 ROSSINI . | 
 STABAT MATER , Latin words , t 
 English adaptation W. Batu ° 6 3 0 

 Specimen pages Lists Works Novello ’ $ Octave Series 




 LONDON : NOVELLO CO . , 69 , DEAN STREET , SOHO ( W. ) , 35 , POULTRY ( E.C 


 SACRED HARMONIC SOCIETY 


 MR . HENRY LESLIE CHOIR 


 CRYSTAL PALACE 


 GENOA 


 1867 


 CORRESPONDENTS 


 C1 


 SRE 


 $ 22 


 TRIPE 


 XUM 


 60Matpstone.—A public performance given 
 Thursday , March 28th , Wesleyan School - room , 
 Maidstone Choral Society . programme consisted portions 
 Handel Messiah , aud Mozart 7wel / ih Mass. solos 
 sustained ladies gentlemen ( amateurs ) connected 
 Society . Mr. George Tolhurst conducted 

 MontTreAL , CaNADA.—The fifth series 
 Grand Instrumental Vocal Concerts given Nordheimer 
 Hall 25th March . programme carefully selected . 
 chief features performance Andante Finale 
 Beethoven Symphony ; violin solo , * * Souvenir de 
 Bellini , ” orchestral accompaniment , ( Artot ) , played 

 Capt . Stephens , Rifle Brigade ; Andante Finale 
 Mendelssohn Pianoforte Concerto G minor . ( performed 
 Mr. Benson ) best executed piece concert . 
 second performance consisted sacred music , 
 embracing selections Mass G , Weber ; alto solo , 
 ‘ Lord mindfal , ” St. Paul , * Inflammatus ” ( Stabat 
 Mater ) , * Hallelujah Chorus ” ( Mount Olives ) , 
 rendered creditable manner . Mr. Torrington 
 conductor 




 KX 


 MONTH , Opera Porus , Handel , Allegretto ( Seventh Symphony 

 Beethoven . 
 OOPER , GEORGE . — Organ Arrangements . 
 . 20 . Price 2s . Contains Chorus , ‘ ‘ rebuked Red 
 Sea ” ( Israel Egypt ) , Handel ; Chorus , * Tu es sacerdos ” ( Dixit 
 Dominus ) , A. Romberg ; Andante ( Quintett Op . 18 ) , Mendel- 
 ssohn 

 MART , HENRY.—Original Compositions 

 Organ . . 3 contains Grand Solemn March . 2s . 6d 

 TEPHENS , CHARLES FE . Fantasia Or- 
 \ gan , jRalph Courteville Chorale , St. James ; dedicated 
 Edward J. Hopkins . 2s , 
 \ ESTBROOK , W. J. — Voluntaries 
 j Organ . No.16 . Price 1s . 6d . Contains:—Introduction 
 Fague Eleventh Trio , Dr. Boyce ; Chorus , * slow 
 degrees wrath God ” ( Belshazzur ) , Handel ; Chorus Recit . , 
 Bia Mater ” ( Stabat Mater ) , G. Rossini . 
 — — . 17 . Price 1s . 6d . Contains:—March , W. J. Westbrook ; 
 Adagio , 8 . Sechter ; Andante ( Sonata Op . 14 ) , Beethoven ; Fugue , 
 T , Adams 

 IELD , HAMILTON . — “ : sword ! sword 




 EARSON 


 BENJAMIN CONGREVE . 


 MR . GOSS NEW ANTHEM EASTER . 


 Q THANKS UNTO LORD . 


 EV . RK . HAKING ANTHEMS 


 ADVENT . EASTER . 


 LENT . TRINITY . 


 CHORAL FESTIVALS . 


 SIMPLEST FORM INTONED SERVICE.—TIHE 


 


 OULE COLLECTION 415 CHANTS , 87 


 PSALTER , PROPER PSALMS , HYMNS | 


 OULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 ORDER HOLY COMMUNION , 


 MHE ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 USELEY.—A CHANT SERVICE 


 DWARD HERBERTS CHANT TE DEUM , 


 N EASY MORNING EVENING 


 STEP SIGHT SINGING . 


 BAPTISTE CALKIN EASY MORNING 


 \J OFFERTORIUM MISIT DEUS MISERICORDIAM SUAM . 


 


 JOSEPH BARNBY 


 , 


 ING 


 OUIS SPOHR ’ S 


 UEXANDRE HARMONIUMS . 


 METZLER CO , 


 37 , GREAT MARLBOROUGH STREET , W. 


 LEXANDRE CHEAP MODEL 


 HARMONIUMS , 


 PRECIOUS JESUS . 


 E LOVE 


 TONIC SOL - FA EDITION 


 PSALMS CHANTS CONGREGATIONAL USE . 


 CONGREGATIONAL CHURCH SER- 


 LEXANDRE HARMONIUMS . 


 METZLER CO.’8 NEW PUBLICATIONS 


 COMMUNION HYMN . 


 CHOIR MUSICAL RECORD . 


 POWDER DRY 


 WRITTEN H. FARNIF . 


 COMPOSED DAN . GODFREY . 


 NEW WORK ORGAN 


 ORGANIST PORTFOLIO 


 CONTENTS VOLUME , BOOKS . 16 Air . Spohr 

 17 Slow Movement Sonata , 
 Op . 2 . ( Pedal Obb . ) Beethoven 

 1 Andante Quartett 




 ORGANIST PORTFOLIO . 88 Adagio Movement . Rinck 49 Allegro Movement . Rinck 
 89 Aria P.F. Sonata . Mozart | 50 Slow — ement 

 40 Arietta . Haydn J. W. C. 0 . Sauerbrey 
 | 41 Slow Movement . Beethoven 51 Slow Movement . W. Volckmar 

 42 Slow Movement . Beethoven 52 Slow Movement . Haydn 
 43 Fugue . J. £ . Ebderlin 53 Ave Verum . Mozart 
 | 44 Agnus Dei 4th Mass | 54 Voluntary . ( Pedal Obb , ) 
 | ( Pedal Obb . ) Mozart A. Hesse 
 CONTENTS SECOND hanes BCOKS , 
 Book VII . oe Boox X. ye 
 | 75 oluntary . Anton Andre 
 ad — 2 m Op . 9 , | 76 Dona nobis pacem . Mozart 
 f ve Marig ubini 77 Prelude . C. G. Hépner 
 56 Ave Maria . Cherubint | rai Attn Dusk [ Mozart 

 " s. Robinsor 
 4 pc ttocsmnge 2 , . 6 . | 79 Andante Sonata , Op . 69 , 
 59 Hallelujah , Amen , Judas ) 80 rg Lord mind . 
 Maccabeus [ Dussex| ul . ” Mendelssohn 
 60 Adagio , L'Invocation Sonata.| 81 Chorale “ Sleepers , awake 




 CONTENTS — — BOOKS , Boox XI 

 84 Aria , La Serenade . Beethoven 
 | 85 Andante , Sonata . Mozart 
 | 86 Voluntary . Hesse 

 7 Andante , Concerto 

 131 Andante , Sonata , Op . 10 

 101 Aria , Sonata , Op.30 . Beethoven Bodenschat 
 | 102 Pastoral Air . Geminiani 197 Aria Op . 107 . Hummel 
 | 103 Andante , Septuor , Op . 74 128 Aria , Methfessel 

 ot Aria , Batam = 7 " |129 Aria . Haydn 

 Boox XIV 

 107 Aria . A. Hesse 
 | 108 Aria , Op . 18 . Beethoven 
 0 1 Steibe 
 ( 1 Slow Mare Stbat | 04 Avia Quartet , Op . 7 . Ha 
 | 111 “ Fixed everlasting | 333 oe wee 
 seat . ” Handel | 
 | 312 Solemn March . Dr. Boyce 
 113 Impromptu , Op . 142 . ( Pedal 
 Obb . ) FF . Schubert 
 _ Boox XVIII . 
 Boox XY . 89 Aria , Cantabile . 2eethoven 
 \ 140 Adagio Lobgesang . 
 414 Aria , Quartett , Op.77 . Hayda 

 Men 
 | 115 “ Thou shalt bring . ” | 141 Aria . Kalkbrenner 
 | Israel E gypt .. Handel 

 certo . ( Pedal Obb ) 
 Morar 

 117 Larghetto , Symphony D. | 143 Andante . Haydn 
 | Beethoven | 144 Prelude . J. E. Eberlin 
 | 118 Prelude . A. Hesse 145 Voluntary . A. Hesse 
 | 119 Adagio Movement . Rinck 146 Ave Maria . Arcadelt 
 120 Aria Varied . ( Pedal Obb . ) | 147 Chorale . MM . G. Fischer 
 A. Hesse | 148 Hallelujah , Saul . ( Pedal 
 | 121 Soft Voluntary . A. Hesse Obb 

 CHAPPELL & CO . , 50 , NEW BOND - STREET , LONDON 




 ! 


 HO 


 ATI 


 LTC 


 NA 


 NA 


 O 


 BAS : 


 TOLU . 


 ASS | 


 ANT 


 ASS.-