


 MUSICAL time 


 singing - class circular . 


 publish month 


 APRIL 


 GAUNTLETT FUND 


 


 subscription 


 list 


 UHH 


 UNAHUNOHKHURA 


 NHANHU WOHHOH 


 COSCMUUMNDOHUNOHUN GD 


 OVUM RU 


 CONHHOMUUNRO 


 SOOURCOHNDNS 


 MNO OH OHUH OOH NR HH 


 SCHROOH OH OHM 


 HOOHMONO 


 CONN 


 , 1876 


 PROFESSIONAL notice 


 USIC engrave , print , PUB- 


 RUSSELL MUSICAL instrument . ' 


 EAN CHEAP MUSICAL instrument . 


 bo 


 agent require town 


 new PEDAL attachment 


 pianoforte 


 testimonial . 


 ARTHUR MARCHANT , 


 J .. G. SMITH , 


 ORGAN - TONED HARMONIUMS 


 HATTERSLEY & CO.”S celebrate 


 ORGAN - tone drawing - room model 


 HATTERSLEY & CO . 


 CELEBRATED school & CHURCH harmonium 


 book design 200 testimonial 


 room : — 


 PLEYEL , WOLFF & CO . PEDAL PIANO 


 PRICE GUINEAS 


 STANDARD AMERICAN ORGANS 


 manufacture 


 PELOUBET , PELTON & CO . , NEW YORK 


 metal equitone flute , KEYS 


 CYLINDER flageolet , KEYS 


 E & W. SNELL improve harmonium . 


 selling thousand . 


 ANGLICAN 


 chant 


 SINGLE DOUBLE 


 edit 


 


 


 EDWIN GEORGE MONK 


 ready EASTER . 


 TONIC SOL - FA edition . 


 manual singing 


 use choir trainer & schoolmaster . 


 thirty - sixth edition . 


 COLLEGIATE school 


 sight - singing manual 


 APPENDIX 


 ( companion work 


 collegiate VOCAL tutor 


 CATHEDRAL chant book 


 AKER PIANOFORTE TUTOR 


 POH EK RHONMY 


 VOCAL composition ROBERT JACKSON 


 choral song 


 school home 


 , , voice 


 - song S.A.T.B 


 anthem voice . 


 HELMHOLTZ tone . 


 new edition 


 DR . BENNETT GILBERT popular work 


 SCHOOL harmony 


 subject special exercise , 


 C. JEFFERYS , 67 , BERNERS ST 


 NELSON popular song 


 ROSE ALLANDALE , " 


 NEW song 


 JEFFERYS ’ MUSICAL journal . 


 melodious MELODIESArranged organ harmonium . 
 JOHN OWEN . 
 shilling net 

 1 . voluntary ; subject Heller , Mendelssohn , Schu- 
 mann , Beethoven . ta 
 » 2 voluntary ; subject Handel , Beethoven , Rossini 

 Mozart , & c 




 TENTH thousand . 


 song , 


 harmonize school purpose , 


 H. c. LUNN element music 


 perform CARL ROSA 


 OPERA COMPANY 


 VERDI IL TROVATORE 


 LONDON : C. JEFFERYS , 67 , BERNERS STREET , W 


 O COUNTRY MUSICSELLERS.—WANTED 


 e , 


 MUSICAL time 


 SINGING - class CIRCULAR . 


 APRIL 1 , 1876 


 reminiscence C. M. VON WEBER 


 ROSSINI 


 MAX MARIA VON WEBERI complete father biography . 
 year , silent hour night , live 
 life — 
 father . wondrous time — time 
 strange believe possi- 
 bility cite spirit world . 
 late evening lay aside onerous 
 official work , settle desk 
 little study material precious 
 task lie pile ceiling — lo ! 
 swarm , kindly spirit , countless 
 fade letter , enormous heap time - wear 
 journal , ancient , mysteriously - sound piano- 
 fortis guitar — real , life , , 
 delusion sense , living dead 
 appear mix . , 
 old gentleman dandy - sword bob - wig , 
 abbé ’ robe violet stocking , court uniform 
 hair - bag ; grave smile face 
 violin , violoncello , conductor ’ baton , zither ; 
 lady , , rosy cheek , hair 
 fashioned la " Victime " " Titus , " wear tight 
 dress ' " Gigot " sleeve , roll 
 music hand , woman sunny eye 
 gaily ring voice — assemble round 
 small pale man seat midst 
 piano 

 Time fulfil purgatorial office . 
 heated sympathy antipathy day 
 politic confine care diplomatist , 
 Art free rouse heart stormy 
 emotion , cool . comprehend , ab- 
 stractedly , chief actor 
 bygone period misunderstand persecute 
 , long share 
 hatred love ; 
 father loathe despise life 
 great attractive myeye . 
 canonization remarkable man 
 fashion . laurel - wreath convert 
 saintly nimbus , fit brow 
 Tiberius Nero . , , great 
 artist hate despise abuse 
 heartily , mutual feeling aversion 
 intense , decidedly original 
 artistic organization . ' critic 
 look , " " Weber exclaim ; 
 " ' creative artist , , , ' 
 true art , ' worth . " 
 Beethoven composer * Freischiitz " ' 
 " tender mannikin , " write parody 
 " Eroica . " poet Tieck designate 
 " Freischiitz " unmusical noise 
 let loose stage ; Andrzas Romberg 
 crack joke Beethoven quartet , 
 " motley stuff , " Spohr consider 
 ' " Ninth Symphony " questionable taste ; Vogler , 
 hear Poissl " Athalia , " find " Don 
 Giovanni " long suitable palate ; 
 Zelter declare " ' Euryanthe " worth 

 perspiration cost author . 
 natural matter course . natural 
 , eye Weber , Rossini 
 appear Lucifer music — , beauti- 
 ful originally , fallen angel . 
 ' * , good , " 
 exclaim , hear second act 
 " Mosé ; " " Satan care 

 424 MUSICAL TIMES.—Aprit 1 , 1876 

 Rossini fill throat 
 rare artist fascinating melody . 
 Weber , spite wrath , experience 
 charm degree , perform- 
 ance " Cenerentola , " listen duet 
 Ambrogi Lablache , abruptly quit 
 house , furious exclamation — ' ' 
 shall run away ; stuff begin ! " 
 heart draw bosom , 
 fain recall confiding passage 
 new work , ' trust God Euryanth ' " 
 cast eye battle array 
 face " army archangel , " him- 
 self . represent Eglantine , 
 Adolar , Lysiart , efficient some- 
 wear singer Griinbaum , excellent weak 
 tenor Haizinger , capital vulgar bass , 
 Forti . heroine , 
 Euryanthe ? Mdme . Ungher - Sabatier , 
 Weber mind write , 
 procure . choice , , 
 run risk place hand Henriette 
 Sontag , barely nineteen year age . 
 year Weber pronounce 
 gift girl " bit goose ; " , 
 tew month , Mdme . Fodor - Mainville , hear- 
 e sing Amazili Spontini opera , 
 exclaim — * little German 
 ! " alternative ; result 
 prove success . performer manifest 
 enthusiasm affection Weber ; true 
 aristocracy mind , independent Press , head 
 genial Kanne , . 
 Vienna public , , 
 long powerful emotion arouse 
 truly great german work , heartily tired 
 insipid imitation " Der 
 Freischiitz , " Weig]l " Eiserne Pforte , " 
 Kreutzer ' ' Libussa . " " grey lion 
 Baden " ( Beethoven ) fitful visit 
 Stainer music - shop remark , " 
 need good german opera ; wish Weber 
 success . " , ' re- 
 hearsal deceive 
 contrast colour work exhibit , 
 compare popular italian production 
 day . create new work , chaste con- 
 ception thoroughly german genius , produce 
 great anxiety mind german earnestness 
 german perseverance , spend 
 life blood — good . 
 reason , , impression pro- 
 duce work stage 
 , comparison glowing , glittering , 
 merrymaking , effervescing , enticing creation 
 " Barber Seville , " " Othello , " ' * Gazza 
 Ladra , " ' Matrimonio segreto , " " Donna del 
 Lago , " like effect dreamy poetry 
 moonlight night contrast sunny day . 
 man specifically german organization , man 
 concentrated intense feeling , pay homage 
 german master ; viennese public large , 
 taste — direct , , chiefly scenic 
 rhetorical effect art — cause attract , 
 like moth , glaring light , forsake 
 german work performance , 
 arrival Barbaja troupe 
 seat theatre engage advance 
 performance . small 
 comfort think light public 
 attract beautiful 
 lustrous 

 Weber possess keen sense advantage 
 wait personal beauty , man . 
 occasion , travel clarionet - virtuoso 
 Barmann , man handsome - form , 
 distinguished reception accord , 
 hotel society , Weber remark : 
 ' " ' surprising ; creator goodness 
 fix letter recommendation 
 exterior , whilst carry pocket . " 
 glance small , fragile figure , , 
 provoke melancholy humour . 
 , time don court 
 uniform capellmeister Dresden , piteously ex- 
 claim , ' ' dear , calf compare favour- 
 ably fine butcher - dog ! " 
 day previous death write wife , 
 ' " ‘ t shrink like dry plum . shave 
 face smoothly henceforward puzzle . " 
 sterling heart stranger envious 
 feeling , painfully touch , careful 
 toilet , glance figure 
 glass , previous rehearsal , find 
 reflect pale , spiritualised face german 
 thinker , weary eye strong spectacle , 
 narrow shoulder transparent hand ; 
 meet opera - house brilliant 
 rival , cradle Apollo Graces 
 equal liberality shower precious gift . 
 broad - shouldered , elastic step , splendid head 
 erect , order proud breast , man con- 
 , year previously 
 , sweet melody , win heart 
 beautiful woman Italy , certain vic- 
 tory . , strange , 
 similarity feature 
 great master , far sickness exuberant 
 health , ingenious work spontaneous creating , 
 Germany Italy , short , liken 
 . outward resemblance 
 physiognomy man , , 
 psychologically establish . Weber 
 exactly Germany Rossini toItaly ? 
 congeniality mind respective native 
 country form exact parallel , 
 account similarity feature . 
 fact exist antipathy Weber 
 Rossini , found alike homogeneous 
 heterogeneous element , violent 
 form explain feeling 
 generosity fortunate . 
 Weber , hand , aversion reach 
 culminate point encounter arena 
 Vienna , gradually lose intensity proportion 
 bodily weakness ' increase , time 
 bring admit melodiousness 
 " Il Barbiere . " pass Paris , way 
 England , resolve pay visit com- 
 poser . previous visit Cherubini , 
 entertain high admiration , 
 strength effort , , , actu- 
 ate desire'to peace 
 world time . Rossini , 
 man world , interview Beethoven 
 Baden , near Vienna , ostentatiously bend 
 knee great master , receive Weber 
 sign homage , omit parting accom- 
 pany " maéstro del franco arciero " foot 
 stair , , want breath 
 compel rest step , fain lean 
 gladly - profferre arm enemy 

 year , time reduce flame 

 MUSICAL TIMES.—Apriz 1 , 1876 . 425 

 historical . picture grand im- 
 mediate past german musical art , aid 
 research recently engage , 
 assume mind degree life distinctness 
 surround remain representative 
 period halo glory . , Ros- 
 simi . contend 
 father — champion worthy 
 steel , right . Rossini , 
 eye , identify graceful 
 sweet sensual charm music . , 
 , spring 1865 occasion 
 prolonged stay Paris , impres- 
 sion mind double life live 
 spirit cite , feel express 
 train convey , mile 
 onward , year golden time 
 music ; centre delightful 
 tone - atmosphere . fancy , round 
 live hero , great depart visibly 
 life sunshine hitherto yield 
 entreat enfold presence 
 silence night . hand 
 Rossini , Auber , Caraffa press , assist 
 create remarkable epoch , 
 lay Beethoven , Schubert , father ; 
 establish complete ' " ' rapport " my- 
 self . day arrival , 
 Prefét , M. Haussmann , apologise dinner 
 General Morin having unable 
 appointment . ' 
 distinguished guest , " ; ' ' guess 
 . " General mention 
 aristocratic . ' higher , " reply Hauss- 
 mann . ' , — Prince Napoleon . " ' high 
 , ' cry Haussmann . " , " Gene- 
 ral , laugh , " Imperial Majesties 
 , paper know 
 . " ' majesty rank , genius 

 reply Haussmann animation ; " Rossini , 
 Auber , Morse " ( inventor telegraphic 
 writing - apparatus ) . ' la bonheur , " respectfully 
 salute distinguished engineer France . 
 impress homage render 
 genius , characteristic french character , 
 write Rossini permit visit , | ( 
 directly receive note wife , in- 
 vite , amiable term , " son 
 great master . " know embarrassment 
 presence great world ; , mount 
 gloomy staircase . 2 , rue de la Chaussée 
 d’Antin , 21st March , 1865 , feel heart 
 beat . admit , , pass 
 spacious somewhat sombre ante- 
 chamber , usher Rossini study . 
 rise huge mass paper pile 
 table , quickly walk : different 
 appearance picture conceive , 
 hero art fair sex — small stature , 
 corpulent , swift movement . deeply 
 impressed . Rossini ; — li verily 
 believe father look like , 
 possession health , reach 
 seventieth year . forehead , eye- 
 brow , , , nose ; different , how- 
 , massive low face , 
 decidedly plain , sensual mouth . smile 
 remind " love - inspire en- 
 chanter ot Pesaro , " extend hand . 
 hand write " ] Barbiere " 
 " tell ; ' ? hold noble 
 . involuntarily incline head 

 clasp arm , exclaim — 
 " , tall strong , 
 like father , ill 
 . " " case , fortunate 
 resemble somewhat , " reply ; ' 
 upper face — forehead especially 
 — like . " ' ' , , " retort laughingly , 
 " musical 
 feature ! appreciate great man 
 grow old wise ! bad 
 , generally . 
 time know work like ' Oberon ' 
 write . alive , 
 credit . , fact , old 
 man , . " 
 relate , astonishing resource 
 memory , particular Weber visit 
 house , death - journey Eng- 
 land . " breathe heavily descend 
 stairs . oblige assist 
 carriage — impossible 
 breathe manner long . 
 die early , " add truly french turn , 
 ' * good ? amuse , long 
 . " ' surely result merely 
 free choice , " venture suggest , ' ' other- 
 wise composer ' Tell ' " — — — " hush , " reply , 
 melancholy wave hand , " let talk 
 ! iam compose continually . look 
 shelf closely pack manuscript ; 
 fill " Tell . " publish 
 , write help . 
 live ancient , 
 consolation know day 
 burn , , find way 
 fire . " express satisfaction 
 heap table work exclu- 
 sively german master : Beethoven , Gluck , Mozart . 
 ' old composer , " reply eagerly , * 
 old ! M. Wagner great genius . 
 maybe . sure , , J shall 
 comprehend , reach year 
 Noah . entertain doubt 
 genius , goodness venti- 
 late direction music . " 
 ( time malicious observation 
 obtain currency , Rossini actually 
 hear overture ' " ' Tannhauser : " " si 
 c’etait de la musique , ce serait horrible . " ) parting , 
 veteran master request company dinner 
 follow Saturday . ' ' shall cause 
 confidential whisper ear , " , 
 ' ' new spanish Romances , tell 
 hear ! " occasion ques- 
 tion find master — appear - day 
 ' ' en belle fourchette””—seated chimney 
 aged Caraffa , pleasant humour , 
 gravely discuss manner certain 
 sicilian mushroom , partake , 
 prepare . decide 
 Rossini Caraffa mouth sensual 
 , refrain smile 
 mind thick lip 
 sing love - message ' Alexis , " " kiss 
 rose . " eye old gentleman sparkle 
 " ' piquante " conversation . small 
 select company soon assemble . 
 vivacious sympathetic Gustave Doré , look 
 moustache chin - beard , round 
 rosy face , like german student ; Professor 
 physical science , Davy Marié , Madame Ludre , 
 clever lecture universal language 

 426 MUSICAL TIMES.—Aprit 1 , 1876 

 tue death Signor Puzzi , occur , 
 advanced age , past month , draw 
 attention fact - know artist 
 connect old King Theatre , day 
 music mere aristocratic luxury , 
 leave . Signor Puzzi , addition 
 hold high position solo horn - player 
 fashionable lyrical establishment Haymarket , 
 active agent engagement 
 foreign vocalist , — Mdile . Toso — 
 bring Italy , subsequently mar- 
 rie . year Madame Puzzi 
 world husband ; 
 annual concert hold 

 news come " Pianoforte Bee " recently 
 Soldiers ’ Institute , Portsmouth ; 
 inform ' novel idea " 
 kind announce Mr. Stroud L. Cocks 
 metropolis . doubt custom 
 hold public test progress 
 branch art rapidly grow ; 
 " novel " idea , assuredly meet- 
 ing organize Mr. Willert Beale Crystal 
 Palace ' ' musical Bees " 
 extensive scale . pleasure 
 insert long account forward Ports- 
 mouth gathering , space permit ; 
 mention plan adopt 
 appropriate prize Schumann pianoforte work , 
 Beethoven Sonatas , edit Agnes Zimmermann , 
 fac - simile Handel manuscript " ' Messiah , " 
 Stainer Barrett " Dictionary musical Terms , " 
 volume Mozart work , worthy 
 imitation similar meeting 

 society protect friend scarcely 
 imagine positive necessity ; 
 amicable relationship long exist 
 composer , publisher , executive artist 
 latteriy disturb Mr. 
 Harry Wall — account 
 number — meeting place , 
 Association form ' ' Vocal 
 Dramatic Artists ’ Protection Society , " object 
 acquaint subscriber 
 circumstance connect copyright pub- 
 lishe work , penalty unknowingly 
 incur performance composition 
 public . Society good wish 
 believe duty 
 claim legal right care knowledge 
 existence right systematically hold 
 moment enforce 




 CRYSTAL PALACEOn February 26th , special feature afternoon 
 performance Mendelssohn g5th Psalm . fine 
 work previously Sydenham , particular 
 interest attach present occasion fact 
 unpublished chorus reason 
 believe Mendelssohn intend finale 
 end work , public hearing . 
 remember print score Psalm 
 begin key E flat end G minor , close , 
 defensible esthetic ground , certainly 
 unsatisfactory musical point view . new 
 finale , publish Novello & Co. , vocal 
 score separate chorus key e flat , 
 consist broad introduction spirited fugue ; 
 material early work . 
 word new chorus fifth , 
 sixth , seventh verse Psalm ; 
 feeling - introduction place 
 hardly felicitous , induce composer 
 suppress chorus prepare work publication 

 follow Saturday ( March 4 ) , Herr Joachim 
 play Beethoven violin concerto unapproach- 
 able style . concert orchestral arrange- 
 ment Schubert great duo C , Op . 140 , bring 

 esteem liberally patronize 




 428Rubinstein ballet music , opera , Feramors , 
 require detailed notice . consist dance 
 wedding march . second number 
 pretty , especially ; fourth 
 weak . style 
 light french opera new german school 
 Rubinstein composition belong 

 Saturday , 25th , pleasing overture 
 ' ' Euterpe " late Charles Edward Horsley , 
 song pen , Beethoven choral sym- 
 phony . performance , regard 
 instrumental , fine hear , 
 Mr. Manns . choral portion , , course , 
 perfect ; difficulty music great 
 wonder : vocal performance 
 fairly satisfactory . solo music 
 sing Mdlle . Johanna Levier , Miss Annie 
 Butterworth , Mr. Edward Lloyd , Signor Foli 

 PHILHARMONIC SOCIETY 

 Concert Society present season 
 St. James Hall 23rd ult . , 
 large audience . programme , present 
 novelty , high degree interesting , Schumann 
 symphony C ( . 2 ) finely render 
 elicit warm mark admiration ; ' ' Scherzo , " 
 characteristic melodious trio , fairly ex- 
 cite hearer frenzy delight . Madame 
 Schumann performance Beethoven pianoforteConcerto 
 G , respect , absolutely perfect dis- 
 arm criticism . intellectual grasp composer 
 meaning , combine mastery executive difficulty 
 rarely exhibit , great artist , occasion , 
 audience effect extempore playing , 
 save Mendelssohn,—whose rendering Con- 
 certo Society Concert distinctly 
 recollection — remark . double 
 recall pianist retirement platform , 
 , hope , convince thoroughly mani- 
 festation exceptional gift appreciate musical 
 England . vocalist Mdlle . Ida Corani 
 Signor Pollione Ronzi , tenor La Scala , 
 Milan , somewhat thin pleasing voice . 
 Mr. Cusins , enthusiastically receive en- 
 trance orchestra , conduct skill 
 judgment 

 ROYAL ACADEMY MUSIC 

 orchestral concert student 
 Institution , St. James Hall , 18th ult . , 
 large appreciative audience . excellence 
 pianoforte instruction strikingly display Mr. 
 Morton , movement Rubinstein Concerto 
 d minor ; Miss Thurgood , movement 
 Beethoven Concerto E flat ; Miss Borton , 
 movement Sir Sterndale Bennett Concerto F minor ; 
 Mr. Matthay , Schumann Concert Allegro D 
 minor ( Op . 134 ) , clever highly promising 
 young pupil forward conclusion 

 performance warmly greet . Miss Gabrielle Vaillant 
 rendering Beethoven romance G , violin , 
 remarkable , purity tone , intelli- 
 gent perception composer intention scarcely beex- 
 pecte student . composition pupil 
 — overture C minor , Miss Oliveria Prescott — 
 clearness writing delicacy instrumentation 
 , , prove good judgment 
 exercise select representative work . 
 somewhat ambitious attempt second ot 
 Handel " Belshazzar ; " result 
 highly gratifying professor present , 
 direction study . 
 choir steady tune , opening 
 chorus , ' , post Euphrates fly , " 
 especially worthy praise . solo entrust 
 Miss Kate Brand , Miss Barkley , Mr. Seligmann Mr. 
 Gordon Gooch , acquit 
 difficult task credit , Miss Kate Brand elicit 
 deserve applause refined interpretation 
 air , " Regard , o son , flow tear , " Mr. Gordon 
 Gooch display good voice method trying 
 recitative , Daniel expound oracle Bel- 
 shazzar . Miss Annie Butterworth sing Stradella song , 
 " Pieta , signore , " " good expression . Handel aria , 
 ' " ' Nasce al Bosco , " opportunity Mr. Eugene 
 Boutenopp , evidence possession excellent 
 voice ; Miss Jessie Jones , student , 
 win honour Academy , 
 ' * hear ye , Israel , " ( ' Elijah " ) admirably 
 majority audience seat ( notwithstanding 
 item programme ) , follow 
 chorus , " afraid , " , consequence , hear 
 usual interruption conclusion 
 concert . performance ably conduct Mr. 
 Walter Macfarren 

 MR . HENRY LESLIE CHOIR 

 principal feature programme concert 
 oth ult . , performance Mendelssohn 
 music ' Antigone , " render additionally 
 attractive Mrs. Stirling intelligent reading text , 
 portion necessary connect 
 piece music . chorus admirably 
 sing , excite usual 
 interest audience , result , 
 detriment work , ' ' hymn 
 Bacchus " quartet , ' ' O Eros , ’' - demand . 
 second concert Mendelssohn " ' Vintage 
 Song " ' ( finely sing choir ) encore , Herr 
 Joachim elicit enthusiastic mark approbation 
 fine interpretation Beethoven Violin 
 Concerto hear . Mr. Leslie conduct 
 accustomed care judgment 

 MR . WALTER BACHE concert 




 QD . J 4 


 LORD strength 


 QA 


 rq 


 rq 


 q2 


 eq 


 1 7 + t 


 lp TC _ 


 + » CHORUS 


 5 = | PAE _ | = 


 1 l 


 ES ce | _ 


 c : SSS SSS ES SS SS 


 437 


 review 


 WILLIAM CZERNY 


 original correspondence 


 consecutive fifth seventh . " 


 EDITOR MUSICAL TIMES 


 DR . STAINER ' * HARMONY . " 


 editor MUSICAL TIMES 


 LISZT " ST . ELIZABETH . " 


 editor MUSICAL TIMES 


 correspondent 


 BRIEF summary country NEWS442 MUSICAL TIMES.—Aprit 1 , 1876 

 cold , " " Gadeamus igitur , " new song conductor , " Regna 
 il terror " ( Tancredi ) , Barcarolefrom Masaniello . prin- 
 cipal instrumental piece Mozart E flat Symphony , 
 character feeling . overture Stradella , 
 Freischiitz , Masaniello . student , Mr. Galletly , , con- 
 junction Mr. Carl Hamilton , good reading adagio 
 finale Beethoven Duet Sonata , pianoforte violoncello , , 
 Op . 69 , far contribute pianoforte solo , ' " ' lie " d flat 
 Esain , Heller Tarantella flat . Professor Oakeley 
 accompany song piano 

 ENNISKILLEN , Co. FERMANAGH , IRELAND.—The member Mr. 
 Arnold Choral Class concert Friday evening , 
 February 25th , protestant Hall , selection Handel 
 Messiah , Mendelssohn Elijah , St. Paul , & c. , creditably 

 erforme . solo , " know redeemer , " , " shall 

 ighteous , " " o rest Lord , " " Lord God Abraham , " 
 sing Miss Bagot , Miss Emily Graham , Miss Graham , Mr. 
 W. R. Cooney . - song include ' " ' blow , ye balmy breeze , " 
 Young ; " Ahcould fancy stray , " Hatton , ( encore ) ; " ' sweet 
 low , " Barnby . song Messrs. Arnold , Black , 
 Leddall , Trimble , McKeague . Beethoven sonata F , Op . 24 , 
 piano violin , play Miss Gray Mr. Arnold , 
 clarionet Fantasia ( Reissiger ) Herr Werner . Mr. Arnold conduct 

 FREEMANTLE.—A lecture Music Musical Compositions 
 deliver connection Freemantle Mutual Improvement 
 Association , Church Schoolrooms , Thursday evening , 
 ult . , Mr. Thos . Miell , jun . Mr. J. Cockburn , vice- 
 president , occupy chair , introduce lecturer , state 
 proceed devote augmentation library 
 fund . Mr. Miell commence lecture brief notice 
 history music early period , refer system 
 notation , law harmony , illustration remark 
 operatic music , perform selection opera Masanjello 

 MANCHESTER.—The recent Concert St. Cecilia Choral 
 Society , able conductorship Mr. Hecht , Free Trade 
 Hall , draw large appreciative audience . member 
 choir , nearly , amateur , intelligent manner 
 sing suggest great labour amateur 
 generally suppose , seldom good fortune 

 conductor non - professional choir command splendid 
 array vocal talent . previous occasion excellence 
 equally distribute . soprano bright fresh 
 , department choir 
 satisfactory . Bach Cantata , ' ' spirit , " Concert 
 open , pathetic characteristic genius com- 
 poser , surprising know ; 
 regret single performance work hear . 
 Schumann Faust music surprise , scene 
 occasion accept fair specimen , 
 hesitation attract english public 
 far composer Paradise Peri . Schu- 
 mann admirably advantage mystic character 
 poem , fancy seldom appropriate subject . 
 Sullivan - song , ' ' watchman , " happy inspira- 
 tion , " shepherd ’ Chorus , " Schubert Rosamunde 
 welcome , contrast prevail sadness 
 . admirable performance Beethoven Concerto 
 C major , amateur , mention feature 
 programme . band , carefully lead Mr. Straus , add con- 
 siderably success evening ; good understanding 
 choir orchestra creditable 
 energetic accomplished conductor , Mr. Hecht , 
 St. Cecilia Society hisown private undertaking . 
 — title ' " Manchester Cathedral Glee Choral 
 Union " Society lately form , prospect ulti- 
 mate success . propose concert season , 
 able direction Mr. J. Kendrick Pyne , organist 
 Cathedral , appoint permanent conductor Association 

 MaIpsTonE.—A sacred Concert West Borough 
 Congregational Church Friday , Feb. 25th , aid Organ 
 Fund . programme consist selection 
 Handel oratorio , fudas Maccabeus , second miscel- 
 laneous . solo vocalist Mrs. Day , Miss Somerton , Messrs. 
 Crowe , Airs , Jefferies , Master P. R. Day . thechoruse 
 great precision care . Mr. T. G. Day conduct 

 art - song . Miss Williams repeat Carola " far away . " 
 Madame Patey receive ovation Giordani ' ' Caro mio ben , " 
 Mrs. Buxton , Miss Gadsden , elicit encore Campana 
 " Tel Rammenti . " - song choir , careful 
 direction Mr. , capitally sing 

 Yarmoutu.—A pianoforte recital lecture great musical 
 composer Tuesday evening , Feb. 2gth , Town Hall , 
 Herr Louis Loffler ; E. P. Youell , Esq . ,in chair . method 
 adopt Herr Loffler short lecture composer 
 order programme , follow 
 performance pianoforte selection work 
 master . way lecturer deal Handel , Mozart , Hummel , 
 Beethoven , Weber , Mendelssohn , Liszt , Thalberg , short 
 account life great composer , point 
 chief incident . conclusion recital , Chairman pay 
 high compliment Herr Loffler , vote thank 
 pass Mayor use hall 

 Yeovit.—A concert Thursday , 16th ult . , 
 conductorship Mr. Loaring , whena interesting programme 
 perform , include Handel " vain man , " Hullah " Storm , " 
 " swallow homeward fly , " Abt , ' ' Market chorus , " 
 Auber , overture Fra Diavolo Guy Mannering 




 TENTH season , 1875—76 . 


 ENGLISH GLEE UNION , 


 assist 


 dure month . 


 OVELLO OCTAVO anthem 


 VOCAL DRAMATIC ARTISTS ’ 


 444 


 LEXANDRA PALACE.—HANDEL 


 YORKSHIRE CONCERT PARTY . 


 RGAN , PIANOFORTE , HARMONIUM , HAR 


 O CHORAL SOCIETIES , CATHEDRAL 


 quarterly sale MUSICAL property 


 WO - manual PEDAL HARMONIUM 


 public notice 


 JSIC 


 PPA- 


 € i8 . ; 


 445 


 second serie . 


 ANGLICAN CHORAL SERVICE book ; 


 USELEY MONK PSALTER 


 OULE collection word 


 PSALTER , PROPER PSALMS , hymn , 


 JOULE DIRECTORIUM CHORI ANGLI- 


 OULE DIRECTORIUM CHORI ANGLI- 


 order HOLY COMMUNION . 


 village ORGANIST 


 446 


 ASTER ANTHEM . 


 " oh ! WORSHIP LORD beauty 


 LORD rise indee : HALLE- 


 consecration banner . 


 FESTIVAL MARCH 


 ORGAN . 


 FREDERICK G. COLE 


 ight welcome home , O PRINCE . " 


 CONCLUDING selection 


 SACRED CANTATA 


 music compose 


 T. BROOKS , R.A.M 


 composition WILHELM schulthe . 


 GAUNTLETT : — 


 " o son daughter " 


 " HAIL , HOLY DAY . " 


 EHOLD good thing . 


 LORD shepherd 


 23rd PSALM . ) 


 O CHORAL society . 


 collection 


 ORGAN 


 


 PIERCE 


 compose 


 CHARLES JOSEPH FROST 


 2 , NEWTON TERRACE , LEE , S.E 


 fifth list subscriber 


 new work amateur organist 


 new work singing class 


 CHAPPELL PENNY OPERATIC - song 


 ANTOINE COURTOIS ’ CORNETS 


 CHAPPELL & CO . , 504 , NEW BOND STREET , LONDON