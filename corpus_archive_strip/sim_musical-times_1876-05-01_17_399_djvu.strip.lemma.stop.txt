


 MUSICAL time 


 singing - class circular . 


 publish month 


 1 , 1876 


 NOVELLO , EWER & CO 


 80 & 81 , QUEEN - STREET , CHEAPSIDEISS ALICE FAIRMAN morning CON- 
 CERT , ST . GEORGE HALL , W. , WEDNESDAY , 
 31st , o’clock.—vocalist : Mesdames Edith wee 
 Blanche Cole , Badias , A. Fairman ; Messrs. E. Lloyd , N. Varley , 
 Wadmore , andL. Thomas . instrumentalist : Madame Varley - Liebe , 
 Messrs. Ganz , Mattei , Richard Blagrove , F. Chatterton . Con- 
 ductor : Messrs. Ganz , F. Kingsbury , F. H. Cowen , S. Naylor . 
 ticket , 10s . 6d . , 5 . , 2 . ,at Messrs. Schott Co. , 159 , Regent- 
 street , 18 , St. peter’s - square , Hammersmith 

 BC RUBERT SOCIETY , Beethoven Rooms , 27 

 Harley - street , W.—President , Sir Juttus BenEepict ; Founder 
 Director , Herr SchuserTH.—Tenth season , 1876.—the 
 concert place Thursday , 11th . Society 
 concert afford excellent opportunity young rise artist 
 appearance public . particular application 
 H. G. Hopper , Hon . Sec 




 HOIR BOYS.—WANTED , treble 


 OPRANO.—RE - engagement require 


 RGANIST.—WANTED , 


 professional notice 


 RUSSELL MUSICAL instrument . 


 EAN CHEAP MUSICAL instrument . 


 AST LONDON ORGAN work 


 USIC engrave , print , PUB 


 £ 6 108 


 451 


 agent require town 


 NEW PEDAL attachment 


 pianoforte 


 testimonial . 


 ARTHUR MARCHANT , 


 ITH , 


 ORGAN - TONED HARMONIUMS 


 HATTERSLEY & CO."S celebrate 


 ORGAN - tone drawing - room model 


 HATTERSLEY & CO . 


 book design 200 testimonial 


 room : — 


 selling thousand 


 PLEYEL , WOLFF & CO . PEDAL PIANO 


 PRICE guinea . 


 STANDARD AMERICAN organ 


 manufacture 


 PELOUBET , PELTON & CO . , NEW YORK 


 METAL equitone flute , KEYS 


 CYLINDER flageolet , KEYS 


 STUTTGART HARMONIUM CO 


 & W. SNELL improve harmonium . 


 452 


 second serie . 


 


 canticle hymn 


 


 CHURCH 


 point chanting , SET appropriate anglican chant , SINGLE DOUBLE 


 


 edit 


 


 REV . SIR F. A. G. OUSELEY , BART . , ETC . , 


 EDWIN GEORGE MONK 


 nearly ready . ' 


 TONIC SOL - FA edition . 


 AKER PIANOFORTE tutor . 


 manual singing 


 use choir trainer & schoolmaster 


 thirty - seventh edition . 


 COLLEGIATE SCHOOL 


 sight - singing manual 


 appendix 


 ( companion work 


 collegiate VOCAL tutor 


 CATHEDRAL chant book 


 son GOD forth war . 


 le 


 OL 


 10.3 


 ical 


 DERN 


 WAR 


 arthur 


 collection ORGAN piece 


 compose 


 CHARLES JOSEPH FROST 


 ORCHESTRA : constitution , 


 VOCAL composition ROBERT JACKSON 


 CHORAL song 


 school home 


 - song S.A.T.B 


 anthem voice 


 publish F. PITMAN , 20 , PATERNOSTER - ROW 


 R. FOWLE " WELCOME HOME MARCH 


 R. FOWLE " FESTIVAL MARCH . " 


 R. FOWLE " right WELCOME HOME , 


 C. JEFFERYS , 67 , BERNERS ST 


 publish 


 NEW SONGS 


 LONDON : C. JEFFERYS , 67 , BERNERS STREET , W 


 RGANIST.—WANTED , - engagement 


 YOUNG AMATEUR organist want 


 MUSICAL time 


 singing - class circular . 


 1 , 1876 


 MUSICAL ENGLAND 


 456 


 LOCKE write vocal music 


 MACBETH 


 dance play ' MACKBETH 


 WITCHES ’ dance 


 DR . SAMUEL SEBASTIAN WESLEY 


 CRYSTAL PALACETHE series Saturday Concerts Crystal Palace 
 conclude , interest having sustained 
 . record leave 
 month , notice fine performance 
 concert rst ult . , Raff violoncello concerto , 
 Signor Piatti , Joachim Raff , prolific 
 live composer , gradually obtain country 
 recognition long receive 
 Germany . symphony 
 hear , fourth ( . 2 , c major ) 
 promise season Philharmonic Concerts . 
 concerto violoncello , like 
 work , remarkable cleverness construction 
 decided originality invention . , , 
 melodious , admirably write solo 
 instrument orchestra , rendering 
 receive Signor Piatti , success 
 certain . concert overture entitle 
 ' Les Muses , " composition , believe 
 recently deceased Alfred Holmes , bring forward 
 time , produce great impression 

 excellent performance Beethoven ' ' Mount 
 Olives , " solo Madame Osgood , 
 Mr. W. H. Cummings , Signor Foli , occupy 
 great afternoon 8th ; special 
 novelty concert production 

 Gore Ouseley " finish oratorio , ' Hagar 




 ALEXANDRA PALACE 


 ROYAL italian OPERAPHILHARMONIC SOCIETY 

 presume funeral service legitimate com- 
 position place philharmonic audience St. 
 James Hall , congratulate Society 
 second presentation Brahms ' " requiem , " ' 
 6th ult . , applaud vehemence 
 complimentary everybody concern execution , 
 audible expression satisfaction 
 shock sensitive nature feel religious 
 purport work . originality display 
 ' * requiem " prevent like comparison 
 masterpiece bequeath 
 great composer pass away ; judge 
 earnest outpouring artist feel 
 importance mission dare think . 
 movement masterly specimen con- 
 structive power ; orchestral colouring , 
 somewhat overladen , generally excellent tone 
 subject text . baritone solo , 
 chorus , ' ' Lord , know measure day , " 
 cite impressive number 
 work ; andthe choral piece write regard 
 solemnity word , careful avoid- 
 ance mere display , fault observable 
 modern german music , elaboration , apparent 
 work composer ; 
 , save restlessness tonality induce restlessness 
 listener , little fault find 
 modulation frequently occur work . 
 solo , Mrs. Osgood Mr. Wadmore exceed- 
 ingly effective , lady rendering somewhat difficult 
 music allot evidence result careful 
 intelligent study . important feature pro- 
 gramme evening fine performance Spohr 
 violin Concerto E minor , Herr Joachim ; 
 add purely orchestral work Beethoven 

 Ruy Blas , " concert 
 respect highestinterest . Mr. Cusins , conduct 
 usual , deserve credit excellent manner 
 direct Brahms ' ' Requiem 




 ROYAL ACADEMY MUSICTHE MUSICAL TIMES.—May 1 , 1876 . 461 

 supper ' ? ( Wagner ) , Mass , . 1 , C ( Beethoven ) ; 
 Friday evening , ' St. Paul " ( Mendelssohn ) . chief 
 novelty Festival Oratorio Mr. Mac- 
 farren , principal solo sing Mr. 
 Santley . danish composer , Niels Gade , repre- 
 sente new sacred Cantata , secular Cantata , 
 " crusader . " Mr. F. H. Cowen new Cantata , 
 " corsair , " base Byron poem ; Richard 
 Wagner sacred Cantata , ' ' supper , " describe 
 grand composition voice orchestra . engage- 
 ment principal singer 
 conclude ; choral rehearsal proceed 
 regularity , band process organisation 

 second Mr. Willem Coenen 
 * * Chamber Concerts Modern Music , " selection 
 work extremely interesting . Brahms Quartett , 
 string , C minor , produce second concert , 
 assuredly assert right hear 
 performance devote chamber composition , 
 satisfied increase familiarity beauty 
 render favourite audience executant . 
 " Adagio " charming movement , popular 
 character course ensure warm welcome 
 accord rest composition , elaborate 
 nature appreciate 
 attentive listener single hearing . Schumann Trio 
 pianoforte , viola , clarionet , concert- 
 giver ably support Messrs. Zerbini Lazarus , 
 produce marked effect , melodious movement 
 display Mr. Lazarus exquisite tone power ex- 
 pression great advantage . Mr. A.C. Mackenzie 
 quartett — play concert , comment 
 season — , receive , brilliancy 
 certainty touch exhibit Mr. Coenen difficult 
 pianoforte theme universal admiration . 
 interesting feature programme 
 final concert , 13th ult . , Raff 
 Sonata D major , pianoforte violoncello , render 
 perfection Messrs. Coenen Daubert . second 
 movement , mark " Vivace , " perfect gem , fairly 
 audience surprise , artist thoroughly 
 sympathise cause 
 murmur satisfaction conclusion , , 
 credit executant , seize encore . 
 Schumann string quartett major ( Op . 41 , . 3 ) , 
 Gernsheim pianoforte quartett C minor ( Op . 20 ) , 
 important item programme , receive 
 warm mark approbation . Mr. Coenen , 
 modesty true artist , rarely 
 forward concert , occasion per- 
 form solo composition , ' ' Caprice Serenade , " 
 delicacy touch brilliancy finger 
 long hear playing . refined 
 melodious subject little sketch base — 
 alternately sing right left hand , surround 
 graceful passage ornament — recom- 
 mend cultivate high order drawing- 
 room music ; attempt gift 
 composer executive power , moderately 
 play , satisfied piece way . 
 close notice , forget 
 special mention Mr. Wiener violin solo , elicit 
 enthusiastic applause , award word 
 praise vocal piece contribute second 
 concert Miss Sophie Ferrari , Miss 




 462the Passiontide Easter service St. Stephen , 
 Lewisham , mark special feature . 
 Thursday evening , Passion week , Mendelssohn 
 ' Christus " perform excellent effect . good 
 Friday evening special service hold , include 
 short selection " Messiah , " solo 
 chorus " Jesus watch pray , " Bach 
 ' " St. Matthew Passion , " solo Mr. G. F , Carter , 
 Westminster Abbey . Rev. Canon Gregory preach . 
 Easter Sunday morning english version Gounod 
 ' Messe Solennelle " " sing . 
 instrumental accompaniment , 
 direction Mr. C. Warwick Jordan , Mus . Bac . , Oxon 

 Miss Amy Stewart ( pupil Herr Sauerbrey ) , 
 pianoforte recital , Langham Hall , 5th ult . , 
 play success selection Chopin , 
 Schubert , Beethoven , Schumann , Heller , 
 composition Herr Sauerbrey . Madame Sauerbrey 
 sing Schubert Serenade Mozart " Voi che sapete . " 
 room fill 

 Art sixth trial new composition ' Musical 
 Artists ’ Society , " hold Royal Academy Music 
 rst ult . , interesting work 
 mention trio D , pianoforte , violin , violon- 
 cello , Mr. F. E. Gladstone ; romance , pianoforte 
 violoncello , Mr. E H. Thorne ; Ferdinand Hiller 
 " Suite Moderne " ( excellently play pianoforte 
 Mr. George Wheeldon ) , pianoforte solo 
 Mr. J. B. Calkin , effectively render composer . 
 - write song Mrs. -O’Leary - Vinning — 
 sing Miss Marion Severn , second Miss 
 A. Butterworth — elicit warm applause . Mr. Arthur 
 O’Leary Mr. Eaton Faning preside pianoforte 




 review 


 TREBLE . - = SS 2S SSS SS SE — 2 


 PIANO . 


 — — — — = SS 


 > = SS — 


 + — ¥ 2 e — 


 ' — _ — _ 


 _ — _ — _ — — _ . — — — — 


 SSS = 


 2 = = 2 SS ES 


 SS = 


 @- TATE 


 SS SSS Gavotte ( Festivale ) . Alfred B. Allen 

 piece compose perform Mr. 
 Kuhe Brighton Festival present year . 
 imagine , originally intend 
 orchestral work , effect enfeebled 
 pianoforte transcription ; theme melo- 
 dious , " Trio " ' 
 mind , consecutive fifth inner 
 bass , 8th bar , , , particularly 
 disagreeable . way , understand 
 diminish 7th g¥ write { 6 - 5 
 resolve 6 - 4 dominant . surely root 
 resolve 7th 6 - 4 , vary 
 notation like apt perplex performer . 
 Mr. Allen doubt chord ought 
 write , let refer Beethoven sonata e flat ( op . 
 31 , . 3 ) , 5th bar movement , 
 surprised , think , find FZ instead 
 gp . 

 Silent Land . - song unaccompanied Choral 
 singe . word Longfellow ' ' Hyperion . " 
 compose Alfred R. Gaul 




 CLARENDON PRESS 


 SS 


 ORIGINAL correspondence 


 DR . STAINER " harmony . " 


 editor MUSICAL TIMES 


 ALBERT G. EMERICK 


 ALOYS HENNES " new method 


 PIANO . " 


 EDITOR MUSICAL TIMES 


 470 


 h. MANNHEIMER 


 " benedicite " SUNG ? 


 EDITOR MUSICAL TIMES 


 CHOIRMASTER 


 PROFESSOR BLACKIE PROFESSOR 


 OAKELEY 


 editor MUSICAL TIMES 


 STER 


 47 


 right representation . 


 EDITOR MUSICAL TIMES 


 editor MUSICAL TIMES 


 correspondent 


 BRIEF SUMMARY COUNTRY NEWSBERWICK - - TWEED.—On 28th March Messiah 
 perform Berwick Choral Union , number upwards 
 voice , assist orchestra Edinburgh 
 Glasgow , include Drechsler - Hamilton family . 
 time Oratorio Berwick , per- 
 formance great success . Mr. Anderson conduct , Mr. 
 Barker presided harmonium 

 BIRMINGHAM.—Mr . R. H. Rickard , Pianoforte Recital 
 Masonic Hall , 30th March , previous departure Leipzig , 
 pursue musical study . programme include prelude 
 fugue Bach , Beethoven Sonata Appassionata , F minor , 
 Op . 57 , Schubert Impromptu , flat , . 4 , op . , Liszt trans- 
 cription Tannhduser March , selection Schumann 
 Fantasiestuck , Chopin Ballade " La Favorite , " Reinecke Ballade , 
 Op . 20 , Weber " Concertstiick , " interpret 
 manner worthy high commendation . - piece 
 orchestral accompaniment judicious care pre- 
 cision Dr. Swinnerton Heap . Mr. E. Allely Mr. Randell 
 contribute vocal music , select work Handel , Mozart , 
 Verdi , Blumenthal , Hatton 

 Botton , LANCASHIRE.—A musical service Wednesday 
 evening , March 2gth , Wesley ehapel , Bradshawgate . 
 Anthem " glad " ( Sir G. Elvey ) , " Credo " ( Haydn 
 Mass ) , ' o Father almighty power " ( Handel udas ) , selec- 
 tion } udgment , Beethoven " hallelujah " ( Engedi ) , 
 efficiently render chapel choir . solo Mr. H. Taylor 
 ( choirmaster ) , Miss Fallows , sing , performance 
 organ Mendelssohn Trumpet Overture C , Guilmant 
 " Marche Funébre et chant Seraphique , " german hymn , vary , 
 Mr. J. T. Flitcroft , organist , highly appreciate . 
 interesting address interval Rev. John 
 Rhodes , subject " ancient modern music compare 

 CAMBRIDGE.—The member St. Paul Musical Society 
 concert 28th March , selection Mendelssohn 
 St. Paul , song - song render . pianoforte 
 solo , Miss Dennis , able accompanist , special feature 
 evening . Concert success , profit , 
 usual , devote Cambridge Industrial School —— special 
 service hold church Saints , Wednesday 
 Easter , consist chiefly Messiah 
 relate Christ passion . choir ( increase 
 ordinary strength ) accompany ( organ ) band 
 - instrument . chorus " behold Lamb God , " 
 follow contralto air , ' ' despise , " sing great 
 feeling Mr. E. J. Bilton , feature second 
 air , " ' smiter , " usually , include . 
 choral Bach " o sacred head surround , " congregation 
 join . , choral accompany 
 instrument , ( arrange Mr. W. C. Dewberry , organist 
 Saints ) , verse sing unaccompanied . chorus 
 " surely hath bear grief , " " stripe , " " 
 like sheep , " effectively sing . choral ' o sinner lift 
 eye faith , " come ; address offertory recit- 
 ative , " , " chorus " trust , " solo , 
 " thy rebuke , " " ' behold , " " cut , " 
 taste regular member choir . inter- 
 val silent prayer , service conclude choral " O 
 love formedst wear , " join large 
 congregation . organ play Mr. Mountain , S. 
 Saviour , Hoxton , service conduct Mr. W. C. 
 Dewberry , A.R.A.M. , organist choirmaster church , 
 zeal judgment success choir 

 DunHAM Massey.—On 22nd ult . , new organ , build M. 
 August Gern ( late foreman work Messrs. Cavaillé - Coll , 
 Paris ) , open church St. Margaret , Mr. ] 
 Matthias Field , organist , presence large congregation . 
 service choral , permanent choir considerably 
 augment . Rev. Geo . London , Altrincham , read , 
 Rev. Dean Howson , Chester , second lesson , 
 response Rev. H. Hignett , Ringway , Altrin- 
 cham . Rev. T. A. Stowell , Salford , preach , Rev. 
 Canon Gore , Bowdon , pronounce benediction . organ , 
 cost £ 1,000 , stand north transept , 
 divide large window . itha entirely - build , pipe 
 old instrument , replace , having use 

 EAstTBOURNE.—On Tuesday , 11th ult . , Mr. J. H. Deane , organist 
 Trinity Church , lecture Friendly Societies ’ Hall , 
 work J. S. Bach later work Beethoven . com- 
 mence Bach Passion ( St. Matthew ) Mr. Deane describe 
 important feature work , illustration , 
 Chorals sing Trinity Church choir , Soprano air 
 render taste Miss Douglas . Mr. Deane , course 
 lecture , endeavour contradict general assumption 
 composition Bach , especially fugue , dry , laboured , 
 expressionless , follow apt illustration support 
 real musical value artistic merit , viz . , Bach consider 
 friend hold amicable conversation , 
 speak , 
 verse readily admit characteristic 
 - writing Bach fugue . Mr. Deane choose illus- 
 tration grand organ Fugue Minor , play 
 piano , assist daughter , Pedal . 
 composition introduce Soprano air , ' ' heart faithful , " 
 bring forward composition jubilant character , 
 contrast Passion music . second 
 lecture Mr. Deane introduce Moscheles piano duet , ' " Hommage 
 Handel , " Cherubini " Ave Maria . " performance illustra- 
 tive Beethoven movement C minor 
 Symphony , Allegretto Scherzo Symphony , 
 song " knowest thou land ? " Mr. Deane point 
 grandeur beauty Beethoven symphony , sum 
 , advise hearer hitherto neglect Bach Beethoven 
 set study work 

 Ecc.ies.—The Eccles Amateur Choral Orchestral Society 
 Concert season Wednesday , March 2gth , 
 Co - operative Hall . programme consist glee , solo , selec- 
 tion opera , & c. vocalist Miss Lowe , Mr. George 
 Barlow , Mr. Ashe . Mr. De Jong play couple solo 
 flute usual taste finish . Mendelssohn Rondo BriRante , 
 Op . 22 , pianoforte orchestral accompaniment play 
 Mr. Lowe , conductor Society . ' ' Shepherds ’ chorus , " 
 Schubert Rosamunde , orchestral accompaniment , choral 
 March , write Mr. C. B. Grundy , Bishop ' ' echo song , " Miss 
 Lowe , flute obbligato , anda violin solo Miss Mather , amember 
 orchestra , feature programme . overture Le 
 Nozze di Figaro , Haydn No.13 Symphony , selection Lucia 
 di Lammermoor creditably perform orchestra 

 second miscellaneous . accompanist Mr. 
 — Hill , Mrs. Hadley , Mr. Walter Lain , organist parish 
 church 

 Norwicu.—The series classical concert , promise 
 Mr. Darken , place St. Andrew Hall , 21st ult . Beethoven . 
 Septuor , arrange quintett pianoforte , violin , viola , violoncello , 
 contra - basso , render Miss Kate Griffiths , Messrs. 
 C. Griffiths , H. Channell , J. Griffiths , G. R. Griffiths . Miss 
 Griffiths play Chopin Scherzo b flat minor 
 sketch Schumann , Henselt , Chopin . Mr. Griffiths contribute 
 violoncello solo Goltermann , Messrs. C. Griffiths H. 
 Channell duet Kalliwoda violin . concert con- 
 clude Hummel trio F , pianoforte , violin violoncello . 
 vocalist Mdlle . Sophie Lowe Mr. Vernon Rigby 

 NotrincHaM.—The report Sacred Harmonic Society , 
 issue , announce season successful musical 
 point view ; owe depression trade , trifling financial 
 loss sustain , view liquidate , 
 member Society , past month , extra 
 concert glee , - song , madrigal solo member 
 . season , grand performance 
 place , subscription guinea , . 
 Mr. Henry Farmer remain able choir - master conductor , 
 Mr. Marriott , hon . sec . decide commence 
 elementary instruction - class , Mr. John Adcock , useful 
 branch , nursery draw supply chorus , 
 large new Hall enable Society greatly increase 

 RamMsGATE.—On Wednesday evening , Lent , special service 
 hold St. Mary Church , feature performance 
 Gounod " green hill far away , " arrange Solo 
 Chorus Mr. J. Finch Thorne , organist choirmaster . 
 Easter service following composition 
 perform : J. Baptiste Calkin Te Deum b flat ; Dr. Stainer 
 Magnificat Nunc Dimittis e ; Sir John Goss , , 
 Anthems " away Lord " ( Dr. Stainer ) , " 
 man come death , " " ' worthy Lamb , " " hallelujah " 
 chorus . mid - day celebration Easter - day 
 Octave fully choral , Marbeck music use occasion 

 Rock Ferry.—On Wednesday , roth ult . , Concert sacred 
 music Hotel Assembly - room . Mendelssohn 
 hymn praise form , second consist 
 selection oratorio music , include Costa " 
 extol thee o God ; " Handel ' * nation ? " Mendelssohn 
 " cast thy burden Lord , " & c. ; Beethoven " hallelujah " 
 Mount Olives , conclude piece . principal 
 soloist Mrs. Billinie Porter , Miss Armstrong , Mr. Alfred 
 Brown , choir number voice . Mr. Billinie Porter 

 accompanist , Mr. Armstrong conduct 




 474 


 OBITUARY 


 GLUCK OPERAS , 


 IPHIGENIA TAURIS 


 


 IPHIGENIA AULIS . 


 WAGNER * OPERAS , 


 LOHENGRIN 


 TANNHAUSER 


 HI 


 475 


 dure month 


 RGAN , PIANOFORTE , HARMONIUM , HAR- 


 TENTH SEASON , 1875—76 


 ENGLISH GLEE UNION , 


 assist 


 O CHORAL SOCIETIES , CATHEDRAL 


 476 


 GOODWIN 


 UNIVERSAL MUSIC LIBRARY , 


 MUSIC PUBLISHING DEPARTMENT 


 


 CHAPLIN HENRY . 48 


 important sale MUSICAL property . 


 business sale . 


 VILLAGE ORGANIST 


 NEW edition 


 DR . BENNETT GILBERT popular work 


 SCHOOL harmony 


 subject special exercise . 


 composition 


 JOHN GOSS 


 O PRAISE LORD 


 MENT 


 YANCY 


 VORK 


 477 


 ANGLICAN HYMN - BOOK 


 NEW EDITION , revise enlarge 


 ANGLICAN CHOKAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 OULE collection word 


 PSALTER , PROPER PSALMS , hymn , 


 OULE DIRECTORIUM CHORI ANGLI- 


 JOULES DIRECTORIUM CHORI ANGLI- 


 order HOLY COMMUNION . 


 REV . THOMAS HELMORE 


 MANUAL PLAIN SONG 


 publish 


 harmony BRIEF DIRECTORY 


 anthem ASCENSION day 


 BARNBY , J.—O RISEN LORD ... 0 


 OISE 


 GADSBY , HENRY.—THE LORD king .. 


 HANDEL.—LIFT head .. 


 KENT , J.—O L ' RD GOVERN 


 LAHEE , HENRY.—GRANT , BESEECH THEE 


 ROGERS , B.—LORD , shall dwell ? ... 


 WHIT SUNDAY 


 BARTHOLOMEW , R.—COME , HOLY SPIRIT , COME . 


 HH OO 


 QOHN NH H HW HH 


 MACFARREN , G. A.—O HOLY ‘ GHOST , ! 


 — HOLY , HOLY , OLY .. © 2 


 ye love 


 anthem WHITSUNTIDE 


 W. W. PEARSON . 


 greatly rejoice 


 short easy anthem 


 parish choir . 


 


 festival anthem 


 


 ACRED MUSIC ALEXANDER COOPER 


 composition WILHELM schulthe 


 VOCAL - song 


 PENNY . 


 MOUNTEBANK . 


 LOVER STAR 


 - song 


 


 CHARLES HARFORD LLOYD 


 SUNNY SHAFT behold . S.A.T.B. 


 PACK cloud away WELCOME day . S.A.T.B. 


 BEAUTY lie spring , A.T.T.B. 


 wet sheet flow sea . T.T.B.B. 


 CHORAL society . 


 ALTER HANDEL THORLEY im- 


 fourteen song 


 set 


 poem ROBERT BURNS , 


 content . 


 reduce shilling 


 national air country 


 word 


 THOMAS MOORE . 


 edit CHARLES W. GLOVER 


 reduce shilling . 


 original composition ORGAN 


 HENRY SMART 


 RNNWH HH MH MDD 


 FRSA SR SRE SN BRE 


 organist companion 


 collection voluntary 


 select arrange 


 SIR JOHN GOSS 


 composer majesty CHAPELS ROYAL , organist ( retire ) ST , PAUL CATHEDRALwee ! g. 
 Agnus Dei ( Mass ) bes ove 
 Dona Nobis , D ( sixth Mass ) « ila --- Mozart 
 introduction ... nts = e - . Rink 
 Lord , remember David ... wx « » Handel | freuet euch alles ( Tad Jesu ) 
 introduction + . Goss | Kyrie , g et 08 b axpen 
 Fugue , C , letter .. C.P.E. Bach Gloria , inf ... 
 Etregeeos(Te deum ) _ ... .. Graun 
 introductory Voluntary , f oa .. Rink | o Lord Governor ( Salmi ) ... x . Marcello 

 . 2 . | rejoice ( Anthem ) ae - . Dr. Croft 
 marvellous work ( creation ) ... pan « - Haydn | Lauda Sion , b flat ss ay ... Naumann 
 Amplius lava wmnigagd eb oe eo = Sarti Solemn March , e flat ... sep .. Dr. Boyce 
 introduction ... ee igs + . Goss | o , thou eternal God ( Crucifixion ) Ay ... Spohr 
 Fugue , e flat « . Albrechtsberger lead , lead ( Judas Mepiate ) ... Handel 
 prepare hymn ( Oceasional Oratorio ) « » Handel Slow mov ement , c = haz ... Beethoven 
 Dead March Sau + » Handel " . 15 . De cea 

 Dr. Croft 

 oan 0 J. & Bach Sancta Maria , inf ... - - .. Mozart 
 o , sweet ... ose os ent introduction fugue ' e flat rink ; 
 introduction rink et expecto resurrexionem et vitam ( fugue ) Cherubini 

 thou didst leave ( Messiah ) ws S. Handel . 16 . 
 Credo , b flat ( Mass ) ... = ... » Haydn ear hear ( Funeral Anthem ) , ... Handel 
 introductory voluntary , F ate és « -- Mozart mighty fallen 
 . 4 . deliver poor jogos Anthem ) Handel 
 Teach , o Lord ( Mount Sinai ) hep -- Neukomm Sanctus ( Mass C ) .. ss af ... Beethoven 
 ae oo mee beam sas ... Haydn Benedictus , F Caldara 
 anctus Dominus , D ayia « - Mozart ee ats ay aie 
 holy , holy ( Redemption ) ... eal qual anelante ( Salmi ) ave eso- veo coo Marcello 
 excellent ( Saul ) w _ ' « . Handel Benedictus , b flat ( Mass ) 
 prai thie’eord } 0 . 5 slow movement , E 
 raise ye ~ord | ( Mount Sinai ) _ ... .. Neukomm Kyrie b flat ( Mass ) . 
 glorify Lord .. introduction 
 fac ut portem ( Stabat Mater ) ... « - » Boccherini Donaaohis ( Mass ) .. 
 Quoniam , b flat ( Mass ) .. wae + . Haydn introductory voluntary , E 
 Agnus Dei , G ... Naumann Voliatar ' 
 Angels , bright fair ( Theodora ) .. Handel 4 : 18 . 
 introductory Voluntary , d 0 . « . Rink Recordare , f ( requiem ) s 
 ae : st 
 round ( Samson ) ous -- » Handel ome \ ( Judas Masih ) Handel 
 Benedictus , e flat ( Tenth Mass ) nig + + Mozart Perché m ’ hai derelitto ( Sev . en Words ) . Haydn 
 Introductory Voluntary , ind   ... M. Haydn death ( Susanna ) .. .. Handel 
 - aa si 9 ex principal major Cum Sancto Spiritu 4 
 key es bs Goss gloria dei ( Fugue ) ji tas oa . Haydn 
 fg 0 . 19 . 
 roy ig " } ( te deum ) i. se ) Baga Virgin Madre ye Wests ) sie « . Haydn 
 > river ( Salmi ) apa ... Marcello 
 Benedictus , B flat — ne ? vee Eybler Overture Saul ( 1st movement ) « ee ~Handel 
 et vitam venturi ‘ int « » Righini Adagio , E ( op . 2 ) Beethoven 
 Benedictus , ing . MM eS « - Righini et gi virdbus ( Fugue ) wee sie cy rc seas 
 conclude Voluntary , inc ves ae « » drobs q sess 4 
 . 8 . : Andante , D ( Sinfonia ) ... ... Mendelssohn 
 Introductory Voluntary , g : + rink achieve glorious work ( Creation ) - . Haydn 
 Benedicta et venerabilis ( Graduale ) « + oe = M. Haydn Hallelujah , amen ( Judas Maccabeus ) . Handel 
 Unendlicher ! Gott unser Herr } Ss J iis 
 woe pe .. Spohr ndante , F . « - Mozart 
 — — ist dein Nam ' . 5 " Cherubini Gloria Patri(Service ) . eee aid .. Dr. Croft 
 Benedictus , G ( Mass ) ae Introductory Voluntary , c Seika .. Rink 
 let acclamation ring ( Athaliah ) | - . Handel Andante . - - minor ey Mendelssohn 
 " ey r se sas 
 Benedictus , ion aati = Sea -+ » Mozart ye . 4 God ( Anthem ) ... < ee — aeons 
 lagvednction .. ... — -- Goss introduction air ( Des Heilands Letze 
 Fugue , g 7 sn ii ee ... Albrechtsberger Stund e ) Spohr 
 uoniam tu solus ... es = 
 ; é ae .. Righini Larghetto ( Sinfonia ' D ) 3 . ... Beethoven 
 cum Saneto Spirits J Lae | Pergolesi Occasional Overture ( 1st movement ) .. Handel 
 Leicht ist das Grab das Weltgericht ie ... Schneider Kyrie G ( Fugue Man : 22 . pear 
 0 . 10 . tie ugu Mass ) ... naa 
 trumpet shall sound ( Messiah ) ... Handel Slow Movement C ( op . 13 ) ah .. Beethoven 
 Gloria excelsis , C ( second Mass ) ... ... Mozart De torrente ( Dixit Dominus ) ie pr po 
 verdure clothe ( Crestion ) m " ne ... Haydn March Occasional Oratorio ... Handel 
 Fugue , ina ... e aee.:~“Padre Martini Concluding Voluntary , G ( Fantasia ) J. S. Bach 
 Quoni — . 11 . . . 23 . 
 uoniam , e flat ... cry aloud shout ° Dr. Croft 
 } ( second Mass ) . rr errs Slow Movement , d fteoae Sinfonia G ) ... Haydn 

 Cum Sancto Spiritu 
 know Redeemer liveth ( Messiah ) - . Handel Blest depart ( judgment ) -.- Spohr 

