


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


FOUNDED IN 15844. 


PUBLISHED ON THE FIRST OF EVERY MONTH


JULY


ROYAL CHORAL SOCIETY. ROYAL 


P. QUARRY, 


3IRMINGHAM & MIDLAND INSTITUTE. 


PRINCE CONSORT ROAD, SOUTH KENSINGTON, S.W. —_———_—_ 


10) 


A COMMEMORATIVE


THE ROYAL COLLEGE OF ORGANISTS


UNIVERSITY OF DURHAM. LONDON ‘COLLEGE OF MUSIC, 


INCORPORATED 1801. EXAMINATIONS—LOCAL AND HIGHER


MR. W. H. BREARE


MUSICIANS. TO STUDENTS AND THE PROFESSION. 


REGISTER OF ORGAN VACANCIES


— AUGUST 16 AND 17 : COMPETITIONS


AL


XUM


1916


MR. FIELD HYDE


THE ART OF VOICE TRAINING


’ WRITTEN SCORE. 


OVERCOME


NO APPARATUS OR SPECIALL) 


ALL DEFECTS OF TECHNIQUE RAPIDLY


TO EVERY PIANIST WHO WISHES 


DURING THE SUMMER MONTH: 


PRACTISING


THE SYSTEM IS INVALUABLE 


MACDONALD SMITH


PROFESSIONAL NOTICES. 


MR. SAMUEL MASTERS


TENOR). 


THE LONDON COLLEGE FOR CHORISTERS. 


COMPOSERS’ MSS


REVISED AND PREPARED FOR PRINTING. 


DR. A. EAGLEFIELD HULL


_ L.R.A.M. (PAPER WORK). 


L.R.A.M. EXAMS


A.R.C.M. (PAPER WORK). 


A.R.C.M


R.C.O. SPECIALIST IN CORRESPONDENCE 


DR. LEWIS’ TEXT-BOOKS


ISS MARGARET YOUNG,L.R.A.M.,A.R.C.M


LINCOLN CATHEDRAL


THE CHURCH OF ST. MARTIN-IN-THE 


RMAGH CATHEDRAL. TENOR WANTED. 


RGANIST AND CHOIRMASTER WANTED 


GIRLS’ HIGH SCHOOL, CHESTERFIELD


RAINBOW


OUR MUSIC READING LADDER FOR I-EGINNERS


WEBSTER’S 


CHILD'S PRIMER


THEORY OF MUSIC


TWENTY-THIRD THOUSAND


DOLL’S-HOUSE SUITE


FIVE LITTLE PIECES 4


LITTLE FOLK 


PIANOFORTE SOLO. 


COMPOSED KV 


HUBERT BATH


MANUSCRIPT PAPER. 


D PIPE 


PES, 4


JRGAN 


WITT. 


JUST PU


THE


TO WOMEN


SPIRIT


PRICE EIGHTPENCE


LAURENCE


BLISHED


ENGLAND 


FOR THE FALLEN


OF


BINYON


SET TO MUSIC FOR TENOR OR SOPRANO SOLO, CHORUS, AND ORCHESTRA


BY


EDWARD ELGAR


AND 3


PRESS NOTICES


DAILY TELEGRAPII


MORNING POST


DAILY CIIRONICLE


DAILY NEWS


OBSERVER


GLOPF


BIRMINGHAM DAILY POST


318


THE RT. HON


UST PUBLISHED


THE ORGAN WORKS


BROADWOOD~ 


PIANOS


GRAHAM P. MOORE, Beringer’s Piano Tutor — - - 2 
The best and most practical ever published 
English and Foreign fingering

Portrait Classics 
The cheapest and best English Edition of the works of Beethoven, 
Bach, Mozart, Chopin, &c. Always ask for Boswortn's Portrait 
Classics, 
Tha sa = ° ee ‘ : 
rhe Lyric Organist. 3 Vols., each 1s. od. 
Edited by Staniev Rorrr. Vol. 
collection

pn




BOSWORTH & CO


JOHN SEBASTIAN BACH. | 


THE CHORALE PRELUDES


WITH AN INTRODUCTION BY


ERNEST NEWMAN


BOOK XV. 


ORGELBUCHLEIN 


LITTLE ORGAN BOOK


IVOR ATKINS


1916


NOW READY


PART I. 


OF A 


BACH'S 


48 


PRELUDES & FUGUES


THE ‘WELL-TEMPERED CLAVICHORD


I—724


BOSWORTH EDITION


D OF


44


XUM


AND SINGING-CLASS C/RCULAR. 


JULY 1, 1916


THE SOCIETY OF ENGLISH SINGERS. 


319


RECOMMENDATIONS ON THE EDUCATION OF SINGERS


321


% RS


CARMEN VOCIS


323


324


325


THE ORGAN AS A SOLO INSTRUMENT. 


THE IDEAL ORGAN


THE MUSICAL T


326


ON PROGRAMMES


ON ‘ ARRANGEMENTS


ON TRICKS


327


A WORD TO OURSELVES


COMMENTS FROM AN ORGAN LOFT. Mr. Bernard Johnson’s article contains much that 
badly needed saying, and I feel pretty sure that 
organists generally, both of the concert and church 
breeds, will heartily agree with him, save in one or two 
matters of detail. For example, he expresses a fear 
that ‘many, if not most, church organists regard the

acert-player as a trickster of the deepest dye—as 
#man who deliberately “lowers the dignity of the 
instrument.”’ He may rest assured that we church 
organists have nothing but admiration and sympathy 
for the work of the best concert organists. Most 
of us feel that the average pianoforte recitalist, 
who gets through a season by ringing the changes 
on a Chopin selection, a couple of Beethoven 
sonatas, a Bach dis-arrangement, and a few other 
standing dishes, with a small group of Debussy, 
Ravel, or Scriabin, just to show his up-to-dateness, 
has an easy task, both mentally and physically, beside 
the municipal organist, who plays at least weekly, 
during the greater part of the year, rarely repeats 
items, and shows an amount of musicianship, taste, 
and resource in his adaptations of pianoforte and 
orchestral works that is demanded of no other 
executant. But all concert organists are not so 
conscientious as Mr. Johnson. There are some, by no

means a negligible minority, who still ‘ tickle the ears 
of the groundlings’ with representations of church 
services or concerts on lakes interrupted by various 
kinds of bad weather—always, however, including

THE SUPPLY OF REAL ORGAN MUSIC

But I fancy many of my church colleagues will be 
inclined to join issue with Mr. Johnson when he 
defends the use of transcriptions by bringing forward 
the well-worn allegation that ‘the literature of the 
organ is extremely limited.’ I have heard this said 
many times, but can never get anything in the way of 
evidence beyond the fact that Beethoven, Mozart, 
Wagner, and a few other classical composers wrote no 
organ music. This is true, but quite immaterial. 
The fact remains that there is a very large organ 
literature of the highest class. If we examine the 
published lists of recital programmes we see little 
evidence of this, I admit. But, for the matter of that, 
if we study a hundred pianoforte or vocal recital 
programmes, we shall come to the conclusion that 
there is not, and never has been, much good pianoforte 
music, or more than a few score of good songs. 
Organists, like too many of their brothers and sisters 
of the concert platform (and even of the conductor’s 
desk), prefer the safe and easy game of follow-my- 
leader to the troublesome and ungrateful réle of 
pioneer

Evidence? Take one organ composer only—Widor. 
He composed ten Symphonies. The general level of 
achievement is very high, and many of the movements 
are amongst the finest things in organ music. How 
many of our concert organists draw on him for 
anything but the Pontifical March of No. 1, the 
Finale of No. 2, the Scherzo, Andante Cantabile, 
and Finale of No. 4, and the first movement, 
Allegretto, and Toccata of No. 5? Portions of the 
magnificent No. 6 are sometimes heard (more, I think, 
in churches than in concert halls, though it is concert 
music of the finest); but it is safe to say that 
Nos. 7, 8, 9, and 1o are practically unknown in 
England. Nos. 9 and 10 have some dry pages, 
perhaps, but I am convinced that in 7 and 8 
(especially the former) we have movements that are 
worthy of being placed among the finest things in 
music (in music, not merely organ music). Most 
of these Widor works demand for their proper 
presentation the two things that all concert organists 
have,—fine instruments and brilliant technique. The 
average church organist, as I said above, has rarely 
both, for obvious reasons. Clearly, it is ‘up to’ the




1, $0, 


329


MISSIONARY WORK


LIGHT MUSIC IN CHURCH


EDINBURGH SOCIETY OF ORGANISTS. 


ORGAN RECITALS


331


BOOKS RECEIVED


A LITTLE BACH PROBLEM.’ 


TO THE EDITOR OF ‘THE MUSICAL TIMES


TO THE EDITOR OF ‘THE MUSICAL TIMES,’ Mr. Newman mentions among the titles of preludes left 
uncomposed in Bach’s manuscript, ‘ Komm, heiliger Geist, 
erfiill’ die Herzen,’ and adds that he does not \know what 
hymn this is. It is not, properly speaking, a hymn at all, 
but a prose translation of an old Latin Antiphon for Whit- 
Sunday, of which there is also the metrical version, ‘ Komm, 
heiliger Geist, Herre Gott,’ with two additional stanzas by 
Luther. Both the prose and the metrical version were in 
use in Lutheran churches: the prose version as adapted to the 
old Plainsong of the original Latin; the metrical version 
with another tune which first appears in ‘ Walther’s 
Gesangbuch,’ 1524. It would appear from what Mr. 
Newman quotes that Bach intended to compose Preludes 
on both the Plainsong and the newer tune. Even in so 
pietistic a book as ‘ Freylinghausen’s Gesangbuch’ one finds 
both the prose version with its adapted Plainsong and the 
metrical version with the new tune. Both are also 
contained in the Saxe-Weissenfels book of 1712. The Latin 
text with its Plainsong is given as the Lutheran Antiphon 
for Whitsun Eve in L. Lossius’s ‘ Psalmodia,’ 1595. Along 
with the use of Latin in the service, Lutheran churches 
retained for some time more of the tradition of Plainsong 
than we did. Although this Antiphon no longer appears in 
the usual Roman books, Baumker (‘Das _ Katholische 
Kirchenlied ’) mentions that it is still sung in several dioceses 
of Roman Catholic Germany before the High Mass on 
Whit-Sunday

The interest one has felt in the old Lutheran hymns and 
music makes one regret all the more keenly that modern 
Germany should have so completely turned its back upon its 
musical-religious past. The spirit of modern Germany is 
certainly not that of Paul Gerhardt and Sebastian Bach, any 
more than it is that of Beethoven or Goethe and Schiller

Gese’s 
five-voice setting is given in Schéberlein’s ‘Schatz des 
The Ascension hymn of 
Gregor Ritzsch beginning with the same words appeared in 
1620, and while manifestly based on the older Easter hymn, | + 
and meant to be sung to the same tune, did not meet with a 
Of the older _

ince il ans ititaites 
ee ee

This typical specimen of our vocal melody certainly does not 
fit in with Mr. Scott’s ‘tunes which everybody can get 
hold of,’ nor has it the smallest feature in common with 
anything to be found in Bach, Mozart, Beethoven, 
Schumann, Schubert, Chopin, &c. It is therefore nota 
melody according to Mr. Scott

What you and your readers will observe in the quotation 
above is its very striking affinity with many a theme 
from ultra-modern Occidental music—a fact of the very 
highest significance




ALT


—<—$——. SONG FROM ‘“* KING ARTHUR” 


ALTO. = —- F3 


SS! SS = SSS ES - =: SS SSE 


PAN. CRN OCB NL


ZN


FAIREST ISLE


337


HANDEL AT CANONS. 


TO THE EDITOR OF ‘THE MUSICAL TIMES


R. A. STREATFEILD


338 THE MUSICAL T


PARIS AND THE LAST COLONNE-LAMOUREUX 


CONCERT. ROYAL INSTITUTION LECTURES

On May 25, June 1 and 8, a series of lectures was 
delivered by Sir Alexander Mackenzie, his subject on the 
first date being ‘ The Beginnings of the Orchestra and its 
Early Combinations.’ He said that the modern orchestra 
was the long and slowly attained result of much experiment, 
ingenuity, and artistry. In order to arrive at an estimate of 
the now completed labours of centuries, it was necessary to 
consider the gradual improvements in the construction of the 
instruments, the invention of new ones, and the efforts of 
musicians to make them speak in concert. It was more than 
doubtful whether Bach ever heard any of his instrumental 
pieces played by others as conceived in his brain. Beethoven 
made unrelenting demands upon the instrumentalists of his 
day, and it was improbable that the high degree of executive 
skill for which his music called could have been quite 
realised

Up to a comparatively recent period the music of the great 
masters compelled the rapid advance of technique, but within 
living memory we had witnessed a reversal of the process ; 
the brilliant accomplishments of the executive artist have 
reacted on the work of the composer, and that to an extent 
that it was not easy to say which was the master and which 
the servant. The desire to keep on adding to the colours on 
an over-filled palette was only a lust for show and mere 
noise. Alps had been rising upon Alps. Certain purple 
patches in the instrumentation of ‘‘ Elektra,” for instance, 
had caused a wicked satirical verse-maker of his acquaintance 
to remark that the orchestra




BRITAIN’S MUSIC TRADE


THE BACH CHOIR


ROYAL COLLEGE OF MUSIC


341


LONDON SYMPHONY ORCHESTRA. 


QUEEN’S HALL


BEECHAM OPERA SEASON, 


ALDWYCH THEATRE


LONDON STRING QUARTETMiss Doris Manuelle brought forward a good programme 
(all foreign) at her recital on May 24, and showed that she 
is making great progress as an artistic singer

A concert was given by the South Hampstead Orchestra, 
under the direction of Mrs. Julian Marshall, at the Hampstead 
Conservatoire, on May 25, The programme included some 
quaint and interesting old Flemish Folk-songs transcribed 
for orchestra by Arthur de Greef, Dvorak’s Legend No. 10 
and the Scherzo from his first Symphony, Beethoven's 
Symphony in C minor, and Elgar’s ‘Carillon.’ The last- 
named work, in which the poem was dramatically recited 
by Mr. Charles Fry, had a very enthusiastic reception

At a remarkable performance of ‘ The Hymn of Praise,’ 
given on May 25 at the Great Central Hall, Bermondsey, 
under Dr. J. E. Borland, five hundred children from the 
local elementary schools co-operated in singing the melody 
of the Chorale and elsewhere in the work. A truly 
educational idea




BY OUR OWN CORRESPONDENTSappeared in our public parks with great popular success. | On May 25, Miss Margaret Bennett gave a pianoforte

recital of works by Beethoven, Mendelssohn, Liszt, Bax

BOURNEMOUTH. | Dohnanyi, and Debussy




CORNWALL


LIVERPOOL


MANCHESTER AND DISTRICT


344


NEWCASTLE AND DISTRICT


OXFORD


345


SHEFFIELD AND DISTRICTHonG-Konc.—Maunder’s ‘ Olivet to Calvary’ was sung 
at the Union Church during Passiontide by the combined 
choirs of the Union and Wesleyan Churches. Mr. 
E. J. Chapman was at the organ, and a good per- 
formance was appreciated by a crowded congregation

HUDDERSFIELD.—Dr. Eaglefield Hull gave a concert on 
May 24, when the following artists appeared: Mr. John Dunn 
(violin), Mrs. W. H. Vanner (pianoforte), Madame Rina 
Robinson, Madame Bessie Taylor, Miss Doris Hall, and 
Mrs. E. R. Benson. The orchestra played Beethoven’s 
‘Egmont’ Overture, Elgar’s ‘Carillon,’ and Fletcher’s Folk- 
tune and Fiddle-dance. Two Shakespearean dances were 
also given to an accompaniment of viols. As a result of the 
concert £185 was handed over to War Funds

JOHANNESBURG.—The new organ erected by Messrs. 
Norman & Beard in the Town Hall was opened on 
March 4 by Mr. Alfred Hollins. We learn from the 
Johannesburg Star that the brilliant recitalist was at his 
best, and that a packed audience gave him an enthusiastic 
reception. Mr. Hollins’s chief items were Mendelssohn’s 
first Sonata, the F major Toccata of Bach, and the 
‘William Tell’ Overture. He also played two pieces of 
his own, written for the occasion, effectively using the 
glockenspiel and carillon, and he improvised in masterly 
manner. From an interesting booklet descriptive of the 
organ we learn that the instrument is a great success, that it 
contains ninety-seven stops, with a bewildering array of 
accessories, among which are percussion instruments, —bass 
drum, side-drum (both with ‘tap’ and ‘roll’ actions), and 
triangle, and that it cost over £13,000




DURING THE LAST MONTH. 


PUBLISHED FOR 


THE H. W. GRAY CO., NEW YORK


READ MUSIC AT SIGHT


T. CUTHBERT’S, KENSINGTON.—SECOND 


TWENTY-ONE GUINEAS OFFERED! 


ANNIVERSARY TUNES & AN ANTHEM


JAMES BROADBENT & SON, LTD, 


13, BRUNSWICK PLACE, LEEDS. 


ARRIVED AT LAST


THE


MUSIC TURNING DEVICE


IRK


E. 50 


A.A. 12 


GANIST, 


COND


EDI 


HEM


LTD


THE MUSICAL


347


ANTHEMS 


TRINITYTIDE


LIST


THE “LUTE” SERIES


MUSIC FOR


HARVEST & OTHER SEASONS


ANTHEMS. 


SERVICES


ORGAN MUSIC


POPULAR CHURCH MUSIC


J. H. MAUNDER


SERVICES, 


ORGAN, 


CHURCH CANTATA, 


348


THE “LUTE” SERIES OF HARVEST ANTHEMS


BE


AEEREREEEEREREE


FOR


BRERRRSRRSS


CEBRERERERERER QR


PERER SEAR TE ARSR RS


HARVEST FESTIVAL MUSIC


CANTATAS. 


SONG OF THANKSGIVING HARVEST CANTATA


FOR SOPRANO, TENOR, AND BASS (OR CONTRALTO) SOLI FOR SOPRANO (OR TENOR) AND CONTRALTO (OR


AND CHORUS BARITONE) SOLI AND CHORUS 


SHAPCOTT WENSLEY JULIUS HARI HARRISON. 


A GOLDEN HARVEST


FOR TENOR AND BASS SOLI AND CHORUS JOHN E. WEST. 


HENRY KNIGHT


THE MUSIC COMPOSED BY A SONG OF THANKSGIVING


THOMAS ADAMS, FOR CHORUS — ORCHESTRA 


THE RAINBOW OF PEACE 


FOR TENOR AND BASS SOLI, CHORUS, AND ORGAN THE GLEANER’S HARVEST


BY 


HARVEST-TIDE 


FOR TENOR AND BASS SOLI, CHORUS, AND ORGAN OR | THE JUBILEE CANTATA 


SMALL ORCHESTRA FOR SOLO VOICES, CHORUS, AND ORCHESTRA


HARVEST CANTATA A HARVEST SONG 


FOR CHORUS, SEMI-CHORUS, AND ORGAN FOR SOPRANO SOLO AND CHORUS


BY BY 


GEORGE GARRETT. C. LEE WILLIAMS. 


TWELVE HYMNS FOR HARVEST | THE SOWER WENT FORTH SOWING


THE JOY OF HARVEST AND SEA 


A HARVEST HYMN OF PRAISE COME, YE THANKFUL PEOPLE, COME 


C. HUBERT H. PARRY


BASIL HARWOOD


SECOND SET


PUBLISHED IN JULY


ELG


CHARLES LISTER BRADLEY, T ? 


IRGAN


RGAN


OOK


ES


NOVELLOS


SMALL ORCHESTRAS


SPECIALLY SUITABLE FOR CINEMAS


FOR


MUSIC


CARSE, A. VON AHN 


COLERIDGE-TAYLOR, S. 


COWEN, F. H


COWEN, F. H


ELGAR, EDWARD... 


ELGAR, EDWARD... 


ELGAR, EDWARD... 


ELGAR, EDWARD... 


ELGAR, EDWARD


FLETCHER, PERCY E


GOUNOD


HOLBROOKE, JOSEPH


HOLST, G. VON 


JOHNSON, BERNARD 


KREUZ, EMIL


MARE, EDWIN H. 


LEMARE, EDWIN H. 


LEMARE, EDWIN H. 


REED, W. H


WENDT, THEO. 


WEST, JOHN E. 


WEST, JOHN E


DAVIES, H. WALFORD... 


DAVIES, H. WALFORD


ABBREVIATIONS


NN


352


CHAPPELL & CO.”S LATEST


SUCCESSFUL SONGS AND BALLADS


LIZA LEHMANN


GUY DHARDELOT


TERESA DEL RIEGO


FLORENCK AYLWARD


DOROTHY FORSTER


TAKE ME TO FLOWERLAND WITH


ELLEN TUCKFIELD


A SILHOUETTE.” 


LILIAN RAY


MYRTA GAMBLE


EDWARD GERMAN. 


HERMANN LOHR. 


MONTAGUE F. PHILLIPS. 


ROBERT CONINGSBY CLARKE. 


HAYDN WOOD. 


“ ROSE OF THE MORNING


DEAR HANDS THAT GAVE ME VIOLFTS


ERIC COATES, 


ALEC WILSON. 


‘* STEPPIN’ DOWN ALONG THE ROAD


FROM HEAVEN). 


F. S. BREVILLE-SMITH. 


GRAHAM PEEL. 


FOR


SOLDIERS, SAILORS, SCHOOLS, HOMES


ETC


CONTAINING : NATIONAL ANTHEMS, ETC., OF THE ALLIES. 


MARCHING SONGS. 


NATIONAL AND FOLK-SONGS. 


HYMNS


PRICE ONE SHILLING


MADE IN ENGLAND