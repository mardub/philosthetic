


THE


ROY AL ACADE


PRINCE CONSORT ROAD, S


COMPLETE MUSICAL E DUC: 


THE ROYAL — G


I ERM 


ROYAL CHORAL SOCIETY. | FIRST PERFORMANCES


MIDSUMMER HALF-TERM B 


ENTRANCE EXAMINATION, 


CL


WEEKLY ORCHESTRAL PRACTICES ARE CONDUCTED


SCHOOL OF


SESSION 1913-1914


MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


FOUNDED IN 1844


PUBLISHED ON THE FIRST OF EVERY MONTH


JUNE 1, 1914


14—1915


ROYAL 


MANCHESTER COLLEGE OF MUSIC. 


M. ANC HESTER SCHOOL OF MUSIC


E OF ORGANISTS


MUSIC. THE TEACHING OF TEACHERS. 


362 THE MUSICAL


THE


ASSOCIATED BOARD 


OF THE R.A.M. AND R.C.M. 


FOR LOCAL EXAMINATIONS IN MUSIC


PIANOFORTE 


HALL, NEW


GCHUMANN STUDIOS


EOLIAN


BOND STREET, W


COMPLETE TRAINING 


PRINCIPAL: MME


FOR PIANISTS


AMINA GOODWIN


SCHUMANN SCHOOLS


THE “AMINA GOODWIN SCHOLARSHIP. 


ALSO


A “ MOZART” SC HOLARSHIP FOR CHILDREN 


UNIVERSITY OF 


FACULTY OF


MANCHESTER. 


MUSIC


7, MORTIMER STREET, REGENT STREET, 


LONDON, W


THE SECRET


LONDON COLLEGE OF MUSIC. 


T. WEEKES


INCORPORATED GUILD OF CHURCH 


MUSICIANS 


ASSOCIATE (A.1.G.C.M.), LICENTIATE (L.1.G.C.M.), FEL


REGISTER OF ORGAN VACANCIES. 


V VICTORIA COL L EGE OF MUSIC, 


LONDON. 


INCORPORATED 1891. 


SI 17, YORK PLACE, W


} MR. THORPE BATES


1914 


SALES 


2 I WEDDINGS AND SPECIAL SERVICES


CHARLES HARPER 


(SOLO ALTO, ST. MARY, STOKE NEWINGTON


| WINIFRED MARWOOD 


BARITONE


OF PIANOFORTE-PLAYING. MR. G -EORGE PARKER _ 


‘HOW TO ACQUIRE EASE OF VOICE PRODUCTION. 


RALTO


A.M


RS


TELL


OPRANO


ONDON


TION


365


CANTATA—ORATORIO—BALLAD


ERNEST COSTA


BASS-BARITONE


LAND ST


MR. WILLIAM COLEMAN


THE MANCHESTER BASS). 


M R. 


CHARLESWORTH GEORGE 


B ASS-BARITONE). 


MR. LEONARD HUBBARD


MR. W. H. BREARE 


TO STUDENTS AND THE PROFESSION. 


MESSIAH


MISS ETHEL VISICK 


RECEIVES PUPILS FOR PIANOFORTE PLAYING. 


THE LONDON COLLSSS FOR CHORISTERS


COMPOSERS’ MSS. 


DR. A. EAGLEFIELD HULL


OF MUSICAL COMPOSITIONS


L.R.A.M. (PAPER WORK). 


LATEST SUCCESSES :— 


L.R.A.M. EXAMS., 1010-13 


A.R.C.M. (PAPER WORK). 


F.R.C.O. SPECIALIST IN CORRESPONDENCE 


THI


5999 


COACHING FOR DEGREES. I.R.A.M., 97-101 INE \ TANTED, ORGANIST AND CHOIRMASTER


GE. 


¢ — AR


___


ETARY


ASTER


HOIR


SALE. 


THE MUSICAL


MINIATURE 


OSTROVSKY APPLIANCE


N.W


T » 19 


48, SOUTHFIELD ROAD, OXFORD. SIX EASY PIECES 


126, HOLT ROAD, LIVERPOOL. FOR VIOLIN AND PIANOFORTE


BY


VIOLIN AND P IANOFORTE


COMPOSED BY CONTENTS


TRINITY COLLEGE


INSTITUTED 1872


WORCESTER


IVELLO'S 


GRADED PIANOFORTE


PLAYER-PIANOS 


ATION


10S


ARKET


ON, W


M.A


1 CHANTS 


CREED


RD


CHESTRA


CLERGY


369


AND SINGING-CLASS CIRCULAR


JUNE 1, 1914HENRI VERBRUGGHEN

When Mr. Daniel Mayer conceived the idea 
of holding a Beethoven Festival at Queen’s Hall 
. this spring, he had first to consider to whom the 
chief direction of such an important musical 
enterprise should be entrusted. The scheme of 
the five concerts included all the nine Symphonies, 
the five Pianoforte concertos, the Violin concerto, 
and various vocal works, and for their due 
performance the London Symphony Orchestra, the 
leeds Philharmonic Choir, some of the greatest 
living pianists, and other soloists were engaged. 
- As we all know Mr. Mayer’s choice fell upon 
Henri Verbrugghen, who, as solo violinist, quartet 
leader, and conductor, had devoted a considerable 
part of his artistic life to the study of all Beethoven’s 
important instrumental compositions except the 
Pianoforte sonatas

It isa pleasure to record that Mr. Verbrugghen 
proved fully equal to the occasion, which was 
probably the most important one in his successful 
artistic career. Special interest was created in his 
interpretation of the Symphonies, because it was 
known that he would introduce certain bold devices 
of his own in order to adapt the orchestration to 
the balance of tone in a large modern orchestra. 
Some of the details of these devices are explained 
below, but it may be said at once that they were 
conceived and carried out in a spirit of reverence 
and respect for the great composer, and with no 
idea of improving his masterpieces

1873. He was the only child of a Belgian 
merchant. His mother had considerable musical

culty, which found happy vent in a devoted 
application to Beethoven’s chamber music. The 
home was the resort of many well-known artists, 
and thus the bias of the boy to the classical 
master was initiated. Henri took eagerly to the 
violin, and made a public appearance as a soloist 
when he was only eight years of age. Later 
his parents desired to shape his education towards 
medicine rather than music, but when at the age 
of thirteen a decision had to be made, the advice 
of Joseph Wieniawski, and Gevaert (the director 
of the Brussels Cénservatoire), was sought, with the 
result that music won the day, and Henri entered 
the Conservatoire and became a pupil of 
Hubay, and, later, of Ysaye, who took unusual 
merest in his musical development. Whilst at 
the Conservatoire he won many prizes, and when

was sixteen he made an important step 
forward in turning special attention to the study 
orchestration. Soon after he was admitted

and joined the Scottish Orchestra, which 
was then founded by George Henschel. He 
also during this summer led the _ orchestra

under Jules Riviere at Llandudno, and later he 
became a member of the Lamoureux Orchestra at 
Paris. During the three following years he was 
deputy-conductor at Llandudno, and it was this 
experience that finally determined him to adopt 
the career of a conductor. He became director of 
music at Colwyn Bay (a position he retained 
|for four years), and he returned to the Scottish 
Orchestra, which was then under Wilhelm Kes 
(the founder of the Concertgebouw Orchestra at 
Amsterdam, now conducted by Mengelberg), and in 
1902 he was appointed leader and deputy-conductor 
of the Scottish Orchestra, under Dr. (now Sir 
| Frederic) Cowen. In the summer of that year 
and for three succeeding years he was leader of 
| the Queen’s Hall Orchestra during the Promenade 
season. After that period he centred attention 
}on Glasgow, where at the Athenzum, which is 
!one of the most important musical educational 
institutions in Scotland, he is chief violin professor, 
}and in 1911 he succeeded Dr. Coward as conductor 
| of the Glasgow Choral Union. During recent years 
|he has conducted symphony concerts at Brussels, 
| Berlin, Munich, and St. Petersburg. Notwith- 
| standing all these varied and widespread activities, 
he has formed a string quartet party, which has 
appeared in many places in Scotland and 
jelsewhere. The present speciality of this party 
lis the sixteen Quartets of Beethoven, the whole 
of which have lately been performed at chamber 
music concerts at Glasgow and Edinburgh

One of the features of the Athenzeum music 
school is the orchestral class of forty string players. 
Mr. Verbrugghen finds it perfectly possible to train 
this class to a high degree of perfection that cannot 
be reached by amateur players on wind instruments. 
3ut he thinks it desirable that his players should 
not restrict their repertory to pieces composed for 
strings only, and so for the purpose of study the 
string parts of symphonies and other works written 
for full orchestra are studied and the missing parts 
are filled in on the pianoforte. For a few years an 
operatic class was a success, but it had to be 
abandoned because it was found to absorb dispro- 
portionately the energies of the school

BEETHOVEN’S SYMPHONIES

The idea of doubling wind parts in Beethoven’s 
symphonies in order to balance the mass of 
strings now employed in a modern full orchestra is 
not a new one. It is an obvious apparent escape 
from a difficulty ; but it is equally obvious that if 
it is applied crudely it leads to more difficulty than 
it removes. ‘There are numerous passages in the 
symphonies that clearly can only be played properly

amember of the Society of Conservatoire




37THE MUSICAL TIMES.—June 1, 1914

conditions, to give scope to his _ individual 
temperament. ‘This consideration alone shows that 
the whole matter must be dealt with scientifically. 
It might seem sufficient to rule that the extra 
instruments are to be used only in Zxééis. But 
Mr. Verbrugghen goes further than this. He 
studies the physical convenience of his principal 
wind players whoimmediately after the characteristic 
gusts of forfe and sforsando, in which Beethoven 
often indulges, are called upon to play delicately : 
a transition of effort that more or less embarrasses 
the most skilful performer

We now give a few examples of Mr. Verbrugghen’s 
method of treatment




= SS SS SS SS


TS 


IN ENGLAND


BOUR


THE B 


SE


MORE 


THE MUSIC AND MUSICAL INSTRU MENTS 


OF THE BIBLE


1914) 


_—____


, 2200


10


IV


I000 B.C


ART


1200 B.C


7, 94


152


378


5555, TT SF


IA 


THE MUSICAL TI


SOME MUSICAL EPITAPHS. 


W,2 K. S


380


DICTION. 


_=—_ CO 


‘A. &M 


385


386


1914


MR. FRED COZENS


ORGAN RECITALS


APPOINTMENTS


CEN 


383


———— 477


W.M


THE MUSICAL TI


MES.—JUNE 1, 1914


BOOKS RECEIVED


403


1674


THE MANUSCRIPTS OF CLEMENTI. 


TO THE EDITOR OF ‘THE MUSICAL TIMES,’ 


WILLIAM


II, CUMMINGS


A SWELL-BOX FOR THE TUBA. 


TO THI AL TIMES


BURR


HUDDERSFIELD ORGANISTS


399public appearance was made as Richard Coeur de Lion in 
the original production of Sullivan’s ‘Ivanhoe’ in 1891

following a conductor as painstaking and inspiring as Si 
Henry Wood, who has been compelled by his many duties 
to relinquish this post; but Mr. Balling soon acquired th 
confidence of his singers, and the concerts have never bee 
more exhilarating and uniformly enjoyable. The princip 
choral works were Bach’s Cantata, ‘ Sleepers, wake,’ and 
double-chorus, ‘ Nun ist das Heil,’ Brahms’s ‘ Song of Destiny, 
Dvorak’s ‘The Spectre’s Bride,’ and Hamilton Harty: 
* Mystic Trumpeter’ (conducted by the composer), all of whit 
were sung with a spirit and force that never slackened, whil 
the volume of tone produced by the choir of some 350 voi 
‘ ; ; |(a different body on each occasion) was excellent. Th 
performance of * Der Rosenkavalier. programme was not confined to choral music, and one coulé 
AMBROSE AUSTIN, on May 14, at the age of eighty-seven. | not but realise the value of the object-lesson when the chor 
For thirty-two years manager of the old St. James’s Hall, | were listening (and most of them did listen attentive 
Piccadilly, Mr. Austin was a well-known personality. to Beethoven’s eighth Symphony, the ‘Oberon’ a0 
JAMES ALEXANDER BROWNE, on April 25, in his seventy- ‘ Meistersinger ’ Overtures, and the ‘ Thalassa’ Symphony

assistant to Sir Arthur Sullivan and to Dr. A. L. Peace, 
both of whom thought highly of his gifts




FIRST STAGE OF THE SEASON


THE GERMAN OPERAS


ITALIAN OPERA


THE STATE PERFORMANCE


MUSIC OF ELIZABETHAN CHOIRBOY PLAYS


THE


ADOLF HENSELT: IN MEMORIAM


MR. ARTHUR HERVEY’S OPERA


THE IMPERIAL CHOIR


THE SOUTH WALES FESTIVALenjoyable programme

Beethoven’s sev

style modern, so that all requirements of modern opera are | Sir Frederic Cowen’s ‘The Veil’ was given by the Neat




SOPRANO


ALTO


TO THE CONDUCTOR AND MEMBERS OF THE MAIDSTONE VOCAL UNION. 


~~: AN UNACCOMPANIED PART SONG FOR §.A.T.B. 


LOVE WAKES AND WEEPS


= = SSS SS SSE LSE —— 


___ LOVE WAKES AND WEEPS


—_ { —_— — 


@_+5 @ = = 


(| $34


LOVE WAKES AND WEEPS


#3 ————— = “E'S $4 


$722 


LUVE WAKES AND WEEPS


XN


LOVE WAKES AND WEEPS


POPU: 


399


TRINITY COLLEGE OF MUSICPOPULAR CLASSICAL CONCERTS AT SWINDON: 
AN EXPERIMENT IN MUSICAL EDUCATION

On April 20 a concert in the Mechanics’ Institute at 
Swindon brought to an end a most successful series of 
Classical Concerts which had been given there during the 
past winter at genuinely popular prices before a genuinely 
working-class audience. The concerts were organized 
by Mr. Felix Schuster, who gave a similar series two years 
220, at the request of the Swindon branch of the Workers’ 
Educational Association. Throughout the season the 
audiences have averaged from four to five hundred, and the 
gympathy and intelligent appreciation of the music shown 
by all who heard it have been quite remarkable. The 
programmes have included Violin sonatas by Beethoven, 
Handel, Tartini, César Franck, and Ludwig Thuille, 
Brahms’s Pianoforte quartet in A major (Op. 26), Dvorak’s 
Concerto for violoncello and his ‘ Dumka’ Trio, and a Bach 
Concerto for two violins. Among the vocal items have 
been Arias by Mozart and Verdi, songs by Brahms, 
Schumann, Couperin, and Gluck, and Vaughan Williams’s 
Song-cycle ‘On Wenlock Edge.’ Beethoven’s Pianoforte 
snata in A flat (Op. 110) met with a_ peculiarly 
enthusiastic reception. On March g Mr. Percy Such’s 
String Orchestra played, and the programme consisted of 
Mozart’s Eine Kleine Nachtmusik and Concerto for pianoforte 
in E flat (No. 9); Bach’s Concerto for violin in E major ; 
W. Richter’s Abendgesang; and Percy Grainger’s Mock 
Morris

Besides Mr. and Mrs. Schuster, the performers who have 
generously given help in this series of concerts have included 
Miss Letty Maitland, Mr. and Mrs. Percy Such, Mr. Percy 
Sharman, Mr. Steuart Wilson, and Mr. A. P. Fachiri. 
Altogether the experiment has been most successful, and it 
is earnestly hoped that it will be repeated next year, 
and that the example of Swindon will be followed 
elsewhere




THE L.C.C. CHORAL UNIONSLondon Concerts

THE BEETHOVEN FESTIVAL

Two concerts of this excellent series organized by Mr. 
Daniel Mayer were reported in our last issue. At the third 
concert, on the afternoon of April 22, the fourth and fifth 
Symphonies were given, Herr Max Pauer played the fourth 
Pianoforte concerto, and Mr. Paul Reimers gave the Song- 
cycle ‘ An die ferne Geliebte.” The next two Symphonies 
followed on the evening of April 23; Zimbalist played the 
Violin concerto and Herr van Rooy sang the Aria ‘ Mit 
Madeln sich vertragen.’ There is no need to extol the per- 
formances, which never descended below the highest artistic 
grade. The climax of the Festival was reached on the 
evening of April 25, when the Leeds Philharmonic Choir 
of 200 voices gave magnificent assistance in the Choral 
Symphony and the choral work ‘ A calm sea and prosperous 
voyage’ (conducted by Mr. H. A. Fricker). Miss Ada 
Forrest, Miss Tilly Koenen, Mr. Reimers, and Herr 
van Rooy as soloists helped to sustain the vitality and 
attraction of the performance. The third Pianoforte 
concerto, with Mr. Arthur Rubinstein as soloist, and the 
eighth Symphony completed the programme. Mr. Henri 
Verbrugghen, whose life and work are dealt with in our 
leading article, conducted throughout the Festival with 
insight and masterly ability

QUEEN’S HALL ORCHESTRA

The Symphony Concert given on April 25 was 
distinguished by the superb playing of Herr Kreisler in 
Brahms’s Violin concerto, a work which he is perhaps better 
fitted to interpret than any other living violinist. The 
Symphony was Schubert’s ‘ Unfinished,’ of which Sir Henry 
Wood conducted an individual performance, exquisite in its 
tone-grading. Beethoven’s ‘Coriolan’ Overture, and 
Bach’s third ‘ Brandenburg’ Concerto were also in the 
programme

A programme of unusual character was chosen for the 
Endowment Fund Concert given by this Orchestra under 
Sir Henry Wood on May 9. It included ‘ Dance in the sun’ 
by Arnold Bax, Percy Grainger’s ‘Shepherd’s hey,’ 
Debussy’s ‘L’Aprés-midi d’un  Faune,’ Stravinsky’s 
‘ Fireworks,’ and Schumann’s Pianoforte concerto, superbly 
played by Herr Dohnanyi. The pianist also appeared as 
composer and conductor in his excellent Suite in F sharp 
minor, the best of last season’s * Promenade’ novelties

Herr Kreisler gave a concert with the assistance of 
Sir Henry Wood and his Orchestra, at Queen’s Hall, on 
May 14. It was a keen pleasure to renew acquaintance 
with his beautiful interpretation of Elgar’s Violin concerto. 
Both in this and in the Concerto of Beethoven he gave 
unbounded delight

NEW SYMPHONY ORCHESTRA




LONDON SYMPHONY ORCHESTRA


\MATEUR ORCHESTRAS


CHAMBERMendelssohn’s ‘St. Paul’ was chosen for performance by 
the Handel Society at (Jueen’s Hall on May 12, and unds 
Dr. Henschel’s direction some attractive choral singing 
Miss Eleanore Osborne, Miss Phyllis Lett, Mi 
Gervase Elwes, and Mr. Thomas Farmer were the soloists 
was Mr. E. G. Croager

A large audience attended the concert of the Wilbeis 
Queen’s Hall on 
Beethoven’s Violin concerto, with Erna Schulz as solos. 
was the chief feature of the programme. ; 
Mr. Walter Hyde, who had not been heard in London for 
The Orchestra played throughout wit 
good tone and capability under Herr Sachse’s direction

May 13, wher




CONCERTS. 


401


VOCAL, RECITALS


PIANOFORTE RECITALS


OTHER


RECITALS AND CONCERTS


402


125


BY OUR OWN CORRESPONDENTS


BIRMINGHAMconcert of the season on May 5, when Elgar’s * The dr 
and ‘ Sea-pictures ’ (with Miss Alice Laki 
* Leonore’ Overture, No. 3, ¥ 
audience. The

of Gerontius’ 
soloist), and Beethoven's

iven before a crowded soloists bes




BOURNEMOUTH


GSI 4, 


BRISTOL. 


I, 1914


AND CORNWALL


DEVON


THE THREE


OTHER DEVONSHIRE TOWNS. 


MC. L. Miss Elena Gerhardt gave a recital of Lieder in the Torquay 
Pavilion on April 25 ; and at a ballad concert in another 
sat of the town on the same date in aid of Boy Scouts’ 
fands Miss Ethel Hook, Messrs. Hastings Wilson and 
. Hugh Peyton (vocalists), and Miss [Isabel Hirschfeld 
lpianoforte) were the performers. This party also appeared 
t Seaton on April 28 in aid of another charity

The programme played by Teignmouth Orchestral Society 
n April 23 included Beethoven’s eighth Symphony. Mr. 
\, ]. James conducted, and the vocalist was Miss Kate 
Rostock. On the occasion of the Benefit of Mr. Basil 
Hindenberg, conductor of the Torquay Municipal Orchestra, 
Sir Henry Wood visited the Pavilion on April 30 and 
snducted works by Tchaikovsky, Schubert, and Wagner. 
Mr. Josef Holbrooke played the solo part of his Poem for 
ianoforte and orchestra, ‘ Gwyn-ap-Nudd,’ and conducted 
isSuite ‘Dreamland.’ Mr. Harold Bonarius, leader of the 
mhestra, was the soloist in Max Bruch’s Concerto, and the 
wealists were Miss Carrie Tubb and Miss Ethel Toms. 
Madame Blanche Newcombe gave two recitals of German 
and English song at Exeter on May 1, when she was assisted 
y Mr. Charles Fry and Miss Maude Dixon

The Exeter Orchestral Society scored a big success at their 
concert on May 5, conducted by Dr. Wood. Mrs. Kenyon 
Miss Dorothy Wood) was the soloist in a Mendelssohn 
Pianoforte concerto, and the hand played some interesting 
pieces by Debussy, Hamish MacCunn, Elgar, and Glinka. 
fr. Sutton Jones was the vocalist. Plympton Wesleyan 
ichestra, conducted by Mr. W. M. Wickett. gave a concert 
on May 6, when a Trio was played by Mr. Wyatt (clarinet), 
Mr. Long (flute), and Miss Chubb (pianoforte) 
Mr. H. Watt-Smy1k conducted a concert of the Ilfracombe 
Orchestral Socicty, a band of thirty performers led by 
Miss A. Clark




CORNWALL


405district was the performance on May 7, by Tywardreath 
Choral Society of the ‘ Hiawatha’ trilogy. Mr. W. Brennand 
Smith conducted

On May 4 Penzance Orchestral Society, a capable body 
with artistic aims, played Schubert’s ‘ Unfinished’ 
Symphony and Beethoven’s ‘Egmont’ Overture. Mr. 
Walter Barnes conducted the forty players, who were led 
by Miss May Stewart

DUBLIN

The College Choral Society gave a performance of 
Cowen’s ‘St. John’s Eve’ under Dr. Marchant’s direction. 
The solos were all taken by members of the Society. In 
connection with the Feis Ceoil an interesting pianoforte and 
song recital was given by Mr. Frederick Dawson and 
Mr. Gordon Cleather on May 5. Mr. Cleather (who was for 
several years resident at Dublin) sang with great refinement 
and artistic feeling. Mr. C. W. Wilson was the accompanist

On Sunday, May 10, Dr. Esposito started a summer 
series of orchestral concerts at Woodbrook Concert Hall, at 
which all Beethoven’s Symphonies will be performed in 
order. Signor Simonetti played Mendelssohn’s Violin 
concerto extremely well, and the orchestra showed to great 
advantage in the accompaniment. On May 17, besides the 
second Symphony, the programme included Wagner’s 
|* Waldweben’ and the ‘ Meistersinger’ Overture. Miss 
Nita Edwards, who has just returned to Dublin from a tour 
round the world with the Quinlan Opera Company, was the 
solo vocalist, and sang with great success

The Feis Ceoil have just published a collection of ‘ Irish 
Airs’ (eighty-five in number), edited by Arthur Darley and 
P. J. McCall. They consist of a selection from the large 
number contributed from time to time at the Festivals held 
|s nce 1897, and are understood to be hitherto unpublished. 
They should prove of much interest




LIVERPOOLThe Walton Philharmonic Society, conducted by Mr. 
Albert Orton, deserves honourable mention for the performance 
of Cowen’s ‘ Sleeping Beauty,’ which closed the Society’s fifth 
season on April 22. The tuneful work affords grateful 
opportunities for an intelligent choir, and the singing was 
generally marked by precision and expression. The vocal 
soloists were Miss Margaret Hadfield, Miss Helen Strain, 
Mr. Lloyd Moore, and Mr. J.C. Brien. By a wise provision, 
six first-class string-players and a timpanist assisted Dr. 
Stanley Dale as pianist, so that the orchestral scope of the 
accompaniments was very usefully suggested. With the aid 
of this small orchestra an admirable performance of Mozart’s 
EK flat major Pianoforte concerto was also given, with 
Mr. Orton as solo player

Another estimable local organization, whose aims are 
instrumental rather than choral, is the Anfield Orchestral 
Society. Under the direction of Mr. William Faulkes the 
Society continues to prosper, and the programmes no less 
than the performances have reflected credit on all concerned. 
At the closing concert of the sixth season, on April 29, 
orchestral movements by Beethoven, Mendelssohn, and 
Weber were played ; also Beethoven's Pianoforte concerto in 
E. flat, Op. 73, in which the soloist was Mr. E. K. Harrison. 
The vocalist was Mrs. Howard Stephens

On April 18, a number of local singers and entertainers 
combined in a performance, given in Hope Ilall on April 18, 
in aid of the widow and daughter of the late Mr. John Henry, 
who was especially well-known in the Welsh community and 
had long been resident in this city as a singer and teacher of 
singing




ABERDEE 


ASPATR L 


BRANDON


BRIEFLY SUMMARIZED. 


EEEE


MUSIC IN PARIS


M. R 


1915. 7 


409L. Whittake

inkerron’ wf Meresting. : , 
April 21, & The Société des Concerts du Conservatoire has closed its 
\. under i <n with two performances of Bach’s ‘Johannes 
perform: My fasion.” 
totes te At her song recital given on May II, Madame Vallin 
[r. Lichtore My Hekking has introduced interesting numbers by 
“BE Grassiand Roland Manuel. 
M. Robert Schmitz has given on May 12, a most 
— ff itteresting pianoforte recital, devoted to modern French 
and Russian music. 
The last concert of the Société Musicale Indépendante 
roduced wi ( Omprised interesting numbers by contemporary Spanish | 
Mile. Yom ff composers, and Greek songs by Emile Riadis. 
M. Message \lbert Roussel’s ‘ Evocations,’ Paul le Fleur’s * Crépuscules | 
mmended. fAmor,’ a ‘Symphonic Suite’ by Darius Milhaud, and a 
d’s ‘Mami Mf ‘Madrigal lyrique’ by O. Klemperer were given at the 
tales of tem fiteenth concert of the Association des Concerts Schmitz. 
our. Itsi le _ 
aracter, k& sia enin-ornaia 
We ‘ . | 
ae Foreign Wotes. 
wo works 
nd the ball BERLIN. 
Under the direction of Madame Heyman-Engel 2 French 
a. Theis pera-buffe evening was given at the Hochschule fiir Musik. | 
and a sal @ Tbe duet from ‘ Der Kapellmeister’ (Paér) and Masse’s one- | 
very ape # at operetta ‘Les Noces de Jeanette’ were the principal | 
e de Josep! atures of the evening —The Karg-Elert evening recently | 
A majority gen at the Harmonium [all proved a great success. An 
rs havedal J Mleresting meeting of the Society for the Protection of Art | 
ic does 2 was held recently. The first part of the programme 
On the is HF “onsisted of an address by Herr Metzler on the disfiguration 
mposet, # af master-works of the 17th and 18th century violin music 
and warm y unstylish modern arrangements. A concert of chamber 
music from the 16th to the 18th century followed. 
ernment & | 
FRANKFORT-ON-MAIN. | 
consisted A highly interesting concert of French music was recently | 
-Korsiors Sen at the Palmgarten. Works by Aubert, Dubois, Bizet, 
of humons ff “Massenet, Saint-Saéns, Thomas, and Bourgeois were heard. | 
aes! HAMBURG. 
inal pats ; : | 
"Nerina ae third great Festival of the German Brahms Society | 
nducted Hy ‘be held at Hamburg (Brahms’s birthplace) on June 4-8, 
1915. The first two Brahms Festivals took place respectively 
‘cal Soci M1909 at Munich and 1912 at Wiesbaden. 
ted tows JENA. 
ts, incladm Th , = ; 
ailles. 0 y € recently discovered Variations by Beethoven for two | 
une 100m *S and English horn (or two violins and viola) will 
it the howt rete be published by the fortunate finder, Prof. Fritz 
h memmbss ara ata discovered also the ‘Jena’ Symphony. The 
sedy-Fast lations, which are on a theme from Mozart's ‘ Don Juan, 
1 (* Ancie “em to have been written about 1795, when Beethoven

composed the




LEIPSIC


MAGDEBURG. 


MEININGEN, 


NAPLES


PETERSBURG


ST


ROTTERDAM


VIENNA


410400), single

second Beethoven Festival has been arranged by

by Bach and Brahms in the programme




ROYAL ACADEMY OF MUSIC, 


A.J


CONTENTS


LUARD-SELBY


ELLINGF' 


ARN 


ELL


0Z. 


AUI


AR 


REL, 


H OL 


ONE 


OB 


__


THE MUSICAL


VALUABLE V 1OL IN~’ 


OR SALI 


IMPORTANT


STUDIES S IN ORGAN TONE | 


MUSIC PUBLISHING


WATERSIDE 


A VALUABLE BOOK FOR TEACHERS AND STUDENTS


TECHNIQUE


AND


E XPRE SSION


PIANOF ‘ORT E PLAYING 


FRANKLIN TAYLOR


EXTRACT FROM PREFACE. 


WITH NUMEROUS MUSICAL EXAMPLES FROM THE 


WORKS OF THE GREAT MASTERS. 


MUSICAL TIMES


ADVERTISEMENTS


THE


SCALE OF TERMS FOR 


NOTICE


ENGLISH LYRIC


SET TO MUSIC


FIRST 


SET


SECOND SE! 


THIRD SET. 


FOURTH SET. 


SIXTH SET


SEVENTH SET


EIGHTH SET. 


NINTH SET. 


NOVELLO


BY


NGS AND SIXPEN


CH


KA


SEI


2 OG 


RRY


THI


4 MUSICAL


AN THEMS


FOR


TRINITYTIDE


COMPLETE


LIST


THE “LUTE


SERIES


DOM


CON COO AA


MUSICAL SETTINGS OF THE


ROMAN LITURGY


EDITED BY


ANSITIS PER VIAM 


413


CANTIONES SACRA


SAMUEL GREGORY OULD


414


THE MUSICAL


1914


ADVENT 


CHRISTMAS 


LENT


EASTER 


HARVEST 


GENERAI


ADVENT 


CHRISTMAS 


LENT


EASTER 


HARVEST 


GENERAL


ADVENT 


CHRISTMAS 


LENT


EASTER 


HARVES1 


GENERA


HARVEST 


GENERA


CHRISTMAS 


LENT


HARVEST 


GENERAI


ADVENT 


CHRISTMAS 


NOVELLO’S ANTHEM BOOK


I 6


I 7


HARVEST 


GENERAL


ADVENT


CHRISTMAS


LENT 


EASTER 


HARVEST 


(GENERAL


ADVENT


CHRISTMAS


EASTER 


HARVEST 


GENERAL


ADVENT


CHRISTMAS


LENT


EASTER 


WHITSUN 


HARVEST 


GENERAL


300K 9


EASTER 


WHITSUN 


HARVEST 


GENERAL


EASTER


WHITSUN 


HARVEST 


GENERAL


ADVENT


CHRISTMAS


EASTER 


HARVEST 


GENERAL


ADVENT


LENT


EASTER


WHITSUN 


HARVEST 


GENERAL


THE MUSICAL


MUSIC FOR 


ANTHEMS


POPULAR CHURCH MUSIC


H. MAUNDER


. SERVICES. 


. CHURCH CANT ATA. 


NOVELLO'S


ANTH


PRECES


EMS. 


HYMNS AN


D TUNES


» TI : J. STAINER 


THE 


FERIAL RESPONSES 


WITH LITANY 


AND THE


AND


WITH LI


ACCORDIN


TALL


EDITED


JOSEPH


RESPONSES 


ANY


IS


BY


BARNBY


NOVELLO


PENCE


EASY ANTHE


GILL'S 


NOVELLOS


MAGNIFICAT AND|N EW SONGS


NUNC DIMITTIS


READY JUNE 3


PRICE TWO SHILLINGS EACH NE? ( 


SET TO MUSIC IN THE KEY OF E MINO


BASIL HARWOOD


S M


I ONDON TOWN. 


\ POEM | 


¥ | THE PATHWAY THRO’ THE POPPIES 


PTOBIAS MATTHAY | THE BOLD GENDARME. _ : 


OVE IN THE MEADOWS. : 


\NI 


STEWART


W AREIN


OVERLE


GRAHA


IES 


"HOMSON


0S


HOMS


NEWTON


AREIN


ETCHER


TCHER


SHAWE


THE MUSICAL


I, 1914. 417


MR. JOSEF 


HOLBROOKE'S 


CELTIC DRAMAS


DYLAN


DRURY AES THEATRE, 


THOMAS BEECHAM


“THE CHILDREN OF DON


OPERETTA


OTHER ORCHESTRAL WORKS. 


“APOLLO AND THE SEAMAN


DRAMATIC CHORAL SYMPHONY


NEW SONGS


EVE OF ST. AGNES 


CANTATA 


CHORUS, AND 


JOHN KEATS


THE


FOR SOLI, ORCHESTRA


THE 


JOHN FRANCIS BARNETT


DAILY TELEGRAPH. 


MORNING POST. 


SUNDAY TIMES. 


MONTHLY MUSICAL RECORD


THE GLOBE. 


THE FLYING DUTCHMAN 


AN OPERA


BY 


RICHARD WAGNER. 


SELECTION FROM ACT IL


RISING 


INTRODUCTION. 


SPINNING CHORUS


SENTA’S BALLAD, 


ETC


COMI


THE VOYAGE OF LOVE 


SONG-CYCLE. 


THE WORDS BY 


HAROLD SIMPSON. 


THE MUSIC BY 


CHORAL SONGS FOR S.AT.B


EDWARD ELGAR. 


THE SHOWER | THE FOUNTAIN


THE WORDS FROM A POEM BY THE WORDS FROM A POEM B\ 808. | 


HENRY VAUGHAN. HENRY VAUGHAN. 


DEATH ON THE HILLS 


THE WORDS ADAPTED FROM THE RUSSIAN OF MAIKO\ BY 


ROSA NEWMARCH. 


—— 838. ° 


LOVE'S TEMPESI SERENADE 


MAIKOV, BY MINSKY, BY 


ROSA NEWMARCH. ROSA NEWMARCH. 


NEW CHORAL WORKS 


BY 


GRANVILLE BANTOCK. 


THE WORLD IS TOO MUCH WITH US) MARCH OF THE CAMERON MEX § 46: 


(SONNET.) FOR CHORUS OF MIXED VOICES. 810, | 


WORDSWORTH


MARY M. CAMPBELL


O CAN YE SEW CUSHIONS? SPRING-ENCHANTMENT


LULLABY.) FIVE-PART SONG FOR MIXED VOICES 


FOR CHORUS OF MIXED VOICES. HELEN F. BANTOCK. 823,” 


CHORAL SUITE. 


FOR MALE, FEMALE, AND CHILDREN’S VOICES 


BALLADE FESTIVAL SONG 


THE WORDS BY


CHARLES NEWTON-ROBINSON. H. ORSMOND ANDERTON


SIAN


1974


419


E. JAQUES-DALCROZE'S


10


988


989. 


1010


IOI


VOCAL WORKS WITH ENGLISH TEXT


CELEBRATED CHILDREN- AND ACTION-SONGS


THE JAQUES-DALCROZE RHYTHMIC GYMNASTICS 


(EURHYTHMICS) 


NOW ISSUED AS A NET BOOK


ADDITIONAL HYMNS 


WITH TUNES CH


FOR USE WITH


HYMNS ANCIENT AND MODERN 


OR ANY OTHER CHURCH HYMNAL


LONDON : NOVELLO AND COMPANY, LIMITED


NOW ISSUED AS A NET BOOK


THE NEW CATHEDRAL PSALTER]° 


CONTAINING 


THE PSALMS OF DAVID 


TOGETHER WITH THE CANTICLES AND PROPER PSALMS. 


EDITED AND POINTED FOR CHANTING BY 


° 29 5 4 


JUST PUBLISHED


NOVELLO’S HANDBOOKS FOR MUSICIANS. 


EDITED BY ERNEST NEWMAN


CHORAL TECHNIQUE & INTERPRETATION


BY 


| HENRY COWARD. 


THE TEACHING AND ACCOMPANIMENT 


OF PLAINSONG


BY


FRANCIS BURGESS


JUST PUBLISHED


THE MUSIC OF THE BIBLE


BY 


JOHN STAINER. 


BY 


” THE REV. F. W. GALPIN, M.A., F.L.S


- JUST PUBLISHED. 


+ PLAYS OF WILLIAM SHAKESPEARE 


WRITTEN AND COMPILED BY 


5 6 MRS. G. T. KIMMINS. 


5 6 


CONTAINING FULL INSTRUCTIONS ON SINGING, 


WITH A DETAILED ANALYSIS OF SOME WELL


LISHED


FOR


COMPOSE


SCOTT


A GU IDE TO SOL OS SING ING! PIANOFORTE SOLO


D BY


BAKER


KNOWN WORKS AND SONGS. 


; BY 


GUSTAVE GARCIA, 


THREE-PART STUDIES


SCHOOLS AND LADIES’ CHOIRS


HUGH BLAIR, 


WITH PREFACE AND DIRECTIONS FOR PRACTICE


JAMES BATES


FOR THE VOICE


G. HENSCHEL


FOU R CHARAC ‘TERISTIC PIECES FOR VIO


C. H. LLOYD


ARRANGEMEN FOR SMALI


FOR


MPOSE


SCOTT


_____


ISHED


SE PIANOFORTE SOLO


BAKER


FRO


BY


ORCHEST


TWO INTERLUDES


FALSTAFF


RA


SED


ELLO AND


IN AND


F. HA


SONATA IN A


PIANOFORTE


NDEL


BY


AP 


NUMEI


THE MUSICAL


I, I914. 423


LOVERS


THE NEW AND REVISED EDITION OF


TO MUSIC


GROVE’S DICTIONARY


OF


MUSIC AND MU


SICIANS 


FIFTEENPENCE WEEKLY


PROSPECTUS (A DEPT


WRITE FOR 


ST. MARTIN’S STREET


CO


LONDON, W.C


APRACTICAL GUIDE


TO THE


THEORY OF


CONTAINING


NUMEROUS TEST-QUESTIONS WITH ANSWERS


JULIA A. O'NEILL


THE TIMES. 


THE MORNING POST. 


MUSICAL OPINION, 


MUSIC


THE LADY. 


HANDBOOK OF


CAMBRIDGE 


UNIVERSITY PRESS


ABDY WILLIAMS, M.A


CAMBRIDGE UNIVERSITY 


FETTER LANE, LONDON


PRESS 


EXAMINATIONS


IN MUSIC


600 QUESTIONS ‘WITH ANSWERS 


ERNEST A. DICKS


PREFACE TO THE NIN1T H EDITION. 


E. A. D


HARVEST FESTIVAL MUSIC


CANTATAS. 


SONG OF THANKSGIVING HARVEST CANTATA


AND CHORUS BARITONE) SOLI AND CHORUS


THE WORDS BY 


ROSE DAFFORNE BETJEMANN 


THE MUSIC BY 


SHAPCOTT WENSLEY JULIUS HARRISON. 


THE WORDS WRITTEN AND ARRANGED BY


FOR TENOR AND BASS SOLI AND CHORUS JOHN E. WEST. 


THOMAS ADAMS, FOR CHORUS AND ORCHESTRA


THOMAS ADAMS. FOR FEM — VOICES


FOR TENOR AND BASS SOLI, CHORUS, AND ORGAN OR THE J UBILEE CANTATA 


SMALL ORCHESTRA FOR SOLO VOICES, CHORUS, AND ORCHESTRA 


BY


HUGH BLAIR. C. M. VON WEBER


HARVEST CANTATA A HARVEST SONG 


FOR CHORUS, SEMI-CHORUS, AND ORGAN FOR SOPRANO SOLO AND CHORUS 


BY BY 


GEORGE GARRETT. C. LEE WILLIAMS. 


TWELVE HYMNS FOR HARVEST THE SOWER WENT FORTH SOWING 


.ET ALL OUR BRETHREN JOIN IN ONE » 08 


THE JOY OF HARVEST AND SEA


A HARVEST HYMN OF PRAISE COME, YE THANKFUL PEOPLE, COME


. THE ORGAN __| ORIGINAL COMPOSITIONS 


‘ ‘OSED BY 


6. REQUIEM ATERNAM : 1 ¢ 


CHORUS


THE ATHEN©UM


ORGAN


THE MUSIC STUDENT. 1A N 


FOR THE


COMPOSED BY


CHURCH FAMILY NEWSPAPER


T NEWSP: UR


NOVELLO’S


ALBUMS FOR THE


TWELVE SELECTED


TWELVE SELECTED


ORGAN


PIECES. 


PIECES


PIECES


J I 


PIECES. 


PIECES


8. V 


UST PUBLISHED


THE MORRIS BOOK


THE MORRIS MEN OF ENGLAND


CECIL I. SHARP


GEORGE BUTTERW( RTH


PART V


MORRIS DANCE TUNES


ARRANGED FOR PIANOFORTE SOLO 


BY 


CECIL J. SHARP


GEORGE BUTTERWORTH


SET IX


SET X. 


N CONTENTS


FRENCH 


MUSICAL DICTION


AN ORTHOLOGIC METHOD FOR ACQUIRING 4 


PERFECT PRONUNCIATION IN THE SPEAKING 


FRENCH LANGUAGE. 


FOR THE SPECIAL USE OF ENGLISH-SPEAKING PBOPLE 


BY 


C. THURWANGER 


OU


THE MUSICAL


TIMES


JUNE 1


IQI4. 427


EEE 


. LITERATURE, 


LE. 


LONDON


IMPORTANT TO CHORAL SOCIETIES. 


READY


NOIV 


CONCERT VERSION OF THE POPULAR COMIC OPERA


TOM JONES 


COMPOSED BY


EDWARD GERMAN


NEW YORK, TORONTO, AND MELBOURNE


NOVELLO’S MUSIC FOR MILITARY BAND


I OUSELEY ) 


LONDON: NOVELLO AND COMPANY, LIMITED


ALSBA


BESS RIS 


NOVELLO'S PART-SONG BOOK


A COLLECTION OF 


PART-SONGS, GLEES, AND MADRIGALS


ERA


NOVELLO’S PART-SONG BOOK. 


(SECOND SERIES


SWEET DAY, SO COOL


PART-SONG FOR S.A.T.B. 


THE WORDS WRITTEN BY GEORGE HERBERT


THE MUSIC COMPOSED BY


EDWARD GERMAN


TS ST = I 


A SESE ES


U.S.A


SWEET DAY, SO COOL


SWEET DAY, SO COOL


= ST == E — 


SWEET DAY, SO COOL


= ; 3 — ———————— | =F 


SO —— — 


7 SSS SS SS 


80 COOL


LSS SS


O— 29 


SWEET DAY, SO COOL


NOVELLO’S


4-3-4


30 


506


519


517


542


ATAAOLSIAAN, SOLION


AONNOGOD SINO SV


ATHINNAL AHL NVAI SV


NIAVVIVHS