


 1/9 


 hm 


 ROYAL CHORAL 


 ROVAL ALBERT HALL 


 KING . 


 FREDERICK BRIDGE , C.V.O 


 SATURDAY AFTERNOON , NOVEMBER 7 , 


 AT 3 } 


 MISS ADA FORREST . 


 MADAME ADA CROSSLEY 


 ROYAL ACADEMY of 


 YORK GATE , MARYLEBONE ROAD 


 MUSIC , 


 LONDON , N.W 


 the ROYAL COLLEGE of MUSIC , 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. 


 the GUILDHALL SCHOOL of MUSIC 


 LANDON RONALD PRINCIPAL 


 H. SAXE WYNDHAM 


 HE MUSICAL TIMES 


 AND SINGING - class circular . 


 found in 1844 . 


 publish on the first of every 


 NOVEMBER 1 


 society 


 MR . BEN DAVIES . 


 MR . ROBERT RADFORD . 


 CHORUS and ORCHESTRA , one thousand . 


 MONTH 


 1914 


 the 


 ASSOCIATED BOARD 


 of the R.A.M. and R.C.M. 


 for local examination in MUSIC 


 10 , 1915 


 the ROYAL COLLEGE of ORGANISTS 


 ROYAL 


 MANCHESTER COLLEGE of MUSIC 


 MIDLAND INSTITUTE . 


 OF MUSIC 


 BIRMINGHAM 


 SCHOOL 


 MANCHESTER SCHOOL of MUSIC . 


 SWORTH 


 ROYAL ACADEMY of MUSIC 


 ROYAL ACADEMY of MUSIC 


 DORIS B. WOOD 


 30ARD of EXAMINATION , 


 examination — local and HIGHER , 


 incorporated guild of CHUR 


 MUSICIANS 


 register of ORGAN vacancy . 


 LONDON COLLEGE of MUSIC . 


 CHL 


 CH § VOCAL 


 TC 


 UNIVERSITY of DURHAM 


 VICTORIA COLLEGE of MUSIC 


 _ — _ — _ 


 — | ESSR 


 MUSICAL 1 ) 


 JUSIC 


 the MUSICAL TIM 


 ES.—NOVEMBER 1 , 1914 . 639 


 UNIVERSITY of LONDON 


 CHURCH ORCHESTRAL SOCIETY . 


 LONDON , N. 


 MR . W. H. BREARE 


 TO student and the PROFESSION 


 URCH 


 TECHNIQUE 


 TECHNIOUE 


 TECHNIQUE 


 MACDONALD SMITH ’S system 


 OF PIANOFORTE TOUCH and TECHNIQUE 


 give permanent mastery over all difficulty of techniqvle , 


 M. MACDONALD SMITH 


 19 , BLOOMSBURY SQUARE , LONDON , W.C 


 640 the 


 MUSICAL time 


 NOVEMBER 1 , IQT4 


 PROFESSIONAL notice . 


 MISS GRACE CLARE 


 CONTRALTO ) , 


 MR . SAMUEL MASTERS 


 ( TENOR ) . 


 MR . MONTAGUE BORWELL 


 BARITONE ) . 


 MISS 


 WINIFRED MARWOOD 


 SOPRANO ) . 


 LANE , CRICKLEWOOD , N.W 


 24 , WALM 


 MR . GEORGE PARKER 


 ( BARITONE ) . 


 MR . 


 CHARLESWORTH GEORGE 


 ( BASS - BARITONE 


 the LONDON COLLEGE for chorister . 


 DR . A. EAGLEFIELD HULL 


 evision of MUSICAL composition 


 F.R.C.O. SPECIALIST in CORRESPONDENCE 


 late success : — 6 F.R. 


 A.R.C.M. ( PAPER work ) . + 


 HUNDRED and twenty - seven success ; ARCH SSIS 


 1897 - 1913 , three hundre and thirty - four . SSIS 


 TUDI 


 the 


 ECHSTEIN HALL STUDIOS , 32 , 34 , 36 , 38 


 RGA 


 the MUSICAL 


 ISS MARGARET * YOUNG , L. R.A.M. A. R. C. M. 


 WESTMINSTER ABBEY . 


 VEW CHURCH.—ALTO want . £ 15 . 


 T. AAAPH CATHEDRAL.—TENOR LAY - CLERK 


 SL JOHN ’S , PRINCESSTREET , EDINBURGH . 


 44 MARCHANT 


 I , 1914 . 641 


 ORGAN for sale . 


 RGAN P RACTICE . — 


 the OLD FIRM 


 P. CONACHER & CO . , LTD . , 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD 


 two gold medal 


 establish 1750 


 GRAY & DAVISON , 


 ORGAN BUILDERS , 


 PRATT STREET , N.W. 


 48 , SOUTHFIELD ROAD , OXFORD 


 128 , HOLT ROAD , LIVERPOOL . 


 PIANO pedal 


 EVERY ~ ORGANIS T 


 NORMAN & BEARD 'S 


 PNEUMATIC PEDAL attachment 


 for the PIANO , 


 the LONDON MUSIC 


 FROM 


 the 


 HOUSE of 


 BOSWORTH 


 CA 


 for 


 th 


 the MU 


 1 


 for 


 the MUSICAL time 


 NOVEMBER 1 , IQT4 . 643 


 BETHLEHEM 


 for four solo voice and choru 


 the word write and arrange by 


 E. CUTHBERT NUNN 


 the music by 


 J. H. MAUNDER 


 the 


 the word write by 


 SHAPCOTT WENSLEY 


 the music for SOPRANO , TENOR and BASS SOLI , 


 CHORUS and ORGAN 


 compose by 


 JOHN E. WEST 


 CHRISTMAS EVE 


 FOR CONTRALTO SOLO , CHORUS and 


 ORCHESTRA 


 compose by 


 NIELS W. GADE 


 CHRISTMAS SCENES 


 for FEMALE voice 


 compose by 


 FREDERIC H. COWEN 


 CHRISTMAS ORATORIO 


 J. S. BACH 


 CANTATAS for CHRISTMAS 


 the nativity 


 for SOPRANO , TENOR , and BASS SOLI , and 


 CHORUS 


 the word and hymn select and the music 


 compose by 


 THOMAS ADAMS 


 story of BETHLEHEM CHRISTMAS CANTATA 


 FOR SOLI and CHORUS 


 the word by 


 DAFFORNE BETJEMANN 


 the music by 


 JULIUS HARRISON 


 ROSE 


 the HOLY child 


 for SOPRANO , TENOR , and BASS SOLI , 


 CHORUS and ORGAN 


 compose by 


 THOMAS ADAMS . 


 YULE - TIDE 


 THOMAS ANDERTON 


 before the PALING 


 of the STARS 


 A CHRISTMAS HYMN 


 by 


 CHRISTINA ROSSETTI . 


 BY 


 B. J. DALE 


 the MUSICAL 


 644 


 TRINITY C 


 SIC 


 G. E. BAMBRIDGE , F.T.C.L. , F.R.A.M. 


 SUITABLI 


 FOR present and CHRISTMAS gift . the C ae PSALTER 

 for Chanting , Words y , together with the 
 miniature MUSIC EDITION of HYMNS ANCIENT and 
 MODERN 
 ( 1889 Eprrion ) . 
 Imperia ( s y 4 e ) , India Paper NET 
 R M s. od . 
 Turkey M ros . ocd 
 k leathe oe . t2s . 6d 
 the CATHEDRAL PRAYER BOOK 
 with Cantick Psalter point for Chanti together with the 
 MINIATURE MUSIC EDITION of HYMNS ANCIENT and 
 modern 
 ( 1889 Eprrion ) 
 Imperi mo ( y i ) India Pape ET 
 Rk and M € ros . ¢ 
 I KEY Mos 12 . 6d 
 t a leathe 1 " 
 I Novi A ( ANY , Limited , 
 NOVELLO ' ’S edition . 
 rHE PIANOFORTE WORKS of 
 BACH , BEETHOVEN . 
 HAYDN . MENDELSSOHN . 
 MOZART . SCHUBERT . 
 SCHUMANN , 
 M ! N Edi t ¢ tt ul pri 

 TIMES 




 NOVEMBER I , IQT4 


 BROADWOOD 


 PLAYER - PIAN(S 


 grand and upright 


 ILLUSTRATED catalogue on application 


 CONDUIT STREET , 


 30SWORTH 


 IRITIC 


 british 


 BRITISH 


 ENGRAVING 


 CO 


 proprietorship 


 british british 


 PAPER . PRINTING 


 BOSWORTH ’S ALBUM of NATIONAL ™ 


 RACH MANINOFF 


 WICKINS 


 WARD 'S 12 progressive 


 LLUM ’S scale and ARPEGGIOS 


 RINCESS ALBUMS 


 BOSWORTH 


 8 , HEDDON STREET , REGENT 


 T. B. HARMS CO 


 NAVAL 


 CO . , 


 NEW YORK 


 AND MILITARY 


 MUSICAL UNION song BOO 


 THEOU 


 1 . a 


 AND singing - class circular . 


 _ _ 


 MARKET 


 DON , W 


 646 


 on form and composition : 


 YOUNG MUSICIAN 


 NEWMAN 


 the 


 ion : obvious duty to tell I . if he will not , I assume 

 ter to nest be can not . now when a teacher say to I , 
 ry fond gi be be a rule that must never be break except 
 to belier jet certain circumstance that justify the 
 u lack tigjpeeaking of it , ' I naturally expect he to tell 
 and woe how to recognise these circumstance when 
 > a ligupearise . it be no use his tell I under what 
 1 be no ymstance Bach or Beethoven or Wagner break 
 mpositigggt ule . if I be only to break it under the 
 as to tyme circumstance , I be merely copy the 
 r that yogpaat composer without understand why . what 
 pond wig rant to know be when and how I may break the 
 se myself . if my teacher do not tell I that , 
 isnot train I properly . but of course , as 
 T quickhfpe wil admit , he can not tell I that . he be simply 
 too meagea vicious circle : the rule be the right way of 
 s. thesimg thing , but the opposite way — the wrong 
 omplishegay — be the right way if it sound right . I submit 
 compose this be not play the game : in business it 

 wo rate 




 the 


 I , 1914 . 649 


 650 the 


 musical time 


 cinepianism 


 through life ’s window . " drama 


 take by STORM . ' COMEDY - drama 


 illustration of the history 


 MR . ALFRED H. LITTLETON , 


 651 


 112 , 1897 


 2 the 


 MUSICAL notation . 


 practical way of express 


 detail of musical composition . 


 ELLIOT BUTTON 


 SECTION v. accidental 


 053 


 3 3 ; : 3 


 the 


 compleat organist 


 CONGREGATIONAI 


 singing 


 654 the 


 I , 1914 


 I , 1914 . 655 


 656 


 4 120 


 ORGAN recital 


 appointment . 


 receive 


 book 


 GERMAN 


 ' THEENGLISH > . 
 the 

 fingering . 
 MUSICAL time , ' 
 Sir,—It be easy to imagine the jubilance with whig 
 | Mr. H. C. Tonking ’s suggestion to re - adopt english fingering 
 in pianoforte music will be receive by patriot all over th . 
 country . for nearly thirty year our treacherous teacher 
 and editor of music have be do their wicked utmost t 
 establish a uniformity in fingering , quite regardless of the 
 fact that the sequence of figure i 2 3 4 5 represent , 
 hideously teutonic aspect of affair ! 
 | Mr. Tonking tell we that publisher would be glad to prin 
 english fingering once more , but that be nothing to th 
 | radiant gladness that would illuminate the countenance 
 small music - dealer all over the country at the prospect oj 
 sell off their old stock of * Popular Classics ’ and early 
 victorian copy of Beethoven , which have lie on their dusty 
 shelf so long ! ; 
 may I venture to call attention to another terrible evil ip 
 |our midst ? it be horrible to think that some dastard ) 
 | teacher of harmony be still allow their pupil to writ 
 | the chord of the german sixth , and may I suggest that som 

 to EDITOR of 




 THOMAS DUNHILL 


 to the editor of ' the MUSICAL TIMES 


 ARTHUR T 


 FROGGATI . 


 MESSRS . BOSWORTH & CO . 


 the appointment of FOREIGN PROFESSO ® 


 AT ABERYSTWYTH 


 the editor of ' the MUSICAI 


 ROBERT 


 WILLEM 


 1G. 


 ES . ' 


 NHILL 


 1914 


 ORTH . 


 > SSOR 


 I , 1914 . 657 


 DAVID JENKINS 


 the position of british MUSICIANS . 


 meeting at QUEEN ’S HALL 


 TIME 


 the MUSICAL 


 COMING SEASON 


 SUPPLEMENTARY 


 IN 


 list 


 the LONDONOrchestral Society and Crystal Palace Chots 

 W. Hedgcock).—Merrie England ; the | 
 Olaf Trygvason ( Grieg ) ; Beethoven ’s C minor 

 and a number of british orchestral work . 
 Phith fy ( Mr. Martin Klickmann ) . — 
 Faust ( Gounod ) ; Paradise and the Peri ( Schumann ) ; | 
 the banner of St. George ; Judas Maccabveus ; the 
 Martyr of Antioch ; Schumann ’s ' Rhenish ' Symphony . 
 Weste Railway Musical Si ( Mr. H. A. | 
 Hughes).—War and Peace ( Parry ) . 
 Si ( Mr. Claud powell).—madrigal | 
 and part - song . | 
 y ( Mr. Claud Powell 




 MISS SCHLESINGER ’s 


 lecture . 


 ROYAL ACADEMY of MUSIC . 


 S.—NOVEMBER I 


 TRINITY COLLEGE of MUSIC 


 QUEEN ’S HALL 


 the 


 CF , 6 — = = 


 2 — - > — _ — 


 SS 


 9 - 6 ! 


 ALTO 


 TENOR 


 SILENT NIGHT 


 SSS 


 — _ - _ - 


 9 - 2 - 7 — — _ — — — — 


 SILENT NIGHT 


 SILENT NIGHT 


 : = — _ _ — _ = 


 — .—F = a — 


 — TS — — — 


 — — _ — £ = — _ — — _ # — _ - # z — 


 7 — * = — 2 


 SILENT night . London Concerts . 
 QUEEN ’S HALL SYMPHONY CONCERTS 

 a ' safe ' programme be choose for the first of the 
 season ’s Symphony Concerts , give under Sir Henry Wood , 
 on October 17 . the Symphony be Beethoven ’s Fifth , and 
 the Concerto that of Tchaikovsky in b flat minor , play , 
 to the great satisfaction of a large audience , by the youthful 
 Solomon . Brahms ’s ' tragic ' Overture and Sir Henry 
 Wood ’s arrangement of a suite in G for string from the 
 organ Sonatas of Bach complete the programme 

 the CLASSICAL CONCERT SOCIETY 




 SOUTH place SUNDAY popular CONCERTS 


 by our own correspondent 


 BIRMINGHAMTHE MUSICAL TIMES.—Novemper 1 , 1914 . 667 

 ee MT 
 rt be on November 23 . the artist who appear be 
 oo an yjss Florence Macbeth , the american co / orafusa soprano , 
 nt chambe , dame Ada Crossley , and Mr. Robert Radford ( vocalist ) , 
 mes be , xb yis Isolde Menges , the talented young violinist , a pupil of 
 ‘ St rank anf ponold Auer , of Petrograd , and Monsieur de Greef , 
 } take plac je accomplished belgian pianist , Mr. R. J. Forbes act 
 mall charg 3s accompanist . the programme submit naturally 
 > Orchesin , ilude patriotic song and popular item which be 
 for Practice B eatly appreciate by the audience . 
 ace Sunda * fy place of the Quinlan opera season the Theatre Royal 
 mmagement arrange for a fortnight ’s popular patriotic 
 Promenade Concerts , from October 12 to October 24 , at 
 er 35 wher pice to suit the masse . the orchestra be that of the 
 > at Queer ’ B Theatre Royal , augment to forty performer , ably 
 eit Lowe onducte by Mr. Harry Rushworth . the programme 
 vere principally compose of light and popular as well as 
 uiriotic item and operatic selection from the work of 
 ng Concer f Sullivan and Edward German , the classical school as well as 
 October ; ff the romantic be entirely exclude . a vocalist or an 
 e Grenadie f instrumentalist appear each evening , all the artist hail 
 f interest : fF fom the Midlands . the experiment prove quite successful , 
 and at least provide some employment for local orchestral 
 payer , who have be so hard hit this season . the 
 Abert Hi Bimingham Festival Choral Society have not yet make 
 1y Orchesn ff i : know whether they will give their project series 
 jon of Mr of Choral Concerts , nor have the various local amateur 
 1d Madincff oral body notify the public of their resumption of the 
 ‘ ernoon , ts cstomary season ’s concert . 
 at ( ) ueen 
 lirection — ' 
 iven to 4 BOURNEMOUTH . 
 lief Fuiff a highly attractive prospectus of the autumn _ season 
 een ’s Hi } amangement have be issue by the Winter Gardens 
 Committee , and little difficulty seem to have be 
 encounter in plan a comprehensive and very attractive 
 Red Cus griesof event . first and foremost , the Symphony Concerts 
 7 by Prt and Monday ' Pops ' be bind to rejoice the heart of those 
 ‘ stra a5 G9 who support the good type of music . no stupid boycott of 
 me Kiki the music write by the great german master of former 
 and othesi ® daysha be institute , although it be understand that no 
 vere prexem live Teuton or Austrian will find representation . 
 the inaugural Symphony Concert be give on October 8 , 
 the programme include Mackenzie ’s lively ' Britannia ' 
 it succes Overture , Parry ’s ' english ' Symphony , a work of solid 
 - Kennett ? worth , and Delius ’s charming two piece for small 
 apart I orchestra ( first performance at these concert ) . Mr. 
 nselves , HR York Bowen ’s performance of Beethoven ’s Pianoforte 
 Cowen , § concerto in e flat be a model of soundness and steadiness , 
 S Stan and in the orchestral item Mr. Godfrey and _ his 
 een 's lif instrumentalist repeat their success of former year . 
 e a bi the first Monday ' pop , ' on October 12 , be devote to 
 e . ith british music by live composer , the following be the 
 & pall decidedly interesting selection make : two military marche , 
 ) HH . MT ' Pomp and circumstance ' ( Elgar ) ; overture to a comedy 
 be SR ( Balfour Gardiner ) ; ' Mock Morris ' and ' Shepherd 's Hey ' 
 ninary © ( Percy Grainger ) ; ' in fairyland ' suite ( Cowen ) ; overture , 
 vefit of SH ' the land of the mountain and the flood ' ( MacCunn ) ; irish 
 rhapsody ( no . 1 ) by Stanford , 
 Pavlova fill the bill — and also , be it say , the Winter 
 Gardens — on October 5 and 6 , her wonderful power of 
 dancing be exemplify in an extremely varied series of 
 § , dance . a recital by Miss Marie Hall , at which the 
 concert - giver be hear to the usual advantage , have be 
 theonly other event of importance up to date . 
 Mr. Philip Cathie , examiner and professor of the violin at 
 the Royal Academy of Music , and Mr. S. H. Braithwaite , 
 professor of theory at that institution , have recently be 
 ‘ point to the staff of the Bournemouth School of Music 

 be 104 Seer 
 ) accoul 
 the pe : BRISTOL . 
 , ol be . . . 
 a d in consequence of the war , several musical Societies of the 
 ci 




 DEVON and CORNWALL 


 YORKSHIREOf music in Yorkshire there be still little to chronicle . the 
 chief event have be the first of the Bradford Subscription 
 Concerts , on October 9 , when Mr. Thomas Beecham 
 conduct the Hallé Orchestra , and make a _ distinct 
 impression by his fiery and brilliant reading of such thing 
 as the ' Thamar ' of Balakirev , and Berlioz ’s ' Carneval 
 Romain ' Overture . he also give a fine performance of | 
 César Franck ’s Symphony , which at last seem to be 
 enter upon a period of comparative popularity . very 
 enjoyable , too , be a Handel Concerto in E minor , a 
 remarkably fresh and vigorous work . Mr. John Coates be 
 the vocalist , and we have two example of the art of another 
 native of Bradford in Mr. Delius ’s recent piece for small 
 orchestra , ' the first cuckoo ' and ' Summer night on the 
 river 

 on October 3 the Leeds Choral Union give a concert on 
 behalf of the War Relief Fund which , if not of great artistic 
 significance , serve its purpose as a patriotic effort . extract 
 from Handel 's martial oratorio , ' Judas Maccabeus ' and 
 ' Israel in Egypt , ' with various National Anthems , be 
 sing with great verve under Dr. Coward ’s direction , and 
 Mr. H. Brearley and Mr. William Hayle , both of the Leeds 
 Parish Church Choir , be excellent vocalist . the Leeds 
 Saturday Orchestral Concerts open a season of six concert 
 on October 17 , when Mr. Fricker and the Leeds Symphony 
 Orchestra offer an attractive programme of well - try 
 orchestral music , include Beethoven ’s Violin concerto , of 
 the solo part in which Mr. Rawdon Briggs give an artistic | 
 interpretation . there be a very large audience , whose 

 here i oo . ag = 
 enthusiasm indicate that there be a plac for such concert , | £ Deskuen . on card . 24 




 scale of term for advertisement 


 dure the last month 


 ANN 


 \ ars ] 


 age 


 ARSO 


 RIOR 


 OOTE 


 CHOO 


 CHOO 


 hymn for use in time of war 


 o LORD - HOSTS , who DIDST upraise 


 3.0 GOD of LOVE 


 4 . LORD , hear thy PEOPLE 's praye r 


 5 . from home of quiet peace 


 6 . LORD of life and light and glory 


 7.o LORD of host , without whose will 


 8.0 LORD our BANNER , GOD of might 


 9 . o great redeemer 


 10 . monarch of the heavenly host 


 HYMN after victory 


 12 , hymn after victory 


 price one penny each . 


 to thee our GOD we fly 


 GOD the all - terrible 


 GOD of our FATHERLAND ' 


 o father , KING of EARTH and SEA 


 . FATHER , FORGIVE 


 FLAG of our country 


 GOD bless our native land 


 the LORD of HOSTS OU R KING shall be 


 ML 


 LONDON : NOVELLO & COMPANY , LIMITED 


 _ _ 


 anthem . 


 music for use 


 part - song , ETC 


 073 


 IN TIME of WAR 


 the MUSICAL TIMES 


 NOVEMBER 1 , IQT4 


 accompaniment 


 : . — with orchestral accompaniment by } 


 JULIUS HARRISON 


 J. # . MAUNDER anthem and carol 


 CHURCH CANTATA . ' CHRISTMAS anthem 


 NOFORT 


 ) LS 


 the MUSICAL 


 the MILITARY 


 CONSISTING 


 THOMAS CONWAY BROWN 


 over 40,000 sell . 


 SIX war hymn 


 S| school choir training 


 at parade service in CAMP and CHURCH 


 a practical course of lesson on 


 voice - production , 


 for the guidance of teacher of class - singing 


 BY 


 MARGARET NICHOLLS . 


 WEBSTER ’S 


 ‘ groundwork of music 


 WEBSTER ’S 


 ONE SHILLING 


 NEW ORGAN music PL aye by 


 MR . HERBERT F. ELLINGFORD , 


 NEW composition by 


 ALSO 


 iwtroduction , variation , and fugue 


 concert song . 


 RAINBOW 


 two FANCIFUL piece 


 FRANCIS EDWARD GLADSTONE 


 our music reading ladder for beginner 


 twenty - second thousand 


 T.B. 


 the word by 


 HENRY NEWBOLT , 


 the music by 


 WILLIAM S. HANNAM 


 FOR S.A.T.B 


 RELUDE IX C SHARP minor 


 compose by 


 arrange for full orchestra 


 HENRY J. WOOD 


 the 


 the fine english make 


 CHAPPELL piano 


 cannot be excel 


 for their exquisite tone , responsive touch , 


 remarkable singing quality , and last durability 


 SAPELLNIKOFF , KATHARINE GOODSON , MARK HAMBOURG , IRENE SCHARRER , 


 ARTHUR DE GREEF , YORK BOWEN , WLADIMIR CERNICOFF 


 and they be be exclusively use on the CLARA BUTT — KENNERLEY RUMFORD TOUR 


 CHAPPELL PLAYER - piano , grand & upright 


 contain the 


 MOST modern improvement and expression device 


 50 , NEW BOND STREET , LONDON , W 


 SOLE agent : — 


 CHRISTMAS DAY PATRIOTIC song 


 VESPER HYMN . for use in time of war 


 0.927 


 LITY 


 JUR 


 GHAM 


 L ANI 


 SINTOSH 


 » . , LTD . 


 * RS . , LT 


 GS 


 GOD the all - terrible 


 HYMN 


 word by 


 H. F. CHORLEY 


 MUSIC by 


 A. LWOFF 


 GOD the all - terrible 


 SECOND 


 extra supplement . 


 NOVELLO ’ S CHRISTMAS CAROLS 


 the PRINCE of PEACE 


 TWELVE 


 TO SIR WALTER PARRATT 


 SS 


 the PRINCE of PEACE 


 the PRINCE of PEACE 


 ¢ 23 


 ( second serie 


 FOR EMPIRE and for KING 


 PATRIOTIC CHORUS 


 PERCY E. FLETCHER 


 — — _ — _ _ _ — 


 for EMPIRE and for KING 


 TP 


 — 4¢ _ — 4 1 


 PUN 


 extra supplement . 


 74 n_| . . 


 — — — _ — = = F 


 BY 


 — — — be = = ~ — = — E 


 wh 


 for EMPIRE and for KING 


 J ! _ 


 extra supplement . 


 for EMPIRE and for KING 


 _ $ < — _ FA > , > 


 ) a 7 


 3 a 


 for EMPIRE and for KING 


 tenor 


 supplement 


 for EMPIRE and for KING 


 2 — _ 


 for EMPIRE and for KING 


 = _ — _ 


 for EMPIRE and for KING 


 contralto 


 SS SS SS SSS 


 SS SS 


 for EMPIRE and for KING 


 for EMP IRE and for KING 


 = = — — — — — — SES SS : 


 for EMPIRE and for KING 


 0 — — 7o EE — $ < ] — $ — $ 


 = — = S=|=_—LSS= s = elss= SSS   sss== 


 _ — _ — — _ 


 ee 


 = = SS = SS 


 for EMPIRE and for eing 


 SS- _ _ OSE tss ESS = SS SE [ 


 FUR EMPIRE 


 and for KING 


 nn ) n 


 _ + — — _ _ 


 2=5 — ha es 


 4 — SE 


 SEES SS 


 for EMPIRE and for KING 


 for EMPIRE and for KING 


 NX 


 CA ' 


 LL _ 


 " 2 RR ONS 7 = EECCA " 


 A ~ — — _ _ _ — — — _ 


 for EMPIRE and for KING 


 — s — s== 


 F SS SS 


 NT ST 


 for EMPIRE and for KING 


 LL . > — 


 for EMPIRE and for KING 


 for EMPIRE and for KING 


 6 — — _ — — — S 


 NOVELLOS CHRISTMAS CAROLS 


 CHRISTMAS CAROLS 


 new and old 


 carol for CHRISTMASTIDE 


 268 


 49 


 270 


 27 


 272 


 OVELLO ’S CHRISTMAS 


 chorus 


 mix voice 


 with word suitable for use at 


 the PRESENT time 


 BELGIAN and FRENCH 


 WITH FRENCH WORDS 


 40 FRENCH NATIONAL and POPULAR 


 33 VIEILLES CHANSONS ET RONDES . 


 CHANT NATIONAL MONTENEGRIN 


 NOVELLO and COMPANY 


 NET , 


 20 


 SATU 


 468 . 


 CHI 


 14 0 


 14 0 


 FRID 


 MES 


 42 


 BA } 


 RC 


 YORK G 


 I 6 


 1 6 


 2 0 


 i 0 


 20 


 20 


 1 6 


 MANC ] 


 20