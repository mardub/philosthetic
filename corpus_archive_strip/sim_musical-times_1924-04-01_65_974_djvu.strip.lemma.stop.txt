


 MUSICAL 


 395 


 singing - class CIRCULAR 


 APRIL 1924 


 JOHN FREDERICK BRIDGE 


 


 306 


 1924 


 personal tribute 


 _ — _ — — _ _ _ _ _ 


 1924 307 


 MUSIC EMPIRE exhibition 


 


 MUSICAL 


 308 


 ' BEL CANTO ' 


 MUSICAL 


 309 


 


 311 


 critic critic 


 312 MUSICAL 


 1924 


 314 MUSICAL 


 1924 


 brow , high LOW 


 t 


 MUSICAL 


 1924of yes , haze fever , 

 evidence _ performing right case _ 
 incidentally dance band hear 
 play song : day . ) 
 popularity enjoy 
 Largo score Handel - know 
 tune , Bach Avza thing , 
 movement extract Beethoven , Chopin , 
 Schubert , Wagner , Elgar , & c. ? Darewskis , 
 Ivor Novellos , Irving Berlins know 
 difference real popularity mere 
 pass craze — craze usually engineer vast 
 cost effort advertising 

 apropos question advertising expense , 
 remember read Canadian Music Trades 
 Review year ago revelation 
 spend popularise song dance 
 music yes , type . sorry 
 note figure . 
 word magnitude 
 understand publisher 
 ' winner ' shutter . 
 article question point 
 success yes , scale , 
 failure , bag 
 good hard money spend vainly push . 
 showing - compounder 
 - ' popular ' song dance ! 
 need push tune Handel 
 rest ' highbrow ' classic 




 MUSICAL 


 316 


 1924 


 1924 


 RECOVERY LOST voice 


 1600 . 


 1924 319 


 MUSICAL 


 320 


 1924 


 FREE counterpoint 


 


 


 1924 


 RHEINBERGER ORGAN SONATAS 


 . 18 , , op . 188 


 MUSICAL ti 


 1924 


 323phrase second subject splendid 
 appearance pedal Posaune , carry 
 soaringly manual . movement 
 admirable voluntary purpose , dignified 
 reasonable length — seven page . 
 registration stand , start 
 ff section page 6 plenty power reserve , 
 able work upa climax 
 page ; pedal reed powerful 
 come beat bar 5 
 page . ( natural miss g 
 right - hand bar 9 page 

 Cafriccio recall Scherzoso e minor 
 Sonata , belong Beethoven type Scherzo| 
 fun fancy energetic . 
 mark Agvfafo , unusual direction 
 Capriccio . movement strongly rhythmical , 
 , like Scherzoso , quiet middle section . 
 likeness working - 
 climax Coda . point 
 Rheinberger infrequent use double pedal — 
 simple effective 

 Phantasie sonorous close , opening 




 $ 3 3 | — 2 = j — — 


 MINOR 


 326 MUSICAL 


 1924 


 26 


 


 ss = 


 _ 


 FORGOTTEN PROPHET 


 


 modern composer modern 


 composition * 


 IODERN 


 MUSICAL ti 


 — APRIL 1 1924 329 


 MUSICAL 


 1924 


 


 331religious music — , , Zhe Dream 

 Gerontius 1 believe accomplish great 
 religious musical work write . asomewhat 
 sweep assertion , doubt , regard 
 purely music , music Bach , Handel , 
 Beethoven — deal sacred subject — 
 music pure abstract strength breadth 
 equally applicable great human 
 emotion — pagan . mean , music 
 , work Bach great Mass 
 reflect embody sorrow joy 
 belief humanity , merely 

 intend speak length regard 
 peculiar attribute composer choose , 
 try find quality exist 
 gain settle place 
 estimation , music possess 
 quality strength originality . 
 think fair deduction 
 consideration history music , classical 
 modern , claim blind 
 spiritual emotional quality , 
 intellectual feature , great music 
 permanence . ' letter killeth , spirit giveth 
 life ' know cleverness 
 essential greatness music , 
 art matter . refer composer 
 clever , mind , deadly insult , 
 second skilful acrobat 

 consideration use 
 beautiful , reply criticism 
 object music truthful 
 happen . ugly , hard , cynical , 
 material , help — 
 realistic . ultimate destiny 
 music wonderful , beautiful thing 
 soul dimly perceive , mere 
 photographic record change , restless , material 
 life . time come , think music 
 know extinct art , 
 noble function — try 
 clothe sound aspiration 
 deep vague word 

 great argument 
 beginning new era 
 music . argument bring 
 conservative stand art , 
 people interest 
 inner history music , realise 
 Bachs , Beethovens , Mozarts , 
 Wagners , live time host 
 pinchbeck impostor hour success — 
 , , outshine time illus- 
 trious contemporary — 
 forget 

 think interested music meet 
 talk frankly subject 
 discuss , bring remark toa 
 close , opinion , 
 opportunity discuss subject , 
 likely clear 
 quality constitute great music , , 
 having define , afraid 
 view forward . zeal eloquence 
 long special prerogative 
 worship new , introduce 
 false god . let learn use 
 weapon defence cherish belief 
 — exciting new creed 
 day 




 


 1924 


 SCHONBERG STYLE tendency 


 old spanish music 


 composer CHILI 


 J. B. COSTANZI 


 LESSON waste 


 cie 


 < 4 . ° 9 ° f 


 ( 1704 - 78 


 


 1924 333 


 GERMAN MUSICIANS distress 


 curious 


 NEWS 


 RIMSKY - KORSAKOV dancer 


 MOSLEM 


 ballet 


 RUSSIA MUSICAL activity 


 unknown work DEBUSSY 


 british music describe abroad 


 contemporary german composer 


 ALBAN BERG ' WOZZECK 


 new russian periodical 


 musical joy EGYP1 


 STRAVINSKY 


 swedish 


 PERIODICAL 


 HALFDAN KJERULF 


 M.-D. CALVOCORESSI 


 N.O.C. 


 MUSICAL 


 1924 335VIOLIN MUSIC 

 feel scarcity modern 
 violin concerto fact 
 violin , essentially melodic instrument , 
 attraction modern composer , 
 complex harmony rich colouring supreme 
 importance . , , great 
 violin concerto remain Beethoven , 
 Brahms , Mendelssohn , 
 Beethoven stand test 
 time , Brahms comparatively recent , 
 Mendelssohn appear popular 
 . justification 

 composer , write solo violin , 
 Beethoven model , deny 
 resource modernity , sake 
 fine balance solo instrument 
 orchestra , order place evidence 
 peculiar genius instrument melody . , 
 P. Stoeving Concerto E violin 
 pianoforte orchestra ( Bosworth ) , 
 obviously inspire excellent intention , fail 
 convince . thing , Mr. Stoeving 
 consistent . write , instance 

 passage describe 
 simple primitive , pass 
 arcady hot - house true Vieuxtemps style . 
 unlikely know 
 Vieuxtemps . , fatuous result certain 
 feat technique Vieuxtemps 
 speciality escape . , decidedly 
 simplicity . B. V. 
 STRING MUSIC 




 CHURCH MUSIC 


 PN 


 MI ] 


 BEE 


 SS 


 TT — } = - — 


 _ — _ 


 DAY THOU 


 MUSICAL T 


 SONGS 


 h. G 


 1924 343 


 344 MUSICAL 


 XUM 


 MUSICAL 


 1924 34 


 _ — _ — — . 


 621of writer - issue newspaper article . 
 rule collection pitchfork ; 
 group set 
 common title . respect , , 
 succumb usual lazy convention : 
 little revision . note tell essay 
 undergo ' slight alteration . ' 
 ' slight ' ? surely material turn weekly 
 gain enormously book - form good 
 deal drastically prune recast — 
 develop case . limitation 
 weekly article lead mere starting 
 hare ; - issue book - form author time 
 space hare purpose . case 
 point article head ' shall reconsider 
 Wolf ? ' Mr. Scholes ask question begin 
 |to case suspicion — share 
 lot — Wolf - rate . 
 page , , course , weekly space 
 . line short end , 
 ' case reconsideration 
 Wolf ? ' ( evidently assume chorus 
 ' yes ' arise recumbent Sunday afternoon form ) 
 add , ' let reconsider . ' 
 — event , print , want 
 critic considering , 
 reconsidering . carry 
 week , , , settle 
 job book . hungry sheep 
 look sniff . 
 ' shall eat ? ' ask Mr. Scholes . . . . ' let 
 eat , ' whisk joint away 

 think , , certain controversial chapter 
 omit recast . dispute 
 carry column journal week 
 week , month month , reader 
 hear start finish , 
 disputant reprint tussle , 
 result hardly fair opponent . 
 example , chapter Stravinsky 
 Mr. Scholes , Mr. Edwin Evans 
 argument appear form brief 
 quotation , knock . 
 Mr. Scholes fault , course ; inevitable 
 result reprint sideof controversy . 
 dispute hold ring . 
 ring : dressing - room 
 flushed Mr. Scholes reproduce 
 fight . similarly , chapter head , ' music 
 making , ' consist dispute 
 present writer fine piece 
 music poor theme . think 
 point long ago settle 
 great composer , chapter alter 
 view . regard open 
 theme Zyrofca fifth Symphonies 
 poor , little 
 weak alternative version quote Mr. Scholes . 
 try imagine person hear 
 fifth Symphony ; play little # o¢fo - theme , 
 ask hethink musical subject . 
 , ' 7 / subject ? merelya cuckoo 
 bad stutter ! ' right . let 
 hear Beethoven ; 
 hat note 
 remind cuckoo . loss 
 remind mention 
 fate knock Door , postman 
 need double rat - tat order send 
 mind fifth Symphony . trans 

 




 _ — _ — — _ — 


 — _ — — — F.R.C.O. ( CHM . ) ; A.R.C.O. ( CHM . ) 


 G , S 


 MUSICAL 


 1924 347 


 CHARLES WILLIAM PERKINS 


 MR , C. W. PERKINS 


 NEW BIRMINGHAM CITY ORGANIST 


 MR . G. D. CUNNINGHAM 


 TIONAL 


 MUSICAL 


 1924 349 


 neglect HANDEL 


 RHEINBERGER ENGLAND 


 MUSICAL 


 350 


 1924 


 ' criticism ' R.C.O. CHOIR 


 training - examination 


 neglect widorneglecte WORKS 

 Sir,—Mr , . Kaikhosru Sorabji , refer ' scandal ' 
 create constant performance certain musical 
 masterpiece , pianist indicate hiatus 
 great injustice ' scandal ' reach | 
 proportion M. de — — play a| 
 Concerto know Grieg . hear 
 M. de — — Liszt ' rarely play ' major Concerto , | 
 Aungarian Fantasia , Beethoven Emperor , 
 Saint - Saéns G minor , Franck symphonic Variations , | 
 good number fine work . M. de — — probably 

 request , music - love people 




 fair criticism ? 


 SHOIR 


 TONES 


 musical 


 1924 351 


 doh - minor — warning 


 1 1 ! ! 


 MUSIC worship 


 MUSICAL 


 1924 


 criticism LIVINGani order 

 Hummel , Beethoven , Rossini , 19th century 

 technical analysis express 




 


 1924 35o 

 exactly . mean critic sensibility | 
 closely comparable composer 
 . able explain poet 
 art , understand meaning 
 word use come spell . 
 way substantial measure able 
 follow process Beethoven actual musical thought , 
 valid appreciation work . 
 possible enjoy comparatively indeterminate 
 impact unknown chord ear , 
 suficient proper appreciation precise 
 faculty musical expression genius 
 Beethoven consist 

 play criticise number passage 
 Scriabin , Stravinsky , Schonberg , Dr. Dyson 
 use attempt discuss problem 
 style form architecture play large 
 conception musical achievement , long 
 actual basis art dispute . bluntly state , 
 grammar syntax music 
 present need systematic co - ordination . 
 observation discussion behaviour 
 inference chord , scale melodic 
 idiom base , definition tonality , modulation , 
 technical abstraction , 
 matter essence pure music , 
 vague external distinction replace 




 pianistic interpretation 


 BACH MUSIC 


 


 


 MUSICAL 


 1924ROYAL ACADEMY MUSIC 

 student ’ chamber concert Duke Hal 
 Wednesday , March 5 , begin . 
 ment Chausson Pianoforte Quartet , 
 performer display admirable ensemble . 
 item programme — composition presen : 
 student — ordinary interest . 
 , movement Pianoforte Sonata F shan 
 minor , Reginald King , play composer , wor ! 
 promise , , piece flute , horn , ané 
 pianoforte Kathleen Summers , composer obtain 
 charming effect combination instrument 
 rarely hear . item include 
 ment Beethoven Sonata pianoforte ’ cello , playe : 
 Mr. Douglas Cameron Mr. Harry Isaacs , firs 
 Ballade pianoforte solo String ( ) uartet , York 
 Bowen , song , recitation Rossetti 7h 
 bless Damozel 

 musica ! social meeting R.A.M. Club 
 place Academy Saturday evening , March 1 . Mis 
 Beatrice Harrison join Mr. John Ireland 
 fine performance new Pianoforte ' Cello Sonata 
 enthusiastic reception . later 
 evening Miss Harrison , inimitable style , play 
 delightful old Sonata Eccles . London Scottish 
 Choir contribute charming selection unaccompanied 
 - song , conductorship Mr. Fulton 




 ROYAL COLLEGE MUSIC 


 JSIC 


 IC 


 


 1924 


 TRINITY COLLEGE MUSIC 


 BYRD MEMORIAL 


 WILLIAM BYRD , 


 COLUMBIA 


 356 MUSICAL 


 1924 


 ALION 


 H.M.V 


 PHILHARMONIC CONCERT 


 NEW BAX SONATA 


 WIGMORE HALL , FEBRUARY 


 20 


 KODALY 


 EOLIAN HALL , FEBRUARY 29 


 MUSICAL 


 1924 


 E. E 


 E. E 


 SOCIETY HOLST 


 PROGRAMME 


 ROYAL CHORAI ELGAR 


 return 


 WIGMORE 


 HALL 


 DAME ETHEL SMYTH MASS 


 ITALIAN BALLET opera 


 OVENT GARDEN 


 GOOSSENS concert 


 OLIAN HALL , FEBRUARY 20 MARCH i2 


 MUSICAL 


 1924 


 BACH CHOIR 


 MUSK | 


 CENTR!I 


 DUTCH 


 CONTEMPORARY MUSIC 


 E. ethe narrative music . technic , , pro- 
 duction — notably high note — need 
 consideration . word Jesus sing good 
 style — earn , simple , affect — Mr. Stuart 
 Robertson , young bass expect 
 day . useful , powerful voice , quality 
 need add . Miss Dorothy Silk sing soprano 
 music , minor aria deeply- 
 moment day . Miss Millicent Russell 
 contralto , sing placid steady level . 
 distinction add 
 singing giving individual word character , 
 high relief . man aria sing Mr. Archibald 
 Winter Mr. Arthur Cranmer . Mr. Lofthouse play 
 continuo pianoforte . C 

 PHILHARMONIC CHOIR 
 Holst hymn Jesus pass test 
 acclaimed work 
 break , win way general esteem . 
 time hear — Queen Hall , Southwark Cathedral , 
 Albert Hall , Queen Hall , Philharmonic 
 Choir concert March 13 — grow stature , ! 
 reveal direct purpose detail 
 expressiveness force language . work 
 impress thoroughly Choir late 
 performance Mr. Charles Kennedy Scott , 
 voice - class orchestra thoroughly grip 
 music effect stirring . hymn Jesu 
 cap fine c minor Pianoforte Concerto Delius , 
 Mr. Howard - Jones play admiration , 
 Concerto cap Parry Blest Pair siren , 
 young work age high mortality . # 
 hour great music , musical London pass 
 blindness . ' 
 programme descend , Franck /syc # 
 , , new light composer . 
 inanimate music come good period ? netd 
 lemonade diluted , drink gallon : 
 doubt Parry , Delius , Holst good deal 
 taste Psyche . come Beethovens 

 Fantasia pianoforte , orchestra , chorus , 

 ck Psyche 
 ser . 
 iod 2 need 
 ik gallon 

 deal 
 Beethoven 
 

 

 MUSICAL TIMES — ApriL 1 1924 359 

 hear . rarely Franck Beethoven 
 work perform thank choir 
 chance acquaintance . 
 confess , , hear understand 
 neglect . M 

 IOSEF HOFFMANN 

 expect certain definite characteristic ' 
 musical reputation complete America , 
 characteristic tendency prejudice 
 Englishman find difficult forget power 
 press boosting process rampant 
 United States . ehleare Josef Hoffmann time 
 Queen Hall recently , Sir Henry Wood superb 
 background intuitive accompany , hard 
 believe pianist cross ' herring 
 pond ' . virtuoso pianist sincerely 
 admire Chopin work orchestral setting . 
 judge Hoffmann exquisitely beautiful performance 
 E minor Concerto devotee , 
 justify work piece picturesque pianism set 
 deliberately orchestral form fit 
 foursquare . satisfaction complete 
 true , turn . Hoffmann .Kolian Hall 
 recital later month commit occasional sin 
 artistic taste expect . 
 artist provocative gift individualism 
 carry excess 
 originality . striving Beethoven endeavour 
 unfold new pianistic possibility Op . 110 appeal 
 Hoffmann reverential nature , allow Sonata 
 express applied artificiality style . 
 colossal urge Schumann Symphonic Studies 
 appeal strongly rhythmic vitality 
 Hoffmann liberally endow , 
 characteristic Chopin major /o / onaise , 
 mettlesome steed prance series exhilarating 
 curvetting Berceuse small Chopin 
 dance piece , child impulsively kick 
 house block card parent build 
 light - fingered dexterity . artist depend 
 Hoffmann rhythmic effect bind 
 develop staccato mannerism , thunderously — 
 horribly — apparent inthe G minor aa//ade , 
 pianist begin dogmatise far left - handed 
 pounding . hearer hard logical 
 reason sign vulgarity superb grace 
 E minor Concerto , try excuse solvist 
 account numerous trial 
 strength abnormally large american concert - hall , 
 argument fall ground Albert Hall 
 follow Sunday , Hoffmann return 
 chastened mood , playing real excellence 

 G. Y. 
 . SOLITO DE 




 SOLIS 


 singer month 


 competition APRIL 


 GAIN 


 MUSICAL 


 1924 361BARMOUTH.—The amateur orchestra , reinforce 
 professional , style Harlech Festival Local 
 Orchestra , concert Barmouth March 5 . 
 Dr. Heath conductor , thirty player 
 draw Criccieth , Blaenau - Festiniog , Dolgelley , 
 Towyn , Aberdovey . programme include Gluck 
 overture / lcest , irish melody arrange 
 Dr. Heath , Tchaikovsky / eg7e string orchestra , 
 Rosamunde music 

 BIRMINGHAM District.—Mr . Eugene Goossens 
 interpretation Brahms second Symphony , City 
 Orchestra , March 6 , clean , penetrating quality . 
 alive warm , 4dag7o movement . Mr. 
 Sammons play Max Bruch G minor Concerto new 
 Vincent violin warm , tone , especially 
 low strings.——At Saturday night concert Mr 
 Appleby Matthews conductor . programme 
 include Borodin B minor Symphony Dvorak Water- 
 Fay — time Birmingham . , 
 , little tepid programme - music , 
 composer good . Miss Winifred Browne 
 soloist Beethoven C minor Pianoforte Concerto . 
 — — Saturday concert programme dance- 
 music . Strauss Wine , Women , Love Waltz 
 Gluck Ballet Furies notably - play 

 Madame Gell Ladies ’ choir delightful 
 - singing piece Brahms _ Holst , 
 Wagner Rhine Maidens music . Sunday 

 concert February 17 , Beethoven seventh Symphony 
 Grieg Sigurd /Jorsaifar Suite play . — — 
 Mr. Charles Knowles singer follow 
 Sunday . scena Zhe Flying Dutchman , Mr. 
 Knowles - produce bass voice 
 pleasure . Clock Symphony Haydn 
 . — — Sunday concert , conduct Mr. 
 Appleby Matthews , bring Brahms _ 
 Symphony , Mozart /domeneo Overture , Beethoven 
 eighth Symphony.——At Miss Sotham Mid - day 
 concert , Philharmonic Quartet play Tanéiév 
 Pianoforte Quartet E , set variation 
 Quartet Dvorak.——On March 13 , Mr. Frederick 
 Dawson mid - day pianoforte recital , 
 play work Beethoven , Scriabin , Chopin . — — 
 Miss Marjorie Hayward Miss Marjory Sotham 
 Aveutzer Sonata mid - day concert.——miss 
 Ethel Cobham Miss Violet Lewis start series 
 mid - day concert Wolverhampton . artist 
 appear Miss Winifred Browne 
 Miss Marie Fromm , Mr. John Goss . March , 
 Miss Mary Abbott Mr. Johan Hock gave Pianoforte 
 Violoncello Sonatas Nicodé Dohnanyi 

 BRaADFoRD.—A fortnight season B.N.O.C. 
 overshadow thing . repertory , 
 concession bad old fashion , include 7he Perfect Fool , 
 Alkestis , Gianni Schicchi.——Of recent orchestral 
 occasion , memorable performance 

 Orchestra , Mr. Julius harrison.——chamber music 
 programme introduce Sir Walford Davies Violin 
 Sonata e minor second Violin Sonata 
 Arnold Bax 

 BRIDPORT.—The Orchestral Society , conduct 
 Mr. Alex . Stone , play York Bowen Suite , 4/ play , 
 Schumann Quartet , Op . 47 , Beethoven 
 Symphony , March 4 

 BRIsTol.—For concert season , 
 February 23 , Choral Society perform Berlioz 
 Faust , assist orchestra . principal singer 
 Miss Florence Mellors , Mr. John Perry , Mr. Herbert 
 Brown . Mr. George Riseley conducted.——The Roya ! 
 Orpheus Glee Society eightieth annual lady ’ 
 Night , Mr. Riseley direction , February 28 . 
 unusual programme include Walmisley / wi#sh 
 tune , Bexfield Zhe Death Hector , Elgar 
 Wanderer Zut , sut , zut ! Patterson Zhe Wedding 
 Shon Maclean , Riseley Old Church Bells , Parry 
 Smile , bonnie lassie , piece . — — 
 Y.M.C.A. concert unemployed , 
 Co - operative Society Choir , Mr. A. F. Lawrence , sing 
 Walmisley Juste - powerful Dudley Buck Hymn 
 Music.——Mr . Herbert Parsons play piece 
 John Ireland recent recital — — Musical Society , 
 form member staff department 
 G.W.R. , comprise March choir - 
 voice string orchestra , conduct Mr. Clare G. 

 Hoffmann Soxg Norns , César Franck Song 
 Ermine , Karel Bendl cantata , Water - Sprite Revenge , 
 Fletcher 7he Galway Piper 

 CaRDIFF.—Bach French Suite ia G , Beethoven 
 Violin Sonata C minor , trio G Haydn 
 Mozart , play University College Chamber 

 concert February 18.——The MHarmonic Choir 
 Male - Voice Party Mr. Falkman Orchestra 
 combine performance , March 2 , Feélicien 




 362 


 1924GAINSBOROUGH.—Hamilton Harty 7he Wystic 7rum- 
 ‘ eler Musical Society February 18 , 
 inder Mr. Alan Stephenson 

 GLasGow.—At final concert season , 
 February 16 , Scottish Orchestra , conduct Emil 
 Mylnarski , play Beethoven C minor Symphony 
 excerpt 7he Mastersingers Tannhiduser 

 HUDDERSFIELD.—An excellent programme 
 Glee Madrigal Society February 26 . include 
 Lasso Hark , echo fall , Wilbye Stay , Corydon , 
 Elgar , song , Holst Song Blacksmith 
 Grail scene Parsi / a/. farewel 
 appearance Dr. C. H. Moody , conduct 
 Society year 

 Leeps.—Chamber music ascendant 
 lately . Mr. J. B. McEwen Nugae hear company 
 Brahms University Mid - day recital . Haydn , 
 Schubert , Speaight Shakespeare Fairy Characters 
 ' bohemian ' concert . Messrs. Sammons , Sharpe , 
 Murdoch play Frank Bridge Phantasy 7rio 
 Choral Union concert . occasion choir , 
 Dr. Coward , sing work Byrd , Weelkes Vesta 
 vas Latmos Hill descend , Bach sleeper , 
 wake,——Mr . Julius Harrison lecture ' Orchestra ' 
 Leeds Grammar School boy 
 February 20.——The Mass G minor Vaughan 
 Williams admirably sing Philharmonic Society 
 March 12 , Mr. Norman Strafford conduct 
 absence Dr. Bairstow 

 LiveERPOOL.—At Crane Hall concert February 20 , 
 Mr. Frank Bertrand play Beethoven Variations 
 C minor pianoforte , Mr. Harry Wilkinson play 
 Elgar ' Cello Concerto.——On February 22 Miss Stillwell 
 play Gerrard Williams Potfourr ? pianoforte , , 
 Miss Isabel McCullagh Miss Mary McCullagh , 
 Schubert Pianoforte Trio e flat.——The final Mossel 
 concert season , February 23 , bring Rosé 
 ( Quartet , Miss Marcia van Dresser , Mr. Egon Petri . 
 Schumann Pianoforte Quintet Hugo Wolf /talian 
 Serenade principal concerted number . — — 
 Vickers concert , February 23 , artist Miss Rosina 
 Buckman , Mr. Maurice d’Oisly , Mr. Kingsley Lark , 
 Vera Hall Pianoforte Trio.——At Crane Hall , 
 February 27 , Miss Dorothy Vincent pianist , Miss 

 Koushen sing russian song vernacular.——sir Henry 
 Wood conduct Philharmonic concert February 27 , 
 programme include Holst Auga/ Overture , V ork 
 Bowen Viola Concerto C minor , Mr. Lionel 

 OxForD.—In connection scheme organize 
 Mr. W. K. Stanton , provide concert boy 
 St. Edward School , Miss Myra Hess play Preludes 
 Fugues Bach : Franck Prelude , Chorale , 
 Fugue , Schumann /'afz//ons , piece Ireland , 
 O’Donnell , Rachmaninov large audience boy 
 February 20.——On February 24 Elizabethan Singers 
 join Mr. Keith Douglas chamber orchestra , 
 combine performance Masque 
 Purcell Déoclesian . band play Holst 
 St. Pauls Suite Mr. Douglas arrangement Rameau 
 Tambourin.——On February 27 party local musician 
 perform Violin Sonata Joseph Gibbs , Elgar Violin 
 Sonata , Hamilton Harty /riskh Romance violin , 
 Scarlatti Concerto sacro tl Santissimo Sacramento fot 
 voice , string trio , pianoforte , 7%e .1 / oor , new song 
 Dr. W. H. Harris , Dr. Ernest Walker 
 pianist , Miss Murray Lambert violinist , Mr. Sumner 
 Austin singer.——Mr . Roland Hayes song 

 recital Town Hall February 28.——The 
 Orchestral Society , final concert March 13 , 
 play Schumann Genoveva Overture , Beethoven 

 Pastoral Symphony , Arthur Bliss row , Mr. Maurice 
 Besly conduct , Miss Dorothy Moulton sing french 
 folk - song orchestral accompaniment.——in _ 
 absence Sir Hugh Allen , Mr. W. K. Stanton conduct 
 Bach Choir March 9 , afraid ( Bach ) , 
 Parry Job , Holst Ode Death sing 




 _ 1 


 363SCARBOROUGH.—The Rosé Quartet hear 
 February 15.——The Philharmonic Society end season 
 March ii miscellaneous programme comprise 
 Stanford Songs fleet , Cornelius Hero Rest , 
 - song , solo singing playing . Dr. Ely 
 conduct 

 SHEFFIELD.—The E flat clarinet Sonata Brahms 
 play March 5 Mr. William Tomlinson , Miss 
 Ethel Cook pianofortee —— ( ueen Hall 
 Orchestra , London , come Sheffield March i8 
 play Beethoven seventh Symphony , Brahms 
 tragic Overture , Bach second Brandenburg Concerto , 
 Dohnanyi Variations , Siegfried /dy// Sir 
 Henry Wood 

 SipMouTH.—On February 21 
 local violinist , recital , assist Mr. Walter 
 Belgrove , vicar - choral Exeter Cathedral . recitalist , 
 play Concerto romantigue minor , Godard , 
 join Miss A. McNeile Concertante violin 
 Dancla , Miss Ethel Gough César Franck 
 sonata pianoforte violin . — — season 
 subscription concert , organize number lady , 
 Miss M. Allen hon . secretary , form 
 pianoforte recital Miss Myra Hess February 2 

 Miss Doris Gough , 

 SWANSEA.—At lecture ' colour Music , ' 
 Sir Walford Davies February 27 , University 
 College Musical Society , Aberystwyth College Trio 
 play music Brahms , Beethoven , Dvorak . 
 Beethoven Archduke Trio , Sir Walford Davies 
 pianoforte 

 TorQuay.—Mr . E. W. Goss Winter Orchestra play 
 Caucasian Suite M. Ippolitov Ivanov February 20 , 
 ani Beethoven String ( Quartet E flat include 
 programme.—-—At Symphony concert 
 February 21 , augmented orchestra play Schubert 
 fifth Symphony , Holst Fugal Concerto flute oboe 
 ( Mr. , Gleghorn Mr. G. Ellis soloist ) , 
 Borodin Steppes Central Asia , Rimsky- 
 Korsakov Capriccio Espagiiol.——On February 23 
 Coleridge - Taylor Petite Suite de Concert play . — — 
 February 27 Quilter Suite , 4s } onu Like /t , Handel 
 Violin Sonata , chief number . — — Burmese 
 Suite Woodforde - Finden play February 29 . 
 — — March 12 , Mr. Henry Crocker Orchestra 
 player perform Beethoven second Symphony , 
 Schumann Pianoforte Concerto , Dr. Harold Rhodes 
 pianist , Purcell Suite strings.——The winter 
 orchestra , conduct Mr. E. W. Goss , recently 
 play suite , 7ze Seasons , string pianoforte , 

 C. Ames , Quilter Zhree English Dances , Suite 
 like , Unfinished Symphony , Saint - Saéns 
 Overture , Princess Jaune , Dancla Petit Symphonic 

 IRELAND 

 February 15 , Gaiety Theatre , Dublin , Mr. 
 Joseph O’Mara Opera Company present per- 
 formance Ireland austrian opera , 7he Afostle 
 St. Otmar , compose Dr. Wilhelm Kienzl , fellow - pupil 
 Weingartner Busoni . Kienzl term 
 intimacy Wagner Bayreuth , keen 
 admirer Schumann , style 
 amalgamation school master . Kienzl 
 bear January , 1857 , live life 
 Gratz . opera , Urvas ? , Dresden 
 February , 1886 , - know production , Der 
 Evangelimann — date 1894 — wonderful vogue . 
 opera englishe 7he Afostle 
 St. Otmar , translation Mr. Walter 
 Meyrowitz , conduct work , uncon- 
 vincing , repeat performance February 2 % , 
 interesting revival —— Philharmonic String 
 ( Quartet recital auspex Royal 
 Dublin Society Theatre Royal February 18 , 
 play Mozart , Schubert , Ravel . - 
 particularly received.——Dr . Esposito Mr. 
 Clyde Twelvetrees pianoforte ’ cello recital 
 auspex February 25 — season , 
 — — AA public meeting hold Mansion House , 
 February 19 , consider scheme erection 
 Citizens ’ Building Concert - Hall Dublin . 
 Feis Ceoil Council offer £ 500 , large 
 subscription promise . year Dublin 
 adequate concert - hall , surely 
 matter urgency erect suitable building central 
 site . — — Signor Viani public concert 
 Theatre Royal March 1 , delight vast audience . 
 song varied , play accom- 
 paniments.——Rummel draw fairly large audience 
 Theatre Royal February 16 , playing Bach , 
 Beethoven , Chopin , Wagner greatly appreciate . 
 recital ( Bach , Beethoven , Schubert programme ) 
 Ulster Hall , Belfast , February 19 , 
 successful , 
 Ulster Hall , Belfast , February 16 , Miss Kathleen 

 Lafla , austrian mezzo - soprano , principal 
 attraction . choice song fine dis- 
 crimination . excellent programme provide 




 GERMANY 


 KODALY SCHONBERG 


 364 MUSICAL 


 1924 


 BORIS GODOUNOV ' GROSSE VOL ! ER 


 LSO 


 SMETANA CENTENARY GERMANY 


 OLI 


 SSMAN 


 NEW YORKhis early inspiration later year ! 
 M. H. FLINt . 
 ROME 
 Augusteum outstanding success 

 member ( ) uartet 
 collaborate work , pro- 
 duction Mozart Beethoven , announce 
 ' novelty ' Augusteum — v.e . , Sinfonia Con- 
 ertante violin , viola , orchestra Mozart , 
 Beethoven Concerto violin , ’ cello , pianoforte , 
 orchestra . follow day Busch Quartet gavea 
 concert Royal Philharmonic Hall , play Schubert , 
 Beethoven , Mozart 

 M. Alfred Cortét visitor Augusteum , 
 concert orchestra 




 LINT 


 65o 

 second concert , organize Royal Philharmonic | programme include 7axnhdauser Overture , Liszt 
 Society , bring hearing interesting unpublished | Zes Pré / udes , Schubert Unfinished Symphony , mid- 
 Quartet C minor , Boccherini , MS . | summer night Dream Overture , Elgar Pomp 
 Library Accademia Sta . Cecilia . movement | Circumstance March , Liszt Concerto , Paul Wells 
 interesting series variation | assist artist , Liszt - Popper Hungarian Rhapsody , 
 phrase - know JlVestminster Chimes . as|with Lionel Bilton , Beethoven C minor Concerto , 
 novelty , programme contain sonata C minor | Mark Hambourg . 
 viola pianoforte , work Alexander Bustini , auspex Women Musical Club 
 jafanese Songs Setaccioli . | young New York String Quartet premiere , lean 

 Amica della Musica succeed engage | mainly modern composer . Cecilia Hansen , 
 - know finnish violoncellist , Lennart von Zweygberg , | brilliant scandinavian violinist , appearance , 
 play fine Stradivarius instrument , know | fully justify New York high opinion playing . 
 . programme include Bach | Moritz Rosenthal return year ’ absence , 
 sonata D major suite G major violoncello | Galli - Curci pay annual visit 




 TORONTO — 


 366 MUSICAL t 


 ORCHESTRAL novelty conductor 


 CHAMBER MUSIK 


 MODERN 


 PAUL BECHERT 


 ECHERT 


 


 1924 367 


 answer BRIEF 


 ANNIVERSARY MUSIC . 


 splendid number 


 1924 programme 


 TASK " 


 - song . 


 " 


 sample post free 


 - SONG 


 


 CHOIR - leader organist 


 26 EASTCASTLE STREET , LONDON , W.1 


 ADAMS . ( L.T.C.L 


 TENOR 


 PERCIVAL 


 dure month . 


 chool song 


 A. B 


 1360 


 1301 


 " T ° ONIC SOL - FA serie : 


 publish 


 H. W. GRAY CO . , NEW YORK . 


 AILE . E.—A 


 organist CHOIRMASTER want 


 ST . PAUL AMERICAN CHURCH , ROME , ITALY . . 


 MAGDALEN COLLEGE , OXFORD . 


 A. B 


 34 


 ORK 


 ante 


 uire 


 nte . 


 ALY 


 VIOLIN 


 extra supplement . 


 NOVELLO - song book 


 collection 


 ENG&AND 


 NOVELLO - song book 


 ( second serie 


 MEMORIAM , ANNIE GOODHART 


 true valour 


 - song mix voice 


 word J. BUNYAN , 1628—88 


 music 


 GEOFFREY SHAW 


 BND 


 true valour 


 ss 2 = f 


 true valour 


 3 sss SS ee sss sle e lie = 


 oo — — SS 


 true valour 


 SS 


 = SS ES SE = 


 299 " 4 


 true valour 


 = SS SS 


 SS 


 ban 


 cn 


 true valour 


 _ = = = — — — _ _ 


 FSB = = = 


 XUM 


 NOVELLO 


 265 " 

 361 Echo word ... ... J.L.Hatton 2d . 
 362 hath pleasant face oe ad . 
 363 time , keeptime .... 4d . 
 364 lo , peaceful shade ... vi 2d . 
 365 lark singe ms 4d . 
 366 Spring , sweet Spring 4d . 
 sip beak 20 . nce ce tee - 4d . 
 368 fishing boat . 0 2d 
 369 lark ... oe 4d 
 370 moon shine . calmly | bright , . 4d . 
 371 Thereproach ... ... oe 2d . 
 372 swing ... ite : je 4d . 
 373 wrecked hope mm 4d . 
 374 twilight ... ee 2d . 
 375 twilight round ae 4d . 
 376 sigh ? 4d . 
 377 shall lover st ad . 
 378 night ... Gounod 4d . 
 379 dawn ‘ day , . Reay 6d . 
 389 calm sea ... H Hiles 6d . 
 381 wreck Hesperus 8d . 
 382 uncertain light .. » Se humann 4d . 
 383 confidence . double chorus , , 4d . 
 384 dream " oe 2d . 
 385 boat p 4d . 
 386 Spring approach Se ymour E gerton 4d . 
 387 wild rose ... ae Sere " 4d . 
 388 wood 4d . 
 389 rose soul ... e 2d . 
 390 adieu Woods ... ... ds 4d . 
 391 King Winter — = 4d . 
 392 Miller ... G.A.Macfarren 2d . 
 393 mountain rill ... - 2d . 
 394 Allisstill ... o ad . 
 395 sleep ! birdisinit nest ; oe 4d . 
 396 hush death . Hiles 8d . 
 397 evening ( hour ) . ‘ Leslie 2 
 398 bright morning star * 4d . 
 399 boat song ( hail chief ) 2d . 
 400 triumph death ... C. Holland 4d . 
 401 bright morning star Pierson 4d . 
 402 bright- haired morn ... S. Reay 4d . 
 403 red o'er forest ... 4d . 
 404 sweetis breath early morn , 4d . 
 405 wavelet ripple Ciro Pinsuti 8d . 
 406 gaily sing play ee 8d . 
 497 gently fall evening ... Marenzio 4d . 
 408 lily white , crimson roses(5v . ) , , 4d . 
 409 shepherd pipe ( 5 v. ) - 4d . 
 410 spring return ( 5 v. ) - .. . 4d . 
 411 rapid bound(é\ ) . : 4d . 
 412 thosedainty daffadillie ( 5 v. } Morley 4d . 
 413 dainty , fine , sweet nymph , , . 4d . 
 414 shoot , false love , icare , o 4d . 
 415 o ny — ( 6 v. ) .. " Palestrina 4d 
 416 ye singer - H. Waelrent 4d . 
 417 fie love .. G.A.Macfarren 2d . 
 418 wind autumn ! ... Chas . Oberthiir 3d . 
 419 softly fall shade Silas 3d . 
 420 love melittle , lovemelong L. Wilson 3d . 
 421 shall | tell love Wesley 4d . 
 422 lover lass J. Booth 2d . 
 423 love questionandreply J.B.Grant 3d . 
 424 , loathe melancholy(5 v.)Lahee 6d . 
 425 evening song . M. Hill 4d . 
 426 welcome dawn summer ’ sday - 4d . 
 427 charge Light Brigade Hecht € d. 
 428 beauty onthe mountain Goss 2d . 
 429 o sweet Mary ( 5v . ) . 6d . 
 430 lo ! wheretherosy - bosom'dhours , , 6d . 
 431 eye glow - worm 6d . 
 432 bell St. Michael Stewart 6d . 
 433 Cruiskeen Lawn ( 5 v. ) e 4d . 
 434 wine cup circle es 4d . 
 435 ye mariner England H. Pierson 2d . 
 436 Vesper Hymn . Beethoven 3d 
 437 sorrow Naumann 3d . 
 438 Swallows Pohlentz 3d . 
 439 hope Faith Weber 3d . 
 440 Hark , hark , l ark Klicken 4d . 
 441 walk dawn Gade 4d . 
 442 winter day J.Caldicott 6d . 
 443 Homeward ... Henry Leslie 6d . 
 444 sea ! calm o'er Marshall 6d . 
 445 rest hath come - 3d . 
 446 Hymntothe Moon ... Josiah Booth 6d . 
 447 Brook . . C.G. Reissiger 4d . 
 448 Secret .. oe 4d . 
 449 odour sweet R. Miller 4d . 
 450 water R. de Curvy 4d . 
 451 Water - lily .. N.W.Gade 3d . 
 452 sone thati love ... F. Kicken 4d . 
 453 tree bud 4d . 
 454 sing bird . Franz Abt 3d . 
 455 o world ! thou art Hiller 6d . 
 456 winter song aoe H. Dorn 4d . 
 457 arrow song ... W. Hay 4d 
 458 king Queens Ciro Pinsuti 4d . 
 459 ask heart ? és 4d . 
 460 Rhine Raft Song ... ad . 
 461 silent tide o 2d . 
 462 apriltime ... ... ... ee 3d 
 463 song Pan ove oe 4d . 
 464 autumn come ... F. Corder 4d 

 - song book ( continue 




 SE