


 MUSICAL time 


 CRAMER patent AMERICAN ORGANS 


 price 12 200 guinea 


 RAMER AMERICAN ORGANS 


 CRAMER £ 12 AMERICAN ORGAN 


 " SIORAMER £ 22 . 2 AMERICAN ORGAN 


 |ORAMER £ 28 AMERICAN ORGAN 


 JORAMER £ 34 AMERICAN ORGAN 


 JORAMER 5 : 5 GUINEA AMERIC ORGAN 


 CRAMER 70 GUINEA AMERICAN ORGAN 


 13 stop 


 JCRAMER 85 GUINEA AMERICAN ORGAN 


 10 stop pedal 


 CRAMER £ 100 AMERICAN ORGAN 


 12 stop , 2 manual 


 PRAMER £ 125 AMERICAN ORGAN 


 16 stop , 2 manual 


 professional notice 


 MR . H. STAFFORD TREGO 


 MR . J. TILLEARD , 


 SIC engrave , print , PUB 


 HARGREAVES AMERICAN IRON frame 


 GRAND pianoforte 


 professional connoisseur ’ HARMONIDM 


 harmonium — harmonium — harmonium 


 DRAWING ROOM MODEL , 


 13 " " " 26 " 


 W. SNELL PEDAL harmonium 


 WILLIAM 


 BRASS , REED , string , DRUM fife band , 


 pianoforte , harmonium 


 set REED instrument sale . 


 PASCALL VOICE JUJUBES . 


 RGAN , PIANOFORTE , HARMONIUM , HAR- 


 COTTELL concert 


 R. LANSDOWNE 


 18.5 


 USI 


 ALI 


 LARI 


 PRICE shilling SIXPENCE " 


 WAGNER 


 EETO 


 TANNHAUSER (= 


 


 


 NATALIA MACFARREN 


 


 ASK NOVELLOS EDITIONS 


 LONDON NEW.YORK : NOVELLO , EWER CO 


 557 


 HARVEST anthem 


 aid 


 NORTH COATES CHURCH ORGAN FUND 


 SECOND conclude volume 


 VILLAGE ORGANIST 


 C , JEFFERYS , 57 , BERNERS ST . 


 CHORAL SOCIETY 


 collection 


 edit 


 MICHAEL WATSON 


 popular 


 SOLOS duet 


 


 


 FELIX GANTIER 


 STEPHEN GLOVER 


 popular MARCHES : — 


 8 3 0 


 LOUIS DUPUIS 


 CATHEDRAL GEMSImperial ) Mass ... o 

 4 , MaLaGca.—Fantasia , subject Beethoven Mass 

 eee eee eee eee ore eee 
 5 Roven.—Fantasia , subject Gounod Messe 




 C. JEFFERYS 57 , BERNERS STREET ( W 


 558 


 O PROFESSIONAL GENTLEMEN.—WANTED , 


 practical PIANOFORTE , HARMONI@ 


 PIANOFORTE MUSIC trade 


 559 


 MUSICAL TIMES 


 560 


 7 = SS SS SS SSS SS SE 


 " POOR ROSY 


 DAY judgment 


 = SS | 


 561 


 NN 


 NATIONAL MUSIC - MEETINGS 


 CRYSTAL PALACE 


 562 


 563 


 MAJESTY OPERAROYAL ACADEMY MUSIC 

 tue annual public concert Institution , 
 conductorship Mr. John Hullah , Hanover 
 Square Rooms 22nd ult . , large audience . 
 pianoforte playing conspicuous feature 
 selection congratulate pro- 
 fessor zealously labour promote 
 healthy development important branch study 
 result exertion ; steady progress 
 vocal department equally apparent 
 moment doubt attend 
 watch interest periodical exhibition school 
 musical instruction , adverse in- 
 fluence , cease assert maintain claim 
 confidence support public . lady pian- 
 ist Miss Whomes , play movement 
 Sir Sterndale Bennett Concerto F minor ac- 
 curacy precision ; Miss Channell , especially dis- 
 tinguishe truly intellectual rendering 
 movement Schumann Concerto minor ; Miss 
 Florence Green , play Hummel " Retour 4 Londres " 
 delicacy refinement elicit warm 
 applause ; Miss Connolly , thoroughly master diffi- 
 cultie Bach " Chromatic Fantasia ; " Miss Baglehole , 
 , Mendelssohn " Serenade Allegro Giojoso , " 
 fully maintain reputation spread 
 wall school rear ; 
 Miss Chapman , play " Agitato Assai , " 
 Sir Sterndale Bennett " ' Suite des Piéces , " commend- 
 able care judgment . male pianist represent 
 Mr. Walter Fitton , exhibit highly cultivate 
 touch musical feeling Handel Fugue E 
 minor Chopin beautiful study black key ; 
 Master Walker , promising little artist , 
 Allegretto Beethoven sonata e. minor ( op . 90 ) 
 appreciation beauty music com- 
 mand passage draw forth loud demon- 
 stration approval ; Mr. Ridgway , , 
 place conclusion unusually long pro- 
 gramme , play movement Schumann Sonata 
 G@ minor reward encour- 
 age applause . instrumentalist 
 mention Mr. Howard , , movement 
 Beethoven violin concerto , executive power , 
 work somewhat difficult , student 
 advanced ; unqualified praise award 
 organ performance Miss Moultrie Mr. Walter 
 Fitton , play Bach Grand Prelude B 
 minor , " Adagio " " Allegro Vivace " 
 Mendelssohn Concerto F minor , decision 
 firmness touch prove solid course 
 organ instruction Institution . vocal portion 
 programme support . Miss. Mayfield , 
 Recit . Aria Vaccaj " Romeo e Giulietta , " display 

 564 

 MUSICAL TIMES.—Avetsr 1 , 1872 

 excellent voice style ; Miss Goode , Mendelssohn 
 " Jerusalem , " especially solo occur MS . 
 anthem Henry Guy ( student ) thoroughly satisfac- 
 tory ; Miss Jessie Jones Beethoven " ah perfido " 
 earnestness dramatic feeling ; Mr. Howells 
 sing Mendelssohn " heart " good ex- 
 pression ; Mr. Pope evidence possession fine 
 bass voice Mozart " Possenti Numi ; " Mr. Henry Guy 
 ( way concert - room ) 
 successful Mozart " O cara immagine ; " Mr. Wad- 
 ( win prize late National Music Meet- 
 ing , Crystal Palace ) gain - deserve applause 
 rendering Handel " Nasce al bosco . " Misses 
 Francis , George , Butterworth , Kaiser , Mrs. Dolby lend 
 valuable assistance , vocalist mention , 
 concerted piece , evidently care- 
 fully prepare . composition 
 student perform — - write sacred - song 
 Roberts , " oh distant star ; " movement Sym- 
 phony m C minor Eaton Faning , exhibit 
 promise ; exceedingly clever anthem Henry Guy ( 
 allude ) , ' " joyful noise unto 
 Lord ; " Festal Overture C Wingham , com- 
 pose commemoration Jubilee Royal Academy 
 Music , clearly write excellently instrument 
 appear scarcely work novice . concert , 
 earnest speech Sir Sterndale Bennett , 
 Principal Academy , refer pre- 
 sent prosperous state Institution , prize dis- 
 tribute Mrs , Gladstone . award follow 

 Femate DerpartMent . — Silver Medals : Miss Florence 
 Green ( Pianoforte ) ; Miss Agnes A. Channell ( Pianoforte ) ; 
 Miss Gertrude Mayfield ( Singing ) ; Miss Rhoda E. Barkley 
 ( General Progress ) . Bronze medal : Misses . F. Holmes , 
 Annie Martin , Emily A. Troup , Florence Firth , Moultrie 
 ( Organ ) . book : Misses F. Baglehole ( Silver Medalist , 1871 ) , 
 Sarah A. C. Goode ( Bronze Medalist , 1870 ) , Hemmings , 
 Dickenson , Hancock , Whomes ( Bronze Medalist , 1871 ) , 
 Chapman , Jessie Jones , Eliza J. Hopkins , Jane Whitaker , 
 Maria L. Bagnall , Younger , Taylor ( Bronze Medalist , 1871 ) , 
 Conolly ( Bronze Medalist , 1871 ) , Waite , Organ ( Silver 
 Medalist , 1870 ) . letter commendation : Misses Deprez , 
 Ludovici , Carpenter , Griffiths , Klugh , Harford , Newall , 
 Judkins , Francis , Brand . Sterndale Bennett Prize ( purse , 
 contain guinea ): Miss Florence Augusta Baglehole . 
 Certificate commendation : Miss Florence Green 

 PHILHARMONIC SOCIETY 

 programme seventh concert , 24th June , 
 contain interesting novelty , term 
 apply Bach Concerto string , perform 
 time country . course 
 somewhat antiquated form , work listen 
 utmost pleasure audience , 
 welcome specimen music period 
 slow movement composer 
 orchestral Suite D interpolate 
 movement Concerto , order final Allegro 
 come ear effect . great 
 contrast Bach quiet masterly composition 
 select Beethoven Symphony ( . 7 ) , 
 commence second , play 
 utmost vigour finish . overture " 
 Midsummer Night Dream " " Der Berggeist ’' ( Spohr ) , 
 orchestral piece , Madame Norman- 
 Néruda excellent performance Spohr dramatic Con- 
 certo violin , feature evening 

 Bettini . eighth concert season wag 
 8th ult . , performance commence 
 Brahms serenade D , orchestra , work unequal 
 merit doubt permanent position 
 music ' " ' Young Germany , ' undoubted 
 mark genius , representative 
 school prevail believe worth g 

 endure Germany , Spite 

 , undoubted merit moye . 
 ment — especially Minwettos Scherzos — thg note 
 work unanimously hail relief . 
 orchestral prelude music Sophocles ’ " Ajax , " 
 Sir Sterndale Bennett ( perform firs 
 time ) , judge " overture , " shor 
 introduction b flat major , lead Allegro 
 tonic minor evidently intend suggest 
 character important illustrative music 
 follow . subject prelude extremely attractive , 
 variety colour instrumentation 
 invest brief Prelude utmost interest , 
 Mr. Charles Hallé performance Mendelssohn Concert 
 D minor , means beyondreproach . manypary 
 passage drag , movement tempo 
 positively alter . applause , , loud 
 enthusiastic , recall conclusion Con 
 certo opinion share genenl 
 audience . Madame Parepa fine singing Beethoven 
 " ah perfido " fully appreciate , Mr , 
 Santley artistic delivery Rossini " ' Alle voci della 
 gloria . " " Beethoven Symphony C minor include 
 programme , Weber " Jubilee " ’ overture , 
 conclude concert , Mr. Cusins , conductor , 
 forward receive earn applause 
 audience 

 ir hope continental neighbour 
 securely rely upou accuracy report ou 
 musical London journal , 
 lead astray . instance , speak 
 performance Arthur Sullivan Z'e Dewm Crystal 
 Palace 18th ult . , Athenewm 
 " conduct composer , " Orchestra tell 
 reader second concert commence 
 Te Dewm " p.m. " consider M. 
 Manns conduct work , composer pees 
 announce , second 
 concert begin soon o’clock , scareél 
 imagine information supp 
 trifle compare follow para ; 
 extract Sunday Times 7th ult . : — " Madlle 
 Christine Nilsson , ' - headed Nightingale , ' 
 week astonish visitor South Londo 
 Palace London Road , Metropolitan Halla 
 Edgware Road 




 565a new Edition " Hymnary " publish 
 Messrs. Novello Co. ina cheap portable form . 
 Hymns print double column type remarkable 
 clearness . understand edition music , 
 considerable time preparation , 
 promise remarkable work 
 kind issue , ready October ist 

 Miss Excuo evening concert Tuesday , 
 2nd ult . , Hanover Square Rooms , assist 
 Madlle . Anna Regan , Miss Dwight , Miss Crichton , Mr. W. 
 C. Bell , Mr. Alfred Bennett , Mr. Barrow , Mr. Frederick 
 Chatterton ( harp ) , Mr. Nicolas Mori ( violin ) . 
 Beethoven " Kreutzer Sonata " ( Mr. Mori ) , Miss 
 Elcho display high power pianist , 
 reward warm deserved applause . Madlle . 
 Regan sing usual excellence , Miss Dwight 
 highly successful ' " Fioraja , ’' Miss Crichton song 
 " dream " artistic finish , Mr. Bell 
 sing " Hybrias Cretan " good effect . Mr. C. F. 
 Webb assist Mr. Lansdowne Cottell accompany 
 vocal music 

 afternoon Friday , 5th ult . ,a musical exercise 
 perform Sheldonian Theatre , Oxford , 
 ve Rector Exeter ( officiate Vice 

 Tue organist write defence Mr. Curwen 
 notation , mention case extraordinary youth 
 " bear bread Tonic Sol - fa , " ' scarcely 
 thank , imagine , print letter 
 stand . advocate new system 
 " protect friend 

 Tue Mozart Beethoven Society concert 
 16th ult . , attractive feature 
 d minor quartet ( Mozart ) , excellently play Herren 
 Stickle , Hunneman , Von Czeke , Schuberth ; ' Deh 
 Vieni , " carefully sing Madlle . Florella ; " La ci darem , " 
 Miss Frenie Herr Carl Bohrer ; ' voi che 
 sapete " ' " lullaby " ( Sullivan ) , render 
 Miss Marie Arthur , young lady promise 

 tur enormous gathering professor , amateur , 
 patron music dinner celebration Jubilee 
 year Royal Academy Music , hold Willis Rooms : 
 3rd ult . , convincing proof widely spread 
 appreciation effort Institution long 
 earnest struggle maintain sound healthful 
 school musical education country . chair 
 occupy President Academy , Lord Dudley , 
 support Principal , Sir Sterndale Bennett , 
 large number professor friend Institu- 
 tion . dinner usual loyal toast having , 
 Mr. John Hullah propose " Houses Parliament , " 
 allude position Academy occupy 
 public esteem , small public money : 
 annually vote maintenance . toast 




 CHORUS , TREBLE SOLO 


 TREBLE 


 1 _ 


 TREBLE 


 TENOR 


 PIANO 


 10 


 P - PP 


 1 " 5 


 J J 


 NY 


 SS 


 = = SS SS 


 pp 


 SS { 


 - t 7 1 7 | 


 SS SA 


 Z 


 " - 4 2 


 lovely THY dwelling fair 


 NX 


 pp 


 t n } 


 — — — = — _ p 


 72 = = 2 


 : BB 


 ENGLAND , FRANCE , PRUSSIA , AUSTRIA , BELGIUM , ITALY , AMERICA , 


 ASS 


 [ PARTS , 1867 


 JOHN BRINSMEAD & SONS 


 18 , WIGM 


 573a rormer volume work gain wide circula- 
 tion , , presume , success 
 induce collection issue present continuation 
 useful series piece , , excep- 
 tion ' , live composer . number , 
 volume , narrowly limited respect difficulty ; 
 pedal , 
 write , easy description ; con- 
 cise extent . contributor Mr. W. H. Callcott , 
 Mrs. Mounsey Bartholomew ( supply graceful 
 piece , prefer second , Andante g ; 
 begin half instead 
 bar ) , M. Delaborde , Dr. Gauntlett ( canzonet begin 
 phrase melody key e flat 
 Schumann , Brahms write beautiful Varia- 
 tion , , highly meritorious , 
 suited pianoforte , tone ac- 
 cordance player touch , organ , 
 admit inflection accent ) , Mr. F ’ . E. Gladstone , 
 organist Chichester Cathedral , Mr. Franz Nava ( 

 iven melodious Andante con grazia ) , Mr. E. Silas 
 Pastorale charmingly quaint , lose 
 effect ought , right hand 
 play portion accompaniment 
 chief melody , devote separate manual 
 , desirable , means 
 individual prominence ) , Mr. John Hullah , Mr. G. A. Mac- 
 farren , Sir Julius Benedict , Mr. Henry Smart , Mr. W. G. 
 Cusins , Mr. C. G. Verrinder , Mr. J. F. Barnett , Mr. R. 
 Forsey Brion ( merit expressive Andante 
 compensate little knowledge , 
 likely bring favourable notice ) , Dr. Rimbault 
 ( piece attractive , prove 
 spend time composition , 
 compel devote countless 
 easy arrangement ) , Mr. C. Goodban , Dr. E , G. Monk ( 
 contrapuntal variation capital tune , " 
 isLuke , " Anglican Hymn Book , grave char- 
 acter piece volume , interesting 
 merit exceptionality ) , Mr. 
 E.H. Turpin ( piece short far 
 prefer , evidence unlucky influence 
 organ - playing writer arm know- 
 ledge practical experience resist , tempt , 
 , sweetly - sustain sound , trust effect 
 strongly - coloured chord remote change key , in- 
 stead true musical idea ) , Dr. Arnold , Mr. Henry Farmer , 
 Mr. S. Reay Newark , Mr. H. S. Irons , Dr. Stainer ( 
 Song praise ambitious neighbour , 
 " ado " small extent slender 
 matter ) , Mr. Joseph Barnby ( piece prefer 
 Allegretto , graceful ) , Dr. Wesley 
 ( Andante somewhat long 
 important piece collection , small pretension 
 large fulfilment , admirably fit instru- 
 ment , high worth musical idea ) . 
 - seven brief , original composition enumerate , 
 extract Mozart , Beethoven , Onslow , 
 fill odd half page , series pleasantly ex- 
 " pan present state musicianship England , 
 find valuable practical use short volun- 
 tarie need , town village 

 Barcarolle , Pianoforte . Martin Miiller 




 SRL 


 2 CRB GY PD 


 575 


 editor MUSICAL TIMES 


 organist ’ salary organist ’ 


 appointment . 


 EDITOR MUSICAL TIMES 


 8 . F 


 EDITOR MUSICAL TIMES . 


 FREDERIC ARCHER 


 CORRESPONDENTSADELAIDE , AUSTRALIA.—There large attendance 
 Quarterly Concert Philharmonic Society , Thursday , 25th 
 April , Town Hall . programme consist Handel Acis 
 Galatea , Mendelssohn Walpurgis Night , 
 effect , soprano solo Handel Serenata ( espe 
 cially song ' ' hush , ye pretty warbling choir " ) sustain 
 Mrs. Harris marked success , chorus interpret 
 precision energy . Mr. E. Spiller conductor , 
 Mr. J. Hall lead orchestra , Mr. J. Shakespeare preside thé 
 pianoforte 

 BeprorD.—Mr . Diemer , talented conductor Bedford 
 Amateur Musical Society , successful morning concert 
 Assembly Rooms Thursday , 4th ult . vocalist Miss 
 Emily Spiller , previously visit Bedford auspex 
 Amateur Society ; Mr. Lazarus ( Royal Italian Opera ) 
 play piece clarionet . remain portion 
 programme Mr. Diemer number young lady 
 pianoforte class , exhibit great skill . Miss Spiller sing 4 
 recit . aria , " love rose " ( H. Smart ) , Verdi aria , ' ' ah ! fors ? 
 lui , " Macfarren ' * Pack cloud away " ( clarionet obdbdligato 
 Mr. Lazarus ) , " Esmeralda " ( W. C. Levey ) , muc 
 applauded . Mr. Lazarus Mr. Diemer Andante 
 Rondo Sonata e flat , Op . 48 ( Weber ) , perform sol 
 Der Freyschiitz excellent style . selection 
 Mr. Diemer young lady ( pianoforte harmonium ) , 
 include Beethoven symphony C minor , . 5 . 
 concert successful 

 BopMin.—Two successful concert Philhar- 
 monic Society , Market House ( especially fitted decora 
 occasion ) , visit Royal Cornwall Agricultural 
 Association , 12th 13th ult . , Haydn Creation , & 
 Miscellaneous Selection perform . soio allo ’ 
 Miss Ellen Horne , Mr. J. Rogers , Mr. J. Lander . band 
 chorus ( number 120 performer ) acquit 




 dure month , 


 LATIN word 


 LATIN word . 


 578 


 ANGLICAN CHORAL SERVICE BOOK , 


 USELEY MONK PSALTER 


 OULE collection 527 chant , 57 


 OULE DIRECTORIUM CHORI ANGLI 


 OULE DIRECTORIUM CHORI ANGLI- 


 ( ORDER HOLY COMMUNION 


 TONIC SOL - FA edition . 


 ready , 


 new grand 


 5b 


 BRREE 


 579 


 ARR CHURCH ENGLAND PSALMODY : 


 NATURAL PRINCIPLES HARMONY , 


 SIXTEENTH edition . 


 collegiate school 


 sight singe manual 


 ( companion work 


 COLLEGIATE VOCAL TUTOR 


 TENTH edition . 


 MR . VERNON RIGBY new song 


 CHORAL SOCIETIES 


 ARK ! " ti breeze TWILIGHT 


 opinion press 


 - song DR . MONK 


 NEW SONG , SUNG MR . SIMS REEVES 


 TIS know , " 


 NOVELLO , E EWER ‘ CO . complete 


 UNIFORM edition 


 580 


 song , 


 JOHN OXENFORD . 


 NOVELLO , EWER & CO . collection 


 english german word 


 NOVELLO 


 original OCTAVO edition 


 OPERASPrice 2 . 6d . ; handsomely bind scarlet cloth , gilt 
 edge , 4s 

 READY . 
 BEETHOVEN FIDELIO 

 german english word . ) 
 great overture usually perform ; 
 Pianoforte Score publish agree original 
 score note sign phrasing 




 AUBER FRA DIAVOLO , 


 MOZART DON GIOVANNI , 


 BELLINI NORMA , 


 VERDI IL TROVATORE 


 DONIZETTI LUCIA DI LAMMERMOOR 


 WEBER OBERON 


 ROSSINIT ’ IL BARBIERE , 


 DONIZETTI LUCREZIA BORGIA , 


 MOZART LE NOZZE DI FIGARO 


 VERDI RIGOLETTO , 


 BELLINI LA SONNAMBULA 


 WEBER DER FREISCHUTZ . 


 WAGNER TANNHAUSER 


 AUBER MASANIELLO 


 BELLINYS PURITANL 


 WAGNER LOHENGRIN 


 VERDI LA TRAVIATA . 


 DONIZETTI LA FIGLIA DEL REGGIMENTO . 


 SIR HENRY R. BISHOP 


 LONDON NEW YORK 


 NOVELLO 


 collection favourite glee 


 english composer 


 


 accompaniment ( AD LIB . ) pianoforte 


 revise 


 J. BARNBY 


 glee PENNY . 


 NOVELLO , EWER CO 


 S.S.B 


 HYMNARY , { 3 


 BOOK CHURCH SONG , 


 edit 


 REV . WILLIAM COOKE , 


 HON . CANON CHESTER } ; 


 REV . BENJAMIN WEBB , 


 VICAR S. ANDREW , WELLS STREET . 


 LONDON NEW YORK : NOVELLO EWER CO . 


 301 


 5 t 


 edit 40 . 


 LONDON : NOVELLO , EWER & CO . , 1 , BERNERS STREET ( W. ) , 35 , POULTRY ( E.C. ) . 85 0 


 J. BARNBY . 


 SIR JULIUS BENEDICT . 


 W. T. BEST . 


 OSKAR BOLCK 


 J. BAPTISTE CALKIN . 


 SIR JOHN GOSS 


 CH . GOUNOD 


 E. J. HOPKINS 


 HENRY LESLIE . 


 G. A. MACFARREN . 


 REV . SIR FRED . A. G. OUSELEY . ; 


 WILLIAM REA . 


 HENRY SMART . 


 ARTHUR 8S. SULLIVAN . 


 E. H. THORNE . 


 BERTHOLD TOURS . 


 JAMES TURLE 


 CHAPPEL ] & CO 


 ALEXANDRE NEW - GUINEA ORGAN HARMONIUM , 


 SOLID OAK CASE , octave , foot - board 


 CHAPPELL & CO . , 50 , NEW BOND street 


 new work singing class 


 CHAPPELL PENNY operatic - song 


 new work ORGAN . 


 HANDEL CHORUSES 


 CHAPPELL & CO . , 50 , NEW BOND STREET , LONDON