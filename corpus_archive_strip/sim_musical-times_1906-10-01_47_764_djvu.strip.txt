


THE MUSICAL TIMES


FOUNDED IN 1844


PUBLISHED ON THE FIRST OF EVERY MONTH


ROYAL ALBERT HALL


FIRST CONCERT. THURSDAY. NOVEMBER | THE ROYAL COLLEGE OF MUSIC


ROYAL ACADEMY OF MUSIC


650 THE MUSICAL 


SOUTHPORI 


MUSICAL FESTIVAL, 


OCTOBER 24, 25, AND 26, 1906. 


MRS. HENRY J. WOOD. MADAME KIRKBY LUNN 


MISS ALICE LAKIN 


MR. JOHN COATES MR. EYNON MORGAN, 


MR. FFRANGCON-DAVIES. 


MR. FREDERIC AUSTIN MR. JOSEPH LYCETT. 


SIR EDWARD ELGAR, M D LL... 


LONDON SYMPHONY ORCHESTRA 


WIND 


S THREE CAVALIER SONGS 


( I NEW SYMPHONK 


VARIATIONS 


I \ THE DREAM OF GERONTIUS 


ROYAL 


MANCHESTER COLLEGE


NEW 


STANLEY WITHERS


UNIVERSITY OF


DURHAM


UNIVERSITY COLLEGE


READING


DEPARTMENT OF MUSIC. 


NATIONAL SOCIETY OF MU SICIANS


BIRMINGHAM


SCHOOL OF MUSIC


AND MIDLAND INSTIT


UTI


SESSION 


1906-1907. 


VICTORIA COLLEGE OF


LONDON


INCORPORATED 1891


MUSIC


INCORPORATED GUILD OF


BUS I ANS


CHURCH


ASSOCIATE (A.1.G.C.M.), LICENTIATE (L.1.G.C.M.), FEI 


NATIONAL CONSERVATOIRE 


LOCAL MUSICAL EXAMINATIONS 


CORRESPONDENCE SCHOOL OF MU 


LONDON. 


GRADUATED POSTAL LESSONS IN HARMONY 


COUNTERPOINT, THEORY OF MUSIC, FORM 


SIC


AND


4.R.C.0


NATIONAL COLLEGE OF MUSIC


A.N.C. M., L. M., F.N.C.M


CHURC ‘H ORGANISTS’ SOCIETY


EXA}4


M. MA


N E


IBBS & TILLETT PROFESSIONAL NOTICES. 


MUSICAL AND CONCERT AGENTS


MADAME CLARA BUTT. {ANGE OF ADDRESS. 


SYSTEM ALEXANDER GUNNER 


SOLOISTS AND DUETTISTS. 


O H


MISS AMY SARGENT MR. WILLIAM WILD 


(TENOR VOCALIS T). 


IT


MR. GE JORGE STUBBS MR. ARTHUR W. ALENN


MR. JAMES COLEMAN


MRO ARTHUR SEDGLEY


; POIN FUGUE, FORM ANALYSIS, ACOUSTICS 


PERFECT TOUCH AND TONE. NE OF A, TT “LESSONS. P. 


LONDON COLLEGE OF MUSIC


GREAT MARLBOROUGH STREET, LONDON, W


FOR MUSICAL EDUCATION AND EXAMINATIONS. 


HIGHER EXAMINATIONS, 1906. 


DIPLOMAS IN PRACTICAL MUSIC


LICENTIATES (L.L.C.M


ASSOCIATES (A.L.C.M 


TEA! 


COACH 


SUCCE: 


SEVEN 


COUNT 


R.C.O


ALI 


L.R.. 


HARMO


MX 


F.R.CO 


AR.C.0


MI


THE MUSICAL


LONDON COLLEGE OF MUSIC


TEACHER’S DIPLOMA. 


DIPLOMAS IN THEORETICAL MUSIC


4.R.A.M., F.R.C.O.), PIANOFORTE, HARMONY


LONDON


BOSWORTH EDITION. 


' S. COLERIDGE- TAYLOR'S


INCIDENTAL MUSIC » STEPHEN PHILLIPS’S DRAMA


TO BE PRODUCED AT THE NERO


FIRST ENTR’ACTE (** NERO”) 


EASTE RN = ANCE


THE MUSICAL TIME


S.—OCTOBER I, 1906. 6


OCTOBER 1, 1906


USICIANS IN THE NATIONAL 


PORTRAIT GALLERY


STANHOPE


MADAME VESTRIS. 


CHARLES 


1797-1856


DRAWING BY


MRS, JAMES


ER


OLOU! 


AL


MATTHEWS


RED E. CHALOD


THE MUSICAL TIM


6055


15


JOHN PYKE HULLAH, LL.D. 


1812-1884. 


PEN( DRAW BY SIR W B. RICHMOND, K.C.B., 


THE REV. JOHN CURWEN. 


1816-1880


95


659


SIR ARTHUR SEYMOL 


1842- 


M.V.O


IR SULLIVAN


1890


1595


660 THE


MUSICAL TIMES


OCTOBER I, 1906


THE PAGE. 


THE BRIGAND. 


WILLIAM SHIELD. 


1748-1829. 


THE MUSICAL


1906. O61


SIR HENRY


ROWLE\ 


1786-1855


BISHOP


CATHERINE (KITTY) STEPHENS


ERW NTI I


1794-1882


AINTED BY


662


LADY VIOLINISTS


FOR


THE


MUSICAL


MADAME MARA


SCHMELING


664 THE MUSICAL


FOR THE BENEFIT OF MRS. CHAZAL


MADAME G


UTHEROT


2 === SSS ES SS SS SS SS


1906. 667


GIUSEPPE TARTINI


668


17¢ 


THE MUSICAL TIMES


FORGOTTEN CONCERT ROOM. 


THE MUSICAL


TIMES


OCTOBER 1, 1906. 671


CONDITIONS


BERTHA HARRISON


THE MUSICAL


TIMES


074with Herr Arthur Nikisch and Herr Michael Balling 
as conductors. In addition to such rarely heard 
operas (in England) as ‘Der Freischiitz,’ ‘ Fidelio’ 
and ‘ Die verkaufte Braut’ (Smetana), performances 
will be given of Wagner’s ‘ Der fliegende Hollander, 
*‘Tannhiauser,’ ‘Lohengrin,’ ‘Die Walkire,’ ‘ Tristan 
und Isolde’ and ‘Die Meistersinger.’ Mr. Dercy 
Pitt will be associated with Mr. Carl Ambruster in the 
direction of the chorus, and the London Symphony 
Orchestra is being negotiated with to perform at all 
the representations

The Beethoven literature is to be enriched by two 
publications of the composer’s letters—a critical 
edition, in four volumes, of ‘ Beethoven’s Briefe und 
Tagebuchblatter’ (edited by Dr. Fritz Prelinger, of 
Vienna), and, for the first time, a complete edition of 
the letters, of which Dr. A. C. Kalischer has under- 
taken the editorship

Handel’s imperfect knowledge of the English 
language is amusingly illustrated by the autograph 
score in the British Museum of his Chandos anthem 
six parts) ‘As pants the hart,’ wherein, with his usual 
mighty pen-strokes, he writes, ‘As fazzts the hart.’ 
That this was not a slip of the pen is shown by his 
having written ‘paints’ throughout the movement 
thirteen times in all

The percentage of those publications 
which paid even the cost of production would be 
even more interesting than the foregoing figures

The manuscript of Beethoven’s ‘Waldstein’ sonata, 
| consisting of thirty-two oblong folio

pages, 1s at




HEREFORD MUSICAL FESTIVAI


AURELIA


THE MUSICAL


TIMES


OCTOBER 1, 1906. 677


SELECTION OF 


PSALMS AND HYMNS


CHURCH OF ENGLAND


REV. CHARLES KEMBLE, M.A., 


THE MUSIC


SELECTED, ARRANGED, AND PARTLY C SEI 


NDON


OHN F. SHAW & CO., 48. PATERNOSTER ROW


S. S. WESLEY. 


675


WV


I, 1906


MR. A. R. REINAGLE AND HIS SALARY


A. R. REINAGLE, 


ORGAN Grand Ul

symphony of Beethoven and the Te Deum of Berlioz 
bear 
typical creations of their respective composers, and 
both owe their conception to the first Napoleon. 
Moreover, 
from that which originally brought them into existence. 
Always an artist of a 
extravagant ideas, Berlioz conceived the thought of| A few days later Berlioz wrote, also to Liszt 
glorifying the military renown of Napoleon I. by the | 
composition of a grand work in which the epic and | 
dramatic should be interwoven, entitled ‘The return

was




THE


MUSICAL TIMES


OCTOBER 1, 1906


ORGAN RECITALS


ORGANIST, CHOIRMASTER AND CHOIR APPOINTMENTS


STRINGED INSTRU MENTS


FIRST CHOIR. SECOND CHOIR. PHIRD CHOIR. 


1883 : 


00


THE


681


1855


682 THE


MUSICAL TIMES


THE MUSICAL


I, 1906. 683


NE\ PART-SONGS


BROOKS RECEIVI


N REINAGLE, 


O. G. SONNECI 


PETER CORNELI


TO MY FRIEND, BASIL JOHNSON


FOUR-PART SONG


+ 5 PP —— _— —— ‘ 


: = = “ —AT 


. : = ~— J ) _—=—, 3 \ — \, { 


—_? — = . F 


A SUNG OF AUTUMN


= — - —_ ‘4 : 


‘ ————_— — P 


—————$___ _ 


— _ ~ =—_


2 1 . 


}_ + @: 6 } = = = | 


2 7 = 4


A SONG OF AUTUMN


' SS 


HEREFORD MUSICAL FESTIVAL. 


(BY OUR SPECIAL CORRESPONDENT


THE


THE MUSICAL TIMES


OCTOBER 1, 1906. 689credit on Dr. Sinclair, the very able conductor of the festival

and the ‘ Introduction and Allegro’ for strings was brilliantly 
played under the composer’s own direction. Dr. Sinclair 
also deserves credit for a finished performance of Brahms’s 
third Symphony, the only symphony—if Dr. Davies’s work 
be excepted—heard at the festival. ‘ Messiah,’ ‘ Elijah,’ and 
‘ The Hymn of Praise,’ are standing dishes at these festivals, 
and occupied their accustomed places. At the chamber 
concerts the Nora Clench (uartet played quartets by 
Beethoven (Op. 135) and Tanéiew (Op. 7), and at the 
orchestral concert Miss: Evangeline Anthony, a Hereford 
lady, played one of Mozart’s violin concertos. There 
was a lavish supply of principal vocalists; in addition 
to those already mentioned must be recalled the names—in 
alphabetical order—of Madame Conly, Miss Gleeson-White, 
Madame Agnes Nicholls, and Madame Siviter; Madame 
Ada Crossley, Miss Muriel Foster, and Miss Gwladys 
Roberts ; Messrs. Beaumont, Ben Davies, and William 
Green; Messrs. Dalton Baker, Ffrangcon-Davies, and 
Watkin Mills. There was a first-rate orchestra of over 
seventy London players (Mr. W. Frye Parker, leader), 
and the organ was taken by Mr. Atkins at the morning 
performances and Dr. Brewer in the evenings, the latter also 
assuming the duties of accompanist at the chamber concert

COMPETITION FESTIVALS




MORPETH (NORTHUMBERLAND). 


PROMENADE CONCERTS


MUSIC IN BRISTOL. 


(FROM OUR OWN CORRESPONDENT


MUSIC IN GLASGOW. 


(FROM OUR OWN CORRESPONDENTMUSIC IN LIVERPOOL AND DISTRICT. 
(FROM OUR OWN CORRESPONDENT

It is impossible to forecast the season’s music in Liverpool 
without a keen sense of satisfaction. The Philharmonic 
Society will, as usual, give twelve concerts. Among the 
works to be performed are Beethoven’s Choral symphony, 
Parry’s ‘ Pied Piper of Ilamelin,’ the ‘Creation’ (Parts 1 
and 2), Bach’s cantata ‘Phoebus and Pan,’ the Finale to 
* Die Meistersinger,’ and Verdi's ‘ Requiem.’ There will be 
also two special orchestral concerts. Dr. Cowen still 
remains conductor

The Ilallé Concert Society announces four concerts, 
conducted by Dr. Richter. As it is two seasons since 
Dr. Richter and his orchestra played in Liverpool, it goes 
without saying that the announcement has caused general 
interest

Franck’s ‘Symphonic Variations’ for pianoforte, Granville 
Bantock’s ‘Sappho’ prelude, Gluck’s overture ‘ Iphigenia 
in Aulis,’ Siebelius’s second symphony, and _ incidental 
music to ‘Pelleas and Melisande,’ and Paul Dukas’ 
‘ L’apprenti Sorcier,’ the last three being given for the first 
time in Liverpool. Mr. Henry J. Wood will conduct a 
Brahms symphony towards the close of the season

The Symphony Orchestra, which is composed chiefly of 
members of the Richter and Philharmonic Orchestras, 
warmly encouraged by its success last season, intends to give 
twelve concerts in the Sun IlIall, under the direction of Mr. 
Vasco Akeroyd. The symphonies to be performed include 
Tchaikovsky’s No. 4, Beethoven’s C minor, Cowen’s 
* Scandinavian,’ Schubert's ‘ Unfinished,’ and Beethoven’s 
* Eroica

Mr. A. P. Mignot has arranged four Schiever Quartet 
concerts, which will be held at the Hlardman Street Kooms 
All serious music-lovers in this district will welcome this 
announcement and the opportunity it affords of wishing the 
distinguished enterprise a season of pronounced success




MUSIC IN MANCHESTER. 


(FROM OUR OWN CORRESPONDENT


692


MUSIC IN NEWCASTLE AND DISTRICT. 


RRI INDENT


MUSIC IN


FROM


NOTTINGHAM AND DISTRICT


OUR OWN CORRESPONDENTThe Nottingham Sacred Harmonic Society, under the 
direction of Mr. Allen Gill, has selected Wagner's 
‘Flying Dutchman’ for its first concert, to be followed 
by Handel’s ‘ Acis and Galatea’ and Gounod’s ‘ Redemption

The City Orchestral Concerts will give their usual two 
performances, with Beethoven’s fifth and Tchaikovsky’s 
Pathetic symphonies as the chief features

A Choral Union is being promoted by the City Education 
Committee for the students of the evening schools, under 
the conductorship of Mr. Arthur Richards, and Gade’s 
‘Erl King’s daughter ’ has been selected for performance




MUSIC IN SHEFFIELD AND DISTRICT. 


(FROM OUR OWN CORRESPONDENT


THE


MUSICAL TIMES


OCTOBER 1, 1906The choral and orchestral societies have resumed their 
activities—mainly confined to rehearsals at present—and we 
are promised an interesting season. The most important 
event will be the performance, for the first time in the city, 
of Elgar's ‘The Apostles,’ to be given by the Sheffield 
Amateur Musical Society. The concert is fixed for 
December 18, and Mr. Henry J. Wood will conduct

The Sheffield Musical Union promises Berlioz’s ‘ Faust,’ 
of which Dr. Coward may be relied upon to give a 
picturesque performance, and ‘Israel in Egypt,’ a work well 
suited to this powerful chorus. An engagement to sing in 
Beethoven’s Choral Symphony at (ueen’s Hall, under 
Dr. Richter, is also among the Union’s fixtures

The Sheffield Choral Union is unfortunately in difficulties. 
\ series of excellent concerts having resulted in heavy 
inancial losses, the Society will at present be continued as a 
vocal class for the rehearsal only of choral works. Bach’s 
‘St. Matthew’ Passion will be the first work studied




BERLIN


COLOUNE


ELBERFELD


HAMBURG


LEIPZIG. 


PARIS. 


PESARO


MILAN


093


604 THE MUSICALmay, or may not, have been aware of

a analysed by 
Grove in his ‘ Beethoven nd is nine 
Novello). An

art’s Symphony in C appeared in THE M Al

transposed

ee (1) A portrait of Beethoven appeared as a 
supplement to THE M 1 Times of January, 1901. 
2) As the new edition of Grove’s ‘ Dictionary of music and 
the rate of one volume per annum, 
ably be completed by the end of 1908

musicians’ is issued 
the work will pr




CONTENTS


SPECIAL NOTICE


DURING THE L. {ST ‘MONTH. 


CH, J


LIMITED


C \VI NDISH, J. 


E"¢ AR, 


OODE RHAM, ALICE


FA PUBLICATIONS


MR


ERNEST PIKE


TENOR) 


695


HOIRMASTE R


A SSISTA ANT


CHOIRMASTER


W ANTED


THE SCHOOL


ANNUAL INCLUDING 


CONTAINS


( ENTION M TEACH


PHEORY QUESTIONS


NTH


FOR OCTOBER


MUSIC REVIEW


THE ‘SCHOOL MUSIC REVIEW


ILLOWING MU


VELLO AND COMPANY


THE MUSICAL


SCALE OF


TIMES


° 30


3 4 


200 


710


TERMS FOR FOR ADV ERTISEMENTS


6 THE MUSICAL


TIMES


OCTOBER 1, 1906


~“HOIR EXCHANGE, 136, FIF TH AVE., NEW 


WANTEI 


N E.¢ 


S I \ 


FOR SALE. VIRGIL PRACTICE CLAVIER 


WW


PIANO, AND


TO PROFESSORS OF SINGING, 


VIOLIN


LARGE TEACHING ROOM


* UITABLE FOR PRESENTATION. —SILVEI 


TWE


D’AL


HARM 


ANAL 


COUN 


DOU B


THE MUSICAL TIM


THE OLD FIRM


P. CONACHER & CO.,, | 


SPRINGWOOD WORKS 


HUDDERSFIELD


TWO GOLD MEDALS. 


NICHOLSON AND CO., 


ORGAN BUILDERS, 


PALACE YARD, WORCESTER. 


(ESTABLISHED 1841


PIANO PEDALS


EVERY ORGANIST


PIANO 


ALMAINE


PIANOS AND


ORGANS


EXTREMELY SUITABLE 


SEASON


FOR PERFORMANCE AT THE 


OF CHRISTMAS


A SACRED CANTATA


FOR FOUR SOLO VOICES, CHORUS, 


ORCHESTRA 


THE WORDS SELECTED FROM HOLY SCRIPTURE 


AND THE MUSIC COMPOSED BY


ALFRED GAUL


AND


FBENEZER PROUT’S WORKS. 


FUGUE. 


: LOUIS SPOHR 


THE ORIANA


COLLECTION OF EARLY MADRIGALS 


BRITISH AND FOREIGN


7. THE NYMPHS AND SHEPHERDS DANCED ( 4, )... GEORGE MARSON 


36. O FLY NOT, LOVE (5 5,  )... THOMAS BATESON 


ED 


10. L 


12. 


13. 


14.* A 


15 


16. 


17 V 


18. 


19. 


20. 


21. 


22. 


23. 


24. F


28. SI 


29. 


30. 


THE MUSICAL TIMES


PROGRESSIVE


FOR THE


PIANOFORTE


EDITED, ARRANGED IN GROUPS, AND THE FINGERING REVISED AND SUPPLEMENTED


FRANKLIN


TAYLOR


2}. ” » 7} §2. » 2 


FIFTY-SIX BOOKS, PRICE ONE SHILLING EACH


IMT THE A


BO'V


SELECTED PIANOFORTE STUDIES


PROGRESSIVELY A


FRANKLIN TAYLOR


IN TWO SETS (EIGHT BOOKS), PRICE ONE SHILLING AND SIXPENCE EACH BOOK


COMPANY, LIMITED


BY


SECTION A.—TECHNICAL PRACTICE. IN SIX BOOKS


SECTION B.—STUDIES. IN SIX BOOKS


PIECES


5. OG


THE MUSICAL TIMES


OCTOBER 1, 1906


NTHEMS FOR TRINITYTIDE


I AM ALPHA AND OMEGA


COMPOSED BY 


CH. GOUNOD


LIGHT OF THE WORLD 


COMPOSED BY 


EDWARD ELGAR


COME, YE CHILDREN 


COMPOSED BY 


JOSIAH BOOTH. 


COMPLETE LIST. 


THE LUTE SERIES


ANTHE


LONDON NOVELLO AND COMP


MS FOR ADVENT


702 THE MUSICAL 


CENTURY COMPOSERS


BY 16TH, 17TH, AND ISTH


ANTHEMS


BATESON, TIIOMAS


BATTEN, ADRIAN ( /. 163 


BECKWITH, JOHN CHRISTMAS (1750—1809). 


BLAKE, EDWARD (1708—1765) 


BLOW, JOHN (1648—1708). 


BOYCE, WILLIAM (171 1779) 


BYRD, WILLIAM (154 1623 


CHILD, WILLIAM (1606 ?—1697 


CLARKE, JEREMIAH (1669 ?—1707). 


CREYGHTON, ROBERT (16392?—1734 


P I O 


CROFT, WILLIAM (1677?2—1727). 


DOWLAND, JOHN (1563 162 


EBDON, THOMAS (1738—1811 


FARRANT, RICHARD (/. 1564—1580). 


GIBBONS, ORL ANDO (1583—1625


G REE NE, MAI R 1 ; (1696 ?—1755


HAYES, PHILIP (1738—1797). 


HAYES, WIL L [AM (1706—1777). 


HUMPHRYS, PELHAM (1647—1674). 


KENT, JAMES (1700—1776


KING, CHARLES (1687—1743). 


LOCKE, MATTHEW (1630?—1677 


NARES, JAMES (1715—1783 


PURCELL, HENRY (1658 ?—1695). 


REDFORD, JOHN (/. 1535). 


ROGERS, BENJAMIN (1614—1698). 


TYE, CHRISTOPHER (1497?—1572


WISE, MICHAEL (1646?—1687). 


SERVICES


OLD ENGLISH ORG AN MUSIC


PREFATORY NOTE


LONDON : LIMITED


NOVELLO AND COMPANY


MUSICAL


704 THE


TIME


CHURCH MUSIC 


SERVICES


ANTHEMS. 


THE 


MORNING AND EVENING 


SERVICE


GETHER W H HE 


OFFICE FOR THE HOLY COMMUNION 


SET TO MUSIC IN THE KEY OF G MINOR


BY


NATHANIEL PATRICK


TRANSPOSED EDITION


CHURCH MUSIC


NEW ANTHEMS BY 


ERNEST EDWIN MITCHELL. 


THE HEAV ENL Y VISION


TO CHORAL AND ORCHESTRAL SOCIETIES. 


EVERAL CHORAL WORKS, 


PART-SONGS


COMPOSITIONS


T. MEE PATTISON


ORIGINAL ORGAN COMPOSITIONS. 


A PRACTICAL METHOD OF 


| TRAINING CHORISTERS


RY


DR. VARLEY ROBERTS


THE < CANTICLES


SET TO ANGLICAN CHANTS


COMPOSED BY 


FREDERICK ILIFFE. 


FI E 


IN 1 


PIL( 


SOR 


THE


THE 


THE 


TRO


THE


1906


PART-SONGS


PETER CORNELIUS


WITH ENGLISH WORDS. 


MALE VOICES. 


MIXED VOICES. 


; SOTTS 


NARCISSUS AND ECHO 


CANTATA FOR CHORUS, SOLI, AND ORCHESTRA 


COMPOSED BY 


EDWIN C. SUCH. 


, BC IOKS THAT SHOULD BE 


MALLINSON RANDALL. 


OF THE CHURCH 


G. EDWARD STUBBS. 


PRACTICAL HINTS ON THE TRAINING OF 


CHOIR BOYS . 3 


HOW TO SING THE CHORAL SERVICE. A 


: F. E. HOWARD. 


W. H. HALL. 


HYMN TO DIONYSUS


THE WORDS FROM THE 


BACCH.E OF EURIPIDES 


TRANSLATION BY 


GILBERT MURRAY 


FOR CHORUS AND ORCHESTRA 


BY 


ERNEST WALKER 


SECOND EDITION


THE HIS TORY 


MENDELSSOHN’S ORATORIO


ELIJAH


F. G. EDWARDS. 


C.B. 


956


ONE


JUST PUBLISHED


BIBLE VERSION


CANTICLES


POINTED FOR CHANTING


THE


EXTRACTS FROM THE PREFACE. 


PRESS NOTICES


LONDON: NOVELLO AND COMPANY, LIMITED


HUNDRED PSALMS


CED AT THE HEREFORD MUSICAL FESTIVAL, 1906. | PRODUCED AT THE HEREFORD MUSICAL FESTIVAL, 1906, 


LIFT UP YOUR HEARTS — 


CRED SYMPMONY, IN F THREE ELIZABETHAN 


: -ASTORALS | 


COMPOSED BY AN IDYLI ; 


(O} 20. ) 


COMPOSED IY 


FHE YORKSHIRE POS 


YORKSHIRE POST. 


: (GLOUCESTER JOURNAL. 


PRODUCED AT THE PHILHARMONIC SOCIETY’S CONCERT, JUNE 14, 1906


SYMPHONIC VARIATIONS


ON AN AFRICAN AIR


BY


S. COLERIDGE-TAYLOR


THE TIMES. MORNING POST. 


1906


PRODUCED AT THE HEREFORD MUSICAL


SOULS


A PSALM OF


THE


FESTIVAL, 1906


RANSOM


THE POOR


SINFONIA SACRA) 


FOR


SOPRANO AND BASS SOLI


CHORUS AND ORCHESTRA


COMPOSED BY


C. HUBERT


PRICE


TWO


THE TIMES


DAILY TELEGRAPH 


DAILY NEWS. 


H. PARRY


SHILLINGS


DAILY CHRONICLE


THE TRIBUNI


GLOBE


THE ATHEN-€UM


THE YORKSHIRE POST


AND COMPANY


LIMITED


INTRODUCTION


ALLEGRO OLD ENGLISH DANCES 


FOR STRINGS FREDERIC H. COWEN


QUARTET AND ORCHESTRA


MAYPOLE DANCE 3; MINUET D’AMOUI 


PEASANTS’ DANCE. 4. OLD DANCE


MINI


THE TIMES


DAILY TELEGRAPH. 


DAILY NEWS 


EVENING STANDARD. 


DAILY ¢ RONICLI 4 


YORKSHIRE POST SUNDAY TIMES. 


WESTERN DAILY PRESS


GLOBI . . 


PA MALL GAZETTI GLASGOW HERALD 


ASGOW NEWS


NOV


THE MUSICAL


__ CHURCH CANTATA. NEW ORGAN COMPOSITION 


SEVEN PIECES


NOVELLO’S MUSIC PRIMERS AND EDUCATIONAL SERIES 


= DR. 


BREATHING


VOICE P RODU CTION


PART I.—EXPLANATORY 


THE LATERAL COSTAL METHOD OF BREATHING 


SPECIAL PHYSICAL EXERCISES FOR THE ATTAINMENT | CHOIRS OF 


OF A CORRECT POSE. \ 


INTRODUCTION, 


FOR


ADES


LOV


ON THE


TE


BY


FI


THE


711


DELES” 


ORGAN


OXON


ARTHU R. S


AP


2, WEDDING MARCH | 5. MARCHE TRIOMPHALI (MUS.D., 


HOLLOWAY


PEAL


INTE -RMEZZO


FOR


EDWIN


TI


IMPOSE


IN


TERY POPULAR 


IRGAN


BY


EMARE


PIANOFORTE PIECE


HOLLOWAY


EASY ANTHEMS, 


D MINOR


SS


(NOVELLO’S EDITIONS


PIANOFORTE. — ORGAN


LOCAL CENTRE.—INTERMEDIATE GRADE. LOCAL CENTRE.—INTERMEDIATE GRADI 


SINGING. 


LOCAL CENTRE DVANCED GRADI LOCAL CENTRE.—INTERMEDIATE GRADE


: BARITONE. 


10 


. , BAS 


SCHOOL EXAMINATIONS.—LOWER DIVISION MEZZO-SOPRANO. 


REINE I ¢ OF; N BARITONE


THE


SCHOOL EXAMIN 


HE EI


PANSE N. 42 


SCHOOL EXAMINAT 


MEZZ


6 ASS. 


CLASS SINGING EX AMIN: ATION. 


VIOLIN 


LOCAL CENTRE.—INTERMEDIATE GRADE. 


. LOCAL CENTRE.—ADVANCED GRADE. 


; SCHOOL EXAMINATIONS.—PRIMARY. 


SCHOOL EXAMINATIONS—ELEMENTARY. 


SCHOOL EXAMINATIONS.—LOWER DIVISION. 


SCHOOL EXAMINATIONS. HIGHE R DIVISION. 


LOCAL CENTRE.—INTERMEDIATE GRADE. 


LOCAL CENTRE.—ADVANCED GRADE. 


1907


SCHOOL EXAMINATIONS.—PRIMARY. 


— -RANO. 


ATIONS.—ELEMENTARY


SOPRANO


CONTRALTO


TENOR. 


BARITONE. 


BASS. 


IONS.—HIGHER DIVISION. 


O-SOPRANO


CONTRALTO


TENOR. 


BARITONE


NOVELLO


THE BALLAD 


| OF MESHULLEMETH 


THE ORATOR 10


C. HUBERT H


JUDITH? 


PARRY


BE THOU FAITHFUL U 


DEATH


ARIA FROM “ST. 


COMPOSED BY


MENDELSSOHN


NTO


PAUL


ARRANGED WITH VIOLONCELLO OBBLIG 


J. W. SLATTER


ATO BY


FAR FROM MY HEAVENLY 


HOME


SACRED SONG 


THE WORDS WRITTEN BY 


HENRY FRANCIS LYTE 


THE MUSIC COMPOSED BY


J. MARGETSON


CHE FARO SENZA 


EURIDICE


THOU ART GONE, ALAS, FOR EVER") 


FROM THE OPERA 


ORPHEUS


OSED BY


TRANSPOSED EDITION 


BREITKOPF AND HARTEL, 


GREAT MARLBOROUGH STREET, W


CLARISSE MALLARD’S 


SONG-CYCLE FOR THE 


CHILDREN 


SONGS, DANCES, AND TABLEAUN VIVANTS. VERY 


SUITABLE FOR SCHOOL ENTERTAINMENTS, I


714 THE MUSIC


ANDANTE IN


FOR THE ORGAN


ENRY SMART


ANDANTE ESPRESSIVO


SYMPHONY IN


ARTHUR SULLIVAN


NGEMENT FOR PIANOFORTI 


WILFRED BENDALL


ALLEGRETTO


SYMPHONY IN E


RTHUI SULLIN AN


KRANGEMENT FOR PIANOFORTI 


WILFRED BENDALL


THE SEASONS


SYMPHONIC SUITI


EDWARD GERMAN


THE SANDS OF


BALLAD


CHORUS AND ORCHESTRA 


CHARLES KINGSLEY


CHARLES A. E. HARRISS


NEW CONCERT SONGS


BY


DR. ARTHUR S. HOLLOWAY


ORGAN > 


ARRANGEMENTS


EDITED BY


JOHN E. WEST


MACKENZIE


SCHU MANN


CESAR CUI


SCHUMANN 


SCHUBERT PRELUDE ro PART IIL. (Tut Avostuts

EDW ARD ELGAR 
Arranged by G. R. Si Al I 
BEETHOVEN 
Arranged by . 
ADORAMUS TI HUGH BLAIR

Arranged by Hucu Bian 
INTERMEZZO (“ Tue B . A RISTOPHANES




ORGAN 


TRANSCRIPTIONS


GEORGE SL BENNETToF JuBAL”).. < A.C. MACKENZIE 1 
. PRELUDE.—(“ Lonencrin”) .. . WAGNER 1 
ANDANTINO.—Sympeuony, No. 4, in F minor) 
rSCHAIKOWSKY 2 
. SLOW MOVEMENT. - \NOFORTE CONCERTO IN B FLAT 
MINOR) - TSCHAIKOWSKY 1 6 
. CORONATION MARCH TSCHAIKOWSKY 2 
. THREE MINUETS.—Sympnuonies tn C, G MINOR, AND 
E FLAT) . . MOZART 2 
. MINUET.—{Sonata in E Fiat). (Op. 31, iii

BEETHOVEN 1 
C. MACKENZIE 
FINAL . - MAY WE <CE AGAIN ")}—“‘ BLest Pair oF

SIREN , ° C. H. H. PARRY 1




MENDELSSOHN 


IMPRO) IPTU IN A 


O RC “A N


COMPOSED BY 


EDWIN H. LEMARE. 


CHR\ 


CHRIS


LET 


HOSA


NOW


SONGS 


EIGHT


METZL


LIGHT


GODARD


METZLER'S S CHRISTM, \S MUSIC


LET S NOW GO EVEN UNTO BETH- SULLIVAN 14 } 


METZLERS BOUND VOLUMES


SUITABLE FOR CHRISTMAS PRESENTS AND S¢ ‘HOOL PRIZES


METZLER’S LATEST NOVELTIES 


SELECTION OF SACRED SONGS


ARRANGED FOR THE PIANOFORTE BY H. M. HIGGS


NEW COMPOSITIONS FOR THE ORGAN 


VOW READY. 


METZLER & CO., LIMITED, 


40 TO 43, GREAT MARLBOROUGH STREET, LONDON, W


CHAPPELL & CO.S NEW PUBLICATIONS, — | 


JUST PUBLISHED. 


SONG WITHOUT WORDS 


- EDWARD GERMAN


PRICE TWO SHILLINGS NET EACH


MAUDE VALERIE WHITE. EDWARD GERMAN. 


‘IN GOLDEN JUNE “WHEN MAIDENS GO A-MAYING. 


‘IN THE SUMMER GARDEN “LOVE IS MEANT TO MAKE US GLAD. 


GUY D’HARDELOT. FRANCO LEONI. 


“ FOR YOU ALONE. “THE MERRY MAIDEN.” 


‘I THINK ‘A BUTTERFLY SONG." 


‘MY HEART WILL KNOW “WHE N HE COMES HOME. 


TERESA DEL RIEGO. HERMANN LOHR. 


BROWN EYES. ‘OH, TO FORGET.’ 


O LOVING FATHER * ALONE


THE SONGS OF SUMMER “THE HUNT'S UP.” 


1 “A CHAIN OF ROSES.” 


‘TO PHYLLIDA “REMEMBER ME


“THE BELL W. H. SQUIRE. 


“LOOK UP, O HEART. “THREE FOR J: ING. 


“OUT OF REACH ERNEST NEWTON. 


“I CLAIM YOU MINE. “THROUGH THE FOREST. 


GEORGE H. CLUTSAM. “LOVE'S ECHO." - 


. OS - 7 


THE RED ROSE TREI FRANK LAMBERT. 


“SWEET, BE NOT PROUD “BID YOU GOOD-MORROW 


» . “THE BURIED ROSE. 


WADDINGTON COOKE. “DEAR HANDS.” 


PAUL A. RUBENS NOEL JOHNSON. 


‘ THE SUMMEI R “WILD ROSES. 


HERBERT BUNNING. ROBERT CONINGSBY CLARKE. 


REVELATION “A BIRTHDAY SONG 


MY SWEETHEART “A DEDICATION 


BERNARD ROLIT “BETTY’'S WAY. 


“THE LITTLE GOLD FIREFLY) “TAKE YOUR LUTE AND SING 


THE FIRST COMPLETE UNIFORM EDITION OF THE WORLD-FAMOUS 


GILBERT AND SULLIVAN OPERAS


THE MIKADO. THE PIRATES OF PENZANCE


THE GONDOLIERS. H.M.S. PINAFORE


THE YEOMEN OF THE GUARD. RUDDIGORE


PATIENCE. UTOPIA, LIMITED


IOLANTHE THE GRAND DUKE


FOR £ 3 CASH


PROSPECTUS WILL BE SENT POST-FREE UPON APPLICATION TO


AND MAY BE HAD OF ALL MUSICSELLERS


SEVE


NOVELLO


PART-SONGS


SECOND SERIES. 


HENRY SMART


SEVEN SHAKSPERE SONGS BY 


G. A. MACFARREN


J. L. HATTON 


HENRY LESLIE. 


EXTRA SUPPLEMENT


S PART-SONG BOOK


A COLLECTION OF 


GLEES, AND 


EACH


SERIES


OR IN


FIRST


21 


SIX MADRIGALS. 


FRANCESCO BERGER. 


I. BAPTISTE CALKIN. 


J. BARNBY. 


G. A. MACF ARREN. 


MADRIGALS


SEPARATE NUMBERS


EIGHT SHAKSPERE SONGS BY


G. A. MACFARREN


HENRY LESLIE. 


HENRY SM ART. 


SAMUEL REAY. 


W. MACF ARREN


J. LEMMENS


HENRY SMART


CIRO PINSUTIL


1 T £ 


HATTON


MALE VOICES


4-3 -9- 9-9-9


2020080


- (S.S.A.T.B, ) 


(S.A.T.B.) } 


32


FRANZ ABT


A. C. MACKENZIE


E. PROUT. 


J. L. HATTON


NR OO GNIS


SOP


(SECOND SERIES.) 


COME TO ME, GENTLE SLEEP 


FOUR-PART SONG 


THE WORDS WRITTEN BY MRS. HEMANS 


THE MUSIC COMPOSED BY 


FREDERIC H. COWEN


CUME TO ME, GENTLE SLEEP


: — \,— —_—- = —9—__9—_ 9-9 _|-9—__|—_9—"_4— 


COME TO ME, GENTLE SLEEP. 


¢————— — - —— —4-———_ ——-3 


2-2 -@ 


<= FS ES —_ = == SS 


COME TO ME, GENTLE SLEEP. 


SSS —-_—-— — _ — ——— —_


T J © Z ' . “ 2


SE + 


COME TO ME, GENTLE SLEEP


53Vov. XV. (continued

Beethoven 
Naumann

Corin for Cleoradying




J. L


~2686.6. 5


PABBA ?E


1906