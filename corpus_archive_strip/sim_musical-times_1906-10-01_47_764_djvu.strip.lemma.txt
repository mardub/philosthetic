


 the MUSICAL TIMES 


 found in 1844 


 publish on the first of every month 


 ROYAL ALBERT HALL 


 first concert . THURSDAY . NOVEMBER | the ROYAL COLLEGE of MUSIC 


 ROYAL ACADEMY of MUSIC 


 650 the MUSICAL 


 SOUTHPORI 


 MUSICAL festival , 


 OCTOBER 24 , 25 , and 26 , 1906 . 


 MRS . HENRY J. WOOD . MADAME KIRKBY LUNN 


 MISS ALICE LAKIN 


 MR . JOHN COATES MR . EYNON MORGAN , 


 MR . FFRANGCON - DAVIES . 


 MR . FREDERIC AUSTIN MR . JOSEPH LYCETT . 


 SIR EDWARD ELGAR , M D LL ... 


 LONDON SYMPHONY ORCHESTRA 


 WIND 


 S three CAVALIER song 


 ( I NEW SYMPHONK 


 variation 


 I \ the DREAM of GERONTIUS 


 ROYAL 


 MANCHESTER COLLEGE 


 NEW 


 STANLEY WITHERS 


 UNIVERSITY OF 


 DURHAM 


 UNIVERSITY COLLEGE 


 READING 


 DEPARTMENT of MUSIC . 


 NATIONAL SOCIETY OF MU SICIANS 


 BIRMINGHAM 


 SCHOOL of MUSIC 


 AND MIDLAND INSTIT 


 UTI 


 SESSION 


 1906 - 1907 . 


 VICTORIA COLLEGE of 


 LONDON 


 incorporate 1891 


 MUSIC 


 INCORPORATED guild of 


 BUS I ANS 


 CHURCH 


 ASSOCIATE ( A.1.G.C.M. ) , LICENTIATE ( L.1.G.C.M. ) , FEI 


 NATIONAL CONSERVATOIRE 


 local MUSICAL examination 


 CORRESPONDENCE SCHOOL of MU 


 LONDON . 


 GRADUATED postal lesson in HARMONY 


 COUNTERPOINT , THEORY of MUSIC , FORM 


 SIC 


 AND 


 4.R.C.0 


 NATIONAL COLLEGE of MUSIC 


 A.N.C. M. , L. M. , F.N.C.M 


 CHURC ' H ORGANISTS ’ SOCIETY 


 EXA}4 


 M. MA 


 N E 


 ibb & TILLETT PROFESSIONAL notice . 


 MUSICAL and CONCERT agent 


 MADAME CLARA BUTT . { ange of address . 


 system ALEXANDER GUNNER 


 soloist and duettist . 


 O H 


 MISS AMY SARGENT MR . WILLIAM WILD 


 ( TENOR VOCALIS t ) . 


 IT 


 MR . GE JORGE STUBBS MR . ARTHUR W. ALENN 


 MR . JAMES COLEMAN 


 MRO ARTHUR SEDGLEY 


 ; POIN FUGUE , form analysis , acoustic 


 perfect touch and tone . NE of A , tt " lesson . P. 


 LONDON COLLEGE of MUSIC 


 GREAT MARLBOROUGH STREET , LONDON , W 


 FOR MUSICAL EDUCATION and EXAMINATIONS . 


 HIGHER EXAMINATIONS , 1906 . 


 DIPLOMAS IN PRACTICAL MUSIC 


 licentiate ( L.L.C.M 


 associate ( A.L.C.M 


 TEA ! 


 COACH 


 SUCCE : 


 SEVEN 


 COUNT 


 R.C.O 


 ALI 


 L.R .. 


 HARMO 


 MX 


 F.R.CO 


 AR.C.0 


 MI 


 THE MUSICAL 


 LONDON COLLEGE of MUSIC 


 TEACHER ’S DIPLOMA . 


 DIPLOMAS IN THEORETICAL MUSIC 


 4.R.A.M. , F.R.C.O. ) , PIANOFORTE , HARMONY 


 LONDON 


 BOSWORTH EDITION . 


 ' S. COLERIDGE- TAYLOR 'S 


 incidental music » STEPHEN PHILLIPS ’S drama 


 to be produce at the nero 


 first entr’acte ( * * nero " ) 


 EASTE RN = ANCE 


 the MUSICAL TIME 


 S.—OCTOBER I , 1906 . 6 


 OCTOBER 1 , 1906 


 USICIANS in the NATIONAL 


 PORTRAIT GALLERY 


 STANHOPE 


 MADAME VESTRIS . 


 CHARLES 


 1797 - 1856 


 drawing by 


 MRS , JAMES 


 ER 


 OLOU ! 


 AL 


 MATTHEWS 


 RED E. CHALOD 


 the MUSICAL TIM 


 6055 


 15 


 JOHN PYKE HULLAH , LL.D. 


 1812 - 1884 . 


 PEN ( draw by SIR W B. RICHMOND , K.C.B. , 


 the REV . JOHN CURWEN . 


 1816 - 1880 


 95 


 659 


 SIR ARTHUR SEYMOL 


 1842- 


 M.V.O 


 IR SULLIVAN 


 1890 


 1595 


 660 the 


 MUSICAL time 


 OCTOBER i , 1906 


 the PAGE . 


 the BRIGAND . 


 WILLIAM SHIELD . 


 1748 - 1829 . 


 the MUSICAL 


 1906 . o61 


 SIR HENRY 


 ROWLE\ 


 1786 - 1855 


 BISHOP 


 CATHERINE ( KITTY ) STEPHENS 


 ERW NTI I 


 1794 - 1882 


 ainte by 


 662 


 LADY VIOLINISTS 


 FOR 


 the 


 MUSICAL 


 MADAME MARA 


 SCHMELING 


 664 the MUSICAL 


 for the benefit of MRS . CHAZAL 


 MADAME G 


 UTHEROT 


 2 = = = SSS ES SS SS SS SS 


 1906 . 667 


 GIUSEPPE TARTINI 


 668 


 17¢ 


 the MUSICAL time 


 FORGOTTEN CONCERT room . 


 the MUSICAL 


 TIMES 


 OCTOBER 1 , 1906 . 671 


 condition 


 BERTHA HARRISON 


 the MUSICAL 


 TIMES 


 074with Herr Arthur Nikisch and Herr Michael Balling 
 a conductor . in addition to such rarely hear 
 opera ( in England ) as ' Der Freischiitz , ' ' Fidelio ' 
 and ' Die verkaufte Braut ' ( Smetana ) , performance 
 will be give of Wagner ’s ' der fliegende Hollander , 
 * ' Tannhiauser , ' ' Lohengrin , ' ' Die Walkire , ' ' Tristan 
 und Isolde ' and ' Die Meistersinger . ' Mr. Dercy 
 Pitt will be associate with Mr. Carl Ambruster in the 
 direction of the chorus , and the London Symphony 
 Orchestra be be negotiate with to perform at all 
 the representation 

 the Beethoven literature be to be enrich by two 
 publication of the composer ’s letter — a critical 
 edition , in four volume , of ' Beethoven ’s Briefe und 
 Tagebuchblatter ' ( edit by Dr. Fritz Prelinger , of 
 Vienna ) , and , for the first time , a complete edition of 
 the letter , of which Dr. A. C. Kalischer have under- 
 take the editorship 

 Handel ’s imperfect knowledge of the english 
 language be amusingly illustrate by the autograph 
 score in the British Museum of his Chandos anthem 
 six part ) ' as pant the hart , ' wherein , with his usual 
 mighty pen - stroke , he write , ' as fazzt the hart . ' 
 that this be not a slip of the pen be show by his 
 having write ' paint ' throughout the movement 
 thirteen time in all 

 the percentage of those publication 
 which pay even the cost of production would be 
 even more interesting than the forego figure 

 the manuscript of Beethoven ’s ' Waldstein ' sonata , 
 | consist of thirty - two oblong folio 

 page , 1 at 




 HEREFORD MUSICAL FESTIVAI 


 AURELIA 


 the MUSICAL 


 TIMES 


 OCTOBER 1 , 1906 . 677 


 selection of 


 PSALMS and HYMNS 


 church of ENGLAND 


 REV . CHARLES KEMBLE , M.A. , 


 the MUSIC 


 select , arrange , and partly C SEI 


 NDON 


 OHN F. SHAW & CO . , 48 . PATERNOSTER ROW 


 S. S. WESLEY . 


 675 


 WV 


 I , 1906 


 MR . A. R. REINAGLE and his salary 


 A. R. REINAGLE , 


 ORGAN Grand Ul 

 symphony of Beethoven and the Te deum of Berlioz 
 bear 
 typical creation of their respective composer , and 
 both owe their conception to the first Napoleon . 
 moreover , 
 from that which originally bring they into existence . 
 always an artist of a 
 extravagant idea , Berlioz conceive the thought of| a few day later Berlioz write , also to Liszt 
 glorify the military renown of Napoleon I. by the | 
 composition of a grand work in which the epic and | 
 dramatic should be interweave , entitle ' the return 

 be 




 the 


 MUSICAL TIMES 


 OCTOBER 1 , 1906 


 ORGAN recital 


 ORGANIST , CHOIRMASTER and CHOIR appointment 


 string INSTRU MENTS 


 FIRST CHOIR . SECOND CHOIR . PHIRD CHOIR . 


 1883 : 


 00 


 the 


 681 


 1855 


 682 the 


 MUSICAL TIMES 


 the MUSICAL 


 I , 1906 . 683 


 NE\ PART - SONGS 


 BROOKS RECEIVI 


 N REINAGLE , 


 O. G. SONNECI 


 PETER CORNELI 


 TO my friend , BASIL JOHNSON 


 four - part song 


 + 5 pp — — _ — — — ' 


 : = = " — at 


 . : = ~ — j ) _ — = — , 3 \ — \ , { 


 — _ ? — = . F 


 A SUNG of AUTUMN 


 = — - — _ ' 4 : 


 ' — — — — _ — — p 


 — — — — — $ _ _ _ _ 


 — _ ~ = — _ 


 2 1 . 


 } _ + @ : 6 } = = = | 


 2 7 = 4 


 A SONG of AUTUMN 


 ' SS 


 HEREFORD MUSICAL FESTIVAL . 


 ( by our SPECIAL correspondent 


 the 


 the MUSICAL TIMES 


 OCTOBER 1 , 1906 . 689credit on Dr. Sinclair , the very able conductor of the festival 

 and the ' introduction and allegro ' for string be brilliantly 
 play under the composer ’s own direction . Dr. Sinclair 
 also deserve credit for a finished performance of Brahms ’s 
 third Symphony , the only symphony — if Dr. Davies ’s work 
 be except — hear at the festival . ' Messiah , ' ' Elijah , ' and 
 ' the hymn of praise , ' be standing dish at these festival , 
 and occupy their accustomed place . at the chamber 
 concert the Nora Clench ( uartet play quartet by 
 Beethoven ( op . 135 ) and Tanéiew ( op . 7 ) , and at the 
 orchestral concert Miss : Evangeline Anthony , a Hereford 
 lady , play one of Mozart ’s violin concerto . there 
 be a lavish supply of principal vocalist ; in addition 
 to those already mention must be recall the name — in 
 alphabetical order — of Madame Conly , Miss Gleeson - White , 
 Madame Agnes Nicholls , and Madame Siviter ; Madame 
 Ada Crossley , Miss Muriel Foster , and Miss Gwladys 
 Roberts ; Messrs. Beaumont , Ben Davies , and William 
 Green ; Messrs. Dalton Baker , Ffrangcon - Davies , and 
 Watkin Mills . there be a first - rate orchestra of over 
 seventy London player ( Mr. W. Frye Parker , leader ) , 
 and the organ be take by Mr. Atkins at the morning 
 performance and Dr. Brewer in the evening , the latter also 
 assume the duty of accompanist at the chamber concert 

 COMPETITION festival 




 MORPETH ( NORTHUMBERLAND ) . 


 PROMENADE concert 


 MUSIC in BRISTOL . 


 ( from our own correspondent 


 music in GLASGOW . 


 ( from our own correspondentmusic in LIVERPOOL and DISTRICT . 
 ( from our own correspondent 

 it be impossible to forecast the season ’s music in Liverpool 
 without a keen sense of satisfaction . the Philharmonic 
 Society will , as usual , give twelve concert . among the 
 work to be perform be Beethoven ’s choral symphony , 
 Parry ’s ' Pied Piper of Ilamelin , ' the ' creation ' ( part 1 
 and 2 ) , Bach ’s cantata ' Phoebus and Pan , ' the Finale to 
 * die Meistersinger , ' and Verdi 's ' Requiem . ' there will be 
 also two special orchestral concert . Dr. Cowen still 
 remain conductor 

 the Ilallé Concert Society announce four concert , 
 conduct by Dr. Richter . as it be two season since 
 Dr. Richter and his orchestra play in Liverpool , it go 
 without say that the announcement have cause general 
 interest 

 Franck ’s ' Symphonic Variations ' for pianoforte , Granville 
 Bantock ’s ' Sappho ' prelude , Gluck ’s overture ' Iphigenia 
 in Aulis , ' Siebelius ’s second symphony , and _ incidental 
 music to ' Pelleas and Melisande , ' and Paul Dukas ’ 
 ' L’apprenti Sorcier , ' the last three be give for the first 
 time in Liverpool . Mr. Henry J. Wood will conduct a 
 Brahms symphony towards the close of the season 

 the Symphony Orchestra , which be compose chiefly of 
 member of the Richter and Philharmonic Orchestras , 
 warmly encourage by its success last season , intend to give 
 twelve concert in the Sun IlIall , under the direction of Mr. 
 Vasco Akeroyd . the symphony to be perform include 
 Tchaikovsky ’s No . 4 , Beethoven ’s C minor , Cowen ’s 
 * Scandinavian , ' Schubert 's ' Unfinished , ' and Beethoven ’s 
 * Eroica 

 Mr. A. P. Mignot have arrange four Schiever Quartet 
 concert , which will be hold at the Hlardman Street Kooms 
 all serious music - lover in this district will welcome this 
 announcement and the opportunity it afford of wish the 
 distinguished enterprise a season of pronounced success 




 MUSIC in MANCHESTER . 


 ( from our own correspondent 


 692 


 MUSIC in NEWCASTLE and DISTRICT . 


 RRI INDENT 


 MUSIC in 


 from 


 NOTTINGHAM and DISTRICT 


 our own CORRESPONDENTThe Nottingham Sacred Harmonic Society , under the 
 direction of Mr. Allen Gill , have select Wagner 's 
 ' fly Dutchman ' for its first concert , to be follow 
 by Handel ’s ' Acis and Galatea ' and Gounod ’s ' Redemption 

 the City Orchestral Concerts will give their usual two 
 performance , with Beethoven ’s fifth and Tchaikovsky ’s 
 pathetic symphony as the chief feature 

 A Choral Union be be promote by the City Education 
 Committee for the student of the evening school , under 
 the conductorship of Mr. Arthur Richards , and Gade ’s 
 ' Erl King ’s daughter ' have be select for performance 




 MUSIC in SHEFFIELD and DISTRICT . 


 ( from our own correspondent 


 the 


 MUSICAL TIMES 


 OCTOBER 1 , 1906the choral and orchestral society have resume their 
 activity — mainly confine to rehearsal at present — and we 
 be promise an interesting season . the most important 
 event will be the performance , for the first time in the city , 
 of Elgar 's ' the Apostles , ' to be give by the Sheffield 
 Amateur Musical Society . the concert be fix for 
 December 18 , and Mr. Henry J. Wood will conduct 

 the Sheffield Musical Union promise Berlioz ’s ' Faust , ' 
 of which Dr. Coward may be rely upon to give a 
 picturesque performance , and ' Israel in Egypt , ' a work well 
 suited to this powerful chorus . an engagement to sing in 
 Beethoven ’s Choral Symphony at ( ueen ’s Hall , under 
 Dr. Richter , be also among the Union ’s fixture 

 the Sheffield Choral Union be unfortunately in difficulty . 
 \ series of excellent concert having result in heavy 
 inancial loss , the Society will at present be continue as a 
 vocal class for the rehearsal only of choral work . Bach ’s 
 ' St. Matthew ' Passion will be the first work study 




 BERLIN 


 COLOUNE 


 ELBERFELD 


 HAMBURG 


 LEIPZIG . 


 PARIS . 


 PESARO 


 MILAN 


 093 


 604 the MUSICALmay , or may not , have be aware of 

 a analyse by 
 Grove in his ' Beethoven nd be nine 
 Novello ) . an 

 art ’s Symphony in C appear in the M Al 

 transpose 

 ee ( 1 ) a portrait of Beethoven appear as a 
 supplement to the M 1 Times of January , 1901 . 
 2 ) as the new edition of Grove ’s ' Dictionary of music and 
 the rate of one volume per annum , 
 ably be complete by the end of 1908 

 musician ’ be issue 
 the work will pr 




 content 


 special notice 


 dure the L. { ST ' MONTH . 


 CH , J 


 LIMITED 


 C \VI NDISH , J. 


 E"¢ AR , 


 OODE RHAM , ALICE 


 FA publication 


 MR 


 ERNEST PIKE 


 TENOR ) 


 695 


 HOIRMASTE R 


 A SSISTA ANT 


 CHOIRMASTER 


 W ANTED 


 the school 


 annual include 


 contain 


 ( ention M teach 


 pheory question 


 nth 


 for OCTOBER 


 MUSIC review 


 the ' SCHOOL MUSIC review 


 ILLOWING MU 


 VELLO and COMPANY 


 the MUSICAL 


 scale of 


 time 


 ° 30 


 3 4 


 200 


 710 


 term for for ADV ertisement 


 6 the MUSICAL 


 time 


 OCTOBER 1 , 1906 


 ~“HOIR EXCHANGE , 136 , FIF th AVE . , NEW 


 WANTEI 


 N E.¢ 


 S I \ 


 for sale . VIRGIL PRACTICE CLAVIER 


 WW 


 PIANO , and 


 TO professor of singing , 


 VIOLIN 


 LARGE TEACHING ROOM 


 * uitable for presentation . — SILVEI 


 TWE 


 D’AL 


 HARM 


 ANAL 


 COUN 


 DOU B 


 the MUSICAL TIM 


 THE OLD FIRM 


 P. CONACHER & CO . , , | 


 SPRINGWOOD WORKS 


 HUDDERSFIELD 


 two gold medal . 


 NICHOLSON and CO . , 


 ORGAN BUILDERS , 


 PALACE YARD , WORCESTER . 


 ( establish 1841 


 PIANO pedal 


 every organist 


 PIANO 


 ALMAINE 


 PIANOS and 


 ORGANS 


 extremely suitable 


 season 


 for performance at the 


 of CHRISTMAS 


 A SACRED CANTATA 


 for four solo voice , CHORUS , 


 ORCHESTRA 


 the word select from HOLY SCRIPTURE 


 and the music compose by 


 ALFRED GAUL 


 AND 


 FBENEZER PROUT ’S work . 


 FUGUE . 


 : LOUIS SPOHR 


 the ORIANA 


 collection of early madrigal 


 british and foreign 


 7 . the NYMPHS and SHEPHERDS dance ( 4 , ) ... GEORGE MARSON 


 36 . O FLY not , LOVE ( 5 5 ,   ) ... THOMAS BATESON 


 ED 


 10 . L 


 12 . 


 13 . 


 14 . * a 


 15 


 16 . 


 17 v 


 18 . 


 19 . 


 20 . 


 21 . 


 22 . 


 23 . 


 24 . f 


 28 . si 


 29 . 


 30 . 


 the MUSICAL time 


 progressive 


 for the 


 PIANOFORTE 


 edit , arrange in group , and the fingering revise and supplement 


 FRANKLIN 


 TAYLOR 


 2 } . " » 7 } § 2 . » 2 


 fifty - six book , price one shilling each 


 imt the a 


 BO'V 


 select PIANOFORTE study 


 progressively a 


 FRANKLIN TAYLOR 


 in two set ( eight book ) , price one shilling and SIXPENCE each book 


 company , limited 


 BY 


 SECTION a.—technical practice . in six book 


 SECTION b.—studie . in six book 


 piece 


 5 . OG 


 the MUSICAL TIMES 


 OCTOBER 1 , 1906 


 nthem for trinitytide 


 I be ALPHA and OMEGA 


 compose by 


 CH . GOUNOD 


 LIGHT of the WORLD 


 compose by 


 EDWARD ELGAR 


 COME , YE CHILDREN 


 compose by 


 JOSIAH BOOTH . 


 complete list . 


 the lute serie 


 ANTHE 


 LONDON NOVELLO and comp 


 m for advent 


 702 the musical 


 century composer 


 BY 16TH , 17th , and ISTH 


 anthem 


 BATESON , TIIOMAS 


 BATTEN , ADRIAN ( /. 163 


 BECKWITH , JOHN CHRISTMAS ( 1750—1809 ) . 


 BLAKE , EDWARD ( 1708—1765 ) 


 BLOW , JOHN ( 1648—1708 ) . 


 BOYCE , WILLIAM ( 171 1779 ) 


 BYRD , WILLIAM ( 154 1623 


 CHILD , WILLIAM ( 1606 ? — 1697 


 CLARKE , JEREMIAH ( 1669 ? — 1707 ) . 


 CREYGHTON , ROBERT ( 16392?—1734 


 p i o 


 CROFT , WILLIAM ( 1677?2—1727 ) . 


 DOWLAND , JOHN ( 1563 162 


 EBDON , THOMAS ( 1738—1811 


 FARRANT , RICHARD ( /. 1564—1580 ) . 


 GIBBONS , ORL ANDO ( 1583—1625 


 G REE NE , MAI R 1 ; ( 1696 ? — 1755 


 HAYES , PHILIP ( 1738—1797 ) . 


 HAYES , WIL L [ AM ( 1706—1777 ) . 


 HUMPHRYS , PELHAM ( 1647—1674 ) . 


 KENT , JAMES ( 1700—1776 


 KING , CHARLES ( 1687—1743 ) . 


 LOCKE , MATTHEW ( 1630?—1677 


 NARES , JAMES ( 1715—1783 


 PURCELL , HENRY ( 1658 ? — 1695 ) . 


 REDFORD , JOHN ( /. 1535 ) . 


 ROGERS , BENJAMIN ( 1614—1698 ) . 


 TYE , CHRISTOPHER ( 1497?—1572 


 WISE , MICHAEL ( 1646?—1687 ) . 


 service 


 OLD ENGLISH ORG an music 


 prefatory note 


 LONDON : limited 


 NOVELLO and COMPANY 


 MUSICAL 


 704 the 


 time 


 CHURCH music 


 service 


 anthem . 


 the 


 morning and evening 


 service 


 GETHER W H HE 


 office for the HOLY COMMUNION 


 set to MUSIC in the key of G MINOR 


 BY 


 NATHANIEL PATRICK 


 TRANSPOSED edition 


 CHURCH MUSIC 


 new anthem by 


 ERNEST EDWIN MITCHELL . 


 the HEAV ENL Y vision 


 to CHORAL and orchestral society . 


 everal choral work , 


 part - song 


 composition 


 T. MEE PATTISON 


 original ORGAN composition . 


 a practical method of 


 | training chorister 


 RY 


 DR . VARLEY ROBERTS 


 the < canticle 


 set to anglican chant 


 compose by 


 FREDERICK ILIFFE . 


 FI E 


 IN 1 


 pil ( 


 SOR 


 the 


 the 


 the 


 tro 


 the 


 1906 


 PART - song 


 PETER CORNELIUS 


 WITH english word . 


 MALE voice . 


 mix voice . 


 ; sott 


 NARCISSUS and ECHO 


 CANTATA for CHORUS , SOLI , and ORCHESTRA 


 compose by 


 EDWIN C. SUCH . 


 , BC IOKS that should be 


 MALLINSON RANDALL . 


 of the church 


 G. EDWARD STUBBS . 


 practical hint on the training of 


 CHOIR boy . 3 


 how to sing the choral service . a 


 : F. E. HOWARD . 


 W. H. HALL . 


 HYMN to DIONYSUS 


 the word from the 


 BACCH.E of EURIPIDES 


 translation BY 


 GILBERT MURRAY 


 for CHORUS and ORCHESTRA 


 BY 


 ERNEST WALKER 


 second edition 


 the his tory 


 MENDELSSOHN ’S oratorio 


 ELIJAH 


 F. G. EDWARDS . 


 C.B. 


 956 


 one 


 just publish 


 bible version 


 canticle 


 point for chanting 


 the 


 extract from the preface . 


 PRESS notice 


 LONDON : NOVELLO and COMPANY , limited 


 HUNDRED psalm 


 ce at the HEREFORD MUSICAL FESTIVAL , 1906 . | produce at the HEREFORD MUSICAL FESTIVAL , 1906 , 


 LIFT up your heart — 


 cre sympmony , in f three ELIZABETHAN 


 : -astoral | 


 compose by an IDYLI ; 


 ( o } 20 . ) 


 compose IY 


 FHE YORKSHIRE POS 


 YORKSHIRE POST . 


 : ( GLOUCESTER JOURNAL . 


 produce at the PHILHARMONIC SOCIETY ’s concert , JUNE 14 , 1906 


 symphonic variation 


 on an african AIR 


 BY 


 S. COLERIDGE - TAYLOR 


 the TIMES . MORNING POST . 


 1906 


 produce at the HEREFORD MUSICAL 


 SOULS 


 A psalm of 


 the 


 FESTIVAL , 1906 


 RANSOM 


 the POOR 


 SINFONIA SACRA ) 


 FOR 


 SOPRANO and BASS SOLI 


 CHORUS and ORCHESTRA 


 compose BY 


 C. HUBERT 


 PRICE 


 two 


 the TIMES 


 DAILY TELEGRAPH 


 DAILY NEWS . 


 H. PARRY 


 SHILLINGS 


 DAILY CHRONICLE 


 the TRIBUNI 


 GLOBE 


 the ATHEN-€UM 


 the YORKSHIRE POST 


 AND COMPANY 


 LIMITED 


 introduction 


 ALLEGRO OLD ENGLISH dance 


 FOR STRINGS FREDERIC H. COWEN 


 QUARTET and ORCHESTRA 


 MAYPOLE DANCE 3 ; MINUET D’AMOUI 


 PEASANTS ’ DANCE . 4 . OLD DANCE 


 MINI 


 the TIMES 


 DAILY TELEGRAPH . 


 DAILY NEWS 


 EVENING STANDARD . 


 DAILY ¢ RONICLI 4 


 YORKSHIRE POST SUNDAY TIMES . 


 WESTERN DAILY PRESS 


 GLOBI . . 


 PA MALL GAZETTI GLASGOW HERALD 


 ASGOW NEWS 


 NOV 


 the MUSICAL 


 _ _ CHURCH CANTATA . NEW ORGAN COMPOSITION 


 SEVEN PIECES 


 NOVELLO ’S MUSIC primer and EDUCATIONAL serie 


 = DR . 


 BREATHING 


 VOICE P RODU ction 


 PART i.—explanatory 


 the lateral costal method of breathing 


 special physical exercise for the attainment | choir of 


 of a correct pose . \ 


 introduction , 


 FOR 


 ADES 


 LOV 


 ON the 


 TE 


 BY 


 FI 


 the 


 711 


 DELES " 


 ORGAN 


 OXON 


 ARTHU R. S 


 AP 


 2 , WEDDING MARCH | 5 . MARCHE TRIOMPHALI ( MUS.D. , 


 HOLLOWAY 


 PEAL 


 INTE -RMEZZO 


 FOR 


 EDWIN 


 TI 


 IMPOSE 


 IN 


 TERY POPULAR 


 IRGAN 


 BY 


 EMARE 


 PIANOFORTE PIECE 


 HOLLOWAY 


 EASY ANTHEMS , 


 D MINOR 


 SS 


 ( NOVELLO ’S edition 


 PIANOFORTE . — ORGAN 


 LOCAL CENTRE.—INTERMEDIATE grade . LOCAL CENTRE.—INTERMEDIATE GRADI 


 SINGING . 


 LOCAL CENTRE DVANCED GRADI LOCAL CENTRE.—INTERMEDIATE grade 


 : BARITONE . 


 10 


 . , BAS 


 SCHOOL EXAMINATIONS.—LOWER DIVISION MEZZO - SOPRANO . 


 REINE I ¢ of ; N BARITONE 


 the 


 SCHOOL EXAMIN 


 HE EI 


 PANSE N. 42 


 SCHOOL EXAMINAT 


 MEZZ 


 6 ASS . 


 class SINGING EX AMIN : ATION . 


 VIOLIN 


 LOCAL CENTRE.—INTERMEDIATE grade . 


 . LOCAL CENTRE.—ADVANCED grade . 


 ; SCHOOL EXAMINATIONS.—PRIMARY . 


 SCHOOL examination — elementary . 


 SCHOOL EXAMINATIONS.—LOWER division . 


 SCHOOL examination . HIGHE R DIVISION . 


 LOCAL CENTRE.—INTERMEDIATE grade . 


 LOCAL CENTRE.—ADVANCED grade . 


 1907 


 SCHOOL EXAMINATIONS.—PRIMARY . 


 — -RANO . 


 ATIONS.—ELEMENTARY 


 SOPRANO 


 CONTRALTO 


 TENOR . 


 BARITONE . 


 BASS . 


 IONS.—HIGHER DIVISION . 


 O - SOPRANO 


 CONTRALTO 


 TENOR . 


 BARITONE 


 NOVELLO 


 the BALLAD 


 | of MESHULLEMETH 


 the orator 10 


 C. HUBERT H 


 JUDITH ? 


 PARRY 


 BE thou faithful U 


 death 


 ARIA from " ST . 


 compose by 


 MENDELSSOHN 


 NTO 


 PAUL 


 arrange with VIOLONCELLO OBBLIG 


 J. W. SLATTER 


 ATO BY 


 FAR FROM my HEAVENLY 


 home 


 SACRED SONG 


 the word write by 


 HENRY FRANCIS LYTE 


 the music compose by 


 J. MARGETSON 


 CHE FARO SENZA 


 EURIDICE 


 THOU ART GONE , ALAS , for ever " ) 


 from the OPERA 


 ORPHEUS 


 ose by 


 transpose edition 


 BREITKOPF and HARTEL , 


 GREAT MARLBOROUGH STREET , W 


 CLARISSE MALLARD ’S 


 SONG - CYCLE for the 


 children 


 SONGS , DANCES , and TABLEAUN vivant . very 


 suitable for school entertainment , I 


 714 the music 


 ANDANTE IN 


 FOR the ORGAN 


 ENRY SMART 


 ANDANTE ESPRESSIVO 


 symphony in 


 ARTHUR SULLIVAN 


 NGEMENT for PIANOFORTI 


 WILFRED BENDALL 


 ALLEGRETTO 


 SYMPHONY in E 


 RTHUI SULLIN AN 


 krangement for pianoforti 


 WILFRED BENDALL 


 the SEASONS 


 SYMPHONIC SUITI 


 EDWARD GERMAN 


 the SANDS of 


 BALLAD 


 CHORUS and ORCHESTRA 


 CHARLES KINGSLEY 


 CHARLES A. E. HARRISS 


 NEW CONCERT SONGS 


 BY 


 DR . ARTHUR S. HOLLOWAY 


 ORGAN > 


 arrangement 


 edit by 


 JOHN E. WEST 


 MACKENZIE 


 SCHU MANN 


 CESAR CUI 


 SCHUMANN 


 SCHUBERT PRELUDE ro PART IIL . ( Tut Avostuts 

 EDW ARD ELGAR 
 arrange by G. R. Si Al I 
 BEETHOVEN 
 arrange by . 
 ADORAMUS TI HUGH BLAIR 

 arrange by Hucu Bian 
 INTERMEZZO ( " Tue B . A RISTOPHANES 




 ORGAN 


 transcription 


 GEORGE SL BENNETToF JuBAL " ) .. < A.C. MACKENZIE 1 
 . PRELUDE . — ( " Lonencrin " ) .. . WAGNER 1 
 ANDANTINO.—Sympeuony , no . 4 , in f minor ) 
 rSCHAIKOWSKY 2 
 . slow movement . - \NOFORTE concerto in B flat 
 minor ) - TSCHAIKOWSKY 1 6 
 . CORONATION MARCH TSCHAIKOWSKY 2 
 . three minuets.—sympnuonie tn c , G MINOR , and 
 E FLAT ) . . MOZART 2 
 . minuet.—{sonata in E Fiat ) . ( op . 31 , iii 

 BEETHOVEN 1 
 C. MACKENZIE 
 FINAL . - may we < ce again " ) } — " ' bl pair of 

 SIREN , ° c. H. H. PARRY 1 




 MENDELSSOHN 


 impro ) IPTU in a 


 O RC " A N 


 compose by 


 EDWIN H. LEMARE . 


 CHR\ 


 CHRIS 


 LET 


 HOSA 


 NOW 


 SONGS 


 eight 


 METZL 


 LIGHT 


 GODARD 


 METZLER 'S S CHRISTM , \S music 


 LET s now go even unto BETH- SULLIVAN 14 } 


 METZLERS bound volume 


 suitable for CHRISTMAS present and s¢ ' hool prize 


 METZLER ’s late novelty 


 selection of sacred song 


 arrange for the pianoforte by H. M. HIGGS 


 new composition for the ORGAN 


 VOW READY . 


 METZLER & CO . , LIMITED , 


 40 to 43 , GREAT MARLBOROUGH STREET , LONDON , W 


 CHAPPELL & CO.S NEW publication , — | 


 just publish . 


 SONG without word 


 - EDWARD GERMAN 


 price two shilling net each 


 MAUDE VALERIE WHITE . EDWARD GERMAN . 


 ‘ in GOLDEN JUNE " when maiden go A - maye . 


 ' in the SUMMER GARDEN " LOVE be meant to make we glad . 


 GUY D’HARDELOT . FRANCO LEONI . 


 " for you alone . " the MERRY MAIDEN . " 


 ' I think ' a butterfly song . " 


 ' my heart will know " whe n he come home . 


 TERESA DEL RIEGO . HERMANN LOHR . 


 BROWN eye . ' oh , to forget . ' 


 O love father * alone 


 the song of SUMMER " the hunt be up . " 


 1 " a chain of rose . " 


 ‘ to PHYLLIDA " remember I 


 " the BELL W. H. SQUIRE . 


 " look up , O HEART . " three for J : ING . 


 " out of REACH ERNEST NEWTON . 


 " I claim you mine . " through the forest . 


 GEORGE H. CLUTSAM . " LOVE be ECHO . " - 


 . OS - 7 


 the RED ROSE TREI FRANK LAMBERT . 


 " sweet , be not proud " bid you GOOD - morrow 


 » . " the bury ROSE . 


 WADDINGTON COOKE . " dear hand . " 


 PAUL A. RUBENS NOEL JOHNSON . 


 ' the SUMMEI R " WILD ROSES . 


 HERBERT BUNNING . ROBERT CONINGSBY CLARKE . 


 revelation " a BIRTHDAY SONG 


 MY SWEETHEART " a dedication 


 BERNARD ROLIT " BETTY ’ 'S way . 


 " the little gold firefly ) " take your lute and sing 


 the first complete uniform edition of the world - famous 


 GILBERT and SULLIVAN OPERAS 


 the MIKADO . the pirate of PENZANCE 


 the gondolier . H.M.S. PINAFORE 


 the YEOMEN of the guard . RUDDIGORE 


 PATIENCE . UTOPIA , LIMITED 


 IOLANTHE the GRAND DUKE 


 for £ 3 cash 


 prospectus will be send POST - free upon application to 


 and may be have of all musicseller 


 SEVE 


 NOVELLO 


 PART - song 


 SECOND serie . 


 HENRY SMART 


 SEVEN SHAKSPERE song by 


 G. A. MACFARREN 


 J. L. HATTON 


 HENRY LESLIE . 


 extra supplement 


 S part - song book 


 a collection of 


 glee , and 


 EACH 


 serie 


 or in 


 first 


 21 


 six madrigal . 


 FRANCESCO BERGER . 


 I. BAPTISTE CALKIN . 


 J. BARNBY . 


 G. A. MACF ARREN . 


 madrigal 


 separate number 


 eight SHAKSPERE song by 


 G. A. MACFARREN 


 HENRY LESLIE . 


 HENRY SM ART . 


 SAMUEL REAY . 


 W. MACF ARREN 


 J. LEMMENS 


 HENRY SMART 


 CIRO PINSUTIL 


 1 t £ 


 HATTON 


 MALE voice 


 4 - 3 -9- 9 - 9 - 9 


 2020080 


 - ( S.S.A.T.B , ) 


 ( S.A.T.B. ) } 


 32 


 FRANZ ABT 


 A. C. MACKENZIE 


 E. PROUT . 


 J. L. HATTON 


 NR OO GNIS 


 SOP 


 ( second serie . ) 


 come to I , GENTLE SLEEP 


 four - part song 


 the word write by MRS . HEMANS 


 the music compose by 


 FREDERIC H. COWEN 


 CUME to I , GENTLE SLEEP 


 : — \ , — — _ — - = — 9—__9 — _ 9 - 9 _ |-9—__|—_9—"_4 — 


 come to I , GENTLE SLEEP . 


 ¢ — — — — — — - — — — 4- — — — _ — — -3 


 2 - 2 -@ 


 < = FS ES — _ = = = SS 


 come to I , GENTLE SLEEP . 


 ss — -_—- — — _ — — — — — _ 


 T J © Z ' . " 2 


 SE + 


 COME to I , GENTLE SLEEP 


 53Vov . XV . ( continue 

 Beethoven 
 Naumann 

 Corin for Cleoradying 




 J. L 


 ~2686.6 . 5 


 PABBA ? e 


 1906