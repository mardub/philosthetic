


 the MUSICAL time 


 AND SINGING - CLASS CIRCULAR 


 found in 1844 


 publish on the first of every month 


 ROYAL CHORAL SOCIETY 


 ROYAL ALBERT HALL . 


 conductor 


 SIR EDWARD ELGAR , O.M. 


 MR . ALBERT COATES . 


 DR . MALCOLM SARGENT . 


 DR . E. C. BAIRSTOW 


 ELIJAH : - - - MENDELSSOHN 


 MADAME MIRIAM LICETTE . | MISS BERTHA STEVENTON . 


 MISS?ASTRA DESMOND . MISS JANET POWELL 


 MR . TUDOR DAVIES . MR . TREFOR JONES 


 MR . PERCY HEMING . MR . EDWARD HALLAND 


 REQUIEM - -   - + + BRAHMS 


 MISS STILES - ALLEN . | MR . ROY HENDERSON 


 CAROLS 


 MISS MEGAN FOS TER 


 MR . WALTER GLYNNE . MR JOHN GOSS 


 MESSIAH - - - - - HANDEL 


 MISS DORA LABBETTE . MISS MARGARET BALFOUR . 


 MR . WALTER WIDDOP . MR . NORMAN ALLIN 


 MISS CAROLINE HATCHARD . 


 MR . FRANCIS RUSSELL . MR . PETER DAWSON 


 the dream of GERONTIUS 


 MIS 


 MR . STEUART WILSON . - HOWARD FRY 


 MESSIAH - - - - HANDEL 


 MISS FLORA WOODMAN . MISS MURIEL BRUNSKILL . 


 MR . BEN DAVIES . MR . CHARLES KNOWLES . 


 MISS DOROTHY SILK . | MISS MURIEL BRUNSKILL . 


 MR . PARRY JONES . | MR . ROBERT RADFORD 


 DOUBLE chorus of 850 voice . 


 the ROYAL ALBERT HALL ORCHESTRA . 


 OCTOBER 1 _ 1927 


 ROYAL ACADEMY of MUSIC . 


 YORK GATE , MARYLEBONE ROAD , LONDON , N.W.1 . 


 HIS MAJESTY the KING . 


 HER MAJESTY the QUEEN . 


 H.R.H. THE DUKE of CONNAUGHT 


 AND STRATHEARN , K.G. 


 H.R.H. THE DUKE of CONNAUGHT 


 AND STRATHEARN , K.G. 


 MICHAELMAS HALF - TERM 


 CHAMBER CONCERT 


 72 scholarship for singer AN 


 INSTRUMENTALISTS 


 L.R.A.M 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W.7 


 . HER MAJESTY the QUEEN , 


 GLASGOW 


 ATHENAZUM SCHOOL of MUSIC . 


 session 1927 - 28 


 866 


 1927 


 VICTORIA EMBANKMENT , E.C.4 


 BIRMINGHAM & MIDLAND INSTITUTE 


 SCHOOL of MUSIC 


 session 1926 - 1927 


 ROYAL 


 MANCHESTER COLLEGE of MUSIC 


 DR . ADOLPH BRODSKY 


 STANLEY WITHERS , M.A 


 96 & 95 , WIMPOLE STREET , W.1 


 TOBIAS MATTHAY 


 PIANOFORTE SCHOOL 


 complete training course for teacher . 


 MANCHESTER SCHOOL of MUSIC . 


 16 , ALBERT SQUARE . 


 LEOPOLD H. CROSS , M.A 


 ROBERT GREGORY 


 UNIVERSITY of DURHAM . 


 MUS . DOC . examination 


 L ONDON SOCIETY of ORG ANISTS . 


 FOUNDED 1913 


 prize of TEN guinea 


 over 1,100 specimen working 


 2 F.R.C.O. 3 A.R.C.O. 2 L.R.A.M 


 th 


 881 


 AND singing - class circular 


 OCTOBER i 1927 


 the HEREFORD FESTIVAL and its 


 con ductor - in - chieforchestral music sometimes evoke damaging 
 comparison with that of regular orchestral 
 conductor 

 critic forget , however , that the most eminent 
 of these conductor have their limitation . 
 for example , at the hand of one whose strong 
 suit be russian music , we may hear performance 
 of Beethoven and Mozart far less good than those 
 of Franck and Delius direct by Dr. Percy 
 Hull at Hereford . it be easy to point out 

 as a well - know critic have recently do ) the 
 b 




 882 


 three russian composer in PARIS 


 ARTHUR LOURIER 


 ARIS 


 OBUKHOV 


 884 


 VYSHNEGRADSKY 


 performer 


 BONAVIA 


 puzzle forunnecessary . consider 

 standing and experience still debate whether 
 Beethoven mark c or € in the trio of 
 the Scherzo of the ninth Symphony , such 
 view can be accept only with extreme 

 caution , if not with downright scepticism . more- 
 over the ' spirit ' can not be apparent before the 




 the 


 1927 885 


 soa point concern the use of the gramophone 
 call for a word . it make possible a standardised 

 performance , and it have further advantage in 
 regard to immediate repetition of an entire 
 work or of certain salient passage . but surface 
 noise , and its lapse from purity of tone and from 
 exact reproduction of tone - colour , be a serious 
 disability . hence some curious finding . thus , 
 in one set of experiment , the victim be ask 
 to express , by mean of figure , the proportion 
 of enjoyment derive from rhythm , Melody , 
 | design , Harmony , and tone - colour . the 
 /composer include Handel , Bach , Beethoven , 
 Wagner , Liszt , Brahms , and Debussy . can the 
 reader guess which score most heavily under 
 ' ' Tone - Colour ' ? of course he can : I hear he 
 reply , in crowd , ' Wagner ! ' wrong : try again ! 
 ' Liszt ? ' wrong : make a third and last shot ! 
 * Debussy . " no : the poll be head by Handel ! 
 Wagner score heavily in rhythm ( 347 ) , Melody 
 |(674 —— yet a few year ago he ' could not write a 
 chune ' ! ) , design ( 295 ) , and Harmony ( 400 ) . in 
 Tone - colour he could manage only a mere 217 . 
 clearly the gramophone , marvellous as it be , be less 
 | 800d than we think it to be in the reproduction 
 of instrumental timbre . this be prove by the 
 lfact that all the ' tone - colour figure be 
 |comparatively low . ( of course the train 
 | musician make good its defect mentally , which 
 the subject in the experiment be probably 
 less able to do 

 the Handel total for tone - colour ( 235 ) raise 
 an interesting point . the eight piece by which 
 he be represent be all take from " the 
 | Messiah . " ( here , by the way , be expose a 
 |weakness in the experiment . what basis of 
 |comparison be there between a selection from 
 an 18th - century oratorio and a series of extract 
 |from Wagner ? ) one would have expect 
 | Handel to receive large figure for everything but 
 | tone - colour and ( possibly ) harmony . yet all be 
 low compare with those give to other composer 

 the whole table be worth quote 

 one 
 | com poser Rhythm Melody Design . harmony . hn 
 | Handel 119 144 137 65 235 
 | Bach 4460 730 727 352 179 
 | Haydn 211 217 ist 61 73 
 Mozart 135 203 153 5u 76 
 | Beethoven 492 522 338 332 1go 
 | Schumann 827 839 227 350 74 
 | Chopin 382 406 220 234 114 
 | Mendelssohn 134 202 78 116 40 
 Wagner 347 674 205 400 217 
 | Liszt 133 173 50 157 q2 
 | Brahms 212 192 124 140 37 
 | MacDowell 135 162 34 152 34 
 Debussy o4 100 22 12 2 
 \‘it % not improbable that yocal tone - colour 

 attract more than instrumental tone - colour , ' say 
 the book , in attempt to explain the curious fact 
 of Handel reach his high total in the column 
 where we should look to find his small . it be 
 more than probable : it be certain — at all event so 
 far as the less sophisticated hearer be concern . 
 he respond to vocal music from the start — 
 partly , of course , because of the word and their 
 association . instrumental tone - colour rarely 

 the audience consist of group of various 
 size up to twenty - four , and their job be to note 
 and describe the change in their attitude to the 
 music as the repetition go on . the work be 
 in four grade : ' severely classical ; serious popular 
 classical ; easy popular classical ; and popular . ' 
 there be a further division into fast and slow 

 the difficulty of classify music in this way be 
 show by the fact that the four * severely classical ' 
 work consist of ( fast ) the first movement of the 
 * unfinishe , ' and the Fugue from Beethoven ’s 
 C major String Quartet ; and ( slow ) a Mozart 
 Andante and Wagner ’s ' Traiume . ' none can be 
 call ' severe , ' for even the Fugue have the attrac- 
 tion of energy , pace , and great rhythmic vitality . 
 in fact , practically all the piece mention in the 

 three category of classical be interchangeable . 
 only when we come to the section head 
 * popular ' be we safe with two fox - trot and two 
 waltz . in the face of this , it be clear that we 
 must not hastily decide as to the comparative 
 attractiveness of the ' severely ' classical , and the 
 _ serious ' and ' easy ' popular classical . the 
 severe ' have a way of become merely ° serious ' 
 after a few hearing , and * easy ' after a good many 
 more 




 dream and reality in toneplace in our conception of tone . the overwhelming 

 fact be that our ear hear other thing , and hear thing 
 | otherwise , than ever before . how many sound crop 
 | up to - day that be unknown a hundred year ago 
 | ( those produce by machinery , x&c . ) ! these sound 
 | induce new reflex which affect our attitude towards 
 music . for we tone have become something elastic 
 and tensile . our conception of tone be similar to that 
 which prevail in the Middle Ages . but of the 
 ' reality ' of tone in the Middle Ages we know nothing : 
 all we have to go by be our dream - conception of it . 
 the great similarity between the Middle Ages and 
 to - day lie in the fact that music of the Middle Ages 
 have not yet a bass , and the music of our young 
 and most ' advanced ' composer have no long 
 a bass . and if we be attract towards early 
 music , it be because this music , however purely 
 ' linear ' and lack in tonal definition it may be , be 
 certainly characterise by a positive conception of tone 
 which we no long can achieve . the change in our 
 | sense of hearing originate , paradoxically enough , in 
 Beethoven ’s deafness . the ' dream tone ' of the late 
 Quartets and Sonatas and of the Ninth Symphony 
 influence Wagner more than Beethoven ’s effort 
 towards new architecture . it be Beethoven ’s grope 
 | attempt to create , on the basis of a new tone - concep- 
 | tion , an architecture unite full definition of form 
 | with boundless scope for the imagination that give 
 | Schénberg the first impulse to write his great chamber 
 | work . Beethoven ’s longing for a great compass of 
 } all instrument be likewise symptomatic . ( the intro 

 duction of new instrument , the decay of the clavichord 

 and organ , complete the ' atonalisation ' of the ear . ) 
 | Beethoven ’s conception of tone be , towards the end , 
 purely ideal ; and this may account for the fact 

 that in his later work there be much that do not 
 ' come off 




 HABA ’S QUARTER - tone composition class 


 NEW music for EDUCATIONAL purpose 


 1927 889 


 RAVEL ’S STYLE 


 JOHANN CHRISTIAN BACH as a symphonist 


 A renaissance for HANDEL ’s opera ? 


 suite in MOSLEM MUSI 


 LEMMENS and J. S. BACH 


 the origin of MOZART ’s style 


 an italian precursor of GLUCK 


 AN appeal 


 M.-D. CALVOCORESSI 


 PRATORIUS and MERSENNE 


 BUSENELLO ’S LIBRETTO to 


 MONTEVERDE ’S ' L’ ° INCORONAZIONE DI 


 POPPA ’ ITS place in the history 


 of the drama and of the opera 


 EFS 


 BRUSSELS , 


 1927 895the MUSICAL ‘ time — Octroser 1 1927 

 896 
 Gramopbone wote Tchaikovsky ’s d major Quartet . playing and 
 safe ee ata recording alike could hardly be better ( 9203 ) . 
 by ' Discus the only pianoforte record be of Evlyn Howard- 
 — Jones in Brahms ’s Capriccio in b minor and a 
 COLUMBIA very un - scottish Ecossaise of Beethoven ’s : crisp 
 this month ’s output contain several first - rate | playing in the latter , and the Brahms be a model of 

 orchestral record . ' those of the Venusberg Music , 
 play by the Philharmonic Orchestra , conduct by 
 Bruno Walter , leave nothing to be desire in the 
 matter of vividness , and be , in fact , among the good 
 of recent record feat ( l1982 - 83 

 Voluntary ' ( arrange by Sir Henry Wood ) and 
 Walford Davies ’s ' Solemn Melody . ' in both the 
 organ , play by Harold Dawber , be use with great 
 effect . Alexander Harris be the trumpet soloist in the 
 ' Voluntary , ' and the’cello solo in the ' Solemn Melody ' 
 be play by Clyde Twelvetrees . 
 the Hallé , conduct by Sir Hamilton Harty . the 
 various constituent — orchestra , soloist , and organ — 
 be blend , balance , and reproduce with unusual 
 success . the organ , for once in a way , sound really 
 like an organ ; and ( in the Purcell piece especially ) 
 the result be a glorious welter of sound ( l1986 

 not every orchestral work bear transference to 
 the military band , and the ' Fidelio ' Overture suffer 
 in part . the playing of La Garde Républicaine 
 Band be of course all we expect from the medium , 
 but not quite what be want for Beethoven ( 9208 

 a cinema organ record of more than average merit 
 be that of Quentin Maclean play , on the fine instru- 
 ment at Shepherd ’s Bush Pavilion , a series of 
 extract call ' Classica , ' arrange by Montague 
 Ewing . the title be flattering to some of the 
 material , which range from the Toreador ’s Song 
 to Handel ’s Largo . Mr. Maclean ’s playing be first- 
 rate ( 9225 




 h.m.v 


 b2 


 1927 897 


 b2493 


 db1042 


 898 


 1927 


 h. g 


 1927 899 


 -q 3 


 the 


 1927 


 book receive 


 song 


 UNISON SONGS 


 _ a 


 part - song for boy ’ and FEMALE voice 


 MALE - voice 


 MIXED - voice 


 PIANOFORTE 


 EASY PIANOFORTE MUSIC 


 1927 905 


 CHAMBER MUSIC 


 VIOLIN 


 B. V 


 ( 7081 


 ( t 303254 


 1927 907 


 AMATEUR STRING QUARTET 


 theto his old pupil at home and abroad ; to musician 
 in general ; and to all who desire to recognise valu- 
 able public service . we warmly commend the Fund 
 to our reader ’ notice . the hon , secretary and 
 treasurer be Mr. H. Dover , 14 , Hinderwell Street , 
 Hull 

 the Sunday Evening Concert Society , now in its 
 eighth year , announce a series of chamber concert 
 of great interest . on October 2 the performer will be 
 the Kutcher Quartet , Harriet Cohen , and John Goss 
 ( Franck ’s Quintet , String Quartets by Beethoven 
 and Dvorak ) ; on the gth , Pianoforte and Violin 
 sonata by Mozart and Brahms , and violin and 
 pianoforte solo by Bach and Brahms , be the fare , 
 the player be William Primrose and Soloman . 
 Isobel Maclaren will sing song by Brahms . the 
 Wood Smith Quartet will play quartet by Mozart , 
 on the 16th ; sextet by Brahms and Schénberg will 
 also be give . these specimen programme will serve 
 to whet the appetite of chamber music enthusiast . 
 the concert will take place at the Working Men ’s 
 College , Crowndale Road , N.W. full programme 
 and other particular from the hon . secretary , 
 Miss Mabel Fletcher , 26 , Uplands Road , n.8 

 our correspondence column have lately show the | 
 need of some regulation in the use of the initial | 
 many of our reader 




 the musical ticharming town of Salzburg , partly because it be on | 48@!n during the action of the play with great effect 

 the way to the austrian Tyrol , but especially since| by 8 p.m. on the same day we be in the 
 I wish to hear as much as I could of the much- | Cathedral to hear the Mass in D. the choir be 
 advertise musical Festival , which go on through | 8004 : so be the soloist , though the tenor be 
 the whole of August . I be able to see and hear incline to portamento , and the bass be swamp 
 ' Everyman ' ; Beethoven ’s Mass in D , and ' Fidelio ' ; | occasionally by the orchestra — not really his or 
 Mozart ’s ' Don Juan ' ; ' the Midsummer night’s| Beethoven ’s fault . in fact , Joseph Messner , the 
 Dream , ' with the whole of Mendelssohn ’s music , | Conductor , hardly realise the acoustic of the lofty 
 and two orchestral concert . the programme of | and spacious Cathedral . the Vienna Philharmonic 
 the latter include Haydn ’s well - know Symphony | Orchestra be obviously good , but its playing do 
 in D , Beethoven ’s fifth Symphony , and three work by | 2 t always sound well , owe to the erratic /empi of 
 Mozart — a March in D , a Serenade , also in D , and a| the conductor . the Gloria and other fast move- 
 work which I have never hear -before , a Concerto for | ™ ent be take too fast for that echo place ; 
 four solo instrument and orchestra . Austria | ‘ he result be often a muddle . on the other hand , 
 evidently be do its musical good . Vienna | some of the slow movement be drag . I have 
 contribute its Philharmonic Orchestra for the Mass | " ¢ver hear the Kyrie take so slowly . the rhythm 
 and for the orchestral ' concert , and its Opera | WS almost lose at time , owe to the undue length- 
 orchestra for the opera . the choir for the Mass , | " e of the note . it be one more illustration of 

 and most of the solo.singer , also come from Vienna 




 ANTHEM 


 A ] | = 4 4 - 


 ALTO ¢ 4 } — — LSS . 


 I _ L 


 GOD send the night 


 F - 1 — — — — 


 = |_—_———. 4 — _ 


 GOD send the night 


 — = — FI 


 SI 


 GOD send the night 


 2 fs 


 927 


 LA though Bottom ’s part , take by Hans Moser , be 
 rather overdone . also he seem to I com- 
 pletely to misunderstand the wonderful line which 
 Shakespeare , at any rate , give to Bottom when the 
 latter wake from sleep at the end of the first scene , 
 act 4 , and try to recall his dream . Herr Moser 
 make they comic ; but there be surely a touch of 
 wistful tragedy in these amazing , halting word which 
 the disillusioned village clown deliver , when the 
 fairy have vanish , leave he on the stage alone ! 
 Lysander , Demetrius , Helena , and Hermia , make the 
 audience laugh a great deal : all of they , however , 
 badly over - act . in fact , the whole trouble be 
 that I be watch a clever production of Reinhardt ’s , 
 with german word , surely not Shakespeare . what 
 have become of the ' Midsummer Night ’s Dream ' 
 that we know so well ? where be the music 
 of Shakespeare ’s verse ? the spirit and the poetry 
 of the play somehow seem largely to have 
 go . brilliant production , but mot Shakespeare . 
 Mendelssohn ’s music , however , sound pleasant 
 throughout 

 on Sunday there be an orchestral concert in the 
 Mozarteum , at 11 a.m. Franz Schalk conduct a 
 programme of three orchestral work , two of they 
 well - know — Haydn ’s Symphony in D and Beethoven ’s 
 fifth . the middle work , by Mozart , be describe 
 as Konzertantes Quartet ( K. v. Anh . 9 ) , for four solo 
 instrument and orchestra . in this latter work the 
 hautboy , clarinet , bassoon , and horn soloist play 
 magnificently . one hardly know whether to admire 
 more the performer or the genius of the composer . 
 all four player be deal with passage of the 
 utmost Virtuosity , apparently with ease ; yet the 
 virtuosity be always effective , and always really 
 musical . we have not learn much about the 
 capacity of those four instrument since Mozart ’s 
 time . Schalk conduct the Haydn Symphony per- 
 functorily , and not always with a clear beat : this 
 cause several smudge in the playing , especially at 
 the beginning and end of phrase — all quite unneces- 
 sary with so fine an orchestra . in the Beethoven 
 Symphony , however , which come last , the conductor 
 really wake up ; and though , I think , several 
 passage be needlessly underline , and the pace 
 of the Scherzo be too slow , the performance be 
 thoughtful and impressive 

 the last musical event in the Festival for I be 
 Mozart ’s ' Don Juan , ' in the Stadttheater , a really 
 good production , well accompany by the orchestra 
 under Schalk . the performer on the stage 
 be unequal . outstanding figure be Leporello 
 ( R. Mayr ) , Zerlina ( Adele Kern ) , and Don Juan ( Hans 
 Duhan ) . Adela Kern be as good in ' Don Juan ' as 
 she be in ' Fidelio . ' she have a charming voice , and 
 be a vivacious actress . Richard Mayr show his 
 versatility by be equally at home and effective in 
 such different part as Rocco and Leporello . Hans 
 Duhan have not a very large voice , but he possess a 
 fine , tall , athletic figure , and he look and act his 
 part . the rest of the principal disappoint I . 
 Elvira ’s and Anna ’s elaborate coloratura part 
 sound difficult and laboured . Claire Born ’s and 
 Maria Nemeth ’s voice seem too heavy and 
 cumbrous for a Mozart opera . Alfred Piccaver 
 ( clearly a favourite with most of the audience ) have a 
 big , but inexpressive voice , and his acting as Ottavio 
 be rather wooden . it be perhaps true that Ottavio 
 do not cut a very fine figure in the opera ; but , then , 
 I be not greatly impress with the same actor and 
 singer in ' Fidelio . ' the chorus sing and act well 




 the three CHOIRS FESTIVAL 


 922 


 CONGRESS of the NATIONAL UNION 


 ORGANISTS ’ association 


 of 


 a. rorert 


 1927 923 


 BACH IN BAGANDA - LAND : an impression 


 of the UGANDA JUBILEE 


 1919 . 


 1927 925 


 the NAMIREMBE CATHEDRAL CHOIR 


 LONDON SOCIETY of ORGANISTS 


 926 


 recital 


 appointment 


 the APPOGGIATURA IN RECITATIVE 


 1927 927 


 FRANCIS J 


 WIND or WYND 


 928 


 student ’ counterpoint 


 iwn 


 1927 929 


 the composer and the larynx 


 demand on technique 


 n.w. 1 


 930 


 1927 


 the AMERICAN CHOIRBOY 


 the work of DELIUS 


 NO 


 LL 


 1927 93 


 DIPLOMAS : ANOTHER aspect 


 L.R.A.M. , A.R.C.M 


 HAYDN and a ' tristan ' progression 


 C. H. STEEL 


 PEACE , PERFECT PEACE 


 454 — } ~ 


 932 


 1927 


 sight - reading in sharp or flat 


 CINEMA ORGANS 


 the VOCAL APPOGGIATURA 


 SINSON 


 RADY 


 d. b 


 1927 


 933 


 who be CHILSTON 


 the idea of ' direction ' in WIRELESS 


 MUSIC 


 M. L 


 SANDRINGHAM CHANT BOOK 


 SHAKESPEARE on DOWLAND 


 n.17 


 1927 


 ROYAL ACADEMY of MUSIC 


 ROYAL COLLEGE of MUSIC 


 TRINITY COLLEGE of MUSICThe arrangement for the new term include a number of 
 student ’ concert and lecture . these begin in the first week 
 with the inaugural address of the Rev. W. J. Foxell , chaplain 
 to the College , on the subject of ' music and beauty , ' and 
 end with a choir and chamber music concert at Grotrian 
 Hall and an orchestral concert at Queen ’s Hall . a large 
 number of distribution of diploma and certificate at 
 local centre have be gazette , at which , the College will 
 be represent by Dr. J. C. Bridge , chairman of the Board , 
 Dr. E. F. Horner , director of examination , and the 
 secretary , Mr. C. N. H. Rodwell 

 CHORAL SOCIETY PROG 
 first list 
 LONDON 
 RoyAL CHoraL Society ( Mr. H , L. Balfour and visit 
 conductors).—October 22 , ' Elijah ' ; November 19 , 
 Brahms ’s ' Requiem ' and ' the hymn of Jesus ' ; 
 December 17 , Carols ; January 7 , ' the Messiah ' ; 
 February 4 , ' Hiawatha ' ; March 3 , ' the Dream of 
 Gerontius ' ; April 6 , ' the Messiah ' ; April 28 , 
 Beethoven ’s Mass in D. 
 PHILHARMONIC CHorR ( Mr. C. Kennedy Scott ) . 
 December 14 , ' Antiphon ' ( Vaughan Williams ) ; 
 ' Psalmus Hungaricus ' ( Kodaly ) ; ' Sleepless Dreams ' 
 and ' hey , nonnyno ' ( Smyth ) ; ' Requiem ' ( Brahms ) ; 
 May 16 , ' the mass of Life ' ( Delius 

 RAMMES 




 ALEXANDRA CHORAL and ORCHESTRAL SOCIETY 


 GR 


 WE 


 WI 


 ABE 


 BED 


 SOCIETY 


 1927 935 


 CENTRAL LONDON CHORAL and ORCHESTRAL SOCIETY 


 CRYSTAL PALACE CHORAL and ORCHESTRAL SOCIETY 


 WILLESDEN GREEN and CRICKLEWOOD CHORAL PROVINCIAL 

 ABERDEEN CHORAL and ORCHESTRAL Sociery ( Mr. 
 Willan Swainson).—Beethoven ’s mass in D } ' sing 
 ye to the Lord 

 BatH CHORAL and ORCHESTRAL Socigery ( Mr. H. T. 
 Sims ) . — ' a Tale of Old Japan ' ; ' Elijah ' ; madrigal 




 FAVERSHAM INSTITUTE PHILHARMONICFrome CHORAL Sociery ( Mr. A. M. Porter ) . — ' the 
 Messiah 

 GLASGOW CHORAL and ORCHESTRAL UNION ( Mr. 
 Wilfred Senior).—Beethoven ’s Mass in D ; ' the 
 Messiah ' ; ' the Dream of Gerontius 

 Grays ( Essex ) and DisTricr CHORAL and MUSICAL 
 Society ( Mr. W. H. Fraser).—‘St . Paul ' ; ' the 

 GvuILpFORD CHORAL Society ( Mr. 
 ' Everyman ' ( Walford Davies 

 HaALirFAxX CuHoRAL Society ( Dr. A. C. Tysoe ) . — 
 Beethoven ’s Mass in C ; Bach ’s Magnificat ; ' the 
 Messiah ' ; ' Semele 

 HASTINGS MADRIGAL Society ( Mr. Reginald E. Groves ) . — 
 * Hiawatha ’s Departure ' ; ' Elijah ' ; ' the Messiah 

 LIVERPOOL PHILHARMONIC Society ( Sir Henry Wood ) . 
 — ' Sea Drift ' ( Delius ) ; ' the last four thing of man ' 
 ( Vycpalek ) ( first performance in England 

 LiverPooL WeLtsH CHORAL UNION ( Dr. Hopkin Evans ) . 
 — ' the Blessed Damozel ' ( Debussy ) ; ' the Dream of 
 Gerontius ' ; ' the Messiah ' ; Beethoven ’s Mass in D 

 LONDONDERRY PHILHARMONIC Society ( Mr. Henry 
 Franklin ) . — ' Hiawatha ’s Wedding - Feast ' ; Holst ’s 
 * Psalm 148 




 SWANSEA : CLYDACH HEBRON CHURCH CHORAL SOCIETY 


 SOCIETY of WOMEN MUSICIANSQUEEN ’S HALL PROMENADE concert 

 at the first Beethoven concert of the season , on 
 August 19 , the Symphony be No . 7 . Mr. Leslie England 
 play in the E flat Pianoforte Concerto with spirit , 
 accuracy , and a sense of measure . Mr. Steuart Wilson 
 sing the song - cycle , ' to his absent sweetheart ' ( in a new 
 translation of his own ) , with just and thoughtful expression . 
 Miss Lilian Stiles - Allen sing Clarchen ’s two song from 
 ' Egmont . ' such incidental lyric do not make very 
 satisfactory concert song , but Miss Stiles - Allen show 
 that for sheer quality of tone she be one of our good soprano 

 Arnold Bax ’s symphonic Variations ( August 20 ) make 
 exceptionai fare for a Saturday night audience , very rich 
 and perhaps somewhat confusing . the composer do not 
 deny himself the expression of any fancy that come into 
 his inventive head . the variation be full of charming 
 excursion , and there be beauty wherever we be lead . this 
 be so much to be grant that it seem ungrateful to feel 
 inclined to complain of a lack of general direction in the 
 work , and to be at time cloyed with the heavy pro- 
 fuseness . Miss Harriet Cohen play the exacting piano- 
 forte solo with , we think , a newly increase power 
 and grip 

 Elgar ’s a flat Symphony distinguish the programme 
 on September 8 . the performance be good in a clear- 
 cut way . it miss the wayward feeling , the unexpected 
 happy thought , and the well poetry peculiar to the 
 composer ’s own conducting of the music . new to Queen ’s 
 Hall be an orchestral Fantasy , ' song of the Gael , ' by 
 Mr. B. Walton O’Donnell , who conduct . a heavy and 
 unfeeling , businesslike hand have here grasp a number of 
 old irish tune and make of they a cockney nosegay 

 at the Beethoven concert on September 9 , Mr. Maurice 
 Cole play the C minor Pianoforte Concerto neatly . Mr. 
 Arthur Fear , a promising young bass , sing ' hear I , ye 
 wind and wave . ' it be a good thing that singer be 
 learn to dispense with a sheet or scrap of paper on the 
 platform . Miss Dorothy Helmrich would have be very 
 good indeed if she have keep to the level of her good . but 
 in ' creation ’s hymn ' we hear two or three different 
 voice 

 a set of orchestral variation with Intermezzo , Scherzo , 
 and Finale be conduct by the composer , Mr. Victor 
 Hely - Hutchinson , on September 10 . the theme have the 
 rustic flavour so much appreciate in these day at the 
 R.C.M. the whole composition be attractive and not 
 commonplace . it may have languish at time . moment 
 of hushed intensity be prolong into emptiness . but 
 we regard it as the proof of a real talent , of genuine 
 musical impulse , and delicacy of mind . Dobnanyi ’s now 
 very familiar and popular ' Nursery song ' variation be 
 play on the same night , and Mr. Harold Williams 
 sing well 




 938 


 MANCHESTERThe proportion of british work in a scheme of twenty 
 concert be , it must be say , ridiculously small — Elgar ( ' the 
 kingdom , ' ' Enigma ' variation , and string introduction 
 and Allegro ) , Bryson ( second Symphony ) , Delius ( ' Paris ' ) , 
 and Harrison ( ' prelude Music ' ) . any conductor ’s most 
 formidable difficulty in draft programme be in find 
 small work of , say , ten or twelve minute ’ duration , to 
 lighten the steady succession of large - scale item . with 
 the work he draw from continental source in past season , 
 Sir Hamilton Harty be no more successful than other 
 man in solve this problem . some of our young britisher 
 would have deserve the severe critical trouncing have 
 they serve up such thing as have to be endure last 
 season . a british score of no well quality than those 
 would have be reject merely on reading , much more 
 on playing . be it seriously to be argue that from our own 
 school there can not be find thing more ' worth while 

 among the standard ' classical ' symphony it be 
 interesting to find Harty include Sibelius ’s no . 2 . 
 Brahms and Strauss , who , with Berlioz , provide Harty ’s 
 main enthusiasm , be well represent , and we may 
 congratulate ourselves on ' Heldenleben ' and ' Don Quixote , ' 
 both in one season . last year ’s choral course of four 
 monumental Masses be now to be match by the fullowe 
 work : ' Israel in Egypt ' ; ' Romeo and Juliet ' ( Berlioz ) ; 
 * Fidelio ' ; ' the Kingdom ' ; the Alto Rhapsody of Brahms 
 and the Choral Symphony of Beethoven ; and the ' Meister- 
 singer ' Finale as the season ’s wind - up ( March 15 ) . the 
 chief instrumental soloist engage be Backhaus , Cortot , 
 Godowsky , Pouishnov , Catterall , d’Aranyi , Sammons , 
 Suggia , and Cassado 

 the unusually full choral season outline in the 
 programme of the Hallé series will be augment by 
 numerous concert under Mr. Brand Lane ’s conductorship , 
 culminate in a complete performance of the ' Hiawatha ' 
 trilogy in March . under his zxgi , Manchester will 
 welcome three visit orchestra — the Berlin Philharmonic , 
 under Furtwingler , the London Symphony , under 
 Beecham , and a specially select Waltz Orchestra under 
 Johann Strauss . if the public that usually find its way to 
 the Hallé Band week after week will only use its chance , 
 it will have a capital opportunity of test relative 
 orchestral value which may or may not sustain its opinion 
 of how the Manchester player ' measure up ' against other 
 famous orchestra . on occasion in the past the Hallé 
 habitué have be inconspicuous in the Brand Lane 
 audience , even when Strauss himself come . otherwise 
 the Brand Lane prospectus promise we ' star ' of vary 
 magnitude , but reputedly all of international fame . 
 ’ Appen th ’ lancashire tenor , Tommy Burke , ’ll sing their 
 yed off ! ( pardon this lapse into native doric 




 the HASLEMERE FESTIVAL of ancient 


 CHAMBER MUSIC 


 E. VAN DER STRAETEN 


 the MARGATE FESTIVAL 


 the WEST WALES FESTIVAL 


 HOLLAND 


 SSBach be not so popular here as in England , but Mischa 
 Elman play the variation from the second Sonata with 
 good effect and obtain a big popular success . his other 
 item be the Mendelssohn Concerto and Lalo ’s Symphonie 
 Espagnole 

 the one serious failure of the season be the music 
 compose and arrange by Arthur Honegger to the film 
 ' Napoleon . ' it contain some good idea , but have all the 
 fault of the average ' fitted ' music , and depend too much 
 on borrowing from Honegger ’s own previous music and 
 upon the ' Marseillaise . ' the autumn : season at Amsterdam 
 begin on Sunday , September i1 , with a popular concert 
 conduct by Cornelis Dopper , the programme of which 
 include Beethoven ’s fifth Symphony , Elgar ’s ' Cockaigne ' 
 Overture , and an ' Adagio con Variazioni ' by the dutch 
 composer Theo van der Bijl 

 HERBERT ANTCLIFFE 




 the SALZBURG FESTIVAL 


 PAUL BECHERTthe scrutinise hearer after seven year ’ perennial repetition , 
 but which gladden the box - office man of the Festival 
 Society . one could but compare , with muted sarcasm , 
 the enormous mean place at the disposal of Reinhardt for 
 his own show , with the modest pain which be expend 
 on the production of ' Don Juan ' and ' the Marriage of 
 Figaro . " the singer ( Richard Mayr , Hans Duhan , 
 Alfred Piccaver , Adele Kern , Claire Born , and Maria 
 Németh ) , as well as the conductor ( Franz Schalk and 
 Robert Heger , respectively ) , be those of the Vienna Opera , 
 which imply a high musical standard of performance . 
 but what scenery ! what shabby , old fashioned costume ! 
 what stage management , or the absence of it ! festival 
 performance ? indeed not 

 yet what the Vienna Staatsoper , and the Salzburg 
 Festival spirit with it , can achieve , be conclusively show 
 by the production of ' Fidelio . " of this achievement it be 
 difficult to speak without succumb to the ' pathetic 
 fallacy . " to sum it up , it be the most superb performance 
 that the Vienna Opera have do in decade , and perhaps 
 the great that Beethoven ’s step - child of opera have ever 
 have the privilege to receive . a marvellous orchestra which 
 evoke , after the ' Leonore ' Overture , an ovation seldom 
 witness , when the whole audience rise as one man to 
 shout its thank ; a scenic setting — by Clemens Holzmeister 
 — which catch the spirit of each scene to its innermost 
 subtlety and project it into the grip audience ; a 
 stage management ( the work of Lothar Wallerstein ) that 
 reflect virtually each rhythmic and dynamic shading of 
 the score and create a perfection of ensemble such as the 
 writer have never witness , and which even divest the 
 ominous Prisoners ’ Scene of its Miannergesangverein 
 atmosphere that have seem inseparable from it ; and a cast 
 of singer who , with hardly an exception , be perfect on 
 the musical , histrionic , and emotional side of their part , 
 and even brave the pitfall of the speak dialogue , which 
 usually tempt pathos on one side or utter passiveness 
 on the other . as Leonore , Lotte Lehmann have truly find 
 herself and her dramatic soul ; she be touching in her 
 noble simplicity and flawless in her command of the 
 intricate vocal difficulty . but notwithstanding her 
 masterly portrayal , notwithstanding Alfred Jerger ’s sinister 
 Pizarro , the great individual triumph of the evening 
 be that of Richard Mayr in the role of Rocco , the aged 
 jail - keeper . this great , mature artist be wonderful in 
 his every tone and pose . his touching warmth of voice , 
 his every move and word reflect a pure , clarify 
 humanity ; his be a big heart , a great mind , and a strong , 
 compelling human force . the singer of the evening 
 verily surpass themselves , their work enhance by a scenic 
 setting of unrivalled beauty , a lighting and grouping of 
 unfathomed suggestiveness , and by the musical guidance of 
 Franz Schalk , the great Beethoven conductor . it be a 
 memorable evening , of a perfection that suffice to lend 
 lustre to all the rest of the Festival 

 for the first time this year the Festival programme 
 seek to include choregraphic art in its radius of 
 action . this be achieve by compromise , for the dance 
 programme of the Festival comprise not , as one would 
 have wish , a modern ballet such as perhaps de Falla ’s 
 * three - Cornered Hat ' or Barték ’s ' wonderful Mandarine . ' 
 a modest dance evening suffice to provide the saltatory 
 portion of the Festival , but it be noteworthy for the 
 presence of Harald Kreutzberg . this young dancer , who 
 actually merit the sobriquet bestow upon he of 
 ' German Nijinsky , ' be surely the one dancer of all Europe , 
 outside of Diaghilev ’s troupe , to divest choregraphic 
 art of that odious air of dilettantism with which ambitious 
 semi - professional have recently surround it . the slender 
 young man — light than air — have an infallible technique , 
 a subtle grace season with a welcome virility , and 
 above all a power of facial expression which stamp 
 he a great mimic actor . his * music - less dance ’ be 
 a series of strong mimic impersonation — E. Th . A , 
 Hoffmann or Edgar Allan Poe set into dance . C major 
 scale couple with the tantalizing , monotonous ticking of 
 a merciless metronome , off stage , accompany the first 
 dance — haunting , slur step behind the scene ; and a 
 polytonal , invisible gramophone the other two , his 
 virtuoso dancing , on the other hand , be electrifying and 




 942reply by post 

 C. E. M.—We know of no work deal specially with 
 keyboard writing ; book on composition ( and some on 
 harmony ) usually contain a section treat it . but surely 
 this be a subject that can well be study from the page of 
 the great composer ’ pianoforte music . begin by take , 
 say , the slow movement from Beethoven ’s sonata in G 
 ( Op . 14 , no . 2 ) ; write a simple tune on the same line , and 
 add variation lay out on the plan of this movement . 
 Beethoven be not a model for advanced pianoforte writing , 
 but the Sonatas contain many simple movement on which 
 you may base your study . for write song - accompani . 
 ment , study the collection of Irish Folk - song by Stanford 
 and Charles Wood ( Boosey ) , and devise accompaniment 
 of your own on similar line from other folk - tune . for 
 perfect example of simplicity and effectiveness , see 
 Stanford ’s accompaniment in ' the National Song Book ' 
 ( Boosey ) . Prof. Kitson ’s recently publish ' Elementary 
 Harmony ' ( Oxford University Press ) contain a chapter 
 on accompaniment writing and another on write piano- 
 forte variation 

 H. W. j.—(1 . ) certainly spare - time pianoforte - tuning 
 and repair be not ' degrade to a music - teacher , ' if a 
 teacher can not fill his day with lesson , and have the ability 
 to make a good job of tuning , why should he not do so ? 
 you ask if the practice be general . we think not , few 
 teacher have the ear and skill that make a fine tuner ; and 
 of these few , most would perhaps regard the job as infra dig . 
 ( 2 . ) we should like a straight talk with you ! you be 
 twenty - three , and you aspire to be an organist and teacher . 
 you say : ' I have no diploma ; I do not believe in they . ' 
 your letter be badly - write , ungrammatical , and even the 
 spell be shaky . however much you may know about 
 music , you have a heap to learn in other way , and if you 
 do not humble yourself and learn it , your chance of 
 success as a professional musician be very slight 




 content 


 music . 


 the MUS SICAL time 


 charge for advertisement 


 YN 


 SPECI AL notice 


 THURSDAY 


 1927 


 dure the last month . 


 MANUEL GARCIA ’S 


 art of SINGING . 


 the SINGER ’s art . 


 city of SALFORD . 


 MUSIC in the PARKS 


 SCHOOL MARCHES 


 for PIANOFORTE SOLO 


 BOOK iii . 


 contentslittle March , A . J. D. Davis 

 March from ' * ' Egmont " Beethoven 

 March from ' " ' St. Polycarp " ' ... F. A. G. Ouseley 




 four 


 TENNYSON lyric 


 SET to music by 


 1 . a lullaby . 


 2 . the city child . 


 3 . the reign of the rose . 


 4 . the throstle . 


 sprig of shamrock 


 four old irish air 


 poem by 


 F. W. HARVEY 


 music adapt and arrange by 


 A. HERBERT BREWER