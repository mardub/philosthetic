


 ) TBR 


 ORK . 


 A L 


 WILSON 


 LARK 


 ON 


 SALE , 


 the MUSI 


 AND SINGING - CLASS CIRCULAR 


 CAL TIMES 


 found in 1844 


 publish on the first of every month 


 ROYAL CHORAL SOCIETY . 


 ROYAL ACADEMY of MUSIC . 


 YORK GATE , MARYLEBONE ROAD , LONDON , N.W.1 


 the ROYAL COLLEGE of MUSIC . 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W.7 . 


 A. R. C. M 


 the ROYAL COLLEGE of MUSIC PATRON 'S FUND 


 GUILDHALL SCHOOL of MUSIC . 


 VICTORIA EMBANKMENT , E.C.4 . 


 PRINCIPAL SIR LANDON RONALD 


 SEPTEMBER 1 


 1922 


 ROYAL 


 MANCHESTER COLLEGE of MUSIC 


 BIRMINGHAM & MIDLAND INSTITUTE 


 SCHOOL of MUSIC 


 session 1921 - 1922 


 the PHILHARMONIC CHOIR . 


 the LONDON COLLEGE for CHORISTERS . , 


 602 the MUSICAL 


 TIMES — SEPTEMBER 


 96 , WIMPOLE STREET , w.1 


 TOBIAS MATTHAY 


 PIANOFORTE SCHOOL 


 complete training course for teacher 


 MANCHESTER SCHOOL of MUSIC . 


 ALBERT J A.R.A.M. 


 A POLYTECHNIC 


 BATTERSE 


 LONDON , S.W.1!1 


 class in theory of music , harmony , and 


 counterpoint 


 university course for degree and high 


 examination in MUSIC 


 lesson in solo - singing , pianoforte , and violin 


 preparation for examination of the associate 


 for degree and diploma in MUSIC 


 5 , EAGLE PARADE , BUXTON . 


 UNIVERSITY of MANCHESTER . 


 faculty of MUSIC 


 the techniquer 


 ZACHAREWITSCH 


 A.R.C.O 


 E IDEAL correspondence 


 school of MUSIC . 


 expert tutor 


 theory of MUSIC , harmony , 


 composition , art of teaching , 


 interpretation , 


 | form and memory training 


 for musician 


 6 , BOL TON ROAD 


 the H. AL if AX 


 competitive MUSICAL festival . 


 twenty - five competitive class 


 over £ 200 in prize . 


 MOSSLEY 


 CO - operative choral 


 F ARNW ORTH , BOLTON 


 SOCIETY 


 SATURDAY , NOVEMBER 11 , 1922 


 CHOIR and SOLO competition , 


 £ 30 in prize . 


 WALSH , 


 J. E 


 19 , BERNERS STREET , W.1 : 


 annual congress , GLASGOW . 


 the 


 . HIGH STREET , MARYLEBONE , W.1 


 XUM 


 NS 


 HERS 


 613 


 1922 


 and SINGING - class circular 


 SEPTEMBER I 


 1922 


 MUSIC in the dominion : 


 A talk WITH DR . VOGT 


 HENRY T. FINCK 


 WARREN 


 XUM 


 615significant chord 
 by ALEXANDER BRENT SMITH 
 Ernest Pontifex , the misfortunate hero of 

 the way of all flesh , ' once remark that he 
 do not care for Beethoven , whereupon he be 
 put in his proper place by Miss Skinner , who 
 confess that , for herself , one simple chord of 
 Beethoven be happiness 

 comment upon this passage , Mr. Ernest 
 Newman say : 
 ' Miss Skinner be to be envy . the 

 to find complete happiness in a simple chord 

 of Beethoven — to say nothing of Scriabin or 

 Schonberg 




 ALTO 


 ASS 


 — — EERE 


 MEDTNER 


 I o17 


 22 


 the MUSICAL TIMES — SEPTEMRER 1 


 1922 


 YINETZO . 


 MUSIC at the 


 PASSION 


 OBERAMMERGAU 


 PLAY 


 620 the 


 1922 


 NURSERIES of ENGLISH SONG — ii 


 the 


 1922 6215 


 JOHN CUNNINGHAM 


 622 the MUSICAL time — SEPTEMBER i 1922 


 = — _ study on the HORN 


 LOHENGRIN 


 13 - 44 , 


 624 thehe 

 from time to time we receive reminder that there 
 be still a good many musician who regard the 
 classical composer as having be infallible . a letter 
 have reach we from a welsh reader complain that 
 in the article on the London String Quartet in our 
 August issue ' Mr. Waldo Warner permit himself to 
 remark that Beethoven sometimes write poorish 
 we be sure that Mr. Warner do not 
 pride himself on having make a discovery . he be 
 merely give the mild expression to a view that 
 have be hold by every musician capable of judge 
 — include Beethoven himself , of course . our 
 go on , painedly : ' perhaps Mr. 
 Warner will kindly inform we on what ground he 
 introduce such a remark . I have not as yet come 
 in contact with any of this so - call " poorish 
 music . " ' if this be so , the writer can have buta 
 very slight acquaintance with Beethoven ’s work . 
 we advise he to refer to the A / usical Times of 
 February , March , April , May , and June , 1919 , where 
 he will find the subject pretty thoroughly discuss 
 in article and correspondence . in the June number 
 will be able to ' come in contact ' with some 
 example of music so feeble as to deserve a much 
 strong adjective than ' poorish . ' when he realise 
 that these example be take from work write in 
 Beethoven ’s prime he will perhaps do a bit of 
 thinking . and if he will turn upalso the J / usica/ 
 Zimes for March , 1920 , he will find under the title , 
 ' Beethoven , after a hundred year , ' a very temperate 
 discussion of the present standing of the composer ’s 
 work . these article be not anti - beethoven ; they 

 when he discuss the relation between song - writer | be merely a plea for the exercise of a sane judgment . 
 and composer , and no one could possibly take umbrage | do our correspondent hold that Shakespeare 
 at they , it be impossible not to notice that under- | never write a poorish play or even a poorish passage ? 
 lie his genial satire be a firm and reason | yet Shakespeare be generally regard as a great 

 Calydon , and express the hope that no musician | 
 have set it to music . the musician present be too | 
 polite to inform he that the line have be set to } 
 music by Granville Bantock with most meticulous | 
 respect for Swinburne ’s text 

 another example of the patronise attitude be | 
 the speech of Sir Aston Webb , the president of the| 
 Royal Academy , who speak of the quartet concert | 
 give in the National Gallery , and imagine , | 
 apparently , that he be pay the high possible | 
 tribute to Beethoven when he say that some of his | 
 friend tell he how greatly the music increase their | 
 appreciation of the picture . it be , at good , a left-| 
 handed compliment , although obviously intend to| 
 be very flattering to his musical audience . he would | 
 probably have be both surprised and indignant if | 
 some of the musician present have tell he that they | 
 think that picture , however beautiful , distract 

 attention from the music 

 Bierey , also , who publish the first account of | school , 
 this epoch - make invention , always figure as ' Captain Bierey 

 genius than Beethoven . Homer be _ probably 
 great than either , yet what do the old tag tell we 
 of his inability to be always as wide awake as he 
 should have be 

 our correspondent be nothing if not thorough in his 
 worship . not only have he ' never come in contact 
 with any poorish music ' by Beethoven ; he go the 
 whole hog and declare that he do not think 
 Beethoven ' capable of write anything that can be 
 place in such a category . ' what ! not if he try 
 very hard ? the amusing thing be that having thus 
 burn his boat the writer go on to play for safety 

 by say ' possibly we may trace some portion that 

 may be term /adiffzrent , but to use Mr. Warner ’s 
 remark be do the great musical genius the 
 world have see a gross injustice . ' we make the 
 writer a present of the margin between ' indifferent ' 
 and ' poorish . ' it be too small to argue about 

 we recommend he to carry out the advice with 
 which he end his letter : ' let we engage our- 
 self in study Beethoven ’s work and cease 
 these injudicious criticism . ' let ’s , by all mean . 
 nothing can be well for Beethoven ’s fame than 
 a really discriminating study of his music ; nothing 
 bad than the blind acceptance of everything he 
 write . no man so great as he be need ask to be 
 save from judicious critic , but he may well pray 
 to be save from his friend 

 in our issue for January last we comment on a 
 remarkable performance of Zhe Messiah at Oundle 
 in which every boy take part . we now learn 
 that Mr. Clement Spurling , the indefatigable music 




 XUM 


 the NATIONAL EISTEDDFOD 


 SIR HUGH ALLEN ’S praise 


 626 the MUSICAL 


 TIMES — SEPTEMBER 1Griffiths , of Aberpennar , that , give proper oppor- 
 tunitie , he would develop into an orchestral leader 
 of high rank . when we remember the very mediocre 
 playing of last year ( although that be a great 
 improvement on previous year ) , it be almost 
 impossible to realise that the same country could 
 have produce a band like that from Aberpennar , 
 which win the prize 

 the test - piece be the slow movement from 
 Beethoven ’s seventh Symphony and Percy E.Fletcher ’s 
 Rustic Suite . as it be decide by the judge that 
 the Beethoven movement be too delicate a piece 
 so large a hall , at the final 

 to be play in 

 be explain when the mark be publish , and it 

 be find that the Aberpennar orchestra have 
 gain ninety - five mark for the Beethoven , and that 
 Mr. Ware ’s band have gain more mark than 
 the win orchestra for the playing of the Fletcher 
 the playing of both band be not only 

 piece 




 voice choir 


 the CONTROVERSIESof 

 the onslaught of ' the formalist . ' ( how beautifully 
 | free — in contrast , say , to the later Beethoven — 
 be the form of Nazareth 

 pernicious BACH 

 ne particularly misguided writer , who will prob- 
 | . uly live to repent it , even object to the inclusion in 
 | the programme of Bach ’s 5 minor massand Brahms ’s 
 | Reguiem . wheare he speak , one would imagine 
 | that Wales have suffer from a surfeit of Bach and 
 | Brahms ; but , as a fact , this be the first per- 
 formance in Wales of the Mass and ( I believe ) the 
 | second of the Aeguiem . he object to the latin 
 | word of the Mass. I be glad to hear that it be 
 | be translate into Welsh , for it be always good 
 |that people should understand what they be 
 singe . it be urge that Wales could learn 
 | more from Tchaikovsky , Debussy , and Ravel . she 
 | might learn a good deal , but how can any one even 

 to the initiative of the enthusiastic Welshman who| begin to understand their true significance without 
 be its conductor . the test - piece be Cyril Jenkins ’s | know their predecessor ? and how many people 
 set of Sea - Fever , and Dr. Joseph Parry’s|in Wales be there who know their Bach , Haydn , 
 Nazareth . the juxtaposition be piquant , since Mr. | Mozart , Beethoven , or Wagner ? it be not their fault , 
 Jenkins have recently get into very hot water by his } but if they think it be an advantage they be vastly 
 unspare condemnation of Parry ’s music , which| mistaken . they speak glibly of the ' Russian Five , ' 
 most Welshmen be incline to worship indis-| quite forget that at Moscow and Petrograd people 
 criminately . speak for myself , I can only say | have opportunity for know the good of all music , 
 that if the rest of Dr. Parry ’s music be like Mazare / h,| which be ( unfortunately ) deny to Cardiff and 
 it deserve all that Mr. Jenkins have say about it.| Swansea . one writer proudly say that a country 
 this feeble mixture of Mendelssohn and Taff water| which could produce a Walford Davies and a 

 sugary in its sentiment and flippant in   its| Vaughan Williams should take a front place among 
 exultation — be an echo of a past happily almost|the musical nation of the world . I heartily agree 




 XUM 


 the other competition 


 the concert 


 MR . R. GWILYM JONES 


 628 


 IN LIGHTER VEIN 


 A. K 


 the PROMENADE concert 


 the SALZBURG FESTIVAL 


 international chamber concert 


 H. G 


 the 


 1922 629 


 630 the 


 4 { BURGER 


 ITHE 


 the MOZART cycle 


 XUM 


 a new theory 


 old composer 


 new information on 


 ON various MODERN composer 


 M.-D. CALVOCORESSI 


 a real APPRECIATION course 


 MAC - DOWELL 


 XUM 


 SS = = — 


 SS — 


 GAS | 


 NS 


 2 SS SS SSS ES SS = SS 


 _ 78 


 XUM 


 PROUD MAISIE 


 SS 


 6 PSS SE SS== SSS : 


 S | 2 SS SS ES SS 


 ~ — — : 4 ; = = = 1 


 0 7 | |—| 


 _ — — — — — — _ : — — — — — — _ — _ — — > > 


 XUM 


 LA A — — — 


 SSS 


 (= SB SS + 


 — _ PP ~ 


 1 = — — — — — _ 


 XUM 


 639 


 1922 


 BIRD MUSIC 


 A. B. S 


 CHORAL MUSIC 


 640 


 1922 


 SONGS 


 PIANOFORTE MUSIC 


 CHAMBER MUSIC 


 XUM 


 H. G 


 H. G 


 641 


 ORCHESTRAL SCORES 


 642 the 


 MUSICAL TIMES — SEPTEMBER 


 YO 


 H. G 


 STRING MUSIC 


 BY ' * DISCUS 


 XUM 


 643 


 1922 


 the SILBERMANN ORGANS at DRESDEN 


 ALL HALLOWS ’ , LOMBARD STREET 


 ROYAL COLLEGE of ORGANISTS 


 annual general meeting 


 fifty - eighth annual . report 


 XUM 


 E. T 


 O. D. 


 645 


 IND organist ’ success 


 HYMN service at YORK MINSTER 


 SIX 


 ROCHESTER DIOCESAN CHOIR FESTIVAI 


 ORGAN recital 


 XUM 


 DID VIADANA USE figure 


 F. T. ARNOLD 


 OUR decadence 


 ( 1600 ) , 


 ( 1700 ) , after carefully read his article I gather that he be quite 
 willing to admit the romantic element , but only on certain 
 condition — that formal beauty and euphony must always 
 predominate ; that music must be able to stand by itself as a 
 concourse of sweet sound in well - order design , able to 
 give pleasure and meaning irrespective of its association 
 with word or a dramatic setting . in short , Mr. Boughton 
 say ' we may swear — but please swear like gentleman ! ' 
 we must call the trumpeter ' naughty , ' not anything else — 
 we must not use Mr. Shaw ’s vulgar stage word 

 Mr. Boughton contend that prior to Beethoven ' under 
 the most vehement stress the music be never allow to 

 the 




 650the term ' euphony ' and ' cacophony ' be surely relative 
 only 

 Mr. Boughton also insist on the impersonality of a 
 composer ’s work , and condemn Beethoven for seek 
 ' self - expression rather than the service of musical art . ' 
 one feel bind to say that if this be true , then musical 
 art be profoundly grateful to Beethoven for his waywardness 

 I agree entirely with one of our foremost dramatic critic 
 who write the other day : ' there be no such thing as 
 nal work any intelligent person ought to be 
 able to draw a fairly faithful portrait of Shakespeare or 
 Molicre from read their play . ' be it not a very great 
 compliment to Mozart with regard to his Regutem to read 
 this : ' I be unable to say which movement be leave 
 unwritten by the master himself ' ? we may also ( especially 
 after hear Mozart ’s Aegutem fairly recently ) disagree 
 with Mr. Boughton over its excellence as a work of art . 
 if this be a ' self - stand piece of music , ' it do not do 
 much to strengthen his argument . as a piece of charac- 
 teristic expression of most beautiful and solemn word , it be 
 a travesty and nothing else . again , say Mr. Boughton , 
 : the music set to verse or to a dance must be a self- 
 stand piece of work . ' do Mr. Boughton really mean 
 us to take this seriously , or be he write with his tongue in 
 his cheek ? do the expression of the word and spirit of a 
 song count for nothing ? if this principle be adopt we 
 shall bad diction than ever . be we to judge 
 Wagner ’s music - drama apart from scenery , action , and 
 general theatrical setting ? isa sacred work to be equally 
 impressive alike in the concert - hall and cathedral ? surely 
 Mr. Boughton be here press his ideal a little too far 




 LIBRETTI IN ENGLISH : an appeal for 


 SANE translation 


 XUM 


 att 


 for 


 SS 


 want — composition for WIND 


 instrument 


 the MUSICAL 


 TIMES — SEPTEMBER I 


 1922 


 woe of a music - master 


 AN EARLY GREEK HYMN 


 WC 


 E. A. WHITE 


 ( SS SS 


 which language 


 XUM 


 HITE 


 the 


 SIR HENRY BISHOP ’s first wife 


 W. 9BURNLEY.—The new organ at the West Gate Congrega 

 tional Chapel be open on July 30 by Mr. Best , organist 
 of St. George ’s Hall , Liverpool . several excellent 
 composition be play by Mr. Best , in admirable style , 
 include the Andante from Haydn ’s ( Quartet in F ; an 
 Andantino from Spohr ’s Symphony , ' the power of 
 Sound ' ; and the grand ' hallelujah ' chorus from 
 Beethoven ’s Znged 

 EcCCLESIOLOGICAL Socitery.—A concert take place at 
 the room of the Architectura ! Union on July 30 , at which 
 the motet choir of the Ecclesiological Society perform a 
 selection of ancient Church music . the principal feature of 
 the programme be Palestrina ’s Mass , which be sing in 
 Latin , and be render much more effective by the versicle 
 and prayer , always say by the priest , be chant by the 
 Rev. Mr. Helmore . the music of Palestrina be wonderfully 
 elaborate , and though there be not three bar of melody 
 throughout , it be a most interesting Mass , contain as it 




 ROYAL COLLEGE of MUSIC 


 ABERYS1 


 yth the 

 the composer ) , ( uilter ’s Chz / dren ’s Overture , a Suite of 
 Rameau , orchestrate by Dukas , the Brahms Double ! 
 Concerto for violin and violoncello , Beethoven ’s fifth 

 Symphony , Mozart ’s G minor Symphony , Elgar ’s Dream 

 HARROGATE.—At the symphony concert on June 29 , 
 Esser ’s orchestral arrangement of 
 in F be the chief item , Mr. Iloward Carr conduct 

 MANCHESTER,.—At the Tuesday noon chamber concert on 
 July 11 , four student of the Royal College of Music play 
 Brahms ’s G minor Pianoforte ( Juartet . on July 18 singing 
 pupil at the same College , under Mr. Richard Evans , 
 provide the programme.——At the Tuesday Mid - day 
 concert on July 25 , Mr. Maurice Cole play pianoforte 
 music by Beethoven , Mozart , and Chopin . — — a novelty 
 be introduce on August i , in the combination of music 
 and the speak word , by Miss Amy Buxton Nowell ( reciter ) 
 and Mr. Eric Fogg ( pianist ) . the ballad , Fatr Hedwig 
 and 7he Heather Youth , and Shelley ’s 7he Fugitives , be 
 arrange to music bySchumann ; 7he 7rum / feter ’s Betrothed 
 by Francois Thorne ( if english ) , and Victor Hugo ’s Za 
 fiancée du timbalier , be also give . Miss Olive D. Murphy 
 sing song by Brahms and Ilugo Wolf 

 NEWCASTLE,.—yOpen - air concert be give on August 8 
 by the Musical Union , unite military band provide the 
 programme in the afternoon , assist by Newcastle Glee- 
 men . in the evening the Festival Choirs , augment by 
 voice from the roman catholic choir , sing Holst ’s 7urn 
 hack , o man , Burke ’s St. Patri Prayer , and oratorio 
 chorus . the roman catholic choir sing the Kyrie and 
 Gloria from Gounod ’s .J / issa Paschalis , and an orchestra 
 play 




 XUM 


 JOHN 


 1922 655 


 MUSIC in IRELAND 


 BERLIOZ in italycomposer . Mendelssohn write 

 Berlioz be a caricature without the shadow of genius , 
 always grope in the dark , and esteem himself a 
 creator of a new world of art . with this idea he write 
 the most detestable stuff , and can speak of no one else 
 but Beethoven , Schiller , and Goethe . he be vain to an 
 inconceivable degree , and treat with the utmost 
 disdain Mozart and Haydn , so that his great enthusiasm 
 for the other be not to I very convincing 

 distrustful and agitated in mind , because his Camille 
 do not write to he , Berlioz decide after only a month at 




 656 the MUSICAL 


 XUMGERMANY 

 there be no need for ' Russil , ' a Russian Society for the 
 dissemination abroad of russian music and _ literature . 
 good russian music have always be appreciate in 
 Germany , and when ' Russil ' with the Philharmonic 
 Society under Pomeranzeff organize a series of concert it 
 sught to have know that only the good of russian music 
 must be submit , as there be already plenty of the mediocre 
 variety in Germany . moreover , the conductor be too fond of 
 exaggeration , of rubato , of brass and drum . the sole 
 attraction of these concert be the reappearance of Kloti 
 in Tchaikovsky ’s Concerto in b flat minor , whom some 
 year ago rumour have declare a victim of the russian 
 revolution . neither Kloti nor Tepito Arriola , who in the 
 Beethovensaal give a concert of classical and modern music , 
 have retain their former excellence . 
 prodigy of marvellous ability , have become a good pianist 
 with great technique — but the marvel have disappear 

 Alexander Borowsky , another foreign pianist , prove him- 
 self in composition akin to his temperament — such as a 
 Gigue by Loeilly , Preludes by Rachmaninov , Prokofiev , 
 and Scriabin — a great and convincing artist , but like many 
 foreigner , he seem to play Bach and Mozart with a lack 
 of purpose 




 VOLKSMUSIK 


 musical festival at DUISBURGRHEINHESSISCHE MUSIKWOCHE 

 in order to prove that the inhabitant of the occupied 
 district be not forget by mother Germania , the 
 Darmstadt Landesorchester under Generalmusikdirektor 
 Michael Balling undertake a musical tour , visit Mayence , 
 Worms , Bingen , Alzey , Oppenheim , Nieder - Ingelheim , 
 and Worrstadt . Mayence be to have two concert , 
 |the second of which ( Beethoven ’s No . 6 and Strauss ’s 
 Alpen - Sinfonie ) have , owe to the excitement attendant upon 
 the murder of Rathenau , to be abandon . the programme 
 of the first concert contain Bruckner ’s Symphony No . 3 , 
 Mozart ’s No . 41 , and Strauss ’s Dow /uan . the Wormer 
 listen to Rudi Stephan ’s J / usth fiir Orchester , and 
 Bruckner ’s No . 3 . at Bingen the orchestra play Strauss ’s 
 Don Juan and Brahms ’s no . 1 . at Oppenheim and Alzey , 
 where the concert take place in the church , the programme 
 include the prelude to Lohengrin , the Adagio from the ninth 
 Symphony , Johanna Senfzer ’s Sonata for two violin and 
 small orchestra , and Strauss ’s Zod und Verkldrung . Nieder- 
 Ingelheim and Worrstadt , with their rural population , be 
 supply with popular music excerpt from the A//dsummer 
 Night ’s Dream , Johann Strauss ’s Aa?ser- Walzer , and the 
 Overture to Avencz . in addition to the above , Frau 
 Johanna Hesse ( soprano ) sing in the small town 
 appropriate song , Frau Kuhn - Liebel sing Volkslieder with 
 |harp , by Friedrich Briickmann , and Konzertmeister 
 | Andreae play the Concertos for violoncello with orchestra , 
 | by d’Albert and Haydn . the tour be a great success , 
 | concert - hall and church be fill to the door 

 MAX REGER - ARCHI 




 a NEW GERMAN OPERA 


 F. ERCKMANN 


 658 


 NEW yorkfriend and other who believe in his enterprise and wish to 
 encourage it 

 the band number sixty player . the same face appear 
 mn the platform year after year . the devotion and loyalty 
 » f Mr. Goldman ’s force be unbounded , and they be deaf 
 to all offer for other Summer work , and equally deaf to all 
 attempt to make they dissatisfied with any plan or 
 condition Mr. Goldman impose . as a large proportion 
 of the music play be write for full orchestra , naturally 
 the clarinet predominate , the part score for first and 
 second violin be give tothem . out of the sixty man , 
 twenty - three play the clarinet . the part write for the 
 ther string be divide between wood - wind and brass 
 in various way . when Mr. Goldman 
 publish transcription for band of orchestral composition , 
 and of these he say that the english example be far the 
 good . many thing that he wish to play be not publish , 
 und these he have specially arrange . music originally 
 write for a band be also on every programme in a less 
 legree , and be plave for encore . every year the classical 
 * xcerpt be play more anc more , and every year there be 
 nore demand for they , as Mr. Goldman continually call 
 for ' request ' programme , when the conspicuous 
 name be those of Beethoven , Wagner , and Tchaikovsky . 
 it be Mr. Goldman ’s aim — as it be that of Theodore 
 Thomas in his famous Central Park Garden Summer 
 concert inaugurate more than fifty year ago — to cultivate 
 the taste of the people , and make they prefer the good and ask 
 for it . the large audience ever see on Columbia Green 
 be there one night early this season , when the prominent 
 name on the programme be that of Beethoven . the 
 Overture Egm the 
 Symphony , and Zeon No . 3 be play to attentive 
 listener , and be follow by deafening applause . 
 if it argue that justice can not be do to 
 composition without string , the answer be that 
 who have not be habitué of the Winter Concert 
 become recruit , go there for the first time in their life 
 to hear the great orchestra play the composition they have 
 learn to love by be introduce to they by Mr. Goldman ’s 
 band 

 possible , use 




 PARIS 


 XUM 


 FLINT 


 659 


 ROME 


 AN important discovery of UNEDITED and 


 unknown MI 


 LEONARD PEYTON 


 dure the last month . 


 REWER , A 


 ISTER 


 JY ENNINGTON - BI ( 


 HAMMOND , 


 J. SHARP 


 publish FOR 


 the H. W. GRAY CO . , NEW YORK 


 CORRESPONDENCE 


 RGAN MUSIC for sale.- 


 TUITION 


 XUM