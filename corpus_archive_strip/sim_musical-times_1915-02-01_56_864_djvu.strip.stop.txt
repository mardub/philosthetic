


 NG 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 FOUNDED 1844 


 PUBLISHED 


 


 MONTH 


 ROYAL CHORAL SOCIETY 


 ROYAL ALBERT HALL 


 SATURDAY AFTERNOON , FEBRUARY 6 , 3 . 


 MISS AGNES NICHOLLS . 


 MR . JOHN COATES . MR . THORPE BATES 


 SATURDAY AFTERNOON , FEBRUARY 27 , 3 . 


 “ DREAM GERONTIUS ” 


 ( ELGAR 


 MADAME CLARA BUTT . 


 MR . GERVASE ELWES . | MR . ROBERT RADFORD 


 BAND CHORUS 


 


 ROYAL ACADEMY MUSIC , 


 YORK GATE , MARYLEBONE ROAD LONDON , N.W 


 LENT HALF - TERM BEGINS THURSDAY , FEBRUARY 138 . 


 ROYAL COLLEGE MUSIC , 


 PRINCE CONSORT ROAD , SOUTH KENSINGTON , S.W. 


 ROYAL COLLEGE ORGANISTS . 


 GUILDHALL SCHOOL MUSIC 


 JOHN CARPENTER ST . , VICTORIA EMBANKMENT , E 


 OPERA 


 WEEKLY ORCHESTRAL PRACTICES CONDUCTED 


 ROYAL 


 MANCHESTER COLLEGE MUSIC 


 BIRMINGHAM & MIDLAND INSTITUTE . 


 SCHOOL MUSIC 


 SESSION 1914 - 1915 


 MANCHESTER SCHOOL MUSIC 


 UNIVERSITY DURHAM . 


 GLASGOW 


 CHORAL ( COMPETITION ) FESTIVAL 


 SATURDAY , , 1915 


 FEMALE - VOICE CHOIRS 


 JUARTETS . 


 66 


 MUSICAL TIMES.—FEBRUARY 


 , IQ15 


 37 , INDERWICK ROAD , STROUD GREEN , 


 LONDON , N 


 NATIONAL CONSERVATOIRE 


 INCORPORATED GUILD CHURCH 


 MUSICIANS 


 ASSOCIATE ( A.1.G.C.M. ) , LICENTIATE ( 1.1.G.C.M. ) , FEL- 


 REGISTER ORGAN VACANCIES . 


 VICTORIA COLLEGE MUSIC , 


 LONDON . 


 INCORPORATED 1861 . 


 LONDON COLLEGE MUSIC , 


 GREAT MARLBOROUGH STREET , LONDON , W 


 BOARD EXAMINATION . 


 F.R.AM 


 -C.0 


 EXAMINATIONS — LOCAL HIGHER 


 MR . W. H. BREARE 


 STUDENTS PROFESSION . 


 PIANO PEDALS 


 - ORGANIST 


 NORMAN & BEARD ‘ 


 PNEUMATIC PEDAL ATTACHMENT 


 PIANO 


 1 . SCHOOL 


 [ NGING 


 D 


 MENT 


 ONDON , W 


 67 


 ROYAL ACADEMY MUSIC 


 SINGING 


 PIANOFORTE PLAYING 


 ROYAL ACADEMY MUSIC . 


 ORGAN PLAYING 


 VIOLIN PLAYING . 


 ‘ CELLO PLAYING . 


 ROYAL ACADEMY MUSIC 


 CHRISTMAS , 1914 


 OLD FIRM 


 P. CONACHER & CO , LTD . , 


 SPRINGWOOD WORKS , 


 HUDDERSFIELD . 


 GOLD MEDALS 


 GRAY & DAVISON 


 ORGAN BUILDERS , 


 PRATT STREET , N.W 


 48 , SOUTHFIELD ROAD , OXFORD . 


 128 , HOLT ROAD , LIVERPOOL 


 RECENTLY PUBLISHED 


 


 DREAM GERONTIUS 


 CARDINAL NEWMAN 


 SET MUSIC 


 CHORUS , ORCHESTRA 


 


 EDWARD ELGAR 


 SOLI 


 MINIATURE SCORE , 


 PETERS EDITION 


 68 


 , 1915 


 PERFECT TECHNIQUE 


 MACDONALD SMITH SYSTEM 


 PIANOFORTE TOUCH TECHNIQUE 


 M. MACDONALD SMITH , 


 19 , BLOOMSBURY SQUARE , LONDON , W.C 


 SIGHT - PLAYING 


 EASY 


 L. M. EHREMAYER , 


 PROFESSIONAL NOTICES 


 MR . SAMUEL MASTERS 


 ( TENOR ) . \ 


 MR . 7 


 CHARLESWORTH GEORGE } < ‘ 


 ( BASS - BARITONE ) . 


 MISS ETHEL VISICK D 


 LONDON COLLEGE CHORISTERS 7 


 COMPOSERS ’ MSS . . 


 F.R.C 


 REVISION MUSICAL COMPOSITIONS 7 


 10 


 F.R.C.O. SPECIALIST CORRESPONDENG 


 CHO 


 RS 


 YRGE 


 7 K 


 JITION 


 ISTERS 


 ICE GIVES 


 LGAR , O.M 


 LULL 


 69 


 L.R.A.M. ( PAPER WORBK 


 LATEST SUCCESSES : — 


 A.R.C.M. ( P 


 PAPER WORK 


 DR . LEWIS ’ TEXT - BOOKS : 


 POSITION 


 R. H. SCOTT- BAKER , ARAM , L.R.A.M 


 ISS MARGARET YOUNG , L.R.A.M.,A.R.C.M. 


 DURHAM CATHEDRAL 


 6 F.R.C. 


 6 F.R.C. 


 10 F.R.C. 


 12 F.R.C 


 N EN ( TENOR “ ARITONE ) WANTED . 


 IANOFORTE VOCAL TEACHIN 


 ORIGINAL TUNES | 


 CATHOLIC HYMNS 


 FRANCIS EDWARD GLADSTONE . 


 


 , 1915 


 ORGAN 


 WALTER G. ALCOCK 


 M.V.O. , 


 A.R.C.M. , F.R.C.O 


 ATHENAUM 


 YORKSHIRE POST 


 YORKSHIRE OBSERVER . 


 SCOTSMAN . , 


 BRISTOL TIMES MIRROR 


 WESTERN MORNING NEWS 


 SUNDAY TIMES . 


 CHOIR , 7 


 MUSIC STUDENT 


 BOOKSELLER 


 CHURCH FAMILY NEWSPAPER . 


 : NOVELLO COMPANY 


 12 


 POPULAR MARCHES 


 


 ORGAN 


 VOLUME 2 


 VOLUME 4 


 TI 


 ES NOVELLO HANDBOOKS MUSICIANS 


 EDITED ERNEST NEWMAN 


 CHORAL TECHNIQUE & INTERPRETATION 


 PLAINSONG 


 


 MUSIC BIBLE 


 WRITTEN COMPILED 


 MRS . G. T. KIMMINS 


 MUSICAL TIMES 


 FEBRUARY , I9QI5 


 BAMBRIDGE , F.T.C.L. , F.R.A.M. 


 CV.G. , 


 NOVELLO ' 


 EDITIONS 


 


 FAVOURITE OPERAS 


 CONCERT USE 


 1 . FAUST 


 2 . IL TROVATORE 


 ANNHAUSER . 


 4 . MARITANA 


 5 . BOHEMIAN GIRL . 


 6 . DAUGHTER 


 REGIMENT 


 NOVELLO 


 BROADWOOD 


 PIANOS 


 BOSWORTH & CO . 


 MUSIC TEACHERS 


 GUIDES 


 MUSIC MERIT 


 CLASSIFIED GRADED 


 READY REFERENCE 


 MUSIC PROFESSION . 


 SPINDLER , TRAVERS , LIFTL , NORDEN , ALETTER 


 BOSWORTH & CO 


 8 , HEDDON STREET , REGENT STREET , LONDON , W. 


 SOLEMN MELODY 


 COMPOSED 


 H. WALFORD DAVIES 


 ORCHESTRA 


 STRINGS ORGAN 


 CE 


 3URLITT , 


 DON , W. 


 METHODS 


 SINGING - CLASS CIRCULAR . 


 FEBRUARY 1 , 1915 


 REFLECTIONS 


 MUSICAL CRITIC . 


 ENGLISH 


 NOVELLO EDITIONS 


 FAVOURITE OPERAS 


 CONCERT USE 


 1 . FAUST . 


 2 . IL TROVATORE . 


 TANNHAUSER . 


 YS 


 4 . MARITANA . 


 5 . BOHEMIAN GIRL . 


 6 . DAUGHTER REGIMENT . 


 BROADWOO 


 PIANOS 


 GUIDES 


 CLASSIFIED GRADED 


 MUSIC PROFESSION 


 BOSWORTH & CO 


 MUSIC TEACHERS 


 MUSIC MERIT 


 READY REFERENCE 


 8 , HEDDON STREET , REGENT STREET , LONDON , W. 


 COMPOSED 


 H. WALFORD DAVIES 


 ORCHESTRA 


 STRINGS ORGAN 


 ORGAN 


 VIOLONCELLO PIANOFORTE 


 SOLEMN MELODY 


 74 


 , 1915 


 REFLECTIONS REFRACTED : _ REPLY 


 ‘ NATIVE COMPOSER . ’ 


 75 


 76 


 , 1915 . _ — _ 


 , 1915 . 77 


 QUESTION BIRMINGHAM 


 FESTIVAL 


 78 


 80 


 , I9I5 


 SMALL ORCHESTRAS . LARGE 
 NICHOLAS GATTY 

 great pity recognise 
 clearly essential difference large 
 small orchestra . involved 
 mere question numbers , properly 
 understood longer perpetrated 
 tonal outrages ‘ Festival ’ performances , 
 - called , Beethoven symphonies like . 
 modern large orchestra came interpret 
 adequately modern music , style writing 
 , place , entirely changed 
 gone ; , second , fresh 
 instruments permanently introduced , 
 integral tonal scheme . newer 
 orchestration involved increase number 
 strings obtain right balance tone , reason 
 wind instruments , 
 augmented number , written 
 penetrating manner . magnificent 
 organization set play music older style 
 thought sufficient double wind 
 parts hope getting good balance , 
 entirely forgotten scoring absolutely 
 different character . result hideous 
 strenuousness expression painful forced tone 
 wind desperate effort 
 dynamic volume sound strings 

 Observe , , point missed , 
 proved ‘ faking ’ adopted Mr. Henri 
 Verbrugghen Beethoven Festival 
 Queen Hall April . worked plan 
 reducing number strings soft passages . 
 reduced loud 
 reason proceeding . Surely 
 sufficiently obvious large string orchestra 
 play softly tosatisfy . matters , 
 wind play loud 
 destroying quality , , typical instrumental 
 passages period found 
 Beethoven scores . view 
 purist desires historically 
 exact possible ; simply wsthetic reasons 
 absurd practice hoping compete 
 huge body strings means doubled wind 
 greatly deplored . course denied 
 moment Beethoven string 
 passages sound truly splendid delivered 
 large body players control expert , 
 requires moment thought realise 
 takes period 
 equally fine effects Bach Handel . Sir 
 Frederick Bridge performs ‘ Messiah ’ 
 Handel scoring , hesitate 
 employ number oboes bassoons , fact 
 happened Handel time . old masters 
 scored differently Beethoven , Mozart , 
 Schubert , Wagner Richard 
 Strauss 

 thing forgotten 
 connection climax dynamic force 
 purely relative thing . high - lights 
 impressive small large 
 orchestra ; mere numbers , awsthetic stand- 
 point , got matter . 
 Probably evil procedure vogue present owes 
 inception Wagner , hesitate 
 alter scoring Choral symphony — 
 hear alterations Scherzo , new 
 horn parts especially , great regret . Fortunately 
 version 




 COMPOSER RUSSIAN 


 NATIONAL ANTHEM 


 MONTAGU - NATHAN 


 USSIAN 


 83 


 84 


 ILLUSTRATIONS HISTORY 


 MUSIC PRINTING LIBRARY 


 MR . ALFRED H. LITTLETON 


 ES.—FEBRUARY , I9QI5 . 85 


 MUSICAL TIM 


 86 


 COPYRIGHT ACT , 1911 . 


 I.—IMPERIAL COPYRIGHT . 


 RIGHTS 


 INFRINGEMENT COPYRIGHT . 


 BB 


 TERM COPYRIGHT 


 COMPULSORY 


 LICENCES 


 PROVISIONS MECHANICAL INSTRUMENTS 


 U MENTS 


 87 


 EXISTING WORKS 


 APPLICATION BRITISH POSSESSIONS 


 SCHEDULE 


 EXISTING RIGHTS 


 INTERNATIONAL COPYRIGHT . 


 


 , 1915 


 SUPPLEMENTAL PROVISIONS 


 COMPLEAT ORGANIST . 


 XII . — INSTRUMENTpassages organ sound like , § fut 
 hopeless muddle ! Old Haydn , lived - day § fie 
 need pains compose hs — lan ) 
 extremely mild ‘ Representation chaos . ’ — fate 
 saved time , convincing , § 
 written rapid passage - work directed » — usu 
 played organ couplers . find 
 O ar afte 
 rganicus.—But — 
 Musicus.—Exactly ! going point ox 
 fault occasionally lies player 
 true , admit . faul 
 composer , writes ‘ organ ’ 
 passage effective half tk 
 stops . example , playing rapid peda 
 solo . ‘ pedal organ , ’ says composer . yu 
 shoot pedal stops , start mad 
 career . , pedal includes 32 - ft , ; 
 heavy booming 16 - ft . open , hoarse 16 - ft . reed 
 pedal stops , manuak 
 played couplers , loyally 
 . semiquavers th 
 bigger pipes heaviest pedal stops ? _ Lister 
 carefully , hear intermittent hoarse noise H 
 — spasmodic coughs underworld , ¢ | 
 monster pain . players 

 reserve slow - speaking heavy stops suitable 
 passages ? admit effectiveness holdin 
 long , relentless pedal - point , delivering 
 grandly - stalking / firmus . composer fr 
 orchestra gave tuba bass - trombone 
 rapid passages unison bass strings ? Eve 
 16 - ft . tone strings rarely called 
 sprint , save special effects . suppos 
 Beethoven double - basses 
 giving fughetta subject th 
 Scherzo C minor Symphony wished 
 passage clear effect purely musical ! 
 bit ! audiences smile th 
 elephantine capers basses point ? Eithe 
 Beethoven meant smile 
 rough jokes , miscalculation . thinkwe 
 know Beethoven decide 5 , 
 case . organists play pedal passages- 
 rapid Beethoven passage , admit 
 quick clear effect — mos 
 lumbering stops drawn , seriously think effec 
 satisfactory . Similarly , play freséo passagt 
 Great , Swell , coupled , whid 
 means doubles reeds included . 5 
 pretty orchestral composer dire 
 strings , wood - wind , brass playa brillias ! 
 cadenza unison 

 Organicus.—It 




 89 


 SSS 


 ORGAN CENTENARY HALL , 


 BRESLAU 


 187 


 94 


 88 


 93 


 ORGAN RECITALS 


 APPOINTMENTS 


 SONGS , parts 

 American cinema picture day ) d 
 , happy ? 
 Mr. Turner letter subject consecutive fifths ist 
 gallant effort good face bad job , afraid 
 help earnest young composer 0 
 open letter addressed . problem , 
 know legitimate consecutive fifths illegitimate ones 
 late Dr. Prout lays , example , ‘ consect 
 tive fifths simultaneous motion allowed betwee 
 categorical . Equally 
 categorical practice composers , great littl 
 Dr. Prout proceeds quotations 
 Beethoven , Haydn , Kullak , use consecutive perfect 
 fifths simultaneous motion parts jus 
 suits . - 
 Dr. Prout attempt difficulty ’ 
 ‘ examples , ’ says , 
 imitation ; experience required understand 
 properly introduced ; needful mentia 
 sake completeness . beginners tht 
 prohibition consecutive fifths strictly attended 
 , reason , sequence occurs 

 student 

 95 

 ae , 
 student quoted Dr. Prout , 
 cat fifths ? sound Kullak puts 
 MES . ’ paper , sound ill paper 
 month j young student better music 
 nail gxteen Kullak ? ‘ Experience , ’ says 
 ) oblige tt e , ‘ required understand 
 » Teacher ’ ly introduced . ’ kind . 
 aan ? s pon thing makes sensible musician think ill 
 oser — mad consecutive fifths sound — talk 
 ow . ’ ‘ Ah EE sense nonsense ; student natural gift 
 100d - dgy | talking sense music use fifths rational way 
 ocal fanene ftom beginning . excuse pedagogue 
 stened : th | bre striking ? gone Dr. Prout 
 w , exercise containing following passage 
 g manly ime bave blue - pencilled till hardly recognisable : 
 erfect ? 
 leasant notes 
 | occurred ty 
 know 
 st ‘ critics ’ ip 
 -S - S ” 
 ain stom 
 nal insanity , 
 ¢ ‘ inexperienced ’ student exercise ; 
 alway — 88 quotation Ravel Sonatina , delicious 
 indulgent passage wish hear . right , 
 d admiringy BF m spite text - books ? Simply sounds 
 ) ight — z.¢. , talks sense . point 
 familiar ring fF complaint text - books professors : 
 wned : , , young student root 
 ole ths | ofthe matter intuitively ‘ experience ’ 
 protest | Dr. Prout says sure guide 
 oser , | write consecutive fifths . ‘ Experience ’ 
 i. simply means good taste , sure sense fitness ; 
 * , ’ | men birth , teaching 
 rat ue toothers . Consecutive fifths , like , 
 - clear wht fF jadged results . pedagogical talk 
 Mr. Corde EF ‘ experience ’ ‘ great masters ’ fudge . 
 ase consecutive fifths sound right , matter 
 rerely JF written man 
 escription ? ’ boy , John Sebastian Leipsic 
 t chapeau John Willie Liverpool . original point 
 whok ‘ rules , ’ teacher , requisite taste , 
 ! ( 4 [ F tguisite sense fitness , student 
 vad writtens fF got grace God . 
 trouble Mr. Turner attempted defence teaching 
 th subject , venture , defence . ‘ Bare 
 Undesirable F consecutive fifths , ’ says , ‘ certainly sound objectionable 
 h andlo diatonic - writing . ’ answer simply , 
 tellsme , J ‘ . ’ depends idea 
 nearly | meant express . pedant hunt 
 n infractions ‘ rules , ’ objectionable 
 gave » — mthe following passage Beethoven : 
 SINESS NOY , 
 " t work 
 umanitarian 
 world , 
 critics 
 tyled 
 hem 
 -e fifths ist fp Ot Borodin song , ‘ sea queen ’ ( Mr. Turner 
 afraid ™ Y , course , writing wholly 
 om poser diatonic , general esthetic problem independent 
 lem , fF Siderations kind ) : 
 imate ones Et 3 
 t * consect cso ea es : = 
 - — Fee ors eer s 
 little P sempre legato . = ; 
 tions eee Se ee 
 ve perfet — — — — 
 | parts jus r 2 

 difficulty ? 
 e student ’ 
 
 fo mention 
 zinners 




 = 2 = S= 


 ERNEST NEWMAN 


 CROATIAN MUSIC . 


 EDITOR ‘ MUSICAL TIMES 


 96 


 , 1915 


 MUSIC NATIONALISM 


 EDITOR ‘ MUSICAL TIMES 


 FRANCIS TOYE 


 SJ 


 MES 


 _ _ — _ --—= — , — _ .K—__—__9 — _ — 


 GLORY WAITS THEE 


 FS = = 


 - — 1 4 2 4 — — — — — 4 


 ACN 


 .——A _ _ 


 — — — _ 3 


 _ — = -= _ 


 NN — -N — _ 


 PP — _ 


 — — — — — — — SS 


 _ = ES 


 = $ : $ -- + — } _ — » @   @~6@ 


 ~. — — _ _ _ _ < — _ , . 


 EE — — 


 GLORY WAITS THEE 


 7 _ 


 , — _ * ° — # _ | 6 : — 8-"6 5 3 2 E = = — — — — — — 


 — ~ P —   — — _ — — — 


 _ — _ 


 AB 


 103 


 CONVENTIONS CHURCH MUSIC . 


 7O EDITOR ‘ MUSICAL TIMES 


 PSALM TUNES : MELODY TENOR . 


 EDITOR ‘ MUSICAL TIMES 


 HARVEY GRACE 


 BIOGRAPHY COLERIDGE - TAYLOR . 


 EDITOR ‘ MUSICAL TIMES 


 MUSICAL 


 104 


 1915 


 MUSIC WAR - TIME COMMITTEE 


 BASE HOSPITAL 


 SOLDIERS ” SAILORS ’ WIVES 


 105 


 CHORAL CAMP CONCERTS 


 SCHOOL CONCERTS 


 ORCHESTRAL CONCERT 


 CHIN - RESTS ABOLISHED 


 106 


 OPERA BLACKPOOL . 


 ( MANCHESTER REPRESENTATIVE 


 LL 


 TIVE 


 107 


 IMMORTAL HOUR ’ BOURNEMOUTH . 


 MR . RUTLAND BOUGHTON NEW WORK 


 BRISTOL MADRIGAL SOCIETY 


 CES 


 ROYAL CHORAL SOCIETY 


 QUEEN HALL ORCHESTRA 


 LONDON TRIO 


 THEMUSICAL TIMES.—Fesrrvuary 1 , 1915 

 
 DEVON CORNWALL . | Horn trio Brahms Sonata piancforte ani 
 ; | horn Beethoven ( Op . 17 ) played . othe 
 PLYMOUTH . : Mis ~atrice ts s : ; 
 pieces , Miss Beatrice Betts sang songs composed fy 
 Concerts War Funds entertainment | Mr. Ley . 
 troops continue occupy solely attention performers CORNWALL 

 E public . yrovision purpose 

 regularly provided free troops hutments , tents | whe " agate x ; : 
 2 ‘ Carol singing usually prominent feature Cambor 
 halls , barracks camps . concerts | ; - ; _ * . 
 : er : pe - | life Christmas season , partis 
 appreciated visit sufficient convince . } : ‘ alee eA tags - 
 : . | fewer number past season 
 cases outlying forts men recruits | ; pe Paty er y 
 ; ~ ; ei custom maintained . Christmas concert wx 
 Kitchener Army living roughest 

 conditions , chivalry , courtesy , gratitude 
 artists serve model picked audience . 
 sight men standing sitting packed close rapt | DUBLIN 
 attention pathos humour , stirs deepest . sree 
 eelings heart . music best pleases Tommy | Sunday Orchestral Concerts - commenced ihe 
 Jack sweetly sentimental Christmas vacation January 10 . _ Dr. Esposito played 
 stirringly patriotic martial kinds . | Beethoven ‘ Waldstein ’ sonata , Op . 53 . Miss Fanm 
 quickly learns , wait invitation join | Vincent ( soprano ) vocalist , orchestral itens 
 singing . large number cases genuine included entr actes Schubert s ° Rosamund , 
 talent gift playing singing discovered , | Wagner * Waldweben , ’ Smetana ‘ Bartered Bride 
 entertainment music time war | Overture . January 17 , Beethoven second Symphony 
 features lasting importance . | chief orchestral piece , strings bani 
 musicians conditions find rag - time played Percy Grainger ' * Molly shore . ’ M 
 popular songs articles abhorrence discovered P. J. Griffith solo violinist , Mr. Irvine Lynd 
 cordially helping concert spirit ( bass ) vocalist . , : : 
 moment . chamber music recitals Royal Dublin Society 
 aid Queen ‘ Work Women ’ Fund , | resumed January 11 , Dr. Esposito , Signa 
 Dr. Weekes Orchestral Society gave concert Simonetti , Mr. Clyde Twelvetrees played Beethoven ’ 
 December 16 , 1914 , works represented Trio E flat , Op . 70 , . 2 , Brahms Horn tro , 
 movement — procedure condemned artistic | Op . 40 . interest taken performan : 
 grounds . Symphony C minor Gade ( | Dr. Esposito new Sonata violin pianoforte 
 movement ) important work band ; | Op . 67 , beautifully played Signor Simonet 
 movement Sonata pianoforte violoncello composer , cordially received . Wa 
 Rubinstein , pieces Reinecke , Beethoven , Bizet , prevented Pianoforte recital arranged January 18 
 Mendelssohn , given . string - players Mr. Harold Bauer taking place . — 
 Extempore Chamber Music Club , members January 9 concert given Theatre Ropd 
 orchestra , played slow movement Tchaikovsky fund provide comforts 7th Battalion Ropil 
 Quartet D. Dr. Weekes Mr. Walter Weekes| Dublin Fusiliers goes . hig 
 conducted , vocalists Miss Tresise | audience , headed Lord Lieutenant Ireland 
 Mr. Robert Chignell . Marchioness Aberdeen , filled seat theatre , 
 War interrupted formal meetings | net result profit £ 203 . Mr. John Coate 
 Extempore Chamber Music Club , playing members Madame Borel , Miss Jean Nolan , Mr. Percy W hiteheai , 
 continued meet privately Sundays , Carlton Quartet ( Miss Lilian Whittaker , Miss Edit 
 good work accomplished enhanced ensemble Mortier , Messrs. Ww . Lewin , Mr. T. W. Hall ) 
 chamber music technique , acquaintance works | vocalists . Signor Simonetti , Mr. Clyde Twelvetrees , 
 new old . Dr. Esposito ( played Mr. John Coates accompaniment 
 annual performances ‘ Messiah ’ the| instrumental soloists , Mr. C. Ww . Wilson 
 Guildhall Choir , conducted borough organist , accompanist . programme included Miss Alice 
 January 2 , assistance good string orchestra and| Finny dramatic sketch ‘ , ’ sketch Mi 
 Misses Mary Leighton Winifred Lewis , Percy French Miss Florence Marks , orchesn 
 Messrs. Sydney Coltham Joseph Farrington | items contributed theatre orchestra 
 principal vocalists . conductorship Mr. John Moody . 
 January 12 , Unitarian Church , Mons J. Stuyct 
 DEVONSHIRE TOWNS . late professor organist Notre Dame College , Antwep 
 arrangements announced early spring } refugee Dublin , gave interesting og 
 management Torquay Pavilion diminution | recital organ built 1911 J. W. Walker & Sos 

 Mr. F. E. Luke 

 interest . special Christmas programme given the| London . 
 orchestra , conducted Mr. Basil Cameron , included — _ — _ 
 Shakesperian concert . violinist , Melsa , Russian EDINBURGH . 
 — donna , Madame Kontratov , boy pianist , ' 
 Solomon , performed . characteristic programmes| features recent concerts Messrs. Patersot ' 
 Russian , Sullivan , soldiers ’ concert . Orchestral series follows : December 

 Miss Marian Pearson , lady resided Egypt , | Miss Harrison solo violinist Brahms Concert 
 obtained interesting Egyptian Arabic music | December 28 , Miss Rosina Buckman , Mr. Frank Mulling 
 incidental use play authorship , ‘ Broken | Mr. Robert Radford vocalists Wagner conctf 
 barriers , ’ scene laid Egypt , which|on January 4 , Mr. Alfred Hollins solo organs ? 
 played Babbacombe January 6 . Arabic ] Guilmant Symphony D minor , Mr. Inches took 
 music Mansour Awad , Sami A. Chawat , | place Emil Mlynarski conductor . Bronislav Huber 
 M. Abdel Messih ( Cairo ) . Oriental songs sources | gave impressive performance Beethovens Viole 
 introduced , musical setting scenes } concerto . January 18 , Sapellnikov   entranc & 
 indicated distinctive touch tact artist . The| audience interpretation Schumann mil 
 author played pianoforte directed } Pianoforte concerto . _ concert , Brahas : 
 performance . C minor Symphony received inspired performance uni 

 Miss E. Knight Bruce ( violin ) Mr. H. G. Ley| baton Mlynarski . } 
 ( pianoforte ) devoted proceeds annual chamber December 26 , Mr. Moonie Choir gave anne 
 concert Exeter , January 8 , patriotic purposes . | performance ‘ Messiah , ’ ending Hallela 
 secured assistance Mr. O. Borsdorf , jun . ( horn ) , and| Chorus . portion Wagner ‘ Parsifal ’ added 

 ced th 
 sito played 
 Miss Fann 
 hestral items 
 Rosamund , 
 tered Bride 
 d Symphony 
 bani 
 hore . ’ Mr 
 rvine Lynd 

 rblin Society 
 sito , Signor 
 Beethoven ’ 
 ; Horn tno , 
 performance 
 oforte , 
 Simonet 

 
 nuary 18 

 Fellowes , leader Scottish Orchestra , played 
 h Concerto violins . 
 City Hall Saturday Evening Concerts Glasgow 
 Choir ( Mr. H. S. Roberton ) gave evening 
 benefit Belgian Relief Fund 

 Comedy overture ’ Florent Schmitt Suite 
 Reflets d’Allemagne , ’ especially 
 received . outstanding feature — 
 noteworthy events present concert season — 
 debit Mr. Albert Sammons solo violinist 
 dleventh Classical Concert January 12 . 
 magnificent interpretation Beethoven Violin concerto 
 deep impression , secured soloist 
 ovation seldom accorded concerts 

 M. Sapellnikov chief attraction twelfth 
 Classical Concert January 19 . Splendidly supported 
 Scottish Orchestra , gave brilliant performance 
 Schumann Pianoforte concerto minor , 
 group solos included performer Gavotte 
 E major Liszt sixth Rhapsody . symphony 




 GLASGOW 


 LIVERPOOLThe seventh Philharmonic Concert January 12 
 conducted Sir Henry Wood , interpretation 
 Brahms Symphony revealed qualities 
 expression sincerity music possesses 
 constructive interest . Rimsky - Korsakov strenuous 
 overture , ‘ Ivan Terrible , ’ Ravel ‘ Pavane sur une 
 Infante défunte , ’ Percy Grainger ‘ Irish tune 
 County Derry ’ ‘ Shepherd Hey ’ completed well- 
 diversified scheme , choir heard _ part- 
 songs Brahms Dudley Buck ‘ Hymn Music 

 Unusual interest attached concert 
 occasion famous violinist Ysaye played 
 public adventurous escape Belgium . 
 César Franck , M. Ysiye native Liége , 
 home Brussels . summer 
 villa Knocke know . M. Ysiye 
 family , exception sons son - - law 
 fighting King Albert army , escaped England 
 enduring manifold hardships land sea . 
 lost baggage , Ysaye managed save 
 priceless Strad , played occasion 
 Viotti Violin concerto , . 22 , minor , old 
 executive mastery suavity tone . evident 
 M. Ysiye greatly touched unmistakable outburst 
 sympathy greeted appearance . playing 
 Viotti melodious old - fashioned material remarkable 
 depth tenderness expression . Beethoven 
 Romance G Saint - Saéns ‘ Havanaise ’ M. Ysaye 
 opportunities display exceptional qualities , 
 technical temperamental 

 Rodewald Club Concert January 11 , 
 programme sustained Mr. Frederic Brandon 
 Mr. Vivian Burrows , collaborated Brahms Sonata 
 , Op . 100 , pianoforte violin , César Franck 
 Sonata instruments , familiar masterpiece 




 MANCHESTER DISTRICT 


 NEWCASTLE DISTRICT . 


 YORKSHIRE . d notin . : - 
 : pleasing pieces Cui Miniature Duets flute 

 ably = violin ( Op . 56 ) Kronke second Suite , ‘ modern 
 ed te syle ’ flute pianoforte . Saturday Orchestral 
 f seasy ( Concert January 16 Mr. Fricker conducted Kalinnikov 
 j G minor Symphony , Mr. Arthur Rubinstein gave 
 igne ’ ; ani nost brilliant artistic interpretation solo 
 ater work , Beethoven C minor Pianoforte concerto . — Mendelssohn 
 er’sfeaturs | ‘ Midsummer Night Dream ’ Elgar ‘ Cockaigne ’ 
 avishing & Overtures features programme . 
 s quis Bradford Mr. Midgley enabled , 
 Dane renewed help anonymous lovers music , resume 

 1 readin excellent free chamber concerts , 
 January 4 second January 18 . 

 er 

 ae January 15 , Bradford Subscription Concert , 
 vals te | Hallé Orchestra , Mr. Verbrugghen , heard 
 y Beethoven seventh Symphony * Egmont ’ Overture , 
 lief Funds   @lndy ‘ Istar ’ Variations , Liszt ‘ Les Préludes 

 Miss Marie Hall soloist movements 
 Lalo ‘ Symphonie Espagnole . ? Mr. Verbrugghen 
 readings Beethoven distinguished 
 _ _ ._. & exceptional fire force , dramatic emotional 
 oa qualities music brought shade 
 Miss Mare fp “ ™ * 8BEration . 
 sarley , ant 
 nm 30h 
 layed 
 ‘ Hal 
 octet F Country Colonial ews . 
 woe mw BRIEFLY SUMMARIZED . 
 fal Wecannot hold responsible opinions expressed 
 thit summary , notices prepared soca : 
 r uwstapers furnished correspondents 
 centre : Correspondents particularly requested enclose srogramme 
 ; mont } forwarding reports concerts . 
 cett Suett , 
 ~~ BRIGHTON . - -The prize - giving Brighton School 
 ars te | % Music took place December 19 , prizes 
 f Mr. [ obs distributed Mrs. Tobias Matthay . holder 
 Mr. Frank new Kuhe Scholarship pianists announced 
 compatist te James Henry Smith , Hove . highly satisfactory 
 Whittaker _ work School read , particular 
 ablicly , nce examination successes 
 rict . . ‘ holarships held Royal College Music 

 DoverR.—On December 19 vocal instrumental 
 concert , organized Mr. H. J. Taylor , borough organist , 
 given Town Hall , artists Miss Curtois , 
 Miss Avis Thorpe , Miss Kathleen Downs , Mr. E. W. 
 Barclay ( vocalists ) , Mr. R. B. Freeman ( violin ) , Miss 
 Cooper ( violoncello ) , Miss Ethel Spain ( pianoforte ) , 
 Mr. Taylor ( organ ) . proceeds devoted 
 assistance Mr. Taylor series Camp Entertainments 




 116 


 1915 


 LATIN ORGANIS 


 EDITED 


 GREGORY OULD 


 


 SAMUEL 


 ENGLISH LYRIC 


 SET MUSIC 


 C. HUBERT H. PARRY 


 SECOND SET 


 FIFTH SET 


 ORIGINAL 


 COMPOSIT IONS 


 4,20 


 


 — — STORY CROSS — ANTHEMS 


 VOICES ORGAN > _ > ; -¢ 


 WORDS 


 COMPANY 


 118 


 IQT5 


 NOVELLO 


 ANTHEMS 


 OSS 


 A.A.T.T.B.B 


 LENT 


 


 SAC 


 HY 


 NT } CANTATAS LENT 


 — % WORDS WRITTEN SELECTED HOMAS ADA MS . 


 BARITONE SOLO CHORUS 


 SET MUSIC 


 J. STAINER 


 E. CUTHBERT NUNN 


 WORDS DERIVED MAINLY ANCIENT SOURCES 


 CHORUS 


 > — 1 APPROPRIATE HYMNS , | COMPOSED 2 


 MUSICAL 


 120 


 TIMES.—FEBRUARY 


 EDWARD BUNNETT 


 REDUCED PRICE 


 PENITENCE 


 98 


 ENTLY PUBLISHED 


 ORGAN MUSIC . 


 PEACE 


 PASSION 


 


 HYMNS SUNG CHOIR 


 CONGREGATION 


 J. VARLEY ROBERTS 


 CRUCIS 


 SACRED CANTATA 


 DEPICTING SCENES O 


 SAVIOUR LIFE EARTH . 


 


 SOLO VOICES ( TE BASS ) CHORUS 


 FREDERICK ILIFFE , M.A , 


 POPULAR EASTER ANTHEM 


 COMPOSED EDWIN A. CLARE . 


 ND CHORD 


 IR 


 ARTH . 


 ND CHORUS 


 IPTURES 


 1000 


 1000 


 1000 


 1000 


 1000 


 1000 


 BRR 


 , 1915 


 EASTER ANTHEMS 


 RECENTLY PUBLISHED 


 EN GATES RIGHTEOUSNESS 


 DISON 


 LONDON : NOVELLO COMPANY , LIMITED 


 MILLION COPIES CALEB SIMPER ' 


 WORLD - WIDE POPULARITY . FAVOURITES . 


 SCHOOL CHOIR TRAINING 


 PRACTICAL COURSE LESSONS 


 VOICE - PRODUCTION 


 GUIDANCE TEACHERS CLASS - SINGING 


 MARGARET NICHOLLS 


 WEBSTER 


 GROUNDWORK MUSIC 


 NOVELLO 


 ANTH EMS 


 HYMNS TUNES 


 RAINBOW 


 MUSIC READING LADDER BEGINNERS 


 SONGS 


 2 D 


 6 3 


 2 , 7 


 1 7 


 4 7 


 1 % 7 


 16 . 7 


 ES 


 ODWARD 


 


 SOLDIERS S 


 


 ASIDB 


 SUN MV SOUL 


 READY 


 POCKET SING : SONG BOOK 


 LEFT 


 JOHNNY . 


 


 TH 


 HYMNS 


 SHORTLY 


 SCHOOLS HOMES 


 PRICE SHILLING 


 NATIONAL HYMNS . 


 MARCHING SONGS 


 NATIONAL FOLK - SONGS . 


 LL PEOPLE EARTH DWELL 


 LONDON 


 NAVAL MILITARY 


 MUSICAL UNION SONG BOOK 


 CONTENTS 


 - SONGS 


 NOVELLO COMPANY , LIMITED 


 SONGS SAILORS 


 WRITTEN W. C. BENNETT . 


 NOVELLO VOCAL ALBUMSEnglish Version Rev. J. TROUTBECK . Price 

 . 6d . ; 
 BEETHOVEN.—Twenty - Soncs . Vol . I. English 

 German words . English Version Rev. 
 J. TRouTBEcK. Price ts . 6d . 
 BEETHOVEN.—SEVENTEEN Soncs . Vol . II . 
 German words . English Version 
 J. TROUTBECK . Price ts . 6d . 
 BEETHOVEN.—TwWEntTy - Soncs . Vol . IIT . English 
 German words . English Version Rev. 
 J. TROUTBECK . Price . 6d . 
 BENNETT , W. STERNDALE . 
 English German words . Price 
 2s . 6d . 
 BISHOP , H. R.—Twenty SoncGs ( 1786 - 1855 ) . 
 W. A. BARRETT . Price 1s . 6d 

 English 
 Rev 




 BRAHMS 


 DVORAK , 


 PROUTBECK 


 LONDON 


 NOVELLO COMPANY , LIMITED 


 - TROUTBEC 


 RANDEGGR 


 RANDEGGE 


 WINTE 


 M ACFARRES 


 , IQT5 . 


 9 — MARCHES 


 16.—SCHWEIZER , OTTO . 


 | ACF ARREM 


 NOVELLO ' 


 PIANOFORTE 


 ALBUMS 


 TOURS . 


 . 48.—SCHUBERT , FRANZ . 


 50.—SCHUBERT , FRANZ . 


 . 51.—SCHUBERT , FRANZ . 


 52.—SCHUBERT , FRANZ . 


 , 1915 


 


 ORGAN 


 FANTASIA 


 


 


 COMPOSED 


 ALEX 


 SHORT PRELUDES 


 ENGLISH MELODIES 


 ORGAN 


 GUILMANT 


 HAWKES & SON 


 ORCHESTRAL PUBLICATION 


 SHEPHERD FENNEL DANCE .. 7 6 


 YADE DE PRINTEMPS 


 RCKUSENTA*IA IRISH AIRS 

 CADENZAS BEETHOVEN CONCERTO 

 Superior Edition op Best Popular VIOLIN 
 great Masters . Edited ALBERT S MMONS 




 HAWKES & SON 


 PHANTASY 


 


 NATIONAL ANTHEM 


 DR . CHAS . W. PEARCE 


 MANSFIELD 


 7 


 SEVENTEENTH - CENTUR 


 SONGS 


 


 SHAKESPERIAN RESTORATION PERIOD 


 SIR FREDERICK BRIDGE , 


 C.V.O 


 ATION 


 10 0 T Sammons 

 ‘ 
 ZOLIN § 
 < M MONS , 
 und David ; 
 ‘ Teuxtemps : 
 W. Ernst ; 
 Beethoven : 
 Schumann 
 les Gouned 
 L tentawski 
 L. Spohr 
 > de Berio 
 Vrentawski 

 F. Chopin 




 LEMS 


 BER 


 127 


 NONUW HO 


 TUR 


 Y PERION 


 SE 


 EDITION NOVELLO 


 DAADHDICIOAGAH WACO 


 PETERS EDITIONSIN BOUND VOLUMES 

 PIANOFO ! ATE SOLOS . s. d. 
 2790a , b BACH , J. S.—48 Preludes Fugues ( Ruthardt ) 7 © 
 200 , 201- — — Short Preludes Fuguesand Inventions iu 2 
 3 parts .   ® , . 
 202 - 204 — — Frer ch E _ — Suites 6 6 
 295b BEETHOVEN ‘ 6 6 
 1801b , c — , % 1 6 © 
 297 ~ Piet ces ( Rondos . & c. ) . 
 144 — Concertos F fantasia , ‘ Op . 80 € 
 196a , b — — Symphonies complete 2 Vols , 5 ° 
 1824a , b — — Album . 1 Vol . « | ? 
 1250 BENDEL , F.—Op . 139 . Au lac de Geneve ~~ 2 % 
 CHOPIN , F.—Pianoforte Works , 3 Vols . 7 © 
 1g00a Vol . I. 14 Waltzes ; 51 M : azurkas ; 10 Polonaises ; 
 1 g Nocturnes . 
 1goob Vol II Ballades ; Impromptus ; Scherzos ; Fautasia ; 
 Studies ; Preludes ; Rondos . 
 1990C Vol . III . Sonatas ; Bercevse ; Barcarolle ; Bolero , & c. ; 
 Cones rtos ; Concert - Pieces . 
 1926 — — Album . 2 Select deeenaaees es 
 1903 — — Polonaises .. ‘ 3 9 
 1904 — Nocturnes . ie 3 9 
 19 6 — — Scherzos F antasia .. 3 9 
 19 ° 9 — Sonatas . 3 9 
 3013 CLEMENTI . ~ ausig . Gradus .. 4 6 
 491 FIEI.D.—18 Nocturnes - 3 6 
 197 HAYDN.—12 Symphonies .. 6 6 
 1148 JENSEN.—Op . 17 . W eee silder 6 3 
 7isa , b KUHLAU.—19 Sonatina » eo 
 1187 LISZT.—Hungarian Fanta - ei . wo fs 
 1704b = NDE L SSOHN.—W = > ! Vol . Il . ( Op . 5 . 7 . 14 , 
 33.72 5 6 
 273 oy 4 RT . — Variations 5 6 
 198 — — 6 Symphonies . 4 06 
 1189 RUBINSTEIN.—Album - . 6 6 
 SCHU MANN.—C€ complete Works , 5 Vols . .. 6 6 
 23008 I. ( Op . 68 , 15 , 124 , 99 , 18 , 19 , 82 , 28 ) . 
 2300b Vol . Il . ( Op . 6 , 9 , 21 , 12 , 16 ) . 
 2300d Vol . LV . ( Op . 32 , 72 , 23 , 111 , 76 , 126 , & c. ) . 
 2300e Vol . V. ( Op . rz , 22 , 14 , 54 , 92 , 134 , 7 ne 
 2854a , b SINDING.—Op . 31 . 6 Pieces(in 1 Vol . ) 7 6 
 2687a , b — — Op . 34 . 6 Piec es ( 1 Vol . ) oe 7 6 
 489 WERER.—Complete Works 6 o 
 717a , b — — Sonatas , Pieces , & c. Concertstiick .. 7 0 ° 
 1826 — _ — - Aibum - ee 3 6 
 ; 2 Album . Vol . I. . cloth 6 6 
 ‘ ne - Pictures . Album Leaves 
 Lyric Pieces ( Book LV . ) . 
 — Album . Vol . II . . cloth 6 6 
 Humoresken . Lyric ‘ Pieces(Book III . ) . Holberg Suite . 
 — - Album . Vol . IV . cloth 6 6 
 Melodies . Northern Dances Popular Songs . 
 Sketches Norwegian Life . 
 — — Peer Gynt Suires , Nos . I. II . cloth 5 6 
 PIANOFORTE DUETS . 
 GRIEG.—Holberg Suite . Norwegian Dances .. 6 6 
 25g1a HANDEL.—12 Organ Concertos . Vol . I. 6 6 
 186a , b HAYDN.—12 Symphonies , 1 Vol . 8 o 
 2125 MOSZKOWSKI.—Spanish Dances 6 6 
 12 MOZART.—Original Compositions 5 ° 
 187b — — 12Symphonies . Vol . II . ( 7—12 ) 6 6 
 2347 SCHU MANN.—Original aes ; $ ° 
 2348 — — 4 Symphonies ee 6 6 
 136 BFETHOVEN.—11 Overtures 40 
 141 BELLINI — ROSSINI.—o Overtures . . ~ 3 6 
 VIOLIN P IANOFORTE . 
 13a BEETHOVEN.—10 Sonatas + + 10 0 
 14 MOZAR r.—Sonatas “ os - . 10 0 
 156a , b SCHUBERT.—Sonatinas Duets ... ‘ - 90 
 191 WEBER.—6 Sonatas .. 40 ° 
 TRIOS ( PIANoFORTF , VIOLIN , ' CELLo ) . 
 1740 MENDELSSOHN . — Trios : 10 © 
 2377 SCHU MAN N.—Trios ( Op . 63 , 80 , 110 ) . s 6 
 QUARTETS ( PIANOFORTE , VIOLIN , VIOLA , Cato ) . 
 1741 MENDELSSOHN . Quartets ... - 14 © 
 SONGS ( ENGLISH GERMAN “ Worps ) . 
 1112a FRANZ.—Album . Vol . I. ; Vol . IIL . ( . — 6 © 
 1427 — Tol . V. 60 
 SONGS ( German Worps ) . 
 LIEDERKRANZ.—High , Medium , Low .. gach 6 6 
 ( Nos . 2071a , 2071b , 2071C¢ ) . 
 3958 LIEDERSCHATZ.—Vol . I. ee 6 o 
 1774a MEN DELSSOH N.—Songs , High 6 © 
 2zob SCHUBERT.—Album I. Medium 6 0 
 20c - — — 50 » Low 60 
 178a = » IL . High 60 
 178b - " + » Medium 6 0 
 178c » , Low 6 0 ° 
 790a — -- Ill. High 6 6 
 790b — - “ , Medium 6 6 
 7g0C — “ fer 6 6 
 2383b SCHUMANN.—Lieder Vol . I. Medium 40 
 2383 — — ” » Low ad 
 3120 BERLIOZ — STRAUSS . — Instrumentation ( 2 Vols . ) 3 

 Lonpoxn : NOVELLO COMPANY , Limirsp 




 CRAMER . 


 - ELECT STUDIE ! 


 EDITED 


 FRANKLIN TAYLOR . 


 CLEMENTI . ' 


 GRADUS AD PARNASSUM { [ & 


 NUMEROUS MUSICAL EXAMPLES T@§ > » . ) . 


 STO } 


 YING 


 LOR 


 CHORAL TECHNIQUE 


 S TH 


 ERS 


 NET 


 , 1915 


 EXTRA SUPPLEMENT 


 REVIEW 


 191 


 SCHOOL MUSIC 


 RHYTHMIC ATTACK 


 TONAL 


 1S 


 PHRASING 


 1 , 19135 


 RELATION VERBAL MUSICAL PHRASING 


 1 RASING 


 TECHNIQUE SAKE 


 EXPRESSION ’ MARKS . 


 SCHOOL MUSIC REVIEW 


 FEBRUARY 1 , IQI5 


 NATIONAL EISTEDDFOD , 


 ABERYSTWYTH . 


 CHIEF CHORAL , MIXED - VOICE 


 MALE - VOICE 


 XUM 


 SCHOOL 


 COMING COMPETITIONS 


 CHILDREN CHOIRS . 


 LADIES ’ CHOIRS . 


 MALE - VOICE CHOIRS 


 NOVELLO 


 EXTRA SUPPLEMENT . 


 NOVELLO PARISH CHOIR BOOK 


 PARISH CHOIR BOOK 


 61 . 


 62 . 


 427 . 


 H. B. C 


 757- 


 21 . 


 623 . 


 366 . 


 365 . 


 467 . 


 448 . 


 610 . 


 531 . 


 193 . 


 586 . 


 529 . 


 596 


 742 & 794 


 POPULAR HYMNS TUNES 


 NOVELLO PARISH CHOIR BOOK 


 UNES 


 RRR 


 L 4 ] 


 NOVELLO ' 


 VESPER HYMNS AMEN 


 VESPER HYMNSAsuton , A. T. Lee.—L ord , kecp safe night ( Card 

 BEETHOVEN ( Adapted from).—Lord , safe night . scitings 
 ( Card 

 Cruicksnank , W. A. C,—Saviour , day ending ( Card 




 AMENS 


 LONDON 


 MITED