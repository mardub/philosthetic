


THE MUSICAL TIMES


AND SINGING-CLASS CIRCULAR. 


PUBLISHED ON THE FIRST OF EVERY MONTH


AUGUST


I, 1875


THE SIR JOHN GOSS EXHIBITION


HE COUNCIL OF THE COLLEGE OF 


ORFOLK AND NORWICH MUSICAL 


TI


FES VAL, 


CONCORDIA 


ANNUAL SUBSCRIPTION, INCLUDING POSTAGE, 


NINETEEN SHILLINGS AND SIXPENCE


OFFICE—LONDON: 1, BERNERS STREET, W


H.E


PROFESSIONAL NOTICES


MR. J. TILLEARD 


USIC ENGRAVED, PRINTED, AND PUB


THE NEW PEDAL ATTACHMENT 


FOR PIANOFORTES


TESTIMONIALS. 


JESSE MINNS, F.C.O., 


AST LONDON ORGAN WORKS, 


, RUSSELL'S MUSICAL INSTRUMENTS. : 


O CHORAL SOCIETIES, CATHEDRAL 


STANDARD AMERICAN ORGANS


MANUFACTURED BY 


PELOUBET, PELTON & CO., NEW YORK


EXPRESSION


BARNETT SAMUEL & SON, 


WHOLESALE MUSICAL INSTRUMENT WAREHOUSE, 


HOUNDSDITCH, LONDON


O PIANOFORTE DEALERS, AUCTIONEERS, 


OR PEDAL PRACTICE.— PEDAL REED


RGAN, PIANOFORTE, HARMONIUM, HAR- 


OTICE OF REMOVAL.—THOMAS S&S. JONES, 


SECOND


SERIES


THE


CANTICLES


OF THE


POINTED FOR CHANTING, AND SET


AND HYMNS


CHURCH


TO APPROPRIATE ANGLICAN CHANTS


SINGLE AND DOUBLE


TOGETHER WITH


EDITED


BY THE


REV. SIR F. A. G. OUSELEY, BART., ETC


AND


EDWIN GEORGE MONK


TONIC SOL-FA EDITION. 


A MANUAL OF SINGING


FOR THE USE OF CHOIR TRAINERS & SCHOOLMASTERS. 


THIRTY-THIRD EDITION. 


THE COLLEGIATE AND SCHOOL


SIGHT SINGING MANUAL


APPENDIX


(COMPANION WORK TO THE ABOVE


COLLEGIATE VOCAL TUTOR


CATHEDRAL CHANT BOOK


SECOND EDITION OF DR. BENNETT GILBERT'S


SCHOOL HARMONY


HARVEST ANTHEMS


ALLCOTT, W. H.—THOU VISITEST THE EARTH AND 


ACFARREN, G. A.—GOD SAID, BEHOLD, I HAVE GIVEN 


AYLOR, W.—THE EYES OF ALL WAIT UPON THEE. 


RIDGE, J. FREDERICK.—GIVE UNTO THE LORD THE 


EETON, HAYDN.—THE EYES OF ALL WAIT UPON 


TWELVE HYMNS WITH TUNES 


HARVEST


SELECTED FROM


THE HYMNARY


PRICE ONE PENNY


PRAISE, O PRAISE OUR HEAVENLY), 


» A EARTH 


C. JEFFERYS, 67, BERNERS ST


JEFFERYS’S MUSICAL JOURNAL


MELODIOUS MELODIESArranged for the Organ or Harmonium by JoHN OWEN. 
Each Part One Shilling, net. 
PART I

Andante (Heller), Lied (Mendelssohn), Schlummerlied (Schumann), 
March (Schumann), Andante (Oesten), Andante Moderato(Schumann), 
Andante Cantabile (Beethoven), Allegretto (Mendelssohn), Traiimerei 
(Schumann), Andante (Beethoven), Wedding March (Mendelssohn

PART II

Angels ever bright and fair (Handel), Lied (Mendelssohn), Andante 
(Russell), Adagio Cantabile (Beethoven), Cujus Animam (Rossini), 
Faith (Glover), Hope (Glover), Charity (Glover), Sanctus (Bartniasky), 
Caprice (Blumenthal), Gloria, from Twelfth Mass (Mozart

PART III.—OPERATIC. ) 
(In the Press.) 
PART IV.—VOLUNTARIES




SONGS FOR CHILDREN. 


CONTENTS


LONDON: C. JEFFERYS, 67, BERNERS STREET, W


RESIDENT MUSIC MASTER REQUIRED


IANO-TUNING.—EMPLOYMENT WANTED, 


THE MUSICAL TIMES 


AND SINGING-CLASS CIRCULAR. 


AUGUST 1, 1875


ESTHER: AN ORATORIO BY G. F. HANDEL.* 


HANDEL WAS ORGANIST OF THIS CHURCH 


FROM THE YEAR 1718 TO 1721, AND 


COMPOSED THE ORATORIO OF ESTHER 


ON THIS ORGAN


BD 


CRYSTAL PALACE


ROYAL ITALIAN OPERA


170


HER MAJESTY’S OPERAPHILHARMONIC SOCIETY

Tue feature of the final concert for the season on the 
5th ult., was an “Idyll” to the memory of the late Sir 
Sterndale Bennett, composed by his fellow-student and 
warm friend, Dr. G. A. Macfarren. The good feeling which 
prompted so graceful a tribute would have been sufficiently 
appreciated had not the artistic worth of the composition 
earned the good opinion of the listeners, and we trust, 
therefore, that Dr. Macfarren—who bowed from his place in 
the room, in acknowledgment of the applause with which 
the Idyll was greeted—would believe that the audience 
desired not only to show admiration of the work, but deep 
and heartfelt sympathy with the feelings of the composer. 
The other orchestral works were Haydn’s Symphony in E 
flat (No. 10} and Beethoven’s in C minor, the concert con- 
cluding with Weber’s ‘Jubilee ’’ Overture. Vieuxtemps’ 
Violin Concerto (No. 5, in A minor) was finely played by 
Herr Wieniawski, and an unusual brightness was given to 
the vocal portion of the programme by the engagement of 
Mdlle. Titiens, who sang the Scena, ‘A qual furor,” 
from Fidelio; the song, ‘ Glécklein im Thal,” from 
Euryanthe, and two of Schumann’s Lieder in her very 
best style. Mr. W. G. Cusins, the Conductor, received 
quite an ovation at the conclusion of the concert

ROYAL ACADEMY OF MUSIC




1872Potter _ Exhibitioner.— Alice Curtis. Westmoreland 
Scholar.—Agnes Larkcom. Sterndale Bennett Scholar.— 
Charlton T. Speer. Welsh Choral Union Scholar.—Mary 
Davies. Parepa-Rosa Scholar.—Annie E. Butterworth

Mr. G. W. HamMmonn’s morning concert at St. James’s 
Hall, on the 8th ult., was attended by a crowded audience. 
Mr. Hammond gave a thoroughly classical reading of 
Beethoven’s ‘‘ Moonlight Sonata,”’ and in the pianoforte 
part of Sir Sterndale Bennett’s Chamber Trio (Op. 26), 
Schumann’s Quintett in E flat (for pianoforte, two violins, 
viola and violoncello), and Mendelssohn’s Concertante 
variations (for pianoforte and violoncello), proved himself 
a sound and reliable performer. He also played with much 
success in Mozart’s Sonata for two pianofortes (ably 
assisted by Mr. W. H. Holmes) and gave two graceful 
pieces of his own composition, which were warmly ap- 
plauded. The other instrumentalists were Messrs. Henry 
Holmes and Mori (violin), Mr. A. Burnett (viola), and Herr 
Lutgen (violoncello). Miss Annie Butterworth, in Mr

O’Leary’s excellent song, ‘‘He roamed in the Forest,” 
was highly effective, and a song by Mr. W. H. Holmes, 
capitally sung by his son, Mr. F. Holmes, was also very 
well received. Other vocal pieces were given by Miss 
Julia Wigan, Mr. Arthur O’Leary acting with his usual 
skill as accompanist




REVIEWS


174THE MUSICAL TIMES.—Aveust 1, 1875

Phrygian mode,” than that they are in ‘‘ Form 5th of the 
model minor scale.” Many of the explanations in the book 
are exceedingly good, but we regret to see that compound 
time is said to be two or three bars of triple time put 
together. That moving in triple divisions should be con- 
founded with moving in triple time is unquestionably a 
misfortune, but it cannot be said to be a fault with a stu- 
dent who is told, as Mr. Spencer tells him, before giving 
some bars with triplets, that sometimes “one half of a 
measure is in duple time and the other half in triple time.” 
Some of the examples of counterpoint are, we think, 
scarcely good enough to place as models before a tyro— 
as, for example, in the last bar of page 86, where the coun- 
terpoint skips from the discord, C, and 11th bar of Ex. 2, 
page 73, where we have only three G’s on the first of the 
bar, the counterpoint afterwards rising to D—but, gen- 
erally, the progressions are fairly good. In speaking of 
the Fugue, no rules are given for the construction of the 
answer; but many valuable extracts from the works of the 
best composers are placed before the student to guide him 
in his labours, and the author deserves much praise for the 
earnest manner in which he urges the necessity of dili- 
gently studying the highest models. The second book, 
which treats of the art of playing the pianoforte, is well 
considered throughout, the early lessons and the remarks 
on fingering being particularly worthy of commendation. 
Quotations from the compositions of Beethoven, Mozart, 
Handel, &c., are given, in illustration of the author’s 
observations, most of which are carefully selected. We 
cannot believe, however, that it is good to transpose and 
vary passages from classical works for the use of young 
players, and must therefore protest against Example 45 
(merely marked “ from Beethoven”), where the opening 
theme of the Sonata in A flat (Op. 26) is transposed a 
semitone higher, not only the melody but the basses 
altered, and bars added at the conclusion, with the subject 
repeated an octave above. Not one of these alterations 
makes the passage in the slightest degree easier to play, 
and we may reasonably wonder, therefore, why such liber- 
ties are taken. In conclusion, it may be said that the 
books are carefully printed—so carefully indeed that Mr. 
Spencer will, we are certain, thank us for pointing out that, 
in the first work, the second subject given out by the alto 
in the Fugue in D minor, at the bottom of page 131, is 
wrongly noted: BY and A should be quavers instead of 
semiquavers ; it is a trifling error, but it may puzzle many 
students

WEEKES & Co




TTS


(= SSS 5 SSS SS SSS 


== 2.21 SS ESS: 


LU 


@ ,DOLO. 


LB 


179


TO CORRESPONDENTS


BRIEF SUMMARY.OF COUNTRY NEWSLancinc CoL_LteGce.—The usual Concert on Prize Day took place on 
Tuesday, the 27th ult. Among the pieces performed may be mentioned 
Mendelssohn's “ Hunting Song,” effectively sung, and two instrumental 
pieces: a pianoforte duet played by the Organist of the College and one 
of his pupils, and a duet for violin and piano played by the Organist 
and one of the boys. The vocal pieces were sung by the Chapel Choir 
and the Concert was conducted by the Organist and Choir-master, 
Mr. B. Manders

MAIDENHEAD.—The Philharmonic Society gave its second concert 
on Wednesday, the 2ist ult. Miss Jessie Royd, Miss Roberts, Mr. 
Selwyn Graham, and Mr. Orlando Christian were the principal 
vocalists; Herr Rosenthal, solo violin and leader of the orchestra, and 
Herr Max Schultz, conductor. The first part consisted of songs, 
Beethoven’s 1st Symphony, Auber’s Overture, Le Cheval de Bronze, 
etc., and the second part Haydn’s Spring and Summer, from the 
Seasons, all of which were creditably rendered. Mr. W. Goulden 
presided at the pianoforte. The concert was very successful

MELBOURNE, AUSTRALIA.—Bach’s Passion (St. Matthew), was given 
by the Philharmonic Society on Good Friday night, for the first time. 
The best available artists of the colonies were present and assisted in 
the performance of the work, which was most excellently rendered. 
Mr. Summers, the conductor of the Society, was presented with an 
honorarium of £50, as a mark of appreciation of the able manner in 
which he produced and conducted the work




DURING THE LAST MONTH. 


NOVELLO, EWER AND CO.’S ONLY COMPLETE AND 


UNIFORM EDITION OF


MENDELSSOHN’S


THIRTEEN TWO-PART SONGS


MR. BRANDON, 


VOCALIST (BASS), 


TENTH SEASON, 1875—76. 


HE ENGLISH GLEE UNION, 


ASSISTED BY 


NOVELLO'S 


SHORT MELODIES FOR THE 


ORGAN


VINCENT NOVELLO


THE ANGLICAN HYMN-BOOK


NEW EDITION, REVISED AND ENLARGED


HE ANGLICAN CHORAL SERVICE BOOK


USELEY AND MONK’S PSALTER AND


OULE’S COLLECTION OF WORDS OF 


HE PSALTER, PROPER PSALMS, HYMNS, 


OULE’S DIRECTORIUM CHORI ANGLI- 


OULE’S DIRECTORIUM CHORI ANGLI- 


HE ORDER FOR THE HOLY COMMUNION


ADDITIONS TO THE 


HANT RESPONSES TO THE COMMAND


THE LATE DR. ELVEY’S PSALTER. 


THE LATE DR. MONSELL’S DEVOTIONAL POEMS


L.RRY 


THE VILLAGE ORGANIST


THE CONGREGATIONAL PSALMIST


EDITED BY


ENLARGED TO 500 TUNES AND CHORALES. 


ANDERSON’S SONGS OF ZION


NEW AND CHEAPER EDITION. 


HELMHOLTZ ON TONE. 


ANGLICAN CHANT SERVICES 


FOR THE CANTICLES


NTHEMS BY W. PATTEN.— 


“OQ WORSHIP THE LORD IN THE BEAUTY OF HOLI- 


T IS A GOOD THING TO GIVE THANKS. 


PLAYED IN NEARLY 1,000 CHURCHES. | 


R. FOWLE’S STANDARD VOLUNTARIES. 


R. FOWLE’S STANDARD ANTHEMS; 124 


OULD YOU GAIN A MAIDEN’S HEART. 


AIL TO OUR HARVEST HOME


MANFRED


A DRAMATIC POEM, IN THREE ACTS, 


MUSIC BY 


ROBERT SCHUMANN, 


THE BENEDICTUS


SET TO MUSIC BY THE FOLLOWING 


MODERN COMPOSERS


SET TO MUSIC BY THE FOLLOWING 


MODERN COMPOSERS


SAMUEL WESLEY’S


MOTETT FOR DOUBLE CHOIR


PERGOLESI’S 


SPAR AT IEA TSR


FEMALE VOICES.) 


JOHN HULLAH. 


BY THE SEA


NOW READY. 


THE


CATHEDRAL PSALTER


POINTED FOR CHANTING


PUBLISHED WITH THE SANCTION OF


THE VERY REY. THE DEAN OF ST. PAUL'S


AND


THE VERY REV. THE DEAN OF WESTMINSTER. 


THE HYMNARY 


A BOOK OF CHURCH SONG


THE FOLLOWING EDITIONS ARE NOW READY :— 


THE NOTATION OF VOCAL MUSIC


ON THE PRINCIPLE OF 


SUBSTITUTION OF PITCH, 


AS ADOPTED IN THE “NATIONAL METHOD.” 


BY W. W. PEARSON


LATIN WORDS ONLY


ANDEL’S ORATORIO THE MESSIAH, 


ENDELSSOHN’S SIX GRAND SONATAS 


A SONG OF DESTINY


SCHICKSALSLIED), 


COMPOSED BY 


JOHANNES BRAHMS, 


REV. J. TROUTBECK, M.A


SONGS AND DUETS


COMPOSED BY


ANTON RUBINSTEIN. 


2. THE MINSTREL


THOU’RT SKE UNTO A FLOWER © 


4. MORNING SONG .. 


5. EVENING SONG 


7. SONG FROM EGMONT 


8. SPRING SONG 


RDNKHNRNHRKRY Y 


TRIOS FOR LADIES’ VOICES


COMPOSED BY


HENRY SMART


PWWWNHWHOW NWWW 


SIX PART-SONGS 


FOR FOUR AND FIVE VOICES, BY


JACQUES BLUMENTHAL. 


J. S. BACH’S


48 PRELUDES AND FUGUES


IN ALL THE MAJOR AND MINOR KEYS


THE WELL-TEMPERED CLAVICHORD,) 


EDITED AND COLLATED WITH ALL FORMER EDITIONS 


OF THE WORK, BY 


W. T. BEST


THE OFFERTORY SENTENCES


WITH AN APPENDIX FOR FESTAL USE, 


SET TO MUSIC BY


JOSEPH BARNBY


BEST’S ARRANGEMENTS 


FROM THE SCORES OF THE GREAT MASTERS. 


FOR THE ORGAN83. Funeral March, ‘‘ On the death ofa hero,” from Piano

forte Sonata, Op. 26 ove aes eco -- Beethoven

March, B minor, Op. 27. No.1 ae . F. Schubert. 
Hunting Song from the ‘‘ Waldscenen,” Op. 82 R. Schumann. 
Adagio from Pianoforte Duets, Op. ro ... See ite Weber. 
84. Fugue,in G minor ... ose eee one wee we Mozart. 
March Triomphale ... Salk eco ine - Moscheles. 
Fantasia from 6th Quartet, in E flat Major «+. Haydn

Chorus “ Behold the wicked bendtheir bow” (Anthem

In the Lord”) = Handel. 
Adagio, SymphonyinG ...0 1.0 se wee ose Haydn. 
94. Scherzo, 2nd Symphony -- Beethoven. 
Overture, Messiah . Handel. 
95. Andante, Sonata, A major, Pianoforte ‘na Violin, bs 
12, No. 2 od . Beethoven. 
Chorus, ‘‘ Go your way into , His ential ?, ‘(Jubilate) Handel. 
Chorus, “ Tell it out among the heathen,” (Anthem, 
“Ocome let ussing”) ... ao ‘ae ? Handel. 
96. Grand March, Op. 4o, No. 2 ie eee ...F. Schubert. 
Bourrée, Pastor fido ... At 2 Handel. 
o7- Passacaille =... seo eee .-F, Couperin. 
Triumphal March oe "< L. Hatton. 
Echo, from the Overture in the Feussh. ts - the 
Pianoforte ... os ot we Bach. 
98. Andante, from the Bagaele, Op.- 33, No. 4 -- Beethoven. 
Andante ‘ ott “ Mozart. 
Fuguein A minor ... se ae ove sad ae Bach

99. Baptismal Song «-. Meyerbeer. 
Air with variations, from Symphony | in 'D ows Haydn. 
Romance for Violin and Orchestra, F major, Op. 50... Beethoven

100.Allegretto (Gratulations Menuet) (posthumous Work) Beethoven. 
Allegretto, from the Violin Sonata in A major eve Handel. 
Prelude and Fugue on the name of “Bach” ... can Bach

Volumes I. to IV., bound in cloth, price £1-16s. each. 
London: Novello, Ewer and Co., 1, Berners Street (W




NOVELLO’S 


ORIGINAL OCTAVO EDITION OF


OPERASEdited, Corrected according to the Original Scores, and 
Translated into English, b 
NATALIA MACFARREN and BERTHOLD Tours. 
Price 2s. 6d. each; or in scarlet cloth, 4s. 
NOW READY

BEETHOVEN’S FIDELIO 
(With German and English words

With the two great Overtures as usually performed;"being the only 
Piano Score that has been published agreeing with the Original 
Score as to the notes and signs for phrasing




AUBER’S FRA DIAVOLO 


MOZART’S DON GIOVANNI 


BELLINI’S NORMA 


VERDI'S IL TROVATORE 


DONIZETTI’S LUCIA DI LAMMERMOOR 


WEBER’S OBERON


ROSSINI’S IL BARBIERE 


DONIZETTI’S LUCREZIA BORGIA 


MOZART’S NOZZE DI FIGARO 


VERDI’S RIGOLETTO 


BELLINI’S LA SONNAMBULA 


WEBER'S DER FREISCHUTZ 


WAGNER’S TANNHAUSER 


AUBER’S MASANIELLO 


BELLINI’S I PURITANI 


WAGNER’S LOHENGRIN 


DONIZETTI’S LA FIGLIA DEL REGGIMENTO 


ROSSINI’S GUILLAUME TELL 


MOZART’S DIE ZAUBERFLOTE 


VERDI'S LA TRAVIATA 


FLOTOW’S MARTHA 


VERDI’S ERNANI 


MOZART’S IL SERAGLIO 


GLUCK’S IPHIGENIA IN TAURIS


BAI


PART SONGS,) NEW SONGS 


BY VARIOUS COMPOSERS. OCTAVO SIZE. NOVELLO EWER AND CO 


ARNBY, |.—THOU WHOM MY 


OCTAVO EDITIONS OF


SERVICES BY MODERN COMPOSERS


PUBLISHED BY NOVELLO, EWER AND CO


LFRED H. LITTLETON.—THE MORNING 


T. BEST.—A MORNING, COMMUNION


BAPTISTE CALKIN.—MORNING, COMMU


R. JOHN B. DYKES.—A MORNING, COM


R. G. M. GARRETT.—A MORNING, COM


R. G. M. GARRETT.—A MORNING, COM- 


IR JOHN GOSS.—THE ORDER FOR THE 


LFRED H. LITTLETON.—THE EVENING 


ALTER MACFARREN.—A SIMPLE MORN


H. MONK.—THE OFFICE OF HOLY 


HUBERT H. PARRY.—A MORNING, COM- 


ENRY SMART.—A MORNING, COMMU- 


SERVICES BY MODERN 


R. J. STAINER.—A MORNING, COMMUNION


R. CHARLES STEGGALL. — A MORNING


S78 R. P. STEWART.—A MORNING, COM- 


H. THORNE.—A MORNING, COMMUNION 


ERTHOLD TOURS.—A MORNING, COM- 


HOMAS TALLIS TRIMNELL. — CHANT 


R.S. S. WESLEY.—A SHORT FULL CATHE- 


NEW EDITIONS (OCTAVO) OF


ORATORIOS, CANTATAS, 


BACH.—O LIGHT EVERLASTING 


FLAT 5-6


LONDON : NOVELLO, EWER AND CO


NANDADWDTOADAOS O


SECOND SERIES


A COLLECTION OF FOUR-PART SONGS AND MADRIGALS


PUBLISHED IN VOLUMES, OCTAVO SIZE, ELEGANTLY BOUND


BY MODERN COMPOSERS


OR IN SEPARATE NUMBERS, AS FOLLOWS


VOLUME I. CONTAINS


SIX SONGS BY 


SIR J. BENEDICT. 


SIX SONGS BY HENRY SMART


SEVEN SHAKSPERE SONGS BY 


G. A. MACFARREN


SIX SONGS BY J. L. HATTON. 


VOLUME II. CONTAINS— 


SIX SONGS BYG.A.MACFARREN


SIX SONGS BY C. A. MACIRONE. 


SIX SONGS BY HENRY LESLIE


SIX MADRIGALS BY VARIOUS 


COMPOSERS. 


VOLUME III. CONTAINS— 


SIX SONGS BY HENRY HILES


SIX SONGS BY 


FRANCESCO BERGER. 


SIX SONGS BY 


J. BAPTISTE CALKIN


S.A.T.B


EIGHT SONGS BY J. BARNBY. 


SIX SONGS BY G.A.MACFARREN. 


VOLUME IV. CONTAINS— 


SIX SONGS BY A. ZIMMERMANN


14


EIGHT SHAKSPERE SONGS BY 


G. A. MACFARREN


SIX SONGS BY HENRY LESLIE


SIX SONGS BY HENRY SMART


SIX SONGS BY SAMUEL REAY. 


VOLUME V. CONTAINS 


SIX SONGS BY A. S. SULLIVAN. 


SIX SONGS BY W. MACFARREN


SD


168 


169 


170 


171 


172 


173


SIX SONGS BY J. LEMMENS


SIX SONGS BY HENRY SMART


SIX SONGS BY CIRO PINSUTI


VOLUME VI. CONTAINS— 


THIRTY-FIVE SONGS BY 


J. L. HATTON


VOLUME VII. CONTAINS


TWENTY-FOUR SONGS BY 


J. L. HATTON. 


FOR MEN’S VOICES


VOLUME VIII. CONTAINS


TWENTY-ONE SONGS BY 


HENRY SMART


VOLUME IX. CONTAINS


TWENTY-FOUR SONGS BY 


WALTER MACFARREN


VOLUME X. CONTAINS— 


SONGS AND MADRIGALS BY 


R. L. DE PEARSALL. 


VOLUME XI. CONTAINS— 


SONGS AND MADRIGALS BY 


R. L. DE PEARSALL


SONGS BY VARIOUS AUTHORS


LONDON: NOVELLO, EWER AND CO., 1, BERNERS STREET (W.), AND 35, POULTRY (E.C.) 


NEW YORK: J. L, PETERS, 843, BROADWAY


CHAPPELL & CO., 50, NEW BOND STREET, SOLE AGENTS


CHAPPELL & CO., 504, NEW BOND STREET, SOLE AGENTS


A NEW WORK FOR AMATEUR ORGANISTS


TWENTY NEW OFFERTORY SENTENCES


FOR FOUR VOICES, WITH ORGAN ACCOMPANIMENT. 


NEW WORK FOR SINGING CLASSES


CHAPPELL’S PENNY OPERATIC PART-SONGS


CHAPPELL & CO., 50, NEW BOND STREET, LONDON