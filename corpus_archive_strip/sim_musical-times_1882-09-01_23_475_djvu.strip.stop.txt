


 SINGING - CLASS CIRCULAR . 


 PUBLISHED MONTH 


 ATTACHING 


 . ER CONCERT KI DAN NING . 


 R. ROBERT HILTON , MR . MONTAGUE WORLOCK , DEVOTED EXCLUSIVELY 


 MR . SANTLEY . | REPORT FESTIVAL . 


 EN « \ \ 


 MOSES EGYPT " * MESSIAH 


 CELLANE “ OU S SE LECTION EVENING , : : 


 VICE - PRESIDENT 


 1 , 1882 


 XUM 


 EMBER , 1882 


 TINEC 


 TICES . 


 NITAC TR PT 


 4 V ‘ TI ATT 


 CHATS 4 


 ~LLAND 


 ET CO 


 MISS EVELYN 


 MORDAUNT 


 AVA 


 MR . VERNEY BI 


 AMF 


 TERPC 


 ) . 


 1 MO 


 JRCHILL SI 


 MULT 


 Q ATS 


 INOW 


 AR ICLE PUPIL 


 CONNECTION . 3S 


 5 PR : AC 1 ICE . 


 XUM 


 FI RAS?S REED , STRING , ! L FIFE 


 ALS FINE COLLECTION ¢ ITALIAN INSTRUMENT : 


 » « 2 3 4 4 » 


 P. , CONACHER COQO . , , | DARI EXHI 3878 


 } \ 1.Y 4 , | ’ 


 ) = VU ‘ \ 


 472 MUSICAL TIMES . — SEPTEMBER 1 , 1882 


 ICATED ) 


 MAJESTY QUEEN 


 LONDON : NOVELLO , EWER CO . LONDON : NOVELLO , EWER CO 


 XUM 


 RIK FO ! 


 URS 


 MENT 


 ANCES 


 MUSICAL TIMES 


 SINGING - CLASS CIRCULAR . 


 SEPTEMBER 1 , 1882 


 HAYDN BIOGRAPHY * 


 XUM 


 73 


 474 


 XUM 


 475 


 OS , 


 NI 


 XUM 


 476 


 GREAT COMPOSERS 


 XUM 


 S COM 


 477 


 XUM 


 478 


 XUM 


 10U 


 479 


 XZOLIAN MUSIC 


 “ OLIAN HARI 


 NI 


 XUM 


 480 


 XUM 


 481 


 RIESENHARFE 


 INVENTIONS SUGGESTIONS 


 XUM 


 1S 


 482 


 XUM 


 483 


 XUM 


 ISSESS 


 ‘ DS 


 484 


 BIRMINGHAM MUSICAL FESTIVAL 


 XUM 


 ALT 


 


 XUM 


 DEDICATED FRIEND , BERTHOLD TOURS 


 = SSS 2 


 XUM 


 XUM 


 KG 


 


 82 


 TS 


 XUM 


 XUM 


 XUM 


 492 


 HEREFORD MUSICAL FESTIVAL , 1882 


 ABRAHAM 


 ORATORIO 


 BERNHARD MOLIQUE 


 PASSIONS 


 ODE MUSIC WILLIAM COLLINS 


 ALICE MARY SMITH 


 SHUNAMMITE 


 SACRED CANTATA 


 SOLI VOICES , CHORUS , ORCHESTRA 


 


 GEORGE GARRETTOctavo , paper Cover , 3S 

 
 CHORAL FANTASIA 
 L. VAN BEETHOVEN 

 English Version NataLiaA MACFARREN 




 COMP 


 LONDON : NOVELLO , EWER CO 


 FESTIVAL 


 BRISTOL MUSICAL 


 1882 


 OCTOBER 


 JASON 


 DRAMATIC CANTATA SOLO 


 CHORUS , ORCHESTRA 


 PO 


 PUBLISHED 


 VOICES 


 EM WRITTEN 


 WILLIAM GRIST 


 A. C. MACKENZIE . 


 NOVELLO COLLECTION 


 - 


 EDITED 


 GEORGE C. MARTIN 


 SUB - ORGANIST ST , PAUL CATHEDRAL , 


 HANDEL FESTIVAL 


 SELECTION 


 1862 , 1874 , 1877 , 1880 . 


 SIXPENCE 


 CHORAL SOCIETIES . _ 


 MESSRS . NOVELLO , EWER & CO 


 HAVING 


 COPIES REMAINING 


 SELECTION MUSIC 


 CHORAL SOCIETIES 


 


 COMPETITION 


 


 


 K 


 NATIONAL MUSIC MEETING 


 CRYSTAL PALACE 1873 , 


 PREPARED SUPPLY 


 SIXPENCE 


 CHERUBIN 


 


 MENDELSSOH 


 EWER & CO 


 NOVELLO 


 - SONG BOOK 


 BOOK 


 11 , J. BAPTISTE CALKIN . 


 12 . J. BARNBY . 


 14 . A. ZIMMERMANN . 


 15 . G. A. MACFAKREN , 


 16 HENRY LESLIE 


 HENRY SMART . 


 LOVE HATH 


 LONDON : NOVELLO 


 OOK 


 - HENRY SMART 


 G. , MACFARREN , 


 C.A MACIRONE , 


 10 , FRANCESCO BERGER , 22 . SNRY ‘ SMART 


 SIXPENCE 


 NOVELLO 


 VILLAGE ORGANIST 


 CONTAINING 


 SEVENTY - VOLUNTARIES , 


 FATHE 


 XUM 


 493 


 “ PARSIFAL ” BAYREUTH . 


 ( SPECIAL CORRESPONDENT 


 XUM 


 494 


 1S 


 XUN 


 JENESS 


 1882 . 495 


 SINGERS ’ FESTIVAL HAMBURGH . 


 XUM 


 496 


 BRUSSELS FESTIVAL . 


 ( CORRESPONDENT . ) 


 FR 


 XUMbelieve , confidently relied 

 order bring toa fitting termination valuable 
 labours Mr. Nagel , retires active conduc- | 
 torship Dundee Amateur Choral Union end | 
 , - fifth year service , time | 
 celebrate - fifth anniversary Society , 
 committee decided hold Musical Festival 
 24th , 25th , 26th January , 1883 . cutlay | P 
 necessary carry scheme great , 
 circular addressed noblemen connected 
 th e district provosts neighbouring | 
 towns , asking patrons , num- | 
 ber gentlemen requesting serve com- 
 mittee , object securing financial success 
 undertaking . Gounod Oratorio , ‘ * ‘ Redemption , ” 
 performed concert ; second | 
 miscellaneous , include Beethoven Choral Symphony ; 
 devoted Handel Oratorio , ‘ ‘ Israel 
 Egypt , ” Mr. Henry Nagel conducting 
 concert , Mr. August Manns second . | 
 exception Mr. Joseph Maas , secured principal 
 tenor , vocalists announced ; orchestra 
 Glasgow Choral Union engaged 

 prospectus Wolverhampton Festival Choral | 
 Society announces Concerts season 1882 - 3 . 
 opening Concert , November 13 , Gounod new 
 Oratorio , ‘ ‘ Redemption , ” ’ performed , 
 principal vocalists Miss Mary Davies , Miss Damian 




 XUI 


 499 


 REVIEWS 


 QQ 


 XUM 


 UDC 181 


 500 


 FOREIGN NOTES 


 XUM 


 501 


 XUMAta General Meeting German Stage Society , 
 held month Munich , presidency Herr 
 von Hilsen ( director Berlin Opera ) , proposal 
 , , special commission 
 elected purpose procuring new translation 
 | libretto Mozart ‘ ‘ Don Giovanni ” uniform 
 use German operatic stage . existing German 
 translations work , known , 
 way Satisfactorily represent Italian original 

 Royal Opera Berlin recommenced perform- 
 ances 2oth ult . Beethoven “ ‘ Fidelio . ” new 
 opera , entitled ‘ ‘ Gudrun , ” August Klughardt , 
 novelties produced season 

 Herr Wachtel , phenomenal apparently inde- 
 fatigable tenor , appearing series 
 favourite véles Kroll’sche Theater Berlin . 
 veteran singer sixtieth year . Dr. Emil Krauss , 
 remembered London amateurs prominent 
 members late German Opera Company Drury 
 Lane , likewise giving series successful im- 
 personations establishment 

 

 Dieppe . — Classical Concert ( July 20 Overture , ‘ ‘ Midsummer 
 Night Dream ” ( Mendelssohn ) ; Trio oboes violoncello 
 ( Beethoven ) ; Scherzo ( L efebvre ) : Prelude ‘ Le Deluge ” ( Saint- 
 Saéns ) ; Scénes Pittoresques ( Massenet 

 CORRESPONDENCE . 
 LISZT * * DANTE ” SYMPHONY . 
 EDITOR ‘ ‘ MUSICAL TIMES 




 26 


 CORRESPONDENTS 


 BRIEF SUMMARY 0 COUNT 


 XUM 


 NEWS 


 XUM 


 504 


 EW , REVISED , ENLARGED EDITION 


 EW , REVISED , ENLARGED EDITION 


 RGANIST , CHOIRMASTER , BASS SINGER 


 CHAMBER ORGAN 


 EASY ANTHEM HARVEST FESTIVALS . 


 MONTH . } 


 MITH , ALICE MARY 


 ~ READY , NEW EDITIONS _ - 


 PARTS 


 4 MUSIC PIANOFORTE . 


 » 5 » SECULAR VOCAL MUSIC . 


 » 6 SACRED MUSIC LATIN WORDS 


 3 ERTHOLY 


 4 |POLONAISI . ; { 


 - T T 


 ) , . ~ : T 5 


 PAUL LEAUMON 


 CAPRICE ESPAGNOL 


 § 0US LE BALCON 


 NS 


 STRAL | 


 XUM 


 ENTREPRENEURS 


 CCNCERT 


 STEDM 


 MUSIC 


 PA — _ — = — _ _ 


 WORARVEST ANTHEMS & HYMNS.[A‘EY , AW 25 , BABYS ! ANTHEM 


 ELBE RT LOWE ’ 


 4 HOLY SI 


 PRICE PENNY . J. BAPTISTE CALKIN . 


 : EDWARD OXENFORD 


 JUBILEE CANTATA MAGNIFICAT & NUNC DIMETTIS 


 ( HARVEST CANTATA ) J. T. MU 


 XUM 


 508 


 MUSICAL TIMES 


 SEPTEMBER , 1882 


 1E FOLLOWING EDITIONS READY 


 UPPLEMENT 1 » CON INING 37 TUNES 


 MATCH EDITION 


 BOOK PSALM } LE VERSION 


 MIAB LE 1 THY DWELLINGS . 


 


 


 NAIN 


 CRED CANTATA 


 SOLI VOICES CHORUS 


 W IDOW 


 SAC 


 LO , EWER 


 EN TATE DOMIN 


 LC 


 LLO , EN 


 | > M . P 


 THEMS 4 


 POPULAR 


 CR AMENT 


 S HOUSE MA 


 Y MANSIONS . 


 J. MAUDE 


 7S , FOSOF 


 


 LORD , YE 


 THIRSTI 


 


 ’ J ] 1 


 TY PS OHR 24 


 ACCOMPANIMENT 


 510 


 DAVIS 


 SONGS . 


 ZINGARA 


 FAVOURITE SCOTCH SONGS 


 MUSICAL TIMES 


 MONARCH WINTER 


 MARCIA GIUBILANT ! ) 


 MARCH € MINOR 


 NEW SONG ( SOPRANO CONTRALTO 


 MIZPAH 


 V7 


 C0 


 ING WELCOME 


 STEPHEN S. STRATTON 


 CONDUCTORS CHOR AL SOCIETIE 


 NEW SONG 


 NGS 


 — LE 


 4 — |S 


 G2 | 


  % O.35 & 3 


 OIRS 


 SECOND SERIES 


 


 CANTIC LES HYMNS 


 R 


 EDITED 


 HARROW SCHOOL MUSIC 


 JOHN FARMER & 


 SINGING QUADRILLES 


 CHORAL SOCIETIES 


 NEW REVISED EDITION , COMPOSED 


 " QI